<?php
#this is only for display the result
function pr($data) {
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function get_excel_obj() {
    $ci = &get_instance();
    $ci->load->library('excel');
    return new $ci->excel;
}

function loggedData() {
    $ci = &get_instance();
    $data = $ci->session->userdata('loginData');
    //print_r($data);
 $sql = "select flag from tblusers where id_tblusers = ".$data->id_tblusers."";
        $content = $CI->query($sql)->result();   
        //echo $content[0]->flag; 
//print_r($content);
    //$data=  getSession(USR_SESSION_NAME);
    return $content;
}

function set_cell_width_auto(&$ex) {
    foreach (range('A', $ex->getActiveSheet()->getHighestDataColumn()) as $col) {
        $ex->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
    }
}

function set_cell_width(&$ex, $width_arr) {
    foreach (range('A', $ex->getActiveSheet()->getHighestDataColumn()) as $col) {
        $ex->getActiveSheet()->getColumnDimension($col)->setWidth($width_arr[$col]);
    }
}

function excel_download($filename, &$ex) {
    $filename = $filename . '.xls';
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $filename . '"');
    header('Cache-Control: max-age=0');
    $objWriter = PHPExcel_IOFactory::createWriter($ex, 'Excel5');
    $objWriter->save('php://output');
    //$objWriter->save(str_replace(__FILE__,$_SERVER['DOCUMENT_ROOT'] .'/assets/excel/'.$filename,__FILE__));
}

function set_page_heading($str, &$ex) {
    $ex->setActiveSheetIndex(0);
    $strlen = strlen($str);
    if ($strlen > 30) {
        $ex->getActiveSheet()->setTitle('Report');
    } else {
        $ex->getActiveSheet()->setTitle($str);
    }
    $ex->getActiveSheet()->setCellValue('A1', $str);
    $ex->getActiveSheet()->getStyle('A1')->getFont()->setSize(15)->setBold(TRUE);
    $ex->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DCDCDC');
}

function excel_download_email($filename, &$ex) {

    $filename = $filename . '.xls'; 
    /*header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $filename . '"');
    header('Cache-Control: max-age=0');*/
    $objWriter = PHPExcel_IOFactory::createWriter($ex, 'Excel5');
    return $objWriter->save(str_replace(__FILE__,getcwd().'/assets/excel/'.$filename,__FILE__));


}


function multiple_set_page_heading(&$ex, $str, $title, $sheet = 0) {
    $ex->setActiveSheetIndex($sheet);
    $ex->getActiveSheet()->setTitle($title);
    $ex->getActiveSheet()->setCellValue('A1', $str);
    $ex->getActiveSheet()->getStyle('A1')->getFont()->setSize(15)->setBold(TRUE);
    $ex->getActiveSheet()->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DCDCDC');
}

function set_table_heading($thead, $startfrom = '3', &$ex,$highestcol=null) {
    $ex->getActiveSheet()->fromArray($thead, null, 'A' . $startfrom);
    $highestCol = ($highestcol==null)?$ex->getActiveSheet()->getHighestColumn():$highestcol;
    $ex->getActiveSheet()->getStyle("A{$startfrom}:{$highestCol}{$startfrom}")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('337ab7');
    $styleArray = array('font' => array(
        'bold' => true,
        'color' => array('rgb' => 'FFFFFF'),
    ),
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            //'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    )
);
    $ex->getActiveSheet()->getStyle("A{$startfrom}:{$highestCol}{$startfrom}")->applyFromArray($styleArray); //getFont()->setBold(TRUE);
    $highestCol = $ex->getActiveSheet()->getHighestColumn();
    $ex->getActiveSheet()->mergeCells("A1:{$highestCol}1");
}

function set_table_data($data, $startfrom = '4', &$ex,$highestcol=null) {
    $ex->getActiveSheet()->fromArray($data, null, 'A' . $startfrom);
    $highestCol = ($highestcol==null)?$ex->getActiveSheet()->getHighestColumn():$highestcol;
    $styleArray = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN
            )
        )
    );
    $ex->getActiveSheet()->getStyle(
        "A{$startfrom}:" .
        $highestCol .
        $ex->getActiveSheet()->getHighestRow()
    )->applyFromArray($styleArray);
}

function set_table_note(&$ex, $msg, $position) {
    $ex->getActiveSheet()->setCellValue($position, $msg);
    $ex->getActiveSheet()->getStyle($position)->getFont()->setSize(10); //->setSuperScript(true);
}

function set_table_heading_width($start, $ex) {
    $end = $ex->getActiveSheet()->getHighestColumn();
    for ($col = $start; $col <= $end; $col++) {
        $ex->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
    }
}

function m_set_table_heading_width($start, $ex) {
    $end = 'Y'; //$ex->getActiveSheet()->getHighestColumn();
    for ($col = $start; $col <= $end; $col++) {
        $ex->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
    }
}
//set row text bold
function set_table_rw_bold(&$ex,$startfrom = '3') {
    //$ex->getActiveSheet()->fromArray($thead, null, 'A' . $startfrom);
    $highestCol = $ex->getActiveSheet()->getHighestColumn();
    $styleArray = array('font' => array(
        'bold' => true,
    )
);
    $ex->getActiveSheet()->getStyle("A{$startfrom}:{$highestCol}{$startfrom}")->applyFromArray($styleArray); 
}

function set_cell_font_color(&$ex,$cell){
    $styleArray = array(
        'font'  => array(
            'bold'  => true,
            'color' => array('rgb' => '337ab7'),
        )
    );
    $ex->getActiveSheet()->getStyle($cell)->applyFromArray($styleArray);
}
function set_col_color(&$ex,$range){
    $ex->getActiveSheet()->getStyle($range)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f2dede');
}

function set_thin_border(&$ex,$range){
    $BStyle = array(
        'borders' => array(
          'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
      )
    );
    $ex->getActiveSheet()->getStyle($range)->applyFromArray($BStyle);
}

function empty_file($name){ 
    $ex = get_excel_obj();
    $ex->disconnectWorksheets();
    $ex->createSheet();
    $fileName = $name;
    excel_download_email($fileName, $ex);
    return $fileName;    
    
}

function isDateBlank($date) {
    $date = trim($date);
    if (!$date || $date == "" || $date == "0000-00-00" || $date == "0000-00-00")
        return true;
}

function timeStamp($T) {
    $T = str_replace('/', '-', $T);
    $T = str_replace('/', '-', $T);
    if (!$T)
        return '';

    if (!is_numeric($T) && isDateBlank($T))
        return '';

    if (!is_numeric($T)) 
        $Tsmall = explode('-',$T); 
        if(strlen($Tsmall[2])==2){ $Tsmall[2] = '20'.$Tsmall[2]; } 
        $Tdate = implode('-',$Tsmall);
        $Tdateval = strtotime($Tdate);
    return date('Y-m-d', $Tdateval);
}


function timeStampShow($T) {
    $T = str_replace('/', '-', $T);
    $T = str_replace('/', '-', $T);
    if (!$T)
        return '';

    if (!is_numeric($T) && isDateBlank($T))
        return '';

    if (!is_numeric($T)) 
      
    //Convert it into a timestamp.
    $timestamp = strtotime($T);
     
    //Convert it to DD-MM-YYYY
    $dmy = date("d-m-Y", $timestamp);
        return $dmy;
}

function timeStampD($T) {
    $T = str_replace('/', '-', $T);
    $T = str_replace('/', '-', $T);
    if (!$T)
        return '';

    if (!is_numeric($T) && isDateBlank($T))
        return '';

    if (!is_numeric($T)) 
      
    //Convert it into a timestamp.
    $timestamp = strtotime($T);
     
    //Convert it to DD-MM-YYYY
    $dmy = date("Y-m-d", $timestamp);
        return $dmy;
}

function addSlash($data) {
    if (is_array($data)) {
        $inf = array();
        foreach ($data as $field => $val) {
            if (!is_array($val))
                $inf[$field] = addslashes($val);
            else
                $inf[$field] = $val;
        }
    } else
        $inf = addslashes($data);

    return $inf;
}
function arrayTrim($arr) {
    if (!$arr){
        return $arr;
    }    
    foreach ($arr as &$v) {
        if (!is_array($v)){
            $v = trim($v);
        }
        else{
            arrayTrim($v);
        }
    }
    return $arr;
}

function arrayUrlDecode($arr) {
    foreach ($arr as &$v) {
        if (!is_array($v))
            $v = urldecode($v);
    }
    return $arr;
}

?>