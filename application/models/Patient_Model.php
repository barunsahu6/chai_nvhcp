<?php 
class Patient_model extends MY_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_Model');
	}

	public function getRawTable($tableName = null)
	{
		$tableData = new stdclass;
		$fields = $this->db->list_fields($tableName);
		foreach ($fields as $field)
		{
			$tableData->{$field} = null;
		}

		return $tableData;
	}

	public function loadPatients()
	{

		/*print_r($_POST); die();*/
		
		$this->db->where('id_mstfacility', $this->input->post('id_mstfacility'));
		if($this->input->post('UID_Num') !== null){
			$this->db->where('UID_Num', $this->input->post('UID_Num'));
		}
		$patientres = $this->db->get('tblpatient');
		if($patientres)
		{
			$content['Patient_List'] = $patientres->result();
		}
		else
		{
			$content['Patient_List'] = array();
		}

		return $content;

	}

	public function loadPatientsFromSession()
	{
		$patientFilters = $this->session->userdata('patientFilters');
		$this->db->where('id_mststate', $patientFilters['id_mststate']);
		$this->db->where('id_mstdistrict', $patientFilters['id_mstdistrict']);
		$this->db->where('id_mstfacility', $patientFilters['id_mstfacility']);

		if($patientFilters['UID_Num'] !== null){

			$this->db->where('UID_Num', $patientFilters['UID_Num']);

		}

		$res = $this->db->get('mstfacility');
		if($res){
			$facilityRow = $res->result();

			$facilityId = $facilityRow[0]->id_mstfacility;
			$this->db->where('id_mstfacility', $facilityId);

			$patientRes = $this->db->get('tblpatient');
			if($patientRes){

				$content['Patient_List'] = $patientRes->result();					
			}
		}else{
			$content['Patient_List'] = array();
		}

		return $content;

	}

	public function loadPatientDetails()
	{ 
		$this->db->trans_start();

		$patient_id = $this->input->post('patient_id');
		$query = "select * from tblpatient where id_tblpatient = '".$patient_id."'";
		$res = $this->db->query($query);
		$resArr = $res->result();
		if(count($resArr)>0){
			
			$content['tblpatient'] = $res->result()[0];
		}
		
		$query = "select * from tblpatientaddltest where PatientID = ?";
		$res = $this->db->query($query, $this->input->post('patient_id'));
		$content['tblpatientaddltest'] = $res->result();

		$query = "select * from tblpatientcirrohosis where PatientID = ?";
		$res = $this->db->query($query, $this->input->post('patient_id'));
		$content['tblpatientcirrohosis'] = $res->result();
			// print_r($content['tblpatientcirrohosis']); die();

		$query = "select * from tblpatientfu where PatientID = ?";
		$res = $this->db->query($query, $this->input->post('patient_id'));
		$content['tblpatientfu'] = $res->result();

		$query = "select * from tblpatientvisit where PatientID = ?";
		$res = $this->db->query($query, $this->input->post('patient_id'));
		$content['tblpatientvisit'] = $res->result();
			//print_r($content['tblpatientvisit']); die();

		$query = "select * from mstfacility where id_mstfacility = ".$content['tblpatient']->id_mstfacility ;
		$facility = $this->Common_Model->query_data($query);

		$query = "select * from mststate where id_mststate = ".$facility[0]->id_mststate;
		$content['mststate'] = $this->Common_Model->query_data($query);

		$query = "select * from mstdistrict where id_mststate = ".$facility[0]->id_mststate;
		$content['District'] = $this->Common_Model->query_data($query);

		$query = "select * from mstfacility where id_mstdistrict = ".$facility[0]->id_mstdistrict;
		$content['Facility'] = $this->Common_Model->query_data($query);

		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE)
		{
			return "Error in prepareing patient details data";					
		}
		// print_r($content); die();
		return $content;

	}

	public function loadDummyPatientDetails()
	{
		$content['tblpatient'] = $this->getRawTable('tblpatient');
		$content['tblpatientaddltest'][0] = $this->getRawTable('tblpatientaddltest');
		$content['tblpatientcirrohosis'][0] = $this->getRawTable('tblpatientcirrohosis');
		$content['tblpatientfu'][0] = $this->getRawTable('tblpatientfu');
		$content['tblpatientvisit'][0] = $this->getRawTable('tblpatientvisit');
		return $content;
	}

	public function prepareMasterData()
	{
		$this->db->trans_start();

		$query = "select * from mststate";
		$content['mststate'] = $this->Common_Model->query_data($query);

		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			return 'Error Preparing Master Data';					
		}

		return $content;
	}

	public function getLocationDets()
	{
		$query = "select * from mststate";
		$content['mststate'] = $this->Common_Model->query_data($query);

		$id_mststate = $this->input->post('id_mststate');
		$query = "select * from mstdistrict where id_mststate = $id_mststate";
		$content['District'] = $this->Common_Model->query_data($query);

		$id_mstdistrict = $this->input->post('id_mstdistrict');
		$query = "select * from mstfacility where id_mstdistrict = $id_mstdistrict";
		$content['Facility'] = $this->Common_Model->query_data($query);

		return $content;
	}

	public function loadPatientsFromFacility($id_mstfacility)
	{

		$patientFilters = $this->session->userdata('patientFilters');
		if($this->input->post('UID_Num') !=  null){
			$uid_and = "and lpad(UID_Num,6,'0') = ".str_pad($this->input->post('UID_Num'),6,'0',STR_PAD_LEFT);
		}else if ($patientFilters["UID_Num"] != "") {
			$uid_and = "and lpad(UID_Num,6,'0') = ".str_pad($patientFilters["UID_Num"],6,'0',STR_PAD_LEFT);
		}
		else
		{
			$uid_and = "";
		}

		$query = "select p.*, l.LookupValue from tblpatient p inner join mstlookup l on p.Status = l.LookupCode where p.id_mstfacility = ".$id_mstfacility." and l.Flag = 13 and l.LanguageID = 1 ".$uid_and." order by p.UID_Num";
		
		// echo $query; die();

		$content['patients'] = $this->Common_Model->query_data($query);
		$content['id_facility'] = $id_mstfacility;

		return $content;
	}

	public function savePatientFilters()
	{

			// set the state, district and facility in session flash data
		$patientFilters = array(
			'id_mststate'	=>	$this->input->post('id_mststate'),
			'id_mstdistrict'	=>	$this->input->post('id_mstdistrict'),
			'id_mstfacility'	=>	$this->input->post('id_mstfacility'),
			'UID_Num'					=>	$this->input->post('UID_Num'),
			);

		$this->session->set_userdata('patientFilters', $patientFilters);
	}
}