<?php 

class Admin_chartreports_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

//print_r($this->session->userdata('filters'));
		$loginData = $this->session->userdata('loginData');
		if($loginData->user_type==1){
			 $this->facility_filterid = " AND 1";
			 $this->facility_filteridp = " AND 1";
			
		}

		if($loginData->user_type==2){
			$this->facility_filterid = "AND s.Session_StateID = ".$loginData->State_ID." AND s.id_mstfacility = ".$loginData->id_mstfacility;

			$this->facility_filteridp = "AND p.Session_StateID = ".$loginData->State_ID." AND p.id_mstfacility = ".$loginData->id_mstfacility;
			
		}
		elseif($loginData->user_type==3){
			$this->facility_filterid = " AND s.Session_StateID = ".$loginData->State_ID;
			$this->facility_filteridp = " AND p.Session_StateID = ".$loginData->State_ID;
			
		}
		elseif($loginData->user_type==4){
			$this->facility_filterid = " AND s.Session_StateID = ".$loginData->State_ID." AND s.Session_DistrictID = ".$loginData->DistrictID;
			$this->facility_filteridp = " AND p.Session_StateID = ".$loginData->State_ID." AND p.Session_DistrictID = ".$loginData->DistrictID;
			
		}
		

		if($this->session->userdata('filters') != null)
		{
			$filters = $this->session->userdata('filters');

			if($filters['id_mstfacility'] == 0)
			{
				$this->facility_filter = "1";
			}
			else
			{
				$this->facility_filter = " tblsummary_new.id_mstfacility = ".$filters['id_mstfacility'];
			}

	

			$this->date_filter = " (case when tblsummary_new.date < '2016-06-18' then '2016-06-18' else tblsummary_new.date end) between '".$filters['startdate']."' and '".$filters['enddate']."'";
		}

	}

	public function cascade()
	{

		$filters = $this->session->userdata('filters');
		
		$query = "SELECT
								    f.facility_name AS hospital,
								    SUM(s.cascade_antibody_positive) AntibodyPositive,
								    SUM(s.cascade_chronic_hcv) AS ChronicHIV,
								    SUM(s.cascade_treatment_initiated) AS TreatmentInitiated,
								    SUM(s.cascade_treatment_completed) AS TreatmentCompleted,
								    SUM(s.cascade_svr_test_done) AS SVRTestDone,
								    SUM(s.cascade_svr_achieved) AS TreatmentSuccessful
								FROM
								    (
								    SELECT
								        *
								    FROM
								        mstfacility
								    WHERE
								        id_mstfacility BETWEEN 1 AND 25
								) f
								LEFT JOIN `tblsummary_new` s ON
								    s.id_mstfacility = f.id_mstfacility
								WHERE
								    (
								        CASE WHEN s.date < '2016-06-18' THEN '2016-06-18' ELSE s.date
								    END 
								) BETWEEN '".$filters['startdate']."' AND '".$filters['enddate']."'  ".$this->facility_filterid."
								GROUP BY
								    s.id_mstfacility";
		
		$result = $this->db->query($query)->result();

		return $result;
	}

	public function district_wise_cirrhosis_no_cirrhosis($facility_wise = NULL)
	{
		$filters = $this->session->userdata('filters');

		if($facility_wise == 1)
		{
			$group_by = " group by id_mstfacility order by id_mstfacility";
			$fields = 's.id_mstfacility,';
			$$filters['startdate'] = '2016-06-18';
			$filters['enddate'] = date('Y-m-d');
			//$category =  " and s.category ='0' ";
			

		}

		$query = "SELECT
									$fields
								    f.facility_name AS facility_short_name,
								    IFNULL(SUM(cirrhosis_status_yes),
								    0) AS Cir,
								    IFNULL(SUM(cirrhosis_status_no),
								    0) AS Noncir
								FROM
								    (
								    SELECT
								        *
								    FROM
								        mstfacility
								    WHERE
								        id_mstfacility BETWEEN 1 AND 25
								) f 
								LEFT JOIN `tblsummary_new` s ON
								    s.id_mstfacility = f.id_mstfacility
								WHERE
								    (
								        CASE WHEN s.date < '2016-06-18' THEN '2016-06-18' ELSE s.date
								    END
										) BETWEEN '".$filters['startdate']."' AND '".$filters['enddate']."' ".$this->facility_filterid."
								GROUP BY
								    s.id_mstfacility";
		$result = $this->db->query($query)->result();

		return $result;
	}

	public function district_wise_genotype()
	{
		$filters = $this->session->userdata('filters');

		$query = "SELECT
								    f.facility_name AS facility_short_name,
								    IFNULL(SUM(s.genotype_1),
								    0) AS gen1,
								    IFNULL(SUM(s.genotype_2),
								    0) AS gen2,
								    IFNULL(SUM(s.genotype_3),
								    0) AS gen3,
								    IFNULL(SUM(s.genotype_4),
								    0) AS gen4,
								    IFNULL(SUM(s.genotype_5),
								    0) AS gen5,
								    IFNULL(SUM(s.genotype_6),
								    0) AS gen6
								FROM
								    (
								    SELECT
								        *
								    FROM
								        mstfacility
								    WHERE
								        id_mstfacility BETWEEN 1 AND 25
										) f
								LEFT JOIN tblsummary_new s ON
								    s.id_mstfacility = f.id_mstfacility
								WHERE
								    (
								        CASE WHEN s.date < '2016-06-18' THEN '2016-06-18' ELSE s.date
								    END
								) BETWEEN '".$filters['startdate']."' AND '".$filters['enddate']."'  ".$this->facility_filterid."
								GROUP BY
								    s.id_mstfacility";

		$result = $this->db->query($query)->result();		

		return $result;
	}

		public function district_wise_age()
	{
		$filters = $this->session->userdata('filters');

		$query = "SELECT
								    f.facility_name AS facility_short_name,
								    IFNULL(SUM(s.age_less_than_10),
								    0) AS aa,
								    IFNULL(SUM(s.age_10_to_20),
								    0) AS bb,
								    IFNULL(SUM(s.age_21_to_30),
								    0) AS cc,
								    IFNULL(SUM(s.age_31_to_40),
								    0) AS dd,
								    IFNULL(SUM(s.age_41_to_50),
								    0) AS ee,
								    IFNULL(SUM(s.age_51_to_60),
								    0) AS ff,
								    IFNULL(SUM(s.age_greater_than_60),
								    0) AS gg
								FROM
								    (
								    SELECT
								        *
								    FROM
								        mstfacility
								    WHERE
								        id_mstfacility BETWEEN 1 AND 25
								) f
								LEFT JOIN tblsummary_new s ON
								    s.id_mstfacility = f.id_mstfacility
								WHERE
								    (
								        CASE WHEN s.date < '2016-06-18' THEN '2016-06-18' ELSE s.date
								    END
								) BETWEEN '".$filters['startdate']."' AND '".$filters['enddate']."'  ".$this->facility_filterid."
								GROUP BY
								    s.id_mstfacility";

		$result = $this->db->query($query)->result();		

		return $result;
	}

	public function district_wise_gender()
	{
		$filters = $this->session->userdata('filters');

		$query = "SELECT
								    f.facility_name AS facility_short_name,
								    IFNULL(
								        SUM(gender_area_male_urban),
								        0
								    ) AS um,
								    IFNULL(
								        SUM(gender_area_male_rural),
								        0
								    ) AS rm,
								    IFNULL(
								        SUM(gender_area_female_urban),
								        0
								    ) AS uf,
								    IFNULL(
								        SUM(gender_area_female_rural),
								        0
								    ) AS rf,
								    IFNULL(
								        SUM(gender_area_trans_urban),
								        0
								    ) AS ut,
								    IFNULL(
								        SUM(gender_area_trans_rural),
								        0
								    ) AS rt
								FROM
								    (
								    SELECT
								        *
								    FROM
								        mstfacility
								    WHERE
								        id_mstfacility BETWEEN 1 AND 25
								) f
								LEFT JOIN tblsummary_new s ON
								    s.id_mstfacility = f.id_mstfacility
								WHERE
								    (
								        CASE WHEN s.date < '2016-06-18' THEN '2016-06-18' ELSE s.date
								    END
								) BETWEEN '".$filters['startdate']."' AND '".$filters['enddate']."'  ".$this->facility_filterid."
								GROUP BY
								    s.id_mstfacility";

		$result = $this->db->query($query)->result();	

		return $result;
	}

	public function district_wise_adherence()
	{
		$filters = $this->session->userdata('filters');

		$query = "SELECT
								    f.facility_name AS facility_short_name,
								    AVG(
								        CASE WHEN s.adh_v2 = 0 OR s.adh_v2 IS NULL THEN NULL ELSE s.adh_v2
								    END
								) AS v2,
								AVG(
								    CASE WHEN s.adh_v3 = 0 OR s.adh_v3 IS NULL THEN NULL ELSE s.adh_v3
								END
								) AS v3,
								AVG(
								    CASE WHEN s.adh_v4 = 0 OR s.adh_v4 IS NULL THEN NULL ELSE s.adh_v4
								END
								) AS v4,
								AVG(
								    CASE WHEN s.adh_v5 = 0 OR s.adh_v5 IS NULL THEN NULL ELSE s.adh_v5
								END
								) AS v5,
								AVG(
								    CASE WHEN s.adh_v6 = 0 OR s.adh_v6 IS NULL THEN NULL ELSE s.adh_v6
								END
								) AS v6,
								AVG(
								    CASE WHEN s.adh_v7 = 0 OR s.adh_v7 IS NULL THEN NULL ELSE s.adh_v7
								END
								) AS v7,
								AVG(
								    CASE WHEN(
								        s.adh_v2 IS NULL AND s.adh_v3 IS NULL AND s.adh_v4 IS NULL AND s.adh_v5 IS NULL AND s.adh_v6 IS NULL
								    ) OR s.adh_overall = 0 OR s.adh_overall IS NULL THEN NULL ELSE s.adh_overall
								END
								) AS overall
								FROM
								    (
								    SELECT
								        *
								    FROM
								        mstfacility
								    WHERE
								        id_mstfacility BETWEEN 1 AND 25
								) f
								LEFT JOIN tblpatient_tat_summary s ON
								    s.id_mstfacility = f.id_mstfacility 
								GROUP BY
								    s.id_mstfacility";

		$result = $this->db->query($query)->result();	

		return $result;
	}

		public function district_wise_risk($facility_wise = NULL)
				{
				$filters = $this->session->userdata('filters');

				if($facility_wise == 1)
				{
				$filters['startdate']= '2016-06-18';
				$filters['enddate'] = date('Y-m-d');

				//$this->category_filter=  "s.category ='0'";

				}

		$query = "SELECT
								    f.facility_name AS facility_short_name,
								    IFNULL(
								        SUM(s.risk_factor_unsafe_injection),
								        0
								    ) AS Unsafe_Injection_Use,
								    IFNULL(SUM(s.risk_factor_ivdu),
								    0) AS IVDU,
								    IFNULL(
								        SUM(s.risk_factor_unprotected_sex),
								        0
								    ) AS Unprotected_Sexual_Practice,
								    IFNULL(SUM(s.risk_factor_surgery),
								    0) AS Surgery,
								    IFNULL(SUM(s.risk_factor_dental),
								    0) AS Dental,
								    IFNULL(SUM(s.risk_factor_others),
								    0) AS Others,
								    IFNULL(
								        SUM(s.risk_factor_not_available),
								        0
								    ) AS not_available
								FROM
								    (
								    SELECT
								        *
								    FROM
								        mstfacility
								    WHERE
								        id_mstfacility BETWEEN 1 AND 25
								) f
								LEFT JOIN tblsummary_new s ON
								    s.id_mstfacility = f.id_mstfacility
								WHERE
								    (
								        CASE WHEN s.date < '2016-06-18' THEN '2016-06-18' ELSE s.date
								    END
								) BETWEEN '".$filters['startdate']."' AND '".$filters['enddate']."'  ".$this->facility_filterid."
								GROUP BY
								    s.id_mstfacility";

		$result = $this->db->query($query)->result();	

		return $result;
	}

	public function tat()
	{
		$filters = $this->session->userdata('filters');

		$query = "SELECT
					    (case CirrhosisStatus when 1 then 'Cirrhosis' when 2 then 'No-Cirrhosis' end) as CirrhosisStatus,
					    AVG(
					        CASE WHEN antibody_hcv <= 0 OR antibody_hcv IS NULL THEN NULL ELSE antibody_hcv
					    END
					) AS one,
					AVG(
					    CASE WHEN hcv_initiation <= 0 OR hcv_initiation IS NULL THEN NULL ELSE hcv_initiation
					END
					) AS two,
					AVG(
					    CASE WHEN initiation_completion <= 0 OR initiation_completion IS NULL THEN NULL ELSE initiation_completion
					END
					) AS three,
					AVG(
					    CASE WHEN completion_svr <= 0 OR completion_svr IS NULL THEN NULL ELSE completion_svr
					END
					) AS four
					FROM
					    `tblpatient_tat_summary` t
					INNER JOIN tblpatient p ON
					    t.patientguid = p.PatientGUID
					WHERE
					    CirrhosisStatus >= 1  ".$this->facility_filteridp."
					GROUP BY
					    CirrhosisStatus";

			$result = $this->db->query($query)->result();	

			return $result;
	}

		public function district_wise_trend()
	{
		$filters = $this->session->userdata('filters');

		$query = "SELECT
								    f.facility_name AS hospital,
								    IFNULL(
								        SUM(
								            s.cascade_treatment_initiated
								        ),
								        0
								    ) AS countIni,
								    'rgba(54, 162, 235, 0.6)' AS color
								FROM
								    (
								    SELECT
								        *
								    FROM
								        mstfacility
								    WHERE
								        id_mstfacility != 26
								) f
								LEFT JOIN `tblsummary_new` s ON s.id_mstfacility = f.id_mstfacility
								WHERE
								    (
								        CASE WHEN s.date < '2016-06-18' THEN '2016-06-18' ELSE s.date
								    END
								) BETWEEN '".$filters['startdate']."' AND '".$filters['enddate']."'  ".$this->facility_filterid."
								GROUP BY
								    s.id_mstfacility";

		$result = $this->db->query($query)->result();	

		return $result;
	}

		public function monthly()
	{
		$filters = $this->session->userdata('filters');

		$query = "SELECT
					    YEAR(tblsummary_new.date) AS year,
					    MONTH(tblsummary_new.date) AS month,
					    LEFT(MONTHNAME(tblsummary_new.date),
					    3) AS monthname,
					    IFNULL(
					        SUM(`cascade_treatment_initiated`),
					        0
					    ) AS count
					FROM
					    `tblsummary_new`
					WHERE
					    DATE < '".date('Y-m-d')."' and id_mstfacility between 1 and 25   
					GROUP BY
					    YEAR(tblsummary_new.date),
					    MONTH(tblsummary_new.date)
					ORDER BY
					    YEAR(tblsummary_new.date)
					DESC
					    ,
					    MONTH
					DESC
					LIMIT 12";

		$result = $this->db->query($query)->result();	

		return $result;
	}

	public function district_wise_occupation()
	{
		$query = "SELECT
								    f.facility_name AS hospital,
								    IFNULL(
								        sum(s.occupation_health_care_worker),
								        0
								    ) AS health_care_worker,
								    IFNULL(sum(s.occupation_dental_worker), 0) AS dental_worker,
								    IFNULL(sum(s.occupation_lab_tech), 0) AS lab_tech,
								    IFNULL(sum(s.occupation_barber), 0) AS barber,
								    IFNULL(sum(s.occupation_cleaner), 0) AS cleaner,
								    IFNULL(sum(s.occupation_truck_driver), 0) AS truck_driver,
								    IFNULL(sum(s.occupation_farmer), 0) AS farmer,
								    IFNULL(sum(s.occupation_labourer), 0) AS laboures,
								    IFNULL(sum(s.occupation_service), 0) AS service,
								    IFNULL(sum(s.occupation_other), 0) AS other
								FROM
								    (
								    SELECT
								        *
								    FROM
								        `mstfacility`
								    WHERE
								        id_mstfacility BETWEEN 1 AND 25
								) f
								LEFT JOIN tblsummary_new s ON
								    s.id_mstfacility = f.id_mstfacility 
								GROUP BY
								    f.id_mstfacility";

		$result = $this->db->query($query)->result();

		return $result;
	}

	public function district_wise_regimen()
	{
		$filters = $this->session->userdata('filters');

		$query = "SELECT
								    f.facility_name,
								    a.count AS regimen1,
								    b.count AS regimen2,
								    c.count AS regimen3,
								    d.count AS regimen4,
								    e.count AS regimen5,
								    ff.count AS regimen6
								FROM
								    (select * from mstfacility where id_mstfacility between 1 and 25 ) f
								LEFT JOIN(
								    SELECT
								        COUNT(patientguid) AS COUNT,
								        id_mstfacility
								    FROM
								        `tblpatient`
								    WHERE T_Initiation IS NOT NULL AND
								        T_Regimen = 1 
								    GROUP BY
								        id_mstfacility
								) a
								ON
								    f.id_mstfacility = a.id_mstfacility
								LEFT JOIN(
								    SELECT
								        COUNT(patientguid) AS COUNT,
								        id_mstfacility
								    FROM
								        `tblpatient`
								    WHERE T_Initiation IS NOT NULL AND
								        T_Regimen = 2
								    GROUP BY
								        id_mstfacility
								) b
								ON
								    f.id_mstfacility = b.id_mstfacility
								LEFT JOIN(
								    SELECT
								        COUNT(patientguid) AS COUNT,
								        id_mstfacility
								    FROM
								        `tblpatient`
								    WHERE T_Initiation IS NOT NULL AND
								        T_Regimen = 3
								    GROUP BY
								        id_mstfacility
								) c
								ON
								    f.id_mstfacility = c.id_mstfacility
								LEFT JOIN(
								    SELECT
								        COUNT(patientguid) AS COUNT,
								        id_mstfacility
								    FROM
								        `tblpatient`
								    WHERE T_Initiation IS NOT NULL AND
								        T_Regimen = 4
								    GROUP BY
								        id_mstfacility
								) d
								ON
								    f.id_mstfacility = d.id_mstfacility
								    LEFT JOIN(
								    SELECT
								        COUNT(patientguid) AS COUNT,
								        id_mstfacility
								    FROM
								        `tblpatient`
								    WHERE T_Initiation IS NOT NULL AND
								        T_Regimen = 5
								    GROUP BY
								        id_mstfacility
								) e
								ON
								    f.id_mstfacility = e.id_mstfacility

								    LEFT JOIN(
								    SELECT
								        COUNT(patientguid) AS COUNT,
								        id_mstfacility
								    FROM
								        `tblpatient`
								    WHERE T_Initiation IS NOT NULL AND
								        T_Regimen = 6
								    GROUP BY
								        id_mstfacility
								) ff
								ON
								    f.id_mstfacility = ff.id_mstfacility

								    ";

			$result = $this->db->query($query)->result();		

			return $result;
	}

	public function facility_calloutput()
	{
		$filters = $this->session->userdata('filters');
		$query = "SELECT
								    a.id_mstfacility,
								    a.facility_name,
								    IFNULL(b.c1, 0) AS c1,
								    IFNULL(c.c1, 0) AS c2,
								    IFNULL(d.c1, 0) AS c3,
								    IFNULL(e.c1, 0) AS c4,
								    IFNULL(f.c1, 0) AS c5,
								    IFNULL(g.c1, 0) AS c6,
								    IFNULL(h.c1, 0) AS c7,
								    IFNULL(i.c1, 0) AS c8,
								    IFNULL(j.c1, 0) AS c9,
								    IFNULL(k.c1, 0) AS c10
								FROM
								    mstfacility a
								LEFT JOIN(
								    SELECT
								        id_mstfacility,
								        COUNT(*) AS c1
								    FROM
								        tblSVRfollowup
								    WHERE
								        CallOutput = 1
								    GROUP BY
								        id_mstfacility
								) b
								ON
								    a.id_mstfacility = b.id_mstfacility
								LEFT JOIN(
								    SELECT
								        id_mstfacility,
								        COUNT(*) AS c1
								    FROM
								        tblSVRfollowup
								    WHERE
								        CallOutput = 2
								    GROUP BY
								        id_mstfacility
								) c
								ON
								    a.id_mstfacility = c.id_mstfacility
								LEFT JOIN(
								    SELECT
								        id_mstfacility,
								        COUNT(*) AS c1
								    FROM
								        tblSVRfollowup
								    WHERE
								        CallOutput = 3
								    GROUP BY
								        id_mstfacility
								) d
								ON
								    a.id_mstfacility = d.id_mstfacility
								LEFT JOIN(
								    SELECT
								        id_mstfacility,
								        COUNT(*) AS c1
								    FROM
								        tblSVRfollowup
								    WHERE
								        CallOutput = 4
								    GROUP BY
								        id_mstfacility
								) e
								ON
								    a.id_mstfacility = e.id_mstfacility
								LEFT JOIN(
								    SELECT
								        id_mstfacility,
								        COUNT(*) AS c1
								    FROM
								        tblSVRfollowup
								    WHERE
								        CallOutput = 5
								    GROUP BY
								        id_mstfacility
								) f
								ON
								    a.id_mstfacility = f.id_mstfacility
								LEFT JOIN(
								    SELECT
								        id_mstfacility,
								        COUNT(*) AS c1
								    FROM
								        tblSVRfollowup
								    WHERE
								        CallOutput = 6
								    GROUP BY
								        id_mstfacility
								) g
								ON
								    a.id_mstfacility = g.id_mstfacility
								LEFT JOIN(
								    SELECT
								        id_mstfacility,
								        COUNT(*) AS c1
								    FROM
								        tblSVRfollowup
								    WHERE
								        CallOutput = 7
								    GROUP BY
								        id_mstfacility
								) h
								ON
								    a.id_mstfacility = h.id_mstfacility
								LEFT JOIN(
								    SELECT
								        id_mstfacility,
								        COUNT(*) AS c1
								    FROM
								        tblSVRfollowup
								    WHERE
								        CallOutput = 8
								    GROUP BY
								        id_mstfacility
								) i
								ON
								    a.id_mstfacility = i.id_mstfacility
								LEFT JOIN(
								    SELECT
								        id_mstfacility,
								        COUNT(*) AS c1
								    FROM
								        tblSVRfollowup
								    WHERE
								        CallOutput = 9
								    GROUP BY
								        id_mstfacility
								) j
								ON
								    a.id_mstfacility = j.id_mstfacility
								LEFT JOIN(
								    SELECT
								        id_mstfacility,
								        COUNT(*) AS c1
								    FROM
								        tblSVRfollowup
								    WHERE
								        CallOutput = 10
								    GROUP BY
								        id_mstfacility
								) k
								ON
								    a.id_mstfacility = k.id_mstfacility";

			$result = $this->db->query($query)->result();		

			return $result;
	}

}