<?php  
class Summary_genderwiseupdate_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_Model');
		$this->load->helper('common');
		ini_set('memory_limit', '-1');
		error_reporting(E_ALL);
		$curnt_month              =date('m',strtotime(date('Y-m-t')));
		if ($curnt_month<4) {
		
		//echo $this->initiate_date =" AND T_Initiation >='".date('Y-04-01', strtotime('-1 year'))."'";
			echo $this->initiate_date =" AND T_Initiation >='2018-09-01'";
		}
		else{
		//echo $this->initiate_date =" AND T_Initiation >='".timeStamp('01-04-'.date('Y'))."'";
			echo $this->initiate_date =" AND T_Initiation >='2018-09-01'";
		}
	}

public function everything_null(){
	$query = "UPDATE
					    tblsummary_genderwise
					SET
						`anti_hcv_screened`          = NULL,
						`anti_hcv_positive`          = NULL,
						`viral_load_tested`          = NULL,
						`viral_load_detected`        = NULL,
						`initiatied_on_treatment`    = NULL,
						`treatment_completed`        = NULL,
						`eligible_for_svr`           = NULL,
						`svr_done`                   = NULL,
						`treatment_successful`       = NULL,
						`svr_success`                = NULL,
						`treatment_failure`          = NULL,
						`hbv_screened`               = NULL,
						`hbv_positive`               = NULL,
						`hbv_initiated_on_treatment` = NULL,
						`hbv_regimen_taf`            = NULL,
						`hbv_regimen_ent`            = NULL,
						`hbv_regimen_tdf`            = NULL,
						`hbv_regimen_other`          = NULL,
						`hbv_elevated_alt_level`     = NULL,
						`hbv_dna_test`               = NULL,
						`hbv_cirrhotic`              = NULL,
						`RegimenChange`              = NULL,
						`hbv_ltfu`                   = NULL,
						`hbv_death_reported`         = NULL,
						`hcv_transfer_out`           = NULL,
						`hcv_treatment_stopped`      = NULL,
						`hcv_ltfu`                   = NULL,
						`hcv_missed_doses`           = NULL,
						`hcv_refered`                = NULL,
						`hcv_death_reported`         = NULL,
						`hcv_registered`             = NULL";

		$this->db->query($query);
}
public function update_cascade_fields_genderwise()
	{
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_screened_adultmale();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_screened_malechildren();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_screened_adultfemale();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_screened_femalechildren();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_screened_transgenderadult();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_screened_transgenderchildren();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_positive_adultmale();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_positive_malechildren();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_positive_adultfemale();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_positive_femalechildren();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_positive_transgenderfemale();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_positive_transgenderchildren();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_tested_adultmale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_tested_childrenmale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_tested_adultfemale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_tested_childrenfemale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_tested_adulttransgender();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_tested_childrentransgender();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_detected_adultmale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_detected_childrenmale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_detected_adultfemale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_detected_childrenfemale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_detected_adulttransgender();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_detected_childrentransgender();
				$this->Summary_genderwiseupdate_model->cascade_initiatied_on_treatment_adultmale();
				$this->Summary_genderwiseupdate_model->cascade_initiatied_on_treatment_childrenmale();
				$this->Summary_genderwiseupdate_model->cascade_initiatied_on_treatment_adultfemale();
				$this->Summary_genderwiseupdate_model->cascade_initiatied_on_treatment_childrenfemale();
				$this->Summary_genderwiseupdate_model->cascade_initiatied_on_treatment_adulttransgender();
				$this->Summary_genderwiseupdate_model->cascade_initiatied_on_treatment_childrentransgender();
				$this->Summary_genderwiseupdate_model->cascade_treatment_completed_adultmale();
				$this->Summary_genderwiseupdate_model->cascade_treatment_completed_childrenmale();
				$this->Summary_genderwiseupdate_model->cascade_treatment_completed_adultfemale();
				$this->Summary_genderwiseupdate_model->cascade_treatment_completed_childrenfemale();
				$this->Summary_genderwiseupdate_model->cascade_treatment_completed_adulttransgender();
				$this->Summary_genderwiseupdate_model->cascade_treatment_completed_childrentransgender();

				/*commented by vikram on 25-03-31*/
				/*$this->Summary_genderwiseupdate_model->cascade_eligible_forsvr_adultmale();
				$this->Summary_genderwiseupdate_model->cascade_eligible_forsvr_childrenmale();
				$this->Summary_genderwiseupdate_model->cascade_eligible_forsvr_adultfemale();
				$this->Summary_genderwiseupdate_model->cascade_eligible_forsvr_childrenfemale();
				$this->Summary_genderwiseupdate_model->cascade_eligible_forsvr_adulttransgender();
				$this->Summary_genderwiseupdate_model->cascade_eligible_forsvr_childrentransgender();*/
				$this->Summary_genderwiseupdate_model->cascade_anti_hepb_positive();
				$this->Summary_genderwiseupdate_model->cascade_anti_hepb_screen();

				/*monthly*/
				$this->Summary_genderwiseupdate_model->hcv_transfer_out_adult_male();
				$this->Summary_genderwiseupdate_model->hcv_transfer_out_adult_female();
				$this->Summary_genderwiseupdate_model->hcv_transfer_out_adult_transgender();
				$this->Summary_genderwiseupdate_model->hcv_transfer_out_children_male();
				$this->Summary_genderwiseupdate_model->hcv_transfer_out_children_female();
				$this->Summary_genderwiseupdate_model->hcv_transfer_out_children_transgender();

				$this->Summary_genderwiseupdate_model->hcv_treatment_stopped_adult_male();
				$this->Summary_genderwiseupdate_model->hcv_treatment_stopped_adult_female();
				$this->Summary_genderwiseupdate_model->hcv_treatment_stopped_adult_transgender();
				$this->Summary_genderwiseupdate_model->hcv_treatment_stopped_children_male();
				$this->Summary_genderwiseupdate_model->hcv_treatment_stopped_children_female();
				$this->Summary_genderwiseupdate_model->hcv_treatment_stopped_children_transgender();

				$this->Summary_genderwiseupdate_model->hcv_ltfu_adult_male();
				$this->Summary_genderwiseupdate_model->hcv_ltfu_adult_female();
				$this->Summary_genderwiseupdate_model->hcv_ltfu_adult_transgender();
				$this->Summary_genderwiseupdate_model->hcv_ltfu_children_male();
				$this->Summary_genderwiseupdate_model->hcv_ltfu_children_female();
				$this->Summary_genderwiseupdate_model->hcv_ltfu_children_transgender();


				$this->Summary_genderwiseupdate_model->hcv_refered_adult_male();
				$this->Summary_genderwiseupdate_model->hcv_refered_adult_female();
				$this->Summary_genderwiseupdate_model->hcv_refered_adult_transgender();
				$this->Summary_genderwiseupdate_model->hcv_refered_children_male();
				$this->Summary_genderwiseupdate_model->hcv_refered_children_female();
				$this->Summary_genderwiseupdate_model->hcv_refered_children_transgender();


				$this->Summary_genderwiseupdate_model->hcv_missed_doses_adult_male();
				$this->Summary_genderwiseupdate_model->hcv_missed_doses_adult_female();
				$this->Summary_genderwiseupdate_model->hcv_missed_doses_adult_transgender();
				$this->Summary_genderwiseupdate_model->hcv_missed_doses_children_male();
				$this->Summary_genderwiseupdate_model->hcv_missed_doses_children_female();
				$this->Summary_genderwiseupdate_model->hcv_missed_doses_children_transgender();


				$this->Summary_genderwiseupdate_model->hcv_death_reported_adult_male();
				$this->Summary_genderwiseupdate_model->hcv_death_reported_adult_female();
				$this->Summary_genderwiseupdate_model->hcv_death_reported_adult_transgender();
				$this->Summary_genderwiseupdate_model->hcv_death_reported_children_male();
				$this->Summary_genderwiseupdate_model->hcv_death_reported_children_female();
				$this->Summary_genderwiseupdate_model->hcv_death_reported_children_transgender();

				$this->Summary_genderwiseupdate_model->hcv_registered_adult_male();
				$this->Summary_genderwiseupdate_model->hcv_registered_adult_female();
				$this->Summary_genderwiseupdate_model->hcv_registered_adult_transgender();
				$this->Summary_genderwiseupdate_model->hcv_registered_children_male();
				$this->Summary_genderwiseupdate_model->hcv_registered_children_female();
				$this->Summary_genderwiseupdate_model->hcv_registered_children_transgender();

				$this->Summary_genderwiseupdate_model->eligible_for_svr_adult_male();
				$this->Summary_genderwiseupdate_model->eligible_for_svr_adult_female();
				$this->Summary_genderwiseupdate_model->eligible_for_svr_adult_transgender();
				$this->Summary_genderwiseupdate_model->eligible_for_svr_children_male();
				$this->Summary_genderwiseupdate_model->eligible_for_svr_children_female();
				$this->Summary_genderwiseupdate_model->eligible_for_svr_children_transgender();

				$this->Summary_genderwiseupdate_model->svr_done_adult_male();
				$this->Summary_genderwiseupdate_model->svr_done_adult_female();
				$this->Summary_genderwiseupdate_model->svr_done_adult_transgender();
				$this->Summary_genderwiseupdate_model->svr_done_children_male();
				$this->Summary_genderwiseupdate_model->svr_done_children_female();
				$this->Summary_genderwiseupdate_model->svr_done_children_transgender();

				$this->Summary_genderwiseupdate_model->svr_success_adult_male();
				$this->Summary_genderwiseupdate_model->svr_success_adult_female();
				$this->Summary_genderwiseupdate_model->svr_success_adult_transgender();
				$this->Summary_genderwiseupdate_model->svr_success_children_male();
				$this->Summary_genderwiseupdate_model->svr_success_children_female();
				$this->Summary_genderwiseupdate_model->svr_success_children_transgender();


		echo "done cascade, ";
	}


public function cascade_anti_hcv_screened_adultmale()
	{
		
			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS anti_hcv_screened
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and gender = 1 and age >= 18 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00') 
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_screened = a.anti_hcv_screened  where s.Gender=1 and s.Agewise=1";

		$this->db->query($query); 


	}

	public function cascade_anti_hcv_screened_malechildren()
	{
		
			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS anti_hcv_screened
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and gender = 1 and age < 18 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00') 
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_screened = a.anti_hcv_screened  where s.Gender=1 and s.Agewise=2";

		$this->db->query($query); 


	}

	public function cascade_anti_hcv_screened_adultfemale()
	{
		
			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS anti_hcv_screened
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and gender = 2 and age >= 18 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00') 
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_screened = a.anti_hcv_screened  where s.Gender=2 and s.Agewise=1";

		$this->db->query($query); 


	}

	public function cascade_anti_hcv_screened_femalechildren()
	{
		
			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS anti_hcv_screened
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and gender = 2 and age < 18 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00') 
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_screened = a.anti_hcv_screened  where s.Gender=2 and s.Agewise=2";

		$this->db->query($query); 


	}


	public function cascade_anti_hcv_screened_transgenderadult()
	{
		
			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS anti_hcv_screened
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and gender = 3 and age >= 18 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00') 
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_screened = a.anti_hcv_screened  where s.Gender=3 and s.Agewise=1";

		$this->db->query($query); 


	}


	public function cascade_anti_hcv_screened_transgenderchildren()
	{
		
			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS anti_hcv_screened
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and gender = 3 and  age < 18 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00') 
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_screened = a.anti_hcv_screened  where s.Gender=3 and s.Agewise=2";

		$this->db->query($query); 


	}
//and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')


	public function cascade_anti_hcv_positive_adultmale()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HCVRapidResult
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV = 1  and gender = 1 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_positive = a.HCVRapidResult where s.Gender=1 and s.Agewise=1";

		$this->db->query($query);
	}


public function cascade_anti_hcv_positive_malechildren()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HCVRapidResult
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV = 1  and gender = 1 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_positive = a.HCVRapidResult where s.Gender=1 and s.Agewise=2";

		$this->db->query($query);
	}
/*female +ve*/

public function cascade_anti_hcv_positive_adultfemale()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HCVRapidResult
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV = 1  and gender = 2 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_positive = a.HCVRapidResult where s.Gender=2 and s.Agewise=1";

		$this->db->query($query);
	}


public function cascade_anti_hcv_positive_femalechildren()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HCVRapidResult
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV = 1  and gender = 2 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_positive = a.HCVRapidResult where s.Gender=2 and s.Agewise=2";

		$this->db->query($query);
	}
	/*end female +ve*/
/*transgender*/

public function cascade_anti_hcv_positive_transgenderfemale()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HCVRapidResult
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV = 1  and gender = 3 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_positive = a.HCVRapidResult where s.Gender=3 and s.Agewise=1";

		$this->db->query($query);
	}


public function cascade_anti_hcv_positive_transgenderchildren()
	{

		

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HCVRapidResult
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV = 1  and gender = 3 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_positive = a.HCVRapidResult where s.Gender=3 and s.Agewise=2";

		$this->db->query($query);
	}
	/*end transgender +ve*/


	public function cascade_viral_load_tested_adultmale()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       count(PatientGUID) as VLSampleCollectionCount
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 and gender = 1 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_tested = a.VLSampleCollectionCount   where s.Gender=1 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_viral_load_tested_childrenmale()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       count(PatientGUID) as VLSampleCollectionCount
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 and gender = 1 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_tested = a.VLSampleCollectionCount   where s.Gender=1 and s.Agewise=2";

		$this->db->query($query);
	}

/*LD Female*/

public function cascade_viral_load_tested_adultfemale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       count(PatientGUID) as VLSampleCollectionCount
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 and gender = 2 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_tested = a.VLSampleCollectionCount  where s.Gender=2 and s.Agewise=1 ";

		$this->db->query($query);
	}

	public function cascade_viral_load_tested_childrenfemale()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       count(PatientGUID) as VLSampleCollectionCount
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 and gender = 2 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_tested = a.VLSampleCollectionCount  where s.Gender=2 and s.Agewise=2 ";

		$this->db->query($query);
	}

	/* VL Transgender*/

	public function cascade_viral_load_tested_adulttransgender()
	{

		

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       count(PatientGUID) as VLSampleCollectionCount
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 and gender = 3 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_tested = a.VLSampleCollectionCount  where s.Gender=3 and s.Agewise=1 ";

		$this->db->query($query);
	}

	public function cascade_viral_load_tested_childrentransgender()
	{

		
				

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       count(PatientGUID) as VLSampleCollectionCount
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 and gender = 3 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_tested = a.VLSampleCollectionCount  where s.Gender=3 and s.Agewise=2 ";

		$this->db->query($query);
	}


	public function cascade_viral_load_detected_adultmale()
	{

				



		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,
					count(*) AS T_DLL_01_VLC_Result
					FROM
					    tblpatient
					WHERE
					   AntiHCV = 1 and gender = 1 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' 
					GROUP BY
					    (
					        T_DLL_01_VLC_Date
					),
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_detected = a.T_DLL_01_VLC_Result where s.Gender=1 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_viral_load_detected_childrenmale()
	{

				



		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,
					count(*) AS T_DLL_01_VLC_Result
					FROM
					    tblpatient
					WHERE
					   AntiHCV = 1 and gender = 1 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' 
					GROUP BY
					    (
					        T_DLL_01_VLC_Date
					),
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_detected = a.T_DLL_01_VLC_Result where s.Gender=1 and s.Agewise=2";

		$this->db->query($query);
	}
/*Vl Dedecated female*/

public function cascade_viral_load_detected_adultfemale()
	{

				



		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,
					count(*) AS T_DLL_01_VLC_Result
					FROM
					    tblpatient
					WHERE
					   AntiHCV = 1 and gender = 2 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' 
					GROUP BY
					    (
					        T_DLL_01_VLC_Date
					),
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_detected = a.T_DLL_01_VLC_Result where s.Gender=2 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_viral_load_detected_childrenfemale()
	{

				



		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,
					count(*) AS T_DLL_01_VLC_Result
					FROM
					    tblpatient
					WHERE
					   AntiHCV = 1 and gender = 2 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' 
					GROUP BY
					    (
					        T_DLL_01_VLC_Date
					),
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_detected = a.T_DLL_01_VLC_Result where s.Gender=2 and s.Agewise=2";

		$this->db->query($query);
	}

	/*VL detacted transgender*/

	public function cascade_viral_load_detected_adulttransgender()
	{

				



		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,
					count(*) AS T_DLL_01_VLC_Result
					FROM
					    tblpatient
					WHERE
					   AntiHCV = 1 and gender = 3 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' 
					GROUP BY
					    (
					        T_DLL_01_VLC_Date
					),
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_detected = a.T_DLL_01_VLC_Result where s.Gender=3 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_viral_load_detected_childrentransgender()
	{

				



		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,
					count(*) AS T_DLL_01_VLC_Result
					FROM
					    tblpatient
					WHERE
					   AntiHCV = 1 and gender = 3 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' 
					GROUP BY
					    (
					        T_DLL_01_VLC_Date
					),
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_detected = a.T_DLL_01_VLC_Result where s.Gender=3 and s.Agewise=2 ";

		$this->db->query($query);
	}



	public function cascade_initiatied_on_treatment_adultmale()
	{

			

		  $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as COUNT
					    FROM
					        tblpatient
					    WHERE
					          gender = 1 and age >= 18 and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)   
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.initiatied_on_treatment = a.COUNT where s.Gender=1 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_initiatied_on_treatment_childrenmale()
	{

				

		  $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as COUNT
					    FROM
					        tblpatient
					    WHERE
					          gender = 1 and age < 18 and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)   
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.initiatied_on_treatment = a.COUNT where s.Gender=1 and s.Agewise=2";

		$this->db->query($query);
	}


/*Ination female*/

public function cascade_initiatied_on_treatment_adultfemale()
	{

				

		  $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as COUNT
					    FROM
					        tblpatient
					    WHERE
					          gender = 2 and age >= 18 and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.initiatied_on_treatment = a.COUNT where s.Gender=2 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_initiatied_on_treatment_childrenfemale()
	{

				

		  $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as COUNT
					    FROM
					        tblpatient
					    WHERE
					          gender = 2 and age < 18 and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)   
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.initiatied_on_treatment = a.COUNT where s.Gender=2 and s.Agewise=2";

		$this->db->query($query);
	}

	/*Inatiation transgender*/

	public function cascade_initiatied_on_treatment_adulttransgender()
	{

				

		  $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as COUNT
					    FROM
					        tblpatient
					    WHERE
					          gender = 3 and age >= 18 and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)   
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.initiatied_on_treatment = a.COUNT where s.Gender=3 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_initiatied_on_treatment_childrentransgender()
	{

				

		  $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as COUNT
					    FROM
					        tblpatient
					    WHERE
					          gender = 3 and age < 18 and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)   
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.initiatied_on_treatment = a.COUNT where s.Gender=3 and s.Agewise=2";

		$this->db->query($query);
	}

	/*end initation*/

	public function cascade_treatment_completed_adultmale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as treatment_completedcount
					    FROM
					        tblpatient
					    WHERE
					        gender = 1 and age >= 18 AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  
					    GROUP BY
					       ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_completed = a.treatment_completedcount  where s.Gender=1 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_treatment_completed_childrenmale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as treatment_completedcount
					    FROM
					        tblpatient
					    WHERE
					        gender = 1 and age < 18 AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  
					    GROUP BY
					       ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_completed = a.treatment_completedcount  where s.Gender=1 and s.Agewise=2";

		$this->db->query($query);
	}

/*start female cascade_treatment_completed */
public function cascade_treatment_completed_adultfemale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as treatment_completedcount
					    FROM
					        tblpatient
					    WHERE
					        gender = 2 and age >= 18 AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  
					    GROUP BY
					       ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_completed = a.treatment_completedcount  where s.Gender=2 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_treatment_completed_childrenfemale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as treatment_completedcount
					    FROM
					        tblpatient
					    WHERE
					        gender = 2 and age < 18 AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  
					    GROUP BY
					       ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_completed = a.treatment_completedcount  where s.Gender=2 and s.Agewise=2";

		$this->db->query($query);
	}

/*End*/
/*Transgender compleated*/

public function cascade_treatment_completed_adulttransgender()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as treatment_completedcount
					    FROM
					        tblpatient
					    WHERE
					        gender = 3 and age >= 18 AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  
					    GROUP BY
					       ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_completed = a.treatment_completedcount  where s.Gender=3 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_treatment_completed_childrentransgender()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as treatment_completedcount
					    FROM
					        tblpatient
					    WHERE
					        gender = 3 and age < 18 AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  
					    GROUP BY
					       ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_completed = a.treatment_completedcount  where s.Gender=3 and s.Agewise=2";

		$this->db->query($query);
	}


/*End*/

/*Start eligable for svr*/
/*public function cascade_eligible_forsvr_adultmale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            AdvisedSVRDate
					) AS dt,

					        count(PatientGUID) as eligible_for_svr
					    FROM
					        tblpatient
					    WHERE
					        gender = 1 and age >= 18 AND AdvisedSVRDate is not null and AdvisedSVRDate!='0000-00-00' AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       AdvisedSVRDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.eligible_for_svr = a.eligible_for_svr  where s.Gender=1 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_eligible_forsvr_childrenmale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            AdvisedSVRDate
					) AS dt,

					        count(PatientGUID) as eligible_for_svr
					    FROM
					        tblpatient
					    WHERE
					        gender = 1 and age < 18 AND AdvisedSVRDate is not null and AdvisedSVRDate!='0000-00-00' AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       AdvisedSVRDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.eligible_for_svr = a.eligible_for_svr  where s.Gender=1 and s.Agewise=2";

		$this->db->query($query);
	}
/*female */
/*public function cascade_eligible_forsvr_adultfemale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            AdvisedSVRDate
					) AS dt,

					        count(PatientGUID) as eligible_for_svr
					    FROM
					        tblpatient
					    WHERE
					        gender = 2 and age >= 18 AND AdvisedSVRDate is not null and AdvisedSVRDate!='0000-00-00' AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       AdvisedSVRDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.eligible_for_svr = a.eligible_for_svr  where s.Gender=2 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_eligible_forsvr_childrenfemale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            AdvisedSVRDate
					) AS dt,

					        count(PatientGUID) as eligible_for_svr
					    FROM
					        tblpatient
					    WHERE
					        gender = 2 and age < 18 AND AdvisedSVRDate is not null and AdvisedSVRDate!='0000-00-00' AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       AdvisedSVRDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.eligible_for_svr = a.eligible_for_svr  where s.Gender=2 and s.Agewise=2";

		$this->db->query($query);
	}
*/
	/*transgender*/

	/*public function cascade_eligible_forsvr_adulttransgender()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            AdvisedSVRDate
					) AS dt,

					        count(PatientGUID) as eligible_for_svr
					    FROM
					        tblpatient
					    WHERE
					        gender = 3 and age >= 18 AND AdvisedSVRDate is not null and AdvisedSVRDate!='0000-00-00' AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       AdvisedSVRDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.eligible_for_svr = a.eligible_for_svr  where s.Gender=3 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_eligible_forsvr_childrentransgender()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            AdvisedSVRDate
					) AS dt,

					        count(PatientGUID) as eligible_for_svr
					    FROM
					        tblpatient
					    WHERE
					        gender = 3 and age < 18 AND AdvisedSVRDate is not null and AdvisedSVRDate!='0000-00-00' AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       AdvisedSVRDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.eligible_for_svr = a.eligible_for_svr  where s.Gender=3 and s.Agewise=2";

		$this->db->query($query);
	}*/



/*end svr*/


	public function cascade_svr_done()
	{

				
	


		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as SVRDrawndatacount
					    FROM
					        tblpatient
					    WHERE
					        SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' AND Status IN (14,15) and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       SVR12W_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.svr_done = a.SVRDrawndatacount ";

		$this->db->query($query);
	}

	public function cascade_treatment_successful()
	{

				


		  $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,

					        COUNT(PatientGUID) as SVR12W_Result
					    FROM
					        tblpatient
					    WHERE
					        SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 and SVR12W_Result=2 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       SVR12W_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_successful = a.SVR12W_Result ";

		$this->db->query($query);
	}

public function cascade_treatment_failure()
	{

				
	


		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,

					        COUNT(PatientGUID) as SVR12W_Result
					    FROM
					        tblpatient
					    WHERE
					        SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND Status IN (14,15) AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and SVR12W_Result=1 
					    GROUP BY
					       SVR12W_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_failure = a.SVR12W_Result ";

		$this->db->query($query);
	}

public function cascade_anti_hepb_positive()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HBSRapidResult
					    FROM
					        tblpatient
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1)
					    GROUP BY
					       
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hbs_positive = a.HBSRapidResult ";

		$this->db->query($query);
	}


	public function cascade_anti_hepb_screen()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HBSRapidResult
					    FROM
					        tblpatient
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hbs_screened = a.HBSRapidResult ";

		$this->db->query($query);
	}

/*Gender wise 20-2-2020*/

public function update_hepb_genderwise(){

	

	 $this->Summary_genderwiseupdate_model->screened_for_hbv_adult_male();
	 $this->Summary_genderwiseupdate_model->screened_for_hbv_adult_female();
	 $this->Summary_genderwiseupdate_model->screened_for_hbv_adult_transgender();
	 $this->Summary_genderwiseupdate_model->screened_for_hbv_children_male();
	 $this->Summary_genderwiseupdate_model->screened_for_hbv_children_female();
	 $this->Summary_genderwiseupdate_model->screened_for_hbv_children_transgender();


	 $this->Summary_genderwiseupdate_model->hbv_positive_adult_male();
	 $this->Summary_genderwiseupdate_model->hbv_positive_adult_female();
	 $this->Summary_genderwiseupdate_model->hbv_positive_adult_transgender();
	 $this->Summary_genderwiseupdate_model->hbv_positive_children_male();
	 $this->Summary_genderwiseupdate_model->hbv_positive_children_female();
	 $this->Summary_genderwiseupdate_model->hbv_positive_children_transgender();

	 $this->Summary_genderwiseupdate_model->hbv_initiated_adult_male();
	 $this->Summary_genderwiseupdate_model->hbv_initiated_adult_female();
	 $this->Summary_genderwiseupdate_model->hbv_initiated_adult_transgender();
	 $this->Summary_genderwiseupdate_model->hbv_initiated_children_male();
	 $this->Summary_genderwiseupdate_model->hbv_initiated_children_female();
	 $this->Summary_genderwiseupdate_model->hbv_initiated_children_transgender();


	 $this->Summary_genderwiseupdate_model->hbv_regimen_taf_adult_male();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_taf_adult_female();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_taf_adult_transgender();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_taf_children_male();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_taf_children_female();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_taf_children_transgender();


	 $this->Summary_genderwiseupdate_model->hbv_regimen_ent_adult_male();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_ent_adult_female();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_ent_adult_transgender();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_ent_children_male();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_ent_children_female();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_ent_children_transgender();

	 $this->Summary_genderwiseupdate_model->hbv_regimen_tdf_adult_male();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_tdf_adult_female();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_tdf_adult_transgender();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_tdf_children_male();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_tdf_children_female();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_tdf_children_transgender();

	 $this->Summary_genderwiseupdate_model->hbv_regimen_other_adult_male();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_other_adult_female();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_other_adult_transgender();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_other_children_male();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_other_children_female();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_other_children_transgender();

	 $this->Summary_genderwiseupdate_model->hbv_elevated_alt_level_adult_male();
	 $this->Summary_genderwiseupdate_model->hbv_elevated_alt_level_adult_female();
	 $this->Summary_genderwiseupdate_model->hbv_elevated_alt_level_adult_transgender();
	 $this->Summary_genderwiseupdate_model->hbv_elevated_alt_level_children_male();
	 $this->Summary_genderwiseupdate_model->hbv_elevated_alt_level_children_female();
	 $this->Summary_genderwiseupdate_model->hbv_elevated_alt_level_children_transgender();

	 $this->Summary_genderwiseupdate_model->hbv_dna_adult_male();
	 $this->Summary_genderwiseupdate_model->hbv_dna_adult_female();
	 $this->Summary_genderwiseupdate_model->hbv_dna_adult_transgender();
	 $this->Summary_genderwiseupdate_model->hbv_dna_children_male();
	 $this->Summary_genderwiseupdate_model->hbv_dna_children_female();
	 $this->Summary_genderwiseupdate_model->hbv_dna_children_transgender();

	 $this->Summary_genderwiseupdate_model->hbv_cirrhotic_adult_male();
	 $this->Summary_genderwiseupdate_model->hbv_cirrhotic_adult_female();
	 $this->Summary_genderwiseupdate_model->hbv_cirrhotic_adult_transgender();
	 $this->Summary_genderwiseupdate_model->hbv_cirrhotic_children_male();
	 $this->Summary_genderwiseupdate_model->hbv_cirrhotic_children_female();
	 $this->Summary_genderwiseupdate_model->hbv_cirrhotic_children_transgender();

	 $this->Summary_genderwiseupdate_model->hbv_regimen_change_adult_male();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_change_adult_female();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_change_adult_transgender();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_change_children_male();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_change_children_female();
	 $this->Summary_genderwiseupdate_model->hbv_regimen_change_children_transgender();


	 $this->Summary_genderwiseupdate_model->hbv_ltfu_adult_male();
	 $this->Summary_genderwiseupdate_model->hbv_ltfu_adult_female();
	 $this->Summary_genderwiseupdate_model->hbv_ltfu_adult_transgender();
	 $this->Summary_genderwiseupdate_model->hbv_ltfu_children_male();
	 $this->Summary_genderwiseupdate_model->hbv_ltfu_children_female();
	 $this->Summary_genderwiseupdate_model->hbv_ltfu_children_transgender();

	 $this->Summary_genderwiseupdate_model->hbv_death_reported_adult_male();
	 $this->Summary_genderwiseupdate_model->hbv_death_reported_adult_female();
	 $this->Summary_genderwiseupdate_model->hbv_death_reported_adult_transgender();
	 $this->Summary_genderwiseupdate_model->hbv_death_reported_children_male();
	 $this->Summary_genderwiseupdate_model->hbv_death_reported_children_female();
	 $this->Summary_genderwiseupdate_model->hbv_death_reported_children_transgender();



		$this->Summary_genderwiseupdate_model->svr_done_out_of_adult_male();
		$this->Summary_genderwiseupdate_model->svr_done_out_of_adult_female();
		$this->Summary_genderwiseupdate_model->svr_done_out_of_adult_transgender();
		$this->Summary_genderwiseupdate_model->svr_done_out_of_children_male();
		$this->Summary_genderwiseupdate_model->svr_done_out_of_children_female();
		$this->Summary_genderwiseupdate_model->svr_done_out_of_children_transgender();
		
		$this->Summary_genderwiseupdate_model->svr_success_out_of_adult_male();
		$this->Summary_genderwiseupdate_model->svr_success_out_of_adult_female();
		$this->Summary_genderwiseupdate_model->svr_success_out_of_adult_transgender();
		$this->Summary_genderwiseupdate_model->svr_success_out_of_children_male();
		$this->Summary_genderwiseupdate_model->svr_success_out_of_children_female();
		$this->Summary_genderwiseupdate_model->svr_success_out_of_children_transgender();

}
/*hepb gender wise summary UPDATE*/

public function screened_for_hbv_adult_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           MAX(
GREATEST(
COALESCE((HBSRapidDate), 0),
COALESCE((HBSElisaDate), 0),  
COALESCE((HBSOtherDate), 0)
))
) AS dt,

       count(PatientGUID) as hbv_screened
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age >= 18 AND (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' ||  HBSElisaDate!='0000-00-00' || HBSOtherDate !='0000-00-00')
   GROUP BY
    GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
	),
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_screened = a.hbv_screened  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function screened_for_hbv_adult_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           MAX(
GREATEST(
COALESCE((HBSRapidDate), 0),
COALESCE((HBSElisaDate), 0),  
COALESCE((HBSOtherDate), 0)
))
) AS dt,

       count(PatientGUID) as hbv_screened
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age >= 18 AND (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' ||  HBSElisaDate!='0000-00-00' || HBSOtherDate !='0000-00-00')
   GROUP BY
    GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
	),
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_screened = a.hbv_screened  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function screened_for_hbv_adult_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           MAX(
GREATEST(
COALESCE((HBSRapidDate), 0),
COALESCE((HBSElisaDate), 0),  
COALESCE((HBSOtherDate), 0)
))
) AS dt,

       count(PatientGUID) as hbv_screened
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age >= 18 AND (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' ||  HBSElisaDate!='0000-00-00' || HBSOtherDate !='0000-00-00')
   GROUP BY
    GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
	),
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_screened = a.hbv_screened  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function screened_for_hbv_children_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           MAX(
GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
))
) AS dt,

       count(PatientGUID) as hbv_screened
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age < 18 AND (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' ||  HBSElisaDate!='0000-00-00' || HBSOtherDate !='0000-00-00')
   GROUP BY
    GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
	),
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_screened = a.hbv_screened  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function screened_for_hbv_children_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           MAX(
GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
))
) AS dt,

       count(PatientGUID) as hbv_screened
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age < 18 AND (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' ||  HBSElisaDate!='0000-00-00' || HBSOtherDate !='0000-00-00')
   GROUP BY
    GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
	),
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_screened = a.hbv_screened where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function screened_for_hbv_children_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           MAX(
GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
))
) AS dt,

       count(PatientGUID) as hbv_screened
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age < 18 AND (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' ||  HBSElisaDate!='0000-00-00' || HBSOtherDate !='0000-00-00')
   GROUP BY
    GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
	),
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_screened = a.hbv_screened  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}

public function hbv_positive_adult_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           MAX(
GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
))
) AS dt,

       count(PatientGUID) as hbv_positive
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age >= 18 AND (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' ||  HBSElisaDate!='0000-00-00' || HBSOtherDate !='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult=1 || HBSOtherResult=1) and HbsAg=1
   GROUP BY
    GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
	),
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_positive = a.hbv_positive  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hbv_positive_adult_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           MAX(
GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
))
) AS dt,

       count(PatientGUID) as hbv_positive
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age >= 18 AND (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' ||  HBSElisaDate!='0000-00-00' || HBSOtherDate !='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult=1 || HBSOtherResult=1) and HbsAg=1
   GROUP BY
    GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
	),
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_positive = a.hbv_positive  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hbv_positive_adult_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           MAX(
GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
))
) AS dt,

       count(PatientGUID) as hbv_positive
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age >= 18 AND (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (p.HBSRapidDate!='0000-00-00' ||  HBSElisaDate!='0000-00-00' || HBSOtherDate !='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult=1 || HBSOtherResult=1) and HbsAg=1
   GROUP BY
    GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
	),
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_positive = a.hbv_positive  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hbv_positive_children_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           MAX(
GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
))
) AS dt,

       count(PatientGUID) as hbv_positive
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age < 18 AND (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (p.HBSRapidDate!='0000-00-00' ||  HBSElisaDate!='0000-00-00' || HBSOtherDate !='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult=1 || HBSOtherResult=1) and HbsAg=1
   GROUP BY
    GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
	),
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_positive = a.hbv_positive  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_positive_children_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           MAX(
GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
))
) AS dt,

       count(PatientGUID) as hbv_positive
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age < 18 AND (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' ||  HBSElisaDate!='0000-00-00' || HBSOtherDate !='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult=1 || HBSOtherResult=1) and HbsAg=1
   GROUP BY
    GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
	),
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_positive = a.hbv_positive where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hbv_positive_children_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           MAX(
GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
))
) AS dt,

       count(PatientGUID) as hbv_positive
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age < 18 AND (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' ||  HBSElisaDate!='0000-00-00' || HBSOtherDate !='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult=1 || HBSOtherResult=1) and HbsAg=1
   GROUP BY
    GREATEST(
	COALESCE((HBSRapidDate), 0),
	COALESCE((HBSElisaDate), 0),  
	COALESCE((HBSOtherDate), 0)
	),
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_positive = a.hbv_positive  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}

public function hbv_initiated_adult_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           T_Initiation
		) AS dt,

       count(PatientGUID) as hbv_initiated_on_treatment
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age >= 18 AND T_Initiation is not null and T_Initiation!='0000-00-00' and (HBSRapidResult=1 || HBSElisaResult=1 || HBSOtherResult=1) and HbsAg=1
   GROUP BY
    T_Initiation,
    id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_initiated_on_treatment = a.hbv_initiated_on_treatment  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hbv_initiated_adult_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          T_Initiation
		) AS dt,

       count(PatientGUID) as hbv_initiated_on_treatment
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age >= 18 AND T_Initiation is not null and T_Initiation!='0000-00-00' and (HBSRapidResult=1 || HBSElisaResult=1 || HBSOtherResult=1) and HbsAg=1
   GROUP BY
    T_Initiation,
       id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_initiated_on_treatment = a.hbv_initiated_on_treatment  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hbv_initiated_adult_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          T_Initiation
		) AS dt,

       count(PatientGUID) as hbv_initiated_on_treatment
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age >= 18 AND T_Initiation is not null and T_Initiation!='0000-00-00' and (HBSRapidResult=1 || HBSElisaResult=1 || HBSOtherResult=1) and HbsAg=1
   GROUP BY
    T_Initiation,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_initiated_on_treatment = a.hbv_initiated_on_treatment  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hbv_initiated_children_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           T_Initiation
         ) AS dt,

       count(PatientGUID) as hbv_initiated_on_treatment
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age < 18 AND T_Initiation is not null and T_Initiation!='0000-00-00' and (HBSRapidResult=1 || HBSElisaResult=1 || HBSOtherResult=1) and HbsAg=1
   GROUP BY
  		T_Initiation,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_initiated_on_treatment = a.hbv_initiated_on_treatment where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_initiated_children_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          T_Initiation
		) AS dt,

       count(PatientGUID) as hbv_initiated_on_treatment
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age < 18 AND T_Initiation is not null and T_Initiation!='0000-00-00' and (HBSRapidResult=1 || HBSElisaResult=1 || HBSOtherResult=1) and HbsAg=1
   GROUP BY
   T_Initiation,
   id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_initiated_on_treatment = a.hbv_initiated_on_treatment where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hbv_initiated_children_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
           T_Initiation
		) AS dt,

       count(PatientGUID) as hbv_initiated_on_treatment
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age < 18 AND T_Initiation is not null and T_Initiation!='0000-00-00' and (HBSRapidResult=1 || HBSElisaResult=1 || HBSOtherResult=1) and HbsAg=1
   GROUP BY
    T_Initiation,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_initiated_on_treatment = a.hbv_initiated_on_treatment where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}



public function hbv_regimen_taf_adult_male(){
	$query = "UPDATE
				tblsummary_genderwise s
				INNER JOIN(
				SELECT hepb.id_mstfacility,

				(
				hepb.PrescribingDate
				) AS dt,

				count(hepb.PatientGUID) as hbv_regimen_taf
				FROM
				tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
				WHERE
				p.gender = 1 and p.age >= 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=1 
				GROUP BY
				hepb.PrescribingDate,
				hepb.id_mstfacility
				) a
				ON
				s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
				SET
				s.hbv_regimen_taf = a.hbv_regimen_taf  where s.Gender=1 and s.Agewise=1";

	$this->db->query($query);
}


public function hbv_regimen_taf_adult_female(){
	$query = "UPDATE
				tblsummary_genderwise s
				INNER JOIN(
				SELECT hepb.id_mstfacility,

				(
				hepb.PrescribingDate
				) AS dt,

				count(hepb.PatientGUID) as hbv_regimen_taf
				FROM
				tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
				WHERE
				p.gender = 2 and p.age >= 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=1 
				GROUP BY
				hepb.PrescribingDate,
				hepb.id_mstfacility
				) a
				ON
				s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
				SET
				s.hbv_regimen_taf = a.hbv_regimen_taf  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}

public function hbv_regimen_taf_adult_transgender(){
	$query = "UPDATE
   		tblsummary_genderwise s
		INNER JOIN(
		   SELECT hepb.id_mstfacility,
		     
		        (
		           hepb.PrescribingDate
				) AS dt,

		       count(hepb.PatientGUID) as hbv_regimen_taf
		   FROM
		      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
		   WHERE
		       p.gender = 3 and p.age >= 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=1 
		   GROUP BY
		   hepb.PrescribingDate,
		    hepb.id_mstfacility
			) a
		ON
		   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
		SET
		   s.hbv_regimen_taf = a.hbv_regimen_taf  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hbv_regimen_taf_children_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_taf
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age < 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=1 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_taf = a.hbv_regimen_taf  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_regimen_taf_children_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_taf
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age < 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=1 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_taf = a.hbv_regimen_taf  where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hbv_regimen_taf_children_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_taf
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age < 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=1 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_taf = a.hbv_regimen_taf  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_regimen_ent_adult_male(){
	$query = "UPDATE
				   tblsummary_genderwise s
			  INNER JOIN(
				SELECT hepb.id_mstfacility,
				     
				        (
				           hepb.PrescribingDate
						) AS dt,

				       count(hepb.PatientGUID) as hbv_regimen_ent
				FROM
				      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
				WHERE
				       p.gender = 1 and p.age >= 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=2 
				GROUP BY
				   hepb.PrescribingDate,
				    hepb.id_mstfacility
					) a
				ON
				   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
				SET
				   s.hbv_regimen_ent = a.hbv_regimen_ent where s.Gender=1 and s.Agewise=1";
				$this->db->query($query);
}


public function hbv_regimen_ent_adult_female(){
	$query = "UPDATE
			   tblsummary_genderwise s
			INNER JOIN(
			   SELECT hepb.id_mstfacility,
			     
			        (
			           hepb.PrescribingDate
					) AS dt,

			       count(hepb.PatientGUID) as hbv_regimen_ent
			   FROM
			      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
			   WHERE
			       p.gender = 2 and p.age >= 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=2 
			   GROUP BY
			   hepb.PrescribingDate,
			    hepb.id_mstfacility
				) a
			ON
			   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
			SET
			   s.hbv_regimen_ent = a.hbv_regimen_ent where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hbv_regimen_ent_adult_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_ent
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age >= 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=2 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_ent = a.hbv_regimen_ent where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hbv_regimen_ent_children_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_ent
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age < 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=2 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_ent = a.hbv_regimen_ent where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_regimen_ent_children_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_ent
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age < 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=2 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_ent = a.hbv_regimen_ent where s.Gender=2 and s.Agewise=2";
 $this->db->query($query);
}

public function hbv_regimen_ent_children_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_ent
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age < 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=2 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_ent = a.hbv_regimen_ent where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}




public function hbv_regimen_tdf_adult_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_tdf
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age >= 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=3 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_tdf = a.hbv_regimen_tdf where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hbv_regimen_tdf_adult_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_tdf
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age >= 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=3 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_tdf = a.hbv_regimen_tdf  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hbv_regimen_tdf_adult_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_tdf
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age >= 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=3 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_tdf = a.hbv_regimen_tdf where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hbv_regimen_tdf_children_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_tdf
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age < 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=3 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_tdf = a.hbv_regimen_tdf where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_regimen_tdf_children_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_tdf
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age < 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=3 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_tdf = a.hbv_regimen_tdf  where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hbv_regimen_tdf_children_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_tdf
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age < 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=3 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_tdf = a.hbv_regimen_tdf where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}

public function hbv_regimen_other_adult_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_other
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age >= 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=99 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_other = a.hbv_regimen_other where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hbv_regimen_other_adult_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_other
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age >= 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=99 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_other = a.hbv_regimen_other where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}


public function hbv_regimen_other_adult_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_other
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age >= 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=99 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_other = a.hbv_regimen_other where s.Gender=3 and s.Agewise=1";
$this->db->query($query);

}

public function hbv_regimen_other_children_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_other
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age < 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=99 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_other = a.hbv_regimen_other where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_regimen_other_children_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_other
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age < 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=99 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_other = a.hbv_regimen_other  where s.Gender=2 and s.Agewise=2";
$this->db->query($query);

}

public function hbv_regimen_other_children_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_regimen_other
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age < 18 AND hepb.PrescribingDate is not null and hepb.PrescribingDate!='0000-00-00' and  hepb.Drugs_Added=99 
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_regimen_other = a.hbv_regimen_other  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}



public function hbv_elevated_alt_level_adult_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.Prescribing_Dt
		) AS dt,

       count(hepb.PatientGUID) as hbv_elevated_alt_level
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
      HbsAg = 1 AND p.gender = 1 and p.age >= 18  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND hepb.Prescribing_Dt is not null AND hepb.Prescribing_Dt!='0000-00-00'  AND hepb.Elevated_ALT_Level in(1,2,3)  
   GROUP BY
   hepb.Prescribing_Dt,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_elevated_alt_level = a.hbv_elevated_alt_level  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hbv_elevated_alt_level_adult_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.Prescribing_Dt
		) AS dt,

       count(hepb.PatientGUID) as hbv_elevated_alt_level
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
     HbsAg = 1 AND  p.gender = 2 and p.age >= 18   and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND hepb.Prescribing_Dt is not null AND hepb.Prescribing_Dt!='0000-00-00'  AND hepb.Elevated_ALT_Level in(1,2,3)  
   GROUP BY
   hepb.Prescribing_Dt,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_elevated_alt_level = a.hbv_elevated_alt_level where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hbv_elevated_alt_level_adult_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.Prescribing_Dt
		) AS dt,

       count(hepb.PatientGUID) as hbv_elevated_alt_level
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
      HbsAg = 1 AND p.gender = 3 and p.age >= 18  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND hepb.Prescribing_Dt is not null AND hepb.Prescribing_Dt!='0000-00-00'  AND hepb.Elevated_ALT_Level in(1,2,3)  
   GROUP BY
   hepb.Prescribing_Dt,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_elevated_alt_level = a.hbv_elevated_alt_level where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hbv_elevated_alt_level_children_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.Prescribing_Dt
		) AS dt,

       count(hepb.PatientGUID) as hbv_elevated_alt_level
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
      HbsAg = 1 AND p.gender = 1 and p.age < 18  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND hepb.Prescribing_Dt is not null AND hepb.Prescribing_Dt!='0000-00-00'  AND hepb.Elevated_ALT_Level in(1,2,3)   
   GROUP BY
   hepb.Prescribing_Dt,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_elevated_alt_level = a.hbv_elevated_alt_level where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_elevated_alt_level_children_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.Prescribing_Dt
		) AS dt,

       count(hepb.PatientGUID) as hbv_elevated_alt_level
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
      HbsAg = 1 AND p.gender = 2 and p.age < 18  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND hepb.Prescribing_Dt is not null AND hepb.Prescribing_Dt!='0000-00-00'  AND hepb.Elevated_ALT_Level in(1,2,3)   
   GROUP BY
   hepb.Prescribing_Dt,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_elevated_alt_level = a.hbv_elevated_alt_level where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hbv_elevated_alt_level_children_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.Prescribing_Dt
		) AS dt,

       count(hepb.PatientGUID) as hbv_elevated_alt_level
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
      HbsAg = 1 AND p.gender = 3 and p.age < 18  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND hepb.Prescribing_Dt is not null AND hepb.Prescribing_Dt!='0000-00-00'  AND hepb.Elevated_ALT_Level in(1,2,3)     
   GROUP BY
   hepb.Prescribing_Dt,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_elevated_alt_level = a.hbv_elevated_alt_level where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}




public function hbv_dna_adult_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           p.T_DLL_01_BVLC_Date
		) AS dt,

       count(hepb.PatientGUID) as hbv_dna_test
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age >= 18 AND  HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND BVLSampleCollectionDate IS NOT NULL AND BVLSampleCollectionDate!='0000-00-00'  AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00'
         GROUP BY
   p.T_DLL_01_BVLC_Date,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_dna_test = a.hbv_dna_test  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hbv_dna_adult_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           p.T_DLL_01_BVLC_Date
		) AS dt,

       count(hepb.PatientGUID) as hbv_dna_test
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age >= 18  AND  HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND BVLSampleCollectionDate IS NOT NULL AND BVLSampleCollectionDate!='0000-00-00'  AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00'   
   GROUP BY
   p.T_DLL_01_BVLC_Date,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_dna_test = a.hbv_dna_test  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hbv_dna_adult_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           p.T_DLL_01_BVLC_Date
		) AS dt,

       count(hepb.PatientGUID) as hbv_dna_test
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age >= 18  AND  HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND BVLSampleCollectionDate IS NOT NULL AND BVLSampleCollectionDate!='0000-00-00'  AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00'    
   GROUP BY
   p.T_DLL_01_BVLC_Date,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_dna_test = a.hbv_dna_test  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hbv_dna_children_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           p.T_DLL_01_BVLC_Date
		) AS dt,

       count(hepb.PatientGUID) as hbv_dna_test
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age < 18  AND  HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND BVLSampleCollectionDate IS NOT NULL AND BVLSampleCollectionDate!='0000-00-00'  AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00'  
   GROUP BY
   p.T_DLL_01_BVLC_Date,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_dna_test = a.hbv_dna_test  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_dna_children_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           p.T_DLL_01_BVLC_Date
		) AS dt,

       count(hepb.PatientGUID) as hbv_dna_test
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age < 18  AND  HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND BVLSampleCollectionDate IS NOT NULL AND BVLSampleCollectionDate!='0000-00-00'  AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00'  
   GROUP BY
   p.T_DLL_01_BVLC_Date,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_dna_test = a.hbv_dna_test  where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hbv_dna_children_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           p.T_DLL_01_BVLC_Date
		) AS dt,

       count(hepb.PatientGUID) as hbv_dna_test
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age < 18  AND  HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND BVLSampleCollectionDate IS NOT NULL AND BVLSampleCollectionDate!='0000-00-00'  AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00'   
   GROUP BY
   p.T_DLL_01_BVLC_Date,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_dna_test = a.hbv_dna_test where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}




public function hbv_cirrhotic_adult_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.Prescribing_Dt
		) AS dt,

       count(hepb.PatientGUID) as hbv_cirrhotic
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age >= 18 AND hepb.V1_Cirrhosis=1
         GROUP BY
   hepb.Prescribing_Dt,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_cirrhotic = a.hbv_cirrhotic where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hbv_cirrhotic_adult_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.Prescribing_Dt
		) AS dt,

       count(hepb.PatientGUID) as hbv_cirrhotic
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age >= 18 AND hepb.V1_Cirrhosis=1    
   GROUP BY
   hepb.Prescribing_Dt,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_cirrhotic = a.hbv_cirrhotic where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hbv_cirrhotic_adult_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.Prescribing_Dt
		) AS dt,

       count(hepb.PatientGUID) as hbv_cirrhotic
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age >= 18 AND hepb.V1_Cirrhosis=1    
   GROUP BY
   hepb.Prescribing_Dt,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_cirrhotic = a.hbv_cirrhotic where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hbv_cirrhotic_children_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.Prescribing_Dt
		) AS dt,

       count(hepb.PatientGUID) as hbv_cirrhotic
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age < 18 AND hepb.V1_Cirrhosis=1    
   GROUP BY
   hepb.Prescribing_Dt,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_cirrhotic = a.hbv_cirrhotic where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_cirrhotic_children_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.Prescribing_Dt
		) AS dt,

       count(hepb.PatientGUID) as hbv_cirrhotic
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age < 18 AND hepb.V1_Cirrhosis=1    
   GROUP BY
   hepb.Prescribing_Dt,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_cirrhotic = a.hbv_cirrhotic where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hbv_cirrhotic_children_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.Prescribing_Dt
		) AS dt,

       count(hepb.PatientGUID) as hbv_cirrhotic
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age < 18 AND hepb.V1_Cirrhosis=1    
   GROUP BY
   hepb.Prescribing_Dt,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_cirrhotic = a.hbv_cirrhotic where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_regimen_change_adult_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as RegimenChange
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age >= 18 AND hepb.RegimenChangeReason is not null and hepb.RegimenChangeReason!=''
         GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.RegimenChange = a.RegimenChange  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hbv_regimen_change_adult_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as RegimenChange
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age >= 18 AND hepb.RegimenChangeReason is not null and hepb.RegimenChangeReason!=''    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.RegimenChange = a.RegimenChange  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hbv_regimen_change_adult_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as RegimenChange
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age >= 18 AND hepb.RegimenChangeReason is not null and hepb.RegimenChangeReason!=''    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.RegimenChange = a.RegimenChange  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hbv_regimen_change_children_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as RegimenChange
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age < 18 AND hepb.RegimenChangeReason is not null and hepb.RegimenChangeReason!=''    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.RegimenChange = a.RegimenChange  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_regimen_change_children_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as RegimenChange
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age < 18 AND hepb.RegimenChangeReason is not null and hepb.RegimenChangeReason!=''    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.RegimenChange = a.RegimenChange  where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hbv_regimen_change_children_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as RegimenChange
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age < 18 AND hepb.RegimenChangeReason is not null and hepb.RegimenChangeReason!=''    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.RegimenChange = a.RegimenChange  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_ltfu_adult_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_ltfu
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age >= 18 AND hepb.InterruptReason=2
         GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_ltfu = a.hbv_ltfu  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hbv_ltfu_adult_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_ltfu
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age >= 18 AND hepb.InterruptReason=2    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_ltfu = a.hbv_ltfu  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hbv_ltfu_adult_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_ltfu
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age >= 18 AND hepb.InterruptReason=2    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_ltfu = a.hbv_ltfu  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hbv_ltfu_children_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_ltfu
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age < 18 AND hepb.InterruptReason=2    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_ltfu = a.hbv_ltfu  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_ltfu_children_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_ltfu
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age < 18 AND hepb.InterruptReason=2    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_ltfu = a.hbv_ltfu  where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hbv_ltfu_children_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_ltfu
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age < 18 AND hepb.InterruptReason=2    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_ltfu = a.hbv_ltfu  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}




public function hbv_death_reported_adult_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_death_reported
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age >= 18 AND hepb.InterruptReason=1
         GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_death_reported = a.hbv_death_reported  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hbv_death_reported_adult_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_death_reported
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age >= 18 AND hepb.InterruptReason=1    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_death_reported = a.hbv_death_reported  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hbv_death_reported_adult_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_death_reported
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age >= 18 AND hepb.InterruptReason=1    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_death_reported = a.hbv_death_reported  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hbv_death_reported_children_male(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_death_reported
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 1 and p.age < 18 AND hepb.InterruptReason=1    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_death_reported = a.hbv_death_reported  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hbv_death_reported_children_female(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_death_reported
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 2 and p.age < 18 AND hepb.InterruptReason=1    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_death_reported = a.hbv_death_reported  where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hbv_death_reported_children_transgender(){
	$query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT hepb.id_mstfacility,
     
        (
           hepb.PrescribingDate
		) AS dt,

       count(hepb.PatientGUID) as hbv_death_reported
   FROM
      tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
   WHERE
       p.gender = 3 and p.age < 18 AND hepb.InterruptReason=1    
   GROUP BY
   hepb.PrescribingDate,
    hepb.id_mstfacility
	) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hbv_death_reported = a.hbv_death_reported  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}

/*update monthly report 5-march 2020*/

public function hcv_transfer_out_adult_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
         UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_transfer_out
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age >= 18 AND TransferRequestAccepted >0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_transfer_out = a.hcv_transfer_out  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hcv_transfer_out_adult_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_transfer_out
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age >= 18 AND TransferRequestAccepted >0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_transfer_out = a.hcv_transfer_out  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hcv_transfer_out_adult_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_transfer_out
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age >= 18 AND TransferRequestAccepted >0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null
   GROUP BY
    UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_transfer_out = a.hcv_transfer_out  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hcv_transfer_out_children_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_transfer_out
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age < 18 AND TransferRequestAccepted >0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_transfer_out = a.hcv_transfer_out  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hcv_transfer_out_children_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_transfer_out
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age < 18 AND TransferRequestAccepted >0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null
   GROUP BY
  UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_transfer_out = a.hcv_transfer_out where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hcv_transfer_out_children_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_transfer_out
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age < 18 AND TransferRequestAccepted >0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_transfer_out = a.hcv_transfer_out  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}
public function hcv_treatment_stopped_adult_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
         UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_treatment_stopped
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age >= 18 AND InterruptReason = 2 and LFUReason= 5 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_treatment_stopped = a.hcv_treatment_stopped  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hcv_treatment_stopped_adult_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_treatment_stopped
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age >= 18 AND InterruptReason = 2 and LFUReason= 5 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_treatment_stopped = a.hcv_treatment_stopped  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hcv_treatment_stopped_adult_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_treatment_stopped
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age >= 18 AND InterruptReason = 2 and LFUReason= 5 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null
   GROUP BY
    UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_treatment_stopped = a.hcv_treatment_stopped  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hcv_treatment_stopped_children_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_treatment_stopped
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age < 18 AND InterruptReason = 2 and LFUReason= 5 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_treatment_stopped = a.hcv_treatment_stopped  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hcv_treatment_stopped_children_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_treatment_stopped
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age < 18 AND InterruptReason = 2 and LFUReason= 5 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null
   GROUP BY
  UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_treatment_stopped = a.hcv_treatment_stopped where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hcv_treatment_stopped_children_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_treatment_stopped
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age < 18 AND InterruptReason = 2 and LFUReason= 5 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_treatment_stopped = a.hcv_treatment_stopped  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}
public function hcv_ltfu_adult_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
         UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_ltfu
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age >= 18 AND InterruptReason = 2 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND LFUReason!=0 and  T_Initiation!='0000-00-00' and T_Initiation is not null AND UpdatedOn!='0000-00-00' and UpdatedOn is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_ltfu = a.hcv_ltfu  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hcv_ltfu_adult_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_ltfu
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age >= 18 AND InterruptReason = 2 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND LFUReason!=0 and  T_Initiation!='0000-00-00' and T_Initiation is not null AND UpdatedOn!='0000-00-00' and UpdatedOn is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_ltfu = a.hcv_ltfu  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hcv_ltfu_adult_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_ltfu
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age >= 18 AND InterruptReason = 2 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND LFUReason!=0 and  T_Initiation!='0000-00-00' and T_Initiation is not null AND UpdatedOn!='0000-00-00' and UpdatedOn is not null
   GROUP BY
    UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_ltfu = a.hcv_ltfu  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hcv_ltfu_children_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_ltfu
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age < 18 AND InterruptReason = 2 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND LFUReason!=0 and  T_Initiation!='0000-00-00' and T_Initiation is not null AND UpdatedOn!='0000-00-00' and UpdatedOn is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_ltfu = a.hcv_ltfu  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hcv_ltfu_children_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_ltfu
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age < 18 AND InterruptReason = 2 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND LFUReason!=0 and  T_Initiation!='0000-00-00' and T_Initiation is not null AND UpdatedOn!='0000-00-00' and UpdatedOn is not null
   GROUP BY
  UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_ltfu = a.hcv_ltfu where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hcv_ltfu_children_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_ltfu
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age < 18 AND InterruptReason = 2 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND LFUReason!=0 and  T_Initiation!='0000-00-00' and T_Initiation is not null AND UpdatedOn!='0000-00-00' and UpdatedOn is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_ltfu = a.hcv_ltfu  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}
public function hcv_refered_adult_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
         UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_refered
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age >= 18 AND IsReferal=1 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_refered = a.hcv_refered  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hcv_refered_adult_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_refered
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age >= 18 AND IsReferal=1 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_refered = a.hcv_refered  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hcv_refered_adult_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_refered
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age >= 18 AND IsReferal=1 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
   GROUP BY
    UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_refered = a.hcv_refered  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hcv_refered_children_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_refered
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age < 18 AND IsReferal=1 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_refered = a.hcv_refered  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hcv_refered_children_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_refered
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age < 18 AND IsReferal=1 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
   GROUP BY
  UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_refered = a.hcv_refered where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hcv_refered_children_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_refered
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age < 18 AND IsReferal=1 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_refered = a.hcv_refered  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}
public function hcv_missed_doses_adult_male(){
  $query = "UPDATE
           tblsummary_genderwise s
        INNER JOIN(
        SELECT p.id_mstfacility,
             
                (
                  v.Visit_Dt
            ) AS dt,

               count(p.PatientGUID) as hcv_missed_doses
        FROM
             `tblpatient` p inner join tblpatientvisit v on p.patientguid=v.patientguid
        WHERE
               p.gender = 1 and p.age >= 18 AND v.PillsLeft >0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
        GROUP BY
          v.Visit_Dt,
            p.id_mstfacility
          ) a
        ON
           s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
        SET
           s.hcv_missed_doses = a.hcv_missed_doses where s.Gender=1 and s.Agewise=1";
        $this->db->query($query);
}


public function hcv_missed_doses_adult_female(){
  $query = "UPDATE
         tblsummary_genderwise s
      INNER JOIN(
         SELECT p.id_mstfacility,
           
              (
                v.Visit_Dt
          ) AS dt,

             count(p.PatientGUID) as hcv_missed_doses
         FROM
           `tblpatient` p inner join tblpatientvisit v on p.patientguid=v.patientguid
         WHERE
             p.gender = 2 and p.age >= 18 AND v.PillsLeft >0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
         GROUP BY
        v.Visit_Dt,
          p.id_mstfacility
        ) a
      ON
         s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
      SET
         s.hcv_missed_doses = a.hcv_missed_doses where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hcv_missed_doses_adult_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT p.id_mstfacility,
     
        (
          v.Visit_Dt
    ) AS dt,

       count(p.PatientGUID) as hcv_missed_doses
   FROM
     `tblpatient` p inner join tblpatientvisit v on p.patientguid=v.patientguid
   WHERE
       p.gender = 3 and p.age >= 18 AND v.PillsLeft >0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
   GROUP BY
  v.Visit_Dt,
    p.id_mstfacility
  ) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_missed_doses = a.hcv_missed_doses where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hcv_missed_doses_children_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT p.id_mstfacility,
     
        (
          v.Visit_Dt
    ) AS dt,

       count(p.PatientGUID) as hcv_missed_doses
   FROM
     `tblpatient` p inner join tblpatientvisit v on p.patientguid=v.patientguid
   WHERE
       p.gender = 1 and p.age < 18 AND v.PillsLeft >0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
   GROUP BY
  v.Visit_Dt,
    p.id_mstfacility
  ) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_missed_doses = a.hcv_missed_doses where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hcv_missed_doses_children_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT p.id_mstfacility,
     
        (
          v.Visit_Dt
    ) AS dt,

       count(p.PatientGUID) as hcv_missed_doses
   FROM
     `tblpatient` p inner join tblpatientvisit v on p.patientguid=v.patientguid
   WHERE
       p.gender = 2 and p.age < 18 AND v.PillsLeft >0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
   GROUP BY
  v.Visit_Dt,
    p.id_mstfacility
  ) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_missed_doses = a.hcv_missed_doses where s.Gender=2 and s.Agewise=2";
 $this->db->query($query);
}

public function hcv_missed_doses_children_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT p.id_mstfacility,
     
        (
          v.Visit_Dt
    ) AS dt,

       count(p.PatientGUID) as hcv_missed_doses
   FROM
     `tblpatient` p inner join tblpatientvisit v on p.patientguid=v.patientguid
   WHERE
       p.gender = 3 and p.age < 18 AND v.PillsLeft >0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
   GROUP BY
  v.Visit_Dt,
    p.id_mstfacility
  ) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_missed_doses = a.hcv_missed_doses where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}

public function hcv_death_reported_adult_male(){

  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
         UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_death_reported
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age >= 18 AND InterruptReason = 1 AND DeathReason!=0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null and UpdatedOn!='0000-00-00' and UpdatedOn is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_death_reported = a.hcv_death_reported  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hcv_death_reported_adult_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_death_reported
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age >= 18 AND InterruptReason = 1 AND DeathReason!=0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null and UpdatedOn!='0000-00-00' and UpdatedOn is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_death_reported = a.hcv_death_reported  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hcv_death_reported_adult_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_death_reported
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age >= 18 AND InterruptReason = 1 AND DeathReason!=0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null and UpdatedOn!='0000-00-00' and UpdatedOn is not null
   GROUP BY
    UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_death_reported = a.hcv_death_reported  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hcv_death_reported_children_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_death_reported
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age < 18 AND InterruptReason = 1 AND DeathReason!=0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null and UpdatedOn!='0000-00-00' and UpdatedOn is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_death_reported = a.hcv_death_reported  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hcv_death_reported_children_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_death_reported
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age < 18 AND InterruptReason = 1 AND DeathReason!=0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null and UpdatedOn!='0000-00-00' and UpdatedOn is not null
   GROUP BY
  UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_death_reported = a.hcv_death_reported where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hcv_death_reported_children_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          UpdatedOn
) AS dt,

       count(PatientGUID) as hcv_death_reported
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age < 18 AND InterruptReason = 1 AND DeathReason!=0 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation!='0000-00-00' and T_Initiation is not null and UpdatedOn!='0000-00-00' and UpdatedOn is not null
   GROUP BY
   UpdatedOn,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_death_reported = a.hcv_death_reported  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}

function hcv_registered_adult_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
         date_of_patient_registration
) AS dt,

       count(PatientGUID) as hcv_registered
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age >= 18 AND (date_of_patient_registration is not null) and (date_of_patient_registration!='0000-00-00') and AntiHCV=1 
   GROUP BY
   date_of_patient_registration,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_registered = a.hcv_registered  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function hcv_registered_adult_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          date_of_patient_registration
) AS dt,

       count(PatientGUID) as hcv_registered
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age >= 18 AND (date_of_patient_registration is not null) and (date_of_patient_registration!='0000-00-00') and AntiHCV=1 
   GROUP BY
   date_of_patient_registration,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_registered = a.hcv_registered  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function hcv_registered_adult_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          date_of_patient_registration
) AS dt,

       count(PatientGUID) as hcv_registered
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age >= 18 AND (date_of_patient_registration is not null) and (date_of_patient_registration!='0000-00-00') and AntiHCV=1 
   GROUP BY
    date_of_patient_registration,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_registered = a.hcv_registered  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function hcv_registered_children_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          date_of_patient_registration
) AS dt,

       count(PatientGUID) as hcv_registered
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age < 18 AND (date_of_patient_registration is not null) and (date_of_patient_registration!='0000-00-00') and AntiHCV=1 
   GROUP BY
   date_of_patient_registration,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_registered = a.hcv_registered  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function hcv_registered_children_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          date_of_patient_registration
) AS dt,

       count(PatientGUID) as hcv_registered
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age < 18 AND (date_of_patient_registration is not null) and (date_of_patient_registration!='0000-00-00') and AntiHCV=1 
   GROUP BY
   date_of_patient_registration,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_registered = a.hcv_registered where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function hcv_registered_children_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          date_of_patient_registration
) AS dt,

       count(PatientGUID) as hcv_registered
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age < 18 AND (date_of_patient_registration is not null) and (date_of_patient_registration!='0000-00-00') and AntiHCV=1 
   GROUP BY
   date_of_patient_registration,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.hcv_registered = a.hcv_registered  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}
public function eligible_for_svr_adult_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
         ETR_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as eligible_for_svr
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age >= 18 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null  
   GROUP BY
   ETR_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.eligible_for_svr = a.eligible_for_svr  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function eligible_for_svr_adult_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          ETR_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as eligible_for_svr
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age >= 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
   ETR_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.eligible_for_svr = a.eligible_for_svr  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function eligible_for_svr_adult_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          ETR_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as eligible_for_svr
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age >= 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
    ETR_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.eligible_for_svr = a.eligible_for_svr  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function eligible_for_svr_children_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          ETR_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as eligible_for_svr
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age < 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
   ETR_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.eligible_for_svr = a.eligible_for_svr  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function eligible_for_svr_children_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          ETR_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as eligible_for_svr
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age < 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
  ETR_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.eligible_for_svr = a.eligible_for_svr where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function eligible_for_svr_children_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          ETR_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as eligible_for_svr
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age < 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
   ETR_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.eligible_for_svr = a.eligible_for_svr  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}
public function svr_done_adult_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
         SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_done
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age >= 18 and AntiHCV=1 AND Status IN (14,15) AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
   SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_done = a.svr_done  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function svr_done_adult_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_done
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age >= 18  and AntiHCV=1 AND Status IN (14,15) AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
   SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_done = a.svr_done  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function svr_done_adult_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_done
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age >= 18  and AntiHCV=1 AND Status IN (14,15) AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
    SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_done = a.svr_done  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function svr_done_children_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_done
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age < 18  and AntiHCV=1 AND Status IN (14,15) AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
   SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_done = a.svr_done  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function svr_done_children_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          ETR_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_done
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age < 18  and AntiHCV=1 AND Status IN (14,15) AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
  ETR_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_done = a.svr_done where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function svr_done_children_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          ETR_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_done
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age < 18  and AntiHCV=1 AND Status IN (14,15) AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
   ETR_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_done = a.svr_done  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}
public function svr_success_adult_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
         SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_success
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age >= 18 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and SVR12W_Result =2 AND Status IN (14,15) and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
   SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_success = a.svr_success  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function svr_success_adult_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_success
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age >= 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and SVR12W_Result =2 AND Status IN (14,15) and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
   SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_success = a.svr_success  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function svr_success_adult_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_success
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age >= 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and SVR12W_Result =2 AND Status IN (14,15) and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
    SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_success = a.svr_success  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function svr_success_children_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_success
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age < 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and SVR12W_Result =2 AND Status IN (14,15) and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
   SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_success = a.svr_success  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function svr_success_children_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          ETR_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_success
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age < 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and SVR12W_Result =2 AND Status IN (14,15) and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
  ETR_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_success = a.svr_success where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function svr_success_children_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          ETR_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_success
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age < 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and SVR12W_Result =2 AND Status IN (14,15) and  T_Initiation!='0000-00-00' and T_Initiation is not null 
   GROUP BY
   ETR_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_success = a.svr_success  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}

// out off 

public function svr_done_out_of_adult_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
         SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_done_out_of
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age >= 18 and AntiHCV=1 AND Status IN (14,15) AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null and ( left(ETR_HCVViralLoad_Dt,7)  = left(SVR12W_HCVViralLoad_Dt,7) )
   GROUP BY
   SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_done_out_of = a.svr_done_out_of  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function svr_done_out_of_adult_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_done_out_of
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age >= 18  and AntiHCV=1 AND Status IN (14,15) AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null and ( left(ETR_HCVViralLoad_Dt,7)  = left(SVR12W_HCVViralLoad_Dt,7) )
   GROUP BY
   SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_done_out_of = a.svr_done_out_of  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function svr_done_out_of_adult_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_done_out_of
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age >= 18  and AntiHCV=1 AND Status IN (14,15) AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null and ( left(ETR_HCVViralLoad_Dt,7)  = left(SVR12W_HCVViralLoad_Dt,7) )
   GROUP BY
    SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_done_out_of = a.svr_done_out_of  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function svr_done_out_of_children_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_done_out_of
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age < 18  and AntiHCV=1 AND Status IN (14,15) AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null and ( left(ETR_HCVViralLoad_Dt,7)  = left(SVR12W_HCVViralLoad_Dt,7) )
   GROUP BY
   SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_done_out_of = a.svr_done_out_of  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function svr_done_out_of_children_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          ETR_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_done_out_of
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age < 18  and AntiHCV=1 AND Status IN (14,15) AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null and ( left(ETR_HCVViralLoad_Dt,7)  = left(SVR12W_HCVViralLoad_Dt,7) )
   GROUP BY
  ETR_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_done_out_of = a.svr_done_out_of where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function svr_done_out_of_children_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          ETR_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_done_out_of
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age < 18  and AntiHCV=1 AND Status IN (14,15) AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and  T_Initiation!='0000-00-00' and T_Initiation is not null and ( left(ETR_HCVViralLoad_Dt,7)  = left(SVR12W_HCVViralLoad_Dt,7) )
   GROUP BY
   ETR_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_done_out_of = a.svr_done_out_of  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}

public function svr_success_out_of_adult_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
         SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_success_out_of
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age >= 18 and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and SVR12W_Result =2 and  T_Initiation!='0000-00-00' and T_Initiation is not null and ( left(ETR_HCVViralLoad_Dt,7)  = left(SVR12W_HCVViralLoad_Dt,7) )
   GROUP BY
   SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_success_out_of = a.svr_success_out_of  where s.Gender=1 and s.Agewise=1";
$this->db->query($query);
}


public function svr_success_out_of_adult_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_success_out_of
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age >= 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and SVR12W_Result =2 and  T_Initiation!='0000-00-00' and T_Initiation is not null and ( left(ETR_HCVViralLoad_Dt,7)  = left(SVR12W_HCVViralLoad_Dt,7) )
   GROUP BY
   SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_success_out_of = a.svr_success_out_of  where s.Gender=2 and s.Agewise=1";
$this->db->query($query);
}
public function svr_success_out_of_adult_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_success_out_of
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age >= 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and SVR12W_Result =2 and  T_Initiation!='0000-00-00' and T_Initiation is not null and ( left(ETR_HCVViralLoad_Dt,7)  = left(SVR12W_HCVViralLoad_Dt,7) )
   GROUP BY
    SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_success_out_of = a.svr_success_out_of  where s.Gender=3 and s.Agewise=1";
$this->db->query($query);
}

public function svr_success_out_of_children_male(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          SVR12W_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_success_out_of
   FROM
       tblpatient p 
   WHERE
       gender = 1 and age < 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and SVR12W_Result =2 and  T_Initiation!='0000-00-00' and T_Initiation is not null and ( left(ETR_HCVViralLoad_Dt,7)  = left(SVR12W_HCVViralLoad_Dt,7) )
   GROUP BY
   SVR12W_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_success_out_of = a.svr_success_out_of  where s.Gender=1 and s.Agewise=2";
$this->db->query($query);
}


public function svr_success_out_of_children_female(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          ETR_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_success_out_of
   FROM
       tblpatient p 
   WHERE
       gender = 2 and age < 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and SVR12W_Result =2 and  T_Initiation!='0000-00-00' and T_Initiation is not null and ( left(ETR_HCVViralLoad_Dt,7)  = left(SVR12W_HCVViralLoad_Dt,7) )
   GROUP BY
  ETR_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_success_out_of = a.svr_success_out_of where s.Gender=2 and s.Agewise=2";
$this->db->query($query);
}
public function svr_success_out_of_children_transgender(){
  $query = "UPDATE
   tblsummary_genderwise s
INNER JOIN(
   SELECT id_mstfacility,
     
        (
          ETR_HCVViralLoad_Dt
) AS dt,

       count(PatientGUID) as svr_success_out_of
   FROM
       tblpatient p 
   WHERE
       gender = 3 and age < 18  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and SVR12W_Result =2 and  T_Initiation!='0000-00-00' and T_Initiation is not null and ( left(ETR_HCVViralLoad_Dt,7)  = left(SVR12W_HCVViralLoad_Dt,7) )
   GROUP BY
   ETR_HCVViralLoad_Dt,
       id_mstfacility
) a
ON
   s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
SET
   s.svr_success_out_of = a.svr_success_out_of  where s.Gender=3 and s.Agewise=2";
$this->db->query($query);
}



/* LFU gender wise 8_8_20*/

//VL LTFU


public function lfu_follow_up_analysis_adult_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					             MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS ltfu_viral_load
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND Gender = 1 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  (T_DLL_01_VLC_Date is null || T_DLL_01_VLC_Date='0000-00-00')
					        and  DATEDIFF (now(), 
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								) ) >7 

					    GROUP BY
					        
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_viral_load_a_male = a.ltfu_viral_load WHERE s.Gender=1 and s.Agewise=1";

		$this->db->query($query); 
}

//

public function lfu_follow_up_analysis_adult_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					             MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(*) AS loss_of_follow_viral_lfu
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 Gender = 2 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  (T_DLL_01_VLC_Date is null || T_DLL_01_VLC_Date='0000-00-00')
					        and  DATEDIFF (now(), 
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								) ) >7 

					    GROUP BY
					        
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_viral_load_a_female = a.loss_of_follow_viral_lfu WHERE s.Gender=2 and s.Agewise=1";

		$this->db->query($query); 
}


//

public function lfu_follow_up_analysis_adult_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					             MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(*) AS loss_of_follow_viral_lfu
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND Gender = 3 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  (T_DLL_01_VLC_Date is null || T_DLL_01_VLC_Date='0000-00-00')
					        and  DATEDIFF (now(), 
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								) ) >7 

					    GROUP BY
					        
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_viral_load_a_transgender = a.loss_of_follow_viral_lfu WHERE s.Gender=3 and s.Agewise=1";

		$this->db->query($query); 
}

//

public function lfu_follow_up_analysis_children_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					             MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(*) AS loss_of_follow_viral_lfu
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND Gender = 1 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  (T_DLL_01_VLC_Date is null || T_DLL_01_VLC_Date='0000-00-00')
					        and  DATEDIFF (now(), 
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								) ) >7 

					    GROUP BY
					        
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_viral_load_c_male = a.loss_of_follow_viral_lfu WHERE s.Gender=1 and s.Agewise=2";

		$this->db->query($query); 
}

//
public function lfu_follow_up_analysis_children_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					             MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(*) AS loss_of_follow_viral_lfu
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND Gender = 2 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  (T_DLL_01_VLC_Date is null || T_DLL_01_VLC_Date='0000-00-00')
					        and  DATEDIFF (now(), 
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								) ) >7 

					    GROUP BY
					        
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_viral_load_c_female = a.loss_of_follow_viral_lfu WHERE s.Gender=2 and s.Agewise=2";

		$this->db->query($query); 
}

//

public function lfu_follow_up_analysis_children_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					             MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(*) AS loss_of_follow_viral_lfu
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND Gender = 3 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  (T_DLL_01_VLC_Date is null || T_DLL_01_VLC_Date='0000-00-00')
					        and  DATEDIFF (now(), 
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								) ) >7 

					    GROUP BY
					        
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_viral_load_c_transgender = a.loss_of_follow_viral_lfu WHERE s.Gender=3 and s.Agewise=2";

		$this->db->query($query); 
}


// end vl ltfu

// Start lfu_follow_1St_dispensation 

public function lfu_follow_1St_dispensation_adult_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,

					        count(PatientGUID) AS loss_of_follow_1St_Count
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND Gender=1 AND Age >=18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_DLL_01_VLC_Date is not null AND T_DLL_01_VLC_Date!='0000-00-00' and T_DLL_01_VLC_Result=1 and  DATEDIFF (now(), T_DLL_01_VLC_Date) >7 and (T_Initiation is null ||  T_Initiation='0000-00-00')  and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					       T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_1St_dispensation_a_male = a.loss_of_follow_1St_Count WHERE s.Gender=1 and s.Agewise=1";

		$this->db->query($query); 
}

//

public function lfu_follow_1St_dispensation_adult_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,

					        count(PatientGUID) AS loss_of_follow_1St_Count
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND Gender=2 AND Age >=18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_DLL_01_VLC_Date is not null AND T_DLL_01_VLC_Date!='0000-00-00' and T_DLL_01_VLC_Result=1 and  DATEDIFF (now(), T_DLL_01_VLC_Date) >7 and (T_Initiation is null ||  T_Initiation='0000-00-00')  and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					       T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_1St_dispensation_a_female = a.loss_of_follow_1St_Count WHERE s.Gender=2 and s.Agewise=1";

		$this->db->query($query); 
}

//

public function lfu_follow_1St_dispensation_adult_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,

					        count(PatientGUID) AS loss_of_follow_1St_Count
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND Gender =3 AND Age >=18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_DLL_01_VLC_Date is not null AND T_DLL_01_VLC_Date!='0000-00-00' and T_DLL_01_VLC_Result=1 and  DATEDIFF (now(), T_DLL_01_VLC_Date) >7 and (T_Initiation is null ||  T_Initiation='0000-00-00')  and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					       T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_1St_dispensation_a_transgender = a.loss_of_follow_1St_Count WHERE s.Gender=3 and s.Agewise=1";

		$this->db->query($query); 
}

//
public function lfu_follow_1St_dispensation_children_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,

					        count(PatientGUID) AS loss_of_follow_1St_Count
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND Gender = 1 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_DLL_01_VLC_Date is not null AND T_DLL_01_VLC_Date!='0000-00-00' and T_DLL_01_VLC_Result=1 and  DATEDIFF (now(), T_DLL_01_VLC_Date) >7 and (T_Initiation is null ||  T_Initiation='0000-00-00')  and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					       T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_1St_dispensation_c_male = a.loss_of_follow_1St_Count WHERE s.Gender=1 and s.Agewise=2";

		$this->db->query($query); 
}
//

public function lfu_follow_1St_dispensation_children_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,

					        count(PatientGUID) AS loss_of_follow_1St_Count
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND Gender = 2 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_DLL_01_VLC_Date is not null AND T_DLL_01_VLC_Date!='0000-00-00' and T_DLL_01_VLC_Result=1 and  DATEDIFF (now(), T_DLL_01_VLC_Date) >7 and (T_Initiation is null ||  T_Initiation='0000-00-00')  and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					       T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_1St_dispensation_c_female = a.loss_of_follow_1St_Count WHERE s.Gender=2 and s.Agewise=2";

		$this->db->query($query); 
}
//

public function lfu_follow_1St_dispensation_children_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,

					        count(PatientGUID) AS loss_of_follow_1St_Count
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND Gender = 3 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_DLL_01_VLC_Date is not null AND T_DLL_01_VLC_Date!='0000-00-00' and T_DLL_01_VLC_Result=1 and  DATEDIFF (now(), T_DLL_01_VLC_Date) >7 and (T_Initiation is null ||  T_Initiation='0000-00-00')  and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					       T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_1St_dispensation_c_transgender = a.loss_of_follow_1St_Count WHERE s.Gender=3 and s.Agewise=2";

		$this->db->query($query); 
}



// End lfu_follow_1St_dispensation


// Start lfu_follow_2St_dispensation

public function lfu_follow_2St_dispensation_adult_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation
					) AS dt,

					        count(PatientGUID) AS follow_2St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					    AntiHCV=1 AND Gender=1 AND Age >=18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS not NULL AND T_Initiation!='0000-00-00' and status in (4,18,20,24)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					     T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_2St_dispensation_a_male = a.follow_2St_dispensation WHERE s.Gender=1 AND s.Agewise=1";

		$this->db->query($query); 
}

//
public function lfu_follow_2St_dispensation_adult_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation
					) AS dt,

					        count(PatientGUID) AS follow_2St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					    AntiHCV=1 AND Gender = 2 AND Age >=18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS not NULL AND T_Initiation!='0000-00-00' and status in (4,18,20,24)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					     T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_2St_dispensation_a_female = a.follow_2St_dispensation WHERE s.Gender=2 AND s.Agewise=1";

		$this->db->query($query); 
}

//
public function lfu_follow_2St_dispensation_adult_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation
					) AS dt,

					        count(PatientGUID) AS follow_2St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					    AntiHCV=1 AND Gender = 3 AND Age >=18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS not NULL AND T_Initiation!='0000-00-00' and status in (4,18,20,24)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					     T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_2St_dispensation_a_transgender = a.follow_2St_dispensation WHERE s.Gender=3 AND s.Agewise=1";

		$this->db->query($query); 
}
//

public function lfu_follow_2St_dispensation_children_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation
					) AS dt,

					        count(PatientGUID) AS follow_2St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					    AntiHCV=1 AND Gender=1 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS not NULL AND T_Initiation!='0000-00-00' and status in (4,18,20,24)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					     T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_2St_dispensation_c_male = a.follow_2St_dispensation WHERE s.Gender=1 AND s.Agewise=2";

		$this->db->query($query); 
}
//

public function lfu_follow_2St_dispensation_children_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation
					) AS dt,

					        count(PatientGUID) AS follow_2St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					    AntiHCV=1 AND Gender =2 Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS not NULL AND T_Initiation!='0000-00-00' and status in (4,18,20,24)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					     T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_2St_dispensation_c_female = a.follow_2St_dispensation WHERE s.Gender=2 AND s.Agewise=2";

		$this->db->query($query); 
}
//

public function lfu_follow_2St_dispensation_children_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation
					) AS dt,

					        count(PatientGUID) AS follow_2St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					    AntiHCV=1 AND Gender = 3 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS not NULL AND T_Initiation!='0000-00-00' and status in (4,18,20,24)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					     T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_2St_dispensation_c_transgender = a.follow_2St_dispensation WHERE s.Gender=3 AND s.Agewise=2";

		$this->db->query($query); 
}

// End lfu_follow_2St_dispensation

// Start lfu_follow_3St_dispensation

public function lfu_follow_3St_dispensation_adult_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_3St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					      AntiHCV=1 AND Gender =1 AND Age >=18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status in (5,6,21,25)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					    T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_3St_dispensation_a_male = a.follow_3St_dispensation WHERE s.Gender=1 AND s.Agewise=1";

		$this->db->query($query); 
}

//

public function lfu_follow_3St_dispensation_adult_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_3St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					      AntiHCV=1 AND Gender = 2 AND Age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status in (5,6,21,25)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					    T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_3St_dispensation_a_female = a.follow_3St_dispensation WHERE s.Gender=2 AND s.Agewise=1";

		$this->db->query($query); 
}

//

public function lfu_follow_3St_dispensation_adult_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_3St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					      AntiHCV=1 AND Gender=3 AND Age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status in (5,6,21,25)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					    T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_3St_dispensation_a_transgender = a.follow_3St_dispensation WHERE s.Gender=3 AND s.Agewise=1";

		$this->db->query($query); 
}

//
public function lfu_follow_3St_dispensation_children_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_3St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					      AntiHCV=1 AND Gender = 1 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status in (5,6,21,25)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					    T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_3St_dispensation_c_male = a.follow_3St_dispensation WHERE s.Gender=1 AND s.Agewise=2";

		$this->db->query($query); 
}
//
public function lfu_follow_3St_dispensation_children_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_3St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					      AntiHCV=1 AND Gender = 2 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status in (5,6,21,25)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					    T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_3St_dispensation_c_female = a.follow_3St_dispensation WHERE s.Gender=2 AND s.Agewise=2";

		$this->db->query($query); 
}
//

public function lfu_follow_3St_dispensation_children_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_3St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					      AntiHCV=1 AND Gender = 3 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status in (5,6,21,25)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					    T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_3St_dispensation_c_transgender = a.follow_3St_dispensation WHERE s.Gender=3 AND s.Agewise=2";

		$this->db->query($query); 
}

// End lfu_follow_3rd_dispensation

// Start lfu_follow_4St_dispensation

public function lfu_follow_4St_dispensation_adult_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					          T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_4St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					      AntiHCV=1 AND Gender = 1 AND Age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and   T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00'  and status in (8,22,27) and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_4St_dispensation_a_male = a.follow_4St_dispensation WHERE s.Gender=1 AND s.Agewise=1";

		$this->db->query($query); 
}

//

public function lfu_follow_4St_dispensation_adult_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					          T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_4St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					      AntiHCV=1 AND Gender = 2 AND Age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and   T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00'  and status in (8,22,27) and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_4St_dispensation_a_female = a.follow_4St_dispensation WHERE s.Gender=2 AND s.Agewise=1";

		$this->db->query($query); 
}

//
public function lfu_follow_4St_dispensation_adult_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					          T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_4St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					      AntiHCV=1 AND Gender = 3 AND Age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and   T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00'  and status in (8,22,27) and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_4St_dispensation_a_transgender = a.follow_4St_dispensation WHERE s.Gender=3 AND s.Agewise=1";

		$this->db->query($query); 
}

//
public function lfu_follow_4St_dispensation_chaildren_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					          T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_4St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					      AntiHCV=1 AND Gender = 1 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and   T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00'  and status in (8,22,27) and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_4St_dispensation_c_male = a.follow_4St_dispensation WHERE s.Gender=1 AND s.Agewise=2";

		$this->db->query($query); 
}

//
public function lfu_follow_4St_dispensation_childreen_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					          T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_4St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					      AntiHCV=1 AND Gender = 2 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and   T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00'  and status in (8,22,27) and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_4St_dispensation_c_female = a.follow_4St_dispensation s.Gender=2 AND s.Agewise=2";

		$this->db->query($query); 
}

//
public function lfu_follow_4St_dispensation_children_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					          T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_4St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					      AntiHCV=1 AND Gender = 3 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and   T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00'  and status in (8,22,27) and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_4St_dispensation_c_transgender = a.follow_4St_dispensation WHERE s.Gender=3 AND s.Agewise=2";

		$this->db->query($query); 
}


// End lfu_follow_4St_dispensation

// Start lfu_follow_5St_dispensation


public function lfu_follow_5St_dispensation_adult_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_5St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					       AntiHCV=1 AND Gender = 1 AND Age >=18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status in (9,27)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					     T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_5St_dispensation_a_male = a.follow_5St_dispensation WHERE s.Gender=1 AND s.Agewise=1";

		$this->db->query($query); 
}

//

public function lfu_follow_5St_dispensation_adult_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_5St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					       AntiHCV=1 AND Gender = 2 AND Age >=18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status in (9,27)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					     T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_5St_dispensation_a_female = a.follow_5St_dispensation WHERE s.Gender=2 AND s.Agewise=1";

		$this->db->query($query); 
}

//

public function lfu_follow_5St_dispensation_adult_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_5St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					       AntiHCV=1 AND Gender = 3 AND Age >=18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status in (9,27)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					     T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_5St_dispensation_a_transgender = a.follow_5St_dispensation WHERE s.Gender=3 AND s.Agewise=1";

		$this->db->query($query); 
}
//

public function lfu_follow_5St_dispensation_children_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_5St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					       AntiHCV=1 AND Gender = 1 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status in (9,27)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					     T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_5St_dispensation_c_male = a.follow_5St_dispensation WHERE s.Gender=1 AND s.Agewise=2";

		$this->db->query($query); 
}

//

public function lfu_follow_5St_dispensation_children_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_5St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					       AntiHCV=1 AND Gender = 2 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status in (9,27)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					     T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_5St_dispensation_c_female = a.follow_5St_dispensation WHERE s.Gender=2 AND s.Agewise=2";

		$this->db->query($query); 
}
//

public function lfu_follow_5St_dispensation_children_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_5St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					       AntiHCV=1 AND Gender = 3 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status in (9,27)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					     T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_5St_dispensation_c_transgender = a.follow_5St_dispensation WHERE s.Gender=3 AND s.Agewise=2";

		$this->db->query($query); 
}

// END lfu_follow_5St_dispensation

// Start lfu_follow_6St_dispensation

public function lfu_follow_6St_dispensation_adult_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					         T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_6St_dispensation
					    FROM
					        tblpatient
					    WHERE
					      AntiHCV=1 AND Gender = 1 AND Age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status =10 and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_6St_dispensation_a_male = a.follow_6St_dispensation WHERE s.Gender=1 AND s.Agewise=1";

		$this->db->query($query); 
}

//

public function lfu_follow_6St_dispensation_adult_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					         T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_6St_dispensation
					    FROM
					        tblpatient
					    WHERE
					      AntiHCV=1 AND Gender = 2 AND Age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status =10 and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_6St_dispensation_a_female = a.follow_6St_dispensation WHERE s.Gender=2 AND s.Agewise=1";

		$this->db->query($query); 
}
//
public function lfu_follow_6St_dispensation_adult_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					         T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_6St_dispensation
					    FROM
					        tblpatient
					    WHERE
					      AntiHCV=1 AND Gender = 3 AND Age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status =10 and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_6St_dispensation_a_transgender = a.follow_6St_dispensation WHERE s.Gender=3 AND s.Agewise=1";

		$this->db->query($query); 
}

//

public function lfu_follow_6St_dispensation_children_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					         T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_6St_dispensation
					    FROM
					        tblpatient
					    WHERE
					      AntiHCV=1 AND Gender = 1 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status =10 and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_6St_dispensation_c_male = a.follow_6St_dispensation WHERE s.Gender=1 AND s.Agewise=2";

		$this->db->query($query); 
}
//

public function lfu_follow_6St_dispensation_children_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					         T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_6St_dispensation
					    FROM
					        tblpatient
					    WHERE
					      AntiHCV=1 AND Gender = 2 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status =10 and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_6St_dispensation_c_female = a.follow_6St_dispensation WHERE s.Gender=2 AND s.Agewise=2";

		$this->db->query($query); 
}

//

public function lfu_follow_6St_dispensation_children_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					         T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_6St_dispensation
					    FROM
					        tblpatient
					    WHERE
					      AntiHCV=1  AND Gender = 3 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status =10 and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_6St_dispensation_c_transgender = a.follow_6St_dispensation WHERE s.Gender=3 AND s.Agewise=2";

		$this->db->query($query); 
}

// End lfu_follow_6St_dispensation


// Start lfu_follow_SVR

public function lfu_follow_SVR_adult_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					   SELECT

					        id_mstfacility,
					        (
					           ETR_HCVViralLoad_Dt
					) AS dt,

					        count(*) AS follow_SVR,
					        datediff(
							NOW(),ETR_HCVViralLoad_Dt),
							SVR12W_HCVViralLoad_Dt
					    FROM
					        tblpatient
					    WHERE
					     AntiHCV=1 AND Gender=1 AND Age >=18  AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' AND ETR_HCVViralLoad_Dt!='1900-01-19' AND status NOT IN (14,15) and datediff(
							NOW(),ETR_HCVViralLoad_Dt) >84 AND (SVR12W_HCVViralLoad_Dt IS NULL || SVR12W_HCVViralLoad_Dt='0000-00-00') and SVR_TreatmentStatus not in (7,1)
				 
					    GROUP BY
					      ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_SVR_a_male = a.follow_SVR WHERE s.Gender=1 AND s.Agewise=1";

		$this->db->query($query); 
}

//

public function lfu_follow_SVR_adult_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					   SELECT

					        id_mstfacility,
					        (
					           ETR_HCVViralLoad_Dt
					) AS dt,

					        count(*) AS follow_SVR,
					        datediff(
							NOW(),ETR_HCVViralLoad_Dt),
							SVR12W_HCVViralLoad_Dt
					    FROM
					        tblpatient
					    WHERE
					     AntiHCV=1 AND Gender=2 AND Age >=18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' AND ETR_HCVViralLoad_Dt!='1900-01-19' AND status NOT IN (14,15) and datediff(
							NOW(),ETR_HCVViralLoad_Dt) >84 AND (SVR12W_HCVViralLoad_Dt IS NULL || SVR12W_HCVViralLoad_Dt='0000-00-00') and SVR_TreatmentStatus not in (7,1)
				 
					    GROUP BY
					      ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_SVR_a_female = a.follow_SVR WHERE s.Gender=2 AND s.Agewise=1";

		$this->db->query($query); 
}
//

public function lfu_follow_SVR_adult_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					   SELECT

					        id_mstfacility,
					        (
					           ETR_HCVViralLoad_Dt
					) AS dt,

					        count(*) AS follow_SVR,
					        datediff(
							NOW(),ETR_HCVViralLoad_Dt),
							SVR12W_HCVViralLoad_Dt
					    FROM
					        tblpatient
					    WHERE
					     AntiHCV=1 AND Gender=3 AND Age >=18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' AND ETR_HCVViralLoad_Dt!='1900-01-19' AND status NOT IN (14,15) and datediff(
							NOW(),ETR_HCVViralLoad_Dt) >84 AND (SVR12W_HCVViralLoad_Dt IS NULL || SVR12W_HCVViralLoad_Dt='0000-00-00') and SVR_TreatmentStatus not in (7,1)
				 
					    GROUP BY
					      ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_SVR_a_transgender = a.follow_SVR WHERE s.Gender=3 AND s.Agewise=1";

		$this->db->query($query); 
}

//

public function lfu_follow_SVR_children_male(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					   SELECT

					        id_mstfacility,
					        (
					           ETR_HCVViralLoad_Dt
					) AS dt,

					        count(*) AS follow_SVR,
					        datediff(
							NOW(),ETR_HCVViralLoad_Dt),
							SVR12W_HCVViralLoad_Dt
					    FROM
					        tblpatient
					    WHERE
					     AntiHCV=1 AND Gender=1 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' AND ETR_HCVViralLoad_Dt!='1900-01-19' AND status NOT IN (14,15) and datediff(
							NOW(),ETR_HCVViralLoad_Dt) >84 AND (SVR12W_HCVViralLoad_Dt IS NULL || SVR12W_HCVViralLoad_Dt='0000-00-00') and SVR_TreatmentStatus not in (7,1)
				 
					    GROUP BY
					      ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_SVR_c_male = a.follow_SVR WHERE s.Gender=1 AND s.Agewise=2";

		$this->db->query($query); 
}

//

public function lfu_follow_SVR_children_female(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					   SELECT

					        id_mstfacility,
					        (
					           ETR_HCVViralLoad_Dt
					) AS dt,

					        count(*) AS follow_SVR,
					        datediff(
							NOW(),ETR_HCVViralLoad_Dt),
							SVR12W_HCVViralLoad_Dt
					    FROM
					        tblpatient
					    WHERE
					     AntiHCV=1 AND Gender=2 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' AND ETR_HCVViralLoad_Dt!='1900-01-19' AND status NOT IN (14,15) and datediff(
							NOW(),ETR_HCVViralLoad_Dt) >84 AND (SVR12W_HCVViralLoad_Dt IS NULL || SVR12W_HCVViralLoad_Dt='0000-00-00') and SVR_TreatmentStatus not in (7,1)
				 
					    GROUP BY
					      ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_SVR_c_female = a.follow_SVR WHERE s.Gender=2 AND s.Agewise=2";

		$this->db->query($query); 
}

//

public function lfu_follow_SVR_children_transgender(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					   SELECT

					        id_mstfacility,
					        (
					           ETR_HCVViralLoad_Dt
					) AS dt,

					        count(*) AS follow_SVR,
					        datediff(
							NOW(),ETR_HCVViralLoad_Dt),
							SVR12W_HCVViralLoad_Dt
					    FROM
					        tblpatient
					    WHERE
					     AntiHCV=1 AND Gender=3 AND Age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' AND ETR_HCVViralLoad_Dt!='1900-01-19' AND status NOT IN (14,15) and datediff(
							NOW(),ETR_HCVViralLoad_Dt) >84 AND (SVR12W_HCVViralLoad_Dt IS NULL || SVR12W_HCVViralLoad_Dt='0000-00-00') and SVR_TreatmentStatus not in (7,1)
				 
					    GROUP BY
					      ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_SVR_c_transgender = a.follow_SVR WHERE s.Gender=3 AND s.Agewise=2";

		$this->db->query($query); 
}

// END lfu_follow_SVR


/*end lfu*/


}	