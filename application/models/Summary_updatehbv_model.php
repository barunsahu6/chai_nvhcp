<?php  
class Summary_updatehbv_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_Model');
		ini_set('memory_limit', '-1');
		set_time_limit(100000);
        ini_set('memory_limit', '100000M');
        ini_set('upload_max_filesize', '300000M');
        ini_set('post_max_size', '200000M');
        ini_set('max_input_time', 100000);
        ini_set('max_execution_time', 0);
        ini_set('memory_limit','-1');

		//error_reporting(E_ALL);
	}


public function null_everything_hbv(){

	$query = "UPDATE
					    tblsummary_new
					SET
						`anti_hbs_screened` = null,
						`anti_hbs_positive` = null,
						`TestForALT_Level` = null,
						`Cirrhotic` = null,
						`ElevatedALT_Level`= NULL,
						`EligibleHBV_DNATest` = null,
						`HBVDNA_TestConducted` = null,
						`EligibleFor_TreatmentHBV` = null,
						`InitiatedOnTreatmentHEPB` = null,
						`ContinuedOnTreatment` = null,
						`compensated_cirrhotic_hepb` = null,
						`decompensated_cirrhotic_hepb` = null,
						`regimen_hepb_male` = null,
						`regimen_hepb_female` = null,
						`regimen_hepb_transgender` = null,
						`age_less_than_10_hepb` = null,
						`age_10_to_20_hepb` = null,
						`age_21_to_30_hepb` = null,
						`age_31_to_40_hepb` = null,
						`age_41_to_50_hepb` = null,
						`age_51_to_60_hepb` = null,
						`age_greater_than_60_hepb` = null,
						`regimen_hepb_TAF` = null,
						`regimen_hepb_Entecavir` = null,
						`regimen_hepb_TDF` = null,
						`hbv_non_cirrhotic`	= NULL,
						`hbv_compensated_cirrhotic` = NULL,
						`hbv_decompensated_cirrhotic` = NULL,
						`hbv_cirrhotic_status_notavaliable` = NULL,
						`regimen_hepb_Others` = null";

		$this->db->query($query);

}


public function update_everything_hepb(){

		$this->cascade_anti_hepb_screen();
		$this->cascade_anti_hepb_positive();
		$this->cascade_anti_testforALT_Level();
		$this->cascade_anti_Cirrhotic();
		$this->cascade_anti_ElevatedALT_Level();
		$this->cascade_anti_EligibleHBV_DNATest();
		$this->cascade_anti_HBVDNA_TestConducted();
		$this->cascade_anti_EligibleFor_TreatmentHBV();
		$this->cascade_anti_InitiatedOnTreatmentHEPB();
		$this->cascade_anti_ContinuedOnTreatment();
		$this->compensated_cirrhotic_hepb();
		$this->decompensated_cirrhotic_hepb();
		$this->regimen_hepb_TAF();
		$this->regimen_hepb_Entecavir();
		$this->regimen_hepb_TDF();
		$this->regimen_hepb_Others();
		$this->age_less_than_10_hepb();
		$this->age_10_to_20_hepb();
		$this->age_21_to_30_hepb();
		$this->age_31_to_40_hepb();
		$this->age_41_to_50_hepb();
		$this->age_51_to_60_hepb();
		$this->age_greater_than_60_hepb();
		$this->update_cirrhotic();
		$this->update_genderwise_InitiatedOnTreatment();
		

}
public function update_genderwise_hepb($gender,$agewise){


		$this->genderwise_hbv_screened($gender,$agewise);
		$this->genderwise_hbv_screened_positive($gender,$agewise);
		$this->genderwise_hbv_testforALT_Level($gender,$agewise);
		$this->genderwise_hbv_Cirrhotic($gender,$agewise);
		$this->genderwise_hbv_ElevatedALT_Level($gender,$agewise);
		$this->genderwise_hbv_EligibleHBV_DNATest($gender,$agewise);
		$this->genderwise_hbv_HBVDNA_TestConducted($gender,$agewise);
		$this->genderwise_hbv_EligibleFor_TreatmentHBV($gender,$agewise);
		$this->genderwise_hbv_InitiatedOnTreatmentHEPB($gender,$agewise);
		$this->genderwise_hbv_ContinuedOnTreatment($gender,$agewise);

		$this->genderwise_hbv_regimen_hepb_TAF($gender,$agewise);
		$this->genderwise_hbv_regimen_hepb_Entecavir($gender,$agewise);
		$this->genderwise_hbv_regimen_hepb_TDF($gender,$agewise);
		$this->genderwise_hbv_regimen_Others($gender,$agewise);

	}



public function cascade_anti_hepb_screen()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HBSRapidResult
					    FROM
					        tblpatient
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hbs_screened = a.HBSRapidResult ";

		$this->db->query($query);
	}



public function cascade_anti_hepb_positive()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HBSRapidResult
					    FROM
					        tblpatient
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1)
					    GROUP BY
					       
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hbs_positive = a.HBSRapidResult ";

		$this->db->query($query);
	}


	
public function cascade_anti_testforALT_Level()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Prescribing_Dt) AS dt,


					      count(p.patientguid)  as TestForALT_Level
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND hepb.Elevated_ALT_Level in(1,2,3)
					    GROUP BY
					       
								hepb.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.TestForALT_Level = a.TestForALT_Level ";

		$this->db->query($query);
	}


public function cascade_anti_Cirrhotic()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Prescribing_Dt) AS dt,


					      count(p.patientguid)  as Cirrhotic
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND hepb.V1_Cirrhosis =1
					    GROUP BY
					       
								hepb.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.Cirrhotic = a.Cirrhotic ";

		$this->db->query($query);
	}

public function cascade_anti_ElevatedALT_Level()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Prescribing_Dt) AS dt,


					      count(p.patientguid)  as ElevatedALT_Level
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND hepb.Elevated_ALT_Level=2
					    GROUP BY
					       
								hepb.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.ElevatedALT_Level = a.ElevatedALT_Level ";

		$this->db->query($query);
	}


public function cascade_anti_EligibleHBV_DNATest()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Prescribing_Dt) AS dt,


					      count(p.patientguid)  as EligibleHBV_DNATest
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  
					    GROUP BY
					       
								p.BVLSampleCollectionDate,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.EligibleHBV_DNATest = a.EligibleHBV_DNATest ";

		$this->db->query($query);
	}



public function cascade_anti_HBVDNA_TestConducted()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (p.T_DLL_01_BVLC_Date) AS dt,


					      count(p.patientguid)  as HBVDNA_TestConducted
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND BVLSampleCollectionDate IS NOT NULL AND BVLSampleCollectionDate!='0000-00-00'  AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00'
					    GROUP BY 
					       
								p.T_DLL_01_BVLC_Date,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.HBVDNA_TestConducted = a.HBVDNA_TestConducted ";

		$this->db->query($query);
	}


public function cascade_anti_EligibleFor_TreatmentHBV()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.PrescribingDate) AS dt,


					      count(p.patientguid)  as EligibleFor_TreatmentHBV
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1)  and hepb.PrescribingDate IS NOT NULL and hepb.PrescribingDate!='0000-00-00' and T_DLL_01_BVLC_Result=1  and TreatmentRecommended=1
					    GROUP BY 
					       
								hepb.PrescribingDate,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.EligibleFor_TreatmentHBV = a.EligibleFor_TreatmentHBV ";

		$this->db->query($query);
	}


public function cascade_anti_InitiatedOnTreatmentHEPB()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Treatment_Dt) AS dt,


					      count(p.patientguid)  as InitiatedOnTreatmentHEPB
					    FROM
					        tblpatient p LEFT JOIN tblpatientdispensationb hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' and hepb.Treatment_Dt IS NOT NULL and hepb.Treatment_Dt!='0000-00-00'  and hepb.VisitNo=1
					    GROUP BY 
					       
								hepb.Treatment_Dt,
					        p.id_mstfacility 
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.InitiatedOnTreatmentHEPB = a.InitiatedOnTreatmentHEPB ";

		$this->db->query($query);
	}

public function cascade_anti_ContinuedOnTreatment()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Treatment_Dt) AS dt,


					      count(p.patientguid)  as ContinuedOnTreatment
					    FROM
					        tblpatient p INNER JOIN hepb_tblpatient b ON p.PatientGUID=b.PatientGUID LEFT JOIN tblpatientdispensationb hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' and hepb.Treatment_Dt IS NOT NULL and hepb.Treatment_Dt!='0000-00-00' and hepb.VisitNo >=1 AND (b.InterruptReason IS NULL || b.InterruptReason=0)
					    GROUP BY 
					       
								hepb.Treatment_Dt,
									hepb.PatientGUID,
					        p.id_mstfacility 
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.ContinuedOnTreatment = a.ContinuedOnTreatment ";

		$this->db->query($query);
	}



public function compensated_cirrhotic_hepb()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Prescribing_Dt) AS dt,


					      count(p.patientguid)  as compensated_cirrhotic_hepb
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND hepb.CirrhosisStatus =1
					    GROUP BY
					       
								hepb.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.compensated_cirrhotic_hepb = a.compensated_cirrhotic_hepb ";

		$this->db->query($query);
	}

	public function decompensated_cirrhotic_hepb()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Prescribing_Dt) AS dt,


					      count(p.patientguid)  as decompensated_cirrhotic_hepb
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND hepb.CirrhosisStatus =2
					    GROUP BY
					       
								hepb.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.decompensated_cirrhotic_hepb = a.decompensated_cirrhotic_hepb ";

		$this->db->query($query);
	}


public function update_genderwise_InitiatedOnTreatment(){

	$this->Summary_updatehbv_model->cascade_anti_InitiatedOnTreatmentHEPB_male();
	$this->Summary_updatehbv_model->cascade_anti_InitiatedOnTreatmentHEPB_frmale();
	$this->Summary_updatehbv_model->cascade_anti_InitiatedOnTreatmentHEPB_transgender();
	
}


public function cascade_anti_InitiatedOnTreatmentHEPB_male()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Treatment_Dt) AS dt,


					      count(p.patientguid)  as InitiatedOnTreatmentHEPB
					    FROM
					        tblpatient p LEFT JOIN tblpatientdispensationb hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' and hepb.Treatment_Dt IS NOT NULL and hepb.Treatment_Dt!='0000-00-00' and hepb.VisitNo=1 AND p.Gender=1
					    GROUP BY 
					       
								hepb.Treatment_Dt,
					        p.id_mstfacility 
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.regimen_hepb_male = a.InitiatedOnTreatmentHEPB ";

		$this->db->query($query);
	}

public function cascade_anti_InitiatedOnTreatmentHEPB_frmale()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Treatment_Dt) AS dt,


					      count(p.patientguid)  as InitiatedOnTreatmentHEPB
					    FROM
					        tblpatient p LEFT JOIN tblpatientdispensationb hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' and hepb.Treatment_Dt IS NOT NULL and hepb.Treatment_Dt!='0000-00-00' and hepb.VisitNo=1 AND p.Gender=2
					    GROUP BY 
					       
								hepb.Treatment_Dt,
					        p.id_mstfacility 
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.regimen_hepb_female = a.InitiatedOnTreatmentHEPB ";

		$this->db->query($query);
	}

public function cascade_anti_InitiatedOnTreatmentHEPB_transgender()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Treatment_Dt) AS dt,


					      count(p.patientguid)  as InitiatedOnTreatmentHEPB
					    FROM
					        tblpatient p LEFT JOIN tblpatientdispensationb hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' and hepb.Treatment_Dt IS NOT NULL and hepb.Treatment_Dt!='0000-00-00' and hepb.VisitNo=1 AND p.Gender=3
					    GROUP BY 
					       
								hepb.Treatment_Dt,
					        p.id_mstfacility 
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.regimen_hepb_transgender = a.InitiatedOnTreatmentHEPB ";

		$this->db->query($query);
	}

public function regimen_hepb_TAF(){

		
		 
		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.PrescribingDate) AS dt,


					      count(p.patientguid)  as EligibleFor_TreatmentHBV
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1)  and hepb.PrescribingDate IS NOT NULL and hepb.PrescribingDate!='0000-00-00' AND hepb.Drugs_Added=1
					    GROUP BY 
					       
								hepb.PrescribingDate,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.regimen_hepb_TAF = a.EligibleFor_TreatmentHBV ";

		$this->db->query($query);

}


public function regimen_hepb_Entecavir(){

		
		 
		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.PrescribingDate) AS dt,


					      count(p.patientguid)  as EligibleFor_TreatmentHBV
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1)  and hepb.PrescribingDate IS NOT NULL and hepb.PrescribingDate!='0000-00-00' AND hepb.Drugs_Added=2
					    GROUP BY 
					       
								hepb.PrescribingDate,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.regimen_hepb_Entecavir = a.EligibleFor_TreatmentHBV ";

		$this->db->query($query);

}

public function regimen_hepb_TDF(){

		
		 
		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.PrescribingDate) AS dt,


					      count(p.patientguid)  as EligibleFor_TreatmentHBV
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1)  and hepb.PrescribingDate IS NOT NULL and hepb.PrescribingDate!='0000-00-00' AND hepb.Drugs_Added=3
					    GROUP BY 
					       
								hepb.PrescribingDate,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.regimen_hepb_TDF = a.EligibleFor_TreatmentHBV ";

		$this->db->query($query);

}

public function regimen_hepb_Others(){

		
		 
		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.PrescribingDate) AS dt,


					      count(p.patientguid)  as EligibleFor_TreatmentHBV
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1)  and hepb.PrescribingDate IS NOT NULL and hepb.PrescribingDate!='0000-00-00' AND hepb.Drugs_Added=99
					    GROUP BY 
					       
								hepb.PrescribingDate,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.regimen_hepb_Others = a.EligibleFor_TreatmentHBV ";

		$this->db->query($query);

}
// AGE WISE

public function age_less_than_10_hepb()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Treatment_Dt) AS dt,


					      count(p.patientguid)  as InitiatedOnTreatmentHEPB
					    FROM
					        tblpatient p LEFT JOIN tblpatientdispensationb hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' and hepb.Treatment_Dt IS NOT NULL and hepb.Treatment_Dt!='0000-00-00' and hepb.VisitNo=1 AND age < 10  
					    GROUP BY 
					       
								hepb.Treatment_Dt,
					        p.id_mstfacility 
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.age_less_than_10_hepb = a.InitiatedOnTreatmentHEPB ";

		$this->db->query($query);
	}

//
	public function age_10_to_20_hepb()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Treatment_Dt) AS dt,


					      count(p.patientguid)  as InitiatedOnTreatmentHEPB
					    FROM
					        tblpatient p LEFT JOIN tblpatientdispensationb hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' and hepb.Treatment_Dt IS NOT NULL and hepb.Treatment_Dt!='0000-00-00' and hepb.VisitNo=1 AND age >= 10 AND age < 20
					    GROUP BY 
					       
								hepb.Treatment_Dt,
					        p.id_mstfacility 
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.age_10_to_20_hepb = a.InitiatedOnTreatmentHEPB ";

		$this->db->query($query);
	}

	public function age_21_to_30_hepb()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Treatment_Dt) AS dt,


					      count(p.patientguid)  as InitiatedOnTreatmentHEPB
					    FROM
					        tblpatient p LEFT JOIN tblpatientdispensationb hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' and hepb.Treatment_Dt IS NOT NULL and hepb.Treatment_Dt!='0000-00-00' and hepb.VisitNo=1 AND  age >= 20 AND age < 30
					    GROUP BY 
					       
								hepb.Treatment_Dt,
					        p.id_mstfacility 
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.age_21_to_30_hepb = a.InitiatedOnTreatmentHEPB ";

		$this->db->query($query);
	}

	public function age_31_to_40_hepb()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Treatment_Dt) AS dt,


					      count(p.patientguid)  as InitiatedOnTreatmentHEPB
					    FROM
					        tblpatient p LEFT JOIN tblpatientdispensationb hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' and hepb.Treatment_Dt IS NOT NULL and hepb.Treatment_Dt!='0000-00-00' and hepb.VisitNo=1 AND age >= 30 AND age < 40 
					    GROUP BY 
					       
								hepb.Treatment_Dt,
					        p.id_mstfacility 
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.age_31_to_40_hepb = a.InitiatedOnTreatmentHEPB ";

		$this->db->query($query);
	}

	public function age_41_to_50_hepb()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Treatment_Dt) AS dt,


					      count(p.patientguid)  as InitiatedOnTreatmentHEPB
					    FROM
					        tblpatient p LEFT JOIN tblpatientdispensationb hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' and hepb.Treatment_Dt IS NOT NULL and hepb.Treatment_Dt!='0000-00-00' and hepb.VisitNo=1 AND age >= 40 AND age < 50 
					    GROUP BY 
					       
								hepb.Treatment_Dt,
					        p.id_mstfacility 
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.age_41_to_50_hepb = a.InitiatedOnTreatmentHEPB ";

		$this->db->query($query);
	}

	public function age_51_to_60_hepb()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Treatment_Dt) AS dt,


					      count(p.patientguid)  as InitiatedOnTreatmentHEPB
					    FROM
					        tblpatient p LEFT JOIN tblpatientdispensationb hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' and hepb.Treatment_Dt IS NOT NULL and hepb.Treatment_Dt!='0000-00-00' and hepb.VisitNo=1 AND age >= 50 AND age < 60 
					    GROUP BY 
					       
								hepb.Treatment_Dt,
					        p.id_mstfacility 
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.age_51_to_60_hepb = a.InitiatedOnTreatmentHEPB ";

		$this->db->query($query);
	}


	public function age_greater_than_60_hepb()
	{

			

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Treatment_Dt) AS dt,


					      count(p.patientguid)  as InitiatedOnTreatmentHEPB
					    FROM
					        tblpatient p LEFT JOIN tblpatientdispensationb hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' and hepb.Treatment_Dt IS NOT NULL and hepb.Treatment_Dt!='0000-00-00' and hepb.VisitNo=1 AND age >= 60 
					    GROUP BY 
					       
								hepb.Treatment_Dt,
					        p.id_mstfacility 
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.age_greater_than_60_hepb = a.InitiatedOnTreatmentHEPB ";

		$this->db->query($query);
	}

/*Gender wise report*/


public function genderwise_hbv_screened($gender,$agewise)
	{

		if($gender==''){
			echo 'Please enter gender';
			return false;
		}else{
			$gender = $gender;
		}

		if($agewise==1){
			$agewisecon = "and age >= 18";
			
		}elseif($agewise==2){
			$agewisecon = "and age < 18";
		}else{
			echo 'Please enter age';
			return false;
		}		

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HBSRapidResult
					    FROM
					        tblpatient
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') AND gender = '".$gender."' ".$agewisecon."
					    GROUP BY
					       
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hbs_screened = a.HBSRapidResult WHERE s.Gender='".$gender."' and s.Agewise='".$agewise."'";

		$this->db->query($query);
	}


public function genderwise_hbv_screened_positive($gender,$agewise)
	{

		if($gender==''){
			echo 'Please enter gender';
			return false;
		}else{
			$gender = $gender;
		}

		if($agewise==1){
			$agewisecon = "and age >= 18";
			
		}elseif($agewise==2){
			$agewisecon = "and age < 18";
		}else{
			echo 'Please enter age';
			return false;
		}		

		$query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HBSRapidResult
					    FROM
					        tblpatient
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') AND (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND gender = '".$gender."' ".$agewisecon."
					    GROUP BY
					       
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hbs_positive = a.HBSRapidResult WHERE s.Gender='".$gender."' and s.Agewise='".$agewise."'";

		$this->db->query($query);
	}

//GEnderwise

	public function genderwise_hbv_testforALT_Level($gender,$agewise)
	{

			if($gender==''){
			echo 'Please enter gender';
			return false;
		}else{
			$gender = $gender;
		}

		if($agewise==1){
			$agewisecon = "and age >= 18";
			
		}elseif($agewise==2){
			$agewisecon = "and age < 18";
		}else{
			echo 'Please enter age';
			return false;
		}		

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Prescribing_Dt) AS dt,


					      count(p.patientguid)  as TestForALT_Level
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND hepb.Elevated_ALT_Level in(1,2,3) AND gender = '".$gender."' ".$agewisecon."
					    GROUP BY
					       
								hepb.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.TestForALT_Level = a.TestForALT_Level WHERE s.Gender='".$gender."' and s.Agewise='".$agewise."'";

		$this->db->query($query);
	}


public function genderwise_hbv_Cirrhotic($gender,$agewise)
	{

			

		if($gender==''){
			echo 'Please enter gender';
			return false;
		}else{
			$gender = $gender;
		}

		if($agewise==1){
			$agewisecon = "and age >= 18";
			
		}elseif($agewise==2){
			$agewisecon = "and age < 18";
		}else{
			echo 'Please enter age';
			return false;
		}		

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Prescribing_Dt) AS dt,


					      count(p.patientguid)  as Cirrhotic
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND hepb.V1_Cirrhosis =2 AND gender = '".$gender."' ".$agewisecon."
					    GROUP BY
					       
								hepb.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.Cirrhotic = a.Cirrhotic WHERE s.Gender='".$gender."' and s.Agewise='".$agewise."'";

		$this->db->query($query);
	}

public function genderwise_hbv_ElevatedALT_Level($gender,$agewise)
	{

			if($gender==''){
			echo 'Please enter gender';
			return false;
		}else{
			$gender = $gender;
		}

		if($agewise==1){
			$agewisecon = "and age >= 18";
			
		}elseif($agewise==2){
			$agewisecon = "and age < 18";
		}else{
			echo 'Please enter age';
			return false;
		}		


		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Prescribing_Dt) AS dt,


					      count(p.patientguid)  as ElevatedALT_Level
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND hepb.Elevated_ALT_Level=1 AND gender = '".$gender."' ".$agewisecon."
					    GROUP BY
					       
								hepb.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.ElevatedALT_Level = a.ElevatedALT_Level WHERE s.Gender='".$gender."' and s.Agewise='".$agewise."'";

		$this->db->query($query);
	}


public function genderwise_hbv_EligibleHBV_DNATest($gender,$agewise)
	{

			if($gender==''){
			echo 'Please enter gender';
			return false;
		}else{
			$gender = $gender;
		}

		if($agewise==1){
			$agewisecon = "and age >= 18";
			
		}elseif($agewise==2){
			$agewisecon = "and age < 18";
		}else{
			echo 'Please enter age';
			return false;
		}		


		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (p.BVLSampleCollectionDate) AS dt,


					      count(p.patientguid)  as EligibleHBV_DNATest
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND BVLSampleCollectionDate IS NOT NULL AND BVLSampleCollectionDate!='0000-00-00' AND gender = '".$gender."' ".$agewisecon."
					    GROUP BY
					       
								p.BVLSampleCollectionDate,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.EligibleHBV_DNATest = a.EligibleHBV_DNATest WHERE s.Gender='".$gender."' and s.Agewise='".$agewise."'";

		$this->db->query($query);
	}



public function genderwise_hbv_HBVDNA_TestConducted($gender,$agewise)
	{

			if($gender==''){
			echo 'Please enter gender';
			return false;
		}else{
			$gender = $gender;
		}

		if($agewise==1){
			$agewisecon = "and age >= 18";
			
		}elseif($agewise==2){
			$agewisecon = "and age < 18";
		}else{
			echo 'Please enter age';
			return false;
		}		

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (p.T_DLL_01_BVLC_Date) AS dt,


					      count(p.patientguid)  as HBVDNA_TestConducted
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND Prescribing_Dt is not null AND Prescribing_Dt!='0000-00-00'  AND BVLSampleCollectionDate IS NOT NULL AND BVLSampleCollectionDate!='0000-00-00'  AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' AND gender = '".$gender."' ".$agewisecon."
					    GROUP BY 
					       
								p.T_DLL_01_BVLC_Date,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.HBVDNA_TestConducted = a.HBVDNA_TestConducted WHERE s.Gender='".$gender."' and s.Agewise='".$agewise."'";

		$this->db->query($query);
	}


public function genderwise_hbv_EligibleFor_TreatmentHBV($gender,$agewise)
	{

			if($gender==''){
			echo 'Please enter gender';
			return false;
		}else{
			$gender = $gender;
		}

		if($agewise==1){
			$agewisecon = "and age >= 18";
			
		}elseif($agewise==2){
			$agewisecon = "and age < 18";
		}else{
			echo 'Please enter age';
			return false;
		}		

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.PrescribingDate) AS dt,


					      count(p.patientguid)  as EligibleFor_TreatmentHBV
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1)  and hepb.PrescribingDate IS NOT NULL and hepb.PrescribingDate!='0000-00-00' AND gender = '".$gender."' ".$agewisecon."
					    GROUP BY 
					       
								hepb.PrescribingDate,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.EligibleFor_TreatmentHBV = a.EligibleFor_TreatmentHBV WHERE s.Gender='".$gender."' and s.Agewise='".$agewise."'";

		$this->db->query($query);
	}


public function genderwise_hbv_InitiatedOnTreatmentHEPB($gender,$agewise)
	{

			if($gender==''){
			echo 'Please enter gender';
			return false;
		}else{
			$gender = $gender;
		}

		if($agewise==1){
			$agewisecon = "and age >= 18";
			
		}elseif($agewise==2){
			$agewisecon = "and age < 18";
		}else{
			echo 'Please enter age';
			return false;
		}		

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Treatment_Dt) AS dt,


					      count(p.patientguid)  as InitiatedOnTreatmentHEPB
					    FROM
					        tblpatient p LEFT JOIN tblpatientdispensationb hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' and hepb.Treatment_Dt IS NOT NULL and hepb.Treatment_Dt!='0000-00-00' and hepb.VisitNo=1 AND gender = '".$gender."' ".$agewisecon."
					    GROUP BY 
					       
								hepb.Treatment_Dt,
					        p.id_mstfacility 
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.InitiatedOnTreatmentHEPB = a.InitiatedOnTreatmentHEPB WHERE s.Gender='".$gender."' and s.Agewise='".$agewise."'";

		$this->db->query($query);
	}

public function genderwise_hbv_ContinuedOnTreatment($gender,$agewise)
	{

			if($gender==''){
			echo 'Please enter gender';
			return false;
		}else{
			$gender = $gender;
		}

		if($agewise==1){
			$agewisecon = "and age >= 18";
			
		}elseif($agewise==2){
			$agewisecon = "and age < 18";
		}else{
			echo 'Please enter age';
			return false;
		}		

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.Treatment_Dt) AS dt,


					      count(p.patientguid)  as ContinuedOnTreatment
					    FROM
					        tblpatient p LEFT JOIN tblpatientdispensationb hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND T_DLL_01_BVLC_Date IS NOT NULL AND T_DLL_01_BVLC_Date!='0000-00-00' and hepb.Treatment_Dt IS NOT NULL and hepb.Treatment_Dt!='0000-00-00' and hepb.VisitNo >=1 AND gender = '".$gender."' ".$agewisecon."
					    GROUP BY 
					       
								hepb.Treatment_Dt,
					        p.id_mstfacility 
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.ContinuedOnTreatment = a.ContinuedOnTreatment WHERE s.Gender='".$gender."' and s.Agewise='".$agewise."'";

		$this->db->query($query);
	}

/*TAF gender wise*/

public function genderwise_hbv_regimen_hepb_TAF($gender,$agewise){

		if($gender==''){
			echo 'Please enter gender';
			return false;
		}else{
			$gender = $gender;
		}

		if($agewise==1){
			$agewisecon = "and age >= 18";
			
		}elseif($agewise==2){
			$agewisecon = "and age < 18";
		}else{
			echo 'Please enter age';
			return false;
		}		
		 
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.PrescribingDate) AS dt,


					      count(p.patientguid)  as regimen_hepb_TAF
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1)  and hepb.PrescribingDate IS NOT NULL and hepb.PrescribingDate!='0000-00-00' AND hepb.Drugs_Added=1 AND gender = '".$gender."' ".$agewisecon."
					    GROUP BY 
					       
								hepb.PrescribingDate,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.regimen_hepb_TAF = a.regimen_hepb_TAF WHERE s.Gender='".$gender."' and s.Agewise='".$agewise."'";

		$this->db->query($query);

}

/*regimen_hepb_Entecavir*/

public function genderwise_hbv_regimen_hepb_Entecavir($gender,$agewise){

		if($gender==''){
			echo 'Please enter gender';
			return false;
		}else{
			$gender = $gender;
		}

		if($agewise==1){
			$agewisecon = "and age >= 18";
			
		}elseif($agewise==2){
			$agewisecon = "and age < 18";
		}else{
			echo 'Please enter age';
			return false;
		}		
		 
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.PrescribingDate) AS dt,


					      count(p.patientguid)  as regimen_hepb_Entecavir
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1)  and hepb.PrescribingDate IS NOT NULL and hepb.PrescribingDate!='0000-00-00' AND hepb.Drugs_Added=2 AND gender = '".$gender."' ".$agewisecon."
					    GROUP BY 
					       
								hepb.PrescribingDate,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.regimen_hepb_Entecavir = a.regimen_hepb_Entecavir WHERE s.Gender='".$gender."' and s.Agewise='".$agewise."'";

		$this->db->query($query);

}

/*regimen_hepb_TDF*/
public function genderwise_hbv_regimen_hepb_TDF($gender,$agewise){

		if($gender==''){
			echo 'Please enter gender';
			return false;
		}else{
			$gender = $gender;
		}

		if($agewise==1){
			$agewisecon = "and age >= 18";
			
		}elseif($agewise==2){
			$agewisecon = "and age < 18";
		}else{
			echo 'Please enter age';
			return false;
		}		
		 
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.PrescribingDate) AS dt,


					      count(p.patientguid)  as regimen_hepb_TDF
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1)  and hepb.PrescribingDate IS NOT NULL and hepb.PrescribingDate!='0000-00-00' AND hepb.Drugs_Added=2 AND gender = '".$gender."' ".$agewisecon."
					    GROUP BY 
					       
								hepb.PrescribingDate,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.regimen_hepb_TDF = a.regimen_hepb_TDF WHERE s.Gender='".$gender."' and s.Agewise='".$agewise."'";

		$this->db->query($query);

}


/*Other*/

public function genderwise_hbv_regimen_Others($gender,$agewise){

		if($gender==''){
			echo 'Please enter gender';
			return false;
		}else{
			$gender = $gender;
		}

		if($agewise==1){
			$agewisecon = "and age >= 18";
			
		}elseif($agewise==2){
			$agewisecon = "and age < 18";
		}else{
			echo 'Please enter age';
			return false;
		}		
		 
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          p.id_mstfacility,
					        (hepb.PrescribingDate) AS dt,


					      count(p.patientguid)  as regimen_hepb_TDF
					    FROM
					        tblpatient p LEFT JOIN hepb_tblpatient hepb ON p.PatientGUID=hepb.PatientGUID
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1)  and hepb.PrescribingDate IS NOT NULL and hepb.PrescribingDate!='0000-00-00' AND  hepb.Drugs_Added=99 AND gender = '".$gender."' ".$agewisecon."
					    GROUP BY 
					       
								hepb.PrescribingDate,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.regimen_hepb_Others = a.regimen_hepb_TDF WHERE s.Gender='".$gender."' and s.Agewise='".$agewise."'";

		$this->db->query($query);

}

/*hep- b*/


public function update_cirrhotic(){

	$this->Summary_updatehbv_model->non_cirrhotic();
	$this->Summary_updatehbv_model->compensated_cirrhotic();
	$this->Summary_updatehbv_model->decompensated_cirrhotic();
	$this->Summary_updatehbv_model->cirrhotic_status_notavaliable();

}
public function non_cirrhotic(){

  $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					    	p.PatientGUID,
					        p.id_mstfacility,
					        (
					            c.Prescribing_Dt
					) AS dt,

					        count(p.PatientGUID) AS non_cirrhotic
					    FROM
					        tblpatient p left join hepb_tblpatient c on p.patientguid=c.PatientGUID
					    WHERE
					       c.V1_Cirrhosis = 2 AND HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00'
					    GROUP BY
					      	c.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.hbv_non_cirrhotic = a.non_cirrhotic";

		$this->db->query($query); 
}

public function compensated_cirrhotic(){

	$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        p.id_mstfacility,
					        (
					            c.Prescribing_Dt
					) AS dt,

					        count(p.PatientGUID) AS compensated_cirrhotic
					    FROM
					       tblpatient p left join hepb_tblpatient c on p.patientguid=c.PatientGUID
					    WHERE
					        c.Result=1 AND HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00'
					    GROUP BY
					      c.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.hbv_compensated_cirrhotic = a.compensated_cirrhotic";

		$this->db->query($query); 
	
}
public function decompensated_cirrhotic(){

	$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        p.id_mstfacility,
					        (
					            c.Prescribing_Dt
					) AS dt,

					        count(p.PatientGUID) AS decompensated_cirrhotic
					    FROM
					       tblpatient p left join hepb_tblpatient c on p.patientguid=c.PatientGUID
					    WHERE
					        c.Result=2 AND HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00'
					    GROUP BY
					       c.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.hbv_decompensated_cirrhotic = a.decompensated_cirrhotic";

		$this->db->query($query); 
}

public function cirrhotic_status_notavaliable(){

	$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        p.id_mstfacility,
					        (
					            c.Prescribing_Dt
					) AS dt,

					        count(p.PatientGUID) AS cirrhotic_status_notavaliable
					    FROM
					       tblpatient p left join hepb_tblpatient c on p.patientguid=c.PatientGUID
					    WHERE
					        c.Result NOT IN(1,2) AND c.V1_Cirrhosis != 2 and c.V1_Cirrhosis IN(0,1) AND HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1) AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00'
					    GROUP BY
					       c.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.hbv_cirrhotic_status_notavaliable = a.cirrhotic_status_notavaliable";

		$this->db->query($query); 
}





}