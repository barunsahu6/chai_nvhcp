<?php  
class Log4php_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		
		//log4php configuration
		require_once(APPPATH .'/third_party/ci_log4php/Logger.php');
		Logger::configure(APPPATH .'/config/log4php.xml');
		// Logger::configure(APPPATH .'/log4php/setting.php');
		$this->logger = Logger::getLogger(basename(__FILE__));
	}

	public function log($class,$id_mstfacility, $activity = '' , $Status = ''){
		$header = "DateTime, IP, UserType, UserID, PageReference, OperationID, Action, Status";
		$data = $this->logData($class,$id_mstfacility, $activity, $Status);
		$file_path = APPPATH . 'logs/Activity_Tracker_Log/Mylog_'.date('Ymd').'.txt';
		$size      = filesize($file_path);
		if($size == 0){
			$this->logger->info($header);
		}
		$this->logger->info($data);
	}


	function logData($PageRefrence=null, $OperationID= null, $activity='', $status='' ){
		$date = date('Y-m-d H:i:s');
		$userIP = $this->input->ip_address();
		$user_type = '';
		$loginData = $this->session->userdata('loginData');

		if($loginData->user_type != ''){
			$user_type = $loginData->user_type;
			$id_tblusers = $loginData->id_tblusers;
			/*if($PageRefrence == 'Login'){
				$userName = $loginData->username;
			}else{
				$userName = $loginData->Operator_Name;
			}*/
		}  

		$data = $date.', '.$userIP.', '.$user_type.', '.$id_tblusers.', '.$PageRefrence.', '.$OperationID.', '.$activity.', '.$status;

		return $data;
	}
}