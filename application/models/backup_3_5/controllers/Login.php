<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->Model('Common_Model');
	}
	public function index()
	{	
		$content['salt'] = md5(uniqid(rand(), TRUE));
		$this->session->set_userdata('salt', $content['salt']);
		$this->load->view('login', $content);
	}


	/**
	 * 6Le6j5cUAAAAAEPUqM3QDl8JIFc2ie2k9pD09PeF
	 * 6Le6j5cUAAAAAPqnaGONpJPJwYDYcWd8CLhSFvKE
	 */
	public function verifylogin()
	{

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		if ($username == '' || $password == '') {
			$this->session->set_flashdata('er_msg', 'Please enter username and password.');
			redirect('login');
		}
		$termsandcondition = $this->security->xss_clean($this->input->post('termsandcondition'));
		if($termsandcondition ==''){
			$this->session->set_flashdata('er_msg', 'Please accept the terms and conditions.');
			redirect('login');
		}
		// check recaptcha 

		$captcha=$this->input->post('g-recaptcha-response');
		
		$data = array(
			'secret' => "6Le6j5cUAAAAAPqnaGONpJPJwYDYcWd8CLhSFvKE",
			'response' => $captcha,
		);

		$verify = curl_init();
		curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
		curl_setopt($verify, CURLOPT_POST, true);
		curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($verify);

		$response = json_decode($response, true);

		if($response['success'] == false)
		{
			$this->session->set_flashdata('er_msg', 'Please check the checkbox');
			redirect('login');
		}

		$this->db->where('username',hash('sha256',$username) );
		$result = $this->db->get('tblusers')->result();
		if (count($result) != 1) {
			$this->session->set_flashdata('er_msg', 'Your username or password was incorrect');
			redirect('login');
		}

		$user_record = $result[0];

		$salt = $this->session->userdata('salt');
		if ($salt == null || trim($salt) == '') {
			$this->log_login($user_record->id_tblusers, "salt not present");
			$this->session->set_flashdata('tr_msg', 'salt not present');
			redirect('login');
		}

		$hashed_password = hash('sha256', $user_record->password . $salt);

		if ($hashed_password != $password) {
			$this->log_login($user_record->id_tblusers, "incorrect password");
			$this->session->set_flashdata('er_msg', 'Your username or password was incorrect');
			redirect('login');
		}

		session_regenerate_id();

		if (!in_array($user_record->user_type, [1,2])) 
		{
			$this->log_login($user_record->id_tblusers, "Your role is not allowed to access date");
			$this->session->set_flashdata('er_msg', 'Your role is not allowed to access date');
			redirect('login');
		}

		$this->log_login($user_record->id_tblusers, "login successful");

		$this->session->set_userdata('loginData',$user_record);
		redirect('dashboard');
	}

	public function logout()
	{
		$this->session->set_flashdata('er_msg', 'Please login again to continue...');
		$this->session->sess_destroy();
		session_regenerate_id();
		redirect('login');
	}

	private function log_login($id_tblusers, $login_result='')
	{
		$insertArr = array(
			'id_tblusers'  => $id_tblusers,
			'login_at'     => date('Y-m-d H:i:s'),
			'ip'           => $this->input->ip_address(),
			'session'      => session_id(),
			'login_result' => $login_result,
		);
		$this->db->insert('tbl_login_log', $insertArr);
	}

}
