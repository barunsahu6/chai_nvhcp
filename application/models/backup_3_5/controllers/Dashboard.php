<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Common_Model');

		$loginData = $this->session->userdata('loginData');
		$loginData = $this->session->userdata('loginData');
		if($loginData == null)
		{
			redirect('login');
		}
	}

	public function index()
	{
		$loginData = $this->session->userdata('loginData');
		//echo "<pre>";print_r($loginData);
		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

		if($REQUEST_METHOD == 'POST')
		{
			$filters['id_mstfacility'] = $this->input->post('facility');	
			$filters['startdate']      = $this->input->post('startdate');	
			$filters['enddate']        = $this->input->post('enddate');	
			$filters['category']       = $this->input->post('category');
		}
		else
		{
			$filters['id_mstfacility'] = 0;	
			$filters['startdate']      = '2016-06-18';	
			$filters['enddate']        = date('Y-m-d');
			$filters['category']       = 0;
		}

		$this->session->set_userdata('filters', $filters);
		$this->load->model('Dashboard_Model');

		$content['filters']             = $this->session->userdata('filters');
		$content['cascade']             = $this->Dashboard_Model->cascade();
		
		if ($loginData->id_mstfacility=='' || $loginData->id_mstfacility==0) {
			$id_mstfacility = 1;
		}else{
			$id_mstfacility = "id_tblusers = ".$loginData->id_tblusers;
		}


		 $sql = "SELECT * FROM `tblusers` WHERE  ".$id_mstfacility;
		$loginuser_data = $this->db->query($sql)->result();
		
		if ($loginData->id_mstfacility=='' || $loginData->id_mstfacility==0) {
			$id_mstfacilityids = 1;
		}else{
			$id_mstfacilityids = "id_mstfacility = ".$loginuser_data[0]->id_mstfacility;
		}
		$sql = "select * from mstfacility where  ".$id_mstfacilityids;
		$content['default_facilities'] = $this->db->query($sql)->result();

		if ($loginData->id_mstfacility=='' || $loginData->id_mstfacility==0) {
			$id_mststate = 1;
		}else{
			$id_mststate = "id_mstdistrict = ".$content['default_facilities'][0]->id_mstdistrict;
		}
		 $sql = "select * from mstdistrict where ".$id_mststate;
		//die($sql);
		$content['default_districts'] = $this->db->query($sql)->result();


		$sql = "select * from mststate";
		$content['states'] = $this->db->query($sql)->result();

		$sql = "select * from mstdistrict";
		$content['districts'] = $this->db->query($sql)->result();

		$sql = "select * from mstfacility";
		$content['facilities'] = $this->db->query($sql)->result();	

		$content['start_date'] = '2016-08-16';
		$content['end_date']   = date('Y-m-d');
		$content['facilities'] = $this->Dashboard_Model->facilities();
		
		$content['subview'] = 'dashboard';
		$this->load->view('pages/main_layout', $content);
	}
}