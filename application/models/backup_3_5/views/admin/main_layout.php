<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('admin/includes/header'); ?>
<?php $this->load->view('admin/includes/menubar'); ?>
<?php $this->load->view('admin/components/'.$subview); ?>

<!-- multipurpose modal -->

<?php $error = $this->session->flashdata('error'); ?>
<div class="modal" data-easein="bounceIn" tabindex="-1" role="dialog" id="multipurpose_modal">
	<div class="modal-dialog" role="document" style="position: relative; top : 30vh;">
		<div class="modal-content">
			<div class="row">
				<!-- <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-xs-8 col-xs-offset-2"> -->
				<div class="col-lg-12">
					<div class="modal-header">
						<button class="btn btn-success btn-block form_buttons" href="" style="color: white;" data-dismiss="modal" aria-label="Close" id="modal_header"><?php echo isset($error)?$error['header']:'Information'; ?></button>

					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12 text-center">
								<i id="modal_text"><?php echo isset($error)?$error['message']:'Error Text'; ?></i>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<script>
	$(".modal").each(function(l){$(this).on("show.bs.modal",function(l){var o=$(this).attr("data-easein");"shake"==o?$(".modal-dialog").velocity("callout."+o):"pulse"==o?$(".modal-dialog").velocity("callout."+o):"tada"==o?$(".modal-dialog").velocity("callout."+o):"flash"==o?$(".modal-dialog").velocity("callout."+o):"bounce"==o?$(".modal-dialog").velocity("callout."+o):"swing"==o?$(".modal-dialog").velocity("callout."+o):$(".modal-dialog").velocity("transition."+o)})});
</script>
<?php $this->load->view('admin/includes/footer'); ?>