<?php 
$loginData = $this->session->userdata('loginData');

$sql = "SELECT Registration FROM `MSTRole` where RoleId = ".$loginData->RoleId;
$result = $this->db->query($sql)->result();
//print_r($result);
?>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #A30A0C !important;
	background-color: white !important;
	border : 1px solid #A30A0C;
}
</style>
<style>
/* The container */
.container {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
  border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.container input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.container .checkmark:after {
 	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: white;
}
input:invalid {
  color: red;
}

select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
.linebks{
	    background-color: #ddd;
    color: #0c0c0c;
    padding-top: 10px;
    padding-bottom: 1px;
}
</style>
<br>
<?php //echo "<pre>";print_r($patient_data); //echo count($patient_data); ?>
<div class="row equal">
	<div class="col-lg-10 col-lg-offset-1">

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="btn-group nav_buttons">
					<a class="btn btn-success" href="<?php echo base_url(); ?>patientinfo/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">1. Registration</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">2. Screening</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">3. Viral Load</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">4. Testing</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/known_history/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">5. Known History</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">6. Prescription</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">7. Dispensation</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">8. SVR</a>
				</div>
			</div>
		</div>

		<!-- <form action="" method="POST" name="registration" id="registration" autocomplete="false"> -->
			<?php
           $attributes = array(
              'id' => 'registration',
              'name' => 'registration',
               'autocomplete' => 'false',
            );
           echo form_open('', $attributes); ?>
<br/>
<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){ ?>
			<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				<?php } ?>
				
			<div class="row">
				<div class="col-md-12">
					<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Patient Registration Module</h3>
				</div>
			</div>

			


			<div class="row">
				<div class="col-lg-3 col-md-3 ">
					<label for="">OPD ID <span class="text-danger">*</span></label>
					<input autocomplete="anyrandomthing" type="text" name="opd_id" id="opd_id" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->OPD_Id)):''; ?>" required maxlength="50">
				</div>
				<div class="col-lg-3 col-md-3">
					<label for="">NVHCP ID <span class="text-danger">*</span></label>
					<b><input type="text" name="hcv_uid_prefix" id="hcv_uid_prefix" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->UID_Prefix:$uid_prefix; ?>" readonly></b>
				</div>
				<div class="col-lg-2 col-md-2">
					<label for=""> &nbsp;</label>
					<input autocomplete="anyrandomthing" type="text" maxlength="6" name="hcv_uid_num" id="hcv_uid_num" class="input_fields form-control" value="<?php if(count($patient_data) >0) { echo  str_pad($patient_data[0]->UID_Num, 6, '0', STR_PAD_LEFT); } else { echo  str_pad($patient_uid, 6, '0', STR_PAD_LEFT); } ?>" onkeypress="return isNumberKey(event)"  required  <?php if(count($patient_data) > 0 && $patient_data[0]->T_DLL_01_Date != '' ) { echo 'readonly';} ?>>
				</div>
			</div>
			<br>
			<div class="row">
				
<?php if(count($patient_data) > 0 && $patient_data[0]->MF2 == 1) {?>


				<div class="col-lg-3 col-md-3 ">
					<label for="patient_type" >Patient Type <span class="text-danger">*</span><span style="margin-left: 20px; font-size: 14px;" class="glyphicon glyphicon-info-sign" data-toggle='tooltip' title="Select 'New' for patients not undergone HCV Treatment in the past and 'Experienced' for patients having undergone HCV Treatment in the past"></span></label>
					<label class="container">
					<input type="radio" name="patient_typedes" disabled value="1"  checked="" id="patient_typenewdes" class="form-control patient_typecheck" <?php echo (count($patient_data) > 0 && $patient_data[0]->PatientType == 1)?'checked':''; ?>>New
					
				</label>
				<label class="container">
					 <input type="radio" disabled name="patient_typedes" value="2" id="patient_typedes" class="form-control patient_typecheck" <?php echo (count($patient_data) > 0 && $patient_data[0]->PatientType == 2)?'checked':''; ?>>Experienced
					 
					</label>
					
				</div>

<?php } else{ ?>


				<div class="col-lg-3 col-md-3 ">
					<label for="patient_type" >Patient Type <span class="text-danger">*</span><span style="margin-left: 20px; font-size: 14px;" class="glyphicon glyphicon-info-sign" data-toggle='tooltip' title="Select 'New' for patients not undergone HCV Treatment in the past and 'Experienced' for patients having undergone HCV Treatment in the past"></span></label>
					<label class="container">
					New<input type="radio" name="patient_type" value="1"  checked="" id="patient_typenew" class="form-control patient_typecheck" <?php echo (count($patient_data) > 0 && $patient_data[0]->PatientType == 1)?'checked':''; ?>>
					<span class="checkmark" ></span>
				</label>
				<label class="container">
					 Experienced<input type="radio" name="patient_type" value="2" id="patient_type" class="form-control patient_typecheck" <?php echo (count($patient_data) > 0 && $patient_data[0]->PatientType == 2)?'checked':''; ?>>
					 <span class="checkmark"></span>
					</label>
					
				</div>

<?php } ?>
<div class="col-lg-3 col-md-3 experienced_fieldscheck" style="display: none;">
			
			<?php if(count($patient_data) > 0 && $patient_data[0]->MF2 == 1) {?>		
				
				<label class="container">
					 <input type="radio" name="patient_type1des" disabled value="2" id="patient_type1des" class="form-control patient_type" <?php echo (count($patient_data) > 0 && $patient_data[0]->ExperiencedCategory  == 1)?'checked':''; ?>>NVHCP
					 
					</label>

					<label class="container">
					<input type="radio" name="patient_type1des" value="1" disabled id="patient_typeoutdes" class="form-control patient_type" <?php echo (count($patient_data) > 0 && $patient_data[0]->ExperiencedCategory  == 2)?'checked':''; ?>>Outside
					
				</label>

<?php } else{ ?>

					
				<label class="container">NVHCP
					 <input type="radio" name="patient_type1" value="2" id="patient_type1" class="form-control patient_type" <?php echo (count($patient_data) > 0 && $patient_data[0]->ExperiencedCategory  == 1)?'checked':''; ?>>
					 <span class="checkmark"></span>
					</label>

					<label class="container">Outside
					<input type="radio" name="patient_type1" value="1" id="patient_typeout" class="form-control patient_type" <?php echo (count($patient_data) > 0 && $patient_data[0]->ExperiencedCategory  == 2)?'checked':''; ?>>
					<span class="checkmark"></span>
				</label>

<?php } ?>
					
				</div>


				<div class="col-lg-3 col-md-3 experienced_fields"  style="display: none;">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<label for="patient_type_state">State <span class="text-danger">*</span></label>
							<select name="patient_type_state" id="patient_type_state" class="input_fields form-control">
								<option value="">Select State</option>
								<?php 
								foreach ($states as $state) {
									?>
									<option value="<?php echo $state->id_mststate; ?>" <?php echo (count($patient_data) > 0 && $patient_data[0]->PTState == $state->id_mststate)?'selected':''; ?> <?php echo (count($patient_data) == 0 && $default_facilities[0]->id_mststate == $state->id_mststate)?'selected':''; ?>><?php echo ucwords(strtolower($state->StateName)); ?></option>
									<?php 
								}
								?>
							</select>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<label for="patient_type_facility">Facility <span class="text-danger">*</span></label>
							<select name="patient_type_facility" id="patient_type_facility" class="input_fields form-control">
								<option value="">Select Facility</option>
								<?php 
								if(count($patient_data) > 0){
								foreach ($facilities as $facility) {
									?>
									<option value="<?php echo $facility->id_mstfacility; ?>" <?php echo (count($patient_data) > 0 && $patient_data[0]->PastFacility == $facility->id_mstfacility)?'selected':''; ?> ><?php echo ucwords(strtolower($facility->facility_short_name)); ?></option>
									<?php 
								} }
								else
						{
							foreach ($facilities as $district) {
								?>
								<option value="<?php echo $district->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $district->id_mstfacility) { echo 'selected';} ?>><?php echo ucwords(strtolower($district->facility_short_name)); ?></option>
								<?php 
							}
						}

								?>
							</select>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<label for="patient_type_treatment_year">Treatment Year </label>
							<select name="patient_type_treatment_year" id="patient_type_treatment_year" class="input_fields form-control">
								<option value="">Select</option>
								<option value="2018" <?php echo (count($patient_data) > 0 && $patient_data[0]->PTYear == '2018')?'selected':''; ?> <?php //$PTYear; ?>>2018</option>
								<option value="2019" <?php echo (count($patient_data) > 0 && $patient_data[0]->PTYear == date('Y'))?'selected':''; ?>>2019</option>
							</select>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<label for="patient_type_treatment_uid">Past Treatment UID </label>
							<input type="text" name="patient_type_treatment_uid" id="patient_type_treatment_uid" class="input_fields form-control" onkeypress="return isNumberKey(event)" value="<?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->PastUID)):''; ?>"  >
						</div>
					</div>

					<div class="col-lg-6 col-md-4">
					<label for="">&nbsp;</label>
					<button class="btn btn-block btn-success form_buttons" style="line-height: 0.9;" id="sync" name="sync" disabled>SYNC</button>
				</div>

				</div>
				
			</div>
			<hr>
			<div class="row">
				<div class="col-lg-3 col-md-3">
					<label for="">Name <span class="text-danger">*</span></label>
					<input autocomplete="anyrandomthing" type="text" name="name" id="name" class="input_fields messagedata form-control" value="<?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FirstName)):''; ?>"  maxlength="50" required  <?php if(count($patient_data) > 0 && $patient_data[0]->T_DLL_01_Date != '' ) { echo 'readonly';} ?>>
				</div>
				<div class="col-lg-3 col-md-3">
					<label for="">Age between 0 and 1 Year?</label>
					<select name="age_between" id="age_between" class="input_fields form-control" required>
						<option value="">Select</option>
						<option value="1"<?php echo (count($patient_data) > 0 && $patient_data[0]->IsAgeMonths == 1)?'selected':''; ?>>Yes</option>
						<option value="2" <?php echo (count($patient_data) == 0)?'selected':''; ?><?php echo (count($patient_data) > 0 && $patient_data[0]->IsAgeMonths == 2)?'selected':''; ?>>No</option>
					</select>
				</div>
				<div class="col-lg-3 col-md-3">
					<label for="" id="age_label"><?php echo (count($patient_data) > 0 && $patient_data[0]->IsAgeMonths == 1)?'Age (in months)':'Age (in years)'; ?> <span class="text-danger">*</span></label>
					<input autocomplete="anyrandomthing" type="text" name="age" id="age" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->Age)):''; ?>" onkeypress="return isNumberKey(event)" required>
				</div>
				<div class="col-lg-3 col-md-3">
					<label for="">Gender <span class="text-danger">*</span></label>
					<select type="text" name="gender" id="gender" class="form-control input_fields" required>
						<option value="">Select</option>
						<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->Gender == 1)?'selected':''; ?>>Male</option>
						<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->Gender == 2)?'selected':''; ?>>Female</option>
						<option value="3" <?php echo (count($patient_data) > 0 && $patient_data[0]->Gender == 3)?'selected':''; ?>>Transgender</option>
					</select>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-md-3">
					<label for="">Select Relative <span class="text-danger">*</span></label>
					<select type="text" name="select_relative" id="select_relative" class="form-control input_fields" required>
						<?php 
						foreach ($relatives_name as $relative) 
						{	
							$selected = '';

							if(count($patient_data) > 0)
							{
								if($relative->LookupCode == $patient_data[0]->Relation)
								{
									$selected = 'selected';
								}
							}
							?>
							<option value="<?php echo $relative->LookupCode; ?>" <?php echo $selected; ?>><?php echo substr($relative->LookupValue, 0, -7); ?></option>
							<?php 
						}
						?>
					</select>
				</div>
				<div class="col-lg-3 col-md-3">
					<label for="">Relative's Name <span class="text-danger">*</span></label>
					<input autocomplete="anyrandomthing" type="text" name="relative_name" id="relative_name" class="input_fields messagedata form-control" value="<?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FatherHusband)):''; ?>" required maxlength="50">
				</div>
				<div class="col-lg-6 col-md-6 ">
					<label for="">Home & Street Address <span class="text-danger">*</span></label>
					<textarea autocomplete="anyrandomthing" rows="5" name="address" id="address" class="form-control" style="border: 1px #CCC solid; width: 100%;" required maxlength="200"><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->Add1)):''; ?></textarea>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-md-3 ">
					<label for="">State <span class="text-danger">*</span></label>
					<select type="text" name="input_state" id="input_state" class="form-control input_fields" required>
						<option value="">Select State</option>
						<?php 
						foreach ($states as $state) {
							?>
							<option value="<?php echo $state->id_mststate; ?>" <?php echo (count($patient_data) > 0 && $patient_data[0]->State == $state->id_mststate)?'selected':''; ?> <?php echo (count($patient_data) == 0 && $default_facilities[0]->id_mststate == $state->id_mststate)?'selected':''; ?>><?php echo ucwords(strtolower($state->StateName)); ?></option>
							<?php 
						}
						?>
						
					</select>
				</div>
				<div class="col-lg-3 col-md-3">
					<label for="">District <span class="text-danger">*</span></label>
					<select type="text" name="input_district" id="input_district" class="form-control input_fields" required>
						<option value="">Select District</option>
						<?php 
						if(count($patient_data) > 0)
						{
							foreach ($patient_districts as $patient_district) {
								?>
								<option value="<?php echo $patient_district->id_mstdistrict; ?>" <?php echo (count($patient_data) > 0 && $patient_data[0]->District == $patient_district->id_mstdistrict)?'selected':''; ?>><?php echo ucwords(strtolower($patient_district->DistrictName)); ?></option>
								<?php 
							}

						}
						else
						{
							foreach ($default_districts as $district) {
								?>
								<option value="<?php echo $district->id_mstdistrict; ?>" <?php if($default_districts[0]->id_mstdistrict == $district->id_mstdistrict) { echo 'selected';} ?>><?php echo ucwords(strtolower($district->DistrictName)); ?></option>
								<?php 
							}
						}
						?>

						<option value="999999" <?php echo (count($patient_data) > 0 && $patient_data[0]->District == '999999')?'selected':''; ?>>Others</option>

					</select>
				</div>


				<!-- District other -->
				<div class="col-lg-3 col-md-3 input_district_other_fields">
					<label for="">Other</label>
					<input type="text" name="input_district_other" id="input_district_other" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->DistrictOther:''; ?>">
					<br class="hidden-lg-*">
				</div>
				<!-- District other -->

				<div class="col-lg-3 col-md-3 ">
					<label for="">Block/Ward</label>
					<select type="text" name="input_block" id="input_block" class="form-control input_fields">
						<option value="">Select Block</option>
						<?php 
						if(count($patient_data) > 0)
						{
							foreach ($patient_blocks as $patient_block) {
								?>
								<option value="<?php echo $patient_block->id_mstblock; ?>" <?php echo (count($patient_data) > 0 && $patient_data[0]->Block == $patient_block->id_mstblock)?'selected':''; ?>><?php echo ucwords(strtolower($patient_block->BlockName)); ?></option>
								<?php 
							}
						}else{

							foreach ($default_block as $block) {
								?>
								<option value="<?php echo $block->id_mstblock; ?>" <?php if($default_block[0]->id_mstblock == $block->id_mstblock) { echo '';} ?>><?php echo ucwords(strtolower($block->BlockName)); ?></option>
								<?php 
							}

						}
						?>
						<option value="999999" <?php echo (count($patient_data) > 0 && $patient_data[0]->Block == '999999')?'selected':''; ?>>Others</option>
					</select>
				</div>

				<!-- block other -->
				<div class="col-lg-3 col-md-3 input_other_fields">
					<label for="">Other</label>
					<input type="text" name="input_block_other" id="input_block_other" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->BlockOther:''; ?>">
					<br class="hidden-lg-*">
				</div>
				<!-- block other -->

				<div class="col-lg-3 col-md-3">
					<label for="">Village/Town/City</label>
					<input autocomplete="anyrandomthing" type="text" name="village" id="village" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->VillageTown)):''; ?>" maxlength="200">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-md-3 ">
					<label for="">Pincode <span class="text-danger">*</span></label>
					<input autocomplete="anyrandomthing" type="text" max="6" min="6" name="pin" id="pin" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->PIN)):''; ?>" onkeypress="return isNumberKey(event)" pattern="[1-9][0-9]{5}" required>
				</div>
				<div class="col-lg-3 col-md-3 ">
					<label for="">Contact Type </label>
					<select type="text" name="contact_type" id="contact_type" class="form-control input_fields" required>
						<option value="2" <?php echo (count($patient_data) == 0)?'selected':''; ?> <?php echo (count($patient_data) > 0 && $patient_data[0]->IsMobile_Landline == 2)?'selected':''; ?>>Mobile</option>
						<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsMobile_Landline == 1)?'selected':''; ?>>Landline</option>
					</select>
				</div>
				<div class="col-lg-3 col-md-3 ">
					<label for=""><span id="contact_no_label">Mobile No.</span><span style="margin-left: 20px; font-size: 14px;" class="glyphicon glyphicon-info-sign" data-toggle='tooltip' title="Enter Valid Contact Number"></span></label>
					<input autocomplete="anyrandomthing" type="text" name="contact_no" id="contact_no" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->Mobile)):''; ?>" onkeypress="return isNumberKey(event)">
				</div>
				<div class="col-lg-3 col-md-3 ">
					<label for="">Consent for Receiving Communication <span class="text-danger">*</span></label>
					<select type="text" name="sms_consent" id="sms_consent" class="form-control input_fields" required>
						<option value="">Select Consent</option>
						<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsSMSConsent == 1)?'selected':''; ?>>Yes</option>
						<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsSMSConsent == 2)?'selected':''; ?>>No</option>
					</select>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-12">
					<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Risk Factors</h3>
				</div>
			</div>
			<div class="row">
				<?php error_reporting(0); foreach ($risk_factor as $risk) { 
					$RISKFF= explode(',', $patient_data[0]->Risk);

					
				 ?>
					<div class="col-md-5 col-md-offset-1 col-xs-12">
						<label class="checkbox-inline"><input type="checkbox" class=" risk_checkboxes" value="<?php echo $risk->LookupCode; ?>" <?php if (in_array($risk->LookupCode,$RISKFF )){ echo "checked"; }?> name="risk[<?php echo $risk->LookupCode; ?>]" id="risk_<?php echo $risk->LookupCode; ?>"   style="width: 20px; height: 20px;"><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;"><?php echo $risk->LookupValue; ?></b></label>
					</div>
					<br>
				<?php } ?>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-md-offset-7 risk_factor_other_field">
					<br>
					<br>
					<label for="">Other</label>
					<input autocomplete="anyrandomthing" type="text" name="risk_factor_other" id="risk_factor_other" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->OtherRisk)):''; ?>"" maxlength="50">
				</div>
			</div>
<input type="hidden" name="patregsub" value="regsave">
		<input type="hidden" name="interruption_status" id="interruption_status" value="<?php  echo (count($patient_data) > 0)?$patient_data[0]->InterruptReason:'';  ?>">

			<br>
			<hr>
			<div class="row">
				<div class="col-lg-2 col-md-2">
					<a class="btn btn-block btn-default form_buttons" href="<?php echo base_url('patientinfo?p=1'); ?>" id="close" name="close" value="close">CLOSE</a>
				</div>
				<div class="col-lg-2 col-md-2">
					<button class="btn btn-block btn-default form_buttons" id="refresh" name="refresh" value="refresh">REFRESH</button>
				</div>
				<div class="col-lg-2 col-md-2">
					<a href="#" class="btn btn-block btn-default form_buttons" id="lock" name="lock" value="lock">LOCK</a>
				</div>
				<?php if($result[0]->Registration == 1) { ?>
				<div class="col-lg-4 col-md-4">
					<input type="submit" class="btn btn-block btn-success form_buttons" id="save" name="save" value="Save">
				</div>
				<?php } ?>
			</div>
			<br>
			<br>
			<br>

				<div class="row" class="text-left">

					<div class="col-md-6">
					 <label class="btn  btn-default form_buttons"  style="text-align: left !important; ">Patient's Status 
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" name=""  readonly="readonly" value="<?php echo (count($patient_status) > 0)?$patient_status[0]->status:''; ?>" class="btn">
					</label>
				</div>

				

				<div class="col-md-6">
					<label class="btn btn-default form_buttons" style="text-align: left !important;     line-height: 3.2 !important;">Patient's Interruption Status 
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<select name="" id="type_val" onchange="openPopup()" class="btn" style="text-align: right!important;">
						<option value="">Select</option>
						<option value="1"  <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason != '')?'selected':''; ?> >Yes</option>
						<option value="0" <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason == '')?'selected':''; ?>>No</option>
					</select>
				
				</label>
				</div>
			
			</div>

			<br>
			<br>
			<br>


		          <?php echo form_close(); ?>
		<!-- </form> -->
	</div>
</div>

<div class="modal fade" id="addMyModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      
        <h4 class="modal-title">Patient's Interruption Status</h4>
      </div>
      <span id='form-error' style='color:red'></span>
      <div class="modal-body">
      <!--   <form role="form" id="newModalForm" method="post"> -->
      	<?php
           $attributes = array(
              'id' => 'newModalForm',
              'name' => 'newModalForm',
               'autocomplete' => 'false',
            );
           echo form_open('', $attributes); ?>


				<div class="form-group">
				<label class="control-label col-md-3" for="email">Reson:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonval" name="resonval" required="required">
				<option value="">Select</option>
				<option value="1">Death</option>
				<option value="2">Loss to followup</option>
				</select> 
				</div>
				</div>
<br/><br/>

				<div class="form-group resonfielddeath" >
				<label class="control-label col-md-3" for="email">Reason for death:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonvaldeath" name="resonvaldeath">
				<option value="">Select</option>
				<?php foreach ($reason_death as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
				
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>

				<div class="form-group resonfieldlfu">
				<label class="control-label col-md-3" for="email">Reason for LFU:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonfluid" name="resonfluid">
				<option value="">Select</option>
				<?php foreach ($reason_flu as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
				
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>


          <div class="modal-footer">
            <button type="submit" class="btn btn-success" id="btnSaveIt">Save</button>
            <button type="button" class="btn btn-default" id="btnCloseIt" data-dismiss="modal">Close</button>
          </div>
           <?php echo form_close(); ?>
       <!--  </form> -->
      </div>
    </div>
  </div>
</div>

<br/><br/><br/>

<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>

<script>

$(document).ready(function(){

var statusval			 = '<?php echo $patient_data[0]->MF1; ?>';
var InterruptReason 	= '<?php echo $patient_data[0]->InterruptReason; ?>';

if(statusval==0 && InterruptReason==1){

					$("#modal_header").text("Further entry is not allowed,as per treatment status...");
					$("#modal_text").text("Not allowed");
					$("#multipurpose_modal").modal("show");
				
	location.href='<?php echo base_url(); ?>patientinfo/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
				
}
});

	function openPopup() {
		var type_val = $('#type_val').val();
		if(type_val=='1'){
    $("#addMyModal").modal();
		}
}

$('.resonfielddeath').hide();
$('.resonfieldlfu').hide();


$('#btnCloseIt').click(function(){

$('#type_val').val('0');
$('#interruption_status').val('');

});

$('#type_val').change(function(){
var type_val = $('#type_val').val();
$('#interruption_status').val(type_val);

});


 $('#btnSaveIt').click(function(e){
      
      e.preventDefault(); 
      $("#form-error").html('');
     
        
	 var formData = new FormData($('#newModalForm')[0]);
	 $('#btnSaveIt').prop('disabled',true);
	 $.ajax({				    	
		url: '<?php echo base_url(); ?>patientinfo/interruptionstatus_process/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
		type: 'POST',
		data: formData,
		dataType: 'json',
		async: false,
        cache: false,
		contentType: false,
		processData: false,
		success: function (data) { 
				
			if(data['status'] == 'true'){
				$("#form-error").html('Data has been successfully submitted');
				setTimeout(function() {
    					location.href='<?php echo base_url(); ?>patientinfo/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
				}, 1000);

			}else{
				$("#form-error").html(data['message']);
				$('#btnSaveIt').prop('disabled',false); 
				return false;
			}   
		}        
	 }); 
		
   

    }); 


	$('#resonval').change(function(){
		

		if($('#resonval').val() == '1'){

		$('.resonfielddeath').show();
		$('.resonfieldlfu').hide();
		$('#resonfluid').val('');

		$('#resonvaldeath').prop('required',true);
		$('#resonfluid').prop('required',false);

	}else if($('#resonval').val() == '2'){
		$('.resonfielddeath').hide();
		$('.resonfieldlfu').show();
		$('#resonvaldeath').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',true);
	}else{

		$('.resonfielddeath').hide();
		$('.resonfieldlfu').hide();
		$('#resonvaldeath').val('');
		$('#resonfluid').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',false);
	}

});

</script>
<?php //if(count($patient_data) > 0 && $patient_data[0]->T_DLL_01_VLC_Result == 1) {?>
	<?php if(count($patient_data) > 0 && $patient_data[0]->MF2 == 1) {?>
	<script>

		$("#lock").click(function(){
				$("#modal_header").text("Contact admin ,for unlocking the record");
					$("#modal_text").text("Contact Admin");
					$("#multipurpose_modal").modal("show");
					return false;

				});

			$(document).ready(function(){
				/*Disable all input type="text" box*/
				$('#opd_id').prop("readonly", true);
				
				$('#age_between').attr("readonly", true);
				$('#gender').attr("readonly", true);

				$('#patient_type_state').attr("readonly", true);
				$('#patient_type_facility').attr("readonly", true);
				$('#patient_type_treatment_year').attr("readonly", true);
				$('#patient_type_treatment_uid').prop("readonly", true);
				

				/*$('#patient_typenew').attr("disabled", true);
				$('#screening input[type="date"]').prop("readonly", true);
				$("#save").attr("disabled", "disabled");
				$("#refresh").attr("disabled", "disabled");
				$('select').attr('disabled', true);*/
				/*Disable textarea using id */
				//$('#screening #txtAddress').prop("readonly", true);
			});
		</script>
<?php  } ?>

<?php if(count($patient_data) > 0 && $patient_data[0]->MF3 == 1) { ?>
<script>
	
	$('#age').prop("readonly", true);
</script>
	<?PHP } ?>

<script>

	

	$(document).ready(function(){


		<?php if(count($patient_data) > 0 && $patient_data[0]->PatientType == 2) {?>

		if($('#patient_type').val() == '2'){

					$("#patient_type").prop('checked',true); 

			}
		if($('#patient_type1').val() == '2'){

				$("#patient_type1").prop('checked',true); 
		}
<?PHP } ?>

<?php if(count($patient_data) > 0 && $patient_data[0]->ExperiencedCategory == 2) { ?>

		
		if($('#patient_typeout').val() == '1'){

				$("#patient_typeout").prop('checked',true); 
				$('.experienced_fields').hide();

				//$('#patient_type_treatment_year').val('');
				//$('#patient_type_treatment_uid').val('');
				//alert('sss');

		}

		
<?PHP } ?>

<?php if(count($patient_data) > 0 && $patient_data[0]->ExperiencedCategory == 1) { ?>

		
		if($('#patient_type1').val() == '2'){

				$("#patient_type1").prop('checked',true); 
				$('.experienced_fields').show();
				
				//alert('sss');

		}

		
<?PHP } ?>

$('#patient_typeout').change(function(){

if($('#patient_typeout').val() == '1'){

				$("#patient_typeout").prop('checked',true); 
				$('.experienced_fields').hide();

				$('#patient_type_treatment_year').val('');
				$('#patient_type_treatment_uid').val('');
				//alert('sss');

		}

});

		$('[data-toggle="tooltip"]').tooltip();

		<?php 
		$error = $this->session->flashdata('error'); 

		if($error != null)
		{
			?>
			$("#multipurpose_modal").modal("show");
			<?php 
		}
		?>

		//$(".risk_factor_other_field").hide();

		$(document).ready(function(){

					
		
		if( $('#risk_99').is(':checked') == 'false'){

		$(".risk_factor_other_field").hide();

		}

				
		<?php if(count($patient_data) > 0 && $patient_data[0]->Risk == 99) {?>
			
			if( $('#risk_99').is(':checked') == 'true'){

			$(".risk_factor_other_field").show();
			
			}
			<?php } else{ ?>
				
		if( $('#risk_99').is(':checked') == false){

		$(".risk_factor_other_field").hide();

		}
			<?php } ?>
	});		


		$('#risk_99').change(function(){
			if($(this).is(':checked'))
			{
				$('.risk_factor_other_field').show();
			}
			else
			{
				$('.risk_factor_other_field').hide();
				$('#risk_factor_other').val("");
			}
		});

		$(".nav_buttons btn").click(function(e){
			console.log($(this+" a").href());
			e.preventDefault();
			window.location = $(this+" a").href();
		});

		$("#refresh").click(function(e){
			if(confirm('All further details will be deleted.Do you want to continue?')){ 
			$("input").val('');
			$("select").val('');
			$("textarea").val('');
			$("#input_district").html('<option>Select District</option>');
			$("#input_block").html('<option>Select Block</option>');
			e.preventDefault();
		}
		});

<?php if(count($patient_data) == 0 ) { ?>
		$('.experienced_fields').hide();
		$('.experienced_fieldscheck').hide();
<?PHP }

 elseif($patient_data[0]->PatientType == 1)  { ?>
		$('.experienced_fields').hide();
		$('.experienced_fieldscheck').hide();
<?PHP } ?>

		$('#table_patient_list tbody tr').click(function(){
			window.location = $(this).data('href');
		});

		$('#input_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'patientinfo/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'POST',
				success : function(data)
				{
					$('#input_district').html(data);
					$('#input_block').html("<option value=''>Select Block</option>");

				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'patientinfo/getBlocks/'; ?>"+$(this).val(),
				
				method : 'POST',
				success : function(data)
				{
					
					if(data!="<option value=''>Select Block</option>"){
					$('#input_block').html(data);
					}else{
						$('#input_block').html("<option value=''>Select</option><option value='999999'>Others</option>");
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});

		<?php if(count($patient_data) == 0) {?>
			$('#save').click(function(e){
				var error_css = {"border":"1px solid red"};
				e.preventDefault();

				if($('#opd_id').val()==""){
					
					//$("#opd_id").css(error_css);
					$("#opd_id").focus();
					return false;
				}

				if($('#hcv_uid_num').val()==""){
					
					//$("#hcv_uid_num").css(error_css);
					$("#hcv_uid_num").focus();
					return false;
				}

				if($('#name').val()==""){
					
					//$("#name").css(error_css);
					$("#name").focus();
					return false;
				}

				if($('#age').val()==""){
					
					//$("#age").css(error_css);
					$("#age").focus();
					return false;
				}

				if($('#gender').val()==""){
					
					//$("#gender").css(error_css);
					$("#gender").focus();
					return false;
				}

				if($('#relative_name').val()==""){
					
					//$("#relative_name").css(error_css);
					$("#relative_name").focus();
					return false;
				}

				if($('#address').val()==""){
				
					//$("#address").css(error_css);
					$("#address").focus();
					return false;
				}
				if($('#input_state').val()==""){
					
					//$("#input_state").css(error_css);
					$("#input_state").focus();
					return false;
				}

				if($('#input_district').val()==""){
					
					//$("#pin").css(error_css);
					$("#input_district").focus();
					return false;
				}
				
				if($('#pin').val()==""){
					
					//$("#pin").css(error_css);
					$("#pin").focus();
					return false;
				}

				if($('#sms_consent').val()==""){
					
					//$("#sms_consent").css(error_css);
					$("#sms_consent").focus();
					return false;
				}


				$.ajax({
					url : "<?php echo base_url().'patientinfo/check_uid/'; ?>"+$("#hcv_uid_prefix").val()+'/'+$("#hcv_uid_num").val(),
					data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
					method : 'POST',
					// async : false,
					success : function(data)
					{
						
						if(data == 0)
						{
							$("#modal_header").text("Error : UID Already Exists ");
							$("#modal_text").text("Please Enter a New UID, The UID entered already exists");
							$("#multipurpose_modal").modal("show");
							$("#hcv_uid_num").css(error_css);
							$("#hcv_uid_num").focus();
							
							return false;
							
						}
						else{
							//alert('else');
							$('#registration').submit();
						}

					},
					error : function(error)
					{
						$("#modal_header").text("Error : Something went wrong!");
						$("#modal_text").text("Please Contact the Admin for further assistance...");
						$("#multipurpose_modal").modal("show");
						//e.preventDefault();
						$("#hcv_uid_num").val('');
							
							return false;
					}
				})
			});
		<?php } ?>

		<?php if(count($patient_data) == 0) {?>
			
				$('#hcv_uid_num').change(function(){
					//jQuery("#hcv_uid_num").on('keyup', '', function() {
var error_css = {"border":"1px solid green"};
var error_cssred = {"border":"1px solid red"};

				$.ajax({
					url : "<?php echo base_url().'patientinfo/check_uid/'; ?>"+$("#hcv_uid_prefix").val()+'/'+$("#hcv_uid_num").val(),
					data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
					method : 'POST',
					cache: 'false',
  					contentType:'application/json',
					// async : false,
					success : function(data)
					{
						//alert(data);
						if(data == 0)
						{	
							
						
							$("#modal_header").text("Error : UID Exists already exists");
							$("#modal_text").text("Please Enter a New UID, The UID entered already exists"); 
							$("#multipurpose_modal").modal("show");
							$("#hcv_uid_num").val('');
							$("#hcv_uid_num").css(error_cssred);
							
							return false;
						}else{

							$("#hcv_uid_num").css(error_css);
						}

					},
					error : function(error)
					{
						$("#modal_header").text("Error : Something went wrong!");
						$("#modal_text").text("Please Contact the Admin for further assistance...");
						$("#multipurpose_modal").modal("show");
						$("#hcv_uid_num").val('');
						return false;
					}

				})
				
			});
		<?php } ?>


		$('.patient_typecheck').change(function(){
			if($(this).val() == 2)
			{
				$('.experienced_fields').hide();
				$('.experienced_fieldscheck').show();
				$('#patient_type_state').prop('required',true);
				$('#patient_type_facility').prop('required',true);
				//$('#patient_type_treatment_year').prop('required',true);
				//$('#patient_type_treatment_uid').prop('required',true);
				$('#sync').prop('disabled',false);


			}
			else
			{
				
				$('.experienced_fieldscheck').hide();
				$('.experienced_fields').hide();
				$('#patient_type_state').prop('required',false);
				$('#patient_type_facility').prop('required',false);
				//$('#patient_type_treatment_year').prop('required',false);
				//$('#patient_type_treatment_uid').prop('required',false);
				$('#sync').prop('disabled',true);

				$("#patient_type1").prop('checked',false); 

				$("#patient_type_treatment_year").val('');
				$("#patient_type_treatment_uid").val('');
			}
		});



		$('.patient_type').change(function(){
			if($(this).val() == 2)
			{	
				$('.experienced_fields').show();
				$('#patient_type_state').prop('required',true);
				$('#patient_type_facility').prop('required',true);
				//$('#patient_type_treatment_year').prop('required',true);
				//$('#patient_type_treatment_uid').prop('required',true);
				$('#sync').prop('disabled',false);
			}
			else
			{

				$('.experienced_fields').hide();
				$('#patient_type_state').prop('required',false);
				$('#patient_type_facility').prop('required',false);
				//$('#patient_type_treatment_year').prop('required',false);
				//$('#patient_type_treatment_uid').prop('required',false);
				$('#sync').prop('disabled',true);
			}
		});

		$('#age_between').change(function(){
			if($(this).val() == 1)
			{
				$('#age_label').html('Age (in months) <span class="text-danger">*</span>');
				$('#age').val(0);
			}
			else
			{
				$('#age_label').html('Age (in years) <span class="text-danger">*</span>');
				$('#age').val(0);
			}
		});

		$("#sync").click(function(e){
			e.preventDefault();
		});	

		$('#contact_type').change(function(){
			if($(this).val() == 1)
			{
				$('#contact_no_label').html('Landline No.');
			}
			else
			{
				$('#contact_no_label').html('Mobile No.');
			}
		});
	});

	function isNumberKey(evt)
	{
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;

		if(evt.srcElement.id == 'hcv_uid_num' && $('#hcv_uid_num').val() > 99999)
		{
			$("#modal_header").html('Past UID cannot be more than 6 digits');
			$("#modal_text").html('Please fill in appropriate UID Num');
			$("#multipurpose_modal").modal("show");
			return false;
		}
		if(evt.srcElement.id == 'patient_type_treatment_uid' && $('#patient_type_treatment_uid').val() > 99999)
		{
			$("#modal_header").html('Past UID cannot be more than 6 digits');
			$("#modal_text").html('Please fill in appropriate UID Num');
			$("#multipurpose_modal").modal("show");
			return false;
		}
		if(evt.srcElement.id == 'contact_no' && $('#contact_no').val() > 999999999)
		{
			$("#modal_header").html('Contact number must be 10 digits');
			$("#modal_text").html('Please fill in appropriate Contact Number');
			$("#multipurpose_modal").modal("show");
			return false;
		}
		/*if(evt.srcElement.id == 'age' && $('#age_between').val() == 2 && $('#age').val() > 99)
		{
			$("#modal_header").html('Age Cannot be more than 3 Digits');
			$("#modal_text").html('Please fill in appropriate age');
			$("#multipurpose_modal").modal("show");

			return false;
		}
		if(evt.srcElement.id == 'age' && $('#age_between').val() == 1 && $('#age').val() > 11)
		{
			console.log($('#age').val());
			$("#modal_header").html('Age in Months Cannot be more than 11 Months');
			$("#modal_text").html('Please fill in appropriate age');
			$("#multipurpose_modal").modal("show");

			return false;
		}*/
		if(evt.srcElement.id == 'pin' && $('#pin').val() > 99999)
		{
			$("#modal_header").html('PIN cannot be more than 6 digits');
			$("#modal_text").html('Please fill in appropriate PIN');
			$("#multipurpose_modal").modal("show");
			return false;
		}

		return true;
	}
$( "#age" ).keyup(function( event ) {
	
	if($('#age_between').val() == 1 && $('#age').val() > 11)
		{
			
			$("#modal_header").html('Age in Months Cannot be more than 11 Months');
			$('#age').val(11);
			$("#modal_text").html('Please fill in appropriate age');
			$("#multipurpose_modal").modal("show");

			return false;
		}

});	

$( "#age" ).keyup(function( event ) {
	
	if($('#age_between').val() == 2 && $('#age').val() > 150)
		{
			
			$("#modal_header").html('Age Cannot be more 150 Years');
			$('#age').val("");
			$("#modal_text").html('Please fill in appropriate age');
			$("#multipurpose_modal").modal("show");

			return false;
		}

});	

$("#pin" ).change(function( event ) {
	if($('#pin').val().length <6)
		{
			
			$("#modal_header").html('PIN must be 6 digits');
			$('#pin').val();
			$("#modal_text").html('Please fill in appropriate PIN');
			$("#multipurpose_modal").modal("show");
			return false;	
		}
});		


$("#contact_no" ).change(function( event ) {
	if($('#contact_no').val().length <10)
		{
			
			$("#modal_header").html('Contact number must be 10 digits');
			$('#contact_no').val("");
			$("#modal_text").html('Please fill in appropriate Contact Number');
			$("#multipurpose_modal").modal("show");
			return false;	
		}
});		

$('.messagedata').keypress(function (e) {
        var regex = new RegExp(/^[a-zA-Z\s]+$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            return false;
        }
    });


$('#patient_type_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'patientinfo/getFacilities/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'POST',
				success : function(data)
				{
					$('#patient_type_facility').html(data);
					//$('#input_block').html("<option value=''>Select Facilities</option>");
				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

</script>


<script>
		// $(".input_district_other_fields").hide();
	// $(".input_other_fields").hide();
// district other
						$("#input_district").change(function(){
						if($(this).val() == "999999")
						{
						$(".input_district_other_fields").show();
						$('#input_district_other').prop('required',true);
						}
						else
						{
						$(".input_district_other_fields").hide();
						$(".input_other_fields").hide();
						$('#input_district_other').val('');
						$('#input_block_other').val('');
						}
						});
// block other
						$("#input_block").change(function(){
						if($(this).val() == "999999")
						{
						$(".input_other_fields").show();
						$('#input_block_other').prop('required',true);
						}
						else
						{
						$(".input_other_fields").hide();
						}
						});
//

<?php if(count($patient_data) > 0 && ($patient_data[0]->District == '999999')) { ?>
		
		var distval = '<?php echo $patient_data[0]->District?>';
		
	
		$(".input_district_other_fields").show();
		$('#input_district_other').prop('required',true);

		
<?PHP } else{?>

	$(".input_district_other_fields").hide();
	$(".input_other_fields").hide();
	
<?php } ?>

<?php if(count($patient_data) > 0 && $patient_data[0]->Block == '999999') { ?>

		$(".input_other_fields").show();
		$('#input_block_other').prop('required',true);

		
<?PHP }else{ ?>
$(".input_other_fields").hide();
<?php } ?>





 $( "#name,#age,#relative_name,#opd_id " ).on( "copy cut paste drop", function() {
                return false;
        });

</script>



