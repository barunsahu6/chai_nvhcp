<?php 
$loginData = $this->session->userdata('loginData');

$sql = "SELECT SVR FROM `MSTRole` where RoleId = ".$loginData->RoleId;
$result = $this->db->query($sql)->result();
?>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #A30A0C !important;
	background-color: white !important;
	border : 1px solid #A30A0C;
}
</style>

<br>

<div class="row equal">
	<!-- <form action="" name="patient_form" id="patient_form" method="POST"> -->
					<?php
           $attributes = array(
              'id' => 'patient_form',
              'name' => 'patient_form',
               'autocomplete' => 'false',
            );
           echo form_open('', $attributes); ?>

		<input type="hidden" name="fetch_uid" id="fetch_uid">
		          <?php echo form_close(); ?>
		<!-- <input type="hidden" name="fetch_uid" id="fetch_uid"> -->
	<!-- </form> -->
	<div class="col-lg-10 col-lg-offset-1">

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="btn-group">
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">1. Registration</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">2. Screening</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">3. Viral Load</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">4. Testing</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/known_history/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">5. Known History</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">6. Prescription</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">7. Dispensation</a>
					<a class="btn btn-success" href="<?php echo base_url(); ?>patientinfo/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">8. SVR</a>
				</div>
			</div>
		</div>

		<!-- <form action="" method="POST" name="registration" id="registration"> -->
			<?php
           $attributes = array(
              'id' => 'registration',
              'name' => 'registration',
               'autocomplete' => 'false',
            );
           echo form_open('', $attributes); ?>
			<div class="row">
				<div class="col-md-12">
					<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Patient SVR Module</h3>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-12">
					<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">SVR Details - Sample Collection</h4>
				</div>
			</div>
			<br>
			<div class="hepc_fields">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<label for="">Sample Drawn On Date <span class="text-danger">*</span></label>
						<input type="date" name="svr_sample_drawn_on" id="svr_sample_drawn_on" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVRDrawnDate:''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" required="">
						<br class="hidden-lg-*">
					</div>
				</div>
					<div class="row" style="margin-left: 0px;">
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" style="background-color: lightgoldenrodyellow;">
								<label for="" style="padding-top: 5px;">Is Sample Stored <?php echo $patient_data[0]->IsSVRSampleStored; ?></label>
								<select name="svr_is_sample_stored" id="svr_is_sample_stored" class="form-control input_fields">
									<!-- <option value="">Select</option> -->
									<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsSVRSampleStored == 2)?'selected':''; ?>>No</option>
							<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsSVRSampleStored == 1)?'selected':''; ?>>Yes</option>
								</select>
								<br>
					</div>
					<div class="col-md-3 hepc_sample_stored_field" style="background-color: lightgoldenrodyellow;">
								<label for="">Sample Storage Temperature (&#176;C)</label>
								<input type="number" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVRStorageTemp:''; ?>" name="svr_sample_storage_temp" id="svr_sample_storage_temp" class="input_fields form-control">
								<br>
					</div>
					</div>
					<br>
					<div class="row" style="margin-left: 0px;">
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" style="background-color: lightblue;">
								<label for="" style="padding-top: 5px;">Is Sample Transported<?php echo $patient_data[0]->IsSVRSampleTransported; ?></label>
								<select class="form-control input_fields" id="svr_is_sample_transported" name="svr_is_sample_transported">
									<option value="">Select</option>
									<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsSVRSampleTransported == 1)?'selected':''; ?>>Yes</option>
							<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsSVRSampleTransported == 2)?'selected':''; ?>>No</option>
								</select>
								<br>
					</div>
					<div class="col-md-3 hepc_sample_transported_field" style="background-color: lightblue;">
								<label for="" style="padding-top: 5px;">Sample Transport Temperature (&#176;C)</label>
								<input type="number" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVRTransportTemp:''; ?>" name="svr_sample_transport_temp" id="svr_sample_transport_temp" class="input_fields form-control">
								<br>
					</div>
					<div class="col-md-2 hepc_sample_transported_field" style="background-color: lightblue;">
								<label for="" style="padding-top: 5px;">Sample Transport Date</label>
								<input type="date" name="svr_sample_transport_date" id="svr_sample_transport_date" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVRTransportDate:''; ?>" class="input_fields form-control" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
								<br>
					</div>
					<div class="col-md-2 hepc_sample_transported_field" style="background-color: lightblue;">
								<label for="" style="padding-top: 5px;">Sample Transported To</label>
								<select type="text" name="svr_sample_transport_to" id="svr_sample_transport_to" class="form-control input_fields" >
									<option value="">Select</option>
									<?php 
							foreach ($search_facilitiesval as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->SVR12W_LabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
								<?php 
							}
							?>
								</select>
								<br>
					</div>
						</div>
						<div class="row" style="margin-left: 0px;">
							<div class="col-md-3 hepc_sample_transported_field" style="background-color: lightblue;">
							<label for="hepc_sample_transported_to_name" style="padding-top: 5px;">Sample Transported By : Name</label>
								<input type="text" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVRTransporterName:''; ?>" name="svr_sample_transport_by_name" id="svr_sample_transport_by_name" class="input_fields form-control messagedata">
								<br>
							</div>
					
							<div class="col-md-2 hepc_sample_transported_field" style="background-color: lightblue;">
								<label for="" style="padding-top: 5px;">Designation</label>
								<select type="text" name="svr_sample_transport_by_designation" id="svr_sample_transport_by_designation" class="form-control input_fields" >
									<option value="">Select</option>
										<?php 
							foreach ($designation_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->SVRTransporterDesignation == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
								</select>
								<br>
							</div>
						</div>
					
					<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<label for="">Remarks</label>
						<textarea rows="5" style="border: 1px #CCC solid; width: 100%;" name="svr_remarks"> <?php echo (count($patient_data) > 0)?$patient_data[0]->SVRTransportRemark:''; ?> </textarea>
					</div>
				</div>
				<br>
			</div>

			<div class="row">
				<div class="col-md-12">
					<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">SVR Details - Result</h4>
				</div>
			</div>
			<br>
			<div class="row" style="margin-left: 0px;">
				<div class="col-md-2 sample_accepted_fields" style="background-color: lightblue;">
					<label for="svr_sample_receipt_date" style="padding-top: 5px;">Sample Receipt Date</label>
					<input type="date" name="svr_sample_receipt_date" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVRReceiptDate:''; ?>" id="svr_sample_receipt_date" class="input_fields form-control" onkeyup="return false" max="<?php echo date('Y-m-d');?>"" required="">
					<br>
				</div>
			    <div class="col-md-3 sample_accepted_fields"  style="background-color: lightblue;">
						<label for="sample_accepted_fields" style="padding-top: 5px;">Sample Received By : Name</label>
					<input type="text" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVRReceiverName:''; ?>" name="svr_sample_receieved_by_name" id="svr_sample_receieved_by_name" class="input_fields form-control messagedata" required="">
					<br>
				</div>
				<div class="col-md-2 sample_accepted_fields" style="background-color: lightblue;">
					<label for="svr_sample_receieved_by_designation" style="padding-top: 5px;">Designation</label>
					<select class="form-control input_fields" id="svr_sample_receieved_by_designation" name="svr_sample_receieved_by_designation">
						<option value="">Select</option>
						<?php 
							foreach ($designation_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->SVRReceiverDesignation == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
					</select>
					<br>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 hepc_sample_accepted_fields">
					<label for="">Test Result Date <span class="text-danger">*</span></label>
					<input type="date" name="svr_result_date" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVR12W_HCVViralLoad_Dt:''; ?>" id="svr_result_date" class="input_fields form-control" onkeyup="return false" max="<?php echo date('Y-m-d');?>"" required="">
					<br class="hidden-lg-*">
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 hepc_sample_accepted_fields">
					<label for="">Viral Load Count <span class="text-danger">*</span></label>
					<input type="text" name="svr_viral_load" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVR12W_HCVViralLoadCount:''; ?>" id="svr_viral_load" class="input_fields form-control" required="" onkeypress="return onlyNumbersWithDot(event);">
					<br class="hidden-lg-*">
				</div>
			<!-- 	<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 hepc_sample_accepted_fields">
					<label for="">Re-enter Viral Load Count</label>
					<input type="text" name="svr_re_entry_viral_load" id="svr_re_entry_viral_load" class="input_fields form-control">
					<br class="hidden-lg-*">
				</div> -->
				<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 hepc_sample_accepted_fields">
					<label for="">Result <span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="svr_result" name="svr_result" required="">
						<option value="">Select</option>
						<?php 
							foreach ($results_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->SVR12W_Result == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
					</select>
				</div>
				<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 hepc_sample_rejected_fields">
					<label for="">Doctor</label>
					<select class="form-control input_fields" id="svr_doctor" name="svr_doctor">
						<option value="">Select</option>
						<?php foreach ($doctors as $row) {?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->SVRDoctor == $row->id_mst_medical_specialists)?'selected':''; ?> value="<?php echo $row->id_mst_medical_specialists; ?>"><?php echo $row->name; ?></option>
							<?php } ?>

					</select>
				</div>
				<div class="row col-lg-2 col-md-3 col-sm-6 col-xs-12 svr_doctor_other_field">
						<label for="">SVR Doctor Other <span class="text-danger">*</span></label>
						<input type="text" name="svr_doctor_other" id="svr_doctor_other" class="input_fields form-control messagedata" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVRDoctorOther:''; ?>">
					</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
					<label for="">Comments</label>
					<textarea rows="5" name="svr_comments" value="" id="svr_comments" class="form-control" style="border: 1px #CCC solid; width: 100%;"><?php echo (count($patient_data) > 0)?$patient_data[0]->SVRComments:''; ?></textarea>
				</div>
			</div>
			<br>
			<input type="hidden" name="interruption_status" id="interruption_status" value="<?php  echo (count($patient_data) > 0)?$patient_data[0]->InterruptReason:'';  ?>">
			
			<div class="row">
				<div class="col-lg-2 col-md-2">
					<a class="btn btn-block btn-default form_buttons" href="<?php echo base_url('patientinfo?p=1'); ?>" id="close" name="close" value="close">CLOSE</a>
				</div>
				<div class="col-lg-2 col-md-2">
					<button class="btn btn-block btn-default form_buttons" id="refresh" name="refresh" value="refresh">REFRESH</button>
				</div>
				<div class="col-lg-2 col-md-2">
					<button class="btn btn-block btn-default form_buttons" id="lock" name="lock" value="lock">LOCK</button>
				</div>
				<?php if($result[0]->SVR == 1) {?>
					<div class="col-lg-6 col-md-6">
						<button class="btn btn-block btn-success form_buttons" id="save" name="save" value="save">SAVE</button>
					</div>
				<?php } ?>
			</div>
			<br>
			<br>
			<br>

			<div class="row" class="text-left">

					<div class="col-md-6">
					 <label class="btn  btn-default form_buttons"  style="text-align: left !important; ">Patient's Status 
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" name=""  readonly="readonly" value="<?php echo (count($patient_status) > 0)?$patient_status[0]->status:''; ?>" class="btn">
					</label>
				</div>

				

				<div class="col-md-6">
					<label class="btn btn-default form_buttons" style="text-align: left !important;     line-height: 3.2 !important;">Patient's Interruption Status 
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<select name="" id="type_val" onchange="openPopup()" class="btn" style="text-align: right!important;">
						<option value="">Select</option>
						<option value="1"  <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason != '')?'selected':''; ?> >Yes</option>
						<option value="0" <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason == '')?'selected':''; ?>>No</option>
					</select>
				
				</label>
				</div>
			
			</div>

		          <?php echo form_close(); ?>
		<!-- </form> -->
	</div>
</div>



<div class="modal fade" id="addMyModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title">Patient's Interruption Status</h4>
      </div>
      <span id='form-error' style='color:red'></span>
      <div class="modal-body">
      <!--   <form role="form" id="newModalForm" method="post"> -->
      	<?php
           $attributes = array(
              'id' => 'newModalForm',
              'name' => 'newModalForm',
               'autocomplete' => 'false',
            );
           echo form_open('', $attributes); ?>


				<div class="form-group">
				<label class="control-label col-md-3" for="email">Reson:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonval" name="resonval" required="required">
				<option value="">Select</option>
				<option value="1">Death</option>
				<option value="2">Loss to followup</option>
				</select> 
				</div>
				</div>
<br/><br/>

				<div class="form-group resonfielddeath" >
				<label class="control-label col-md-3" for="email">Reason for death:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonvaldeath" name="resonvaldeath">
				<option value="">Select</option>
				<?php foreach ($reason_death as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
				
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>

				<div class="form-group resonfieldlfu">
				<label class="control-label col-md-3" for="email">Reason for LFU:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonfluid" name="resonfluid">
				<option value="">Select</option>
				<?php foreach ($reason_flu as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
				
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>


			<div class="control-label col-md-3">
				
				<input type="radio" name="interruption_stage" value="1">
				<label>ETR</label>
			</div>
			<div class="control-label col-md-3">
				<input type="radio" name="interruption_stage" value="2">
				<label>SVR</label>
			</div>

			<div class="control-label col-md-3">
				<input type="radio" name="interruption_stage" value="3">
				<label>NONE</label>
			</div>
			<br/><br/>

			


          <div class="modal-footer">
            <button type="submit" class="btn btn-success" id="btnSaveIt">Save</button>
            <button type="button" class="btn btn-default" id="btnCloseIt" data-dismiss="modal">Close</button>
          </div>
           <?php echo form_close(); ?>
       <!--  </form> -->
      </div>
    </div>
  </div>
</div>

<br/><br/><br/>

<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>

<script>
	function openPopup() {
		var type_val = $('#type_val').val();
		if(type_val=='1'){
    $("#addMyModal").modal();
		}
}

$('.resonfielddeath').hide();
$('.resonfieldlfu').hide();


$('#btnCloseIt').click(function(){

$('#type_val').val('0');
$('#interruption_status').val('');

});

$('#type_val').change(function(){
var type_val = $('#type_val').val();
$('#interruption_status').val(type_val);

});


 $('#btnSaveIt').click(function(e){
      
      e.preventDefault(); 
      $("#form-error").html('');
     
        
	 var formData = new FormData($('#newModalForm')[0]);
	 $('#btnSaveIt').prop('disabled',true);
	 $.ajax({				    	
		url: '<?php echo base_url(); ?>patientinfo/interruptionstatus_process/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
		type: 'POST',
		data: formData,
		dataType: 'json',
		async: false,
        cache: false,
		contentType: false,
		processData: false,
		success: function (data) { 
				
			if(data['status'] == 'true'){
				$("#form-error").html('Data has been successfully submitted');
				setTimeout(function() {
    					location.href='<?php echo base_url(); ?>patientinfo/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
				}, 1000);

			}else{
				$("#form-error").html(data['message']);
				$('#btnSaveIt').prop('disabled',false); 
				return false;
			}   
		}        
	 }); 
		
   

    }); 


	$('#resonval').change(function(){
		

		if($('#resonval').val() == '1'){

		$('.resonfielddeath').show();
		$('.resonfieldlfu').hide();
		$('#resonfluid').val('');

		$('#resonvaldeath').prop('required',true);
		$('#resonfluid').prop('required',false);

	}else if($('#resonval').val() == '2'){
		$('.resonfielddeath').hide();
		$('.resonfieldlfu').show();
		$('#resonvaldeath').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',true);
	}else{

		$('.resonfielddeath').hide();
		$('.resonfieldlfu').hide();
		$('#resonvaldeath').val('');
		$('#resonfluid').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',false);
	}

});
</script>


<script type="text/javascript">

	$("#svr_doctor").change(function(){
					if($(this).val() == 999)
					{
						$(".svr_doctor_other_field").show();
						$('#svr_doctor_other').prop('required',true);
					}
					else
					{
						$(".svr_doctor_other_field").hide();
						$('#svr_doctor_other').prop('required',false);
						$('#svr_doctor_other').val('');
					}
				});



	$('#svr_sample_storage_temp').change(function( event ) {
		
	var svr_sample_storage_temp = $('#svr_sample_storage_temp').val();

		if(svr_sample_storage_temp > 100){

						$("#modal_header").text("Sample Storage Temperature (°C) Less than or equal to 100 to -100");
						$("#modal_text").text("Please check Sample Storage Temperature (°C)");
						$('#svr_sample_storage_temp').val(0);
						$("#multipurpose_modal").modal("show");
						return false;

			
		}

		if(svr_sample_storage_temp < -100){

						$("#modal_header").text("Sample Storage Temperature (°C) Less than or equal to 100 to -100");
						$("#modal_text").text("Please check Sample Storage Temperature (°C)");
						$('#svr_sample_storage_temp').val(0);
						$("#multipurpose_modal").modal("show");
						return false;

			
		}

		});


	$('#svr_sample_transport_temp').change(function( event ) {
		
	var svr_sample_transport_temp = $('#svr_sample_transport_temp').val();

		if(svr_sample_transport_temp > 100){

						$("#modal_header").text("Sample Storage Temperature (°C) Less than or equal to 100 to -100");
						$("#modal_text").text("Please check Sample Storage Temperature (°C)");
						$('#svr_sample_transport_temp').val(0);
						$("#multipurpose_modal").modal("show");
						return false;

			
		}

		if(svr_sample_transport_temp < -100){

						$("#modal_header").text("Sample Storage Temperature (°C) Less than or equal to 100 to -100");
						$("#modal_text").text("Please check Sample Storage Temperature (°C)");
						$('#svr_sample_transport_temp').val(0);
						$("#multipurpose_modal").modal("show");
						return false;

			
		}

		});



$(".svr_doctor_other_field").hide();
	<?php 
				if( count($patient_data) > 0 && $patient_data[0]->SVRDoctor == 999 ) {
				?>

						$(".svr_doctor_other_field").show();
						$('#svr_doctor_other').prop('required',true);
					<?php } else{ ?>
					
						$(".svr_doctor_other_field").hide();
						$('#svr_doctor_other').prop('required',false);
				<?php	}  ?>

	$("#svr_sample_drawn_on" ).change(function( event ) {

var date_of_prescribing_testsdate = '<?php echo $patient_data[0]->Next_Visitdt; ?>';
var svr_sample_drawn_on = $("#svr_sample_drawn_on" ).val();
//alert(date_of_prescribing_testsdate);

			var date = new Date('<?php echo $patient_data[0]->Next_Visitdt; ?>');
			var remaningpillsval =  7;
			days = parseInt(remaningpillsval, 10);
			if(!isNaN(date.getTime())){
			date.setDate(date.getDate() - days);



if(date.toInputFormat() > svr_sample_drawn_on ){

	$("#modal_header").text("Visit Date greater than or equal to Prescribing Date"+date_of_prescribing_testsdate);
					$("#modal_text").text("Please check dates");
					$("#svr_sample_drawn_on" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

}

});

	


 $("#svr_sample_transport_date" ).change(function( event ) {

//var screening_date = '<?php echo $patient_data[0]->Next_Visitdt; ?>';


var svr_sample_transport_date = $("#svr_sample_transport_date").val();
var svr_sample_drawn_on = $("#svr_sample_drawn_on" ).val();
//alert(svr_sample_transport_date+'/'+svr_sample_drawn_on);
if(svr_sample_drawn_on > svr_sample_transport_date){

	$("#modal_header").text("Sample Transport Date  greater than or equal Sample Drawn On Date .");
					$("#modal_text").text("Please check dates");
					$("#svr_sample_transport_date" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

});



Date.prototype.toInputFormat = function() {
       var yyyy = this.getFullYear().toString();
       var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
       var dd  = this.getDate().toString();
       return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
    };



		$('.messagedata').keypress(function (e) {
        var regex = new RegExp(/^[a-zA-Z\s]+$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            return false;
        }
    });

				function onlyNumbersWithDot(e) {           
            var charCode;
            if (e.keyCode > 0) {
                charCode = e.which || e.keyCode;
            }
            else if (typeof (e.charCode) != "undefined") {
                charCode = e.which || e.keyCode;
            }
            if (charCode == 46)
                return true
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }




 $("#svr_sample_receipt_date" ).change(function( event ) {

//var screening_date = '<?php echo $patient_data[0]->Next_Visitdt; ?>';


var svr_sample_drawn_on = $("#svr_sample_drawn_on").val();
var svr_sample_receipt_date = $("#svr_sample_receipt_date" ).val();

if(svr_sample_drawn_on > svr_sample_receipt_date){

	$("#modal_header").text("Sample Receipt Date  greater than or equal Sample Receipt Date ");
					$("#modal_text").text("Please check dates");
					$("#svr_sample_receipt_date" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

});


 $("#svr_result_date" ).change(function( event ) {

//var screening_date = '<?php echo $patient_data[0]->Next_Visitdt; ?>';


var svr_sample_receipt_date = $("#svr_sample_receipt_date").val();
var svr_result_date = $("#svr_result_date" ).val();

if(svr_sample_receipt_date > svr_result_date){

	$("#modal_header").text("Test Result Date greater than or equal Sample Receipt Date");
					$("#modal_text").text("Please check dates");
					$("#svr_result_date" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

});



</script>
<?php if($result[0]->SVR == 1) {?>
	<script>

		$(document).ready(function(){




			<?php 
			$error = $this->session->flashdata('error'); 

			if($error != null)
			{
				?>
				$("#multipurpose_modal").modal("show");
				<?php 
			}
			?>

			$("#refresh").click(function(e){
			// if(confirm('All further details will be deleted.Do you want to continue?')){ 
				$("input").val('');
				$("select").val('');
				$("textarea").val('');
				$("#input_district").html('<option>Select District</option>');
				$("#input_block").html('<option>Select Block</option>');

				e.preventDefault();
			// }
			});

<?php if(count($patient_data) > 0 && $patient_data[0]->IsSVRSampleStored == 1) {?>
				$('.hepc_sample_stored_field').show();
<?php } else { ?>
			
			$('.hepc_sample_stored_field').hide();

<?php } ?>
<?php if(count($patient_data) > 0 && $patient_data[0]->IsSVRSampleTransported == 1) {?>
			$('.hepc_sample_transported_field').show();
	<?php } else{ ?>
			$('.hepc_sample_transported_field').hide();
	<?php } ?>



			$("#svr_is_sample_stored").change(function(){
				if($(this).val() == 1)
				{
					$(".hepc_sample_stored_field").show();
					$('#svr_sample_storage_temp').prop('required',true); 
				}
				else
				{
					$(".hepc_sample_stored_field").hide();
					$('#svr_sample_storage_temp').prop('required',false); 
				}
			});

			$("#svr_is_sample_transported").change(function(){
				if($(this).val() == 1)
				{
					$(".hepc_sample_transported_field").show();
					$(".sample_accepted_fields").show();
					$('#svr_sample_transport_to').prop('required',true); 
					$('#svr_sample_transport_by_name').prop('required',true); 
					$('#svr_sample_transport_by_designation').prop('required',true);

					$('#svr_sample_receipt_date').prop('required',true); 
					$('#svr_sample_receieved_by_name').prop('required',true); 
					$('#svr_sample_receieved_by_designation').prop('required',true); 
				}
				else
				{
					$(".hepc_sample_transported_field").hide();
					$(".sample_accepted_fields").hide();
					$('#svr_sample_transport_to').prop('required',false); 
					$('#svr_sample_transport_by_name').prop('required',false); 
					$('#svr_sample_transport_by_designation').prop('required',false);
					 
					$('#svr_sample_receipt_date').prop('required',false); 
					$('#svr_sample_receieved_by_name').prop('required',false); 
					$('#svr_sample_receieved_by_designation').prop('required',false); 
					$('#svr_sample_receipt_date').val('');
					$('#svr_sample_receieved_by_name').val('');
					$('#svr_sample_receieved_by_designation').val('');
				}
			});

		});
	</script>
<?php } else { ?>
	<script>
		$(document).ready(function(){
			$('input').prop('disabled', true);
			$('select').prop('disabled', true);
			$('textarea').prop('disabled', true);
		});
	</script>
	<?php } ?>