<?php 
$loginData = $this->session->userdata('loginData');

$sql = "SELECT VL FROM `MSTRole` where RoleId = ".$loginData->RoleId;
$result = $this->db->query($sql)->result();
?>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #A30A0C !important;
	background-color: white !important;
	border : 1px solid #A30A0C;
}

select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}

</style>
<?php //echo "<pre>";print_r($patient_data); ?>
<br>
<div class="row equal">
	<div class="col-lg-10 col-lg-offset-1">

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="btn-group">
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">1. Registration</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">2. Screening</a>
					<a class="btn btn-success" href="<?php echo base_url(); ?>patientinfo/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">3. Viral Load</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">4. Testing</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/known_history/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">5. Known History</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">6. Prescription</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">7. Dispensation</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">8. SVR</a>
				</div>
			</div>
		</div>

		<!-- <form action="" method="POST" name="patient_viral_load_form" id="patient_viral_load_form"> -->
						<?php
           $attributes = array(
              'id' => 'patient_viral_load_form',
              'name' => 'patient_viral_load_form',
               'autocomplete' => 'false',
            );
           echo form_open('', $attributes); ?>

           <?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){ ?>
			<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				<?php } ?>
			<div class="row">
				<div class="col-md-12">
					<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Patient Viral Load Module</h3>
				</div>
			</div>
			<br>

			<div class="row">
				<div class="col-md-4">
					<h4><b>VIRAL LOAD TEST DETAILS</b></h4>
				</div>
				<div class="col-md-2">
					<label class="checkbox-inline"><input type="checkbox" checked value="1" name="check_hepc" id="check_hepc" style="width: 20px; height: 20px;"><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">HEP-C</b></label>
				</div>
				<?php 
				if($patient_data[0]->HBSRapidResult == 1 || $patient_data[0]->HBSElisaResult == 1 || $patient_data[0]->HBSOtherResult == 1 || $patient_data[0]->HBCRapidResult == 1 || $patient_data[0]->HBCElisaResult == 1 || $patient_data[0]->HBCOtherResult == 1)
				{

					?>
					<div class="col-md-2">
						<label class="checkbox-inline"><input type="checkbox" value="1" name="check_hepb" id="check_hepb" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->VLHepB == 1)?'checked':''; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">HEP-B</b></label>
					</div>

				<?php } ?>
			</div>
			<hr>

			<!-- hepc_fields -->

			<div class="row hepc_fields">
				<div class="col-md-12">
					<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">HEP-C Viral Load - Sample Collection
						<?php if(count($patient_data) > 0 && $patient_data[0]->MF4 == 1) {?>
							<p class="text-right""> Re-enter VL data in case of sample rejection</p>
						<?php } else{?>
						<p class="text-right""><a href="#" align="right" id="creenterrejection"> Re-enter VL data in case of sample rejection</a></p>
					<?php } ?>

					</h4>
				</div> 
			</div>
			<br class="hepc_fields">
			<div class="hepc_fields">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<label for="">Sample Drawn On Date <span class="hepc_required" style="color: red;">*</span></label>
						<input type="date" name="hepc_vl_sample_drawn_date" id="hepc_vl_sample_drawn_date" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->VLSampleCollectionDate:''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" required>
						<br class="hidden-lg-*">
					</div>
				</div>
				<div class="row" style="margin-left: 0px;">
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" style="background-color: lightgoldenrodyellow;">
						<label for="" style="padding-top: 5px;">Is Sample Stored</label>
						<select name="hepc_is_sample_stored" id="hepc_is_sample_stored" class="form-control input_fields">
							<!-- <option value="">Select</option> -->
							<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsVLSampleStored == 2)?'selected':''; ?>>No</option>
							<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsVLSampleStored == 1)?'selected':''; ?>>Yes</option>
							
						</select>
						<br>
					</div>
					<div class="col-md-3 hepc_sample_stored_field" style="background-color: lightgoldenrodyellow;">
						<label for="hepc_sample_storage_temp" style="padding-top: 5px;">Sample Storage Temperature (&#176;C)</label> <!-- onkeypress="return onlyNumbersWithDot1(event);" -->
						<input type="number" maxlength="4" name="hepc_sample_storage_temp" id="hepc_sample_storage_temp" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->VLStorageTemp:''; ?>" <?php if(count($patient_data) > 0 && $patient_data[0]->MF4 == 1) {?> readonly <?php } ?>>
						<br>
					</div>
					<div class="col-md-2 hepc_sample_stored_field" style="background-color: lightgoldenrodyellow;">
						<label for="hepc_sample_stored_duration" style="padding-top: 5px;">Sample Storage Duration</label>
						<select class="form-control input_fields" id="hepc_sample_stored_duration_more_less_than_day" name="hepc_sample_stored_duration_more_less_than_day">
							<option value="">Select</option>
							<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->Storage_days_hrs == 1)?'selected':''; ?>>Less than 1 day</option>
							<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->Storage_days_hrs == 2)?'selected':''; ?>>More than 1 day</option>
						</select>
						<br>
					</div>
					<div class="col-md-3 hepc_sample_stored_field" style="background-color: lightgoldenrodyellow;">
						<label for="hepc_sample_storage_temp" id="hepc_sample_stored_duration_label" style="padding-top: 5px;">Duration (in hours)</label>
						<input type="text" name="hepc_sample_stored_duration" id="hepc_sample_stored_duration" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->Storageduration:''; ?>" onkeypress="return onlyNumbersWithDot(event);">
						<br>
					</div>
				</div>
				<br>
				<div class="row" style="margin-left: 0px;">
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" style="background-color: lightblue;">
						<label for="hepc_is_sample_transported" style="padding-top: 5px;">Is Sample Transported</label>
						<select class="form-control input_fields" id="hepc_is_sample_transported" name="hepc_is_sample_transported">
							<!-- <option value="">Select</option> -->
							<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsVLSampleTransported == 2)?'selected':''; ?>>No</option>
							<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsVLSampleTransported == 1)?'selected':''; ?>>Yes</option>
							
						</select>
						<br>
					</div>

					<div class="col-md-3 hepc_sample_transported_field" style="background-color: lightblue;">
						<label for="hepc_sample_transport_temp" style="padding-top: 5px;">Sample Transport Temperature (&#176;C)</label>
						<input type="number" name="hepc_sample_transport_temp" id="hepc_sample_transport_temp" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->VLTransportTemp:''; ?>" <?php if(count($patient_data) > 0 && $patient_data[0]->MF4 == 1) {?> readonly <?php } ?>>
						<br>
					</div>
					<div class="col-md-2 hepc_sample_transported_field" style="background-color: lightblue;">
						<label for="hepc_sample_transport_date" style="padding-top: 5px;">Sample Transport Date</label>
						<input type="date" name="hepc_sample_transport_date" id="hepc_sample_transport_date" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->VLTransportDate:''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
						<br>
					</div>
					<div class="col-md-2 hepc_sample_transported_field" style="background-color: lightblue;">
						<label for="hepc_sample_transported_to" style="padding-top: 5px;">Sample Transported To</label>
						<select type="text" name="hepc_sample_transported_to" id="hepc_sample_transported_to" class="form-control input_fields" >
							<option value="">Select</option>
							<?php 
							foreach ($sample_transported_to_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->VLLabID == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
						</select>
						<br>
					</div>
					<div class="col-md-2 hepc_sample_transported_other_field" style="background-color: lightblue;">
						<label for="hepc_sample_transported_to_other" style="padding-top: 5px;">Other</label>
						<input type="text" name="hepc_sample_transported_to_other" id="hepc_sample_transported_to_other" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->VLLabID_Other:''; ?>">
						<br>
					</div>
				</div>

				<div class="row" style="margin-left: 0px;">
					<div class="col-md-3 hepc_sample_transported_field" style="background-color: lightblue;">
						<label for="hepc_sample_transported_to_name" style="padding-top: 5px;">Sample Transported By : Name</label>
						<input type="text" name="hepc_sample_transported_to_name" id="hepc_sample_transported_to_name" class="input_fields form-control messagedata" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->VLTransporterName:''; ?>">
						<br>
					</div>
					<div class="col-md-2 hepc_sample_transported_field" style="background-color: lightblue;">
						<label for="hepc_sample_transported_to_designation" style="padding-top: 5px;">Designation</label>
						<select type="text" name="hepc_sample_transported_to_designation" id="hepc_sample_transported_to_designation" class="form-control input_fields" >
							<option value="">Select</option>
							<?php 
							foreach ($designation_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->VLTransporterDesignation == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
						</select>
						<br>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<label for="">Remarks</label>
						<textarea rows="5" style="border: 1px #CCC solid; width: 100%;" name="hepc_sample_remarks" id="hepc_sample_remarks" class="form-control" <?php if(count($patient_data) > 0 && $patient_data[0]->MF4 == 1) {?> readonly <?php } ?>><?php echo (count($patient_data) > 0)?$patient_data[0]->VLSCRemarks:''; ?></textarea>
					</div>
				</div>
				<br>
			</div>

			<div class="row hepc_fields">
				<div class="col-md-12">
					<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">HEP-C Confirmatory Viral Load Details - Result</h4>
				</div>
			</div>
			<br class="hepc_fields">
			<div class="hepc_fields">
				<div class="row" style="margin-left: 0px;">
					<div class="col-md-2 hepc_sample_transported_field" style="background-color: lightblue;">
						<label for="hepc_sample_receipt_date" style="padding-top: 5px;">Sample Receipt Date</label>
						<input type="date" name="hepc_sample_receipt_date" id="hepc_sample_receipt_date" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->VLRecieptDate:''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
						<br>
					</div>
					<div class="col-md-3 hepc_sample_transported_field" style="background-color: lightblue;">
						<label for="hepc_sample_transported_to_name" style="padding-top: 5px;">Sample Received By : Name</label>
						<input type="text" name="hepc_sample_received_by_name" id="hepc_sample_received_by_name" class="input_fields form-control messagedata" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->VLReceiverName:''; ?>">
						<br>
					</div>
					<div class="col-md-2 hepc_sample_transported_field" style="background-color: lightblue;">
						<label for="hepc_sample_received_by_designation" style="padding-top: 5px;">Designation</label>
						<select type="text" name="hepc_sample_received_by_designation" id="hepc_sample_received_by_designation" class="form-control input_fields">
							<option value="">Select</option>
							<?php 
							foreach ($designation_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->VLReceiverDesignation == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
						</select>
						<br>
					</div>
				</div>
				<br class="hepc_sample_transported_field">
				<div class="row">
					<div class="col-md-2">
						<label for="">Is Sample Accepted</label>
						<select class="form-control input_fields" id="hepc_is_sample_accepted" name="hepc_is_sample_accepted" required="">
							
							<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsSampleAccepted == 1)?'selected':''; ?>>Yes</option>
							<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsSampleAccepted == 2)?'selected':''; ?>>No</option>
							<!-- <option value="">Select</option> -->
						</select>
						<br class="hidden-lg-*">
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 hepc_sample_accepted_fields">
						<label for="">Test Result Date</label>
						<input type="date" name="hepc_vl_result_date" id="hepc_vl_result_date" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->T_DLL_01_VLC_Date:''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
						<br class="hidden-lg-*">
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 hepc_sample_accepted_fields">
						<label for="">Viral Load</label>
						<input type="text" name="hepc_vl" id="hepc_vl" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->T_DLL_01_VLCount:''; ?>" onkeypress="return onlyNumbersWithDot(event);">
						<br class="hidden-lg-*">
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 hepc_sample_accepted_fields">
						<label for="">Re-enter Viral Load</label>
						<input type="text" name="hepc_vl_reenter" id="hepc_vl_reenter" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->T_DLL_01_VLCount:''; ?>" onkeypress="return onlyNumbersWithDot(event);">
						<br class="hidden-lg-*">
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 hepc_sample_accepted_fields">
						<label for="">Result</label>
						<select class="form-control input_fields" id="hepc_vl_result" name="hepc_vl_result">
							<option value="">Select</option>
							<?php 
							foreach ($results_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->T_DLL_01_VLC_Result == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
						</select>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hepc_sample_rejected_fields">
						<label for="">Reason for Rejection</label>
						<select class="form-control input_fields" id="hepc_sample_reason_for_rejection" name="hepc_sample_reason_for_rejection">
							<option value="">Select</option>
							<?php 
							foreach ($rejection_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->RejectionReason == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
						</select>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hepc_sample_reason_for_rejection_other_field">
						<label for="">Others specify</label>
						<input type="text" class="form-control input_fields" name="hepc_sample_reason_for_rejection_other" id="hepc_sample_reason_for_rejection_other">
					</div>
				</div>
				<div class="row">
					
					<div class="col-lg-12 col-md-2 col-sm-6 col-xs-12">
						<label for="">Remarks</label>
						<textarea rows="5" style="border: 1px #CCC solid; width: 100%;" name="hepc_vl_remarks" id="hepc_vl_remarks" class="form-control" <?php if(count($patient_data) > 0 && $patient_data[0]->MF4 == 1) {?> readonly <?php } ?>><?php echo (count($patient_data) > 0)?$patient_data[0]->VLResultRemarks:''; ?></textarea>
						<br class="hidden-lg-*">
					</div>
				</div>
				<br>
			</div>

			<!-- hepb_fields -->

			<div class="row hepb_fields">
				<div class="col-md-12">
					<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">HEP-B Viral Load - Sample Collection
					<?php if(count($patient_data) > 0 && $patient_data[0]->MF4 == 1) {?>
							<p class="text-right""> Re-enter VL data in case of sample rejection</p>
						<?php } else{?>
						<p class="text-right""><a href="#" align="right" id="breenterrejection"> Re-enter VL data in case of sample rejection</a></p>
					<?php } ?>
				</h4>
				</div>
			</div>
			<br class="hepb_fields">
			<div class="hepb_fields">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<label for="">Sample Drawn On Date</label>
						<input type="date" name="hepb_vl_sample_drawn_date" id="hepb_vl_sample_drawn_date" class="input_fields form-control"value="<?php echo (count($patient_data) > 0)?$patient_data[0]->BVLSampleCollectionDate:''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
						<br class="hidden-lg-*">
					</div>
				</div>

				<div class="row" style="margin-left: 0;">
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" style="background-color: lightgoldenrodyellow;">
						<label for="" style="padding-top: 5px;">Is Sample Stored</label>
						<select name="hepb_is_sample_stored" id="hepb_is_sample_stored" class="form-control input_fields">
							<option value="">Select</option>
							<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsVLSampleStored == 1)?'selected':''; ?>>Yes</option>
							<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsVLSampleStored == 2)?'selected':''; ?>>No</option>
						</select>
						<br>
					</div>

					<div class="col-md-3 hepb_sample_stored_field" style="background-color: lightgoldenrodyellow;">
						<label for="hepb_sample_storage_temp" style="padding-top: 5px;">Sample Storage Temperature (&#176;C)</label>
						<input type="number" name="hepb_sample_storage_temp" id="hepb_sample_storage_temp" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->BVLStorageTemp:''; ?>">
						<br>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 hepb_sample_stored_field" style="background-color: lightgoldenrodyellow;">
						<label for="" style="padding-top: 5px;">Sample Storage Duration</label>
						<select class="form-control input_fields" id="hepb_sample_stored_duration_more_less_than_day" name="hepb_sample_stored_duration_more_less_than_day">
							<option value="">Select</option>
							<option value="1" <?php  echo (count($patient_data) > 0 && $patient_data[0]->BStorage_days_hrs == 1)?'selected':''; ?>>Less than 1 day</option>
							<option value="2" <?php  echo (count($patient_data) > 0 && $patient_data[0]->BStorage_days_hrs == 2)?'selected':''; ?>>More than 1 day</option>
						</select>
						<br>
					</div>
					<div class="col-md-2 hepb_sample_stored_field" style="background-color: lightgoldenrodyellow;">
						<label for="hepb_sample_stored_duration" id="hepb_sample_stored_duration_label" style="padding-top: 5px;">Duration (in hours)</label>
						<input type="text" name="hepb_sample_stored_duration" id="hepb_sample_stored_duration" class="input_fields form-control"  onkeypress="return onlyNumbersWithDot(event);" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->BStorageduration:''; ?>">
						<br>
					</div>
				</div>
				<br>
				<div class="row" style="margin-left: 0;">
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" style="background-color: lightblue;">
						<label for="" style="padding-top: 5px;">Is Sample Transported</label>
						<select class="form-control input_fields" id="hepb_is_sample_transported" name="hepb_is_sample_transported">
							<option value="">Select</option>
							<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsBVLSampleTransported == 1)?'selected':''; ?>>Yes</option>
							<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsBVLSampleTransported == 2)?'selected':''; ?>>No</option>
						</select>
						<br>
					</div>
					<div class="col-md-3 hepb_sample_transported_field" style="background-color: lightblue;">
						<label for="" style="padding-top: 5px;">Sample Transport Temperature (&#176;C)</label>
						<input type="number" name="hepb_sample_transport_temp" id="hepb_sample_transport_temp" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->BVLTransportTemp:''; ?>">
						<br>
					</div>
					<div class="col-md-2 hepb_sample_transported_field" style="background-color: lightblue;">
						<label for="" style="padding-top: 5px;">Sample Transport Date</label>
						<input type="date" name="hepb_sample_transport_date" id="hepb_sample_transport_date" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->BVLTransportDate:''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
						<br>
					</div>
					<div class="col-md-2 hepb_sample_transported_field" style="background-color: lightblue;">
						<label for="" style="padding-top: 5px;">Sample Transported To</label>
						<select type="text" name="hepb_sample_transported_to" id="hepb_sample_transported_to" class="form-control input_fields">
							<option value="">Select</option>
							<?php 
							foreach ($sample_transported_to_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->BVLLabID == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
						</select>
						<br>
					</div>
				</div>
				<div class="row" style="margin-left: 0;">
					<div class="col-md-3 hepb_sample_transported_field" style="background-color: lightblue;">
						<label for="" style="padding-top: 5px;">Sample Transported By : Name</label>
						<input type="text" name="hepb_sample_transported_to_name" id="hepb_sample_transported_to_name" class="input_fields form-control input_fields form-control messagedata" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->BVLTransporterName:''; ?>">
						<br>
					</div>
					<div class="col-md-2 hepb_sample_transported_field" style="background-color: lightblue;">
						<label for="" style="padding-top: 5px;">Designation</label>
						<select type="text" name="hepb_sample_transported_to_designation" id="hepb_sample_transported_to_designation" class="form-control input_fields">
							<option value="">Select</option>
							<?php 
							foreach ($designation_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->BVLTransporterDesignation == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
						</select>
						<br>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<label for="">Remarks</label>
						<textarea rows="5" style="border: 1px #CCC solid; width: 100%;" name="hepb_sample_remarks" id="hepb_sample_remarks" class="form-control"><?php echo (count($patient_data) > 0)?$patient_data[0]->BVLSCRemarks:''; ?></textarea>
					</div>
				</div>
				<br>
			</div>


			<div class="row hepb_fields">
				<div class="col-md-12">
					<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">HEP-B Confirmatory Viral Load Details - Result</h4>
				</div>
			</div>
			<br class="hepb_fields">
			<div class="hepb_fields">
				<div class="row" style="margin-left: 0px;">
					<div class="col-md-2 hepb_sample_transported_field" style="background-color: lightblue;">
						<label for="hepb_sample_receipt_date" style="padding-top: 5px;">Sample Receipt Date</label>
						<input type="date" name="hepb_sample_receipt_date" id="hepb_sample_receipt_date" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->BVLRecieptDate:''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
						<br>
					</div>
					<div class="col-md-3 hepb_sample_transported_field" style="background-color: lightblue;">
						<label for="hepb_sample_transported_to_name" style="padding-top: 5px;">Sample Received By : Name</label>
						<input type="text" name="hepb_sample_received_by_name" id="hepb_sample_received_by_name" class="input_fields form-control input_fields form-control messagedata" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->BVLReceiverName:''; ?>">
						<br>
					</div>
					<div class="col-md-2 hepb_sample_transported_field" style="background-color: lightblue;">
						<label for="hepb_sample_received_by_designation" style="padding-top: 5px;">Designation</label>
						<select type="text" name="hepb_sample_received_by_designation" id="hepb_sample_received_by_designation" class="form-control input_fields">
							<option value="">Select</option>
							<?php 
							foreach ($designation_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->BVLReceiverDesignation == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
						</select>
						<br>
					</div>
				</div>
				<br class="hepb_sample_transported_field">
				<div class="row">
					<div class="col-md-2">
						<label for="">Is Sample Accepted</label>
						<select class="form-control input_fields" id="hepb_is_sample_accepted" name="hepb_is_sample_accepted">
							<option value="">Select</option>
							<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsBSampleAccepted == 1)?'selected':''; ?>>Yes</option>
							<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsBSampleAccepted == 2)?'selected':''; ?>>No</option>
						</select>
						<br class="hidden-lg-*">
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 hepb_sample_accepted_fields">
						<label for="">Test Result Date</label>
						<input type="date" name="hepb_vl_result_date" id="hepb_vl_result_date" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->T_DLL_01_BVLC_Date:''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
						<br class="hidden-lg-*">
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 hepb_sample_accepted_fields">
						<label for="">Viral Load</label>
						<input type="text" name="hepb_vl" id="hepb_vl" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->T_DLL_01_BVLCount:''; ?>">
						<br class="hidden-lg-*" onkeypress="return onlyNumbersWithDot(event);">
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 hepb_sample_accepted_fields">
						<label for="">Re-enter Viral Load</label>
						<input type="text" name="hepb_vl_reenter" id="hepb_vl_reenter" class="input_fields form-control" onkeypress="return onlyNumbersWithDot(event);">
						<br class="hidden-lg-*" >
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 hepb_sample_accepted_fields">
						<label for="">Result</label>
						<select class="form-control input_fields" id="hepb_vl_result" name="hepb_vl_result">
							<option value="">Select</option>
							<?php 
							foreach ($results_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->T_DLL_01_BVLC_Result == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
						</select>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hepb_sample_rejected_fields">
						<label for="">Reason for Rejection</label>
						<select class="form-control input_fields" id="hepb_sample_reason_for_rejection" name="hepb_sample_reason_for_rejection">
							<option value="">Select</option>
							<?php 
							foreach ($rejection_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->BRejectionReason == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
						</select>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hepb_sample_reason_for_rejection_other_field">
						<label for="">Others specify</label>
						<input type="text" class="form-control input_fields" name="hepb_sample_reason_for_rejection_other" id="hepb_sample_reason_for_rejection_other">
					</div>

				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<label for="">Remarks</label>
						<textarea rows="5" style="border: 1px #CCC solid; width: 100%;" name="hepb_vl_remarks" id="hepb_vl_remarks" class="form-control"><?php echo (count($patient_data) > 0)?$patient_data[0]->BVLResultRemarks:''; ?></textarea>
						<br class="hidden-lg-*">
					</div>
				</div>
				<br>
			</div>
			<br>
			<input type="hidden" name="interruption_status" id="interruption_status" value="<?php  echo (count($patient_data) > 0)?$patient_data[0]->InterruptReason:'';  ?>">
			<div class="row">
				<div class="col-lg-2 col-md-2">
					<a class="btn btn-block btn-default form_buttons" href="<?php echo base_url('patientinfo?p=1'); ?>" id="close" name="close" value="close">CLOSE</a>
				</div>
				<div class="col-lg-2 col-md-2">
					<button class="btn btn-block btn-default form_buttons" id="refresh" name="refresh" value="refresh">REFRESH</button>
				</div>
				<div class="col-lg-2 col-md-2">
					<a href="#" class="btn btn-block btn-default form_buttons" id="lock" name="lock" value="lock">LOCK</a>
				</div>
				<?php if($result[0]->VL == 1) {?>
					<div class="col-lg-6 col-md-6">
						<button class="btn btn-block btn-success form_buttons" id="save" name="save" value="save">SAVE</button>
					</div>
				<?php } ?>
			</div>
			<br>
			<br>
			<br>
 <?php echo form_close(); ?>
			<div class="row" class="text-left">

					<div class="col-md-6">
					 <label class="btn  btn-default form_buttons"  style="text-align: left !important; ">Patient's Status 
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" name=""  readonly="readonly" value="<?php echo (count($patient_status) > 0)?$patient_status[0]->status:''; ?>" class="btn">
					</label>
				</div>

				

				<div class="col-md-6">
					<label class="btn btn-default form_buttons" style="text-align: left !important;     line-height: 3.2 !important;">Patient's Interruption Status 
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<select name="" id="type_val" onchange="openPopup()" class="btn" style="text-align: right!important;">
						<option value="">Select</option>
						<option value="1"  <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason != '')?'selected':''; ?> >Yes</option>
						<option value="0" <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason == '')?'selected':''; ?>>No</option>
					</select>
				
				</label>
				</div>
			
			</div>

		         
		<!-- </form> -->
	</div>
</div>



<div class="modal fade" id="addMyModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title">Patient's Interruption Status</h4>
      </div>
      <span id='form-error' style='color:red'></span>
      <div class="modal-body">
      <!--   <form role="form" id="newModalForm" method="post"> -->
      	<?php
           $attributes = array(
              'id' => 'newModalForm',
              'name' => 'newModalForm',
               'autocomplete' => 'false',
            );
           echo form_open('', $attributes); ?>


				<div class="form-group">
				<label class="control-label col-md-3" for="email">Reson:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonval" name="resonval" required="required">
				<option value="">Select</option>
				<option value="1">Death</option>
				<option value="2">Loss to followup</option>
				</select> 
				</div>
				</div>
<br/><br/>

				<div class="form-group resonfielddeath" >
				<label class="control-label col-md-3" for="email">Reason for death:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonvaldeath" name="resonvaldeath">
				<option value="">Select</option>
				<?php foreach ($reason_death as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
				
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>

				<div class="form-group resonfieldlfu">
				<label class="control-label col-md-3" for="email">Reason for LFU:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonfluid" name="resonfluid">
				<option value="">Select</option>
				<?php foreach ($reason_flu as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
				
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>


          <div class="modal-footer">
            <button type="submit" class="btn btn-success" id="btnSaveIt">Save</button>
            <button type="button" class="btn btn-default" id="btnCloseIt" data-dismiss="modal">Close</button>
          </div>
           <?php echo form_close(); ?>
       <!--  </form> -->
      </div>
    </div>
  </div>
</div>
<br/><br/><br/>


<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>

<script>

	$(document).ready(function(){

var statusval			 = '<?php echo $patient_data[0]->MF3; ?>';
var InterruptReason 	= '<?php echo $patient_data[0]->InterruptReason; ?>';

if(statusval==0 && InterruptReason==1){

					$("#modal_header").text("Further entry is not allowed,as per treatment status...");
					$("#modal_text").text("Not allowed");
					$("#multipurpose_modal").modal("show");
				
	location.href='<?php echo base_url(); ?>patientinfo/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
				
}
});

	function openPopup() {
		var type_val = $('#type_val').val();
		if(type_val=='1'){
    $("#addMyModal").modal();
		}
}

$('.resonfielddeath').hide();
$('.resonfieldlfu').hide();


$('#btnCloseIt').click(function(){

$('#type_val').val('0');
$('#interruption_status').val('');

});

$('#type_val').change(function(){
var type_val = $('#type_val').val();
$('#interruption_status').val(type_val);

});

 $('#btnSaveIt').click(function(e){
      
      e.preventDefault(); 
      $("#form-error").html('');
     
        
	 var formData = new FormData($('#newModalForm')[0]);
	 $('#btnSaveIt').prop('disabled',true);
	 $.ajax({				    	
		url: '<?php echo base_url(); ?>patientinfo/interruptionstatus_process/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
		type: 'POST',
		data: formData,
		dataType: 'json',
		async: false,
        cache: false,
		contentType: false,
		processData: false,
		success: function (data) { 
				
			if(data['status'] == 'true'){
				$("#form-error").html('Data has been successfully submitted');
				setTimeout(function() {
    					location.href='<?php echo base_url(); ?>patientinfo/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
				}, 1000);

			}else{
				$("#form-error").html(data['message']);
				$('#btnSaveIt').prop('disabled',false); 
				return false;
			}   
		}        
	 }); 
		
   

    }); 


	$('#resonval').change(function(){
		

		if($('#resonval').val() == '1'){

		$('.resonfielddeath').show();
		$('.resonfieldlfu').hide();
		$('#resonfluid').val('');

		$('#resonvaldeath').prop('required',true);
		$('#resonfluid').prop('required',false);

	}else if($('#resonval').val() == '2'){
		$('.resonfielddeath').hide();
		$('.resonfieldlfu').show();
		$('#resonvaldeath').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',true);
	}else{

		$('.resonfielddeath').hide();
		$('.resonfieldlfu').hide();
		$('#resonvaldeath').val('');
		$('#resonfluid').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',false);
	}

});
</script>


	<?php if(count($patient_data) > 0 && $patient_data[0]->MF4 == 1) {?>
	<script>	

		$("#lock").click(function(){
				$("#modal_header").text("Contact admin ,for unlocking the record");
					$("#modal_text").text("Contact Admin");
					$("#multipurpose_modal").modal("show");
					return false;

				});

			
				/*Disable all input type="text" box*/
				//alert('<?php echo $patient_data[0]->MF5; ?>');
				$('#patient_viral_load_form input[type="text"]').attr("readonly", true);
				$('#patient_viral_load_form input[type="checkbox"]').attr("disabled", true);
				$('#patient_viral_load_form input[type="date"]').attr("readonly", true);
				$("#save").attr("disabled", true);
				$("#refresh").attr("disabled", true);
				$('#patient_viral_load_form select').attr('disabled', true);
				$('#reenterrejection').attr("href",attr('disabled'));
				/*Disable textarea using id */
				$('#patient _viral_load_form #txtAddress').prop("readonly", true);
		
		</script>
<?php  } ?>


<?php if($result[0]->VL == 1) {?>
	<script>

		$(document).ready(function(){

			<?php 
			$error = $this->session->flashdata('error'); 

			if($error != null)
			{
				?>
				$("#multipurpose_modal").modal("show");
				<?php 
			}
			?>

			var screening_date = '<?php $patient_data[0]->T_DLL_01_Date; ?>'

			$("#refresh").click(function(e){
			if(confirm('All further details will be deleted.Do you want to continue?')){ 
				$("input").val('');
				$("select").val('');
				$("textarea").val('');
				$("#input_district").html('<option>Select District</option>');
				$("#input_block").html('<option>Select Block</option>');

				e.preventDefault();
			}
			});

			$(".hepc_sample_reason_for_rejection_other_field").hide();
			$(".hepb_sample_reason_for_rejection_other_field").hide();

			<?php if(count($patient_data) > 0 && $patient_data[0]->IsVLSampleStored == 1) {?>
				$(".hepc_sample_stored_field").show();
			<?php } else {?>
				$(".hepc_sample_stored_field").hide();
			<?php } ?>

			<?php if(count($patient_data) > 0 && $patient_data[0]->IsVLSampleTransported == 1) {?>
				$(".hepc_sample_transported_field").show();
			<?php } else {?>
				$(".hepc_sample_transported_field").hide();
			<?php } ?>

			<?php if(count($patient_data) > 0 && $patient_data[0]->IsSampleAccepted == 2) {?>
				$(".hepc_sample_rejected_fields").show();
				$(".hepc_sample_accepted_fields").hide();
			<?php } else {?>
				$(".hepc_sample_rejected_fields").hide();
				$(".hepc_sample_accepted_fields").show();
			<?php } ?>

			<?php if(count($patient_data) > 0 && $patient_data[0]->VLHepB == 1) {?>
				$(".hepb_fields").show();
			<?php } else {?>
				$(".hepb_fields").hide();
			<?php } ?>

			<?php if(count($patient_data) > 0 && $patient_data[0]->IsBVLSampleStored == 1) {?>
				$(".hepb_sample_stored_field").show();
			<?php } else {?>
				$(".hepb_sample_stored_field").hide();
			<?php } ?>

			<?php if(count($patient_data) > 0 && $patient_data[0]->IsBVLSampleTransported == 1) {?>
				$(".hepb_sample_transported_field").show();
			<?php } else {?>
				$(".hepb_sample_transported_field").hide();
			<?php } ?>

			<?php if(count($patient_data) > 0 && $patient_data[0]->IsBSampleAccepted == 2) {?>
				$(".hepb_sample_rejected_fields").show();
				$(".hepb_sample_accepted_fields").hide();
			<?php } else {?>
				$(".hepb_sample_rejected_fields").hide();
				$(".hepb_sample_accepted_fields").show();
			<?php } ?>

			$('#table_patient_list tbody tr').click(function(){
				window.location = $(this).data('href');
			});

			$("#check_hepc").click(function(){

				if($(this).is(':checked'))
				{
					$(".hepc_fields").show();
				}
				else
				{
					$(".hepc_fields").hide();

				}
			});	

			$("#check_hepb").click(function(){

				if($(this).is(':checked'))
				{
					$(".hepb_fields").show();
				}
				else
				{
					$(".hepb_fields").hide();

				}
			});	

			$("#hepc_is_sample_stored").change(function(){
				if($(this).val() == 1)
				{
					$(".hepc_sample_stored_field").show();
				}
				else
				{
					$(".hepc_sample_stored_field").hide();
				}
			});

			$("#hepc_is_sample_transported").change(function(){
				if($(this).val() == 1)
				{
					$(".hepc_sample_transported_field").show();
					$("#hepc_sample_transported_to_other").hide();	

					$('#hepc_sample_transport_temp').prop('required',true); 
					//$('#hepc_sample_transport_date').prop('required',true); 
					$('#hepc_sample_transported_to').prop('required',true); 
					$('#hepc_sample_transported_to_name').prop('required',true); 
					$('#hepc_sample_transported_to_designation').prop('required',true); 


				}
				else
				{
					$(".hepc_sample_transported_field").hide();
				}
			});
			
// lab_other
$(".hepc_sample_transported_other_field").hide();
			$("#hepc_sample_transported_to").change(function(){
				if($(this).val() == 99)
				{
					$(".hepc_sample_transported_other_field").show();
					$("#hepc_sample_transported_to_other").show();
				}
				else
				{
					$(".hepc_sample_transported_other_field").hide();
				}
			});
// lab_other

		$(document).ready(function(){

			if($('#hepc_vl_result_date').val()!=''){
				$('#hepc_vl_result_date').prop('required',true); 
					$('#hepc_vl').prop('required',true);
					$('#hepc_vl_reenter').prop('required',true);
					$('#hepc_vl_result').prop('required',true);
			}

			$("#hepc_is_sample_accepted").change(function(){

				if($(this).val() == 1)
				{
					//alert('vvvvv');
					$(".hepc_sample_accepted_fields").show();
					$(".hepc_sample_rejected_fields").hide();

					/*$('#hepc_vl_result_date').prop('required',true); 
					$('#hepc_vl').prop('required',true);
					$('#hepc_vl_reenter').prop('required',true);
					$('#hepc_vl_result').prop('required',true);*/

					

					


				}
				else if($(this).val() == 2)
				{
					$(".hepc_sample_accepted_fields").hide();
					$(".hepc_sample_rejected_fields").show();
					$('#hepc_vl_result_date').prop('required',false); 
					$('#hepc_vl').prop('required',false);
					$('#hepc_vl_reenter').prop('required',false);
					$('#hepc_vl_result').prop('required',false);

				}
				else
				{
					$(".hepc_sample_accepted_fields").show();
					$(".hepc_sample_rejected_fields").hide();

					

				}
			});


});

			$("#hepb_is_sample_stored").change(function(){
				if($(this).val() == 1)
				{
					$(".hepb_sample_stored_field").show();
				}
				else
				{
					$(".hepb_sample_stored_field").hide();
				}
			});

			$("#hepb_is_sample_transported").change(function(){
				if($(this).val() == 1)
				{
					$(".hepb_sample_transported_field").show();
				}
				else
				{
					$(".hepb_sample_transported_field").hide();
				}
			});

			$("#hepb_is_sample_accepted").change(function(){

				if($(this).val() == 1)
				{
					$(".hepb_sample_accepted_fields").show();
					$(".hepb_sample_rejected_fields").hide();
				}
				else if($(this).val() == 2)
				{
					$(".hepb_sample_accepted_fields").hide();
					$(".hepb_sample_rejected_fields").show();
				}
				else
				{
					$(".hepb_sample_accepted_fields").show();
					$(".hepb_sample_rejected_fields").hide();
				}
			});

			$("#hepc_sample_stored_duration_more_less_than_day").change(function(){

				if($(this).val() == 1)
				{
					$("#hepc_sample_stored_duration_label").html('Duration (in hours)');
				}
				else if($(this).val() == 2)
				{
					$("#hepc_sample_stored_duration_label").html('Duration (in days)');
				}
				else
				{
					$("#hepc_sample_stored_duration_label").html('Duration (in hours)');
				}
			});

			$("#hepb_sample_stored_duration_more_less_than_day").change(function(){

				if($(this).val() == 1)
				{
					$("#hepb_sample_stored_duration_label").html('Duration (in hours)');
				}
				else if($(this).val() == 2)
				{
					$("#hepb_sample_stored_duration_label").html('Duration (in days)');
				}
				else
				{
					$("#hepb_sample_stored_duration_label").html('Duration (in hours)');
				}
			});

			$("#hepc_sample_reason_for_rejection").change(function(){

				if($(this).val() == 99)
				{
					$(".hepc_sample_reason_for_rejection_other_field").show();
				}
				else
				{
					$(".hepc_sample_reason_for_rejection_other_field").hide();
				}
			});

			$("#hepb_sample_reason_for_rejection").change(function(){

				if($(this).val() == 99)
				{
					$(".hepb_sample_reason_for_rejection_other_field").show();
				}
				else
				{
					$(".hepb_sample_reason_for_rejection_other_field").hide();
				}
			});

			$("#hepc_vl_sample_drawn_date").change(function(e){
				if(!check_hepc_dates())
				{
					e.preventDefault();
				}
			});

			$("#hepc_sample_transport_date").change(function(e){
				if(!check_hepc_dates())
				{
					e.preventDefault();
				}
			});

			$("#hepc_sample_receipt_date").change(function(e){
				if(!check_hepc_dates())
				{
					e.preventDefault();
				}
			});

			$("#hepc_vl_result_date").change(function(e){
				if(!check_hepc_dates())
				{
					e.preventDefault();
				}
			});

			$("#hepb_vl_sample_drawn_date").change(function(e){
				if(!check_hepb_dates())
				{
					e.preventDefault();
				}
			});

			$("#hepb_sample_transport_date").change(function(e){
				if(!check_hepb_dates())
				{
					e.preventDefault();
				}
			});

			$("#hepb_sample_receipt_date").change(function(e){
				if(!check_hepb_dates())
				{
					e.preventDefault();
				}
			});

			$("#hepb_vl_result_date").change(function(e){
				if(!check_hepb_dates())
				{
					e.preventDefault();
				}
			});

			$("#patient_viral_load_form").submit(function(e){
				if(!check_hepc_dates())
				{
					e.preventDefault();
				}
				if(!check_hepb_dates())
				{
					e.preventDefault();
				}
			})
		});




function check_hepc_dates()
{
	if($("#hepc_vl_sample_drawn_date").val() != '')
	{
		var hepc_vl_sample_drawn_date  = new Date($("#hepc_vl_sample_drawn_date").val());

		if($("#hepc_sample_transport_date").val() != '')
		{
			var hepc_sample_transport_date  = new Date($("#hepc_sample_transport_date").val());

			if(hepc_vl_sample_drawn_date > hepc_sample_transport_date)
			{
				$("#modal_header").text("Hep-C Sample Drawn Date Cannot be later than Sample Transport Date");
				$("#hepc_sample_transport_date").val('');
				$("#modal_text").text("Please check dates");
				$("#multipurpose_modal").modal("show");
				return false;
			}
		}
		if($("#hepc_sample_receipt_date").val() != '')
		{
			var hepc_sample_receipt_date  = new Date($("#hepc_sample_receipt_date").val());

			if(hepc_vl_sample_drawn_date > hepc_sample_transport_date)
			{
				$("#modal_header").text("Hep-C Sample Drawn Date Cannot be later than Sample Transport Date");
				$("#modal_text").text("Please check dates");
				$("#hepc_sample_receipt_date").val('');
				$("#multipurpose_modal").modal("show");
				return false;
			}
		}
		if($("#hepc_vl_result_date").val() != '')
		{
			var hepc_vl_result_date  = new Date($("#hepc_vl_result_date").val());

			if(hepc_vl_sample_drawn_date > hepc_vl_result_date)
			{
				$("#modal_header").text("Hep-C Sample Drawn Date Cannot be later than Test Result Date");
				$("#modal_text").text("Please check dates");
				$("#hepc_vl_result_date").val('');
				$("#multipurpose_modal").modal("show");
				return false;
			}
		}
	}

	if($("#hepc_sample_transport_date").val() != '')
	{
		var hepc_sample_transport_date  = new Date($("#hepc_sample_transport_date").val());

		if($("#hepc_vl_sample_drawn_date").val() != '')
		{
			var hepc_vl_sample_drawn_date  = new Date($("#hepc_vl_sample_drawn_date").val());

			if(hepc_sample_transport_date < hepc_vl_sample_drawn_date)
			{
				$("#modal_header").text("Hep-C Sample Transport Date Cannot be earlier than Sample Drawn Date");
				$("#modal_text").text("Please check dates");
				$("#hepc_sample_transport_date").val('');
				$("#multipurpose_modal").modal("show");
				return false;
			}
		}
		if($("#hepc_sample_receipt_date").val() != '')
		{
			var hepc_sample_receipt_date  = new Date($("#hepc_sample_receipt_date").val());

			if(hepc_sample_transport_date > hepc_sample_receipt_date)
			{
				$("#modal_header").text("Hep-C Sample Receipt Date Cannot be than Sample Transport Date");
				$("#modal_text").text("Please check dates");
				$("#hepc_sample_receipt_date").val('');
				$("#multipurpose_modal").modal("show");
				return false;
			}
		}
		if($("#hepc_vl_result_date").val() != '')
		{
			var hepc_vl_result_date  = new Date($("#hepc_vl_result_date").val());

			if(hepc_sample_transport_date > hepc_vl_result_date)
			{
				$("#modal_header").text("Hep-C  Test Result Date Cannot be before  than Sample Transport Date");
				$("#modal_text").text("Please check dates");
				$("#hepc_vl_result_date").val('');
				$("#multipurpose_modal").modal("show");
				return false;
			}
		}
	}

	if($("#hepc_sample_receipt_date").val() != '')
	{
		var hepc_sample_receipt_date  = new Date($("#hepc_sample_receipt_date").val());

		if($("#hepc_vl_sample_drawn_date").val() != '')
		{
			var hepc_vl_sample_drawn_date  = new Date($("#hepc_vl_sample_drawn_date").val());

			if(hepc_sample_receipt_date < hepc_vl_sample_drawn_date)
			{
				$("#modal_header").text("Hep-C Sample Receipt Date Cannot be ealier than Sample Drawn Date");
				$("#modal_text").text("Please check dates");
				$("#hepc_sample_receipt_date").val('');
				$("#multipurpose_modal").modal("show");
				return false;
			}
		}
		if($("#hepc_sample_transport_date").val() != '')
		{
			var hepc_sample_transport_date  = new Date($("#hepc_sample_transport_date").val());

			if(hepc_sample_receipt_date < hepc_sample_transport_date)
			{
				$("#modal_header").text("Hep-C Sample Receipt Date Cannot be later than Sample Transport Date");
				$("#modal_text").text("Please check dates");
				$("#hepc_sample_transport_date").val('');
				$("#multipurpose_modal").modal("show");
				return false;
			}
		}
		if($("#hepc_vl_result_date").val() != '')
		{
			var hepc_vl_result_date  = new Date($("#hepc_vl_result_date").val());

			if(hepc_sample_receipt_date > hepc_vl_result_date)
			{
				$("#modal_header").text("Hep-C Sample Receipt Date Cannot be later than Test Result Date");
				$("#modal_text").text("Please check dates");
				$("#hepc_vl_result_date").val('');
				$("#multipurpose_modal").modal("show");
				return false;
			}
		}
	}

	if($("#hepc_vl_result_date").val() != '')
	{
		var hepc_vl_result_date  = new Date($("#hepc_vl_result_date").val());

		if($("#hepc_vl_sample_drawn_date").val() != '')
		{
			var hepc_vl_sample_drawn_date  = new Date($("#hepc_vl_sample_drawn_date").val());

			if(hepc_vl_result_date < hepc_vl_sample_drawn_date)
			{
				$("#modal_header").text("Hep-C Test Result Date Cannot be earlier than Sample Drawn Date");
				$("#modal_text").text("Please check dates");
				$("#hepc_vl_result_date").val('');
				$("#multipurpose_modal").modal("show");
				return false;
			}
		}
		if($("#hepc_sample_transport_date").val() != '')
		{
			var hepc_sample_transport_date  = new Date($("#hepc_sample_transport_date").val());

			if(hepc_vl_result_date < hepc_sample_transport_date)
			{
				$("#modal_header").text("Hep-C Test Result Date Cannot be earlier than Sample Transport Date");
				$("#modal_text").text("Please check dates");
				$("#hepc_sample_transport_date").val('');
				$("#multipurpose_modal").modal("show");
				return false;
			}
		}
		if($("#hepc_sample_receipt_date").val() != '')
		{
			var hepc_sample_receipt_date  = new Date($("#hepc_sample_receipt_date").val());

			if(hepc_vl_result_date < hepc_sample_receipt_date)
			{
				$("#modal_header").text("Hep-C Test Result Date Cannot be earlier than Sample Receipt Date");
				$("#modal_text").text("Please check dates");
				$("#hepc_sample_receipt_date").val('');
				$("#multipurpose_modal").modal("show");
				return false;
			}
		}
	}

	return true;
}

function check_hepb_dates()
{
	if($("#hepb_vl_sample_drawn_date").val() != '')
	{
		var hepb_vl_sample_drawn_date  = new Date($("#hepb_vl_sample_drawn_date").val());

		if($("#hepb_sample_transport_date").val() != '')
		{
			var hepb_sample_transport_date  = new Date($("#hepb_sample_transport_date").val());

			if(hepb_vl_sample_drawn_date > hepb_sample_transport_date)
			{
				$("#modal_header").text("Hep-B Sample Drawn Date Cannot be later than Sample Transport Date");
				$("#modal_text").text("Please check dates");
				$("#multipurpose_modal").modal("show");
				$("#hepb_sample_transport_date").val('');
				return false;
			}
		}
		if($("#hepb_sample_receipt_date").val() != '')
		{
			var hepb_sample_receipt_date  = new Date($("#hepb_sample_receipt_date").val());

			if(hepb_vl_sample_drawn_date > hepb_sample_transport_date)
			{
				$("#modal_header").text("Hep-B Sample Transport Date Cannot be later than Sample Receipt Date");
				$("#modal_text").text("Please check dates");
				$("#multipurpose_modal").modal("show");
				$("#hepb_sample_receipt_date").val('');
				return false;
			}
		}
		if($("#hepb_vl_result_date").val() != '')
		{
			var hepb_vl_result_date  = new Date($("#hepb_vl_result_date").val());

			if(hepb_vl_sample_drawn_date > hepb_vl_result_date)
			{
				$("#modal_header").text("Hep-B Sample Drawn Date Cannot be later than Test Result Date");
				$("#modal_text").text("Please check dates");
				$("#multipurpose_modal").modal("show");
				$("#hepb_vl_result_date").val('');
				return false;
			}
		}
	}

	if($("#hepb_sample_transport_date").val() != '')
	{
		var hepb_sample_transport_date  = new Date($("#hepb_sample_transport_date").val());

		if($("#hepb_vl_sample_drawn_date").val() != '')
		{
			var hepb_vl_sample_drawn_date  = new Date($("#hepb_vl_sample_drawn_date").val());

			if(hepb_sample_transport_date < hepb_vl_sample_drawn_date)
			{
				$("#modal_header").text("Hep-B Sample Transport Date Cannot be earlier than Sample Drawn Date");
				$("#modal_text").text("Please check dates");
				$("#multipurpose_modal").modal("show");
				$("#hepb_vl_sample_drawn_date").val('');
				return false;
			}
		}
		if($("#hepb_sample_receipt_date").val() != '')
		{
			var hepb_sample_receipt_date  = new Date($("#hepb_sample_receipt_date").val());

			if(hepb_sample_transport_date > hepb_sample_receipt_date)
			{
				$("#modal_header").text("Hep-B Sample Transport Date Cannot be later than Sample Receipt Date");
				$("#modal_text").text("Please check dates");
				$("#multipurpose_modal").modal("show");
				$("#hepb_sample_receipt_date").val('');
				return false;
			}
		}
		if($("#hepb_vl_result_date").val() != '')
		{
			var hepb_vl_result_date  = new Date($("#hepb_vl_result_date").val());

			if(hepb_sample_transport_date > hepb_vl_result_date)
			{
				$("#modal_header").text("Hep-B Sample Transport Date Cannot be later than Test Result Date");
				$("#modal_text").text("Please check dates");
				$("#multipurpose_modal").modal("show");
				$("#hepb_vl_result_date").val('');
				return false;
			}
		}
	}

	if($("#hepb_sample_receipt_date").val() != '')
	{
		var hepb_sample_receipt_date  = new Date($("#hepb_sample_receipt_date").val());

		if($("#hepb_vl_sample_drawn_date").val() != '')
		{
			var hepb_vl_sample_drawn_date  = new Date($("#hepb_vl_sample_drawn_date").val());

			if(hepb_sample_receipt_date < hepb_vl_sample_drawn_date)
			{
				$("#modal_header").text("Hep-B Sample Receipt Date Cannot be ealier than Sample Drawn Date");
				$("#modal_text").text("Please check dates");
				$("#multipurpose_modal").modal("show");
				$("#hepb_vl_sample_drawn_date").val('');
				return false;
			}
		}
		if($("#hepb_sample_transport_date").val() != '')
		{
			var hepb_sample_transport_date  = new Date($("#hepb_sample_transport_date").val());

			if(hepb_sample_receipt_date < hepb_sample_transport_date)
			{
				$("#modal_header").text("Hep-B Sample Receipt Date Cannot be later than Sample Transport Date");
				$("#modal_text").text("Please check dates");
				$("#multipurpose_modal").modal("show");
				$("#hepb_sample_transport_date").val('');
				return false;
			}
		}
		if($("#hepb_vl_result_date").val() != '')
		{
			var hepb_vl_result_date  = new Date($("#hepb_vl_result_date").val());

			if(hepb_sample_receipt_date > hepb_vl_result_date)
			{
				$("#modal_header").text("Hep-B Sample Receipt Date Cannot be later than Test Result Date");
				$("#modal_text").text("Please check dates");
				$("#multipurpose_modal").modal("show");
				$("#hepb_vl_result_date").val('');
				return false;
			}
		}
	}

	if($("#hepb_vl_result_date").val() != '')
	{
		var hepb_vl_result_date  = new Date($("#hepb_vl_result_date").val());

		if($("#hepb_vl_sample_drawn_date").val() != '')
		{
			var hepb_vl_sample_drawn_date  = new Date($("#hepb_vl_sample_drawn_date").val());

			if(hepb_vl_result_date < hepb_vl_sample_drawn_date)
			{
				$("#modal_header").text("Hep-B Test Result Date Cannot be earlier than Sample Drawn Date");
				$("#modal_text").text("Please check dates");
				$("#multipurpose_modal").modal("show");
				$("#hepb_vl_sample_drawn_date").val('');
				return false;
			}
		}
		if($("#hepb_sample_transport_date").val() != '')
		{
			var hepb_sample_transport_date  = new Date($("#hepb_sample_transport_date").val());

			if(hepb_vl_result_date < hepb_sample_transport_date)
			{
				$("#modal_header").text("Hep-B Test Result Date Cannot be earlier than Sample Transport Date");
				$("#modal_text").text("Please check dates");
				$("#multipurpose_modal").modal("show");
				$("#hepb_sample_transport_date").val('');
				return false;
			}
		}
		if($("#hepb_sample_receipt_date").val() != '')
		{
			var hepb_sample_receipt_date  = new Date($("#hepb_sample_receipt_date").val());

			if(hepb_vl_result_date < hepb_sample_receipt_date)
			{
				$("#modal_header").text("Hep-B Test Result Date Cannot be earlier than Sample Receipt Date");
				$("#modal_text").text("Please check dates");
				$("#multipurpose_modal").modal("show");
				$("#hepb_sample_receipt_date").val('');
				return false;
			}
		}
	}

	return true;
}

</script>
<?php } else { ?>
	<script>
		$(document).ready(function(){
			$('input').prop('disabled', true);
			$('select').prop('disabled', true);
			$('textarea').prop('disabled', true);
		});



	</script>
<?php } ?>

<script>
	function onlyNumbersWithDot(e) {           
            var charCode;
            if (e.keyCode > 0) {
                charCode = e.which || e.keyCode;
            }
            else if (typeof (e.charCode) != "undefined") {
                charCode = e.which || e.keyCode;
            }
            if (charCode == 46)
                return true
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

$("#hepc_sample_storage_temp" ).change(function( event ) {
	
var hepc_sample_storage_temp = $('#hepc_sample_storage_temp').val();

	if(hepc_sample_storage_temp >100){

					$("#modal_header").text("Sample Storage Temperature (°C) Less than or equal to 100 to -100");
					$("#modal_text").text("Please check Sample Storage Temperature (°C)");
					$('#hepc_sample_storage_temp').val(0);
					$("#multipurpose_modal").modal("show");
					return false;

		
	}

	if(hepc_sample_storage_temp < -100){

					$("#modal_header").text("Sample Storage Temperature (°C) Less than or equal to 100 to -100");
					$("#modal_text").text("Please check Sample Storage Temperature (°C)");
					$('#hepc_sample_storage_temp').val(0);
					$("#multipurpose_modal").modal("show");
					return false;

		
	}

/* if( hepc_sample_storage_temp > '-100') {
	//alert('less than or equal to -100');
	$('#hepc_sample_storage_temp').val(0);
}*/

});

	$("#hepc_sample_stored_duration" ).change(function( event ) {
				
				var hepc_sample_storedcheck = $('#hepc_sample_stored_duration_more_less_than_day').val();
				var hepc_sample_stored_duration = $('#hepc_sample_stored_duration').val();
				if(hepc_sample_storedcheck ==1 && hepc_sample_stored_duration >24){
				 

					$("#modal_header").text("Duration Less than or equal to 24 (in hours)");
					$("#modal_text").text("Please check Duration(in hours)");
					$('#hepc_sample_stored_duration').val(12);
					$("#multipurpose_modal").modal("show");
					return false;

	}

});

		$("#hepc_sample_stored_duration_more_less_than_day" ).change(function( event ) {
				$('#hepc_sample_stored_duration').val('');
				var hepc_sample_storedcheck = $('#hepc_sample_stored_duration_more_less_than_day').val();
				var hepc_sample_stored_duration = $('#hepc_sample_stored_duration').val();
				if(hepc_sample_storedcheck ==1 && hepc_sample_stored_duration >24){
				 

					$("#modal_header").text("Duration Less than or equal to 24 (in hours)");
					$("#modal_text").text("Please check Duration(in hours)");
					$('#hepc_sample_stored_duration').val(12);
					$("#multipurpose_modal").modal("show");
					return false;

	}

});


	$("#hepc_is_sample_stored" ).change(function( event ) {

		if($("#hepc_is_sample_stored" ).val() ==2){
		$('#hepc_sample_storage_temp').val('');
		$('#hepc_sample_stored_duration').val('');
		$('#hepc_sample_stored_duration_more_less_than_day').val('');
		}

	});



$("#hepc_sample_transport_temp" ).change(function( event ) {
	
var hepc_sample_transport_temp = $('#hepc_sample_transport_temp').val();

	if(hepc_sample_transport_temp >100){

					$("#modal_header").text("Sample Transport Temperature (°C) Less than or equal to 100 to -100");
					$("#modal_text").text("Please check Sample Storage Temperature (°C)");
					$('#hepc_sample_transport_temp').val(0);
					$("#multipurpose_modal").modal("show");
					return false;

		
	}

if(hepc_sample_transport_temp < -100){

					$("#modal_header").text("Sample Transport Temperature (°C) Less than or equal to 100 to -100");
					$("#modal_text").text("Please check Sample Storage Temperature (°C)");
					$('#hepc_sample_transport_temp').val(0);
					$("#multipurpose_modal").modal("show");
					return false;		
	}
/* if( hepc_sample_storage_temp > '-100') {
	//alert('less than or equal to -100');
	$('#hepc_sample_storage_temp').val(0);
}*/

});


//helpb

	$("#hepb_is_sample_stored" ).change(function( event ) {

		if($("#hepb_is_sample_stored" ).val() ==2){
		$('#hepb_sample_storage_temp').val('');
		$('#hepb_sample_stored_duration').val('');
		$('#hepb_sample_stored_duration_more_less_than_day').val('');
		}

	});



$("#hepb_sample_transport_temp" ).change(function( event ) {
	
var hepb_sample_transport_temp = $('#hepb_sample_transport_temp').val();

	if(hepb_sample_transport_temp >100){

					$("#modal_header").text("Sample Transport Temperature (°C) Less than or equal to 100 to -100");
					$("#modal_text").text("Please check Sample Storage Temperature (°C)");
					$('#hepb_sample_transport_temp').val(0);
					$("#multipurpose_modal").modal("show");
					return false;

		
	}

if(hepb_sample_transport_temp < -100){

					$("#modal_header").text("Sample Transport Temperature (°C) Less than or equal to 100 to -100");
					$("#modal_text").text("Please check Sample Storage Temperature (°C)");
					$('#hepb_sample_transport_temp').val(0);
					$("#multipurpose_modal").modal("show");
					return false;		
	}
/* if( hepc_sample_storage_temp > '-100') {
	//alert('less than or equal to -100');
	$('#hepc_sample_storage_temp').val(0);
}*/

});

$("#hepc_vl_sample_drawn_date" ).change(function( event ) {

var screening_date = '<?php echo $patient_data[0]->T_DLL_01_Date; ?>';
//alert(screening_date);
var hepc_vl_sample_drawn_date = $("#hepc_vl_sample_drawn_date" ).val();
if(screening_date > hepc_vl_sample_drawn_date){

	$("#modal_header").text("Sample Drawn On Date  greater than or equal to Screening Details Min Date");
					$("#modal_text").text("Please check dates");
					$("#hepc_vl_sample_drawn_date" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

});

//helpb
$("#hepb_vl_sample_drawn_date" ).change(function( event ) {

var screening_date = '<?php echo $patient_data[0]->T_DLL_01_Date; ?>';
//alert(screening_date);
var hepb_vl_sample_drawn_date = $("#hepb_vl_sample_drawn_date" ).val();
if(screening_date > hepb_vl_sample_drawn_date){

	$("#modal_header").text("Sample Drawn On Date  greater than or equal to Screening Details Min Date");
					$("#modal_text").text("Please check dates");
					$("#hepb_vl_sample_drawn_date" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

});	

$("#hepc_is_sample_transported" ).change(function( event ) {

		if($("#hepc_is_sample_transported" ).val() ==2){
		$('#hepc_sample_transport_temp').val('');
		$('#hepc_sample_transport_date').val('');
		$('#hepc_sample_transported_to').val('');
		$('#hepc_sample_transported_to_other').val('');
		$('#hepc_sample_transported_to_name').val('');
		$('#hepc_sample_transported_to_designation').val('');
		}

	});
//helpb
$("#hepb_is_sample_transported" ).change(function( event ) {

		if($("#hepb_is_sample_transported" ).val() ==2){
		$('#hepb_sample_transport_temp').val('');
		$('#hepb_sample_transport_date').val('');
		$('#hepb_sample_transported_to').val('');
		$('#hepb_sample_transported_to_other').val('');
		$('#hepb_sample_transported_to_name').val('');
		$('#hepb_sample_transported_to_designation').val('');
		}

	});

$("#hepc_is_sample_accepted" ).change(function( event ) {

		if($("#hepc_is_sample_accepted" ).val() ==2){
		$('#hepc_vl_result_date').val('');
		$('#input_fields form-control').val('');
		$('#hepc_vl_reenter').val('');
		$('#hepc_vl_result').val('');
		$('#hepc_vl').val('');
		}

	});
//helpb
$("#hepb_is_sample_accepted" ).change(function( event ) {

		if($("#hepb_is_sample_accepted" ).val() ==2){
		$('#hepb_vl_result_date').val('');
		$('#input_fields form-control').val('');
		$('#hepb_vl_reenter').val('');
		$('#hepb_vl_result').val('');
		$('#hepb_vl').val('');
		}

	});

$('.messagedata').keypress(function (e) {
        var regex = new RegExp(/^[a-zA-Z\s]+$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            return false;
        }
    });


$("#patient_viral_load_form").submit(function(e){

		

			if($("#check_hepb").is(":checked") && $("#hepb_vl_sample_drawn_date").val()=="")
			{
				$("#modal_header").text("HEP-B Viral Load - Sample Collection");
				$("#modal_text").text("Please fill details before submitting");
				$("#multipurpose_modal").modal("show");
				e.preventDefault();
				return false;
			}


 });			

$("#hepb_sample_storage_temp" ).change(function( event ) {
	
var hepb_sample_storage_temp = $('#hepb_sample_storage_temp').val();

	if(hepb_sample_storage_temp >100){

					$("#modal_header").text("Sample Storage Temperature (°C) Less than or equal to 100 to -100");
					$("#modal_text").text("Please check Sample Storage Temperature (°C)");
					$('#hepb_sample_storage_temp').val('');
					$("#multipurpose_modal").modal("show");
					return false;

		
	}

	if(hepb_sample_storage_temp < -100){

					$("#modal_header").text("Sample Storage Temperature (°C) Less than or equal to 100 to -100");
					$("#modal_text").text("Please check Sample Storage Temperature (°C)");
					$('#hepb_sample_storage_temp').val('');
					$("#multipurpose_modal").modal("show");
					return false;

		
	}

/* if( hepc_sample_storage_temp > '-100') {
	//alert('less than or equal to -100');
	$('#hepc_sample_storage_temp').val(0);
}*/

});

$("#hepc_sample_stored_duration" ).change(function( event ) {
				
				var hepc_sample_storedcheck = $('#hepc_sample_stored_duration_more_less_than_day').val();
				var hepc_sample_stored_duration = $('#hepc_sample_stored_duration').val();
				if(hepc_sample_storedcheck ==1 && hepc_sample_stored_duration >24){
				 

					$("#modal_header").text("Duration Less than or equal to 24 (in hours)");
					$("#modal_text").text("Please check Duration(in hours)");
					$('#hepc_sample_stored_duration').val(12);
					$("#multipurpose_modal").modal("show");
					return false;

	}

});

		$("#hepb_sample_stored_duration" ).change(function( event ) {
				
				var hepc_sample_storedcheck = $('#hepb_sample_stored_duration_more_less_than_day').val();
				var hepc_sample_stored_duration = $('#hepb_sample_stored_duration').val();
				if(hepc_sample_storedcheck == 1 && hepc_sample_stored_duration >24){
				 

					$("#modal_header").text("Duration Less than or equal to 24 (in hours)");
					$("#modal_text").text("Please check Duration(in hours)");
					$('#hepb_sample_stored_duration').val(12);
					$("#multipurpose_modal").modal("show");
					return false;

	}

});


		$("#hepb_sample_stored_duration_more_less_than_day" ).change(function( event ) {
				$('#hepb_sample_stored_duration').val('');
				var hepc_sample_storedcheck = $('#hepb_sample_stored_duration_more_less_than_day').val();
				var hepc_sample_stored_duration = $('#hepb_sample_stored_duration').val();
				if(hepc_sample_storedcheck ==1 && hepc_sample_stored_duration >24){
				 

					$("#modal_header").text("Duration Less than or equal to 24 (in hours)");
					$("#modal_text").text("Please check Duration(in hours)");
					$('#hepb_sample_stored_duration').val(12);
					$("#multipurpose_modal").modal("show");
					return false;

	}

});



$("#hepb_sample_transport_temp" ).change(function( event ) {
	
var hepb_sample_transport_temp = $('#hepb_sample_transport_temp').val();

	if(hepb_sample_transport_temp >100){

					$("#modal_header").text("Sample Transport Temperature (°C) Less than or equal to 100 to -100");
					$("#modal_text").text("Please check Sample Storage Temperature (°C)");
					$('#hepb_sample_transport_temp').val('');
					$("#multipurpose_modal").modal("show");
					return false;

		
	}

	if(hepb_sample_transport_temp < -100){

					$("#modal_header").text("Sample Transport Temperature (°C) Less than or equal to 100 to -100");
					$("#modal_text").text("Please check Sample Storage Temperature (°C)");
					$('#hepb_sample_transport_temp').val('');
					$("#multipurpose_modal").modal("show");
					return false;

		
	}



});


$("#hepc_vl_sample_drawn_date" ).change(function( event ) {

var date_of_prescribing_tests = $("#date_of_prescribing_tests" ).val();
//alert(screening_date);
var last_test_result_date = $("#last_test_result_date" ).val();
if(date_of_prescribing_tests > last_test_result_date){

	$("#modal_header").text("Date of Last Test Result  greater than or equal to Date of Prescribing Tests");
					$("#modal_text").text("Please check dates");
					$("#last_test_result_date" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

});

/*Max Date Today Code*/
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!

			var yyyy = today.getFullYear();
			 if(dd<10){
			        dd='0'+dd;
			    } 
			    if(mm<10){
			        mm='0'+mm;	
			    }

			today = yyyy+'-'+mm+'-'+dd;
			document.getElementById("hepc_vl_sample_drawn_date").setAttribute("max", today);
			document.getElementById("hepc_sample_transport_date").setAttribute("max", today);
			document.getElementById("hepc_sample_receipt_date").setAttribute("max", today);
			document.getElementById("hepc_vl_result_date").setAttribute("max", today);
			document.getElementById("hepb_vl_sample_drawn_date").setAttribute("max", today); 
			document.getElementById("hepb_sample_transport_date").setAttribute("max", today); 


	$('#creenterrejection').click(function( event ) {
		//alert('ddd');
		$('#hepc_vl_sample_drawn_date').val('');
		$('#hepc_is_sample_stored').val('');
		$('#hepc_sample_storage_temp').val('');
		$('#hepc_sample_stored_duration_more_less_than_day').val('');
		$('#hepc_sample_stored_duration').val('');
		$('#hepc_is_sample_transported').val('');

		$('#hepc_sample_transport_temp').val('');
		$('#hepc_sample_transport_date').val('');
		$('#hepc_sample_transported_to').val('');
		$('#hepc_sample_transported_to_other').val('');
		$('#hepc_sample_transported_to_name').val('');
		$('#hepc_sample_transported_to_designation').val('');

		$('#hepc_sample_remarks').val('');
		$('#hepc_sample_receipt_date').val('');
		$('#hepc_sample_received_by_name').val('');
		$('#hepc_sample_received_by_designation').val('');
		$('#hepc_is_sample_accepted').val('');
		$('#hepc_vl_result_date').val('');
		$('#hepc_vl').val('');
		$('#hepc_vl_reenter').val('');
		$('#hepc_vl_result').val('');
		$('#hepc_sample_reason_for_rejection').val('');
		
		$('#hepc_vl_remarks').val('');
		$('#hepb_vl_sample_drawn_date').val('');
		$('#hepb_is_sample_stored').val('');
		$('#hepb_sample_storage_temp').val('');
		$('#hepb_sample_stored_duration_more_less_than_day').val('');
		$('#hepb_sample_stored_duration').val('');


		$('#hepc_vl_sample_drawn_date').attr("readonly", false);
		$('#hepc_is_sample_stored').attr("readonly", false);
		$('#hepc_sample_storage_temp').attr("readonly",false);
		$('#hepc_sample_stored_duration_more_less_than_day').attr("readonly", false);
		$('#hepc_sample_stored_duration').attr("readonly", false);
		$('#hepc_is_sample_transported').attr("readonly", false);

		$('#hepc_sample_transport_temp').attr("readonly", false);
		$('#hepc_sample_transport_date').attr("readonly", false);
		$('#hepc_sample_transported_to').attr("readonly", false);
		$('#hepc_sample_transported_to_other').attr("readonly", false);
		$('#hepc_sample_transported_to_name').attr("readonly", false);
		$('#hepc_sample_transported_to_designation').attr("readonly", false);

		$('#hepc_sample_remarks').attr("readonly", false);
		$('#hepc_sample_receipt_date').attr("readonly", false);
		$('#hepc_sample_received_by_name').attr("readonly", false);
		$('#hepc_sample_received_by_designation').attr("readonly",false);


	})		


//help b vl 
	$('#breenterrejection').click(function( event ) {

		$('#hepb_vl_sample_drawn_date').val('');
		$('#hepb_is_sample_stored').val('');
		$('#hepb_sample_storage_temp').val('');
		$('#hepb_sample_stored_duration_more_less_than_day').val('');
		$('#hepb_sample_stored_duration').val('');
		$('#hepb_is_sample_transported').val('');

		$('#hepb_sample_transport_temp').val('');
		$('#hepb_sample_transport_date').val('');
		$('#hepb_sample_transported_to').val('');
		$('#hepb_sample_transported_to_name').val('');
		$('#hepb_sample_transported_to_designation').val('');
		$('#hepb_sample_remarks').val('');

		$('#hepb_sample_receipt_date').val('');
		$('#hepb_sample_received_by_name').val('');
		$('#hepb_sample_received_by_designation').val('');
		$('#hepb_is_sample_accepted').val('');
		$('#hepb_vl_result_date').val('');
		$('#hepb_vl').val('');
		$('#hepb_vl_reenter').val('');
		$('#hepb_vl_result').val('');
		$('#hepb_vl_remarks').val('');
		$('#hepb_sample_reason_for_rejection').val('');

		$('#hepb_vl_sample_drawn_date').attr("readonly", false);
		$('#hepb_is_sample_stored').attr("readonly", false);
		$('#hepb_sample_storage_temp').attr("readonly",false);
		$('#hepb_sample_stored_duration_more_less_than_day').attr("readonly", false);
		$('#hepb_sample_stored_duration').attr("readonly", false);
		$('#hepb_is_sample_transported').attr("readonly", false);

		$('#hepb_sample_transport_temp').attr("readonly", false);
		$('#hepb_sample_transport_date').attr("readonly", false);
		$('#hepb_sample_transported_to').attr("readonly", false);
		$('#hepb_sample_transported_to_other').attr("readonly", false);
		$('#hepb_sample_transported_to_name').attr("readonly", false);
		$('#hepb_sample_transported_to_designation').attr("readonly", false);

		$('#hepb_sample_remarks').attr("readonly", false);
		$('#hepb_sample_receipt_date').attr("readonly", false);
		$('#hepb_sample_received_by_name').attr("readonly", false);
		$('#hepb_sample_received_by_designation').attr("readonly",false);
	})

$("#hepc_vl_reenter" ).change(function( event ) {

	var hepc_vl = $("#hepc_vl").val();
	var hepc_vl_reenter = $("#hepc_vl_reenter").val();

	if(hepc_vl != hepc_vl_reenter){

	$("#modal_header").text("Viral Load does not match re viral load");
					$("#modal_text").text("Please check dates");
					$("#hepc_vl_reenter" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}


});

//help b vl 

$("#hepb_vl" ).change(function( event ) {

$("#hepb_vl_reenter" ).val('');
});

$("#hepb_vl_reenter" ).change(function( event ) {

	var hepb_vl = $("#hepb_vl").val();
	var hepb_vl_reenter = $("#hepb_vl_reenter").val();

	if(hepb_vl != hepb_vl_reenter){

	$("#modal_header").text("Viral Load does not match re viral load");
					$("#modal_text").text("Please check dates");
					$("#hepb_vl_reenter" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}


});

$("#hepb_vl" ).change(function( event ) {

$("#hepb_vl_reenter" ).val('');
});

 $("form").submit(function(){
           

if($('#hepc_vl_result').val()=="" && $('#hepc_vl_result_date').val()==''){

	$("#modal_header").text("Data saved partially");
					$("#modal_text").text("Data saved partially");
					$("#multipurpose_modal").modal("show");
					//return false;
}	



});
$('#save').click(function( event ) {

 if($('#hepc_vl_result_date').val()!=''){
				$('#hepc_vl_result_date').prop('required',true); 
					$('#hepc_vl').prop('required',true);
					$('#hepc_vl_reenter').prop('required',true);
					$('#hepc_vl_result').prop('required',true);
			}
});
/*
$('#save').click(function( event ) {

if( $("#hepc_is_sample_accepted").val() ==1 && $("#hepc_vl_result_date").val() =='' ){

	$("#modal_header").text("Please Enter Test Result Date");
					$("#modal_text").text("Please check dates");
					$("#multipurpose_modal").modal("show");
					return false;

}

if( $("#hepc_is_sample_accepted").val() ==1 && $("#hepc_vl").val() =='' ){

	$("#modal_header").text("Please Enter Viral Load");
					$("#modal_text").text("Please check dates");
					$("#multipurpose_modal").modal("show");
					return false;

}

if( $("#hepc_is_sample_accepted").val() ==1 && $("#hepc_vl_reenter").val() =='' ){

	$("#modal_header").text("Please Enter Re Viral Load");
					$("#modal_text").text("Please check dates");
					$("#multipurpose_modal").modal("show");
					return false;

}

if( $("#hepc_is_sample_accepted").val() ==1 && $("#hepc_vl_result").val() =='' ){

	$("#modal_header").text("Please Enter Result");
					$("#modal_text").text("Please check dates");
					$("#multipurpose_modal").modal("show");
					return false;

}


});*/
$(document).ready(function(){
<?php if(count($patient_data) > 0 && $patient_data[0]->IsSampleAccepted == 2) { ?>

var hepc_is_sample_accepted = $('#hepc_is_sample_accepted').val();

//alert(hepc_is_sample_accepted);

$('#hepc_vl_sample_drawn_date').attr("readonly", "true");
		$('#hepc_is_sample_stored').attr("readonly", "true");
		$('#hepc_sample_storage_temp').attr("readonly", "true");
		$('#hepc_sample_stored_duration_more_less_than_day').attr("readonly", "true");
		$('#hepc_sample_stored_duration').attr("readonly", "true");
		$('#hepc_is_sample_transported').attr("readonly", "true");

		$('#hepc_sample_transport_temp').attr("readonly", "true");
		$('#hepc_sample_transport_date').attr("readonly", "true");
		$('#hepc_sample_transported_to').attr("readonly", "true");
		$('#hepc_sample_transported_to_other').attr("readonly", "true");
		$('#hepc_sample_transported_to_name').attr("readonly", "true");
		$('#hepc_sample_transported_to_designation').attr("readonly", "true");

		$('#hepc_sample_remarks').attr("readonly", "true");
		$('#hepc_sample_receipt_date').attr("readonly", "true");
		$('#hepc_sample_received_by_name').attr("readonly", "true");
		$('#hepc_sample_received_by_designation').attr("readonly", "true");
	
		$("#modal_header").html('Further entry is not allowed, as per treatment status...');
		$("#modal_text").html('Sample Accepted');
		$("#multipurpose_modal").modal("show");
	/*var readonly_select = $('select');
	$(readonly_select).attr('readonly', true).attr('data-original-value', $(readonly_select).val()).on('change', function(i) {
	$(i.target).val($(this).attr('data-original-value'));
	});*/


	<?php  } ?>


	// $("#save").change(function(){
		<?php if(count($patient_data) > 0 && $patient_data[0]->IsBSampleAccepted == 2) { ?>
		$('#hepb_vl_sample_drawn_date').attr("readonly", "true");
		$('#hepb_is_sample_stored').attr("readonly", "true");
		$('#hepb_sample_storage_temp').attr("readonly", "true");
		$('#hepb_sample_stored_duration_more_less_than_day').attr("readonly", "true");
		$('#hepb_sample_stored_duration').attr("readonly", "true");
		$('#hepb_is_sample_transported').attr("readonly", "true");

		$('#hepb_sample_transport_temp').attr("readonly", "true");
		$('#hepb_sample_transport_date').attr("readonly", "true");
		$('#hepb_sample_transported_to').attr("readonly", "true");
		$('#hepb_sample_transported_to_other').attr("readonly", "true");
		$('#hepb_sample_transported_to_name').attr("readonly", "true");
		$('#hepb_sample_transported_to_designation').attr("readonly", "true");

		$('#hepb_sample_remarks').attr("readonly", "true");
		$('#hepb_sample_receipt_date').attr("readonly", "true");
		$('#hepb_sample_received_by_name').attr("readonly", "true");
		$('#hepb_sample_received_by_designation').attr("readonly", "true");
	// $("#modal_header").html('Further entry is not allowed, as per treatment status...');
<?php  } ?>

// });

$("#hepc_vl_result").change(function(){
	var hepc_vl_result = $('#hepc_vl_result').val();
	if(hepc_vl_result =='1'){

			$('#hepc_vl_result_date').prop('required',true); 
					$('#hepc_vl').prop('required',true);
					$('#hepc_vl_reenter').prop('required',true);
					$('#hepc_vl_result').prop('required',true);


	}else{

		$('#hepc_vl_result_date').prop('required',false); 
					$('#hepc_vl').prop('required',false);
					$('#hepc_vl_reenter').prop('required',false);
					$('#hepc_vl_result').prop('required',false);
	}


});

});
</script>
