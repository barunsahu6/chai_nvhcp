<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}
	
textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #A30A0C !important;
	background-color: white !important;
	border : 1px solid #A30A0C;
}
.auto {cursor: auto;}
</style>
 
<br/>
<br/>
<br/>
<?php  if(!empty($_GET['p'])){  if($_GET['p']==1) { ?>
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<a href="<?php echo base_url(); ?>patientinfo/patient_register" class="btn btn-block btn-success text-center" style="font-weight: 600;">NEW PATIENT</a>
	</div>
</div>

<br>
<div class="row">
	<div class="col-md-6 col-md-offset-3 text-center">
		<span style="font-weight: 800;font-size: 20px;position: absolute;margin-top: 6px; padding: 0 10px; background-color: white; margin-left: -23px;">OR</span>
		<hr>
	</div>
</div>
<?php  } } ?>
<!-- <form method="POST" name="search_form"> -->
				<?php
           $attributes = array(
              'name' => 'search_form',
               'autocomplete' => 'false',
            );
           echo form_open('patientinfo?p='.$_GET['p'], $attributes); ?>
<div class="row">

	<div class="col-md-2 col-md-offset-3">
		<br>
		<label for="">Search By</label>
		<select type="text" name="search_by" id="search_by" class="form-control input_fields" required>
			<option value="">Select</option>
			<option value="1" <?php if($this->input->post('search_by')==1) { echo 'selected';} ?>>UID/Contact No.</option>
			<option value="2" <?php if($this->input->post('search_by')==2) { echo 'selected';} ?>>Status</option>
		</select>
	</div>
	<div class="col-md-3 uid_field" style="display: none;">
		<br>
		<label for="uid">UID/Contact No.</label>
		<input type="text" class="form-control input_fields" name="uid_contact" id="uid_contact" value="<?php if(isset($_POST['uid_contact'])) { echo $_POST['uid_contact'];} ?>" >
	</div>
	<div class="col-md-3 status_field" style="display: none;">
		<br>
		<label for="uid">Status</label>
		<select type="text" name="search_status" id="search_status" class="form-control input_fields" >
			<option value="">Select Status</option>
			<?php foreach ($status as $row) { ?>
				<option value="<?php echo $row->LookupCode; ?>" <?php if($row->LookupCode==$this->input->post('search_status')) { echo 'selected';} ?>><?php echo $row->LookupValue; ?></option>
			<?php } ?>
		</select>
	</div>
	<div class="col-md-1" style="padding-left: 0;">
		<br>
		<label for="">&nbsp;</label>
		<button class="btn btn-block btn-success text-center" style="font-weight: 600; line-height: inherit; border-radius: 0;">SEARCH</button>
	</div>
</div>
		          <?php echo form_close(); ?>
<!-- </form> -->
<br>

<div class="row">
	<div class="col-md-6 col-md-offset-3 text-center">
		<p><kbd>Patient List</kbd></p>
	</div>
</div>
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		
		 <div class="text-right"><?php if(isset($offsetdata)) { echo ($offsetdata+1).'-'. (count($patient_list)+$offsetdata); } else { echo '0';} ?> <label for="total"> Of  <?php  if (isset($patient_listCount) ) echo ($patient_listCount); ?> records.</label></div>
		<table class="table table-striped table-bordered table-hover" id="table_patient_list" style="width:100%">

			
			<thead>
				<tr>
					<th style="width: 35%">UID</th>
					<th style="width: 40%">Name</th>
					<th style="width: 25%">Status</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(count($patient_list) > 0)
				{
					//echo "<pre>";
					//print_r($patient_list);exit;

					foreach ($patient_list as $patient) {
						?>
						<tr data-href="<?php echo base_url().'patientinfo/patient_redirect/'.$patient->PatientGUID; ?>">
							<td><?php echo str_pad($patient->UID_Num, 6, '0', STR_PAD_LEFT);  //echo $patient->UID_Prefix.'-'.str_pad($patient->UID_Num, 6, '0', STR_PAD_LEFT); ?></td>
							<td style="width: 50%;overflow: hidden;"><?php echo ($patient->FirstName); ?></td>
							<td><?php echo $patient->status; ?></td>
						</tr>
						<?php 
					}
				}

				else
				{
					?>
					<?php 	if($this->input->post('search_by')==1) { ?>
					<tr data-href="#">
						<td class="text-center auto" colspan=3 >No Patients in this UID/Contact No.</td>
					</tr>
				<?php } elseif($this->input->post('search_by')==2){ ?>
					<tr data-href="#">
						<td class="text-center auto" colspan=3>No Patients in this Status.</td>
					</tr>
				<?php } ?>
					<?php 
				}
				?>
			</tbody>
		</table>

<div class="text-left">
Showing <?php if (isset($patient_list) && is_array($patient_list)) echo count($patient_list); ?> results
</div> 

<div class="text-right">
<?php if (isset($patient_list) && count($patient_list) >0 && $patient_list!='' && is_array($patient_list)) echo $page_links; ?>  
</div> 

		
	</div>
</div>


<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>


<script type="text/javascript" src="<?php echo site_url('application');?>/third_party/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('application');?>/third_party/js/jquery.dataTables.min.js"></script>


<script>

		/*$(document).ready(function() {
		$('#table_patient_list').DataTable({
		"pageLength":50
		});
		} );*/

	$(document).ready(function(){

		$(".uid_field").hide();
		$(".status_field").hide();

		$('#table_patient_list tbody tr').click(function(){
			window.location = $(this).data('href');
		});

	<?php 	if($this->input->post('search_by')==1) { ?>
			$(".uid_field").show();
				$(".status_field").hide();
				$("#search_by").prop('required', true);
				$("#uid_contact").prop('required', true);
				$("#search_status").prop('required', false);
				$("#search_status").val('');

		<?php	}elseif($this->input->post('search_by')==2){ ?>

				$(".uid_field").hide();
				$(".status_field").show();
				$("#search_by").prop('required', false);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', true);
				$("#uid_contact").val('');
		<?php	} else { ?>

				$(".uid_field").hide();
				$(".status_field").hide();
				$("#search_by").prop('required', true);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', false);

		<?php } ?>

		$("#search_by").change(function(){

			if($(this).val() == 1)
			{
				$(".uid_field").show();
				$(".status_field").hide();
				$("#search_by").prop('required', true);
				$("#uid_contact").prop('required', true);
				$("#search_status").prop('required', false);
				$("#search_status").val('');
			}
			else if($(this).val() == 2)
			{
				$(".uid_field").hide();
				$(".status_field").show();
				$("#search_by").prop('required', false);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', true);
				$("#uid_contact").val('');
			}
			else
			{
				$(".uid_field").hide();
				$(".status_field").hide();
				$("#search_by").prop('required', true);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', false);
			}
		});
	});
</script>