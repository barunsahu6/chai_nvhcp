<?php  
class Hfm_Dashboard_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		// echo "<pre>"; print_r($this->session->userdata('filters')['id_mstfacility']); exit; 
		//pr($this->session->userdata('filters'));


		}
	public function get_mtc_tc_info_first_time_only(){

		$loginData = $this->session->userdata('loginData'); 

		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
		}

		$query = "SELECT Q.id_mststate,IFNULL(Q1.mtc_established,0) AS mtc_established ,IFNULL(Q2.tc_established,0) AS tc_established,Q3.districts,IFNULL(Q2.districts_with_tc,0)AS districts_with_tc FROM (select StateName,id_mststate from mststate group by id_mststate) as Q
		Left JOIN (SELECT COUNT(id_mstfacility) AS mtc_established,id_mststate,id_mstfacility from mstfacility WHERE is_Mtc=1 group by id_mststate) AS Q1
		ON Q.id_mststate=Q1.id_mststate 

		Left JOIN (SELECT COUNT(id_mstfacility) AS tc_established,COUNT(DISTINCT id_mstdistrict) AS districts_with_tc,id_mststate,id_mstdistrict,id_mstfacility from mstfacility WHERE is_Mtc=0 group by id_mststate) AS Q2
		ON Q.id_mststate=Q2.id_mststate

		Left JOIN (SELECT COUNT(id_mstdistrict) AS districts,id_mststate,id_mstdistrict from mstdistrict  group BY id_mststate) AS Q3
		ON Q.id_mststate=Q3.id_mststate
		ORDER BY Q.id_mststate " .$sess_where;

					     //print_r($query); die();

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}

public function get_hfm_report_data(){

		$loginData = $this->session->userdata('loginData'); 
		$filter=$this->session->userdata('hfm_filter');
		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
		}

		 $query = "SELECT * FROM (Select * from tbl_hfm_report where financial_year=?) as hfm
			left join
			(SELECT StateName,id_mststate from mststate) as st
			 on st.id_mststate=hfm.id_mststate ORDER BY st.StateName ASC ";

					     //print_r($query); die();

		$result = $this->db->query($query,[$filter['year']])->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}

public function hfm_report_summary($is_Mtc=NULL){

		$loginData = $this->session->userdata('loginData'); 
		$filter=$this->session->userdata('hfm_filter');
		if($is_Mtc==1){
	$is_Mtc_qry="ifnull(hfm_table.target_mtc,0) as target_mtc,ifnull(hfm_table.established_mtc,0) as established_mtc,ifnull(patient_table.facilty_with_patient_entry,0) as facilty_with_patient_entry";
			$hfm_table_col_sum="ifnull(SUM(target_mtc),0) AS target_mtc,ifnull(SUM(established_mtc),0) AS established_mtc";
			$num_district='';
		}
		elseif($is_Mtc==0){
		$is_Mtc_qry="ifnull(hfm_table.established_tc,0) as established_tc,ifnull(hfm_table.district_with_tc,0) as district_with_tc,ifnull(patient_table.facilty_with_patient_entry,0) as facilty_with_patient_entry,ifnull(district.districts,0) as district";
		$hfm_table_col_sum="ifnull(SUM(district_with_tc),0) AS district_with_tc,ifnull(SUM(tc_established),0) AS established_tc";
		$num_district='Left JOIN (SELECT COUNT(id_mstdistrict) AS districts,id_mststate,id_mstdistrict from mstdistrict group BY id_mststate) AS district
				ON st.id_mststate=district.id_mststate';
		}
		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
		}

	 $query = "SELECT st.StateName,".$is_Mtc_qry." FROM 
(SELECT StateName,id_mststate FROM mststate) AS st
LEFT JOIN 
(SELECT ".$hfm_table_col_sum.",id_mststate  FROM tbl_hfm_report GROUP BY id_mststate) AS hfm_table
ON st.id_mststate=hfm_table.id_mststate
LEFT JOIN 
(SELECT Count(Distinct p.id_mstfacility) AS facilty_with_patient_entry,f.id_mststate FROM mstfacility f LEFT JOIN tblpatient p ON p.id_mstfacility=f.id_mstfacility WHERE f.is_Mtc=".$is_Mtc." GROUP BY id_mststate) AS patient_table
ON hfm_table.id_mststate=patient_table.id_mststate ".$num_district." ORDER BY st.StateName ASC";

					     //print_r($query); die();

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}

public function filtered_hfm_report($is_Mtc=NULL){

		$loginData = $this->session->userdata('loginData'); 
		$filter=$this->session->userdata('hfm_search_filter');
		//pr($filter);exit();
		if($is_Mtc==1){
	$is_Mtc_qry="ifnull(hfm_table.target_mtc,0) as target_mtc,ifnull(patient_table.facilty_with_patient_entry,0) AS facilty_with_patient_entry,ifnull(f.established,0) as established";
			$hfm_table_col_sum="LEFT JOIN 
(SELECT ifnull(SUM(target_mtc),0) AS target_mtc,id_mststate FROM tbl_hfm_report WHERE financial_year>= '".$filter['year1']."' AND financial_year <= '".$filter['year2']."' GROUP BY id_mststate) AS hfm_table
ON st.id_mststate=hfm_table.id_mststate ";
			$num_district='';
			$districts_with_tc='';
		}
		elseif($is_Mtc==0){
		$is_Mtc_qry="ifnull(hfm_table.target_tc,0) as target_tc,ifnull(patient_table.facilty_with_patient_entry,0) AS facilty_with_patient_entry,ifnull(f.established,0) as established,ifnull(district.districts,0) as district,ifnull(f.district_with_tc,0) as district_with_tc";
		$hfm_table_col_sum="LEFT JOIN 
(SELECT ifnull(SUM(target_tc),0) AS target_tc,id_mststate FROM tbl_hfm_report WHERE financial_year>= '".$filter['year1']."' AND financial_year <= '".$filter['year2']."' GROUP BY id_mststate) AS hfm_table
ON st.id_mststate=hfm_table.id_mststate ";
		$num_district="Left JOIN (SELECT COUNT(id_mstdistrict) AS districts,id_mststate,id_mstdistrict from mstdistrict group BY id_mststate) AS district
				ON st.id_mststate=district.id_mststate";
				$districts_with_tc="COUNT(DISTINCT id_mstdistrict) AS district_with_tc,";
		}


		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
		}

		$query = "SELECT st.StateName,".$is_Mtc_qry." FROM 
(SELECT StateName,id_mststate FROM mststate) AS st
LEFT JOIN
(SELECT count(distinct id_mstfacility) as established,".$districts_with_tc."id_mststate FROM mstfacility WHERE (CASE when  ActiveFrom IS NULL then '2019-04-01' when ActiveFrom='0000-00-00' then '2019-04-01' ELSE ActiveFrom END) BETWEEN '".$filter['startdate']."' AND '".$filter['enddate']."' AND is_Mtc=".$is_Mtc." GROUP BY id_mststate) AS f
ON  st.id_mststate=f.id_mststate ".$hfm_table_col_sum."
 
LEFT JOIN 
(SELECT Count(Distinct p.id_mstfacility) AS facilty_with_patient_entry,f.id_mststate FROM mstfacility f LEFT JOIN tblpatient p ON p.id_mstfacility=f.id_mstfacility WHERE f.is_Mtc=".$is_Mtc." AND (CASE when p.CreatedOn IS NULL then '2019-04-01' when p.CreatedOn='0000-00-00' then '2019-04-01' ELSE p.CreatedOn END) BETWEEN '".$filter['startdate']."' AND '".$filter['enddate']."' GROUP BY id_mststate) AS patient_table
ON st.id_mststate=patient_table.id_mststate ".$num_district." ORDER BY st.StateName ASC";

					     //print_r($query); die();

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}	
public function hepatitis_treatment($flag=NULL)
	{	

		$loginData = $this->session->userdata('loginData'); 
		$filter=$this->session->userdata('hfm_search_filter');

		if ($flag!=NULL) {
			$date_filter="date between '".$filter['startdate']."' AND '".$filter['enddate']."'";
		}
		else{
			$date_filter="date between '2019-04-01' AND '".date('Y-m-d')."'";
		}
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}

			 $query = "SELECT * FROM 
			 (SELECT s.StateName,s.id_mststate FROM mststate s group BY id_mststate)AS st
			 LEFT JOIN
             (SELECT * FROM (SELECT ifnull(sum(initiatied_on_treatment),0) AS initiatied_on_treatment,ifnull(sum(treatment_successful),0) AS treatment_successful,Session_StateID 
             FROM tblsummary_new 
             WHERE ".$date_filter." GROUP BY Session_StateID) AS t)AS ts
ON st.id_mststate=ts.Session_StateID ORDER BY st.StateName ASC";

					     //print_r($query); die();

		//$result = $this->db->query($query)->result();
	   
		
		$result = $this->db->query($query)->result_array();
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}
}