<?php 
class Inventory_Model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
		// $this->file_path = realpath(APPPATH . '../datafiles');
		// $this->banner_path = realpath(APPPATH . '../banners');
		// $this->gallery_path_url = base_url().'datafiles/';
    }
   public function get_inventory_details($flag=NULL){

		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
		if($flag==NULL){
			$flag="AND i.flag='R'";
			$facility_id="source_name";
			$date_type="i.Entry_Date";
		}
		else if($flag=='R'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="source_name";
			$date_type="i.Entry_Date";
		}
		else if($flag=='T'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="transfer_to";
			$date_type="i.Entry_Date";
		}
		else if($flag=='U'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="transfer_to";
			$date_type="i.Entry_Date";
		}
		else if($flag=='W'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="transfer_to";
			$date_type="i.Entry_Date";
		}
		else if($flag=='I'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="id_mstfacility";
			$date_type="i.indent_date";

		}
		else if($flag=='F'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="id_mstfacility";
			$date_type="i.failure_date";

		}

		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		   $query = "SELECT i.*,f.facility_short_name,s.strength FROM `tbl_inventory` i left join mstfacility f on i.".$facility_id."=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength where i.is_deleted='0' AND  ".$date_type." BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND  ".$date_type." BETWEEN '".$date1."' AND '".$date2."' AND ".$itemname." ".$sess_where." ".$flag." ORDER BY id_mst_drugs";

					  /*print_r($query); die();*/

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
public function get_items($drug_id=NULL,$type=NULL){

				if ($drug_id=='' || $drug_id==NULL) {
				$drug_id= ' AND 1 ORDER BY d.id_mst_drugs';
				}
				else
				{
				$drug_id= " AND s.id_mst_drug_strength = ".$drug_id."";
		
				}
				if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
		 $query = "SELECT * FROM `mst_drugs` d left join `mst_drug_strength` s on d.id_mst_drugs=s.id_mst_drug where d.is_deleted=0 ".$type."".$drug_id;

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}

function delete_receipt($id=NULL)
{
	$loginData = $this->session->userdata('loginData'); 
   $this->db->where('inventory_id', $id);
   $data=array('is_deleted'=>'1',"updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
   $this->db->update('tbl_inventory',$data); 
}


public function edit_inventory_receipt($inventory_id=NULL){

		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
			}

		 $query = "SELECT * FROM `tbl_inventory` where is_deleted='0' AND inventory_id=".$inventory_id." ".$sess_where;

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
public function get_receipt_in_transfer_out($id_mst_drugs=NULL,$batch_num=NULL){
		
		//echo "///".$id_mst_drugs;
		if($id_mst_drugs==NULL)
		{
			$and="AND Acceptance_Status='1' GROUP BY id_mst_drugs";
			$and1="AND Acceptance_Status='1' GROUP BY id_mst_drugs";
			$where="a.Remaining>0";
		}
		elseif ($id_mst_drugs!=NULL && $batch_num!=NULL) {
			$and="AND i.id_mst_drugs='".$id_mst_drugs."' AND i.batch_num ='".$batch_num."' AND Acceptance_Status='1' GROUP BY batch_num,id_mst_drugs";
			$and1="AND id_mst_drugs='".$id_mst_drugs."' AND batch_num ='".$batch_num."' AND Acceptance_Status='1' GROUP BY batch_num,id_mst_drugs";
			$where="a.Remaining>0";
		}
		else{
			$and="AND i.id_mst_drugs='".$id_mst_drugs."' AND Acceptance_Status='1' GROUP BY batch_num,id_mst_drugs";
			$and1="AND id_mst_drugs='".$id_mst_drugs."' GROUP BY batch_num,id_mst_drugs";
			$where="a.Remaining>0";
		}
		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_wherep = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "i.id_mststate = '".$loginData->State_ID."'";
				$sess_wherep = "id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "i.id_mststate = '".$loginData->State_ID."' ";
				$sess_wherep = "id_mststate = '".$loginData->State_ID."' ";
			}

	$query = "SELECT * from (SELECT (t1.cnt)-(ifnull(t2.cnt,0)) AS remaining,t1.batch_num
from (SELECT ifnull(SUM(quantity),0) AS cnt,batch_num,id_mst_drugs FROM tbl_inventory WHERE flag ='R' AND is_deleted='0' AND Acceptance_Status='1'AND ".$sess_wherep." AND flag='R' ".$and1.") as t1
left join (SELECT ifnull(SUM(quantity),0) AS cnt,batch_num,id_mst_drugs FROM tbl_inventory WHERE flag IN('T','U','W') AND is_deleted='0' AND ".$sess_wherep." ".$and1.") as t2 ON t1.batch_num=t2.batch_num) a
  LEFT JOIN 
   (SELECT i.id_mst_drugs,i.drug_name,Expiry_Date,i.type,i.batch_num,s.strength,n.drug_name as name FROM `tbl_inventory` i inner join `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs  where i.is_deleted='0' AND Acceptance_Status='1' AND ".$sess_where." AND flag='R' ".$and.") b
   ON a.batch_num=b.batch_num WHERE ".$where." AND b.Expiry_Date>=CURDATE()";

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}	
function get_all_facilities(){
	$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}

$query = "SELECT id_mstfacility,facility_short_name FROM `mstfacility` where ".$sess_where;

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}	
public function Remaining_stock($batch_num=NULL,$type=NULL){
		if($type==1){
			$quantity='quantity';
		}
		elseif($type==2){
			$quantity='quantity_screening_tests';
		}
		else{
			$quantity='quantity';
		}
		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
			}

		 $query = "SELECT ((SELECT ifnull(SUM(".$quantity."),0) FROM tbl_inventory WHERE flag ='R' AND is_deleted='0' AND Acceptance_Status='1' AND batch_num='".$batch_num."' ".$sess_where.") - (SELECT ifnull(SUM(".$quantity."),0) FROM tbl_inventory WHERE flag IN('T','U','W') AND is_deleted='0' AND batch_num='".$batch_num."' ".$sess_where.")) AS Remaining";

					   // print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
public function getmaxdate($batch_num=NULL){
		
		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
			}

		 $query = "SELECT date_format(adddate(MAX(Expiry_Date),INTERVAL 1 DAY),'%d-%m-%Y') AS Last_Date from tbl_inventory WHERE flag='U' AND is_deleted='0' AND Acceptance_Status='1' AND batch_num='".$batch_num."' ".$sess_where."";
					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}

}
