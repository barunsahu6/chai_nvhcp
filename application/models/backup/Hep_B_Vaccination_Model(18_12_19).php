<?php 
class Hep_B_Vaccination_Model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }
   public function get_hepb_vacc($hepb_id=NULL){

   	$filters1=$this->session->userdata('filters1');
   	if ($hepb_id==NULL|| $hepb_id=='') {
   		$hepb='AND 1';
   	}
   	
   	else{
   		$hepb="AND id_health_worker_hepb=".$hepb_id." AND is_locked='0'";
   	}
   	if($filters1['id_mstfacility']!=NULL){
   		$sess_wherep="AND id_mststate = '".$filters1['id_search_state']."' AND id_mstfacility='".$filters1['id_mstfacility']."'";
   	}
   	else{
   		$sess_wherep="AND 1";
   	}
   	$hepb_repo_filter = $this->session->userdata('hepb_repo_filter1');
   	if($hepb_repo_filter!== FALSE){
   		$date_filter="AND dos1 BETWEEN '".$hepb_repo_filter['Start_Date']."' AND '".$hepb_repo_filter['End_Date']."'";
   	}
   	else{
   		$date_filter="AND 1";
   	}
   	$loginData = $this->session->userdata('loginData'); 
   	if( ($loginData) && $loginData->user_type == '1' ){
   		$sess_where = "AND 1";
   	}
   	elseif( ($loginData) && $loginData->user_type == '2' ){
   		
   		$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
   	}
   	elseif( ($loginData) && $loginData->user_type == '3' ){ 
   		$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
   	}
   	elseif( ($loginData) && $loginData->user_type == '4' ){ 
   		$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
   	}
   	$query = "SELECT id_health_worker_hepb,worker_name,father_or_husband,CASE
   	WHEN gender = 1 THEN 'Male'
   	WHEN gender = 2 THEN 'Female'
   	WHEN gender = 3 THEN 'Transgender'
   	END AS Gender,designation,age,dos1,dos2,dos3,is_locked from `tbl_healtcare_worker_hepb` where is_deleted='0' ".$sess_where." ".$hepb." ".$date_filter." ".$sess_wherep." order by dos3,dos2,dos1 ASC";
   	
   	
   	$result1 = $this->db->query($query)->result();
   	$result=is_array($result1) ? array($result1) :$result1;
   	if(count($result) == 1)
   	{
   		return $result[0];
   	}
   	else
   	{
   		return $result;
   	}

	}
function get_State_hepb_Report(){

	$hepb_repo_filter = $this->session->userdata('hepb_repo_filter1');
	$filters1=$this->session->userdata('filters1');
		//pr($filters1);die();
	if($hepb_repo_filter!= FALSE){
		$date_filter="AND dos1 BETWEEN '".$hepb_repo_filter['Start_Date']."' AND '".$hepb_repo_filter['End_Date']."'";
	}
	else{
		$date_filter="AND 1";
		
	}
	
	$loginData = $this->session->userdata('loginData'); 
	if( ($loginData) && $loginData->user_type == '1' ){
		$sess_where = " AND 1";
		$hospital="st.StateName";
		$qry="inner join `mststate` st on Q.id_mststate=st.id_mststate";
		
	}
	elseif( ($loginData) && $loginData->user_type == '2' ){
		
		$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
	}
	elseif( ($loginData) && $loginData->user_type == '3' ){ 
		$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
		$hospital="concat(f.City, '-', f.FacilityType) AS hospital";
		$qry="inner JOIN  `mstfacility` f ON Q.id_mstfacility=f.id_mstfacility";
		
	}
	elseif( ($loginData) && $loginData->user_type == '4' ){ 
		$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
	}
	if($filters1['id_search_state']=='' && $loginData->user_type=='1'){
		$sess_wherep = "AND 1";
		$hospital="st.StateName,st.id_mststate";
		$qry="inner join `mststate` st on Q.id_mststate=st.id_mststate Order BY st.StateName ASC";
		$groupby="id_mststate";
	}
	else{
		if($filters1['id_search_state']==NULL){
			$State_ID=$loginData->State_ID;
		}
		else{
			$State_ID=$filters1['id_search_state'];
			
		}
		if($filters1['id_input_district']==''|| $filters1['id_input_district']==NULL){
				$sess_wherep = "AND id_mststate = '".$State_ID."'";
			}
			else{
				$sess_wherep = "AND id_mststate = '".$State_ID."' AND id_mstdistrict='".$filters1['id_input_district']."'";
			}
		$hospital="concat(f.City, '-', f.FacilityType) AS hospital,f.id_mstfacility";
		$qry="inner JOIN  `mstfacility` f ON Q.id_mstfacility=f.id_mstfacility Order By f.City ASC";
		$groupby="id_mstfacility";
	}

	//Start query---
 	$query = "SELECT ".$hospital.",Q1.male_dos1,Q2.male_dos2,Q3.male_dos3,Q4.female_dos1,Q5.female_dos2,Q6.female_dos3,Q7.transgender_dos1,Q8.transgender_dos2,Q9.transgender_dos3
	FROM 
	(SELECT id_mststate,id_mstfacility FROM tbl_healtcare_worker_hepb WHERE is_deleted='0' ".$sess_where." ".$sess_wherep." ".$date_filter." GROUP BY ".$groupby.") Q
	LEFT JOIN
	
	(SELECT count(ifnull(dos1,0))AS male_dos1,".$groupby." FROM tbl_healtcare_worker_hepb WHERE dos1 != '0000-00-00' And gender=1 ".$sess_where." ".$sess_wherep." ".$date_filter." group BY gender,".$groupby.") Q1
	ON
	Q.".$groupby." = Q1.".$groupby." 
	LEFT JOIN  (SELECT count(ifnull(dos2,0))AS male_dos2,".$groupby." FROM tbl_healtcare_worker_hepb WHERE dos2 != '0000-00-00' And gender=1 ".$sess_where." ".$sess_wherep." ".$date_filter." group BY gender,".$groupby.") Q2
	ON
	Q.".$groupby." = Q2.".$groupby."
	LEFT JOIN  (SELECT count(ifnull(dos3,0))AS male_dos3,".$groupby." FROM tbl_healtcare_worker_hepb WHERE dos3!= '0000-00-00' And gender=1 ".$sess_where." ".$sess_wherep." ".$date_filter." group BY gender,".$groupby.") Q3
	ON
	Q.".$groupby." = Q3.".$groupby."
	LEFT JOIN
	
	(SELECT count(ifnull(dos1,0))AS female_dos1,".$groupby." FROM tbl_healtcare_worker_hepb WHERE dos1 != '0000-00-00' And gender=2 ".$sess_where." ".$sess_wherep." ".$date_filter." group BY gender,".$groupby.") Q4
	ON
	Q.".$groupby." = Q4.".$groupby." 
	LEFT JOIN  (SELECT count(ifnull(dos2,0))AS female_dos2,".$groupby." FROM tbl_healtcare_worker_hepb WHERE dos2 != '0000-00-00' And gender=2 ".$sess_where." ".$sess_wherep." ".$date_filter." group BY gender,".$groupby.") Q5
	ON
	Q.".$groupby." = Q5.".$groupby."
	LEFT JOIN  (SELECT count(ifnull(dos3,0))AS female_dos3,".$groupby." FROM tbl_healtcare_worker_hepb WHERE dos3!= '0000-00-00' And gender=2 ".$sess_where." ".$sess_wherep." ".$date_filter." group BY gender,id_mststate) Q6
	ON
	Q.".$groupby." = Q6.".$groupby."  
	LEFT JOIN
	
	(SELECT count(ifnull(dos1,0))AS transgender_dos1,".$groupby." FROM tbl_healtcare_worker_hepb WHERE dos1 != '0000-00-00' And gender=3 ".$sess_where." ".$sess_wherep." ".$date_filter." group BY gender,".$groupby.") Q7
	ON
	Q.".$groupby." = Q7.".$groupby." 
	LEFT JOIN  (SELECT count(ifnull(dos2,0))AS transgender_dos2,".$groupby." FROM tbl_healtcare_worker_hepb WHERE dos2 != '0000-00-00' And gender=3 ".$sess_where." ".$sess_wherep." ".$date_filter." group BY gender,".$groupby.") Q8
	ON
	Q.".$groupby." = Q8.".$groupby."
	LEFT JOIN  (SELECT count(ifnull(dos3,0))AS transgender_dos3,".$groupby." FROM tbl_healtcare_worker_hepb WHERE dos3!= '0000-00-00' And gender=3 ".$sess_where." ".$sess_wherep." ".$date_filter." group BY gender,".$groupby.") Q9
	ON
	Q.".$groupby." = Q9.".$groupby." ".$qry."";
	
	$result1 = $this->db->query($query)->result();
	$result=is_array($result1) ? array($result1) :$result1;
	if(count($result) == 1)
	{
		return $result[0];
	}
	else
	{
		return $result;
	}

}	
}
