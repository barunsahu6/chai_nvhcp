<?php  
class Summary_update_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_Model');
		ini_set('memory_limit', '-1');
		error_reporting(E_ALL);
	}

	public function null_everything()
	{
		$query = "UPDATE
					    tblsummary_new
					SET
						`anti_hcv_screened`  = NULL,
						`anti_hcv_positive`  = NULL,
						`viral_load_tested`  = NULL,
						`viral_load_detected`  = NULL,
						`initiatied_on_treatment`  = NULL,
						`treatment_completed`  = NULL,
						`treatment_failure` = NULL,
						`svr_done`  = NULL,
						`screened_for_hav`  = NULL,
						`hav_positive_patients`  = NULL,
						`patients_managed_at_facility`  = NULL,
						`patients_referred_for_management`  = NULL,
						`screened_for_hev`  = NULL,
						`treatment_successful` = NULL,
						`hev_positive_patients`  = NULL,
						`patients_managed_at_facility_hev`  = NULL,
						`patients_referred_for_management_hev`  = NULL,
						`non_cirrhotic`  = NULL,
						`compensated_cirrhotic`  = NULL,
						`decompensated_cirrhotic`  = NULL,
						`reg_initiated_on_treatment`  = NULL,
						`distribution_reg1_SOF_DCV`  = NULL,
						`distribution_reg2_SOF_VEL`  = NULL,
						`distribution_reg3_SOF_VEL_Ribavirin`  = NULL,
						`distribution_reg4_SOF_VEL_24_weeks`  = NULL,
						`treatment_successful_svr_tested`  = NULL,
						`treatment_unsuccessful_svr_tested`  = NULL,
						`reg_wise_initiated_on_treatment`  = NULL,
						`reg_wise_reg1_SOF_DCV`  = NULL,
						`reg_wise_reg2_SOF_VEL`  = NULL,
						`reg_wise_reg3_SOF_VEL_Ribavirin`  = NULL,
						`reg_wise_reg4_SOF_VEL_24_weeks`  = NULL,
						follow_viral_load = NULL,
						follow_1St_dispensation = NULL,
						follow_2St_dispensation = NULL,
						follow_3St_dispensation = NULL,
						follow_4St_dispensation = NULL,
						follow_5St_dispensation = NULL,
						follow_6St_dispensation = NULL,
						follow_SVR = NULL,
						`loss_of_follow_viral_load`  = NULL,
						`loss_of_follow_1St_dispensation`  = NULL,
						`loss_of_follow_2St_dispensation`  = NULL,
						`loss_of_follow_3St_dispensation`  = NULL,
						`loss_of_follow_4St_dispensation`  = NULL,
						`loss_of_follow_5St_dispensation`  = NULL,
						`loss_of_follow_6St_dispensation`  = NULL,
						`loss_of_follow_SVR`  = NULL,
						`adherence_regimen_1`  = NULL,
						`adherence_regimen_2`  = NULL,
						`adherence_regimen_3`  = NULL,
						`adherence_regimen_4`  = NULL,
						`anty_hcv_test_to_anti_hcv_result`  = NULL,
						`anti_hcv_result_to_viral_load_test`  = NULL,
						`viral_load_test_to_delivery_of_vl_result_patient`  = NULL,
						`delivery_result_to_patient_prescription_base_line_test`  = NULL,
						`prescription_baseline_testing_date_baseline_tests`  = NULL,
						`date_of_baseline_tests_result_baseline_tests`  = NULL,
						`result_of_baseline_tests_to_initiation_of_treatment`  = NULL,
						`treatment_completion_to_SVR`  = NULL,
						`vl_samples_accepted`  = NULL,
						`vl_samples_rejected`  = NULL,
						`age_less_than_10`  = NULL,
						`age_10_to_20`  = NULL,
						`age_21_to_30`  = NULL,
						`age_31_to_40`  = NULL,
						`age_41_to_50`  = NULL,
						`age_51_to_60`  = NULL,
						`age_greater_than_60`  = NULL,
						`treatment_experienced_patients`  = NULL,
						`people_who_inject_drugs`  = NULL,
						`persons_with_chronic_kidney_disease`  = NULL,
						`persons_with_HIV_HCV_Co_infection`  = NULL,
						`persons_with_HBV_HCV_Co_infection`  = NULL,
						`persons_with_TB_HCV_Co_infection`  = NULL,
						`pregnant_women`  = NULL,
						`loss_to_follow_patient_referred_to_mtc` = NULL,
						`loss_to_follow_patient_reporting_at_mtc` = NULL";

		$this->db->query($query);
	}

	public function update_summary_all()
	{	
		
		$this->update_cascade_fields();

		$this->update_age_fields();
		//$this->update_gender_area_fields();
		$this->update_cirrhosis_status_fields();
		$this->update_genotype_fields();
		$this->update_risk_factor_fields();
		$this->update_occupation_fields();
		$this->update_missed_appointment_fields();
		$this->update_lfu_fields();
		$this->update_follow_up_fields();
	}

// age functions

	public function update_age_fields()
	{
		$this->age_less_than_10();
		$this->age_10_to_20();
		$this->age_21_to_30();
		$this->age_31_to_40();
		$this->age_41_to_50();
		$this->age_51_to_60();
		$this->age_greater_than_60();

		echo "age, ";
	}

	public function age_less_than_10()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
			



		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					    (
					         T_Initiation
					    
					) AS dt,
					COUNT(PatientGUID) AS Patients,
					id_mstfacility
					FROM
					    `tblpatient`
					WHERE
					    age < 10 AND age > 0 AND NextVisitPurpose > 1 AND T_Initiation IS NOT NULL  ".$where."
					GROUP BY
					    (
					         T_Initiation
					    
					),
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.age_less_than_10 = a.Patients where  ".$where1;

		$this->db->query($query);
	}

	public function age_10_to_20()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        (
					        T_Initiation
					    
					) AS dt,
					        COUNT(PatientGUID) AS Patients,
					        id_mstfacility
					    FROM
					        `tblpatient`
					    WHERE
					        age >= 10 AND age < 20 AND T_Initiation IS NOT NULL  ".$where."
					        GROUP BY
					    (
					        T_Initiation
					    
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.age_10_to_20 = a.Patients where ".$where1;

		$this->db->query($query);
	}

	public function age_21_to_30()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        (
					         T_Initiation
					    
					) AS dt,
					        COUNT(PatientGUID) AS Patients,
					        id_mstfacility
					    FROM
					        `tblpatient`
					    WHERE
					        age >= 20 AND age < 30 AND T_Initiation IS NOT NULL ".$where."
					    GROUP BY
					        (
					        T_Initiation
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.age_21_to_30 = a.Patients where  ".$where1;

		$this->db->query($query);
	}

	public function age_31_to_40()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        (
					        T_Initiation
					    
					) AS dt,
					        COUNT(PatientGUID) AS Patients,
					        id_mstfacility
					    FROM
					        `tblpatient`
					    WHERE
					        age >= 30 AND age < 40 AND T_Initiation IS NOT NULL  ".$where."
					    GROUP BY
					        (
					         T_Initiation
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.age_31_to_40 = a.Patients where ".$where1;

		$this->db->query($query);
	}

	public function age_41_to_50()
	{


				
				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       (
					         T_Initiation
					) AS dt,
					        COUNT(PatientGUID) AS Patients,
					        id_mstfacility
					    FROM
					        `tblpatient`
					    WHERE
					        age >= 40 AND age < 50 AND T_Initiation IS NOT NULL ".$where."
					    GROUP BY
					        (
					         T_Initiation
					    
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.age_41_to_50 = a.Patients where  ".$where1;

		$this->db->query($query);
	}

	public function age_51_to_60()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        (
					        T_Initiation
					) AS dt,
					        COUNT(PatientGUID) AS Patients,
					        id_mstfacility
					    FROM
					        `tblpatient`
					    WHERE
					        age >= 50 AND age < 60 AND T_Initiation IS NOT NULL  ".$where."
					    GROUP BY
					        (
					        T_Initiation
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.age_51_to_60 = a.Patients where  ".$where1;

		$this->db->query($query);
	}

	public function age_greater_than_60()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        (
					         T_Initiation
					) AS dt,
					        COUNT(PatientGUID) AS Patients,
					        id_mstfacility
					    FROM
					        `tblpatient`
					    WHERE
					        age >= 60 AND T_Initiation IS NOT NULL  ".$where."
					    GROUP BY
					        (
					         T_Initiation
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.age_greater_than_60 = a.Patients where  ".$where1;

		$this->db->query($query);
	}

// cascade functions

	public function update_cascade_fields()
	{
				$this->Summary_update_model->cascade_anti_hcv_screened();
				$this->Summary_update_model->cascade_anti_hcv_positive();
				$this->Summary_update_model->cascade_viral_load_tested();
				$this->Summary_update_model->cascade_viral_load_detected();
				$this->Summary_update_model->cascade_initiatied_on_treatment();
				$this->Summary_update_model->cascade_treatment_completed();
				$this->Summary_update_model->cascade_svr_done();
				$this->Summary_update_model->cascade_treatment_successful();
				$this->Summary_update_model->cascade_treatment_failure();

		echo "cascade, ";
	}
public function adherence_followup_analysis(){
		$this->Summary_update_model->follow_up_analysis();
		$this->Summary_update_model->follow_1St_dispensation();
		$this->Summary_update_model->follow_2St_dispensation();
		$this->Summary_update_model->follow_3St_dispensation();
		$this->Summary_update_model->follow_4St_dispensation();
		$this->Summary_update_model->follow_5St_dispensation();
		$this->Summary_update_model->follow_6St_dispensation();
		$this->Summary_update_model->follow_SVR();
}

public function lfu_adherence_followup_analysis(){

		$this->Summary_update_model->lfu_follow_up_analysis();
		$this->Summary_update_model->lfu_follow_1St_dispensation();
		$this->Summary_update_model->lfu_follow_2St_dispensation();
		$this->Summary_update_model->lfu_follow_3St_dispensation();
		$this->Summary_update_model->lfu_follow_4St_dispensation();
		$this->Summary_update_model->lfu_follow_5St_dispensation();
		$this->Summary_update_model->lfu_follow_6St_dispensation();
		$this->Summary_update_model->lfu_follow_SVR();

}
public function vl_samples_accepted_rejected(){

		$this->Summary_update_model->samples_accepted_vl();
		$this->Summary_update_model->samples_rejected_vl();

}
/*VL sample accept and reject*/
public function samples_accepted_vl(){

	$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					        count(*) AS vl_samples_accepted
					    FROM
					        tblpatient
					    WHERE
					        IsSampleAccepted = 1 and VLSampleCollectionDate is not null 
					    GROUP BY
					       VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.vl_samples_accepted = a.vl_samples_accepted";

		$this->db->query($query); 
}


public function samples_rejected_vl(){

	$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					        count(*) AS vl_samples_rejected
					    FROM
					        tblpatient
					    WHERE
					        IsSampleAccepted = 2 and VLSampleCollectionDate is not null 
					    GROUP BY
					       VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.vl_samples_rejected = a.vl_samples_rejected";

		$this->db->query($query); 
}

/*HCV ADHERENCE AND LOSS TO FOLLOW UP ANALYSIS*/
public function follow_up_analysis(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					             MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS follow_viral_load
					    FROM
					        tblpatient
					    WHERE
					        (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null ) and (HCVRapidDate !='0000-00-00' || HCVElisaDate  !='0000-00-00' || HCVOtherDate  !='0000-00-00')
					    GROUP BY
					       T_DLL_01_VLC_Date,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.follow_viral_load = a.follow_viral_load";

		$this->db->query($query); 
}

public function follow_1St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,

					        count(PatientGUID) AS follow_1St_dispensation
					    FROM
					        tblpatient
					    WHERE
					        T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Result=1
					    GROUP BY
					       T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.follow_1St_dispensation = a.follow_1St_dispensation";

		$this->db->query($query); 
}

public function follow_2St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (

					        T_Initiation 
					           
					) AS dt,

					        count(PatientGUID) AS follow_2St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					         T_Initiation is not null and status in (3,4)
					    GROUP BY
					     T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.follow_2St_dispensation = a.follow_2St_dispensation";

		$this->db->query($query); 
}

public function follow_3St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) AS follow_3St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					       T_Initiation is not null and status in (5,6)
					    GROUP BY
					    T_Initiation,
					       id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.follow_3St_dispensation = a.follow_3St_dispensation";

		$this->db->query($query); 
}

public function follow_4St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					          T_Initiation
					) AS dt,

					        count(PatientGUID) AS follow_4St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					       T_Initiation is not null and status in (7,8)
					    GROUP BY
					     T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.follow_4St_dispensation = a.follow_4St_dispensation";

		$this->db->query($query); 
}

public function follow_5St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					          T_Initiation
					) AS dt,

					        count(PatientGUID) AS follow_5St_dispensation
					    FROM
					        tblpatient
					    WHERE
					        T_Initiation is not null and status=9
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.follow_5St_dispensation = a.follow_5St_dispensation";

		$this->db->query($query); 
}

public function follow_6St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					          T_Initiation
					) AS dt,

					        count(*) AS follow_6St_dispensation
					    FROM
					        tblpatient
					    WHERE
					        T_Initiation is not null and status=10
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.follow_6St_dispensation = a.follow_6St_dispensation";

		$this->db->query($query); 
}

public function follow_SVR(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) AS follow_SVR
					    FROM
					        tblpatient
					    WHERE
					        ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and status = 13
					    GROUP BY
					      ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.follow_SVR = a.follow_SVR";

		$this->db->query($query); 
}

/* LFU*/

public function lfu_follow_up_analysis(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					             MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(*) AS loss_of_follow_viral_lfu
					    FROM
					        tblpatient
					    WHERE
					        (T_DLL_01_VLC_Date is null || T_DLL_01_VLC_Date='0000-00-00')
					        and  DATEDIFF (now(), 
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								) ) >7 

					    GROUP BY
					       T_DLL_01_VLC_Date,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.loss_of_follow_viral_load = a.loss_of_follow_viral_lfu";

		$this->db->query($query); 
}

public function lfu_follow_1St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,

					        count(PatientGUID) AS loss_of_follow_1St_Count
					    FROM
					        tblpatient
					    WHERE
					        T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Result=1 and  DATEDIFF (now(), T_DLL_01_VLC_Date) >7 and (T_Initiation is null ||  T_Initiation='0000-00-00')
					    GROUP BY
					       T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.loss_of_follow_1St_dispensation = a.loss_of_follow_1St_Count";

		$this->db->query($query); 
}

public function lfu_follow_2St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation
					) AS dt,

					        count(PatientGUID) AS follow_2St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					     T_Initiation IS not NULL and status in (3,4) and DATEDIFF (NOW(),Next_Visitdt ) >30
					    GROUP BY
					     T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.loss_of_follow_2St_dispensation = a.follow_2St_dispensation";

		$this->db->query($query); 
}

public function lfu_follow_3St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_3St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					       T_Initiation IS NOT NULL and status in (5,6) and DATEDIFF (NOW(),Next_Visitdt ) >30
					    GROUP BY
					    T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.loss_of_follow_3St_dispensation = a.follow_3St_dispensation";

		$this->db->query($query); 
}

public function lfu_follow_4St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					          T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_4St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					        T_Initiation IS NOT NULL and status in (7,8) and DATEDIFF (NOW(),Next_Visitdt ) >30
					    GROUP BY
					      T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.loss_of_follow_4St_dispensation = a.follow_4St_dispensation";

		$this->db->query($query); 
}

public function lfu_follow_5St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_5St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					        T_Initiation IS NOT NULL and status =9 and DATEDIFF (NOW(),Next_Visitdt ) >30
					    GROUP BY
					     T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.loss_of_follow_5St_dispensation = a.follow_5St_dispensation";

		$this->db->query($query); 
}

public function lfu_follow_6St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					         T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_6St_dispensation
					    FROM
					        tblpatient
					    WHERE
					       T_Initiation IS NOT NULL and status =10 and DATEDIFF (NOW(),Next_Visitdt ) >30
					    GROUP BY
					      T_Initiation ,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.loss_of_follow_6St_dispensation = a.follow_6St_dispensation";

		$this->db->query($query); 
}

public function lfu_follow_SVR(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(*) AS follow_SVR
					    FROM
					        tblpatient
					    WHERE
					        ETR_HCVViralLoad_Dt IS NOT NULL 
					    GROUP BY
					      ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.loss_of_follow_SVR = a.follow_SVR";

		$this->db->query($query); 
}
/*end lfu*/
/**/

/*Non Cirrhotic", "Compensated Cirrhotic","Decompensated Cirrhotic*/
public function update_cirrhotic(){

	$this->Summary_update_model->non_cirrhotic();
	$this->Summary_update_model->compensated_cirrhotic();
	$this->Summary_update_model->decompensated_cirrhotic();

}
public function non_cirrhotic(){

 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					    	p.PatientGUID,
					        p.id_mstfacility,
					        (
					            c.Prescribing_Dt
					) AS dt,

					        count(p.PatientGUID) AS non_cirrhotic
					    FROM
					        tblpatient p left join tblpatientcirrohosis c on p.patientguid=c.PatientGUID
					    WHERE
					        p.V1_Cirrhosis=1 
					    GROUP BY
					       c.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.non_cirrhotic = a.non_cirrhotic";

		$this->db->query($query); 
}

public function compensated_cirrhotic(){

	$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        p.id_mstfacility,
					        (
					            c.Prescribing_Dt
					) AS dt,

					        count(p.PatientGUID) AS compensated_cirrhotic
					    FROM
					       tblpatient p left join tblpatientcirrohosis c on p.patientguid=c.PatientGUID
					    WHERE
					        p.Result=1 
					    GROUP BY
					       c.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.compensated_cirrhotic = a.compensated_cirrhotic";

		$this->db->query($query); 
	
}
public function decompensated_cirrhotic(){

	$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        p.id_mstfacility,
					        (
					            c.Prescribing_Dt
					) AS dt,

					        count(p.PatientGUID) AS decompensated_cirrhotic
					    FROM
					       tblpatient p left join tblpatientcirrohosis c on p.patientguid=c.PatientGUID
					    WHERE
					        p.Result=2
					    GROUP BY
					       c.Prescribing_Dt,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.decompensated_cirrhotic = a.decompensated_cirrhotic";

		$this->db->query($query); 
}


	public function cascade_anti_hcv_screened()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS anti_hcv_screened
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_screened = a.anti_hcv_screened  ";

		$this->db->query($query); 


	}

	public function cascade_anti_hcv_positive()
	{

			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HCVRapidResult
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV = 1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_positive = a.HCVRapidResult ";

		$this->db->query($query);
	}

	public function cascade_viral_load_tested()
	{

			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       count(PatientGUID) as VLSampleCollectionCount
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_tested = a.VLSampleCollectionCount   ";

		$this->db->query($query);
	}

	public function cascade_viral_load_detected()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";



		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,
					count(*) AS T_DLL_01_VLC_Result
					FROM
					    tblpatient
					WHERE
					   AntiHCV = 1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 
					GROUP BY
					    (
					        T_DLL_01_VLC_Date
					),
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_detected = a.T_DLL_01_VLC_Result ";

		$this->db->query($query);
	}

	public function cascade_initiatied_on_treatment()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		  $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as COUNT
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_Initiation!='0000-00-00' and T_Initiation is not null
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.initiatied_on_treatment = a.COUNT";

		$this->db->query($query);
	}

	public function cascade_treatment_completed()
	{

			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as treatment_completedcount
					    FROM
					        tblpatient
					    WHERE
					        ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_completed = a.treatment_completedcount  ";

		$this->db->query($query);
	}

	public function cascade_svr_done()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as SVRDrawndatacount
					    FROM
					        tblpatient
					    WHERE
					        SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       SVR12W_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.svr_done = a.SVRDrawndatacount ";

		$this->db->query($query);
	}

	public function cascade_treatment_successful()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		  $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,

					        COUNT(PatientGUID) as SVR12W_Result
					    FROM
					        tblpatient
					    WHERE
					        SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 and SVR12W_Result=2 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       SVR12W_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_successful = a.SVR12W_Result ";

		$this->db->query($query);
	}

public function cascade_treatment_failure()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,

					        COUNT(PatientGUID) as SVR12W_Result
					    FROM
					        tblpatient
					    WHERE
					        SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and SVR12W_Result=1 
					    GROUP BY
					       SVR12W_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_failure = a.SVR12W_Result ";

		$this->db->query($query);
	}

public function cascade_hav_patients(){

	$this->Summary_update_model->cascade_anti_hav_screened();
	$this->Summary_update_model->cascade_hav_positive_patients();
	$this->Summary_update_model->cascade_hav_managed_at_facility();
	$this->Summary_update_model->cascade_hav_referred_for_management();

}

public function cascade_hev_patients(){

	$this->Summary_update_model->cascade_anti_hev_screened();
	$this->Summary_update_model->cascade_hev_positive_patients();
	$this->Summary_update_model->cascade_hev_managed_at_facility();
	$this->Summary_update_model->cascade_hev_referred_for_management();

}


/*HAV */
public function cascade_anti_hav_screened()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HAVRapidDate), 0), 
								COALESCE((HAVElisaDate), 0),  
								COALESCE((HAVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS screened_hav_screened
					    FROM
					        tblpatient
					    WHERE
					        LgmAntiHAV=1 
					    GROUP BY
					    
								GREATEST(
								COALESCE((HAVRapidDate), 0), 
								COALESCE((HAVElisaDate), 0),  
								COALESCE((HAVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.screened_for_hav = a.screened_hav_screened";

		$this->db->query($query); 


	}

	public function cascade_hav_positive_patients()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HAVRapidDate), 0), 
								COALESCE((HAVElisaDate), 0),  
								COALESCE((HAVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS hav_positive_patients
					    FROM
					        tblpatient
					    WHERE
					        HAVRapidResult=1 OR HAVElisaResult=1 OR HAVOtherResult=1 
					    GROUP BY
					    
								GREATEST(
								COALESCE((HAVRapidDate), 0), 
								COALESCE((HAVElisaDate), 0),  
								COALESCE((HAVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.hav_positive_patients = a.hav_positive_patients";

		$this->db->query($query); 


	}

public function cascade_hav_managed_at_facility()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HAVRapidDate), 0), 
								COALESCE((HAVElisaDate), 0),  
								COALESCE((HAVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS patients_managed_at_facility
					    FROM
					        tblpatient
					    WHERE
					       (HAVRapidResult=1 OR HAVElisaResult=1 OR HAVOtherResult=1) and Refer_FacilityHAV=1
					    GROUP BY
					    
								GREATEST(
								COALESCE((HAVRapidDate), 0), 
								COALESCE((HAVElisaDate), 0),  
								COALESCE((HAVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.patients_managed_at_facility = a.patients_managed_at_facility";

		$this->db->query($query); 


	}

public function cascade_hav_referred_for_management()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HAVRapidDate), 0), 
								COALESCE((HAVElisaDate), 0),  
								COALESCE((HAVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS patients_referred_for_management
					    FROM
					        tblpatient
					    WHERE
					       (HAVRapidResult=1 OR HAVElisaResult=1 OR HAVOtherResult=1) and Refer_HigherFacilityHAV=2
					    GROUP BY
					    
								GREATEST(
								COALESCE((HAVRapidDate), 0), 
								COALESCE((HAVElisaDate), 0),  
								COALESCE((HAVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.patients_referred_for_management = a.patients_referred_for_management";

		$this->db->query($query); 


	}

/*HEV PAtient*/

public function cascade_anti_hev_screened()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HEVRapidDate), 0), 
								COALESCE((HEVElisaDate), 0),  
								COALESCE((HEVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS screened_hev_screened
					    FROM
					        tblpatient
					    WHERE
					        LgmAntiHEV=1 
					    GROUP BY
					    
							
								GREATEST(
								COALESCE((HEVRapidDate), 0), 
								COALESCE((HEVElisaDate), 0),  
								COALESCE((HEVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.screened_for_hev = a.screened_hev_screened";

		$this->db->query($query); 


	}

	public function cascade_hev_positive_patients()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HEVRapidDate), 0), 
								COALESCE((HEVElisaDate), 0),  
								COALESCE((HEVOtherDate), 0) 
								))
					) AS dt,

					        count(*) AS hev_positive_patients
					    FROM
					        tblpatient
					    WHERE
					       LgmAntiHEV=1 and  HEVRapidResult=1 OR HEVElisaResult=1 OR HEVOtherResult=1 
					    GROUP BY
					    
								GREATEST(
								COALESCE((HEVRapidDate), 0), 
								COALESCE((HEVElisaDate), 0),  
								COALESCE((HEVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.hev_positive_patients = a.hev_positive_patients";

		$this->db->query($query); 


	}

public function cascade_hev_managed_at_facility()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HEVRapidDate), 0), 
								COALESCE((HEVElisaDate), 0),  
								COALESCE((HEVOtherDate), 0) 
								))
					) AS dt,

					        count(*) AS patients_managed_at_facilityhev
					    FROM
					        tblpatient
					    WHERE
					       (HEVRapidResult=1 OR HEVElisaResult=1 OR HEVOtherResult=1) and Refer_FacilityHEV=1
					    GROUP BY
					    
								GREATEST(
								COALESCE((HEVRapidDate), 0), 
								COALESCE((HEVElisaDate), 0),  
								COALESCE((HEVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.patients_managed_at_facility_hev = a.patients_managed_at_facilityhev";

		$this->db->query($query); 


	}

public function cascade_hev_referred_for_management()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		  $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HEVRapidDate), 0), 
								COALESCE((HEVElisaDate), 0),  
								COALESCE((HEVOtherDate), 0) 
								))
					) AS dt,

					        count(*) AS patients_referred_for_management_hev
					    FROM
					        tblpatient
					    WHERE
					       (HEVRapidResult=1 OR HEVElisaResult=1 OR HEVOtherResult=1) and Refer_HigherFacilityHEV=2
					    GROUP BY
					    
								GREATEST(
								COALESCE((HEVRapidDate), 0), 
								COALESCE((HEVElisaDate), 0),  
								COALESCE((HEVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.patients_referred_for_management_hev = a.patients_referred_for_management_hev";

		$this->db->query($query); 


	}

/*End HEV */
	



// cirrhosis status functions

	public function update_cirrhosis_status_fields()
	{
		$this->cirrhosis_status_yes();
		$this->cirrhosis_status_no();

		echo 'cirrhosis, ';
	}

	public function cirrhosis_status_yes()
	{
				
				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					         (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					) AS dt,

					        id_mstfacility,
					        COUNT(V1_Cirrhosis) AS COUNT
					    FROM 
					    tblpatient where V1_Cirrhosis = 1  ".$where."

					    GROUP BY
					        (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					)
					) a
					ON
					    s.date = a.dt AND s.id_mstfacility = a.id_mstfacility
					SET
					    s.cirrhosis_status_yes = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function cirrhosis_status_no()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					      
					         (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					) AS dt,

					        V1_Cirrhosis,
					        id_mstfacility,
					        COUNT(V1_Cirrhosis) AS COUNT
					    FROM
					        tblpatient 
					       
					    WHERE
					        V1_Cirrhosis = 2 ".$where."
					    GROUP BY
					         (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					)
					) a
					ON
					    s.date = a.dt AND s.id_mstfacility = a.id_mstfacility
					SET
					    s.cirrhosis_status_no = a.COUNT where ".$where1;

		$this->db->query($query);
	}

// genotype functions

	public function update_genotype_fields()
	{	

		$this->genotype1();
		$this->genotype2();
		$this->genotype3();
		$this->genotype4();
		$this->genotype5();
		$this->genotype6();

		echo 'genotype , ';
	}

	public function genotype1()
	{
$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					         (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					) AS dt,

					        tblpatient.id_mstfacility,
					        COUNT(*) AS COUNT
					    FROM
					        `tblpatient` 
					    INNER JOIN tblpatientcirrohosis  ON
					        tblpatient.PatientGUID = tblpatientcirrohosis.PatientGUID
					    WHERE
					        tblpatientcirrohosis.GenotypeTest_Result = 1 AND tblpatient.T_Initiation IS NOT NULL  ".$where."
					    GROUP BY
					        (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					),
					        tblpatient.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.genotype_1 = a.COUNT where ".$where1;

		$this->db->query($query);
	}

	public function genotype2()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					         (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					) AS dt,
					        tblpatient.id_mstfacility,
					        COUNT(*) AS COUNT
					    FROM
					        `tblpatient`
					    INNER JOIN tblpatientcirrohosis ON
					        tblpatient.PatientGUID = tblpatientcirrohosis.PatientGUID
					    WHERE
					        tblpatientcirrohosis.GenotypeTest_Result = 2 AND tblpatient.T_Initiation IS NOT NULL  ".$where."
					    GROUP BY
					         (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					),
					        tblpatient.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.genotype_2 = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function genotype3()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					) AS dt,
					        tblpatient.id_mstfacility,
					        COUNT(*) AS COUNT
					    FROM
					        `tblpatient`
					    INNER JOIN tblpatientcirrohosis ON
					        tblpatient.PatientGUID = tblpatientcirrohosis.PatientGUID
					    WHERE
					        tblpatientcirrohosis.GenotypeTest_Result = 3 AND tblpatient.T_Initiation IS NOT NULL  ".$where."
					    GROUP BY
					         (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					),
					        tblpatient.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.genotype_3 = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function genotype4()
	{
				
				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					         (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					) AS dt,
					        tblpatient.id_mstfacility,
					        COUNT(*) AS COUNT
					    FROM
					        `tblpatient`
					    INNER JOIN tblpatientcirrohosis ON
					        tblpatient.PatientGUID = tblpatientcirrohosis.PatientGUID
					    WHERE
					        tblpatientcirrohosis.GenotypeTest_Result = 4 AND tblpatient.T_Initiation IS NOT NULL  ".$where."
					    GROUP BY
					         (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					),
					        tblpatient.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.genotype_4 = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function genotype5()
	{
$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					         (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					) AS dt,
					        tblpatient.id_mstfacility,
					        COUNT(*) AS COUNT
					    FROM
					        `tblpatient`
					    INNER JOIN tblpatientcirrohosis ON
					        tblpatient.PatientGUID = tblpatientcirrohosis.PatientGUID
					    WHERE
					        tblpatientcirrohosis.GenotypeTest_Result = 5 AND tblpatient.T_Initiation IS NOT NULL  ".$where."
					    GROUP BY
					        (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					),
					        tblpatient.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.genotype_5 = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function genotype6()
	{

			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					         (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					) AS dt,
					        tblpatient.id_mstfacility,
					        COUNT(*) AS COUNT
					    FROM
					        `tblpatient`
					    INNER JOIN tblpatientcirrohosis ON
					        tblpatient.PatientGUID = tblpatientcirrohosis.PatientGUID
					    WHERE
					        tblpatientcirrohosis.GenotypeTest_Result = 6 AND tblpatient.T_Initiation IS NOT NULL  ".$where."
					    GROUP BY
					         (
					            CASE WHEN tblpatient.T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_Initiation
					        END
					),
					        tblpatient.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.genotype_6 = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

// risk factor functions

	public function update_risk_factor_fields()
	{	

		$this->risk_factors_unsafe_injection_use();
		$this->risk_factors_IVDU();
		$this->risk_factors_unprotected_sex();
		$this->risk_factors_surgery();
		$this->risk_factors_dental();
		$this->risk_factors_others();
		$this->risk_factors_not_available();

		echo 'risk_factors , ';
	}

	public function risk_factors_unsafe_injection_use()
	{

			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        COUNT(PatientGUID) AS COUNT,
					         
					         (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        id_mstfacility
					    FROM
					        tblpatient
					    WHERE
					        RiskFactor = 1 AND T_Initiation IS NOT NULL  ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					)
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.risk_factor_unsafe_injection = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function risk_factors_IVDU()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        COUNT(PatientGUID) AS COUNT,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        id_mstfacility
					    FROM
					        tblpatient
					    WHERE
					        RiskFactor = 2 AND T_Initiation IS NOT NULL  ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					)
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.risk_factor_ivdu = a.COUNT where   ".$where1;

		$this->db->query($query);
	}

	public function risk_factors_unprotected_sex()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        COUNT(PatientGUID) AS COUNT,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        id_mstfacility
					    FROM
					        tblpatient
					    WHERE
					        RiskFactor = 3 AND T_Initiation IS NOT NULL ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					)
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.risk_factor_unprotected_sex = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function risk_factors_surgery()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        COUNT(PatientGUID) AS COUNT,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        id_mstfacility
					    FROM
					        tblpatient
					    WHERE
					        RiskFactor = 4 AND T_Initiation IS NOT NULL ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					)
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.risk_factor_surgery = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function risk_factors_dental()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        COUNT(PatientGUID) AS COUNT,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        id_mstfacility
					    FROM
					        tblpatient
					    WHERE
					        RiskFactor = 5 AND T_Initiation IS NOT NULL  ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					)
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.risk_factor_dental = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function risk_factors_others()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        COUNT(PatientGUID) AS COUNT,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        id_mstfacility
					    FROM
					        tblpatient
					    WHERE
					        RiskFactor = 6 AND T_Initiation IS NOT NULL ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					)
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.risk_factor_others = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function risk_factors_not_available()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        COUNT(PatientGUID) AS COUNT,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        id_mstfacility
					    FROM
					        tblpatient
					    WHERE
					        (
					            RiskFactor IS NULL || RiskFactor = 0
					        ) AND T_Initiation IS NOT NULL  ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					)
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.risk_factor_not_available = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

// occupation functions

	public function update_occupation_fields()
	{	

		$this->occupation_health_care_worker();
		$this->occupation_dental_worker();
		$this->occupation_lab_tech();
		$this->occupation_barber();
		$this->occupation_cleaner();
		$this->occupation_truck_driver();
		$this->occupation_farmer();
		$this->occupation_labourer();
		$this->occupation_service();
		$this->occupation_other();

		echo 'occupation, ';
	}

	public function occupation_health_care_worker()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        OccupationID,
					        COUNT(*) AS COUNT
					    FROM
					        tblpatient
					    WHERE
					        occupationid = 1 AND T_Initiation IS NOT NULL ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.occupation_health_care_worker = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function occupation_dental_worker()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        COUNT(*) AS COUNT
					    FROM
					        tblpatient
					    WHERE
					        occupationid = 2 AND T_Initiation IS NOT NULL ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.occupation_dental_worker = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function occupation_lab_tech()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        COUNT(*) AS COUNT
					    FROM
					        tblpatient
					    WHERE
					        occupationid = 3 AND T_Initiation IS NOT NULL ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.occupation_lab_tech = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function occupation_barber()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        COUNT(*) AS COUNT
					    FROM
					        tblpatient
					    WHERE
					        occupationid = 4 AND T_Initiation IS NOT NULL ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.occupation_barber = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function occupation_cleaner()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";



		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        COUNT(*) AS COUNT
					    FROM
					        tblpatient
					    WHERE
					        occupationid = 5 AND T_Initiation IS NOT NULL ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.occupation_cleaner = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function occupation_truck_driver()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        COUNT(*) AS COUNT
					    FROM
					        tblpatient
					    WHERE
					        occupationid = 6 AND T_Initiation IS NOT NULL ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.occupation_truck_driver = a.COUNT where ".$where1;

		$this->db->query($query);
	}

	public function occupation_farmer()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";



		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        COUNT(*) AS COUNT
					    FROM
					        tblpatient
					    WHERE
					        occupationid = 7 AND T_Initiation IS NOT NULL  ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.occupation_farmer = a.COUNT where ".$where1;

		$this->db->query($query);
	}

	public function occupation_labourer()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        COUNT(*) AS COUNT
					    FROM
					        tblpatient
					    WHERE
					        occupationid = 8 AND T_Initiation IS NOT NULL ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.occupation_labourer = a.COUNT where ".$where1;

		$this->db->query($query);
	}

	public function occupation_service()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        COUNT(*) AS COUNT
					    FROM
					        tblpatient
					    WHERE
					        occupationid = 9 AND T_Initiation IS NOT NULL ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.occupation_service = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function occupation_other()
	{


			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					) AS dt,
					        COUNT(*) AS COUNT
					    FROM
					        tblpatient
					    WHERE
					        occupationid = 10 AND T_Initiation IS NOT NULL ".$where."
					    GROUP BY
					        (
					            CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation
					        END
					),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.occupation_other = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

// missed appointment functions

public function update_missed_appointment_fields()
	{	

		$this->missed_follow_up_visits();
		$this->missed_svr();
		$this->missed_treatment_initiation();
		$this->missed_confirmatory_test();

		echo 'missed, ';
	}

	public function missed_follow_up_visits()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
						    
						    (
					            CASE WHEN Next_Visitdt < '2016-06-18' THEN '2016-06-18' ELSE Next_Visitdt
					        END
					) AS dt,
						    id_mstfacility,
						    COUNT(patientguid) AS COUNT
						FROM
						    tblpatient
						WHERE
						    nextvisitpurpose BETWEEN 2 AND 98 AND Next_Visitdt IS NOT NULL and Next_Visitdt < date(now())  ".$where."
						GROUP BY
						   (  CASE WHEN Next_Visitdt < '2016-06-18' THEN '2016-06-18' ELSE Next_Visitdt
					        END
					),
						    id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.missed_follow_up_visits = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function missed_svr()
	{


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
							   
							     (
					            CASE WHEN tblpatient.Next_Visitdt < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.Next_Visitdt
					        END
					) AS dt,
							    tblpatient.id_mstfacility,
							    COUNT(tblpatient.patientguid) AS COUNT
							FROM
							    tblpatient
							INNER JOIN cumulative_svr ON
							    tblpatient.PatientGUID = cumulative_svr.patientguid
							WHERE
							    nextvisitpurpose = 99 AND cumulative_svr.svr_date IS NULL AND Next_Visitdt IS NOT NULL AND Next_Visitdt < DATE(NOW())  ".$where."
							GROUP BY
							     (
					            CASE WHEN tblpatient.Next_Visitdt < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.Next_Visitdt
					        END
					),
							    tblpatient.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.missed_svr = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function missed_treatment_initiation()
	{	
		$days = $this->get_missed_days();
		$missed_TI_days = $days[0]->missed_appointment_treatment_initiation;



				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";



		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
							     (
					            CASE WHEN tblpatient.T_DLL_01_VLC_Date < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_DLL_01_VLC_Date
					        END
					) AS dt,

							    tblpatient.id_mstfacility,
							    COUNT(tblpatient.patientguid) AS COUNT
							FROM
							    tblpatient
							WHERE
							    nextvisitpurpose = 1 AND T_DLL_01_VLC_Date IS NOT NULL AND(
							        DATEDIFF(
							            DATE(NOW()),
							            tblpatient.T_DLL_01_VLC_Date) > ".$missed_TI_days."
							        )  ".$where."
							    GROUP BY
							        (
					            CASE WHEN tblpatient.T_DLL_01_VLC_Date < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_DLL_01_VLC_Date
					        END
					),
							        tblpatient.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.missed_treatment_initiation = a.COUNT where ".$where1;

		$this->db->query($query);
	}

	public function missed_confirmatory_test()
	{
		$days = $this->get_missed_days();
		$missed_confirmatory_days = $days[0]->missed_appointment_confirmatory;


				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";



		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
							    (
					             CASE WHEN tblpatient.T_DLL_01_Date < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_DLL_01_Date
					        END
					) AS dt,

							    tblpatient.id_mstfacility,
							    COUNT(tblpatient.patientguid) AS COUNT
							FROM
							    tblpatient
							WHERE
							    nextvisitpurpose = - 1 AND(
							        DATEDIFF(
							            DATE(NOW()),
							            tblpatient.T_DLL_01_Date) > ".$missed_confirmatory_days."
							        )  ".$where."
							    GROUP BY
							        (
					            CASE WHEN tblpatient.T_DLL_01_Date < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.T_DLL_01_Date
					        END
					),
							        tblpatient.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.missed_confirmatory_test = a.COUNT where ".$where1;
		$this->db->query($query);
	}

	public function get_missed_days()
	{
		$query = "select * from mstappstateconfiguration";
		$result = $this->db->query($query)->result();

		return $result;
	}

// loss to follow up functions

	public function update_lfu_fields()
	{	

		$lfu_days = $this->get_lfu_days();

		$this->lfu_visits($lfu_days);
		$this->lfu_svr($lfu_days);
		$this->lfu_treatment_initiation($lfu_days);
		$this->lfu_confirmatory($lfu_days);

		echo 'lfu, ';
	}

	public function lfu_visits($lfu_days = null)
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
						    id_mstfacility,
						    COUNT(patientguid) AS COUNT,
						    (
					            CASE WHEN Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE Next_VisitDt
					        END
					) AS dt
						FROM
						    tblpatient
						WHERE
						    nextvisitpurpose BETWEEN 2 AND 98 AND Next_VisitDt IS NOT NULL AND(
						        DATEDIFF(DATE(NOW()),
						        Next_VisitDt) >= ".$lfu_days->loss_to_followup_days.")  ".$where."
						GROUP BY
						    (
					            CASE WHEN Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE Next_VisitDt
					        END
					),
						    id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_visits = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function lfu_svr($lfu_days)
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
							    tblpatient.id_mstfacility,
							    COUNT(tblpatient.patientguid) AS COUNT,
							    (
					            CASE WHEN tblpatient.Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.Next_VisitDt
					        END
					) AS dt

							FROM
							    tblpatient
							INNER JOIN cumulative_svr ON
							    tblpatient.PatientGUID = cumulative_svr.patientguid
							WHERE
							    tblpatient.nextvisitpurpose = 99 AND cumulative_svr.svr_date IS NULL AND Next_VisitDt IS NOT NULL AND(
							        DATEDIFF(DATE(NOW()),
							        tblpatient.Next_VisitDt) >= ".$lfu_days->loss_to_followup_days.") ".$where."
						GROUP BY
						   (
					            CASE WHEN tblpatient.Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE tblpatient.Next_VisitDt
					        END
					),
						    tblpatient.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_svr = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function lfu_treatment_initiation($lfu_days)
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";



		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
							    id_mstfacility,
							    COUNT(patientguid) AS COUNT,
							    (
					            CASE WHEN Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE Next_VisitDt
					        END
					) AS dt

							FROM
							    tblpatient
							WHERE
							    nextvisitpurpose = 1 AND Next_VisitDt IS NOT NULL AND(
							        DATEDIFF(DATE(NOW()),
							        Next_VisitDt) >= ".$lfu_days->lfu_treatment_initiation.") ".$where."
						GROUP BY
						     (
					            CASE WHEN Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE Next_VisitDt
					        END
					),
						    id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_treatment_initiation = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function lfu_confirmatory($lfu_days = null)
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
							    id_mstfacility,
							    COUNT(patientguid) AS COUNT,
							     (
					            CASE WHEN Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE Next_VisitDt
					        END
					) AS dt

							FROM
							    tblpatient
							WHERE
							    nextvisitpurpose = -1 AND Next_VisitDt IS NOT NULL AND(
							        DATEDIFF(DATE(NOW()),
							        Next_VisitDt) >= ".$lfu_days->lfu_confirmatory.")  ".$where."
						GROUP BY
						     (
					            CASE WHEN Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE Next_VisitDt
					        END
					),
						    id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.lfu_confirmatory = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function get_lfu_days()
	{
		$query    = "SELECT * FROM mstappstateconfiguration";
		$result   = $this->db->query($query)->result();
		return $result[0];
	}

// follow up functions

	public function update_follow_up_fields()
	{	

		$this->follow_up_visits();
		$this->follow_up_svr();
		$this->follow_up_treatment_initiation();
		$this->follow_up_confirmatory();

		echo 'follow up, ';
	}

	public function follow_up_visits()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
							    id_mstfacility,
							    COUNT(patientguid) AS COUNT,
							   
							     (
					            CASE WHEN Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE Next_VisitDt
					        END
					) AS dt
							FROM
							    tblpatient
							WHERE
							    nextvisitpurpose BETWEEN 2 AND 98 AND Next_Visitdt >= DATE(NOW()) AND Next_Visitdt < DATE_ADD(DATE(NOW()),
							    INTERVAL 7 DAY) ".$where."
							GROUP BY
							     (
					            CASE WHEN Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE Next_VisitDt
					        END
					),
							    id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.follow_up_visits = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function follow_up_svr()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
							    id_mstfacility,
							    COUNT(patientguid) AS COUNT,
							     (
					            CASE WHEN Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE Next_VisitDt
					        END
					) AS dt
							FROM
							    tblpatient
							WHERE
							    nextvisitpurpose = 99 AND Next_Visitdt >= DATE(NOW()) AND Next_Visitdt < DATE_ADD(DATE(NOW()),
							    INTERVAL 7 DAY)  ".$where."
							GROUP BY
							     (
					            CASE WHEN Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE Next_VisitDt
					        END
					),
							    id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.follow_up_svr = a.COUNT where ".$where1;

		$this->db->query($query);
	}

	public function follow_up_treatment_initiation()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
							    id_mstfacility,
							    COUNT(patientguid) AS COUNT,
							     (
					            CASE WHEN Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE Next_VisitDt
					        END
					) AS dt
							FROM
							    tblpatient
							WHERE
							    nextvisitpurpose = 1 AND Next_Visitdt >= DATE(NOW()) AND Next_Visitdt < DATE_ADD(DATE(NOW()),
							    INTERVAL 7 DAY)  ".$where."
							GROUP BY
							     (
					            CASE WHEN Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE Next_VisitDt
					        END
					),
							    id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.follow_up_treatment_initiation = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

	public function follow_up_confirmatory()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";


		$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
							    id_mstfacility,
							    COUNT(patientguid) AS COUNT,
							     (
					            CASE WHEN Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE Next_VisitDt
					        END
					) AS dt
							FROM
							    tblpatient
							WHERE
							    nextvisitpurpose = -1 AND Next_Visitdt >= DATE(NOW()) AND Next_Visitdt < DATE_ADD(DATE(NOW()),
							    INTERVAL 7 DAY)  ".$where."
							GROUP BY
							     (
					            CASE WHEN Next_VisitDt < '2016-06-18' THEN '2016-06-18' ELSE Next_VisitDt
					        END
					),
							    id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.follow_up_confirmatory = a.COUNT where  ".$where1;

		$this->db->query($query);
	}

public function update_patient_guid(){

				$query = "UPDATE
					    core_diagnostics_raw_import s
					INNER JOIN(
					    select 
					    p.PatientGUID,c.institution_name from core_diagnostics_raw_import c 
					    inner join mstfacility m inner join
					   tblpatient p on
             c.institution_name=m.facility_name and c.uid=p.UID_Num 
            and m.id_mstfacility=p.id_mstfacility
					) a
					ON
					    s.institution_name = a.institution_name
					SET
					    s.PatientGUID = a.PatientGUID";
					  $this->db->query($query);
}

public function update_all_core_data(){

	$updatestr = "UPDATE
    `core_diagnostics_raw_import` AS `dest`,
    (
        SELECT
            *
        FROM
            `core_diagnostics_raw_import`
        WHERE
            uid like '%/%'
    ) AS `src`
SET
    
   dest.uid = REPLACE(dest.uid,'/','-')";

$this->db->query($updatestr);
	$this->update_srv_core_data();
	//$this->confirmatory_svr_date();
	//$this->genotype_svr_date();

}

public function update_all_core_dataor(){
	$updatestr = "UPDATE
    `core_diagnostics_raw_import` AS `dest`,
    (
        SELECT
            *
        FROM
            `core_diagnostics_raw_import`
        WHERE
            uid like '%/%'
    ) AS `src`
SET
    
   dest.uid = REPLACE(dest.uid,'/','-')";
   
$this->db->query($updatestr);

	$this->update_srv_core_dataor();
	$this->confirmatory_svr_dateor();
	$this->genotype_svr_dateor();

}

public function save_svrdata(){

	$sql_add_new_patientguids = "insert into tblpatientSVR (PatientGUID)
    SELECT p.PatientGUID FROM 
	 tblpatient p 
	left join tblpatientSVR s on p.patientguid = s.PatientGUID where s.PatientGUID is null";
    $this->Common_Model->update_data_sql($sql_add_new_patientguids);

}
public function update_srv_core_data(){

	$query = "UPDATE
					    tblpatientSVR s
					INNER JOIN(SELECT 
    p.patientguid as patientguid,
    c.report_date AS svr_date,
    CASE c.result
    WHEN 'Not-Detected' THEN 2
    ELSE 1
    END AS result,
    c.quant_result AS viralload
    
    FROM
    core_diagnostics_raw_import c
    INNER JOIN
    mstfacility m
	 INNER JOIN
	 tblpatient p on
	c.institution_name=m.facility_name 
	and c.uid=p.UID_Num 
	and m.id_mstfacility=p.id_mstfacility

    WHERE
    c.test_name= 'HCV Viral RNA Quantitative' and
    c.svr = 'YES' AND
   c.result  IN ('Not-Detected' ,'Detected') ) AS a on s.PatientGUID=a.patientguid
					SET
					    s.SVRDate = a.svr_date ,
					    s.SVRResult = a.result,
					    s.SVRVLC = a.viralload,
					    s.svr_from = 'C'";
					  $this->db->query($query);

}


public function confirmatory_svr_date(){

	$query = "UPDATE
					    tblpatientSVR s
					INNER JOIN(SELECT 
    p.patientguid as patientguid,
    c.report_date AS svr_date,
    CASE c.result
    WHEN 'Not-Detected' THEN 2
    ELSE 1
    END AS result,
    c.quant_result AS viralload
    
    FROM
   core_diagnostics_raw_import c
    INNER JOIN
    mstfacility m
	 INNER JOIN
	 tblpatient p on
	c.institution_name=m.facility_name 
	and c.uid=p.UID_Num
	and m.id_mstfacility=p.id_mstfacility

    WHERE
    c.svr = 'NO' AND
    c.test_name= 'HCV Viral RNA Quantitative' and
   c.result  IN ('Not-Detected' ,'Detected') ) AS a on s.PatientGUID=a.patientguid
					SET
								s.confirmatory_test_date       = a.svr_date ,
								s.confirmatory_test_result     = a.result,
								s.confirmatory_test_viral_load = a.viralload,
								s.confirmatory_from            = 'C' ";
					  $this->db->query($query);

}

public function genotype_svr_date(){

	$query = "UPDATE
					    tblpatientSVR s
					INNER JOIN(SELECT 
    p.patientguid as patientguid,
    c.report_date AS svr_date,
    CASE c.quant_result
    WHEN 'Genotype 1' THEN 1
    WHEN 'Genotype 2' THEN 2
    WHEN 'Genotype 3' THEN 3
    WHEN 'Genotype 4' THEN 4
    WHEN 'Genotype 5' THEN 5
    WHEN 'Genotype 6' THEN 6
	WHEN 'Genotype 1b' THEN 1
	WHEN 'Genotype 1a' THEN 1
    
    END AS result,
    c.quant_result AS viralload
    
    FROM
    core_diagnostics_raw_import c
    INNER JOIN
    mstfacility m
	 INNER JOIN
	 tblpatient p on
	c.institution_name=m.facility_name 
	and c.uid=p.UID_Num
	and m.id_mstfacility=p.id_mstfacility

    WHERE
   
    c.test_name ='HCV Genotype' AND
   c.result  IN ('Not-Detected' ,'Detected') ) AS a  on s.PatientGUID=a.patientguid
					SET
								s.genotype_test_date   = a.svr_date ,
								s.genotype_test_result = a.result,
								s.genotype_from        = 'C' ";

					  $this->db->query($query);

}


public function update_srv_core_dataor(){

	$query = "UPDATE
					    tblpatientSVR s
					INNER JOIN(SELECT 
    p.patientguid as patientguid,
    c.report_date AS svr_date,
    CASE c.result
    WHEN 'Not-Detected' THEN 2
    ELSE 1
    END AS result,
    c.quant_result AS viralload
    
    FROM
    core_diagnostics_raw_import c
    INNER JOIN
    mstfacility m
	 INNER JOIN
	 tblpatient p on
	c.institution_name=m.facility_name 
	and  c.uid = CONCAT(p.`UID_Prefix` ,'-', lpad(p.`UID_Num`, 6, 0))
	and m.id_mstfacility=p.id_mstfacility

    WHERE
    c.test_name= 'HCV Viral RNA Quantitative' and
    c.svr = 'YES' AND
   c.result  IN ('Not-Detected' ,'Detected') ) AS a on s.PatientGUID=a.patientguid
					SET
					    s.SVRDate = a.svr_date ,
					    s.SVRResult = a.result,
					    s.SVRVLC = a.viralload,
					    s.svr_from = 'C'";
					  $this->db->query($query);

}


public function confirmatory_svr_dateor(){

	$query = "UPDATE
					    tblpatientSVR s
					INNER JOIN(SELECT 
    p.patientguid as patientguid,
    c.report_date AS svr_date,
    CASE c.result
    WHEN 'Not-Detected' THEN 2
    ELSE 1
    END AS result,
    c.quant_result AS viralload
    
    FROM
   core_diagnostics_raw_import c
    INNER JOIN
    mstfacility m
	 INNER JOIN
	 tblpatient p on
	c.institution_name=m.facility_name 
	and c.uid = CONCAT(p.`UID_Prefix` ,'-', lpad(p.`UID_Num`, 6, 0))
	and m.id_mstfacility=p.id_mstfacility

    WHERE
    c.svr = 'NO' AND
    c.test_name= 'HCV Viral RNA Quantitative' and
   c.result  IN ('Not-Detected' ,'Detected') ) AS a on s.PatientGUID=a.patientguid
					SET
								s.confirmatory_test_date       = a.svr_date ,
								s.confirmatory_test_result     = a.result,
								s.confirmatory_test_viral_load = a.viralload,
								s.confirmatory_from            = 'C' ";
					  $this->db->query($query);

}

public function genotype_svr_dateor(){

	$query = "UPDATE
					    tblpatientSVR s
					INNER JOIN(SELECT 
    p.patientguid as patientguid,
    c.report_date AS svr_date,
    CASE c.quant_result
    WHEN 'Genotype 1' THEN 1
    WHEN 'Genotype 2' THEN 2
    WHEN 'Genotype 3' THEN 3
    WHEN 'Genotype 4' THEN 4
    WHEN 'Genotype 5' THEN 5
    WHEN 'Genotype 6' THEN 6
	WHEN 'Genotype 1b' THEN 1
	WHEN 'Genotype 1a' THEN 1
    
    END AS result,
    c.quant_result AS viralload
    
    FROM
    core_diagnostics_raw_import c
    INNER JOIN
    mstfacility m
	 INNER JOIN
	 tblpatient p on
	c.institution_name=m.facility_name 
	and  c.uid = CONCAT(p.`UID_Prefix` ,'-', lpad(p.`UID_Num`, 6, 0))
	and m.id_mstfacility=p.id_mstfacility

    WHERE
   
    c.test_name ='HCV Genotype' AND
   c.result  IN ('Not-Detected' ,'Detected') ) AS a  on s.PatientGUID=a.patientguid
					SET
								s.genotype_test_date   = a.svr_date ,
								s.genotype_test_result = a.result,
								s.genotype_from        = 'C' ";

					  $this->db->query($query);

}


/*Loss to Follow Up for Special Cases*/
public function update_lossup_spcase(){
	$this->loss_to_followupspcase_refmtc();
	$this->loss_to_followupspcase_inicate();
}
public function loss_to_followupspcase_refmtc()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		  $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as IsReferal
					    FROM
					        tblpatient
					    WHERE
					         IsReferal=1 and  ReferalDate is not null and ReferalDate!='0000-00-00'
					    GROUP BY
					       ReferalDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.loss_to_follow_patient_referred_to_mtc = a.IsReferal ";

		$this->db->query($query);
	}


public function loss_to_followupspcase_inicate()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		  $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					          T_Initiation 
					) AS dt,

					        count(PatientGUID) as IsReferal1
					    FROM
					        tblpatient
					    WHERE
					         IsReferal=1 and T_Initiation is not null and ReferalDate!='0000-00-00'
					    GROUP BY
					       ReferalDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.loss_to_follow_patient_reporting_at_mtc = a.IsReferal1 ";

		$this->db->query($query);
	}
	

/* end */	
/*update draw Adherence State*/
public function Adherencesummarytest(){

	$this->Summary_update_model->adherref_one();
	$this->Summary_update_model->adherref_two();
	$this->Summary_update_model->adherref_three();
	$this->Summary_update_model->adherref_three24();

}
public function adherref_one(){
	$loginData = $this->session->userdata('loginData'); 
				

		  $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(

					SELECT p.id_mstfacility ,
					 (
					            T_Initiation
					) AS dt,
					avg(v.Adherence) as Countval from tblpatient p inner join tblpatientvisit v on p.PatientGUID=v.PatientGUID

					    WHERE
					        v.Adherence>0  and T_Regimen=1
					        group by  p.T_Regimen,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.reg_wise_reg1_SOF_DCV = a.Countval ";

		$this->db->query($query);
}

public function adherref_two(){
	$loginData = $this->session->userdata('loginData'); 
				

		  $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(

					SELECT p.id_mstfacility ,
					 (
					            T_Initiation
					) AS dt,
					avg(v.Adherence)as Countval from tblpatient p inner join tblpatientvisit v on p.PatientGUID=v.PatientGUID

					    WHERE
					        v.Adherence>0  and T_Regimen=2
					        group by  p.T_Regimen,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.reg_wise_reg2_SOF_VEL = a.Countval ";

		$this->db->query($query);
}


public function adherref_three(){
	$loginData = $this->session->userdata('loginData'); 
				

		  $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(

					SELECT p.id_mstfacility ,
					 (
					            T_Initiation
					) AS dt,
					avg(v.Adherence)as Countval from tblpatient p inner join tblpatientvisit v on p.PatientGUID=v.PatientGUID

					    WHERE
					        v.Adherence>0  and T_Regimen=3
					        group by  p.T_Regimen,
					        p.id_mstfacility
					   
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.reg_wise_reg3_SOF_VEL_Ribavirin = a.Countval ";

		$this->db->query($query);
}

public function adherref_three24(){
	$loginData = $this->session->userdata('loginData'); 
				

		  $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(

					SELECT p.id_mstfacility ,
					 (
					            T_Initiation
					) AS dt,
					avg(v.Adherence) as Countval from tblpatient p inner join tblpatientvisit v on p.PatientGUID=v.PatientGUID

					    WHERE
					         v.Adherence>0  and T_Regimen=3 and T_DurationValue=24
					        group by  p.T_Regimen,
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.reg_wise_reg4_SOF_VEL_24_weeks = a.Countval ";

		$this->db->query($query);
}


/*End update draw Adherence State*/

public function timearounddata_summaryupdate(){

	$this->Summary_update_model->anti_hcv_result_to_viral_load_test();
	$this->Summary_update_model->viral_load_test_to_delivery_of_vl_result_patient();
	$this->Summary_update_model->delivery_result_to_patient_prescription_base_line_test();
	$this->Summary_update_model->prescription_baseline_testing_date_baseline_tests();
	$this->Summary_update_model->date_of_baseline_tests_result_baseline_tests();
	$this->Summary_update_model->result_of_baseline_tests_to_initiation_of_treatment();
	$this->Summary_update_model->treatment_completion_to_SVR();

}

/*Turn-Around Time Analysis update summary table start*/
public function anti_hcv_result_to_viral_load_test(){

	 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(

					select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select id_mstfacility,patientguid,T_Initiation,datediff(p.T_DLL_01_VLC_Date,
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								)) as dt from tblpatient p where p.T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' and (HCVRapidDate is not null || HCVElisaDate is not null  || HCVOtherDate is not null ) AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' and datediff(p.T_DLL_01_VLC_Date,
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								)) >0 )  a
								group by id_mstfacility,
								DATE_FORMAT(STR_TO_DATE(T_Initiation,'%d-%m-%Y'), '%Y-%m-%d') )aa
					ON
					    s.id_mstfacility = aa.id_mstfacility AND s.date = aa.dtval
					SET
					    s.anti_hcv_result_to_viral_load_test = aa.Count ";

					    $this->db->query($query);
}

public function viral_load_test_to_delivery_of_vl_result_patient(){

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(

					select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select id_mstfacility,patientguid,T_Initiation,datediff(p.T_DLL_01_VLC_Date,
							VLSampleCollectionDate) as dt from tblpatient p where p.T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00'  AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' and datediff(p.T_DLL_01_VLC_Date,
							VLSampleCollectionDate) >0 )  a
								group by id_mstfacility,
								DATE_FORMAT(STR_TO_DATE(T_Initiation,'%d-%m-%Y'), '%Y-%m-%d') ) aa
					ON
					    s.id_mstfacility = aa.id_mstfacility AND s.date = aa.dtval
					SET
					    s.viral_load_test_to_delivery_of_vl_result_patient = aa.Count ";
					    $this->db->query($query);
}


public function delivery_result_to_patient_prescription_base_line_test(){

 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(

					select id_mstfacility,T_Initiation AS dtval, FORMAT (avg(dt),0 ) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,c.Prescribing_Dt,datediff(c.Prescribing_Dt,
							p.T_DLL_01_VLC_Date) as dt from tblpatient p LEFT JOIN tblpatientcirrohosis c on p.PatientGUID=c.PatientGUID  where p.T_DLL_01_VLC_Date is not null and p.T_DLL_01_VLC_Date!='0000-00-00' AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00' and datediff(c.Prescribing_Dt,
							p.T_DLL_01_VLC_Date) >0 )  a
								group by id_mstfacility,
								DATE_FORMAT(STR_TO_DATE(T_Initiation,'%d-%m-%Y'), '%Y-%m-%d')  ) aa
					ON
					    s.id_mstfacility = aa.id_mstfacility AND s.date = aa.dtval
					SET
					    s.delivery_result_to_patient_prescription_base_line_test = aa.Count ";
					    $this->db->query($query);
}


public function prescription_baseline_testing_date_baseline_tests(){

	$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(

					select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,c.Prescribing_Dt,p.PrescribingDate,datediff(p.PrescribingDate,
							c.Prescribing_Dt) as dt from tblpatient p LEFT JOIN tblpatientcirrohosis c on p.PatientGUID=c.PatientGUID  where p.PrescribingDate is not null and p.PrescribingDate!='0000-00-00' AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00' and datediff(p.PrescribingDate,
							c.Prescribing_Dt) >0 )  a
								group by id_mstfacility,
								DATE_FORMAT(STR_TO_DATE(T_Initiation,'%d-%m-%Y'), '%Y-%m-%d')  ) aa
					ON
					    s.id_mstfacility = aa.id_mstfacility AND s.date = aa.dtval
					SET
					    s.prescription_baseline_testing_date_baseline_tests = aa.Count ";
					    $this->db->query($query);

}

public function date_of_baseline_tests_result_baseline_tests(){

	$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(

					select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,c.Prescribing_Dt,p.PrescribingDate,datediff(p.PrescribingDate,
							c.Prescribing_Dt) as dt from tblpatient p LEFT JOIN tblpatientcirrohosis c on p.PatientGUID=c.PatientGUID  where p.PrescribingDate is not null and p.PrescribingDate!='0000-00-00' AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00' and datediff(p.PrescribingDate,
							c.Prescribing_Dt) >0 )  a
								group by id_mstfacility,
								DATE_FORMAT(STR_TO_DATE(T_Initiation,'%d-%m-%Y'), '%Y-%m-%d')  ) aa
					ON
					    s.id_mstfacility = aa.id_mstfacility AND s.date = aa.dtval
					SET
					    s.date_of_baseline_tests_result_baseline_tests = aa.Count ";
					    $this->db->query($query);
}


public function result_of_baseline_tests_to_initiation_of_treatment(){

	$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(

					select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,c.Prescribing_Dt,p.PrescribingDate,datediff(DATE_FORMAT(STR_TO_DATE(p.T_Initiation,'%d-%m-%Y'), '%Y-%m-%d'),
							p.PrescribingDate) as dt from tblpatient p LEFT JOIN tblpatientcirrohosis c on p.PatientGUID=c.PatientGUID  where p.PrescribingDate is not null and p.PrescribingDate!='0000-00-00' AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00' and datediff(DATE_FORMAT(STR_TO_DATE(T_Initiation,'%d-%m-%Y'), '%Y-%m-%d'),
							p.PrescribingDate) >0 )  a
								group by id_mstfacility,
								DATE_FORMAT(STR_TO_DATE(T_Initiation,'%d-%m-%Y'), '%Y-%m-%d')  ) aa
					ON
					    s.id_mstfacility = aa.id_mstfacility AND s.date = aa.dtval
					SET
					    s.result_of_baseline_tests_to_initiation_of_treatment = aa.Count ";
					    $this->db->query($query);
}

public function treatment_completion_to_SVR(){

	$query = "UPDATE
					    tblsummary_new s
					INNER JOIN(

					select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,p.ETR_HCVViralLoad_Dt,p.PrescribingDate,datediff(p.SVR12W_HCVViralLoad_Dt,
							p.ETR_HCVViralLoad_Dt) as dt from tblpatient p  where p.ETR_HCVViralLoad_Dt is not null and p.ETR_HCVViralLoad_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00' and datediff(SVR12W_HCVViralLoad_Dt,
							p.ETR_HCVViralLoad_Dt) >0 )  a
								group by id_mstfacility,
								DATE_FORMAT(STR_TO_DATE(T_Initiation,'%d-%m-%Y'), '%Y-%m-%d')  ) aa
					ON
					    s.id_mstfacility = aa.id_mstfacility AND s.date = aa.dtval
					SET
					    s.treatment_completion_to_SVR = aa.Count ";
					    $this->db->query($query);
}

/*end Turn-Around Time Analysis*/


}