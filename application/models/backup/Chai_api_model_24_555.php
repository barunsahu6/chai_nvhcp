<?php  
class Chai_api_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Common_Model");
	}

	public function get_token()
	{
		$requested_method = $this->input->server('REQUEST_METHOD');
		
		if($requested_method != "POST")
		{
			return "ERROR: Request method not supported. Please use HTTP POST method";
		}

		$username = $this->security->xss_clean($this->input->post('username'));

		$this->db->where('lower(username)',hash('sha256',$username));
		$result = $this->db->get('tblusers')->result();
		if (count($result) < 1) {
			return "ERROR: This user does not exist";
		}

		$token = hash('sha256', date('Y-m-d H:i:s') . rand(1000,9999));
		$updateArr = array(
			'token'            => $token,
			'token_valid_till' =>	date("Y-m-d H:i:s", strtotime('+30 minutes')),
		);

		$this->db->where('lower(username)',hash('sha256',$username));
		$this->db->update('tblusers', $updateArr);

		return $token;
	}

	private function getfacilityid($username, $password)
	{
		$query = "select * from tblusers where username = ?";
		$res = $this->db->query($query, [$username])->result();
		return $res[0]->id_mstfacility;
	}

	private function getuserid($username, $password)
	{
		$query = "select * from tblusers where username = ?";
		$res = $this->db->query($query, [$username])->result();
		return $res[0]->id_tblusers;
	}

	public function uploaddata()
	{
		$data = $this->input->post('data');

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$id_mstfacility = $this->getfacilityid($username, $password);
		$id_tblusers    = $this->getuserid($username, $password);

		$uniqid_json = uniqid();
		$insert_data = array(
			"id_tblusers"    => $id_tblusers,
			"json_guid"      => $uniqid_json,
			"json"           => $data,
			"save_status"    => null,
			"username"       => $this->input->post('username'),
			"id_mstfacility" => $id_mstfacility,
			"uploaded_on"    => date('Y-m-d H:i:s'),
			);

		$this->db->insert('tbl_received_json' ,$insert_data);
		
		$data_array = json_decode($data);

		$this->db->trans_begin();
		foreach ($data_array as $tablename => $tabledata) {
			switch($tablename)
			{
				case "tblpatient" : $this->tblpatient($tabledata);
								break;
				case "tblpatientaddltest" : $this->tblpatientaddltest($tabledata);
								break;
				case "tblpatientcirrohosis" : $this->tblpatientcirrohosis($tabledata);
								break;
				case "tblpatientfu" : $this->tblpatientfu($tabledata);
								break;
				case "tblpatientvisit" : $this->tblpatientvisit($tabledata);
								break;
				// case "tblstock" : $this->tblstock($tabledata);
				// 				break;
				//case "tblDeleteVisits" : $this->tblDeleteVisits($tabledata);
								break;
				case "tblusers" : $this->tblusers($tabledata);
								break;
				case "tblSVRfollowup" : $this->tblSVRfollowup($tabledata);
								break;
				case "tblRiskProfile" : $this->tblRiskProfile($tabledata);
								break;
				case "tblpatient_regimen_drug_data" : $this->tblpatient_regimen_drug_data($tabledata);
								break;
				default : 
					// $res['error'] = 'Error : An unknown table was submitted';
					// return $res;
					// break;
			}
		}

		if ($this->db->trans_status() === FALSE)
		{
			// print_r($this->db->error()); die();
		        $this->db->trans_rollback();
		        $res['error'] = 'Error : Transaction Failed';
				log_message('error',$uniqid_json.' transaction failed');

				$update_array = array(
					"save_status" => "fail"
					);
				$this->db->where('json_guid', $uniqid_json);
				$this->db->update('tbl_received_json', $update_array);
		}
		else
		{		
				log_message('info',$uniqid_json.' transaction successful');
		        $this->db->trans_commit();
		        $res = null;

		        $update_array = array(
					"save_status" => "success"
					);
				$this->db->where('json_guid', $uniqid_json);
				$this->db->update('tbl_received_json', $update_array);
		}

		return $res;

	}

	private function tblpatient($tabledata)
	{	
		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$id_mstfacility = $this->getfacilityid($username, $password);

		foreach ($tabledata as $row) 
		{	
			$row->id_mstfacility = $id_mstfacility;
			// print_r($row); die();
			if($row->T_DLL_01_VLC_Date == "" || $row->T_DLL_01_VLC_Date == '0000-00-00')
			{
				$row->T_DLL_01_VLC_Date = null;	
			}
			if($row->Cirr_TestDate == "" || $row->Cirr_TestDate == '0000-00-00')
			{
				$row->Cirr_TestDate = null;	
			}
			if($row->ETR_HCVViralLoad_Dt == "" || $row->ETR_HCVViralLoad_Dt == '0000-00-00')
			{
				$row->ETR_HCVViralLoad_Dt = null;	
			}
			if($row->SVR12W_HCVViralLoad_Dt == "" || $row->SVR12W_HCVViralLoad_Dt == '0000-00-00')
			{
				$row->SVR12W_HCVViralLoad_Dt = null;	
			}
			if($row->InitiationDt == "" || $row->InitiationDt == '0000-00-00')
			{
				$row->InitiationDt = null;	
			}
			if(isset($row->ETRDt))
			{
				if($row->ETRDt == "" || $row->ETRDt == '0000-00-00')
				{
					$row->ETRDt = null;	
				}
			}
			if($row->DeliveryDt == "" || $row->DeliveryDt == '0000-00-00')
			{
				$row->DeliveryDt = null;	
			}
			if($row->AdvisedSVRDate == "" || $row->AdvisedSVRDate == '0000-00-00')
			{
				$row->AdvisedSVRDate = null;	
			}
			if(isset($row->ETRDt))
			{
				if($row->Current_Visitdt == "" || $row->Current_Visitdt == '0000-00-00')
				{
					$row->Current_Visitdt = null;	
				}
			}
			if(isset($row->ETRDt))
			{
				if($row->Next_Visitdt == "" || $row->Next_Visitdt == '0000-00-00')
				{
					$row->Next_Visitdt = null;	
				}
			}
			if($row->CreatedOn == "" || $row->CreatedOn == '0000-00-00')
			{
				$row->CreatedOn = null;	
			}
			if($row->UpdatedOn == "" || $row->UpdatedOn == '0000-00-00')
			{
				$row->UpdatedOn = null;	
			}
			if($row->T_Initiation == "" || $row->T_Initiation == '0000-00-00')
			{
				$row->T_Initiation = null;	
			}
			if($row->Next_Visitdt == "" || $row->Next_Visitdt == '0000-00-00')
			{
				$row->Next_Visitdt = null;	
			}
			if($row->Current_Visitdt == "" || $row->Current_Visitdt == '0000-00-00')
			{
				$row->Current_Visitdt = null;	
			}
			if($row->ETRTestDate == "" || $row->ETRTestDate == '0000-00-00')
			{
				$row->ETRTestDate = null;	
			}
			if(isset($row->UploadedOn))
			{
				if($row->UploadedOn == "" || $row->UploadedOn == '0000-00-00')
				{
					$row->UploadedOn = null;	
				}
			}
			if($row->LastPillDate == "" || $row->LastPillDate == '0000-00-00')
			{
				$row->LastPillDate = null;	
			}

			$row->linelist_synced = 0;

			//checking if the patientguid exists
			$sql_record_exists = "select * from tblpatient where patientguid = '".$row->PatientGUID."'";

			$res_record_exists = $this->Common_Model->query_data($sql_record_exists);
			if(count($res_record_exists) > 0)
			{

				if($res_record_exists[0]->SVR12W_HCVViralLoad_Dt != null && ($row->SVR12W_HCVViralLoad_Dt == null || $row->SVR12W_HCVViralLoad_Dt == ''))
				{
					unset($row->SVR12W_HCVViralLoad_Dt);
					unset($row->SVR12W_HCVViralLoadCount);
					unset($row->SVR12W_Result);
				}
				$this->Common_Model->update_data('tblpatient', $row, 'patientguid',$row->PatientGUID);
			}
			else
			{
				$this->Common_Model->insert_data('tblpatient', $row);
			}
		}
	}

	private function tblpatientaddltest($tabledata)
	{	
		foreach ($tabledata as $row) 
		{	
			if($row->CreatedOn == "" || $row->CreatedOn == '0000-00-00')
			{
				$row->CreatedOn = null;	
			}
			if($row->UpdatedOn == "" || $row->UpdatedOn == '0000-00-00')
			{
				$row->UpdatedOn = null;	
			}
			if(isset($row->UploadedOn))
			{
				if($row->UploadedOn == "" || $row->UploadedOn == '0000-00-00')
				{
					$row->UploadedOn = null;	
				}
			}
			$this->Common_Model->insert_data('tblpatientaddltest', $row);
		}
	}

	private function tblpatientcirrohosis($tabledata)
	{	
		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$id_mstfacility = $this->getfacilityid($username, $password);
		foreach ($tabledata as $row) 
		{	
			$row->id_mstfacility = $id_mstfacility;
			if($row->Fibroscan_Dt == "" || $row->Fibroscan_Dt == '0000-00-00')
			{
				$row->Fibroscan_Dt = null;	
			}
			if($row->Clinical_US_Dt == "" || $row->Clinical_US_Dt == '0000-00-00')
			{
				$row->Clinical_US_Dt = null;	
			}
			/*if($row->APRI_Dt == "" || $row->APRI_Dt == '0000-00-00')
			{
				$row->APRI_Dt = null;	
			}
			if($row->FIB4_Dt == "" || $row->FIB4_Dt == '0000-00-00')
			{
				$row->FIB4_Dt = null;	
			}*/
			if($row->CreatedOn == "" || $row->CreatedOn == '0000-00-00')
			{
				$row->CreatedOn = null;	
			}
			if($row->UpdatedOn == "" || $row->UpdatedOn == '0000-00-00')
			{
				$row->UpdatedOn = null;	
			}
			if(isset($row->UploadedOn))
			{
				if($row->UploadedOn == "" || $row->UploadedOn == '0000-00-00')
				{
					$row->UploadedOn = null;	
				}
			}

			//checking if the patientguid exists
			$sql_record_exists = "select * from tblpatientcirrohosis where patientguid = '".$row->PatientGUID."'";

			$res_record_exists = $this->Common_Model->query_data($sql_record_exists);
			if(count($res_record_exists) > 0)
			{
				$this->Common_Model->update_data('tblpatientcirrohosis', $row, 'patientguid',$row->PatientGUID);
				
				$error = $this->db->error();
				if($error['code'] != 0)
				{
					print_r($error);
						die('error in db op');
				}
			}
			else
			{
				$this->Common_Model->insert_data('tblpatientcirrohosis', $row);
			}

		}
	}

	private function tblpatientfu($tabledata)
	{	
		foreach ($tabledata as $row) 
		{
			if($row->CreatedOn == "" || $row->CreatedOn == '0000-00-00')
			{
				$row->CreatedOn = null;	
			}
			if($row->UpdatedOn == "" || $row->UpdatedOn == '0000-00-00')
			{
				$row->UpdatedOn = null;	
			}
			if(isset($row->UploadedOn))
			{
				if($row->UploadedOn == "" || $row->UploadedOn == '0000-00-00')
				{
					$row->UploadedOn = null;	
				}
			}

				$this->Common_Model->insert_data('tblpatientfu', $row);
		}
	}

	private function tblpatientvisit($tabledata)
	{	
		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$id_mstfacility = $this->getfacilityid($username, $password);

		foreach ($tabledata as $row) 
		{	
			$row->id_mstfacility = $id_mstfacility;
			if($row->Visit_Dt == "" || $row->Visit_Dt == '0000-00-00')
			{
				$row->Visit_Dt = null;	
			}
			if($row->NextVisit_Dt == "" || $row->NextVisit_Dt == '0000-00-00')
			{
				$row->NextVisit_Dt = null;	
			}
			if($row->CreatedOn == "" || $row->CreatedOn == '0000-00-00')
			{
				$row->CreatedOn = null;	
			}
			if($row->UpdatedOn == "" || $row->UpdatedOn == '0000-00-00')
			{
				$row->UpdatedOn = null;	
			}
			if(isset($row->UploadedOn))
			{
				if($row->UploadedOn == "" || $row->UploadedOn == '0000-00-00')
				{
					$row->UploadedOn = null;	
				}
			}

			//checking if the patientguid exists
			$sql_record_exists = "select * from tblpatientvisit where patientvisitguid = '".$row->PatientVisitGUID."'";

			$res_record_exists = $this->Common_Model->query_data($sql_record_exists);
			if(count($res_record_exists) > 0)
			{
				$this->Common_Model->update_data('tblpatientvisit', $row, 'patientvisitguid',$row->PatientVisitGUID);
			}
			else
			{
				$this->Common_Model->insert_data('tblpatientvisit', $row);
			}
		}
	}

	private function tblstock($tabledata)
	{	
		foreach ($tabledata as $row) 
		{
			if($row->CreatedOn == "" || $row->CreatedOn == '0000-00-00')
			{
				$row->CreatedOn = null;	
			}
			if($row->UpdatedOn == "" || $row->UpdatedOn == '0000-00-00')
			{
				$row->UpdatedOn = null;	
			}
			if(isset($row->UploadedOn))
			{
				if($row->UploadedOn == "" || $row->UploadedOn == '0000-00-00')
				{
					$row->UploadedOn = null;	
				}
			}
			
			$row->id_mstfacility = $row->FacilityID;
			unset($row->FacilityID);
			// print_r($row); die();
			$this->Common_Model->insert_data('tblstock', $row);
		}
	}

	private function tblDeleteVisits($tabledata)
	{	
		foreach ($tabledata as $row) 
		{
			$query = "delete from tblpatientvisit where PatientVisitGUID = '".$row->DeletedGUID."'";
			$this->db->query($query);
		}
	}

	private function tblusers($tabledata)
	{
		foreach ($tabledata as $row) {
			$sql = "update tblusers set Mobile = '".$row->Mobile."', Email = '".$row->Email."' where id_tblusers = ".$row->id_tblusers;
			$this->Common_Model->update_data_sql($sql);
		}
	}

	public function uploadstock()
	{
		$data = $this->input->post('data');

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$id_mstfacility = $this->getfacilityid($username, $password);
		$id_tblusers    = $this->getuserid($username, $password);

		$uniqid_json = uniqid();
		$insert_data = array(
			"id_tblusers"    => $id_tblusers,
			"json_guid"      => $uniqid_json,
			"json"           => $data,
			"username"       => $this->input->post('username'),
			"id_mstfacility" => $id_mstfacility,
			"uploaded_on"    => date('Y-m-d H:i:s'),
			);

		$this->db->insert('tbl_received_json' ,$insert_data);
		
		$data_array = json_decode($data);

		$this->db->trans_begin();
		foreach ($data_array as $tablename => $tabledata) {
			switch($tablename)
			{
				case "tblstock" : $this->tblstock($tabledata);
								break;
				default : 
					// $res['error'] = 'Error : An unknown table was submitted';
					// return $res;
					// break;
			}
		}

		if ($this->db->trans_status() === FALSE)
		{
			// print_r($this->db->error()); die();
		        $this->db->trans_rollback();
		        $res['error'] = 'Error : Transaction Failed';
				log_message('error',$uniqid_json.' transaction failed');
		}
		else
		{		
				log_message('info',$uniqid_json.' transaction successful');
		        $this->db->trans_commit();
		        $res = null;
		}

		return $res;

	}

	public function tblSVRFollowUp($tabledata)
	{
		
		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$id_mstfacility = $this->getfacilityid($username, $password);
		$id_tblusers    = $this->getuserid($username, $password);

		foreach ($tabledata as $row) {

			$row->id_mstfacility = $id_mstfacility;	

			if($row->FollowupDate == '0000-00-00' || $row->FollowupDate == "")
			{
				$row->FollowupDate = null;	
			}
			if($row->SVRdate == '0000-00-00' || $row->SVRdate == "")
			{
				$row->SVRdate = null;	
			}
			if($row->CreatedOn == '0000-00-00' || $row->CreatedOn == "")
			{
				$row->CreatedOn = null;	
			}
			if($row->UpdatedOn == '0000-00-00' || $row->UpdatedOn == "")
			{
				$row->UpdatedOn = null;	
			}
			if($row->UploadedOn == '0000-00-00' || $row->UploadedOn == "")
			{
				$row->UploadedOn = null;	
			}

			$this->db->insert('tblSVRfollowup', $row);
		}

	}

	public function tblRiskProfile($tabledata)
	{
		
		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$id_mstfacility = $this->getfacilityid($username, $password);
		$id_tblusers    = $this->getuserid($username, $password);

		if(count($tabledata) > 0)
		{

			foreach ($tabledata as $row) 
			{
				
				$res_patientguids_array[] = "'".$row->PatientGUID."'";

			}
			$patientguids = implode($res_patientguids_array,',');


			$query = "insert into deleted_records_tblRiskProfile(`PatientGUID`, `RiskID`, `deleted_on`)
						SELECT `PatientGUID`, `RiskID`, date(now()) as deleted_on FROM `tblRiskProfile` where patientguid in (".$patientguids.")";
			$result = $this->db->query($query);

			$query = "delete FROM `tblRiskProfile` where patientguid in (".$patientguids.")";

			$result = $this->db->query($query);

			foreach ($tabledata as $row) {

				// $row->id_mstfacility = $id_mstfacility;	

				$this->db->insert('tblRiskProfile', $row);
			}
		}

	}

	public function tblpatient_regimen_drug_data($tabledata)
	{
		
		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$id_mstfacility = $this->getfacilityid($username, $password);
		$id_tblusers    = $this->getuserid($username, $password);

		if(count($tabledata) > 0)
		{

			foreach ($tabledata as $row) 
			{
				
				$res_patientguids_array[] = "'".$row->patientguid."'";

			}
			$patientguids = implode($res_patientguids_array,',');

			$query = "insert into deleted_records_tblpatient_regimen_drug_data(`patientguid`, `visitguid`, `visit_no`, `id_mst_drugs`, `id_mst_drug_strength`, `id_mstfacility`, `created_by`, `created_on`, `updated_on`, `updated_by`,deleted_on)
						SELECT `patientguid`, `visitguid`, `visit_no`, `id_mst_drugs`, `id_mst_drug_strength`, `id_mstfacility`, `created_by`, `created_on`, `updated_on`, `updated_by`, date(now()) as deleted_on FROM `tblpatient_regimen_drug_data` where patientguid in (".$patientguids.")";
			$result = $this->db->query($query);

			$query = "delete FROM `tblpatient_regimen_drug_data` where patientguid in (".$patientguids.")";
			$result = $this->db->query($query);


			foreach ($tabledata as $row) {
				
				if($row->created_on == '0000-00-00' || $row->created_on == "")
				{
					$row->created_on = null;	
				}
				if($row->updated_on == '0000-00-00' || $row->updated_on == "")
				{
					$row->updated_on = null;	
				}

				$this->db->insert('tblpatient_regimen_drug_data', $row);
			}
		}

	}

}