<?php  
class Summary_genderwiseupdate_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_Model');
		ini_set('memory_limit', '-1');
		error_reporting(E_ALL);
	}

public function everything_null(){
	$query = "UPDATE
					    tblsummary_genderwise
					SET
						`anti_hcv_screened`  = NULL,
						`anti_hcv_positive`  = NULL,
						`viral_load_tested`  = NULL,
						`viral_load_detected`  = NULL,
						`initiatied_on_treatment`  = NULL,
						`treatment_completed`  = NULL,
						`eligible_for_svr` = NULL,
						`svr_done`  = NULL,	
						`treatment_successful` = NULL,
						`treatment_failure` = NULL";

		$this->db->query($query);
}
public function update_cascade_fields_genderwise()
	{
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_screened_adultmale();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_screened_malechildren();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_screened_adultfemale();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_screened_femalechildren();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_screened_transgenderadult();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_screened_transgenderchildren();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_positive_adultmale();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_positive_malechildren();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_positive_adultfemale();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_positive_femalechildren();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_positive_transgenderfemale();
				$this->Summary_genderwiseupdate_model->cascade_anti_hcv_positive_transgenderchildren();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_tested_adultmale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_tested_childrenmale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_tested_adultfemale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_tested_childrenfemale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_tested_adulttransgender();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_tested_childrentransgender();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_detected_adultmale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_detected_childrenmale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_detected_adultfemale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_detected_childrenfemale();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_detected_adulttransgender();
				$this->Summary_genderwiseupdate_model->cascade_viral_load_detected_childrentransgender();
				$this->Summary_genderwiseupdate_model->cascade_initiatied_on_treatment_adultmale();
				$this->Summary_genderwiseupdate_model->cascade_initiatied_on_treatment_childrenmale();
				$this->Summary_genderwiseupdate_model->cascade_initiatied_on_treatment_adultfemale();
				$this->Summary_genderwiseupdate_model->cascade_initiatied_on_treatment_childrenfemale();
				$this->Summary_genderwiseupdate_model->cascade_initiatied_on_treatment_adulttransgender();
				$this->Summary_genderwiseupdate_model->cascade_initiatied_on_treatment_childrentransgender();
				$this->Summary_genderwiseupdate_model->cascade_treatment_completed_adultmale();
				$this->Summary_genderwiseupdate_model->cascade_treatment_completed_childrenmale();
				$this->Summary_genderwiseupdate_model->cascade_treatment_completed_adultfemale();
				$this->Summary_genderwiseupdate_model->cascade_treatment_completed_childrenfemale();
				$this->Summary_genderwiseupdate_model->cascade_treatment_completed_adulttransgender();
				$this->Summary_genderwiseupdate_model->cascade_treatment_completed_childrentransgender();
				$this->Summary_genderwiseupdate_model->cascade_eligible_forsvr_adultmale();
				$this->Summary_genderwiseupdate_model->cascade_eligible_forsvr_childrenmale();
				$this->Summary_genderwiseupdate_model->cascade_eligible_forsvr_adultfemale();
				$this->Summary_genderwiseupdate_model->cascade_eligible_forsvr_childrenfemale();
				$this->Summary_genderwiseupdate_model->cascade_eligible_forsvr_adulttransgender();
				$this->Summary_genderwiseupdate_model->cascade_eligible_forsvr_childrentransgender();
				$this->Summary_genderwiseupdate_model->cascade_anti_hepb_positive();
				$this->Summary_genderwiseupdate_model->cascade_anti_hepb_screen();

		echo "done cascade, ";
	}


public function cascade_anti_hcv_screened_adultmale()
	{
		
			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS anti_hcv_screened
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and gender = 1 and age >= 18 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00') 
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_screened = a.anti_hcv_screened  where s.Gender=1 and s.Agewise=1";

		$this->db->query($query); 


	}

	public function cascade_anti_hcv_screened_malechildren()
	{
		
			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS anti_hcv_screened
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and gender = 1 and age < 18 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00') 
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_screened = a.anti_hcv_screened  where s.Gender=1 and s.Agewise=2";

		$this->db->query($query); 


	}

	public function cascade_anti_hcv_screened_adultfemale()
	{
		
			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS anti_hcv_screened
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and gender = 2 and age >= 18 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00') 
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_screened = a.anti_hcv_screened  where s.Gender=2 and s.Agewise=1";

		$this->db->query($query); 


	}

	public function cascade_anti_hcv_screened_femalechildren()
	{
		
			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS anti_hcv_screened
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and gender = 2 and age < 18 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00') 
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_screened = a.anti_hcv_screened  where s.Gender=2 and s.Agewise=2";

		$this->db->query($query); 


	}


	public function cascade_anti_hcv_screened_transgenderadult()
	{
		
			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS anti_hcv_screened
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and gender = 3 and age >= 18 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00') 
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_screened = a.anti_hcv_screened  where s.Gender=3 and s.Agewise=1";

		$this->db->query($query); 


	}


	public function cascade_anti_hcv_screened_transgenderchildren()
	{
		
			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS anti_hcv_screened
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and gender = 3 and  age < 18 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00') 
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_screened = a.anti_hcv_screened  where s.Gender=3 and s.Agewise=2";

		$this->db->query($query); 


	}
//and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')


	public function cascade_anti_hcv_positive_adultmale()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HCVRapidResult
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV = 1  and gender = 1 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_positive = a.HCVRapidResult where s.Gender=1 and s.Agewise=1";

		$this->db->query($query);
	}


public function cascade_anti_hcv_positive_malechildren()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HCVRapidResult
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV = 1  and gender = 1 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_positive = a.HCVRapidResult where s.Gender=1 and s.Agewise=2";

		$this->db->query($query);
	}
/*female +ve*/

public function cascade_anti_hcv_positive_adultfemale()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HCVRapidResult
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV = 1  and gender = 2 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_positive = a.HCVRapidResult where s.Gender=2 and s.Agewise=1";

		$this->db->query($query);
	}


public function cascade_anti_hcv_positive_femalechildren()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HCVRapidResult
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV = 1  and gender = 2 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_positive = a.HCVRapidResult where s.Gender=2 and s.Agewise=2";

		$this->db->query($query);
	}
	/*end female +ve*/
/*transgender*/

public function cascade_anti_hcv_positive_transgenderfemale()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HCVRapidResult
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV = 1  and gender = 3 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_positive = a.HCVRapidResult where s.Gender=3 and s.Agewise=1";

		$this->db->query($query);
	}


public function cascade_anti_hcv_positive_transgenderchildren()
	{

		

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HCVRapidResult
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV = 1  and gender = 3 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hcv_positive = a.HCVRapidResult where s.Gender=3 and s.Agewise=2";

		$this->db->query($query);
	}
	/*end transgender +ve*/


	public function cascade_viral_load_tested_adultmale()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       count(PatientGUID) as VLSampleCollectionCount
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 and gender = 1 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_tested = a.VLSampleCollectionCount   where s.Gender=1 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_viral_load_tested_childrenmale()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       count(PatientGUID) as VLSampleCollectionCount
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 and gender = 1 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_tested = a.VLSampleCollectionCount   where s.Gender=1 and s.Agewise=2";

		$this->db->query($query);
	}

/*LD Female*/

public function cascade_viral_load_tested_adultfemale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       count(PatientGUID) as VLSampleCollectionCount
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 and gender = 2 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_tested = a.VLSampleCollectionCount  where s.Gender=2 and s.Agewise=1 ";

		$this->db->query($query);
	}

	public function cascade_viral_load_tested_childrenfemale()
	{

			

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       count(PatientGUID) as VLSampleCollectionCount
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 and gender = 2 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_tested = a.VLSampleCollectionCount  where s.Gender=2 and s.Agewise=2 ";

		$this->db->query($query);
	}

	/* VL Transgender*/

	public function cascade_viral_load_tested_adulttransgender()
	{

		

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       count(PatientGUID) as VLSampleCollectionCount
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 and gender = 3 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_tested = a.VLSampleCollectionCount  where s.Gender=3 and s.Agewise=1 ";

		$this->db->query($query);
	}

	public function cascade_viral_load_tested_childrentransgender()
	{

		
				

		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       count(PatientGUID) as VLSampleCollectionCount
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 and gender = 3 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        VLSampleCollectionDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_tested = a.VLSampleCollectionCount  where s.Gender=3 and s.Agewise=2 ";

		$this->db->query($query);
	}


	public function cascade_viral_load_detected_adultmale()
	{

				



		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,
					count(*) AS T_DLL_01_VLC_Result
					FROM
					    tblpatient
					WHERE
					   AntiHCV = 1 and gender = 1 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' 
					GROUP BY
					    (
					        T_DLL_01_VLC_Date
					),
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_detected = a.T_DLL_01_VLC_Result where s.Gender=1 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_viral_load_detected_childrenmale()
	{

				



		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,
					count(*) AS T_DLL_01_VLC_Result
					FROM
					    tblpatient
					WHERE
					   AntiHCV = 1 and gender = 1 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' 
					GROUP BY
					    (
					        T_DLL_01_VLC_Date
					),
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_detected = a.T_DLL_01_VLC_Result where s.Gender=1 and s.Agewise=2";

		$this->db->query($query);
	}
/*Vl Dedecated female*/

public function cascade_viral_load_detected_adultfemale()
	{

				



		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,
					count(*) AS T_DLL_01_VLC_Result
					FROM
					    tblpatient
					WHERE
					   AntiHCV = 1 and gender = 2 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' 
					GROUP BY
					    (
					        T_DLL_01_VLC_Date
					),
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_detected = a.T_DLL_01_VLC_Result where s.Gender=2 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_viral_load_detected_childrenfemale()
	{

				



		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,
					count(*) AS T_DLL_01_VLC_Result
					FROM
					    tblpatient
					WHERE
					   AntiHCV = 1 and gender = 2 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' 
					GROUP BY
					    (
					        T_DLL_01_VLC_Date
					),
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_detected = a.T_DLL_01_VLC_Result where s.Gender=2 and s.Agewise=2";

		$this->db->query($query);
	}

	/*VL detacted transgender*/

	public function cascade_viral_load_detected_adulttransgender()
	{

				



		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,
					count(*) AS T_DLL_01_VLC_Result
					FROM
					    tblpatient
					WHERE
					   AntiHCV = 1 and gender = 3 and age >= 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' 
					GROUP BY
					    (
					        T_DLL_01_VLC_Date
					),
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_detected = a.T_DLL_01_VLC_Result where s.Gender=3 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_viral_load_detected_childrentransgender()
	{

				



		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,
					count(*) AS T_DLL_01_VLC_Result
					FROM
					    tblpatient
					WHERE
					   AntiHCV = 1 and gender = 3 and age < 18 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' 
					GROUP BY
					    (
					        T_DLL_01_VLC_Date
					),
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.viral_load_detected = a.T_DLL_01_VLC_Result where s.Gender=3 and s.Agewise=2 ";

		$this->db->query($query);
	}



	public function cascade_initiatied_on_treatment_adultmale()
	{

			

		  $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as COUNT
					    FROM
					        tblpatient
					    WHERE
					          gender = 1 and age >= 18 and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.initiatied_on_treatment = a.COUNT where s.Gender=1 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_initiatied_on_treatment_childrenmale()
	{

				

		  $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as COUNT
					    FROM
					        tblpatient
					    WHERE
					          gender = 1 and age < 18 and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.initiatied_on_treatment = a.COUNT where s.Gender=1 and s.Agewise=2";

		$this->db->query($query);
	}


/*Ination female*/

public function cascade_initiatied_on_treatment_adultfemale()
	{

				

		  $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as COUNT
					    FROM
					        tblpatient
					    WHERE
					          gender = 2 and age >= 18 and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.initiatied_on_treatment = a.COUNT where s.Gender=2 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_initiatied_on_treatment_childrenfemale()
	{

				

		  $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as COUNT
					    FROM
					        tblpatient
					    WHERE
					          gender = 2 and age < 18 and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.initiatied_on_treatment = a.COUNT where s.Gender=2 and s.Agewise=2";

		$this->db->query($query);
	}

	/*Inatiation transgender*/

	public function cascade_initiatied_on_treatment_adulttransgender()
	{

				

		  $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as COUNT
					    FROM
					        tblpatient
					    WHERE
					          gender = 3 and age >= 18 and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.initiatied_on_treatment = a.COUNT where s.Gender=3 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_initiatied_on_treatment_childrentransgender()
	{

				

		  $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as COUNT
					    FROM
					        tblpatient
					    WHERE
					          gender = 3 and age < 18 and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  
					    GROUP BY
					      T_Initiation,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.initiatied_on_treatment = a.COUNT where s.Gender=3 and s.Agewise=2";

		$this->db->query($query);
	}

	/*end initation*/

	public function cascade_treatment_completed_adultmale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as treatment_completedcount
					    FROM
					        tblpatient
					    WHERE
					        gender = 1 and age >= 18 AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_completed = a.treatment_completedcount  where s.Gender=1 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_treatment_completed_childrenmale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as treatment_completedcount
					    FROM
					        tblpatient
					    WHERE
					        gender = 1 and age < 18 AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_completed = a.treatment_completedcount  where s.Gender=1 and s.Agewise=2";

		$this->db->query($query);
	}

/*start female cascade_treatment_completed */
public function cascade_treatment_completed_adultfemale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as treatment_completedcount
					    FROM
					        tblpatient
					    WHERE
					        gender = 2 and age >= 18 AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_completed = a.treatment_completedcount  where s.Gender=2 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_treatment_completed_childrenfemale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as treatment_completedcount
					    FROM
					        tblpatient
					    WHERE
					        gender = 2 and age < 18 AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_completed = a.treatment_completedcount  where s.Gender=2 and s.Agewise=2";

		$this->db->query($query);
	}

/*End*/
/*Transgender compleated*/

public function cascade_treatment_completed_adulttransgender()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as treatment_completedcount
					    FROM
					        tblpatient
					    WHERE
					        gender = 3 and age >= 18 AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_completed = a.treatment_completedcount  where s.Gender=3 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_treatment_completed_childrentransgender()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as treatment_completedcount
					    FROM
					        tblpatient
					    WHERE
					        gender = 3 and age < 18 AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       ETR_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_completed = a.treatment_completedcount  where s.Gender=3 and s.Agewise=2";

		$this->db->query($query);
	}


/*End*/

/*Start eligable for svr*/
public function cascade_eligible_forsvr_adultmale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            AdvisedSVRDate
					) AS dt,

					        count(PatientGUID) as eligible_for_svr
					    FROM
					        tblpatient
					    WHERE
					        gender = 1 and age >= 18 AND AdvisedSVRDate is not null and AdvisedSVRDate!='0000-00-00' AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       AdvisedSVRDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.eligible_for_svr = a.eligible_for_svr  where s.Gender=1 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_eligible_forsvr_childrenmale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            AdvisedSVRDate
					) AS dt,

					        count(PatientGUID) as eligible_for_svr
					    FROM
					        tblpatient
					    WHERE
					        gender = 1 and age < 18 AND AdvisedSVRDate is not null and AdvisedSVRDate!='0000-00-00' AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       AdvisedSVRDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.eligible_for_svr = a.eligible_for_svr  where s.Gender=1 and s.Agewise=2";

		$this->db->query($query);
	}
/*female */
public function cascade_eligible_forsvr_adultfemale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            AdvisedSVRDate
					) AS dt,

					        count(PatientGUID) as eligible_for_svr
					    FROM
					        tblpatient
					    WHERE
					        gender = 2 and age >= 18 AND AdvisedSVRDate is not null and AdvisedSVRDate!='0000-00-00' AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       AdvisedSVRDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.eligible_for_svr = a.eligible_for_svr  where s.Gender=2 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_eligible_forsvr_childrenfemale()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            AdvisedSVRDate
					) AS dt,

					        count(PatientGUID) as eligible_for_svr
					    FROM
					        tblpatient
					    WHERE
					        gender = 2 and age < 18 AND AdvisedSVRDate is not null and AdvisedSVRDate!='0000-00-00' AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       AdvisedSVRDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.eligible_for_svr = a.eligible_for_svr  where s.Gender=2 and s.Agewise=2";

		$this->db->query($query);
	}

	/*transgender*/

	public function cascade_eligible_forsvr_adulttransgender()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            AdvisedSVRDate
					) AS dt,

					        count(PatientGUID) as eligible_for_svr
					    FROM
					        tblpatient
					    WHERE
					        gender = 3 and age >= 18 AND AdvisedSVRDate is not null and AdvisedSVRDate!='0000-00-00' AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       AdvisedSVRDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.eligible_for_svr = a.eligible_for_svr  where s.Gender=3 and s.Agewise=1";

		$this->db->query($query);
	}

	public function cascade_eligible_forsvr_childrentransgender()
	{

			
		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            AdvisedSVRDate
					) AS dt,

					        count(PatientGUID) as eligible_for_svr
					    FROM
					        tblpatient
					    WHERE
					        gender = 3 and age < 18 AND AdvisedSVRDate is not null and AdvisedSVRDate!='0000-00-00' AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       AdvisedSVRDate,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.eligible_for_svr = a.eligible_for_svr  where s.Gender=3 and s.Agewise=2";

		$this->db->query($query);
	}



/*end svr*/


	public function cascade_svr_done()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as SVRDrawndatacount
					    FROM
					        tblpatient
					    WHERE
					        SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       SVR12W_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.svr_done = a.SVRDrawndatacount ";

		$this->db->query($query);
	}

	public function cascade_treatment_successful()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		  $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,

					        COUNT(PatientGUID) as SVR12W_Result
					    FROM
					        tblpatient
					    WHERE
					        SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 and SVR12W_Result=2 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       SVR12W_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_successful = a.SVR12W_Result ";

		$this->db->query($query);
	}

public function cascade_treatment_failure()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		 $query = "UPDATE
					    tblsummary_genderwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,

					        COUNT(PatientGUID) as SVR12W_Result
					    FROM
					        tblpatient
					    WHERE
					        SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and SVR12W_Result=1 
					    GROUP BY
					       SVR12W_HCVViralLoad_Dt,
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.treatment_failure = a.SVR12W_Result ";

		$this->db->query($query);
	}

public function cascade_anti_hepb_positive()
	{

			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HBSRapidResult
					    FROM
					        tblpatient
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00') and (HBSRapidResult=1 || HBSElisaResult = 1 || HBSOtherResult=1)
					    GROUP BY
					       
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hbs_positive = a.HBSRapidResult ";

		$this->db->query($query);
	}


	public function cascade_anti_hepb_screen()
	{

			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HBSRapidResult
					    FROM
					        tblpatient
					    WHERE
					        HbsAg = 1  and (HBSRapidDate is not null || HBSElisaDate is not null || HBSOtherDate is not null) and (HBSRapidDate!='0000-00-00' || HBSElisaDate!='0000-00-00' || HBSOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE((HBSRapidDate), 0), 
								COALESCE((HBSElisaDate), 0),  
								COALESCE((HBSOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND s.date = a.dt
					SET
					    s.anti_hbs_screened = a.HBSRapidResult ";

		$this->db->query($query);
	}

}	