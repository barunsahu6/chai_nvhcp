<?php  
class Offline_monthly_entry_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		// echo "<pre>"; print_r($this->session->userdata('filters')['id_mstfacility']); exit; 
		//pr($this->session->userdata('filters'));


		}
	public function get_info_monthly_record($month=NULL,$year=NULL,$flag=NULL){

		$loginData = $this->session->userdata('loginData'); 

		if ($flag=='hepc') {
			$flag="HCV='1'";
		}
		elseif ($flag=='hepb') {
			$flag="HBV='1'";
		}

		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
		}

		$query ="select * from tbl_offline_monthly_records where month=? and year=? and ".$flag." ".$sess_where;

					     //print_r($query); die();

		$result = $this->db->query($query,[$month,$year])->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
	public function get_monthly_record($month=NULL,$year=NULL,$flag=NULL){

		$loginData = $this->session->userdata('loginData'); 

		if ($flag=='hepc') {
			$flag="HCV='1'";
		}
		elseif ($flag=='hepb') {
			$flag="HBV='1'";
		}

		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
		}

		$query ="select * from tbl_offline_entry where month=? and year=? and ".$flag." ".$sess_where;

					     //print_r($query); die();

		$result = $this->db->query($query,[$month,$year])->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
	public function get_male_monthly_record($month=NULL,$year=NULL,$flag=NULL){

		$loginData = $this->session->userdata('loginData'); 

		if ($flag=='hepc') {
			$flag="HCV='1'";
		}
		elseif ($flag=='hepb') {
			$flag="HBV='1'";
		}


		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
		}

		$query ="select * from tbl_offline_monthly_records where month=? and year=? and genderwise= ? and agewise= ? and ".$flag." ".$sess_where;
					     //print_r($query); die();

		$result = $this->db->query($query,[$month,$year,1,1])->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
	public function get_female_monthly_record($month=NULL,$year=NULL,$flag=NULL){

		$loginData = $this->session->userdata('loginData'); 

		if ($flag=='hepc') {
			$flag="HCV='1'";
		}
		elseif ($flag=='hepb') {
			$flag="HBV='1'";
		}


		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
		}

		$query ="select * from tbl_offline_monthly_records where month=? and year=? and genderwise= ? and agewise= ? and ".$flag." ".$sess_where;

					     //print_r($query); die();

		$result = $this->db->query($query,[$month,$year,2,1])->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
	public function get_transgender_monthly_record($month=NULL,$year=NULL,$flag=NULL){

		$loginData = $this->session->userdata('loginData'); 

		if ($flag=='hepc') {
			$flag="HCV='1'";
		}
		elseif ($flag=='hepb') {
			$flag="HBV='1'";
		}


		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
		}

		$query ="select * from tbl_offline_monthly_records where month=? and year=? and genderwise= ? and agewise= ? and ".$flag." ".$sess_where;

					     //print_r($query); die();

		$result = $this->db->query($query,[$month,$year,3,1])->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
	public function get_children_monthly_record($month=NULL,$year=NULL,$flag=NULL){

		$loginData = $this->session->userdata('loginData'); 

		if ($flag=='hepc') {
			$flag="HCV='1'";
		}
		elseif ($flag=='hepb') {
			$flag="HBV='1'";
		}


		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
		}

		$query ="select * from tbl_offline_monthly_records where month=? and year=? and genderwise= ? and agewise= ? and ".$flag." ".$sess_where;

					     //print_r($query); die();

		$result = $this->db->query($query,[$month,$year,4,2])->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
}