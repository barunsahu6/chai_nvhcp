<?php  
class Dashboard_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		// echo "<pre>"; print_r($this->session->userdata('filters')['id_mstfacility']); exit; 
		//pr($this->session->userdata('filters'));
		if($this->session->userdata('filters') != null)
		{

			
			$filters = $this->session->userdata('filters');
//pr($filters);

			if($filters['startdate']==""){
				$startdateval = date('2019-07-01');
			}else{
				$startdateval = $filters['startdate'];
			}
			if($filters['enddate']==""){
				$enddateval = date('Y-m-01');
			}else{
				$enddateval = $filters['enddate'];
			}

			if($filters['id_mstfacility']      == 0)
				{
				$loginData = $this->session->userdata('loginData'); 

					if( ($loginData) && $loginData->user_type == '1' ){
					$this->facility_filter = " 1";
					$this->facility_filter1 = " 1";
					$this->date_filter      = "CreatedOn >'2018-09-01' AND CreatedOn between '".$startdateval."' and '".$enddateval."'";
					$this->end_dateget = $enddateval;
					$this->id_mstfacilityf = "AND 1";
					}
					elseif( ($loginData) && $loginData->user_type == '2' ){

					$this->facility_filter = " tblsummary_new.Session_StateID = '".$loginData->State_ID."' AND tblsummary_new.Session_DistrictID ='".$loginData->DistrictID."' AND tblsummary_new.id_mstfacility='".$loginData->id_mstfacility."'";
					$this->id_mstfacilityf = "AND 1";

					$this->facility_filter1 = " p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."' AND p.id_mstfacility='".$loginData->id_mstfacility."'";
					$this->id_mstfacilityf = "AND 1";

					}
					elseif( ($loginData) && $loginData->user_type == '3' ){ 
					$this->facility_filter              = " tblsummary_new.Session_StateID = '".$loginData->State_ID."'";
					$this->id_mstfacilityf = "AND 1";
					}
					elseif( ($loginData) && $loginData->user_type == '4' ){ 
					$this->facility_filter = " Session_StateID = '".$loginData->State_ID."' ";

					$this->facility_filter1 = " p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."'";
					}
				}
				else
				{
				
				}

				if($filters['id_search_state']  == 0)
				{
				$loginData = $this->session->userdata('loginData'); 
				$this->facility_state              = " and 1";
				$this->facility_state1              = " and 1";
					//if( ($loginData) && $loginData->user_type == '1' ){
					$this->facility_state  = "AND 1";
					$this->facility_state1  = "AND 1";
					$this->id_mstfacilityf = "AND 1";
					$this->geojson_state= "AND 1";

					//}
					
				}
				else
				{
				$this->facility_state              = " and Session_StateID = ".$filters['id_search_state'];
				$this->facility_statea              = " and Session_StateID = ".$filters['id_search_state'];
				$this->facility_state1              = " p.Session_StateID = ".$filters['id_search_state'];
				$this->id_mstfacilityf = "AND 1";
				$this->geojson_state= "AND id_mststate = ".$filters['id_search_state'];
				}

				if($filters['id_input_district']      == '' || $filters['id_input_district']      == 0 )
				{
				$loginData = $this->session->userdata('loginData'); 

					//if( ($loginData) && $loginData->user_type == '1' ){
					$this->facility_district = "AND 1";
					$this->facility_district1 = "AND 1";
					//}
				$this->facility_district              = "AND 1";	
				}
				else
				{
				 $this->facility_district              = "AND Session_DistrictID = ".$filters['id_input_district'];
				 $this->facility_district1              = "AND p.Session_DistrictID = ".$filters['id_input_district'];
				$this->id_mstfacilityf = "AND 1";
				}

					if( ($filters['id_mstfacility']      == 0 || $filters['id_mstfacility']      == '') )
				{
				$loginData = $this->session->userdata('loginData'); 

					//if( ($loginData) && $loginData->user_type == '1' ){
					//$this->facility_district = "AND 1";
					//$this->facility_district1 = "AND 1";
					//}
				$this->id_mstfacility              = "AND 1";
				$this->id_mstfacilityp              = "AND 1";	
				}
				else
				{
				$this->id_mstfacility              = "AND id_mstfacility = ".$filters['id_mstfacility'];
				$this->id_mstfacilityp              = "AND p.id_mstfacility = ".$filters['id_mstfacility'];
				$this->id_mstfacilitya              = "AND id_mstfacility = ".$filters['id_mstfacility'];
				$this->id_mstfacilityf 				= "AND tblsummary_new.id_mstfacility = ".$filters['id_mstfacility'];
				}
			$this->date_filter = " tblsummary_new.date >'2018-09-01' AND tblsummary_new.date  between '".$startdateval."' and '".$enddateval."'";
			$this->date_filtergen = " p.date >'2018-09-01' AND p.date  between '".$startdateval."' and '".$enddateval."'";
			
/*TAT*/
		if ($enddateval==date('Y-m-d')) {
			$this->T_Initiation = "T_Initiation > '2018-09-01' AND T_Initiation between '".$startdateval."' and  ADDDATE('".$enddateval."',INTERVAL -1 DAY) ";
		$this->T_DLL_01_VLC_Date      = "AND T_DLL_01_VLC_Date > '2018-09-01' AND T_DLL_01_VLC_Date between '".$startdateval."' and ADDDATE('".$enddateval."',INTERVAL -1 DAY) ";
		$this->VLSampleCollectionDate = "AND  VLSampleCollectionDate > '2018-09-01' AND  VLSampleCollectionDate between '".$startdateval."' and ADDDATE('".$enddateval."',INTERVAL -1 DAY) ";
		$this->Prescribing_Dt         = "AND  Prescribing_Dt > '2018-09-01' AND  Prescribing_Dt between '".$startdateval."' and ADDDATE('".$enddateval."',INTERVAL -1 DAY) ";
		$this->PrescribingDate        = "AND  PrescribingDate > '2018-09-01' AND  PrescribingDate between '".$startdateval."' and ADDDATE('".$enddateval."',INTERVAL -1 DAY) ";
		$this->T_Initiationtat          = "AND  T_Initiation > '2018-09-01' AND  T_Initiation between '".$startdateval."' and ADDDATE('".$enddateval."',INTERVAL -1 DAY) ";
		$this->SVR12W_HCVViralLoad_Dt = "AND  SVR12W_HCVViralLoad_Dt > '2018-09-01' AND  SVR12W_HCVViralLoad_Dt between '".$startdateval."' and ADDDATE('".$enddateval."',INTERVAL -1 DAY) ";
		$this->date_filterp = "CreatedOn >'2018-09-01' AND CreatedOn between '".$startdateval."' and ADDDATE('".$enddateval."',INTERVAL -1 DAY) ";
			$this->date_filterjson = "(Case when CreatedOn IS NULL then '2018-09-01' when CreatedOn='0000-00-00' then '2018-09-01' ELSE CreatedOn END) between '".$startdateval."' and  ADDDATE('".$enddateval."',INTERVAL -1 DAY) ";
		}
		else{
			$this->T_Initiation = "T_Initiation > '2018-09-01' AND T_Initiation between '".$startdateval."' and  '".$enddateval."'";
			$this->T_DLL_01_VLC_Date      = "AND T_DLL_01_VLC_Date > '2018-09-01' AND T_DLL_01_VLC_Date between '".$startdateval."' and '".$enddateval."'";
		$this->VLSampleCollectionDate = "AND  VLSampleCollectionDate > '2018-09-01' AND  VLSampleCollectionDate between '".$startdateval."' and '".$enddateval."'";
		$this->Prescribing_Dt         = "AND  Prescribing_Dt > '2018-09-01' AND  Prescribing_Dt between '".$startdateval."' and '".$enddateval."'";
		$this->PrescribingDate        = "AND  PrescribingDate > '2018-09-01' AND  PrescribingDate between '".$startdateval."' and '".$enddateval."'";
		$this->T_Initiationtat          = "AND  T_Initiation > '2018-09-01' AND  T_Initiation between '".$startdateval."' and '".$enddateval."'";
		$this->SVR12W_HCVViralLoad_Dt = "AND  SVR12W_HCVViralLoad_Dt > '2018-09-01' AND  SVR12W_HCVViralLoad_Dt between '".$startdateval."' and '".$enddateval."'";
		$this->date_filterp = "CreatedOn >'2018-09-01' AND CreatedOn between '".$startdateval."' and '".$enddateval."'";
			$this->date_filterjson = "(Case when CreatedOn IS NULL then '2018-09-01' when CreatedOn='0000-00-00' then '2018-09-01' ELSE CreatedOn END) between '".$startdateval."' and  ADDDATE('".$enddateval."',INTERVAL -1 DAY) ";
		}
		



		}

$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
			}

	}
	public function initiated_treatment(){

		

		$query = "SELECT
						
					    ifnull(count(T_Initiation), 0) AS T_Initiation
					FROM
					    `tblpatient`
					WHERE
					     T_Initiation is not null and T_Initiation!='0000-00-00' "  .$sess_where;

					     //print_r($query); die();

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}

public function treatment_completed(){

	$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
			}

		$query = "SELECT
						
					    ifnull(count(*), 0) AS treatment_completed
					FROM
					    `tblpatient`
					WHERE
					     SVR12W_Result=2 and InterruptReason!=1  " .$sess_where;

					     //print_r($query); die();

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}


	public function cascade($excel = NULL)
	{	

		

		 $query = "SELECT
						
					    ifnull(SUM(anti_hcv_screened), 0) AS anti_hcv_screened,
					    ifnull(SUM(anti_hcv_positive), 0) AS anti_hcv_positive,
					    ifnull(SUM(viral_load_tested), 0) AS viral_load_tested,
					    ifnull(SUM(viral_load_detected), 0) AS viral_load_detected,
					    ifnull(SUM(initiatied_on_treatment), 0) AS initiatied_on_treatment,
					    ifnull(SUM(treatment_completed), 0) AS treatment_completed,
					    ifnull(SUM(svr_done), 0) AS svr_done,
					     ifnull(SUM(treatment_successful), 0) AS treatment_successful,
					      ifnull(SUM(treatment_failure), 0) AS treatment_failure
					FROM
					    `tblsummary_new`
					WHERE
					  date > '2018-01-01' AND"  .$this->date_filter.' '.$sess_where .' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;

					     //print_r($query); die();

		//$result = $this->db->query($query)->result();
	    if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}
/*cascase start*/

/*end cascade*/
public function cascade_hav_patients($excel = NULL){


		 $query = "SELECT
						
					    ifnull(SUM(screened_for_hav), 0) AS screened_for_hav,
					    ifnull(SUM(hav_positive_patients), 0) AS hav_positive_patients,
					    ifnull(SUM(patients_managed_at_facility), 0) AS patients_managed_at_facility,
					    ifnull(SUM(patients_referred_for_management), 0) AS patients_referred_for_management
			
					FROM
					    `tblsummary_new`
					WHERE
					   date > '2018-01-01' AND"  .$this->date_filter.''.$sess_where.' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;

					     //print_r($query); die();

		//$result = $this->db->query($query)->result();
						if ($excel== NULL) {
						$result = $this->db->query($query)->result();
						}else{
						$result = $this->db->query($query)->result_array();
						}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}

public function cascade_hev_patients($excel = NULL){

	

		$query = "SELECT
						
					    ifnull(SUM(screened_for_hev), 0) AS screened_for_hev,
					    ifnull(SUM(hev_positive_patients), 0) AS hev_positive_patients,
					    ifnull(SUM(patients_managed_at_facility_hev), 0) AS patients_managed_at_facility_hev,
					    ifnull(SUM(patients_referred_for_management_hev), 0) AS patients_referred_for_management_hev
			
					FROM
					    `tblsummary_new`
					WHERE
					    "  .$this->date_filter.''.$sess_where  .' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;

					     //print_r($query); die();

		//$result = $this->db->query($query)->result();
					    if ($excel== NULL) {
						$result = $this->db->query($query)->result();
						}else{
						$result = $this->db->query($query)->result_array();
						}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}

/*cirrhotic_details*/

public function cirrhotic_details($excel = NULL){

	

		  $query = "SELECT
						
					    ifnull(SUM(non_cirrhotic), 0) AS non_cirrhotic,
					    ifnull(SUM(compensated_cirrhotic), 0) AS compensated_cirrhotic,
					    ifnull(SUM(decompensated_cirrhotic), 0) AS decompensated_cirrhotic,
					    ifnull(SUM(cirrhotic_status_notavaliable), 0) AS cirrhotic_status_notavaliable
			
					FROM
					    `tblsummary_new`
					WHERE
					    "  .$this->date_filter.''.$sess_where.' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;;

					    //print_r($query); die();

		//$result = $this->db->query($query)->result();
	    if ($excel== NULL) {
			$result = $this->db->query($query)->result();
		}else{
			$result = $this->db->query($query)->result_array();
		}


		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}


	
	

	public function cirrhosis_status()
	{

		$query = "SELECT
					    ifnull(sum(cirrhosis_status_yes), 0) as cirrhosis_status_yes,
					    ifnull(sum(cirrhosis_status_no), 0) as cirrhosis_status_no
					FROM
					    `tblsummary_new`
					WHERE
					    ".$this->facility_filter.' and '.$this->date_filter." ";

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}

	public function havhbc_status()
	{

		$query = "SELECT
					    ifnull(sum(HAVRapidResult), 0) as HAVRapidResult,
					    ifnull(sum(HEVRapidResult), 0) as HEVRapidResult
					FROM
					    `tblpatient` as p where '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;
					";

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}

	public function age($excel = NULL)
	{

		

		 $query = "SELECT
							
					    IFNULL(
				         SUM(age_less_than_10),0) AS age_less_than_10,
					    IFNULL( SUM(age_10_to_20),0) AS age_10_to_20,
					     IFNULL(SUM(age_21_to_30),0) AS age_21_to_30,
					     IFNULL(SUM(age_31_to_40),0) AS age_31_to_40,
					     IFNULL(SUM(age_41_to_50),0) AS age_41_to_50,
					     IFNULL(SUM(age_51_to_60),0) AS age_51_to_60,
					    IFNULL( SUM(age_greater_than_60),0) AS age_greater_than_60
					FROM
					    `tblsummary_new`
					WHERE 
					  "  .$this->date_filter.''.$sess_where  .' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;

		//$result = $this->db->query($query)->result();
		  if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}

	
public function risk_factor_analysis($excel = NULL){

	
		 $query = "SELECT
							
					    IFNULL(
				         SUM(gender_risk_factor_male),0) AS gender_risk_factor_male,
					    IFNULL( SUM(gender_risk_factor_female),0) AS gender_risk_factor_female,
					     IFNULL(SUM(gender_risk_factor_transgender),0) AS gender_risk_factor_transgender
					FROM
					    `tblsummary_new`
					WHERE 
					  "  .$this->date_filter.''.$sess_where  .' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;

		//$result = $this->db->query($query)->result();
					  if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}

public function age_wise_vl_detected($excel = NULL){

							
							
							$query = "SELECT
							
							IFNULL(
							SUM(vl_detected_age_less_than_10),0) AS vl_detected_age_less_than_10,
							IFNULL( SUM(vl_detected_age_10_to_20),0) AS vl_detected_age_10_to_20,
							IFNULL(SUM(vl_detected_age_21_to_30),0) AS vl_detected_age_21_to_30,
							IFNULL(SUM(vl_detected_age_31_to_40),0) AS vl_detected_age_31_to_40,
							IFNULL(SUM(vl_detected_age_41_to_50),0) AS vl_detected_age_41_to_50,
							IFNULL(SUM(vl_detected_age_51_to_60),0) AS vl_detected_age_51_to_60,
							IFNULL( SUM(vl_detected_age_greater_than_60),0) AS vl_detected_age_greater_than_60
							FROM
							`tblsummary_new`
							WHERE 
							"  .$this->date_filter.''.$sess_where  .' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;
							
							//$result = $this->db->query($query)->result();
							if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}
							
							if(count($result) == 1)
							{
							return $result[0];
							}
							else
							{
							return $result;
							}

}	

	
	public function lfu()
	{
		$query = "SELECT
						Session_StateID as hospital,
					    IFNULL(SUM(lfu_visits),
					    0) AS lfu_visits,
					    IFNULL(SUM(lfu_svr),
					    0) AS lfu_svr,
					    IFNULL(
					        SUM(lfu_treatment_initiation),
					        0
					    ) AS lfu_treatment_initiation,
					    IFNULL(SUM(lfu_confirmatory),
					    0) AS lfu_confirmatory
					FROM
					    `tblsummary_new`
					WHERE id_mstfacility between 1 and 25 and 
					    ".$this->facility_filter.' and '.$this->date_filter." ";

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}

	public function lfu_facility_wise_data()
	{
		$query = "SELECT
					    f.id_mstfacility,
					    f.facility_name as hospital,
					    IFNULL(SUM(lfu_visits),
					    0) AS lfu_visits,
					    IFNULL(SUM(lfu_svr),
					    0) AS lfu_svr,
					    IFNULL(
					        SUM(lfu_treatment_initiation),
					        0
					    ) AS lfu_treatment_initiation,
					    IFNULL(SUM(lfu_confirmatory),
					    0) AS lfu_confirmatory
					FROM
					    (
					    SELECT
					        id_mstfacility,
					        facility_name
					    FROM
					        mstfacility
					    WHERE
					        id_mstfacility BETWEEN 1 AND 25
					) f
					LEFT JOIN `tblsummary_new` ON
					    f.id_mstfacility = tblsummary_new.id_mstfacility
					WHERE
					    ".$this->facility_filter." and ".$this->date_filter." 
					GROUP BY
					    tblsummary_new.id_mstfacility
					ORDER BY
					    f.id_mstfacility";

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}

	public function follow_up()
	{
		$query = "SELECT
						Session_StateID as hospital,
					    IFNULL(SUM(follow_up_visits),
					    0) AS follow_up_visits,
					    IFNULL(SUM(follow_up_svr),
					    0) AS follow_up_svr,
					    IFNULL(
					        SUM(follow_up_treatment_initiation),
					        0
					    ) AS follow_up_treatment_initiation,
					    IFNULL(SUM(follow_up_confirmatory),
					    0) AS follow_up_confirmatory
					FROM
					    `tblsummary_new`
					WHERE
					    ".$this->facility_filter." ";
					    // .' and '.$this->date_filter;

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}

	

	public function tat()
	{

		if($this->session->userdata('filters')['id_mstfacility'] == 0)
		{
			$facility_filter = "";
		}
		else
		{
			$facility_filter = "AND p.id_mstfacility = ".$this->session->userdata('filters')['id_mstfacility'];
		}

	
		if($this->session->userdata('filters')['category']==3){
			$this->tbpatient_category_filtevalr = " AND p.DiagnosticCentreID >0 AND  p.DiagnosticUID >0";
		}	



		$query = "SELECT
					    cirrhosis_status,
					    AVG(
					        CASE WHEN antibody_hcv <= 0 OR antibody_hcv IS NULL THEN NULL ELSE antibody_hcv
					    END
					) AS one,
					AVG(
					    CASE WHEN hcv_initiation <= 0 OR hcv_initiation IS NULL THEN NULL ELSE hcv_initiation
					END
					) AS two,
					AVG(
					    CASE WHEN initiation_completion <= 0 OR initiation_completion IS NULL THEN NULL ELSE initiation_completion
					END
					) AS three,
					AVG(
					    CASE WHEN completion_svr <= 0 OR completion_svr IS NULL THEN NULL ELSE completion_svr
					END
					) AS four
					FROM
					    `tblpatient_tat_summary` t
					INNER JOIN tblpatient p ON
					    t.patientguid = p.PatientGUID
					WHERE
					    cirrhosis_status >= 1  ";
		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}

	

public function trend()
	{

$end_dateget = $this->end_dateget;
$edate = new DateTime($end_dateget);
$datecheck_now = $edate->format('Y-m-d H:i:s'); 


		 $query = "SELECT
					    YEAR(tblsummary_new.date) AS YEAR,
					    MONTH(tblsummary_new.date) AS MONTH,
					    LEFT(MONTHNAME(tblsummary_new.date),
					    3) AS MONTHNAME,
					    IFNULL(
					        SUM(`anti_hcv_screened`),
					        0
					    ) AS COUNT
					FROM
					    `tblsummary_new`
					    where YEAR(date) *10000+month(date)*100 between year(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH) )*10000+month(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH))*100  and year('".$datecheck_now."')*10000+month('".$datecheck_now."')*100   ".$sess_where."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
					
					GROUP BY
					    YEAR(tblsummary_new.date),
					    MONTH(tblsummary_new.date)
					ORDER BY
					    YEAR(tblsummary_new.date) DESC
					,MONTH(tblsummary_new.date) DESC
					    
					LIMIT 12";

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return array_reverse($result, true);
		}
	}
public function trendanti_hcv_positive(){

$end_dateget = $this->end_dateget;
$edate = new DateTime($end_dateget);
$datecheck_now = $edate->format('Y-m-d H:i:s'); 
				

	$query = "SELECT
					    YEAR(tblsummary_new.date) AS YEAR,
					    MONTH(tblsummary_new.date) AS MONTH,
					    LEFT(MONTHNAME(tblsummary_new.date),
					    3) AS MONTHNAME,
					    IFNULL(
					        SUM(`anti_hcv_positive`),
					        0
					    ) AS COUNT
					FROM
					    `tblsummary_new`  where YEAR(date) *10000+month(date)*100 between year(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH) )*10000+month(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH))*100  and year('".$datecheck_now."')*10000+month('".$datecheck_now."')*100   ".$sess_where."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
					
					GROUP BY
					    YEAR(tblsummary_new.date),
					    MONTH(tblsummary_new.date)
					ORDER BY
					    YEAR(tblsummary_new.date) DESC
					,MONTH(tblsummary_new.date) DESC
					    
					LIMIT 12";

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return array_reverse($result, true);
		}

}

/**/



public function trendviral_load_tested(){

	
$end_dateget = $this->end_dateget;
$edate = new DateTime($end_dateget);
$datecheck_now = $edate->format('Y-m-d H:i:s'); 



	 $query = "SELECT
					    YEAR(tblsummary_new.date) AS YEAR,
					    MONTH(tblsummary_new.date) AS MONTH,
					    LEFT(MONTHNAME(tblsummary_new.date),
					    3) AS MONTHNAME,
					    IFNULL(
					        SUM(`viral_load_tested`),
					        0
					    ) AS COUNT
					FROM
					    `tblsummary_new`
					
					where YEAR(date) *10000+month(date)*100 between year(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH) )*10000+month(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH))*100  and year('".$datecheck_now."')*10000+month('".$datecheck_now."')*100   ".$sess_where."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
					
					GROUP BY
					    YEAR(tblsummary_new.date),
					    MONTH(tblsummary_new.date)
					ORDER BY
					    YEAR(tblsummary_new.date) DESC
					,MONTH(tblsummary_new.date) DESC
					   
					LIMIT 12";

		$result = $this->db->query($query)->result();
		//pr($result);

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return array_reverse($result, true);
		}

}
public function trendviral_load_detected($excel = NULL,$sel_id=NULL){

$end_dateget = $this->end_dateget;
$edate = new DateTime($end_dateget);
$datecheck_now = $edate->format('Y-m-d H:i:s'); 
if($sel_id==NULL)
{
$sel_id='viral_load_detected';
}		

	 $query = "SELECT
					    YEAR(tblsummary_new.date) AS YEAR,
					    MONTH(tblsummary_new.date) AS MONTH,
					    LEFT(MONTHNAME(tblsummary_new.date),
					    3) AS MONTHNAME,
					    IFNULL(
					        SUM(`".$sel_id."`),
					        0
					    ) AS COUNT
					FROM
					    `tblsummary_new`
					
					 where YEAR(date) *10000+month(date)*100 between year(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH) )*10000+month(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH))*100  and year('".$datecheck_now."')*10000+month('".$datecheck_now."')*100   ".$sess_where."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
					
					GROUP BY
					    YEAR(tblsummary_new.date),
					    MONTH(tblsummary_new.date)
					ORDER BY
					    YEAR(tblsummary_new.date) DESC
					,MONTH(tblsummary_new.date) DESC
					   
					LIMIT 12";

		//$result = $this->db->query($query)->result();
				if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return array_reverse($result, true);
		}

}

public function trendinitiatied_on_treatment(){

$end_dateget = $this->end_dateget;
$edate = new DateTime($end_dateget);
$datecheck_now = $edate->format('Y-m-d H:i:s'); 
				
	$query = "SELECT
					    YEAR(tblsummary_new.date) AS YEAR,
					    MONTH(tblsummary_new.date) AS MONTH,
					    LEFT(MONTHNAME(tblsummary_new.date),
					    3) AS MONTHNAME,
					    IFNULL(
					        SUM(`initiatied_on_treatment`),
					        0
					    ) AS COUNT
					FROM
					    `tblsummary_new`
					
					where YEAR(date) *10000+month(date)*100 between year(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH) )*10000+month(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH))*100  and year('".$datecheck_now."')*10000+month('".$datecheck_now."')*100   ".$sess_where."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
					
					GROUP BY
					    YEAR(tblsummary_new.date),
					    MONTH(tblsummary_new.date)
					ORDER BY
					    YEAR(tblsummary_new.date) DESC
					,MONTH(tblsummary_new.date) DESC
					   
					LIMIT 12";

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return array_reverse($result, true);
		}

}

public function trendtreatment_completed(){

$end_dateget = $this->end_dateget;
$edate = new DateTime($end_dateget);
$datecheck_now = $edate->format('Y-m-d H:i:s'); 

	$query = "SELECT
					    YEAR(tblsummary_new.date) AS YEAR,
					    MONTH(tblsummary_new.date) AS MONTH,
					    LEFT(MONTHNAME(tblsummary_new.date),
					    3) AS MONTHNAME,
					    IFNULL(
					        SUM(`treatment_completed`),
					        0
					    ) AS COUNT
					FROM
					    `tblsummary_new`
					
					where YEAR(date) *10000+month(date)*100 between year(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH) )*10000+month(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH))*100  and year('".$datecheck_now."')*10000+month('".$datecheck_now."')*100   ".$sess_where."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
					
					GROUP BY
					    YEAR(tblsummary_new.date),
					    MONTH(tblsummary_new.date)
					ORDER BY
					    YEAR(tblsummary_new.date) DESC
					,MONTH(tblsummary_new.date) DESC
					   
					LIMIT 12";

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return array_reverse($result, true);
		}

}

public function trendsvr_done(){

$end_dateget = $this->end_dateget;
$edate = new DateTime($end_dateget);
$datecheck_now = $edate->format('Y-m-d H:i:s'); 
			
	$query = "SELECT
					    YEAR(tblsummary_new.date) AS YEAR,
					    MONTH(tblsummary_new.date) AS MONTH,
					    LEFT(MONTHNAME(tblsummary_new.date),
					    3) AS MONTHNAME,
					    IFNULL(
					        SUM(`svr_done`),
					        0
					    ) AS COUNT
					FROM
					    `tblsummary_new`
					
					
					 where YEAR(date) *10000+month(date)*100 between year(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH) )*10000+month(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH))*100  and year('".$datecheck_now."')*10000+month('".$datecheck_now."')*100   ".$sess_where."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
					
					GROUP BY
					    YEAR(tblsummary_new.date),
					    MONTH(tblsummary_new.date)
					ORDER BY
					    YEAR(tblsummary_new.date) DESC
					,MONTH(tblsummary_new.date) DESC
					   
					LIMIT 12";

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return array_reverse($result, true);
		}

}

public function trendtreatment_successful(){

$end_dateget = $this->end_dateget;
$edate = new DateTime($end_dateget);
$datecheck_now = $edate->format('Y-m-d H:i:s'); 

	$query = "SELECT
					    YEAR(tblsummary_new.date) AS YEAR,
					    MONTH(tblsummary_new.date) AS MONTH,
					    LEFT(MONTHNAME(tblsummary_new.date),
					    3) AS MONTHNAME,
					    IFNULL(
					        SUM(`treatment_successful`),
					        0
					    ) AS COUNT
					FROM
					    `tblsummary_new`
					
					where YEAR(date) *10000+month(date)*100 between year(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH) )*10000+month(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH))*100  and year('".$datecheck_now."')*10000+month('".$datecheck_now."')*100   ".$sess_where."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
					
					GROUP BY
					    YEAR(tblsummary_new.date),
					    MONTH(tblsummary_new.date)
					ORDER BY
					    YEAR(tblsummary_new.date) DESC
					,MONTH(tblsummary_new.date) DESC
					   
					LIMIT 12";

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return array_reverse($result, true);
		}

}


public function trendtreatment_failure(){


			$end_dateget = $this->end_dateget;
$edate = new DateTime($end_dateget);
$datecheck_now = $edate->format('Y-m-d H:i:s'); 	

	$query = "SELECT
					    YEAR(tblsummary_new.date) AS YEAR,
					    MONTH(tblsummary_new.date) AS MONTH,
					    LEFT(MONTHNAME(tblsummary_new.date),
					    3) AS MONTHNAME,
					    IFNULL(
					        SUM(`treatment_failure`),
					        0
					    ) AS COUNT
					FROM
					    `tblsummary_new`
					
					where YEAR(date) *10000+month(date)*100 between year(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH) )*10000+month(DATE_ADD('".$datecheck_now."',INTERVAL -11 MONTH))*100  and year('".$datecheck_now."')*10000+month('".$datecheck_now."')*100   ".$sess_where."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
					
					GROUP BY
					    YEAR(tblsummary_new.date),
					    MONTH(tblsummary_new.date)
					ORDER BY
					    YEAR(tblsummary_new.date) DESC
					,MONTH(tblsummary_new.date) DESC
					   
					LIMIT 12";

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return array_reverse($result, true);
		}

}

	public function map_data()
	{	

		

		 $query = "SELECT
				    CONCAT(f.City,'-',f.FacilityType) as Facilityname,
				    p.PIN,
					 f.id_mstfacility,
				    f.id_mstdistrict,
				    IFNULL(
				        COUNT(
				            p.PatientGUID
				        ),
				        0
				    ) AS patients
				FROM
				    tblpatient p
				INNER JOIN mstfacility f ON p.id_mstfacility = f.id_mstfacility
				WHERE  "  .$this->T_Initiation.''.$sess_where.' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacilityp. " GROUP BY
				    p.PIN";
				/*$query = "SELECT
				    mstfacility.id_mstfacility,
				    mstfacility.id_mstdistrict,
				    IFNULL(
				        SUM(
				            tblsummary_new.cascade_treatment_initiated
				        ),
				        0
				    ) AS patients
				FROM
				    `tblsummary_new`
				INNER JOIN mstfacility ON tblsummary_new.id_mstfacility = mstfacility.id_mstfacility
				where ".$this->date_filter."
				GROUP BY
				    mstfacility.id_mstfacility";*/

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}

public function regdistribution($excel = NULL){

	

		$query = "SELECT
						
					    ifnull(SUM(regimenwisedistribution_reg1), 0) AS regimenwisedistribution_reg1,
					    ifnull(SUM(regimenwisedistribution_reg2), 0) AS regimenwisedistribution_reg2,
					    ifnull(SUM(regimenwisedistribution_reg3), 0) AS regimenwisedistribution_reg3,
					    ifnull(SUM(regimenwisedistribution_reg24), 0) AS regimenwisedistribution_reg24
			
					FROM
					    `tblsummary_new`
					WHERE
					    "  .$this->date_filter.''.$sess_where.' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;

					    
						if ($excel== NULL) {
						$result = $this->db->query($query)->result();
						}else{
						$result = $this->db->query($query)->result_array();
						}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}



public function regdistribution_svrtested($excel = NULL){

	

		 $query = "SELECT
						
					    ifnull(SUM(reg_pass1), 0) AS reg_pass1,
					    ifnull(SUM(reg_fail1), 0) AS reg_fail1,
					    ifnull(SUM(reg_pass2), 0) AS reg_pass2,
					    ifnull(SUM(reg_fail2), 0) AS reg_fail2,
					     ifnull(SUM(reg_pass3), 0) AS reg_pass3,
					    ifnull(SUM(reg_fail3), 0) AS reg_fail3,
					     ifnull(SUM(reg_pass24), 0) AS reg_pass24,
					    ifnull(SUM(reg_fail24), 0) AS reg_fail24
			
					FROM
					    `tblsummary_new`
					WHERE
					    "  .$this->date_filter.''.$sess_where.' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;

					     //print_r($query); die();

		//$result = $this->db->query($query)->result();
						if ($excel== NULL) {
						$result = $this->db->query($query)->result();
						}else{
						$result = $this->db->query($query)->result_array();
						}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}



	public function regimen($excel = NULL)
	{
		

		   $query = "SELECT
					 
					    IFNULL( COUNT(PatientGUID),0) AS COUNT,
					    T_Regimen
					FROM
					    `tblpatient`
					WHERE
					    T_Initiation IS NOT NULL AND T_Regimen > 0  and ".$this->T_Initiation." ".$sess_where." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
					GROUP BY
					    T_Regimen";

		//$result = $this->db->query($query)->result();
		if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
	}

	public function regimen24()
	{
			

		   $query = "SELECT
					    COUNT(PatientGUID) AS COUNT
					FROM
					    `tblpatient`
					WHERE
					    T_Initiation IS NOT NULL AND T_Regimen =2 and T_DurationValue=24  ".$sess_where." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility." and ".$this->T_Initiation."
					";

		$result = $this->db->query($query)->result();
		

		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
	}

/**/

public function regimensucess($excel = NULL)
	{
		
		  $query = "SELECT
					    COUNT(PatientGUID) AS COUNT,
					    T_Regimen
					FROM
					    `tblpatient`
					WHERE
					    PrescribingDate IS NOT NULL AND T_Regimen > 0 and SVR12W_Result=2    ".$sess_where." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility." and  ".$this->T_Initiation."
					GROUP BY
					    T_Regimen";
//and SVR12W_Result=2
		//$result = $this->db->query($query)->result();

		if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
	}

	public function regimensucess24($excel = NULL)
	{
			

		   $query = "SELECT
					    COUNT(PatientGUID) AS COUNT
					FROM
					    `tblpatient`
					WHERE
					    PrescribingDate IS NOT NULL AND T_Regimen =3 and SVR12W_Result=2 and T_DurationValue=24  ".$sess_where." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility." and ".$this->T_Initiation." 
					";
//and SVR12W_Result=2
		//$result = $this->db->query($query)->result();
		if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
	}

/*failure*/
public function regimenfailure($excel = NULL)
	{
		

		  $query = "SELECT
					   IFNULL( COUNT(PatientGUID),0) AS COUNT,
					    T_Regimen
					FROM
					    `tblpatient`
					WHERE
					    (PrescribingDate is NULL)  and SVR12W_Result=1  ".$sess_where." and ".$this->T_Initiation."
					GROUP BY
					    T_Regimen";
//and SVR12W_Result=1
		//$result = $this->db->query($query)->result();
	    if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
	}

	public function regimenfailure24($excel = NULL)
	{
			
		   $query = "SELECT
					    COUNT(PatientGUID) AS COUNT
					FROM
					    `tblpatient`
					WHERE
					    (PrescribingDate =NULL or PrescribingDate='') AND T_Regimen =2 and SVR12W_Result=1 and T_DurationValue=24   ".$sess_where." and ".$this->T_Initiation."
					";
//and SVR12W_Result=1
		//$result = $this->db->query($query)->result();
		if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
	}

public function anty_hcv_to_tesult(){

	

   $query = "SELECT CEIL(COUNT(DATEDIFF( T_DLL_01_VLC_Date,HCVRapidDate))/2) as median ,sum(DATEDIFF( T_DLL_01_VLC_Date,HCVRapidDate)) as count from tblpatient where HCVRapidDate is not null and HCVRapidDate!='0000-00-00' and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='' ".$sess_where."";
$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}
public function anty_hcv_to_tesult_median(){


		$query = "SELECT CEIL(COUNT(DATEDIFF( T_DLL_01_VLC_Date,HCVRapidDate))/2) as median ,sum(DATEDIFF( T_DLL_01_VLC_Date,HCVRapidDate)) as count from tblpatient where HCVRapidDate is not null and HCVRapidDate!='0000-00-00' and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='' ".$sess_where."";

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}

public function vl_samples_accepted_rejected(){

	

		$query = "SELECT
						
					    ifnull(SUM(vl_samples_accepted), 0) AS vl_samples_accepted,
					    ifnull(SUM(vl_samples_rejected), 0) AS vl_samples_rejected
			
					FROM
					    `tblsummary_new`
					WHERE
					    "  .$this->date_filter.''.$sess_where.' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;

					     //print_r($query); die();

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}

public function treatment_Initiations_by_District($excel = NULL,$sel_id=NULL)
{

if($sel_id==NULL)
{
$sel_id='anti_hcv_screened';
}
$loginData = $this->session->userdata('loginData');

if( ($loginData) && $loginData->user_type == '1' ){
$sess_where = "AND 1";
$id_mststate = " where 1";
}

elseif( ($loginData) && $loginData->user_type == '3' ){
$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
$id_mststate = "where id_mststate = ".$loginData->State_ID."";
}else{
$sess_where="";
$id_mststate="";
}


 $query = "SELECT
   concat(f.City, '-', f.FacilityType) AS hospital,
   ifnull(SUM($sel_id), 0) AS countIni,
   'rgba(53, 196, 249)' AS color
FROM
(
   SELECT
       *
   FROM
       mstfacility  ".$id_mststate."
   
) f
LEFT JOIN
   `tblsummary_new`
ON
   tblsummary_new.id_mstfacility = f.id_mstfacility
WHERE
   (
      date > '2018-01-01' AND  ".$this->date_filter.' '.$sess_where. ' '.$this->facility_state."
   )
GROUP BY
   tblsummary_new.id_mstfacility ";

//' .$this->facility_district . ' ' .$this->id_mstfacilityf.
//$result = $this->db->query($query)->result();
if ($excel== NULL) {
$result = $this->db->query($query)->result();
}else{
$result = $this->db->query($query)->result_array();
}

if(count($result) == 1)
{
return $result[0];
}
else
{
return $result;
}
}

	public function treatment_Initiations_by_Districthav($excel = NULL,$sel_id=NULL)
{

$loginData = $this->session->userdata('loginData');
if($sel_id==NULL)
{
$sel_id="screened_for_hav";
}
if( ($loginData) && $loginData->user_type == '1' ){
$sess_where = "AND 1";
$id_mststate = " where 1";
}

elseif( ($loginData) && $loginData->user_type == '3' ){
$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
$id_mststate = " where  id_mststate = ".$loginData->State_ID."";
}else{
$sess_where="";
$id_mststate="";
}


 $query = "SELECT
  concat(f.City, '-', f.FacilityType) AS hospital,
   ifnull(SUM(tblsummary_new.".$sel_id."), 0) AS countIni,
  'rgba(53, 196, 249)' AS color
FROM
(
   SELECT
       *
   FROM
       mstfacility  ".$id_mststate."
   
) f
LEFT JOIN
   `tblsummary_new`
ON
   tblsummary_new.id_mstfacility = f.id_mstfacility
WHERE
   (
     date > '2018-01-01' AND ".$this->date_filter.''.$sess_where. ' '.$this->facility_state."
   )
GROUP BY
   tblsummary_new.id_mstfacility";

//$result = $this->db->query($query)->result();
if ($excel== NULL) {
$result = $this->db->query($query)->result();
}else{
$result = $this->db->query($query)->result_array();
}

if(count($result) == 1)
{
return $result[0];
}
else
{
return $result;
}
}

	public function treatment_Initiations_by_Districthev($excel = NULL,$sel_id=NULL)
{

$loginData = $this->session->userdata('loginData');
if($sel_id==NULL)
{
$sel_id="screened_for_hev";
}
if( ($loginData) && $loginData->user_type == '1' ){
$sess_where = "AND 1";
$id_mststate = "where 1";
}

elseif( ($loginData) && $loginData->user_type == '3' ){
$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
$id_mststate = "where id_mststate = ".$loginData->State_ID."";
}else{
$sess_where="";
$id_mststate="";
}


$query = "SELECT
   concat(f.City, '-', f.FacilityType) AS hospital,
   ifnull(SUM(tblsummary_new.".$sel_id."), 0) AS countIni,
   'rgba(53, 196, 249)' AS color
FROM
(
   SELECT
       *
   FROM
       mstfacility  ".$id_mststate."
   
) f
LEFT JOIN
   `tblsummary_new`
ON
   tblsummary_new.id_mstfacility = f.id_mstfacility
WHERE
   (
     date > '2018-01-01' AND ".$this->date_filter.''.$sess_where. ' '.$this->facility_state."
   )
GROUP BY
   tblsummary_new.id_mstfacility";

//$result = $this->db->query($query)->result();
if ($excel== NULL) {
$result = $this->db->query($query)->result();
}else{
$result = $this->db->query($query)->result_array();
}

if(count($result) == 1)
{
return $result[0];
}
else
{
return $result;
}
}

	public function treatment_by_Districtsvrsuc($excel = NULL)
	{

		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				 $sess_where = "AND 1";
				 $id_mststate = "where 1";
			}
		
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				 $sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
				 $id_mststate = "where id_mststate = ".$loginData->State_ID."";
			}else{
				$sess_where="";
				 $id_mststate="";
			}
		

		 $query = "SELECT
					    concat(f.City, '-', f.FacilityType) AS hospital,
					    ifnull(SUM(tblsummary_new.treatment_successful), 0) AS countIni,
					    '#18bb4b' AS color
					FROM
					(
					    SELECT
					        *
					    FROM
					        mstfacility  ".$id_mststate."
					    
					) f
					LEFT JOIN
					    `tblsummary_new`
					ON
					    tblsummary_new.id_mstfacility = f.id_mstfacility
					WHERE
					    (
					        ".$this->date_filter.''.$sess_where. ' '.$this->facility_state."
					    ) 
					GROUP BY
					    tblsummary_new.id_mstfacility
					    UNION
							SELECT
							'State Average' AS hospital,
							(ifnull(sum(tblsummary_new.treatment_successful),0)*100)/sum(ifnull(tblsummary_new.treatment_successful,0)+ifnull(tblsummary_new.treatment_failure,0)) AS countIni,
								'#64b5f6' AS color
							FROM
							(
							SELECT
							*
							FROM
							mstfacility
							
							) f
							LEFT JOIN 
							
							`tblsummary_new` 
							ON
							tblsummary_new.id_mstfacility = f.id_mstfacility
							WHERE
							(
							".$this->date_filter.''.$sess_where. ' '.$this->facility_state."
							) 
							ORDER BY
							hospital
					    ";

		//$result = $this->db->query($query)->result();
		if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}

	public function treatment_by_Districtsvrunsuc($excel = NULL)
	{

		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
				 $id_mststate = "where 1";
			}
		
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
				 $id_mststate = "where id_mststate = ".$loginData->State_ID."";
			}else{
				$sess_where="";
				 $id_mststate="";
			}
		

		 $query = "SELECT
					   concat(f.City, '-', f.FacilityType) AS hospital,
					    ifnull(SUM(tblsummary_new.treatment_failure), 0) AS countIni,
					   '#f4511e' AS color
					FROM
					(
					    SELECT
					        *
					    FROM
					        mstfacility  ".$id_mststate."
					    
					) f
					LEFT JOIN
					    `tblsummary_new`
					ON
					    tblsummary_new.id_mstfacility = f.id_mstfacility
					WHERE
					    (
					        ".$this->date_filter.''.$sess_where. ' '.$this->facility_state."
					    ) 
					GROUP BY
					    tblsummary_new.id_mstfacility
					    UNION
							SELECT
							'State Average' AS hospital,
							(ifnull(sum(tblsummary_new.treatment_failure),0)*100)/sum(ifnull(tblsummary_new.treatment_successful,0)+ifnull(tblsummary_new.treatment_failure,0)) AS countIni,
							'#f4511e' AS color
							FROM
							(
							SELECT
							*
							FROM
							mstfacility
							
							) f
							LEFT JOIN 
							
							`tblsummary_new` 
							ON
							tblsummary_new.id_mstfacility = f.id_mstfacility
							WHERE
							(
							".$this->date_filter.''.$sess_where. ' '.$this->facility_state."
							) 
							ORDER BY
							hospital
					    ";

		//$result = $this->db->query($query)->result();

	    		if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}

/*Start Loss Follow Up across districts number*/

public function lossupollowupacrossdistict($excel = NULL){

	$loginData = $this->session->userdata('loginData'); 
	$filters = $this->session->userdata('filters');
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
				 $id_mststate = "where 1";
			}
		
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
				 $id_mststate = "where id_mststate = ".$loginData->State_ID."";
			}else{
				$sess_where="";
				 $id_mststate="";
			}

			if($filters['id_search_state']!='' || $filters['id_search_state']!=0){
				
				$id_mststate = "where id_mststate = ".$filters['id_search_state']."";
			}

    $query = "SELECT
					     concat(f.City, '-', f.FacilityType) AS hospital,
					     countIni,
					    'rgba(53, 196, 249)' AS color
					     FROM
					     (
					    SELECT
					        *
					    FROM mstfacility ".$id_mststate."  order by City asc
					    
					) f left join 
					        (select count(PatientGUID) AS countIni,id_mstfacility  from tblpatient p  WHERE
					       p.AntiHCV=1 and  p.T_DLL_01_VLC_Date is not null and p.T_DLL_01_VLC_Result=1 and  DATEDIFF (now(), p.T_DLL_01_VLC_Date) >7 and (p.T_Initiation is null ||  p.T_Initiation='0000-00-00'  ".$this->T_DLL_01_VLC_Date."  ".$sess_where." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility.")  GROUP BY
					        p.id_mstfacility) d on
						d.id_mstfacility = f.id_mstfacility   group by f.id_mstfacility ORDER BY hospital";
					/*UNION
						SELECT
					    'National Average' AS hospital,
					      AVG(countIni) AS countIni,
					    '#64b5f6' AS color
					     FROM
					     (
					    SELECT
					        *
					    FROM mstfacility ".$id_mststate."  order by City asc
					    
					) f left join 
					        (select COUNT(PatientGUID) AS countIni,id_mstfacility from tblpatient p  WHERE
					       p.AntiHCV=1 and  p.T_DLL_01_VLC_Date is not null and p.T_DLL_01_VLC_Result=1 and  DATEDIFF (now(), p.T_DLL_01_VLC_Date) >7 and (p.T_Initiation is null ||  p.T_Initiation='0000-00-00')  GROUP BY
					        p.id_mstfacility) d on
					d.id_mstfacility = f.id_mstfacility
					ORDER BY hospital";*/

		//$result = $this->db->query($query)->result();
					    if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}

/*Turn-Around Time Analysis across districts*/
public function trunaroundtimeanalys_districts($excel = NULL){


	$loginData = $this->session->userdata('loginData'); 
	$filters = $this->session->userdata('filters');
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
				 $id_mststate = "and 1";
			}
		
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
				 $id_mststate = "and id_mststate = ".$loginData->State_ID."";
			}else{
				$sess_where="";
				 $id_mststate="";
			}

			if($filters['id_search_state']!='' || $filters['id_search_state']!=0){
				
				$id_mststate = "and id_mststate = ".$filters['id_search_state']."";
			}


	 $query="select hospital ,id_mstfacility,T_Initiation AS dtval, avg(dt) as countIni,'rgba(53, 196, 249)' AS color from (
select concat(s.City, '-', s.FacilityType)  AS hospital,p.id_mstfacility,patientguid,T_Initiation,datediff(p.T_DLL_01_VLC_Date,
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								)) as dt from tblpatient p  right join mstfacility s on s.id_mstfacility=p. id_mstfacility where p.T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' and (HCVRapidDate is not null || HCVElisaDate is not null  || HCVOtherDate is not null ) AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' and datediff(p.T_DLL_01_VLC_Date,
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								)) >0 ".$this->T_DLL_01_VLC_Date."  ".$id_mststate." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility.")aa  group by id_mstfacility
						UNION
						select 'State  Average' AS hospital ,id_mstfacility,T_Initiation AS dtval, avg(dt) as countIni,'#64b5f6' AS color from (
select concat(s.City, '-', s.FacilityType)  AS hospital,p.id_mstfacility,patientguid,T_Initiation,datediff(p.T_DLL_01_VLC_Date,
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								)) as dt from tblpatient p  right join mstfacility s on s.id_mstfacility=p. id_mstfacility where p.T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' and (HCVRapidDate is not null || HCVElisaDate is not null  || HCVOtherDate is not null ) AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' and datediff(p.T_DLL_01_VLC_Date,
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								)) >0 ".$this->T_DLL_01_VLC_Date."  ".$id_mststate." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility.")aa 
										
							";

 if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}

/*end Turn-Around Time Analysis across districts*/

/*Follow Up across districts end */
public function lossfollowacrossdist($excel = NULL,$sel_id=NULL){


$loginData = $this->session->userdata('loginData');
if($sel_id==NULL)
{
$sel_id='reg_wise_reg1_SOF_DCV';
}
if( ($loginData) && $loginData->user_type == '1' ){
$sess_where = "AND 1";
$id_mststate = "where 1";
}

elseif( ($loginData) && $loginData->user_type == '3' ){
$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
$id_mststate = "where id_mststate = ".$loginData->State_ID."";
}else{
$sess_where="";
$id_mststate="";
}


 $query = "SELECT
  concat(f.City, '-', f.FacilityType) AS hospital,
   ifnull(SUM(tblsummary_new.".$sel_id."), 0) AS countIni,
   'rgba(53, 196, 249)' AS color
FROM
(
   SELECT
       *
   FROM
       mstfacility  ".$id_mststate."
   
) f
LEFT JOIN
   `tblsummary_new`
ON
   tblsummary_new.id_mstfacility = f.id_mstfacility
WHERE
   (
       ".$this->date_filter.''.$sess_where. ' '.$this->facility_state."
   )
GROUP BY
   tblsummary_new.id_mstfacility ORDER BY
hospital
   ";
 /*  UNION
SELECT
'State Average' AS hospital,
avg((tblsummary_new.".$sel_id.")) AS countIni,
 '#64b5f6' AS color
FROM
(
SELECT
*
FROM
mstfacility

) f
LEFT JOIN

`tblsummary_new`
ON
tblsummary_new.id_mstfacility = f.id_mstfacility
WHERE
(
".$this->date_filter.''.$sess_where. ' '.$this->facility_state."
)
ORDER BY
hospital
   ";*/

//$result = $this->db->query($query)->result();
   if ($excel== NULL) {
$result = $this->db->query($query)->result();
}else{
$result = $this->db->query($query)->result_array();
}

if(count($result) == 1)
{
return $result[0];
}
else
{
return $result;
}
}


public function no_of_patients_eligible_stage($excel = NULL){

	

		 $query = "SELECT
						
					    ifnull(SUM(follow_viral_load), 0) AS follow_viral_load,
					    ifnull(SUM(follow_1St_dispensation), 0) AS follow_1St_dispensation,
					    ifnull(SUM(follow_2St_dispensation), 0) AS follow_2St_dispensation,
					    ifnull(SUM(follow_3St_dispensation), 0) AS follow_3St_dispensation,
					    ifnull(SUM(follow_4St_dispensation), 0) AS follow_4St_dispensation,
					    ifnull(SUM(follow_5St_dispensation), 0) AS follow_5St_dispensation,
					    ifnull(SUM(follow_6St_dispensation), 0) AS follow_6St_dispensation,
					    ifnull(SUM(follow_SVR), 0) AS follow_SVR
					FROM
					    `tblsummary_new`
					WHERE
					    "  .$this->date_filter.''.$sess_where.' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;;


		//$result = $this->db->query($query)->result();
		if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}

//


//

/*Turn-Around Time Analysis*/
public function actual_days_taken($excel = NULL){

	
		 $query = "SELECT
						
					    ifnull(SUM(anty_hcv_test_to_anti_hcv_result), 0) AS anty_hcv_test_to_anti_hcv_result,
					    ifnull(SUM(anti_hcv_result_to_viral_load_test), 0) AS anti_hcv_result_to_viral_load_test,
					    ifnull(SUM(viral_load_test_to_delivery_of_vl_result_patient), 0) AS viral_load_test_to_delivery_of_vl_result_patient,
					    ifnull(SUM(delivery_result_to_patient_prescription_base_line_test), 0) AS delivery_result_to_patient_prescription_base_line_test,
					    ifnull(SUM(prescription_baseline_testing_date_baseline_tests), 0) AS prescription_baseline_testing_date_baseline_tests,
					    ifnull(SUM(date_of_baseline_tests_result_baseline_tests), 0) AS date_of_baseline_tests_result_baseline_tests,
					    ifnull(SUM(result_of_baseline_tests_to_initiation_of_treatment), 0) AS result_of_baseline_tests_to_initiation_of_treatment,
					    ifnull(SUM(treatment_completion_to_SVR), 0) AS treatment_completion_to_SVR
					FROM
					    `tblsummary_new`
					WHERE
					    "  .$this->date_filter.''.$sess_where.' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;


		//$result = $this->db->query($query)->result();
					    if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}
public function state_tat_median(){


}

public function no_of_patients_ltfu_stage($excel = NULL){

	

		 $query = "SELECT
						
					    ifnull(SUM(loss_of_follow_viral_load), 0) AS loss_of_follow_viral_load,
					    ifnull(SUM(loss_of_follow_1St_dispensation), 0) AS loss_of_follow_1St_dispensation,
					    ifnull(SUM(loss_of_follow_2St_dispensation), 0) AS loss_of_follow_2St_dispensation,
					    ifnull(SUM(loss_of_follow_3St_dispensation), 0) AS loss_of_follow_3St_dispensation,
					    ifnull(SUM(loss_of_follow_4St_dispensation), 0) AS loss_of_follow_4St_dispensation,
					    ifnull(SUM(loss_of_follow_5St_dispensation), 0) AS loss_of_follow_5St_dispensation,
					    ifnull(SUM(loss_of_follow_6St_dispensation), 0) AS loss_of_follow_6St_dispensation,
					    ifnull(SUM(loss_of_follow_SVR), 0) AS loss_of_follow_SVR
					FROM
					    `tblsummary_new`
					WHERE
					    "  .$this->date_filter.''.$sess_where   .' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;


		//$result = $this->db->query($query)->result();
	    		if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}

/*Lost to follow up to special case*/
public function lost_to_followup_special_case($excel = NULL){

		$query = "SELECT
						
					    ifnull(SUM(loss_to_follow_patient_referred_to_mtc), 0) AS loss_to_follow_patient_referred_to_mtc,
					    ifnull(SUM(loss_to_follow_patient_reporting_at_mtc), 0) AS loss_to_follow_patient_reporting_at_mtc,
					    ifnull(SUM(loss_to_follow_up), 0) AS loss_to_follow_up			
					FROM
					    `tblsummary_new`
					WHERE
					    "  .$this->date_filter.''.$sess_where.' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;

					     //print_r($query); die();

		//$result = $this->db->query($query)->result();
					    if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}

public function screening_vs_treatment_initiations(){

		

		$query = "SELECT
						
					    ifnull(SUM(anti_scv_screened_male), 0) AS anti_scv_screened_male,
					    ifnull(SUM(anti_scv_screened_female), 0) AS anti_scv_screened_female,
					    ifnull(SUM(anti_scv_screened_transgender), 0) AS anti_scv_screened_transgender,	
					    ifnull(SUM(vl_test_male), 0) AS vl_test_male,
					    ifnull(SUM(vl_test_female), 0) AS vl_test_female,
					    ifnull(SUM(vl_test_transgender), 0) AS vl_test_transgender,
					    ifnull(SUM(treatment_initiations_male), 0) AS treatment_initiations_male,
					    ifnull(SUM(treatment_initiations_female), 0) AS treatment_initiations_female,
					    ifnull(SUM(treatment_initiations_transgender), 0) AS treatment_initiations_transgender		
					FROM
					    `tblsummary_new`
					WHERE
					     "  .$this->date_filter.' '.$sess_where .' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;

					     //print_r($query); die();

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}

/*National dashboard*/

public function screened_antihcv_across_states($sel_id=NULL){

if($sel_id==NULL)
{
$sel_id='anti_hcv_screened';
}
$loginData = $this->session->userdata('loginData');

if( ($loginData) && $loginData->user_type == '1' ){
$sess_where = "AND 1";
$id_mststate = "1";
}

elseif( ($loginData) && $loginData->user_type == '3' ){
$sess_where = "AND 1";//"AND Session_StateID = '".$loginData->State_ID."'";
$id_mststate = " 1";//"id_mststate = ".$loginData->State_ID."";
}else{
$sess_where="";
$id_mststate="";
}

   $query = "SELECT
   f.StateName AS hospital,
   ifnull(t.".$sel_id.", 0) AS countIni,
   'rgba(53, 196, 249)' AS color
FROM
(
   SELECT
       *
   FROM
       mststate order by StateName asc
   
) f
LEFT JOIN
   (Select sum(".$sel_id.") AS ".$sel_id.",Session_StateID from tblsummary_new 
WHERE
      ".$this->date_filter.''.$sess_where."
GROUP BY Session_StateID) AS t
ON
    f.id_mststate=t.Session_StateID 
 order by f.StateName asc";

$result = $this->db->query($query)->result();

if(count($result) == 1)
{
return $result[0];
}
else
{
return $result;
}
}




	public function treatment_Initiations_by_statehev($excel = NULL,$sel_id=NULL)
{

$loginData = $this->session->userdata('loginData');
if($sel_id==NULL)
{
$sel_id='screened_for_hev';
}
if( ($loginData) && $loginData->user_type == '1' ){
$sess_where = "AND 1";
$id_mststate = " 1";
}

elseif( ($loginData) && $loginData->user_type == '3' ){
$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
$id_mststate = "id_mststate = ".$loginData->State_ID."";
}else{
$sess_where="";
$id_mststate="";
}


$query = "SELECT
   f.StateName AS hospital,
   ifnull(t.".$sel_id.", 0) AS countIni,
   'rgba(53, 196, 249)' AS color
FROM
(
   SELECT
       *
   FROM
       mststate order by StateName asc
   
) f
LEFT JOIN
   (Select sum(".$sel_id.") AS ".$sel_id.",Session_StateID from tblsummary_new 
WHERE
      ".$this->date_filter.''.$sess_where."
GROUP BY Session_StateID) AS t
ON
    f.id_mststate=t.Session_StateID 
 order by f.StateName asc";


//$result = $this->db->query($query)->result();
if ($excel== NULL) {
$result = $this->db->query($query)->result();
}else{
$result = $this->db->query($query)->result_array();
}

if(count($result) == 1)
{
return $result[0];
}
else
{
return $result;
}
}


public function treatment_by_statesvrsucess($excel = NULL)
	{

		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				 $id_mststate = "1";
			}
		
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " tblsummary_new.Session_StateID = '".$loginData->State_ID."'";
				 $id_mststate = "tblsummary_new.id_mststate = ".$loginData->State_ID."";
			}else{
				$sess_where="";
				 $id_mststate="";
			}
						   


			$query="SELECT
		   				 f.StateName AS hospital,
		   					    ifnull(t.reg_pass1, 0) AS countIni,
		   					    '#18bb4b' AS color
		   					FROM
		   					(
		   					     SELECT
		   					        *
		   					    FROM mststate order by StateName asc
		   					    
		   					) f
		   					LEFT JOIN
		   					(SELECT Session_StateID,SUM(reg_pass1) AS reg_pass1 FROM tblsummary_new where ".$this->date_filter." AND ".$sess_where." 
		   					GROUP BY Session_StateID) AS t
		   					ON
		   					   t.Session_StateID = f.id_mststate 
		   				
			    UNION
		   
		   	SELECT
					'National Average' AS hospital,
			  	(ifnull(sum(t.reg_pass1),0)*100)/sum(ifnull(t.reg_pass1,0)+ifnull(t.reg_fail1,0)) AS countIni,
					'#64b5f6' AS color
			FROM
			(
			     SELECT
			        *
			    FROM mststate order by StateName asc
			    
			) f
			LEFT JOIN
			(SELECT Session_StateID,sum(reg_pass1) AS reg_pass1,sum(reg_fail1) AS reg_fail1 FROM tblsummary_new where ".$this->date_filter." AND ".$sess_where."  GROUP BY Session_StateID)  AS t
			ON
			   t.Session_StateID = f.id_mststate 
		   	ORDER BY hospital ASC" ;




		 /*echo $query = "SELECT
					   f.StateName AS hospital,
					    ifnull(SUM(tblsummary_new.reg_pass1), 0) AS countIni,
					    '#18bb4b' AS color
					FROM
					(
					     SELECT
					        *
					    FROM mststate order by StateName asc
					    
					) f
					LEFT JOIN
					    `tblsummary_new`
					ON
					   tblsummary_new.Session_StateID = f.id_mststate 
					where ".$this->date_filter." AND ".$sess_where." 
					GROUP BY
					    tblsummary_new.Session_StateID
					    UNION
							SELECT
							'National Average' AS hospital,
								(ifnull(sum(tblsummary_new.reg_pass1),0)*100)/sum(ifnull(tblsummary_new.reg_pass1,0)+ifnull(tblsummary_new.reg_fail1,0)) AS countIni,
							'#64b5f6' AS color
							FROM
							(
							 SELECT
					        *
					    FROM mststate order by StateName asc
							
							) f
							LEFT JOIN 
							
							`tblsummary_new` 
							ON
							 tblsummary_new.Session_StateID = f.id_mststate
							
							ORDER BY
							hospital
					    ";*/

		//$result = $this->db->query($query)->result();

	    		if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}



public function treatment_by_statesvrunsuc($excel = NULL)
	{

		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
				 $id_mststate = "1";
			}
		
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
				 $id_mststate = "id_mststate = ".$loginData->State_ID."";
			}else{
				$sess_where="";
				 $id_mststate="";
			}
		
$query="SELECT
		   				 f.StateName AS hospital,
		   					    ifnull(t.reg_fail1, 0) AS countIni,
		   					    '#f4511e' AS color
		   					FROM
		   					(
		   					     SELECT
		   					        *
		   					    FROM mststate order by StateName asc
		   					    
		   					) f
		   					LEFT JOIN
		   					(SELECT Session_StateID,SUM(reg_fail1) AS reg_fail1 FROM tblsummary_new where ".$this->date_filter." ".$sess_where." 
		   					GROUP BY Session_StateID) AS t
		   					ON
		   					   t.Session_StateID = f.id_mststate 
		   				
			    UNION
		   
		   	SELECT
					'National Average' AS hospital,
			  	(ifnull(sum(t.reg_fail1),0)*100)/sum(ifnull(t.reg_pass1,0)+ifnull(t.reg_fail1,0)) AS countIni,
					'#f4511e' AS color
			FROM
			(
			     SELECT
			        *
			    FROM mststate order by StateName asc
			    
			) f
			LEFT JOIN
			(SELECT Session_StateID,sum(reg_pass1) AS reg_pass1,sum(reg_fail1) AS reg_fail1 FROM tblsummary_new where ".$this->date_filter." ".$sess_where."  GROUP BY Session_StateID)  AS t
			ON
			   t.Session_StateID = f.id_mststate 
		   	ORDER BY hospital ASC" ;







/*
		$query = "SELECT
					   f.StateName AS hospital,
					    ifnull(SUM(tblsummary_new.reg_fail1), 0) AS countIni,
					   '#f4511e' AS color
					FROM
					(
					     SELECT
					        *
					    FROM mststate order by StateName asc
					    
					) f
					LEFT JOIN
					    `tblsummary_new`
					ON
					   tblsummary_new.Session_StateID = f.id_mststate
				where ".$this->date_filter." ".$sess_where." 
					GROUP BY
					    tblsummary_new.Session_StateID
					    UNION
							SELECT
							'National Average' AS hospital,
							(ifnull(sum(tblsummary_new.reg_fail1),0)*100)/sum(ifnull(tblsummary_new.reg_pass1,0)+ifnull(tblsummary_new.reg_fail1,0)) AS countIni,
							'#f4511e' AS color
							FROM
							(
							 SELECT
					        *
					    FROM mststate order by StateName asc
							
							) f
							LEFT JOIN 
							
							`tblsummary_new` 
							ON
							 tblsummary_new.Session_StateID = f.id_mststate
							
							ORDER BY
							hospital asc
					    ";*/

		//$result = $this->db->query($query)->result();

	    		if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}



public function lossfollowacrossstate($excel = NULL){

	$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
				 $id_mststate = "where 1";
			}
		
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
				 $id_mststate = "where id_mststate = ".$loginData->State_ID."";
			}else{
				$sess_where="";
				 $id_mststate="";
			}
		

		/* $query = "SELECT
					    f.StateName AS hospital,
					    (ifnull(SUM(loss_of_follow_1St_dispensation), 0) + ifnull(sum(loss_of_follow_2St_dispensation), 0) + ifnull(sum(loss_of_follow_3St_dispensation), 0) +  ifnull(sum(loss_of_follow_4St_dispensation), 0)  +  ifnull(SUM(loss_of_follow_5St_dispensation), 0) + ifnull(SUM(loss_of_follow_6St_dispensation), 0) ) AS countIni,
					    'rgba(53, 196, 249)' AS color
					FROM
					(
					    SELECT
					        *
					    FROM mststate order by StateName asc
					    
					) f
					LEFT JOIN
					    `tblsummary_new`
					ON
					     tblsummary_new.Session_StateID = f.id_mststate
					WHERE
					    (
					        ".$this->date_filter.''.$sess_where."
					    ) 
					GROUP BY
					    tblsummary_new.Session_StateID
					    UNION
							select 'National Average' AS hospital,avg(countIni) as countIni ,'rgba(118, 238, 0, 1)' AS color from(
SELECT
							'National Average' AS hospital,
						f.statename,
							(ifnull(SUM(loss_of_follow_1St_dispensation), 0) + ifnull(sum(loss_of_follow_2St_dispensation), 0) + ifnull(sum(loss_of_follow_3St_dispensation), 0) +  ifnull(sum(loss_of_follow_4St_dispensation), 0)  +  ifnull(SUM(loss_of_follow_5St_dispensation), 0) + ifnull(SUM(loss_of_follow_6St_dispensation), 0) )  AS countIni,
							'rgba(118, 238, 0, 1)' AS color
							FROM
							(
							SELECT
					        *
					    FROM mststate order by StateName asc
							
							) f
							LEFT JOIN 
							
							`tblsummary_new` 
							ON
							 tblsummary_new.Session_StateID = f.id_mststate  WHERE
					    (
					        ".$this->date_filter.''.$sess_where."
					    ) 
							 group by f.statename)a where countini is not null ORDER BY hospital
							
							
					    ";*/

					   $query = "SELECT
					    f.StateName AS hospital,
					     countIni,
					    'rgba(53, 196, 249)' AS color
					     FROM
					     (
					    SELECT
					        *
					    FROM mststate order by StateName asc
					    
					) f left join 
					        (select count(PatientGUID) AS countIni,Session_StateID  from tblpatient p  WHERE
					       p.AntiHCV=1 and  p.T_DLL_01_VLC_Date is not null and p.T_DLL_01_VLC_Result=1 and  DATEDIFF (now(), p.T_DLL_01_VLC_Date) >7 and (p.T_Initiation is null ||  p.T_Initiation='0000-00-00') ".$this->T_DLL_01_VLC_Date."  GROUP BY
					        p.Session_StateID) d on
					d.Session_StateID = f.id_mststate   group by f.id_mststate ORDER BY hospital";
					/*UNION
						SELECT
					    'National Average' AS hospital,
					      AVG(countIni) AS countIni,
					    '#64b5f6' AS color
					     FROM
					     (
					    SELECT
					        *
					    FROM mststate order by StateName asc
					    
					) f left join 
					        (select COUNT(PatientGUID) AS countIni,Session_StateID from tblpatient p  WHERE
					       p.AntiHCV=1 and  p.T_DLL_01_VLC_Date is not null and p.T_DLL_01_VLC_Result=1 and  DATEDIFF (now(), p.T_DLL_01_VLC_Date) >7 and (p.T_Initiation is null ||  p.T_Initiation='0000-00-00') ".$this->T_DLL_01_VLC_Date."  GROUP BY
					        p.Session_StateID) d on
					d.Session_StateID = f.id_mststate
					ORDER BY hospital  ";*/

		//$result = $this->db->query($query)->result();
					    if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}


public function Adherenceaccrossstate($sel_id=NULL){

$loginData = $this->session->userdata('loginData');
if($sel_id==NULL)
{
$sel_id='reg_wise_reg1_SOF_DCV';
}else {
	$sel_id=$sel_id;
}
if( ($loginData) && $loginData->user_type == '1' ){
$sess_where = "AND 1";
$id_mststate = "where 1";
}

elseif( ($loginData) && $loginData->user_type == '3' ){
$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
$id_mststate = "where id_mststate = ".$loginData->State_ID."";
}else{
$sess_where="";
$id_mststate="";
}


  $query = "SELECT
   f.StateName AS hospital,
   ifnull(t.".$sel_id.", 0) AS countIni,
   'rgba(53, 196, 249)' AS color
FROM
(
   SELECT
       *
   FROM
       mststate order by StateName asc
   
) f
LEFT JOIN
   (Select avg(".$sel_id.") AS ".$sel_id.",Session_StateID from tblsummary_new 
WHERE
      ".$this->date_filter.''.$sess_where."
GROUP BY Session_StateID) AS t
ON
    f.id_mststate=t.Session_StateID 
 order by f.StateName asc";
/*
$query = "SELECT
   f.StateName AS hospital,
   ifnull(SUM(tblsummary_new.".$sel_id."), 0) AS countIni,
   'rgba(53, 196, 249)' AS color
FROM
(
   SELECT
       *
   FROM mststate order by StateName asc
   
) f
LEFT JOIN
   `tblsummary_new`
ON
    tblsummary_new.Session_StateID = f.id_mststate
WHERE
   (
       ".$this->date_filter."".$sess_where."
   )
GROUP BY
   tblsummary_new.Session_StateID ORDER BY
hospital
   ";*/
/*national Average commented*/

  /* UNION
SELECT
'National Average' AS hospital,
ifnull(SUM(tblsummary_new.".$sel_id.") / 23, 0) AS countIni,
 '#64b5f6' AS color
FROM
(
SELECT
       *
   FROM mststate order by StateName asc

) f
LEFT JOIN

`tblsummary_new`
ON
tblsummary_new.Session_StateID = f.id_mststate
WHERE
(
".$this->date_filter."".$sess_where."
)
ORDER BY
hospital
   ";*/

$result = $this->db->query($query)->result();


if(count($result) == 1)
{
return $result[0];
}
else
{
return $result;
}
}





public function treatment_Initiations_by_Districthav_state($excel = NULL,$sel_id=NULL)
{

$loginData = $this->session->userdata('loginData');
if($sel_id==NULL)
{
$sel_id='screened_for_hav';
}
if( ($loginData) && $loginData->user_type == '1' ){
$sess_where = "AND 1";
$id_mststate = "1";
}

elseif( ($loginData) && $loginData->user_type == '3' ){
$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
$id_mststate = "id_mststate = ".$loginData->State_ID."";
}else{
$sess_where="";
$id_mststate="";
}


$query = "SELECT
   f.StateName AS hospital,
   ifnull(t.".$sel_id.", 0) AS countIni,
   'rgba(53, 196, 249)' AS color
FROM
(
   SELECT
       *
   FROM
       mststate order by StateName asc
   
) f
LEFT JOIN
   (Select sum(".$sel_id.") AS ".$sel_id.",Session_StateID from tblsummary_new 
WHERE
      ".$this->date_filter.''.$sess_where."
GROUP BY Session_StateID) AS t
ON
    f.id_mststate=t.Session_StateID 
 order by f.StateName asc";

//$result = $this->db->query($query)->result();
if ($excel== NULL) {
$result = $this->db->query($query)->result();
}else{
$result = $this->db->query($query)->result_array();
}

if(count($result) == 1)
{
return $result[0];
}
else
{
return $result;
}
}



	public function facilities($fields = "*")
	{
		$query = "SELECT
					    ".$fields."
					FROM
					    `mstfacility`
					WHERE
					    id_mstfacility BETWEEN 1 AND 25";

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}


/*adherence percentage*/

public function adherencepercentagedata($excel = NULL)
	{
		

 
		   $query ="SELECT case when T_DurationValue=24 and p.T_Regimen=3 then 'regimen4' else t_regimen end as COUNTval,avg(v.Adherence)as COUNT from tblpatient p inner join tblpatientvisit v on p.PatientGUID=v.PatientGUID where v.Adherence>0 AND  ".$this->T_Initiation." ".$sess_where ." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacilityp." group by case when T_DurationValue=24 and p.T_Regimen=3 then 'regimen4' else t_regimen end  ";	 
//and SVR12W_Result=1
		//$result = $this->db->query($query)->result();
	    if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
	}

	public function adherencepercentagedd($excel = NULL)
	{
	

		   $query = "select case when T_DurationValue=24 and p.T_Regimen=3 then 'regimen4' else t_regimen end as COUNT,avg(v.Adherence)as COUNT from tblpatient p inner join tblpatientvisit v on p.PatientGUID=v.PatientGUID where v.Adherence>0 ".$sess_where."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacilityp." group by case when T_DurationValue=24 and p.T_Regimen=3 then 'regimen4' else t_regimen end ";
//and SVR12W_Result=1
		//$result = $this->db->query($query)->result();
		if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
	}
/*speacl case*/
public function special_casedata_exp($excel = NULL){

	

 $sql = "SELECT count(patientguid) as count from tblpatient p where PatientType=2 and AntiHCV=1 AND ".$this->date_filterp." ".$sess_where." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."";
		if ($excel== NULL) {
		$result = $this->db->query($sql)->result();
		}else{
		$result = $this->db->query($sql)->result_array();
		}
		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}

	}
	
	public function special_casedata_durgs($excel = NULL){

		


		 $sql = "SELECT count(patientguid) as count,m.LookupValue,p.Risk,m.LookupCode FROM mstlookup m left join tblpatient p  ON find_in_set(m.LookupCode,p.Risk) where m.flag = 3 and p.AntiHCV=1 and m.LookupCode=2 and m.LanguageID = 1 AND ".$this->date_filterp." ".$sess_where." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."  group by m.LookupCode order by m.Sequence ASC";
		if ($excel== NULL) {
		$result = $this->db->query($sql)->result();
		}else{
		$result = $this->db->query($sql)->result_array();
		}
		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
	}

	public function special_casedata_kidn($excel = NULL){	

		

		$sql = "select count(p.PatientGUID) as count from tblpatient p inner join tblRiskProfile r on p.PatientGUID=r.PatientGUID where  AntiHCV=1 and r.RiskID=13 AND ".$this->date_filterp." ".$sess_where." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."";
		if ($excel== NULL) {
		$result = $this->db->query($sql)->result();
		}else{
		$result = $this->db->query($sql)->result_array();
		}
		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
}
public function special_casedata_hivhcv($excel = NULL){




		$sql = "select count(p.PatientGUID) as count from tblpatient p inner join tblRiskProfile r on p.PatientGUID=r.PatientGUID where r.RiskID=2 and p.AntiHCV=1 AND ".$this->date_filterp."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility." ".$sess_where." ";
		if ($excel== NULL) {
		$result = $this->db->query($sql)->result();
		}else{
		$result = $this->db->query($sql)->result_array();
		}
		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
}
public function special_casedata_hbvhcv($excel = NULL){

	
		$sql = "select count(p.PatientGUID) as count from tblpatient p inner join tblRiskProfile r on p.PatientGUID=r.PatientGUID where r.RiskID=4 and p.AntiHCV=1  AND ".$this->date_filterp." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility." ".$sess_where." ";
		if ($excel== NULL) {
		$result = $this->db->query($sql)->result();
		}else{
		$result = $this->db->query($sql)->result_array();
		}
		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
}
public function special_casedata_pregnant($excel = NULL){

	

		$sql = "SELECT count(patientguid) as count from tblpatient p where Pregnant=1  and AntiHCV=1 AND ".$this->date_filterp." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility." ".$sess_where." ";
		if ($excel== NULL) {
		$result = $this->db->query($sql)->result();
		}else{
		$result = $this->db->query($sql)->result_array();
		}
		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
}

public function age_gpdata($excel = NULL,$sel_id=NULL){


	$loginData = $this->session->userdata('loginData');
		//echo "<pre>";print_r($loginData);
		if($sel_id==NULL){
			$sel_id='1';

		}
		
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_wherep = " 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' ";
			}

 $sql = "SELECT LookupValue , count(patientguid) as count FROM (select * from  mstlookup  where flag=62 )l left join (select patientguid,t_initiation,risk,case when  age <10 THEN 1  WHEN age >= 10 AND AGE<20 THEN 2 WHEN AGE>=20 AND AGE<30 THEN 3 WHEN AGE>=30 AND AGE<40 THEN 4 WHEN AGE >=40 AND AGE<50 THEN 5 WHEN AGE>=50 AND AGE<60 THEN 6 WHEN AGE>=60 THEN 7 END as ageband from tblpatient WHERE  AntiHCV=1 and T_Initiation is not null and T_Initiation!='0000-00-00' AND find_in_set(".$sel_id.",RISK) and ".$sess_wherep."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility." )p on p.ageband=l.lookupcode        group by  lookupvalue order by Sequence asc";
		

		if ($excel== NULL) {
		$result = $this->db->query($sql)->result();
		}else{
		$result = $this->db->query($sql)->result_array();
		}
		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}

}

public function age_gender($excel = NULL,$sel_id=NULL){

	$loginData = $this->session->userdata('loginData');
		//echo "<pre>";print_r($loginData);
if($sel_id==NULL){
	$sel_id='1';
}
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_wherep = " 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' ";
			}


		 $sql = "SELECT LookupValue ,    count(patientguid) as count FROM (select * from  mstlookup  where flag=1 and LanguageID=1 )l left join (select patientguid,t_initiation,risk,gender from tblpatient WHERE  AntiHCV=1 and T_Initiation is not null   and T_Initiation!='0000-00-00'  AND find_in_set(13,RISK) and T_Initiation!='0000-00-00' AND find_in_set(".$sel_id.",RISK) and ".$sess_wherep."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility.")p on p.gender=l.lookupcode        group by  LookupValue order by Sequence asc";
		if ($excel== NULL) {
		$result = $this->db->query($sql)->result();
		}else{
		$result = $this->db->query($sql)->result_array();
		}
		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}

}

public function age_gpdatavlded($excel = NULL){

	$loginData = $this->session->userdata('loginData');
		//echo "<pre>";print_r($loginData);

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_wherep = " 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' ";
			}

		 $sql = "SELECT LookupValue ,    count(patientguid) as count FROM (select * from  mstlookup  where flag=62 )l left join (select patientguid,t_initiation,risk,case when  age <10 THEN 1  WHEN age >= 10 AND AGE<20 THEN 2 WHEN AGE>=20 AND AGE<30 THEN 3 WHEN AGE>=30 AND AGE<40 THEN 4 WHEN AGE >=40 AND AGE<50 THEN 5 WHEN AGE>=50 AND AGE<60 THEN 6 WHEN AGE>=60 THEN 7 END as ageband from tblpatient WHERE  AntiHCV=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Result = 1 and T_DLL_01_VLC_Date!='0000-00-00'  and ".$sess_wherep."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility." ".$this->T_DLL_01_VLC_Date.")p on p.ageband=l.lookupcode      group by  LookupValue order by Sequence asc";
		if ($excel== NULL) {
		$result = $this->db->query($sql)->result();
		}else{
		$result = $this->db->query($sql)->result_array();
		}
		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}

}



public function side_effects($excel = NULL){

	$loginData = $this->session->userdata('loginData');
		//echo "<pre>";print_r($loginData);

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_wherep = " 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' ";
			}

  $sql = "SELECT count(patientguid) as count,m.LookupValue,p.Risk,m.LookupCode FROM mstlookup m left join tblpatient p  ON find_in_set(m.LookupCode,p.Risk) where m.flag = 3 and m.LanguageID = 1 and p.AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1   and ".$sess_where."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility." group by m.LookupCode order by m.Sequence ASC";
		if ($excel== NULL) {
		$result = $this->db->query($sql)->result();
		}else{
		$result = $this->db->query($sql)->result_array();
		}
		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
}


/*Turn-Around Time Analysis update summary table start*/

public function anti_hcv_result_to_viral_load_test($excel = NULL){


	$loginData = $this->session->userdata('loginData');
		//echo "<pre>";print_r($loginData);

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_wherep = " 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " Session_StateID = '".$loginData->State_ID."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " Session_StateID = '".$loginData->State_ID."' ";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' ";
			}
	 $sql = "select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select id_mstfacility,patientguid,T_Initiation,datediff(p.T_DLL_01_VLC_Date,
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								)) as dt from tblpatient p where p.T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' and (HCVRapidDate is not null || HCVElisaDate is not null  || HCVOtherDate is not null ) AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' and datediff(p.T_DLL_01_VLC_Date,
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								)) >0  ".$this->T_DLL_01_VLC_Date." AND ".$sess_where. ' '.$this->facility_state.' ' .$this->facility_district . ' ' .$this->id_mstfacility.")  a
								
					";

						if ($excel== NULL) {
						$result = $this->db->query($sql)->result();
						}else{
						$result = $this->db->query($sql)->result_array();
						}
						if(count($result) == 1)
						{
						return $result;
						}
						else
						{
						return $result;
						}
}

public function viral_load_test_to_delivery_of_vl_result_patient($excel = NULL){

		 $sql = "select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select id_mstfacility,patientguid,T_Initiation,datediff(p.T_DLL_01_VLC_Date,
							VLSampleCollectionDate) as dt from tblpatient p where p.T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00'  AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' and datediff(p.T_DLL_01_VLC_Date,
							VLSampleCollectionDate) >0 ".$this->VLSampleCollectionDate."  ".$sess_where. ' '.$this->facility_state.' ' .$this->facility_district . ' ' .$this->id_mstfacility." )  a
								
					
					";
					    if ($excel== NULL) {
						$result = $this->db->query($sql)->result();
						}else{
						$result = $this->db->query($sql)->result_array();
						}
						if(count($result) == 1)
						{
						return $result;
						}
						else
						{
						return $result;
						}
}


public function delivery_result_to_patient_prescription_base_line_test($excel = NULL){

 $sql = "select id_mstfacility,T_Initiation AS dtval, FORMAT (avg(dt),0 ) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,c.Prescribing_Dt,datediff(c.Prescribing_Dt,
							p.T_DLL_01_VLC_Date) as dt from tblpatient p LEFT JOIN tblpatientcirrohosis c on p.PatientGUID=c.PatientGUID  where p.T_DLL_01_VLC_Date is not null and p.T_DLL_01_VLC_Date!='0000-00-00' AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00' and datediff(c.Prescribing_Dt,
							p.T_DLL_01_VLC_Date) >0 ".$this->Prescribing_Dt." ".$sess_where. ' '.$this->facility_state.' ' .$this->facility_district . ' ' .$this->id_mstfacility.")  a
								
					
					 ";
					   
					 if ($excel== NULL) {
						$result = $this->db->query($sql)->result();
						}else{
						$result = $this->db->query($sql)->result_array();
						}
						if(count($result) == 1)
						{
						return $result;
						}
						else
						{
						return $result;
						}

}


public function prescription_baseline_testing_date_baseline_tests($excel = NULL){

	$sql = "select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,c.Prescribing_Dt,p.PrescribingDate,datediff(p.PrescribingDate,
							c.Prescribing_Dt) as dt from tblpatient p LEFT JOIN tblpatientcirrohosis c on p.PatientGUID=c.PatientGUID  where p.PrescribingDate is not null and p.PrescribingDate!='0000-00-00' AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00' and datediff(p.PrescribingDate,
							c.Prescribing_Dt) >0  ".$this->PrescribingDate."  ".$sess_where. ' '.$this->facility_state.' ' .$this->facility_district . ' ' .$this->id_mstfacility.")  a
								
					
					";
					    if ($excel== NULL) {
						$result = $this->db->query($sql)->result();
						}else{
						$result = $this->db->query($sql)->result_array();
						}
						if(count($result) == 1)
						{
						return $result;
						}
						else
						{
						return $result;
						}

}

public function date_of_baseline_tests_result_baseline_tests($excel = NULL){

	$sql = "select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,c.Prescribing_Dt,p.PrescribingDate,datediff(p.PrescribingDate,
							c.Prescribing_Dt) as dt from tblpatient p LEFT JOIN tblpatientcirrohosis c on p.PatientGUID=c.PatientGUID  where p.PrescribingDate is not null and p.PrescribingDate!='0000-00-00' AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00' and datediff(p.PrescribingDate,
							c.Prescribing_Dt) >0  ".$this->PrescribingDate." ".$sess_where. ' '.$this->facility_state.' ' .$this->facility_district . ' ' .$this->id_mstfacility.")  a
								
					
					";
					    if ($excel== NULL) {
						$result = $this->db->query($sql)->result();
						}else{
						$result = $this->db->query($sql)->result_array();
						}
						if(count($result) == 1)
						{
						return $result;
						}
						else
						{
						return $result;
						}
}


public function result_of_baseline_tests_to_initiation_of_treatment($excel = NULL){

	$sql = "select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,c.Prescribing_Dt,p.PrescribingDate,datediff(p.T_Initiation,
							p.PrescribingDate) as dt from tblpatient p LEFT JOIN tblpatientcirrohosis c on p.PatientGUID=c.PatientGUID  where p.PrescribingDate is not null and p.PrescribingDate!='0000-00-00' AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00' and datediff(T_Initiation,
							p.PrescribingDate) >0 ".$this->T_Initiationtat." ".$sess_where. ' '.$this->facility_state.' ' .$this->facility_district . ' ' .$this->id_mstfacility.")  a
								
					";
					    if ($excel== NULL) {
						$result = $this->db->query($sql)->result();
						}else{
						$result = $this->db->query($sql)->result_array();
						}
						if(count($result) == 1)
						{
						return $result;
						}
						else
						{
						return $result;
						}
}

public function treatment_completion_to_SVR($excel = NULL){
//SVR12W_HCVViralLoad_Dt
	 $sql = "select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,p.ETR_HCVViralLoad_Dt,p.PrescribingDate,datediff(p.SVRDrawnDate,
							p.ETR_HCVViralLoad_Dt) as dt from tblpatient p  where p.ETR_HCVViralLoad_Dt is not null and p.ETR_HCVViralLoad_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00'  and ETR_HCVViralLoad_Dt!='1900-01-19' and datediff(SVR12W_HCVViralLoad_Dt,
							p.ETR_HCVViralLoad_Dt) >0 ".$this->SVR12W_HCVViralLoad_Dt." ".$sess_where. ' '.$this->facility_state.' ' .$this->facility_district . ' ' .$this->id_mstfacility.")  a
								
					";
					   if ($excel== NULL) {
						$result = $this->db->query($sql)->result();
						}else{
						$result = $this->db->query($sql)->result_array();
						}
						if(count($result) == 1)
						{
						return $result;
						}
						else
						{
						return $result;
						}
}

/*end Turn-Around Time Analysis*/


public function TurnAroundAnalysisState($excel = NULL){

	$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
				 $id_mststate = "where 1";
			}
		
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
				 $id_mststate = "where id_mststate = ".$loginData->State_ID."";
			}else{
				$sess_where="";
				 $id_mststate="";
			}
		

		
					   /* $query = "SELECT
					    f.StateName AS hospital,
					    (ifnull(SUM(anti_hcv_result_to_viral_load_test), 0) + ifnull(sum(viral_load_test_to_delivery_of_vl_result_patient), 0) + ifnull(sum(delivery_result_to_patient_prescription_base_line_test), 0) +  ifnull(sum(prescription_baseline_testing_date_baseline_tests), 0)  +  ifnull(SUM(date_of_baseline_tests_result_baseline_tests), 0) + ifnull(SUM(result_of_baseline_tests_to_initiation_of_treatment), 0) + ifnull(SUM(treatment_completion_to_SVR), 0)) AS countIni,
					    'rgba(53, 196, 249)' AS color
					FROM
					(
					    SELECT
					        *
					    FROM mststate order by StateName asc
					    
					) f
					LEFT JOIN
					    `tblsummary_new`
					ON
					     tblsummary_new.Session_StateID = f.id_mststate
					WHERE
					    (
					        ".$this->date_filter.''.$sess_where."
					    ) 
					GROUP BY
					    tblsummary_new.Session_StateID
					    UNION
							SELECT
							'National Average' AS hospital,
							
							avg(anti_hcv_result_to_viral_load_test + viral_load_test_to_delivery_of_vl_result_patient + delivery_result_to_patient_prescription_base_line_test +  prescription_baseline_testing_date_baseline_tests  + date_of_baseline_tests_result_baseline_tests )  AS countIni,
							'rgba(118, 238, 0, 1)' AS color
							FROM
							(
							SELECT
					        *
					    FROM mststate order by StateName asc
							
							) f
							LEFT JOIN 
							
							`tblsummary_new` 
							ON
							 tblsummary_new.Session_StateID = f.id_mststate
							WHERE
							(
							".$this->date_filter.''.$sess_where."
							) 
							ORDER BY
							hospital
					    ";*/

					    $query = "select StateName AS hospital,Session_StateID,T_Initiation AS dtval, avg(dt) as countIni,'rgba(53, 196, 249)' AS color from (
select StateName,Session_StateID,patientguid,T_Initiation,datediff(p.T_DLL_01_VLC_Date,
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								)) as dt from tblpatient p  right join mststate s on s.id_mststate=p. Session_StateID where p.T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' and (HCVRapidDate is not null || HCVElisaDate is not null  || HCVOtherDate is not null ) AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' and datediff(p.T_DLL_01_VLC_Date,
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								)) >0 ".$this->T_DLL_01_VLC_Date." ".$sess_where. ' '.$this->facility_state.' ' .$this->facility_district . ' ' .$this->id_mstfacility.")aa group by Session_StateID";


		//$result = $this->db->query($query)->result();
					    if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}
public function map_national($excel = NULL)
	{	
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}

		 /* $query = "SELECT COUNT(Session_DistrictID), Session_DistrictID FROM `tblpatient`
					WHERE
					    "  .$this->date_filterp.' '.$sess_where .' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility." GROUP by Session_DistrictID ORDER BY `COUNT(Session_DistrictID)` DESC";*/

				$query="SELECT s.json_state_id,s.StateName,ifnull(t.count,0) as count FROM
						(SELECT json_state_id,StateName,id_mststate FROM gjson_district group BY id_mststate)AS s
						LEFT JOIN
						(SELECT COUNT(anti_hcv_positive) AS count,Session_StateID,Session_DistrictID FROM tblsummary_new 
					WHERE
					    ".$this->date_filter.' '.$sess_where .' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility." GROUP by Session_StateID) AS t
						ON 
						t.Session_StateID=s.id_mststate order BY s.StateName asc";
					     //print_r($query); die();

		//$result = $this->db->query($query)->result();
	    if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}
public function map_state($excel = NULL)
	{	

		$loginData = $this->session->userdata('loginData'); 
		//pr($loginData);
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
				$sess_wheres = "1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_wheres = "id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
				$sess_wheres = "id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
				$sess_wheres = "1";
			}

		 /* $query = "SELECT COUNT(Session_DistrictID), Session_DistrictID FROM `tblpatient`
					WHERE
					    "  .$this->date_filterp.' '.$sess_where .' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility." GROUP by Session_DistrictID ORDER BY `COUNT(Session_DistrictID)` DESC";*/

				 $query="SELECT s.json_state_id,s.StateName,json_district_id,DistrictName,lt.Latitude AS Latitude,lt.Longitude as Longitude,ifnull(t.count,0) as count FROM
					(SELECT json_state_id,StateName,id_mststate,json_district_id,DistrictName,id_mstdistrict FROM gjson_district 
					WHERE "
					.$sess_wheres." ".$this->geojson_state."
					 group BY id_mstdistrict)AS s
					LEFT JOIN
					(SELECT COUNT(anti_hcv_positive) AS count,Session_StateID,Session_DistrictID FROM tblsummary_new 
					WHERE
					    ".$this->date_filter.' '.$sess_where .' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility." GROUP by Session_DistrictID) AS t
					ON 
					t.Session_StateID=s.id_mststate AND t.Session_DistrictID=s.id_mstdistrict
					LEFT JOIN
					(SELECT id_mststate,Latitude,Longitude FROM mststatelatlongs GROUP by id_mststate) AS lt
					ON 
					lt.id_mststate=s.id_mststate
					 order BY s.StateName ASC,s.DistrictName asc";
					     //print_r($query); die();

		//$result = $this->db->query($query)->result();
	    if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}

/*hep-b*/

public function cascade_hepb($excel = NULL)
	{	

		
		 $query = "SELECT
						
					    ifnull(SUM(anti_hbs_screened), 0) AS anti_hbs_screened,
					    ifnull(SUM(anti_hbs_positive), 0) AS anti_hbs_positive,
					    ifnull(SUM(TestForALT_Level), 0) AS TestForALT_Level,
					    ifnull(SUM(Cirrhotic), 0) AS Cirrhotic,
					    ifnull(SUM(ElevatedALT_Level), 0) AS ElevatedALT_Level,
					    ifnull(SUM(EligibleHBV_DNATest), 0) AS EligibleHBV_DNATest,
					    ifnull(SUM(HBVDNA_TestConducted), 0) AS HBVDNA_TestConducted,
					     ifnull(SUM(EligibleFor_TreatmentHBV), 0) AS EligibleFor_TreatmentHBV,
					      ifnull(SUM(InitiatedOnTreatmentHEPB), 0) AS InitiatedOnTreatmentHEPB,
					       ifnull(SUM(ContinuedOnTreatment), 0) AS ContinuedOnTreatment
					FROM
					    `tblsummary_new`
					WHERE
					  date > '2018-01-01' AND"  .$this->date_filter.' '.$sess_where .' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;

					     //print_r($query); die();

		//$result = $this->db->query($query)->result();
	    if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}


public function trend_hepb()
	{

		$loginData = $this->session->userdata('loginData'); 
		if( ($loginData) && $loginData->user_type == '1' ){
							$sess_where = "AND 1";
							}
							elseif( ($loginData) && $loginData->user_type == '2' ){
							
							$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
							}
							elseif( ($loginData) && $loginData->user_type == '3' ){ 
							 $sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
							}
							elseif( ($loginData) && $loginData->user_type == '4' ){ 
							$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
							}
		 $query = "SELECT
					    YEAR(tblsummary_new.date) AS YEAR,
					    MONTH(tblsummary_new.date) AS MONTH,
					    LEFT(MONTHNAME(tblsummary_new.date),
					    3) AS MONTHNAME,
					    IFNULL(
					        SUM(`InitiatedOnTreatmentHEPB`),
					        0
					    ) AS COUNT
					FROM
					    `tblsummary_new`
					    where YEAR(date) *10000+month(date)*100 between year(DATE_ADD(now(),INTERVAL -11 MONTH) )*10000+month(DATE_ADD(now(),INTERVAL -11 MONTH))*100  and year(now())*10000+month(now())*100   ".$sess_where."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
					
					GROUP BY
					    YEAR(tblsummary_new.date),
					    MONTH(tblsummary_new.date)
					ORDER BY
					    YEAR(tblsummary_new.date) DESC
					,MONTH(tblsummary_new.date) DESC
					    
					LIMIT 12";

		$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return array_reverse($result, true);
		}
	}


public function monthwisetraending($sel_id=NULL,$excel = NULL){

if($sel_id==NULL)
{
$sel_id='InitiatedOnTreatmentHEPB';
}else{
$sel_id = $sel_id;	
}
$loginData = $this->session->userdata('loginData');

if( ($loginData) && $loginData->user_type == '1' ){
							$sess_where = "AND 1";
							}
							elseif( ($loginData) && $loginData->user_type == '2' ){
							
							$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
							}
							elseif( ($loginData) && $loginData->user_type == '3' ){ 
							 $sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
							}
							elseif( ($loginData) && $loginData->user_type == '4' ){ 
							$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
							}

    $query = "SELECT
					    YEAR(tblsummary_new.date) AS YEAR,
					    MONTH(tblsummary_new.date) AS MONTH,
					    LEFT(MONTHNAME(tblsummary_new.date),
					    3) AS MONTHNAME,
					    IFNULL(
					        SUM(".$sel_id."),
					        0
					    ) AS COUNT
					FROM
					    `tblsummary_new`
					    where YEAR(date) *10000+month(date)*100 between year(DATE_ADD(now(),INTERVAL -11 MONTH) )*10000+month(DATE_ADD(now(),INTERVAL -11 MONTH))*100  and year(now())*10000+month(now())*100   ".$sess_where."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
					
					GROUP BY
					    YEAR(tblsummary_new.date),
					    MONTH(tblsummary_new.date)
					ORDER BY
					    YEAR(tblsummary_new.date) DESC
					,MONTH(tblsummary_new.date) DESC
					    
					LIMIT 12";

$result = $this->db->query($query)->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return array_reverse($result, true);
		}

}

public function monthwise_indicator($sel_id=NULL){

if($sel_id==NULL)
{
$sel_id='InitiatedOnTreatmentHEPB';
}else{
$sel_id = $sel_id;	
}
$loginData = $this->session->userdata('loginData');

if( ($loginData) && $loginData->user_type == '1' ){
							$sess_where = "AND 1";
							}
							elseif( ($loginData) && $loginData->user_type == '2' ){
							
							$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
							}
							elseif( ($loginData) && $loginData->user_type == '3' ){ 
							 $sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
							}
							elseif( ($loginData) && $loginData->user_type == '4' ){ 
							$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
							}

    $query = "SELECT
					    YEAR(tblsummary_new.date) AS YEAR,
					    MONTH(tblsummary_new.date) AS MONTH,
					    LEFT(MONTHNAME(tblsummary_new.date),
					    3) AS MONTHNAME,
					    IFNULL(
					        SUM(".$sel_id."),
					        0
					    ) AS COUNT
					FROM
					    `tblsummary_new`
					    where YEAR(date) *10000+month(date)*100 between year(DATE_ADD(now(),INTERVAL -11 MONTH) )*10000+month(DATE_ADD(now(),INTERVAL -11 MONTH))*100  and year(now())*10000+month(now())*100   ".$sess_where."  ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
					
					GROUP BY
					    YEAR(tblsummary_new.date),
					    MONTH(tblsummary_new.date)
					ORDER BY
					    YEAR(tblsummary_new.date) DESC
					,MONTH(tblsummary_new.date) DESC
					    
					LIMIT 12";

$result = $this->db->query($query)->result();

if(count($result) == 1)
{
return $result[0];
}
else
{
return $result;
}
}


public function cirrhotic_details_hepb($excel = NULL){

	$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
			}

		 $query = "SELECT
						
					    ifnull(SUM(non_cirrhotic_hepb), 0) AS non_cirrhotic,
					    ifnull(SUM(compensated_cirrhotic_hepb), 0) AS compensated_cirrhotic,
					    ifnull(SUM(decompensated_cirrhotic_hepb), 0) AS decompensated_cirrhotic

			
					FROM
					    `tblsummary_new`
					WHERE
					    "  .$this->date_filter.''.$sess_where.' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;;

					     //print_r($query); die();

		//$result = $this->db->query($query)->result();
	    if ($excel== NULL) {
			$result = $this->db->query($query)->result();
		}else{
			$result = $this->db->query($query)->result_array();
		}


		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}



public function regdistribution_hepb($excel = NULL){

	

		$query = "SELECT
						
					    ifnull(SUM(regimen_hepb_TAF), 0) AS regimenwisedistribution_reg1,
					    ifnull(SUM(regimen_hepb_Entecavir), 0) AS regimenwisedistribution_reg2,
					    ifnull(SUM(regimen_hepb_TDF), 0) AS regimenwisedistribution_reg3,
					    ifnull(SUM(regimen_hepb_Others), 0) AS regimenwisedistribution_reg24
			
					FROM
					    `tblsummary_new`
					WHERE
					    "  .$this->date_filter.''.$sess_where.' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;

					     //print_r($query); die();

		//$result = $this->db->query($query)->result();
						if ($excel== NULL) {
						$result = $this->db->query($query)->result();
						}else{
						$result = $this->db->query($query)->result_array();
						}

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}


public function adherencepercentagedata_hepb($excel = NULL)
	{
		


		  $query ="select case when  c.RegimenPrescribed=99 then 'regimen4' else RegimenPrescribed end as COUNT,avg(c.Adherence)as COUNT FROM `tblpatient` p inner join hepb_tblpatient b on p.PatientGUID=b.PatientGUID  inner join tblpatientdispensationb c on b.patientguid=c.PatientGUID  WHERE p.HbsAg=1 AND c.VisitNo>1  AND c.Treatment_Dt IS NOT NULL and c.Treatment_Dt!='0000-00-00'  ".$sess_where ." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacilityp." group by case when  c.RegimenPrescribed=99 then 'regimen4' else RegimenPrescribed end  ";		    
//and SVR12W_Result=1
		//$result = $this->db->query($query)->result();
	    if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}

		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}
	}



public function age_gender_hepb($excel = NULL,$sel_id=NULL){

	$loginData = $this->session->userdata('loginData');
		//echo "<pre>";print_r($loginData);
if($sel_id==NULL){
	$sel_id='1';
}
		


		$sql = "SELECT
						
					    ifnull(SUM(regimen_hepb_male), 0) AS regimen_hepb_male,
					    ifnull(SUM(regimen_hepb_female), 0) AS regimen_hepb_female,
					    ifnull(SUM(regimen_hepb_transgender), 0) AS regimen_hepb_transgender
			
					FROM
					    `tblsummary_new`
					WHERE
					    "  .$this->date_filter.''.$sess_where.' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;
		if ($excel== NULL) {
		$result = $this->db->query($sql)->result();
		}else{
		$result = $this->db->query($sql)->result_array();
		}
		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}

}


public function agewise_initiations($excel = NULL){

	$loginData = $this->session->userdata('loginData'); 

			

		$query = "SELECT
						
					    ifnull(SUM(age_less_than_10_hepb), 0) AS age_less_than_10_hepb,
					    ifnull(SUM(age_10_to_20_hepb), 0) AS age_10_to_20_hepb,
					    ifnull(SUM(age_21_to_30_hepb), 0) AS age_21_to_30_hepb,
					    ifnull(SUM(age_31_to_40_hepb), 0) AS age_31_to_40_hepb,
					    ifnull(SUM(age_41_to_50_hepb), 0) AS age_41_to_50_hepb,
					    ifnull(SUM(age_51_to_60_hepb), 0) AS age_51_to_60_hepb,
					    ifnull(SUM(age_greater_than_60_hepb), 0) AS age_greater_than_60_hepb
			
					FROM
					    `tblsummary_new`
					WHERE
					    "  .$this->date_filter.''.$sess_where.' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;

					     //print_r($query); die();

		//$result = $this->db->query($query)->result();
						if ($excel== NULL) {
						$result = $this->db->query($query)->result();
						}else{
						$result = $this->db->query($query)->result_array();
						}

		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}

}

public function genderwise_vldata($excel = NULL){

	
//" s where s.date between '2019-01-01' and '2019-12-31' and s.Session_StateID=3";
			
		 $sql = "SELECT sum(case when p.Gender=1  then  p.viral_load_tested ELSE 0 END) male,sum(case when p.Gender=2  then  p.viral_load_tested ELSE 0 END) female,sum(case when p.Gender=3  then  p.viral_load_tested ELSE 0 END) transgender from tblsummary_genderwise p
					WHERE
					   "  .$this->date_filtergen.''.$sess_where.' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility;

					    
	 		if ($excel== NULL) {
		$result = $this->db->query($sql)->result();
		}else{
		$result = $this->db->query($sql)->result_array();
		}
		if(count($result) == 1)
		{
			return $result;
		}
		else
		{
			return $result;
		}




}

public function map_facility_data($excel = NULL)
	{	

		$loginData = $this->session->userdata('loginData'); 
		//pr($loginData);
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_wheres = "1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " Session_StateID = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_wheres = "Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " Session_StateID = '".$loginData->State_ID."'";
				$sess_wheres = "Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " Session_StateID = '".$loginData->State_ID."'";
				$sess_wheres = "1";
			}

		 /* $query = "SELECT COUNT(Session_DistrictID), Session_DistrictID FROM `tblpatient`
					WHERE
					    "  .$this->date_filterp.' '.$sess_where .' '.$this->facility_state .' '.$this->facility_district.' '.$this->id_mstfacility." GROUP by Session_DistrictID ORDER BY `COUNT(Session_DistrictID)` DESC";*/

				 $query="SELECT * FROM 
							(SELECT Pin,COUNT(p.PatientGUID) as COUNT,Session_StateID FROM tblpatient p WHERE ".$sess_where." ".$this->facility_state." ".$this->facility_district." ".$this->id_mstfacilityp." AND (p.HCVRapidDate is NOT NULL || p.HCVRapidDate!='0000-00-00' || p.HCVElisaDate is NOT NULL || p.HCVElisaDate!='0000-00-00') and AntiHCV=1 and (p.HCVRapidResult=1 || p.HCVElisaResult=1 || p.HCVOtherResult=1) GROUP BY pin) AS p
							LEFT JOIN 
							(SELECT * from tbl_pin_latlongs WHERE ".$sess_wheres." ".$this->facility_statea." GROUP  BY Pin) AS lt
							ON p.Pin=lt.Pin AND p.Session_StateID=lt.Session_StateID WHERE lt.Pin IS NOT NULL ";
					     //print_r($query); die();

		//$result = $this->db->query($query)->result();
	    if ($excel== NULL) {
		$result = $this->db->query($query)->result();
		}else{
		$result = $this->db->query($query)->result_array();
		}
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
	}

}