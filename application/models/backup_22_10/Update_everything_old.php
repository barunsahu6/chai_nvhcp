<?php 

class Update_everything_old extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Common_Model");
	}

	public function update_everything()
	{
		$this->update_summary();
		 $this->update_lal_data();
		$this->update_tblpatientSVR();
		$this->update_cumulativesvr();
		$this->update_tat();
		$this->update_gradient_table();
		$this->import_eaushadhi();
	}

	public function update_summary()
	{

		$this->db->where_not_in('marker', '3');
		$this->db->order_by('id_tbl_service_tracker', 'DESC');
		$this->db->limit(1);
		$res_tracker = $this->db->get('tbl_service_tracker')->result();

		$update_array = array(
			"marker"             => 2,
			"marker_description" => "started updating summary",
		);
		$this->db->where('id_tbl_service_tracker', $res_tracker[0]->id_tbl_service_tracker);
		$this->db->update("tbl_service_tracker", $update_array);

	    // die();

		$sql_max_date = "select max(date) as date from tblsummary";
		$res_max_date = $this->Common_Model->query_data($sql_max_date);
		$max_date = strtotime($res_max_date[0]->date); 
		if(date('Y-m-d') > date('Y-m-d',$max_date))
		{
			$sql_mstfacility = "select id_mstfacility from mstfacility";
			$res_mstfacility = $this->Common_Model->query_data($sql_mstfacility);
			foreach ($res_mstfacility as $facility) {
				
				$insert_array = array(
					"id_mstfacility" => $facility->id_mstfacility,
					"date" => date('Y-m-d'),
				);
				$this->Common_Model->insert_data('tblsummary', $insert_array);
			}
		}


		$sql_everything_null = "update tblsummary set antibody=null,antibodypositive=null,rna=null,chronichiv=null,treatmentinitiated=null,treatmentmid=null,treatmentcompleted=null, TreatmentSuccessful = null, Gen1= null,Gen2= null,Gen3= null, Gen4= null, Gen5= null, Gen6= null, SVR = null";
		$this->Common_Model->update_data_sql($sql_everything_null);

		$sql_update_antibody_positive = "update tblsummary s inner join (select id_mstfacility,T_AntiHCV01_Date as dt,count(patientguid) as Anti_Positive from tblpatient where T_AntiHCV01_Result=1 group by T_AntiHCV01_Date,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.AntibodyPositive=a.Anti_Positive";
		$this->Common_Model->update_data_sql($sql_update_antibody_positive);

		$sql_update_confirmatory_done = "update tblsummary s inner join (select id_mstfacility,T_DLL_01_VLC_Date as dt ,count(patientguid) as Confirmatory from tblpatient where T_DLL_01_VLC_Date is not null group by T_DLL_01_VLC_Date,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.RNA=a.Confirmatory";
		$this->Common_Model->update_data_sql($sql_update_confirmatory_done);

		$sql_update_chronic_hcv = "update tblsummary s inner join (select id_mstfacility,T_DLL_01_VLC_Date as dt ,count(patientguid) as Confirmatory from tblpatient where T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Result=1 group by T_DLL_01_VLC_Date,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.ChronicHIV=a.Confirmatory";
		$this->Common_Model->update_data_sql($sql_update_chronic_hcv);

		$sql_update_chronic_hcv_not_infected = "update tblsummary s inner join (select id_mstfacility,T_DLL_01_VLC_Date as dt ,count(patientguid) as Confirmatory from tblpatient where T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Result=2 group by T_DLL_01_VLC_Date,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.ChronicHIV_not_infected=a.Confirmatory";
		$this->Common_Model->update_data_sql($sql_update_chronic_hcv_not_infected);

		$sql_update_treatment_initiated = "update tblsummary s inner join (select id_mstfacility,(case when T_Initiation  <'2016-06-18' then '2016-06-18' else T_Initiation end) as dt,count(patientguid) as initiated from tblpatient where T_Initiation is not null group by (case when T_Initiation  <'2016-06-18' then '2016-06-18' else T_Initiation end),id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.TreatmentInitiated=a.initiated";
		$this->Common_Model->update_data_sql($sql_update_treatment_initiated);

		$sql_update_treatment_initiated_lfu = "update tblsummary s inner join (select id_mstfacility,date_add(T_DLL_01_VLC_Date, INTERVAL 31 DAY) as dt,count(patientguid) as TreatmentInitiated_lfu from tblpatient where T_DLL_01_VLC_Date is not null and T_Initiation is null group by date_add(T_DLL_01_VLC_Date, INTERVAL 31 DAY),id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.TreatmentInitiated_lfu=a.TreatmentInitiated_lfu";
		$this->Common_Model->update_data_sql($sql_update_treatment_initiated_lfu);

		$sql_update_treatment_completed = "update tblsummary s inner join ( select id_mstfacility,ETR_HCVViralLoad_Dt as dt ,count(patientguid) ETR from tblpatient where ETR_HCVViralLoad_Dt is not null group by ETR_HCVViralLoad_Dt,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.TreatmentCompleted=a.etr";
		$this->Common_Model->update_data_sql($sql_update_treatment_completed);

		$sql_update_treatment_completed_lfu = "update tblsummary s inner join ( select id_mstfacility,date_add(Next_Visitdt, INTERVAL 31 day) as dt ,count(patientguid) Treatment_completed_lfu from tblpatient where ETR_HCVViralLoad_Dt is null and NextVisitPurpose > 1 and NextVisitPurpose < 99 and Next_Visitdt is not null group by date_add(Next_Visitdt, INTERVAL 31 day),id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.TreatmentCompleted_lfu=a.Treatment_completed_lfu";
		$this->Common_Model->update_data_sql($sql_update_treatment_completed_lfu);

		$sql_update_svr = "update tblsummary s inner join ( select id_mstfacility,SVR12W_HCVViralLoad_Dt as dt ,count(patientguid) svr from tblpatient where SVR12W_HCVViralLoad_Dt is not null and status in (14,15) group by SVR12W_HCVViralLoad_Dt ,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.SVR=a.svr";
		$this->Common_Model->update_data_sql($sql_update_svr);

		$sql_update_svr = "update tblsummary s inner join ( select p.id_mstfacility,s.SVRDate as dt ,count(s.patientguid) svr from tblpatient p inner join tblpatientSVR s on s.PatientGUID = p.PatientGUID  where s.SVRDate is not null group by s.SVRDate ,p.id_mstfacility order by s.SVRDate ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.SVR=a.svr";
		$this->Common_Model->update_data_sql($sql_update_svr);

		$sql_update_svr_lfu = "update tblsummary s inner join ( select id_mstfacility,date_add(ETR_HCVViralLoad_Dt, INTERVAL 31 day) as dt ,count(patientguid) svr_lfu from tblpatient where SVR12W_HCVViralLoad_Dt is null and ETR_HCVViralLoad_Dt is not null group by date_add(ETR_HCVViralLoad_Dt, INTERVAL 31 day) ,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.SVR_lfu=a.svr_lfu";
		$this->Common_Model->update_data_sql($sql_update_svr_lfu);

		// $sql_update_svr_achieved = "update tblsummary s inner join (select id_mstfacility,SVR12W_HCVViralLoad_Dt as dt ,count(patientguid) TreatmentSuccessful from tblpatient where SVR12W_HCVViralLoad_Dt is not null and status = 14 group by SVR12W_HCVViralLoad_Dt,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.SVRAchieved=a.TreatmentSuccessful";
		// $this->Common_Model->update_data_sql($sql_update_svr_achieved);

		$sql_update_svr_achieved = "update tblsummary s inner join (select p.id_mstfacility,s.SVRDate as dt ,count(s.patientguid) TreatmentSuccessful from tblpatient p inner join tblpatientSVR s on s.PatientGUID = p.PatientGUID  where s.SVRDate is not null and s.SVRResult = 2 group by s.SVRDate ,p.id_mstfacility order by s.SVRDate ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.TreatmentSuccessful=a.TreatmentSuccessful";
		$this->Common_Model->update_data_sql($sql_update_svr_achieved);

		// $sql_update_treatment_successful = "update tblsummary s inner join (select id_mstfacility,SVR12W_HCVViralLoad_Dt as dt ,count(patientguid) TreatmentSuccessful from tblpatient where SVR12W_HCVViralLoad_Dt is not null and status = 14 group by SVR12W_HCVViralLoad_Dt,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.TreatmentSuccessful=a.TreatmentSuccessful";
		// $this->Common_Model->update_data_sql($sql_update_treatment_successful);

		$sql_update_treatment_not_successful = "update tblsummary s inner join (select id_mstfacility,SVR12W_HCVViralLoad_Dt as dt ,count(patientguid) Treatment_not_successful from tblpatient where SVR12W_HCVViralLoad_Dt is not null and status = 15 group by SVR12W_HCVViralLoad_Dt,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.Treatment_not_successful=a.Treatment_not_successful";
		$this->Common_Model->update_data_sql($sql_update_treatment_not_successful);

		// $sql_update_gen1 = "update tblsummary s inner join (select id_mstfacility, GenotypeTest_Dt  as dt ,count(patientguid) as g1 from tblpatientcirrohosis where GenotypeTest_Dt  is not null and GenotypeTest_Result=1 group by id_mstfacility, (case when GenotypeTest_Dt  <'2016-06-18' then '2016-06-18' else GenotypeTest_Dt end)) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.Gen1 =a.g1";
		// $this->Common_Model->update_data_sql($sql_update_gen1);

		// $sql_update_gen2 = "update tblsummary s inner join (select id_mstfacility, GenotypeTest_Dt as dt ,count(patientguid) as g2 from tblpatientcirrohosis where GenotypeTest_Dt is not null and GenotypeTest_Result=2 group by id_mstfacility,(case when GenotypeTest_Dt  <'2016-06-18' then '2016-06-18' else GenotypeTest_Dt end) ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.Gen2 =a.g2";
		// $this->Common_Model->update_data_sql($sql_update_gen2);

		// $sql_update_gen3 = "update tblsummary s inner join (select id_mstfacility, GenotypeTest_Dt as dt ,count(patientguid) as g3 from tblpatientcirrohosis where GenotypeTest_Dt is not null and GenotypeTest_Result=3 group by id_mstfacility,(case when GenotypeTest_Dt  <'2016-06-18' then '2016-06-18' else GenotypeTest_Dt end) ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.Gen3 =a.g3";
		// $this->Common_Model->update_data_sql($sql_update_gen3);

		// $sql_update_gen4 = "update tblsummary s inner join (select id_mstfacility, GenotypeTest_Dt as dt ,count(patientguid) as g4 from tblpatientcirrohosis where GenotypeTest_Dt is not null and GenotypeTest_Result=4 group by id_mstfacility,(case when GenotypeTest_Dt  <'2016-06-18' then '2016-06-18' else GenotypeTest_Dt end) ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.Gen4 =a.g4";
		// $this->Common_Model->update_data_sql($sql_update_gen4);

		// $sql_update_gen5 = "update tblsummary s inner join (select id_mstfacility, GenotypeTest_Dt as dt ,count(patientguid) as g5 from tblpatientcirrohosis where GenotypeTest_Dt is not null and GenotypeTest_Result=5 group by id_mstfacility,(case when GenotypeTest_Dt  <'2016-06-18' then '2016-06-18' else GenotypeTest_Dt end) ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.Gen5 =a.g5";
		// $this->Common_Model->update_data_sql($sql_update_gen5);

		// $sql_update_gen6 = "update tblsummary s inner join (select id_mstfacility, GenotypeTest_Dt as dt ,count(patientguid) as g6 from tblpatientcirrohosis where GenotypeTest_Dt is not null and GenotypeTest_Result=6 group by id_mstfacility,(case when GenotypeTest_Dt  <'2016-06-18' then '2016-06-18' else GenotypeTest_Dt end) ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.Gen6 =a.g6";
		// $this->Common_Model->update_data_sql($sql_update_gen6);

		echo "<br><code>summary table updated</code>";
	}

	public function update_gradient_table()
	{	
		$this->db->where_not_in('marker', '3');
		$this->db->order_by('id_tbl_service_tracker', 'DESC');
		$this->db->limit(1);
		$res_tracker = $this->db->get('tbl_service_tracker')->result();

		$update_array = array(
			"marker"             => 7,
			"marker_description" => "started updating gradient table",
		);
		$this->db->where('id_tbl_service_tracker', $res_tracker[0]->id_tbl_service_tracker);
		$this->db->update("tbl_service_tracker", $update_array);

		$sql_columns = "SHOW columns FROM tblgradient";

		$res_columns = $this->Common_Model->query_data($sql_columns);

		$sql_null = "update tblgradient set cirr_anti_hcv = null,
		cirr_anti_hcv_color = null,
		cirr_hcv_ini = null,
		cirr_hcv_ini_color = null,
		cirr_ini_completion = null,
		cirr_ini_completion_color = null,
		cirr_completion_svr = null,
		cirr_completion_svr_color = null,
		no_cirr_anti_hcv = null,
		no_cirr_anti_hcv_color = null,
		no_cirr_hcv_ini = null,
		no_cirr_hcv_ini_color = null,
		no_cirr_ini_completion = null,
		no_cirr_ini_completion_color = null,
		no_cirr_completion_svr = null,
		no_cirr_completion_svr_color = null";

		$res_null = $this->Common_Model->update_data_sql($sql_null);

		foreach ($res_columns as $row) {
			$columns[] = $row->Field;
		}
		array_splice($columns,0,2);
		// print_r($columns); die();

		$array_tat_fields = ['antibody_hcv','hcv_initiation','initiation_completion','completion_svr'];
		$array_grad_fields = ['anti_hcv','hcv_ini','ini_completion','completion_svr'];

		for ($i = 1; $i <= 2; $i++) {
			if($i == 1)
			{
				$cirr = "cirr_";
			}
			else{
				$cirr = "no_cirr_";
			}
			for($j = 0; $j < count($array_tat_fields); $j++)
			{

				$sql = "update tblgradient g inner join (select id_mstfacility, avg(".$array_tat_fields[$j].") as val from tblpatient_tat_summary t where cirrhosis_status = ".$i." group by id_mstfacility) a on a.id_mstfacility = g.id_mstfacility set g.".$cirr.$array_grad_fields[$j]." = a.val";
    	// echo $sql; die();
				$res = $this->Common_Model->update_data_sql($sql);
			}

		}

		for ($i = 0; $i < count($columns); $i= $i+2) {

			$column = $columns[$i];
			$sql = "SELECT ".$column." as val FROM `tblgradient` where ".$column." is not null order by ".$column." asc";
		// echo $sql;
				$res = $this->Common_Model->query_data($sql);

				foreach ($res as $row) {
					$values[] = $row->val;
				};

		// print_r($values);

				$q1 = (count($values)+1)/4;
				$q1_value = $values[round($q1) - 1];

		// echo "q1_value : ".$q1." : ".$q1_value; 

				$q2 = (count($values)+1)/2;
				$q2_value = $values[round($q2) - 1];

		// echo "q2_value : ".$q2." : ".$q2_value; 

				$q3 = ((count($values)+1)/4)*3;
				$q3_value = $values[round($q3) - 1];

		// echo "q3_value : ".$q3." : ".$q3_value; 

				$color[] = "#31849B";
				$color[] = "#93CDDD";
				$color[] = "#B6DDE8";
				$color[] = "#DBE5F1";
				$color[] = "lightgray";
				$color[] = "#538ED5";

				$sql_color = "update tblgradient set ".$column."_color = '".$color[0]."' where ".$column." < ".$q1_value;
				$res_color = $this->Common_Model->update_data_sql($sql_color);

				$sql_color = "update tblgradient set ".$column."_color = '".$color[1]."' where ".$column." > ".$q1_value."  and cirr_anti_hcv <= ".$q2_value;
				$res_color = $this->Common_Model->update_data_sql($sql_color);

				$sql_color = "update tblgradient set ".$column."_color = '".$color[5]."' where ".$column." = ".$q2_value;
				$res_color = $this->Common_Model->update_data_sql($sql_color);

				$sql_color = "update tblgradient set ".$column."_color = '".$color[2]."' where ".$column." > ".$q2_value."  and cirr_anti_hcv <= ".$q3_value;
				$res_color = $this->Common_Model->update_data_sql($sql_color);

				$sql_color = "update tblgradient set ".$column."_color = '".$color[3]."' where ".$column." > ".$q3_value;
				$res_color = $this->Common_Model->update_data_sql($sql_color);

				$sql_color = "update tblgradient set ".$column."_color = '".$color[4]."' where ".$column." is null ";
				$res_color = $this->Common_Model->update_data_sql($sql_color);
			}
			echo "<br><code>gradient table updated</code>";
	}

	public function update_tat()
	{

		$this->db->where_not_in('marker', '3');
		$this->db->order_by('id_tbl_service_tracker', 'DESC');
		$this->db->limit(1);
		$res_tracker = $this->db->get('tbl_service_tracker')->result();

		$update_array = array(
			"marker"             => 6,
			"marker_description" => "started updating TAT Report",
		);
		$this->db->where('id_tbl_service_tracker', $res_tracker[0]->id_tbl_service_tracker);
		$this->db->update("tbl_service_tracker", $update_array);

		$sql_add_new_records = "insert into tblpatient_tat_summary (patientguid, cirrhosis_status, id_mstfacility)
		SELECT p.patientguid, p.v1_cirrhosis, p.id_mstfacility FROM `tblpatient_tat_summary` t right join tblpatient p on t.patientguid = p.PatientGUID where t.patientguid is null";
		$res_add_new_records = $this->Common_Model->update_data_sql($sql_add_new_records);

		$sql_anti_hcv = "update tblpatient_tat_summary t inner join (select PatientGUID,
			datediff(p.T_DLL_01_VLC_Date, (case when T_AntiHCV01_Date  <'2016-06-18' then '2016-06-18' else T_AntiHCV01_Date end)) as date_diff
			FROM
			tblpatient p 

			WHERE
			p.T_DLL_01_VLC_Date IS NOT NULL
			AND p.T_AntiHCV01_Date IS NOT NULL
			and ((case when T_AntiHCV01_Date  <'2016-06-18' then '2016-06-18' else T_AntiHCV01_Date end) >= STR_TO_DATE('2016-06-18','%Y-%m-%d'))
			AND p.CardType = 2) p on p.PatientGUID=t.PatientGUID

			set antibody_hcv=p.date_diff";
			$res_anti_hcv = $this->Common_Model->update_data_sql($sql_anti_hcv);

			$sql_hcv_initiation = "update tblpatient_tat_summary t inner join (select PatientGUID,
				datediff(p.T_Initiation, p.T_DLL_01_VLC_Date) as date_diff
				FROM
				tblpatient p 

				WHERE
				p.T_DLL_01_VLC_Date IS NOT NULL
				AND p.T_Initiation IS NOT NULL
				and (p.T_DLL_01_VLC_Date > STR_TO_DATE('2016-06-18','%Y-%m-%d'))
				AND p.CardType = 2) p on p.PatientGUID=t.PatientGUID

				set hcv_initiation=p.date_diff";
				$res_hcv_initiation = $this->Common_Model->update_data_sql($sql_hcv_initiation);

				$sql_initiation_completion = "update tblpatient_tat_summary t inner join (select PatientGUID,
					datediff(p.ETR_HCVViralLoad_Dt, p.T_Initiation) as date_diff
					FROM
					tblpatient p 

					WHERE
					p.T_Initiation IS NOT NULL
					AND p.ETR_HCVViralLoad_Dt IS NOT NULL
					and (p.T_Initiation > STR_TO_DATE('2016-06-18','%Y-%m-%d'))
					AND p.CardType = 2) p on p.PatientGUID=t.PatientGUID

					set initiation_completion=p.date_diff";
					$res_initiation_completion = $this->Common_Model->update_data_sql($sql_initiation_completion);

					$sql_completion_svr = "update tblpatient_tat_summary t inner join (select PatientGUID,
						datediff(p.SVR12W_HCVViralLoad_Dt, p.ETR_HCVViralLoad_Dt) as date_diff
						FROM
						tblpatient p 

						WHERE
						p.ETR_HCVViralLoad_Dt IS NOT NULL
						AND p.SVR12W_HCVViralLoad_Dt IS NOT NULL
						and (p.ETR_HCVViralLoad_Dt > STR_TO_DATE('2016-06-18','%Y-%m-%d'))
						AND p.CardType = 2  ) p on p.PatientGUID=t.PatientGUID

						set completion_svr=p.date_diff";
						$res_completion_svr = $this->Common_Model->update_data_sql($sql_completion_svr);

						$sql_adh_2 = "update tblpatient_tat_summary t 
						inner join 
						(select PatientGUID, Adherence from tblpatientvisit
							where visitno = 2 and Adherence is not null and Adherence <> 0) v2
							on t.patientguid = v2.patientguid
							set t.adh_v2 = v2.adherence";
							$sql_adh_2 = $this->Common_Model->update_data_sql($sql_adh_2);

							$sql_adh_3 = "update tblpatient_tat_summary t 
							inner join 
							(select PatientGUID, Adherence from tblpatientvisit
								where visitno = 3 and Adherence is not null and Adherence <> 0) v3
								on t.patientguid = v3.patientguid
								set t.adh_v3 = v3.adherence";
								$sql_adh_3 = $this->Common_Model->update_data_sql($sql_adh_3);

								$sql_adh_4 = "update tblpatient_tat_summary t 
								inner join 
								(select PatientGUID, Adherence from tblpatientvisit
									where visitno = 4 and Adherence is not null and Adherence <> 0) v4
									on t.patientguid = v4.patientguid
									set t.adh_v4 = v4.adherence";
									$sql_adh_4 = $this->Common_Model->update_data_sql($sql_adh_4);

									$sql_adh_5 = "update tblpatient_tat_summary t 
									inner join 
									(select PatientGUID, Adherence from tblpatientvisit
										where visitno = 5 and Adherence is not null and Adherence <> 0) v5
										on t.patientguid = v5.patientguid
										set t.adh_v5 = v5.adherence";
										$sql_adh_5 = $this->Common_Model->update_data_sql($sql_adh_5);

										$sql_adh_6 = "update tblpatient_tat_summary t 
										inner join 
										(select PatientGUID, Adherence from tblpatientvisit
											where visitno = 6 and Adherence is not null and Adherence <> 0) v6
											on t.patientguid = v6.patientguid
											set t.adh_v6 = v6.adherence";
											$sql_adh_6 = $this->Common_Model->update_data_sql($sql_adh_6);

			// 		$sql_adh_7 = "update tblpatient_tat_summary t 
			// inner join 
			// (select PatientGUID, Adherence from tblpatientvisit
			//  where visitno = 7 and Adherence is not null and Adherence <> 0) v7
			//  on t.patientguid = v7.patientguid
			//  set t.adh_v7 = v7.adherence";
			// 		$sql_adh_7 = $this->Common_Model->update_data_sql($sql_adh_7);

											$sql_adh_overall = "update tblpatient_tat_summary t inner join 
											(select v1.patientguid, (v1.pconsumed/v1.datediff)*100 as adh from (SELECT 
												p.PatientGUID,
												datediff((CASE
													WHEN SVR12W_HCVViralLoad_Dt IS NOT NULL THEN SVR12W_HCVViralLoad_Dt
													WHEN ETR_HCVViralLoad_Dt IS NOT NULL THEN ETR_HCVViralLoad_Dt
													ELSE v1.max_date
													END),p.T_Initiation) as datediff,
													(ifnull(p.T_NoPillStart,0)+ifnull(v1.pissued,0)-ifnull(v1.pleft,0)) as pconsumed
													FROM
													tblpatient p
													INNER JOIN
													(SELECT 
														MAX(visit_dt) AS max_date,
														PatientGUID,
														SUM(PillsIssued) AS pissued,
														SUM(PillsLeft) AS pleft
														FROM
														tblpatientvisit v
														GROUP BY patientguid) v1 ON v1.patientguid = p.patientguid) v1 where datediff > 0) ad 
														on t.patientguid = ad.patientguid
														set t.adh_overall = (case when ad.adh > 100 then 100
															else ad.adh end)";
															$sql_adh_overall = $this->Common_Model->update_data_sql($sql_adh_overall);

															$sql_genotype = "update `tblpatient_tat_summary` t 
															inner join tblpatientcirrohosis c 
															on t.patientguid = c.PatientGUID
															set t.genotype = c.GenotypeTest_Result";
															$res_genotype = $this->Common_Model->update_data_sql($sql_genotype);

															$sql_cirrhosis_status = "update tblpatient_tat_summary t 
															inner join tblpatient p 
															on t.patientguid = p.PatientGUID
															set t.cirrhosis_status = p.V1_Cirrhosis";
															$res_cirrhosis_status = $this->Common_Model->update_data_sql($sql_cirrhosis_status);



															echo "<br><code>tat updated</code>";
	}

	public function update_lal_data()
	{

		$insert_array = array(
			"marker"             => 3,
			"marker_description" => "started updating lal data",
			"start_time"         => date('Y-m-d H:i:s'),
		);
		$this->db->insert("tbl_service_tracker", $insert_array);

		// die();

		$lal_db = $this->load->database('lal_lab', true);

		$sql_testresult_last_date = "select max(inserttime) as last_date from lal_punjab_testresults";
		$last_date = $this->Common_Model->query_data($sql_testresult_last_date);

		$res = $lal_db->query("select * from punjab_testresults where convert(date,inserttime) >'".$last_date[0]->last_date."'");

		foreach ($res->result() as $row) {
			$res = $this->Common_Model->insert_data('lal_punjab_testresults',$row);
		}

		$sql_headerinfo_last_date = "select max(inserttime) as last_date from lal_punjab_headerinfo";
		$last_date_headerinfo = $this->Common_Model->query_data($sql_headerinfo_last_date);

		$res_headerinfo = $lal_db->query("select * from punjab_headerinfo where convert(date,inserttime) >'".$last_date_headerinfo[0]->last_date."'");

		foreach ($res_headerinfo->result() as $row) {
			$res = $this->Common_Model->insert_data('lal_punjab_headerinfo',$row);
		}

		$sql_set_facilityid = "update `lal_punjab_headerinfo` l inner join mstfacility f on l.hospitalcode = f.FacilityCode set l.id_mstfacility = f.id_mstfacility where l.id_mstfacility is null";
		$this->Common_Model->update_data_sql($sql_set_facilityid);

		$sql_year_dot = "update `lal_punjab_headerinfo` set str_uid = substring_index(uid,'.',-1) where length(uid) > 0 and uid REGEXP '[.]' AND str_uid IS NULL";
		$sql_year_dot = $this->Common_Model->update_data_sql($sql_year_dot);

		$sql_year_hyphen = "update `lal_punjab_headerinfo` set str_uid = substring_index(uid,'-',-1) where length(uid) > 0 and uid REGEXP '[-]' AND str_uid IS NULL";
		$sql_year_hyphen = $this->Common_Model->update_data_sql($sql_year_hyphen);

		$sql_year_slash = "update `lal_punjab_headerinfo` set str_uid = substring_index(uid,'/',-1) where length(uid) > 0 and uid REGEXP '[/]' AND str_uid IS NULL";
		$sql_year_slash = $this->Common_Model->update_data_sql($sql_year_slash);

		$sql_uid_aplhanumeric = "update 
		`lal_punjab_headerinfo`
		set str_uid =  LEFT(
			IF(
				CAST(CONCAT(uid, '1') AS UNSIGNED) = 0,
				REVERSE(
					CAST(
						REVERSE(CONCAT(uid, '1')) AS UNSIGNED
					)
				),
				CAST(CONCAT(uid, '1') AS UNSIGNED)
			),
			LENGTH(
				IF(
					CAST(CONCAT(uid, '1') AS UNSIGNED) = 0,
					REVERSE(
						CAST(
							REVERSE(CONCAT(uid, '1')) AS UNSIGNED
						)
					),
					CAST(CONCAT(uid, '1') AS UNSIGNED)
				)
			) -1
		)
		WHERE
		uid IS NOT NULL AND uid <> ' ' AND LENGTH(uid) > 0 AND uid <> '.' AND str_uid IS NULL and length(cast(uid as unsigned)) < 10 and cast(reverse(uid) as unsigned) != 0";
		$res_uid_aplhanumeric = $this->Common_Model->update_data_sql($sql_uid_aplhanumeric);

		$sql_update_patientguid = "UPDATE
		`lal_punjab_headerinfo` l
		INNER JOIN(
		SELECT
		p.patientguid,
		a.uid,
		a.id_mstfacility
		FROM
		tblpatient p
		INNER JOIN(
		SELECT
		CAST(str_uid AS UNSIGNED) AS uid,
		patientguid,
		HOSPITALCODE,
		id_mstfacility,
		INSERTTIME
		FROM
		`lal_punjab_headerinfo`
		WHERE
		patientguid IS NULL AND str_uid > 0 AND(uid NOT REGEXP '[-]') AND id_mstfacility IS NOT NULL AND HOSPITALCODE IS NOT NULL  and str_uid is not null
		) a
		ON
		a.uid = p.uid_num AND a.id_mstfacility = p.id_mstfacility
		) b
		ON
		l.uid = b.uid AND l.id_mstfacility = b.id_mstfacility
		SET
		l.patientguid = b.patientguid where l.patientguid is null";
		$this->Common_Model->update_data_sql($sql_update_patientguid);

		$this->db->where('marker', '3');
		$this->db->order_by('id_tbl_service_tracker', 'DESC');
		$this->db->limit(1);
		$res_tracker = $this->db->get('tbl_service_tracker')->result();

		$update_array = array(
			"marker_description" => "finished updating lal data",
			"end_time"           => date('Y-m-d H:i:s'),
		);
		$this->db->where('id_tbl_service_tracker', $res_tracker[0]->id_tbl_service_tracker);
		$this->db->update("tbl_service_tracker", $update_array);

		echo '<br><code>lal data updated</code>';

	}

	public function update_tblpatientSVR()
	{

		$this->db->where_not_in('marker', '3');
		$this->db->order_by('id_tbl_service_tracker', 'DESC');
		$this->db->limit(1);
		$res_tracker = $this->db->get('tbl_service_tracker')->result();

		$update_array = array(
			"marker"             => 4,
			"marker_description" => "started updating patientSVR table",
		);
		$this->db->where('id_tbl_service_tracker', $res_tracker[0]->id_tbl_service_tracker);
		$this->db->update("tbl_service_tracker", $update_array);

		//updating patientsvr with new patientguids

		$sql_add_new_patientguids = "insert into tblpatientSVR (patientguid)
		SELECT h.patientguid FROM `lal_punjab_headerinfo` h inner join lal_punjab_testresults t on h.itl_labnum = t.labnum left join tblpatientSVR s on h.patientguid = s.PatientGUID where s.PatientGUID is null and h.patientguid is not null group by h.patientguid";
		$this->Common_Model->update_data_sql($sql_add_new_patientguids);


		// updating tblpatientSVR with svr data

		$sql_update_svr = "update tblpatientSVR s 
		inner join 
		(SELECT 
			h.patientguid as patientguid,
			STR_TO_DATE(t.report_date, '%b %d %Y') AS svr_date,
			CASE t.result
			WHEN 'Not detected' THEN 2
			WHEN 'Target Not Detected' THEN 2
			ELSE 1
			END AS result,
			CAST(t.result AS UNSIGNED) AS viralload
			FROM
			lal_punjab_headerinfo h
			INNER JOIN
			lal_punjab_testresults t ON h.itl_labnum = t.labnum
			WHERE
			h.patientguid IS NOT NULL
			AND t.customername = 'PUNJAB HEALTH SYSTEMS CORPORATION (PHASE-2' and t.test_name = 'HEPATITIS C VIRAL (HCV RNA) QUANTITATIVE REAL TIME PCR'
		) a 
		on s.patientguid = a.patientguid
		set s.SVRDate = a.svr_date, s.SVRResult = a.result, s.SVRVLC = a.viralload, s.svr_from = 'L' where s.SVRDate is null";
		$this->Common_Model->update_data_sql($sql_update_svr);

		$sql_update_confirmatory = "UPDATE tblpatientSVR s
		INNER JOIN
		(SELECT 
		h.patientguid AS patientguid,
		STR_TO_DATE(t.report_date, '%b %d %Y') AS confirmatory_date,
		CASE t.result
		WHEN 'Not detected' THEN 2
		WHEN 'Target Not Detected' THEN 2
		ELSE 1
		END AS result,
		CAST(t.result AS UNSIGNED) AS viralload
		FROM
		lal_punjab_headerinfo h
		INNER JOIN lal_punjab_testresults t ON h.itl_labnum = t.labnum
		WHERE
		h.patientguid IS NOT NULL
		AND t.customername = 'PUNJAB HEALTH SYSTEMS CORPORATION'
		AND t.test_name = 'HEPATITIS C VIRAL (HCV RNA) QUANTITATIVE REAL TIME PCR') a ON s.patientguid = a.patientguid 
		SET 
		s.confirmatory_test_date = a.confirmatory_date,
		s.confirmatory_test_result = a.result,
		s.confirmatory_test_viral_load = a.viralload,
		s.confirmatory_from = 'L'
		WHERE
		s.confirmatory_test_date IS NULL";
		$this->Common_Model->update_data_sql($sql_update_confirmatory);

		$sql_update_genotype = "UPDATE tblpatientSVR s
		INNER JOIN
		(SELECT 
		h.patientguid AS patientguid,
		STR_TO_DATE(t.report_date, '%b %d %Y') AS genotype_date,
		CASE
		WHEN result LIKE '%1%' THEN 1
		WHEN result LIKE '%2%' THEN 2
		WHEN result LIKE '%3%' THEN 3
		WHEN result LIKE '%4%' THEN 4
		WHEN result LIKE '%5%' THEN 5
		WHEN result LIKE '%6%' THEN 6
		ELSE ''
		END AS result
		FROM
		lal_punjab_headerinfo h
		INNER JOIN
		lal_punjab_testresults t ON h.itl_labnum = t.labnum
		WHERE
		h.patientguid IS NOT NULL
		AND t.test_name = 'HEPATITIS C VIRAL (HCV RNA) GENOTYPE'
		AND (result LIKE '%1%' or result LIKE '%2%' or result LIKE '%3%' or result LIKE '%4%' or result LIKE '%5%' or result LIKE '%6%')) a ON s.patientguid = a.patientguid 
		SET 
		s.genotype_test_date = a.genotype_date,
		s.genotype_test_result = a.result,
		s.genotype_from = 'L'
		WHERE
		s.genotype_test_date IS NULL";
		$this->Common_Model->update_data_sql($sql_update_genotype);


		echo "<br><code>tblpatientSVRupdated</code>";
	} 

	public function update_cumulativesvr()
	{	
		$this->db->where_not_in('marker', '3');
		$this->db->order_by('id_tbl_service_tracker', 'DESC');
		$this->db->limit(1);
		$res_tracker = $this->db->get('tbl_service_tracker')->result();

		$update_array = array(
			"marker"             => 5,
			"marker_description" => "started updating cumulative SVR table",
		);
		$this->db->where('id_tbl_service_tracker', $res_tracker[0]->id_tbl_service_tracker);
		$this->db->update("tbl_service_tracker", $update_array);

		$sql = 'insert into cumulative_svr(patientguid) SELECT p.patientguid FROM tblpatient p left join `cumulative_svr` c on p.PatientGUID = c.patientguid where c.patientguid is null';
		$this->Common_Model->update_data_sql($sql);

		$sql = 'update `cumulative_svr` c inner join tblpatientSVR s on c.patientguid = s.PatientGUID set c.svr_date = s.SVRDate, c.svr_result = s.SVRResult, c.svr_viral_load = s.SVRVLC where c.svr_date is null';
		$this->Common_Model->update_data_sql($sql);

		$sql = 'UPDATE
		`cumulative_svr` c
		INNER JOIN
		tblpatient p
		ON
		c.patientguid = p.PatientGUID
		SET
		c.svr_date = p.SVR12W_HCVViralLoad_Dt,
		c.svr_result = p.SVR12W_Result,
		c.svr_viral_load = p.SVR12W_HCVViralLoadCount
		WHERE
		c.svr_date IS NULL AND p.SVR12W_HCVViralLoad_Dt IS NOT NULL';
		$this->Common_Model->update_data_sql($sql);

		echo "<br><code>cumulative svr updated</code>";
	}

	public function import_eaushadhi()
	{
		// test on this code and it will give $return as json array

		$query = "SELECT date(max(Trans_Date)) as last_import_date FROM `tbl_stock_eaushadhi`";
		$result = $this->db->query($query)->result();

		if($result[0]->last_import_date == null)
		{
			$start_date = date_create('2018-02-22');
		}
		else
		{
			$start_date = date_create($result[0]->last_import_date);
			date_add($start_date, date_interval_create_from_date_string("1 days"));
		}
		
		while($start_date <= date_create(date('Y-m-d')))
		{

		for($facility_type_code = 0; $facility_type_code <= 2; $facility_type_code++)
		{

			$host     = "http://eaushadhipb.in/HISUtilities/services/restful/DataService/getDataJSON/".$facility_type_code;
			$username = "cdac";
			$password = "cdac";

			$postData = json_encode(array( "primaryKeys" => array($start_date->format('d-M-Y'))));
			// print_r($postData); die();

			$process  = curl_init($host);
			curl_setopt($process, CURLOPT_HTTPHEADER, array('Content-Type: text/plain', $additionalHeaders));
			curl_setopt($process, CURLOPT_USERPWD, $username . ":" . $password);
			curl_setopt($process, CURLOPT_TIMEOUT, 30);
			curl_setopt($process, CURLOPT_POST, 1);
			curl_setopt($process, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($process, CURLOPT_POSTFIELDS, $postData);
			$return = curl_exec($process);
			curl_close($process);

			$return = json_decode($return);

			$count  = count($return->dataVO->dataValue);
			if($return->dataVO->dataValue == NULL){
				echo "<br><code>No Record found for : ".$facility_type_code."</code>"; 
			}
			else
			{
				$start_arr = array(
					"marker"             => 99,
					"marker_description" => "Started importing eaushadhi data",
					"start_time"         => date('Y-m-d H:i:s')
				);
				$this->db->trans_start();
				$this->db->insert('tbl_service_tracker', $start_arr);
				$where = $this->db->insert_id();

					foreach ($return->dataVO->dataValue as $row ) {

						$insert_array = array(
							"Trans_Date"    =>	date('Y-m-d',strtotime($row->item[0])),
							"Wh_Id"         =>	$row->item[1],
							"Wh_Name"       =>  $row->item[2],
							"Drug_Id"       =>	$row->item[3],
							"facility_type" =>  $facility_type_code,
							"Drug_Name"     =>	$row->item[4],
							"Batch_No"      =>	$row->item[5],
							"Expiry_Date"   =>	date('Y-m-d',strtotime($row->item[6])),
							"Op_Balance"    =>	$row->item[7],
							"Issue_Qty"     =>	$row->item[8],
							"Req_Str_Id"    =>	$row->item[9],
							"Issued_To"     =>	$row->item[10],
							"Created_On"    =>  date('Y-m-d H:i:s'),
							"Is_Deleted"    =>  0,
						);
						$this->db->insert('tbl_stock_eaushadhi',$insert_array);
					}
				$this->db->set('end_time', date('Y-m-d H:i:s'));
				$this->db->where('id_tbl_service_tracker', $where);
				$this->db->update('tbl_service_tracker');
				$this->db->trans_complete();

			echo "<br><code>inserted records for facility type : ".$facility_type_code."</code>";
			
			}

		}
		date_add($start_date, date_interval_create_from_date_string("1 days"));
	}
		if ($this->db->trans_status() === FALSE){
			echo "<br><code>Error while updating records</code>";
		}
		else{
			echo "<br><code>Successfully inserted and updated records</code>";
		}
	}
}