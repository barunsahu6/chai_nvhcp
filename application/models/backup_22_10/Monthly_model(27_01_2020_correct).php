<?php 
Class Monthly_model extends CI_Model
{
	public function __construct()
	{
		if($this->session->userdata('filters1') != null)
		{
				$filters1                           = $this->session->userdata('filters1');
				
				if($filters1['id_mstfacility']      == 0)
				{
				$loginData = $this->session->userdata('loginData'); 

					if( ($loginData) && $loginData->user_type == '1' ){
					$this->facility_filter = "AND 1";
					$this->facility_filter1 = "AND 1";
					$this->date_filter      = " CreatedOn >'2018-09-01' AND CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

					//$this->HAVRapidDate      = " HAVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
					$this->HAVRapidDate      = " CreatedOn < curdate() AND (HAVRapidDate >'2018-09-01' || HAVElisaDate > '2018-09-01' || HAVOtherDate > '2018-09-01') AND HAVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."' || HAVElisaDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."' || HAVOtherDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'" ;

					$this->HBSRapidDate      = " CreatedOn < curdate() AND HBSRapidDate >'2018-09-01' AND HBSRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
					$this->HCVRapidDate      = " CreatedOn < curdate() AND HCVRapidDate >'2018-09-01' AND HCVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
					$this->HEVRapidDate      = " CreatedOn < curdate() AND HEVRapidDate >'2018-09-01' AND HEVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

					$this->HEPCRapidDate      = " CreatedOn < curdate() AND (HCVRapidDate >'2018-09-01' || HCVElisaDate > '2018-09-01' || HCVOtherDate > '2018-09-01') AND HCVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."' || HCVElisaDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'" || "HCVOtherDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'" ;


					$this->date_filter1      = " AND p.CreatedOn >'2018-09-01' AND p.CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

					}
					elseif( ($loginData) && $loginData->user_type == '2' ){

					$this->facility_filter = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";

					$this->facility_filter1 = "AND p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."' AND p.id_mstfacility='".$loginData->id_mstfacility."'";
					$this->date_filter1      = " and p.CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

						$this->HAVRapidDate      = " CreatedOn < curdate() AND (HAVRapidDate >'2018-09-01' || HAVElisaDate > '2018-09-01' || HAVOtherDate > '2018-09-01') AND HAVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."' || HAVElisaDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."' || HAVOtherDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'" ;
					

	


					}
					elseif( ($loginData) && $loginData->user_type == '3' ){ 
					$this->facility_filter = "AND Session_StateID = '".$loginData->State_ID."'";
					$this->facility_filter1 = "AND p.Session_StateID = '".$loginData->State_ID."'";
					$this->date_filter1      = " and p.CreatedOn >'2018-09-01' AND p.CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
					}
					elseif( ($loginData) && $loginData->user_type == '4' ){ 
					$this->facility_filter = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
					$this->date_filter1      = " and p.CreatedOn >'2018-09-01' AND p.CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

					$this->facility_filter1 = "AND p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."'";
					$this->date_filter1      = " and p.CreatedOn >'2018-09-01' AND p.CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
					}
				}
				else
				{
				$this->facility_filter              = "AND id_mstfacility = ".$filters1['id_mstfacility'];
				$this->facility_filter1              = "AND p.id_mstfacility = ".$filters1['id_mstfacility'];
				$this->date_filter1      = " and p.CreatedOn >'2018-09-01' AND p.CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
				}

				if($filters1['id_search_state']      == 0)
				{
				$loginData = $this->session->userdata('loginData'); 

					//if( ($loginData) && $loginData->user_type == '1' ){
					$this->facility_state  = "AND 1";
					$this->facility_state1  = "AND 1";
					$this->facility_statesumm  = "AND 1";
					//}
					
				}
				else
				{
				$this->facility_state              = "AND Session_StateID = ".$filters1['id_search_state'];
				$this->facility_state1              = "AND p.Session_StateID = ".$filters1['id_search_state'];
				}

				if($filters1['id_input_district']      == 0)
				{
				$loginData = $this->session->userdata('loginData'); 

					//if( ($loginData) && $loginData->user_type == '1' ){
					$this->facility_district = "AND 1";
					$this->facility_district1 = "AND 1";
					$this->facility_districtsumm = "AND 1";
					//}
					
				}
				else
				{
				$this->facility_district              = "AND Session_DistrictID = ".$filters1['id_input_district'];
				$this->facility_district1              = "AND p.Session_DistrictID = ".$filters1['id_input_district'];
				$this->date_filter1      = " and p.CreatedOn >'2018-09-01' AND p.CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

				}

		
				//$this->HAVRapidDate      = " HAVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

					$this->HAVRapidDate = " CreatedOn < curdate() AND (HAVRapidDate >'2018-09-01' || HAVElisaDate > '2018-09-01' || HAVOtherDate > '2018-09-01') AND HAVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."' || HAVElisaDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."' || HAVOtherDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'" ;

					$this->HBSRapidDate      = " CreatedOn < curdate() AND HBSRapidDate >'2018-09-01' AND HBSRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
					$this->HCVRapidDate      = " CreatedOn < curdate() AND HCVRapidDate >'2018-09-01' AND HCVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
					$this->HEVRapidDate      = " CreatedOn < curdate() AND HEVRapidDate >'2018-09-01' AND HEVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

			$this->date_filter      = " CreatedOn > '2018-09-01' AND CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
			$this->T_Initiation     = " CreatedOn < curdate() AND T_Initiation > '2018-09-01' AND T_Initiation between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
			
			$this->date_filtersumm      = " date > '2018-09-01' AND date between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";


			$this->data_ETRTestDate = " CreatedOn < curdate() AND ETR_HCVViralLoad_Dt > '2018-09-01' AND ETR_HCVViralLoad_Dt between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

			$this->Date_SVR_TestDate = " CreatedOn < curdate() AND ETR_HCVViralLoad_Dt > '2018-09-01' AND DATE_ADD(ETR_HCVViralLoad_Dt,INTERVAL 84 DAY) between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

			$this->SVR12W_HCVViralLoad_Dt = " CreatedOn < curdate() AND SVR12W_HCVViralLoad_Dt > '2018-09-01' AND SVR12W_HCVViralLoad_Dt between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

			$this->data_UpdatedOn = " CreatedOn < curdate() AND UpdatedOn>'2018-09-01' AND UpdatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

			$this->T_DLL_01_BVLC_Date =" CreatedOn < curdate() AND T_DLL_01_BVLC_Date >'2018-09-01' AND T_DLL_01_BVLC_Date between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
			$this->ETR_HCVVIRALLOAD_DT = "CreatedOn < curdate() AND T_DLL_01_BVLC_Date >'2018-09-01' AND T_DLL_01_BVLC_Date between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

			$this->T_DLL_01_VLC_Date = "CreatedOn < curdate() AND T_DLL_01_VLC_Date >'2018-09-01' AND T_DLL_01_VLC_Date between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
			
			$this->data_visitUpdatedOn = "v.CreatedOn < curdate() AND  v.Visit_Dt >'2018-09-01' AND v.Visit_Dt between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
			if (!empty($filters1['enddate1'])) {
				$this->enddate=$filters1['enddate1'];
			}

		}

		parent::__construct();
	}

	public function get_data_1_1($flag=null)
	{
		if ($flag==1) {
			$date_filter_late=$this->date_filter." AND CreatedOn < '".$this->enddate."' AND (UpdatedOn < '".$this->enddate."' OR UpdatedOn IS NULL)";
		}
		else{
			$date_filter_late=$this->date_filter." AND CreatedOn < curdate() AND (UpdatedOn < Curdate() OR UpdatedOn IS NULL)";
		}

		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$date_filter_late." and gender = 1 and age>= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$date_filter_late." and gender = 2 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();
		

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$date_filter_late." and gender = 3  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$date_filter_late." and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();

		return $result;
	}

	public function get_data_2_1($flag=null)
	{
		if ($flag==1) {
			$T_Initiation_late=$this->T_Initiation." AND CreatedOn < '".$this->enddate."' AND (UpdatedOn < '".$this->enddate."' OR UpdatedOn IS NULL)";
		}
		else{
			$T_Initiation_late=$this->T_Initiation." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)";
		}
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		  $sql                = "SELECT case when  count(patientguid) = 0 then '-' else  count(patientguid) end as count FROM `tblpatient` where   ".$T_Initiation_late."  and gender = 1 and age >= 18 and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and  T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$T_Initiation_late."  and gender = 2 and age >= 18 and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$T_Initiation_late."  and gender = 3  and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$T_Initiation_late." and age < 18 and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_2_2($flag=null)
	{
		if ($flag==1) {
			$date_filter_late=$this->date_filter." AND CreatedOn < '".$this->enddate."' AND (UpdatedOn < '".$this->enddate."' OR UpdatedOn IS NULL)";
		}
		else{
			$date_filter_late=$this->date_filter." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)";
}
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$date_filter_late." and gender = 1 and age >= 18 AND TransferUID >0 AND TransferFromFacility >0 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where     ".$date_filter_late." and gender = 2 and age >= 18 AND TransferUID >0 AND TransferFromFacility >0 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where     ".$date_filter_late." and gender = 3  AND TransferUID >0 AND TransferFromFacility >0 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$date_filter_late." AND age < 18 AND TransferUID >0 AND TransferFromFacility >0 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_2_3($flag=null)
	{
		if ($flag==1) {
			$T_Initiation_late=$this->T_Initiation." AND CreatedOn < '".$this->enddate."' AND (UpdatedOn < '".$this->enddate."' OR UpdatedOn IS NULL)";
		}
		else{
			$T_Initiation_late=$this->T_Initiation." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)";
		}
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$T_Initiation_late."  and gender = 1 and age >= 18 and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$T_Initiation_late."  and gender = 2 and age >= 18 and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$T_Initiation_late."  and gender = 3 and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$T_Initiation_late." and  age < 18 and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_3_1($flag=null)
	{
		if ($flag==1) {
			$data_ETRTestDate_late=$this->data_ETRTestDate." AND CreatedOn < '".$this->enddate."' AND (UpdatedOn < '".$this->enddate."' OR UpdatedOn IS NULL)";
		}
		else{
			$data_ETRTestDate_late=$this->data_ETRTestDate." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)";
		}
		
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$data_ETRTestDate_late." and gender = 1 and age >= 18 and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00'  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where   ".$data_ETRTestDate_late."  and gender = 2 and age >= 18  and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00'  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();
		

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where   ".$data_ETRTestDate_late."  and gender = 3 and  ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00'  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where   ".$data_ETRTestDate_late."  and age < 18 and  ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_3_2($flag=NULL)
	{	
		if ($flag==1) {
			$data_UpdatedOn_late=$this->data_UpdatedOn." AND CreatedOn < '".$this->enddate."' AND (UpdatedOn < '".$this->enddate."' OR UpdatedOn IS NULL)";
		}
		else{
			$data_UpdatedOn_late=$this->data_UpdatedOn." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)";
		}
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and TransferRequestAccepted >0 and gender = 1 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and TransferRequestAccepted >0 and gender = 2 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and TransferRequestAccepted >0 and gender = 3  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and TransferRequestAccepted >0 and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) ";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_3_3($flag=NULL)
	{	
		if ($flag==1) {
			$data_UpdatedOn_late=$this->data_UpdatedOn." AND CreatedOn < '".$this->enddate."' AND (UpdatedOn < '".$this->enddate."' OR UpdatedOn IS NULL)";
		}
		else{
			$data_UpdatedOn_late=$this->data_UpdatedOn." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)";
		}
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and InterruptReason = 8 and gender = 1 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and InterruptReason = 8 and gender = 2 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and InterruptReason = 8 and gender = 3  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and InterruptReason = 8 and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) ";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_3_4($flag=NULL)
	{	
		if ($flag==1) {
			$data_UpdatedOn_late=$this->data_UpdatedOn." AND CreatedOn < '".$this->enddate."' AND (UpdatedOn < '".$this->enddate."' OR UpdatedOn IS NULL)";
		}
		else{
			$data_UpdatedOn_late=$this->data_UpdatedOn." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)";
		}
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and SVR_TreatmentStatus = 5 and gender = 1 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and SVR_TreatmentStatus = 5 and gender = 2 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and SVR_TreatmentStatus = 5 and gender = 3  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and SVR_TreatmentStatus = 5 and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_3_5($flag=NULL)
	{	
		if ($flag==1) {
			$data_visitUpdatedOn_late=$this->data_visitUpdatedOn." AND v.CreatedOn < '".$this->enddate."' AND (v.UpdatedOn < '".$this->enddate."' OR v.UpdatedOn IS NULL)";
		}
		else{
			$data_visitUpdatedOn_late=$this->data_visitUpdatedOn." AND (v.UpdatedOn < curdate() OR v.UpdatedOn IS NULL)";
		}
		
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."' AND p.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND p.Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."'";
			}

		$sql                = "SELECT  case when  count(DISTINCT p.PatientGUID) =0 then '-' else  count(DISTINCT p.PatientGUID) end as count  FROM `tblpatient` p inner join tblpatientvisit v on p.patientguid=v.patientguid where  PillsLeft >0 and ".$data_visitUpdatedOn_late." and gender = 1 and age >= 18 ".$this->facility_state1 ." ".$this->facility_district1." ".$this->facility_filter1." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT  case when  count(DISTINCT p.PatientGUID) =0 then '-' else  count(DISTINCT p.PatientGUID) end as count FROM `tblpatient` p inner join tblpatientvisit v on p.patientguid=v.patientguid  where  ".$data_visitUpdatedOn_late." and gender = 2 and age >= 18 ".$this->facility_state1 ." ".$this->facility_district1." ".$this->facility_filter1." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT  case when  count(DISTINCT p.PatientGUID) =0 then '-' else  count(DISTINCT p.PatientGUID) end as count FROM `tblpatient` p inner join tblpatientvisit v on p.patientguid=v.patientguid  where  ".$data_visitUpdatedOn_late." and gender = 3 ".$this->facility_state1 ." ".$this->facility_district1." ".$this->facility_filter1." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT  case when  count(DISTINCT p.PatientGUID) =0 then '-' else  count(DISTINCT p.PatientGUID) end as count FROM `tblpatient` p inner join tblpatientvisit v on p.patientguid=v.patientguid  where  ".$data_visitUpdatedOn_late." and age < 18 ".$this->facility_state1 ." ".$this->facility_district1." ".$this->facility_filter1." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_3_6($flag=NULL)
	{

		if ($flag==1) {
			$data_UpdatedOn_late=$this->data_UpdatedOn." AND CreatedOn < '".$this->enddate."' AND (UpdatedOn < '".$this->enddate."' OR UpdatedOn IS NULL)";
		}
		else{
			$data_UpdatedOn_late=$this->data_UpdatedOn." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)";
		}
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late."  and gender = 1 and age >= 18 and IsReferal=1 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late."  and gender = 2 and age >= 18 and IsReferal=1 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late."  and gender = 3  and IsReferal=1 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." ";
		$result['transgender']   = $this->db->query($sql)->result();

		
		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late."  and age < 18 and IsReferal=1 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_3_7($flag=NULL)
	{	
		if ($flag==1) {
			$data_UpdatedOn_late=$this->data_UpdatedOn." AND CreatedOn < '".$this->enddate."' AND (UpdatedOn < '".$this->enddate."' OR UpdatedOn IS NULL)";
		}
		else{
			$data_UpdatedOn_late=$this->data_UpdatedOn." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)";
		}
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late."  and gender = 1 and SVR_TreatmentStatus = 5 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and SVR_TreatmentStatus =5 and gender = 2 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and SVR_TreatmentStatus =5 and gender = 3  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$data_UpdatedOn_late." and SVR_TreatmentStatus =5 and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_4_1($flag=NULL)
	{	
		if ($flag==1) {
			$Date_SVR_TestDate_late=$this->Date_SVR_TestDate." AND CreatedOn < '".$this->enddate."' AND (UpdatedOn < '".$this->enddate."' OR UpdatedOn IS NULL)";
		}
		else{
			$Date_SVR_TestDate_late=$this->Date_SVR_TestDate." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)";
		}
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$Date_SVR_TestDate_late." and status = 13 and gender = 1 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$Date_SVR_TestDate_late." and gender = 2 and status = 13 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$Date_SVR_TestDate_late." and gender = 3 and status = 13  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$Date_SVR_TestDate_late." and status = 13 and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_4_2($flag=NULL)
	{	
		if ($flag==1) {
			$SVR12W_HCVViralLoad_Dt_late=$this->SVR12W_HCVViralLoad_Dt." AND CreatedOn < '".$this->enddate."'  AND (UpdatedOn < '".$this->enddate."' OR UpdatedOn IS NULL)";
		}
		else{
			$SVR12W_HCVViralLoad_Dt_late=$this->SVR12W_HCVViralLoad_Dt." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)";
		}
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$SVR12W_HCVViralLoad_Dt_late." and gender = 1 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$SVR12W_HCVViralLoad_Dt_late." and gender = 2 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$SVR12W_HCVViralLoad_Dt_late." and gender = 3  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();

		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$SVR12W_HCVViralLoad_Dt_late." and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_4_3($flag=NULL)
	{	if ($flag==1) {
			$SVR12W_HCVViralLoad_Dt_late=$this->SVR12W_HCVViralLoad_Dt." AND CreatedOn < '".$this->enddate."'  AND (UpdatedOn < '".$this->enddate."' OR UpdatedOn IS NULL)";
		}
		else{
			$SVR12W_HCVViralLoad_Dt_late=$this->SVR12W_HCVViralLoad_Dt." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)";
		}
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}
			
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$SVR12W_HCVViralLoad_Dt_late." and SVR12W_Result =2 and gender = 1 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$SVR12W_HCVViralLoad_Dt_late." and SVR12W_Result =2 and gender = 2 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$SVR12W_HCVViralLoad_Dt_late." and SVR12W_Result =2 and gender = 3  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();

		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$SVR12W_HCVViralLoad_Dt_late." and SVR12W_Result =2 and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) ";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

		public function hepatitis_c_a(){

				  //$sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.T_DLL_01_VLC_Date is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and ".$this->T_DLL_01_VLC_Date." and p.Session_StateID is not null ";

			 $sql="SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->date_filter." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";

			//$sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where   (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and ".$this->HEPCRapidDate." and p.Session_StateID is not null ";
				$result = $this->db->query($sql)->result();
				return $result;

					
		}

		public function hepatitis_c_b(){

					 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.T_Initiation is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and ".$this->T_Initiation." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) and p.Session_StateID is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
					$result = $this->db->query($sql)->result();
					return $result;
					
		}


		public function hep_b(){

					 $sql = "SELECT COUNT( * ) as count FROM `tblpatient` WHERE `T_DLL_01_BVLC_Date` IS NOT NULL and ".$this->T_DLL_01_BVLC_Date." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and Session_StateID is not null";
					$result = $this->db->query($sql)->result();
					return $result;
		}

			public function hep_c(){

					  $sql = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` WHERE `ETR_HCVVIRALLOAD_DT` IS NOT NULL and ".$this->data_ETRTestDate." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and Session_StateID is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
					$result = $this->db->query($sql)->result();
					return $result;
		}
		public function reason_flu(){

			  $sql = "select count(LFUReason) as count ,m.LookupValue  from mstlookup m left join tblpatient p on m.LookupCode=p.LFUReason where m.flag = 49 and m.LanguageID = 1 ".$this->date_filter1." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state1 ." ".$this->facility_district1." ".$this->facility_filter1." group by m.LookupCode";
					$result = $this->db->query($sql)->result();
					return $result;

		}

		public function reasons_for_death(){

			  $sql = "select count(DeathReason) as count ,m.LookupValue  from mstlookup m left join tblpatient p on m.LookupCode=p.DeathReason where m.flag = 48 and m.LanguageID = 1 ".$this->date_filter1." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state1 ." ".$this->facility_district1." ".$this->facility_filter1." group by m.LookupCode";
					$result = $this->db->query($sql)->result();
					return $result;

		}

		public function hepatitis_a(){

			  $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where (p.HAVRapidDate is not null AND p.HAVRapidDate!='0000-00-00') || (p.HCVElisaDate is not null and p.HCVElisaDate !='0000-00-00' ) || (p.HCVOtherDate is not NULL and p.HCVOtherDate  !='0000-00-00' )  AND ".$this->HAVRapidDate." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
		}
	public function hepatitis_b(){
		 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.HBSRapidDate is not null and p.HBSRapidDate!='0000-00-00' AND ".$this->HBSRapidDate." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
	}	

	public function hepatitis_c(){
			 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.HCVRapidDate is not null and p.HCVRapidDate!='0000-00-00' AND ".$this->HCVRapidDate." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
	}
	public function hepatitis_d(){

			$sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.HEVRapidDate is not null and p.HEVRapidDate!='0000-00-00' AND ".$this->HEVRapidDate." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
	}

	/*positive*/
	public function hepatitisp_a(){

			 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.HAVRapidDate is not null and p.HAVRapidDate!='0000-00-00' and HAVRapidResult=1 AND ".$this->date_filter." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
		}
	public function hepatitisp_b(){
		 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.HBSRapidDate is not null and p.HBSRapidDate!='0000-00-00' and HBSRapidResult=1 AND ".$this->date_filter." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
	}	

	public function hepatitisp_c(){
			 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.HCVRapidDate is not null and p.HCVRapidDate!='0000-00-00' AND HCVRapidResult=1 AND ".$this->date_filter." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
	}
	public function hepatitisp_d(){

			$sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.HEVRapidDate is not null and p.HEVRapidDate!='0000-00-00' AND HEVRapidResult=1 AND ".$this->date_filter." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
	}

	public function hcv_vl_detected(){

		 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.T_DLL_01_VLC_Date is not null and p.T_DLL_01_VLC_Date!='0000-00-00' AND T_DLL_01_VLC_Result=1 AND ".$this->date_filter." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
	}
//T_DLL_01_BVLC_Date
public function hep_b_confirmatory(){

		 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.T_DLL_01_BVLC_Date is not null and p.T_DLL_01_BVLC_Date!='0000-00-00'  AND ".$this->date_filter." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
}
//T_Initiation
public function firstst_dispensation(){

		 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.T_Initiation is not null and p.T_Initiation!='0000-00-00'  AND ".$this->date_filter." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
}

//ETR_HCVViralLoad_Dt

public function etr_during(){

	 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.ETR_HCVViralLoad_Dt is not null and p.ETR_HCVViralLoad_Dt!='0000-00-00'  AND ".$this->date_filter." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;

}

public function svr_not_detected(){

	$sql = "Select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.SVR12W_HCVViralLoad_Dt is not null and p.SVR12W_HCVViralLoad_Dt!='0000-00-00' AND SVR12W_Result=2  AND ".$this->date_filter." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;

}
public function hbv_detected(){

	$sql = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.T_DLL_01_BVLC_Date is not null and p.T_DLL_01_BVLC_Date!='0000-00-00' AND T_DLL_01_BVLC_Result=1  AND ".$this->date_filter." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
}


public function nationaldata_statewise(){

$loginData = $this->session->userdata('loginData');
		//echo "<pre>";print_r($loginData);

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_wherep = " 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' ";
			}

$query=" SELECT  s.StateName,t1.count,t2.count1,t3.count2    
FROM 
mststate s  left join 
(
SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as COUNT,Session_StateID FROM `tblpatient` p where  ".$this->date_filter." AND CreatedOn < curdate() AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
GROUP BY p.Session_StateID) t1  on s.id_mststate=t1.Session_StateID 
left join 
(select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count1,Session_StateID from tblpatient p1 where p1.T_Initiation is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and ".$this->T_Initiation." and p1.Session_StateID is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) group by p1.Session_StateID) t2 on 
s.id_mststate =t2.Session_StateID
left join 
(SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count2,Session_StateID FROM tblpatient p2 WHERE `ETR_HCVVIRALLOAD_DT` IS NOT NULL  and ".$this->data_ETRTestDate." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and p2.Session_StateID is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) group by p2.Session_StateID )
  t3 on s.id_mststate=t3.Session_StateID order by s.StateName ASC";

 /*$query=" SELECT  s.StateName,t1.count,t2.count1,t3.count2    
FROM 
mststate s  left join 
(
SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end AS COUNT,Session_StateID
FROM tblpatient p
WHERE p.T_DLL_01_VLC_Date IS NOT NULL  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and ".$this->T_DLL_01_VLC_Date." and p.Session_StateID is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
GROUP BY p.Session_StateID) t1  on s.id_mststate=t1.Session_StateID 
left join 
(select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count1,Session_StateID from tblpatient p1 where p1.T_Initiation is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and ".$this->T_Initiation." and p1.Session_StateID is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) group by p1.Session_StateID) t2 on 
s.id_mststate =t2.Session_StateID
left join 
(SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count2,Session_StateID FROM tblpatient p2 WHERE `ETR_HCVVIRALLOAD_DT` IS NOT NULL  and ".$this->data_ETRTestDate."  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and p2.Session_StateID is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) group by p2.Session_StateID )
  t3 on s.id_mststate=t3.Session_StateID order by s.StateName ASC";*/

	  /*$query = "SELECT
    f.StateName AS StateName,
   ifnull(SUM(tblsummary_new.serological_tests), 0) AS anti_hcv_screened,
   ifnull(SUM(tblsummary_new.initiatied_on_treatment), 0) AS initiatied_on_treatment,
   ifnull(SUM(tblsummary_new.treatment_completed), 0) AS treatment_completed
   
FROM
(
    SELECT
       *
   FROM mststate order by StateName asc
   
) f
LEFT JOIN
   `tblsummary_new`
ON
   tblsummary_new.Session_StateID = f.id_mststate
WHERE
   (
      ".$this->date_filtersumm." and ".$sess_where." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
   )
GROUP BY
   tblsummary_new.Session_StateID order by f.StateName";*/


 $result = $this->db->query($query)->result();


if(count($result) == 1)
{
return $result;
}
else
{
return $result;
}

}

/*hmis report*/
public function hims_hepatitis_a(){


$sql = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility where (p.HAVRapidDate is not null || p.HAVElisaDate is not null ) and (p.HAVRapidDate!='0000-00-00' ||  p.HAVElisaDate!='0000-00-00') AND FacilityType='DH' AND ".$this->HEPCRapidDate." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
$result = $this->db->query($sql)->result();
return $result;
}
public function hims_hepatitis_b(){


$sql = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility where (p.HBSRapidDate is not null || p.HBSElisaDate is not null ) and (p.HBSRapidDate!='0000-00-00' ||  p.HBSElisaDate!='0000-00-00') AND FacilityType='DH' AND HbsAg = 1 AND ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
$result = $this->db->query($sql)->result();
return $result;
}
public function hims_hepatitis_c(){


$sql = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility where (p.HCVRapidDate is not null || p.HCVElisaDate is not null ) and (p.HCVRapidDate!='0000-00-00' ||  p.HCVElisaDate!='0000-00-00') AND FacilityType='DH' AND AntiHCV = 1 AND ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
$result = $this->db->query($sql)->result();
return $result;
}
public function hims_hepatitis_e(){


$sql = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility where (p.HEVRapidDate is not null || p.HEVElisaDate is not null ) and (p.HEVRapidDate!='0000-00-00' ||  p.HEVElisaDate!='0000-00-00') AND FacilityType='DH' AND ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
$result = $this->db->query($sql)->result();
return $result;
}

public function hims_hepatitis_a_positive(){


$sql = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility where (p.HAVRapidDate is not null || p.HAVElisaDate is not null ) and (p.HAVRapidDate!='0000-00-00' ||  p.HAVElisaDate!='0000-00-00') and (p.HAVRapidResult=1 || p.HAVElisaResult=1 )AND FacilityType='DH' AND ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
$result = $this->db->query($sql)->result();
return $result;
}
public function hims_hepatitis_b_positive(){


$sql = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility where (p.HBSRapidDate is not null || p.HBSElisaDate is not null ) and (p.HBSRapidDate!='0000-00-00' ||  p.HBSElisaDate!='0000-00-00') and (p.HBSRapidResult=1 || p.HBSElisaResult=1 )AND FacilityType='DH' AND HbsAg = 1 AND ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
$result = $this->db->query($sql)->result();
return $result;
}
public function hims_hepatitis_c_positive(){


$sql = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility where (p.HCVRapidDate is not null || p.HCVElisaDate is not null ) and (p.HCVRapidDate!='0000-00-00' ||  p.HCVElisaDate!='0000-00-00') and (p.HCVRapidResult=1 || p.HCVElisaResult=1 ) AND FacilityType='DH' AND AntiHCV = 1 AND ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
$result = $this->db->query($sql)->result();
return $result;
}
public function hims_hepatitis_e_positive(){


$sql = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility where (p.HEVRapidDate is not null || p.HEVElisaDate is not null ) and (p.HEVRapidDate!='0000-00-00' ||  p.HEVElisaDate!='0000-00-00') and (p.HEVRapidResult=1 || p.HEVElisaResult=1 ) AND FacilityType='DH' AND ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
$result = $this->db->query($sql)->result();
return $result;
}
public function hims_hepatitis_c_positive_elegible(){


$sql = "SELECT case when COUNT(p.patientguid) =0 then '-' else COUNT(p.patientguid) end as count FROM mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility where AntiHCV = 1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' AND FacilityType='DH' AND AntiHCV = 1 AND ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
$result = $this->db->query($sql)->result();
return $result;
}
public function hims_hepatitis_b_positive_HBV_DNA(){


$sql = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility where (p.HBSRapidDate is not null || p.HBSElisaDate is not null ) and (p.HBSRapidDate!='0000-00-00' ||  p.HBSElisaDate!='0000-00-00') and (p.HBSRapidResult=1 || p.HBSElisaResult=1 )AND (p.BVLSampleCollectionDate is not null) and (p.BVLSampleCollectionDate!='0000-00-00') AND FacilityType='DH' AND HbsAg = 1 AND ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
$result = $this->db->query($sql)->result();
return $result;
}
public function hmis_hep_c_T_Initiation(){

		 $sql = "SELECT case when  COUNT(p.patientguid) =0 then '-' else  COUNT(p.patientguid) end as count from  mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility where p.T_Initiation is not null and p.T_Initiation!='0000-00-00' AND f.FacilityType='DH' AND p.AntiHCV=1 AND (p.HCVRapidResult=1 || p.HCVElisaResult=1) AND ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
				$result = $this->db->query($sql)->result();
				return $result;
}
public function hmis_hep_c_treatment_completed(){

	 $sql = "SELECT case when  COUNT(p.patientguid) =0 then '-' else  COUNT(p.patientguid) end as count from  mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility where p.ETR_HCVViralLoad_Dt is not null and p.ETR_HCVViralLoad_Dt!='0000-00-00' AND f.FacilityType='DH' AND p.AntiHCV=1 AND (p.HCVRapidResult=1 || p.HCVElisaResult=1) AND ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
				$result = $this->db->query($sql)->result();
				return $result;

}
public function SVR12Wdetected(){

	$sql = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility WHERE p.SVR12W_HCVViralLoad_Dt is not null AND p.SVR12W_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND FacilityType='DH' AND ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
				$result = $this->db->query($sql)->result();
				return $result;

}
public function hmis_hbv_elegible(){

	$sql = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from  mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility where p.T_DLL_01_BVLC_Date is not null and p.T_DLL_01_BVLC_Date!='0000-00-00' AND T_DLL_01_BVLC_Result=1  AND FacilityType='DH' AND ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
				$result = $this->db->query($sql)->result();
				return $result;
}
public function hmis_hepB_T_Initiation(){

		 $sql = "SELECT case when  COUNT(p.patientguid) =0 then '-' else  COUNT(p.patientguid) end as count from  mstfacility f LEFT JOIN tblpatient p ON f.id_mstfacility=p.id_mstfacility where p.T_Initiation is not null and p.T_Initiation!='0000-00-00' AND f.FacilityType='DH' and (p.HBSRapidResult=1 || p.HBSElisaResult=1 )AND FacilityType='DH' AND HbsAg = 1 AND ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1."";
				$result = $this->db->query($sql)->result();
				return $result;
}
public function hepatitis_b_a(){

			
			$sql="SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->date_filter." AND (UpdatedOn < curdate() OR UpdatedOn IS NULL)  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter1." and HbsAg=1 AND (HBSRapidResult=1 || HBSElisaResult=1 || HBSOtherResult = 1)";
				$result = $this->db->query($sql)->result();
				return $result;			
		}
}	