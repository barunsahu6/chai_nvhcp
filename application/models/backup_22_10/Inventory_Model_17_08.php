<?php 
class Inventory_Model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
		// $this->file_path = realpath(APPPATH . '../datafiles');
		// $this->banner_path = realpath(APPPATH . '../banners');
		// $this->gallery_path_url = base_url().'datafiles/';
    }
   public function get_inventory_details($flag=NULL){

		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
		$is_utilized="";
		if($flag==NULL){
			$flag="AND i.flag='R'";
			$facility_id="source_name";
			$date_type="i.Entry_Date";
		}
		else if($flag=='R'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="source_name";
			$date_type="i.Entry_Date";
		}
		else if($flag=='L'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="transfer_to";
			$date_type="i.dispatch_date";
		}
		else if($flag=='U'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="id_mstfacility";
			$date_type="i.from_Date";
			$is_utilized=",'1' as is_utilized";
		}
		else if($flag=='I'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="id_mstfacility";
			$date_type="i.indent_date";

		}
		else if($flag=='F'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="id_mstfacility";
			$date_type="i.failure_date";

		}

		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		   $query = "SELECT i.*,f.facility_short_name,s.strength".$is_utilized." FROM `tbl_inventory` i left join mstfacility f on i.".$facility_id."=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength where i.is_deleted='0' AND  ".$date_type." BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND  ".$date_type." BETWEEN '".$date1."' AND '".$date2."' AND ".$itemname." ".$sess_where." ".$flag." ORDER BY ".$date_type." DESC";

					  /*print_r($query); die();*/

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
public function get_indent_data($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		    $query = "SELECT i2.*,case when i3.indent_num IS NULL then '0' ELSE '1' end as is_receipt FROM 
				(SELECT indent_num,inventory_id FROM tbl_inventory i WHERE (Flag='R' OR Flag='I') AND is_deleted='0' AND ".$itemname." ".$sess_where." group by indent_num) AS i1
				LEFT JOIN 
				(SELECT i.id_mst_drugs,i.inventory_id,i.indent_num,i.drug_name,i.type,i.indent_date,i.indent_remark,i.quantity,i.approved_quantity,i.indent_accept_date,i.indent_status,s.strength FROM `tbl_inventory` i INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength where i.is_deleted='0' AND i.indent_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.indent_date BETWEEN '".$date1."' AND '".$date2."' AND ".$itemname." ".$sess_where." AND i.Flag='I' and (i.pending_quantity=0 or i.pending_quantity is null) group by indent_num ORDER BY i.indent_date DESC) AS i2
				ON i1.indent_num=i2.indent_num
				LEFT JOIN 
				(SELECT indent_num,inventory_id FROM tbl_inventory WHERE ((Flag='R' AND id_mstfacility='".$loginData->id_mstfacility."') OR (Flag='I' AND id_mstfacility='".$loginData->id_mstfacility."' AND relocation_status>=1)) AND is_deleted='0') AS i3
				ON i1.indent_num=i3.indent_num
				WHERE i2.inventory_id IS NOT NULL group by i2.indent_num";
		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}
public function get_relocation_data($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		     $query = "SELECT case when (indent_accept_date is null or indent_accept_date='0000-00-00') then 'm' ELSE 'n' END AS status,f.facility_short_name,i.id_mst_drugs,i.Flag,i.inventory_id,i.indent_num,i.drug_name,i.is_notification,i.type,i.dispatch_date,i.is_temp,case when (i.Flag='R' or (i.Flag='I' and is_temp='1')) then i.quantity_dispatched else i.relocated_quantity end as relocated_quantity,case when (i.request_date is null or i.request_date='0000-00-00') then i.Entry_Date ELSE i.request_date end as request_date,i.relocation_status,i.indent_status,i.relocation_remark,i.issue_num,i.from_to_type,i.batch_num,case when i.requested_quantity is null then i.pending_quantity when i.requested_quantity=0 then i.pending_quantity else i.requested_quantity end as requested_quantity,s.strength FROM `tbl_inventory` i left join mstfacility f on case when indent_status IS NULL then i.transfer_to=f.id_mstfacility when indent_status IS NOT NULL then i.id_mstfacility=f.id_mstfacility  ELSE 0 END INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength where ((i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."') || (i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.Entry_Date BETWEEN '".$date1."' AND '".$date2."' AND i.is_temp='1')) AND ".$itemname." AND i.is_deleted='0' and (((i.flag='I' or i.flag='R') AND i.transfer_to=?) OR ( i.flag='L' AND i.id_mstfacility=?)) order by is_notification DESC,is_temp DESC,i.request_date DESC";
		$result1 = $this->db->query($query,[$loginData->id_mstfacility,$loginData->id_mstfacility])->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}
public function get_receiving_data($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		         $query = "SELECT distinct case when q.Flag='I' then q.quantity when q.flag='L' then q.requested_quantity end AS show_quantity,i.*,f.facility_short_name,i.*,s.* FROM
 (SELECT inventory_id,requested_quantity,quantity,indent_num,type FROM tbl_inventory i WHERE (id_mstfacility=".$loginData->id_mstfacility.") OR ( Flag='L' And transfer_to=".$loginData->id_mstfacility." and is_temp='0') AND ".$itemname.") AS p
 LEFT JOIN 
						(SELECT case when (Flag='I' OR flag='R') then transfer_to ELSE id_mstfacility end as facility,case when Flag='R' then 'm' ELSE 'n' END AS status,i.*  FROM tbl_inventory i WHERE ((Flag='I'  AND id_mstfacility=".$loginData->id_mstfacility." AND relocation_status>=1 and i.dispatch_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND (i.dispatch_date BETWEEN '".$date1."' AND '".$date2."') or (i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."' AND i.Entry_Date BETWEEN '".$date1."' AND '".$date2."' )) OR (flag='R' AND id_mstfacility=".$loginData->id_mstfacility." and i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."' AND i.Entry_Date BETWEEN '".$date1."' AND '".$date2."') OR (Flag='L' AND transfer_to=".$loginData->id_mstfacility." AND relocation_status>=1 and i.dispatch_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."' AND i.dispatch_date BETWEEN '".$date1."' AND '".$date2."')) AND ".$itemname." ) AS i
						ON p.inventory_id=i.inventory_id
						 LEFT JOIN
						  (SELECT id_mstfacility,facility_short_name FROM mstfacility) AS f
						 		ON i.facility=f.id_mstfacility
						 INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON i.id_mst_drugs=s.id_mst_drug_strength 
							LEFT JOIN 
					(SELECT quantity,requested_quantity,approved_quantity,indent_num,inventory_id,flag FROM tbl_inventory WHERE (Flag='I' OR Flag='R' AND id_mstfacility=".$loginData->id_mstfacility.") OR (Flag='L' AND transfer_to=".$loginData->id_mstfacility.")) AS q
					 
							ON p.indent_num=q.indent_num 	
				
								where i.is_deleted='0' group BY i.inventory_id order by i.dispatch_date DESC,i.Entry_Date DESC,i.indent_status ASC";

	///i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND 

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}
public function get_no_utilization_data($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
if ($flag==1) {
 $query = "SELECT i1.*,i3.rem AS available_quantity,i2.inventory_id,'0' AS is_utilized FROM 
				(SELECT i.id_mst_drugs,i.inventory_id,i.drug_name,i.batch_num,i.from_Date,i.to_Date,i.dispensed_quantity,type,i.repeat_quantity,i.control_used,i.utilization_purpose,s.strength FROM `tbl_inventory` i INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength WHERE ((((Flag='R' AND i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.Entry_Date BETWEEN '".$date1."' AND '".$date2."' ) OR FLag='F' OR FLag='U' OR Flag='L') AND id_mstfacility=".$loginData->id_mstfacility.") or (relocated_quantity!=0 or relocated_quantity is not null and Flag='I' AND transfer_to=".$loginData->id_mstfacility.")) AND i.is_deleted='0' and i.is_temp='0' AND ".$itemname.") AS i1
					LEFT join
			(SELECT inventory_id,batch_num FROM tbl_inventory i WHERE Flag='U' AND utilization_purpose IS NOT NULL AND  is_deleted='0' AND ".$itemname." AND i.id_mstfacility=".$loginData->id_mstfacility.") AS i2
			ON i1.batch_num=i2.batch_num
				LEFT JOIN 
				(SELECT SUM(IFNULL((case when id_mstfacility=".$loginData->id_mstfacility." then quantity_received ELSE 0 END ),0)-(ifnull(quantity_rejected,0)+ifnull(dispensed_quantity,0)+ifnull(control_used,0)+ifnull(returned_quantity,0)+ IFNULL((case when (transfer_to=".$loginData->id_mstfacility." and Flag='I') then relocated_quantity when (id_mstfacility=".$loginData->id_mstfacility." and Flag='L') then relocated_quantity ELSE 0 END),0))) as rem,type,batch_num,inventory_id from tbl_inventory i WHERE i.is_deleted ='0' AND ".$itemname." and (id_mstfacility=".$loginData->id_mstfacility." or transfer_to=".$loginData->id_mstfacility.")  GROUP BY batch_num ) AS i3
			ON i1.inventory_id=i3.inventory_id
			WHERE i2.inventory_id IS NULL and i3.rem>0 GROUP BY batch_num";
}
if ($flag==2) {
	$query="SELECT i.id_mst_drugs,i.inventory_id,i.type,i.indent_num,i.drug_name,i.batch_num,i.from_Date,i.to_Date,i.dispensed_quantity,i.repeat_quantity,i.control_used,i.utilization_purpose,s.strength,'1' as is_utilized,available_quantity FROM `tbl_inventory` i left join mstfacility f on i.id_mstfacility=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength where i.is_deleted='0' and i.is_temp='0' AND  to_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND  to_Date BETWEEN '".$date1."' AND '".$date2."' AND ".$itemname." ".$sess_where." AND Flag='U' ORDER BY to_Date DESC";
}
if ($flag==3) {
	  $query = "SELECT i1.*,i3.rem AS available_quantity,'0' AS is_utilized FROM 
				(SELECT i.id_mst_drugs,i.inventory_id,i.drug_name,i.batch_num,i.from_Date,i.to_Date,i.dispensed_quantity,type,i.repeat_quantity,i.control_used,i.utilization_purpose,s.strength FROM `tbl_inventory` i INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength WHERE ((((Flag='R' AND i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.Entry_Date BETWEEN '".$date1."' AND '".$date2."' ) OR FLag='F' OR FLag='U' OR Flag='L') AND id_mstfacility=".$loginData->id_mstfacility.") or (relocated_quantity!=0 or relocated_quantity is not null and Flag='I' AND transfer_to=".$loginData->id_mstfacility.")) AND i.is_deleted='0' and i.is_temp='0' AND ".$itemname.") AS i1
					LEFT join
			(SELECT inventory_id,batch_num FROM tbl_inventory i WHERE Flag='U' AND utilization_purpose IS NOT NULL AND  is_deleted='0' AND ".$itemname." AND i.id_mstfacility=".$loginData->id_mstfacility.") AS i2
			ON i1.batch_num=i2.batch_num
				LEFT JOIN 
				(SELECT SUM(IFNULL((case when id_mstfacility=".$loginData->id_mstfacility." then quantity_received ELSE 0 END ),0)-(ifnull(quantity_rejected,0)+ifnull(dispensed_quantity,0)+ifnull(control_used,0)+ifnull(returned_quantity,0)+ IFNULL((case when (transfer_to=".$loginData->id_mstfacility." and Flag='I') then relocated_quantity when (id_mstfacility=".$loginData->id_mstfacility." and Flag='L') then relocated_quantity ELSE 0 END),0))) as rem,type,batch_num,inventory_id from tbl_inventory i WHERE i.is_deleted ='0' AND ".$itemname." and (id_mstfacility=".$loginData->id_mstfacility." or transfer_to=".$loginData->id_mstfacility.")  GROUP BY batch_num ) AS i3
			ON i1.inventory_id=i3.inventory_id
			WHERE i2.inventory_id IS NULL and i3.rem>0 GROUP BY batch_num
			union
			(SELECT i.id_mst_drugs,i.inventory_id,i.drug_name,i.batch_num,i.from_Date,i.to_Date,i.dispensed_quantity,i.type,i.repeat_quantity,i.control_used,i.utilization_purpose,s.strength,available_quantity,'1' as is_utilized FROM `tbl_inventory` i left join mstfacility f on i.id_mstfacility=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength where i.is_deleted='0' and i.is_temp='0' AND  to_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND  to_Date BETWEEN '".$date1."' AND '".$date2."' AND ".$itemname." ".$sess_where." AND Flag='U' ORDER BY to_Date DESC)
				";
}
			  
		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}
public function  get_items($drug_id=NULL,$type=NULL,$flag=NULL){

		$loginData = $this->session->userdata('loginData'); 
		
		


				if ($drug_id=='' || $drug_id==NULL) {
				 $drug_id= ' AND 1';
				}
				else
				{
				$drug_id= " AND s.id_mst_drug_strength = ".$drug_id."";
		
				}
				if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
				if ($flag!=NULL || $flag!='') {
				$left_join=" LEFT JOIN tbl_inventory i on s.id_mst_drug_strength=i.id_mst_drugs";
				if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
				}
				elseif( ($loginData) && $loginData->user_type == '2' ){

						$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
					}
				elseif( ($loginData) && $loginData->user_type == '3' ){ 
						$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
					}
				elseif( ($loginData) && $loginData->user_type == '4' ){ 
						$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
					}

				if ($flag=='I') {
					$orderby="GROUP by i.id_mst_drugs ORDER BY i.type DESC,i.indent_date DESC";
					$flag='';
				}

				if ($flag=='R') {
					$orderby="ORDER BY i.type DESC,i.Entry_Date DESC";
					$flag='';
				}
				if ($flag=='F') {
					$orderby="GROUP by i.id_mst_drugs ORDER BY i.type DESC,i.Entry_Date DESC ";
					 $flag="AND i.Flag='R' ";
				}
				if ($flag=='U') {
					$orderby="GROUP by i.id_mst_drugs ORDER BY i.type DESC,i.to_Date DESC";
					 $flag="AND i.Flag='U' ";
				}
				if ($flag=='L') {
					$orderby="GROUP by i.id_mst_drugs ORDER BY i.type DESC,i.request_date DESC";
					 $flag="";
					 $sess_where="AND ((i.Flag='L' AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."') or (i.Flag='I' and transfer_to='".$loginData->id_mstfacility."'))";
				}
				}
				else{
					$left_join='';
					$orderby='ORDER BY d.type DESC,d.id_mst_drugs ASC';
				 $sess_where='';
				 $flag='';
				}
			
		  $query = "SELECT d.*,s.* FROM `mst_drugs` d left join `mst_drug_strength` s on d.id_mst_drugs=s.id_mst_drug".$left_join." where d.is_deleted=0 and ((d.id_mst_drugs=1 and s.strength=400) or (d.id_mst_drugs=4 and (s.strength=30 or s.strength=60)) or (d.id_mst_drugs=3 and s.strength=200) or (d.id_mst_drugs=8 and s.strength=100) or (d.id_mst_drugs=6) or (d.id_mst_drugs=7))".$flag." ".$type." ".$sess_where."".$drug_id." ".$orderby;

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}

function delete_receipt($id=NULL)
{
	$loginData = $this->session->userdata('loginData'); 
   $this->db->where('inventory_id', $id);
   $data=array('is_deleted'=>'1',"updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
   $this->db->update('tbl_inventory',$data); 
}


public function edit_inventory_receipt($inventory_id=NULL){

		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
			}

		 $query = "SELECT * FROM `tbl_inventory` where is_deleted='0' AND inventory_id=".$inventory_id." ".$sess_where;

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
public function get_receipt_in_transfer_out($id_mst_drugs=NULL,$batch_num=NULL){
		
		//echo "///".$id_mst_drugs;
		if($id_mst_drugs==NULL)
		{
			$and="AND Acceptance_Status='1' GROUP BY id_mst_drugs";
			$and1="AND Acceptance_Status='1' GROUP BY id_mst_drugs";
			$where="a.Remaining>0";
		}
		elseif ($id_mst_drugs!=NULL && $batch_num!=NULL) {
			$and="AND i.id_mst_drugs='".$id_mst_drugs."' AND i.batch_num ='".$batch_num."' AND Acceptance_Status='1' GROUP BY batch_num,id_mst_drugs";
			$and1="AND id_mst_drugs='".$id_mst_drugs."' AND batch_num ='".$batch_num."' AND Acceptance_Status='1' GROUP BY batch_num,id_mst_drugs";
			$where="a.Remaining>0";
		}
		else{
			$and="AND i.id_mst_drugs='".$id_mst_drugs."' AND Acceptance_Status='1' GROUP BY batch_num,id_mst_drugs";
			$and1="AND id_mst_drugs='".$id_mst_drugs."' GROUP BY batch_num,id_mst_drugs";
			$where="a.Remaining>0";
		}
		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_wherep = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "i.id_mststate = '".$loginData->State_ID."'";
				$sess_wherep = "id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "i.id_mststate = '".$loginData->State_ID."' ";
				$sess_wherep = "id_mststate = '".$loginData->State_ID."' ";
			}

	$query = "SELECT * from (SELECT (t1.cnt)-(ifnull(t2.cnt,0)) AS remaining,t1.batch_num
from (SELECT ifnull(SUM(quantity),0) AS cnt,batch_num,id_mst_drugs FROM tbl_inventory WHERE flag ='R' AND is_deleted='0' AND Acceptance_Status='1'AND ".$sess_wherep." AND flag='R' ".$and1.") as t1
left join (SELECT ifnull(SUM(quantity),0) AS cnt,batch_num,id_mst_drugs FROM tbl_inventory WHERE flag IN('T','U','W') AND is_deleted='0' AND ".$sess_wherep." ".$and1.") as t2 ON t1.batch_num=t2.batch_num) a
  LEFT JOIN 
   (SELECT i.id_mst_drugs,i.drug_name,Expiry_Date,i.type,i.batch_num,s.strength,n.drug_name as name FROM `tbl_inventory` i inner join `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs  where i.is_deleted='0' AND Acceptance_Status='1' AND ".$sess_where." AND flag='R' ".$and.") b
   ON a.batch_num=b.batch_num WHERE ".$where." AND b.Expiry_Date>=CURDATE()";

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}	
function get_all_facilities(){
	$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."' and id_mstfacility!='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}

 $query = "SELECT id_mstfacility,facility_short_name FROM `mstfacility` where ".$sess_where;

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}	
public function Remaining_stock($batch_num=NULL,$type=NULL){
		if($type==1){
			$quantity='quantity';
		}
		elseif($type==2){
			$quantity='quantity_screening_tests';
		}
		else{
			$quantity='quantity';
		}
		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
			}

		 $query = "SELECT ((SELECT ifnull(SUM(".$quantity."),0) FROM tbl_inventory WHERE flag ='R' AND is_deleted='0' AND Acceptance_Status='1' AND batch_num='".$batch_num."' ".$sess_where.") - (SELECT ifnull(SUM(".$quantity."),0) FROM tbl_inventory WHERE flag IN('T','U','W') AND is_deleted='0' AND batch_num='".$batch_num."' ".$sess_where.")) AS Remaining";

					   // print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
public function getmaxdate($batch_num=NULL){
		
		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
			}

		 $query = "SELECT date_format(adddate(MAX(Expiry_Date),INTERVAL 1 DAY),'%d-%m-%Y') AS Last_Date from tbl_inventory WHERE flag='U' AND is_deleted='0' AND Acceptance_Status='1' AND batch_num='".$batch_num."' ".$sess_where."";
					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
   public function get_new_request($flag=NULL){

		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
		
		 $query = "SELECT i.*,f.facility_short_name,s.strength,d.drug_name as drug FROM `tbl_inventory` i left join mstfacility f on i.id_mstfacility=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength INNER join mst_drugs d on i.drug_name=d.id_mst_drugs where i.is_deleted=? AND i.flag=? and i.id_mstfacility=? and relocation_status=? order by dispatch_date";

					  /*print_r($query); die();*/

		$result1 = $this->db->query($query,['0','I',$loginData->id_mstfacility,1])->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
	 public function get_new_relocation_request($flag=NULL){

		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
		
		 $query = "SELECT i.*,f.facility_short_name,s.strength,d.drug_name as drug FROM `tbl_inventory` i left join mstfacility f on i.id_mstfacility=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength INNER join mst_drugs d on i.drug_name=d.id_mst_drugs where i.is_deleted=? AND i.flag=? and transfer_to=? and indent_status!=? order by indent_accept_date";

					  /*print_r($query); die();*/

		$result1 = $this->db->query($query,['0','I',$loginData->id_mstfacility,0])->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
public function get_rem_item_name($inventory_id=NULL){

		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
			}
			if ($inventory_id!="" || $inventory_id!=NULL) {
		$qry=" and inventory_id!=?";
		$arr=[$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$loginData->id_mstfacility,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$loginData->id_mstfacility,$loginData->id_mstfacility,$inventory_id];
	}
	else{
		$qry="";
		$arr=[$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$loginData->id_mstfacility,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$loginData->id_mstfacility,$loginData->id_mstfacility,];
	}
		 $query = "SELECT dr.drug_name,dr.strength,dr.type,dr.id_mst_drug_strength FROM 
				(SELECT Flag,inventory_id,batch_num,id_mst_drugs FROM tbl_inventory WHERE (((Flag='R' OR FLag='F' OR FLag='U' OR Flag='L' or Flag='I') AND id_mstfacility=?) or (relocated_quantity!=0 or relocated_quantity is not null and Flag='I' AND transfer_to=?)) and is_deleted=?) AS p
			LEFT JOIN 
				(SELECT SUM(IFNULL((case when id_mstfacility=? then quantity_received ELSE 0 END ),0)-(ifnull(quantity_rejected,0)+ifnull(dispensed_quantity,0)+ifnull(control_used,0)+ifnull(returned_quantity,0)+ IFNULL((case when (transfer_to=? and Flag='I') then relocated_quantity when (id_mstfacility=? and Flag='L') then relocated_quantity ELSE 0 END),0))) as rem,type,batch_num,inventory_id,id_mst_drugs from tbl_inventory where is_deleted=? and (id_mstfacility=? or transfer_to=?) ".$qry." GROUP BY batch_num) AS i1
				ON p.inventory_id=i1.inventory_id
			  INNER JOIN (SELECT d.drug_name,s.strength,s.id_mst_drug_strength,d.type FROM `mst_drugs` d left join `mst_drug_strength` s on d.id_mst_drugs=s.id_mst_drug where ((d.id_mst_drugs=1 and s.strength=400) or (d.id_mst_drugs=4 and (s.strength=30 or s.strength=60)) or (d.id_mst_drugs=3 and s.strength=200) or (d.id_mst_drugs=8 and s.strength=100) or (d.id_mst_drugs=6) or (d.id_mst_drugs=7))) AS dr
			 on dr.id_mst_drug_strength=p.id_mst_drugs
			 WHERE i1.rem>0
			 GROUP BY p.id_mst_drugs";
					     //print_r($query); die();

		$result1 = $this->db->query($query,$arr)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}	
public function get_pending_indent($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			if ($filter['id_mstfacility']!='' || $filter['id_mstfacility']!=NULL) {
			$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' and i.id_mstfacility='".$filter['id_mstfacility']."'";
			
			}
			else{
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
				
			}
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		   $query = "SELECT i.id_mst_drugs,i.inventory_id,i.indent_num,i.drug_name,i.type,i.indent_date,i.indent_remark,i.quantity,i.approved_quantity,i.indent_remark,i.indent_accept_date,i.relocation_status,i.indent_status,f.facility_short_name,s.strength,d.drug_name as drug FROM `tbl_inventory` i left join mstfacility f on i.id_mstfacility=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength INNER join mst_drugs d on i.drug_name=d.id_mst_drugs WHERE i.is_deleted='0' AND (i.flag='I' AND (i.relocation_status is null or i.relocation_status=0) ||  (i.indent_accept_date is null or i.indent_accept_date='000-00-00') ) AND i.quantity is not null and  i.indent_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND  i.indent_date BETWEEN '".$date1."' AND '".$date2."' AND ".$itemname." ".$sess_where;
		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}		

public function get_indent_status($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
					$sess_where1 = " 1";
			}
				elseif( ($loginData) && $loginData->user_type == '3' ){ 
			if ($filter['id_mstfacility']!='' || $filter['id_mstfacility']!=NULL) {
			$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' and i.id_mstfacility='".$filter['id_mstfacility']."'";
			$sess_where1 = " id_mststate = '".$loginData->State_ID."' and id_mstfacility='".$filter['id_mstfacility']."'";
			}
			else{
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
				$sess_where1 = " id_mststate = '".$loginData->State_ID."'";
			}
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		    $query = "SELECT * FROM 
						(SELECT i.*,f.facility_short_name,s.strength,d.drug_name as drug FROM tbl_inventory i left join mstfacility f on i.id_mstfacility=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength INNER join mst_drugs d on i.drug_name=d.id_mst_drugs WHERE (i.flag='I' || i.flag='R') AND (i.indent_status IS NOT NULL OR i.indent_status>0) AND (i.requested_quantity IS NOT NULL OR i.requested_quantity=0)  AND i.indent_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND  i.indent_date BETWEEN '".$date1."' AND '".$date2."' AND ".$itemname." ".$sess_where." GROUP BY i.indent_num ) AS p
						LEFT JOIN 
						(SELECT facility_short_name AS req_facility,id_mstfacility FROM mstfacility WHERE ".$sess_where1.")AS t
						ON 
						t.id_mstfacility=p.transfer_to";
		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}		
public function get_all_indent_data($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
					$sess_where1 = " 1";
			}
	elseif( ($loginData) && $loginData->user_type == '3' ){ 
			if ($filter['id_mstfacility']!='' || $filter['id_mstfacility']!=NULL) {
			$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' and i.id_mstfacility='".$filter['id_mstfacility']."'";
			$sess_where1 = " id_mststate = '".$loginData->State_ID."' and id_mstfacility='".$filter['id_mstfacility']."'";
			}
			else{
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
				$sess_where1 = " id_mststate = '".$loginData->State_ID."'";
			}
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		    $query = "SELECT * FROM 
						(SELECT i.*,f.facility_short_name,s.strength,d.drug_name as drug FROM tbl_inventory i left join mstfacility f on i.id_mstfacility=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength INNER join mst_drugs d on i.drug_name=d.id_mst_drugs WHERE (((i.flag='I' AND (i.indent_status IS NOT NULL OR i.indent_status>0) and (indent_accept_date is not null or indent_accept_date='0000-00-00')) or (i.flag='I' AND (i.indent_status IS NULL OR i.indent_status=0 OR i.indent_status>0) and relocation_status>=1)) OR  (i.flag='R' AND (i.indent_status IS NOT NULL OR i.indent_status>0)) OR  (i.flag='L' AND (i.indent_status IS NULL OR i.indent_status=0))) AND ((i.indent_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND  i.indent_date BETWEEN '".$date1."' AND '".$date2."') || (i.dispatch_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND  i.dispatch_date BETWEEN '".$date1."' AND '".$date2."')) AND ".$itemname." ".$sess_where." ) AS p
						LEFT JOIN 
						(SELECT facility_short_name AS req_facility,id_mstfacility FROM mstfacility WHERE ".$sess_where1.")AS t
						ON 
						t.id_mstfacility=p.transfer_to";
		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}		
public function count_new_receipt($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 

		 $query = "SELECT i.count,i.is_new FROM
				 (SELECT inventory_id FROM tbl_inventory i WHERE (id_mstfacility='".$loginData->id_mstfacility."' OR transfer_to='".$loginData->id_mstfacility."') AND is_temp='0') AS p
				 LEFT JOIN 
						(SELECT COUNT(i.relocation_status) as COUNT,COUNT(CASE WHEN i.is_notification = '0' then 1 ELSE NULL END) as is_new,i.inventory_id,refrence_id,relocation_status,i.is_notification,i.is_deleted FROM tbl_inventory i WHERE ((Flag='I'  AND id_mstfacility='".$loginData->id_mstfacility."' AND relocation_status<=2 AND relocation_status IS NOT NULL) OR (Flag='L' AND transfer_to='".$loginData->id_mstfacility."' AND relocation_status<=2 and relocation_status is not null)) AND i.is_deleted='0') AS i
						ON p.inventory_id=i.inventory_id	
				
								where 1 AND i.is_deleted='0' group BY i.inventory_id";

	///i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND 

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}
public function count_new_relocation_request($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 

		 $query = "SELECT COUNT(inventory_id) as count,COUNT(CASE WHEN (i.is_notification = '0') then 1 ELSE NULL END) as is_new FROM `tbl_inventory` i where ((i.flag='I' AND i.transfer_to='".$loginData->id_mstfacility."' AND i.indent_status IS NOT NULL And (i.indent_accept_date is not null || i.indent_accept_date!='0000-00-00' ) AND relocation_status IS null ) OR ((i.flag='R' or i.Flag='I') AND i.transfer_to='".$loginData->id_mstfacility."' AND i.is_temp='1')) and is_deleted='0'";

	///i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND 

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}
public function get_rem_indent_item($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 

		$query = "SELECT distinct dr.drug_name,dr.strength,dr.type,dr.id_mst_drug_strength,i1.rem,i1.type,i1.indent_num FROM 
			(SELECT id_mst_drugs,Flag,inventory_id,indent_num FROM tbl_inventory WHERE (Flag='I' OR Flag='R') AND is_deleted ='0'  and id_mstfacility='".$loginData->id_mstfacility."' AND (from_to_type=1 or from_to_type=2) ) AS p
			LEFT JOIN 
			(SELECT SUM((ifnull(quantity,0)-ifnull(quantity_received,0))+ifnull(quantity_rejected,0))as rem,type,indent_num,inventory_id from tbl_inventory where is_deleted ='0'  and id_mstfacility='".$loginData->id_mstfacility."' GROUP BY indent_num
			) AS i1
			ON p.inventory_id=i1.inventory_id
			INNER JOIN (SELECT d.drug_name,s.strength,s.id_mst_drug_strength,d.type FROM `mst_drugs` d left join `mst_drug_strength` s on d.id_mst_drugs=s.id_mst_drug where ((d.id_mst_drugs=1 and s.strength=400) or (d.id_mst_drugs=4 and (s.strength=30 or s.strength=60)) or (d.id_mst_drugs=3 and s.strength=200) or (d.id_mst_drugs=8 and s.strength=100) or (d.id_mst_drugs=6) or (d.id_mst_drugs=7))) AS dr
						 on dr.id_mst_drug_strength=p.id_mst_drugs group by dr.id_mst_drug_strength";

	///i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND 

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}
}
