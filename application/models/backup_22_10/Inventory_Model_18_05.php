<?php 
class Inventory_Model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
		// $this->file_path = realpath(APPPATH . '../datafiles');
		// $this->banner_path = realpath(APPPATH . '../banners');
		// $this->gallery_path_url = base_url().'datafiles/';
    }
   public function get_inventory_details($flag=NULL){

		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
		$is_utilized="";
		if($flag==NULL){
			$flag="AND i.flag='R'";
			$facility_id="source_name";
			$date_type="i.Entry_Date";
		}
		else if($flag=='R'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="source_name";
			$date_type="i.Entry_Date";
		}
		else if($flag=='L'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="transfer_to";
			$date_type="i.dispatch_date";
		}
		else if($flag=='U'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="id_mstfacility";
			$date_type="i.from_Date";
			$is_utilized=",'1' as is_utilized";
		}
		else if($flag=='I'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="id_mstfacility";
			$date_type="i.indent_date";

		}
		else if($flag=='F'){
			$flag="AND i.flag='".$flag."'";
			$facility_id="id_mstfacility";
			$date_type="i.failure_date";

		}

		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		   $query = "SELECT i.*,f.facility_short_name,s.strength".$is_utilized." FROM `tbl_inventory` i left join mstfacility f on i.".$facility_id."=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength where i.is_deleted='0' AND  ".$date_type." BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND  ".$date_type." BETWEEN '".$date1."' AND '".$date2."' AND ".$itemname." ".$sess_where." ".$flag." ORDER BY ".$date_type." DESC";

					  /*print_r($query); die();*/

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
public function get_indent_data($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		  $query = "SELECT i2.*,case when i3.indent_num IS NULL then '0' ELSE '1' end as is_receipt FROM 
				(SELECT indent_num,inventory_id FROM tbl_inventory i WHERE (Flag='R' OR Flag='I') AND is_deleted='0' AND ".$itemname." ".$sess_where.") AS i1
				LEFT JOIN 
				(SELECT i.id_mst_drugs,i.inventory_id,i.indent_num,i.drug_name,i.type,i.indent_date,i.indent_remark,i.quantity,i.approved_quantity,i.indent_accept_date,i.indent_status,s.strength FROM `tbl_inventory` i INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength where i.is_deleted='0' AND i.indent_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.indent_date BETWEEN '".$date1."' AND '".$date2."' AND ".$itemname." ".$sess_where." AND i.Flag='I' and (i.pending_quantity=0 or i.pending_quantity is null) ORDER BY i.indent_date DESC) AS i2
				ON i1.inventory_id=i2.inventory_id
				LEFT JOIN 
				(SELECT indent_num,inventory_id FROM tbl_inventory WHERE Flag='R' AND is_deleted='0') AS i3
				ON i1.indent_num=i3.indent_num
				WHERE i2.inventory_id IS NOT NULL";
		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}
public function get_relocation_data($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		   $query = "SELECT case when indent_status IS NULL then 'm' ELSE 'n' END AS status,f.facility_short_name,i.id_mst_drugs,i.inventory_id,i.indent_num,i.drug_name,i.type,i.dispatch_date,i.relocated_quantity,i.request_date,i.relocation_status,i.indent_status,i.relocation_remark,i.issue_num,i.from_to_type,i.batch_num,case when i.requested_quantity is null then i.pending_quantity when i.requested_quantity=0 then i.pending_quantity else i.requested_quantity end as requested_quantity,s.strength FROM `tbl_inventory` i left join mstfacility f on case when indent_status IS NULL then i.transfer_to=f.id_mstfacility when indent_status IS NOT NULL then i.id_mstfacility=f.id_mstfacility  ELSE 0 END = 1 INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength where i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND ".$itemname." AND i.is_deleted='0' and (i.flag='I' AND i.transfer_to=? AND i.indent_status IS NOT NULL ) OR ( i.flag='L' AND i.id_mstfacility=?)";
		$result1 = $this->db->query($query,[$loginData->id_mstfacility,$loginData->id_mstfacility])->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}
public function get_receiving_data($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		    $query = " SELECT f.facility_short_name,i.*,s.* FROM
						(SELECT case when (Flag='I' OR flag='R') then transfer_to ELSE id_mstfacility end as facility,case when Flag='R' then 'm' ELSE 'n' END AS status,i.*  FROM tbl_inventory i WHERE (((Flag='I' AND (relocation_status=2) ) OR flag='R') AND (id_mstfacility=?)) OR (Flag='L' AND (transfer_to=?)) AND relocation_status=2) AS i
						 LEFT JOIN
						  (SELECT id_mstfacility,facility_short_name FROM mstfacility) AS f
						 		ON i.facility=f.id_mstfacility
						 INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON i.id_mst_drugs=s.id_mst_drug_strength 
								where ".$itemname." AND i.is_deleted='0'";

	///i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND 

		$result1 = $this->db->query($query,[$loginData->id_mstfacility,$loginData->id_mstfacility])->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}
public function get_no_utilization_data($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		  $query = "SELECT i2.*,case when i3.indent_num IS NULL then '0' ELSE '1' end as is_utilized,case when i3.available_quantity IS NULL then i4.rem ELSE i3.available_quantity end as available_quantity FROM 
				(SELECT Flag,inventory_id,batch_num,indent_num FROM tbl_inventory WHERE (((Flag='R' OR FLag='F' OR FLag='U' OR Flag='L') AND id_mstfacility=".$loginData->id_mstfacility.") or (relocated_quantity!=0 or relocated_quantity is not null and Flag='I' AND transfer_to=".$loginData->id_mstfacility.")) AND is_deleted='0' AND ".$itemname.") AS i1
				LEFT JOIN 
				(SELECT i.id_mst_drugs,i.inventory_id,i.indent_num,i.drug_name,i.type,i.batch_num,i.from_Date,i.to_Date,i.dispensed_quantity,i.repeat_quantity,i.control_used,i.utilization_purpose,s.strength FROM `tbl_inventory` i INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength where i.is_deleted='0' AND i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.Entry_Date BETWEEN '".$date1."' AND '".$date2."' AND ".$itemname." ".$sess_where." AND i.flag='R' ORDER BY Entry_Date)AS i2
				ON i1.inventory_id=i2.inventory_id
				LEFT JOIN 
				(SELECT available_quantity,indent_num,inventory_id,type FROM tbl_inventory i WHERE Flag='U' AND is_deleted='0' AND ".$itemname." ".$sess_where.") AS i3
				ON i1.indent_num=i3.indent_num
			LEFT JOIN 
				(SELECT SUM(IFNULL((case when id_mstfacility=".$loginData->id_mstfacility." then quantity_received ELSE 0 END ),0)-(ifnull(quantity_rejected,0)+ifnull(dispensed_quantity,0)+ifnull(control_used,0)+ifnull(returned_quantity,0)+ IFNULL((case when (transfer_to=".$loginData->id_mstfacility." and Flag='I') then relocated_quantity when (id_mstfacility=".$loginData->id_mstfacility." and Flag='L') then relocated_quantity ELSE 0 END),0))) as rem,type,batch_num,inventory_id from tbl_inventory i where is_deleted ='0' AND ".$itemname." and (id_mstfacility=".$loginData->id_mstfacility." or transfer_to=".$loginData->id_mstfacility.") GROUP BY batch_num ) AS i4
			ON i1.inventory_id=i4.inventory_id	
				WHERE i2.inventory_id IS not NULL AND i3.inventory_id IS NULL and (case when i3.available_quantity IS NULL then i4.rem ELSE i3.available_quantity end)> 0";
		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}
public function get_items($drug_id=NULL,$type=NULL,$flag=NULL){

		$loginData = $this->session->userdata('loginData'); 
		
		


				if ($drug_id=='' || $drug_id==NULL) {
				 $drug_id= ' AND 1';
				}
				else
				{
				$drug_id= " AND s.id_mst_drug_strength = ".$drug_id."";
		
				}
				if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
				if ($flag!=NULL || $flag!='') {
				$left_join=" LEFT JOIN tbl_inventory i on s.id_mst_drug_strength=i.id_mst_drugs";
				if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
				}
				elseif( ($loginData) && $loginData->user_type == '2' ){

						$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
					}
				elseif( ($loginData) && $loginData->user_type == '3' ){ 
						$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
					}
				elseif( ($loginData) && $loginData->user_type == '4' ){ 
						$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
					}

				if ($flag=='I') {
					$orderby="GROUP by i.id_mst_drugs ORDER BY i.indent_date DESC";
					$flag='';
				}

				if ($flag=='R') {
					$orderby="ORDER BY i.Entry_Date DESC";
					$flag='';
				}
				if ($flag=='F') {
					$orderby="GROUP by i.id_mst_drugs ORDER BY i.Entry_Date DESC ";
					 $flag="AND i.Flag='R' ";
				}
				if ($flag=='U') {
					$orderby="GROUP by i.id_mst_drugs ORDER BY i.to_Date DESC";
					 $flag="AND i.Flag='U' ";
				}
				if ($flag=='L') {
					$orderby="GROUP by i.id_mst_drugs ORDER BY i.dispatch_date DESC";
					 $flag="AND i.Flag='L' ";
				}
				}
				else{
					$left_join='';
					$orderby='ORDER BY d.id_mst_drugs';
				 $sess_where='';
				 $flag='';
				}
			
		  $query = "SELECT d.*,s.* FROM `mst_drugs` d left join `mst_drug_strength` s on d.id_mst_drugs=s.id_mst_drug".$left_join." where d.is_deleted=0 ".$flag." ".$type." ".$sess_where."".$drug_id." ".$orderby;

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}

function delete_receipt($id=NULL)
{
	$loginData = $this->session->userdata('loginData'); 
   $this->db->where('inventory_id', $id);
   $data=array('is_deleted'=>'1',"updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
   $this->db->update('tbl_inventory',$data); 
}


public function edit_inventory_receipt($inventory_id=NULL){

		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
			}

		 $query = "SELECT * FROM `tbl_inventory` where is_deleted='0' AND inventory_id=".$inventory_id." ".$sess_where;

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
public function get_receipt_in_transfer_out($id_mst_drugs=NULL,$batch_num=NULL){
		
		//echo "///".$id_mst_drugs;
		if($id_mst_drugs==NULL)
		{
			$and="AND Acceptance_Status='1' GROUP BY id_mst_drugs";
			$and1="AND Acceptance_Status='1' GROUP BY id_mst_drugs";
			$where="a.Remaining>0";
		}
		elseif ($id_mst_drugs!=NULL && $batch_num!=NULL) {
			$and="AND i.id_mst_drugs='".$id_mst_drugs."' AND i.batch_num ='".$batch_num."' AND Acceptance_Status='1' GROUP BY batch_num,id_mst_drugs";
			$and1="AND id_mst_drugs='".$id_mst_drugs."' AND batch_num ='".$batch_num."' AND Acceptance_Status='1' GROUP BY batch_num,id_mst_drugs";
			$where="a.Remaining>0";
		}
		else{
			$and="AND i.id_mst_drugs='".$id_mst_drugs."' AND Acceptance_Status='1' GROUP BY batch_num,id_mst_drugs";
			$and1="AND id_mst_drugs='".$id_mst_drugs."' GROUP BY batch_num,id_mst_drugs";
			$where="a.Remaining>0";
		}
		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_wherep = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "i.id_mststate = '".$loginData->State_ID."'";
				$sess_wherep = "id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "i.id_mststate = '".$loginData->State_ID."' ";
				$sess_wherep = "id_mststate = '".$loginData->State_ID."' ";
			}

	$query = "SELECT * from (SELECT (t1.cnt)-(ifnull(t2.cnt,0)) AS remaining,t1.batch_num
from (SELECT ifnull(SUM(quantity),0) AS cnt,batch_num,id_mst_drugs FROM tbl_inventory WHERE flag ='R' AND is_deleted='0' AND Acceptance_Status='1'AND ".$sess_wherep." AND flag='R' ".$and1.") as t1
left join (SELECT ifnull(SUM(quantity),0) AS cnt,batch_num,id_mst_drugs FROM tbl_inventory WHERE flag IN('T','U','W') AND is_deleted='0' AND ".$sess_wherep." ".$and1.") as t2 ON t1.batch_num=t2.batch_num) a
  LEFT JOIN 
   (SELECT i.id_mst_drugs,i.drug_name,Expiry_Date,i.type,i.batch_num,s.strength,n.drug_name as name FROM `tbl_inventory` i inner join `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs  where i.is_deleted='0' AND Acceptance_Status='1' AND ".$sess_where." AND flag='R' ".$and.") b
   ON a.batch_num=b.batch_num WHERE ".$where." AND b.Expiry_Date>=CURDATE()";

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}	
function get_all_facilities(){
	$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}

$query = "SELECT id_mstfacility,facility_short_name FROM `mstfacility` where ".$sess_where;

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}	
public function Remaining_stock($batch_num=NULL,$type=NULL){
		if($type==1){
			$quantity='quantity';
		}
		elseif($type==2){
			$quantity='quantity_screening_tests';
		}
		else{
			$quantity='quantity';
		}
		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
			}

		 $query = "SELECT ((SELECT ifnull(SUM(".$quantity."),0) FROM tbl_inventory WHERE flag ='R' AND is_deleted='0' AND Acceptance_Status='1' AND batch_num='".$batch_num."' ".$sess_where.") - (SELECT ifnull(SUM(".$quantity."),0) FROM tbl_inventory WHERE flag IN('T','U','W') AND is_deleted='0' AND batch_num='".$batch_num."' ".$sess_where.")) AS Remaining";

					   // print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
public function getmaxdate($batch_num=NULL){
		
		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
			}

		 $query = "SELECT date_format(adddate(MAX(Expiry_Date),INTERVAL 1 DAY),'%d-%m-%Y') AS Last_Date from tbl_inventory WHERE flag='U' AND is_deleted='0' AND Acceptance_Status='1' AND batch_num='".$batch_num."' ".$sess_where."";
					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
   public function get_new_request($flag=NULL){

		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
		
		 $query = "SELECT i.*,f.facility_short_name,s.strength,d.drug_name as drug FROM `tbl_inventory` i left join mstfacility f on i.id_mstfacility=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength INNER join mst_drugs d on i.drug_name=d.id_mst_drugs where i.is_deleted=? AND i.flag=? and i.id_mstfacility=? and relocation_status=? order by dispatch_date";

					  /*print_r($query); die();*/

		$result1 = $this->db->query($query,['0','I',$loginData->id_mstfacility,1])->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
	 public function get_new_relocation_request($flag=NULL){

		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
		
		 $query = "SELECT i.*,f.facility_short_name,s.strength,d.drug_name as drug FROM `tbl_inventory` i left join mstfacility f on i.id_mstfacility=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength INNER join mst_drugs d on i.drug_name=d.id_mst_drugs where i.is_deleted=? AND i.flag=? and transfer_to=? and indent_status!=? order by indent_accept_date";

					  /*print_r($query); die();*/

		$result1 = $this->db->query($query,['0','I',$loginData->id_mstfacility,0])->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}
public function get_rem_item_name($inventory_id=NULL){

		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
			}
			if ($inventory_id!="" || $inventory_id!=NULL) {
		$qry=" and inventory_id!=?";
		$arr=[$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$loginData->id_mstfacility,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$loginData->id_mstfacility,$loginData->id_mstfacility,$inventory_id];
	}
	else{
		$qry="";
		$arr=[$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$loginData->id_mstfacility,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$loginData->id_mstfacility,$loginData->id_mstfacility,];
	}
		 $query = "SELECT dr.drug_name,dr.strength,dr.type,dr.id_mst_drug_strength FROM 
				(SELECT Flag,inventory_id,batch_num,id_mst_drugs FROM tbl_inventory WHERE (((Flag='R' OR FLag='F' OR FLag='U' OR Flag='L' or Flag='I') AND id_mstfacility=?) or (relocated_quantity!=0 or relocated_quantity is not null and Flag='I' AND transfer_to=?)) and is_deleted=?) AS p
			LEFT JOIN 
				(SELECT SUM(IFNULL((case when id_mstfacility=? then quantity_received ELSE 0 END ),0)-(ifnull(quantity_rejected,0)+ifnull(dispensed_quantity,0)+ifnull(control_used,0)+ifnull(returned_quantity,0)+ IFNULL((case when (transfer_to=? and Flag='I') then relocated_quantity when (id_mstfacility=? and Flag='L') then relocated_quantity ELSE 0 END),0))) as rem,type,batch_num,inventory_id,id_mst_drugs from tbl_inventory where is_deleted=? and (id_mstfacility=? or transfer_to=?) ".$qry." GROUP BY batch_num) AS i1
				ON p.inventory_id=i1.inventory_id
			  INNER JOIN (SELECT d.drug_name,s.strength,s.id_mst_drug_strength,d.type FROM `mst_drugs` d left join `mst_drug_strength` s on d.id_mst_drugs=s.id_mst_drug) AS dr
			 on dr.id_mst_drug_strength=p.id_mst_drugs
			 WHERE i1.rem>0
			 GROUP BY p.id_mst_drugs";
					     //print_r($query); die();

		$result1 = $this->db->query($query,$arr)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}	
public function get_pending_indent($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		   $query = "SELECT i.id_mst_drugs,i.inventory_id,i.indent_num,i.drug_name,i.type,i.indent_date,i.indent_remark,i.quantity,i.approved_quantity,i.indent_AcceptReject_date,i.indent_status,f.facility_short_name,s.strength,d.drug_name as drug FROM `tbl_inventory` i left join mstfacility f on i.id_mstfacility=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength INNER join mst_drugs d on i.drug_name=d.id_mst_drugs WHERE i.is_deleted='0' AND i.flag='I' AND indent_status=0 AND (indent_accept_date is null or indent_accept_date='0000-00-00') ".$sess_where;
		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}		

public function get_indent_status($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		    $query = "SELECT * FROM 
						(SELECT i.*,f.facility_short_name,s.strength,d.drug_name as drug FROM tbl_inventory i left join mstfacility f on i.id_mstfacility=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength INNER join mst_drugs d on i.drug_name=d.id_mst_drugs WHERE i.flag='I' AND (i.indent_status IS NOT NULL OR i.indent_status>0) AND (i.requested_quantity IS NOT NULL OR i.requested_quantity=0) ".$sess_where." GROUP BY i.indent_num ) AS p
						LEFT JOIN 
						(SELECT facility_short_name AS req_facility,id_mstfacility FROM mstfacility WHERE id_mststate='".$loginData->State_ID."')AS t
						ON 
						t.id_mstfacility=p.transfer_to";
		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}		
public function get_all_indent_data($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		    $query = "SELECT * FROM 
						(SELECT i.*,f.facility_short_name,s.strength,d.drug_name as drug FROM tbl_inventory i left join mstfacility f on i.id_mstfacility=f.id_mstfacility INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength INNER join mst_drugs d on i.drug_name=d.id_mst_drugs WHERE i.flag='I' AND (i.indent_status IS NOT NULL OR i.indent_status>0) and (indent_accept_date is not null or indent_accept_date='0000-00-00')".$sess_where." ) AS p
						LEFT JOIN 
						(SELECT facility_short_name AS req_facility,id_mstfacility FROM mstfacility WHERE id_mststate='".$loginData->State_ID."')AS t
						ON 
						t.id_mstfacility=p.transfer_to";
		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}		

}
