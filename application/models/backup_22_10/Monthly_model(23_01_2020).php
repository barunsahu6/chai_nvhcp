<?php 
Class Monthly_model extends CI_Model
{
	public function __construct()
	{
		if($this->session->userdata('filters1') != null)
		{
				$filters1                           = $this->session->userdata('filters1');
				
				if($filters1['id_mstfacility']      == 0)
				{
				$loginData = $this->session->userdata('loginData'); 

					if( ($loginData) && $loginData->user_type == '1' ){
					$this->facility_filter = "AND 1";
					$this->facility_filter1 = "AND 1";
					$this->date_filter      = " CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

					//$this->HAVRapidDate      = " HAVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
					$this->HAVRapidDate      = "HAVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."' || HAVElisaDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'" || "HCVOtherDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'" ;

					$this->HBSRapidDate      = " HBSRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
					$this->HCVRapidDate      = " HCVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
					$this->HEVRapidDate      = " HEVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

					$this->HEPCRapidDate      = "HCVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."' || HCVElisaDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'" || "HCVOtherDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'" ;


					$this->date_filter1      = " and p.CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

					}
					elseif( ($loginData) && $loginData->user_type == '2' ){

					$this->facility_filter = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";

					$this->facility_filter1 = "AND p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."' AND p.id_mstfacility='".$loginData->id_mstfacility."'";
					$this->date_filter1      = " and p.CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

					 $this->HAVRapidDate      = "HAVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."' || HAVElisaDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'" || "HCVOtherDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'" ;

					

	


					}
					elseif( ($loginData) && $loginData->user_type == '3' ){ 
					$this->facility_filter = "AND Session_StateID = '".$loginData->State_ID."'";
					$this->facility_filter1 = "AND p.Session_StateID = '".$loginData->State_ID."'";
					$this->date_filter1      = " and p.CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
					}
					elseif( ($loginData) && $loginData->user_type == '4' ){ 
					$this->facility_filter = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
					$this->date_filter1      = " and p.CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

					$this->facility_filter1 = "AND p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."'";
					$this->date_filter1      = " and p.CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
					}
				}
				else
				{
				$this->facility_filter              = "AND id_mstfacility = ".$filters1['id_mstfacility'];
				$this->facility_filter1              = "AND p.id_mstfacility = ".$filters1['id_mstfacility'];
				$this->date_filter1      = " and p.CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
				}

				if($filters1['id_search_state']      == 0)
				{
				$loginData = $this->session->userdata('loginData'); 

					//if( ($loginData) && $loginData->user_type == '1' ){
					$this->facility_state  = "AND 1";
					$this->facility_state1  = "AND 1";
					$this->facility_statesumm  = "AND 1";
					//}
					
				}
				else
				{
				$this->facility_state              = "AND Session_StateID = ".$filters1['id_search_state'];
				$this->facility_state1              = "AND p.Session_StateID = ".$filters1['id_search_state'];
				}

				if($filters1['id_input_district']      == 0)
				{
				$loginData = $this->session->userdata('loginData'); 

					//if( ($loginData) && $loginData->user_type == '1' ){
					$this->facility_district = "AND 1";
					$this->facility_district1 = "AND 1";
					$this->facility_districtsumm = "AND 1";
					//}
					
				}
				else
				{
				$this->facility_district              = "AND Session_DistrictID = ".$filters1['id_input_district'];
				$this->facility_district1              = "AND p.Session_DistrictID = ".$filters1['id_input_district'];
				$this->date_filter1      = " and p.CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

				}

		
				//$this->HAVRapidDate      = " HAVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
				$this->HAVRapidDate      = "HAVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."' || HAVElisaDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'" || "HCVOtherDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'" ;

					$this->HBSRapidDate      = " HBSRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
					$this->HCVRapidDate      = " HCVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
					$this->HEVRapidDate      = " HEVRapidDate between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

			$this->date_filter      = " CreatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
			$this->T_Initiation     = " T_Initiation between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
			
			$this->date_filtersumm      = " date between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";


			$this->data_ETRTestDate = " ETR_HCVViralLoad_Dt between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

			$this->Date_SVR_TestDate = " DATE_ADD(ETR_HCVViralLoad_Dt,INTERVAL 84 DAY) between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

			$this->SVR12W_HCVViralLoad_Dt = " SVR12W_HCVViralLoad_Dt between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

			$this->data_UpdatedOn = " UpdatedOn between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

			$this->T_DLL_01_BVLC_Date =" T_DLL_01_BVLC_Date between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
			$this->ETR_HCVVIRALLOAD_DT = "T_DLL_01_BVLC_Date between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";

			$this->T_DLL_01_VLC_Date = "T_DLL_01_VLC_Date between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";
			
			$this->data_visitUpdatedOn = " v.Visit_Dt between '".($filters1['startdate'])."' and '".($filters1['enddate'])."'";



		}

		parent::__construct();
	}

	public function get_data_1_1()
	{
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		   $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->date_filter." and gender = 1 and age>= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->date_filter." and gender = 2 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();
		

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->date_filter." and gender = 3  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->date_filter." and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();

		return $result;
	}

	public function get_data_2_1()
	{	
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		  $sql                = "SELECT case when  count(patientguid) = 0 then '-' else  count(patientguid) end as count FROM `tblpatient` where   ".$this->T_Initiation."  and gender = 1 and age >= 18 and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and  T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->T_Initiation."  and gender = 2 and age >= 18 and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->T_Initiation."  and gender = 3  and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$this->T_Initiation." and age < 18 and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_2_2()
	{	
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$this->date_filter." and gender = 1 and age >= 18 AND TransferUID >0 AND TransferFromFacility >0 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where     ".$this->date_filter." and gender = 2 and age >= 18 AND TransferUID >0 AND TransferFromFacility >0 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where     ".$this->date_filter." and gender = 3  AND TransferUID >0 AND TransferFromFacility >0 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$this->date_filter." AND age < 18 AND TransferUID >0 AND TransferFromFacility >0 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_2_3()
	{	
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$this->T_Initiation."  and gender = 1 and age >= 18 and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$this->T_Initiation."  and gender = 2 and age >= 18 and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$this->T_Initiation."  and gender = 3 and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$this->T_Initiation." and  age < 18 and Session_StateID is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_3_1()
	{	
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where    ".$this->data_ETRTestDate." and gender = 1 and age >= 18 and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00'  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where   ".$this->data_ETRTestDate."  and gender = 2 and age >= 18  and ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00'  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();
		

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where   ".$this->data_ETRTestDate."  and gender = 3 and  ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00'  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where   ".$this->data_ETRTestDate."  and age < 18 and  ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_3_2()
	{	
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and TransferRequestAccepted >0 and gender = 1 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and TransferRequestAccepted >0 and gender = 2 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and TransferRequestAccepted >0 and gender = 3  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and TransferRequestAccepted >0 and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) ";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_3_3()
	{	
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and InterruptReason = 8 and gender = 1 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and InterruptReason = 8 and gender = 2 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and InterruptReason = 8 and gender = 3  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and InterruptReason = 8 and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) ";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_3_4()
	{	
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and SVR_TreatmentStatus = 5 and gender = 1 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and SVR_TreatmentStatus = 5 and gender = 2 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and SVR_TreatmentStatus = 5 and gender = 3  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and SVR_TreatmentStatus = 5 and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_3_5()
	{	
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."' AND p.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND p.Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."'";
			}

		$sql                = "SELECT  case when  count(DISTINCT p.PatientGUID) =0 then '-' else  count(DISTINCT p.PatientGUID) end as count  FROM `tblpatient` p inner join tblpatientvisit v on p.patientguid=v.patientguid where  PillsLeft >0 and ".$this->data_visitUpdatedOn." and gender = 1 and age >= 18 ".$this->facility_state1 ." ".$this->facility_district1." ".$this->facility_filter1." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT  case when  count(DISTINCT p.PatientGUID) =0 then '-' else  count(DISTINCT p.PatientGUID) end as count FROM `tblpatient` p inner join tblpatientvisit v on p.patientguid=v.patientguid  where  ".$this->data_visitUpdatedOn." and gender = 2 and age >= 18 ".$this->facility_state1 ." ".$this->facility_district1." ".$this->facility_filter1." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT  case when  count(DISTINCT p.PatientGUID) =0 then '-' else  count(DISTINCT p.PatientGUID) end as count FROM `tblpatient` p inner join tblpatientvisit v on p.patientguid=v.patientguid  where  ".$this->data_visitUpdatedOn." and gender = 3 ".$this->facility_state1 ." ".$this->facility_district1." ".$this->facility_filter1." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT  case when  count(DISTINCT p.PatientGUID) =0 then '-' else  count(DISTINCT p.PatientGUID) end as count FROM `tblpatient` p inner join tblpatientvisit v on p.patientguid=v.patientguid  where  ".$this->data_visitUpdatedOn." and age < 18 ".$this->facility_state1 ." ".$this->facility_district1." ".$this->facility_filter1." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_3_6()
	{	
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn."  and gender = 1 and age >= 18 and IsReferal=1 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn."  and gender = 2 and age >= 18 and IsReferal=1 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn."  and gender = 3  and IsReferal=1 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." ";
		$result['transgender']   = $this->db->query($sql)->result();

		
		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn."  and age < 18 and IsReferal=1 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_3_7()
	{	
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn."  and gender = 1 and SVR_TreatmentStatus = 5 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and SVR_TreatmentStatus =5 and gender = 2 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and SVR_TreatmentStatus =5 and gender = 3  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->data_UpdatedOn." and SVR_TreatmentStatus =5 and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_4_1()
	{	
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		 $sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->Date_SVR_TestDate." and status = 13 and gender = 1 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->Date_SVR_TestDate." and gender = 2 and status = 13 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->Date_SVR_TestDate." and gender = 3 and status = 13  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->Date_SVR_TestDate." and status = 13 and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_4_2()
	{	

		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->SVR12W_HCVViralLoad_Dt." and gender = 1 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->SVR12W_HCVViralLoad_Dt." and gender = 2 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->SVR12W_HCVViralLoad_Dt." and gender = 3  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();

		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->SVR12W_HCVViralLoad_Dt." and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

	public function get_data_4_3()
	{	
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}
			
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->SVR12W_HCVViralLoad_Dt." and SVR12W_Result =2 and gender = 1 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['male']     = $this->db->query($sql)->result();
		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->SVR12W_HCVViralLoad_Dt." and SVR12W_Result =2 and gender = 2 and age >= 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['female']   = $this->db->query($sql)->result();

		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->SVR12W_HCVViralLoad_Dt." and SVR12W_Result =2 and gender = 3  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
		$result['transgender']   = $this->db->query($sql)->result();

		
		$sql                = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->SVR12W_HCVViralLoad_Dt." and SVR12W_Result =2 and age < 18 ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) ";
		$result['children'] = $this->db->query($sql)->result();
		
		return $result;
	}

		public function hepatitis_c_a(){

				  //$sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.T_DLL_01_VLC_Date is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and ".$this->T_DLL_01_VLC_Date." and p.Session_StateID is not null ";

			$sql="SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` where  ".$this->date_filter."  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";

			//$sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where   (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and ".$this->HEPCRapidDate." and p.Session_StateID is not null ";
				$result = $this->db->query($sql)->result();
				return $result;

					
		}

		public function hepatitis_c_b(){

					 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.T_Initiation is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and ".$this->T_Initiation."  and p.Session_StateID is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
					$result = $this->db->query($sql)->result();
					return $result;
					
		}


		public function hep_b(){

					 $sql = "SELECT COUNT( * ) as count FROM `tblpatient` WHERE `T_DLL_01_BVLC_Date` IS NOT NULL and ".$this->T_DLL_01_BVLC_Date."  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and Session_StateID is not null";
					$result = $this->db->query($sql)->result();
					return $result;
		}

			public function hep_c(){

					  $sql = "SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count FROM `tblpatient` WHERE `ETR_HCVVIRALLOAD_DT` IS NOT NULL and ".$this->data_ETRTestDate."  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and Session_StateID is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)";
					$result = $this->db->query($sql)->result();
					return $result;
		}
		public function reason_flu(){

			  $sql = "select count(LFUReason) as count ,m.LookupValue  from mstlookup m left join tblpatient p on m.LookupCode=p.LFUReason where m.flag = 49 and m.LanguageID = 1 ".$this->date_filter1."  ".$this->facility_state1 ." ".$this->facility_district1." ".$this->facility_filter1." group by m.LookupCode";
					$result = $this->db->query($sql)->result();
					return $result;

		}

		public function reasons_for_death(){

			  $sql = "select count(DeathReason) as count ,m.LookupValue  from mstlookup m left join tblpatient p on m.LookupCode=p.DeathReason where m.flag = 48 and m.LanguageID = 1 ".$this->date_filter1." ".$this->facility_state1 ." ".$this->facility_district1." ".$this->facility_filter1." group by m.LookupCode";
					$result = $this->db->query($sql)->result();
					return $result;

		}

		public function hepatitis_a(){

			  $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where (p.HAVRapidDate is not null AND p.HAVRapidDate!='0000-00-00') || (p.HCVElisaDate is not null and p.HCVElisaDate !='0000-00-00' ) || (p.HCVOtherDate is not NULL and p.HCVOtherDate  !='0000-00-00' )  AND ".$this->HAVRapidDate." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
		}
	public function hepatitis_b(){
		 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.HBSRapidDate is not null and p.HBSRapidDate!='0000-00-00' AND ".$this->HBSRapidDate." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
	}	

	public function hepatitis_c(){
			 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.HCVRapidDate is not null and p.HCVRapidDate!='0000-00-00' AND ".$this->HCVRapidDate." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
	}
	public function hepatitis_d(){

			$sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.HEVRapidDate is not null and p.HEVRapidDate!='0000-00-00' AND ".$this->HEVRapidDate." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
	}

	/*positive*/
	public function hepatitisp_a(){

			 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.HAVRapidDate is not null and p.HAVRapidDate!='0000-00-00' and HAVRapidResult=1 AND ".$this->date_filter." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
		}
	public function hepatitisp_b(){
		 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.HBSRapidDate is not null and p.HBSRapidDate!='0000-00-00' and HBSRapidResult=1 AND ".$this->date_filter." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
	}	

	public function hepatitisp_c(){
			 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.HCVRapidDate is not null and p.HCVRapidDate!='0000-00-00' AND HCVRapidResult=1 AND ".$this->date_filter." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
	}
	public function hepatitisp_d(){

			$sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.HEVRapidDate is not null and p.HEVRapidDate!='0000-00-00' AND HEVRapidResult=1 AND ".$this->date_filter." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
	}

	public function hcv_vl_detected(){

		 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.T_DLL_01_VLC_Date is not null and p.T_DLL_01_VLC_Date!='0000-00-00' AND T_DLL_01_VLC_Result=1 AND ".$this->date_filter." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
	}
//T_DLL_01_BVLC_Date
public function hep_b_confirmatory(){

		 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.T_DLL_01_BVLC_Date is not null and p.T_DLL_01_BVLC_Date!='0000-00-00'  AND ".$this->date_filter." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
}
//T_Initiation
public function firstst_dispensation(){

		 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.T_Initiation is not null and p.T_Initiation!='0000-00-00'  AND ".$this->date_filter." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
}

//ETR_HCVViralLoad_Dt

public function etr_during(){

	 $sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.ETR_HCVViralLoad_Dt is not null and p.ETR_HCVViralLoad_Dt!='0000-00-00'  AND ".$this->date_filter." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;

}

public function svr_not_detected(){

	$sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.SVR12W_HCVViralLoad_Dt is not null and p.SVR12W_HCVViralLoad_Dt!='0000-00-00' AND SVR12W_Result=2  AND ".$this->date_filter." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;

}
public function hbv_detected(){

	$sql = "select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count from tblpatient p where p.T_DLL_01_BVLC_Date is not null and p.T_DLL_01_BVLC_Date!='0000-00-00' AND T_DLL_01_BVLC_Result=1  AND ".$this->date_filter." ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."";
				$result = $this->db->query($sql)->result();
				return $result;
}


public function nationaldata_statewise(){

$loginData = $this->session->userdata('loginData');
		//echo "<pre>";print_r($loginData);

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_wherep = " 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' ";
			}

$query=" SELECT  s.StateName,t1.count,t2.count1,t3.count2    
FROM 
mststate s  left join 
(
SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as COUNT,Session_StateID FROM `tblpatient` p where  ".$this->date_filter."  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
GROUP BY p.Session_StateID) t1  on s.id_mststate=t1.Session_StateID 
left join 
(select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count1,Session_StateID from tblpatient p1 where p1.T_Initiation is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and ".$this->T_Initiation." and p1.Session_StateID is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) group by p1.Session_StateID) t2 on 
s.id_mststate =t2.Session_StateID
left join 
(SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count2,Session_StateID FROM tblpatient p2 WHERE `ETR_HCVVIRALLOAD_DT` IS NOT NULL  and ".$this->data_ETRTestDate."  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and p2.Session_StateID is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) group by p2.Session_StateID )
  t3 on s.id_mststate=t3.Session_StateID order by s.StateName ASC";

 /*$query=" SELECT  s.StateName,t1.count,t2.count1,t3.count2    
FROM 
mststate s  left join 
(
SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end AS COUNT,Session_StateID
FROM tblpatient p
WHERE p.T_DLL_01_VLC_Date IS NOT NULL  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and ".$this->T_DLL_01_VLC_Date." and p.Session_StateID is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)
GROUP BY p.Session_StateID) t1  on s.id_mststate=t1.Session_StateID 
left join 
(select case when  count(patientguid) =0 then '-' else  count(patientguid) end as count1,Session_StateID from tblpatient p1 where p1.T_Initiation is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter."  and ".$this->T_Initiation." and p1.Session_StateID is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) group by p1.Session_StateID) t2 on 
s.id_mststate =t2.Session_StateID
left join 
(SELECT case when  count(patientguid) =0 then '-' else  count(patientguid) end as count2,Session_StateID FROM tblpatient p2 WHERE `ETR_HCVVIRALLOAD_DT` IS NOT NULL  and ".$this->data_ETRTestDate."  ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and p2.Session_StateID is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) group by p2.Session_StateID )
  t3 on s.id_mststate=t3.Session_StateID order by s.StateName ASC";*/

	  /*$query = "SELECT
    f.StateName AS StateName,
   ifnull(SUM(tblsummary_new.serological_tests), 0) AS anti_hcv_screened,
   ifnull(SUM(tblsummary_new.initiatied_on_treatment), 0) AS initiatied_on_treatment,
   ifnull(SUM(tblsummary_new.treatment_completed), 0) AS treatment_completed
   
FROM
(
    SELECT
       *
   FROM mststate order by StateName asc
   
) f
LEFT JOIN
   `tblsummary_new`
ON
   tblsummary_new.Session_StateID = f.id_mststate
WHERE
   (
      ".$this->date_filtersumm." and ".$sess_where." ".$this->facility_state ." ".$this->facility_district." ".$this->id_mstfacility."
   )
GROUP BY
   tblsummary_new.Session_StateID order by f.StateName";*/


 $result = $this->db->query($query)->result();


if(count($result) == 1)
{
return $result;
}
else
{
return $result;
}

}



}	