<?php 
class Inventory_Report_Model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
		// $this->file_path = realpath(APPPATH . '../datafiles');
		// $this->banner_path = realpath(APPPATH . '../banners');
		// $this->gallery_path_url = base_url().'datafiles/';
    }
   

public function get_lab_stock_report($type=NULL,$id_mstfacility=NULL,$id_mst_drugs=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
	//pr($invrepo);exit();
		if ($invrepo['item_type']=='') {
				$itemname= 'AND 1 ';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else{
				$itemname= "AND id_mst_drugs = ".$invrepo['item_type']."";
			}
		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
				$hospital="";
				$qry="";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}
			if($id_mstfacility=='' && ($loginData->user_type=='1' || $loginData->user_type=='3' || $loginData->user_type=='2')){
				$sess_wherep = "AND 1";
			}
			else{
				$sess_wherep="AND id_mstfacility='".$id_mstfacility."' and id_mst_drugs=".$id_mst_drugs."";
			}
				
			
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
		   $query = "SELECT DISTINCT i1.id_mstfacility,n.drug_name as NAME,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,i1.batch_num,i2.Expire_stock,i2.quantity_received,i2.warehouse_quantity_received,i2.uslp_quantity_received,i2.tp_quantity_received,i2.quantity_rejected,i2.warehouse_quantity_rejected,i2.uslp_quantity_rejected,i2.tp_quantity_rejected,(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0)) as total_receipt,(ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)) as total_rejected,i3.dispensed_quantity,i3.control_used,(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)) as total_utilize,i4.auto_relocated_quantity,i5.manual_relocated_quantity,(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)) as total_transfer_out,i6.returned_quantity FROM 
(SELECT id_mstfacility,id_mst_drugs,inventory_id,transfer_to,batch_num,type,drug_name FROM tbl_inventory where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname.") AS i1
LEFT JOIN 
(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.warehouse_quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and(i.Acceptance_Status='1' OR i.Acceptance_Status='3') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
ON 
i1.inventory_id=i2.inventory_id
Left JOIN
(SELECT sum(i.dispensed_quantity) AS dispensed_quantity,sum(i.control_used) AS control_used,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
on
i1.id_mst_drugs=i3.id_mst_drugs AND i1.batch_num=i3.batch_num
Left JOIN
(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
on
i1.id_mst_drugs=i4.id_mst_drugs  AND i1.batch_num=i4.batch_num AND i1.id_mstfacility=i4.transfer_to
Left JOIN

(SELECT id_mstfacility,sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
on
i1.id_mst_drugs=i5.id_mst_drugs AND i1.batch_num=i5.batch_num AND i1.inventory_id=i4.inventory_id
Left JOIN
(SELECT sum(i.returned_quantity) AS returned_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i6
on
i1.id_mst_drugs=i6.id_mst_drugs AND i1.batch_num=i6.batch_num
inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs
WHERE i2.quantity_received IS not NULL order by i1.id_mst_drugs";

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}	
function get_facility_inventory_Report($type=NULL,$id_mststate=NULL,$id_mst_drugs=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
		if ($invrepo['item_type']=='') {
				$itemname= '';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else{
				$itemname= "AND id_mst_drugs = ".$invrepo['item_type']."";
			}

		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on Q.id_mststate=st.id_mststate";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";

			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}
			if($id_mststate=='' && ($loginData->user_type=='1' || $loginData->user_type=='3')){
				$sess_wherep = "AND 1";
			}
			else{
				$sess_wherep="AND id_mststate='".$id_mststate."' and id_mst_drugs=".$id_mst_drugs."";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}

	$query="SELECT q.id_mststate,q.hospital,q.id_mstfacility,q.drug_name as NAME,q.drug_name,q.id_mst_drugs,q.type,q.strength,
				sum(q.quantity_received) AS quantity_received,
				SUM(case when q.Expired_stock<1 then 0 ELSE q.Expired_stock END) AS Expired_stock,
				sum(q.warehouse_quantity_received) AS warehouse_quantity_received,
				sum(q.uslp_quantity_received) AS uslp_quantity_received,
				sum(q.tp_quantity_received) AS tp_quantity_received,
				sum(q.quantity_rejected) AS quantity_rejected,
				sum(q.warehouse_quantity_rejected) AS warehouse_quantity_rejected,
				sum(q.uslp_quantity_rejected) AS uslp_quantity_rejected,
				sum(q.tp_quantity_rejected) AS tp_quantity_rejected,
				sum(IFNULL(q.total_receipt,0)) as total_receipt,
				sum(IFNULL(q.total_rejected,0)) as total_rejected,
				sum(q.dispensed_quantity) AS dispensed_quantity,
				sum(q.control_used) AS control_used,
				sum(IFNULL(q.total_utilize,0)) as total_utilize,
				sum(q.auto_relocated_quantity) AS auto_relocated_quantity,
				sum(q.manual_relocated_quantity) AS manual_relocated_quantity,
				sum(IFNULL(q.total_transfer_out,0)) as total_transfer_out,
				sum(q.returned_quantity) AS returned_quantity
				FROM (
				SELECT DISTINCT i1.id_mststate,i1.id_mstfacility,concat(f.City, '-', f.FacilityType) AS hospital,n.drug_name as NAME,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,i1.batch_num,
				(i2.Expire_stock-((ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0))+(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0))+(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0))+IFNULL(i6.returned_quantity,0))) as Expired_stock,
				i2.quantity_received,
				i2.warehouse_quantity_received,
				i2.uslp_quantity_received,
				i2.tp_quantity_received,
				i2.quantity_rejected,
				i2.warehouse_quantity_rejected,
				i2.uslp_quantity_rejected,
				i2.tp_quantity_rejected,
				(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0)) as total_receipt,
				(ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)) as total_rejected,
				i3.dispensed_quantity,
				i3.control_used,
				(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)) as total_utilize,
				i4.auto_relocated_quantity,
				i5.manual_relocated_quantity,
				(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)) as total_transfer_out,
				i6.returned_quantity 
				FROM 
				(SELECT id_mststate,id_mstfacility,id_mst_drugs,inventory_id,transfer_to,batch_num,type,drug_name FROM tbl_inventory where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname."  ) AS i1
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.warehouse_quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and(i.Acceptance_Status='1' OR i.Acceptance_Status='3') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
				ON 
				i1.inventory_id=i2.inventory_id
				Left JOIN
				(SELECT sum(i.dispensed_quantity) AS dispensed_quantity,sum(i.control_used) AS control_used,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname."  GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
				on
				i1.id_mst_drugs=i3.id_mst_drugs AND i1.batch_num=i3.batch_num
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' ".$type." ".$itemname."GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
				on
				i1.id_mst_drugs=i4.id_mst_drugs  AND i1.batch_num=i4.batch_num AND i1.id_mstfacility=i4.transfer_to
				Left JOIN

				(SELECT id_mstfacility,sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
				on
				i1.id_mst_drugs=i5.id_mst_drugs AND i1.batch_num=i5.batch_num AND i1.inventory_id=i5.inventory_id
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.inventory_id,id_mst_drugs,batch_num,id_mstfacility FROM tbl_inventory i WHERE i.Flag='F' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mst_drugs,id_mstfacility ORDER BY id_mst_drugs) AS i6
				on
				i1.id_mst_drugs=i6.id_mst_drugs AND i1.batch_num=i6.batch_num AND i1.id_mstfacility=i6.id_mstfacility
				inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs 
				inner join `mstfacility` f on i1.id_mstfacility=f.id_mstfacility) AS q 
				WHERE quantity_received IS NOT NULL GROUP BY q.id_mstfacility,q.id_mst_drugs order BY q.id_mst_drugs";

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}	
function get_state_inventory_Report($type=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
		if ($invrepo['item_type']=='') {
				$itemname= '';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else{
				$itemname= "AND id_mst_drugs = ".$invrepo['item_type']."";
			}

		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on Q.id_mststate=st.id_mststate";

			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";
				$hospital="concat(f.City, '-', f.FacilityType) AS hospital,";
				$qry="inner JOIN  `mstfacility` f ON i1.id_mstfacility=f.id_mstfacility";

			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}
			if($filters1['id_search_state']=='' && $loginData->user_type=='1'){
				$sess_wherep = "AND 1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on i1.id_mststate=st.id_mststate";
			}
			else{
				if($filters1['id_search_state']==NULL){
					$State_ID=$loginData->State_ID;
				}
				else{
					$State_ID=$filters1['id_search_state'];
				}
				$sess_wherep = "AND id_mststate = '".$State_ID."'";
				$hospital="concat(f.City, '-', f.FacilityType) AS hospital,";
				$qry="inner JOIN  `mstfacility` f ON i1.id_mstfacility=f.id_mstfacility";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
$query="SELECT q.id_mststate,q.StateName,q.drug_name as NAME,q.drug_name,q.id_mst_drugs,q.type,q.strength,
				sum(q.quantity_received) AS quantity_received,
				SUM(case when q.Expired_stock<1 then 0 ELSE q.Expired_stock END) AS Expired_stock,
				sum(q.warehouse_quantity_received) AS warehouse_quantity_received,
				sum(q.uslp_quantity_received) AS uslp_quantity_received,
				sum(q.tp_quantity_received) AS tp_quantity_received,
				sum(q.quantity_rejected) AS quantity_rejected,
				sum(q.warehouse_quantity_rejected) AS warehouse_quantity_rejected,
				sum(q.uslp_quantity_rejected) AS uslp_quantity_rejected,
				sum(q.tp_quantity_rejected) AS tp_quantity_rejected,
				sum(IFNULL(q.total_receipt,0)) as total_receipt,
				sum(IFNULL(q.total_rejected,0)) as total_rejected,
				sum(q.dispensed_quantity) AS dispensed_quantity,
				sum(q.control_used) AS control_used,
				sum(IFNULL(q.total_utilize,0)) as total_utilize,
				sum(q.auto_relocated_quantity) AS auto_relocated_quantity,
				sum(q.manual_relocated_quantity) AS manual_relocated_quantity,
				sum(IFNULL(q.total_transfer_out,0)) as total_transfer_out,
				sum(q.returned_quantity) AS returned_quantity
				FROM (
				SELECT DISTINCT i1.id_mststate,i1.id_mstfacility,".$hospital."n.drug_name as NAME,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,i1.batch_num,
				(i2.Expire_stock-((ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0))+(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0))+(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0))+IFNULL(i6.returned_quantity,0))) as Expired_stock,
				i2.quantity_received,
				i2.warehouse_quantity_received,
				i2.uslp_quantity_received,
				i2.tp_quantity_received,
				i2.quantity_rejected,
				i2.warehouse_quantity_rejected,
				i2.uslp_quantity_rejected,
				i2.tp_quantity_rejected,
				(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0)) as total_receipt,
				(ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)) as total_rejected,
				i3.dispensed_quantity,
				i3.control_used,
				(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)) as total_utilize,
				i4.auto_relocated_quantity,
				i5.manual_relocated_quantity,
				(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)) as total_transfer_out,
				i6.returned_quantity 
				FROM 
				(SELECT id_mststate,id_mstfacility,id_mst_drugs,inventory_id,transfer_to,batch_num,type,drug_name FROM tbl_inventory where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname."  ) AS i1
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.warehouse_quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and(i.Acceptance_Status='1' OR i.Acceptance_Status='3') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
				ON 
				i1.inventory_id=i2.inventory_id
				Left JOIN
				(SELECT sum(i.dispensed_quantity) AS dispensed_quantity,sum(i.control_used) AS control_used,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname."  GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
				on
				i1.id_mst_drugs=i3.id_mst_drugs AND i1.batch_num=i3.batch_num
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' ".$type." ".$itemname."GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
				on
				i1.id_mst_drugs=i4.id_mst_drugs  AND i1.batch_num=i4.batch_num AND i1.id_mstfacility=i4.transfer_to
				Left JOIN

				(SELECT id_mstfacility,sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
				on
				i1.id_mst_drugs=i5.id_mst_drugs AND i1.batch_num=i5.batch_num AND i1.inventory_id=i5.inventory_id
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.inventory_id,id_mst_drugs,batch_num,id_mstfacility FROM tbl_inventory i WHERE i.Flag='F' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mst_drugs,id_mstfacility ORDER BY id_mst_drugs) AS i6
				on
				i1.id_mst_drugs=i6.id_mst_drugs AND i1.batch_num=i6.batch_num AND i1.id_mstfacility=i6.id_mstfacility
				inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs ".$qry.") AS q 
				WHERE quantity_received IS NOT NULL GROUP BY q.id_mststate,q.id_mst_drugs order BY q.id_mst_drugs";

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}	
public function get_stock_rejected_data_national($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			/*$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";*/
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		        $query = "SELECT * FROM 
(SELECT id_mst_drugs,drug_name,id_mststate,id_mstfacility,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received when i.from_to_type=1 then i.warehouse_quantity_received  when (i.from_to_type=3 AND i.flag='R') then  i.quantity_received when (i.from_to_type=4 AND i.flag='R') then  i.quantity_received ELSE 0 END) as quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected when i.from_to_type=1 then i.warehouse_quantity_rejected  when (i.from_to_type=3 AND i.flag='R') then  i.quantity_rejected when (i.from_to_type=4 AND i.flag='R') then  i.quantity_rejected ELSE 0 END) as quantity_rejected,i.type FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and(i.Acceptance_Status='2'  OR i.Acceptance_Status='3') ".$sess_where." AND ".$itemname." and i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."' GROUP BY id_mst_drugs,id_mststate ORDER BY i.id_mst_drugs,i.id_mststate) AS i
LEFT JOIN
						  (SELECT id_mststate,StateName FROM mststate) AS f
						 		ON i.id_mststate=f.id_mststate
						 INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON i.id_mst_drugs=s.id_mst_drug_strength";

	///i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND 

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}	
public function get_stock_rejected_data_state($type=NULL,$id_mststate=NULL,$id_mst_drugs=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			/*$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";*/
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
				$sess_where1 = " 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_where1 = " id_mststate = '".$loginData->State_ID."' id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
				$sess_where1 = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}

			if($id_mststate=='' && ($loginData->user_type=='1' || $loginData->user_type=='3')){
				$sess_wherep = "AND 1";
			}
			else{
				$sess_wherep="AND id_mststate='".$id_mststate."' and id_mst_drugs=".$id_mst_drugs."";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}

		        $query = "SELECT * FROM 
(SELECT id_mst_drugs,id_mststate,id_mstfacility,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received when i.from_to_type=1 then i.warehouse_quantity_received  when (i.from_to_type=3 AND i.flag='R') then  i.quantity_received when (i.from_to_type=4 AND i.flag='R') then  i.quantity_received ELSE 0 END) as quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected when i.from_to_type=1 then i.warehouse_quantity_rejected  when (i.from_to_type=3 AND i.flag='R') then  i.quantity_rejected when (i.from_to_type=4 AND i.flag='R') then  i.quantity_rejected ELSE 0 END) as quantity_rejected,i.type FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and(i.Acceptance_Status='2'  OR i.Acceptance_Status='3') ".$sess_where." AND ".$itemname." and i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."' GROUP BY id_mst_drugs,id_mstfacility ORDER BY i.id_mst_drugs,i.id_mststate) AS i
LEFT JOIN
						  (SELECT id_mststate,concat(f.City, '-', f.FacilityType) AS hospital,id_mstfacility FROM mstfacility f where ".$sess_where1.") AS f
						 		ON i.id_mstfacility=f.id_mstfacility
						 INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON i.id_mst_drugs=s.id_mst_drug_strength";

	///i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND 

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}	
public function get_stock_rejected_data_facility($type=NULL,$id_mstfacility=NULL,$id_mst_drugs=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			/*$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";*/
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
				$sess_where1 = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_where1 = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
				$sess_where1 = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}

			if($id_mstfacility=='' && ($loginData->user_type=='1' || $loginData->user_type=='3' || $loginData->user_type=='2')){
				$sess_wherep = "AND 1";
			}
			else{
				$sess_wherep="AND id_mstfacility='".$id_mstfacility."' and id_mst_drugs=".$id_mst_drugs."";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}

		         $query = "SELECT s.*,i.id_mst_drugs,i.type,i.id_mststate,i.id_mstfacility,i.Entry_Date,i.from_to_type,i.batch_num, i.quantity_received, i.quantity_rejected,case when i.from_to_type=3 then 'Unregistered Source' when i.from_to_type=4 then 'Third Party' when i.from_to_type=1 then (case when i.source_name=996 then 'abc' when i.source_name=997 then 'abcd' when i.source_name=998 then 'abcde' end) else f.hospital end as received_from,flg.LookupValue as rejection_reason FROM 
(SELECT id_mst_drugs,id_mststate,id_mstfacility,Entry_Date,from_to_type,batch_num,rejection_reason,transfer_to,source_name,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received when i.from_to_type=1 then i.warehouse_quantity_received  when (i.from_to_type=3 AND i.flag='R') then  i.quantity_received when (i.from_to_type=4 AND i.flag='R') then  i.quantity_received ELSE 0 END) as quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected when i.from_to_type=1 then i.warehouse_quantity_rejected  when (i.from_to_type=3 AND i.flag='R') then  i.quantity_rejected when (i.from_to_type=4 AND i.flag='R') then  i.quantity_rejected ELSE 0 END) as quantity_rejected,i.type FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and(i.Acceptance_Status='2'  OR i.Acceptance_Status='3') ".$sess_where." AND ".$itemname." and i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."' GROUP BY batch_num,id_mst_drugs ORDER BY i.id_mst_drugs) AS i
LEFT JOIN
						  (SELECT id_mststate,concat(f.City, '-', f.FacilityType) AS hospital,id_mstfacility FROM mstfacility f where ".$sess_where1.") AS f
						 		ON i.transfer_to=f.id_mstfacility
						 		LEFT JOIN
						  (SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag='78') AS flg
						 		ON i.rejection_reason=flg.LookupCode
						 INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON i.id_mst_drugs=s.id_mst_drug_strength";

	///i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND 

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}	
public function get_stock_expiry_facility_wise($type=NULL,$id_mstfacility=NULL,$id_mst_drugs=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
	//pr($invrepo);exit();
		if ($invrepo['item_type']=='') {
				$itemname= 'AND 1 ';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else{
				$itemname= "AND id_mst_drugs = ".$invrepo['item_type']."";
			}
		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
				$hospital="";
				$qry="";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}
			if($id_mstfacility=='' && ($loginData->user_type=='1' || $loginData->user_type=='3' || $loginData->user_type=='2')){
				$sess_wherep = "AND 1";
			}
			else{
				$sess_wherep="AND id_mstfacility='".$id_mstfacility."' and id_mst_drugs=".$id_mst_drugs."";
			}
				
			
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
		   $query = "SELECT DISTINCT i1.id_mstfacility,n.drug_name as NAME,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,i1.batch_num,i2.Expire_stock,i2.Entry_Date,i2.Expiry_Date,i2.days,i2.quantity_received,i2.warehouse_quantity_received,i2.uslp_quantity_received,i2.tp_quantity_received,i2.quantity_rejected,i2.warehouse_quantity_rejected,i2.uslp_quantity_rejected,i2.tp_quantity_rejected,(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0)) as total_receipt,(ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)) as total_rejected,i3.dispensed_quantity,i3.control_used,(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)) as total_utilize,i4.auto_relocated_quantity,i5.manual_relocated_quantity,(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)) as total_transfer_out,i6.returned_quantity FROM 
(SELECT id_mstfacility,id_mst_drugs,inventory_id,transfer_to,batch_num,type,drug_name FROM tbl_inventory where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname.") AS i1
LEFT JOIN 
(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,i.Entry_Date,i.Expiry_Date,DATEDIFF(i.Expiry_Date,i.Entry_Date) as days,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.warehouse_quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and(i.Acceptance_Status='1' OR i.Acceptance_Status='3') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
ON 
i1.inventory_id=i2.inventory_id
Left JOIN
(SELECT sum(i.dispensed_quantity) AS dispensed_quantity,sum(i.control_used) AS control_used,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
on
i1.id_mst_drugs=i3.id_mst_drugs AND i1.batch_num=i3.batch_num
Left JOIN
(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
on
i1.id_mst_drugs=i4.id_mst_drugs  AND i1.batch_num=i4.batch_num AND i1.id_mstfacility=i4.transfer_to
Left JOIN

(SELECT id_mstfacility,sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
on
i1.id_mst_drugs=i5.id_mst_drugs AND i1.batch_num=i5.batch_num AND i1.inventory_id=i4.inventory_id
Left JOIN
(SELECT sum(i.returned_quantity) AS returned_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i6
on
i1.id_mst_drugs=i6.id_mst_drugs AND i1.batch_num=i6.batch_num
inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs
WHERE i2.quantity_received IS not NULL order by i1.id_mst_drugs";

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}	
function get_stock_expiry_state($type=NULL,$id_mststate=NULL,$id_mst_drugs=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
		if ($invrepo['item_type']=='') {
				$itemname= '';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else{
				$itemname= "AND id_mst_drugs = ".$invrepo['item_type']."";
			}

		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on Q.id_mststate=st.id_mststate";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";

			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}
			if($id_mststate=='' && ($loginData->user_type=='1' || $loginData->user_type=='3')){
				$sess_wherep = "AND 1";
			}
			else{
				$sess_wherep="AND id_mststate='".$id_mststate."' and id_mst_drugs=".$id_mst_drugs."";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}

	$query="SELECT q.id_mststate,q.hospital,q.id_mstfacility,q.drug_name as NAME,q.drug_name,q.id_mst_drugs,q.type,q.strength,
				sum(q.quantity_received) AS quantity_received,
				SUM(case when q.Expired_stock<1 then 0 ELSE q.Expired_stock END) AS Expired_stock,
				sum(q.warehouse_quantity_received) AS warehouse_quantity_received,
				sum(q.uslp_quantity_received) AS uslp_quantity_received,
				sum(q.tp_quantity_received) AS tp_quantity_received,
				sum(q.quantity_rejected) AS quantity_rejected,
				sum(q.warehouse_quantity_rejected) AS warehouse_quantity_rejected,
				sum(q.uslp_quantity_rejected) AS uslp_quantity_rejected,
				sum(q.tp_quantity_rejected) AS tp_quantity_rejected,
				sum(IFNULL(q.total_receipt,0)) as total_receipt,
				sum(IFNULL(q.total_rejected,0)) as total_rejected,
				sum(q.dispensed_quantity) AS dispensed_quantity,
				sum(q.control_used) AS control_used,
				sum(IFNULL(q.total_utilize,0)) as total_utilize,
				sum(q.auto_relocated_quantity) AS auto_relocated_quantity,
				sum(q.manual_relocated_quantity) AS manual_relocated_quantity,
				sum(IFNULL(q.total_transfer_out,0)) as total_transfer_out,
				sum(q.returned_quantity) AS returned_quantity
				FROM (
				SELECT DISTINCT i1.id_mststate,i1.id_mstfacility,concat(f.City, '-', f.FacilityType) AS hospital,n.drug_name as NAME,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,i1.batch_num,
				(i2.Expire_stock-((ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0))+(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0))+(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0))+IFNULL(i6.returned_quantity,0))) as Expired_stock,
				i2.quantity_received,
				i2.warehouse_quantity_received,
				i2.uslp_quantity_received,
				i2.tp_quantity_received,
				i2.quantity_rejected,
				i2.warehouse_quantity_rejected,
				i2.uslp_quantity_rejected,
				i2.tp_quantity_rejected,
				(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0)) as total_receipt,
				(ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)) as total_rejected,
				i3.dispensed_quantity,
				i3.control_used,
				(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)) as total_utilize,
				i4.auto_relocated_quantity,
				i5.manual_relocated_quantity,
				(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)) as total_transfer_out,
				i6.returned_quantity 
				FROM 
				(SELECT id_mststate,id_mstfacility,id_mst_drugs,inventory_id,transfer_to,batch_num,type,drug_name FROM tbl_inventory where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname."  ) AS i1
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.warehouse_quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and(i.Acceptance_Status='1' OR i.Acceptance_Status='3') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
				ON 
				i1.inventory_id=i2.inventory_id
				Left JOIN
				(SELECT sum(i.dispensed_quantity) AS dispensed_quantity,sum(i.control_used) AS control_used,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname."  GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
				on
				i1.id_mst_drugs=i3.id_mst_drugs AND i1.batch_num=i3.batch_num
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' ".$type." ".$itemname."GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
				on
				i1.id_mst_drugs=i4.id_mst_drugs  AND i1.batch_num=i4.batch_num AND i1.id_mstfacility=i4.transfer_to
				Left JOIN

				(SELECT id_mstfacility,sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
				on
				i1.id_mst_drugs=i5.id_mst_drugs AND i1.batch_num=i5.batch_num AND i1.inventory_id=i5.inventory_id
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.inventory_id,id_mst_drugs,batch_num,id_mstfacility FROM tbl_inventory i WHERE i.Flag='F' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mst_drugs,id_mstfacility ORDER BY id_mst_drugs) AS i6
				on
				i1.id_mst_drugs=i6.id_mst_drugs AND i1.batch_num=i6.batch_num AND i1.id_mstfacility=i6.id_mstfacility
				inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs 
				inner join `mstfacility` f on i1.id_mstfacility=f.id_mstfacility) AS q 
				WHERE quantity_received IS NOT NULL GROUP BY q.id_mstfacility,q.id_mst_drugs order BY q.id_mst_drugs";

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}	
function get_stock_expiry_national($type=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
		if ($invrepo['item_type']=='') {
				$itemname= '';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else{
				$itemname= "AND id_mst_drugs = ".$invrepo['item_type']."";
			}

		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on Q.id_mststate=st.id_mststate";

			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";
				$hospital="concat(f.City, '-', f.FacilityType) AS hospital,";
				$qry="inner JOIN  `mstfacility` f ON i1.id_mstfacility=f.id_mstfacility";

			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}
			if($filters1['id_search_state']=='' && $loginData->user_type=='1'){
				$sess_wherep = "AND 1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on i1.id_mststate=st.id_mststate";
			}
			else{
				if($filters1['id_search_state']==NULL){
					$State_ID=$loginData->State_ID;
				}
				else{
					$State_ID=$filters1['id_search_state'];
				}
				$sess_wherep = "AND id_mststate = '".$State_ID."'";
				$hospital="concat(f.City, '-', f.FacilityType) AS hospital,";
				$qry="inner JOIN  `mstfacility` f ON i1.id_mstfacility=f.id_mstfacility";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
$query="SELECT q.id_mststate,q.StateName,q.drug_name as NAME,q.drug_name,q.id_mst_drugs,q.type,q.strength,
				sum(q.quantity_received) AS quantity_received,
				SUM(case when q.Expired_stock<1 then 0 ELSE q.Expired_stock END) AS Expired_stock,
				sum(q.warehouse_quantity_received) AS warehouse_quantity_received,
				sum(q.uslp_quantity_received) AS uslp_quantity_received,
				sum(q.tp_quantity_received) AS tp_quantity_received,
				sum(q.quantity_rejected) AS quantity_rejected,
				sum(q.warehouse_quantity_rejected) AS warehouse_quantity_rejected,
				sum(q.uslp_quantity_rejected) AS uslp_quantity_rejected,
				sum(q.tp_quantity_rejected) AS tp_quantity_rejected,
				sum(IFNULL(q.total_receipt,0)) as total_receipt,
				sum(IFNULL(q.total_rejected,0)) as total_rejected,
				sum(q.dispensed_quantity) AS dispensed_quantity,
				sum(q.control_used) AS control_used,
				sum(IFNULL(q.total_utilize,0)) as total_utilize,
				sum(q.auto_relocated_quantity) AS auto_relocated_quantity,
				sum(q.manual_relocated_quantity) AS manual_relocated_quantity,
				sum(IFNULL(q.total_transfer_out,0)) as total_transfer_out,
				sum(q.returned_quantity) AS returned_quantity
				FROM (
				SELECT DISTINCT i1.id_mststate,i1.id_mstfacility,".$hospital."n.drug_name as NAME,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,
				(i2.Expire_stock-((ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0))+(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0))+(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0))+IFNULL(i6.returned_quantity,0))) as Expired_stock,
				i2.quantity_received,
				i2.warehouse_quantity_received,
				i2.uslp_quantity_received,
				i2.tp_quantity_received,
				i2.quantity_rejected,
				i2.warehouse_quantity_rejected,
				i2.uslp_quantity_rejected,
				i2.tp_quantity_rejected,
				(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0)) as total_receipt,
				(ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)) as total_rejected,
				i3.dispensed_quantity,
				i3.control_used,
				(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)) as total_utilize,
				i4.auto_relocated_quantity,
				i5.manual_relocated_quantity,
				(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)) as total_transfer_out,
				i6.returned_quantity 
				FROM 
				(SELECT id_mststate,id_mstfacility,id_mst_drugs,inventory_id,transfer_to,batch_num,type,drug_name FROM tbl_inventory where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname."  ) AS i1
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.warehouse_quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and(i.Acceptance_Status='1' OR i.Acceptance_Status='3') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
				ON 
				i1.inventory_id=i2.inventory_id
				Left JOIN
				(SELECT sum(i.dispensed_quantity) AS dispensed_quantity,sum(i.control_used) AS control_used,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname."  GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
				on
				i1.id_mst_drugs=i3.id_mst_drugs AND i1.batch_num=i3.batch_num
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' ".$type." ".$itemname."GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
				on
				i1.id_mst_drugs=i4.id_mst_drugs  AND i1.batch_num=i4.batch_num AND i1.id_mstfacility=i4.transfer_to
				Left JOIN

				(SELECT id_mstfacility,sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
				on
				i1.id_mst_drugs=i5.id_mst_drugs AND i1.batch_num=i5.batch_num AND i1.inventory_id=i5.inventory_id
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.inventory_id,id_mst_drugs,batch_num,id_mstfacility FROM tbl_inventory i WHERE i.Flag='F' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mst_drugs,id_mstfacility ORDER BY id_mst_drugs) AS i6
				on
				i1.id_mst_drugs=i6.id_mst_drugs AND i1.batch_num=i6.batch_num AND i1.id_mstfacility=i6.id_mstfacility
				inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs ".$qry.") AS q 
				WHERE quantity_received IS NOT NULL GROUP BY q.id_mststate,q.id_mst_drugs order BY q.id_mst_drugs";

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}	
}
