<?php 
class Inventory_Report_Model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
		// $this->file_path = realpath(APPPATH . '../datafiles');
		// $this->banner_path = realpath(APPPATH . '../banners');
		// $this->gallery_path_url = base_url().'datafiles/';
		$this->where_condition="(sum(IFNULL(q.total_receipt,0))>0 || sum(IFNULL(q.total_utilize_camp,0)) > 0 || sum(IFNULL(q.total_utilize,0))>0 || sum(IFNULL(q.total_transfer_out,0))>0 || sum(q.returned_quantity)>0 || SUM(case when q.Expire_stock<1 then 0 ELSE q.Expire_stock END)> 0) || sum(IFNULL(q.total_receipt1,0))-(((SUM(case when q.Expire_stock1<1 then 0 ELSE q.Expire_stock1 END))-(sum(q.returned_quantity1)+sum(IFNULL(q.total_utilize1,0))+sum(IFNULL(q.total_utilize_camp1,0))+sum(IFNULL(q.total_rejected1,0))+sum(IFNULL(q.total_transfer_out1,0))))+(sum(q.returned_quantity1)+sum(IFNULL(q.total_utilize1,0))+sum(IFNULL(q.total_utilize_camp1,0))+sum(IFNULL(q.total_rejected1,0))+sum(IFNULL(q.total_transfer_out1,0))))>0";

		$this->where_condition1="(sum(IFNULL(q.total_receipt,0))>0 || sum(IFNULL(q.total_utilize,0))>0 || sum(IFNULL(q.total_transfer_out,0))>0 || sum(q.returned_quantity)>0 || SUM(case when q.Expired_stock<1 then 0 ELSE q.Expired_stock END)> 0) || sum(IFNULL(q.total_receipt,0))-(((SUM(case when q.Expired_stock<1 then 0 ELSE q.Expired_stock END))-(sum(q.returned_quantity)+sum(IFNULL(q.total_utilize,0))+sum(IFNULL(q.total_rejected,0))+sum(IFNULL(q.total_transfer_out,0))))+(sum(q.returned_quantity)+sum(IFNULL(q.total_utilize,0))+sum(IFNULL(q.total_rejected,0))+sum(IFNULL(q.total_transfer_out,0))))>0";
    }
   

public function get_lab_stock_report($type=NULL,$id_mstfacility=NULL,$id_mst_drugs=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
	//pr($invrepo);exit();
		if ($invrepo['item_type']=='') {
				$itemname= 'AND 1 ';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else{
				$itemname= "AND id_mst_drugs = ".$invrepo['item_type']."";
			}
		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
				$sess_wherep='And 1';
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
				$hospital="";
				$qry="";
				$sess_wherep='AND 1';
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";
				$sess_wherep='And 1';
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
				$sess_wherep='AND 1';
			}
			if(($id_mstfacility=='' || $id_mstfacility==NULL) && ($loginData->user_type=='1' || $loginData->user_type=='3' || $loginData->user_type=='2')){
				$sess_wherep = "AND 1";
			}
			else{
				 $sess_wherep="AND id_mstfacility='".$id_mstfacility."' and id_mst_drugs=".$id_mst_drugs."";
			}
				
			
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
				
				$startdate1=$filters1['startdate1'];
				$startdate=$filters1['startdate'];
				$enddate=$filters1['enddate'];
			    $Entry_Date="i.Entry_Date BETWEEN '".$startdate."' And '".$enddate."'";
				$dispensed_date="(i.from_Date BETWEEN '".$startdate."' And '".$enddate."'AND i.to_Date BETWEEN '".$startdate."' And '".$enddate."')";
				$dispatch_date="i.dispatch_date BETWEEN '".$startdate."' And '".$enddate."'";
				$failure_date="i.failure_date BETWEEN '".$startdate."' And '".$enddate."'";

				$Entry_Date1="i.Entry_Date <'".$startdate."'";
				$dispensed_date1="i.to_Date <'".$startdate."'";
				$dispatch_date1="i.dispatch_date <'".$startdate."'";
				$failure_date1="i.failure_date <'".$startdate."'";

				$Entry_Date2="i.Entry_Date <'".$startdate1."'";
				$dispensed_date2="i.to_Date <'".$startdate1."'";
				$dispatch_date2="i.dispatch_date <'".$startdate1."'";
				$failure_date2="i.failure_date <'".$startdate1."'";

				$date_filter="(i.indent_date<='".$enddate."' OR i.Entry_Date<='".$enddate."' OR i.dispatch_date<='".$enddate."' OR i.to_Date<='".$enddate."')";

				


		        $query = "SELECT DISTINCT i1.id_mststate,i1.id_mstfacility,n.drug_name as NAME,n.lead_time,s.buffer_stock,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,i1.batch_num,IFNULL(i2.Expire_stock,0) as Expire_stock,
					ifnull(i2.quantity_received,0) as quantity_received,
					ifnull(i2.warehouse_quantity_received,0) as warehouse_quantity_received,
					ifnull(i2.uslp_quantity_received,0) as uslp_quantity_received,
					ifnull(i2.tp_quantity_received,0) as tp_quantity_received,
					ifnull(i2.quantity_rejected,0) as quantity_rejected,
					ifnull(i2.warehouse_quantity_rejected,0) as warehouse_quantity_rejected,
					ifnull(i2.uslp_quantity_rejected,0) as uslp_quantity_rejected,
					ifnull(i2.tp_quantity_rejected,0) as tp_quantity_rejected,
					ifnull(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0),0) as total_receipt,
					IFNULL((ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)),0) as total_rejected,
					ifnull(i3.dispensed_quantity,0) as dispensed_quantity,
					ifnull(i3.control_used,0) as control_used,
					ifnull(i3.dispensed_quantity_camp,0) as dispensed_quantity_camp,
					ifnull(i3.control_used_camp,0) as control_used_camp,
					IFNULL((ifnull(i3.dispensed_quantity_camp,0)+ifnull(i3.control_used_camp,0)),0) as total_utilize_camp,
					IFNULL((ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)),0) as total_utilize,
					ifnull(i4.auto_relocated_quantity,0) as auto_relocated_quantity,
					ifnull(i5.manual_relocated_quantity,0) as manual_relocated_quantity,
					IFNULL((ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)),0) as total_transfer_out,
					ifnull(i6.returned_quantity,0) as returned_quantity,
					IFNULL(i7.Expire_stock,0) as Expire_stock1,
					ifnull(i7.quantity_received,0) as quantity_received1,
					ifnull(i7.warehouse_quantity_received,0) as warehouse_quantity_received1,
					ifnull(i7.uslp_quantity_received,0) as uslp_quantity_received1,
					ifnull(i7.tp_quantity_received,0) as tp_quantity_received1,
					ifnull(i7.quantity_rejected,0) as quantity_rejected1,
					ifnull(i7.warehouse_quantity_rejected,0) as warehouse_quantity_rejected1,
					ifnull(i7.uslp_quantity_rejected,0) as uslp_quantity_rejected1,
					ifnull(i7.tp_quantity_rejected,0) as tp_quantity_rejected1,
					ifnull(ifnull(i7.quantity_received,0)+ifnull(i7.warehouse_quantity_received,0)+ifnull(i7.uslp_quantity_received,0)+ifnull(i7.tp_quantity_received,0),0) as total_receipt1,
					IFNULL((ifnull(i7.quantity_rejected,0)+ifnull(i7.warehouse_quantity_rejected,0)+ifnull(i7.uslp_quantity_rejected,0)+ifnull(i7.tp_quantity_rejected,0)),0) as total_rejected1,
					ifnull(i8.dispensed_quantity,0) as dispensed_quantity1,
					ifnull(i8.control_used,0) as control_used1,
					ifnull(i8.dispensed_quantity_camp,0) as dispensed_quantity_camp1,
					ifnull(i8.control_used_camp,0) as control_used_camp1,
					IFNULL((ifnull(i8.dispensed_quantity_camp,0)+ifnull(i8.control_used_camp,0)),0) as total_utilize_camp1,
					IFNULL((ifnull(i8.dispensed_quantity,0)+ifnull(i8.control_used,0)),0) as total_utilize1,
					ifnull(i9.auto_relocated_quantity,0) as auto_relocated_quantity1,
					ifnull(i10.manual_relocated_quantity,0) as manual_relocated_quantity1,
					IFNULL((ifnull(i9.auto_relocated_quantity,0)+ifnull(i10.manual_relocated_quantity,0)),0) as total_transfer_out1,
					ifnull(i11.returned_quantity,0) as returned_quantity1,
					IFNULL(i12.Expire_stock,0) as Expire_stock2,
					ifnull(i12.quantity_received,0) as quantity_received2,
					ifnull(i12.warehouse_quantity_received,0) as warehouse_quantity_received2,
					ifnull(i12.uslp_quantity_received,0) as uslp_quantity_received2,
					ifnull(i12.tp_quantity_received,0) as tp_quantity_received2,
					ifnull(i12.quantity_rejected,0) as quantity_rejected2,
					ifnull(i12.warehouse_quantity_rejected,0) as warehouse_quantity_rejected2,
					ifnull(i12.uslp_quantity_rejected,0) as uslp_quantity_rejected2,
					ifnull(i12.tp_quantity_rejected,0) as tp_quantity_rejected2,
					ifnull(ifnull(i12.quantity_received,0)+ifnull(i12.warehouse_quantity_received,0)+ifnull(i12.uslp_quantity_received,0)+ifnull(i12.tp_quantity_received,0),0) as total_receipt2,
					IFNULL((ifnull(i12.quantity_rejected,0)+ifnull(i12.warehouse_quantity_rejected,0)+ifnull(i12.uslp_quantity_rejected,0)+ifnull(i12.tp_quantity_rejected,0)),0) as total_rejected2,
					ifnull(i13.dispensed_quantity,0) as dispensed_quantity2,
					ifnull(i13.control_used,0) as control_used2,
					ifnull(i13.dispensed_quantity_camp,0) as dispensed_quantity_camp2,
					ifnull(i13.control_used_camp,0) as control_used_camp2,
					IFNULL((ifnull(i13.dispensed_quantity_camp,0)+ifnull(i13.control_used_camp,0)),0) as total_utilize_camp2,
					IFNULL((ifnull(i13.dispensed_quantity,0)+ifnull(i13.control_used,0)),0) as total_utilize2,
					ifnull(i14.auto_relocated_quantity,0) as auto_relocated_quantity2,
					ifnull(i15.manual_relocated_quantity,0) as manual_relocated_quantity2,
					IFNULL((ifnull(i14.auto_relocated_quantity,0)+ifnull(i15.manual_relocated_quantity,0)),0) as total_transfer_out2,
					ifnull(i16.returned_quantity,0) as returned_quantity2
					 FROM 
				(SELECT id_mststate,id_mstfacility,id_mst_drugs,inventory_id,transfer_to,batch_num,type,drug_name FROM tbl_inventory i where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$date_filter." AND i.is_deleted='0' and i.is_temp='0' AND ((i.Flag='I' and (i.relocation_status!=0 || i.relocation_status is not null)and relocation_status>2) || i.Flag='R' || i.Flag='U' || i.Flag='L') GROUP BY batch_num,id_mstfacility,id_mst_drugs) AS i1
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
				ON 
				i1.id_mst_drugs=i2.id_mst_drugs AND i1.batch_num=i2.batch_num and i1.id_mstfacility=i2.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
				on
				i1.id_mst_drugs=i3.id_mst_drugs AND i1.batch_num=i3.batch_num and i1.id_mstfacility=i3.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
				on
				i1.id_mst_drugs=i4.id_mst_drugs  AND i1.batch_num=i4.batch_num AND i1.id_mstfacility=i4.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
				on
				i1.id_mst_drugs=i5.id_mst_drugs AND i1.batch_num=i5.batch_num and i1.id_mstfacility=i5.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i6
				on
				i1.id_mst_drugs=i6.id_mst_drugs AND i1.batch_num=i6.batch_num and i1.id_mstfacility=i6.id_mstfacility
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i7
				ON 
				i1.id_mst_drugs=i7.id_mst_drugs AND i1.batch_num=i7.batch_num and i1.id_mstfacility=i7.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i8
				on
				i1.id_mst_drugs=i8.id_mst_drugs AND i1.batch_num=i8.batch_num and i1.id_mstfacility=i8.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i9
				on
				i1.id_mst_drugs=i9.id_mst_drugs  AND i1.batch_num=i9.batch_num AND i1.id_mstfacility=i9.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i10
				on
				i1.id_mst_drugs=i10.id_mst_drugs AND i1.batch_num=i10.batch_num and i1.id_mstfacility=i10.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i11
				on
				i1.id_mst_drugs=i11.id_mst_drugs AND i1.batch_num=i11.batch_num and i1.id_mstfacility=i11.id_mstfacility
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i12
				ON 
				i1.id_mst_drugs=i11.id_mst_drugs AND i1.batch_num=i12.batch_num and i1.id_mstfacility=i12.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i13
				on
				i1.id_mst_drugs=i13.id_mst_drugs AND i1.batch_num=i13.batch_num and i1.id_mstfacility=i13.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i14
				on
				i1.id_mst_drugs=i14.id_mst_drugs  AND i1.batch_num=i14.batch_num AND i1.id_mstfacility=i14.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i15
				on
				i1.id_mst_drugs=i15.id_mst_drugs AND i1.batch_num=i15.batch_num AND i1.id_mstfacility=i15.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i16
				on
				i1.id_mst_drugs=i16.id_mst_drugs AND i1.batch_num=i16.batch_num and i1.id_mstfacility=i16.id_mstfacility
				inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs
				where i1.batch_num is not null order by i1.id_mst_drugs";

					     
/*if ($is_last_month==1) {
	print_r($query); die();
}*/
		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}	
function get_facility_inventory_Report($type=NULL,$id_mststate=NULL,$id_mst_drugs=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
		if ($invrepo['item_type']=='') {
				$itemname= '';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else{
				$itemname= "AND id_mst_drugs = ".$invrepo['item_type']."";
			}

		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on Q.id_mststate=st.id_mststate";

			}
			elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";

			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}
			if($id_mststate=='' && ($loginData->user_type=='1' || $loginData->user_type=='3' || $loginData->user_type=='2')){
				$sess_wherep = "AND 1";
			}
			else{
				 $sess_wherep="AND id_mststate='".$id_mststate."' and id_mst_drugs=".$id_mst_drugs."";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}

			$startdate1=$filters1['startdate1'];
				$startdate=$filters1['startdate'];
				$enddate=$filters1['enddate'];
			    $Entry_Date="i.Entry_Date BETWEEN '".$startdate."' And '".$enddate."'";
				$dispensed_date="(i.from_Date BETWEEN '".$startdate."' And '".$enddate."'AND i.to_Date BETWEEN '".$startdate."' And '".$enddate."')";
				$dispatch_date="i.dispatch_date BETWEEN '".$startdate."' And '".$enddate."'";
				$failure_date="i.failure_date BETWEEN '".$startdate."' And '".$enddate."'";

				$Entry_Date1="i.Entry_Date <'".$startdate."'";
				$dispensed_date1="i.to_Date <'".$startdate."'";
				$dispatch_date1="i.dispatch_date <'".$startdate."'";
				$failure_date1="i.failure_date <'".$startdate."'";

				$Entry_Date2="i.Entry_Date <'".$startdate1."'";
				$dispensed_date2="i.to_Date <'".$startdate1."'";
				$dispatch_date2="i.dispatch_date <'".$startdate1."'";
				$failure_date2="i.failure_date <'".$startdate1."'";

				$date_filter="(i.indent_date<='".$enddate."' OR i.Entry_Date<='".$enddate."' OR i.dispatch_date<='".$enddate."' OR i.to_Date<='".$enddate."')";


	 $query="SELECT q.id_mststate,q.hospital,q.id_mstfacility,q.lead_time,q.buffer_stock,q.NAME as NAME,q.drug_name,q.id_mst_drugs,q.type,q.strength,
				sum(q.quantity_received) AS quantity_received,
				SUM(case when q.Expire_stock<1 then 0 ELSE q.Expire_stock END) AS Expire_stock,
				sum(q.warehouse_quantity_received) AS warehouse_quantity_received,
				sum(q.uslp_quantity_received) AS uslp_quantity_received,
				sum(q.tp_quantity_received) AS tp_quantity_received,
				sum(q.quantity_rejected) AS quantity_rejected,
				sum(q.warehouse_quantity_rejected) AS warehouse_quantity_rejected,
				sum(q.uslp_quantity_rejected) AS uslp_quantity_rejected,
				sum(q.tp_quantity_rejected) AS tp_quantity_rejected,
				sum(IFNULL(q.total_receipt,0)) as total_receipt,
				sum(IFNULL(q.total_rejected,0)) as total_rejected,
				sum(q.dispensed_quantity) AS dispensed_quantity,
				sum(q.control_used) AS control_used,
				sum(q.dispensed_quantity_camp) AS dispensed_quantity_camp,
				sum(q.control_used_camp) AS control_used_camp,
				sum(IFNULL(q.total_utilize_camp,0)) as total_utilize_camp,
				sum(IFNULL(q.total_utilize,0)) as total_utilize,
				sum(q.auto_relocated_quantity) AS auto_relocated_quantity,
				sum(q.manual_relocated_quantity) AS manual_relocated_quantity,
				sum(IFNULL(q.total_transfer_out,0)) as total_transfer_out,
				sum(q.returned_quantity) AS returned_quantity,
				sum(q.quantity_received1) AS quantity_received1,
				SUM(case when q.Expire_stock1<1 then 0 ELSE q.Expire_stock1 END) AS Expire_stock1,
				sum(q.warehouse_quantity_received1) AS warehouse_quantity_received1,
				sum(q.uslp_quantity_received1) AS uslp_quantity_received1,
				sum(q.tp_quantity_received1) AS tp_quantity_received1,
				sum(q.quantity_rejected1) AS quantity_rejected1,
				sum(q.warehouse_quantity_rejected1) AS warehouse_quantity_rejected1,
				sum(q.uslp_quantity_rejected1) AS uslp_quantity_rejected1,
				sum(q.tp_quantity_rejected1) AS tp_quantity_rejected1,
				sum(IFNULL(q.total_receipt1,0)) as total_receipt1,
				sum(IFNULL(q.total_rejected1,0)) as total_rejected1,
				sum(q.dispensed_quantity1) AS dispensed_quantity1,
				sum(q.control_used1) AS control_used1,
				sum(q.dispensed_quantity_camp1) AS dispensed_quantity_camp1,
				sum(q.control_used_camp1) AS control_used_camp1,
				sum(IFNULL(q.total_utilize_camp,0)) as total_utilize_camp1,
				sum(IFNULL(q.total_utilize1,0)) as total_utilize1,
				sum(q.auto_relocated_quantity1) AS auto_relocated_quantity1,
				sum(q.manual_relocated_quantity1) AS manual_relocated_quantity1,
				sum(IFNULL(q.total_transfer_out1,0)) as total_transfer_out1,
				sum(q.returned_quantity1) AS returned_quantity1,
				sum(q.quantity_received2) AS quantity_received2,
				SUM(case when q.Expire_stock2<1 then 0 ELSE q.Expire_stock2 END) AS Expire_stock2,
				sum(q.warehouse_quantity_received2) AS warehouse_quantity_received2,
				sum(q.uslp_quantity_received2) AS uslp_quantity_received2,
				sum(q.tp_quantity_received2) AS tp_quantity_received2,
				sum(q.quantity_rejected2) AS quantity_rejected2,
				sum(q.warehouse_quantity_rejected2) AS warehouse_quantity_rejected2,
				sum(q.uslp_quantity_rejected2) AS uslp_quantity_rejected2,
				sum(q.tp_quantity_rejected2) AS tp_quantity_rejected2,
				sum(IFNULL(q.total_receipt2,0)) as total_receipt2,
				sum(IFNULL(q.total_rejected2,0)) as total_rejected2,
				sum(q.dispensed_quantity2) AS dispensed_quantity2,
				sum(q.control_used2) AS control_used2,
				sum(q.dispensed_quantity_camp2) AS dispensed_quantity_camp2,
				sum(q.control_used_camp2) AS control_used_camp2,
				sum(IFNULL(q.total_utilize_camp2,0)) as total_utilize_camp2,
				sum(IFNULL(q.total_utilize2,0)) as total_utilize2,
				sum(q.auto_relocated_quantity2) AS auto_relocated_quantity2,
				sum(q.manual_relocated_quantity2) AS manual_relocated_quantity2,
				sum(IFNULL(q.total_transfer_out2,0)) as total_transfer_out2,
				sum(q.returned_quantity2) AS returned_quantity2
				FROM (
				SELECT DISTINCT i1.id_mststate,i1.id_mstfacility,concat(f.City, '-', f.FacilityType) AS hospital,n.drug_name as NAME,n.lead_time,s.buffer_stock,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,i1.batch_num,IFNULL(i2.Expire_stock,0) as Expire_stock,
					ifnull(i2.quantity_received,0) as quantity_received,
					ifnull(i2.warehouse_quantity_received,0) as warehouse_quantity_received,
					ifnull(i2.uslp_quantity_received,0) as uslp_quantity_received,
					ifnull(i2.tp_quantity_received,0) as tp_quantity_received,
					ifnull(i2.quantity_rejected,0) as quantity_rejected,
					ifnull(i2.warehouse_quantity_rejected,0) as warehouse_quantity_rejected,
					ifnull(i2.uslp_quantity_rejected,0) as uslp_quantity_rejected,
					ifnull(i2.tp_quantity_rejected,0) as tp_quantity_rejected,
					ifnull(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0),0) as total_receipt,
					IFNULL((ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)),0) as total_rejected,
					ifnull(i3.dispensed_quantity,0) as dispensed_quantity,
					ifnull(i3.control_used,0) as control_used,
					ifnull(i3.dispensed_quantity_camp,0) as dispensed_quantity_camp,
					ifnull(i3.control_used_camp,0) as control_used_camp,
					IFNULL((ifnull(i3.dispensed_quantity_camp,0)+ifnull(i3.control_used_camp,0)),0) as total_utilize_camp,
					IFNULL((ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)),0) as total_utilize,
					ifnull(i4.auto_relocated_quantity,0) as auto_relocated_quantity,
					ifnull(i5.manual_relocated_quantity,0) as manual_relocated_quantity,
					IFNULL((ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)),0) as total_transfer_out,
					ifnull(i6.returned_quantity,0) as returned_quantity,
					IFNULL(i7.Expire_stock,0) as Expire_stock1,
					ifnull(i7.quantity_received,0) as quantity_received1,
					ifnull(i7.warehouse_quantity_received,0) as warehouse_quantity_received1,
					ifnull(i7.uslp_quantity_received,0) as uslp_quantity_received1,
					ifnull(i7.tp_quantity_received,0) as tp_quantity_received1,
					ifnull(i7.quantity_rejected,0) as quantity_rejected1,
					ifnull(i7.warehouse_quantity_rejected,0) as warehouse_quantity_rejected1,
					ifnull(i7.uslp_quantity_rejected,0) as uslp_quantity_rejected1,
					ifnull(i7.tp_quantity_rejected,0) as tp_quantity_rejected1,
					ifnull(ifnull(i7.quantity_received,0)+ifnull(i7.warehouse_quantity_received,0)+ifnull(i7.uslp_quantity_received,0)+ifnull(i7.tp_quantity_received,0),0) as total_receipt1,
					IFNULL((ifnull(i7.quantity_rejected,0)+ifnull(i7.warehouse_quantity_rejected,0)+ifnull(i7.uslp_quantity_rejected,0)+ifnull(i7.tp_quantity_rejected,0)),0) as total_rejected1,
					ifnull(i8.dispensed_quantity,0) as dispensed_quantity1,
					ifnull(i8.control_used,0) as control_used1,
					ifnull(i8.dispensed_quantity_camp,0) as dispensed_quantity_camp1,
					ifnull(i8.control_used_camp,0) as control_used_camp1,
					IFNULL((ifnull(i8.dispensed_quantity_camp,0)+ifnull(i8.control_used_camp,0)),0) as total_utilize_camp1,
					IFNULL((ifnull(i8.dispensed_quantity,0)+ifnull(i8.control_used,0)),0) as total_utilize1,
					ifnull(i9.auto_relocated_quantity,0) as auto_relocated_quantity1,
					ifnull(i10.manual_relocated_quantity,0) as manual_relocated_quantity1,
					IFNULL((ifnull(i9.auto_relocated_quantity,0)+ifnull(i10.manual_relocated_quantity,0)),0) as total_transfer_out1,
					ifnull(i11.returned_quantity,0) as returned_quantity1,
					IFNULL(i12.Expire_stock,0) as Expire_stock2,
					ifnull(i12.quantity_received,0) as quantity_received2,
					ifnull(i12.warehouse_quantity_received,0) as warehouse_quantity_received2,
					ifnull(i12.uslp_quantity_received,0) as uslp_quantity_received2,
					ifnull(i12.tp_quantity_received,0) as tp_quantity_received2,
					ifnull(i12.quantity_rejected,0) as quantity_rejected2,
					ifnull(i12.warehouse_quantity_rejected,0) as warehouse_quantity_rejected2,
					ifnull(i12.uslp_quantity_rejected,0) as uslp_quantity_rejected2,
					ifnull(i12.tp_quantity_rejected,0) as tp_quantity_rejected2,
					ifnull(ifnull(i12.quantity_received,0)+ifnull(i12.warehouse_quantity_received,0)+ifnull(i12.uslp_quantity_received,0)+ifnull(i12.tp_quantity_received,0),0) as total_receipt2,
					IFNULL((ifnull(i12.quantity_rejected,0)+ifnull(i12.warehouse_quantity_rejected,0)+ifnull(i12.uslp_quantity_rejected,0)+ifnull(i12.tp_quantity_rejected,0)),0) as total_rejected2,
					ifnull(i13.dispensed_quantity,0) as dispensed_quantity2,
					ifnull(i13.control_used,0) as control_used2,
					ifnull(i13.dispensed_quantity_camp,0) as dispensed_quantity_camp2,
					ifnull(i13.control_used_camp,0) as control_used_camp2,
					IFNULL((ifnull(i13.dispensed_quantity_camp,0)+ifnull(i13.control_used_camp,0)),0) as total_utilize_camp2,
					IFNULL((ifnull(i13.dispensed_quantity,0)+ifnull(i13.control_used,0)),0) as total_utilize2,
					ifnull(i14.auto_relocated_quantity,0) as auto_relocated_quantity2,
					ifnull(i15.manual_relocated_quantity,0) as manual_relocated_quantity2,
					IFNULL((ifnull(i14.auto_relocated_quantity,0)+ifnull(i15.manual_relocated_quantity,0)),0) as total_transfer_out2,
					ifnull(i16.returned_quantity,0) as returned_quantity2
				FROM 
				(SELECT id_mststate,id_mstfacility,id_mst_drugs,inventory_id,transfer_to,batch_num,type,drug_name FROM tbl_inventory i where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname."And is_deleted='0' AND ".$date_filter." AND ((i.Flag='I' and (i.relocation_status!=0 || i.relocation_status is not null)and relocation_status>2) || i.Flag='R' || i.Flag='U' || i.Flag='L') GROUP BY batch_num,id_mstfacility,id_mst_drugs) AS i1
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
				ON 
				i1.id_mst_drugs=i2.id_mst_drugs AND i1.batch_num=i2.batch_num and i1.id_mstfacility=i2.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
				on
				i1.id_mst_drugs=i3.id_mst_drugs AND i1.batch_num=i3.batch_num and i1.id_mstfacility=i3.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
				on
				i1.id_mst_drugs=i4.id_mst_drugs  AND i1.batch_num=i4.batch_num AND i1.id_mstfacility=i4.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
				on
				i1.id_mst_drugs=i5.id_mst_drugs AND i1.batch_num=i5.batch_num and i1.id_mstfacility=i5.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i6
				on
				i1.id_mst_drugs=i6.id_mst_drugs AND i1.batch_num=i6.batch_num and i1.id_mstfacility=i6.id_mstfacility
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i7
				ON 
				i1.id_mst_drugs=i7.id_mst_drugs AND i1.batch_num=i7.batch_num and i1.id_mstfacility=i7.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i8
				on
				i1.id_mst_drugs=i8.id_mst_drugs AND i1.batch_num=i8.batch_num and i1.id_mstfacility=i8.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i9
				on
				i1.id_mst_drugs=i9.id_mst_drugs  AND i1.batch_num=i9.batch_num AND i1.id_mstfacility=i9.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i10
				on
				i1.id_mst_drugs=i10.id_mst_drugs AND i1.batch_num=i10.batch_num and i1.id_mstfacility=i10.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i11
				on
				i1.id_mst_drugs=i11.id_mst_drugs AND i1.batch_num=i11.batch_num and i1.id_mstfacility=i11.id_mstfacility
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i12
				ON 
				i1.id_mst_drugs=i11.id_mst_drugs AND i1.batch_num=i12.batch_num and i1.id_mstfacility=i12.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i13
				on
				i1.id_mst_drugs=i13.id_mst_drugs AND i1.batch_num=i13.batch_num and i1.id_mstfacility=i13.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i14
				on
				i1.id_mst_drugs=i14.id_mst_drugs  AND i1.batch_num=i14.batch_num AND i1.id_mstfacility=i14.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i15
				on
				i1.id_mst_drugs=i15.id_mst_drugs AND i1.batch_num=i15.batch_num AND i1.id_mstfacility=i15.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i16
				on
				i1.id_mst_drugs=i16.id_mst_drugs AND i1.batch_num=i16.batch_num and i1.id_mstfacility=i16.id_mstfacility
				inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs
				inner JOIN  `mstfacility` f ON i1.id_mstfacility=f.id_mstfacility
				where i1.batch_num is not null) AS q 
				GROUP BY q.id_mstfacility,q.id_mst_drugs having (".$this->where_condition." ) order BY q.id_mst_drugs";

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}	
function get_state_inventory_Report($type=NULL,$is_sum=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
		if ($invrepo['item_type']=='') {
				$itemname= '';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else{
				$itemname= "AND id_mst_drugs = ".$invrepo['item_type']."";
			}

		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on i1.id_mststate=st.id_mststate";

			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on i1.id_mststate=st.id_mststate";
					$sess_wherep = "AND id_mststate = '".$loginData->State_ID."'";

			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}
			if($filters1['id_search_state']=='' && $loginData->user_type=='1'){
				$sess_wherep = "AND 1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on i1.id_mststate=st.id_mststate";
			}
			else{
				if($filters1['id_search_state']==NULL){
					$State_ID=$loginData->State_ID;
				}
				else{
					$State_ID=$filters1['id_search_state'];
				}
				$sess_wherep = "AND id_mststate = '".$State_ID."'";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
				$groupby='q.id_mststate,';
				if ($is_sum==NULL) {
					$groupby='q.id_mststate,';
				}
				else{
					$groupby='';
				}
			$startdate1=$filters1['startdate1'];
				$startdate=$filters1['startdate'];
				$enddate=$filters1['enddate'];
			    $Entry_Date="i.Entry_Date BETWEEN '".$startdate."' And '".$enddate."'";
				$dispensed_date="(i.from_Date BETWEEN '".$startdate."' And '".$enddate."'AND i.to_Date BETWEEN '".$startdate."' And '".$enddate."')";
				$dispatch_date="i.dispatch_date BETWEEN '".$startdate."' And '".$enddate."'";
				$failure_date="i.failure_date BETWEEN '".$startdate."' And '".$enddate."'";

				$Entry_Date1="i.Entry_Date <'".$startdate."'";
				$dispensed_date1="i.to_Date <'".$startdate."'";
				$dispatch_date1="i.dispatch_date <'".$startdate."'";
				$failure_date1="i.failure_date <'".$startdate."'";

				$Entry_Date2="i.Entry_Date <'".$startdate1."'";
				$dispensed_date2="i.to_Date <'".$startdate1."'";
				$dispatch_date2="i.dispatch_date <'".$startdate1."'";
				$failure_date2="i.failure_date <'".$startdate1."'";

				$date_filter="(i.indent_date<='".$enddate."' OR i.Entry_Date<='".$enddate."' OR i.dispatch_date<='".$enddate."' OR i.to_Date<='".$enddate."')";
						
					
 $query="SELECT q.id_mststate,q.StateName,q.NAME as NAME,q.lead_time,q.buffer_stock,q.drug_name,q.id_mst_drugs,q.type,q.strength,
				sum(q.quantity_received) AS quantity_received,
				SUM(case when q.Expire_stock<1 then 0 ELSE q.Expire_stock END) AS Expire_stock,
				sum(q.warehouse_quantity_received) AS warehouse_quantity_received,
				sum(q.uslp_quantity_received) AS uslp_quantity_received,
				sum(q.tp_quantity_received) AS tp_quantity_received,
				sum(q.quantity_rejected) AS quantity_rejected,
				sum(q.warehouse_quantity_rejected) AS warehouse_quantity_rejected,
				sum(q.uslp_quantity_rejected) AS uslp_quantity_rejected,
				sum(q.tp_quantity_rejected) AS tp_quantity_rejected,
				sum(IFNULL(q.total_receipt,0)) as total_receipt,
				sum(IFNULL(q.total_rejected,0)) as total_rejected,
				sum(q.dispensed_quantity) AS dispensed_quantity,
				sum(q.control_used) AS control_used,
				sum(q.dispensed_quantity_camp) AS dispensed_quantity_camp,
				sum(q.control_used_camp) AS control_used_camp,
				sum(IFNULL(q.total_utilize_camp,0)) as total_utilize_camp,
				sum(IFNULL(q.total_utilize,0)) as total_utilize,
				sum(q.auto_relocated_quantity) AS auto_relocated_quantity,
				sum(q.manual_relocated_quantity) AS manual_relocated_quantity,
				sum(IFNULL(q.total_transfer_out,0)) as total_transfer_out,
				sum(q.returned_quantity) AS returned_quantity,
				sum(q.quantity_received1) AS quantity_received1,
				SUM(case when q.Expire_stock1<1 then 0 ELSE q.Expire_stock1 END) AS Expire_stock1,
				sum(q.warehouse_quantity_received1) AS warehouse_quantity_received1,
				sum(q.uslp_quantity_received1) AS uslp_quantity_received1,
				sum(q.tp_quantity_received1) AS tp_quantity_received1,
				sum(q.quantity_rejected1) AS quantity_rejected1,
				sum(q.warehouse_quantity_rejected1) AS warehouse_quantity_rejected1,
				sum(q.uslp_quantity_rejected1) AS uslp_quantity_rejected1,
				sum(q.tp_quantity_rejected1) AS tp_quantity_rejected1,
				sum(IFNULL(q.total_receipt1,0)) as total_receipt1,
				sum(IFNULL(q.total_rejected1,0)) as total_rejected1,
				sum(q.dispensed_quantity1) AS dispensed_quantity1,
				sum(q.control_used1) AS control_used1,
				sum(q.dispensed_quantity_camp1) AS dispensed_quantity_camp1,
				sum(q.control_used_camp1) AS control_used_camp1,
				sum(IFNULL(q.total_utilize_camp,0)) as total_utilize_camp1,
				sum(IFNULL(q.total_utilize1,0)) as total_utilize1,
				sum(q.auto_relocated_quantity1) AS auto_relocated_quantity1,
				sum(q.manual_relocated_quantity1) AS manual_relocated_quantity1,
				sum(IFNULL(q.total_transfer_out1,0)) as total_transfer_out1,
				sum(q.returned_quantity1) AS returned_quantity1,
				sum(q.quantity_received2) AS quantity_received2,
				SUM(case when q.Expire_stock2<1 then 0 ELSE q.Expire_stock2 END) AS Expire_stock2,
				sum(q.warehouse_quantity_received2) AS warehouse_quantity_received2,
				sum(q.uslp_quantity_received2) AS uslp_quantity_received2,
				sum(q.tp_quantity_received2) AS tp_quantity_received2,
				sum(q.quantity_rejected2) AS quantity_rejected2,
				sum(q.warehouse_quantity_rejected2) AS warehouse_quantity_rejected2,
				sum(q.uslp_quantity_rejected2) AS uslp_quantity_rejected2,
				sum(q.tp_quantity_rejected2) AS tp_quantity_rejected2,
				sum(IFNULL(q.total_receipt2,0)) as total_receipt2,
				sum(IFNULL(q.total_rejected2,0)) as total_rejected2,
				sum(q.dispensed_quantity2) AS dispensed_quantity2,
				sum(q.control_used2) AS control_used2,
				sum(q.dispensed_quantity_camp2) AS dispensed_quantity_camp2,
				sum(q.control_used_camp2) AS control_used_camp2,
				sum(IFNULL(q.total_utilize_camp2,0)) as total_utilize_camp2,
				sum(IFNULL(q.total_utilize2,0)) as total_utilize2,
				sum(q.auto_relocated_quantity2) AS auto_relocated_quantity2,
				sum(q.manual_relocated_quantity2) AS manual_relocated_quantity2,
				sum(IFNULL(q.total_transfer_out2,0)) as total_transfer_out2,
				sum(q.returned_quantity2) AS returned_quantity2
				FROM (
				SELECT DISTINCT i1.id_mststate,i1.id_mstfacility,st.StateName,n.drug_name as NAME,n.lead_time,s.buffer_stock,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,i1.batch_num,IFNULL(i2.Expire_stock,0) as Expire_stock,
					ifnull(i2.quantity_received,0) as quantity_received,
					ifnull(i2.warehouse_quantity_received,0) as warehouse_quantity_received,
					ifnull(i2.uslp_quantity_received,0) as uslp_quantity_received,
					ifnull(i2.tp_quantity_received,0) as tp_quantity_received,
					ifnull(i2.quantity_rejected,0) as quantity_rejected,
					ifnull(i2.warehouse_quantity_rejected,0) as warehouse_quantity_rejected,
					ifnull(i2.uslp_quantity_rejected,0) as uslp_quantity_rejected,
					ifnull(i2.tp_quantity_rejected,0) as tp_quantity_rejected,
					ifnull(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0),0) as total_receipt,
					IFNULL((ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)),0) as total_rejected,
					ifnull(i3.dispensed_quantity,0) as dispensed_quantity,
					ifnull(i3.control_used,0) as control_used,
					ifnull(i3.dispensed_quantity_camp,0) as dispensed_quantity_camp,
					ifnull(i3.control_used_camp,0) as control_used_camp,
					IFNULL((ifnull(i3.dispensed_quantity_camp,0)+ifnull(i3.control_used_camp,0)),0) as total_utilize_camp,
					IFNULL((ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)),0) as total_utilize,
					ifnull(i4.auto_relocated_quantity,0) as auto_relocated_quantity,
					ifnull(i5.manual_relocated_quantity,0) as manual_relocated_quantity,
					IFNULL((ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)),0) as total_transfer_out,
					ifnull(i6.returned_quantity,0) as returned_quantity,
					IFNULL(i7.Expire_stock,0) as Expire_stock1,
					ifnull(i7.quantity_received,0) as quantity_received1,
					ifnull(i7.warehouse_quantity_received,0) as warehouse_quantity_received1,
					ifnull(i7.uslp_quantity_received,0) as uslp_quantity_received1,
					ifnull(i7.tp_quantity_received,0) as tp_quantity_received1,
					ifnull(i7.quantity_rejected,0) as quantity_rejected1,
					ifnull(i7.warehouse_quantity_rejected,0) as warehouse_quantity_rejected1,
					ifnull(i7.uslp_quantity_rejected,0) as uslp_quantity_rejected1,
					ifnull(i7.tp_quantity_rejected,0) as tp_quantity_rejected1,
					ifnull(ifnull(i7.quantity_received,0)+ifnull(i7.warehouse_quantity_received,0)+ifnull(i7.uslp_quantity_received,0)+ifnull(i7.tp_quantity_received,0),0) as total_receipt1,
					IFNULL((ifnull(i7.quantity_rejected,0)+ifnull(i7.warehouse_quantity_rejected,0)+ifnull(i7.uslp_quantity_rejected,0)+ifnull(i7.tp_quantity_rejected,0)),0) as total_rejected1,
					ifnull(i8.dispensed_quantity,0) as dispensed_quantity1,
					ifnull(i8.control_used,0) as control_used1,
					ifnull(i8.dispensed_quantity_camp,0) as dispensed_quantity_camp1,
					ifnull(i8.control_used_camp,0) as control_used_camp1,
					IFNULL((ifnull(i8.dispensed_quantity_camp,0)+ifnull(i8.control_used_camp,0)),0) as total_utilize_camp1,
					IFNULL((ifnull(i8.dispensed_quantity,0)+ifnull(i8.control_used,0)),0) as total_utilize1,
					ifnull(i9.auto_relocated_quantity,0) as auto_relocated_quantity1,
					ifnull(i10.manual_relocated_quantity,0) as manual_relocated_quantity1,
					IFNULL((ifnull(i9.auto_relocated_quantity,0)+ifnull(i10.manual_relocated_quantity,0)),0) as total_transfer_out1,
					ifnull(i11.returned_quantity,0) as returned_quantity1,
					IFNULL(i12.Expire_stock,0) as Expire_stock2,
					ifnull(i12.quantity_received,0) as quantity_received2,
					ifnull(i12.warehouse_quantity_received,0) as warehouse_quantity_received2,
					ifnull(i12.uslp_quantity_received,0) as uslp_quantity_received2,
					ifnull(i12.tp_quantity_received,0) as tp_quantity_received2,
					ifnull(i12.quantity_rejected,0) as quantity_rejected2,
					ifnull(i12.warehouse_quantity_rejected,0) as warehouse_quantity_rejected2,
					ifnull(i12.uslp_quantity_rejected,0) as uslp_quantity_rejected2,
					ifnull(i12.tp_quantity_rejected,0) as tp_quantity_rejected2,
					ifnull(ifnull(i12.quantity_received,0)+ifnull(i12.warehouse_quantity_received,0)+ifnull(i12.uslp_quantity_received,0)+ifnull(i12.tp_quantity_received,0),0) as total_receipt2,
					IFNULL((ifnull(i12.quantity_rejected,0)+ifnull(i12.warehouse_quantity_rejected,0)+ifnull(i12.uslp_quantity_rejected,0)+ifnull(i12.tp_quantity_rejected,0)),0) as total_rejected2,
					ifnull(i13.dispensed_quantity,0) as dispensed_quantity2,
					ifnull(i13.control_used,0) as control_used2,
					ifnull(i13.dispensed_quantity_camp,0) as dispensed_quantity_camp2,
					ifnull(i13.control_used_camp,0) as control_used_camp2,
					IFNULL((ifnull(i13.dispensed_quantity_camp,0)+ifnull(i13.control_used_camp,0)),0) as total_utilize_camp2,
					IFNULL((ifnull(i13.dispensed_quantity,0)+ifnull(i13.control_used,0)),0) as total_utilize2,
					ifnull(i14.auto_relocated_quantity,0) as auto_relocated_quantity2,
					ifnull(i15.manual_relocated_quantity,0) as manual_relocated_quantity2,
					IFNULL((ifnull(i14.auto_relocated_quantity,0)+ifnull(i15.manual_relocated_quantity,0)),0) as total_transfer_out2,
					ifnull(i16.returned_quantity,0) as returned_quantity2
				FROM 
				(SELECT id_mststate,id_mstfacility,id_mst_drugs,inventory_id,transfer_to,batch_num,type,drug_name FROM tbl_inventory i where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$date_filter." AND i.is_deleted='0' and i.is_temp='0' AND ((i.Flag='I' and (i.relocation_status!=0 || i.relocation_status is not null)and relocation_status>2) || i.Flag='R' || i.Flag='U' || i.Flag='L') GROUP BY batch_num,id_mstfacility,id_mst_drugs) AS i1
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
				ON 
				i1.id_mst_drugs=i2.id_mst_drugs AND i1.batch_num=i2.batch_num and i1.id_mstfacility=i2.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
				on
				i1.id_mst_drugs=i3.id_mst_drugs AND i1.batch_num=i3.batch_num and i1.id_mstfacility=i3.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
				on
				i1.id_mst_drugs=i4.id_mst_drugs  AND i1.batch_num=i4.batch_num AND i1.id_mstfacility=i4.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
				on
				i1.id_mst_drugs=i5.id_mst_drugs AND i1.batch_num=i5.batch_num and i1.id_mstfacility=i5.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i6
				on
				i1.id_mst_drugs=i6.id_mst_drugs AND i1.batch_num=i6.batch_num and i1.id_mstfacility=i6.id_mstfacility
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i7
				ON 
				i1.id_mst_drugs=i7.id_mst_drugs AND i1.batch_num=i7.batch_num and i1.id_mstfacility=i7.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i8
				on
				i1.id_mst_drugs=i8.id_mst_drugs AND i1.batch_num=i8.batch_num and i1.id_mstfacility=i8.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i9
				on
				i1.id_mst_drugs=i9.id_mst_drugs  AND i1.batch_num=i9.batch_num AND i1.id_mstfacility=i9.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i10
				on
				i1.id_mst_drugs=i10.id_mst_drugs AND i1.batch_num=i10.batch_num and i1.id_mstfacility=i10.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i11
				on
				i1.id_mst_drugs=i11.id_mst_drugs AND i1.batch_num=i11.batch_num and i1.id_mstfacility=i11.id_mstfacility
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i12
				ON 
				i1.id_mst_drugs=i11.id_mst_drugs AND i1.batch_num=i12.batch_num and i1.id_mstfacility=i12.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i13
				on
				i1.id_mst_drugs=i13.id_mst_drugs AND i1.batch_num=i13.batch_num and i1.id_mstfacility=i13.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i14
				on
				i1.id_mst_drugs=i14.id_mst_drugs  AND i1.batch_num=i14.batch_num AND i1.id_mstfacility=i14.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i15
				on
				i1.id_mst_drugs=i15.id_mst_drugs AND i1.batch_num=i15.batch_num AND i1.id_mstfacility=i15.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i16
				on
				i1.id_mst_drugs=i16.id_mst_drugs AND i1.batch_num=i16.batch_num and i1.id_mstfacility=i16.id_mstfacility
				inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs
				".$qry." where i1.batch_num is not null) AS q 
				 GROUP BY ".$groupby."q.id_mst_drugs having ".$this->where_condition." order BY q.id_mst_drugs";

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}	
public function get_stock_rejected_data_national($flag=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			/*$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";*/
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
		        $query = "SELECT * FROM 
(SELECT id_mst_drugs,drug_name,id_mststate,id_mstfacility,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received when i.from_to_type=1 then i.quantity_received  when (i.from_to_type=3 AND i.flag='R') then  i.quantity_received when (i.from_to_type=4 AND i.flag='R') then  i.quantity_received ELSE 0 END) as quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected when i.from_to_type=1 then i.warehouse_quantity_rejected  when (i.from_to_type=3 AND i.flag='R') then  i.quantity_rejected when (i.from_to_type=4 AND i.flag='R') then  i.quantity_rejected ELSE 0 END) as quantity_rejected,i.type FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and(i.Acceptance_Status='2'  OR i.Acceptance_Status='3') ".$sess_where." AND ".$itemname." and i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."' GROUP BY id_mst_drugs,id_mststate ORDER BY i.id_mst_drugs,i.id_mststate) AS i
LEFT JOIN
						  (SELECT id_mststate,StateName FROM mststate) AS f
						 		ON i.id_mststate=f.id_mststate
						 INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON i.id_mst_drugs=s.id_mst_drug_strength";

	///i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND 

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}	
public function get_stock_rejected_data_state($type=NULL,$id_mststate=NULL,$id_mst_drugs=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			/*$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";*/
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
				$sess_where1 = " 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_where1 = " id_mststate = '".$loginData->State_ID."' id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
				$sess_where1 = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}

			if($id_mststate=='' && ($loginData->user_type=='1' || $loginData->user_type=='3')){
				$sess_wherep = "AND 1";
			}
			else{
				$sess_wherep="AND id_mststate='".$id_mststate."' and id_mst_drugs=".$id_mst_drugs."";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}

		        $query = "SELECT * FROM 
(SELECT id_mst_drugs,id_mststate,id_mstfacility,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received when i.from_to_type=1 then i.quantity_received  when (i.from_to_type=3 AND i.flag='R') then  i.quantity_received when (i.from_to_type=4 AND i.flag='R') then  i.quantity_received ELSE 0 END) as quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected when i.from_to_type=1 then i.warehouse_quantity_rejected  when (i.from_to_type=3 AND i.flag='R') then  i.quantity_rejected when (i.from_to_type=4 AND i.flag='R') then  i.quantity_rejected ELSE 0 END) as quantity_rejected,i.type FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and(i.Acceptance_Status='2'  OR i.Acceptance_Status='3') ".$sess_where." AND ".$itemname." and i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."' GROUP BY id_mst_drugs,id_mstfacility ORDER BY i.id_mst_drugs,i.id_mststate) AS i
LEFT JOIN
						  (SELECT id_mststate,concat(f.City, '-', f.FacilityType) AS hospital,id_mstfacility FROM mstfacility f where ".$sess_where1.") AS f
						 		ON i.id_mstfacility=f.id_mstfacility
						 INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON i.id_mst_drugs=s.id_mst_drug_strength";

	///i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND 

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}	
public function get_stock_rejected_data_facility($type=NULL,$id_mstfacility=NULL,$id_mst_drugs=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			/*$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";*/
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
				//$sess_where1 = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_where1 = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
				$sess_where1 = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}

			if($id_mstfacility=='' && ($loginData->user_type=='1' || $loginData->user_type=='3' || $loginData->user_type=='2')){
				$sess_wherep = "AND 1";
			}
			else{
				$sess_wherep="AND id_mstfacility='".$id_mstfacility."' and id_mst_drugs=".$id_mst_drugs."";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}

		         $query = "SELECT s.*,i.id_mst_drugs,i.type,i.id_mststate,i.id_mstfacility,i.Entry_Date,i.from_to_type,i.batch_num, i.quantity_received, i.quantity_rejected,case when i.from_to_type=3 then 'Unregistered Source' when i.from_to_type=4 then 'Third Party' when i.from_to_type=1 then (case when i.source_name=996 then 'abc' when i.source_name=997 then 'abcd' when i.source_name=998 then 'abcde' end) else f.hospital end as received_from,flg.LookupValue as rejection_reason FROM 
(SELECT id_mst_drugs,id_mststate,id_mstfacility,Entry_Date,from_to_type,batch_num,rejection_reason,transfer_to,source_name,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received when i.from_to_type=1 then i.quantity_received  when (i.from_to_type=3 AND i.flag='R') then  i.quantity_received when (i.from_to_type=4 AND i.flag='R') then  i.quantity_received ELSE 0 END) as quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected when i.from_to_type=1 then i.warehouse_quantity_rejected  when (i.from_to_type=3 AND i.flag='R') then  i.quantity_rejected when (i.from_to_type=4 AND i.flag='R') then  i.quantity_rejected ELSE 0 END) as quantity_rejected,i.type FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and(i.Acceptance_Status='2'  OR i.Acceptance_Status='3') ".$sess_where." AND ".$itemname." ".$sess_wherep." and i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."' GROUP BY batch_num,id_mst_drugs ORDER BY i.id_mst_drugs) AS i
LEFT JOIN
						  (SELECT id_mststate,concat(f.City, '-', f.FacilityType) AS hospital,id_mstfacility FROM mstfacility f) AS f
						 		ON i.transfer_to=f.id_mstfacility
						 		LEFT JOIN
						  (SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag='78') AS flg
						 		ON i.rejection_reason=flg.LookupCode
						 INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON i.id_mst_drugs=s.id_mst_drug_strength";

	///i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND 

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}	
public function get_stock_expiry_facility_wise($type=NULL,$id_mstfacility=NULL,$id_mst_drugs=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
	//pr($invrepo);exit();
		if ($invrepo['item_type']=='') {
				$itemname= 'AND 1 ';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drugkit'){
		 $itemname= "AND type IN (".$invrepo['item_mst_look'][0]->LookupCode.",".$invrepo['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$invrepo['item_type']."'";
			}
		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
				$hospital="";
				$qry="";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}
			if($id_mstfacility=='' && ($loginData->user_type=='1' || $loginData->user_type=='3' || $loginData->user_type=='2')){
				$sess_wherep = "AND 1";
			}
			else{
				$sess_wherep="AND id_mstfacility='".$id_mstfacility."' and id_mst_drugs=".$id_mst_drugs."";
			}
				
			
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
				if ($invrepo['expiry_filter']=='') {

				$condition='';
			}
			elseif ($invrepo['expiry_filter']==1 || $invrepo['expiry_filter']=='1') {

				$condition=' And i2.days<0';
			}
			elseif ($invrepo['expiry_filter']==2 || $invrepo['expiry_filter']=='2') {

				$condition=' And i2.days<=180';
			}
			elseif ($invrepo['expiry_filter']==3 || $invrepo['expiry_filter']=='3') {

				$condition=' And i2.days>180';
			}

		    $query = "SELECT DISTINCT i1.id_mstfacility,n.drug_name as NAME,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,i1.batch_num,i2.Expire_stock,i2.Entry_Date,i2.Expiry_Date,i2.days,i2.quantity_received,i2.warehouse_quantity_received,i2.uslp_quantity_received,i2.tp_quantity_received,i2.quantity_rejected,i2.warehouse_quantity_rejected,i2.uslp_quantity_rejected,i2.tp_quantity_rejected,(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0)) as total_receipt,(ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)) as total_rejected,i3.dispensed_quantity,i3.control_used,(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)) as total_utilize,i4.auto_relocated_quantity,i5.manual_relocated_quantity,(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)) as total_transfer_out,i6.returned_quantity FROM 
(SELECT id_mstfacility,id_mst_drugs,inventory_id,transfer_to,batch_num,type,drug_name FROM tbl_inventory where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." group by batch_num,id_mstfacility,id_mst_drugs) AS i1
LEFT JOIN 
(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,i.Entry_Date,i.Expiry_Date,DATEDIFF(i.Expiry_Date,i.Entry_Date) as days,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
ON 
i1.inventory_id=i2.inventory_id
Left JOIN
(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
on
i1.id_mst_drugs=i3.id_mst_drugs AND i1.batch_num=i3.batch_num
Left JOIN
(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
on
i1.id_mst_drugs=i4.id_mst_drugs  AND i1.batch_num=i4.batch_num AND i1.id_mstfacility=i4.transfer_to
Left JOIN

(SELECT id_mstfacility,sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
on
i1.id_mst_drugs=i5.id_mst_drugs AND i1.batch_num=i5.batch_num AND i1.inventory_id=i4.inventory_id
Left JOIN
(SELECT sum(i.returned_quantity) AS returned_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i6
on
i1.id_mst_drugs=i6.id_mst_drugs AND i1.batch_num=i6.batch_num
inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs
where i2.batch_num is not null ".$condition." order by i1.id_mst_drugs";

					     //print_r($query); die();

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}	
function get_stock_expiry_state($type=NULL,$id_mststate=NULL,$id_mst_drugs=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
		if ($invrepo['item_type']=='') {
				$itemname= '';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drugkit'){
		 $itemname= "AND type IN (".$invrepo['item_mst_look'][0]->LookupCode.",".$invrepo['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$invrepo['item_type']."'";
			}

		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on Q.id_mststate=st.id_mststate";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";

			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}
			if($id_mststate=='' && ($loginData->user_type=='1' || $loginData->user_type=='3')){
				$sess_wherep = "AND 1";
			}
			else{
				$sess_wherep="AND id_mststate='".$id_mststate."' and id_mst_drugs=".$id_mst_drugs."";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
			if ($invrepo['expiry_filter']=='') {

				$condition='';
			}
			elseif ($invrepo['expiry_filter']==1 || $invrepo['expiry_filter']=='1') {

				$condition=' And i2.days<0';
			}
			elseif ($invrepo['expiry_filter']==2 || $invrepo['expiry_filter']=='2') {

				$condition=' And i2.days<=180';
			}
			elseif ($invrepo['expiry_filter']==3 || $invrepo['expiry_filter']=='3') {

				$condition=' And i2.days>180';
			}

	$query="SELECT q.id_mststate,q.hospital,q.id_mstfacility,q.drug_name as NAME,q.drug_name,q.id_mst_drugs,q.type,q.strength,
				sum(q.quantity_received) AS quantity_received,
				SUM(case when q.Expired_stock<1 then 0 ELSE q.Expired_stock END) AS Expired_stock,
				sum(q.warehouse_quantity_received) AS warehouse_quantity_received,
				sum(q.uslp_quantity_received) AS uslp_quantity_received,
				sum(q.tp_quantity_received) AS tp_quantity_received,
				sum(q.quantity_rejected) AS quantity_rejected,
				sum(q.warehouse_quantity_rejected) AS warehouse_quantity_rejected,
				sum(q.uslp_quantity_rejected) AS uslp_quantity_rejected,
				sum(q.tp_quantity_rejected) AS tp_quantity_rejected,
				sum(IFNULL(q.total_receipt,0)) as total_receipt,
				sum(IFNULL(q.total_rejected,0)) as total_rejected,
				sum(q.dispensed_quantity) AS dispensed_quantity,
				sum(q.control_used) AS control_used,
				sum(IFNULL(q.total_utilize,0)) as total_utilize,
				sum(q.auto_relocated_quantity) AS auto_relocated_quantity,
				sum(q.manual_relocated_quantity) AS manual_relocated_quantity,
				sum(IFNULL(q.total_transfer_out,0)) as total_transfer_out,
				sum(q.returned_quantity) AS returned_quantity
				FROM (
				SELECT DISTINCT i1.id_mststate,i1.id_mstfacility,concat(f.City, '-', f.FacilityType) AS hospital,n.drug_name as NAME,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,i1.batch_num,
				(i2.Expire_stock-((ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0))+(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0))+(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0))+IFNULL(i6.returned_quantity,0))) as Expired_stock,
				i2.quantity_received,
				i2.warehouse_quantity_received,
				i2.uslp_quantity_received,
				i2.tp_quantity_received,
				i2.quantity_rejected,
				i2.warehouse_quantity_rejected,
				i2.uslp_quantity_rejected,
				i2.tp_quantity_rejected,
				(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0)) as total_receipt,
				(ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)) as total_rejected,
				i3.dispensed_quantity,
				i3.control_used,
				(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)) as total_utilize,
				i4.auto_relocated_quantity,
				i5.manual_relocated_quantity,
				(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)) as total_transfer_out,
				i6.returned_quantity 
				FROM 
				(SELECT id_mststate,id_mstfacility,id_mst_drugs,inventory_id,transfer_to,batch_num,type,drug_name FROM tbl_inventory where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." group by batch_num,id_mstfacility,id_mst_drugs ) AS i1
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,DATEDIFF(i.Expiry_Date,i.Entry_Date) as days,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
				ON 
				i1.inventory_id=i2.inventory_id
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname."  GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
				on
				i1.id_mst_drugs=i3.id_mst_drugs AND i1.batch_num=i3.batch_num
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' ".$type." ".$itemname."GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
				on
				i1.id_mst_drugs=i4.id_mst_drugs  AND i1.batch_num=i4.batch_num AND i1.id_mstfacility=i4.transfer_to
				Left JOIN

				(SELECT id_mstfacility,sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
				on
				i1.id_mst_drugs=i5.id_mst_drugs AND i1.batch_num=i5.batch_num AND i1.inventory_id=i5.inventory_id
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.inventory_id,id_mst_drugs,batch_num,id_mstfacility FROM tbl_inventory i WHERE i.Flag='F' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mst_drugs,id_mstfacility ORDER BY id_mst_drugs) AS i6
				on
				i1.id_mst_drugs=i6.id_mst_drugs AND i1.batch_num=i6.batch_num AND i1.id_mstfacility=i6.id_mstfacility
				inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs 
				inner join `mstfacility` f on i1.id_mstfacility=f.id_mstfacility where i2.batch_num is not null".$condition.") AS q 
				 GROUP BY q.id_mstfacility,q.id_mst_drugs having (".$this->where_condition1.") order BY q.id_mst_drugs";

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}	
function get_stock_expiry_national($type=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
		if ($invrepo['item_type']=='') {
				$itemname= '';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drugkit'){
		 $itemname= "AND type IN (".$invrepo['item_mst_look'][0]->LookupCode.",".$invrepo['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$invrepo['item_type']."'";
			}

		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on Q.id_mststate=st.id_mststate";

			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";
				$hospital="concat(f.City, '-', f.FacilityType) AS hospital,";
				$qry="inner JOIN  `mstfacility` f ON i1.id_mstfacility=f.id_mstfacility";

			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}
			if($filters1['id_search_state']=='' && $loginData->user_type=='1'){
				$sess_wherep = "AND 1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on i1.id_mststate=st.id_mststate";
			}
			else{
				if($filters1['id_search_state']==NULL){
					$State_ID=$loginData->State_ID;
				}
				else{
					$State_ID=$filters1['id_search_state'];
				}
				$sess_wherep = "AND id_mststate = '".$State_ID."'";
				$hospital="concat(f.City, '-', f.FacilityType) AS hospital,";
				$qry="inner JOIN  `mstfacility` f ON i1.id_mstfacility=f.id_mstfacility";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
				if ($invrepo['expiry_filter']=='') {

				$condition='';
			}
			elseif ($invrepo['expiry_filter']==1) {

				$condition=' And i2.days<0';
			}
			elseif ($invrepo['expiry_filter']==2) {

				$condition=' And i2.days<=180';
			}
			elseif ($invrepo['expiry_filter']==3) {

				$condition=' And i2.days>180';
			}
$query="SELECT q.id_mststate,q.StateName,q.drug_name as NAME,q.drug_name,q.id_mst_drugs,q.type,q.strength,
				sum(q.quantity_received) AS quantity_received,
				SUM(case when q.Expired_stock<1 then 0 ELSE q.Expired_stock END) AS Expired_stock,
				sum(q.warehouse_quantity_received) AS warehouse_quantity_received,
				sum(q.uslp_quantity_received) AS uslp_quantity_received,
				sum(q.tp_quantity_received) AS tp_quantity_received,
				sum(q.quantity_rejected) AS quantity_rejected,
				sum(q.warehouse_quantity_rejected) AS warehouse_quantity_rejected,
				sum(q.uslp_quantity_rejected) AS uslp_quantity_rejected,
				sum(q.tp_quantity_rejected) AS tp_quantity_rejected,
				sum(IFNULL(q.total_receipt,0)) as total_receipt,
				sum(IFNULL(q.total_rejected,0)) as total_rejected,
				sum(q.dispensed_quantity) AS dispensed_quantity,
				sum(q.control_used) AS control_used,
				sum(IFNULL(q.total_utilize,0)) as total_utilize,
				sum(q.auto_relocated_quantity) AS auto_relocated_quantity,
				sum(q.manual_relocated_quantity) AS manual_relocated_quantity,
				sum(IFNULL(q.total_transfer_out,0)) as total_transfer_out,
				sum(q.returned_quantity) AS returned_quantity
				FROM (
				SELECT DISTINCT i1.id_mststate,i1.id_mstfacility,".$hospital."n.drug_name as NAME,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,
				(i2.Expire_stock-((ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0))+(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0))+(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0))+IFNULL(i6.returned_quantity,0))) as Expired_stock,
				i2.quantity_received,
				i2.warehouse_quantity_received,
				i2.uslp_quantity_received,
				i2.tp_quantity_received,
				i2.quantity_rejected,
				i2.warehouse_quantity_rejected,
				i2.uslp_quantity_rejected,
				i2.tp_quantity_rejected,
				(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0)) as total_receipt,
				(ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)) as total_rejected,
				i3.dispensed_quantity,
				i3.control_used,
				(ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)) as total_utilize,
				i4.auto_relocated_quantity,
				i5.manual_relocated_quantity,
				(ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)) as total_transfer_out,
				i6.returned_quantity 
				FROM 
				(SELECT id_mststate,id_mstfacility,id_mst_drugs,inventory_id,transfer_to,batch_num,type,drug_name FROM tbl_inventory where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname."  ) AS i1
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,DATEDIFF(i.Expiry_Date,i.Entry_Date) as days,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
				ON 
				i1.inventory_id=i2.inventory_id
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname."  GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
				on
				i1.id_mst_drugs=i3.id_mst_drugs AND i1.batch_num=i3.batch_num
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='I' ".$type." ".$itemname."GROUP BY batch_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
				on
				i1.id_mst_drugs=i4.id_mst_drugs  AND i1.batch_num=i4.batch_num AND i1.id_mstfacility=i4.transfer_to
				Left JOIN

				(SELECT id_mstfacility,sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.inventory_id,id_mst_drugs,batch_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
				on
				i1.id_mst_drugs=i5.id_mst_drugs AND i1.batch_num=i5.batch_num AND i1.inventory_id=i5.inventory_id
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.inventory_id,id_mst_drugs,batch_num,id_mstfacility FROM tbl_inventory i WHERE i.Flag='F' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." GROUP BY batch_num,id_mst_drugs,id_mstfacility ORDER BY id_mst_drugs) AS i6
				on
				i1.id_mst_drugs=i6.id_mst_drugs AND i1.batch_num=i6.batch_num AND i1.id_mstfacility=i6.id_mstfacility
				inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs ".$qry." where i2.batch_num is not null ".$condition.") AS q 
				 GROUP BY q.id_mststate,q.id_mst_drugs having (".$this->where_condition1." ) order BY q.id_mst_drugs";

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}	
public function get_lab_indent_forcast($type=NULL,$id_mstfacility=NULL,$id_mst_drugs=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
	//pr($invrepo);exit();
		if ($invrepo['item_type']=='') {
				$itemname= 'AND 1 ';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else{
				$itemname= "AND id_mst_drugs = ".$invrepo['item_type']."";
			}

		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
				$sess_wherep='And 1';
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
				$hospital="";
				$qry="";
				$sess_wherep='AND 1';
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";
				$sess_wherep='And 1';
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
				$sess_wherep='AND 1';
			}
			if(($id_mstfacility=='' || $id_mstfacility==NULL) && ($loginData->user_type=='1' || $loginData->user_type=='3' || $loginData->user_type=='2')){
				$sess_wherep = "AND 1";
			}
			else{
				 $sess_wherep="AND id_mstfacility='".$id_mstfacility."' and id_mst_drugs=".$id_mst_drugs."";
			}
				
			
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
				
				$startdate1=$filters1['startdate1'];
				$startdate=$filters1['startdate'];
				$enddate=$filters1['enddate'];
			    $Entry_Date="i.Entry_Date BETWEEN '".$startdate."' And '".$enddate."'";
				$dispensed_date="(i.from_Date BETWEEN '".$startdate."' And '".$enddate."'AND i.to_Date BETWEEN '".$startdate."' And '".$enddate."')";
				$dispatch_date="i.dispatch_date BETWEEN '".$startdate."' And '".$enddate."'";
				$failure_date="i.failure_date BETWEEN '".$startdate."' And '".$enddate."'";

				$Entry_Date1="i.Entry_Date <'".$startdate."'";
				$dispensed_date1="i.to_Date <'".$startdate."'";
				$dispatch_date1="i.dispatch_date <'".$startdate."'";
				$failure_date1="i.failure_date <'".$startdate."'";

				$Entry_Date2="i.Entry_Date <'".$startdate1."'";
				$dispensed_date2="i.to_Date <'".$startdate1."'";
				$dispatch_date2="i.dispatch_date <'".$startdate1."'";
				$failure_date2="i.failure_date <'".$startdate1."'";

				$date_filter="(i.indent_date<='".$enddate."' OR i.Entry_Date<='".$enddate."' OR i.dispatch_date<='".$enddate."' OR i.to_Date<='".$enddate."')";

				


		          $query = "SELECT DISTINCT i1.quantity,i1.indent_date,i1.indent_remark,i1.id_mststate,i1.id_mstfacility,n.drug_name as NAME,n.lead_time,s.buffer_stock,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,i1.indent_num,IFNULL(i2.Expire_stock,0) as Expire_stock,
					ifnull(i2.quantity_received,0) as quantity_received,
					ifnull(i2.warehouse_quantity_received,0) as warehouse_quantity_received,
					ifnull(i2.uslp_quantity_received,0) as uslp_quantity_received,
					ifnull(i2.tp_quantity_received,0) as tp_quantity_received,
					ifnull(i2.quantity_rejected,0) as quantity_rejected,
					ifnull(i2.warehouse_quantity_rejected,0) as warehouse_quantity_rejected,
					ifnull(i2.uslp_quantity_rejected,0) as uslp_quantity_rejected,
					ifnull(i2.tp_quantity_rejected,0) as tp_quantity_rejected,
					ifnull(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0),0) as total_receipt,
					IFNULL((ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)),0) as total_rejected,
					ifnull(i3.dispensed_quantity,0) as dispensed_quantity,
					ifnull(i3.control_used,0) as control_used,
					ifnull(i3.dispensed_quantity_camp,0) as dispensed_quantity_camp,
					ifnull(i3.control_used_camp,0) as control_used_camp,
					IFNULL((ifnull(i3.dispensed_quantity_camp,0)+ifnull(i3.control_used_camp,0)),0) as total_utilize_camp,
					IFNULL((ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)),0) as total_utilize,
					ifnull(i4.auto_relocated_quantity,0) as auto_relocated_quantity,
					ifnull(i5.manual_relocated_quantity,0) as manual_relocated_quantity,
					IFNULL((ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)),0) as total_transfer_out,
					ifnull(i6.returned_quantity,0) as returned_quantity,
					IFNULL(i7.Expire_stock,0) as Expire_stock1,
					ifnull(i7.quantity_received,0) as quantity_received1,
					ifnull(i7.warehouse_quantity_received,0) as warehouse_quantity_received1,
					ifnull(i7.uslp_quantity_received,0) as uslp_quantity_received1,
					ifnull(i7.tp_quantity_received,0) as tp_quantity_received1,
					ifnull(i7.quantity_rejected,0) as quantity_rejected1,
					ifnull(i7.warehouse_quantity_rejected,0) as warehouse_quantity_rejected1,
					ifnull(i7.uslp_quantity_rejected,0) as uslp_quantity_rejected1,
					ifnull(i7.tp_quantity_rejected,0) as tp_quantity_rejected1,
					ifnull(ifnull(i7.quantity_received,0)+ifnull(i7.warehouse_quantity_received,0)+ifnull(i7.uslp_quantity_received,0)+ifnull(i7.tp_quantity_received,0),0) as total_receipt1,
					IFNULL((ifnull(i7.quantity_rejected,0)+ifnull(i7.warehouse_quantity_rejected,0)+ifnull(i7.uslp_quantity_rejected,0)+ifnull(i7.tp_quantity_rejected,0)),0) as total_rejected1,
					ifnull(i8.dispensed_quantity,0) as dispensed_quantity1,
					ifnull(i8.control_used,0) as control_used1,
					ifnull(i8.dispensed_quantity_camp,0) as dispensed_quantity_camp1,
					ifnull(i8.control_used_camp,0) as control_used_camp1,
					IFNULL((ifnull(i8.dispensed_quantity_camp,0)+ifnull(i8.control_used_camp,0)),0) as total_utilize_camp1,
					IFNULL((ifnull(i8.dispensed_quantity,0)+ifnull(i8.control_used,0)),0) as total_utilize1,
					ifnull(i9.auto_relocated_quantity,0) as auto_relocated_quantity1,
					ifnull(i10.manual_relocated_quantity,0) as manual_relocated_quantity1,
					IFNULL((ifnull(i9.auto_relocated_quantity,0)+ifnull(i10.manual_relocated_quantity,0)),0) as total_transfer_out1,
					ifnull(i11.returned_quantity,0) as returned_quantity1,
					IFNULL(i12.Expire_stock,0) as Expire_stock2,
					ifnull(i12.quantity_received,0) as quantity_received2,
					ifnull(i12.warehouse_quantity_received,0) as warehouse_quantity_received2,
					ifnull(i12.uslp_quantity_received,0) as uslp_quantity_received2,
					ifnull(i12.tp_quantity_received,0) as tp_quantity_received2,
					ifnull(i12.quantity_rejected,0) as quantity_rejected2,
					ifnull(i12.warehouse_quantity_rejected,0) as warehouse_quantity_rejected2,
					ifnull(i12.uslp_quantity_rejected,0) as uslp_quantity_rejected2,
					ifnull(i12.tp_quantity_rejected,0) as tp_quantity_rejected2,
					ifnull(ifnull(i12.quantity_received,0)+ifnull(i12.warehouse_quantity_received,0)+ifnull(i12.uslp_quantity_received,0)+ifnull(i12.tp_quantity_received,0),0) as total_receipt2,
					IFNULL((ifnull(i12.quantity_rejected,0)+ifnull(i12.warehouse_quantity_rejected,0)+ifnull(i12.uslp_quantity_rejected,0)+ifnull(i12.tp_quantity_rejected,0)),0) as total_rejected2,
					ifnull(i13.dispensed_quantity,0) as dispensed_quantity2,
					ifnull(i13.control_used,0) as control_used2,
					ifnull(i13.dispensed_quantity_camp,0) as dispensed_quantity_camp2,
					ifnull(i13.control_used_camp,0) as control_used_camp2,
					IFNULL((ifnull(i13.dispensed_quantity_camp,0)+ifnull(i13.control_used_camp,0)),0) as total_utilize_camp2,
					IFNULL((ifnull(i13.dispensed_quantity,0)+ifnull(i13.control_used,0)),0) as total_utilize2,
					ifnull(i14.auto_relocated_quantity,0) as auto_relocated_quantity2,
					ifnull(i15.manual_relocated_quantity,0) as manual_relocated_quantity2,
					IFNULL((ifnull(i14.auto_relocated_quantity,0)+ifnull(i15.manual_relocated_quantity,0)),0) as total_transfer_out2,
					ifnull(i16.returned_quantity,0) as returned_quantity2
					 FROM 
				(SELECT indent_date,quantity,indent_remark,id_mststate,id_mstfacility,id_mst_drugs,inventory_id,transfer_to,indent_num,type,drug_name FROM tbl_inventory i where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$date_filter." AND i.is_deleted='0' AND i.indent_accept_date IS NOT null AND ((i.Flag='I' and (i.relocation_status!=0 || i.relocation_status is not null)and relocation_status>2) || i.Flag='R' || i.Flag='U' || i.Flag='L') group by indent_num,id_mstfacility) AS i1
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
				ON 
				i1.inventory_id=i2.inventory_id
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
				on
				i1.id_mst_drugs=i3.id_mst_drugs AND i1.indent_num=i3.indent_num
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
				on
				i1.id_mst_drugs=i4.id_mst_drugs  AND i1.indent_num=i4.indent_num AND i1.id_mstfacility=i4.transfer_to
				Left JOIN

				(SELECT id_mstfacility,sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
				on
				i1.id_mst_drugs=i5.id_mst_drugs AND i1.indent_num=i5.indent_num AND i1.inventory_id=i5.inventory_id
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i6
				on
				i1.id_mst_drugs=i6.id_mst_drugs AND i1.indent_num=i6.indent_num
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i7
				ON 
				i1.inventory_id=i7.inventory_id
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i8
				on
				i1.id_mst_drugs=i8.id_mst_drugs AND i1.indent_num=i8.indent_num
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i9
				on
				i1.id_mst_drugs=i9.id_mst_drugs  AND i1.indent_num=i9.indent_num AND i1.id_mstfacility=i9.transfer_to
				Left JOIN

				(SELECT id_mstfacility,sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i10
				on
				i1.id_mst_drugs=i10.id_mst_drugs AND i1.indent_num=i10.indent_num AND i1.inventory_id=i10.inventory_id
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i11
				on
				i1.id_mst_drugs=i11.id_mst_drugs AND i1.indent_num=i11.indent_num
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i12
				ON 
				i1.inventory_id=i12.inventory_id
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i13
				on
				i1.id_mst_drugs=i13.id_mst_drugs AND i1.indent_num=i13.indent_num
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i14
				on
				i1.id_mst_drugs=i14.id_mst_drugs  AND i1.indent_num=i14.indent_num AND i1.id_mstfacility=i14.transfer_to
				Left JOIN

				(SELECT id_mstfacility,sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i15
				on
				i1.id_mst_drugs=i15.id_mst_drugs AND i1.indent_num=i15.indent_num AND i1.id_mstfacility=i15.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.inventory_id,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i16
				on
				i1.id_mst_drugs=i16.id_mst_drugs AND i1.indent_num=i16.indent_num
				inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs
				where i1.indent_num is not null order by i1.id_mst_drugs";

					     
/*if ($is_last_month==1) {
	print_r($query); die();
}*/
		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}	
function get_facility_lab_forcast_Report($type=NULL,$id_mststate=NULL,$id_mst_drugs=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
		if ($invrepo['item_type']=='') {
				$itemname= '';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else{
				$itemname= "AND id_mst_drugs = ".$invrepo['item_type']."";
			}

		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on Q.id_mststate=st.id_mststate";

			}
			elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";

			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}
			if($id_mststate=='' && ($loginData->user_type=='1' || $loginData->user_type=='3' || $loginData->user_type=='2')){
				$sess_wherep = "AND 1";
			}
			else{
				 $sess_wherep="AND id_mststate='".$id_mststate."' and id_mst_drugs=".$id_mst_drugs."";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}

			$startdate1=$filters1['startdate1'];
				$startdate=$filters1['startdate'];
				$enddate=$filters1['enddate'];
			    $Entry_Date="i.Entry_Date BETWEEN '".$startdate."' And '".$enddate."'";
				$dispensed_date="(i.from_Date BETWEEN '".$startdate."' And '".$enddate."'AND i.to_Date BETWEEN '".$startdate."' And '".$enddate."')";
				$dispatch_date="i.dispatch_date BETWEEN '".$startdate."' And '".$enddate."'";
				$failure_date="i.failure_date BETWEEN '".$startdate."' And '".$enddate."'";

				$Entry_Date1="i.Entry_Date <'".$startdate."'";
				$dispensed_date1="i.to_Date <'".$startdate."'";
				$dispatch_date1="i.dispatch_date <'".$startdate."'";
				$failure_date1="i.failure_date <'".$startdate."'";

				$Entry_Date2="i.Entry_Date <'".$startdate1."'";
				$dispensed_date2="i.to_Date <'".$startdate1."'";
				$dispatch_date2="i.dispatch_date <'".$startdate1."'";
				$failure_date2="i.failure_date <'".$startdate1."'";

				$date_filter="(i.indent_date<='".$enddate."' OR i.Entry_Date<='".$enddate."' OR i.dispatch_date<='".$enddate."' OR i.to_Date<='".$enddate."')";


            $query="SELECT q.id_mststate,q.hospital,q.id_mstfacility,q.lead_time,q.buffer_stock,q.NAME as NAME,q.drug_name,q.id_mst_drugs,q.type,q.strength,
				sum(q.quantity) AS quantity,
				sum(q.quantity_received) AS quantity_received,
				SUM(case when q.Expire_stock<1 then 0 ELSE q.Expire_stock END) AS Expire_stock,
				sum(q.warehouse_quantity_received) AS warehouse_quantity_received,
				sum(q.uslp_quantity_received) AS uslp_quantity_received,
				sum(q.tp_quantity_received) AS tp_quantity_received,
				sum(q.quantity_rejected) AS quantity_rejected,
				sum(q.warehouse_quantity_rejected) AS warehouse_quantity_rejected,
				sum(q.uslp_quantity_rejected) AS uslp_quantity_rejected,
				sum(q.tp_quantity_rejected) AS tp_quantity_rejected,
				sum(IFNULL(q.total_receipt,0)) as total_receipt,
				sum(IFNULL(q.total_rejected,0)) as total_rejected,
				sum(q.dispensed_quantity) AS dispensed_quantity,
				sum(q.control_used) AS control_used,
				sum(q.dispensed_quantity_camp) AS dispensed_quantity_camp,
				sum(q.control_used_camp) AS control_used_camp,
				sum(IFNULL(q.total_utilize_camp,0)) as total_utilize_camp,
				sum(IFNULL(q.total_utilize,0)) as total_utilize,
				sum(q.auto_relocated_quantity) AS auto_relocated_quantity,
				sum(q.manual_relocated_quantity) AS manual_relocated_quantity,
				sum(IFNULL(q.total_transfer_out,0)) as total_transfer_out,
				sum(q.returned_quantity) AS returned_quantity,
				sum(q.quantity_received1) AS quantity_received1,
				SUM(case when q.Expire_stock1<1 then 0 ELSE q.Expire_stock1 END) AS Expire_stock1,
				sum(q.warehouse_quantity_received1) AS warehouse_quantity_received1,
				sum(q.uslp_quantity_received1) AS uslp_quantity_received1,
				sum(q.tp_quantity_received1) AS tp_quantity_received1,
				sum(q.quantity_rejected1) AS quantity_rejected1,
				sum(q.warehouse_quantity_rejected1) AS warehouse_quantity_rejected1,
				sum(q.uslp_quantity_rejected1) AS uslp_quantity_rejected1,
				sum(q.tp_quantity_rejected1) AS tp_quantity_rejected1,
				sum(IFNULL(q.total_receipt1,0)) as total_receipt1,
				sum(IFNULL(q.total_rejected1,0)) as total_rejected1,
				sum(q.dispensed_quantity1) AS dispensed_quantity1,
				sum(q.control_used1) AS control_used1,
				sum(q.dispensed_quantity_camp1) AS dispensed_quantity_camp1,
				sum(q.control_used_camp1) AS control_used_camp1,
				sum(IFNULL(q.total_utilize_camp,0)) as total_utilize_camp1,
				sum(IFNULL(q.total_utilize1,0)) as total_utilize1,
				sum(q.auto_relocated_quantity1) AS auto_relocated_quantity1,
				sum(q.manual_relocated_quantity1) AS manual_relocated_quantity1,
				sum(IFNULL(q.total_transfer_out1,0)) as total_transfer_out1,
				sum(q.returned_quantity1) AS returned_quantity1,
				sum(q.quantity_received2) AS quantity_received2,
				SUM(case when q.Expire_stock2<1 then 0 ELSE q.Expire_stock2 END) AS Expire_stock2,
				sum(q.warehouse_quantity_received2) AS warehouse_quantity_received2,
				sum(q.uslp_quantity_received2) AS uslp_quantity_received2,
				sum(q.tp_quantity_received2) AS tp_quantity_received2,
				sum(q.quantity_rejected2) AS quantity_rejected2,
				sum(q.warehouse_quantity_rejected2) AS warehouse_quantity_rejected2,
				sum(q.uslp_quantity_rejected2) AS uslp_quantity_rejected2,
				sum(q.tp_quantity_rejected2) AS tp_quantity_rejected2,
				sum(IFNULL(q.total_receipt2,0)) as total_receipt2,
				sum(IFNULL(q.total_rejected2,0)) as total_rejected2,
				sum(q.dispensed_quantity2) AS dispensed_quantity2,
				sum(q.control_used2) AS control_used2,
				sum(q.dispensed_quantity_camp2) AS dispensed_quantity_camp2,
				sum(q.control_used_camp2) AS control_used_camp2,
				sum(IFNULL(q.total_utilize_camp2,0)) as total_utilize_camp2,
				sum(IFNULL(q.total_utilize2,0)) as total_utilize2,
				sum(q.auto_relocated_quantity2) AS auto_relocated_quantity2,
				sum(q.manual_relocated_quantity2) AS manual_relocated_quantity2,
				sum(IFNULL(q.total_transfer_out2,0)) as total_transfer_out2,
				sum(q.returned_quantity2) AS returned_quantity2
				FROM (
				SELECT DISTINCT i1.id_mststate,i1.id_mstfacility,concat(f.City, '-', f.FacilityType) AS hospital,n.drug_name as NAME,n.lead_time,s.buffer_stock,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,i1.indent_num,IFNULL(i2.Expire_stock,0) as Expire_stock,
					ifnull(i1.quantity,0) as quantity,
					ifnull(i2.quantity_received,0) as quantity_received,
					ifnull(i2.warehouse_quantity_received,0) as warehouse_quantity_received,
					ifnull(i2.uslp_quantity_received,0) as uslp_quantity_received,
					ifnull(i2.tp_quantity_received,0) as tp_quantity_received,
					ifnull(i2.quantity_rejected,0) as quantity_rejected,
					ifnull(i2.warehouse_quantity_rejected,0) as warehouse_quantity_rejected,
					ifnull(i2.uslp_quantity_rejected,0) as uslp_quantity_rejected,
					ifnull(i2.tp_quantity_rejected,0) as tp_quantity_rejected,
					ifnull(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0),0) as total_receipt,
					IFNULL((ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)),0) as total_rejected,
					ifnull(i3.dispensed_quantity,0) as dispensed_quantity,
					ifnull(i3.control_used,0) as control_used,
					ifnull(i3.dispensed_quantity_camp,0) as dispensed_quantity_camp,
					ifnull(i3.control_used_camp,0) as control_used_camp,
					IFNULL((ifnull(i3.dispensed_quantity_camp,0)+ifnull(i3.control_used_camp,0)),0) as total_utilize_camp,
					IFNULL((ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)),0) as total_utilize,
					ifnull(i4.auto_relocated_quantity,0) as auto_relocated_quantity,
					ifnull(i5.manual_relocated_quantity,0) as manual_relocated_quantity,
					IFNULL((ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)),0) as total_transfer_out,
					ifnull(i6.returned_quantity,0) as returned_quantity,
					IFNULL(i7.Expire_stock,0) as Expire_stock1,
					ifnull(i7.quantity_received,0) as quantity_received1,
					ifnull(i7.warehouse_quantity_received,0) as warehouse_quantity_received1,
					ifnull(i7.uslp_quantity_received,0) as uslp_quantity_received1,
					ifnull(i7.tp_quantity_received,0) as tp_quantity_received1,
					ifnull(i7.quantity_rejected,0) as quantity_rejected1,
					ifnull(i7.warehouse_quantity_rejected,0) as warehouse_quantity_rejected1,
					ifnull(i7.uslp_quantity_rejected,0) as uslp_quantity_rejected1,
					ifnull(i7.tp_quantity_rejected,0) as tp_quantity_rejected1,
					ifnull(ifnull(i7.quantity_received,0)+ifnull(i7.warehouse_quantity_received,0)+ifnull(i7.uslp_quantity_received,0)+ifnull(i7.tp_quantity_received,0),0) as total_receipt1,
					IFNULL((ifnull(i7.quantity_rejected,0)+ifnull(i7.warehouse_quantity_rejected,0)+ifnull(i7.uslp_quantity_rejected,0)+ifnull(i7.tp_quantity_rejected,0)),0) as total_rejected1,
					ifnull(i8.dispensed_quantity,0) as dispensed_quantity1,
					ifnull(i8.control_used,0) as control_used1,
					ifnull(i8.dispensed_quantity_camp,0) as dispensed_quantity_camp1,
					ifnull(i8.control_used_camp,0) as control_used_camp1,
					IFNULL((ifnull(i8.dispensed_quantity_camp,0)+ifnull(i8.control_used_camp,0)),0) as total_utilize_camp1,
					IFNULL((ifnull(i8.dispensed_quantity,0)+ifnull(i8.control_used,0)),0) as total_utilize1,
					ifnull(i9.auto_relocated_quantity,0) as auto_relocated_quantity1,
					ifnull(i10.manual_relocated_quantity,0) as manual_relocated_quantity1,
					IFNULL((ifnull(i9.auto_relocated_quantity,0)+ifnull(i10.manual_relocated_quantity,0)),0) as total_transfer_out1,
					ifnull(i11.returned_quantity,0) as returned_quantity1,
					IFNULL(i12.Expire_stock,0) as Expire_stock2,
					ifnull(i12.quantity_received,0) as quantity_received2,
					ifnull(i12.warehouse_quantity_received,0) as warehouse_quantity_received2,
					ifnull(i12.uslp_quantity_received,0) as uslp_quantity_received2,
					ifnull(i12.tp_quantity_received,0) as tp_quantity_received2,
					ifnull(i12.quantity_rejected,0) as quantity_rejected2,
					ifnull(i12.warehouse_quantity_rejected,0) as warehouse_quantity_rejected2,
					ifnull(i12.uslp_quantity_rejected,0) as uslp_quantity_rejected2,
					ifnull(i12.tp_quantity_rejected,0) as tp_quantity_rejected2,
					ifnull(ifnull(i12.quantity_received,0)+ifnull(i12.warehouse_quantity_received,0)+ifnull(i12.uslp_quantity_received,0)+ifnull(i12.tp_quantity_received,0),0) as total_receipt2,
					IFNULL((ifnull(i12.quantity_rejected,0)+ifnull(i12.warehouse_quantity_rejected,0)+ifnull(i12.uslp_quantity_rejected,0)+ifnull(i12.tp_quantity_rejected,0)),0) as total_rejected2,
					ifnull(i13.dispensed_quantity,0) as dispensed_quantity2,
					ifnull(i13.control_used,0) as control_used2,
					ifnull(i13.dispensed_quantity_camp,0) as dispensed_quantity_camp2,
					ifnull(i13.control_used_camp,0) as control_used_camp2,
					IFNULL((ifnull(i13.dispensed_quantity_camp,0)+ifnull(i13.control_used_camp,0)),0) as total_utilize_camp2,
					IFNULL((ifnull(i13.dispensed_quantity,0)+ifnull(i13.control_used,0)),0) as total_utilize2,
					ifnull(i14.auto_relocated_quantity,0) as auto_relocated_quantity2,
					ifnull(i15.manual_relocated_quantity,0) as manual_relocated_quantity2,
					IFNULL((ifnull(i14.auto_relocated_quantity,0)+ifnull(i15.manual_relocated_quantity,0)),0) as total_transfer_out2,
					ifnull(i16.returned_quantity,0) as returned_quantity2
				FROM 
				(SELECT indent_date,quantity,indent_remark,id_mststate,id_mstfacility,id_mst_drugs,inventory_id,transfer_to,indent_num,type,drug_name FROM tbl_inventory i where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname."And is_deleted='0' AND ".$date_filter." AND ((i.Flag='I' and (i.relocation_status!=0 || i.relocation_status is not null )and relocation_status>2) || i.Flag='R' || i.Flag='U' || i.Flag='L') GROUP BY indent_num,id_mstfacility,id_mst_drugs) AS i1
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
				ON 
				i1.id_mst_drugs=i2.id_mst_drugs AND i1.indent_num=i2.indent_num and i1.id_mstfacility=i2.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
				on
				i1.id_mst_drugs=i3.id_mst_drugs AND i1.indent_num=i3.indent_num and i1.id_mstfacility=i3.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
				on
				i1.id_mst_drugs=i4.id_mst_drugs  AND i1.indent_num=i4.indent_num AND i1.id_mstfacility=i4.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
				on
				i1.id_mst_drugs=i5.id_mst_drugs AND i1.indent_num=i5.indent_num and i1.id_mstfacility=i5.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i6
				on
				i1.id_mst_drugs=i6.id_mst_drugs AND i1.indent_num=i6.indent_num and i1.id_mstfacility=i6.id_mstfacility
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i7
				ON 
				i1.id_mst_drugs=i7.id_mst_drugs AND i1.indent_num=i7.indent_num and i1.id_mstfacility=i7.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i8
				on
				i1.id_mst_drugs=i8.id_mst_drugs AND i1.indent_num=i8.indent_num and i1.id_mstfacility=i8.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i9
				on
				i1.id_mst_drugs=i9.id_mst_drugs  AND i1.indent_num=i9.indent_num AND i1.id_mstfacility=i9.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i10
				on
				i1.id_mst_drugs=i10.id_mst_drugs AND i1.indent_num=i10.indent_num and i1.id_mstfacility=i10.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i11
				on
				i1.id_mst_drugs=i11.id_mst_drugs AND i1.indent_num=i11.indent_num and i1.id_mstfacility=i11.id_mstfacility
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i12
				ON 
				i1.id_mst_drugs=i11.id_mst_drugs AND i1.indent_num=i12.indent_num and i1.id_mstfacility=i12.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i13
				on
				i1.id_mst_drugs=i13.id_mst_drugs AND i1.indent_num=i13.indent_num and i1.id_mstfacility=i13.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i14
				on
				i1.id_mst_drugs=i14.id_mst_drugs  AND i1.indent_num=i14.indent_num AND i1.id_mstfacility=i14.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i15
				on
				i1.id_mst_drugs=i15.id_mst_drugs AND i1.indent_num=i15.indent_num AND i1.id_mstfacility=i15.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i16
				on
				i1.id_mst_drugs=i16.id_mst_drugs AND i1.indent_num=i16.indent_num and i1.id_mstfacility=i16.id_mstfacility
				inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs
				inner JOIN  `mstfacility` f ON i1.id_mstfacility=f.id_mstfacility
				where i1.indent_num is not null) AS q 
				GROUP BY q.id_mstfacility,q.id_mst_drugs having ".$this->where_condition." order BY q.id_mst_drugs";

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}	
function get_state_forcast_Report($type=NULL,$is_last_month=NULL){
	$invrepo=$this->session->userdata('inv_repo_filter');
	$filters1=$this->session->userdata('filters1');
		if ($invrepo['item_type']=='') {
				$itemname= '';
			}
			else if($invrepo['item_type']=='Kit'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][1]->LookupCode."'";
			}
			else if($invrepo['item_type']=='drug'){
				$itemname= "AND type = '".$invrepo['item_mst_look'][0]->LookupCode."'";
			}
			else{
				$itemname= "AND id_mst_drugs = ".$invrepo['item_type']."";
			}

		$loginData = $this->session->userdata('loginData'); 
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on Q.id_mststate=st.id_mststate";

			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."'";
				$hospital="concat(f.City, '-', f.FacilityType) AS hospital,";
				$qry="inner JOIN  `mstfacility` f ON i1.id_mstfacility=f.id_mstfacility";

			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "id_mststate = '".$loginData->State_ID."' ";
			}
			if($filters1['id_search_state']=='' && $loginData->user_type=='1'){
				$sess_wherep = "AND 1";
				$hospital="st.StateName,";
				$qry="inner join `mststate` st on i1.id_mststate=st.id_mststate";
			}
			else{
				if($filters1['id_search_state']==NULL){
					$State_ID=$loginData->State_ID;
				}
				else{
					$State_ID=$filters1['id_search_state'];
				}
				$sess_wherep = "AND id_mststate = '".$State_ID."'";
				$hospital="concat(f.City, '-', f.FacilityType) AS hospital,";
				$qry="inner JOIN  `mstfacility` f ON i1.id_mstfacility=f.id_mstfacility";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
			$startdate1=$filters1['startdate1'];
				$startdate=$filters1['startdate'];
				$enddate=$filters1['enddate'];
			    $Entry_Date="i.Entry_Date BETWEEN '".$startdate."' And '".$enddate."'";
				$dispensed_date="(i.from_Date BETWEEN '".$startdate."' And '".$enddate."'AND i.to_Date BETWEEN '".$startdate."' And '".$enddate."')";
				$dispatch_date="i.dispatch_date BETWEEN '".$startdate."' And '".$enddate."'";
				$failure_date="i.failure_date BETWEEN '".$startdate."' And '".$enddate."'";

				$Entry_Date1="i.Entry_Date <'".$startdate."'";
				$dispensed_date1="i.to_Date <'".$startdate."'";
				$dispatch_date1="i.dispatch_date <'".$startdate."'";
				$failure_date1="i.failure_date <'".$startdate."'";

				$Entry_Date2="i.Entry_Date <'".$startdate1."'";
				$dispensed_date2="i.to_Date <'".$startdate1."'";
				$dispatch_date2="i.dispatch_date <'".$startdate1."'";
				$failure_date2="i.failure_date <'".$startdate1."'";

				$date_filter="(i.indent_date<='".$enddate."' OR i.Entry_Date<='".$enddate."' OR i.dispatch_date<='".$enddate."' OR i.to_Date<='".$enddate."')";
						
					
$query="SELECT q.id_mststate,q.StateName,q.NAME as NAME,q.lead_time,q.buffer_stock,q.drug_name,q.id_mst_drugs,q.type,q.strength,
				sum(q.quantity_received) AS quantity_received,
				sum(q.quantity) AS quantity,
				SUM(case when q.Expire_stock<1 then 0 ELSE q.Expire_stock END) AS Expire_stock,
				sum(q.warehouse_quantity_received) AS warehouse_quantity_received,
				sum(q.uslp_quantity_received) AS uslp_quantity_received,
				sum(q.tp_quantity_received) AS tp_quantity_received,
				sum(q.quantity_rejected) AS quantity_rejected,
				sum(q.warehouse_quantity_rejected) AS warehouse_quantity_rejected,
				sum(q.uslp_quantity_rejected) AS uslp_quantity_rejected,
				sum(q.tp_quantity_rejected) AS tp_quantity_rejected,
				sum(IFNULL(q.total_receipt,0)) as total_receipt,
				sum(IFNULL(q.total_rejected,0)) as total_rejected,
				sum(q.dispensed_quantity) AS dispensed_quantity,
				sum(q.control_used) AS control_used,
				sum(q.dispensed_quantity_camp) AS dispensed_quantity_camp,
				sum(q.control_used_camp) AS control_used_camp,
				sum(IFNULL(q.total_utilize_camp,0)) as total_utilize_camp,
				sum(IFNULL(q.total_utilize,0)) as total_utilize,
				sum(q.auto_relocated_quantity) AS auto_relocated_quantity,
				sum(q.manual_relocated_quantity) AS manual_relocated_quantity,
				sum(IFNULL(q.total_transfer_out,0)) as total_transfer_out,
				sum(q.returned_quantity) AS returned_quantity,
				sum(q.quantity_received1) AS quantity_received1,
				SUM(case when q.Expire_stock1<1 then 0 ELSE q.Expire_stock1 END) AS Expire_stock1,
				sum(q.warehouse_quantity_received1) AS warehouse_quantity_received1,
				sum(q.uslp_quantity_received1) AS uslp_quantity_received1,
				sum(q.tp_quantity_received1) AS tp_quantity_received1,
				sum(q.quantity_rejected1) AS quantity_rejected1,
				sum(q.warehouse_quantity_rejected1) AS warehouse_quantity_rejected1,
				sum(q.uslp_quantity_rejected1) AS uslp_quantity_rejected1,
				sum(q.tp_quantity_rejected1) AS tp_quantity_rejected1,
				sum(IFNULL(q.total_receipt1,0)) as total_receipt1,
				sum(IFNULL(q.total_rejected1,0)) as total_rejected1,
				sum(q.dispensed_quantity1) AS dispensed_quantity1,
				sum(q.control_used1) AS control_used1,
				sum(q.dispensed_quantity_camp1) AS dispensed_quantity_camp1,
				sum(q.control_used_camp1) AS control_used_camp1,
				sum(IFNULL(q.total_utilize_camp,0)) as total_utilize_camp1,
				sum(IFNULL(q.total_utilize1,0)) as total_utilize1,
				sum(q.auto_relocated_quantity1) AS auto_relocated_quantity1,
				sum(q.manual_relocated_quantity1) AS manual_relocated_quantity1,
				sum(IFNULL(q.total_transfer_out1,0)) as total_transfer_out1,
				sum(q.returned_quantity1) AS returned_quantity1,
				sum(q.quantity_received2) AS quantity_received2,
				SUM(case when q.Expire_stock2<1 then 0 ELSE q.Expire_stock2 END) AS Expire_stock2,
				sum(q.warehouse_quantity_received2) AS warehouse_quantity_received2,
				sum(q.uslp_quantity_received2) AS uslp_quantity_received2,
				sum(q.tp_quantity_received2) AS tp_quantity_received2,
				sum(q.quantity_rejected2) AS quantity_rejected2,
				sum(q.warehouse_quantity_rejected2) AS warehouse_quantity_rejected2,
				sum(q.uslp_quantity_rejected2) AS uslp_quantity_rejected2,
				sum(q.tp_quantity_rejected2) AS tp_quantity_rejected2,
				sum(IFNULL(q.total_receipt2,0)) as total_receipt2,
				sum(IFNULL(q.total_rejected2,0)) as total_rejected2,
				sum(q.dispensed_quantity2) AS dispensed_quantity2,
				sum(q.control_used2) AS control_used2,
				sum(q.dispensed_quantity_camp2) AS dispensed_quantity_camp2,
				sum(q.control_used_camp2) AS control_used_camp2,
				sum(IFNULL(q.total_utilize_camp2,0)) as total_utilize_camp2,
				sum(IFNULL(q.total_utilize2,0)) as total_utilize2,
				sum(q.auto_relocated_quantity2) AS auto_relocated_quantity2,
				sum(q.manual_relocated_quantity2) AS manual_relocated_quantity2,
				sum(IFNULL(q.total_transfer_out2,0)) as total_transfer_out2,
				sum(q.returned_quantity2) AS returned_quantity2
				FROM (
				SELECT DISTINCT i1.id_mststate,i1.id_mstfacility,st.StateName,n.drug_name as NAME,n.lead_time,s.buffer_stock,i1.drug_name,i1.id_mst_drugs,i1.type,s.strength,i1.indent_num,IFNULL(i2.Expire_stock,0) as Expire_stock,
					ifnull(i1.quantity,0) as quantity,
					ifnull(i2.quantity_received,0) as quantity_received,
					ifnull(i2.warehouse_quantity_received,0) as warehouse_quantity_received,
					ifnull(i2.uslp_quantity_received,0) as uslp_quantity_received,
					ifnull(i2.tp_quantity_received,0) as tp_quantity_received,
					ifnull(i2.quantity_rejected,0) as quantity_rejected,
					ifnull(i2.warehouse_quantity_rejected,0) as warehouse_quantity_rejected,
					ifnull(i2.uslp_quantity_rejected,0) as uslp_quantity_rejected,
					ifnull(i2.tp_quantity_rejected,0) as tp_quantity_rejected,
					ifnull(ifnull(i2.quantity_received,0)+ifnull(i2.warehouse_quantity_received,0)+ifnull(i2.uslp_quantity_received,0)+ifnull(i2.tp_quantity_received,0),0) as total_receipt,
					IFNULL((ifnull(i2.quantity_rejected,0)+ifnull(i2.warehouse_quantity_rejected,0)+ifnull(i2.uslp_quantity_rejected,0)+ifnull(i2.tp_quantity_rejected,0)),0) as total_rejected,
					ifnull(i3.dispensed_quantity,0) as dispensed_quantity,
					ifnull(i3.control_used,0) as control_used,
					ifnull(i3.dispensed_quantity_camp,0) as dispensed_quantity_camp,
					ifnull(i3.control_used_camp,0) as control_used_camp,
					IFNULL((ifnull(i3.dispensed_quantity_camp,0)+ifnull(i3.control_used_camp,0)),0) as total_utilize_camp,
					IFNULL((ifnull(i3.dispensed_quantity,0)+ifnull(i3.control_used,0)),0) as total_utilize,
					ifnull(i4.auto_relocated_quantity,0) as auto_relocated_quantity,
					ifnull(i5.manual_relocated_quantity,0) as manual_relocated_quantity,
					IFNULL((ifnull(i4.auto_relocated_quantity,0)+ifnull(i5.manual_relocated_quantity,0)),0) as total_transfer_out,
					ifnull(i6.returned_quantity,0) as returned_quantity,
					IFNULL(i7.Expire_stock,0) as Expire_stock1,
					ifnull(i7.quantity_received,0) as quantity_received1,
					ifnull(i7.warehouse_quantity_received,0) as warehouse_quantity_received1,
					ifnull(i7.uslp_quantity_received,0) as uslp_quantity_received1,
					ifnull(i7.tp_quantity_received,0) as tp_quantity_received1,
					ifnull(i7.quantity_rejected,0) as quantity_rejected1,
					ifnull(i7.warehouse_quantity_rejected,0) as warehouse_quantity_rejected1,
					ifnull(i7.uslp_quantity_rejected,0) as uslp_quantity_rejected1,
					ifnull(i7.tp_quantity_rejected,0) as tp_quantity_rejected1,
					ifnull(ifnull(i7.quantity_received,0)+ifnull(i7.warehouse_quantity_received,0)+ifnull(i7.uslp_quantity_received,0)+ifnull(i7.tp_quantity_received,0),0) as total_receipt1,
					IFNULL((ifnull(i7.quantity_rejected,0)+ifnull(i7.warehouse_quantity_rejected,0)+ifnull(i7.uslp_quantity_rejected,0)+ifnull(i7.tp_quantity_rejected,0)),0) as total_rejected1,
					ifnull(i8.dispensed_quantity,0) as dispensed_quantity1,
					ifnull(i8.control_used,0) as control_used1,
					ifnull(i8.dispensed_quantity_camp,0) as dispensed_quantity_camp1,
					ifnull(i8.control_used_camp,0) as control_used_camp1,
					IFNULL((ifnull(i8.dispensed_quantity_camp,0)+ifnull(i8.control_used_camp,0)),0) as total_utilize_camp1,
					IFNULL((ifnull(i8.dispensed_quantity,0)+ifnull(i8.control_used,0)),0) as total_utilize1,
					ifnull(i9.auto_relocated_quantity,0) as auto_relocated_quantity1,
					ifnull(i10.manual_relocated_quantity,0) as manual_relocated_quantity1,
					IFNULL((ifnull(i9.auto_relocated_quantity,0)+ifnull(i10.manual_relocated_quantity,0)),0) as total_transfer_out1,
					ifnull(i11.returned_quantity,0) as returned_quantity1,
					IFNULL(i12.Expire_stock,0) as Expire_stock2,
					ifnull(i12.quantity_received,0) as quantity_received2,
					ifnull(i12.warehouse_quantity_received,0) as warehouse_quantity_received2,
					ifnull(i12.uslp_quantity_received,0) as uslp_quantity_received2,
					ifnull(i12.tp_quantity_received,0) as tp_quantity_received2,
					ifnull(i12.quantity_rejected,0) as quantity_rejected2,
					ifnull(i12.warehouse_quantity_rejected,0) as warehouse_quantity_rejected2,
					ifnull(i12.uslp_quantity_rejected,0) as uslp_quantity_rejected2,
					ifnull(i12.tp_quantity_rejected,0) as tp_quantity_rejected2,
					ifnull(ifnull(i12.quantity_received,0)+ifnull(i12.warehouse_quantity_received,0)+ifnull(i12.uslp_quantity_received,0)+ifnull(i12.tp_quantity_received,0),0) as total_receipt2,
					IFNULL((ifnull(i12.quantity_rejected,0)+ifnull(i12.warehouse_quantity_rejected,0)+ifnull(i12.uslp_quantity_rejected,0)+ifnull(i12.tp_quantity_rejected,0)),0) as total_rejected2,
					ifnull(i13.dispensed_quantity,0) as dispensed_quantity2,
					ifnull(i13.control_used,0) as control_used2,
					ifnull(i13.dispensed_quantity_camp,0) as dispensed_quantity_camp2,
					ifnull(i13.control_used_camp,0) as control_used_camp2,
					IFNULL((ifnull(i13.dispensed_quantity_camp,0)+ifnull(i13.control_used_camp,0)),0) as total_utilize_camp2,
					IFNULL((ifnull(i13.dispensed_quantity,0)+ifnull(i13.control_used,0)),0) as total_utilize2,
					ifnull(i14.auto_relocated_quantity,0) as auto_relocated_quantity2,
					ifnull(i15.manual_relocated_quantity,0) as manual_relocated_quantity2,
					IFNULL((ifnull(i14.auto_relocated_quantity,0)+ifnull(i15.manual_relocated_quantity,0)),0) as total_transfer_out2,
					ifnull(i16.returned_quantity,0) as returned_quantity2
				FROM 
				(SELECT indent_date,quantity,indent_remark,id_mststate,id_mstfacility,id_mst_drugs,inventory_id,transfer_to,indent_num,type,drug_name FROM tbl_inventory i where ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$date_filter." AND i.is_deleted='0' AND i.indent_accept_date IS NOT null AND ((i.Flag='I' and (i.relocation_status!=0 || i.relocation_status is not null) and relocation_status>2) || i.Flag='R' || i.Flag='U' || i.Flag='L') GROUP BY indent_num,id_mstfacility,id_mst_drugs) AS i1
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i2
				ON 
				i1.id_mst_drugs=i2.id_mst_drugs AND i1.indent_num=i2.indent_num and i1.id_mstfacility=i2.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i3
				on
				i1.id_mst_drugs=i3.id_mst_drugs AND i1.indent_num=i3.indent_num and i1.id_mstfacility=i3.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i4
				on
				i1.id_mst_drugs=i4.id_mst_drugs  AND i1.indent_num=i4.indent_num AND i1.id_mstfacility=i4.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i5
				on
				i1.id_mst_drugs=i5.id_mst_drugs AND i1.indent_num=i5.indent_num and i1.id_mstfacility=i5.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i6
				on
				i1.id_mst_drugs=i6.id_mst_drugs AND i1.indent_num=i6.indent_num and i1.id_mstfacility=i6.id_mstfacility
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i7
				ON 
				i1.id_mst_drugs=i7.id_mst_drugs AND i1.indent_num=i7.indent_num and i1.id_mstfacility=i7.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i8
				on
				i1.id_mst_drugs=i8.id_mst_drugs AND i1.indent_num=i8.indent_num and i1.id_mstfacility=i8.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i9
				on
				i1.id_mst_drugs=i9.id_mst_drugs  AND i1.indent_num=i9.indent_num AND i1.id_mstfacility=i9.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i10
				on
				i1.id_mst_drugs=i10.id_mst_drugs AND i1.indent_num=i10.indent_num and i1.id_mstfacility=i10.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date1." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i11
				on
				i1.id_mst_drugs=i11.id_mst_drugs AND i1.indent_num=i11.indent_num and i1.id_mstfacility=i11.id_mstfacility
				LEFT JOIN 
				(SELECT sum(case when DATEDIFF(i.Expiry_Date,i.Entry_Date)<1 then (case when ((i.from_to_type=2 AND i.flag='I') OR i.from_to_type=3 OR i.from_to_type=4) then i.quantity_received ELSE i.warehouse_quantity_received END) ELSE 0 END) AS Expire_stock,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_received ELSE 0 END) quantity_received,sum(case when i.from_to_type=1 then i.quantity_received ELSE 0 END) warehouse_quantity_received,sum(case when i.from_to_type=3 then i.quantity_received ELSE 0 END) uslp_quantity_received,sum(case when i.from_to_type=4 then i.quantity_received ELSE 0 END) tp_quantity_received,sum(case when (i.from_to_type=2 AND i.flag='I') then i.quantity_rejected ELSE 0 END) quantity_rejected,sum(case when i.from_to_type=1 then i.warehouse_quantity_rejected ELSE 0 END) warehouse_quantity_rejected,sum(case when i.from_to_type=3 then i.quantity_rejected ELSE 0 END) uslp_quantity_rejected,sum(case when i.from_to_type=4 then i.quantity_rejected ELSE 0 END) tp_quantity_rejected,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R') and ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$Entry_Date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i12
				ON 
				i1.id_mst_drugs=i11.id_mst_drugs AND i1.indent_num=i12.indent_num and i1.id_mstfacility=i12.id_mstfacility
				Left JOIN
				(SELECT sum(case when i.utilization_purpose=2 then i.dispensed_quantity else 0 end) AS dispensed_quantity,sum(case when i.utilization_purpose=2 then i.control_used else 0 end) AS control_used,sum(case when i.utilization_purpose=1 then i.dispensed_quantity else 0 end) AS dispensed_quantity_camp,sum(case when i.utilization_purpose=1 then i.control_used else 0 end) AS control_used_camp,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='U') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$dispensed_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i13
				on
				i1.id_mst_drugs=i13.id_mst_drugs AND i1.indent_num=i13.indent_num and i1.id_mstfacility=i13.id_mstfacility
				Left JOIN
				(SELECT transfer_to,sum(case when (i.flag='I') then i.relocated_quantity ELSE 0 END) auto_relocated_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='I' and ".$dispatch_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,transfer_to,id_mst_drugs ORDER BY id_mst_drugs) AS i14
				on
				i1.id_mst_drugs=i14.id_mst_drugs  AND i1.indent_num=i14.indent_num AND i1.id_mstfacility=i14.transfer_to
				Left JOIN

				(SELECT sum(case when (i.flag='L') then i.relocated_quantity ELSE 0 END) manual_relocated_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE i.flag='L' AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." and ".$dispatch_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i15
				on
				i1.id_mst_drugs=i15.id_mst_drugs AND i1.indent_num=i15.indent_num AND i1.id_mstfacility=i15.id_mstfacility
				Left JOIN
				(SELECT sum(i.returned_quantity) AS returned_quantity,i.id_mstfacility,id_mst_drugs,indent_num FROM tbl_inventory i WHERE (i.Flag='F') AND ".$sess_where." ".$sess_wherep." ".$type." ".$itemname." AND ".$failure_date2." AND i.is_deleted='0' and i.is_temp='0' GROUP BY indent_num,id_mstfacility,id_mst_drugs ORDER BY id_mst_drugs) AS i16
				on
				i1.id_mst_drugs=i16.id_mst_drugs AND i1.indent_num=i16.indent_num and i1.id_mstfacility=i16.id_mstfacility
				inner join `mst_drug_strength` s ON i1.id_mst_drugs=s.id_mst_drug_strength inner join `mst_drugs` n on s.id_mst_drug=n.id_mst_drugs
				".$qry." where i1.indent_num is not null) AS q 
				 GROUP BY q.id_mststate,q.id_mst_drugs having ".$this->where_condition." order BY q.id_mst_drugs";

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

}
public function hcv_initiated_2_1($Session_StateID=null){

	$loginData = $this->session->userdata('loginData'); 

		$filters1                           = $this->session->userdata('filters1');

		$date_filter_reg="date between '".$filters1['treatment_start_date']."' AND '".$filters1['treatment_end_date']."'";

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2'){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
			$group='';
			$sess_where1='';
				if ($Session_StateID!=NULL) {
					$group=' group by Session_StateID';
					//$sess_where1=" AND Session_StateID = '".$Session_StateID."'";
				}
//" s where s.date between '2019-01-01' and '2019-12-31' and s.Session_StateID=3";
			
		 $query = "SELECT sum(p.initiatied_on_treatment) as initiatied_on_treatment from tblsummary_genderwise p
					WHERE
					   ".$date_filter_reg.' '.$sess_where.' '.$sess_where." ".$group;

					    
	 
		$result = $this->db->query($query)->result();
		
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}		
public function hcv_initiated_facility($id_mstfacility=null){

	$loginData = $this->session->userdata('loginData'); 

		$filters1                           = $this->session->userdata('filters1');

		$date_filter_reg="date between '".$filters1['treatment_start_date']."' AND '".$filters1['treatment_end_date']."'";

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2'){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
			$group='';
			$sess_where1='';
				if ($id_mstfacility!=NULL) {
					$group=' group by id_mstfacility';
					//$sess_where1=" AND Session_StateID = '".$Session_StateID."'";
				}
//" s where s.date between '2019-01-01' and '2019-12-31' and s.Session_StateID=3";
			
		 $query = "SELECT sum(p.initiatied_on_treatment) as initiatied_on_treatment from tblsummary_genderwise p
					WHERE
					   ".$date_filter_reg.' '.$sess_where.' '.$sess_where." ".$group;

					    
	 
		$result = $this->db->query($query)->result();
		
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}
}	
public function get_stock_mismatch_data_national($is_sum=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			/*$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";*/
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
			$groupby=',i.id_mststate';
				if ($is_sum==NULL) {
					$groupby=',i.id_mststate';
				}
				else{
					$groupby='';
				}
		        $query = "SELECT * FROM 
(SELECT i.id_mst_drugs,i.drug_name,i.id_mststate,i.id_mstfacility,sum(ifnull(i.quantity,0)) as quantity,sum(ifnull(i.approved_quantity,0)) as approved_quantity,sum(i.quantity_received) as quantity_received,sum(i.quantity_dispatched) as quantity_dispatched,sum(i.quantity_dispatched)-sum(i.quantity_received) as mismatch_quantity,i.type FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R' or i.flag='L') and(i.quantity_received is not null and i.quantity_dispatched is not NULL) and i.quantity_received<i.quantity_dispatched ".$sess_where." AND ".$itemname." and i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."' GROUP BY i.id_mst_drugs".$groupby." ORDER BY i.id_mst_drugs".$groupby.") AS i
LEFT JOIN
						  (SELECT id_mststate,StateName FROM mststate) AS f
						 		ON i.id_mststate=f.id_mststate
						 INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON i.id_mst_drugs=s.id_mst_drug_strength";

	///i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND 

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}		
public function get_stock_mismatch_data_state($type=NULL,$id_mststate=NULL,$id_mst_drugs=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			/*$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";*/
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
			if($id_mststate=='' && ($loginData->user_type=='1' || $loginData->user_type=='3')){
				$sess_wherep = "AND 1";
			}
			else{
				$sess_wherep="AND id_mststate='".$id_mststate."' and id_mst_drugs=".$id_mst_drugs."";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}
			$groupby=',i.id_mstfacility';
		        $query = "SELECT * FROM 
(SELECT i.id_mst_drugs,i.drug_name,i.id_mststate,i.id_mstfacility,sum(ifnull(i.quantity,0)) as quantity,sum(ifnull(i.approved_quantity,0)) as approved_quantity,sum(i.quantity_received) as quantity_received,sum(i.quantity_dispatched) as quantity_dispatched,sum(i.quantity_dispatched)-sum(i.quantity_received) as mismatch_quantity,i.type FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R' or i.flag='L') and(i.quantity_received is not null and i.quantity_dispatched is not NULL) and i.quantity_received<i.quantity_dispatched ".$sess_where." AND ".$itemname." ".$sess_wherep." and i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."' GROUP BY i.id_mst_drugs".$groupby." ORDER BY i.id_mst_drugs,i.id_mstfacility) AS i
inner JOIN 
						  ( SELECT concat(f.City, '-', f.FacilityType) AS hospital,id_mstfacility from `mstfacility` f) AS f
						 		ON i.id_mstfacility=f.id_mstfacility
						 INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON i.id_mst_drugs=s.id_mst_drug_strength";

	///i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND 

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}	
public function get_stock_mismatch_data_facility($type=NULL,$id_mstfacility=NULL,$id_mst_drugs=NULL){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "i.type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "i.type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "i.type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "i.id_mst_drugs = '".$filter['item_type']."'";
			}
			/*$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";*/
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'  AND i.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND i.id_mststate = '".$loginData->State_ID."' ";
			}
			$groupby=',i.id_mstfacility';
			if(($id_mstfacility=='' || $id_mstfacility==NULL) && ($loginData->user_type=='1' || $loginData->user_type=='2')){
				$sess_wherep = "AND 1 ";
			}
			else{
				$sess_wherep="AND i.id_mstfacility='".$id_mstfacility."' and id_mst_drugs=".$id_mst_drugs."";
			}
			if($type=='' || $type==NULL){
					$type='AND 1';
				}
				elseif ($type==1) {
					$type='AND type=1';
				}
				elseif ($type==2) {
					$type='AND type=2';
				}	
		        $query = "SELECT * FROM 
(SELECT sum(ifnull(i.quantity,0)) as quantity1,sum(ifnull(i.approved_quantity,0)) as approved_quantity1,sum(i.quantity_received) as quantity_received1,sum(i.quantity_dispatched) as quantity_dispatched1,sum(i.quantity_dispatched)-sum(i.quantity_received) as mismatch_quantity,i.* FROM tbl_inventory i WHERE (i.Flag='I' OR i.flag='R' or i.flag='L') and(i.quantity_received is not null and i.quantity_dispatched is not NULL) and i.quantity_received < i.quantity_dispatched ".$sess_where." AND ".$itemname." ".$sess_wherep." and i.Entry_Date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."' GROUP BY i.batch_num,i.id_mst_drugs".$groupby." ORDER BY i.batch_num,i.id_mst_drugs,i.id_mstfacility) AS i
inner JOIN 
						  ( SELECT concat(f.City, '-', f.FacilityType) AS hospital,id_mstfacility from `mstfacility` f) AS f
						 		ON i.transfer_to=f.id_mstfacility
						 INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON i.id_mst_drugs=s.id_mst_drug_strength";

	///i.request_date BETWEEN '".$filter['Start_Date']."' AND '".$filter['End_Date']."'  AND i.request_date BETWEEN '".$date1."' AND '".$date2."' AND 

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}		
public function get_hcv_elisa_count(){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
		$filters1=$this->session->userdata('filters1');	
			
			/*$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";*/
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'  AND p.id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_where1 = " id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
				$sess_where1 = "AND id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
			}
			$query="";
			$query1="LEFT JOIN
						(SELECT gap_remark,id_mst_drugs FROM tbl_inventory_mismatch where ".$sess_where1.") AS mis
						on
						mis.id_mst_drugs=s.id_mst_drug_strength";
			if ($filter['item_type']=='') {
				$query= "(Select p.visits,s.*,i.*,2 as type,mis.gap_remark from ((SELECT count(case when (p.HCVRapidDate IS NOT NULL OR p.HCVRapidDate!='0000-00-00' ) then 1 ELSE 0 END) as visits,13 as id_mst_drug_strength FROM tblpatient p WHERE ((p.HCVRapidDate IS NOT NULL OR p.HCVRapidDate!='0000-00-00' ) || (p.HCVElisaDate IS NOT NULL OR p.HCVElisaDate=!'0000-00-00')) ".$sess_where." and (HCVRapidDate between '2020-02-01' and '".($filters1['End_Date'])."' || HCVElisaDate between '2020-02-01' and '".($filters1['End_Date'])."' )) UNION (SELECT count(case when (p.HCVElisaDate IS NOT NULL OR p.HCVElisaDate!='0000-00-00') then 1 ELSE 0 END) as visits, 14  as id_mst_drug_strength FROM tblpatient p WHERE ((p.HCVElisaDate IS NOT NULL OR p.HCVElisaDate=!'0000-00-00')) ".$sess_where." and (HCVElisaDate between '2020-02-01' and '".($filters1['End_Date'])."' || HCVElisaDate between '2020-02-01' and '".($filters1['End_Date'])."' ))) AS p
					INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
							ON p.id_mst_drug_strength=s.id_mst_drug_strength
							
							LEFT JOIN 
							(SELECT SUM(dispensed_quantity) AS dispensed_quantity,SUM(control_used) AS control_used,id_mst_drugs FROM tbl_inventory WHERE Flag='U' and from_Date between '2020-05-01' and '".($filters1['End_Date'])."' and to_Date between '2020-05-01' and '".($filters1['End_Date'])."') AS i
							ON s.id_mst_drug_strength=i.id_mst_drugs ".$query1." )
							 UNION ALL 
							(SELECT e.visits,s.*,i.*,1 as type,mis.gap_remark FROM 
							(SELECT p.PatientGUID FROM tblpatient p WHERE p.T_Initiation between '2020-05-01' and '".($filters1['End_Date'])."' ".$sess_where.") AS p
							LEFT JOIN 
							(SELECT (count(visit_no)) as visits,id_mst_drug_strength,id_mst_drugs,PatientGUID FROM tblpatient_regimen_drug_data GROUP BY id_mst_drug_strength,PatientGUID) AS e
							ON p.PatientGUID=e.PatientGUID
							INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON e.id_mst_drug_strength=s.id_mst_drug_strength
								LEFT JOIN 
							(SELECT SUM(dispensed_quantity) AS dispensed_quantity,SUM(control_used) AS control_used,id_mst_drugs FROM tbl_inventory WHERE Flag='U' and from_Date between '2020-05-01' and '".($filters1['End_Date'])."' and to_Date between '2020-05-01' and '".($filters1['End_Date'])."') AS i
							ON s.id_mst_drug_strength=i.id_mst_drugs ".$query1." )";
			}
			else if($filter['item_type']=='Kit'){
				$query= "(Select p.visits,s.*,i.*,2 as type,mis.gap_remark from ((SELECT count(case when (p.HCVRapidDate IS NOT NULL OR p.HCVRapidDate!='0000-00-00' ) then 1 ELSE 0 END) as visits,13 as id_mst_drug_strength FROM tblpatient p WHERE ((p.HCVRapidDate IS NOT NULL OR p.HCVRapidDate!='0000-00-00' ) || (p.HCVElisaDate IS NOT NULL OR p.HCVElisaDate=!'0000-00-00')) ".$sess_where." and (HCVRapidDate between '2020-02-01' and '".($filters1['End_Date'])."' || HCVElisaDate between '2020-02-01' and '".($filters1['End_Date'])."' )) UNION (SELECT count(case when (p.HCVElisaDate IS NOT NULL OR p.HCVElisaDate!='0000-00-00') then 1 ELSE 0 END) as visits, 14  as id_mst_drug_strength FROM tblpatient p WHERE ((p.HCVElisaDate IS NOT NULL OR p.HCVElisaDate=!'0000-00-00')) ".$sess_where." and (HCVElisaDate between '2020-02-01' and '".($filters1['End_Date'])."' || HCVElisaDate between '2020-02-01' and '".($filters1['End_Date'])."' ))) AS p
					INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
							ON p.id_mst_drug_strength=s.id_mst_drug_strength
							
							LEFT JOIN 
							(SELECT SUM(dispensed_quantity) AS dispensed_quantity,SUM(control_used) AS control_used,id_mst_drugs FROM tbl_inventory WHERE Flag='U' and from_Date between '2020-05-01' and '".($filters1['End_Date'])."' and to_Date between '2020-05-01' and '".($filters1['End_Date'])."') AS i
							ON s.id_mst_drug_strength=i.id_mst_drugs  ".$query1." )";
			}
			else if($filter['item_type']=='drug'){
				$query= "(SELECT e.visits,s.*,i.*,1 as type,mis.gap_remark FROM 
							(SELECT p.PatientGUID FROM tblpatient p WHERE p.T_Initiation between '2020-05-01' and '".($filters1['End_Date'])."' ".$sess_where.") AS p
							LEFT JOIN 
							(SELECT (count(visit_no)) as visits,id_mst_drug_strength,id_mst_drugs,PatientGUID FROM tblpatient_regimen_drug_data GROUP BY id_mst_drug_strength,PatientGUID) AS e
							ON p.PatientGUID=e.PatientGUID
							INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON e.id_mst_drug_strength=s.id_mst_drug_strength
								LEFT JOIN 
							(SELECT SUM(dispensed_quantity) AS dispensed_quantity,SUM(control_used) AS control_used,id_mst_drugs FROM tbl_inventory WHERE Flag='U' and from_Date between '2020-05-01' and '".($filters1['End_Date'])."' and to_Date between '2020-05-01' and '".($filters1['End_Date'])."') AS i
							ON s.id_mst_drug_strength=i.id_mst_drugs  ".$query1." )";
		
			}
			else{
				if ($filter['item_type']==13 || $filter['item_type']==14) {
					if (($filter['item_type']==13)) {
						$query= "(Select p.visits,s.*,i.*,2 as type,mis.gap_remark from ((SELECT count(case when (p.HCVRapidDate IS NOT NULL OR p.HCVRapidDate!='0000-00-00' ) then 1 ELSE 0 END) as visits,13 as id_mst_drug_strength FROM tblpatient p WHERE ((p.HCVRapidDate IS NOT NULL OR p.HCVRapidDate!='0000-00-00' ) || (p.HCVElisaDate IS NOT NULL OR p.HCVElisaDate=!'0000-00-00')) ".$sess_where." and (HCVRapidDate between '2020-02-01' and '".($filters1['End_Date'])."' || HCVElisaDate between '2020-02-01' and '".($filters1['End_Date'])."' ))) AS p
					INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
							ON p.id_mst_drug_strength=s.id_mst_drug_strength
							
							LEFT JOIN 
							(SELECT SUM(dispensed_quantity) AS dispensed_quantity,SUM(control_used) AS control_used,id_mst_drugs FROM tbl_inventory WHERE Flag='U' and from_Date between '2020-05-01' and '".($filters1['End_Date'])."' and to_Date between '2020-05-01' and '".($filters1['End_Date'])."') AS i
							ON s.id_mst_drug_strength=i.id_mst_drugs ".$query1." )";
					}
					if (($filter['item_type']==14)) {
						$query= "(Select p.visits,s.*,i.*,2 as type,mis.gap_remark from ((SELECT count(case when (p.HCVElisaDate IS NOT NULL OR p.HCVElisaDate!='0000-00-00') then 1 ELSE 0 END) as visits, 14  as id_mst_drug_strength FROM tblpatient p WHERE ((p.HCVElisaDate IS NOT NULL OR p.HCVElisaDate=!'0000-00-00')) ".$sess_where." and (HCVElisaDate between '2020-02-01' and '".($filters1['End_Date'])."' || HCVElisaDate between '2020-02-01' and '".($filters1['End_Date'])."' ))) AS p
					INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
							ON p.id_mst_drug_strength=s.id_mst_drug_strength
							
							LEFT JOIN 
							(SELECT SUM(dispensed_quantity) AS dispensed_quantity,SUM(control_used) AS control_used,id_mst_drugs FROM tbl_inventory WHERE Flag='U' and from_Date between '2020-05-01' and '".($filters1['End_Date'])."' and to_Date between '2020-05-01' and '".($filters1['End_Date'])."') AS i
							ON s.id_mst_drug_strength=i.id_mst_drugs ".$query1." )";
					}
				}
				else{
					$itemname= "id_mst_drug_strength = '".$filter['item_type']."'";
					$itemname1= "id_mst_drugs = '".$filter['item_type']."'";
					$query="(SELECT e.visits,s.*,i.*,1 as type,mis.gap_remark FROM 
							(SELECT p.PatientGUID FROM tblpatient p WHERE p.T_Initiation between '2020-05-01' and '".($filters1['End_Date'])."' ".$sess_where.") AS p
							LEFT JOIN 
							(SELECT (count(visit_no)) as visits,id_mst_drug_strength,id_mst_drugs,PatientGUID FROM tblpatient_regimen_drug_data where ".$itemname." GROUP BY id_mst_drug_strength,PatientGUID) AS e
							ON p.PatientGUID=e.PatientGUID
							INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON e.id_mst_drug_strength=s.id_mst_drug_strength
								LEFT JOIN 
							(SELECT SUM(dispensed_quantity) AS dispensed_quantity,SUM(control_used) AS control_used,id_mst_drugs FROM tbl_inventory WHERE Flag='U' and from_Date between '2020-05-01' and '".($filters1['End_Date'])."' and to_Date between '2020-05-01' and '".($filters1['End_Date'])."' and ".$itemname1.") AS i
							ON s.id_mst_drug_strength=i.id_mst_drugs  ".$query1." )";
				}
				
			}
		        /*$query = "";*/

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}		

public function get_info_monthly_record_state_wise($month=NULL,$year=NULL,$is_sum=NULL){

		$loginData = $this->session->userdata('loginData'); 
		$filter=$this->session->userdata('invfilter');
		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
		}

		if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "id_mst_drugs = '".$filter['item_type']."'";
			}
		$query ="SELECT * from 
		(select * from tbl_inventory_mismatch where month=? and year=? and ".$itemname." ".$sess_where.") as m
		INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON m.id_mst_drugs=s.id_mst_drug_strength
								inner JOIN 
						  ( SELECT concat(f.City, '-', f.FacilityType) AS hospital,id_mstfacility from `mstfacility` f) AS f
						 		ON m.id_mstfacility=f.id_mstfacility";

					     //print_r($query); die();

		$result = $this->db->query($query,[$month,$year])->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}	
	public function get_info_monthly_record($month=NULL,$year=NULL,$id_mst_drugs=NULL){

		$loginData = $this->session->userdata('loginData'); 

		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
		}

		echo $query ="select * from tbl_inventory_mismatch where month=? and year=? and id_mst_drugs=".$id_mst_drugs." ".$sess_where;

					     //print_r($query); die();

		$result = $this->db->query($query,[$month,$year])->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}	
	public function get_info_monthly_record_state_wise_sum($month=NULL,$year=NULL){

		$loginData = $this->session->userdata('loginData'); 
		$filter=$this->session->userdata('invfilter');
		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
		}

		if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "id_mst_drugs = '".$filter['item_type']."'";
			}
		$query ="select sum(mis) as mis,sum(utilize) as utilize,id_mst_drugs,id_mststate from tbl_inventory_mismatch where month=? and year=? and ".$itemname." ".$sess_where." group by id_mst_drugs,id_mststate";

					     //print_r($query); die();

		$result = $this->db->query($query,[$month,$year])->result();

		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}

	}	
public function get_stock_out_instance_facility(){
	$loginData = $this->session->userdata('loginData'); 
		
		$filter=$this->session->userdata('invfilter');	
		$filters1=$this->session->userdata('filters1');	
			
			/*$datearr=(explode("-",$filter['year']));
			$date1=$datearr[0]."-04-01";
			$date2=$datearr[1]."-03-31";*/
			//echo $date1."//".$date2;

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'  AND p.id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_where1 = " id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
				$sess_where1 = "AND id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' ";
			}
			if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "id_mst_drugs = '".$filter['item_type']."'";
			}
			 $query="SELECT COUNT(i.batch_num) as count,s.*,i.type,MAX(i.out_Date) as out_Date,mis.* from (SELECT id_mst_drugs,MAX(GREATEST(COALESCE((to_Date), 0),COALESCE((Entry_Date), 0),COALESCE((dispatch_date), 0),COALESCE((failure_date), 0))) AS out_Date,SUM(IFNULL((case when id_mstfacility='".$loginData->id_mstfacility."' then quantity_received ELSE 0 END ),0)-(ifnull(quantity_rejected,0)+ifnull(dispensed_quantity,0)+ifnull(control_used,0)+ifnull(returned_quantity,0)+ IFNULL((case when (transfer_to='".$loginData->id_mstfacility."' and Flag='I') then relocated_quantity when (id_mstfacility='".$loginData->id_mstfacility."' and Flag='L') then relocated_quantity ELSE 0 END),0))) as rem,type,batch_num,inventory_id from tbl_inventory where is_deleted ='0' and is_temp='0' AND  (id_mstfacility='".$loginData->id_mstfacility."' OR (transfer_to='".$loginData->id_mstfacility."' AND flag='L')) AND batch_num IS NOT null GROUP BY batch_num) as i
					INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON i.id_mst_drugs=s.id_mst_drug_strength
								LEFT JOIN
						(SELECT * FROM tbl_inventory_mismatch where ".$sess_where1.") AS mis
						on
						i.id_mst_drugs=mis.id_mst_drugs
								WHERE i.rem=0 and (i.out_Date) between '".($filters1['Start_Date'])."' and '".($filters1['End_Date'])."' GROUP BY i.id_mst_drugs";
			$query1="";
			
				
		        /*$query = "";*/

		$result1 = $this->db->query($query)->result();
		$result=is_array($result1) ? array($result1) :$result1;
		if(count($result) == 1)
		{
			return $result[0];
		}
		else
		{
			return $result;
		}		
}	
public function get_stock_out_state_wise($month=NULL,$year=NULL,$is_sum=NULL){

		$loginData = $this->session->userdata('loginData'); 
		$filter=$this->session->userdata('invfilter');
		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
		}

		if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "id_mst_drugs = '".$filter['item_type']."'";
			}
		$query ="SELECT * from 
		(select * from tbl_inventory_mismatch where month=? and year=? and (stockout_date is not null or stockout_date!='0000-00-00') and ".$itemname." ".$sess_where.") as m
		INNER JOIN 
						(Select id_mst_drug_strength,strength from mst_drug_strength) AS s
								ON m.id_mst_drugs=s.id_mst_drug_strength
								inner JOIN 
						  ( SELECT concat(f.City, '-', f.FacilityType) AS hospital,id_mstfacility from `mstfacility` f) AS f
						 		ON m.id_mstfacility=f.id_mstfacility";

					     //print_r($query); die();

		$result = $this->db->query($query,[$month,$year])->result();

		if(count($result) == 1)
		{
			//echo 'hii';
			return array($result[0]);
		}
		else
		{
			return $result;
		}

	}
	public function get_stock_out_state_wise_sum($month=NULL,$year=NULL){

		$loginData = $this->session->userdata('loginData'); 
		$filter=$this->session->userdata('invfilter');
		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND id_mststate = '".$loginData->State_ID."'  AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND id_mststate = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND id_mststate = '".$loginData->State_ID."' ";
		}

		if ($filter['item_type']=='') {
				$itemname= '1';
			}
			else if($filter['item_type']=='Kit'){
				$itemname= "type = '".$filter['item_mst_look'][1]->LookupCode."'";
			}
			else if($filter['item_type']=='drug'){
				$itemname= "type = '".$filter['item_mst_look'][0]->LookupCode."'";
			}
			else if($filter['item_type']=='drugkit'){
		 $itemname= "type IN (".$filter['item_mst_look'][0]->LookupCode.",".$filter['item_mst_look'][1]->LookupCode.")";
			}
			else{
				$itemname= "id_mst_drugs = '".$filter['item_type']."'";
			}
		$query ="select sum(out_count) as out_count,id_mst_drugs,id_mststate from tbl_inventory_mismatch where month=? and year=? and (stockout_date is not null or stockout_date!='0000-00-00') and ".$itemname." ".$sess_where." group by id_mst_drugs,id_mststate";

					     //print_r($query); die();

		$result = $this->db->query($query,[$month,$year])->result();

		if(count($result) == 1)
		{
			return array($result[0]);
		}
		else
		{
			return $result;
		}

	}			
}
