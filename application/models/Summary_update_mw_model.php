<?php  
class Summary_update_mw_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_Model');
		ini_set('memory_limit', '-1');
		set_time_limit(100000);
        ini_set('memory_limit', '100000M');
        ini_set('upload_max_filesize', '300000M');
        ini_set('post_max_size', '200000M');
        ini_set('max_input_time', 100000);
        ini_set('max_execution_time', 0);
        ini_set('memory_limit','-1');

		error_reporting(E_ALL);
	}
public function nullregupdate_regall(){

	$query = "UPDATE
					    tblsummary_new_monthwise
					SET
						`regimenwisedistribution_reg1` = null,
						`regimenwisedistribution_reg2` = null,
						`regimenwisedistribution_reg3` = null,
						`regimenwisedistribution_reg24` = null";

		$this->db->query($query);

}
	public function null_everything()
	{
		$query = "UPDATE
					    tblsummary_new_monthwise
					SET
						`anti_hcv_screened`  = NULL,
						`anti_hcv_positive`  = NULL,
						`viral_load_tested`  = NULL,
						`viral_load_detected`  = NULL,
						`initiatied_on_treatment`  = NULL,
						`treatment_completed`  = NULL,
						`treatment_failure` = NULL,
						`svr_done`  = NULL,
						`screened_for_hav`  = NULL,
						`hav_positive_patients`  = NULL,
						`patients_managed_at_facility`  = NULL,
						`patients_referred_for_management`  = NULL,
						`screened_for_hev`  = NULL,
						`treatment_successful` = NULL,
						`hev_positive_patients`  = NULL,
						`patients_managed_at_facility_hev`  = NULL,
						`patients_referred_for_management_hev`  = NULL,
						`non_cirrhotic`  = NULL,
						`compensated_cirrhotic`  = NULL,
						`decompensated_cirrhotic`  = NULL,
						`cirrhotic_status_notavaliable`= NULL,
						`reg_initiated_on_treatment`  = NULL,
						`distribution_reg1_SOF_DCV`  = NULL,
						`distribution_reg2_SOF_VEL`  = NULL,
						`distribution_reg3_SOF_VEL_Ribavirin`  = NULL,
						`distribution_reg4_SOF_VEL_24_weeks`  = NULL,
						`treatment_successful_svr_tested`  = NULL,
						`treatment_unsuccessful_svr_tested`  = NULL,
						`reg_wise_initiated_on_treatment`  = NULL,
						`reg_wise_reg1_SOF_DCV`  = NULL,
						`reg_wise_reg2_SOF_VEL`  = NULL,
						`reg_wise_reg3_SOF_VEL_Ribavirin`  = NULL,
						`reg_wise_reg4_SOF_VEL_24_weeks`  = NULL,
						`reg_wise_others` = NULL,
						follow_viral_load = NULL,
						follow_1St_dispensation = NULL,
						follow_2St_dispensation = NULL,
						follow_3St_dispensation = NULL,
						follow_4St_dispensation = NULL,
						follow_5St_dispensation = NULL,
						follow_6St_dispensation = NULL,
						follow_SVR = NULL,
						`loss_of_follow_viral_load`  = NULL,
						`loss_of_follow_1St_dispensation`  = NULL,
						`loss_of_follow_2St_dispensation`  = NULL,
						`loss_of_follow_3St_dispensation`  = NULL,
						`loss_of_follow_4St_dispensation`  = NULL,
						`loss_of_follow_5St_dispensation`  = NULL,
						`loss_of_follow_6St_dispensation`  = NULL,
						`loss_of_follow_SVR`  = NULL,
						`adherence_regimen_1`  = NULL,
						`adherence_regimen_2`  = NULL,
						`adherence_regimen_3`  = NULL,
						`adherence_regimen_4`  = NULL,
						`anty_hcv_test_to_anti_hcv_result`  = NULL,
						`anti_hcv_result_to_viral_load_test`  = NULL,
						`viral_load_test_to_delivery_of_vl_result_patient`  = NULL,
						`delivery_result_to_patient_prescription_base_line_test`  = NULL,
						`prescription_baseline_testing_date_baseline_tests`  = NULL,
						`date_of_baseline_tests_result_baseline_tests`  = NULL,
						`result_of_baseline_tests_to_initiation_of_treatment`  = NULL,
						`treatment_completion_to_SVR`  = NULL,
						`vl_samples_accepted`  = NULL,
						`vl_samples_rejected`  = NULL,
						`age_less_than_10`  = NULL,
						`age_10_to_20`  = NULL,
						`age_21_to_30`  = NULL,
						`age_31_to_40`  = NULL,
						`age_41_to_50`  = NULL,
						`age_51_to_60`  = NULL,
						`age_greater_than_60`  = NULL,
						`treatment_experienced_patients`  = NULL,
						`people_who_inject_drugs`  = NULL,
						`persons_with_chronic_kidney_disease`  = NULL,
						`persons_with_HIV_HCV_Co_infection`  = NULL,
						`persons_with_HBV_HCV_Co_infection`  = NULL,
						`persons_with_TB_HCV_Co_infection`  = NULL,
						`pregnant_women`  = NULL,
						`loss_to_follow_patient_referred_to_mtc` = NULL,
						`regimenwisedistribution_reg1` = null,
						`regimenwisedistribution_reg2` = null,
						`regimenwisedistribution_reg3` = null,
						`regimenwisedistribution_reg24` = null,
						`reg_pass1`  = NULL,
						`reg_fail1`  = NULL,
						`reg_pass2`  = NULL,
						`reg_fail2`  = NULL,
						`reg_pass3`  = NULL,
						`reg_fail3`  = NULL,
						`reg_pass24`  = NULL,
						`reg_fail24`  = NULL,
						`reg_fail_other` = null,
						`pregnant_women`  = NULL,
					    `anti_scv_screened_male` = NULL,
					    `anti_scv_screened_female` = NULL,
					    `anti_scv_screened_transgender`= NULL,
					    `vl_test_male` = NULL,
					    `vl_test_female` = NULL,
					    `vl_test_transgender` = NULL,
					    `treatment_initiations_male` = NULL,
					    `treatment_initiations_female` = NULL,
					    `treatment_initiations_transgender` = NULL,	
						`loss_to_follow_patient_reporting_at_mtc` = NULL,
						`cohort_based_vlpositive` = NULL,
						`cohort_based_T_Initiatied` = NULL,
						`cohort_based_treatment_completed` = NULL ";

		$this->db->query($query);
	}

	public function update_summary_all()
	{	
		
		$this->update_cascade_fields();

		$this->update_age_fields();
		
		
	}

// age functions

	public function update_age_fields()
	{
		$this->age_less_than_10();
		$this->age_10_to_20();
		$this->age_21_to_30();
		$this->age_31_to_40();
		$this->age_41_to_50();
		$this->age_51_to_60();
		$this->age_greater_than_60();

		echo "age, ";
	}

	public function age_less_than_10()
	{

			

		$query = "UPDATE
					    tblsummary_new_monthwise_monthwise s
					INNER JOIN(
					    SELECT
					    (
					         T_DLL_01_VLC_Date
					    
					) AS dt,
					COUNT(PatientGUID) AS Patients,
					id_mstfacility
					FROM
					    `tblpatient`
					WHERE
					     AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and T_DLL_01_VLC_Result=1 AND T_DLL_01_VLC_Date IS NOT NULL  AND T_DLL_01_VLC_Date!='0000-00-00' AND age < 10  
					GROUP BY
					    
					         YEAR(T_DLL_01_VLC_Date),
					          MONTH(T_DLL_01_VLC_Date),

					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.age_less_than_10 = a.Patients";

		$this->db->query($query);
	}

	public function age_10_to_20()
	{


				

		$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT
					        (
					        T_DLL_01_VLC_Date
					    
					) AS dt,
					        COUNT(PatientGUID) AS Patients,
					        id_mstfacility
					    FROM
					        `tblpatient`
					    WHERE
					        AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and T_DLL_01_VLC_Result=1 AND T_DLL_01_VLC_Date IS NOT NULL  AND T_DLL_01_VLC_Date!='0000-00-00' AND age >= 10 AND age < 20
					        GROUP BY
					    
					         YEAR(T_DLL_01_VLC_Date),
					          MONTH(T_DLL_01_VLC_Date),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.age_10_to_20 = a.Patients";

		$this->db->query($query);
	}

	public function age_21_to_30()
	{


				

		$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT
					        (
					         T_DLL_01_VLC_Date
					    
					) AS dt,
					        COUNT(PatientGUID) AS Patients,
					        id_mstfacility
					    FROM
					        `tblpatient`
					    WHERE
					        AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and T_DLL_01_VLC_Result=1 AND T_DLL_01_VLC_Date IS NOT NULL  AND T_DLL_01_VLC_Date!='0000-00-00' AND age >= 20 AND age < 30
					    GROUP BY
					    
					         YEAR(T_DLL_01_VLC_Date),
					          MONTH(T_DLL_01_VLC_Date),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.age_21_to_30 = a.Patients";

		$this->db->query($query);
	}

	public function age_31_to_40()
	{



		$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT
					        (
					        T_DLL_01_VLC_Date
					    
					) AS dt,
					        COUNT(PatientGUID) AS Patients,
					        id_mstfacility
					    FROM
					        `tblpatient`
					    WHERE
					        AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and T_DLL_01_VLC_Result=1 AND T_DLL_01_VLC_Date IS NOT NULL  AND T_DLL_01_VLC_Date!='0000-00-00' AND age >= 30 AND age < 40 
					    GROUP BY
					    
					         YEAR(T_DLL_01_VLC_Date),
					          MONTH(T_DLL_01_VLC_Date),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.age_31_to_40 = a.Patients";

		$this->db->query($query);
	}

	public function age_41_to_50()
	{



		$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT
					       (
					         T_DLL_01_VLC_Date
					) AS dt,
					        COUNT(PatientGUID) AS Patients,
					        id_mstfacility
					    FROM
					        `tblpatient`
					    WHERE
					       AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND T_DLL_01_VLC_Result=1 AND T_DLL_01_VLC_Date IS NOT NULL  AND T_DLL_01_VLC_Date!='0000-00-00' AND age >= 40 AND age < 50 
					    GROUP BY
					    
					         YEAR(T_DLL_01_VLC_Date),
					          MONTH(T_DLL_01_VLC_Date),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.age_41_to_50 = a.Patients";

		$this->db->query($query);
	}

	public function age_51_to_60()
	{



		$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT
					        (
					        T_DLL_01_VLC_Date
					) AS dt,
					        COUNT(PatientGUID) AS Patients,
					        id_mstfacility
					    FROM
					        `tblpatient`
					    WHERE
					        AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and T_DLL_01_VLC_Result=1 AND T_DLL_01_VLC_Date IS NOT NULL  AND T_DLL_01_VLC_Date!='0000-00-00' AND age >= 50 AND age < 60 
					    GROUP BY
					    
					         YEAR(T_DLL_01_VLC_Date),
					          MONTH(T_DLL_01_VLC_Date),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.age_51_to_60 = a.Patients";

		$this->db->query($query);
	}

	public function age_greater_than_60()
	{



		$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT
					        (
					         T_DLL_01_VLC_Date
					) AS dt,
					        COUNT(PatientGUID) AS Patients,
					        id_mstfacility
					    FROM
					        `tblpatient`
					    WHERE
					        AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and T_DLL_01_VLC_Result=1 AND T_DLL_01_VLC_Date IS NOT NULL  AND T_DLL_01_VLC_Date!='0000-00-00' AND age >= 60 
					   GROUP BY
					    
					         YEAR(T_DLL_01_VLC_Date),
					          MONTH(T_DLL_01_VLC_Date),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.age_greater_than_60 = a.Patients";

		$this->db->query($query);
	}

// cascade functions

	public function update_cascade_fields()
	{
				$this->Summary_update_mw_model->cascade_anti_hcv_screened();
				$this->Summary_update_mw_model->cascade_anti_hcv_positive();
				$this->Summary_update_mw_model->cascade_viral_load_tested();
				$this->Summary_update_mw_model->cascade_viral_load_detected();
				$this->Summary_update_mw_model->cascade_initiatied_on_treatment();
				$this->Summary_update_mw_model->cascade_treatment_completed();
				$this->Summary_update_mw_model->cascade_svr_done();
				$this->Summary_update_mw_model->cascade_treatment_successful();
				$this->Summary_update_mw_model->cascade_treatment_failure();

				$this->Summary_update_mw_model->cohort_based_vl();
				$this->Summary_update_mw_model->cohort_based_initiatied_on_treatment();
				$this->Summary_update_mw_model->cohort_based_treatment_completed();
				$this->Summary_update_mw_model->etr_update();

		echo "cascade, ";
	}
public function adherence_followup_analysis(){
		$this->Summary_update_mw_model->follow_up_analysis();
		$this->Summary_update_mw_model->follow_1St_dispensation();
		$this->Summary_update_mw_model->follow_2St_dispensation();
		$this->Summary_update_mw_model->follow_3St_dispensation();
		$this->Summary_update_mw_model->follow_4St_dispensation();
		$this->Summary_update_mw_model->follow_5St_dispensation();
		$this->Summary_update_mw_model->follow_6St_dispensation();
		$this->Summary_update_mw_model->follow_SVR();
}

public function lfu_adherence_followup_analysis(){

		$this->Summary_update_mw_model->lfu_follow_up_analysis();
		$this->Summary_update_mw_model->lfu_follow_1St_dispensation();
		$this->Summary_update_mw_model->lfu_follow_2St_dispensation();
		$this->Summary_update_mw_model->lfu_follow_3St_dispensation();
		$this->Summary_update_mw_model->lfu_follow_4St_dispensation();
		$this->Summary_update_mw_model->lfu_follow_5St_dispensation();
		$this->Summary_update_mw_model->lfu_follow_6St_dispensation();
		$this->Summary_update_mw_model->lfu_follow_SVR();

}
public function vl_samples_accepted_rejected(){

		$this->Summary_update_mw_model->samples_accepted_vl();
		$this->Summary_update_mw_model->samples_rejected_vl();

}
/*VL sample accept and reject*/
public function samples_accepted_vl(){

	$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					        count(*) AS vl_samples_accepted
					    FROM
					        tblpatient
					    WHERE
					        IsSampleAccepted = 1 and VLSampleCollectionDate is not null 
					    GROUP BY
					       YEAR(VLSampleCollectionDate),
					       MONTH(VLSampleCollectionDate),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.vl_samples_accepted = a.vl_samples_accepted";

		$this->db->query($query); 
}


public function samples_rejected_vl(){

	$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					        count(*) AS vl_samples_rejected
					    FROM
					        tblpatient
					    WHERE
					        IsSampleAccepted = 2 and VLSampleCollectionDate is not null 
					    GROUP BY
					       YEAR(VLSampleCollectionDate),
					       MONTH(VLSampleCollectionDate),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.vl_samples_rejected = a.vl_samples_rejected";

		$this->db->query($query); 
}

/*HCV ADHERENCE AND LOSS TO FOLLOW UP ANALYSIS*/
public function follow_up_analysis(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					             MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS follow_viral_load
					    FROM
					        tblpatient
					    WHERE
					       
					        AntiHCV = 1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					     GROUP BY
					       
								GREATEST(
								COALESCE(YEAR(HCVRapidDate), 0), 
								COALESCE(YEAR(HCVElisaDate), 0),  
								COALESCE(YEAR(HCVOtherDate), 0) 
								),
								GREATEST(
								COALESCE(MONTH(HCVRapidDate), 0), 
								COALESCE(MONTH(HCVElisaDate), 0),  
								COALESCE(MONTH(HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.follow_viral_load = a.follow_viral_load";

		$this->db->query($query); 
}

public function follow_1St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,

					        count(PatientGUID) AS follow_1St_dispensation
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' and T_DLL_01_VLC_Result=1
					    GROUP BY
					        year(T_Initiation),
MONTH(T_Initiation), 
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.follow_1St_dispensation = a.follow_1St_dispensation";

		$this->db->query($query); 
}

public function follow_2St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (

					        T_Initiation 
					           
					) AS dt,

					        count(PatientGUID) AS follow_2St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					       AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and T_Initiation is not null AND T_Initiation!='0000-00-00' and status in (4,18,20,24) 
					    GROUP BY
					      year(T_Initiation),
MONTH(T_Initiation), 
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.follow_2St_dispensation = a.follow_2St_dispensation";

		$this->db->query($query); 
}

public function follow_3St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) AS follow_3St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					     AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and T_Initiation is not null AND T_Initiation!='0000-00-00' and status in (5,6,21,25) 
					    GROUP BY
					     year(T_Initiation),
MONTH(T_Initiation), 
					       id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.follow_3St_dispensation = a.follow_3St_dispensation";

		$this->db->query($query); 
}

public function follow_4St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					          T_Initiation
					) AS dt,

					        count(PatientGUID) AS follow_4St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					    AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and T_Initiation is not null AND T_Initiation!='0000-00-00' and status in (8,22,27) 
					    GROUP BY
					      year(T_Initiation),
MONTH(T_Initiation), 
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.follow_4St_dispensation = a.follow_4St_dispensation";

		$this->db->query($query); 
}

public function follow_5St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					          T_Initiation
					) AS dt,

					        count(PatientGUID) AS follow_5St_dispensation
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation is not null AND T_Initiation!='0000-00-00' and status in (9,27) 
					    GROUP BY
					       year(T_Initiation),
MONTH(T_Initiation), 
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.follow_5St_dispensation = a.follow_5St_dispensation";

		$this->db->query($query); 
}

public function follow_6St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					          T_Initiation
					) AS dt,

					        count(*) AS follow_6St_dispensation
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation is not null AND T_Initiation!='0000-00-00' and status=10 
					    GROUP BY
					       year(T_Initiation),
MONTH(T_Initiation), 
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.follow_6St_dispensation = a.follow_6St_dispensation";

		$this->db->query($query); 
}

public function follow_SVR(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) AS follow_SVR
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  ETR_HCVViralLoad_Dt is not null AND T_Initiation is not null AND T_Initiation!='0000-00-00' and ETR_HCVViralLoad_Dt!='0000-00-00' and status = 13 
					    GROUP BY
					      year(ETR_HCVViralLoad_Dt),
					      month(ETR_HCVViralLoad_Dt),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.follow_SVR = a.follow_SVR";

		$this->db->query($query); 
}

/* LFU*/

public function lfu_follow_up_analysis(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					             MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(*) AS loss_of_follow_viral_lfu
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  (T_DLL_01_VLC_Date is null || T_DLL_01_VLC_Date='0000-00-00')
					        and  DATEDIFF (now(), 
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								) ) >7 

					    GROUP BY
					        
					       
								GREATEST(
								COALESCE(YEAR(HCVRapidDate), 0), 
								COALESCE(YEAR(HCVElisaDate), 0),  
								COALESCE(YEAR(HCVOtherDate), 0) 
								),
								GREATEST(
								COALESCE(MONTH(HCVRapidDate), 0), 
								COALESCE(MONTH(HCVElisaDate), 0),  
								COALESCE(MONTH(HCVOtherDate), 0) 
								)
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.loss_of_follow_viral_load = a.loss_of_follow_viral_lfu";

		$this->db->query($query); 
}

public function lfu_follow_1St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,

					        count(PatientGUID) AS loss_of_follow_1St_Count
					    FROM
					        tblpatient
					    WHERE
					       AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_DLL_01_VLC_Date is not null AND T_DLL_01_VLC_Date!='0000-00-00' and T_DLL_01_VLC_Result=1 and  DATEDIFF (now(), T_DLL_01_VLC_Date) >7 and (T_Initiation is null ||  T_Initiation='0000-00-00')  and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					        year(T_DLL_01_VLC_Date),
MONTH(T_DLL_01_VLC_Date), 
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.loss_of_follow_1St_dispensation = a.loss_of_follow_1St_Count";

		$this->db->query($query); 
}

public function lfu_follow_2St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation
					) AS dt,

					        count(PatientGUID) AS follow_2St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					    AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS not NULL AND T_Initiation!='0000-00-00' and status in (4,18,20,24)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      year(T_Initiation),
MONTH(T_Initiation), 
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.loss_of_follow_2St_dispensation = a.follow_2St_dispensation";

		$this->db->query($query); 
}

public function lfu_follow_3St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_3St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					      AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status in (5,6,21,25)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					     year(T_Initiation),
MONTH(T_Initiation), 
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.loss_of_follow_3St_dispensation = a.follow_3St_dispensation";

		$this->db->query($query); 
}

public function lfu_follow_4St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					          T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_4St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					      AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and   T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00'  and status in (8,22,27) and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					       year(T_Initiation),
MONTH(T_Initiation), 
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.loss_of_follow_4St_dispensation = a.follow_4St_dispensation";

		$this->db->query($query); 
}

public function lfu_follow_5St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_5St_dispensation
					    FROM
					        tblpatient 
					    WHERE
					       AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status in (9,27)  and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      year(T_Initiation),
MONTH(T_Initiation), 
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.loss_of_follow_5St_dispensation = a.follow_5St_dispensation";

		$this->db->query($query); 
}

public function lfu_follow_6St_dispensation(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					         T_Initiation 
					) AS dt,

					        count(PatientGUID) AS follow_6St_dispensation
					    FROM
					        tblpatient
					    WHERE
					      AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and  T_Initiation IS NOT NULL AND T_Initiation!='0000-00-00' and status =10 and DATEDIFF (NOW(),Next_Visitdt ) >30 and SVR_TreatmentStatus not in (7,1)
					    GROUP BY
					      year(T_Initiation),
							MONTH(T_Initiation), 
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.loss_of_follow_6St_dispensation = a.follow_6St_dispensation";

		$this->db->query($query); 
}

public function lfu_follow_SVR(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					   SELECT

					        id_mstfacility,
					        (
					           ETR_HCVViralLoad_Dt
					) AS dt,

					        count(*) AS follow_SVR,
					        datediff(
							NOW(),ETR_HCVViralLoad_Dt),
							SVR12W_HCVViralLoad_Dt
					    FROM
					        tblpatient
					    WHERE
					     AntiHCV=1  AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' AND ETR_HCVViralLoad_Dt!='1900-01-19' AND status NOT IN (14,15) and datediff(
							NOW(),ETR_HCVViralLoad_Dt) >84 AND (SVR12W_HCVViralLoad_Dt IS NULL || SVR12W_HCVViralLoad_Dt='0000-00-00') and SVR_TreatmentStatus not in (7,1)
				 
					    GROUP BY
					      YEAR(ETR_HCVViralLoad_Dt),
					      MONTH(ETR_HCVViralLoad_Dt),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.loss_of_follow_SVR = a.follow_SVR";

		$this->db->query($query); 
}
/*end lfu*/
/**/

/*Non Cirrhotic", "Compensated Cirrhotic","Decompensated Cirrhotic*/
public function update_cirrhotic(){

	$this->Summary_update_mw_model->non_cirrhotic();
	$this->Summary_update_mw_model->compensated_cirrhotic();
	$this->Summary_update_mw_model->decompensated_cirrhotic();
	$this->Summary_update_mw_model->cirrhotic_status_notavaliable();

}
public function non_cirrhotic(){

 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT
					    	p.PatientGUID,
					        p.id_mstfacility,
					        (
					            p.T_DLL_01_VLC_Date
					) AS dt,

					        count(p.PatientGUID) AS non_cirrhotic
					    FROM
					        tblpatient p left join tblpatientcirrohosis c on p.patientguid=c.PatientGUID
					    WHERE
					       p.V1_Cirrhosis = 2 AND AntiHCV = 1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00'
					    GROUP BY
					      	year(p.T_DLL_01_VLC_Date),
					      	MONTH(p.T_DLL_01_VLC_Date),
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.non_cirrhotic = a.non_cirrhotic";

		$this->db->query($query); 
}

public function compensated_cirrhotic(){

	$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        p.id_mstfacility,
					        (
					            p.T_DLL_01_VLC_Date
					) AS dt,

					        count(p.PatientGUID) AS compensated_cirrhotic
					    FROM
					       tblpatient p left join tblpatientcirrohosis c on p.patientguid=c.PatientGUID
					    WHERE
					        p.Result=1 AND AntiHCV = 1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00'
					    GROUP BY
					      year(p.T_DLL_01_VLC_Date),
					      	MONTH(p.T_DLL_01_VLC_Date),
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.compensated_cirrhotic = a.compensated_cirrhotic";

		$this->db->query($query); 
	
}
public function decompensated_cirrhotic(){

	$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        p.id_mstfacility,
					        (
					            p.T_DLL_01_VLC_Date
					) AS dt,

					        count(p.PatientGUID) AS decompensated_cirrhotic
					    FROM
					       tblpatient p left join tblpatientcirrohosis c on p.patientguid=c.PatientGUID
					    WHERE
					        p.Result=2 AND AntiHCV = 1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00'
					    GROUP BY
					      year(p.T_DLL_01_VLC_Date),
					      	MONTH(p.T_DLL_01_VLC_Date),
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.decompensated_cirrhotic = a.decompensated_cirrhotic";

		$this->db->query($query); 
}

public function cirrhotic_status_notavaliable(){

	$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        p.id_mstfacility,
					        (
					            p.T_DLL_01_VLC_Date
					) AS dt,

					        count(p.PatientGUID) AS cirrhotic_status_notavaliable
					    FROM
					       tblpatient p left join tblpatientcirrohosis c on p.patientguid=c.PatientGUID
					    WHERE
					        p.Result NOT IN(1,2) AND p.V1_Cirrhosis != 2 and V1_Cirrhosis IN(0,1) AND AntiHCV = 1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00'
					    GROUP BY
					      year(p.T_DLL_01_VLC_Date),
					      	MONTH(p.T_DLL_01_VLC_Date),
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.cirrhotic_status_notavaliable = a.cirrhotic_status_notavaliable";

		$this->db->query($query); 
}



/* Start Regimen Wise Distribution */
public function regimenwisedistribution_reg1(){

	$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					SELECT
					  id_mstfacility,
					        (
					            T_Initiation
					) AS dt,
					    count(PatientGUID) AS regimenwisedistribution_reg1
					FROM
					    `tblpatient`
					WHERE
					     AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND T_Initiation IS NOT NULL and T_Initiation!='0000-00-00' and T_Initiation!='1970-01-01' AND T_Regimen =1 AND T_DurationValue=12 
					GROUP BY
					id_mstfacility,
					    YEAR(T_Initiation),
					    MONTH(T_Initiation) ) a 
					    ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.regimenwisedistribution_reg1 = a.regimenwisedistribution_reg1";

					    	$this->db->query($query); 
}

public function regimenwisedistribution_reg2(){

	$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					SELECT
					  id_mstfacility,
					        (
					            T_Initiation
					) AS dt,
					    count(PatientGUID) AS regimenwisedistribution_reg2
					FROM
					    `tblpatient`
					WHERE
					    AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND  T_Initiation IS NOT NULL and T_Initiation!='0000-00-00' and T_Initiation!='1970-01-01' AND T_Regimen =2 AND T_DurationValue=12 
					GROUP BY
					id_mstfacility,
					     YEAR(T_Initiation),
					    MONTH(T_Initiation) ) a 
					    ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.regimenwisedistribution_reg2 = a.regimenwisedistribution_reg2";
					    	$this->db->query($query); 
}

public function regimenwisedistribution_reg3(){

	$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					SELECT
					  id_mstfacility,
					        (
					            T_Initiation
					) AS dt,
					    count(PatientGUID) AS regimenwisedistribution_reg3
					FROM
					    `tblpatient`
					WHERE
					    AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND  T_Initiation IS NOT NULL and T_Initiation!='0000-00-00' and T_Initiation!='1970-01-01' AND T_Regimen =3 AND T_DurationValue=12 
					GROUP BY
					id_mstfacility,
					     YEAR(T_Initiation),
					    MONTH(T_Initiation) ) a 
					    ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.regimenwisedistribution_reg3 = a.regimenwisedistribution_reg3";
					    	$this->db->query($query); 
}

public function regimenwisedistribution_reg24(){

	$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					SELECT
					  id_mstfacility,
					        (
					            T_Initiation
					) AS dt,
					    count(PatientGUID) AS regimenwisedistribution_reg24
					FROM
					    `tblpatient`
					WHERE
					   AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND  T_Initiation IS NOT NULL and T_Initiation!='0000-00-00' and T_Initiation!='1970-01-01' AND T_Regimen =2 AND T_DurationValue=24 
					GROUP BY
					id_mstfacility,
					     YEAR(T_Initiation),
					    MONTH(T_Initiation) ) a 
					    ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.regimenwisedistribution_reg24 = a.regimenwisedistribution_reg24";
					    	$this->db->query($query); 
}

/*End  Regimen Wise Distribution */
/*Start Regimen-Wise Success Rate of Treatment (SVR Tested) */
public function regimen_treatment_succe_failreg1()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					   SELECT
					  id_mstfacility,
					        (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,
					    
					    sum(case when SVR12W_Result=2 then 1 else 0 end)  AS pass,sum(case when SVR12W_Result=1 then 1 else 0 end)  AS fail
					FROM
					    tblpatient
					WHERE
					    T_Regimen =1 and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00'
					GROUP BY
					id_mstfacility,
					    YEAR(SVR12W_HCVViralLoad_Dt),
					    MONTH(SVR12W_HCVViralLoad_Dt)
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.reg_pass1 = a.pass,s.reg_fail1= a.fail";

		$this->db->query($query);
	}

public function regimen_treatment_succe_failreg2()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					   SELECT
					  id_mstfacility,
					        (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,
					    
					    sum(case when SVR12W_Result=2 then 1 else 0 end)  AS pass,sum(case when SVR12W_Result=1 then 1 else 0 end)  AS fail
					FROM
					    tblpatient
					WHERE
					   T_Regimen =2 and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00'
					GROUP BY
					id_mstfacility,
					    YEAR(SVR12W_HCVViralLoad_Dt),
					    MONTH(SVR12W_HCVViralLoad_Dt)
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.reg_pass2 = a.pass,s.reg_fail2= a.fail";

		$this->db->query($query);
	}

public function regimen_treatment_succe_failreg3()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					   SELECT
					  id_mstfacility,
					        (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,
					    
					    sum(case when SVR12W_Result=2 then 1 else 0 end)  AS pass,sum(case when SVR12W_Result=1 then 1 else 0 end)  AS fail
					FROM
					    tblpatient
					WHERE
					    T_Regimen =3 and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00'
					GROUP BY
					id_mstfacility,
					    YEAR(SVR12W_HCVViralLoad_Dt),
					    MONTH(SVR12W_HCVViralLoad_Dt)
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.reg_pass3 = a.pass,s.reg_fail3= a.fail";

		$this->db->query($query);
	}
//other regimin

	public function regimen_treatment_succe_other()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					   SELECT
					  id_mstfacility,
					        (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,
					    
					    sum(case when SVR12W_Result=2 then 1 else 0 end)  AS pass,sum(case when SVR12W_Result=1 then 1 else 0 end)  AS fail
					FROM
					    tblpatient
					WHERE
					    T_Regimen NOT IN(1,2,3)  and SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00'
					GROUP BY
					id_mstfacility,
					     YEAR(SVR12W_HCVViralLoad_Dt),
					    MONTH(SVR12W_HCVViralLoad_Dt)
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.reg_sucess_other = a.pass,s.reg_fail_other= a.fail";

		$this->db->query($query);
	}
	// end
public function regimen_treatment_succe_failreg24(){

	$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					SELECT
					  id_mstfacility,
					        (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,
					     sum(case when SVR12W_Result=2 then 1 else 0 end)  AS pass,sum(case when SVR12W_Result=1 then 1 else 0 end)  AS fail
					FROM
					    `tblpatient`
					WHERE
					    T_Initiation IS NOT NULL and T_Initiation!='0000-00-00'  AND T_DurationValue=24 and SVR12W_HCVViralLoad_Dt is not null  and SVR12W_HCVViralLoad_Dt!='0000-00-00'
					GROUP BY
					id_mstfacility,
					     YEAR(SVR12W_HCVViralLoad_Dt),
					    MONTH(SVR12W_HCVViralLoad_Dt)) a 
					    ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.reg_pass24 = a.pass,s.reg_fail24= a.fail";
					    	$this->db->query($query); 
}

/*end Regimen-Wise Success Rate of Treatment (SVR Tested)*/

	public function cascade_anti_hcv_screened()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS anti_hcv_screened
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00') 
					    GROUP BY
					       
								GREATEST(
								COALESCE(YEAR(HCVRapidDate), 0), 
								COALESCE(YEAR(HCVElisaDate), 0),  
								COALESCE(YEAR(HCVOtherDate), 0) 
								),
								GREATEST(
								COALESCE(MONTH(HCVRapidDate), 0), 
								COALESCE(MONTH(HCVElisaDate), 0),  
								COALESCE(MONTH(HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.anti_hcv_screened = a.anti_hcv_screened  ";

		$this->db->query($query); 


	}
//and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')


	public function cascade_anti_hcv_positive()
	{

			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT
					       
					          id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					      count(patientguid)  as HCVRapidResult
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV = 1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00')
					    GROUP BY
					       
								GREATEST(
								COALESCE(YEAR(HCVRapidDate), 0), 
								COALESCE(YEAR(HCVElisaDate), 0),  
								COALESCE(YEAR(HCVOtherDate), 0) 
								),
								GREATEST(
								COALESCE(MONTH(HCVRapidDate), 0), 
								COALESCE(MONTH(HCVElisaDate), 0),  
								COALESCE(MONTH(HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.anti_hcv_positive = a.HCVRapidResult ";

		$this->db->query($query);
	}

	public function cascade_viral_load_tested()
	{

			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       count(PatientGUID) as VLSampleCollectionCount
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        year(VLSampleCollectionDate),
					        month(VLSampleCollectionDate),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.viral_load_tested = a.VLSampleCollectionCount   ";

		$this->db->query($query);
	}

	public function cascade_viral_load_detected()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";



		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT
					        id_mstfacility,
					        (
					            T_DLL_01_VLC_Date
					) AS dt,
					count(*) AS T_DLL_01_VLC_Result
					FROM
					    tblpatient
					WHERE
					   AntiHCV = 1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Result=1 and T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' 
					GROUP BY
					    
					        year(T_DLL_01_VLC_Date),
					        MONTH(T_DLL_01_VLC_Date),
					
					id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.viral_load_detected = a.T_DLL_01_VLC_Result ";

		$this->db->query($query);
	}

	public function cascade_initiatied_on_treatment()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as COUNT
					    FROM
					        tblpatient
					    WHERE
					        T_Initiation!='0000-00-00' and T_Initiation is not null and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  
					    GROUP BY
					       year(T_Initiation),
							MONTH(T_Initiation), 
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.initiatied_on_treatment = a.COUNT";

		$this->db->query($query);
	}

	public function cascade_treatment_completed()
	{

			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as treatment_completedcount
					    FROM
					        tblpatient
					    WHERE
					        ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       YEAR(ETR_HCVViralLoad_Dt),
					       MONTH(ETR_HCVViralLoad_Dt),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.treatment_completed = a.treatment_completedcount  ";

		$this->db->query($query);
	}

	public function cascade_svr_done()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as SVRDrawndatacount
					    FROM
					        tblpatient
					    WHERE
					        SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' AND Status IN (14,15) and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       year(SVR12W_HCVViralLoad_Dt),
					       MONTH(SVR12W_HCVViralLoad_Dt),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.svr_done = a.SVRDrawndatacount ";

		$this->db->query($query);
	}

	public function cascade_treatment_successful()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,

					        COUNT(PatientGUID) as SVR12W_Result
					    FROM
					        tblpatient
					    WHERE
					        SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 and SVR12W_Result=2 AND Status IN (14,15) AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) 
					    GROUP BY
					       year(SVR12W_HCVViralLoad_Dt),
					       MONTH(SVR12W_HCVViralLoad_Dt),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.treatment_successful = a.SVR12W_Result ";

		$this->db->query($query);
	}

public function cascade_treatment_failure()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            SVR12W_HCVViralLoad_Dt
					) AS dt,

					        COUNT(PatientGUID) as SVR12W_Result
					    FROM
					        tblpatient
					    WHERE
					        SVR12W_HCVViralLoad_Dt is not null and SVR12W_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1) AND Status IN (14,15) and SVR12W_Result=1 
					    GROUP BY
					       year(SVR12W_HCVViralLoad_Dt),
					       MONTH(SVR12W_HCVViralLoad_Dt),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.treatment_failure = a.SVR12W_Result ";

		$this->db->query($query);
	}

public function cascade_hav_patients(){

	$this->Summary_update_mw_model->cascade_anti_hav_screened();
	$this->Summary_update_mw_model->cascade_hav_positive_patients();
	$this->Summary_update_mw_model->cascade_hav_managed_at_facility();
	$this->Summary_update_mw_model->cascade_hav_referred_for_management();

}

public function cascade_hev_patients(){

	$this->Summary_update_mw_model->cascade_anti_hev_screened();
	$this->Summary_update_mw_model->cascade_hev_positive_patients();
	$this->Summary_update_mw_model->cascade_hev_managed_at_facility();
	$this->Summary_update_mw_model->cascade_hev_referred_for_management();

}
public function regimenwisedistribution_regall(){
	$this->Summary_update_mw_model->regimenwisedistribution_reg1();
	$this->Summary_update_mw_model->regimenwisedistribution_reg2();
	$this->Summary_update_mw_model->regimenwisedistribution_reg3();
	$this->Summary_update_mw_model->regimenwisedistribution_reg24();

	$this->Summary_update_mw_model->regimen_treatment_succe_failreg1();
	$this->Summary_update_mw_model->regimen_treatment_succe_failreg2();
	$this->Summary_update_mw_model->regimen_treatment_succe_failreg3();
	$this->Summary_update_mw_model->regimen_treatment_succe_failreg24();
	$this->Summary_update_mw_model->regimen_treatment_succe_other();
	

	$this->Summary_update_mw_model->genderwise_anti_hcv_screened();
	$this->Summary_update_mw_model->genderwise_viral_load_tested();
	$this->Summary_update_mw_model->genderwise_initiatied_on_treatment();
	
}

/*HAV */
public function cascade_anti_hav_screened()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HAVRapidDate), 0), 
								COALESCE((HAVElisaDate), 0),  
								COALESCE((HAVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS screened_hav_screened
					    FROM
					        tblpatient
					    WHERE
					        LgmAntiHAV=1 
					    GROUP BY
					    
								GREATEST(
								COALESCE(YEAR(HAVRapidDate), 0), 
								COALESCE(YEAR(HAVElisaDate), 0),  
								COALESCE(YEAR(HAVOtherDate), 0) 
								),
								GREATEST(
								COALESCE(MONTH(HAVRapidDate), 0), 
								COALESCE(MONTH(HAVElisaDate), 0),  
								COALESCE(MONTH(HAVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.screened_for_hav = a.screened_hav_screened";

		$this->db->query($query); 


	}

	public function cascade_hav_positive_patients()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HAVRapidDate), 0), 
								COALESCE((HAVElisaDate), 0),  
								COALESCE((HAVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS hav_positive_patients
					    FROM
					        tblpatient
					    WHERE
					        HAVRapidResult=1 OR HAVElisaResult=1 OR HAVOtherResult=1 
					    GROUP BY
					    
								GREATEST(
								COALESCE(YEAR(HAVRapidDate), 0), 
								COALESCE(YEAR(HAVElisaDate), 0),  
								COALESCE(YEAR(HAVOtherDate), 0) 
								),
								GREATEST(
								COALESCE(MONTH(HAVRapidDate), 0), 
								COALESCE(MONTH(HAVElisaDate), 0),  
								COALESCE(MONTH(HAVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.hav_positive_patients = a.hav_positive_patients";

		$this->db->query($query); 


	}

public function cascade_hav_managed_at_facility()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HAVRapidDate), 0), 
								COALESCE((HAVElisaDate), 0),  
								COALESCE((HAVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS patients_managed_at_facility
					    FROM
					        tblpatient
					    WHERE
					       (HAVRapidResult=1 OR HAVElisaResult=1 OR HAVOtherResult=1) and Refer_FacilityHAV=1
					    GROUP BY
					    
								GREATEST(
								COALESCE(YEAR(HAVRapidDate), 0), 
								COALESCE(YEAR(HAVElisaDate), 0),  
								COALESCE(YEAR(HAVOtherDate), 0) 
								),
								GREATEST(
								COALESCE(MONTH(HAVRapidDate), 0), 
								COALESCE(MONTH(HAVElisaDate), 0),  
								COALESCE(MONTH(HAVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.patients_managed_at_facility = a.patients_managed_at_facility";

		$this->db->query($query); 


	}

public function cascade_hav_referred_for_management()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HAVRapidDate), 0), 
								COALESCE((HAVElisaDate), 0),  
								COALESCE((HAVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS patients_referred_for_management
					    FROM
					        tblpatient
					    WHERE
					       (HAVRapidResult=1 OR HAVElisaResult=1 OR HAVOtherResult=1) and Refer_HigherFacilityHAV=2
					    GROUP BY
					    
								GREATEST(
								COALESCE(YEAR(HAVRapidDate), 0), 
								COALESCE(YEAR(HAVElisaDate), 0),  
								COALESCE(YEAR(HAVOtherDate), 0) 
								),
								GREATEST(
								COALESCE(MONTH(HAVRapidDate), 0), 
								COALESCE(MONTH(HAVElisaDate), 0),  
								COALESCE(MONTH(HAVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.patients_referred_for_management = a.patients_referred_for_management";

		$this->db->query($query); 


	}

/*HEV PAtient*/

public function cascade_anti_hev_screened()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HEVRapidDate), 0), 
								COALESCE((HEVElisaDate), 0),  
								COALESCE((HEVOtherDate), 0) 
								))
					) AS dt,

					        count(PatientGUID) AS screened_hev_screened
					    FROM
					        tblpatient
					    WHERE
					        LgmAntiHEV=1 
					    GROUP BY
					    
							
								GREATEST(
								COALESCE(YEAR(HEVRapidDate), 0), 
								COALESCE(YEAR(HEVElisaDate), 0),  
								COALESCE(YEAR(HEVOtherDate), 0) 
								),
								GREATEST(
								COALESCE(MONTH(HEVRapidDate), 0), 
								COALESCE(MONTH(HEVElisaDate), 0),  
								COALESCE(MONTH(HEVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.screened_for_hev = a.screened_hev_screened";

		$this->db->query($query); 


	}

	public function cascade_hev_positive_patients()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HEVRapidDate), 0), 
								COALESCE((HEVElisaDate), 0),  
								COALESCE((HEVOtherDate), 0) 
								))
					) AS dt,

					        count(*) AS hev_positive_patients
					    FROM
					        tblpatient
					    WHERE
					       LgmAntiHEV=1 and  HEVRapidResult=1 OR HEVElisaResult=1 OR HEVOtherResult=1 
					    GROUP BY
					    
								GREATEST(
								COALESCE(YEAR(HEVRapidDate), 0), 
								COALESCE(YEAR(HEVElisaDate), 0),  
								COALESCE(YEAR(HEVOtherDate), 0) 
								),
								GREATEST(
								COALESCE(MONTH(HEVRapidDate), 0), 
								COALESCE(MONTH(HEVElisaDate), 0),  
								COALESCE(MONTH(HEVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.hev_positive_patients = a.hev_positive_patients";

		$this->db->query($query); 


	}

public function cascade_hev_managed_at_facility()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HEVRapidDate), 0), 
								COALESCE((HEVElisaDate), 0),  
								COALESCE((HEVOtherDate), 0) 
								))
					) AS dt,

					        count(*) AS patients_managed_at_facilityhev
					    FROM
					        tblpatient
					    WHERE
					       (HEVRapidResult=1 OR HEVElisaResult=1 OR HEVOtherResult=1) and Refer_FacilityHEV=1
					    GROUP BY
					    
								GREATEST(
								COALESCE(YEAR(HEVRapidDate), 0), 
								COALESCE(YEAR(HEVElisaDate), 0),  
								COALESCE(YEAR(HEVOtherDate), 0) 
								),
								GREATEST(
								COALESCE(MONTH(HEVRapidDate), 0), 
								COALESCE(MONTH(HEVElisaDate), 0),  
								COALESCE(MONTH(HEVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.patients_managed_at_facility_hev = a.patients_managed_at_facilityhev";

		$this->db->query($query); 


	}

public function cascade_hev_referred_for_management()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					           MAX(
								GREATEST(
								COALESCE((HEVRapidDate), 0), 
								COALESCE((HEVElisaDate), 0),  
								COALESCE((HEVOtherDate), 0) 
								))
					) AS dt,

					        count(*) AS patients_referred_for_management_hev
					    FROM
					        tblpatient
					    WHERE
					       (HEVRapidResult=1 OR HEVElisaResult=1 OR HEVOtherResult=1) and Refer_HigherFacilityHEV=2
					    GROUP BY
					    
								GREATEST(
								COALESCE(YEAR(HEVRapidDate), 0), 
								COALESCE(YEAR(HEVElisaDate), 0),  
								COALESCE(YEAR(HEVOtherDate), 0) 
								),
								GREATEST(
								COALESCE(MONTH(HEVRapidDate), 0), 
								COALESCE(MONTH(HEVElisaDate), 0),  
								COALESCE(MONTH(HEVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.patients_referred_for_management_hev = a.patients_referred_for_management_hev";

		$this->db->query($query); 


	}

/*End HEV */
	

	public function get_missed_days()
	{
		$query = "select * from mstappstateconfiguration";
		$result = $this->db->query($query)->result();

		return $result;
	}

// loss to follow up functions

	

	public function get_lfu_days()
	{
		$query    = "SELECT * FROM mstappstateconfiguration";
		$result   = $this->db->query($query)->result();
		return $result[0];
	}



public function update_patient_guid(){

				$query = "UPDATE
					    core_diagnostics_raw_import s
					INNER JOIN(
					    select 
					    p.PatientGUID,c.institution_name from core_diagnostics_raw_import c 
					    inner join mstfacility m inner join
					   tblpatient p on
             c.institution_name=m.facility_name and c.uid=p.UID_Num 
            and m.id_mstfacility=p.id_mstfacility
					) a
					ON
					    s.institution_name = a.institution_name
					SET
					    s.PatientGUID = a.PatientGUID";
					  $this->db->query($query);
}

public function update_all_core_data(){

	$updatestr = "UPDATE
    `core_diagnostics_raw_import` AS `dest`,
    (
        SELECT
            *
        FROM
            `core_diagnostics_raw_import`
        WHERE
            uid like '%/%'
    ) AS `src`
SET
    
   dest.uid = REPLACE(dest.uid,'/','-')";

$this->db->query($updatestr);
	$this->update_srv_core_data();
	//$this->confirmatory_svr_date();
	//$this->genotype_svr_date();

}

public function update_all_core_dataor(){
	$updatestr = "UPDATE
    `core_diagnostics_raw_import` AS `dest`,
    (
        SELECT
            *
        FROM
            `core_diagnostics_raw_import`
        WHERE
            uid like '%/%'
    ) AS `src`
SET
    
   dest.uid = REPLACE(dest.uid,'/','-')";
   
$this->db->query($updatestr);

	$this->update_srv_core_dataor();
	$this->confirmatory_svr_dateor();
	$this->genotype_svr_dateor();

}

public function save_svrdata(){

	$sql_add_new_patientguids = "insert into tblpatientSVR (PatientGUID)
    SELECT p.PatientGUID FROM 
	 tblpatient p 
	left join tblpatientSVR s on p.patientguid = s.PatientGUID where s.PatientGUID is null";
    $this->Common_Model->update_data_sql($sql_add_new_patientguids);

}
public function update_srv_core_data(){

	$query = "UPDATE
					    tblpatientSVR s
					INNER JOIN(SELECT 
    p.patientguid as patientguid,
    c.report_date AS svr_date,
    CASE c.result
    WHEN 'Not-Detected' THEN 2
    ELSE 1
    END AS result,
    c.quant_result AS viralload
    
    FROM
    core_diagnostics_raw_import c
    INNER JOIN
    mstfacility m
	 INNER JOIN
	 tblpatient p on
	c.institution_name=m.facility_name 
	and c.uid=p.UID_Num 
	and m.id_mstfacility=p.id_mstfacility

    WHERE
    c.test_name= 'HCV Viral RNA Quantitative' and
    c.svr = 'YES' AND
   c.result  IN ('Not-Detected' ,'Detected') ) AS a on s.PatientGUID=a.patientguid
					SET
					    s.SVRDate = a.svr_date ,
					    s.SVRResult = a.result,
					    s.SVRVLC = a.viralload,
					    s.svr_from = 'C'";
					  $this->db->query($query);

}


public function confirmatory_svr_date(){

	$query = "UPDATE
					    tblpatientSVR s
					INNER JOIN(SELECT 
    p.patientguid as patientguid,
    c.report_date AS svr_date,
    CASE c.result
    WHEN 'Not-Detected' THEN 2
    ELSE 1
    END AS result,
    c.quant_result AS viralload
    
    FROM
   core_diagnostics_raw_import c
    INNER JOIN
    mstfacility m
	 INNER JOIN
	 tblpatient p on
	c.institution_name=m.facility_name 
	and c.uid=p.UID_Num
	and m.id_mstfacility=p.id_mstfacility

    WHERE
    c.svr = 'NO' AND
    c.test_name= 'HCV Viral RNA Quantitative' and
   c.result  IN ('Not-Detected' ,'Detected') ) AS a on s.PatientGUID=a.patientguid
					SET
								s.confirmatory_test_date       = a.svr_date ,
								s.confirmatory_test_result     = a.result,
								s.confirmatory_test_viral_load = a.viralload,
								s.confirmatory_from            = 'C' ";
					  $this->db->query($query);

}

public function genotype_svr_date(){

	$query = "UPDATE
					    tblpatientSVR s
					INNER JOIN(SELECT 
    p.patientguid as patientguid,
    c.report_date AS svr_date,
    CASE c.quant_result
    WHEN 'Genotype 1' THEN 1
    WHEN 'Genotype 2' THEN 2
    WHEN 'Genotype 3' THEN 3
    WHEN 'Genotype 4' THEN 4
    WHEN 'Genotype 5' THEN 5
    WHEN 'Genotype 6' THEN 6
	WHEN 'Genotype 1b' THEN 1
	WHEN 'Genotype 1a' THEN 1
    
    END AS result,
    c.quant_result AS viralload
    
    FROM
    core_diagnostics_raw_import c
    INNER JOIN
    mstfacility m
	 INNER JOIN
	 tblpatient p on
	c.institution_name=m.facility_name 
	and c.uid=p.UID_Num
	and m.id_mstfacility=p.id_mstfacility

    WHERE
   
    c.test_name ='HCV Genotype' AND
   c.result  IN ('Not-Detected' ,'Detected') ) AS a  on s.PatientGUID=a.patientguid
					SET
								s.genotype_test_date   = a.svr_date ,
								s.genotype_test_result = a.result,
								s.genotype_from        = 'C' ";

					  $this->db->query($query);

}


public function update_srv_core_dataor(){

	$query = "UPDATE
					    tblpatientSVR s
					INNER JOIN(SELECT 
    p.patientguid as patientguid,
    c.report_date AS svr_date,
    CASE c.result
    WHEN 'Not-Detected' THEN 2
    ELSE 1
    END AS result,
    c.quant_result AS viralload
    
    FROM
    core_diagnostics_raw_import c
    INNER JOIN
    mstfacility m
	 INNER JOIN
	 tblpatient p on
	c.institution_name=m.facility_name 
	and  c.uid = CONCAT(p.`UID_Prefix` ,'-', lpad(p.`UID_Num`, 6, 0))
	and m.id_mstfacility=p.id_mstfacility

    WHERE
    c.test_name= 'HCV Viral RNA Quantitative' and
    c.svr = 'YES' AND
   c.result  IN ('Not-Detected' ,'Detected') ) AS a on s.PatientGUID=a.patientguid
					SET
					    s.SVRDate = a.svr_date ,
					    s.SVRResult = a.result,
					    s.SVRVLC = a.viralload,
					    s.svr_from = 'C'";
					  $this->db->query($query);

}


public function confirmatory_svr_dateor(){

	$query = "UPDATE
					    tblpatientSVR s
					INNER JOIN(SELECT 
    p.patientguid as patientguid,
    c.report_date AS svr_date,
    CASE c.result
    WHEN 'Not-Detected' THEN 2
    ELSE 1
    END AS result,
    c.quant_result AS viralload
    
    FROM
   core_diagnostics_raw_import c
    INNER JOIN
    mstfacility m
	 INNER JOIN
	 tblpatient p on
	c.institution_name=m.facility_name 
	and c.uid = CONCAT(p.`UID_Prefix` ,'-', lpad(p.`UID_Num`, 6, 0))
	and m.id_mstfacility=p.id_mstfacility

    WHERE
    c.svr = 'NO' AND
    c.test_name= 'HCV Viral RNA Quantitative' and
   c.result  IN ('Not-Detected' ,'Detected') ) AS a on s.PatientGUID=a.patientguid
					SET
								s.confirmatory_test_date       = a.svr_date ,
								s.confirmatory_test_result     = a.result,
								s.confirmatory_test_viral_load = a.viralload,
								s.confirmatory_from            = 'C' ";
					  $this->db->query($query);

}

public function genotype_svr_dateor(){

	$query = "UPDATE
					    tblpatientSVR s
					INNER JOIN(SELECT 
    p.patientguid as patientguid,
    c.report_date AS svr_date,
    CASE c.quant_result
    WHEN 'Genotype 1' THEN 1
    WHEN 'Genotype 2' THEN 2
    WHEN 'Genotype 3' THEN 3
    WHEN 'Genotype 4' THEN 4
    WHEN 'Genotype 5' THEN 5
    WHEN 'Genotype 6' THEN 6
	WHEN 'Genotype 1b' THEN 1
	WHEN 'Genotype 1a' THEN 1
    
    END AS result,
    c.quant_result AS viralload
    
    FROM
    core_diagnostics_raw_import c
    INNER JOIN
    mstfacility m
	 INNER JOIN
	 tblpatient p on
	c.institution_name=m.facility_name 
	and  c.uid = CONCAT(p.`UID_Prefix` ,'-', lpad(p.`UID_Num`, 6, 0))
	and m.id_mstfacility=p.id_mstfacility

    WHERE
   
    c.test_name ='HCV Genotype' AND
   c.result  IN ('Not-Detected' ,'Detected') ) AS a  on s.PatientGUID=a.patientguid
					SET
								s.genotype_test_date   = a.svr_date ,
								s.genotype_test_result = a.result,
								s.genotype_from        = 'C' ";

					  $this->db->query($query);

}


/*Loss to Follow Up for Special Cases*/
public function update_lossup_spcase(){
	$this->loss_to_followupspcase_refmtc();
	$this->loss_to_followupspcase_inicate();
}
public function loss_to_followupspcase_refmtc()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        count(PatientGUID) as IsReferal
					    FROM
					        tblpatient
					    WHERE
					         IsReferal=1 and  ReferalDate is not null and ReferalDate!='0000-00-00'
					    GROUP BY
					       YEAR(ReferalDate),
					       MONTH(ReferalDate),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.loss_to_follow_patient_referred_to_mtc = a.IsReferal ";

		$this->db->query($query);
	}


public function loss_to_followupspcase_inicate()
	{

				$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";
				$where2 = " Session_StateID = '".$loginData->State_ID."'";
	


		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					          T_Initiation 
					) AS dt,

					        count(PatientGUID) as IsReferal1
					    FROM
					        tblpatient
					    WHERE
					         IsReferal=1 and T_Initiation is not null and ReferalDate!='0000-00-00'
					    GROUP BY
					       YEAR(ReferalDate),
					       MONTH(ReferalDate),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.loss_to_follow_patient_reporting_at_mtc = a.IsReferal1 ";

		$this->db->query($query);
	}
	

/* end */	
/*update draw Adherence State*/
public function Adherencesummarytest(){

	$this->Summary_update_mw_model->adherref_one();
	$this->Summary_update_mw_model->adherref_two();
	$this->Summary_update_mw_model->adherref_three();
	$this->Summary_update_mw_model->adherref_three24();
	$this->Summary_update_mw_model->adherref_others();

}
public function adherref_one(){
	$loginData = $this->session->userdata('loginData'); 
				

		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(

					SELECT p.id_mstfacility ,
					 (
					            T_Initiation
					) AS dt,
					avg(v.Adherence) as Countval from tblpatient p inner join tblpatientvisit v on p.PatientGUID=v.PatientGUID

					    WHERE
					        v.Adherence>0  and T_Regimen=1
					        group by   year(p.T_Initiation),
MONTH(p.T_Initiation), 
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.reg_wise_reg1_SOF_DCV = a.Countval ";

		$this->db->query($query);
}

public function adherref_two(){
	$loginData = $this->session->userdata('loginData'); 
				

		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(

					SELECT p.id_mstfacility ,
					 (
					            T_Initiation
					) AS dt,
					avg(v.Adherence)as Countval from tblpatient p inner join tblpatientvisit v on p.PatientGUID=v.PatientGUID

					    WHERE
					        v.Adherence>0  and T_Regimen=2
					        group by   year(p.T_Initiation),
MONTH(p.T_Initiation), 
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.reg_wise_reg2_SOF_VEL = a.Countval ";

		$this->db->query($query);
}


public function adherref_three(){
	$loginData = $this->session->userdata('loginData'); 
				

		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(

					SELECT p.id_mstfacility ,
					 (
					            T_Initiation
					) AS dt,
					avg(v.Adherence)as Countval from tblpatient p inner join tblpatientvisit v on p.PatientGUID=v.PatientGUID

					    WHERE
					        v.Adherence>0  and T_Regimen=3
					        group by   year(p.T_Initiation),
MONTH(p.T_Initiation), 
					        p.id_mstfacility
					   
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.reg_wise_reg3_SOF_VEL_Ribavirin = a.Countval ";

		$this->db->query($query);
}

public function adherref_three24(){

	$loginData = $this->session->userdata('loginData'); 
				

		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(

					SELECT p.id_mstfacility ,
					 (
					            T_Initiation
					) AS dt,
					avg(v.Adherence) as Countval from tblpatient p inner join tblpatientvisit v on p.PatientGUID=v.PatientGUID

					    WHERE
					         v.Adherence>0 AND  T_DurationValue=24 and T_Initiation is not null and T_Initiation!='0000-00-00'
					        group by   year(p.T_Initiation),
MONTH(p.T_Initiation), 
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.reg_wise_reg4_SOF_VEL_24_weeks = a.Countval ";

		$this->db->query($query);
}

public function adherref_others(){
	$loginData = $this->session->userdata('loginData'); 
				

		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(

					SELECT p.id_mstfacility ,
					 (
					            T_Initiation
					) AS dt,
					avg(v.Adherence) as Countval from tblpatient p inner join tblpatientvisit v on p.PatientGUID=v.PatientGUID

					    WHERE
					         v.Adherence>0  AND T_DurationValue!=24 AND T_Regimen NOT IN(1,2,3) and p.T_Initiation is not null and T_Initiation!='0000-00-00'
					        group by   year(p.T_Initiation),
MONTH(p.T_Initiation), 
					        p.id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.reg_wise_others = a.Countval ";

		$this->db->query($query);
}


/*End update draw Adherence State*/

public function timearounddata_summaryupdate(){

	$this->Summary_update_mw_model->anti_hcv_result_to_viral_load_test();
	$this->Summary_update_mw_model->viral_load_test_to_delivery_of_vl_result_patient();
	$this->Summary_update_mw_model->delivery_result_to_patient_prescription_base_line_test();
	$this->Summary_update_mw_model->prescription_baseline_testing_date_baseline_tests();
	$this->Summary_update_mw_model->date_of_baseline_tests_result_baseline_tests();
	$this->Summary_update_mw_model->result_of_baseline_tests_to_initiation_of_treatment();
	$this->Summary_update_mw_model->treatment_completion_to_SVR();
	$this->Summary_update_mw_model->hepatitis_c_adata();

}

/*Turn-Around Time Analysis update summary table start*/
public function anti_hcv_result_to_viral_load_test(){

	 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(

					select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select id_mstfacility,patientguid,T_Initiation,datediff(p.T_DLL_01_VLC_Date,
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								)) as dt from tblpatient p where p.T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00' and (HCVRapidDate is not null || HCVElisaDate is not null  || HCVOtherDate is not null ) AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' and datediff(p.T_DLL_01_VLC_Date,
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								)) >0 )  a
								group by id_mstfacility,
								YEAR(T_Initiation), MONTH(T_Initiation))aa
					ON
					    s.id_mstfacility = aa.id_mstfacility AND year(s.date )= year(aa.dtval) and month(s.date )= month(aa.dtval)
					SET
					    s.anti_hcv_result_to_viral_load_test = aa.Count ";

					    $this->db->query($query);
}

public function viral_load_test_to_delivery_of_vl_result_patient(){

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(

					select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select id_mstfacility,patientguid,T_Initiation,datediff(p.T_DLL_01_VLC_Date,
							VLSampleCollectionDate) as dt from tblpatient p where p.T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Date!='0000-00-00'  AND T_Initiation IS NOT  NULL AND T_Initiation!='0000-00-00' and datediff(p.T_DLL_01_VLC_Date,
							VLSampleCollectionDate) >0 )  a
								group by id_mstfacility,
								YEAR(T_Initiation), MONTH(T_Initiation)) aa
					ON
					    s.id_mstfacility = aa.id_mstfacility AND year(s.date )= year(aa.dtval) and month(s.date )= month(aa.dtval)
					SET
					    s.viral_load_test_to_delivery_of_vl_result_patient = aa.Count ";
					    $this->db->query($query);
}


public function delivery_result_to_patient_prescription_base_line_test(){

 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(

					select id_mstfacility,T_Initiation AS dtval, FORMAT (avg(dt),0 ) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,c.Prescribing_Dt,datediff(c.Prescribing_Dt,
							p.T_DLL_01_VLC_Date) as dt from tblpatient p LEFT JOIN tblpatientcirrohosis c on p.PatientGUID=c.PatientGUID  where p.T_DLL_01_VLC_Date is not null and p.T_DLL_01_VLC_Date!='0000-00-00' AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00' and datediff(c.Prescribing_Dt,
							p.T_DLL_01_VLC_Date) >0 )  a
								group by id_mstfacility,
								YEAR(T_Initiation), MONTH(T_Initiation)  ) aa
					ON
					    s.id_mstfacility = aa.id_mstfacility AND year(s.date )= year(aa.dtval) and month(s.date )= month(aa.dtval)
					SET
					    s.delivery_result_to_patient_prescription_base_line_test = aa.Count ";
					    $this->db->query($query);
}


public function prescription_baseline_testing_date_baseline_tests(){

	$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(

					select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,c.Prescribing_Dt,p.PrescribingDate,datediff(p.PrescribingDate,
							c.Prescribing_Dt) as dt from tblpatient p LEFT JOIN tblpatientcirrohosis c on p.PatientGUID=c.PatientGUID  where p.PrescribingDate is not null and p.PrescribingDate!='0000-00-00' AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00' and datediff(p.PrescribingDate,
							c.Prescribing_Dt) >0 )  a
								group by id_mstfacility,
								YEAR(T_Initiation), MONTH(T_Initiation)  ) aa
					ON
					    s.id_mstfacility = aa.id_mstfacility AND year(s.date )= year(aa.dtval) and month(s.date )= month(aa.dtval)
					SET
					    s.prescription_baseline_testing_date_baseline_tests = aa.Count ";
					    $this->db->query($query);

}

public function date_of_baseline_tests_result_baseline_tests(){

	$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(

					select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,c.Prescribing_Dt,p.PrescribingDate,datediff(p.PrescribingDate,
							c.Prescribing_Dt) as dt from tblpatient p LEFT JOIN tblpatientcirrohosis c on p.PatientGUID=c.PatientGUID  where p.PrescribingDate is not null and p.PrescribingDate!='0000-00-00' AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00' and datediff(p.PrescribingDate,
							c.Prescribing_Dt) >0 )  a
								group by id_mstfacility,
								YEAR(T_Initiation), MONTH(T_Initiation) ) aa
					ON
					    s.id_mstfacility = aa.id_mstfacility AND year(s.date )= year(aa.dtval) and month(s.date )= month(aa.dtval)
					SET
					    s.date_of_baseline_tests_result_baseline_tests = aa.Count ";
					    $this->db->query($query);
}


public function result_of_baseline_tests_to_initiation_of_treatment(){

	$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(

					select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,c.Prescribing_Dt,p.PrescribingDate,datediff(p.T_Initiation,
							p.PrescribingDate) as dt from tblpatient p LEFT JOIN tblpatientcirrohosis c on p.PatientGUID=c.PatientGUID  where p.PrescribingDate is not null and p.PrescribingDate!='0000-00-00' AND c.Prescribing_Dt IS NOT NULL AND c.Prescribing_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00' and datediff(T_Initiation,
							p.PrescribingDate) >0 )  a
								group by id_mstfacility,
								YEAR(T_Initiation), MONTH(T_Initiation) ) aa
					ON
					    s.id_mstfacility = aa.id_mstfacility AND year(s.date )= year(aa.dtval) and month(s.date )= month(aa.dtval)
					SET
					    s.result_of_baseline_tests_to_initiation_of_treatment = aa.Count ";
					    $this->db->query($query);
}

public function treatment_completion_to_SVR(){

	$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(

					select id_mstfacility,T_Initiation AS dtval, avg(dt) as Count from (
select p.id_mstfacility,p.patientguid,p.T_Initiation,p.ETR_HCVViralLoad_Dt,p.PrescribingDate,datediff(p.SVR12W_HCVViralLoad_Dt,
							p.ETR_HCVViralLoad_Dt) as dt from tblpatient p  where p.ETR_HCVViralLoad_Dt is not null and p.ETR_HCVViralLoad_Dt!='0000-00-00' AND p.T_Initiation IS NOT  NULL AND p.T_Initiation!='0000-00-00' and datediff(SVR12W_HCVViralLoad_Dt,
							p.ETR_HCVViralLoad_Dt) >0 )  a
								group by id_mstfacility,YEAR(T_Initiation), MONTH(T_Initiation)  ) aa
					ON
					    s.id_mstfacility = aa.id_mstfacility AND year(s.date )= year(aa.dtval) and month(s.date )= month(aa.dtval)
					SET
					    s.treatment_completion_to_SVR = aa.Count ";
					    $this->db->query($query);
}

/*end Turn-Around Time Analysis*/


public function hepatitis_c_adata(){

				 /* $sql = "select count(*) as count from tblpatient p where p.T_DLL_01_VLC_Date is not null ".$this->facility_state ." ".$this->facility_district." ".$this->facility_filter." and ".$this->T_DLL_01_VLC_Date." ";
				$result = $this->db->query($sql)->result();
				return $result;*/

					


$query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					         T_DLL_01_VLC_Date 
					) AS dt,

					        count(PatientGUID) AS serological_tests
					    FROM
					        tblpatient
					    WHERE
					      T_DLL_01_VLC_Date IS NOT NULL
					    GROUP BY
					      YEAR(T_DLL_01_VLC_Date),
					      MONTH(T_DLL_01_VLC_Date),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.serological_tests = a.serological_tests";

		$this->db->query($query); 

		}		

/*Gender wise anti_hcv_screened */

public function genderwise_anti_hcv_screened()
	{
		
			$loginData = $this->session->userdata('loginData'); 
				$where = "AND Session_StateID = '".$loginData->State_ID."'";
				$where1 = " s.Session_StateID = '".$loginData->State_ID."'";

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT

					        id_mstfacility,
					        (
					            MAX(
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								))
					) AS dt,


					         sum(case when Gender=1 then 1 else 0 end)  AS anti_male,sum(case when Gender=2 then 1 else 0 end)  AS anti_female, sum(case when Gender = 3 then 1 else 0 end) AS anti_transgender 
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 and (HCVRapidDate is not null || HCVElisaDate is not null || HCVOtherDate is not null) and (HCVRapidDate!='0000-00-00' || HCVElisaDate!='0000-00-00' || HCVOtherDate!='0000-00-00') 
					    GROUP BY
					       
								GREATEST(
								COALESCE(YEAR(HCVRapidDate), 0), 
								COALESCE(YEAR(HCVElisaDate), 0),  
								COALESCE(YEAR(HCVOtherDate), 0) 
								),
								GREATEST(
								COALESCE(MONTH(HCVRapidDate), 0), 
								COALESCE(MONTH(HCVElisaDate), 0),  
								COALESCE(MONTH(HCVOtherDate), 0) 
								),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.anti_scv_screened_male = a.anti_male , s.anti_scv_screened_female = a.anti_female , s.anti_scv_screened_transgender = a.anti_transgender  ";

		$this->db->query($query); 


	}


public function genderwise_viral_load_tested()
	{

			

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT
					       
					         id_mstfacility,
					        (
					            VLSampleCollectionDate
					) AS dt,

					       sum(case when Gender=1 then 1 else 0 end)  AS anti_male,sum(case when Gender=2 then 1 else 0 end)  AS anti_female, sum(case when Gender = 3 then 1 else 0 end) AS anti_transgender 
					    FROM
					        tblpatient
					    WHERE
					         AntiHCV = 1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and VLSampleCollectionDate IS NOT NULL AND VLSampleCollectionDate!='0000-00-00' 
					    GROUP BY
					        YEAR(VLSampleCollectionDate),
					         MONTH(VLSampleCollectionDate),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.vl_test_male = a.anti_male,s.vl_test_female = a.anti_female,s.vl_test_transgender = a.anti_transgender   ";

		$this->db->query($query);
	}


public function genderwise_initiatied_on_treatment()
	{

			

		  $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            T_Initiation
					) AS dt,

					        sum(case when Gender=1 then 1 else 0 end)  AS anti_male,sum(case when Gender=2 then 1 else 0 end)  AS anti_female, sum(case when Gender = 3 then 1 else 0 end) AS anti_transgender 
					    FROM
					        tblpatient
					    WHERE
					        AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_Initiation!='0000-00-00' and T_Initiation is not null  and T_Initiation!='1970-01-01'
					    GROUP BY
					       year(T_Initiation),
MONTH(T_Initiation), 
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.treatment_initiations_male = a.anti_male,s.treatment_initiations_female = a.anti_female,s.treatment_initiations_transgender = a.anti_transgender";

		$this->db->query($query);
	}
/*Cohort-based progress on patients/persons benefitted (HCV) report VL*/

public function cohort_based_vl(){


					$query = "UPDATE
					tblsummary_new_monthwise s
					INNER JOIN(
					SELECT id_mstfacility,
					
					(
					T_DLL_01_VLC_Date
					) AS dt,
					
					count(patientguid) as cohort_based_vlpositive
					FROM
					tblpatient 
					WHERE
					AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_DLL_01_VLC_Date is not null  and T_DLL_01_VLC_Date!='1970-01-01' and T_DLL_01_VLC_Date!='0000-00-00' and (left(
					            
								GREATEST(
								COALESCE((HCVRapidDate), 0), 
								COALESCE((HCVElisaDate), 0),  
								COALESCE((HCVOtherDate), 0) 
								)
					,7)) = left(T_DLL_01_VLC_Date,7)

					GROUP BY
					YEAR(T_DLL_01_VLC_Date),
					MONTH(T_DLL_01_VLC_Date),
					id_mstfacility
					) a
					ON
					s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					s.cohort_based_vlpositive = a.cohort_based_vlpositive";

$this->db->query($query);

}


public function cohort_based_initiatied_on_treatment(){


					$query = "UPDATE
					tblsummary_new_monthwise s
					INNER JOIN(
					SELECT id_mstfacility,
					
					(
					T_Initiation
					) AS dt,
					
					count(patientguid) as cohort_based_T_Initiatied
					FROM
					tblpatient 
					WHERE
					AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and T_Initiation!='0000-00-00' and T_Initiation is not null  and T_DLL_01_VLC_Date is not null  and T_DLL_01_VLC_Date!='1970-01-01' and T_DLL_01_VLC_Date!='0000-00-00' and  T_Initiation!='1970-01-01'  and ( left(T_DLL_01_VLC_Date,7)  = left(T_Initiation,7) )
					GROUP BY
					YEAR(T_Initiation),
					MONTH(T_Initiation),
					id_mstfacility
					) a
					ON
					s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					s.cohort_based_T_Initiatied = a.cohort_based_T_Initiatied";

$this->db->query($query);

}


public function cohort_based_treatment_completed()
	{

			

		 $query = "UPDATE
					    tblsummary_new_monthwise s
					INNER JOIN(
					    SELECT id_mstfacility,
					       
					         (
					            ETR_HCVViralLoad_Dt
					) AS dt,

					        count(PatientGUID) as cohort_based_treatment_completed
					    FROM
					        tblpatient
					    WHERE
					        ETR_HCVViralLoad_Dt is not null and ETR_HCVViralLoad_Dt!='0000-00-00' and AntiHCV=1 AND (HCVRapidResult=1 || HCVElisaResult=1 || HCVOtherResult = 1)  and ( left(T_Initiation,7)  = left(ETR_HCVViralLoad_Dt,7) )
					    GROUP BY
					       YEAR(ETR_HCVViralLoad_Dt),
					       MONTH(ETR_HCVViralLoad_Dt),
					        id_mstfacility
					) a
					ON
					    s.id_mstfacility = a.id_mstfacility AND year(s.date )= year(a.dt) and month(s.date )= month(a.dt)
					SET
					    s.cohort_based_treatment_completed = a.cohort_based_treatment_completed  ";

		$this->db->query($query);
	}

public function adherencepercentagedata(){
	
}





public function etr_update(){

	/*$query="UPDATE tblpatient a INNER JOIN ( SELECT p.`Status`,p.PatientGUID,p.next_visitdt,v.mx,p.ETR_HCVViralLoad_Dt,DATEDIFF(p.Next_Visitdt,
v.mx)
FROM tblpatient p
INNER JOIN (
SELECT patientguid, MAX(Visit_Dt) mx
FROM tblpatientvisit
GROUP BY patientguid) v ON p.patientguid=v.patientguid
WHERE p.`Status` IN (7,11,17,23,28) AND  ETR_HCVViralLoad_Dt='1900-01-19'  AND DATEDIFF(p.Next_Visitdt,
v.mx) >30 ) s ON s.PatientGUID=a.PatientGUID
SET a.ETR_HCVViralLoad_Dt=s.next_visitdt,a.`Status`= 13,Adherence=100,AdvisedSVRDate=DATE_ADD(s.Next_Visitdt,INTERVAL+84 day)";*/

$query="UPDATE tblpatient a INNER JOIN ( SELECT p.`Status`,p.PatientGUID,p.next_visitdt,v.mx,p.ETR_HCVViralLoad_Dt,DATEDIFF(p.Next_Visitdt,
v.mx)
FROM tblpatient p
INNER JOIN (
SELECT patientguid, MAX(Visit_Dt) mx
FROM tblpatientvisit
GROUP BY patientguid) v ON p.patientguid=v.patientguid
WHERE(p.`Status` IN (7,11,17,23,28) )  AND NOW()>(p.Next_Visitdt) AND (ETR_HCVViralLoad_Dt IS null || ETR_HCVViralLoad_Dt='0000-00-00') ) s ON s.PatientGUID=a.PatientGUID
SET a.ETR_HCVViralLoad_Dt=s.next_visitdt,a.`Status`= 13,Adherence=100,AdvisedSVRDate=DATE_ADD(s.Next_Visitdt,INTERVAL+84 day)";


$this->db->query($query);
}

}