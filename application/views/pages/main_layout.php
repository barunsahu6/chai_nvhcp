<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('pages/includes/header'); ?>
<?php $this->load->view('pages/includes/menubar'); ?>
<?php $this->load->view('pages/components/'.$subview); ?>

<!-- multipurpose modal -->
<style type="text/css">
 <style>
  .dataTables_wrapper .dataTables_paginate .paginate_button {
    
    padding: 0px 0px 0px 1px;
   
    text-decoration: none !important; 
    cursor: pointer; 
 
}

  .model-lg{
    width: 87% !important;
  }
.active{
  background-color: #fff;
}
.pagination>.active>a:hover{
background-color: #337ab7;
color: #fff;
}
.dataTables_filter input { width: 50px; }
  a{
    cursor: pointer;
  }
  .dataTables_filter {
display: none;
}
    #bottom_note,#info_text{
      padding-left: 15%;
      padding-right: 15%;
    }
    #dialog-confirm{
        display: none;
    }
    .ui-widget-overlay {
    background: #868686;
    opacity: .4;
    filter: Alpha(Opacity=50);
}
.ui-dialog .ui-dialog-buttonpane button {
    padding: 10px;
}
.ui-dialog .ui-dialog-titlebar-close {
   display: none;
}
option{
        background: #f3f3f3;
        color:#000;
    }
   #modal_header{
    white-space: pre-wrap;
   }
   .table-bordered>tbody>tr>th{
    vertical-align: top;
   text-align: center;
  }
  .left_nav_bar{
 padding-top: 0;
 padding-left: 7px;
 padding-right: 0px
 padding-bottom:0px;
  }
  dl dt{
    display: inline-block;
    width: 20px;
    height: 20px;
    vertical-align: middle;
}
dl dd{
    display: inline-block;
    margin: 0px 10px;
    padding-bottom: 0;
    vertical-align: middle;
}
dl dt.red,.red_status{
    background: #e9635e;
}
dl dt.green,.green_status{
    background: #87c830;
}
dl dt.yellow,.yellow_status{
    background: #ffd400;
}
dl dt.purple,.purple_status{
    background: #b19cd9;
}
dl dt.orange,.orange_status{
    background: #fe7e0f;
}
dl dt.darkYellow_status,.darkYellow_status{
    background: #ef9b0f;
}
dl dt.manual,.manual{
    background: #2460A7FF;
}
dl dt.auto,.auto{
    background: #643E46FF;
}
dl dt.auto_pending,.auto_pending{
    background: #643E46FF;
}
input: required: invalid {
    outline: none;
    border: none;
}
.item{
    width:auto;
    text-align: left;
  }
  @media (min-width: 768px) and (max-width: 1400px) {
  .set_margin{
    width: 83.1%;
    }
    

}
  @media (min-width: 1920px ) {
   
    .left_nav_bar{
      width: 12.666667%;
    }
.set_margin{
    width: 86.5%;
    }
}


</style>
<?php $error = $this->session->flashdata('error'); ?>
<div class="modal" data-easein="bounceIn" tabindex="-1" role="dialog" id="multipurpose_modal">
	<div class="modal-dialog" role="document" style="position: relative; top : 30vh;">
		<div class="modal-content">
			<div class="row">
				<!-- <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-xs-8 col-xs-offset-2"> -->
				<div class="col-lg-12">
					<div class="modal-header">
						<button class="btn btn-warning btn-block " style="color: white;padding-bottom: 15px;padding-top: 15px;" href="" data-dismiss="modal" aria-label="Close" id="modal_header"><?php echo isset($error)?$error['header']:'Information'; ?></button>

					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12 text-center">
								<i id="modal_text"><?php echo isset($error)?$error['message']:'Error Text'; ?></i>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<script>
	$(".modal").each(function(l){$(this).on("show.bs.modal",function(l){var o=$(this).attr("data-easein");"shake"==o?$(".modal-dialog").velocity("callout."+o):"pulse"==o?$(".modal-dialog").velocity("callout."+o):"tada"==o?$(".modal-dialog").velocity("callout."+o):"flash"==o?$(".modal-dialog").velocity("callout."+o):"bounce"==o?$(".modal-dialog").velocity("callout."+o):"swing"==o?$(".modal-dialog").velocity("callout."+o):$(".modal-dialog").velocity("transition."+o)})});

	$(document).ready(function(){
    $(document).keydown(function(event) {
        if (event.ctrlKey==true && (event.which == '118' || event.which == '86')) {
            //alert('thou. shalt. not. PASTE!');
            event.preventDefault();
         }
    });
});

	$(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
   
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
      var startval=$('#startdate').val();
        var endval=$('#enddate').val();
         $('#startdate,#enddate').change(function() {
    var startdate = $('#startdate').datepicker('getDate');
    var enddate = $('#enddate').datepicker('getDate');
    if ( startdate > enddate && startdate!=null && enddate!=null) { 
      $("#modal_header").text("From Date cannot be before To Date");
      $("#modal_text").text("Please check Dates");
      $(this).val('');
      if ($('#startdate').val()=='') {
        $('#startdate').val(startval);
      }
      else if ($('#enddate').val()=='') {
        $('#enddate').val(endval);
      }
      $("#multipurpose_modal").modal("show");
      return false;
    }
    else{
      return true;
    }
  });
});

</script>
<script type="text/javascript">
	$('.hasCal').each(function() {
        ddate=$(this).val();
        yRange="1990:"+new Date().getFullYear();
        yr=$(this).attr('year-range');
        if(yr){
            yRange=yr;
        }
        
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
            changeMonth: true,
            yearRange: yRange,
            maxDate: 0,
        });
        $(this).attr('placeholder','dd-mm-yy');
        if($(this).attr('noweekend')){
            $(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
        }
        $(this).datepicker( "setDate", ddate);
    });



     $('.hasCalreport').each(function() {
        ddate=$(this).val();
        yRange="2018:"+new Date().getFullYear();
        yr=$(this).attr('year-range');
        if(yr){
            yRange=yr;
        }
        
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
            changeMonth: true,
            yearRange: yRange,
            maxDate: 0,
        });
        $(this).attr('placeholder','dd-mm-yy');
        if($(this).attr('noweekend')){
            $(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
        }
        $(this).datepicker( "setDate", ddate);
    });

     $('.hasCaldash').each(function() {
        ddate=$(this).val();
        yRange="2000:"+new Date().getFullYear();
        yr=$(this).attr('year-range');
        if(yr){
            yRange=yr;
        }
        
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
            changeMonth: true,
            yearRange: yRange,
            maxDate: 0,
        });
        $(this).attr('placeholder','dd-mm-yy');
        if($(this).attr('noweekend')){
            $(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
        }
        $(this).datepicker( "setDate", ddate);
    });

     
</script>
<?php $this->load->view('pages/includes/footer'); ?>