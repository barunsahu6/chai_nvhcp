<?php 
$loginData = $this->session->userdata('loginData');

$sql = "SELECT Dispense FROM `MSTRole` where RoleId = ".$loginData->RoleId;
$result = $this->db->query($sql)->result();

?>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #A30A0C !important;
	background-color: white !important;
	border : 1px solid #A30A0C;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
</style>

<br>
<?php //echo '<pre>'; print_r($visit_details); ?>
<div class="row equal">
	<!-- <form action="" name="patient_form" id="patient_form" method="POST"> -->
				        <?php
           $attributes = array(
               'id' => 'patient_form',
               'name' => 'patient_form',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>
           <input type="hidden" name="fetch_uid" id="fetch_uid">

        <?php echo form_close(); ?>
		<!-- <input type="hidden" name="fetch_uid" id="fetch_uid"> -->
	<!-- </form> -->
	<div class="col-lg-10 col-lg-offset-1">

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="btn-group">
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">1. Registration</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">2. Screening</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">3. Viral Load</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">4. Testing</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/known_history/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">5. Known History</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">6. Prescription</a>
					<a class="btn btn-success" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">7. Dispensation</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">8. SVR</a>
				</div>
			</div>
		</div>

		<!-- <form action="" method="POST" name="registration" id="registration"> -->
								        <?php
           $attributes = array(
               'id' => 'registration',
               'name' => 'registration',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>

			<div class="row">
				<div class="col-md-12">
					<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Patient Dispensation Module</h3>
				</div>
			</div>
			<br>
			
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="btn-group">
						<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo $patient_data[0]->PatientGUID; ?>/1">1<sup>st</sup> Rx</a>
						<a class="btn <?php echo ($visit == '2')?'btn-success':'btn-default'; ?>" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo $patient_data[0]->PatientGUID; ?>/2">2<sup>nd</sup> Rx</a>
						<a class="btn <?php echo ($visit == '3')?'btn-success':'btn-default'; ?>" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo $patient_data[0]->PatientGUID; ?>/3">3<sup>rd</sup> Rx</a>
						<?php if(count($patient_data) > 0 and $patient_data[0]->T_DurationValue == 24){  ?>
							<a class="btn <?php echo ($visit == '4')?'btn-success':'btn-default'; ?>" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo $patient_data[0]->PatientGUID; ?>/4">4<sup>th</sup> Rx</a>
							<a class="btn <?php echo ($visit == '5')?'btn-success':'btn-default'; ?>" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo $patient_data[0]->PatientGUID; ?>/5">5<sup>th</sup> Rx</a>
							<a class="btn <?php echo ($visit == '6')?'btn-success':'btn-default'; ?>" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo $patient_data[0]->PatientGUID; ?>/6">6<sup>th</sup> Rx</a>
						<?php } ?>
						<a class="btn <?php echo ($visit == 'eot')?'btn-success':'btn-default'; ?>" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo $patient_data[0]->PatientGUID; ?>/eot">EoT</a>
					</div>
				</div>
			</div>
			<hr style="border-top: 2px solid #000; margin-left: 30px; margin-right: 30px;">
			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="">Visit Date<span class="text-danger">*</span></label>
					<input type="text" name="visit_date" id="visit_date" class="input_fields form-control hasCal dateInpt input2" value="<?php echo (count($visit_details) > 0)?timeStampShow($visit_details[0]->Visit_Dt):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>"" required="">
					<br class="hidden-lg-*">
				</div>
				<div class="col-md-3">
					<label for="">Haemoglobin</label>
					<input type="text" name="haemoglobin" id="haemoglobin" class="input_fields form-control"  onkeypress="return onlyNumbersWithDot(event);" maxlength="4" value="<?php if(count($visit_details )>0 && $visit_details[0]->Haemoglobin!=""){ echo $visit_details[0]->Haemoglobin;} ?>">
					<br class="hidden-lg-*">
				</div>
				<div class="col-md-3">
					<label for="">Platelet Count</label>
					<input type="text" name="platelet_count" id="platelet_count" class="input_fields form-control"  onkeypress="return onlyNumbersWithDot(event);" maxlength="8" value="<?php if(count($visit_details )>0 && $visit_details[0]->PlateletCount!=""){ echo $visit_details[0]->PlateletCount;} ?>">
					<br class="hidden-lg-*">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3" style="background-color: lightblue; padding-top: 15px; padding-bottom: 15px;">
					<label for="">Regimen Prescribed <span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="regimen_prescribed" name="regimen_prescribed" required="" disabled="">
						<option value="">Select</option>
						<?php foreach ($regimen_prescribed as $row) {?>
							<option <?php echo (count($patient_data) > 0 && $patient_data[0]->T_Regimen == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-3 sofosbuvir_field" style="background-color: lightblue; padding-top: 15px; padding-bottom: 15px;">
					<label for="">Sofosbuvir<span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="sofosbuvir" name="sofosbuvir" disabled="">
						<option value="">Select</option>
						<?php foreach ($mst_drug_strength as  $value) { ?>
							
							<option value="<?php  echo $value->id_mst_drug_strength; ?>" <?php if($regimen_drug_data[0]->id_mst_drugs ==  $value->id_mst_drug_strength ){ echo 'selected';}  ?>><?php  echo $value->strength; ?></option>
							<?php } ?>
					</select>
				</div>
				<div class="col-md-3 daclatasvir_field" style="background-color: lightblue; padding-top: 15px; padding-bottom: 15px;">
					<label for="">Daclatasvir<span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="daclatasvir" name="daclatasvir" disabled="">
						<option value="">Select</option>
						<?php foreach ($mst_drug_Daclatasvir as  $value) { ?>
							
							<option value="<?php  echo $value->id_mst_drug_strength; ?>" <?php if($regimen_drug_data[1]->id_mst_drug_strength ==  $value->id_mst_drug_strength ){ echo 'selected';}  ?>><?php  echo $value->strength; ?> mg</option>
							<?php } ?>
					</select>
				</div>
				<div class="col-md-3 velpatasvir_field" style="background-color: lightblue; padding-top: 15px; padding-bottom: 15px;">
					<label for="">Velpatasvir<span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="velpatasvir" name="velpatasvir" disabled="">
						<option value="">Select</option>
						<?php foreach ($mst_drug_Velpatasvir as  $value) { ?>
							<option value="<?php  echo $value->id_mst_drug_strength; ?>" <?php if($regimen_drug_data[1]->id_mst_drug_strength ==  $value->id_mst_drug_strength ){ echo 'selected';}  ?>><?php  echo $value->strength; ?> mg</option>
						<?php  } ?>
					</select>
				</div>
				<div class="col-md-3 ribavrin_field" style="background-color: lightblue; padding-top: 15px; padding-bottom: 15px;">
					<label for="">Ribavrin<span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="ribavrin" name="ribavrin" disabled="">
						<option value="">Select</option>
							<?php foreach ($mst_drug_Ribavrin as  $value) { ?>
							<option value="<?php  echo $value->id_mst_drug_strength; ?>" <?php if($regimen_drug_data[2]->id_mst_drug_strength ==  $value->id_mst_drug_strength ){ echo 'selected';}  ?>><?php  echo $value->strength; ?> mg</option>
						<?php  } ?>
					</select>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="">Place Of Dispensation</label>
					<select class="form-control input_fields" id="place_of_dispensation" name="place_of_dispensation" readonly>
						<option value="">Select</option>
						<?php error_reporting(0); if(count($patient_data) > 0 && !empty($patient_data[0]->ReferTo ) ) { ?>
							<option value="<?php  echo $ReferToFaci[0]->id_mstfacility; ?>" <?php if($patient_data[0]->PrescribingFacility == $ReferToFaci[0]->id_mstfacility){ echo "selected"; }  ?>><?php echo $ReferToFaci[0]->FacilityCode; ?></option>

						<?php } ?>
									<?php foreach ($place_of_dispensation as $row) { ?>
								<option  value="<?php echo $row->id_mstfacility; ?>"
								<?php 
								if($patient_data[0]->PrescribingFacility == null && $row->id_mstfacility == $loginData->id_mstfacility)
									echo "selected"; 
								else if ($patient_data[0]->PrescribingFacility != null && $patient_data[0]->PrescribingFacility == $row->id_mstfacility)
									echo "selected"; 
								else
									echo '';
									?>
								><?php echo $row->FacilityCode; ?></option>
							
						<?php } ?>
					</select>
				</div>
				<div class="col-md-3">
					<label for="">Pills Given</label>
					<input type="text" name="pills_given" id="pills_given" class="input_fields form-control" value="28" onkeypress="return onlyNumbersWithDot(event);" readonly="" maxlength="2">
					<br class="hidden-lg-*">
				</div>
				<div class="col-md-3">
					<label for="">Pills Left</label>
					<input type="text" name="pills_left" id="pills_left" class="input_fields form-control" onkeypress="return onlyNumbersWithDot(event);" maxlength="2" value="<?php if(count($visit_details )>0 && $visit_details[0]->PillsLeft!=""){ echo $visit_details[0]->PillsLeft;} else{ echo 0;} ?>">
					<br class="hidden-lg-*">
				</div>
			</div>
			<div class="row">
				<div class="col-md-3" style="background-color: lightyellow; padding-top: 15px; padding-bottom: 15px;">
					<label for="">Adherence(%)</label>
					<input type="text" name="adherence" id="adherence" class="input_fields form-control" value="<?php if(count($visit_details )>0 && $visit_details[0]->Adherence!=""){ echo $visit_details[0]->Adherence;} ?>" readonly="">
					<br class="hidden-lg-*">
				</div>
				<div class="col-md-3 adherence_field" style="background-color: lightyellow; padding-top: 15px; padding-bottom: 15px;">
					<label for="">Reason for Low Adherence</label>
					<select class="form-control input_fields" id="reason_low_adherence" name="reason_low_adherence">
						<option value="">Select</option>
						<?php foreach ($low_adherence as $row) {?>
							<option value="<?php echo $row->LookupCode; ?>"  <?php echo (count($visit_details) > 0 && $visit_details[0]->NAdherenceReason == $row->LookupCode)?'selected':''; ?>><?php echo $row->LookupValue; ?></option>
						<?php } ?>
					</select>
					<br>
				</div>
				<div class="col-md-3 reason_low_adherence_fields" style="background-color: lightyellow; padding-top: 15px; padding-bottom: 15px;">
					<label for="">Low Adherence Reason Other</label>
					<input type="text" name="reason_low_adherence_other" id="reason_low_adherence_other" class="input_fields form-control" value="<?php echo (count($visit_details) > 0)?$visit_details[0]->NAdherenceReasonOther:''; ?>">
					<br class="hidden-lg-*">
				</div>

			</div>
			<div class="row">
				<div class="col-md-3">
					<label for="">Advised Next Visit Date</label>
					<input type="text" name="advised_visit_date" id="advised_visit_date" class="input_fields form-control " readonly value="<?php if( count($visit_details )>0 && $visit_details[0]->NextVisit_Dt!=""){ echo timeStampD($visit_details[0]->NextVisit_Dt);} ?>">
					<br class="hidden-lg-*">
				</div>
				<div class="col-md-3">
					<label for="">Doctor</label>
					<select class="form-control input_fields" id="doctor" name="doctor">
						<option value="">Select</option>
						<?php foreach ($doctors as $row) {?>
							<option value="<?php echo $row->id_mst_medical_specialists; ?>" <?php if(count($visit_details )>0 && $visit_details[0]->Doctor == $row->id_mst_medical_specialists) { echo 'selected';} ?>><?php echo $row->name; ?></option>
						<?php } ?>
					</select>
				</div>
					<div class="col-md-3 doctor_other_field">
						<label for="">Doctor Other <span class="text-danger"></span></label>
						<input type="text" name="doctor_other" id="doctor_other" class="input_fields form-control messagedata" value="<?php if(count($visit_details )>0){ echo (count($patient_data) > 0)?$visit_details[0]->DoctorOther:''; }?>">
					</div>


				<div class="row">
				<div class="col-md-4">
					<label>Side Effects</label><br/>

					<?php foreach ($side_effects as $row) { 

						 if(count($visit_details )>0 ){

						 	$SideEffectValue  = $visit_details[0]->SideEffectValue;
						 	$sideef = explode(',', $SideEffectValue);
						 	$checked = '';
									if (in_array($row->LookupCode, $sideef))
										$checked = 'checked';
						 }
						?>

					<span class="fill-control-description"><?php echo $row->LookupValue; ?></span>
					
					<label class="custom-control overflow-checkbox">
					<input type="checkbox" class="overflow-control-input" value="<?php echo $row->LookupCode; ?>" id="side_effects<?php echo $row->LookupCode; ?>" name="side_effects[]" <?php if(count($visit_details )>0 ){ echo $checked;} ?>>
					<span class="overflow-control-indicator"></span></label>
					<?php } ?>

					<!-- <select class="form-control input_fields" id="side_effects" name="side_effects">
						<option value="">Select</option>
						<?php //foreach ($side_effects as $row) {?>
							<option value="<?php echo $row->LookupCode; ?>" <?php if(count($visit_details )>0 && $visit_details[0]->SideEffectValue == $row->LookupCode) { echo 'selected';} ?>><?php echo $row->LookupValue; ?></option>
						<?php //} ?>
					</select> -->
				</div></div>
				<div class="col-md-3 side_effects_other_field">
						<label for="">Other <span class="text-danger">*</span></label>
						<input type="text" name="side_effect_other" id="side_effect_other" class="input_fields form-control messagedata" value="<?php echo (count($visit_details) > 0)?$visit_details[0]->SideEffect:''; ?>">
					</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<label for="">Comments</label>
					<textarea rows="5" name="comments" id="comments" class="form-control" style="border: 1px #CCC solid; width: 100%;" maxlength="500"><?php if(count($visit_details )>0 && $visit_details[0]->Comments!=""){ echo $visit_details[0]->Comments;} ?></textarea>
					<br class="hidden-lg-*">
				</div>
			</div>
			<br>
			<input type="hidden" name="interruption_status" id="interruption_status" value="<?php  echo (count($patient_data) > 0)?$patient_data[0]->InterruptReason:'';  ?>">
			<div class="row">
				<div class="col-lg-3 col-md-2">
					<a class="btn btn-block btn-default form_buttons" href="<?php echo base_url('patientinfo?p=1'); ?>" id="close" name="close" value="close">CLOSE</a>
				</div>
				<div class="col-lg-3 col-md-2">
					<button class="btn btn-block btn-default form_buttons" id="refresh" name="refresh" value="refresh">REFRESH</button>
				</div>
				<div class="col-lg-3 col-md-2">
					<a href="#" class="btn btn-block btn-default form_buttons" id="lock" name="lock" value="lock">LOCK</a>
				</div>
				<?php if($result[0]->Dispense == 1) {?>
					<div class="col-lg-3 col-md-2">
						<button class="btn btn-block btn-success form_buttons" id="save" name="save" value="save">SAVE</button>
					</div>
				<?php } ?>
			</div>
			<input type="hidden" class="hasCal dateInpt input2" name="NextVisit_Dtn" id="NextVisit_Dtn" value="<?php echo timeStampShow($visit_detailsdate[0]->NextVisit_Dt); ?>">
			<input type="hidden" name="Next_Visitdtn" class="hasCal dateInpt input2"  id="Next_Visitdtn" value="<?php echo timeStampShow($patient_data[0]->Next_Visitdt); ?>">

			<br><br><br>
			  <?php echo form_close(); ?>
			<div class="row" class="text-left">

					<div class="col-md-6">
					 <label class="btn  btn-default form_buttons"  style="text-align: left !important; ">Patient's Status 
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" name=""  readonly="readonly" value="<?php echo (count($patient_status) > 0)?$patient_status[0]->status:''; ?>" class="btn">
					</label>
				</div>

				

				<div class="col-md-6">
					<label class="btn btn-default form_buttons" style="text-align: left !important;     line-height: 3.2 !important;">Patient's Interruption Status 
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<select name="" id="type_val" onchange="openPopup()" class="btn" style="text-align: right!important;">
						<option value="">Select</option>
						<option value="1"  <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason != '')?'selected':''; ?> >Yes</option>
						<option value="0" <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason == '')?'selected':''; ?>>No</option>
					</select>
				
				</label>
				</div>
			
			</div>
      
		<!-- </form> -->
	</div>
</div>



<div class="modal fade" id="addMyModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title">Patient's Interruption Status</h4>
      </div>
      <span id='form-error' style='color:red'></span>
      <div class="modal-body">
      <!--   <form role="form" id="newModalForm" method="post"> -->
      	<?php
           $attributes = array(
              'id' => 'newModalForm',
              'name' => 'newModalForm',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>


				<div class="form-group">
				<label class="control-label col-md-3" for="email">Reson:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonval" name="resonval" required="required">
				<option value="">Select</option>
				<option value="1">Death</option>
				<option value="2">Loss to followup</option>
				</select> 
				</div>
				</div>
<br/><br/>

				<div class="form-group resonfielddeath" >
				<label class="control-label col-md-3" for="email">Reason for death:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonvaldeath" name="resonvaldeath">
				<option value="">Select</option>
				<?php foreach ($reason_death as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
				
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>

				<div class="form-group resonfieldlfu">
				<label class="control-label col-md-3" for="email">Reason for LFU:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonfluid" name="resonfluid">
				<option value="">Select</option>
				<?php foreach ($reason_flu as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
				
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>

			<div class="control-label col-md-3">
				
			<input type="radio" name="interruption_stage" value="1">
			<label>ETR</label>
			</div>
			<div class="control-label col-md-3">
			<input type="radio" name="interruption_stage" value="2">
			<label>SVR</label>
			</div>
			<div class="control-label col-md-3">
			<input type="radio" name="interruption_stage" value="3">
			<label>NONE</label>
			</div>
			<br/><br/>
		</div>

          <div class="modal-footer">
            <button type="submit" class="btn btn-success" id="btnSaveIt">Save</button>
            <button type="button" class="btn btn-default" id="btnCloseIt" data-dismiss="modal">Close</button>
          </div>
           <?php echo form_close(); ?>
       <!--  </form> -->
      </div>
    </div>
  </div>
</div>

<br/><br/><br/>

<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>

<script>
	//$('#advised_visit_date').datepicker({minDate:-1,maxDate:-2}).attr('readonly','readonly'); 
	$("#advised_visit_date").datepicker({minDate:-1,maxDate:-2}).attr('readonly','readonly'); 
	function openPopup() {
		var type_val = $('#type_val').val();
		if(type_val=='1'){
    $("#addMyModal").modal();
		}
}

$('.resonfielddeath').hide();
$('.resonfieldlfu').hide();


$('#btnCloseIt').click(function(){

$('#type_val').val('0');
$('#interruption_status').val('');

});

$('#type_val').change(function(){
var type_val = $('#type_val').val();
$('#interruption_status').val(type_val);

});


 $('#btnSaveIt').click(function(e){
      
      e.preventDefault(); 
      $("#form-error").html('');
     
        
	 var formData = new FormData($('#newModalForm')[0]);
	 $('#btnSaveIt').prop('disabled',true);
	 $.ajax({				    	
		url: '<?php echo base_url(); ?>patientinfo/interruptionstatus_process/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
		type: 'POST',
		data: formData,
		dataType: 'json',
		async: false,
        cache: false,
		contentType: false,
		processData: false,
		success: function (data) { 
				
			if(data['status'] == 'true'){
				$("#form-error").html('Data has been successfully submitted');
				// redirectstatus
				if(data['interruptstage'] == 2)
					{
    					location.href='<?php echo base_url(); ?>patientinfo/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
					}
					else if(data['interruptstage'] == 1)
					{
						visit = 'eot';
    					location.href='<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>eot';
					}
				setTimeout(function() {
    					location.href='<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
				}, 1000);

			}else{
				$("#form-error").html(data['message']);
				$('#btnSaveIt').prop('disabled',false); 
				return false;
			}   
		}        
	 }); 
		
   

    }); 


	$('#resonval').change(function(){
		

		if($('#resonval').val() == '1'){

		$('.resonfielddeath').show();
		$('.resonfieldlfu').hide();
		$('#resonfluid').val('');

		$('#resonvaldeath').prop('required',true);
		$('#resonfluid').prop('required',false);

	}else if($('#resonval').val() == '2'){
		$('.resonfielddeath').hide();
		$('.resonfieldlfu').show();
		$('#resonvaldeath').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',true);
	}else{

		$('.resonfielddeath').hide();
		$('.resonfieldlfu').hide();
		$('#resonvaldeath').val('');
		$('#resonfluid').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',false);
	}

});
</script>

			<?php if(count($patient_data) > 0 && $patient_data[0]->NextVisitPurpose > 98) {?>
	<script>

		$("#lock").click(function(){
				$("#modal_header").text("Contact admin ,for unlocking the record");
					$("#modal_text").text("Contact Admin");
					$("#multipurpose_modal").modal("show");
					return false;

				});
			
				/*Disable all input type="text" box*/
				//alert('<?php echo $patient_data[0]->MF5; ?>');
				$('#registration input[type="text"]').attr("readonly", true);
				$('#registration input[type="checkbox"]').attr("disabled", true);
				$('#registration input[type="date"]').attr("readonly", true);
				$("#save").attr("disabled", true);
				$("#refresh").attr("disabled", true);
				$('#registration select').attr('disabled', true);
				/*Disable textarea using id */
				$('#registration #txtAddress').prop("readonly", true);
		
		</script>
<?php  } ?>


<script>
	function onlyNumbersWithDot(e) {           
            var charCode;
            if (e.keyCode > 0) {
                charCode = e.which || e.keyCode;
            }
            else if (typeof (e.charCode) != "undefined") {
                charCode = e.which || e.keyCode;
            }
            if (charCode == 46)
                return true
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
</script>
<?php if($result[0]->Dispense == 1) {?>
	<script>
		

		$(document).ready(function(){
			<?php if(count($patient_data) > 0 && $patient_data[0]->T_Regimen == 1) {?>
					
						$(".sofosbuvir_field").show();
					$(".daclatasvir_field").show();
					$(".ribavrin_field").hide();
					$(".velpatasvir_field").hide();
						
					<?php }?>

					<?php if(count($patient_data) > 0 && $patient_data[0]->T_Regimen == 2) {?>
					
						$(".sofosbuvir_field").show();
					$(".velpatasvir_field").show();
					$(".daclatasvir_field").hide();
					$(".ribavrin_field").hide();
					<?php }?>
					<?php if(count($patient_data) > 0 && $patient_data[0]->T_Regimen == 3) {?>
					//alert(<?php echo $patient_data[0]->RecommendedRegimen; ?>);
					$(".sofosbuvir_field").show();
					$(".ribavrin_field").show();
					$(".velpatasvir_field").show();
					$(".daclatasvir_field").hide();
						
					<?php } ?>
				});

		$(document).ready(function(){

			<?php 
			$error = $this->session->flashdata('error'); 

			if($error != null)
			{
				?>
				$("#multipurpose_modal").modal("show");
				<?php 
			}
			?>

			$("#refresh").click(function(e){
			// if(confirm('All further details will be deleted.Do you want to continue?')){ 
				$("input").val('');
				$("select").val('');
				$("textarea").val('');
				$("#input_district").html('<option>Select District</option>');
				$("#input_block").html('<option>Select Block</option>');

				e.preventDefault();
			// }
			});
			
			

			$('#table_patient_list tbody tr').click(function(){
				window.location = $(this).data('href');
			});

			$("#regimen_prescribed").change(function(){

				if($(this).val() == 1)
				{
					$(".sofosbuvir_field").show();
					$(".daclatasvir_field").show();
					$(".ribavrin_field").hide();
					$(".velpatasvir_field").hide();
				}
				else if($(this).val() == 2)
				{
					$(".sofosbuvir_field").show();
					$(".velpatasvir_field").show();
					$(".daclatasvir_field").hide();
					$(".ribavrin_field").hide();
				}
				else if($(this).val() == 3)
				{
					$(".sofosbuvir_field").show();
					$(".ribavrin_field").show();
					$(".velpatasvir_field").show();
					$(".daclatasvir_field").hide();
				}
				else
				{
					$(".sofosbuvir_field").hide();
					$(".daclatasvir_field").hide();
					$(".ribavrin_field").hide();
					$(".velpatasvir_field").hide();
				}
			});

			$("#adherence").change(function(){
				if($(this).val() < 100)
				{
					$(".adherence_field").show();
				}
				else
				{
					$(".adherence_field").hide();
				}
			});
		})


		$("#side_effects99").click(function(){
					if($(this).val() == 99 && $(this).prop("checked") == true)
					{
						
						$(".side_effects_other_field").show();
						$('#side_effect_other').prop('required',true);
					}
					else
					{
						//$(".side_effects_other_field").hide();
						$('#side_effect_other').val('');
						
					}
				});
<?php if(count($visit_details) > 0 && !empty($visit_details[0]->SideEffect)) {?>
$(".side_effects_other_field").show();
<?php } else{?>
$(".side_effects_other_field").hide();
<?php  } ?>


		    $(".sofosbuvir_field").hide();
			$(".daclatasvir_field").hide();
			$(".ribavrin_field").hide();
			$(".velpatasvir_field").hide();
			$(".adherence_field").hide();
			//$(".side_effects_other_field").hide();
			$(".doctor_other_field").hide();


			$("#doctor").change(function(){
					if($(this).val() == 999)
					{
						$(".doctor_other_field").show();
						$('#doctor_other').prop('required',true);
					}
					else
					{
						$(".doctor_other_field").hide();
						$('#doctor_other').prop('required',false);
					}
				});

	</script>
<?php } else { ?>
	<script>
		$(document).ready(function(){
			$('input').prop('disabled', true);
			$('select').prop('disabled', true);
			$('textarea').prop('disabled', true);
		});
	</script>
	<?php } ?>

	<script>
		
/*$("#visit_date" ).change(function( event ) {

var date_of_prescribing_testsdate = '<?php echo $patient_data[0]->Next_Visitdt; ?>';
var visit_date = $("#visit_date" ).val();

if(date_of_prescribing_testsdate > visit_date ){

					$("#modal_header").text("Visit Date  greater than or equal to must be "+date_of_prescribing_testsdate);
					$("#modal_text").text("Please check dates");
					$("#visit_date" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}
});*/

$("#visit_date" ).change(function( event ) {

// // for 7 greater days
<?php if($visit!=2){ ?>
var date =$("#NextVisit_Dtn" ).datepicker('getDate'); 
<?php } else{?>
var date = $("#Next_Visitdtn" ).datepicker('getDate');
<?php } ?>
var visit_date = $("#visit_date" ).datepicker('getDate');
//alert(visit_date+'/'+date);

		var remaningdays = 3;
		days = parseInt(remaningdays, 10);
		if(!isNaN(date.getTime())){
		date.setDate(date.getDate() + days);
		}

// for 7 less days
<?php if($visit!=2){ ?>

			var date1 = $("#NextVisit_Dtn" ).datepicker('getDate'); 
		<?php } else{?>
			var date1 = $("#Next_Visitdtn" ).datepicker('getDate');
		<?php }?>

			days1 = parseInt(remaningdays, 10);
			if(!isNaN(date1.getTime())){
					date1.setDate(date1.getDate() - days1);
				}
			<?php if($visit!=2){ ?>
			var d = $("#NextVisit_Dtn" ).val(); 
		<?php } else { ?>
			var d = $("#Next_Visitdtn" ).val(); 
		<?php } ?>

		if(date < visit_date || date1 > visit_date){

							$("#modal_header").text("Visit Date should be before 3 days or after 3 days from "+d);
							$("#modal_text").text("Please check dates");
							$("#visit_date" ).val('');
							$("#multipurpose_modal").modal("show");
							return false;
			
		}

		});


/*$(document).ready(function(){	
	pills_givencal();*/

	$("#pills_left" ).change(function() {
		
		pills_givencal();

		

	});
	$("#visit_date" ).change(function( event ) {
		
		pills_givencal();
	});


//});
	

	function pills_givencal()
	{
		
			var date = $("#visit_date").datepicker('getDate'); 
			//alert(date);
			<?php if($visit!=2){ ?>
			var datedb = $("#NextVisit_Dtn" ).datepicker('getDate'); 
		<?php } else { ?>
			var datedb = $("#Next_Visitdtn" ).datepicker('getDate');
		<?php  } ?>

			var pills_left =0;
			var pills_given = parseInt($('#pills_given').val());
			// alert(pills_given);
			var pills_left = $('#pills_left').val();
			if(pills_left == ''){
				var pills_left = 0;
			}else{
				var pills_left = parseInt(pills_left);
			}

			var pills_given1 = parseInt($('#pills_given').val());
			var pills_left1 = parseInt($('#pills_left').val());

		if(pills_left1 > pills_given1){

					$("#modal_header").text("Pills Left not greater then Pills Given");
					$("#modal_text").text("haemoglobin greater than zero");
					$('#pills_left').val('');
					$("#multipurpose_modal").modal("show");
					return false;
		}

		var minutes = 1000*60;
            var hours = minutes*60;
            var days = hours*24;

		 var diff_date = Math.round((date - datedb)/days);
           

			var remaningpills = parseInt(pills_given)+ parseInt(pills_left);
			// alert(remaningpills);
			var remaningpillsval = (remaningpills-3);
			days1 = parseInt(remaningpillsval, 10);
			if(diff_date>1){


				var pills_left1 = 31-(diff_date)-pills_left;
				
			}
			else{
				if(diff_date>0){
					var diff_date1 = diff_date-pills_left;
				}else{
					var diff_date1 = 0;
				}
				var pills_left1 = 31-pills_left-diff_date;
				
			}

// alert(pills_left1);
			var remaningpills = parseInt(pills_given) + parseInt(pills_left);
			//var remaningpillsval = (remaningpills - 3);
			var remaningpillsval = (remaningpills-3);
			//alert(remaningpillsval);
			var Adherence = ( ( pills_left1 ) * 100) / 28;
			days = parseInt(remaningpillsval, 10);
//alert(days);
			if(!isNaN(date.getTime())){
			date.setDate(date.getDate() + days);
			if(Adherence>100){
				var Adherence = 100
			}else{
				var Adherence = Adherence;
			}
			if(date!='' && $('#pills_left').val()!=''){

			$('#adherence').val(Adherence.toFixed(2));
		}
			$("#advised_visit_date").val(date.toInputFormat());
			//alert(date);

			} /*else {
			alert("Invalid Date");  
			}*/
		

//alert(Adherence);
if(Adherence==100){
	$('.adherence_field').hide();
	$('#reason_low_adherence').prop('required',false);
	//$('.adherence_field').reason_low_adherence_fields();
}else{
	$('.adherence_field').show();
	$('#reason_low_adherence').prop('required',true);
}

	

}


$("#reason_low_adherence").change(function(){
	if($(this).val() == 99)
	{
		$('.reason_low_adherence_fields').show();
		$('#reason_low_adherence_other').val('');

	}
	else
	{
		
		$('.reason_low_adherence_fields').hide();
		
	}
});

<?php if(count($visit_details) > 0 && $visit_details[0]->NAdherenceReason == 99) {?>
$('.reason_low_adherence_fields').show();
<?php } else { ?>
$('.reason_low_adherence_fields').hide();
<?php } ?>
<?php if(count($patient_data) > 0 && $patient_data[0]->Adherence == 100) {?>

$('.adherence_field').hide();
<?php } else {?>
$('.adherence_field').show();
<?php } ?>


 Date.prototype.toInputFormat = function() {
       var yyyy = this.getFullYear().toString();
       var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
       var dd  = this.getDate().toString();
       return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
    };

/*$("#haemoglobin" ).change(function( event ) {

var haemoglobin = $('#haemoglobin').val();

if(haemoglobin > 20 || haemoglobin < 1){

					$("#modal_header").text("Haemoglobin between 10 to 20");
					$("#modal_text").text("For men, 13.5 to 17.5 grams per deciliter");
					$('#haemoglobin').val('');
					$("#multipurpose_modal").modal("show");
					return false;

}

});*/
	</script>