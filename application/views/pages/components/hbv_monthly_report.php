<style>
thead
{
	background-color: #085786;
	color: white;
}

.table-bordered>tbody>tr>td.data_td
{
	font-weight: 600;
	font-size: 15px;
	text-align: center;
	vertical-align: middle;
}
</style>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 3 !important;
	width: 100% !important;
	font-size: 14px !important;
	font-family: 'Source Sans Pro';
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
	.col-md-2{
		width: 16.33%;
		padding-left: 8px;
		padding-right: 8px;
	}
	.btn-width{
	width: 12%;
	margin-top: 20px;
}
.pd-15{
	padding-left: 15px;
}
.pb-20{
	padding-bottom: 20px;
}
.container{
	width: 76%;
}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 4 !important;
		width: 100% !important;
		font-size: 14px !important;
		font-family: 'Source Sans Pro';
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 7px 12px;
	margin-bottom: 0;
	font-size: 13px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
 .table-bordered>thead>tr>th{
	vertical-align: middle;
	font-size: 13px;
}
.table-bordered>tbody>tr>td{
	vertical-align: middle;
	font-size: 13px;
	color: #000000;
}
</style>
</style>
<br>

<?php $loginData = $this->session->userdata('loginData');
//echo "<pre>"; print_r($loginData);

if($loginData->user_type==1) { 
$select = '';
}else{
$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
$select4 = '';	
}

$filters1 = $this->session->userdata('filters1'); 
//pr($filters1);
 ?>
<div class="container">
		<div class="panel panel-default" id="Monthly_Report_Panel">
				<div class="panel-heading" style="background-color: #333333;padding: 3px 0px;">
					<h4 class="text-center" style="background-color: #333333;color: white; font-family: 'Source Sans Pro';letter-spacing: .75px;">Monthly Report (HBV)
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php //echo base_url(); ?>application/third_party/images/excel_black.png" alt=""></a> -->
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php //echo base_url(); ?>application/third_party/images/pdf_color.png" alt=""></a> -->
		</h4>
		<!-- <input type="button" onclick="printDiv('printableArea')" value="Print" /> -->
		
</div>

<?php
           $attributes = array(
              'id' => 'filter_form',
              'name' => 'filter_form',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>
<div class="row" style="padding-left: 15px; padding-top: 15px;">

	<div class="col-md-2 col-sm-12">
		<label for="">State</label>
		<select type="text" name="search_state" id="search_state" class="form-control input_fields" required <?php echo $select2.$select3.$select4; ?>>
			<option value="0">All States</option>
			<?php 
			foreach ($states as $state) {
				?>
				<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo $state->StateName; ?></option>
				<?php 
			}
			?>
		</select>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">District</label>
		<select type="text" name="input_district" id="input_district" class="form-control input_fields"   <?php echo $select.$select2.$select4; ?>>
			

			<?php foreach ($districts as  $value) { ?>
				<option value="<?php echo $value->id_mstdistrict; ?>" <?php if($this->input->post('input_district')==$value->id_mstdistrict) { echo 'selected';}  ?>><?php echo $value->DistrictName; ?></option>
			<?php } ?>
			<?php 
			
			?>
		</select>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">Facility</label>
		<select type="text" name="facility" id="mstfacilitylogin" class="form-control input_fields"  <?php echo $select.$select2; ?>>
			<option value="">Select Facility</option>
			<?php 
			
			?>
		</select>
	</div>


	<div class="col-md-2 col-sm-12">
		<label for="">From Date</label>
		<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')) { echo $this->input->post('startdate'); }else{ echo timeStampShow(date('Y-m-01')); } ?>" required>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">To date</label>
		<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')) { echo $this->input->post('enddate'); }else{  echo timeStampShow(date('Y-m-d'));} ?>" required>
	</div>
<div class="col-lg-2 col-md-2 col-sm-12 btn-width" style="margin-top: 20px;">
					<button type="submit" class="btn btn-block btn-success" id="monthreport" style="line-height: 1; font-weight: 600;font-size: 15px;">Search</button>
				</div><span style="margin-top: 25px;margin-right: 25px; float: right;"><a href="javascript:void(0)"><i class="fa fa-2x fa-download" id="excel_button" onclick="tableToExcel('testTable', 'W3C Example','HBV_Monthly_Report.xls')" title="Excel Download"></i></a></span>
			</div>


 <?php echo form_close(); ?>
<br>
</div>
<div class="row" id="printableArea">

	<div class="col-md-12">
		<table class="table table-bordered table-highlighted" id="testTable" >
			<thead>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';">1</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';">Number of Hepatitis B infected people seeking care at the treatment center (Registering in Care)</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;"nowrap="nowrap">&nbsp;&nbsp;Adult Male&nbsp;&nbsp;</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;" nowrap="nowrap">Adult Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">Children&nbsp;<&nbsp;18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;"nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</thead>
			<tbody>
				<tr style="font-family:'Source Sans Pro';">
					<td>1.1</td>
					<td>Total number of patients screened for HBsAg in this month</td>
					<td class="data_td"><?php echo !empty($count_1_1->male)>0 ? $count_1_1->male : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_1_1->female)>0 ? $count_1_1->female : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_1_1->transgender)>0 ? $count_1_1->transgender  : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_1_1->children)>0 ? $count_1_1->children : "-"; ?></td>
					<td class="data_td"><?php echo ($count_1_1->male + $count_1_1->female + $count_1_1->children +$count_1_1->transgender); ?></td>
				</tr>

				<tr style="font-family:'Source Sans Pro';">
					<td>1.2</td>
					<td>Total Number of patients found positive for HBsAg in this month</td>
					<td class="data_td"><?php echo !empty($count_1_2->male)>0 ? $count_1_2->male : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_1_2->female)>0 ? $count_1_2->female : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_1_2->transgender)>0 ? $count_1_2->transgender  : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_1_2->children)>0 ? $count_1_2->children : "-"; ?></td>
					<td class="data_td"><?php echo ($count_1_2->male + $count_1_2->female + $count_1_2->children +$count_1_2->transgender); ?></td>
				</tr>



			</tbody>
			<thead>
				<th style="background-color: #085786;color: white; ">2</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';">Initiation of treatment</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;"nowrap="nowrap">Adult Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">Adult Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;"nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</thead>
			<tbody>
				<tr style="font-family:'Source Sans Pro';">
					<td >2.1</td>
					<td>Number of new patients started on treatment during this month</td>
					<td class="data_td"><?php echo !empty($count_2_1->male)>0 ? $count_2_1->male : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_2_1->female)>0 ? $count_2_1->female : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_2_1->transgender)>0 ? $count_2_1->transgender  : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_2_1->children)>0 ? $count_2_1->children : "-"; ?></td>
					<td class="data_td"><?php echo ($count_2_1->male + $count_2_1->female + $count_2_1->children +$count_2_1->transgender); ?></td>
				</tr>
				<tr style="font-family:'Source Sans Pro';">
					<td>2.2</td>
					<td>Number of patients on treatment "transferred in" during this month</td>
					<td class="data_td"><?php echo "-"; ?></td>
					<td class="data_td"><?php echo "-"; ?></td>
					<td class="data_td"><?php echo "-"; ?></td>
					<td class="data_td"><?php echo "-"; ?></td>
						<td class="data_td"><?php echo "0"; ?></td>
				</tr>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>2.3</td>
					<td>Cumulative number of patients ever received treatment (number at the end of this month) </td>
					
					<td class="data_td"><?php echo !empty($count_2_1->male)>0 ? $count_2_1->male : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_2_1->female)>0 ? $count_2_1->female : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_2_1->transgender)>0 ? $count_2_1->transgender  : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_2_1->children)>0 ? $count_2_1->children : "-"; ?></td>
					<td class="data_td"><?php echo ($count_2_1->male + $count_2_1->female + $count_2_1->children +$count_2_1->transgender); ?></td>
				</tr>
			</tbody>

			<thead>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';">3</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';">Regimen status</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;"nowrap="nowrap">&nbsp;&nbsp;Adult Male&nbsp;&nbsp;</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;" nowrap="nowrap">Adult Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">Children&nbsp;<&nbsp;18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;"nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</thead>
			<tbody>
				<tr style="font-family:'Source Sans Pro';">
					<td>3.1</td>
					<td>Cumulative number of Hepatitis B patients on TAF</td>
					<td class="data_td"><?php echo !empty($count_3_1->male)>0 ? $count_3_1->male : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_1->female)>0 ? $count_3_1->female : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_1->transgender)>0 ? $count_3_1->transgender  : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_1->children)>0 ? $count_3_1->children : "-"; ?></td>
					<td class="data_td"><?php echo ($count_3_1->male + $count_3_1->female + $count_3_1->children +$count_3_1->transgender); ?></td>
				</tr>

				<tr style="font-family:'Source Sans Pro';">
					<td>3.2</td>
					<td>Cumulative number of Hepatitis B patients on Entecavir</td>
					<td class="data_td"><?php echo !empty($count_3_2->male)>0 ? $count_3_2->male : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_2->female)>0 ? $count_3_2->female : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_2->transgender)>0 ? $count_3_2->transgender  : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_2->children)>0 ? $count_3_2->children : "-"; ?></td>
					<td class="data_td"><?php echo ($count_3_2->male + $count_3_2->female + $count_3_2->children +$count_3_2->transgender); ?></td>
				</tr>
				

				<tr style="font-family:'Source Sans Pro';">
					<td>3.3</td>
					<td>Cumulative number of Hepatitis B patients on TDF</td>
					<td class="data_td"><?php echo !empty($count_3_3->male)>0 ? $count_3_3->male : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_3->female)>0 ? $count_3_3->female : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_3->transgender)>0 ? $count_3_3->transgender  : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_3->children)>0 ? $count_3_3->children : "-"; ?></td>
					<td class="data_td"><?php echo ($count_3_3->male + $count_3_3->female + $count_3_3->children +$count_3_3->transgender); ?></td>
				</tr>
					<tr style="font-family:'Source Sans Pro';">
					<td>3.4</td>
					<td>Cumulative number of Hepatitis B patients on Other - Interferon based regimen</td>
					<td class="data_td"><?php echo !empty($count_3_4->male)>0 ? $count_3_4->male : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_4->female)>0 ? $count_3_4->female : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_4->transgender)>0 ? $count_3_4->transgender  : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_4->children)>0 ? $count_3_4->children : "-"; ?></td>
					<td class="data_td"><?php echo ($count_3_4->male + $count_3_4->female + $count_3_4->children +$count_3_4->transgender); ?></td>
				</tr>

			</tbody>

			<thead>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';">4</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';">Treatment status (at the end of the month) out of all patients ever started on treatment</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;"nowrap="nowrap">Adult Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">Adult Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">Total</th>
			</thead>
			<tbody>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>4.1</td>
					<td>The number of all patients whose regimen got changed in this month due to medical reasons</td>
					<td class="data_td"><?php echo !empty($count_4_1->male)>0 ? $count_4_1->male : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_1->female)>0 ? $count_4_1->female : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_1->transgender)>0 ? $count_4_1->transgender : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_1->children)>0 ? $count_4_1->children : "-"; ?></td>
					<td class="data_td"><?php echo ($count_4_1->male + $count_4_1->female+ $count_4_1->children + $count_4_1->transgender); ?></td>
				</tr>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>4.2</td>
					<td>Number of patients who are lost to follow-up (LTFU)</td>
						<td class="data_td"><?php echo !empty($count_4_2->male)>0 ? $count_4_2->male : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_2->female)>0 ? $count_4_2->female : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_2->transgender)>0 ? $count_4_2->transgender : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_2->children)>0 ? $count_4_2->children : "-"; ?></td>
					<td class="data_td"><?php echo ($count_4_2->male + $count_4_2->female+ $count_4_2->children + $count_4_2->transgender); ?></td>
				</tr>


				<?php $total_chlildren_4_3=($count_4_3['children_male'][0]->count+$count_4_3['children_female'][0]->count+$count_4_3['children_transgender'][0]->count); ?>


				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>4.3</td>
					<td>The number of patients who did not return and missed their doses in this month</td>
						<td class="data_td"><?php echo !empty($count_4_3['adult_male'][0]->count)>0 ? $count_4_3['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_3['adult_female'][0]->count)>0 ? $count_4_3['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_3['adult_transgender'][0]->count)>0 ? ($count_4_3['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_4_3)>0 ? $total_chlildren_4_3 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_4_3['adult_male'][0]->count + $count_4_3['adult_female'][0]->count + $total_chlildren_4_3 + $count_4_3['adult_transgender'][0]->count); ?></td>
				</tr>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>4.4</td>
					<td>Total number of patients Referred to Higher center for further Management</td>
					<td class="data_td"><?php echo "-"; ?></td>
					<td class="data_td"><?php echo "-"; ?></td>
					<td class="data_td"><?php echo "-"; ?></td>
					<td class="data_td"><?php echo "-"; ?></td>
						<td class="data_td"><?php echo "0"; ?></td>
				</tr>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>4.5</td>
					<td>Number of deaths reported</td>
				<td class="data_td"><?php echo !empty($count_4_5->male)>0 ? $count_4_5->male : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_5->female)>0 ? $count_4_5->female : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_5->transgender)>0 ? $count_4_5->transgender : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_5->children)>0 ? $count_4_5->children : "-"; ?></td>
					<td class="data_td"><?php echo ($count_4_5->male + $count_4_5->female+ $count_4_5->children + $count_4_5->transgender); ?></td>
				</tr>
			</tbody>
			<thead>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';">5</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';">Testing status and VL status out of all patients with HBsAg positive</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;"nowrap="nowrap">Adult Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">Adult Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">Total</th>
			</thead>
			<tbody>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>5.1</td>
					<td>Number of patients tested for persistently elevated ALT levels</td>
					<td class="data_td"><?php echo !empty($count_5_1->male)>0 ? $count_5_1->male : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_1->female)>0 ? $count_5_1->female : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_1->transgender)>0 ? $count_5_1->transgender : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_1->children)>0 ? $count_5_1->children : "-"; ?></td>
					<td class="data_td"><?php echo ($count_5_1->male + $count_5_1->female + $count_5_1->children + $count_5_1->transgender); ?></td>
				</tr>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>5.2</td>
					<td>Number of patients whose HBV DNA tests were conducted during the month</td>
				<td class="data_td"><?php echo !empty($count_5_2->male)>0 ? $count_5_2->male : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_2->female)>0 ? $count_5_2->female : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_2->transgender)>0 ? $count_5_2->transgender : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_2->children)>0 ? $count_5_2->children : "-"; ?></td>
					<td class="data_td"><?php echo ($count_5_2->male + $count_5_2->female + $count_5_2->children + $count_5_2->transgender); ?></td>
				</tr>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>5.3</td>
					<td>Patients identified as cirrhotic during the month</td>
				<td class="data_td"><?php echo !empty($count_5_3->male)>0 ? $count_5_3->male : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_3->female)>0 ? $count_5_3->female : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_3->transgender)>0 ? $count_5_3->transgender : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_3->children)>0 ? $count_5_3->children : "-"; ?></td>
					<td class="data_td"><?php echo ($count_5_3->male + $count_5_3->female + $count_5_3->children + $count_5_3->transgender); ?></td>
					
				</tr>
					

			</tbody>
		</table>
	</div>
</div>
</div>
<br>
<br>
<br>
<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name,filename) {
    if (!table.nodeType) table = document.getElementById(table)
            var newtable=document.createElement("table");
            var tbody =document.createElement("tbody");
             var row = document.createElement("tr");
             row.setAttribute("style", "background-color: black;color: white; font-family:'Source Sans Pro';");
  				var cell1 = document.createElement("td");
  				cell1.colSpan=7;
  				cell1.innerHTML ="Date as on : - <?php echo date('jS \ F Y'); ?>";
  				row.appendChild(cell1);
 			 	tbody.appendChild(row);
 			 	newtable.appendChild(tbody);
             var clonedTable=newtable.cloneNode(true);
             var clonedTable1=table.cloneNode(true);
            clonedTable.appendChild(clonedTable1);
    var ctx = {worksheet: name || 'Worksheet', table: clonedTable.innerHTML}
    //window.location.href = uri + base64(format(template, ctx))
     var link = document.createElement('a');
    link.download =filename;
    link.href = uri + base64(format(template, ctx));
    link.click();
     clonedTable.remove();
  }
})()
</script>
<script>

	
$(document).ready(function(){
$('[data-toggle="tooltip"]').tooltip();

$("#search_state").trigger('change');


});
$('#search_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');
					//$('#input_district').val($('#input_district').val());
				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
					$('#mstfacilitylogin').html(data);
					
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

</script>