<?php 
$loginData = $this->session->userdata('loginData');

$sql = "SELECT Baseline FROM `MSTRole` where RoleId = ".$loginData->RoleId;
$result = $this->db->query($sql)->result();
//echo '/'.$patient_data[0]->ChildScore;
?>

<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #A30A0C !important;
	background-color: white !important;
	border : 1px solid #A30A0C;
}
</style>
<style>
	select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
.tooltip
{
	min-width: 200px;
}
</style>
<br>
<div class="row equal">

	<!-- <form action="" name="patient_form" id="patient_form" method="POST"> -->
					<?php
           $attributes = array(
              'id' => 'patient_form',
              'name' => 'patient_form',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>

		<input type="hidden" name="fetch_uid" id="fetch_uid">
	     <?php echo form_close(); ?>
<!-- 		<input type="hidden" name="fetch_uid" id="fetch_uid">
	</form> -->
	<div class="col-lg-10 col-lg-offset-1">

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="btn-group">
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">1. Registration</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">2. Screening</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">3. Viral Load</a>
					<a class="btn btn-success" href="<?php echo base_url(); ?>patientinfo/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">4. Testing</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/known_history/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">5. Known History</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">6. Prescription</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">7. Dispensation</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">8. SVR</a>
				</div>
			</div>
		</div>

		<!-- <form action="" method="POST" name="registration" id="registration"> -->
						<?php
           $attributes = array(
              'id' => 'registration',
              'name' => 'registration',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>

           <?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){ ?>
			<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				<?php } ?>
			<div class="row">
				<h4 class="text-center"><p>Patient Name - <strong><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FirstName)):''; ?></strong>  (<?php echo (count($patient_data) > 0)?$patient_data[0]->UID_Prefix:$uid_prefix; echo '-' .str_pad($patient_data[0]->UID_Num, 6, '0', STR_PAD_LEFT); ?>)<p></h4>
				<div class="col-md-10" style="padding-right: 0;">
					
					<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Patient Testing Module <?php echo set_hepc(); ?></h3>
				</div>
				<div class="col-md-2" style="padding-left: 0;">
					<h3><a href="<?php echo base_url(); ?>patientinfo/patient_testing_followup_visit/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>" class="btn btn-success btn-block" style="border-radius: 0; line-height: 2.2; font-weight: 500; font-weight: 600; padding-left: 5px; border : 2px solid #A30A0C;">Add FollowUp Visit</a></h3>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4">
					<h4><b>BASELINE TEST DETAILS</b></h4>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<label for="">Date of Prescribing Tests <span class="text-danger">*</span></label>
					<input type="text" name="date_of_prescribing_tests" id="date_of_prescribing_tests" class="input_fields form-control hasCal dateInpt input2" value="<?php echo (count($patient_cirrohosis_data) > 0)?timeStampShow($patient_cirrohosis_data[0]->Prescribing_Dt):''; ?>" required onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" >
					<br class="hidden-lg-*">
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<label for="" style="font-size: 12px;">Date of issue of last investigation report <span class="text-danger">*</span></label>
					<input type="text" required="" name="last_test_result_date" id="last_test_result_date" class="input_fields form-control hasCal dateInpt input2" value="<?php echo (count($patient_cirrohosis_data) > 0)?timeStampShow($patient_cirrohosis_data[0]->LastTest_Dt):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
					<br class="hidden-lg-*">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<label for="">Haemoglobin (g/dL)<span class="text-danger">*</span></label>
					<input type="text" onkeypress="return onlyNumbersWithDot(event);" name="haemoglobin" id="haemoglobin" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Haemoglobin:''; ?>" required maxlength="4">
					<br class="hidden-lg-*">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<label for="">S. Albumin (g/dL)<span class="text-danger">*</span></label>
					<input type="text" onkeypress="return onlyNumbersWithDot(event);" name="albumin" id="albumin" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Albumin:''; ?>" required maxlength="4">
					<br class="hidden-lg-*">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<label for="">Serum Bilirubin Total (mg/dL) <span class="text-danger">*</span></label>
					<input type="text" onkeypress="return onlyNumbersWithDot(event);" name="bilirubin" id="bilirubin" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Bilrubin:''; ?>" required maxlength="4">
					<br class="hidden-lg-*">
				</div>
				
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<label for="">ALT (IU/L)<span class="text-danger">*</span></label>
					<input type="text" onkeypress="return onlyNumbersWithDot(event);" name="alt" id="alt" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->ALT:''; ?>" required maxlength="4">
					<br class="hidden-lg-*">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<label for="">AST (IU/L)<span class="text-danger">*</span></label>
					<input type="text" onkeypress="return onlyNumbersWithDot(event);" name="ast" id="ast" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->AST:''; ?>" required maxlength="4">
					<br class="hidden-lg-*">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<label for="">AST ULN (Upper Limit of Normal) (IU/L)<span class="text-danger">*</span></label>
					<input type="text"  onkeypress="return onlyNumbersWithDot(event);" name="ast_uln" id="ast_uln" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->AST_ULN:''; ?>" required maxlength="4">
					<br class="hidden-lg-*">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<label for="">Platelet Count (per microlitre)<span class="text-danger">*</span></label>
					<input type="text" onkeypress="return onlyNumbersWithDot(event);" name="platelet_count" id="platelet_count" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Platelets:''; ?>" required maxlength="8">
					<br class="hidden-lg-*">
				</div>
			</div>
			<div class="row">

				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<label for="">Weight (in Kgs) <?php  if($loginData->State_ID != 3 ){ ?><span class="text-danger">*</span> <?php } ?></label>
					<input type="text"  onkeypress="return onlyNumbersWithDot(event);" name="weight" id="weight" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->Weight:''; ?>" <?php  if($loginData->State_ID != 3 ){ ?> required <?php } ?>  maxlength="3">
					<br class="hidden-lg-*">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 creatinine">
					<label for="">S. Creatinine (in mg/dL) </label>
					<input type="text" onkeypress="return onlyNumbersWithDot(event);" name="creatinine" id="creatinine" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Creatinine:''; ?>" maxlength="5" > 
					<br class="hidden-lg-*">
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 egfrhideshow">
					<label for="">eGFR (estimated glomerular filtration rate) (mL/min/1.73 m2)</label>
					<input type="text" onkeypress="return onlyNumbersWithDot(event);" name="egfr" id="egfr" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->V1_EGFR:''; ?>" readonly >
					<br class="hidden-lg-*">
				</div>
			</div>
	
			<br class="visible-lg-*">
			<div class="row">
				<div class="col-md-12">
					<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">Criteria for Evaluating Cirrhosis</h4>
				</div>
			</div>
			<br>
			
			<div class="row">
				<div class="col-md-2">
					<label class="checkbox-inline"><input type="checkbox" value="1" name="ultrasound" id="ultrasound" style="width: 20px; height: 20px;" <?php echo (count($patient_cirrohosis_data) > 0 && $patient_cirrohosis_data[0]->Clinical_US == 1)?'checked':''; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Ultrasound</b></label>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ultrasound_fields">
					<label for="">Ultrasound Date</label>
					<input type="text" name="ultrasound_date" id="ultrasound_date" class="input_fields form-control hasCal dateInpt input2" value="<?php echo (count($patient_cirrohosis_data) > 0)?timeStampShow($patient_cirrohosis_data[0]->Clinical_US_Dt):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
					<br class="hidden-lg-*">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-2">
					<label class="checkbox-inline"><input type="checkbox" value="1" name="fibroscan" id="fibroscan" style="width: 20px; height: 20px;" <?php echo (count($patient_cirrohosis_data) > 0 && $patient_cirrohosis_data[0]->Fibroscan == 1)?'checked':''; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Fibroscan</b></label>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fibroscan_fields">
					<label for="">Fibroscan Date</label>
					<input type="text" onkeypress="return onlyNumbersWithDot(event);" name="fibroscan_date" id="fibroscan_date" class="input_fields form-control hasCal dateInpt input2" value="<?php echo (count($patient_cirrohosis_data) > 0)?timeStampShow($patient_cirrohosis_data[0]->Fibroscan_Dt):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
					<br class="hidden-lg-*">
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fibroscan_fields">
					<label for="">LSM value (in Kpa)</label>
					<input type="text" onkeypress="return onlyNumbersWithDot(event);" name="fibroscan_lsm" id="fibroscan_lsm" class="input_fields form-control" value="<?php echo (count($patient_cirrohosis_data) > 0)?$patient_cirrohosis_data[0]->Fibroscan_LSM:''; ?>" maxlength="5">
					<br class="hidden-lg-*">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-2">
					<label class="checkbox-inline"><input type="checkbox" value="1" name="apri" id="apri" style="width: 20px; height: 20px;" <?php echo (count($patient_cirrohosis_data) > 0 && $patient_cirrohosis_data[0]->APRI == 1)?'checked':''; ?> checked><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">APRI</b></label>
				</div>
				<!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 apri_fields">
					<label for="">APRI Date</label>
					<input type="date" name="apri_date" id="apri_date" class="input_fields form-control" value="" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
					<br class="hidden-lg-*">
				</div> -->
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 apri_fields">
					<label for="">APRI Score</label>
					<input type="text" onkeypress="return onlyNumbersWithDot(event);" name="apri_score" id="apri_score" class="input_fields form-control" value="<?php echo (count($patient_cirrohosis_data) > 0)?$patient_cirrohosis_data[0]->APRI_Score:''; ?>" readonly> <?php if( (count($patient_cirrohosis_data) > 0) && $patient_cirrohosis_data[0]->APRI_Score >2 ) { ?> <p id="apri_textshow2">Patient may be a complicated Hepatitis C patient/Cirrhotic</p><?php }elseif((count($patient_cirrohosis_data) > 0) && $patient_cirrohosis_data[0]->APRI_Score <2 ) { ?> <p id="apri_textshow2">Patient may be a non-complicated Hepatitis C patient/Non-cirrhotic</p> <?php } ?>
					<span id="apri_textshow1"></span>
						
					<br class="hidden-lg-*">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-2">
					<label class="checkbox-inline"><input type="checkbox" value="1" name="fib4" id="fib4" style="width: 20px; height: 20px;" <?php echo (count($patient_cirrohosis_data) > 0 && $patient_cirrohosis_data[0]->FIB4 == 1)?'checked':''; ?> checked><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">FIB 4</b></label>
				</div>
			<!-- 	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fib4_fields">
					<label for="">FIB 4 Date</label>
					<input type="date" onkeypress="return onlyNumbersWithDot(event);" name="fib4_date" id="fib4_date" class="input_fields form-control" value="">
					<br class="hidden-lg-*">
				</div> -->
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fib4_fields">
					<label for="">FIB 4 Score</label>
					<input type="text" onkeypress="return onlyNumbersWithDot(event);" name="fib4_score" id="fib4_score" class="input_fields form-control" value="<?php echo (count($patient_cirrohosis_data) > 0)?$patient_cirrohosis_data[0]->FIB4_FIB4:''; ?>" readonly><?php if( (count($patient_cirrohosis_data) > 0) && $patient_cirrohosis_data[0]->FIB4_FIB4 <='3.25') { ?> <p id="fib_textshow2">Patient may be a non-complicated Hepatitis C patient/Non-cirrhotic</p><?php }elseif((count($patient_cirrohosis_data) > 0) && $patient_cirrohosis_data[0]->FIB4_FIB4 >'3.25') { ?> <p id="fib_textshow2">Patient may be a complicated Hepatitis C patient/Cirrhotic</p> <?php } ?>
					<span id="fib_textshow1">
					<br class="hidden-lg-*">
				</div>
			</div>
			<br>
			<br>
			<div class="row">
				<div class="col-md-12">
					<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">Complicated/Uncomplicated</h4>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<label for="">Complicated/Uncomplicated <span class="text-danger">*</span><span style="margin-left: 10px; font-size: 14px;position: absolute;margin-top: 0px;" class="glyphicon glyphicon-info-sign" data-toggle='tooltip' title="1.Uncomplicated patients are those who are non-cirrhotic based on APRI/FIB-4 score,  
					2.Complicated patients are those who are cirrhotic based on APRI/FIB-4 score"></span></label>
					<select name="complicated_uncomplicated" id="complicated_uncomplicated" class="form-control input_fields">
						<option value="">Select</option>
						<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->V1_Cirrhosis == 1)?'selected':''; ?>>Complicated</option>
						<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->V1_Cirrhosis == 2)?'selected':''; ?>>Uncomplicated</option>
					</select>
				</div>
				
			</div>
			<br class="visible-lg-*">

			<div class="row compensated_decompensated_field">
				<div class="col-md-12">
					<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;" id="compensated_decompensated_label"><?php if(count($patient_data) > 0 && $patient_data[0]->Result == 1) { ?> Compensated Cirrhosis <?php } elseif(count($patient_data) > 0 && $patient_data[0]->Result == 2) {  ?>Decompensated Cirrhosis <?php } ?></h4>
				</div>
			</div>
			<br>
			<!-- <div class="row compensated_decompensated_field"> -->
				<div class="row complicated_field">
			<!-- 	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<label id="cirrohsis_test_date_label" for="cirrohsis_test_date">Date</label>
					<input type="text" name="cirrohsis_test_date" id="cirrohsis_test_date" class="input_fields form-control hasCal dateInpt input2" value="<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->Cirr_TestDate):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
					<br class="hidden-lg-*">
				</div> -->
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
					<label id="variceal_bleed_label" for="variceal_bleed">Variceal Bleed</label>
					<select name="variceal_bleed" id="variceal_bleed" class="form-control input_fields">
						<option value="">Select</option>
						<?php foreach ($options_variceal_bleed as $row) {?>
							<option <?php echo (count($patient_data) > 0 && $patient_data[0]->Cirr_VaricealBleed == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
						<?php } ?>
					</select>
					<br class="hidden-lg-*">
				</div>
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
					<label id="ascites_label" for="ascites">Ascites</label>
					<select name="ascites" id="ascites" class="form-control input_fields">
						<option value="">Select</option>
						<?php foreach ($options_ence_ascites as $row) {?>
							<option <?php echo (count($patient_data) > 0 && $patient_data[0]->Cirr_Ascites == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
						<?php } ?>
					</select>
					<br class="hidden-lg-*">
				</div>
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
					<label id="encephalopathy_label" for="encephalopathy">Encephalopathy</label>
					<select name="encephalopathy" id="encephalopathy" class="form-control input_fields">
						<option value="">Select</option>
						<?php foreach ($options_ence_ascites as $row) {?>
							<option <?php echo (count($patient_data) > 0 && $patient_data[0]->Cirr_Encephalopathy == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
						<?php } ?>
					</select>
					<br class="hidden-lg-*">
				</div>

				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<label for="">PT INR <?php  if($loginData->State_ID != 3 ){ ?><span class="text-danger">*</span><?php } ?></label>
					<input type="text" onkeypress="return onlyNumbersWithDot(event);" name="inr" id="inr" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->V1_INR:''; ?>" <?php  if($loginData->State_ID != 3 ){ ?> required <?php } ?>  maxlength="4">
					<br class="hidden-lg-*">
				</div>

				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
					<label id="child_score_label" for="child_score">Child Pugh Score</label>

					<input type="text" name="child_score" id="child_score" class="input_fields form-control" value="<?php if(count($patient_data) > 0 && $patient_data[0]->ChildScore == 5 || $patient_data[0]->ChildScore == 6) { echo 'A'; } elseif(count($patient_data) > 0 && $patient_data[0]->ChildScore <=7 ||  $patient_data[0]->ChildScore <=9) { echo 'B'; } elseif(count($patient_data) > 0 && $patient_data[0]->ChildScore >=10 ||  $patient_data[0]->ChildScore <=15) { echo 'C'; } else { echo ''; }?>" readonly>

<br class="visible-lg-*">
					<input type="hidden" id="child_score1" name="child_score1" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->ChildScore:''; ?>">
<!-- 								  <script type="text/javascript">    if($('#child_score').val() ==5 || $('#child_score').val()==6){
            $('#child_score').te("A");
        }else if($('#child_score').val() >=7 && $('#child_score').val() <=9){
            $('#child_score').val("B");
        }else if($('#child_score').val() >=10 && $('#child_score').val() <=15){
            $('#child_score').val("C");
        }
</script> -->  
<input type="hidden" name="interruption_status" id="interruption_status" value="<?php  echo (count($patient_data) > 0)?$patient_data[0]->InterruptReason:'';  ?>">
					<br class="hidden-lg-*">
				</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 complicated_field">
					<label for="">Severity of HEP-C</label>
					<select name="compensated_decompensated" id="compensated_decompensated" class="form-control input_fields">
						<option value="">Select</option>
						<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->Result == 1)?'selected':''; ?>>Compensated Cirrhosis</option>
						<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->Result == 2)?'selected':''; ?>>Decompensated Cirrhosis</option>
					</select>
				</div>

			</div>
			<div class="row">
			
				</div>

			<br/>
			<div class="row">
				<div class="col-lg-3 col-md-2">
					<a class="btn btn-block btn-default form_buttons" href="<?php echo base_url('patientinfo?p=1'); ?>" id="close" name="close" value="close">CLOSE</a>
				</div>
				<div class="col-lg-3 col-md-2">
					<button class="btn btn-block btn-default form_buttons" id="refresh" name="refresh" value="refresh">REFRESH</button>
				</div>
						<?php if($loginData->RoleId!=99){ ?>
				<div class="col-lg-3 col-md-2">
					<a href="" class="btn btn-block btn-default form_buttons" id="lock" name="lock" value="lock">LOCK</a>
				</div>
			<?php } else{?>
				<div class="col-lg-3 col-md-2">
					<a href="javascript:void(0)" class="btn btn-block btn-default form_buttons" onclick="openPopupUnlock()" id="" name="unlock" value="lock">LOCK</a>
				</div>
			<?php } ?>
				<?php if($result[0]->Baseline == 1) {?>
					<div class="col-lg-3 col-md-3">
						<button class="btn btn-block btn-success form_buttons" id="save" name="save" value="save">SAVE</button>
					</div>
				<?php } ?>
			</div>
			<input type="hidden" class="input_fields form-control hasCal dateInpt input2" name="T_DLL_01_Daten" id="T_DLL_01_Daten" value="<?php echo timeStampShow($patient_data[0]->T_DLL_01_Date); ?>">
			<br>
			<br>
			<br>
 <?php echo form_close(); ?>
			<div class="row" class="text-left">

					<div class="col-md-6 card well">
					 <label class="col-sm-4"  style="text-align: left !important; line-height: 2.2 !important; ">Patient's Status -</label>
					<div class="col-sm-8" style="text-align: left !important; line-height: 2.2 !important; ">
					<label><?php echo (count($patient_status) > 0)?$patient_status[0]->status:''; ?></label>

					</div>
				</div>

				

				<div class="col-md-6 card well">
					<label class="col-sm-6" style="text-align: left !important; ">Patient's Interruption Status </label>
					<div class="col-sm-6">
					<select name="" id="type_val" onchange="openPopup()" class="btn" style="text-align: right!important;">
						<option value="">Select</option>
						<option value="1"  <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason != '')?'selected':''; ?> >Yes</option>
						<option value="0" <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason == '')?'selected':''; ?>>No</option>
					</select>
				
				</div>
				</div>
			
			</div>

		         
		<!-- </form> -->
	</div>
</div>

<br/><br/><br/>

<div class="modal fade" id="addMyModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      
        <h4 class="modal-title">Patient's Interruption Status</h4>
      </div>
      <span id='form-error' style='color:red'></span>
      <div class="modal-body">
      <!--   <form role="form" id="newModalForm" method="post"> -->
      	<?php
           $attributes = array(
              'id' => 'newModalForm',
              'name' => 'newModalForm',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>


				<div class="form-group">
				<label class="control-label col-md-3" for="email">Reason:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonval" name="resonval" required="required">
				<option value="">Select</option>
				<?php foreach ($InterruptReason as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>

				<div class="form-group resonfielddeath" >
				<label class="control-label col-md-3" for="email">Reason for death:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonvaldeath" name="resonvaldeath">
				<option value="">Select</option>
				<?php foreach ($reason_death as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
				
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>

				<div class="form-group resonfieldlfu">
				<label class="control-label col-md-3" for="email">Reason for LFU:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonfluid" name="resonfluid">
				<option value="">Select</option>
				<?php foreach ($reason_flu as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
				
			<?php } ?>
				</select> 
				</div>
				</div>
<div class="form-group resonfieldothers" >
				<label class="control-label col-md-3" for="email">Other(specify):</label>
				<div class="col-md-9">
				<input type="text" class="form-control" id="otherInterrupt" name="otherInterrupt">
				</div>
				</div>
<br/><br/>


          <div class="modal-footer">
            <button type="submit" class="btn btn-success" id="btnSaveIt">Save</button>
            <button type="button" class="btn btn-default" id="btnCloseIt" data-dismiss="modal">Close</button>
          </div>
           <?php echo form_close(); ?>
       <!--  </form> -->
      </div>
    </div>
  </div>
</div>

<!-- Unlock box start-->
<div class="modal fade" id="addMyModalUnbox" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title">Unlock Process</h4>
      </div>
      <span id='form-error' style='color:red'></span>
      <div class="modal-body">
      <!--   <form role="form" id="newModalForm" method="post"> -->
      	<?php
           $attributes = array(
              'id' => 'unlockModalForm',
              'name' => 'unlockModalForm',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>


				<div class="form-group">
				<label class="control-label col-md-3" for="email">Reason:</label>
				<div class="col-md-9">
				<select class="form-control" id="unlockresonval" name="unlockresonval" required="required">
					<option value="">Select</option>
				<?php foreach ($unlockprocess as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>

<div class="form-group unlockresonfieldothers" >
				<label class="control-label col-md-3" for="email">Other(specify):</label>
				<div class="col-md-9">
				<input type="text" class="form-control" id="unlockotherInterrupt" name="unlockotherInterrupt">
				</div>
				</div>
<br/><br/>


          <div class="modal-footer">
            <button type="submit" class="btn btn-success" id="unlock">Save</button>
            <button type="button" class="btn btn-default" id="btnunlock" data-dismiss="modal">Close</button>
          </div>
           <?php echo form_close(); ?>
       <!--  </form> -->
      </div>
    </div>
  </div>
</div>
<br/><br/><br/>
<!-- end unlock -->

<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>

<script>
$('[data-toggle="tooltip"]').tooltip();

	$(document).ready(function(){

		childscore();
if($('#child_score1').val()!='0.0'){
		checkcomdes();
	}


		if($('#complicated_uncomplicated').val()==''){
			
			$('#complicated_uncomplicated').val('2');
			
			
		}

		if($('#complicated_uncomplicated').val()=='1'){
			<?php  if($loginData->State_ID != 3 ){ ?>
			$('#inr').attr('required', 'required');
		<?php } ?>

			
		}else{
			
		$('#inr').removeAttr('required');	
		}



	});

<?php //if($loginData->State_ID  !=3 ){ ?>
function checkcomdes(){
	if($('#apri_score').val() >2 || $('#fib4_score').val() > 3.2 ){

$('#complicated_uncomplicated').val('1');
<?php  if($loginData->State_ID != 3 ){ ?>
$('#inr').attr('required', 'required');
<?php } ?>
$('.complicated_field').show();	
}else{
	$('#complicated_uncomplicated').val('2');
	$('#inr').removeAttr('required');	
$('.complicated_field').hide();	
}

//if($('#child_score1').val() >= 7 || $('#variceal_bleed').val()=='1'){
if($('#child_score1').val()==5 || $('#child_score1').val()==6){

$('#compensated_decompensated').val('1');
//alert(1);
$("#variceal_bleed").removeAttr('required');
$("#variceal_bleed").removeAttr('required');
$("#ascites").removeAttr('required');
$("#encephalopathy").removeAttr('required');
$("#inr").removeAttr('required');
$("#child_score1").removeAttr('required');

}else{
//alert(1);
	$('#compensated_decompensated').val('2');
}



}

<?php //} ?>



	$(document).ready(function(){
		
	<?php if(count($patient_data) > 0 && $patient_data[0]->Result == 1) { ?>
				//alert('if');
					$(".compensated_decompensated_field").show();
					$("#compensated_decompensated_label").html('Compensated Cirrhosis');
					
					$("#cirrohsis_test_date_label").html('Date');
					$("#variceal_bleed_label").html('Variceal Bleed');
					$("#ascites_label").html('Ascites');
					$("#encephalopathy_label").html('Encephalopathy');
					$("#child_score_label").html('Child Pugh Score');

					$("#cirrohsis_test_date").removeAttr('required');
					$("#variceal_bleed").removeAttr('required');
					$("#ascites").removeAttr('required');
					$("#encephalopathy").removeAttr('required');
					$("#child_score").removeAttr('required');
				
			<?php	} elseif(count($patient_data) > 0 && $patient_data[0]->Result == 2){ ?>
				
					$(".compensated_decompensated_field").show();
					$("#compensated_decompensated_label").html('Decompensated Cirrhosis');
					
					$("#cirrohsis_test_date_label").html('Date <span class="text-danger">*</span>');
					$("#variceal_bleed_label").html('Variceal Bleed <span class="text-danger">*</span>');
					$("#ascites_label").html('Ascites <span class="text-danger">*</span>');
					$("#encephalopathy_label").html('Encephalopathy <span class="text-danger">*</span>');
					$("#child_score_label").html('Child Pugh Score <span class="text-danger">*</span>');

					//$("#cirrohsis_test_date").attr('required', 'required');
					$("#variceal_bleed").attr('required', 'required');
					$("#ascites").attr('required', 'required');
					$("#encephalopathy").attr('required', 'required');
					$("#child_score").attr('required', 'required');
				<?php } else { ?>
				
					$(".compensated_decompensated_field").hide();
				<?php } ?>
	});
</script>

<script>
	
$(document).ready(function(){

var statusval			 = '<?php echo $patient_data[0]->MF4; ?>';
var InterruptReason 	= '<?php echo $patient_data[0]->InterruptReason; ?>';

if(statusval==0 && InterruptReason==1){

					$("#modal_header").text("Further entry is not allowed,as per treatment status...");
					$("#modal_text").text("Not allowed");
					$("#multipurpose_modal").modal("show");
				
	location.href='<?php echo base_url(); ?>patientinfo/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
				
}
});


	function openPopup() {
		var type_val = $('#type_val').val();
		if(type_val=='1'){
    $("#addMyModal").modal();
		}
}

$('.resonfielddeath').hide();
$('.resonfieldlfu').hide();
$('.resonfieldothers').hide();

$('#btnCloseIt').click(function(){

$('#type_val').val('0');
$('#interruption_status').val('');

});

$('#type_val').change(function(){
var type_val = $('#type_val').val();
$('#interruption_status').val(type_val);

});


 $('#btnSaveIt').click(function(e){
      
      e.preventDefault(); 
      $("#form-error").html('');
     
        
	 var formData = new FormData($('#newModalForm')[0]);
	 $('#btnSaveIt').prop('disabled',true);
	 $.ajax({				    	
		url: '<?php echo base_url(); ?>patientinfo/interruptionstatus_process/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
		data : {
		<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
		},
		type: 'POST',
		//data: formData,
		dataType: 'json',
		//async: false,
        //cache: false,
		//contentType: false,
		//processData: false,
		success: function (data) { 
				
			if(data['status'] == 'true'){
				$("#form-error").html('Data has been successfully submitted');
				setTimeout(function() {
    					location.href='<?php echo base_url(); ?>patientinfo/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
				}, 1000);

			}else{
				$("#form-error").html(data['message']);
				$('#btnSaveIt').prop('disabled',false); 
				return false;
			}   
		}        
	 }); 
		
   

    }); 

function openPopupUnlock() {
		
		$("#addMyModalUnbox").modal();
		
		}
		
		$('.unlockresonfieldothers').hide();
		$('#unlockresonval').change(function(){
		var unlockresonval = $('#unlockresonval').val();
		if(unlockresonval  ==4){
		$('.unlockresonfieldothers').show();
		}else{
		$('.unlockresonfieldothers').hide();	
		}
		});

	$('#resonval').change(function(){
		

		if($('#resonval').val() == '1'){

		$('.resonfielddeath').show();
		$('.resonfieldlfu').hide();
		$('.resonfieldothers').hide();
		$('#resonfluid').val('');

		$('#resonvaldeath').prop('required',true);
		$('#resonfluid').prop('required',false);
		$('#otherInterrupt').prop('required',false);

	}else if($('#resonval').val() == '2'){
		$('.resonfielddeath').hide();
		$('.resonfieldlfu').show();
		$('.resonfieldothers').hide();
		$('#resonvaldeath').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',true);
		$('#otherInterrupt').prop('required',false);
	}else if($('#resonval').val() == '99'){

		$('.resonfieldothers').show();
		$('.resonfielddeath').hide();
		$('.resonfieldlfu').hide();
		$('#otherInterrupt').prop('required',true);
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',false);
	}

	else{

		$('.resonfielddeath').hide();
		$('.resonfieldlfu').hide();
		$('.resonfieldothers').hide();
		$('#resonvaldeath').val('');
		$('#resonfluid').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',false);
		$('#otherInterrupt').prop('required',false);
	}

});

</script>
<?php //if(count($patient_data) > 0 && $patient_data[0]->CirrhosisStatus == 2 || $patient_data[0]->CirrhosisStatus == 1) {
 if(count($patient_data) > 0 && $patient_data[0]->V1_Cirrhosis == 1 ) {  ?>
<script>
	
$(".compensated_decompensated_field").show();
</script>
	<?php } else{ ?>
<script>
$(".compensated_decompensated_field").hide();
</script>





	<?php } ?>

	<?php if(count($patient_data) > 0 && $patient_data[0]->MF5 == 1) {?>
	<script>
			
			$("#lock").click(function(){
				$("#modal_header").text("Contact admin ,for unlocking the record");
					$("#modal_text").text("Contact Admin");
					$("#multipurpose_modal").modal("show");
					return false;

				});

			/*Unlock process start*/
				
	$("#unlock").click(function(e){
	e.preventDefault(); 
      $("#form-error").html('');


	 	var formData = new FormData($('#unlockModalForm')[0]);
		 $.ajax({				    	
			url: '<?php echo base_url(); ?>Unlock/unlock_process_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
			type: 'POST',
			data: formData,
			dataType: 'json',
			async: false,
	        cache: false,
			contentType: false,
			processData: false,
			success: function (data) { 
				
			if(data['status'] == 'true'){
				alert('Data has been successfully unlock');
				setTimeout(function() {
    					location.href='<?php echo base_url(); ?>patientinfo/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
				}, 1000);

				}else{
				$("#form-error").html(data['message']);
				
				return false;
				}   
				}        
				}); 
				
				
				});

				/*end unlock process*/


				/*Disable all input type="text" box*/
				//alert('<?php echo $patient_data[0]->MF5; ?>');
				$('#registration input[type="text"]').attr("readonly", true);
				$('#registration input[type="checkbox"]').attr("disabled", true);
				$('#registration input[type="date"]').attr("readonly", true);
				$("#save").attr("disabled", true);
				$("#refresh").attr("disabled", true);
				$('#registration select').attr('disabled', true);
				/*Disable textarea using id */
				$('#registration #txtAddress').prop("readonly", true);
		
		</script>
<?php  } ?>



<?php if($result[0]->Baseline == 1) {?>
	<script>
		
		$(document).ready(function(){

			<?php 
			$error = $this->session->flashdata('error'); 

			if($error != null)
			{
				?>
				$("#multipurpose_modal").modal("show");
				<?php 
			}
			?>

			$("#refresh").click(function(e){
			if(confirm('All further details will be deleted.Do you want to continue?')){ 
				$("input").val('');
				$("select").val('');
				$("textarea").val('');
				$("#input_district").html('<option>Select District</option>');
				$("#input_block").html('<option>Select Block</option>');

				e.preventDefault();
			}
			});

			
			$(".complicated_field").hide();

			<?php if(count($patient_cirrohosis_data) > 0 && $patient_cirrohosis_data[0]->Clinical_US == 1) {?>
				$(".ultrasound_fields").show();
			<?php } else {?>
				$(".ultrasound_fields").hide();
			<?php } ?>

			<?php if(count($patient_cirrohosis_data) > 0 && $patient_cirrohosis_data[0]->Fibroscan == 1) {?>
				$(".fibroscan_fields").show();
			<?php } else {?>
				$(".fibroscan_fields").hide();
			<?php } ?>

			<?php if(count($patient_cirrohosis_data) > 0 && $patient_cirrohosis_data[0]->APRI == 1) {?>
				$(".apri_fields").show();
			<?php } else {?>
				$(".apri_fields").show();
			<?php } ?>

			<?php if(count($patient_cirrohosis_data) > 0 && $patient_cirrohosis_data[0]->FIB4 == 1) {?>
				$(".fib4_fields").show();
			<?php } else {?>
				$(".fib4_fields").show();
			<?php } ?>

			

			$('#table_patient_list tbody tr').click(function(){
				window.location = $(this).data('href');
			});


				<?php if(count($patient_data) > 0 && $patient_data[0]->V1_Cirrhosis == 1) { ?>
				
					$(".complicated_field").show();
				<?php } else{ ?>
			

					$(".complicated_field").hide();
				<?php } ?>



			$("#complicated_uncomplicated").change(function(){

				if($(this).val() == 1)
				{
					$(".complicated_field").show();
					$('#compensated_decompensated').val('');
					$('#compensated_decompensated').attr('required', 'required');
					<?php  if($loginData->State_ID != 3 ){ ?>
					$('#inr').attr('required', 'required');
				<?php } ?>
				}
				else
				{

					$(".complicated_field").hide();
					$(".compensated_decompensated_field").hide();
					$('#compensated_decompensated').val('');
					$('#compensated_decompensated').removeAttr('required');

					$("#variceal_bleed").removeAttr('required');
					$("#ascites").removeAttr('required');
					$("#encephalopathy").removeAttr('required');
					$("#child_score").removeAttr('required');
					$('#inr').removeAttr('required');

				}
			});

			$("#compensated_decompensated").change(function(){

				if($(this).val() == 1)
				{
					$(".compensated_decompensated_field").show();
					$("#compensated_decompensated_label").html('Compensated Cirrhosis');
					
					$("#cirrohsis_test_date_label").html('Date');
					$("#variceal_bleed_label").html('Variceal Bleed');
					$("#ascites_label").html('Ascites');
					$("#encephalopathy_label").html('Encephalopathy');
					$("#child_score_label").html('Child Pugh Score');

					$("#cirrohsis_test_date").removeAttr('required');
					$("#variceal_bleed").removeAttr('required');
					$("#ascites").removeAttr('required');
					$("#encephalopathy").removeAttr('required');
					$("#child_score").removeAttr('required');
				}
				else if($(this).val() == 2)
				{
					$(".compensated_decompensated_field").show();
					$("#compensated_decompensated_label").html('Decompensated Cirrhosis');
					
					$("#cirrohsis_test_date_label").html('Date <span class="text-danger">*</span>');
					$("#variceal_bleed_label").html('Variceal Bleed <span class="text-danger">*</span>');
					$("#ascites_label").html('Ascites <span class="text-danger">*</span>');
					$("#encephalopathy_label").html('Encephalopathy <span class="text-danger">*</span>');
					$("#child_score_label").html('Child Pugh Score <span class="text-danger">*</span>');

					//$("#cirrohsis_test_date").attr('required', 'required');
					$("#variceal_bleed").attr('required', 'required');
					$("#ascites").attr('required', 'required');
					$("#encephalopathy").attr('required', 'required');
					$("#child_score").attr('required', 'required');
				}
				else
				{
					$(".compensated_decompensated_field").hide();
				}
			});

			$("#ultrasound").change(function(){
				if($(this).is(':checked'))
				{
					$(".ultrasound_fields").show();
				}
				else
				{
					$(".ultrasound_fields").hide();
				}
			});

			$("#fibroscan").change(function(){
				if($(this).is(':checked'))
				{
					$(".fibroscan_fields").show();
				}
				else
				{
					$(".fibroscan_fields").hide();
				}
			});

			$("#apri").change(function(){
				if($(this).is(':checked'))
				{
					$(".apri_fields").show();
				}
				else
				{
					$(".apri_fields").show();
				}
			});

			$("#fib4").change(function(){
				if($(this).is(':checked'))
				{
					$(".fib4_fields").show();
				}
				else
				{
					$(".fib4_fields").show();
				}
			});

			$("#ast").change(function(){
				//alert('dddd');
				calculateAPRI();
				<?php //if($loginData->State_ID  !=3 ){ ?>
				checkcomdes();
			<?php //} ?>
			});

			$("#ast_uln").change(function(){
				//alert('eeee');
				calculateAPRI();
				<?php //if($loginData->State_ID  !=3 ){ ?>
				checkcomdes();
			<?php //} ?>
			});

			$("#platelet_count").change(function(){
				//alert('ffff');
				calculateAPRI();
				<?php //if($loginData->State_ID  !=3 ){ ?>
				checkcomdes();
			<?php //} ?>
			});
		});

function calculateAPRI()
{
	ast            = parseFloat($("#ast").val());
	ast_uln        = parseFloat($("#ast_uln").val());
	platelet_count = parseFloat($("#platelet_count").val());

	if(ast == '')
	{
		ast = 0;
	}
	if(ast_uln == '')
	{
		ast_uln = 0;
	}
	if(platelet_count == '')
	{
		platelet_count = 0;
	}

	//apri = (ast/ast*100)/(platelet_count/1000);
	apri = (ast/ast_uln*100)/(platelet_count/1000);
	//alert(apri);

if($("#ast").val()!='' && $("#ast_uln").val()!='' && $("#platelet_count").val()!=''){
	$("#apri_score").val(apri.toFixed(2));
	
	if(apri.toFixed(2) >2 ){
		
	$('#apri_textshow1').html('Patient may be a complicated Hepatitis C patient/Cirrhotic');
	$('#apri_textshow2').hide();
	
} else if(apri.toFixed(2) < 2){
	$('#apri_textshow1').html('Patient may be a non-complicated Hepatitis C patient/Non-cirrhotic');
	$('#apri_textshow2').hide();
}

}

}

function onlyNumbersWithDot(e) {           
            var charCode;
            if (e.keyCode > 0) {
                charCode = e.which || e.keyCode;
            }
            else if (typeof (e.charCode) != "undefined") {
                charCode = e.which || e.keyCode;
            }
            if (charCode == 46)
                return true
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }


/*$("#variceal_bleed").change(function(){
childscore();
checkcomdes();
});*/

// ChildScore
$childscore =  0

			$("#variceal_bleed").change(function(){
			   if($(this).val() < 2 )
				{
					var variceal_bleed = 1;
					// alert($bilirubin1).val(); 
				}
				if($(this).val() >= 2 && $(this).val() <= 3)
				{
					var variceal_bleed = 2;
					// alert($bilirubin1).val(); 					
				}
				 if($(this).val() > 3 )
				{
					var variceal_bleed = 3;
					// alert($bilirubin1).val(); 
				}
				// alert($("#bilirubin").val());
				childscore();
				<?php //if($loginData->State_ID  !=3 ){ ?>
				checkcomdes();
			<?php //} ?>
			});

			$("#bilirubin").change(function(){
			   if($(this).val() < 2 )
				{
					var bilirubin1 = 1;
					// alert($bilirubin1).val(); 
				}
				if($(this).val() >= 2 && $(this).val() <= 3)
				{
					var bilirubin1 = 2;
					// alert($bilirubin1).val(); 					
				}
				 if($(this).val() > 3 )
				{
					var bilirubin1 = 3;
					// alert($bilirubin1).val(); 
				}
				// alert($("#bilirubin").val());
				childscore();
				<?php //if($loginData->State_ID  !=3 ){ ?>
				checkcomdes();
			<?php //} ?>
			});

			$("#albumin").change(function(){
			   if($(this).val() > 3.5 )
				{
					var albumin1 = 1;
					// alert($albumin1).val(); 
				}
				if($(this).val() >= 2.8 && $(this).val() <= 3.5)
				{
					var albumin1 = 2;
					// alert($albumin1).val(); 					
				}
				 if($(this).val() < 2.8)
				{
					var albumin1 = 3;
					// alert($albumin1).val(); 
				}
				childscore();
				<?php //if($loginData->State_ID  !=3 ){ ?>
				checkcomdes();
			<?php //} ?>
			});

			$("#inr").change(function(){
			   if($(this).val() < 1.7 )
				{
					var inr1 = 1;
					// alert($inr1).val(); 
				}
				if($(this).val() >= 1.7 && $(this).val() <= 2.3)
				{
					var inr1 = 2;
					// alert($inr1).val(); 					
				}
				 if($(this).val() > 2.3 )
				{
					var inr1 = 3;
					// alert($inr1).val(); 
				}
				childscore();
				<?php //if($loginData->State_ID  !=3 ){ ?>
				checkcomdes();
			<?php //} ?>
			});

			$("#encephalopathy").change(function(){
			   if($(this).val() == 1 )
				{
					var encephalopathy1 = 1;
					// alert($encephalopathy1).val(); 
				}
				if($(this).val() == 2)
				{
					var encephalopathy1 = 2;
					// alert($encephalopathy1).val(); 					
				}
				 if($(this).val() == 3)
				{
					var encephalopathy1 = 3;
					// alert($encephalopathy1).val(); 
				}
				childscore();
				<?php //if($loginData->State_ID  !=3 ){ ?>
				checkcomdes();
			<?php //} ?>
			});

			$("#ascites").change(function(){
			   if($(this).val() == 1 )
				{
					var ascites1 = 1;
					// alert($ascites1).val(); 
				}
				if($(this).val() == 2)
				{
					var ascites1 = 2;
					// alert($ascites1).val(); 					
				}
				 if($(this).val() == 3 )
				{
					var ascites1 = 3;
					// alert($ascites1).val(); 
				}
				childscore();
				<?php //if($loginData->State_ID  !=3 ){ ?>
				checkcomdes();
			<?php //} ?>
			});



	function childscore(){
			 
			// alert($bilirubin1 + $albumin1 + $inr1 + $encephalopathy1 + $ascites1);
			//$('#child_score').val($bilirubin1 + $albumin1 + $inr1 + $encephalopathy1 + $ascites1);

			//alert($('#child_score1').val());


if($('#bilirubin').val()==''){
	var bilirubin1 =  0;
}else{
	var bilirubin1 =  $('#bilirubin').val();;
}
if($('#albumin').val()==''){
	var albumin1 =  0;
}else{
	var albumin1 =  $('#albumin').val();;
}
if($('#inr').val()==''){
	var inr1 =  0;
}else{
	var inr1 =  $('#inr').val();;
}
if($('#encephalopathy').val()==''){
	var encephalopathy1 =  0;
}else{
	var encephalopathy1 =  $('#encephalopathy').val();;
}
if($('#ascites').val()==''){
	var ascites1 =  0;
}else{
	var ascites1 =  $('#ascites').val();;
}

		$('#child_score1').val(parseFloat(bilirubin1) + parseFloat(albumin1) + parseFloat(inr1) + parseFloat(encephalopathy1) + parseFloat(ascites1));   

 //alert(parseFloat(bilirubin1) + parseFloat(albumin1) + parseFloat(inr1) + parseFloat(encephalopathy1) + parseFloat(ascites1));



			if($('#child_score1').val() ==5 || $('#child_score1').val()==6){

			$('#child_score').val("A");
		//alert('A');

			}else if($('#child_score1').val() <=7 || $('#child_score1').val() <=9){
			$('#child_score').val("B");
			//alert('B');
			}else if($('#child_score1').val() >=10 || $('#child_score1').val() <=15){

			$('#child_score').val("C");
//alert('C');
			}
var chaildscoresss = $('#child_score').val();

   
		}

// ChildScore end
</script>
<?php } else { ?>
	<script>
		$(document).ready(function(){
			$('input').prop('disabled', true);
			$('select').prop('disabled', true);
			$('textarea').prop('disabled', true);
		});

	</script>
	<?php } ?>
	<script>
					<?php if(count($patient_data) > 0 && $patient_data[0]->Weight == '') { ?>
					$(".creatinine").hide();
					//$(".egfrhideshow").hide();
				<?php } else { ?>
					$(".creatinine").show();
					//$(".egfrhideshow").show();
				<?php } ?>
					$("#weight").keyup(function(){
						
						if($(this).val() == "")
						{
						$(".creatinine").hide();
						//$(".egfrhideshow").hide();
						}
						else
						{
						$(".creatinine").show();
						//$(".egfrhideshow").show();
						//$('#creatinine').prop('required',true);
						}
						});


$("#haemoglobin" ).change(function( event ) {
	
var haemoglobin = $('#haemoglobin').val();

	if(haemoglobin <= 0){

					$("#modal_header").text("Enter Valide Value for Haemoglobin");
					$("#modal_text").text("haemoglobin greater than zero");
					$('#haemoglobin').val('');
					$("#multipurpose_modal").modal("show");
					return false;

		
	}
});


$("#albumin" ).change(function( event ) {
	
var albumin = $('#albumin').val();

	if(albumin <= 0){

					$("#modal_header").text("Please Enter greater than zero");
					$("#modal_text").text("albumin greater than zero");
					$('#albumin').val('');
					$("#multipurpose_modal").modal("show");
					return false;

		
	}
});

$("#bilirubin" ).change(function( event ) {
	
var bilirubin = $('#bilirubin').val();

	if(bilirubin <= 0){

					$("#modal_header").text("Please Enter greater than zero");
					$("#modal_text").text("bilirubin greater than zero");
					$('#bilirubin').val('');
					$("#multipurpose_modal").modal("show");
					return false;

		
	}
});

$("#inr" ).change(function( event ) {
	
var inr = $('#inr').val();

	if(inr <= 0){

					$("#modal_header").text("Please Enter greater than zero");
					$("#modal_text").text("inr greater than zero");
					$('#inr').val('');
					$("#multipurpose_modal").modal("show");
					return false;

		
	}
});

$("#alt" ).change(function( event ) {
	
var alt = $('#alt').val();

	if(alt <= 0){

					$("#modal_header").text("Please Enter greater than zero");
					$("#modal_text").text("alt greater than zero");
					$('#alt').val('');
					$("#multipurpose_modal").modal("show");
					return false;

		
	}
});


/*$("#ast" ).change(function( event ) {
	
var ast = $('#ast').val();

	if(ast <= 0){

					$("#modal_header").text("Please Enter greater than zero");
					$("#modal_text").text("ast greater than zero");
					$('#ast').val('');
					$("#multipurpose_modal").modal("show");
					return false;

		
	}
});*/



$("#ast_uln" ).change(function( event ) {
	
var ast_uln = $('#ast_uln').val();

	if(ast_uln <= 0){

					$("#modal_header").text("Please Enter greater than zero");
					$("#modal_text").text("ast_uln greater than zero");
					$('#ast_uln').val('');
					$("#multipurpose_modal").modal("show");
					return false;

		
	}
}); 

$("#platelet_count" ).change(function( event ) {
	
var platelet_count = $('#platelet_count').val();

	if(platelet_count <= 0){

					$("#modal_header").text("Please Enter greater than zero");
					$("#modal_text").text("platelet count greater than zero");
					$('#platelet_count').val('');
					$("#multipurpose_modal").modal("show");
					return false;

		
	}
}); 

$("#weight" ).change(function( event ) {
	
var weight = $('#weight').val();

	if(weight <= 0){

					$("#modal_header").text("Please Enter greater than zero");
					$("#modal_text").text("weight count greater than zero");
					$('#weight').val('');
					$("#multipurpose_modal").modal("show");
					return false;

		
	}
}); 

$("#creatinine" ).change(function( event ) {
	
var creatinine = $('#creatinine').val();

	if(creatinine <= 0){

					$("#modal_header").text("Please Enter greater than zero");
					$("#modal_text").text("creatinine count greater than zero");
					$('#creatinine').val('');
					$("#multipurpose_modal").modal("show");
					return false;

		
	}
}); 

$("#creatinine" ).change(function( event ) {
	
	
var weight = $('#weight').val();
var creatinine = $('#creatinine').val();
var patage = <?php echo $patient_data[0]->Age; ?>;
var patGender = <?php echo $patient_data[0]->Gender; ?>;
var IsAgeMonths = <?php echo $patient_data[0]->IsAgeMonths; ?>;
if(creatinine == ''){
	var creatininedata = 1;
}else{
	var creatininedata = $('#creatinine').val();
}
if(IsAgeMonths == 1){
	var patagecal = patage /12;
}else{
	var patagecal =<?php echo $patient_data[0]->Age; ?>;
}
if(patGender == 1){
	var genderageval = 1;
}else{
	var genderageval = 0.85;
}


var calculateeGFR = (140 - patagecal) * (weight * genderageval) / (72 * creatinine) ;
if($('#weight').val()!='' && $('#creatinine').val()!=''){
$('#egfr').val(calculateeGFR.toFixed(3));
//alert(calculateeGFR.toFixed(3));
}
});

$("#weight" ).change(function( event ) {
	
	
var weight = parseFloat($('#weight').val());
var creatinine = parseInt($('#creatinine').val());

var patage = '<?php echo $patient_data[0]->Age; ?>';
var patGender = '<?php echo $patient_data[0]->Gender; ?>';
var IsAgeMonths = '<?php echo $patient_data[0]->IsAgeMonths; ?>';

	var weightdata = parseFloat($('#weight').val());; 


	
	var creatininedata = parseFloat($('#creatinine').val());

if(IsAgeMonths == 1){
	var patagecal = patage /12;
}else{
	var patagecal ='<?php echo $patient_data[0]->Age; ?>';
}
if(patGender == 1){
	var genderageval = 1;
}else{
	var genderageval = '0.85';
}


var calculateeGFR = (140 - parseFloat(patagecal)) * (parseFloat(weightdata) * parseFloat(genderageval)) / (72 * parseFloat(creatininedata));
if($('#weight').val()!='' && $('#creatinine').val()!=''){
$('#egfr').val(calculateeGFR.toFixed(3));
//alert(calculateeGFR.toFixed(3));
}
});

$("#ast").change(function(){
				//alert($("#ast").val());
				calculateFIB4();
				<?php //if($loginData->State_ID  !=3 ){ ?>
				checkcomdes();
			<?php //} ?>
			});

			$("#alt").change(function(){
				// alert($("#alt").val());
				calculateFIB4();
				<?php //if($loginData->State_ID  !=3 ){ ?>
				checkcomdes();
			<?php //} ?>
			});

			$("#platelet_count").change(function(){
				//alert($("#platelet_count").val());
				calculateFIB4();
				<?php //if($loginData->State_ID  !=3 ){ ?>
				checkcomdes();
			<?php //} ?>
			});

function calculateFIB4()
{
	var ast            = $("#ast").val();
	var alt            = $("#alt").val();
	var platelet_count = $("#platelet_count").val();

	var patage = '<?php echo $patient_data[0]->Age; ?>';
	var IsAgeMonths = '<?php echo $patient_data[0]->IsAgeMonths; ?>';

	if(IsAgeMonths == 1){
	var age = patage /12;
	}else{
		var age ='<?php echo $patient_data[0]->Age; ?>';

	}
//Age*AST/(ALT)^(1/2)/(Platelet Count/1000)			

var ageast = (age * ast);
var altsquare = Math.pow(alt, 1/2);
var platcount = (platelet_count / 1000);

	

	var calculateFIB4data = ( ( ageast / altsquare) /  platcount ) ;
if($("#ast").val()!='' && $("#alt").val()!='' && $("#platelet_count").val()!=''){

	$('#fib4_score').val(calculateFIB4data.toFixed(3));
	
	if(parseFloat(calculateFIB4data.toFixed(3)) <= '3.25'){
		
	$('#fib_textshow1').html('Patient may be a non-complicated Hepatitis C patient/Non-cirrhotic');
	$('#fib_textshow2').hide();
	
}
else if(parseFloat(calculateFIB4data.toFixed(3)) > '3.25' ){
	
		$('#fib_textshow1').html('Patient may be a complicated Hepatitis C patient/Cirrhotic');
		$('#fib_textshow2').hide();
}

}

}

	$(document).ready(function(){

$("#date_of_prescribing_tests" ).change(function( event ) {

var date_of_prescribing_testsdate = $("#T_DLL_01_Daten" ).datepicker('getDate');
var date_of_prescribing_tests = $("#date_of_prescribing_tests" ).datepicker('getDate');
//alert(date_of_prescribing_testsdate);


if(date_of_prescribing_testsdate > date_of_prescribing_tests){

	$("#modal_header").text("Selected date cannot be before Screening Details Min Date");
					$("#modal_text").text("Please check dates");
					$("#date_of_prescribing_tests" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

});

$("#last_test_result_date" ).change(function( event ) {

//var date_of_prescribing_tests = $("#date_of_prescribing_tests" ).val();
//var last_test_result_date = $("#last_test_result_date" ).val();

var date_of_prescribing_tests = $("#date_of_prescribing_tests" ).datepicker('getDate');
var last_test_result_date = $("#last_test_result_date" ).datepicker('getDate');



if(date_of_prescribing_tests > last_test_result_date){

	$("#modal_header").text("Selected date cannot be before date of Prescribing Tests");
					$("#modal_text").text("Please check dates");
					$("#last_test_result_date" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

});

});


/*Max Date Today Code*/
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!

			var yyyy = today.getFullYear();
			 if(dd<10){
			        dd='0'+dd;
			    } 
			    if(mm<10){
			        mm='0'+mm;	
			    }

			today = yyyy+'-'+mm+'-'+dd;
			document.getElementById("date_of_prescribing_tests").setAttribute("max", today);
			document.getElementById("last_test_result_date").setAttribute("max", today);
			document.getElementById("ultrasound_date").setAttribute("max", today);
			document.getElementById("fibroscan_date").setAttribute("max", today);
			
/*
$("#haemoglobin" ).change(function( event ) {

var haemoglobin = $('#haemoglobin').val();

if(haemoglobin > 20 || haemoglobin < 1){

					$("#modal_header").text("Haemoglobin between 10 to 20");
					$("#modal_text").text("For men, 13.5 to 17.5 grams per deciliter");
					$('#haemoglobin').val('');
					$("#multipurpose_modal").modal("show");
					return false;

}

});*/

$("#compensated_decompensated").change(function( event ) {

var compensated_decompensated = $("#compensated_decompensated").val();
if(compensated_decompensated ==1)
{
childscore();
}else{
	childscore();
}

	});

$("#save").click(function( event ) {

if(($('#variceal_bleed').val() == 1 && $('#child_score1').val() >= 7 && $('#compensated_decompensated').val()==1) || ($('#variceal_bleed').val() == 2 && $('#child_score1').val() < 7 && $('#compensated_decompensated').val()==2))
{	
	var r = confirm("Mismatch in cirrhosis result want to verify?");
	  
if(r == true){
	return false;
}else{
	return true;
}


}else{
	return true;
}

});


$("#cirrohsis_test_date" ).change(function( event ) {
var cirrohsis_test_date = $("#cirrohsis_test_date" ).val();

var last_test_result_date = $("#last_test_result_date" ).val();
if(cirrohsis_test_date < last_test_result_date){

$("#modal_header").text("Selected Date greater than or equal to Test Result Date");
$("#modal_text").text("Please check dates");
$("#cirrohsis_test_date" ).val('');
$("#multipurpose_modal").modal("show");
return false;

}

});

</script>