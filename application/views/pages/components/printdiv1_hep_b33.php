<style type="text/css">
	.col-md-12{
		padding-right: 0px;!important;
		padding-left: 0px;!important;
	}
</style>
<script type="text/javascript">
	function printDiv(divName) {
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
	}
$(document).ready(function () {
	printDiv('printableArea');

});
 window.onmousemove = function() {
  window.close();
}

Q(window.print()).then(function () {window.close(); });
</script>
<button class="printbtn1 f-right btn btn-info" type="button" onclick="printDiv('printableArea')" value="Print" /><i class="fa fa-print" aria-hidden="true"></i> </button>

	<div class="container" id="printableArea">
		<div >
			<p class="text-center" style="font-size: 17px">National Viral Hepatitis Control Program | Department of Health & Family Welfare - <State Name> <br> 
				<b>Patient Record Copy</b>
			</p>
		</div>

		<!-- .......................patient_register............................... -->

		<h5 style="margin-left: 15px; margin-bottom: -20px; margin-top: 90px;">Patient Registration</h5>
		<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
			<tr style="border: 1px solid black;">
				<td><p style="margin-left: 20px">OPD ID :- <span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->OPD_Id)):''; ?></span>
				</p></td>
				<td colspan="2"><p>NVHCP ID :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->UID_Prefix:$uid_prefix; ?></span></p>
				</td>
			</tr>
			<tr style="border: 1px solid black;">
				<td ><p style="margin-left: 20px">Patient type :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->PatientType == 1)?'New':'experienced'; ?></span></p>
				</td>
				<td colspan="2"><p>Previous UID :- 
					<span><?php if(count($patient_data) >0) { echo  str_pad($patient_data[0]->UID_Num, 6, '0', STR_PAD_LEFT); } else { echo  str_pad($patient_uid, 6, '0', STR_PAD_LEFT); } ?></span></p>
				</td>
			</tr>

			<tr>
				<td ><p style="margin-left: 20px">Name :- <span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FirstName)):''; ?></span></p>
				</td>
				<td><p>Age(months/years) :- <span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->Age)):''; ?></span></p>
				</td>
				<td><p>Gender :- <span><?php if(count($patient_data) > 0 )
				if($patient_data[0]->Gender == 1) { echo 'Male'; } 
				if($patient_data[0]->Gender == 2) { echo 'Female'; }
				if($patient_data[0]->Gender == 3) { echo 'Transgender'; }  ?></span></p>
			</td>
		</tr>
		<tr>			
			<td><p style="margin-left: 20px">Relative Type :- 
				<?php if(count($patient_data) > 0)
				{
					if($patient_data[0]->Relation == 1){ echo 'Father'; }
					if($patient_data[0]->Relation == 2){ echo 'Husband'; }
					if($patient_data[0]->Relation == 3){ echo 'Guardian'; }
					if($patient_data[0]->Relation == 4){ echo 'Mother'; }
				} ?></p>
			</td>
			<td colspan="2"><p>Relative's Name :- <span><span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FatherHusband)):''; ?></span></span></p>
			</td>
		</tr>

		<tr>
			<td colspan="3"><p style="margin-left: 20px">Home Street and Address :- <span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->Add1)):''; ?></span></p>
			</td>
		</tr><br>

		<tr>
			<td ><p style="margin-left: 20px">State:- <span>
				<?php if(!empty($states)) { echo $states[0]->StateName; } ?>
			</span></p>
		</td>
		<td><p>District :- <span><?php if(!empty($district)) { echo $district[0]->DistrictName; } ?></span></p>
		</td>
		<td><p>Block/Ward :- <span><?php if(!empty($block)) { echo $block[0]->BlockName; } ?></span></p>
		</td>
	</tr>
	<tr>
		<td><p style="margin-left: 20px">Village/Town/City :- <span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->VillageTown)):''; ?></span></p>
		</td>
		<td colspan="2"><p>Pin code :- <span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->PIN)):''; ?></span></p>
		</td>
	</tr>

	<tr>
		<td><p style="margin-left: 20px">Contact No :- <span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->Mobile)):''; ?></span></p>
		</td>
		<td colspan="2"><p>Consent for Receiving Communication :- 
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->IsSMSConsent == 1){ echo 'Yes'; }
				if($patient_data[0]->IsSMSConsent == 2){ echo 'No'; }
			} ?></p></td>
		</tr><br>

		<tr>
			<td colspan="3"><p style="margin-left: 20px">Risk factor:- <?php error_reporting(0); 
			foreach ($risk_factor as $risk) { 
				$RISKFF= explode(',', $patient_data[0]->Risk);
				?>&nbsp;&nbsp;
				<?php if (in_array($risk->LookupCode,$RISKFF )){ ?>
					<input type="checkbox"  value="<?php echo $risk->LookupCode; ?>" <?php if (in_array($risk->LookupCode,$RISKFF )){ echo "checked"; }?>  id="risk_<?php echo $risk->LookupCode; ?>" >&nbsp;&nbsp; <?php echo $risk->LookupValue; ?>

					<?php } }?></p>
				</td>
			</tr>

		</table>

		<!-- ...........................patient_screening............................ -->


<?php  if(count($patient_data) > 0 && $patient_data[0]->MF2 == 1){ ?>

		<h5 style="margin-left: 15px; margin-bottom: 10px;">Screening Details</h5>
		<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
				<tr style="border: 1px solid black;">
				<td><p style="margin-left: 20px">Test types:</p></td>
				<?php if(count($patient_data) > 0 && $patient_data[0]->LgmAntiHAV == 1 ) {?>
				<td><p><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->LgmAntiHAV == 1)?"checked":''; ?>>&nbsp;&nbsp;IgM Anti HAV</p>
				</td>
			<?php } ?>
			<?php if(count($patient_data) > 0 && $patient_data[0]->HbsAg == 1 ) {?>
				<td><p><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HbsAg == 1)?"checked":''; ?>>&nbsp;&nbsp;HBsAg</p>
				</td>
			<?php } ?>
					<?php if(count($patient_data) > 0 && $patient_data[0]->AntiHCV == 1 ) { ?>
				<td><p><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->AntiHCV == 1)?"checked":''; ?>>&nbsp;&nbsp;Anti HCV</p>
				</td>
			<?php } ?>
			<?php if(count($patient_data) > 0 && $patient_data[0]->LgmAntiHEV == 1 ) { ?>
				<td><p><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->LgmAntiHEV == 1)?"checked":''; ?>>&nbsp;&nbsp;IgM Anti HEV</p>
				</td>
			<?php } ?>
			</tr>
</table>

		<?php if (count($patient_data) > 0 && $patient_data[0]->LgmAntiHAV == 1){ ?>

			<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
			
		<h5 style="margin-left: 15px; margin-bottom: 10px;">Screening Details - lgM Anti HAV Testing</h5>

			<tr>
				<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?"checked":""; ?>>&nbsp;&nbsp;Rapid Diagnostic Testing:</p>
				</td>
				<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?timeStampShow($patient_data[0]->HAVRapidDate):''; ?></span></p>
				</td>
				<td colspan="4"><p>Result :- <span>
					<?php if(count($patient_data) > 0){
						if($patient_data[0]->HAVRapidResult == 1){echo 'Positive';}
						if($patient_data[0]->HAVRapidResult == 2){echo 'Negative';}
					} ?>
				</span></p>
			</td>
		</tr>
<?php if (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1){ ?>
		<tr>
			<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?"checked":""; ?>>&nbsp;&nbsp;ELISA Test:</p>
			</td>
			<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?timeStampShow($patient_data[0]->HAVElisaDate):''; ?></span></p>
			</td>
			<td colspan="4"><p>Result :- <span>
				<?php if(count($patient_data) > 0){
					if($patient_data[0]->HAVElisaResult == 1){echo 'Positive';}
					if($patient_data[0]->HAVElisaResult == 2){echo 'Negative';}
				} ?>
			</span></p>
		</td>
	</tr>
<?php } ?>
<?php if (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1){ ?>

	<tr>
		<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?"checked":""; ?>>&nbsp;&nbsp;Other test:</p>
		</td>


		<td ><p> <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?$patient_data[0]->HAVOtherName:''; ?></p>
		</td>

		<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?timeStampShow($patient_data[0]->HAVOtherDate):''; ?></span></p>
		</td>
		<td colspan="4">Result :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->HAVOtherResult == 1){echo 'Positive';}
				if($patient_data[0]->HAVOtherResult == 2){echo 'Negative';}
			} ?>
		</span></p>
	</td>
</tr>
<?php } ?>


</table>

		<?php  } ?>

		<?php if (count($patient_data) > 0 && $patient_data[0]->HbsAg == 1){ ?>

			<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
			
		<h5 style="margin-left: 15px; margin-bottom: 10px;">Screening Details - HBsAg Testing</h5>
<?php if (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1){ ?>
			<tr>
				<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?"checked":""; ?>>&nbsp;&nbsp;Rapid Diagnostic Testing:</p>
				</td>
				<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?timeStampShow($patient_data[0]->HBSRapidDate):''; ?></span></p>
				</td>
				<td colspan="4"><p>Result :- <span>
					<?php if(count($patient_data) > 0){
						if($patient_data[0]->HBSRapidResult == 1){echo 'Positive';}
						if($patient_data[0]->HBSRapidResult == 2){echo 'Negative';}
					} ?>
				</span></p>
			</td>
		</tr>
<?php } ?>

<?php if (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1){ ?>
			<tr>
			
				<td colspan="4"><p>Place of Testing :- <span>
					<?php if(count($patient_data) > 0){
						if($patient_data[0]->HBSRapidPlace == 1){echo 'Govt. Lab';}
						if($patient_data[0]->HBSRapidPlace == 2){echo 'Private Lab-PPP';}
					} ?>
				</span></p>
			</td>
		</tr>

			<!-- <?php if($patient_data[0]->HBSRapidPlace == 1) { ?>
				<tr>
				<td ><p>Lab Name :- <span>
					<?php if(count($patient_data) > 0){
						if($patient_data[0]->HBSRapidPlace == 1){echo 'Govt. Lab';}
						if($patient_data[0]->HBSRapidPlace == 2){echo 'Private Lab-PPP';}
					} ?>
				</span></p>
			</td>
		</tr>
		<?php } ?> -->
<?php } ?>


		<?php if (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1){ ?>
		<tr>
			<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?"checked":""; ?>>&nbsp;&nbsp;ELISA Test:</p>
			</td>
			<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?timeStampShow($patient_data[0]->HBSElisaDate):''; ?></span></p>
			</td>
			<td colspan="4"><p>Result :- <span>
				<?php if(count($patient_data) > 0){
					if($patient_data[0]->HBSElisaResult == 1){echo 'Positive';}
					if($patient_data[0]->HBSElisaResult == 2){echo 'Negative';}
				} ?>
			</span></p>
		</td>
	</tr>
<?php } ?>
<?php if (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1){ ?>
	<tr>
		<td ><p style="margin-left: 20px"><input type="checkbox" name=""  <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?"checked":""; ?>>&nbsp;&nbsp;Other test:</p>
		</td>
		<td><p><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?$patient_data[0]->HBSOtherName:''; ?></p></td>

		<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?timeStampShow($patient_data[0]->HBSOtherDate):''; ?></span></p>
		</td>
		<td colspan="4">Result :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->HBSOtherResult == 1){echo 'Positive';}
				if($patient_data[0]->HBSOtherResult == 2){echo 'Negative';}
			} ?>
		</span></p>
	</td>
</tr>
<?php } ?>

</table>
		<?php } ?>

		<?php if (count($patient_data) > 0 && $patient_data[0]->AntiHCV == 1 ){ ?>

		<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
			
		<h5 style="margin-left: 15px; margin-bottom: 10px;">Screening Details - Anti HCV Testing</h5>
<?php if (count($patient_data) > 0 && $patient_data[0]->AntiHCV == 1 ){ ?>
			<tr>
				<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?"checked":""; ?>>&nbsp;&nbsp;Rapid Diagnostic Testing:</p>
				</td>
				<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?timeStampShow($patient_data[0]->HCVRapidDate):''; ?></span></p>
				</td>
				<td colspan="4"><p>Result :- <span>
					<?php if(count($patient_data) > 0){
						if($patient_data[0]->HCVRapidResult == 1){echo 'Positive';}
						if($patient_data[0]->HCVRapidResult == 2){echo 'Negative';}
					} ?>
				</span></p>
			</td>
		</tr>
<?php } ?>
<?php if (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1 ){ ?>
		<tr>

			<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?"checked":""; ?>>&nbsp;&nbsp;ELISA Test:</p>
			</td>
			<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?timeStampShow($patient_data[0]->HCVElisaDate):''; ?></span></p>
			</td>
			<td colspan="4"><p>Result :- <span>
				<?php if(count($patient_data) > 0){
					if($patient_data[0]->HCVElisaResult == 1){echo 'Positive';}
					if($patient_data[0]->HCVElisaResult == 2){echo 'Negative';}
				} ?>
			</span></p>
		</td>
	</tr>
<?php } ?>

<?php if (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1 ){ ?>
	<tr>
		<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?"checked":""; ?>>&nbsp;&nbsp;Other test:</p>
		</td>
		<td><p><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?$patient_data[0]->HCVOtherName:''; ?></p></td>

		<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?timeStampShow($patient_data[0]->HCVOtherDate):''; ?></span></p>
		</td>
		<td colspan="4">Result :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->HCVOtherResult == 1){echo 'Positive';}
				if($patient_data[0]->HCVOtherResult == 2){echo 'Negative';}
			} ?>
		</span></p>
	</td>
</tr>
<?php } ?>

</table>
<?php } ?>
<!-- lgM Anti HEV -->

<?php if (count($patient_data) > 0 && $patient_data[0]->LgmAntiHEV == 1 ){ ?>

		<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
			
		<h5 style="margin-left: 15px; margin-bottom: 10px;">Screening Details - lgM Anti HEV Testing</h5>
<?php if (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1 ){ ?>
			<tr>
				<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?"checked":""; ?>>&nbsp;&nbsp;Rapid Diagnostic Testing:</p>
				</td>
				<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?timeStampShow($patient_data[0]->HEVRapidDate):''; ?></span></p>
				</td>
				<td colspan="4"><p>Result :- <span>
					<?php if(count($patient_data) > 0){
						if($patient_data[0]->HEVRapidResult == 1){echo 'Positive';}
						if($patient_data[0]->HEVRapidResult == 2){echo 'Negative';}
					} ?>
				</span></p>
			</td>
		</tr>
<?php } ?>
<?php if (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1 ){ ?>
		<tr>

			<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?"checked":""; ?>>&nbsp;&nbsp;ELISA Test:</p>
			</td>
			<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?timeStampShow($patient_data[0]->HEVElisaDate):''; ?></span></p>
			</td>
			<td colspan="4"><p>Result :- <span>
				<?php if(count($patient_data) > 0){
					if($patient_data[0]->HEVElisaResult == 1){echo 'Positive';}
					if($patient_data[0]->HEVElisaResult == 2){echo 'Negative';}
				} ?>
			</span></p>
		</td>
	</tr>
<?php } ?>

<?php if (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1 ){ ?>
	<tr>
		<td ><p style="margin-left: 20px"><input type="checkbox" name=""<?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?"checked":""; ?>>&nbsp;&nbsp;Other test:</p>
		</td>
		<td><p><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?$patient_data[0]->HEVOtherName:''; ?></p></td>
		

		<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?timeStampShow($patient_data[0]->HEVOtherDate):''; ?></span></p>
		</td>
		<td >Result :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->HCVOtherResult == 1){echo 'Positive';}
				if($patient_data[0]->HCVOtherResult == 2){echo 'Negative';}
			} ?>
		</span></p>
	</td>
</tr>
<?php } ?>

</table>
<?php } } ?>
<!-- end lgM Anti HEV -->
<!-- $patient_data[0]->SVR12W_Result !='' -->
<?php if(count($patient_data) > 0 && ($patient_data[0]->T_DLL_01_VLC_Result == 1  ) ){ ?>
<!-- .....................patient_viral_load....................... -->

<br/>
<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
				<tr style="border: 1px solid black;">
				
				<?php if(count($patient_data) > 0 && $patient_data[0]->HbsAg == 1 ) { ?>
				<td><p><input type="checkbox" checked value="1" name="check_hepc" id="check_hepc" style="width: 20px; height: 20px;">  HEP-C</p>
				</td>
			<?php } ?>

				<?php if (count($patient_data) > 0 && $patient_data[0]->VLHepB == 1){ ?>
				<?php 	if($patient_data[0]->HBSRapidResult == 1 || $patient_data[0]->HBSElisaResult == 1 || $patient_data[0]->HBSOtherResult == 1 || $patient_data[0]->HBCRapidResult == 1 || $patient_data[0]->HBCElisaResult == 1 || $patient_data[0]->HBCOtherResult == 1) 
				{ ?>
				<td><p><input type="checkbox" value="1" name="check_hepb" id="check_hepb" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->VLHepB == 1)?'checked':''; ?>>  HEP-B</p></td>

				</td>
			<?php } }?>

			

				
			<?php //} ?>
			</tr>
</table>
<br/>
<h5 style="margin-left: 15px;">Hep C Viral Load Test Details</h5>

<table style="border: 1px solid black;width: 95%; margin-left: 15px;">	
	<tr>
		<td><p style="margin-left: 20px">Sample Drawn Date :- <span><?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->VLSampleCollectionDate):''; ?></span></p>
		</td>
		<!-- <td colspan="4"><p></p>
		</td> -->
</tr>

<tr>
		<td><p style="margin-left: 20px">Is Sample Stored :- <span>

		<?php if(count($patient_data) > 0){
				if($patient_data[0]->IsVLSampleStored == 1){echo 'Yes';}
				if($patient_data[0]->IsVLSampleStored == 2){echo 'No';}
			} ?>
				
			</span></p>
		</td>
		<?php if(count($patient_data) > 0 && $patient_data[0]->IsVLSampleStored == 1){ ?>
		<td><p>Sample Storage Temperature (&#176;C) :- <span>
		
			<?php
				if($patient_data[0]->VLStorageTemp == 1){echo '<2';}
				if($patient_data[0]->VLStorageTemp == 2){echo '2-8';}
				if($patient_data[0]->VLStorageTemp == 3){echo '>8';} ?>
			
				
			</span></p>
		</td>
		<?php } ?>
		<?php  if(count($patient_data) > 0 && $patient_data[0]->IsVLSampleStored == 1){ ?>
		<td><p>Sample Storage Duration :- <span>
			<?php
				if($patient_data[0]->Storage_days_hrs == 1){echo 'Less than 1 day';}
				if($patient_data[0]->Storage_days_hrs == 2){echo 'More than 1 day';} ?>
			
		</span></p>
	</td>
	<?php } ?>

	<?php  if(count($patient_data) > 0 && $patient_data[0]->IsVLSampleStored == 1){ ?>
		<td ><p>Duration :- <span>
			
			<?php echo (count($patient_data) > 0)?$patient_data[0]->Storageduration:''; ?>
			
		</span></p>
	</td>
	<?php } ?>
		<!-- <td colspan="4"><p></p>
		</td> -->
</tr>

<!-- transport -->

<tr>
		<td><p style="margin-left: 20px">Is Sample Transported :- <span>

		<?php if(count($patient_data) > 0){
				if($patient_data[0]->IsVLSampleTransported == 1){echo 'Yes';}
				if($patient_data[0]->IsVLSampleTransported == 2){echo 'No';}
			} ?>
				
			</span></p>
		</td>
		<?php if(count($patient_data) > 0 && $patient_data[0]->IsVLSampleTransported == 1){ ?>
		<td><p>Sample Storage Temperature (&#176;C) :- <span>
		
			<?php
				if($patient_data[0]->VLTransportTemp == 1){echo '<2';}
				if($patient_data[0]->VLTransportTemp == 2){echo '2-8';}
				if($patient_data[0]->VLTransportTemp == 3){echo '>8';} ?>
			
				
			</span></p>
		</td>
		<?php } ?>

		<?php  if(count($patient_data) > 0 && $patient_data[0]->IsVLSampleTransported == 1){ ?>
		<td ><p>Sample Transport Date :- <span>
			
			<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->VLTransportDate):''; ?>
			
		</span></p>
	</td>
	<?php } ?>

		<?php  if(count($patient_data) > 0 && $patient_data[0]->IsVLSampleTransported == 1){ ?>
		<td><p>Sample Transported To:- <span>
			<?php
				if($patient_data[0]->VLLabID == 1){echo 'Lab1';}
				if($patient_data[0]->VLLabID == 2){echo 'Lab2';}
				if($patient_data[0]->VLLabID == 3){echo 'Lab3';} 
				if($patient_data[0]->VLLabID == 99){echo 'Others';} ?>
			</p>
			
		</span></p>
	</td>
	<?php } ?>

			<td colspan="4"><p></p>
		</td>

</tr>
<!-- end transport -->
<tr>
<?php if($patient_data[0]->VLLabID == 99){ ?>
	<td><p style="margin-left: 20px">Transported To Others Name:- <?php echo (count($patient_data) > 0)?$patient_data[0]->VLLabID_Other:''; ?></p></td>	

	<?php } ?>	
<?php  if(count($patient_data) > 0 && $patient_data[0]->VLTransporterName != ''){ ?>
	<td><p style="margin-left: 20px">Sample Transported By : Name:- <?php echo (count($patient_data) > 0)?$patient_data[0]->VLTransporterName:''; ?></p></td>	
<?php } ?>

	<td colspan="5"><p style="margin-left: 20px">Remarks :- <?php echo (count($patient_data) > 0)?$patient_data[0]->VLResultRemarks:''; ?></p></td>				
</tr>	

<!-- Sample Receipt Date -->



<tr><td><p style="margin-left: 20px">HEP-C Confirmatory Viral Load Details - Result</p></td>
<td colspan="4"><p></p>
		</td>
	</tr>
<tr>
<?php if(count($patient_data) > 0 && $patient_data[0]->IsVLSampleTransported == 1){ ?>
<td ><p style="margin-left: 20px">Sample Receipt Date :- <span>
	<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->VLRecieptDate):''; ?>
	</span></p> </td>

<td ><p>VSample Received By : Name :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->VLReceiverName:''; ?>
	
</span></p>
		</td>
		
	<!-- 	<td colspan="3"><p>Result :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->T_DLL_01_VLC_Result == 1){echo 'Detected';}
				if($patient_data[0]->T_DLL_01_VLC_Result == 2){echo 'Not Detected';}
			} ?>
		</span></p>
	</td> -->

</tr>

<!-- end Sample Receipt Date -->
<?php } ?>


<tr>


<td ><p style="margin-left: 20px">Is Sample Accepted :- <span>
	<?php if(count($patient_data) > 0){
				if($patient_data[0]->IsSampleAccepted == 1){echo 'Yes';}
				if($patient_data[0]->IsSampleAccepted == 2){echo 'No';}
			} ?>
	</span></p> </td>

<td ><p>Viral Load :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->T_DLL_01_VLCount:''; ?>
	
</span></p>
		</td>

		<td colspan="3"><p>Result :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->T_DLL_01_VLC_Result == 1){echo 'Detected';}
				if($patient_data[0]->T_DLL_01_VLC_Result == 2){echo 'Not Detected';}
			} ?>
		</span></p>
	</td>

</tr>

</table>
<?php } ?>
<?php if (count($patient_data) > 0 && $patient_data[0]->VLHepB == 1){ ?>
          

          <!-- HEP-B Start Base Line Test Details...-->

<h5 style="margin-left: 15px;">HEP- B Baseline Test Details</h5>

<table style="border: 1px solid black;width: 95%; margin-left: 15px;">	
	<tr>
		<td><p style="margin-left: 20px">Date of Prescribing :- <span><?php echo (count($patient_datahepb) > 0)?timeStampShow($patient_datahepb[0]->Prescribing_Dt):''; ?></span></p>
		</td>
		<td><p style="margin-left: 20px">Date of issue of last investigation :- <span><?php echo (count($patient_datahepb) > 0)?timeStampShow($patient_datahepb[0]->LastTest_Dt):''; ?></span></p>
		</td>
		<td colspan="3">
	</td>
</tr>
</table>
<table  style="border: 1px solid black;width: 95%; margin-left: 15px;">
	<thead>
		<th style="border: 1px solid black; text-align: center;">Sr. No.</th>
		<th style="border: 1px solid black; text-align: center;">Haemoglobin</th>
		<th style="border: 1px solid black; text-align: center;">S.Albumin</th>
		<th style="border: 1px solid black; text-align: center;">Serum Bilirubin Total(mg/dL)</th>
		<th style="border: 1px solid black; text-align: center;">ALT (IU/L)</th>
		<th style="border: 1px solid black; text-align: center;">AST (IU/L)</th>
		<th style="border: 1px solid black; text-align: center;">AST ULN (Upper Limit of Normal) (IU/L)</th>
		<th style="border: 1px solid black; text-align: center;">Platelet Count (per microlitre)</th>
		<th style="border: 1px solid black; text-align: center;">Weight (in Kgs)</th>
		<th style="border: 1px solid black; text-align: center;">S. Creatinine (mg/dL)</th>
		<th style="border: 1px solid black; text-align: center;">eGFR (estimated glomerular filtration rate) (mL/min/1.73 m2)</th>
	</thead>
	<tbody>
		<?php foreach ($patient_datahepb as $Key => $patient_datahepb) { ?>
			<tr>
				<td style="border: 1px solid black; text-align: center; font-weight: bold;"><?php echo $Key+1; ?></td>
				<td style="border: 1px solid black; text-align: center;"><?php echo (count($patient_datahepb) > 0)?$patient_datahepb->V1_Haemoglobin:''; ?></td>
				<td style="border: 1px solid black; text-align: center;"><?php echo (count($patient_datahepb) > 0)?$patient_datahepb->V1_Albumin:''; ?></td>
				<td style="border: 1px solid black; text-align: center;"><?php echo (count($patient_datahepb) > 0)?$patient_datahepb->V1_Bilrubin:''; ?></td>
				<td style="border: 1px solid black; text-align: center;"><?php echo (count($patient_datahepb) > 0)?$patient_datahepb->ALT:''; ?></td>
				<td style="border: 1px solid black; text-align: center;"><?php echo (count($patient_datahepb) > 0)?$patient_datahepb->AST:''; ?></td>
				<td style="border: 1px solid black; text-align: center;"><?php echo (count($patient_datahepb) > 0)?$patient_datahepb->AST_ULN:''; ?></td>
				<td style="border: 1px solid black; text-align: center;"><?php echo (count($patient_datahepb) > 0)?$patient_datahepb->V1_Platelets:''; ?></td>
				<td style="border: 1px solid black; text-align: center;"><?php echo (count($patient_datahepb) > 0)?$patient_datahepb->Weight:''; ?></td>
				<td style="border: 1px solid black; text-align: center;"><?php echo (count($patient_datahepb) > 0)?$patient_datahepb->V1_Creatinine:''; ?></td>
				<td style="border: 1px solid black; text-align: center;"><?php echo (count($patient_datahepb) > 0)?$patient_datahepb->V1_EGFR:''; ?></td>
			</tr>

			<!-- <h5 style="margin-left: 15px;">Criteria for Evaluating Cirrhosis</h5> -->
			<tr>
			<td style="border: 1px solid black;" colspan="6"><p style="margin-left: 15px;"><input type="checkbox" value="1" name="check_hepb" id="check_hepb" style="width: 20px; height: 20px;" <?php echo (count($patient_datahepb) > 0 && $patient_datahepb->Clinical_US == 1)?'checked':''; ?>>  Ultrasound</p></td>
			<td style="border: 1px solid black;" colspan="5"><sapn style="margin-left: 20px;">Ultrasound Date :- </sapn><?php echo (count($patient_datahepb) > 0)?timeStampShow($patient_datahepb->Clinical_US_Dt):''; ?></td>
		</tr>
		<tr>
			<td style="border: 1px solid black;" colspan="6"><p style="margin-left: 15px;"><input type="checkbox" value="1" name="check_hepb" id="check_hepb" style="width: 20px; height: 20px;" <?php echo (count($patient_datahepb) > 0 && $patient_datahepb->Fibroscan == 1)?'checked':''; ?>>  Fibroscan</p></td>
			<td style="border: 1px solid black;" colspan="5"><sapn style="margin-left: 20px;">Fibroscan Date :- </sapn><?php echo (count($patient_datahepb) > 0)?timeStampShow($patient_datahepb->Fibroscan_Dt):''; ?></td>
			</tr>

			<tr>
				<td style="border: 1px solid black;" colspan="6"><p style="margin-left: 15px;"><input type="checkbox" value="1" name="check_hepb" id="check_hepb" style="width: 20px; height: 20px;" <?php echo (count($patient_datahepb) > 0 && $patient_datahepb->APRI == 1)?'checked':''; ?>>  APRI</p></td>
				<td style="border: 1px solid black;" colspan="5"><sapn style="margin-left: 20px;">APRI Score :- </sapn><?php if( (count($patient_datahepb) > 0) && $patient_datahepb->APRI_Score >2 ) { ?> <p id="apri_textshow2"><sapn style="margin-left: 20px;">Patient may be a complicated Hepatitis B patient/Cirrhotic</sapn></p><?php }elseif((count($patient_datahepb) > 0) && $patient_datahepb->APRI_Score <2 ) { ?> <p id="apri_textshow2"><sapn style="margin-left: 20px;">Patient may be a non-complicated Hepatitis B patient/Non-cirrhotic</sapn></p> <?php } ?></td>
			</tr>
			<tr>
				<td style="border: 1px solid black;" colspan="6"><p style="margin-left: 15px;"><input type="checkbox" value="1" name="check_hepb" id="check_hepb" style="width: 20px; height: 20px;" <?php echo (count($patient_datahepb) > 0 && $patient_datahepb->FIB4 == 1)?'checked':''; ?>>  FIB 4</p></td>
				<td style="border: 1px solid black;" colspan="5"><sapn style="margin-left: 20px;">FIB 4 Score :- </sapn><?php if( (count($patient_datahepb) > 0) && $patient_datahepb->FIB4_FIB4 <='3.25') { ?> <p id="fib_textshow2"><sapn style="margin-left: 20px;">Patient may be a non-complicated Hepatitis B patient/Non-cirrhotic</sapn></p><?php }elseif((count($patient_datahepb) > 0) && $patient_datahepb->FIB4_FIB4 >'3.25') { ?> <p id="fib_textshow2"><sapn style="margin-left: 20px;">Patient may be a complicated Hepatitis B patient/Cirrhotic</sapn></p> <?php } ?></td>
			</tr>
			<tr>
				<td style="border: 1px solid black;" colspan="11"><sapn style="margin-left: 20px;">Complicated/Uncomplicated : - </sapn><?php if($patient_datahepb->V1_Cirrhosis == 1){echo 'Complicated';} ?><?php if($patient_datahepb->V1_Cirrhosis == 2){echo 'Uncomplicated';} ?></td>
			</tr>
			 <?php if($patient_datahepb->V1_Cirrhosis == 1){ ?>
			<tr>
				<td style="border: 1px solid black;" colspan="11">Decompensated Cirrhosis</td>
			</tr>
			<tr>
				<td style="border: 1px solid black;" colspan="2">Variceal Bleed :- <?php if(count($patient_datahepb) > 0){
						if($patient_datahepb->Cirr_VaricealBleed == 1){echo 'Yes';}
						if($patient_datahepb->Cirr_VaricealBleed == 2){echo 'No';}
					} ?>
				</td>
				<td style="border: 1px solid black;" colspan="2">Ascites :- <?php if(count($patient_datahepb) > 0){
			if($patient_datahepb->Cirr_Ascites == 1){echo 'None';}
			if($patient_datahepb->Cirr_Ascites == 2){echo 'Mild to Moderate';}
			if($patient_datahepb->Cirr_Ascites == 3){echo 'Severe';}
		} ?>
			</td>
				<td style="border: 1px solid black;" colspan="2">Encephalopathy :- <?php if(count($patient_datahepb) > 0){
			if($patient_datahepb->Cirr_Encephalopathy == 1){echo 'None';}
			if($patient_datahepb->Cirr_Encephalopathy == 2){echo 'Mild to Moderate';}
			if($patient_datahepb->Cirr_Encephalopathy == 3){echo 'Severe';}
		} ?>
		</td>
				<td style="border: 1px solid black;" colspan="">PT INR :- <?php echo (count($patient_datahepb) > 0)?$patient_datahepb->V1_INR:''; ?></td>
				<td style="border: 1px solid black;" colspan="2">Child Pugh Score :- <?php if(count($patient_datahepb) > 0 && $patient_datahepb->ChildScore == 5 || $patient_datahepb->ChildScore == 6) { echo 'A'; } elseif(count($patient_datahepb) > 0 && $patient_datahepb->ChildScore >=7 ||  $patient_datahepb->ChildScore <=9) { echo 'B'; } elseif(count($patient_datahepb) > 0 && $patient_datahepb->ChildScore >=7 ||  $patient_datahepb->ChildScore <=9) { echo 'C'; } else { echo ''; }?>
					
				</td>
				<td style="border: 1px solid black;" colspan="2">Severity of Hep-B :- <?php if(count($patient_datahepb) > 0){
				if($patient_datahepb->CirrhosisStatus == 1){echo 'Compensated Cirrohosis';}
				if($patient_datahepb->CirrhosisStatus == 2){echo 'Decompensated Cirrohosis';}
			} ?></td>
			</tr>
			<tr>
				<td>
					
				</td>
			</tr>
		<?php } ?>
	<?php	} ?>
	</tbody>
</table>



<!-- End Base Line Test Module..... -->

<!-- Start HBV DNA test Module for downloading... -->

<h5 style="margin-left: 15px;">HEP- B HBV DNA Module</h5>

<table style="border: 1px solid black;width: 95%; margin-left: 15px;">	
	<tr>
		<td><p style="margin-left: 20px">HBV DNA TEST DETAILS :- <input type="checkbox" checked value="1" name="check_hepb" id="check_hepb" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->VLHepB == 1)?'checked':''; ?>>
		HEP-B</p>
		</td>	
</tr>
</table>


<table  style="width:95%; margin-right: 0px; margin-top: 20px; margin-left: 15px;">

			
			<thead style="border: 1px solid black;">
				<tr>
					<th style="text-align: center;border: 1px solid black;">Visit No.</th> 
					<th style="text-align: center;border: 1px solid black;">Sample Accepted</th>
					<th style="text-align: center;border: 1px solid black;">Test Result date</th>
					<th style="text-align: center;border: 1px solid black;">Result</th>
					<th style="text-align: center;border: 1px solid black;">HBV DNA</th>
				</tr>
			</thead>
			<tbody style="border: 1px solid black;">

<?php if($tblpatientvlsamplelistlist){ foreach ($tblpatientvlsamplelistlist as  $value) {
 ?>
<tr>
	<td style="border: 1px solid black; text-align: center; font-weight: bold;"><?php echo  $value->Visit_No; ?></td>
	<td style="border: 1px solid black; text-align: center;"><?php echo  $value->IsBSampleAccepted; ?></td>
	<td style="border: 1px solid black; text-align: center;"><?php echo  timeStampShow($value->T_DLL_01_BVLC_Date); ?></td>
	<td style="border: 1px solid black; text-align: center;"><?php echo  $value->T_DLL_01_BVLC_Result; ?></td>
	<td style="border: 1px solid black; text-align: center;"><?php echo  $value->T_DLL_01_BVLCount; ?></td>
</tr>

			<tr>
				<td><p style="margin-left: 20px">Sample Drawn Date :- <span><?php echo (count($patient_data) > 0)?timeStampShow($value->BVLSampleCollectionDate):''; ?></span></p>
				</td>
				
		</tr>

		<tr>
				<td><p style="margin-left: 20px">Is Sample Stored :- <span>

				<?php if(count($tblpatientvlsamplelistlist) > 0){
						if($value->IsBVLSampleStored == 1){echo 'Yes';}
						if($value->IsBVLSampleStored == 2){echo 'No';}
					} ?>
						
					</span></p>
				</td>
				<?php if(count($tblpatientvlsamplelistlist) > 0 && $value->IsBVLSampleStored == 1){ ?>
				<td><p>Sample Storage Temperature (&#176;C) :- <span>
				
					<?php
						if($value->BVLStorageTemp == 1){echo '<2';}
						if($value->BVLStorageTemp == 2){echo '2-8';}
						if($value->BVLStorageTemp == 3){echo '>8';} ?>
					
						
					</span></p>
				</td>
				<?php } ?>
				<?php  if(count($tblpatientvlsamplelistlist) > 0 && $value->IsBVLSampleStored == 1){ ?>
				<td><p>Sample Storage Duration :- <span>
					<?php
						if($value->BStorage_days_hrs == 1){echo 'Less than 1 day';}
						if($value->BStorage_days_hrs == 2){echo 'More than 1 day';} ?>
					
				</span></p>
			</td>
			<?php } ?>

			<?php  if(count($tblpatientvlsamplelistlist) > 0 && $value->IsBVLSampleStored == 1){ ?>
				<td ><p>Duration :- <span>
					
					<?php echo (count($tblpatientvlsamplelistlist) > 0)?$value->BStorageduration:''; ?>
					
				</span></p>
			</td>
			<?php } ?>

		</tr>

		<!-- transport -->

		<tr>
				<td><p style="margin-left: 20px">Is Sample Transported :- <span>

				<?php if(count($tblpatientvlsamplelistlist) > 0){
						if($value->IsBVLSampleTransported == 1){echo 'Yes';}
						if($value->IsBVLSampleTransported == 2){echo 'No';}
					} ?>
						
					</span></p>
				</td>
				<?php if(count($tblpatientvlsamplelistlist) > 0 && $value->IsVLSampleTransported == 1){ ?>
				<td><p>Sample Storage Temperature (&#176;C) :- <span>
				
					<?php
						if($value->BVLTransportTemp == 1){echo '<2';}
						if($value->BVLTransportTemp == 2){echo '2-8';}
						if($value->BVLTransportTemp == 3){echo '>8';} ?>
					
						
					</span></p>
				</td>
				<?php } ?>

				<?php  if(count($tblpatientvlsamplelistlist) > 0 && $value->IsBVLSampleTransported == 1){ ?>
				<td ><p>Sample Transport Date :- <span>
					
					<?php echo (count($tblpatientvlsamplelistlist) > 0)?timeStampShow($value->BVLTransportDate):''; ?>
					
				</span></p>
			</td>
			<?php } ?>

				<?php  if(count($tblpatientvlsamplelistlist) > 0 && $value->IsBVLSampleTransported == 1){ ?>
				<td><p>Sample Transported To:- <span>
					<?php
						if($value->BVLLabID == 1){echo 'Lab1';}
						if($value->BVLLabID == 2){echo 'Lab2';}
						if($value->BVLLabID == 3){echo 'Lab3';} 
						if($value->BVLLabID == 99){echo 'Others';} ?>
					</p>
					
				</span></p>
			</td>
			<?php } ?>

			

		</tr>
		<!-- end transport -->
		<tr>
		<?php if($value->VLLabID == 99){ ?>
			<td><p style="margin-left: 20px">Transported To Others Name:- <?php echo (count($tblpatientvlsamplelistlist) > 0)?$value->BVLLabID_Other:''; ?></p></td>	

			<?php } ?>	
		<?php  if(count($tblpatientvlsamplelistlist) > 0 && $value->BVLTransporterName != ''){ ?>
			<td><p style="margin-left: 20px">Sample Transported By : Name:- <?php echo (count($tblpatientvlsamplelistlist) > 0)?$value->BVLTransporterName:''; ?></p></td>	
		<?php } ?>

			<td colspan="5"><p style="margin-left: 20px">Remarks :- <?php echo (count($tblpatientvlsamplelistlist) > 0)?$value->BVLResultRemarks:''; ?></p></td>				
		</tr>	

		<!-- Sample Receipt Date -->
		<tr>
		<?php if(count($tblpatientvlsamplelistlist) > 0 && $value->IsBVLSampleTransported == 1){ ?>
		<td ><p style="margin-left: 20px">Sample Receipt Date :- <span>
			<?php echo (count($tblpatientvlsamplelistlist) > 0)?timeStampShow($value->BVLRecieptDate):''; ?>
			</span></p> </td>

		<td ><p>VSample Received By : Name :- <span><?php echo (count($tblpatientvlsamplelistlist) > 0)?$value->BVLReceiverName:''; ?>
			
		</span></p>
				</td>
				
			<!-- 	<td colspan="3"><p>Result :- <span>
					<?php if(count($patient_data) > 0){
						if($patient_data[0]->T_DLL_01_VLC_Result == 1){echo 'Detected';}
						if($patient_data[0]->T_DLL_01_VLC_Result == 2){echo 'Not Detected';}
					} ?>
				</span></p>
			</td> -->
				<td colspan="6">
			</td>
		</tr>

		<!-- end Sample Receipt Date -->
		<?php } ?>
		

		<?php } } ?>
					</tbody>
				</table>


		<!-- End HBV DNA test module.... -->



<?php } ?>

<!-- HEP-B END -->

<!-- ................known_history.............. -->
<?php // if(count($patient_data) > 0 && $patient_data[0]->MF5 == 1){ ?>

<h5 style="margin-left: 15px;margin-bottom: -10px;margin-top: 40px;">Known History</h5>
<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
	<tr>
		<td colspan="5"><p style="margin-left: 20px">Treatment experienced :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->PatientType == 1){echo 'No';}
				if($patient_data[0]->PatientType == 2){echo 'Yes';}						
			} ?></span></p>
		</td>
	</tr>


<?php  if(count($patient_data) > 0 && $patient_data[0]->PatientType == 2){ ?>
	<tr>
		<td ><p style="margin-left: 20px">Treating hospital/health facility :- <span><?php echo (count($FacilityCode) > 0)?$FacilityCode[0]->FacilityCode:''; ?></span></p>
		</td>

		<td><p>Previous regimen :- <span><?php echo (count($past_regimen) > 0)?$past_regimen[0]->regimen_name:''; ?></span></p>
		</td>

		<td colspan="3"><p>Previous duration :- <span><?php echo (count($previous_duration) > 0)?$previous_duration[0]->LookupValue:''; ?></span></p>
		</td>

	</tr>

	<tr>
		<td ><p style="margin-left: 20px">Previous Status :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->HCVPastTreatment == 2){echo 'Interrupted';}
				if($patient_data[0]->HCVPastTreatment == 3){echo 'Completed';}						
			} ?>
		</span></p>
	</td>
	<td><p>No. of weeks completed :- <span><?php echo (count($weeks) > 0)?$weeks[0]->LookupValue:''; ?></span></p>
	</td>
	<td colspan="3"><p>Last pill taken on :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->LastPillDate:''; ?></span></p>
	</td>
</tr>
<tr>
	<td colspan="5">
		<p style="margin-left: 20px">Past treatment outcome:&nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVPastOutcome == 1)?'checked':''; ?>> &nbsp;&nbsp;SVR Pending &nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVPastOutcome == 2)?'checked':''; ?>> &nbsp;&nbsp;SVR achieved &nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVPastOutcome == 3)?'checked':''; ?>> &nbsp;&nbsp;SVR not achieved</p>

		</td>
	</tr>
<?php  } ?>
<?php   ?>


	<tr>
			<td colspan="5">
				<?php foreach ($ailments_list as $ailment) { 

					$checked = '';

					foreach ($patient_tblRiskProfile_data as $row) 
					{

						if($row->RiskID == $ailment->LookupCode)
						{
							$checked = 'checked';
							break;
						}
					}
					?>
					<?php  if(!empty($checked)){?>
			<input type="checkbox" value="<?php echo $ailment->LookupCode; ?>" name="ailment[<?php echo $ailment->LookupCode; ?>]" id="ailment_<?php echo $ailment->LookupCode; ?>" <?php echo $checked; ?>><span><?php echo $ailment->LookupValue; ?></span>

		<?php } }?>
				</td>
			</tr>

<br/>

	<tr>
<?php if((count($hiv_regimen) > 0) && $hiv_regimen[0]->LookupValue){ ?>
		<td ><p style="margin-left: 20px">HIV/ART regimen :- <span>
			<?php echo (count($hiv_regimen) > 0)?$hiv_regimen[0]->LookupValue:''; ?>
		</span></p>
	</td>
<?php } ?>
<?php if((count($renal_ckd) > 0) && $renal_ckd[0]->LookupValue){ ?>
	<td colspan="4"><p style="margin-left: 20px">Renal/CKD stage :- <span>
		<?php echo (count($renal_ckd) > 0)?$renal_ckd[0]->LookupValue:''; ?>
	</span></p>
<?php } ?>
</td>

</tr>
<?php   if(count($patient_datahepb_knownhistory) > 0){ ?>
<tr>
	<td colspan="5"><p style="margin-left: 20px">Referred :- <span>
		<?php
			if($patient_datahepb_knownhistory[0]->IsReferal == 1){echo 'Yes';}
			if($patient_datahepb_knownhistory[0]->IsReferal == 2){echo 'No';}						
		 ?></span></p>
	</td>
</tr>
<?php } ?>
<br>			

<tr>
	<?php if((count($referring_doctor) > 0) && $referring_doctor[0]->name ){ ?> 
	<td ><p style="margin-left: 20px">Referring doctor :- <span>
		<?php echo (count($referring_doctor) > 0)?$referring_doctor[0]->name:''; ?>
	</span></p>
</td>
<?php } ?>
<?php if((count($referred_to) > 0) && $referred_to[0]->facility_short_name){ ?>
<td><p>Referred to :- <span>
	<?php echo (count($referred_to) > 0)?$referred_to[0]->facility_short_name:''; ?>
</span></p>
</td>	
<?php } ?>
<?php if((count($patient_data) > 0) && $patient_data[0]->ReferalDate){ ?>			
<td colspan="3"><p>Date of referral :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->ReferalDate:''; ?></span></p>
</td>	
<?php } ?>			
</tr>
<?php if((count($patient_data) > 0) && $patient_data[0]->SCRemarks){ ?>
<tr>
	<td colspan="5"><p style="margin-left: 20px">Observations :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->SCRemarks:''; ?></span></p>
	</td>
	<?php } ?>		
		<td colspan="5">
	</td>		
</tr>	
</table>	

<?php if($patient_data[0]->IsReferal == 1) { ?>
<!-- ...........................Patient Referral Slip....................................... -->


<h5 style="margin-left: 15px;">Patient Referral Slip</h5>
<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
	<tr>
		<td colspan="2"><p style="margin-left: 20px">Patient Name : - <b><?php echo (count($patient_data) > 0)?ucfirst($patient_data[0]->FirstName):''; ?></b> has been referred by (Doctor Name) :- 
			<b><?php echo (count($referring_doctor) > 0)?ucfirst($referring_doctor[0]->name):''; ?> </b>
			at (Name of the Centre)  
			<b><?php echo (count($patient_data) > 0)?$oncenter[0]->facility_short_name :'NULL'; ?></b> 
			on (Date) <b> <?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->ReferalDate):''; ?></b> 
			to (Name of ‘referred to’ Hospital)<b>  <?php echo (count($patient_data) > 0)?$referred_to[0]->facility_short_name :'NULL'; ?></b> 
			for management of Hepatitis-C. 
		The patient has (degree of cirrhosis) <b>
		<?php  if($patient_data[0]->V1_Cirrhosis == 1){echo 'Complicated';}
			if($patient_data[0]->V1_Cirrhosis == 2){echo 'Uncomplicated';} ?></b> and (co-morbidity, if any)   and is placed under <b> <?php if(count($risk_name) > 0){
foreach ($risk_name  as $value) {
	echo $value->LookupValue .' , ';
	
}
	
}
?></b>.</p></td>				
	</tr>
	<tr style="height: 70px;">
		<td colspan="2"></td>
	</tr>	
	<tr>
		<td><p style="margin-left: 20px">Signature</p></td>
		<td><p style="margin-right: -105px">Name and designation</p></td>
	</tr>		
</table>
<?php } ?>

<?php //} ?>
<?php //if(count($patient_data) > 0 &&  $patient_data[0]->PrescribingDate !='' ) { ?>
<h5 style="margin-left: 15px; margin-top: 35px;" >Prescription information</h5>
<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
	<tr>
		<td><p style="margin-left: 20px">Prescribing date :- <span><?php echo (count($patient_datahepb_knownhistory) > 0)?timeStampShow($patient_datahepb_knownhistory[0]->PrescribingDate):''; ?></span></p>
		</td>
		
		<td><p>Prescribing Doctor :- <span><?php echo (count($prescribing_doctor) > 0)?$prescribing_doctor[0]->name:''; ?></span></p>
		</td>

		<td><p>Prescribing Facility : <?php echo (count($dispensationdata_val) > 0)?$dispensationdata_val[0]->facility_short_name:''; ?></p></td>
	</tr>

	<tr>
		<td ><p style="margin-left: 20px">Regimen Prescribed :- <span><?php echo (count($regimen_prescribed) > 0)?$regimen_prescribed[0]->regimen_name:''; ?></span></p>
		</td>
		

</tr>

<tr>
	<td ><p style="margin-left: 20px"><span><?php echo (count($regimen_prescribed) > 0)?$regimen_prescribed[0]->regimen_name:''; ?></span> :- <span><?php if(isset($mst_drug_strength_hepb)){ echo (count($mst_drug_strength_hepb) > 0)?$mst_drug_strength_hepb[0]->strength:''; }?></span></p>
	</td>

	

</tr>

<tr>
	<td ><p style="margin-left: 20px">Place of dispensation :  <?php echo (count($dispensationdata_val) > 0)?$dispensationdata_val[0]->facility_short_name:''; ?></p></td>
	<td colspan="2"><p></p></td>

</tr>
</table>
<?php// } ?>
<!-- ...............................Dispensation information................................. -->


<?php //if(count($patient_data) > 0 &&  $patient_data[0]->T_Initiation !='' ) { ?>
<h5 style="margin-left: 15px;">Dispensation information</h5>
<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
<tr style="border: 1px solid black;">
<td colspan="4"><p style="margin-left: 20px">Date Of Treatment Initiation  :- <span><?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->PrescribingDate):''; ?></span></p></td>

<!-- <td colspan="3"><p style="margin-left: 20px">Days of Pills Dispensed  :- <span><?php echo '28'; ?></span></p></td> -->

<td colspan="3"><p style="margin-left: 20px">Advised Next Visit Date  :- <span><?php echo (count($patient_data) > 0)?timeStampShow($dispensation_result[0]->Visit_Dt):''; ?></span></p></td>

</tr>			

	<tr style="border: 1px solid black;">
		<td style="border: 1px solid black;text-align: center;"><p>Dispensation Number</p></td>
		<td style="border: 1px solid black;text-align: center;"><p>Visit Date</p></td>
		<td style="border: 1px solid black;text-align: center;"><p>Pills Dispensed</p></td>
		<td style="border: 1px solid black;text-align: center;"><p>Advised Next visit Date</p></td>
		<td style="border: 1px solid black;text-align: center;"><p>Place Of Dispensation</p></td>
		<!-- <td style="border: 1px solid black;text-align: center;"><p>Reasons for non- adherence</p></td> -->
		<!-- <td style="border: 1px solid black;text-align: center;"><p>Side Effects</p></td> -->
		<!-- <td style="border: 1px solid black;text-align: center;"><p>Haemoglobin</p></td> -->
		<!-- <td style="border: 1px solid black;text-align: center;"><p>Platelet</p></td> -->
		<!-- <td style="border: 1px solid black;text-align: center;"><p> Comments</p></td> -->
	</tr>

	<?php if(count($tblpatientdispensationb_list) > 0){ 
		foreach($tblpatientdispensationb_list as $row){
			?>
			<tr style="border: 1px solid black;">
				<td style="border: 1px solid black;text-align: center;"><?=$row->VisitNo?></td>
				<td style="border: 1px solid black;text-align: center;"><?=timeStampShow($row->Treatment_Dt);?></td>
				<td style="border: 1px solid black;text-align: center;"><?=($row->PillsDispensed == 0 ? '--':$row->PillsDispensed)?></td>
				<td style="border: 1px solid black;text-align: center;"><?=timeStampShow($row->NextVisit)?></td>				
				<td style="border: 1px solid black;text-align: center;"><?=$row->FacilityCode; ?></td>
				<!-- <td style="border: 1px solid black;text-align: center;"><?=$row->NAdherenceReason; ?></td> -->
				<!-- <td style="border: 1px solid black;text-align: center;"><?=$row->SideEffectValue; ?></td> -->
				
				<!-- <td style="border: 1px solid black;text-align: center;"><?=$row->Haemoglobin; ?></td> -->
				<!-- <td style="border: 1px solid black;text-align: center;"><?=$row->PlateletCount; ?></td> -->
				<!-- <td style="border: 1px solid black;text-align: center;"><?=$row->Comments; ?></td> -->
				
			</tr>
		<?php } }?>		

	</table>
<?php //} ?>
<?php if(count($patient_data) > 0 && ($patient_data[0]->SVR12W_Result == 1 || $patient_data[0]->SVR12W_Result == 2)){ ?>
	<!-- ......................SVR information.................................. -->

	<h5 style="margin-left: 15px;">SVR information</h5>
	<table style="border: 1px solid black;width: 95%; margin-left: 15px;">			

		<tr>
			<td><p style="margin-left: 20px;">Sample Drawn Date :- <span><?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->SVRDrawnDate):''; ?></span></p>
			</td>

			<td><p>Viral Load :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->SVR12W_HCVViralLoadCount:''; ?></span></p>
			</td>
			<td><p>Result :- <span><?php echo (count($svr_result) > 0)?$svr_result[0]->LookupValue:''; ?></span></p>
			</td>

		</tr>

		<tr>
			<td colspan="3"><p style="margin-left: 20px;">Remarks :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->SVRTransportRemark:''; ?></span></p>
			</td>
		</tr>
	</table>
<?php  } ?>

<?php if(count($patient_data) > 0 && $patient_data[0]->InterruptReason == 1 ){ ?>  
	<!-- ...................Interrupted Patient information........................ -->
	<h5 style="margin-left: 15px; margin-top: 60px;">Interrupted Patient information</h5>
	<table style="border: 1px solid black;width: 95%; margin-left: 15px;">			

		<tr>
			<td><p style="margin-left: 20px;">Reason for interruption :- <span>
				<?php if(isset($InterruptReason)){
					if(count($InterruptReason)>0){
						echo $InterruptReason[0]->LookupValue;
					}
				}?></span></p></td>
				<td><p>Reason for (Death/LFU) :- <span>
				<?php if(isset($DeathReason)){
					if(count($DeathReason)>0){
						echo $DeathReason[0]->LookupValue;
					}
				}?></span></p></td>
				<td><p> Patient referred for:&nbsp;&nbsp;
					<input type="checkbox" name="">&nbsp;&nbsp;ETR
					<input type="checkbox" name="">&nbsp;&nbsp;SVR</p>
				</td>

			</tr>

			<tr>
				<td colspan="3"><p style="margin-left: 20px;"><input type="checkbox" name="">&nbsp;&nbsp;None:</p></td>
			</tr>
		</table>
<?php } ?>
		<div style="line-height: 250px;">
			<table style="width: 95%; margin-left: 25px;">
				<tr>
					<td><p>Signature</p></td>
					<td><p style="margin-right: -105px;">Name and designation</p></td>
				</tr>
			</table>			
		</div>

	</div>