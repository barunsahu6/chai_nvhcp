<style>
thead
{
	background-color: #085786;
	color: white;
}

.table-bordered>tbody>tr>td.data_td
{
	font-weight: 600;
	font-size: 15px;
	text-align: center;
	vertical-align: middle;
	background-color: #e4e4e4;
	border-color:#ffffff;
	border-width:1px;
	color:#000000;
}
.table-bordered>tbody>tr>td.data_td_late
{
	font-weight: 600;
	font-size: 16px;
	background-color: #ffffff;
	text-align: center;
	vertical-align: middle;
}
</style>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 3 !important;
	width: 100% !important;
	font-size: 14px !important;
	font-family: 'Source Sans Pro';
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
	.col-md-2{
		width: 16.33%;
		padding-left: 8px;
		padding-right: 8px;
	}
	.btn-width{
	width: 12%;
	margin-top: 20px;
}
.pd-15{
	padding-left: 15px;
}
.pb-20{
	padding-bottom: 20px;
}
.container{
	width: 100%;
}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 4 !important;
		width: 100% !important;
		font-size: 14px !important;
		font-family: 'Source Sans Pro';
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 7px 12px;
	margin-bottom: 0;
	font-size: 13px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
 .table-bordered>thead>tr>th{
	vertical-align: middle;
	font-size: 13px;
}
.table-bordered>tbody>tr>td{
	vertical-align: middle;
	font-size: 13px;
	color: #000000;
}
</style>
</style>
<br>

<?php $loginData = $this->session->userdata('loginData');
//echo "<pre>"; print_r($loginData);

if($loginData->user_type==1) { 
$select = '';
}else{
$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
$select4 = '';	
}

$filters1 = $this->session->userdata('filters1'); 
//pr($filters1);
 ?>
<div class="container">
		<div class="panel panel-default" id="Monthly_Report_Panel">
				<div class="panel-heading" style="background-color: #333333;padding: 3px 0px;">
					<h4 class="text-center" style="background-color: #333333;color: white; font-family: 'Source Sans Pro';letter-spacing: .75px;">Offline Entry Comparison Report (HBV)
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/excel_black.png" alt=""></a> -->
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/pdf_color.png" alt=""></a> -->
		</h4>
		<!-- <input type="button" onclick="printDiv('printableArea')" value="Print" /> -->
		
</div>

<?php
           $attributes = array(
              'id' => 'filter_form',
              'name' => 'filter_form',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>
<div class="row" style="padding: 5px 0px 5px 15px;">

	<div class="col-md-2 col-sm-12 col-md-offset-2">
		<label for="">State</label>
		<select type="text" name="search_state" id="search_state" class="form-control input_fields" required <?php echo $select2.$select3.$select4; ?>>
			<option value="0">All States</option>
			<?php 
			foreach ($states as $state) {
				?>
				<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo $state->StateName; ?></option>
				<?php 
			}
			?>
		</select>
	</div>
<!-- 	<div class="col-md-2 col-sm-12">
	<label for="">District</label>
	<select type="text" name="input_district" id="input_district" class="form-control input_fields"   <?php echo $select.$select2.$select4; ?>>
		

		<?php foreach ($districts as  $value) { ?>
			<option value="<?php echo $value->id_mstdistrict; ?>" <?php if($this->input->post('input_district')==$value->id_mstdistrict) { echo 'selected';}  ?>><?php echo $value->DistrictName; ?></option>
		<?php } ?>
		<?php 
		
		?>
	</select>
</div>
<div class="col-md-2 col-sm-12">
	<label for="">Facility</label>
	<select type="text" name="facility" id="mstfacilitylogin" class="form-control input_fields"  <?php echo $select.$select2; ?>>
		<option value="">Select Facility</option>
		<?php 
		
		?>
	</select>
</div> -->
	<div class="col-md-2 col-sm-12">
		<label for="">Year</label>
		<select name="year" id="year" required class="form-control input_fields" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2020', date('Y'));
										$flag=0;
										foreach($dates as $date){
												$year = $date;
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									elseif($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
															
								}
								?>
							</select>
	</div>

	<div class="col-md-2 col-sm-12">
		<label for="">Month</label>
		<select type="text" name="month" id="month" class="form-control input_fields" required>
<option value="">Select</option>
<?php
	$flag=0;
for ($i = 3; $i <=12;$i++) {
$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
if($this->input->post('month')==$i){
echo "<option value=".$i." selected>".$date_str ."</option>";
$flag=1;
}
elseif($i==date('m') && $flag==0){
	echo "<option value=".$i." selected>".$date_str ."</option>";
}
else{
	echo "<option value=".$i.">".$date_str ."</option>";

}
} ?>
</select>
	</div>

<div class="col-lg-2 col-md-2 col-sm-12 btn-width" style="margin-top: 20px;">
					<button type="submit" class="btn btn-block btn-success" id="monthreport" style="line-height: 1.2; font-weight: 600;">SEARCH</button>
				</div><span style="margin-top: 25px;margin-right: 25px; float: right;"><a href="javascript:void(0)"><i class="fa fa-2x fa-download" id="excel_button" onclick="tableToExcel('testTable', 'W3C Example','Late_Monthly_Report(HBV).xls')" title="Excel Download"></i></a></span>
			</div>

 <?php echo form_close(); ?>
<br>
</div>
<div class="row" id="printableArea">

	<div class="col-md-12">
		<table class="table table-bordered table-highlighted" id="testTable" >

			<thead>
					<th style="background-color: white;color: black;font-family:'Source Sans Pro';text-align: center;border:none;" colspan="2">&nbsp;</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;" colspan="5">Regular Monthly Entry</th>
				

				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;" colspan="5">Late Entry</th>
					<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;" colspan="5">Offline Monthly Entry</th>
			</thead>
			<thead>
					<th style="background-color: #bbbbbb;color: black;">1</th>
				<th style="background-color: #bbbbbb;color: black; font-family:'Source Sans Pro';">Number of Hepatitis B infected people seeking care at the treatment center (Registering in Care)</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" >Adult <br>Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult<br> Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult<br> Male</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult<br> Female</th>
				<th style="background-color: #01446b;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>


				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" >Adult <br>Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult<br> Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

			</thead>
			<tbody>


				<?php $total_chlildren_1_1=($count_1_1['children_male'][0]->count+$count_1_1['children_female'][0]->count+$count_1_1['children_transgender'][0]->count); ?>

				<tr style="font-family:'Source Sans Pro';">
					<td>1.1</td>
					<td>Total number of patients screened for HBsAg in that month</td>
				<td class="data_td"><?php echo !empty($count_1_1['adult_male'][0]->count)>0 ? $count_1_1['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_1_1['adult_female'][0]->count)>0 ? $count_1_1['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_1_1['adult_transgender'][0]->count)>0 ? ($count_1_1['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_1_1)>0 ? $total_chlildren_1_1 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_1_1['adult_male'][0]->count + $count_1_1['adult_female'][0]->count + $total_chlildren_1_1 + $count_1_1['adult_transgender'][0]->count); ?></td>

					<td class="data_td_late"><?php echo !empty($late_count_1_1->male)>0 ? $late_count_1_1->male : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_1_1->female)>0 ? $late_count_1_1->female : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_1_1->transgender)>0 ? $late_count_1_1->transgender  : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_1_1->children)>0 ? $late_count_1_1->children : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_1_1->male + $late_count_1_1->female + $late_count_1_1->children +$late_count_1_1->transgender); ?></td>


					<td class="text-center data_td"><?php echo !empty($male->hbv_screened)>0 ? $male->hbv_screened : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_screened)>0 ? $female->hbv_screened : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_screened)>0 ? $transgender->hbv_screened : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_screened)>0 ? $children->hbv_screened : "-"; ?></td>
					<?php
						$sum_1_1=(!empty($male->hbv_screened) > 0 ? $male->hbv_screened : 0) +(!empty($female->hbv_screened) >0 ? $female->hbv_screened : 0) +(!empty($transgender->hbv_screened)>0 ? $transgender->hbv_screened : 0) +(!empty($children->hbv_screened) >0 ? $children->hbv_screened : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_1_1; ?></td>
				</tr>

					<?php $total_chlildren_1_2=($count_1_2['children_male'][0]->count+$count_1_2['children_female'][0]->count+$count_1_2['children_transgender'][0]->count); ?>

				<tr style="font-family:'Source Sans Pro';">
					<td>1.2</td>
					<td>Total Number of patients found positive for HBsAg in that month</td>
					<td class="data_td"><?php echo !empty($count_1_2['adult_male'][0]->count)>0 ? $count_1_2['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_1_2['adult_female'][0]->count)>0 ? $count_1_2['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_1_2['adult_transgender'][0]->count)>0 ? ($count_1_2['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_1_2)>0 ? $total_chlildren_1_2 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_1_2['adult_male'][0]->count + $count_1_2['adult_female'][0]->count + $total_chlildren_1_2 + $count_1_2['adult_transgender'][0]->count); ?></td>



					<td class="data_td_late"><?php echo !empty($late_count_1_2->male)>0 ? $late_count_1_2->male : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_1_2->female)>0 ? $late_count_1_2->female : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_1_2->transgender)>0 ? $late_count_1_2->transgender  : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_1_2->children)>0 ? $late_count_1_2->children : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_1_2->male + $late_count_1_2->female + $late_count_1_2->children +$late_count_1_2->transgender); ?></td>

						
						<td class="text-center data_td"><?php echo !empty($male->hbv_positive)>0 ? $male->hbv_positive : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_positive)>0 ? $female->hbv_positive : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_positive)>0 ? $transgender->hbv_positive : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_positive)>0 ? $children->hbv_positive : "-"; ?></td>
					<?php
						$sum_1_2=(!empty($male->hbv_positive) > 0 ? $male->hbv_positive : 0) +(!empty($female->hbv_positive) >0 ? $female->hbv_positive : 0) +(!empty($transgender->hbv_positive)>0 ? $transgender->hbv_positive : 0) +(!empty($children->hbv_positive) >0 ? $children->hbv_positive : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_1_2; ?></td>	


				</tr>



			</tbody>
			<thead>
				<th style="background-color: #bbbbbb;color: black; ">2</th>
				<th style="background-color: #bbbbbb;color: black;font-family:'Source Sans Pro';">Initiation of treatment</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" >Adult <br>Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult <br>Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult <br>Male</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult <br>Female</th>
				<th style="background-color: #01446b;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>


				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" >Adult <br>Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult<br> Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</thead>
			<tbody>

					<?php $total_chlildren_2_1=($count_2_1['children_male'][0]->count+$count_2_1['children_female'][0]->count+$count_2_1['children_transgender'][0]->count); ?>
				<tr style="font-family:'Source Sans Pro';">
					<td >2.1</td>
					<td>Number of new patients started on treatment during this month</td>
						<td class="data_td"><?php echo !empty($count_2_1['adult_male'][0]->count)>0 ? $count_2_1['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_2_1['adult_female'][0]->count)>0 ? $count_2_1['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_2_1['adult_transgender'][0]->count)>0 ? ($count_2_1['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_2_1)>0 ? $total_chlildren_2_1 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_2_1['adult_male'][0]->count + $count_2_1['adult_female'][0]->count + $total_chlildren_2_1 + $count_2_1['adult_transgender'][0]->count); ?></td>

					<td class="data_td_late"><?php echo !empty($late_count_2_1->male)>0 ? $late_count_2_1->male : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_2_1->female)>0 ? $late_count_2_1->female : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_2_1->transgender)>0 ? $late_count_2_1->transgender  : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_2_1->children)>0 ? $late_count_2_1->children : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_2_1->male + $late_count_2_1->female + $late_count_2_1->children +$late_count_2_1->transgender); ?></td>

						<td class="text-center data_td"><?php echo !empty($male->hbv_treatment_start)>0 ? $male->hbv_treatment_start : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_treatment_start)>0 ? $female->hbv_treatment_start : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_treatment_start)>0 ? $transgender->hbv_treatment_start : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_treatment_start)>0 ? $children->hbv_treatment_start : "-"; ?></td>
					<?php
						$sum_2_1=(!empty($male->hbv_treatment_start) > 0 ? $male->hbv_treatment_start : 0) +(!empty($female->hbv_treatment_start) >0 ? $female->hbv_treatment_start : 0) +(!empty($transgender->hbv_treatment_start)>0 ? $transgender->hbv_treatment_start : 0) +(!empty($children->hbv_treatment_start) >0 ? $children->hbv_treatment_start : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_2_1; ?></td>

				</tr>
				<tr style="font-family:'Source Sans Pro';">
					<td>2.2</td>
					<td>Number of patients on treatment "transferred in" during this month</td>

					<td class="data_td"><?php echo "-"; ?></td>
					<td class="data_td"><?php echo "-"; ?></td>
					<td class="data_td"><?php echo "-"; ?></td>
					<td class="data_td"><?php echo "-"; ?></td>
					<td class="data_td"><?php echo "0"; ?></td>	

					<td class="data_td_late"><?php echo "-"; ?></td>
					<td class="data_td_late"><?php echo "-"; ?></td>
					<td class="data_td_late"><?php echo "-"; ?></td>
					<td class="data_td_late"><?php echo "-"; ?></td>
						<td class="data_td_late"><?php echo "0"; ?></td>

					<td class="text-center data_td"><?php echo !empty($male->hbv_transferred_in)>0 ? $male->hbv_transferred_in : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_transferred_in)>0 ? $female->hbv_transferred_in : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_transferred_in)>0 ? $transgender->hbv_transferred_in : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_transferred_in)>0 ? $children->hbv_transferred_in : "-"; ?></td>
					<?php
						$sum_2_2=(!empty($male->hbv_transferred_in) > 0 ? $male->hbv_transferred_in : 0) +(!empty($female->hbv_transferred_in) >0 ? $female->hbv_transferred_in : 0) +(!empty($transgender->hbv_transferred_in)>0 ? $transgender->hbv_transferred_in : 0) +(!empty($children->hbv_transferred_in) >0 ? $children->hbv_transferred_in : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_2_2; ?></td>

				</tr>

				<?php 
						$male_2_3=(!empty($male->hbv_transferred_in) > 0 ? $male->hbv_transferred_in : 0) +(!empty($male->hbv_treatment_start) > 0 ? $male->hbv_treatment_start : 0);
						$female_2_3=(!empty($female->hbv_transferred_in) > 0 ? $female->hbv_transferred_in : 0) +(!empty($female->hbv_treatment_start) > 0 ? $female->hbv_treatment_start : 0);
						$transgender_2_3=(!empty($transgender->hbv_transferred_in) > 0 ? $transgender->hbv_transferred_in : 0) +(!empty($transgender->hbv_treatment_start) > 0 ? $transgender->hbv_treatment_start : 0);
						$children_2_3=(!empty($children->hbv_transferred_in) > 0 ? $children->hbv_transferred_in : 0) +(!empty($children->hbv_treatment_start) > 0 ? $children->hbv_treatment_start : 0);

				?>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>2.3</td>
					<td>Cumulative number of patients ever received treatment (number at the end of this month) </td>

					<td class="data_td"><?php echo !empty($count_2_1['adult_male'][0]->count)>0 ? $count_2_1['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_2_1['adult_female'][0]->count)>0 ? $count_2_1['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_2_1['adult_transgender'][0]->count)>0 ? ($count_2_1['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_2_1)>0 ? $total_chlildren_2_1 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_2_1['adult_male'][0]->count + $count_2_1['adult_female'][0]->count + $total_chlildren_2_1 + $count_2_1['adult_transgender'][0]->count); ?></td>


					<td class="data_td_late"><?php echo !empty($late_count_2_1->male)>0 ? $late_count_2_1->male : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_2_1->female)>0 ? $late_count_2_1->female : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_2_1->transgender)>0 ? $late_count_2_1->transgender  : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_2_1->children)>0 ? $late_count_2_1->children : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_2_1->male + $late_count_2_1->female + $late_count_2_1->children +$late_count_2_1->transgender); ?></td>

						<td class="text-center data_td"><?php echo !empty($male_2_3)>0 ? $male_2_3 : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($female_2_3)>0 ? $female_2_3 : "-"; ?></td>
						<td class="text-center data_td"><?php echo !empty($transgender_2_3)>0 ? $transgender_2_3 : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children_2_3)>0 ? $children_2_3 : "-"; ?></td>
					
					<td class="text-center data_td"><?php echo ($male_2_3+$female_2_3+$transgender_2_3+$children_2_3); ?></td>

				</tr>
			</tbody>

			<thead>
				<th style="background-color: #bbbbbb;color: black; font-family:'Source Sans Pro';">3</th>
				<th style="background-color: #bbbbbb;color: black; font-family:'Source Sans Pro';">Regimen status</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" >Adult <br>Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult <br>Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult <br>Male</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult <br>Female</th>
				<th style="background-color: #01446b;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" >Adult <br>Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult<br> Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</thead>
			<tbody>

			<?php $total_chlildren_3_1=($count_3_1['children_male'][0]->count+$count_3_1['children_female'][0]->count+$count_3_1['children_transgender'][0]->count); ?>

			<?php $total_chlildren_3_2=($count_3_2['children_male'][0]->count+$count_3_2['children_female'][0]->count+$count_3_2['children_transgender'][0]->count); ?>

			<?php $total_chlildren_3_3=($count_3_3['children_male'][0]->count+$count_3_3['children_female'][0]->count+$count_3_3['children_transgender'][0]->count); ?>

			<?php $total_chlildren_3_4=($count_3_4['children_male'][0]->count+$count_3_4['children_female'][0]->count+$count_3_4['children_transgender'][0]->count); ?>

				<tr style="font-family:'Source Sans Pro';">
					<td>3.1</td>
					<td>Cumulative number of Hepatitis B patients on TAF</td>

					<td class="data_td"><?php echo !empty($count_3_1['adult_male'][0]->count)>0 ? $count_3_1['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_1['adult_female'][0]->count)>0 ? $count_3_1['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_1['adult_transgender'][0]->count)>0 ? ($count_3_1['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_3_1)>0 ? $total_chlildren_3_1 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_3_1['adult_male'][0]->count + $count_3_1['adult_female'][0]->count + $total_chlildren_3_1 + $count_3_1['adult_transgender'][0]->count); ?></td>


					<td class="data_td_late"><?php echo !empty($late_count_3_1->male)>0 ? $late_count_3_1->male : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_3_1->female)>0 ? $late_count_3_1->female : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_3_1->transgender)>0 ? $late_count_3_1->transgender  : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_3_1->children)>0 ? $late_count_3_1->children : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_3_1->male + $late_count_3_1->female + $late_count_3_1->children +$late_count_3_1->transgender); ?></td>


						<td class="text-center data_td"><?php echo !empty($male->hbv_taf)>0 ? $male->hbv_taf : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_taf)>0 ? $female->hbv_taf : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_taf)>0 ? $transgender->hbv_taf : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_taf)>0 ? $children->hbv_taf : "-"; ?></td>
					<?php
						$sum_3_1=(!empty($male->hbv_taf) > 0 ? $male->hbv_taf : 0) +(!empty($female->hbv_taf) >0 ? $female->hbv_taf : 0) +(!empty($transgender->hbv_taf)>0 ? $transgender->hbv_taf : 0) +(!empty($children->hbv_taf) >0 ? $children->hbv_taf : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_3_1; ?></td>
				</tr>

				<tr style="font-family:'Source Sans Pro';">
					<td>3.2</td>
					<td>Cumulative number of Hepatitis B patients on Entecavir</td>
					<td class="data_td"><?php echo !empty($count_3_2['adult_male'][0]->count)>0 ? $count_3_2['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_2['adult_female'][0]->count)>0 ? $count_3_2['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_2['adult_transgender'][0]->count)>0 ? ($count_3_2['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_3_2)>0 ? $total_chlildren_3_2 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_3_2['adult_male'][0]->count + $count_3_2['adult_female'][0]->count + $total_chlildren_3_2 + $count_3_2['adult_transgender'][0]->count); ?></td>

					<td class="data_td_late"><?php echo !empty($late_count_3_2->male)>0 ? $late_count_3_2->male : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_3_2->female)>0 ? $late_count_3_2->female : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_3_2->transgender)>0 ? $late_count_3_2->transgender  : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_3_2->children)>0 ? $late_count_3_2->children : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_3_2->male + $late_count_3_2->female + $late_count_3_2->children +$late_count_3_2->transgender); ?></td>

						<td class="text-center data_td"><?php echo !empty($male->hbv_ent)>0 ? $male->hbv_ent : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_ent)>0 ? $female->hbv_ent : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_ent)>0 ? $transgender->hbv_ent : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_ent)>0 ? $children->hbv_ent : "-"; ?></td>
					<?php
						$sum_3_2=(!empty($male->hbv_ent) > 0 ? $male->hbv_ent : 0) +(!empty($female->hbv_ent) >0 ? $female->hbv_ent : 0) +(!empty($transgender->hbv_ent)>0 ? $transgender->hbv_ent : 0) +(!empty($children->hbv_ent) >0 ? $children->hbv_ent : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_3_2; ?></td>
				</tr>
				

				<tr style="font-family:'Source Sans Pro';">
					<td>3.3</td>
					<td>Cumulative number of Hepatitis B patients on TDF</td>
				<td class="data_td"><?php echo !empty($count_3_3['adult_male'][0]->count)>0 ? $count_3_3['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_3['adult_female'][0]->count)>0 ? $count_3_3['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_3['adult_transgender'][0]->count)>0 ? ($count_3_3['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_3_3)>0 ? $total_chlildren_3_3 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_3_3['adult_male'][0]->count + $count_3_3['adult_female'][0]->count + $total_chlildren_3_3 + $count_3_3['adult_transgender'][0]->count); ?></td>

					<td class="data_td_late"><?php echo !empty($late_count_3_3->male)>0 ? $late_count_3_3->male : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_3_3->female)>0 ? $late_count_3_3->female : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_3_3->transgender)>0 ? $late_count_3_3->transgender  : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_3_3->children)>0 ? $late_count_3_3->children : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_3_3->male + $late_count_3_3->female + $late_count_3_3->children +$late_count_3_3->transgender); ?></td>

						<td class="text-center data_td"><?php echo !empty($male->hbv_tdf)>0 ? $male->hbv_tdf : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_tdf)>0 ? $female->hbv_tdf : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_tdf)>0 ? $transgender->hbv_tdf : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_tdf)>0 ? $children->hbv_tdf : "-"; ?></td>
					<?php
						$sum_3_3=(!empty($male->hbv_tdf) > 0 ? $male->hbv_tdf : 0) +(!empty($female->hbv_tdf) >0 ? $female->hbv_tdf : 0) +(!empty($transgender->hbv_tdf)>0 ? $transgender->hbv_tdf : 0) +(!empty($children->hbv_tdf) >0 ? $children->hbv_tdf : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_3_3; ?></td>
				</tr>
					<tr style="font-family:'Source Sans Pro';">
					<td>3.4</td>
					<td>Cumulative number of Hepatitis B patients on Other - Interferon based regimen</td>
					<td class="data_td"><?php echo !empty($count_3_4['adult_male'][0]->count)>0 ? $count_3_4['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_4['adult_female'][0]->count)>0 ? $count_3_4['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_3_4['adult_transgender'][0]->count)>0 ? ($count_3_4['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_3_4)>0 ? $total_chlildren_3_4 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_3_4['adult_male'][0]->count + $count_3_4['adult_female'][0]->count + $total_chlildren_3_4 + $count_3_4['adult_transgender'][0]->count); ?></td>


					<td class="data_td_late"><?php echo !empty($late_count_3_4->male)>0 ? $late_count_3_4->male : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_3_4->female)>0 ? $late_count_3_4->female : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_3_4->transgender)>0 ? $late_count_3_4->transgender  : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_3_4->children)>0 ? $late_count_3_4->children : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_3_4->male + $late_count_3_4->female + $late_count_3_4->children +$late_count_3_4->transgender); ?></td>

						<td class="text-center data_td"><?php echo !empty($male->hbv_interferon)>0 ? $male->hbv_interferon : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_interferon)>0 ? $female->hbv_interferon : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_interferon)>0 ? $transgender->hbv_interferon : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_interferon)>0 ? $children->hbv_interferon : "-"; ?></td>
					<?php
						$sum_3_4=(!empty($male->hbv_interferon) > 0 ? $male->hbv_interferon : 0) +(!empty($female->hbv_interferon) >0 ? $female->hbv_interferon : 0) +(!empty($transgender->hbv_interferon)>0 ? $transgender->hbv_interferon : 0) +(!empty($children->hbv_interferon) >0 ? $children->hbv_interferon : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_3_4; ?></td>
				</tr>

			</tbody>

			<thead>
				<th style="background-color: #bbbbbb;color:black ; font-family:'Source Sans Pro';">4</th>
				<th style="background-color: #bbbbbb;color:black ;font-family:'Source Sans Pro';">Treatment status (at the end of the month) out of all patients ever started on treatment</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" >Adult <br>Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult <br>Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult <br>Male</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult <br>Female</th>
				<th style="background-color: #01446b;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>


				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" >Adult <br>Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult<br> Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</thead>
			<tbody>
				<?php $total_chlildren_4_1=($count_4_1['children_male'][0]->count+$count_4_1['children_female'][0]->count+$count_4_1['children_transgender'][0]->count); ?>
					
					<?php $total_chlildren_4_2=($count_4_2['children_male'][0]->count+$count_4_2['children_female'][0]->count+$count_4_2['children_transgender'][0]->count); ?>

					<?php $total_chlildren_4_3=($count_4_3['children_male'][0]->count+$count_4_3['children_female'][0]->count+$count_4_3['children_transgender'][0]->count); ?>

					<?php $total_chlildren_4_5=($count_4_5['children_male'][0]->count+$count_4_5['children_female'][0]->count+$count_4_5['children_transgender'][0]->count); ?>
				

				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>4.1</td>
					<td>The number of all patients whose regimen got changed in this month due to medical reasons</td>
								<td class="data_td"><?php echo !empty($count_4_1['adult_male'][0]->count)>0 ? $count_4_1['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_1['adult_female'][0]->count)>0 ? $count_4_1['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_1['adult_transgender'][0]->count)>0 ? ($count_4_1['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_4_1)>0 ? $total_chlildren_4_1 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_4_1['adult_male'][0]->count + $count_4_1['adult_female'][0]->count + $total_chlildren_4_3 + $count_4_1['adult_transgender'][0]->count); ?></td>

						<td class="data_td_late"><?php echo !empty($late_count_4_1->male)>0 ? $late_count_4_1->male : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_4_1->female)>0 ? $late_count_4_1->female : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_4_1->transgender)>0 ? $late_count_4_1->transgender : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_4_1->children)>0 ? $late_count_4_1->children : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_4_1->male + $late_count_4_1->female+ $late_count_4_1->children + $late_count_4_1->transgender); ?></td>

						<td class="text-center data_td"><?php echo !empty($male->hbv_regimen_changed)>0 ? $male->hbv_regimen_changed : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_regimen_changed)>0 ? $female->hbv_regimen_changed : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_regimen_changed)>0 ? $transgender->hbv_regimen_changed : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_regimen_changed)>0 ? $children->hbv_regimen_changed : "-"; ?></td>
					<?php
						$sum_4_1=(!empty($male->hbv_regimen_changed) > 0 ? $male->hbv_regimen_changed : 0) +(!empty($female->hbv_regimen_changed) >0 ? $female->hbv_regimen_changed : 0) +(!empty($transgender->hbv_regimen_changed)>0 ? $transgender->hbv_regimen_changed : 0) +(!empty($children->hbv_regimen_changed) >0 ? $children->hbv_regimen_changed : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_4_1; ?></td>
				</tr>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>4.2</td>
					<td>Number of patients who are lost to follow-up (LTFU)</td>
							<td class="data_td"><?php echo !empty($count_4_2['adult_male'][0]->count)>0 ? $count_4_2['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_2['adult_female'][0]->count)>0 ? $count_4_2['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_2['adult_transgender'][0]->count)>0 ? ($count_4_2['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_4_2)>0 ? $total_chlildren_4_2 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_4_2['adult_male'][0]->count + $count_4_2['adult_female'][0]->count + $total_chlildren_4_3 + $count_4_2['adult_transgender'][0]->count); ?></td>



					<td class="data_td_late"><?php echo !empty($late_count_4_2->male)>0 ? $late_count_4_2->male : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_4_2->female)>0 ? $late_count_4_2->female : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_4_2->transgender)>0 ? $late_count_4_2->transgender : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_4_2->children)>0 ? $late_count_4_2->children : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_4_2->male + $late_count_4_2->female+ $late_count_4_2->children + $late_count_4_2->transgender); ?></td>

						<td class="text-center data_td"><?php echo !empty($male->hbv_ltfu)>0 ? $male->hbv_ltfu : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_ltfu)>0 ? $female->hbv_ltfu : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_ltfu)>0 ? $transgender->hbv_ltfu : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_ltfu)>0 ? $children->hbv_ltfu : "-"; ?></td>
					<?php
						$sum_4_2=(!empty($male->hbv_ltfu) > 0 ? $male->hbv_ltfu : 0) +(!empty($female->hbv_ltfu) >0 ? $female->hbv_ltfu : 0) +(!empty($transgender->hbv_ltfu)>0 ? $transgender->hbv_ltfu : 0) +(!empty($children->hbv_ltfu) >0 ? $children->hbv_ltfu : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_4_2; ?></td>
				</tr>


				


				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>4.3</td>
					<td>The number of patients who did not return and missed their doses in this month(defaulter)</td>
						<td class="data_td"><?php echo !empty($count_4_3['adult_male'][0]->count)>0 ? $count_4_3['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_3['adult_female'][0]->count)>0 ? $count_4_3['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_3['adult_transgender'][0]->count)>0 ? ($count_4_3['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_4_3)>0 ? $total_chlildren_4_3 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_4_3['adult_male'][0]->count + $count_4_3['adult_female'][0]->count + $total_chlildren_4_3 + $count_4_3['adult_transgender'][0]->count); ?></td>

					<td class="data_td_late"><?php echo !empty($late_count_4_3['adult_male'][0]->count)>0 ? $late_count_4_3['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_4_3['adult_female'][0]->count)>0 ? $late_count_4_3['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_4_3['adult_transgender'][0]->count)>0 ? ($late_count_4_3['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($total_chlildren_4_3)>0 ? $total_chlildren_4_3 : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_4_3['adult_male'][0]->count + $late_count_4_3['adult_female'][0]->count + $total_chlildren_4_3 + $late_count_4_3['adult_transgender'][0]->count); ?></td>

						<td class="text-center data_td"><?php echo !empty($male->hbv_missed_doses)>0 ? $male->hbv_missed_doses : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_missed_doses)>0 ? $female->hbv_missed_doses : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_missed_doses)>0 ? $transgender->hbv_missed_doses : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_missed_doses)>0 ? $children->hbv_missed_doses : "-"; ?></td>
					<?php
						$sum_4_3=(!empty($male->hbv_missed_doses) > 0 ? $male->hbv_missed_doses : 0) +(!empty($female->hbv_missed_doses) >0 ? $female->hbv_missed_doses : 0) +(!empty($transgender->hbv_missed_doses)>0 ? $transgender->hbv_missed_doses : 0) +(!empty($children->hbv_missed_doses) >0 ? $children->hbv_missed_doses : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_4_3; ?></td>
				</tr>
				 <tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>4.4</td>
					<td>Total number of patients Referred to Higher center for further Management</td>
					<td class="data_td"><?php echo "-"; ?></td>
					<td class="data_td"><?php echo "-"; ?></td>
					<td class="data_td"><?php echo "-"; ?></td>
					<td class="data_td"><?php echo "-"; ?></td>
						<td class="data_td"><?php echo "0"; ?></td>
				
					<td class="data_td_late"><?php echo "-"; ?></td>
					<td class="data_td_late"><?php echo "-"; ?></td>
					<td class="data_td_late"><?php echo "-"; ?></td>
					<td class="data_td_late"><?php echo "-"; ?></td>
					<td class="data_td_late"><?php echo "0"; ?></td>	

						<td class="text-center data_td"><?php echo !empty ($male->hbv_reffered)>0 ? $male->hbv_reffered : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_reffered)>0 ? $female->hbv_reffered : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_reffered)>0 ? $transgender->hbv_reffered : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_reffered)>0 ? $children->hbv_reffered : "-"; ?></td>
					<?php
						$sum_4_4=(!empty($male->hbv_reffered) > 0 ? $male->hbv_reffered : 0) +(!empty($female->hbv_reffered) >0 ? $female->hbv_reffered : 0) +(!empty($transgender->hbv_reffered)>0 ? $transgender->hbv_reffered : 0) +(!empty($children->hbv_reffered) >0 ? $children->hbv_reffered : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_4_4; ?></td>
				</tr> 
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>4.5</td>
					<td>Number of deaths reported</td>
				<td class="data_td"><?php echo !empty($count_4_5['adult_male'][0]->count)>0 ? $count_4_5['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_5['adult_female'][0]->count)>0 ? $count_4_5['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_4_5['adult_transgender'][0]->count)>0 ? ($count_4_5['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_4_5)>0 ? $total_chlildren_4_5 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_4_5['adult_male'][0]->count + $count_4_5['adult_female'][0]->count + $total_chlildren_4_3 + $count_4_5['adult_transgender'][0]->count); ?></td>


					<td class="data_td_late"><?php echo !empty($late_count_4_5->male)>0 ? $late_count_4_5->male : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_4_5->female)>0 ? $late_count_4_5->female : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_4_5->transgender)>0 ? $late_count_4_5->transgender : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_4_5->children)>0 ? $late_count_4_5->children : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_4_5->male + $late_count_4_5->female+ $late_count_4_5->children + $late_count_4_5->transgender); ?></td>


						<td class="text-center data_td"><?php echo !empty($male->hbv_death_reported)>0 ? $male->hbv_death_reported : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_death_reported)>0 ? $female->hbv_death_reported : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_death_reported)>0 ? $transgender->hbv_death_reported : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_death_reported)>0 ? $children->hbv_death_reported : "-"; ?></td>
					<?php
						$sum_4_4=(!empty($male->hbv_death_reported) > 0 ? $male->hbv_death_reported : 0) +(!empty($female->hbv_death_reported) >0 ? $female->hbv_death_reported : 0) +(!empty($transgender->hbv_death_reported)>0 ? $transgender->hbv_death_reported : 0) +(!empty($children->hbv_death_reported) >0 ? $children->hbv_death_reported : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_4_4; ?></td>
				</tr>
			</tbody>
			<thead>
				<th style="background-color: #bbbbbb;color: black;font-family:'Source Sans Pro';">5</th>
				<th style="background-color: #bbbbbb;color: black;font-family:'Source Sans Pro';">Testing status and VL status out of all patients with HBsAg positive</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" >Adult <br>Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult <br>Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult <br>Male</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult <br>Female</th>
				<th style="background-color: #01446b;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>


				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" >Adult <br>Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Adult<br> Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</thead>
			<tbody>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">


					<?php $total_chlildren_5_1=($count_5_1['children_male'][0]->count+$count_5_1['children_female'][0]->count+$count_5_1['children_transgender'][0]->count); ?>

					<?php $total_chlildren_5_2=($count_5_2['children_male'][0]->count+$count_5_2['children_female'][0]->count+$count_5_2['children_transgender'][0]->count); ?>

					<?php $total_chlildren_5_3=($count_5_3['children_male'][0]->count+$count_5_3['children_female'][0]->count+$count_5_3['children_transgender'][0]->count); ?>

					<td>5.1</td>
					<td>Number of patients tested for persistently elevated ALT levels</td>
				
				<td class="data_td"><?php echo !empty($count_5_1['adult_male'][0]->count)>0 ? $count_5_1['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_1['adult_female'][0]->count)>0 ? $count_5_1['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_1['adult_transgender'][0]->count)>0 ? ($count_5_1['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_5_1)>0 ? $total_chlildren_5_1 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_5_1['adult_male'][0]->count + $count_5_1['adult_female'][0]->count + $total_chlildren_5_1 + $count_5_1['adult_transgender'][0]->count); ?></td>

					<td class="data_td_late"><?php echo !empty($late_count_5_1->male)>0 ? $late_count_5_1->male : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_5_1->female)>0 ? $late_count_5_1->female : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_5_1->transgender)>0 ? $late_count_5_1->transgender : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_5_1->children)>0 ? $late_count_5_1->children : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_5_1->male + $late_count_5_1->female + $late_count_5_1->children + $late_count_5_1->transgender); ?></td>


						<td class="text-center data_td"><?php echo !empty($male->hbv_alt_levels)>0 ? $male->hbv_alt_levels : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_alt_levels)>0 ? $female->hbv_alt_levels : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_alt_levels)>0 ? $transgender->hbv_alt_levels : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_alt_levels)>0 ? $children->hbv_alt_levels : "-"; ?></td>
					<?php
						$sum_5_1=(!empty($male->hbv_alt_levels) > 0 ? $male->hbv_alt_levels : 0) +(!empty($female->hbv_alt_levels) >0 ? $female->hbv_alt_levels : 0) +(!empty($transgender->hbv_alt_levels)>0 ? $transgender->hbv_alt_levels : 0) +(!empty($children->hbv_alt_levels) >0 ? $children->hbv_alt_levels : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_5_1; ?></td>
				</tr>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>5.2</td>
					<td>Number of patients whose HBV DNA tests were conducted during the month</td>
				
					<td class="data_td"><?php echo !empty($count_5_2['adult_male'][0]->count)>0 ? $count_5_2['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_2['adult_female'][0]->count)>0 ? $count_5_2['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_2['adult_transgender'][0]->count)>0 ? ($count_5_2['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_5_2)>0 ? $total_chlildren_5_2 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_5_2['adult_male'][0]->count + $count_5_2['adult_female'][0]->count + $total_chlildren_5_2 + $count_5_2['adult_transgender'][0]->count); ?></td>

					<td class="data_td_late"><?php echo !empty($late_count_5_2->male)>0 ? $late_count_5_2->male : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_5_2->female)>0 ? $late_count_5_2->female : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_5_2->transgender)>0 ? $late_count_5_2->transgender : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_5_2->children)>0 ? $late_count_5_2->children : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_5_2->male + $late_count_5_2->female + $late_count_5_2->children + $late_count_5_2->transgender); ?></td>


						<td class="text-center data_td"><?php echo !empty($male->hbv_dna)>0 ? $male->hbv_dna : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_dna)>0 ? $female->hbv_dna : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_dna)>0 ? $transgender->hbv_dna : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_dna)>0 ? $children->hbv_dna : "-"; ?></td>
					<?php
						$sum_5_2=(!empty($male->hbv_dna) > 0 ? $male->hbv_dna : 0) +(!empty($female->hbv_dna) >0 ? $female->hbv_dna : 0) +(!empty($transgender->hbv_dna)>0 ? $transgender->hbv_dna : 0) +(!empty($children->hbv_dna) >0 ? $children->hbv_dna : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_5_2; ?></td>
				</tr>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>5.3</td>
					<td>Patients identified as cirrhotic during the month</td>
				
						<td class="data_td"><?php echo !empty($count_5_3['adult_male'][0]->count)>0 ? $count_5_3['adult_male'][0]->count: "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_3['adult_female'][0]->count)>0 ? $count_5_3['adult_female'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($count_5_3['adult_transgender'][0]->count)>0 ? ($count_5_3['adult_transgender'][0]->count) : "-"; ?></td>
					<td class="data_td"><?php echo !empty($total_chlildren_5_3)>0 ? $total_chlildren_5_3 : "-"; ?></td>
					<td class="data_td"><?php echo ($count_5_3['adult_male'][0]->count + $count_5_3['adult_female'][0]->count + $total_chlildren_5_3 + $count_5_3['adult_transgender'][0]->count); ?></td>

					

					<td class="data_td_late"><?php echo !empty($late_count_5_3->male)>0 ? $late_count_5_3->male : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_5_3->female)>0 ? $late_count_5_3->female : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_5_3->transgender)>0 ? $late_count_5_3->transgender : "-"; ?></td>
					<td class="data_td_late"><?php echo !empty($late_count_5_3->children)>0 ? $late_count_5_3->children : "-"; ?></td>
					<td class="data_td_late"><?php echo ($late_count_5_3->male + $late_count_5_3->female + $late_count_5_3->children + $late_count_5_3->transgender); ?></td>


						<td class="text-center data_td"><?php echo !empty($male->hbv_cirrhotic)>0 ? $male->hbv_cirrhotic : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty ($female->hbv_cirrhotic)>0 ? $female->hbv_cirrhotic : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender->hbv_cirrhotic)>0 ? $transgender->hbv_cirrhotic : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children->hbv_cirrhotic)>0 ? $children->hbv_cirrhotic : "-"; ?></td>
					<?php
						$sum_5_3=(!empty($male->hbv_cirrhotic) > 0 ? $male->hbv_cirrhotic : 0) +(!empty($female->hbv_cirrhotic) >0 ? $female->hbv_cirrhotic : 0) +(!empty($transgender->hbv_cirrhotic)>0 ? $transgender->hbv_cirrhotic : 0) +(!empty($children->hbv_cirrhotic) >0 ? $children->hbv_cirrhotic : 0);
					 ?>
					<td class="text-center data_td"><?php echo $sum_5_3; ?></td>
				</tr>
					

			</tbody>
		</table>
	</div>
</div>
</div>
<br>
<br>
<br>
<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name,filename) {
    if (!table.nodeType) table = document.getElementById(table)
            var newtable=document.createElement("table");
            var tbody =document.createElement("tbody");
             var row = document.createElement("tr");
             row.setAttribute("style", "background-color: black;color: white; font-family:'Source Sans Pro';");
  				var cell1 = document.createElement("td");
  				cell1.colSpan=7;
  				cell1.innerHTML ="Date as on : - <?php echo date('jS \ F Y'); ?>";
  				row.appendChild(cell1);
 			 	tbody.appendChild(row);
 			 	newtable.appendChild(tbody);
             var clonedTable=newtable.cloneNode(true);
             var clonedTable1=table.cloneNode(true);
            clonedTable.appendChild(clonedTable1);
    var ctx = {worksheet: name || 'Worksheet', table: clonedTable.innerHTML}
    //window.location.href = uri + base64(format(template, ctx))
     var link = document.createElement('a');
    link.download =filename;
    link.href = uri + base64(format(template, ctx));
    link.click();
     clonedTable.remove();
  }
})()
</script>
<script>

	
$(document).ready(function(){
$('[data-toggle="tooltip"]').tooltip();

$("#search_state").trigger('change');
$('#year').change(function(){
	var count=0;
	<?php  $count=0; ?>;
	var options='<option value="">Select</option>';
	var currentYear = (new Date).getFullYear();
	if (currentYear==$('#year').val()) {
			<?php
		$flag=0;
		$count=date("m");
	for ($i = 3; $i <=$count;$i++) {
	$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
	if($this->input->post('month')==$i){ 	?>
	options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php $flag=1;
	}	
	elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php }
	else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	}
	else{
		<?php
		$flag=0;
		$count=12;
		for ($i = 3; $i <=$count;$i++) {
		$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
		if($this->input->post('month')==$i){ 	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php $flag=1;
		}	
		elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php }
		else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	
	}
	//alert(options);
$('#month').html(options);
});
$("#year").trigger('change');

});
$('#search_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');
					//$('#input_district').val($('#input_district').val());
				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
					$('#mstfacilitylogin').html(data);
					
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

</script>