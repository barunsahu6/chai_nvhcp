<style type="text/css">
	.col-md-12{
		padding-right: 0px;!important;
		padding-left: 0px;!important;
	}
</style>

<button class="printbtn1 f-right btn btn-info" type="button" onclick="printDiv('printableArea')" value="Print" /><i class="fa fa-print" aria-hidden="true"></i> </button>

	<div class="container" id="printableArea">
		<div >
			<p class="text-center" style="font-size: 17px"><b>National Viral Hepatitis Control Program | Department of Health & Family Welfare</b> - 	
			</p>
		</div>
		<!-- .......................patient_register............................... -->
		<h5 style="margin-left: 15px; margin-bottom: -20px; margin-top: 90px;">Patient Registration</h5>
		<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
			<tr style="border: 1px solid black;">
				<td><p style="margin-left: 20px">OPD ID :- <span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->OPD_Id)):''; ?></span>
				</p></td>
				<td colspan="2"><p>NVHCP ID :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->UID_Prefix:$uid_prefix; ?></span></p>
				</td>
			</tr>
			<tr style="border: 1px solid black;">
				<td ><p style="margin-left: 20px">Patient type :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->PatientType == 1)?'New':'experienced'; ?></span></p>
				</td>
				<td colspan="2"><p>Previous UID :- 
					<span><?php if(count($patient_data) >0) { echo  str_pad($patient_data[0]->UID_Num, 6, '0', STR_PAD_LEFT); } else { echo  str_pad($patient_uid, 6, '0', STR_PAD_LEFT); } ?></span></p>
				</td>
			</tr>

			<tr>
				<td ><p style="margin-left: 20px">Name :- <span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FirstName)):''; ?></span></p>
				</td>
				<td><p>Age(months/years) :- <span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->Age)):''; ?></span></p>
				</td>
				<td><p>Gender :- <span><?php if(count($patient_data) > 0 )
				if($patient_data[0]->Gender == 1) { echo 'Male'; } 
				if($patient_data[0]->Gender == 2) { echo 'Female'; }
				if($patient_data[0]->Gender == 3) { echo 'Transgender'; }  ?></span></p>
			</td>
		</tr>
		<tr>			
			<td><p style="margin-left: 20px">Relative Type :- 
				<?php if(count($patient_data) > 0)
				{
					if($patient_data[0]->Relation == 1){ echo 'Father'; }
					if($patient_data[0]->Relation == 2){ echo 'Husband'; }
					if($patient_data[0]->Relation == 3){ echo 'Guardian'; }
					if($patient_data[0]->Relation == 4){ echo 'Mother'; }
				} ?></p>
			</td>
			<td colspan="2"><p>Relative's Name :- <span><span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FatherHusband)):''; ?></span></span></p>
			</td>
		</tr>

		<tr>
			<td colspan="3"><p style="margin-left: 20px">Home Street and Address :- <span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->Add1)):''; ?></span></p>
			</td>
		</tr><br>

		<tr>
			<td ><p style="margin-left: 20px">State:- <span>
				<?php if(!empty($states)) { echo $states[0]->StateName; } ?>
			</span></p>
		</td>
		<td><p>District :- <span><?php if(!empty($district)) { echo $district[0]->DistrictName; } ?></span></p>
		</td>
		<td><p>Block/Ward :- <span><?php if(!empty($block)) { echo $block[0]->BlockName; } ?></span></p>
		</td>
	</tr>
	<tr>
		<td><p style="margin-left: 20px">Village/Town/City :- <span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->VillageTown)):''; ?></span></p>
		</td>
		<td colspan="2"><p>Pin code :- <span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->PIN)):''; ?></span></p>
		</td>
	</tr>

	<tr>
		<td><p style="margin-left: 20px">Contact No :- <span><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->Mobile)):''; ?></span></p>
		</td>
		<td colspan="2"><p>Consent for Receiving Communication :- 
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->IsSMSConsent == 1){ echo 'Yes'; }
				if($patient_data[0]->IsSMSConsent == 2){ echo 'No'; }
			} ?></p></td>
		</tr><br>

		<tr>
			<td colspan="3"><p style="margin-left: 20px">Risk factor:- <?php error_reporting(0); 
			foreach ($risk_factor as $risk) { 
				$RISKFF= explode(',', $patient_data[0]->Risk);
				?>&nbsp;&nbsp;
				<?php if (in_array($risk->LookupCode,$RISKFF )){ ?>
					<input type="checkbox"  value="<?php echo $risk->LookupCode; ?>" <?php if (in_array($risk->LookupCode,$RISKFF )){ echo "checked"; }?>  id="risk_<?php echo $risk->LookupCode; ?>" >&nbsp;&nbsp; <?php echo $risk->LookupValue; ?>

					<?php } }?></p>
				</td>
			</tr>

		</table>

		<!-- ...........................patient_screening............................ -->




		<h5 style="margin-left: 15px; margin-bottom: 10px;">Screening Details</h5>
		<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
				<tr style="border: 1px solid black;">
				<td><p style="margin-left: 20px">Test types:</p></td>
				<td><p><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->LgmAntiHAV == 1)?"checked":''; ?>>&nbsp;&nbsp;IgM Anti HAV</p>
				</td>
				<td><p><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HbsAg == 1)?"checked":''; ?>>&nbsp;&nbsp;HBsAg</p>
				</td>
				
				<td><p><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->AntiHCV == 1)?"checked":''; ?>>&nbsp;&nbsp;Anti HCV</p>
				</td>
				<td><p><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->LgmAntiHEV == 1)?"checked":''; ?>>&nbsp;&nbsp;IgM Anti HEV</p>
				</td>
			</tr>
</table>

		<?php if (count($patient_data) > 0 && $patient_data[0]->LgmAntiHAV == 1){ ?>

			<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
			
		<h5 style="margin-left: 15px; margin-bottom: 10px;">Screening Details - lgM Anti HAV Testing</h5>

			<tr>
				<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?"checked":""; ?>>&nbsp;&nbsp;Rapid Diagnostic Testing:</p>
				</td>
				<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?timeStampShow($patient_data[0]->HAVRapidDate):''; ?></span></p>
				</td>
				<td colspan="4"><p>Result :- <span>
					<?php if(count($patient_data) > 0){
						if($patient_data[0]->HAVRapidResult == 1){echo 'Positive';}
						if($patient_data[0]->HAVRapidResult == 2){echo 'Negative';}
					} ?>
				</span></p>
			</td>
		</tr>

		<tr>
			<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?"checked":""; ?>>&nbsp;&nbsp;ELISA Test:</p>
			</td>
			<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?timeStampShow($patient_data[0]->HAVElisaDate):''; ?></span></p>
			</td>
			<td colspan="4"><p>Result :- <span>
				<?php if(count($patient_data) > 0){
					if($patient_data[0]->HAVElisaResult == 1){echo 'Positive';}
					if($patient_data[0]->HAVElisaResult == 2){echo 'Negative';}
				} ?>
			</span></p>
		</td>
	</tr>

	<tr>
		<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?"checked":""; ?>>&nbsp;&nbsp;Other test:</p>
		</td>


		<td ><input autocomplete="anyrandomthing" type="text" name="hav_other_test_name" id="hav_other_test_name" class="input_fields form-control hav_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?$patient_data[0]->HAVOtherName:''; ?>">
		</td>

		<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?timeStampShow($patient_data[0]->HAVOtherDate):''; ?></span></p>
		</td>
		<td colspan="4">Result :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->HAVOtherResult == 1){echo 'Positive';}
				if($patient_data[0]->HAVOtherResult == 2){echo 'Negative';}
			} ?>
		</span></p>
	</td>
</tr>


</table>

		<?php  } ?>

		<?php if (count($patient_data) > 0 && $patient_data[0]->HbsAg == 1){ ?>

			<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
			
		<h5 style="margin-left: 15px; margin-bottom: 10px;">Screening Details - HBsAg Testing</h5>

			<tr>
				<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?"checked":""; ?>>&nbsp;&nbsp;Rapid Diagnostic Testing:</p>
				</td>
				<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?timeStampShow($patient_data[0]->HBSRapidDate):''; ?></span></p>
				</td>
				<td colspan="4"><p>Result :- <span>
					<?php if(count($patient_data) > 0){
						if($patient_data[0]->HBSRapidResult == 1){echo 'Positive';}
						if($patient_data[0]->HBSRapidResult == 2){echo 'Negative';}
					} ?>
				</span></p>
			</td>
		</tr>

		<tr>
			<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?"checked":""; ?>>&nbsp;&nbsp;ELISA Test:</p>
			</td>
			<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?timeStampShow($patient_data[0]->HAVElisaDate):''; ?></span></p>
			</td>
			<td colspan="4"><p>Result :- <span>
				<?php if(count($patient_data) > 0){
					if($patient_data[0]->HAVElisaResult == 1){echo 'Positive';}
					if($patient_data[0]->HAVElisaResult == 2){echo 'Negative';}
				} ?>
			</span></p>
		</td>
	</tr>

	<tr>
		<td ><p style="margin-left: 20px"><input type="checkbox" name=""  <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?"checked":""; ?>>&nbsp;&nbsp;Other test:</p>
		</td>
		<td><input autocomplete="anyrandomthing" type="text" name="hbs_other_test_name" id="hbs_other_test_name" class="input_fields form-control hbs_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?$patient_data[0]->HBSOtherName:''; ?>"></td>

		<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?timeStampShow($patient_data[0]->HBSElisaDate):''; ?></span></p>
		</td>
		<td colspan="4">Result :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->HBSElisaResult == 1){echo 'Positive';}
				if($patient_data[0]->HBSElisaResult == 2){echo 'Negative';}
			} ?>
		</span></p>
	</td>
</tr>


</table>
		<?php } ?>

		<?php if (count($patient_data) > 0 && $patient_data[0]->AntiHCV == 1 ){ ?>

		<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
			
		<h5 style="margin-left: 15px; margin-bottom: 10px;">Screening Details - Anti HCV Testing</h5>

			<tr>
				<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?"checked":""; ?>>&nbsp;&nbsp;Rapid Diagnostic Testing:</p>
				</td>
				<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?timeStampShow($patient_data[0]->HCVRapidDate):''; ?></span></p>
				</td>
				<td colspan="4"><p>Result :- <span>
					<?php if(count($patient_data) > 0){
						if($patient_data[0]->HCVRapidResult == 1){echo 'Positive';}
						if($patient_data[0]->HCVRapidResult == 2){echo 'Negative';}
					} ?>
				</span></p>
			</td>
		</tr>

		<tr>

			<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?"checked":""; ?>>&nbsp;&nbsp;ELISA Test:</p>
			</td>
			<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?timeStampShow($patient_data[0]->HCVElisaDate):''; ?></span></p>
			</td>
			<td colspan="4"><p>Result :- <span>
				<?php if(count($patient_data) > 0){
					if($patient_data[0]->HCVElisaResult == 1){echo 'Positive';}
					if($patient_data[0]->HCVElisaResult == 2){echo 'Negative';}
				} ?>
			</span></p>
		</td>
	</tr>

	<tr>
		<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?"checked":""; ?>>&nbsp;&nbsp;Other test:</p>
		</td>
		<td><input  type="text" name="hbc_other_test_name" id="hbc_other_test_name" class="input_fields form-control hbc_other_fields"  value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?$patient_data[0]->HBCOtherName:''; ?>"></td>

		<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?timeStampShow($patient_data[0]->HCVOtherDate):''; ?></span></p>
		</td>
		<td colspan="4">Result :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->HCVOtherResult == 1){echo 'Positive';}
				if($patient_data[0]->HCVOtherResult == 2){echo 'Negative';}
			} ?>
		</span></p>
	</td>
</tr>


</table>
<?php } ?>
<!-- lgM Anti HEV -->

<?php if (count($patient_data) > 0 && $patient_data[0]->AntiHCV == 1 ){ ?>

		<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
			
		<h5 style="margin-left: 15px; margin-bottom: 10px;">Screening Details - lgM Anti HEV Testing</h5>

			<tr>
				<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?"checked":""; ?>>&nbsp;&nbsp;Rapid Diagnostic Testing:</p>
				</td>
				<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?timeStampShow($patient_data[0]->HEVRapidDate):''; ?></span></p>
				</td>
				<td colspan="4"><p>Result :- <span>
					<?php if(count($patient_data) > 0){
						if($patient_data[0]->HEVRapidResult == 1){echo 'Positive';}
						if($patient_data[0]->HEVRapidResult == 2){echo 'Negative';}
					} ?>
				</span></p>
			</td>
		</tr>

		<tr>

			<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?"checked":""; ?>>&nbsp;&nbsp;ELISA Test:</p>
			</td>
			<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?timeStampShow($patient_data[0]->HEVElisaDate):''; ?></span></p>
			</td>
			<td colspan="4"><p>Result :- <span>
				<?php if(count($patient_data) > 0){
					if($patient_data[0]->HEVElisaResult == 1){echo 'Positive';}
					if($patient_data[0]->HEVElisaResult == 2){echo 'Negative';}
				} ?>
			</span></p>
		</td>
	</tr>

	<tr>
		<td ><p style="margin-left: 20px"><input type="checkbox" name=""<?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?"checked":""; ?>>&nbsp;&nbsp;Other test:</p>
		</td>
		<td><input  type="text" name="hev_other_test_name" id="hev_other_test_name" class="input_fields form-control hev_other_fields"  value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?$patient_data[0]->HEVOtherName:''; ?>"></td>
		<td><input  type="text" name="hbc_other_test_name" id="hbc_other_test_name" class="input_fields form-control hbc_other_fields"  value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?$patient_data[0]->HBCOtherName:''; ?>"></td>

		<td><p>Date :- <span><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?timeStampShow($patient_data[0]->HCVOtherDate):''; ?></span></p>
		</td>
		<td colspan="4">Result :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->HCVOtherResult == 1){echo 'Positive';}
				if($patient_data[0]->HCVOtherResult == 2){echo 'Negative';}
			} ?>
		</span></p>
	</td>
</tr>


</table>
<?php } ?>
<!-- end lgM Anti HEV -->

<?php if(count($patient_data) > 0 && ($patient_data[0]->T_DLL_01_VLC_Result == 2 || $patient_data[0]->SVR12W_Result !='' ) ){ ?>
<!-- .....................patient_viral_load....................... -->
<h5 style="margin-left: 15px;">Hep C Viral Load Test Details</h5>
<table style="border: 1px solid black;width: 95%; margin-left: 15px;">	
	<tr>
		<td><p style="margin-left: 20px">Sample Drawn Date :- <span><?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->VLSampleCollectionDate):''; ?></span></p>
		</td>
		<td><p>Viral Load :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->T_DLL_01_VLCount:''; ?></span></p>
		</td>
		<td colspan="3"><p>Result :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->T_DLL_01_VLC_Result == 1){echo 'Detected';}
				if($patient_data[0]->T_DLL_01_VLC_Result == 2){echo 'Not Detected';}
			} ?>
		</span></p>
	</td>
</tr>

<tr>
	<td colspan="5"><p style="margin-left: 20px">Remarks :- <?php echo (count($patient_data) > 0)?$patient_data[0]->VLResultRemarks:''; ?></p></td>				
</tr>			
</table>
<?php } ?>
<!-- .........................patient_testing............................... -->
<?php if(count($patient_data) > 0 && ($patient_data[0]->Result == 2 && $fac_user[0]->is_Mtc!=1 && !in_array('2',$patient_tblRiskProfile_data) || $patient_data[0]->IsSVRSampleAccepted !='' ) ){ ?>
<h5 style="margin-left: 15px;margin-bottom: -10px; margin-top:250px;" >Testing information</h5>
<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
	<tr>
		<td colspan="7"><p style="margin-left: 20px">Date of prescribing tests :- <span><?php echo (count($patient_cirrohosis_data) > 0)?timeStampShow($patient_cirrohosis_data[0]->Prescribing_Dt):''; ?></span></p>
		</td>			
	</tr>

	<tr>
		<td ><p style="margin-left: 20px">Haemoglobin :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Haemoglobin:''; ?></span></p>
		</td>
		<td><p>S. Albumin :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Albumin:''; ?></span></p>
		</td>
		<td colspan="5"><p>S. Bilirubin (Total) :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Bilrubin:''; ?></span></p>
		</td>
	</tr>

	<tr>
		<td ><p style="margin-left: 20px">PT INR :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->V1_INR:''; ?></span></p>
		</td>
		<td><p>ALT :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->ALT:''; ?></span></p>
		</td>
		<td colspan="5"><p>AST :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->AST:''; ?></span></p>
		</td>
	</tr>
	<tr>
		<td ><p style="margin-left: 20px">AST ULN (Upper Limit of Normal) :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->AST_ULN:''; ?></span></p>
		</td>
		<td><p>Platelet count :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Platelets:''; ?></span></p>
		</td>
		<td colspan="5"><p>Weight (in kgs) :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->Weight:''; ?></span></p>
		</td>
	</tr>
	<tr>
		<td ><p style="margin-left: 20px">S. Creatinine (in mg/dL) :-<span><?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Creatinine:''; ?></span></p>
		</td>
		<td colspan="6"><p> eGFR (estimated glomerular filtration rate) :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->V1_EGFR:''; ?></span></p>
		</td>
	</tr>

	<tr>
		<td colspan="7"><p style="margin-left: 20px">Severity of Hep-C :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->CirrhosisStatus == 1){echo 'Compensated Cirrohosis';}
				if($patient_data[0]->CirrhosisStatus == 2){echo 'Decompensated Cirrohosis';}
			} ?>
		</span></p>
	</td>

</tr>

<tr>
	<td colspan="7"><p style="margin-left: 20px"><b>Criteria for evaluating cirrhosis:</b></p>
	</td>
</tr><br>			

<tr>
	<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_cirrohosis_data) > 0 && $patient_cirrohosis_data[0]->Clinical_US == 1)?'checked':''; ?>>&nbsp;&nbsp;Ultrasound</p></td>

	<td><p>Ultrasound Date :- <span><?php echo (count($patient_cirrohosis_data) > 0)?timeStampShow($patient_cirrohosis_data[0]->Clinical_US_Dt):''; ?></span></p>
	</td>

	<td><p><input type="checkbox" name="" <?php echo (count($patient_cirrohosis_data) > 0 && $patient_cirrohosis_data[0]->Fibroscan == 1)?'checked':''; ?>>&nbsp;&nbsp;Fibroscan</p>
	</td>
	<td><p>Fibroscan Date :- <span><?php echo (count($patient_cirrohosis_data) > 0)?timeStampShow($patient_cirrohosis_data[0]->Fibroscan_Dt):''; ?></span></p>
	</td>
	<td colspan="3"><p>LSM value :- <span><?php echo (count($patient_cirrohosis_data) > 0)?$patient_cirrohosis_data[0]->Fibroscan_LSM:''; ?></span></p>
	</td>
</tr>

<tr>
	<td ><p style="margin-left: 20px"><input type="checkbox" name="" <?php echo (count($patient_cirrohosis_data) > 0 && $patient_cirrohosis_data[0]->APRI == 1)?'checked':''; ?>>&nbsp;&nbsp;APRI:</p></td>
	<td><p>APRI score :- <span><?php echo (count($patient_cirrohosis_data) > 0)?$patient_cirrohosis_data[0]->APRI_Score:''; ?></span></p>
	</td>
	<td><p><input type="checkbox" name="" <?php echo (count($patient_cirrohosis_data) > 0 && $patient_cirrohosis_data[0]->FIB4 == 1)?'checked':''; ?>>&nbsp;&nbsp;FIB 4:</p></td>
	<td colspan="4"><p>FIB 4 score :- <span>
		<?php echo (count($patient_cirrohosis_data) > 0)?$patient_cirrohosis_data[0]->FIB4_FIB4:''; ?></span></p>
	</td>				
</tr>

<tr>
	<td ><p style="margin-left: 20px">Variceal bleed :- <span>
		<?php if(count($patient_data) > 0){
			if($patient_data[0]->Cirr_VaricealBleed == 1){echo 'Yes';}
			if($patient_data[0]->Cirr_VaricealBleed == 2){echo 'No';}
		} ?></span></p>
	</td>
	<td><p>Ascites :- <span>
		<?php if(count($patient_data) > 0){
			if($patient_data[0]->Cirr_Ascites == 1){echo 'None';}
			if($patient_data[0]->Cirr_Ascites == 2){echo 'Mild to Moderate';}
			if($patient_data[0]->Cirr_Ascites == 2){echo 'Severe';}
		} ?></span></p>
	</td>
	<td><p>Encephalopathy :- <span>
		<?php if(count($patient_data) > 0){
			if($patient_data[0]->Cirr_Encephalopathy == 1){echo 'None';}
			if($patient_data[0]->Cirr_Encephalopathy == 2){echo 'Mild to Moderate';}
			if($patient_data[0]->Cirr_Encephalopathy == 2){echo 'Severe';}
		} ?></span></p>
	</td>
	<td colspan="4"><p>Child Pugh Score :- <span><?php if(count($patient_data) > 0 && $patient_data[0]->ChildScore == 5 || $patient_data[0]->ChildScore == 6) { echo 'A'; } elseif(count($patient_data) > 0 && $patient_data[0]->ChildScore >=7 ||  $patient_data[0]->ChildScore <=9) { echo 'B'; } elseif(count($patient_data) > 0 && $patient_data[0]->ChildScore >=7 ||  $patient_data[0]->ChildScore <=9) { echo 'C'; } else { echo ''; }?></span></p>
	</td>
</tr>
</table>

<!-- ................known_history.............. -->
<h5 style="margin-left: 15px;margin-bottom: -10px;margin-top: 40px;">Known History</h5>
<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
	<tr>
		<td colspan="5"><p style="margin-left: 20px">Treatment experienced :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->PatientType == 1){echo 'Yes';}
				if($patient_data[0]->PatientType == 2){echo 'No';}						
			} ?></span></p>
		</td>
	</tr>

	<tr>
		<td ><p style="margin-left: 20px">Treating hospital/health facility :- <span><?php echo (count($FacilityCode) > 0)?$FacilityCode[0]->FacilityCode:''; ?></span></p>
		</td>

		<td><p>Previous regimen :- <span><?php echo (count($past_regimen) > 0)?$past_regimen[0]->regimen_name:''; ?></span></p>
		</td>

		<td colspan="3"><p>Previous duration :- <span><?php echo (count($previous_duration) > 0)?$previous_duration[0]->LookupValue:''; ?></span></p>
		</td>

	</tr>

	<tr>
		<td ><p style="margin-left: 20px">Previous Status :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->HCVPastTreatment == 2){echo 'Interrupted';}
				if($patient_data[0]->HCVPastTreatment == 3){echo 'Completed';}						
			} ?>
		</span></p>
	</td>
	<td><p>No. of weeks completed :- <span><?php echo (count($weeks) > 0)?$weeks[0]->LookupValue:''; ?></span></p>
	</td>
	<td colspan="3"><p>Last pill taken on :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->LastPillDate:''; ?></span></p>
	</td>
</tr>
<tr>
	<td colspan="5">
		<p style="margin-left: 20px">Past treatment outcome:&nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVPastOutcome == 1)?'checked':''; ?>> &nbsp;&nbsp;SVR Pending &nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVPastOutcome == 2)?'checked':''; ?>> &nbsp;&nbsp;SVR achieved &nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVPastOutcome == 3)?'checked':''; ?>> &nbsp;&nbsp;SVR not achieved</p>

		</td>
	</tr>
	<tr>
		<td colspan="5"><p style="margin-left: 20px">Co-morbidities:&nbsp;&nbsp;
			<input type="checkbox" name=""></p>
		</td>				
	</tr>

	<tr>
		<td ><p style="margin-left: 20px">HIV/ART regimen :- <span>
			<?php echo (count($hiv_regimen) > 0)?$hiv_regimen[0]->LookupValue:''; ?>
		</span></p>
	</td>
	<td colspan="4"><p style="margin-left: 20px">Renal/CKD stage :- <span>
		<?php echo (count($renal_ckd) > 0)?$renal_ckd[0]->LookupValue:''; ?>
	</span></p>
</td>

</tr>

<tr>
	<td colspan="5"><p style="margin-left: 20px">Referred :- <span>
		<?php if(count($patient_data) > 0){
			if($patient_data[0]->IsReferal == 1){echo 'Yes';}
			if($patient_data[0]->IsReferal == 2){echo 'No';}						
		} ?></span></p>
	</td>
</tr><br>			

<tr>
	<td ><p style="margin-left: 20px">Referring doctor :- <span>
		<?php echo (count($referring_doctor) > 0)?$referring_doctor[0]->name:''; ?>
	</span></p>
</td>
<td><p>Referred to :- <span>
	<?php echo (count($referred_to) > 0)?$referred_to[0]->facility_short_name:''; ?>
</span></p>
</td>				
<td colspan="3"><p>Date of referral :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->ReferalDate:''; ?></span></p>
</td>				
</tr>

<tr>
	<td colspan="5"><p style="margin-left: 20px">Observations :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->SCRemarks:''; ?></span></p>
	</td>				
</tr>	
</table>		

<!-- ...........................Patient Referral Slip....................................... -->

<h5 style="margin-left: 15px;">Patient Referral Slip</h5>
<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
	<tr>
		<td colspan="2"><p style="margin-left: 20px">(Patient Name) <b><?php echo (count($patient_data) > 0)?$patient_data[0]->FirstName :''; ?></b> has been referred by (doctor name) 
			<b><?php echo (count($referring_doctor) > 0)?$referring_doctor[0]->name:''; ?> </b>
			at (Name of the centre) 
			<b><?php echo (count($patient_data) > 0)?$patient_data[0]->PreviousTreatingHospital :'NULL'; ?></b> 
			on (date) <b><?php echo (count($patient_data) > 0)?$patient_data[0]->ReferalDate:''; ?></b> 
			to (Name of ‘referred to’ Hospital)<b><?php echo (count($patient_data) > 0)?$patient_data[0]->PreviousTreatingHospital :'NULL'; ?></b> 
			for management of Hepatitis-C. 
		The patient has (degree of cirrhosis)........................................ and (co-morbidity, if any)................................................. and is placed under (risk factor group).</p></td>				
	</tr>
	<tr style="height: 70px;">
		<td colspan="2"></td>
	</tr>	
	<tr>
		<td><p style="margin-left: 20px">Signature</p></td>
		<td><p>Name and designation</p></td>
	</tr>		
</table>

<!-- .........................Prescription information....................................... -->
<h5 style="margin-left: 15px; margin-top: 200px;" >Prescription information</h5>
<table style="border: 1px solid black;width: 95%; margin-left: 15px;">
	<tr>
		<td><p style="margin-left: 20px">Prescribing date :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->PrescribingDate:''; ?></span></p>
		</td>

		<td><p>Prescribing Doctor :- <span><?php echo (count($prescribing_doctor) > 0)?$prescribing_doctor[0]->name:''; ?></span></p>
		</td>

		<td><p>Place of dispensation:</p></td>
	</tr>

	<tr>
		<td ><p style="margin-left: 20px">Regimen Prescribed :- <span><?php echo (count($regimen_prescribed) > 0)?$regimen_prescribed[0]->LookupValue:''; ?></span></p>
		</td>
		<td colspan="2"><p> Duration (weeks) :- <span>
			<?php if(count($patient_data) > 0){
				if($patient_data[0]->T_DurationValue == 12){echo '12';}
				if($patient_data[0]->T_DurationValue == 24){echo '24';}						
				if($patient_data[0]->T_DurationValue == 99){echo 'Other';}						
			} ?>
		</span></p>
	</td>

</tr>

<tr>
	<td ><p style="margin-left: 20px">Sofosbuvir: (display strength) :- <span><?php if(isset($Sofosbuvir_mst_drug_strength)){ echo (count($Sofosbuvir_mst_drug_strength) > 0)?$Sofosbuvir_mst_drug_strength[0]->strength:''; }?></span></p>
	</td>

	<td><p>Daclatasvir: (display strength) :- <span><?php if(isset($Daclatasvir_mst_drug_strength)){ echo (count($Daclatasvir_mst_drug_strength) > 0)?$Daclatasvir_mst_drug_strength[0]->strength:''; }?></span></p>
	</td>

	<td><p> Velpatasvir: (display strength) :- <span><?php if(isset($Velpatasvir_mst_drug_strength)){ echo (count($Velpatasvir_mst_drug_strength) > 0)?$Velpatasvir_mst_drug_strength[0]->strength:''; }?></span></p>
	</td>
</tr>
<tr>
	<td colspan="3"><p style="margin-left: 20px">Ribavirin: (display strength) :- <span><span><?php if(isset($Ribavrin_mst_drug_strength)){ echo (count($Ribavrin_mst_drug_strength) > 0)?$Ribavrin_mst_drug_strength[0]->strength:''; }?></span></span></p>
	</td>

</tr>
<tr>
	<td ><p style="margin-left: 20px">rescription facility: ........................................................</p></td>
	<td colspan="2"><p>Prescribing doctor: .................................................</p></td>

</tr>
</table>

<!-- ...............................Dispensation information................................. -->
<h5 style="margin-left: 15px;">Dispensation information</h5>
<table style="border: 1px solid black;width: 95%; margin-left: 15px;">			

	<tr style="border: 1px solid black;">
		<td style="border: 1px solid black;text-align: center;"><p>Dispensation Date</p></td>
		<td style="border: 1px solid black;text-align: center;"><p>Advised Next Visit Date</p></td>
		<td style="border: 1px solid black;text-align: center;"><p>Pills Dispensed</p></td>
		<td style="border: 1px solid black;text-align: center;"><p>Pills Left</p></td>
		<td style="border: 1px solid black;text-align: center;"><p>Adherence (%)</p></td>
		<td style="border: 1px solid black;text-align: center;"><p>Reasons for non- adherence</p></td>
		<td style="border: 1px solid black;text-align: center;"><p>Side Effects</p></td>
		<td style="border: 1px solid black;text-align: center;"><p>Haemoglobin</p></td>
		<td style="border: 1px solid black;text-align: center;"><p>Platelet</p></td>
		<td style="border: 1px solid black;text-align: center;"><p> Comments</p></td>
	</tr>

	<?php if(count($dispensation_result) > 0){ 
		foreach($dispensation_result as $row){
			?>
			<tr style="border: 1px solid black;">
				<td style="border: 1px solid black;text-align: center;"><?=$row->Visit_Dt;?></td>
				<td style="border: 1px solid black;text-align: center;"><?=$row->NextVisit_Dt;?></td>
				<td style="border: 1px solid black;text-align: center;"><?=$row->PDispensation;?></td>
				<td style="border: 1px solid black;text-align: center;"><?=$row->PillsLeft;?></td>				
				<td style="border: 1px solid black;text-align: center;"><?=$row->Adherence; ?></td>
				<td style="border: 1px solid black;text-align: center;"><?=$row->NAdherenceReason; ?></td>
				<td style="border: 1px solid black;text-align: center;"><?=$row->SideEffectValue; ?></td>
				
				<td style="border: 1px solid black;text-align: center;"><?=$row->Haemoglobin; ?></td>
				<td style="border: 1px solid black;text-align: center;"><?=$row->PlateletCount; ?></td>
				<td style="border: 1px solid black;text-align: center;"><?=$row->Comments; ?></td>
				
			</tr>
		<?php } }?>		

	</table>
<?php } ?>
<?php if(count($patient_data) > 0 && ($patient_data[0]->SVR12W_Result == 1 || $patient_data[0]->SVR12W_Result == 2)){ ?>
	<!-- ......................SVR information.................................. -->

	<h5 style="margin-left: 15px;">SVR information</h5>
	<table style="border: 1px solid black;width: 95%; margin-left: 15px;">			

		<tr>
			<td><p style="margin-left: 20px;">Sample Drawn Date :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->SVRDrawnDate:''; ?></span></p>
			</td>

			<td><p>Viral Load :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->SVR12W_HCVViralLoadCount:''; ?></span></p>
			</td>
			<td><p>Result :- <span><?php echo (count($svr_result) > 0)?$svr_result[0]->LookupValue:''; ?></span></p>
			</td>

		</tr>

		<tr>
			<td colspan="3"><p style="margin-left: 20px;">Remarks :- <span><?php echo (count($patient_data) > 0)?$patient_data[0]->SVRTransportRemark:''; ?></span></p>
			</td>
		</tr>
	</table>
<?php  } ?>

<?php if(count($patient_data) > 0 && $patient_data[0]->InterruptReason == 1 ){ ?>  
	<!-- ...................Interrupted Patient information........................ -->
	<h5 style="margin-left: 15px; margin-top: 60px;">Interrupted Patient information</h5>
	<table style="border: 1px solid black;width: 95%; margin-left: 15px;">			

		<tr>
			<td><p style="margin-left: 20px;">Reason for interruption :- <span>
				<?php if(isset($InterruptReason)){
					if(count($InterruptReason)>0){
						echo $InterruptReason[0]->LookupValue;
					}
				}?></span></p></td>
				<td><p>Reason for (Death/LFU) :- <span>
				<?php if(isset($DeathReason)){
					if(count($DeathReason)>0){
						echo $DeathReason[0]->LookupValue;
					}
				}?></span></p></td>
				<td><p> Patient referred for:&nbsp;&nbsp;
					<input type="checkbox" name="">&nbsp;&nbsp;ETR
					<input type="checkbox" name="">&nbsp;&nbsp;SVR</p>
				</td>

			</tr>

			<tr>
				<td colspan="3"><p style="margin-left: 20px;"><input type="checkbox" name="">&nbsp;&nbsp;None:</p></td>
			</tr>
		</table>
<?php } ?>
		<div style="line-height: 250px;">
			<table style="width: 95%; margin-left: 25px;">
				<tr>
					<td><p>Signature</p></td>
					<td><p>Name and designation</p></td>
				</tr>
			</table>			
		</div>

	</div>