<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #A30A0C !important;
	background-color: white !important;
	border : 1px solid #A30A0C;
}
</style>

<br>

<div class="row equal">

	<!-- <form action="" name="patient_form" id="patient_form" method="POST"> -->
					<?php
           $attributes = array(
              'id' => 'patient_form',
              'name' => 'patient_form',
               'autocomplete' => 'false',
            );
           echo form_open('', $attributes); ?>

		<input type="hidden" name="fetch_uid" id="fetch_uid">
		          <?php echo form_close(); ?>
		<!-- <input type="hidden" name="fetch_uid" id="fetch_uid"> -->
	<!-- </form> -->
	<div class="col-lg-10 col-lg-offset-1">

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="btn-group">
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">1. Registration</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">2. Screening</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">3. Viral Load</a>
					<a class="btn btn-success" href="<?php echo base_url(); ?>patientinfo/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">4. Testing</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/known_history/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">5. Known History</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">6. Prescription</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">7. Dispensation</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">8. SVR</a>
				</div>
			</div>
		</div>

		<!-- <form action="" method="POST" name="registration" id="registration"> -->
									<?php
           $attributes = array(
              'id' => 'registration',
              'name' => 'registration',
               'autocomplete' => 'false',
            );
           echo form_open('', $attributes); ?>
           <div class="row">
				<div class="col-md-12">
					<h4 class="text-center"><p>Patient Name - <strong><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FirstName)):''; ?></strong>  (<?php echo (count($patient_data) > 0)?$patient_data[0]->UID_Prefix:$uid_prefix; echo '-' .str_pad($patient_data[0]->UID_Num, 6, '0', STR_PAD_LEFT); ?>)<p></h4>
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Patient Testing FollowUp Visits <?php echo set_hepc(); ?></h3>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-3">
					<h4 class="text-center">Follow Up Visits</h4>
					<table class="table table-hover table-bordered table-striped">
						<thead>
							<th>S.no</th>
							<th>Visit Date</th>
						</thead>
						<tbody>
							<?php foreach ($patient_tblpatientaddltest_data as $row) { ?>
								<tr>
									<td><?php echo $row->Visit_No; ?></td>
									<td><?php echo $row->Visit_Date; ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<div class="col-md-9" style="border-left: 1px solid #CCC;">
					<br>
					<div class="row">
						<div class="col-md-4">
							<h4><b>BASELINE TEST DETAILS</b></h4>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<label for="">Date <span class="text-danger">*</span></label>
							<input type="date" name="date_of_follow_up" id="date_of_follow_up" class="input_fields form-control" value="<?php //echo (count($patient_data) > 0)?$patient_data[0]->V1_Haemoglobin:''; ?>" required>
							<br class="hidden-lg-*">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<label for="">Haemoglobin (g/dL)</label>
							<input type="text" name="haemoglobin" id="haemoglobin" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Haemoglobin:''; ?>">
							<br class="hidden-lg-*">
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<label for="">S. Albumin (g/dL)</label>
							<input type="text" name="albumin" id="albumin" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Albumin:''; ?>">
							<br class="hidden-lg-*">
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<label for="">S.Bilirubin (Total) (mg/dL) </label>
							<input type="text" name="bilirubin" id="bilirubin" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Bilrubin:''; ?>">
							<br class="hidden-lg-*">
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<label for="">PT INR</label>
							<input type="text" name="inr" id="inr" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->V1_INR:''; ?>">
							<br class="hidden-lg-*">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<label for="">ALT (IU/L)</label>
							<input type="text" name="alt" id="alt" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->ALT:''; ?>">
							<br class="hidden-lg-*">
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<label for="">AST (IU/L)</label>
							<input type="text" name="ast" id="ast" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->AST:''; ?>">
							<br class="hidden-lg-*">
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<label for="">AST ULN (Upper Limit of Normal) (IU/L)</label>
							<input type="text" name="ast_uln" id="ast_uln" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->AST_ULN:''; ?>">
							<br class="hidden-lg-*">
						</div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<label for="">Platelet Count (per microlitre)</label>
							<input type="text" name="platelet_count" id="platelet_count" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Platelets:''; ?>">
							<br class="hidden-lg-*">
						</div>
					</div>
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<label for="">S. Creatinine (in mg/dL)</label>
							<input type="text" name="creatinine" id="creatinine" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->V1_Creatinine:''; ?>">
							<br class="hidden-lg-*">
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-md-offset-3">
							<button class="btn btn-block btn-success form_buttons" id="save" name="save" value="save">SAVE</button>
						</div>
					</div>
				</div>
			</div>
			
			<br><br><br>
		          <?php echo form_close(); ?>			
		<!-- </form> -->
	</div>
</div>


<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>

<script>
	
	$(document).ready(function(){

	})
</script>