<style>
thead
{
	background-color: #085786;
	color: white;
}

.table-bordered>tbody>tr>td.data_td
{
	font-weight: 600;
	font-size: 15px;
	text-align: center;
	vertical-align: middle;
	background-color: #e4e4e4;
	border-color:#ffffff;
	border-width:1px;
	color:#000000;
}
.table-bordered>tbody>tr>td.data_td_late
{
	font-weight: 600;
	font-size: 16px;
	background-color: #ffffff;
}
</style>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 5 !important;
	width: 100% !important;
	font-size: 14px !important;
	font-family: 'Source Sans Pro';
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
.col-md-2{
		width: 16.33%;
		padding-left: 8px;
		padding-right: 8px;
	}
	.btn-width{
	width: 12%;
	margin-top: 20px;
}
.pd-15{
	padding-left: 15px;
}
.pb-20{
	padding-bottom: 20px;
}
.container{
	width: 76%;
}
}

@media (max-width: 768px) {

.input_fields
	{
		height: 40px !important;
		border-radius: 5 !important;
		width: 100% !important;
		font-size: 14px !important;
		font-family: 'Source Sans Pro';
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 13px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
	border-radius: 0;
}
.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
.table-bordered>thead>tr>th{
	vertical-align: middle;
	font-size: 13px;
	font-family: 'Source Sans Pro';
	background-color: #085786;
}
.table-bordered>tbody>tr>td{
	vertical-align: middle;
	font-size: 13px;
	color: #000000;
	font-family: 'Source Sans Pro';
}
</style>
</style>
<br>

<?php $loginData = $this->session->userdata('loginData');
//echo "<pre>"; print_r($loginData);

if($loginData->user_type==1) { 
$select = '';
}else{
$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
$select4 = '';	
}

$filters1 = $this->session->userdata('filters1'); 
//pr($filters1);
 ?>
<div class="container">
<div class="row">
	<div class="col-md-12" style="padding: 0px;">
		<div class="panel panel-default" id="Record_receipt_panel">
				<div class="panel-heading" style="background-color: #333333;padding: 3px 0px;">
					<h4 class="text-center" style="background-color: #333333;color: white;font-family: 'Source Sans Pro';letter-spacing: .75px;">Late Entry Monthly Report (HCV)
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/excel_black.png" alt=""></a> -->
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/pdf_color.png" alt=""></a> -->
		</h4>
		<!-- <input type="button" onclick="printDiv('printableArea')" value="Print" /> -->
		

	</div>
<table class="table table-bordered table-highlighted" >
			<thead>
				<th style="background-color: #ffcccb;color: black; font-family:'Source Sans Pro';font-size: 14px;text-align: center;font-weight: 500" colspan="12">Comparison between the "Regular Entry" (records that freeze for the previous month on the 5th of the next month) and the "Late Entry" (which keeps on updating as more entries are uploaded) for selected dates</th>
			</thead>
		</table>

<?php
           $attributes = array(
              'id' => 'filter_form',
              'name' => 'filter_form',
               'autocomplete' => 'false',
            );
           echo form_open('', $attributes); ?>
<div class="row" style="padding: 5px 0px 5px 15px;">

	<div class="col-md-2 col-sm-12">
		<label for="">State</label>
		<select type="text" name="search_state" id="search_state" class="form-control input_fields" required <?php echo $select2.$select3.$select4; ?>>
			<option value="0">All States</option>
			<?php 
			foreach ($states as $state) {
				?>
				<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo $state->StateName; ?></option>
				<?php 
			}
			?>
		</select>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">District</label>
		<select type="text" name="input_district" id="input_district" class="form-control input_fields"   <?php echo $select.$select2.$select4; ?>>
			

			<?php foreach ($districts as  $value) { ?>
				<option value="<?php echo $value->id_mstdistrict; ?>" <?php if($this->input->post('input_district')==$value->id_mstdistrict) { echo 'selected';}  ?>><?php echo $value->DistrictName; ?></option>
			<?php } ?>
			<?php 
			
			?>
		</select>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">Facility</label>
		<select type="text" name="facility" id="mstfacilitylogin" class="form-control input_fields"  <?php echo $select.$select2; ?>>
			<option value="">Select Facility</option>
			<?php 
			
			?>
		</select>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">From Date</label>
		<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')) { echo $this->input->post('startdate'); }else{ echo timeStampShow(date('Y-m-01')); } ?>" required>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">To date</label>
		<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')) { echo $this->input->post('enddate'); }else{  echo timeStampShow(date('Y-m-d'));} ?>" required>
	</div>

<div class="col-lg-2 col-md-2 col-sm-12 btn-width" style="margin-top: 20px;">
					<button type="submit" class="btn btn-block btn-success" id="monthreport" style="line-height: 1.2; font-weight: 600;">SEARCH</button>
				</div><span style="margin-top: 25px;margin-right: 25px; float: right;"><a href="javascript:void(0)"><i class="fa fa-2x fa-download" id="excel_button" onclick="tableToExcel('testTable', 'W3C Example','Late_Monthly_Report.xls')" title="Excel Download"></i></a></span>
			</div>


 <?php echo form_close(); ?>
<br>
</div>
</div>
<div class="row" id="printableArea">

	<div class="col-md-12">
		<table class="table table-bordered table-highlighted" id="testTable" >
			<thead>
				<th style="background-color: #ffcccb;color: black; font-family:'Source Sans Pro';font-size: 14px;text-align: center;font-weight: 500" colspan="12">This data is from <?php echo date("F-Y", strtotime( $filters1['startdate1'])); ?> to  <?php echo date("F-Y", strtotime($filters1['enddate'])); ?></th>

				
			</thead>
			

			<thead>
					<th style="background-color: white;color: black;font-family:'Source Sans Pro';text-align: center;border:none;" colspan="2">&nbsp;</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;" colspan="5">Regular Monthly Entry</th>
				

				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;" colspan="5">Late Entry</th>
				
			</thead>
			<thead>
				<th style="background-color: #bbbbbb;color: black;">1</th>
				<th style="background-color: #bbbbbb;color: black;font-family:'Source Sans Pro';">Number of Hepatitis C infected people seeking care at the treatment center (Registering in Care)</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" nowrap="nowrap">Adult Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Male</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Female</th>
				<th style="background-color: #01446b;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</thead>
			<tbody>
				<tr style="font-family:'Source Sans Pro';">
					<td>1.1</td>
					<td>Cumulative number of persons registered in Hepatitis C care at the beginning of this month</td>
						<td class="text-center data_td"><?php echo $count_1_1['male'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_1_1['female'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_1_1['transgender'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_1_1['children'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo ($count_1_1['male'][0]->count + $count_1_1['female'][0]->count + $count_1_1['children'][0]->count + $count_1_1['transgender'][0]->count); ?></td>
					
					<td class="text-center data_td_late"><?php echo !empty($late_count_1_1->male)>0 ? $late_count_1_1->male : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_1_1->female)>0 ? $late_count_1_1->female : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_1_1->transgender)>0 ? $late_count_1_1->transgender  : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_1_1->children)>0 ? $late_count_1_1->children : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo ($late_count_1_1->male + $late_count_1_1->female + $late_count_1_1->children +$late_count_1_1->transgender); ?></td>
				</tr>
					<td>1.2</td>
				<td>Number of new persons registered in during this month</td>
						<td class="text-center data_td"><?php echo $count_1_2['male'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_1_2['female'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_1_2['transgender'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_1_2['children'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo ($count_1_2['male'][0]->count + $count_1_2['female'][0]->count + $count_1_2['children'][0]->count + $count_1_2['transgender'][0]->count); ?></td>
					
					<td class="text-center data_td_late"><?php echo !empty($late_count_1_2->male)>0 ? $late_count_1_2->male : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_1_2->female)>0 ? $late_count_1_2->female : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_1_2->transgender)>0 ? $late_count_1_2->transgender  : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_1_2->children)>0 ? $late_count_1_2->children : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo ($late_count_1_2->male + $late_count_1_2->female + $late_count_1_2->children +$late_count_1_2->transgender); ?></td>
				</tr>

					<?php

					$male_1_3=$count_1_1['male'][0]->count+$count_1_2['male'][0]->count;
					$female_1_3=$count_1_1['female'][0]->count+$count_1_2['female'][0]->count;
					$transgender_1_3=$count_1_1['transgender'][0]->count+$count_1_2['transgender'][0]->count;
					$children_1_3=$count_1_1['children'][0]->count+$count_1_2['children'][0]->count;

						$late_male_1_3=$late_count_1_1->male + $late_count_1_2->male;
						$late_female_1_3=$late_count_1_1->female+$late_count_1_2->female;
						$late_transgender_1_3=$late_count_1_1->transgender+$late_count_1_2->transgender;
						$late_children_1_3=$late_count_1_1->children+$late_count_1_2->children;


				 ?>

				<td>1.3</td>
					<td>Cumulative number of persons registered at the end of this month = 1.1 + 1.2</td>
						<td class="text-center data_td"><?php echo !empty($male_1_3)>0 ? ($male_1_3) : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($female_1_3)>0 ? ($female_1_3) : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($transgender_1_3)>0 ? ($transgender_1_3)  : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children_1_3)>0 ? ($children_1_3) : "-"; ?></td>
					<td class="text-center data_td"><?php echo ($male_1_3+$female_1_3+$transgender_1_3+$children_1_3); ?></td>
					
					<td class="text-center data_td_late"><?php echo !empty($late_male_1_3)>0 ? ($late_male_1_3) : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_female_1_3)>0 ? ($late_female_1_3) : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_transgender_1_3)>0 ? ($late_transgender_1_3)  : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_children_1_3)>0 ? ($late_children_1_3) : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo ($late_male_1_3+$late_female_1_3+$late_transgender_1_3+$late_children_1_3); ?></td>
				</tr>
			</tbody>
			<thead>
				<th style="background-color: #bbbbbb;color: black;">2</th>
				<th style="background-color: #bbbbbb;color: black;font-family:'Source Sans Pro';">Initiation of Treatment</th>
			<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" nowrap="nowrap">Adult Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Male</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Female</th>
				<th style="background-color: #01446b;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</thead>
			<tbody>

				<tr style="font-family:'Source Sans Pro';">
					<td>2.1</td>
					<td>Cumulative number of patients ever started on Treatment (Number at the beginning of this month)</td>
				<td class="text-center data_td"><?php echo $count_2_1['male'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_2_1['female'][0]->count; ?></td>
						<td class="text-center data_td"><?php echo $count_2_1['transgender'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_2_1['children'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo ($count_2_1['male'][0]->count + $count_2_1['female'][0]->count + $count_2_1['children'][0]->count + $count_2_1['transgender'][0]->count); ?></td>


				<td class="text-center data_td_late"><?php echo !empty($late_count_2_1->male)>0 ? $late_count_2_1->male : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_2_1->female)>0 ? $late_count_2_1->female : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_2_1->transgender)>0 ? $late_count_2_1->transgender  : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_2_1->children)>0 ? $late_count_2_1->children : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo ($late_count_2_1->male + $late_count_2_1->female + $late_count_2_1->children +$late_count_2_1->transgender); ?></td>
				</tr>

				<tr style="font-family:'Source Sans Pro';">
					<td >2.2</td>
					<td>Number of new patients started on treatment during this month</td>
				<td class="text-center data_td"><?php echo $count_2_2['male'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_2_2['female'][0]->count; ?></td>
						<td class="text-center data_td"><?php echo $count_2_2['transgender'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_2_2['children'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo ($count_2_2['male'][0]->count + $count_2_2['female'][0]->count + $count_2_2['children'][0]->count + $count_2_2['transgender'][0]->count); ?></td>


					<td class="text-center data_td_late"><?php echo !empty($Ini_male->initiatied_on_treatment)>0 ? $Ini_male->initiatied_on_treatment :"-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($Ini_female->initiatied_on_treatment)>0 ? $Ini_female->initiatied_on_treatment :"-"; ?></td>
						<td class="text-center data_td_late"><?php echo  !empty($Ini_transgender->initiatied_on_treatment)>0 ?$Ini_transgender->initiatied_on_treatment : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($Ini_children->initiatied_on_treatment)>0 ? $Ini_children->initiatied_on_treatment : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo ($Ini_male->initiatied_on_treatment + $Ini_female->initiatied_on_treatment + $Ini_transgender->initiatied_on_treatment + $Ini_children->initiatied_on_treatment); ?></td>
				</tr>
				
				<tr style="font-family:'Source Sans Pro';">
					<td>2.3</td>
					<td>Number of patients on treatment "transferred in" during this month</td>
						<!-- <td class="text-center data_td"><?php echo !empty($count_2_3['male'][0]->count)>0 ? $count_2_3['male'][0]->count: "-"; ?></td>
											<td class="text-center data_td"><?php echo !empty($count_2_3['female'][0]->count)>0 ? $count_2_3['female'][0]->count : "-"; ?></td>
											<td class="text-center data_td"><?php echo !empty($count_2_3['transgender'][0]->count)>0 ? $count_2_3['transgender'][0]->count : "-"; ?></td>
											<td class="text-center data_td"><?php echo !empty($count_2_3['children'][0]->count)>0 ? $count_2_3['children'][0]->count : "-"; ?></td>
											<td class="text-center data_td"><?php echo ($count_2_3['male'][0]->count + $count_2_3['female'][0]->count + $count_2_3['children'][0]->count + $count_2_3['transgender'][0]->count); ?></td>

											<td class="text-center data_td_late"><?php echo !empty($late_count_2_3['male'][0]->count)>0 ? $late_count_2_3['male'][0]->count: "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_2_3['female'][0]->count)>0 ? $late_count_2_3['female'][0]->count : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_2_3['transgender'][0]->count)>0 ? $late_count_2_3['transgender'][0]->count : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_2_3['children'][0]->count)>0 ? $late_count_2_3['children'][0]->count : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo ($late_count_2_3['male'][0]->count + $late_count_2_3['female'][0]->count + $late_count_2_3['children'][0]->count + $count_2_3['transgender'][0]->count); ?></td> -->

				<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "0"; ?></td>


				
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "0"; ?></td>

				</tr>

					<?php

					$male_2_4=$count_2_1['male'][0]->count+$count_2_2['male'][0]->count/*+$count_2_3['male'][0]->count*/;
					$female_2_4=$count_2_1['female'][0]->count+$count_2_2['female'][0]->count/*+$count_2_3['female'][0]->count */; 
					$transgender_2_4=$count_2_1['transgender'][0]->count+$count_2_2['transgender'][0]->count/*+$count_2_3['transgender'][0]->count*/;
					$children_2_4=$count_2_1['children'][0]->count+$count_2_2['children'][0]->count/*+$count_2_3['children'][0]->count*/;

				$late_male_2_4=$late_count_2_1->male+$Ini_male->initiatied_on_treatment/*+$late_count_2_3['male'][0]->count*/;
				$late_female_2_4=$late_count_2_1->female+$Ini_female->initiatied_on_treatment/*+$late_count_2_3['female'][0]->count*/;
				$late_transgender_2_4=$late_count_2_1->transgender+$Ini_transgender->initiatied_on_treatment/*+$late_count_2_3['transgender'][0]->count*/;
				$late_children_2_4= $late_count_2_1->children+$Ini_children->initiatied_on_treatment/*+$late_count_2_3['children'][0]->count*/; ?>

				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>2.4</td>
					<td>Cumulative number of patients ever received Treatment (Number at the end of this month) = 2.1+ 2.2 +2.3 </td>
					<td class="text-center data_td"><?php echo !empty($male_2_4)>0 ? $male_2_4 : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($female_2_4)>0 ? $female_2_4 : "-"; ?></td>
						<td class="text-center data_td"><?php echo !empty($transgender_2_4)>0 ? $transgender_2_4 : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($children_2_4)>0 ? $children_2_4 : "-"; ?></td>
					
					<td class="text-center data_td"><?php echo ($male_2_4+$female_2_4+$transgender_2_4+$children_2_4); ?></td>

				<td class="text-center data_td_late"><?php echo !empty($late_male_2_4)>0 ? $late_male_2_4 : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_female_2_4)>0 ? $late_female_2_4 : "-"; ?></td>
						<td class="text-center data_td_late"><?php echo !empty($late_transgender_2_4)>0 ? $late_transgender_2_4 : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_children_2_4)>0 ? $late_children_2_4 : "-"; ?></td>
					
					<td class="text-center data_td_late"><?php echo ($late_male_2_4+$late_female_2_4+$late_transgender_2_4+$late_children_2_4); ?></td>
				</tr>
			</tbody>
			<thead>
				<th style="background-color: #bbbbbb;color: black;font-family:'Source Sans Pro';">3</th>
				<th style="background-color: #bbbbbb;color: black;font-family:'Source Sans Pro';">Treatment status (at the end of the month) <!-- out of all patients ever started on treatment (2.4) --></th>
			<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" nowrap="nowrap">Adult Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Male</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Female</th>
				<th style="background-color: #01446b;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</thead>
			<tbody>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>3.1</td>
					<td>Cumulative number of patients who have completed treatment<div class="pull-right text-right glyphicon glyphicon-info-sign" style="font-size: 14px; padding: 0px;"  data-toggle="tooltip" title="" data-original-title="Number of patients that have completed their treatment regimen (Pre-SVR)">

</div></td>
						<td class="text-center data_td"><?php echo $count_3_1['male'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_3_1['female'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_3_1['transgender'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_3_1['children'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo ($count_3_1['male'][0]->count + $count_3_1['female'][0]->count + $count_3_1['children'][0]->count + $count_3_1['transgender'][0]->count); ?></td>

						<td class="text-center data_td_late"><?php echo !empty($late_count_3_1->male)>0 ? $late_count_3_1->male : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_1->female)>0 ? $late_count_3_1->female : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_1->transgender)>0 ? $late_count_3_1->transgender : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_1->children)>0 ? $late_count_3_1->children : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo ($late_count_3_1->male + $late_count_3_1->female+ $late_count_3_1->children + $late_count_3_1->transgender); ?></td>
				</tr>

				<?php 

				$male_sum=/*$count_3_3['male'][0]->count+*/$count_3_5['male'][0]->count+$count_3_8['male'][0]->count;
				 $female_sum=/*$count_3_3['female'][0]->count+*/$count_3_5['female'][0]->count+$count_3_8['female'][0]->count;
				$transgender_sum=/*$count_3_3['transgender'][0]->count+*/$count_3_5['transgender'][0]->count+$count_3_8['transgender'][0]->count;
				$children_sum=/*$count_3_3['children'][0]->count+*/$count_3_5['children'][0]->count+$count_3_8['children'][0]->count;

				$male_3_2=$male_2_4-($count_3_1['male'][0]->count+$male_sum);
					$female_3_2=$female_2_4-($count_3_1['female'][0]->count+$female_sum);
					$transgender_3_2=$transgender_2_4-($count_3_1['transgender'][0]->count+$transgender_sum);
					$children_3_2=$children_2_4-($count_3_1['children'][0]->count+$children_sum);

					 $late_male_sum=/*$late_count_3_3->male+*/$late_count_3_5->male+$late_count_3_8->male;
					$late_female_sum=/*$late_count_3_3->female+*/$late_count_3_5->female+$late_count_3_8->female;
					$late_transgender_sum=/*$late_count_3_3->transgender+*/$late_count_3_5->transgender+$late_count_3_8->transgender;
					$late_children_sum=/*$late_count_3_3->children+*/$late_count_3_5->children+$late_count_3_8->children;


					$late_male_3_2=$late_male_2_4-($late_count_3_1->male+$late_male_sum);
					$late_female_3_2=$late_female_2_4-($late_count_3_1->female+$late_female_sum);
					$late_transgender_3_2=$late_transgender_2_4-($late_count_3_1->transgender+$late_transgender_sum);
					$late_children_3_2=$late_children_2_4-($late_count_3_1->children+$late_children_sum);

					 ?>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>3.2</td>
						<td>Cumulative number of patients who are currently taking treatment
							(3.2 = 2.4-(3.1+(3.3+3.5+3.8)))</td>
					<td class="data_td"><?php echo $male_3_2 > 0 ? $male_3_2 : "0"; ?></td>
					<td class="data_td"><?php echo $female_3_2 > 0 ? $female_3_2 : "0"; ?></td>
					<td class="data_td"><?php echo $transgender_3_2 > 0 ? $transgender_3_2 : "0"; ?></td>
					<td class="data_td"><?php echo $children_3_2 > 0 ?  $children_3_2 : "0" ; ?></td>
					<td class="data_td"><?php echo ($male_3_2+$female_3_2+$transgender_3_2+$children_3_2)>0 ? ($male_3_2+$female_3_2+$transgender_3_2+$children_3_2) : "0" ; ?></td>

					<td class="text-center data_td_late"><?php echo $late_male_3_2 > 0 ? $late_male_3_2 : "0"; ?></td>
					<td class="text-center data_td_late"><?php echo $late_female_3_2 > 0 ? $late_female_3_2 : "0"; ?></td>
					<td class="text-center data_td_late"><?php echo $late_transgender_3_2 > 0 ? $late_transgender_3_2 : "0"; ?></td>
					<td class="text-center data_td_late"><?php echo $late_children_3_2 > 0 ? $late_children_3_2 : "0"; ?></td>
					<td class="text-center data_td_late"><?php echo ($late_male_3_2+$late_female_3_2+$late_transgender_3_2+$late_children_3_2) > 0 ? ($late_male_3_2+$late_female_3_2+$late_transgender_3_2+$late_children_3_2) : "0"; ?></td>
				</tr>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>3.3</td>
					<td>Number of patients who "transferred out"</td>
					 <td class="text-center data_td"><?php echo !empty($count_3_3['male'][0]->count)>0 ? $count_3_3['male'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_3['female'][0]->count)>0 ? $count_3_3['female'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_3['transgender'][0]->count)>0 ? $count_3_3['transgender'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_3['children'][0]->count)>0 ? $count_3_3['children'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo ($count_3_3['male'][0]->count + $count_3_3['female'][0]->count + $count_3_3['children'][0]->count + $count_3_3['transgender'][0]->count); ?></td>
					
									<td class="text-center data_td_late"><?php echo !empty($late_count_3_3->male)>0 ? $late_count_3_3->male : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_3->female)>0 ? $late_count_3_3->female : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_3->transgender)>0 ? $late_count_3_3->transgender : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_3->children)>0 ? $late_count_3_3->children : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo ($late_count_3_3->male + $late_count_3_3->female+ $late_count_3_3->children + $late_count_3_3->transgender); ?></td> 


			<!-- 	<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "0"; ?></td>


				
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "0"; ?></td> -->
				</tr>
				 <tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>3.4</td>
					<td>The number of all patients whose treatment status in this month is “stopped treatment” due to side effects/medical reasons</td>
				 <td class="text-center data_td"><?php echo !empty($count_3_4['male'][0]->count)>0 ? $count_3_4['male'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_4['female'][0]->count)>0 ? $count_3_4['female'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_4['transgender'][0]->count)>0 ? $count_3_4['transgender'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_4['children'][0]->count)>0 ? $count_3_4['children'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo ($count_3_4['male'][0]->count + $count_3_4['female'][0]->count + $count_3_4['children'][0]->count + $count_3_4['transgender'][0]->count); ?></td>
				
						<td class="text-center data_td_late"><?php echo !empty($late_count_3_4->male)>0 ? $late_count_3_4->male : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_4->female)>0 ? $late_count_3_4->female : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_4->transgender)>0 ? $late_count_3_4->transgender : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_4->children)>0 ? $late_count_3_4->children : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo ($late_count_3_4->male + $late_count_3_4->female+ $late_count_3_4->children + $late_count_3_4->transgender); ?></td> 

				<!-- <td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "0"; ?></td>


				
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "0"; ?></td> -->
				</tr>
				</tr> 
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>3.5</td>
					<td>Cumulative Number of patients who are lost to follow-up (LTFU)</td>
					<td class="text-center data_td"><?php echo !empty($count_3_5['male'][0]->count)>0 ? $count_3_5['male'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_5['female'][0]->count)>0 ? $count_3_5['female'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_5['transgender'][0]->count)>0 ? $count_3_5['transgender'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_5['children'][0]->count)>0 ? $count_3_5['children'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo ($count_3_5['male'][0]->count + $count_3_5['female'][0]->count + $count_3_5['children'][0]->count + $count_3_5['transgender'][0]->count); ?></td>

					<td class="text-center data_td_late"><?php echo !empty($late_count_3_5->male)>0 ? $late_count_3_5->male : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_5->female)>0 ? $late_count_3_5->female : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_5->transgender)>0 ? $late_count_3_5->transgender : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_5->children)>0 ? $late_count_3_5->children : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo ($late_count_3_5->male + $late_count_3_5->female+ $late_count_3_5->children + $late_count_3_5->transgender); ?></td>
				</tr>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>3.6</td>
					<td>The number of patients who did not return to the facilty and missed their doses in this month (Defaulter)</td>
						<td class="text-center data_td"><?php echo !empty($count_3_6['male'][0]->count)>0 ? $count_3_6['male'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_6['female'][0]->count)>0 ? $count_3_6['female'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_6['transgender'][0]->count)>0 ? $count_3_6['transgender'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_6['children'][0]->count)>0 ? $count_3_6['children'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo ($count_3_6['male'][0]->count + $count_3_6['female'][0]->count + $count_3_6['children'][0]->count + $count_3_6['transgender'][0]->count); ?></td>

				<!-- 	<td class="text-center data_td_late"><?php echo !empty($late_count_3_6->male)>0 ? $late_count_3_6->male : "-"; ?></td>
				<td class="text-center data_td_late"><?php echo !empty($late_count_3_6->female)>0 ? $late_count_3_6->female : "-"; ?></td>
				<td class="text-center data_td_late"><?php echo !empty($late_count_3_6->transgender)>0 ? $late_count_3_6->transgender : "-"; ?></td>
				<td class="text-center data_td_late"><?php echo !empty($late_count_3_6->children)>0 ? $late_count_3_6->children : "-"; ?></td>
				<td class="text-center data_td_late"><?php echo ($late_count_3_6->male + $late_count_3_6->female+ $late_count_3_6->children + $late_count_3_6->transgender); ?></td> -->

					<td class="text-center data_td_late"><?php echo !empty($count_3_6['male'][0]->count)>0 ? $count_3_6['male'][0]->count : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($count_3_6['female'][0]->count)>0 ? $count_3_6['female'][0]->count : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($count_3_6['transgender'][0]->count)>0 ? $count_3_6['transgender'][0]->count : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($count_3_6['children'][0]->count)>0 ? $count_3_6['children'][0]->count : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo ($count_3_6['male'][0]->count + $count_3_6['female'][0]->count + $count_3_6['children'][0]->count + $count_3_6['transgender'][0]->count); ?></td>
				</tr>
				 <tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>3.7</td>
					<td>Total number of patients referred to higher center for further management</td>
						<!-- <td class="text-center data_td"><?php echo !empty($count_3_7['male'][0]->count)>0 ? $count_3_7['male'][0]->count : "-"; ?></td>
											<td class="text-center data_td"><?php echo !empty($count_3_7['female'][0]->count)>0 ? $count_3_7['female'][0]->count : "-"; ?></td>
											<td class="text-center data_td"><?php echo !empty($count_3_7['transgender'][0]->count)>0 ? $count_3_7['transgender'][0]->count : "-"; ?></td>
											<td class="text-center data_td"><?php echo !empty($count_3_7['children'][0]->count)>0 ? $count_3_7['children'][0]->count : "-"; ?></td>
											<td class="text-center data_td"><?php echo ($count_3_7['male'][0]->count + $count_3_7['female'][0]->count + $count_3_7['children'][0]->count + $count_3_7['transgender'][0]->count); ?></td>
										
											<td class="text-center data_td_late"><?php echo !empty($late_count_3_7->male)>0 ? $late_count_3_7->male : "-"; ?></td>
											<td class="text-center data_td_late"><?php echo !empty($late_count_3_7->female)>0 ? $late_count_3_7->female : "-"; ?></td>
											<td class="text-center data_td_late"><?php echo !empty($late_count_3_7->transgender)>0 ? $late_count_3_7->transgender : "-"; ?></td>
											<td class="text-center data_td_late"><?php echo !empty($late_count_3_7->children)>0 ? $late_count_3_7->children : "-"; ?></td>
											<td class="text-center data_td_late"><?php echo ($late_count_3_7->male + $late_count_3_7->female+ $late_count_3_7->children + $late_count_3_7->transgender); ?></td> -->

				<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "-"; ?></td>
				<td class="text-center data_td"><?php echo "0"; ?></td>


				
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "-"; ?></td>
				<td class="text-center data_td_late"><?php echo "0"; ?></td>
				</tr>
				</tr> 
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>3.8</td>
					<td>Number of deaths reported</td>
						<td class="text-center data_td"><?php echo !empty($count_3_8['male'][0]->count)>0 ? $count_3_8['male'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_8['female'][0]->count)>0 ? $count_3_8['female'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_8['transgender'][0]->count)>0 ? $count_3_8['transgender'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo !empty($count_3_8['children'][0]->count)>0 ? $count_3_8['children'][0]->count : "-"; ?></td>
					<td class="text-center data_td"><?php echo ($count_3_8['male'][0]->count + $count_3_8['female'][0]->count + $count_3_8['children'][0]->count + $count_3_8['transgender'][0]->count); ?></td>

					<td class="text-center data_td_late"><?php echo !empty($late_count_3_8->male)>0 ? $late_count_3_8->male : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_8->female)>0 ? $late_count_3_8->female : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_8->transgender)>0 ? $late_count_3_8->transgender : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_3_8->children)>0 ? $late_count_3_8->children : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo ($late_count_3_8->male + $late_count_3_8->female+ $late_count_3_8->children + $late_count_3_8->transgender); ?></td>
				</tr>
			</tbody>
			<thead>
				<th style="background-color: #bbbbbb;color: black;font-family:'Source Sans Pro';">4</th>
				<th style="background-color: #bbbbbb;color: black;font-family:'Source Sans Pro';">Sustained Virologic Response</th>
			<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" nowrap="nowrap">Adult Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Male</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Female</th>
				<th style="background-color: #01446b;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</thead>
			<tbody>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>4.1</td>
					<td>Cumulative number of patients who are eligible for SVR ( i.e have completed 12 weeks after end of treatment)<!--  ( Out of 3.1) --></td>
					<td class="text-center data_td"><?php echo $count_4_1['male'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_4_1['female'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_4_1['transgender'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_4_1['children'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo ($count_4_1['male'][0]->count + $count_4_1['female'][0]->count + $count_4_1['children'][0]->count + $count_4_1['transgender'][0]->count); ?></td>

		
					<td class="text-center data_td_late"><?php echo !empty($late_count_4_1->male)>0 ? $late_count_4_1->male : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_4_1->female)>0 ? $late_count_4_1->female : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_4_1->transgender)>0 ? $late_count_4_1->transgender : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo !empty($late_count_4_1->children)>0 ? $late_count_4_1->children : "-"; ?></td>
					<td class="text-center data_td_late"><?php echo ($late_count_4_1->male + $late_count_4_1->female + $late_count_4_1->children + $late_count_4_1->transgender); ?></td>
				</tr>
				 <tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>4.2</td>
					<td>Number of patients who have undergone SVR out of the eligible patient ( out of 4.1)</td>
					<td class="text-center data_td"><?php echo $count_4_2['male'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_4_2['female'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_4_2['transgender'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_4_2['children'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo ($count_4_2['male'][0]->count + $count_4_2['female'][0]->count + $count_4_2['children'][0]->count + $count_4_2['transgender'][0]->count); ?></td>
				
					<td class="text-center data_td_late"><?php echo $late_count_4_2['male'][0]->count; ?></td>
					<td class="text-center data_td_late"><?php echo $late_count_4_2['female'][0]->count; ?></td>
					<td class="text-center data_td_late"><?php echo $late_count_4_2['transgender'][0]->count; ?></td>
					<td class="text-center data_td_late"><?php echo $late_count_4_2['children'][0]->count; ?></td>
					<td class="text-center data_td_late"><?php echo ($late_count_4_2['male'][0]->count + $late_count_4_2['female'][0]->count + $late_count_4_2['children'][0]->count + $late_count_4_2['transgender'][0]->count); ?></td>



					<!-- <td class="data_td_late text-center"><?php echo !empty($late_count_4_2->male)>0 ? $late_count_4_2->male : "-"; ?></td>
					<td class="data_td_late text-center"><?php echo !empty($late_count_4_2->female)>0 ? $late_count_4_2->female : "-"; ?></td>
					<td class="data_td_late text-center"><?php echo !empty($late_count_4_2->transgender)>0 ? $late_count_4_2->transgender : "-"; ?></td>
					<td class="data_td_late text-center"><?php echo !empty($late_count_4_2->children)>0 ? $late_count_4_2->children : "-"; ?></td>
					<td class="data_td_late text-center"><?php echo ($late_count_4_2->male + $late_count_4_2->female + $late_count_4_2->children + $late_count_4_2->transgender); ?></td> -->
				</tr>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>4.3</td>
					<td>Number of undetectable HCV RNA ( out of 4.2)</td>
					<td class="text-center data_td"><?php echo $count_4_3['male'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_4_3['female'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_4_3['transgender'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo $count_4_3['children'][0]->count; ?></td>
					<td class="text-center data_td"><?php echo ($count_4_3['male'][0]->count + $count_4_3['female'][0]->count + $count_4_3['children'][0]->count + $count_4_3['transgender'][0]->count); ?></td>
				
					<td class="text-center data_td_late"><?php echo $late_count_4_3['male'][0]->count; ?></td>
					<td class="text-center data_td_late"><?php echo $late_count_4_3['female'][0]->count; ?></td>
					<td class="text-center data_td_late"><?php echo $late_count_4_3['transgender'][0]->count; ?></td>
					<td class="text-center data_td_late"><?php echo $late_count_4_3['children'][0]->count; ?></td>
					<td class="text-center data_td_late"><?php echo ($late_count_4_3['male'][0]->count + $late_count_4_3['female'][0]->count + $late_count_4_3['children'][0]->count + $late_count_4_3['transgender'][0]->count); ?></td>

					<!-- <td class="data_td_late text-center"><?php echo !empty($late_count_4_3->male)>0 ? $late_count_4_3->male : "-"; ?></td>
					<td class="data_td_late text-center"><?php echo !empty($late_count_4_3->female)>0 ? $late_count_4_3->female : "-"; ?></td>
					<td class="data_td_late text-center"><?php echo !empty($late_count_4_3->transgender)>0 ? $late_count_4_3->transgender : "-"; ?></td>
					<td class="data_td_late text-center"><?php echo !empty($late_count_4_3->children)>0 ? $late_count_4_3->children : "-"; ?></td>
					<td class="data_td_late text-center"><?php echo ($late_count_4_3->male + $late_count_4_3->female + $late_count_4_3->children + $late_count_4_3->transgender); ?></td> -->
				
					
				</tr>
					

			</tbody>
		</table>
	</div>
</div>
</div>
<br>
<br>
<br>
<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name,filename) {
    if (!table.nodeType) table = document.getElementById(table)
            var newtable=document.createElement("table");
            var tbody =document.createElement("tbody");
             var row = document.createElement("tr");
             row.setAttribute("style", "background-color: black;color: white; font-family:'Source Sans Pro';");
  				var cell1 = document.createElement("td");
  				cell1.colSpan=7;
  				cell1.innerHTML ="Date as on : - <?php echo date('jS \ F Y'); ?>";
  				row.appendChild(cell1);
 			 	tbody.appendChild(row);
 			 	newtable.appendChild(tbody);
             var clonedTable=newtable.cloneNode(true);
             var clonedTable1=table.cloneNode(true);
            clonedTable.appendChild(clonedTable1);
    var ctx = {worksheet: name || 'Worksheet', table: clonedTable.innerHTML}
    //window.location.href = uri + base64(format(template, ctx))
     var link = document.createElement('a');
    link.download =filename;
    link.href = uri + base64(format(template, ctx));
    link.click();
     clonedTable.remove();
  }
})()
</script>
<script>

	
$(document).ready(function(){

$('[data-toggle="tooltip"]').tooltip();
$("#search_state").trigger('change');

$('#year').change(function(){
	var count=0;
	<?php  $count=0; ?>;
	var options='<option value="">Select</option>';
	var currentYear = (new Date).getFullYear();
	if (currentYear==$('#year').val()) {
			<?php
		$flag=0;
		$count=date("m");
	for ($i = 1; $i <=$count;$i++) {
	$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
	if($this->input->post('month')==$i){ 	?>
	options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php $flag=1;
	}	
	elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php }
	else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	}
	else{
		<?php
		$flag=0;
		$count=12;
		for ($i = 1; $i <=$count;$i++) {
		$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
		if($this->input->post('month')==$i){ 	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php $flag=1;
		}	
		elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php }
		else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	
	}
	//alert(options);
$('#month').html(options);
});
$("#year").trigger('change');
});
$('#search_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');
					//$('#input_district').val($('#input_district').val());
				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
					$('#mstfacilitylogin').html(data);
					
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

</script>