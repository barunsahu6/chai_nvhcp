<?php 
$loginData = $this->session->userdata('loginData');

$sql = "SELECT SVR FROM `MSTRole` where RoleId = ".$loginData->RoleId;
$result = $this->db->query($sql)->result();
?>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #A30A0C !important;
	background-color: white !important;
	border : 1px solid #A30A0C;
}
.clearable__clear{
  position: absolute;
  right:32px; top:29px;
  padding: 0 8px;
  font-style: normal;
  font-size: 1.2em;
  user-select: none;
  cursor: pointer;
}

.clearable__clear1{
  position: absolute;
  right:32px; top:33px;
  padding: 0 8px;
  font-style: normal;
  font-size: 1.2em;
  user-select: none;
  cursor: pointer;
}
</style>

<br>

<div class="row equal">
	<!-- <form action="" name="patient_form" id="patient_form" method="POST"> -->
					<?php
           $attributes = array(
              'id' => 'patient_form',
              'name' => 'patient_form',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>

		<input type="hidden" name="fetch_uid" id="fetch_uid">
		          <?php echo form_close(); ?>
		<!-- <input type="hidden" name="fetch_uid" id="fetch_uid"> -->
	<!-- </form> -->
	<div class="col-lg-10 col-lg-offset-1">

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="btn-group">
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">1. Registration</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">2. Screening</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">3. Viral Load</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">4. Testing</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/known_history/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">5. Known History</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">6. Prescription</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">7. Dispensation</a>
					<a class="btn btn-success" href="<?php echo base_url(); ?>patientinfo/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">8. SVR</a>
				</div>
			</div>
		</div>

		<!-- <form action="" method="POST" name="registration" id="registration"> -->
			<?php
           $attributes = array(
              'id' => 'registration',
              'name' => 'registration',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>
			<div class="row">
				<div class="col-md-12">
					<h4 class="text-center"><p>Patient Name - <strong><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FirstName)):''; ?></strong>  (<?php echo (count($patient_data) > 0)?$patient_data[0]->UID_Prefix:$uid_prefix; echo '-' .str_pad($patient_data[0]->UID_Num, 6, '0', STR_PAD_LEFT); ?>)<p>

						<?php if(count($patient_data) > 0 && ($patient_data[0]->SVR12W_Result == 1 || $patient_data[0]->SVR12W_Result == 2)){ ?>
							<p class="text-right"> <a target="_blanck"  href="<?php echo base_url().'printdiv/index/'.$patient_data[0]->PatientGUID; ?>"><i class="fa fa-print" aria-hidden="true"></i></a></p>
						<?php } ?>
					</h4>

					<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Patient SVR Module <?php echo set_hepc(); ?></h3>
				</div>
			</div>
			<br>

			<div class="container">
			<div class="row text-right">
			<div class="col-md-4 card well" style="background-color:#c30d10;">
			<div class="col-sm-4" >
    	<p style="margin: -10px;  margin-right: -181px; "><a href="#" align="right" id="creenterrejection" style="color: white; font-size: 12px;"> Re-enter VL data in case of sample rejection</a></p>
    </div>
</div>
</div>
</div>


			<div class="row hepc_fields">
				<div class="col-md-12">
					<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">HEP-C Viral Load - Sample Collection
						
						
					
					</h4>
				</div> 
			</div>
			<br>
			<div class="hepc_fields">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<label for="">Sample Drawn On Date <span class="text-danger">*</span></label>
						<input type="text" name="svr_sample_drawn_on" id="svr_sample_drawn_on" class="input_fields form-control hasCal dateInpt input2" value="<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->SVRDrawnDate):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" required="">
						<i class="fa fa-times clearable__clear"  id="clear-dates" title="clear date" ></i>
						<br class="hidden-lg-*">
						
					</div>
				</div>
					<div class="row" style="margin-left: 0px;">
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" style="background-color: lightgoldenrodyellow;">
								<label>Is Sample Stored </label>
								<select name="svr_is_sample_stored" id="svr_is_sample_stored" class="form-control input_fields">
									<option value="">Select</option>
									
							<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsSVRSampleStored == 1)?'selected':''; ?>>Yes</option>
							<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsSVRSampleStored == 2)?'selected':''; ?>>No</option>
								</select>
								<br>
					</div>
					<div class="col-md-3 col-md-3 col-sm-6 col-xs-12 hepc_sample_stored_field" style="background-color: lightgoldenrodyellow;">
								<label for="">Sample Storage Temperature (&#176;C)</label>
								<select name="svr_sample_storage_temp" id="svr_sample_storage_temp" class="input_fields form-control">
								<option>Select Temperature</option>
							<?php foreach ($vl_temp as $temp_val) { ?>
							<option value="<?php echo $temp_val->LookupCode; ?>" <?php echo (count($patient_data) > 0 && $patient_data[0]->SVRStorageTemp == $temp_val->LookupCode)?'selected':''; ?>><?php echo $temp_val->LookupValue; ?></option>
						<?php } ?>
						</select>
								<!-- <input type="number" value="<?php //echo (count($patient_data) > 0)?$patient_data[0]->SVRStorageTemp:''; ?>" name="svr_sample_storage_temp" id="svr_sample_storage_temp" class="input_fields form-control"> -->
								<br>
					</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hepc_sample_stored_field" style="background-color: lightgoldenrodyellow;">
						<label for="" >Sample Storage Duration</label>
						<select class="form-control input_fields" id="hepb_sample_stored_duration_more_less_than_day" name="hepb_sample_stored_duration_more_less_than_day">
							<option value="">Select</option>
							<option value="1" <?php  echo (count($patient_data) > 0 && $patient_data[0]->BStorage_days_hrs == 1)?'selected':''; ?>>Less than 1 day</option>
							<option value="2" <?php  echo (count($patient_data) > 0 && $patient_data[0]->BStorage_days_hrs == 2)?'selected':''; ?>>More than 1 day</option>
						</select>
						<br>
					</div>
					<div class="col-md-2 col-md-2 col-sm-6 col-xs-12 hepc_sample_stored_field" style="background-color: lightgoldenrodyellow;">
						<label for="hepb_sample_stored_duration" id="hepb_sample_stored_duration_label" >Duration (in hours)</label>
						<input type="text" name="hepb_sample_stored_duration" id="hepb_sample_stored_duration" class="input_fields form-control"  onkeypress="return onlyNumbersWithDot(event);" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->BStorageduration:''; ?>">
						<br>
					</div>
				</div>
					


					<br>
					<div class="row" style="margin-left: 0px;">
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" style="background-color: lightblue;">
								<label for="" style="padding-top: 5px;">Is Sample Transported</label>
								<select class="form-control input_fields" id="svr_is_sample_transported" name="svr_is_sample_transported">
									<option value="">Select</option>
									<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsSVRSampleTransported == 1)?'selected':''; ?>>Yes</option>
							<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsSVRSampleTransported == 2)?'selected':''; ?>>No</option>
								</select>
								<br>
					</div>
					<div class="col-md-3 hepc_sample_transported_field" style="background-color: lightblue;">
								<label for="" style="padding-top: 5px;">Sample Transport Temperature (&#176;C)</label>

								<select name="svr_sample_transport_temp" id="svr_sample_transport_temp" class="input_fields form-control">
								<option>Select Temperature</option>
							<?php foreach ($vl_temp as $temp_val) { ?>
							<option value="<?php echo $temp_val->LookupCode; ?>" <?php echo (count($patient_data) > 0 && $patient_data[0]->SVRTransportTemp == $temp_val->LookupCode)?'selected':''; ?>><?php echo $temp_val->LookupValue; ?></option>
						<?php } ?>
					</select>

								<!-- <input type="number" value="<?php //echo (count($patient_data) > 0)?$patient_data[0]->SVRTransportTemp:''; ?>" name="svr_sample_transport_temp" id="svr_sample_transport_temp" class="input_fields form-control"> -->
								<br>
					</div>
					<div class="col-md-2 hepc_sample_transported_field" style="background-color: lightblue;">
								<label for="" style="padding-top: 5px;">Sample Transport Date</label>
								<input type="text" name="svr_sample_transport_date" id="svr_sample_transport_date" class="input_fields form-control hasCal dateInpt input2" value="<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->SVRTransportDate):''; ?>" class="input_fields form-control" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
								<i class="fa fa-times clearable__clear1"  aria-hidden="true" id="clear-dates1" title="clear date"></i>
								<br>
								
					</div>
					<div class="col-md-2 hepc_sample_transported_field" style="background-color: lightblue;">
								<label for="" style="padding-top: 5px;">Sample Transported To</label>
								<select type="text" name="svr_sample_transport_to" id="svr_sample_transport_to" class="form-control input_fields" >
									<option value="">Select</option>
									<?php 
							foreach ($search_facilitiesval as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->SVR12W_LabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
								<?php 
							}
							?>
								</select>
								<br>
					</div>
						</div>
						<div class="row" style="margin-left: 0px;">
							<div class="col-md-3 hepc_sample_transported_field" style="background-color: lightblue;">
							<label for="hepc_sample_transported_to_name" style="padding-top: 5px;">Sample Transported By : Name</label>
								<input type="text" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVRTransporterName:''; ?>" name="svr_sample_transport_by_name" id="svr_sample_transport_by_name" class="input_fields form-control messagedata">
								<br>
							</div>
					
							<div class="col-md-2 hepc_sample_transported_field" style="background-color: lightblue;">
								<label for="" style="padding-top: 5px;">Designation</label>
								<select type="text" name="svr_sample_transport_by_designation" id="svr_sample_transport_by_designation" class="form-control input_fields" >
									<option value="">Select</option>
										<?php 
							foreach ($designation_options_list as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->SVRTransporterDesignation == $row->designation_listId)?'selected':''; ?> value="<?php echo $row->designation_listId; ?>"><?php echo $row->Designation; ?></option>
								<?php 
							}
							?>
								</select>
								<br>
							</div>
						</div>
					
					<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<label for="">Remarks</label>
						<textarea rows="5" style="border: 1px #CCC solid; width: 100%;" name="svr_remarks" id="svr_remarks"> <?php echo (count($patient_data) > 0)?$patient_data[0]->SVRTransportRemark:''; ?> </textarea>
					</div>
				</div>
				<br>
			</div>

			<div class="row">
				<div class="col-md-12">
					<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">SVR Details - Result</h4>
				</div>
			</div>
			<br>
			<div class="row" style="margin-left: 0px;">
				<div class="col-md-2 sample_accepted_fields" style="background-color: lightblue;">
					<label for="svr_sample_receipt_date" style="padding-top: 5px;">Sample Receipt Date</label>
					<input type="text" name="svr_sample_receipt_date" value="<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->SVRReceiptDate):''; ?>" id="svr_sample_receipt_date" class="input_fields form-control hasCal dateInpt input2" onkeyup="return false" max="<?php echo date('Y-m-d');?>"" >
					<i class="fa fa-times clearable__clear1"   aria-hidden="true" id="clear-dates2" title="clear date"></i>
					<br>
					
				</div>
			    <div class="col-md-3 sample_accepted_fields"  style="background-color: lightblue;">
						<label for="sample_accepted_fields" style="padding-top: 5px;">Sample Received By : Name</label>
					<input type="text" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVRReceiverName:''; ?>" name="svr_sample_receieved_by_name" id="svr_sample_receieved_by_name" class="input_fields form-control messagedata" >
					<br>
				</div>
				<div class="col-md-2 sample_accepted_fields" style="background-color: lightblue;">
					<label for="svr_sample_receieved_by_designation" style="padding-top: 5px;">Designation</label>
					<select class="form-control input_fields" id="svr_sample_receieved_by_designation" name="svr_sample_receieved_by_designation">
						<option value="">Select</option>
						<?php 
							foreach ($designation_options_list as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->SVRReceiverDesignation == $row->designation_listId)?'selected':''; ?> value="<?php echo $row->designation_listId; ?>"><?php echo $row->Designation; ?></option>
								<?php 
							}
							?>
					</select>
					<br>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-2">
						<label for="">Is Sample Accepted <span class="text-danger">*</span></label>
						<select class="form-control input_fields" id="hepc_is_sample_accepted" name="hepc_is_sample_accepted" required="">
							
							<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsSVRSampleAccepted == 1)?'selected':''; ?>>Yes</option>
							<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->IsSVRSampleAccepted == 2)?'selected':''; ?>>No</option>
							<!-- <option value="">Select</option> -->
						</select>
						<br class="hidden-lg-*">
					</div>
				<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 hepc_sample_accepted_fields">
					<label for="">Test Result Date <span class="text-danger">*</span></label>
					<input type="text" name="svr_result_date" value="<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->SVR12W_HCVViralLoad_Dt):''; ?>" id="svr_result_date" class="input_fields form-control hasCal dateInpt input2" onkeyup="return false" max="<?php echo date('Y-m-d');?>" required="">
					<i class="fa fa-times clearable__clear" aria-hidden="true" id="clear-dates3" title="clear date"></i>
					<br class="hidden-lg-*">
					
				</div>

				<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 hepc_sample_accepted_fields">
					<label for="">Result <span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="svr_result" name="svr_result" required="">
						<option value="">Select</option>
						<?php 
							foreach ($results_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->SVR12W_Result == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
					</select>
				</div>

				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hepc_sample_accepted_fields">
					<label for="">Viral Load Count <span class="text-danger">*</span></label>
					<input type="text" name="svr_viral_load" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVR12W_HCVViralLoadCount:''; ?>" id="svr_viral_load" class="input_fields form-control" required="" onkeypress="return onlyNumbersWithDot(event);">
					<br class="hidden-lg-*">
				</div>
			 	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hepc_sample_accepted_fields">
					<label for="">Re-enter Viral Load Count <span class="text-danger">*</span></label>
					<input type="text" name="svr_re_entry_viral_load" id="svr_re_entry_viral_load" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVR12W_HCVViralLoadCount:''; ?>" class="input_fields form-control" required="" onkeypress="return onlyNumbersWithDot(event);">
					<br class="hidden-lg-*">
				</div> 
				
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hepc_sample_rejected_fields">
						<label for="">Reason for Rejection</label>
						<select class="form-control input_fields" id="hepc_sample_reason_for_rejection" name="hepc_sample_reason_for_rejection">
							<option value="">Select</option>
							<?php 
							foreach ($rejection_options as $row) {
								?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->SVRRejectionReason == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php 
							}
							?>
						</select>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 hepc_sample_reason_for_rejection_other_field">
						<label for="">Others specify</label>
						<input type="text" class="form-control input_fields" name="hepc_sample_reason_for_rejection_other" id="hepc_sample_reason_for_rejection_other" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVRRejectionReasonOther:''; ?>">
					</div>

				<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 hepc_sample_rejected_fields">
					<label for="">Doctor</label>
					<select class="form-control input_fields" id="svr_doctor" name="svr_doctor">
						<option value="">Select</option>
						<?php foreach ($doctors as $row) {?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->SVRDoctor == $row->id_mst_medical_specialists)?'selected':''; ?> value="<?php echo $row->id_mst_medical_specialists; ?>"><?php echo $row->name; ?></option>
							<?php } ?>

					</select>
				</div>
				<div class="row col-lg-2 col-md-3 col-sm-6 col-xs-12 svr_doctor_other_field">
						<label for="">SVR Doctor Other <span class="text-danger">*</span></label>
						<input type="text" name="svr_doctor_other" id="svr_doctor_other" class="input_fields form-control messagedata" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->SVRDoctorOther:''; ?>">
					</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
					<label for="">Comments</label>
					<textarea rows="5" name="svr_comments" value="" id="svr_comments" class="form-control" style="border: 1px #CCC solid; width: 100%;"><?php echo (count($patient_data) > 0)?$patient_data[0]->SVRComments:''; ?></textarea>
				</div>
			</div>

			

			<br>
			<input type="hidden" name="interruption_status" id="interruption_status" value="<?php  echo (count($patient_data) > 0)?$patient_data[0]->InterruptReason:'';  ?>">
			
			<input type="hidden" name="Next_Visitdtn" id="Next_Visitdtn" class="hasCal2 dateInpt input2" value="<?php echo timeStampShow($patient_data[0]->AdvisedSVRDate); ?>">
			<div class="row">
				<div class="col-lg-3 col-md-2">
					<a class="btn btn-block btn-default form_buttons" href="<?php echo base_url('patientinfo?p=1'); ?>" id="close" name="close" value="close">CLOSE</a>
				</div>
				<div class="col-lg-3 col-md-2">
					<button class="btn btn-block btn-default form_buttons" id="refresh" name="refresh" value="refresh">REFRESH</button>
				</div>
				<div class="col-lg-3 col-md-2">
					<button class="btn btn-block btn-default form_buttons" id="lock" name="lock" value="lock">LOCK</button>
				</div>
				<?php if($result[0]->SVR == 1) {?>
					<div class="col-lg-3 col-md-2">
						<button class="btn btn-block btn-success form_buttons" id="save" name="save" value="save">SAVE</button>
					</div>
				<?php } ?>
			</div>
			<br>
			<br>
			<br>

			<div class="row" class="text-left">

					<div class="col-md-6 card well">
					 <label class="col-sm-4"  style="text-align: left !important; line-height: 2.2 !important; ">Patient's Status -</label>
					<div class="col-sm-8" style="text-align: left !important; line-height: 2.2 !important; ">
					<label><?php echo (count($patient_status) > 0)?$patient_status[0]->status:''; ?></label>

					</div>
				</div>

				

				<div class="col-md-6 card well">
					<label class="col-sm-6" style="text-align: left !important; ">Patient's Interruption Status </label>
					<div class="col-sm-6">
					<select name="" id="type_val" onchange="openPopup()" class="btn" style="text-align: right!important;">
						<option value="">Select</option>
						<option value="1"  <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason != '')?'selected':''; ?> >Yes</option>
						<option value="0" <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason == '')?'selected':''; ?>>No</option>
					</select>
				
				</div>
				</div>
			
			</div>

		          <?php echo form_close(); ?>
		<!-- </form> -->
	</div>
</div>



<div class="modal fade" id="addMyModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title">Patient's Interruption Status</h4>
      </div>
      <span id='form-error' style='color:red'></span>
      <div class="modal-body">
      <!--   <form role="form" id="newModalForm" method="post"> -->
      	<?php
           $attributes = array(
              'id' => 'newModalForm',
              'name' => 'newModalForm',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>


				<div class="form-group">
				<label class="control-label col-md-3" for="email">Reason:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonval" name="resonval" required="required">
				<option value="">Select</option>
				<?php foreach ($InterruptReason as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
			<?php } ?>

				</select> 
				</div>
				</div>
<br/><br/>

				<div class="form-group resonfielddeath" >
				<label class="control-label col-md-3" for="email">Reason for death:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonvaldeath" name="resonvaldeath">
				<option value="">Select</option>
				<?php foreach ($reason_death as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
				
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>

				<div class="form-group resonfieldlfu">
				<label class="control-label col-md-3" for="email">Reason for LFU:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonfluid" name="resonfluid">
				<option value="">Select</option>
				<?php foreach ($reason_flu as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
				
			<?php } ?>
				</select> 
				</div>
				</div>
<div class="form-group resonfieldothers" >
				<label class="control-label col-md-3" for="email">Other(specify):</label>
				<div class="col-md-9">
				<input type="text" class="form-control" id="otherInterrupt" name="otherInterrupt">
				</div>
				</div>
<br/><br/>


			<!-- <div class="control-label col-md-3">
				
				<input type="radio" name="interruption_stage" value="1">
				<label>ETR</label>
			</div> -->

<div class="svrRecommendedhs">			
			<div class="form-group" >
<label>SVR Recommended</label>
			</div>

			<div class="control-label col-md-3">
				<input type="radio" name="interruption_stage" value="2">
				<label>Yes</label>
			</div>

			<div class="control-label col-md-3">
				<input type="radio" name="interruption_stage" value="3">
				<label>No</label>
			</div>
		</div>
			<br/><br/>

			


          <div class="modal-footer">
            <button type="submit" class="btn btn-success" id="btnSaveIt">Save</button>
            <button type="button" class="btn btn-default" id="btnCloseIt" data-dismiss="modal">Close</button>
          </div>
           <?php echo form_close(); ?>
       <!--  </form> -->
      </div>
    </div>
  </div>
</div>

<br/><br/><br/>

<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>

<script>


	$(document).ready(function(){
		if($('#hepc_is_sample_accepted').val()=='1'){
			$('.hepc_sample_accepted_fields').show();
			$('.hepc_sample_rejected_fields').hide();
			
		}

	});

<?php if((count($patient_data) > 0 ) && $patient_data[0]->SVR12W_Result == 2){ ?>
$('#svr_viral_load').attr("readonly", true);
$('#svr_re_entry_viral_load').attr("readonly", true);
<?php }  ?>
$("#svr_result").change(function( event ) {
	//alert($('#hepc_vl').val());
		if($('#svr_result').val() == 2){
		$('#svr_viral_load').val('<15 IU/mL');
		$('#svr_re_entry_viral_load').val('<15 IU/mL');
		$('#svr_viral_load').attr("readonly", true);
		$('#svr_re_entry_viral_load').attr("readonly", true);

	}else{
		
		$('#svr_viral_load').val('');
		$('#svr_re_entry_viral_load').val('');
		$('#svr_viral_load').attr("readonly", false);
		$('#svr_re_entry_viral_load').attr("readonly", false);
	}

});
<?php if(count($patient_data) > 0 && $patient_data[0]->IsSVRSampleTransported == 1 ){ ?>

$(".sample_accepted_fields").show();
<?php } else { ?>
$(".sample_accepted_fields").hide();
	<?php  } ?>
	$(document).ready(function(){
		if($('#svr_is_sample_stored').val()==''){
			$('#svr_is_sample_stored').val('2');
			
		}

	});


	$(document).ready(function(){
		if($('#svr_is_sample_transported').val()==''){
			$('#svr_is_sample_transported').val('2');
			$(".sample_accepted_fields").hide();
		}

	});

$(document).ready(function(){
var $dates = $('#svr_sample_drawn_on').datepicker();

$('#clear-dates').on('click', function () {
    $dates.datepicker('setDate', null);
});
var $dates1 = $('#svr_sample_transport_date').datepicker();
$('#clear-dates1').on('click', function () {
    $dates1.datepicker('setDate', null);
});
var $dates2 = $('#svr_sample_receipt_date').datepicker();
$('#clear-dates2').on('click', function () {
    $dates2.datepicker('setDate', null);
});
var $dates3= $('#svr_result_date').datepicker();
$('#clear-dates3').on('click', function () {
    $dates3.datepicker('setDate', null);
});

});	

$(document).ready(function(){
<?php if(count($patient_data) > 0 && $patient_data[0]->SVR12W_Result==2){ ?>

					$("#modal_header").text("Patient Treatment Successful");
					$("#modal_text").text("Successful");
					$("#multipurpose_modal").modal("show");
					return false;

<?php }elseif(count($patient_data) > 0 && $patient_data[0]->SVR12W_Result==1){ ?>
					$("#modal_header").text("Patient Treatment Unsuccessful");
					$("#modal_text").text("Unsuccessful");
					$("#multipurpose_modal").modal("show");
					return false;
<?php } ?>
});

 $('.hasCal2').each(function() {
        ddate=$(this).val();
        yRange="1990:"+new Date().getFullYear();
        yr=$(this).attr('year-range');
        if(yr){
            yRange=yr;
        }
        
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
            changeMonth: true,
            yearRange: yRange,
           // maxDate: 0,
        });
        $(this).attr('placeholder','dd-mm-yy');
        if($(this).attr('noweekend')){
            $(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
        }
        $(this).datepicker( "setDate", ddate);
    }); 
 
/*$("#svr_viral_load").change(function( event ) {
	
		if($('#svr_viral_load').val() > 0){
		$('#svr_result').val(1);
	}else{
		$('#svr_result').val('');
	}

});*/

<?php if(count($patient_data) > 0 && $patient_data[0]->SVRRejectionReason == 99){ ?>
$(".hepc_sample_reason_for_rejection_other_field").show();
<?php } else{?>
	$(".hepc_sample_reason_for_rejection_other_field").hide();
<?php } ?>
$("#hepc_sample_reason_for_rejection").change(function(){

				if($(this).val() == 99)
				{
					$(".hepc_sample_reason_for_rejection_other_field").show();
				}
				else
				{
					$(".hepc_sample_reason_for_rejection_other_field").hide();
					$('#hepc_sample_reason_for_rejection_other').val('');
				}
			});


 <?php if(count($patient_data) > 0 && $patient_data[0]->IsSVRSampleAccepted == 1){ ?>
					$(".hepc_sample_accepted_fields").show();
					$(".hepc_sample_rejected_fields").hide();
 <?php } else{?>
$(".hepc_sample_accepted_fields").hide();
					$(".hepc_sample_rejected_fields").show();
					$('#svr_result_date').prop('required',false); 
					$('#svr_viral_load').prop('required',false);
					$('#svr_re_entry_viral_load').prop('required',false);
					$('#svr_result').prop('required',false);

					$('#svr_result_date').val('');
					$('#svr_viral_load').val('');
					$('#svr_re_entry_viral_load').val('');
					$('#svr_result').val('');
 <?php  } ?>

 /*$(".hepc_sample_reason_for_rejection_other_field").hide();
  $(".hepc_sample_rejected_fields").hide();*/
 
	$("#hepc_is_sample_accepted").change(function(){

				if($(this).val() == 1)
				{
					
					$(".hepc_sample_accepted_fields").show();
					$(".hepc_sample_rejected_fields").hide();
					$(".hepc_sample_reason_for_rejection_other_field").hide();

					$('#hepc_sample_reason_for_rejection').val('');
					$('#hepc_sample_reason_for_rejection_other').val('');
					$('#svr_doctor').val('');
					/*$('#hepc_vl_result_date').prop('required',true); 
					$('#hepc_vl').prop('required',true);
					$('#hepc_vl_reenter').prop('required',true);
					$('#hepc_vl_result').prop('required',true);*/

					

					


				}
				else if($(this).val() == 2)
				{
					
					$(".hepc_sample_accepted_fields").hide();
					$(".hepc_sample_rejected_fields").show();
					$('#svr_result_date').prop('required',false); 
					$('#svr_viral_load').prop('required',false);
					$('#svr_re_entry_viral_load').prop('required',false);
					$('#svr_result').prop('required',false);

					$('#svr_result_date').val('');
					$('#svr_viral_load').val('');
					$('#svr_re_entry_viral_load').val('');
					$('#svr_result').val('');

				}
				else
				{
					$(".hepc_sample_accepted_fields").show();
					$(".hepc_sample_rejected_fields").hide();

					

				}
			});

$("#hepc_sample_stored_duration_more_less_than_day").change(function(){

				if($(this).val() == 1)
				{
					$("#hepc_sample_stored_duration_label").html('Duration (in hours)');
				}
				else if($(this).val() == 2)
				{
					$("#hepc_sample_stored_duration_label").html('Duration (in days)');
				}
				else
				{
					$("#hepc_sample_stored_duration_label").html('Duration (in hours)');
				}
			});

			$("#hepb_sample_stored_duration_more_less_than_day").change(function(){

				if($(this).val() == 1)
				{
					$("#hepb_sample_stored_duration_label").html('Duration (in hours)');
				}
				else if($(this).val() == 2)
				{
					$("#hepb_sample_stored_duration_label").html('Duration (in days)');
				}
				else
				{
					$("#hepb_sample_stored_duration_label").html('Duration (in hours)');
				}
			});

		$("#hepb_sample_stored_duration" ).change(function( event ) {
				
				var hepc_sample_storedcheck = $('#hepb_sample_stored_duration_more_less_than_day').val();
				var hepc_sample_stored_duration = $('#hepb_sample_stored_duration').val();
				if(hepc_sample_storedcheck == 1 && hepc_sample_stored_duration >24){
				 

					$("#modal_header").text("Duration Less than or equal to 24 (in hours)");
					$("#modal_text").text("Please check Duration(in hours)");
					$('#hepb_sample_stored_duration').val(12);
					$("#multipurpose_modal").modal("show");
					return false;

	}

});

	$("#hepb_is_sample_stored" ).change(function( event ) {

		if($("#hepb_is_sample_stored" ).val() ==2){
		$('#hepb_sample_storage_temp').val('');
		$('#hepb_sample_stored_duration').val('');
		$('#hepb_sample_stored_duration_more_less_than_day').val('');
		}

	});
$("#hepb_sample_stored_duration_more_less_than_day" ).change(function( event ) {
				$('#hepb_sample_stored_duration').val('');
				var hepc_sample_storedcheck = $('#hepb_sample_stored_duration_more_less_than_day').val();
				var hepc_sample_stored_duration = $('#hepb_sample_stored_duration').val();
				if(hepc_sample_storedcheck ==1 && hepc_sample_stored_duration >24){
				 

					$("#modal_header").text("Duration Less than or equal to 24 (in hours)");
					$("#modal_text").text("Please check Duration(in hours)");
					$('#hepb_sample_stored_duration').val(12);
					$("#multipurpose_modal").modal("show");
					return false;

	}

});

	function openPopup() {
		var type_val = $('#type_val').val();
		if(type_val=='1'){
    $("#addMyModal").modal();
		}
}

$('.resonfielddeath').hide();
$('.resonfieldlfu').hide();
$('.resonfieldothers').hide();

$('#btnCloseIt').click(function(){

$('#type_val').val('0');
$('#interruption_status').val('');

});

$('#type_val').change(function(){
var type_val = $('#type_val').val();
$('#interruption_status').val(type_val);

});


 $('#btnSaveIt').click(function(e){
      
      e.preventDefault(); 
      $("#form-error").html('');
     
        
	 var formData = new FormData($('#newModalForm')[0]);
	 $('#btnSaveIt').prop('disabled',true);
	 $.ajax({				    	
		url: '<?php echo base_url(); ?>patientinfo/interruptionstatus_process/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
		type: 'POST',
		data: formData,
		dataType: 'json',
		async: false,
        cache: false,
		contentType: false,
		processData: false,
		success: function (data) { 
				
			if(data['status'] == 'true'){
				$("#form-error").html('Data has been successfully submitted');

				if(data['interruptstage'] == 2)
					{	
						alert('Patient interruption status has been successfully updated.Please update the SVR page when the patient has conducted the SVR test.');

						location.href='<?php echo base_url(); ?>patientinfo/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';

					}
					else if(data['interruptstage'] == 3)
					{
						alert('Patient interruption status has been successfully updated. SVR has not been recommended.');
						location.href='<?php echo base_url(); ?>patientinfo/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
					}
					else{
				setTimeout(function() {
    					location.href='<?php echo base_url(); ?>patientinfo/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
				}, 1000);
			}

			}else{
				$("#form-error").html(data['message']);
				$('#btnSaveIt').prop('disabled',false); 
				return false;
			}   
		}        
	 }); 
		
   

    }); 

$('#resonval').change(function(){
		

		if($('#resonval').val() == '1'){

		$('.resonfielddeath').show();
		$('.resonfieldlfu').hide();
		$('.resonfieldothers').hide();
		$('#resonfluid').val('');

		$('#resonvaldeath').prop('required',true);
		$('#resonfluid').prop('required',false);
		$('#otherInterrupt').prop('required',false);
		$('.svrRecommendedhs').hide();

	}else if($('#resonval').val() == '2'){
		$('.resonfielddeath').hide();
		$('.resonfieldlfu').show();
		$('.resonfieldothers').hide();
		$('#resonvaldeath').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',true);
		$('#otherInterrupt').prop('required',false);
		$('.svrRecommendedhs').show();
	}else if($('#resonval').val() == '99'){

		$('.resonfieldothers').show();
		$('.resonfielddeath').hide();
		$('.resonfieldlfu').hide();
		$('#otherInterrupt').prop('required',true);
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',false);
		$('.svrRecommendedhs').show();
	}

	else{

		$('.resonfielddeath').hide();
		$('.resonfieldlfu').hide();
		$('.resonfieldothers').hide();
		$('#resonvaldeath').val('');
		$('#resonfluid').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',false);
		$('#otherInterrupt').prop('required',false);
	}

});
</script>


<script type="text/javascript">

	$("#svr_doctor").change(function(){
					if($(this).val() == 999)
					{
						$(".svr_doctor_other_field").show();
						$('#svr_doctor_other').prop('required',true);
					}
					else
					{
						$(".svr_doctor_other_field").hide();
						$('#svr_doctor_other').prop('required',false);
						$('#svr_doctor_other').val('');
					}
				});



	$('#svr_sample_storage_temp').change(function( event ) {
		
	var svr_sample_storage_temp = $('#svr_sample_storage_temp').val();

		if(svr_sample_storage_temp > 100){

						$("#modal_header").text("Sample Storage Temperature (°C) Less than or equal to 100 to -100");
						$("#modal_text").text("Please check Sample Storage Temperature (°C)");
						$('#svr_sample_storage_temp').val(0);
						$("#multipurpose_modal").modal("show");
						return false;

			
		}

		if(svr_sample_storage_temp < -100){

						$("#modal_header").text("Sample Storage Temperature (°C) Less than or equal to 100 to -100");
						$("#modal_text").text("Please check Sample Storage Temperature (°C)");
						$('#svr_sample_storage_temp').val(0);
						$("#multipurpose_modal").modal("show");
						return false;

			
		}

		});


	$('#svr_sample_transport_temp').change(function( event ) {
		
	var svr_sample_transport_temp = $('#svr_sample_transport_temp').val();

		if(svr_sample_transport_temp > 100){

						$("#modal_header").text("Sample Storage Temperature (°C) Less than or equal to 100 to -100");
						$("#modal_text").text("Please check Sample Storage Temperature (°C)");
						$('#svr_sample_transport_temp').val(0);
						$("#multipurpose_modal").modal("show");
						return false;

			
		}

		if(svr_sample_transport_temp < -100){

						$("#modal_header").text("Sample Storage Temperature (°C) Less than or equal to 100 to -100");
						$("#modal_text").text("Please check Sample Storage Temperature (°C)");
						$('#svr_sample_transport_temp').val(0);
						$("#multipurpose_modal").modal("show");
						return false;

			
		}

		});



$(".svr_doctor_other_field").hide();
	<?php 
				if( count($patient_data) > 0 && $patient_data[0]->SVRDoctor == 999 ) {
				?>

						$(".svr_doctor_other_field").show();
						$('#svr_doctor_other').prop('required',true);
					<?php } else{ ?>
					
						$(".svr_doctor_other_field").hide();
						$('#svr_doctor_other').prop('required',false);
				<?php	}  ?>

	$("#svr_sample_drawn_on" ).change(function( event ) {

var date_of_prescribing_testsdate = $("#Next_Visitdtn" ).datepicker('getDate');
var svr_sample_drawn_on = $("#svr_sample_drawn_on" ).datepicker('getDate');

var date_of_prescribing_testsdate1 = $("#Next_Visitdtn" ).val();
//alert(date_of_prescribing_testsdate);

			var date = $("#Next_Visitdtn" ).datepicker('getDate');
			var remaningpillsval =  3;
			days = parseInt(remaningpillsval, 10);
			if(!isNaN(date.getTime())){
			date.setDate(date.getDate() - days);



if(date > svr_sample_drawn_on ){

	$("#modal_header").text("Visit Date greater than or equal to Prescribing Date"+date_of_prescribing_testsdate1);
					$("#modal_text").text("Please check dates");
					$("#svr_sample_drawn_on" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

}

});

	


 $("#svr_sample_transport_date" ).change(function( event ) {

//var screening_date = '<?php echo $patient_data[0]->Next_Visitdt; ?>';


var svr_sample_transport_date = $("#svr_sample_transport_date").datepicker('getDate');
var svr_sample_drawn_on = $("#svr_sample_drawn_on" ).datepicker('getDate');
//alert(svr_sample_transport_date+'/'+svr_sample_drawn_on);
if(svr_sample_drawn_on > svr_sample_transport_date){

	$("#modal_header").text("Sample Transport Date  greater than or equal Sample Drawn On Date .");
					$("#modal_text").text("Please check dates");
					$("#svr_sample_transport_date" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

});



Date.prototype.toInputFormat = function() {
       var yyyy = this.getFullYear().toString();
       var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
       var dd  = this.getDate().toString();
       return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
    };



		$('.messagedata').keypress(function (e) {
        var regex = new RegExp(/^[a-zA-Z\s]+$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            return false;
        }
    });

				function onlyNumbersWithDot(e) {           
            var charCode;
            if (e.keyCode > 0) {
                charCode = e.which || e.keyCode;
            }
            else if (typeof (e.charCode) != "undefined") {
                charCode = e.which || e.keyCode;
            }
            if (charCode == 46)
                return true
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }




 $("#svr_sample_receipt_date" ).change(function( event ) {

//var screening_date = '<?php echo $patient_data[0]->Next_Visitdt; ?>';


var svr_sample_drawn_on = $("#svr_sample_drawn_on").datepicker('getDate');
var svr_sample_receipt_date = $("#svr_sample_receipt_date" ).datepicker('getDate');

if(svr_sample_drawn_on > svr_sample_receipt_date){

	$("#modal_header").text("Sample Receipt Date  greater than or equal Sample Receipt Date ");
					$("#modal_text").text("Please check dates");
					$("#svr_sample_receipt_date" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

});


 $("#svr_result_date" ).change(function( event ) {

//var screening_date = '<?php echo $patient_data[0]->Next_Visitdt; ?>';



var svr_result_date = $("#svr_result_date" ).datepicker('getDate');

if($("#svr_sample_receipt_date").datepicker('getDate')!=null){
var svr_sample_receipt_date = $("#svr_sample_receipt_date").datepicker('getDate');
//alert('notblanck'+$("#svr_sample_receipt_date").datepicker('getDate'));
}else{
	var svr_sample_receipt_date = $("#svr_sample_drawn_on").datepicker('getDate');
	//alert('blanck');
}

if(svr_sample_receipt_date > svr_result_date){

	$("#modal_header").text("Test Result Date greater than or equal Sample Receipt Date");
					$("#modal_text").text("Please check dates");
					$("#svr_result_date" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

});



$('#creenterrejection').click(function( event ) {
		//alert('ddd');
		$('#svr_sample_drawn_on').val('');
		$('#svr_is_sample_stored').val('');
		$('#svr_sample_storage_temp').val('');
		$('#svr_is_sample_transported').val('');
		$('#svr_sample_transport_temp').val('');
		$('#svr_sample_transport_date').val('');

		$('#svr_sample_transport_to').val('');
		$('#svr_sample_transport_by_name').val('');
		$('#svr_sample_transport_by_designation').val('');
		$('#svr_remarks').val('');
		$('#svr_sample_receipt_date').val('');
		$('#svr_sample_receieved_by_name').val('');

		$('#svr_sample_receieved_by_designation').val('');
		$('#svr_result_date').val('');
		$('#svr_viral_load').val('');
		$('#svr_result').val('');
		$('#svr_doctor').val('');
		$('#svr_re_entry_viral_load').val('');
		


	})	


$("#svr_re_entry_viral_load" ).change(function( event ) {

	var hepc_vl = $("#svr_viral_load").val();
	var hepc_vl_reenter = $("#svr_re_entry_viral_load").val();

	if(hepc_vl != hepc_vl_reenter){

	$("#modal_header").text("Viral Load does not match re viral load");
					$("#modal_text").text("Please check dates");
					$("#svr_re_entry_viral_load" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}


});

</script>
<?php if($result[0]->SVR == 1) {?>
	<script>

		$(document).ready(function(){




			<?php 
			$error = $this->session->flashdata('error'); 

			if($error != null)
			{
				?>
				$("#multipurpose_modal").modal("show");
				<?php 
			}
			?>

			$("#refresh").click(function(e){
			// if(confirm('All further details will be deleted.Do you want to continue?')){ 
				$("input").val('');
				$("select").val('');
				$("textarea").val('');
				$("#input_district").html('<option>Select District</option>');
				$("#input_block").html('<option>Select Block</option>');

				e.preventDefault();
			// }
			});

<?php if(count($patient_data) > 0 && $patient_data[0]->IsSVRSampleStored == 1) {?>
				$('.hepc_sample_stored_field').show();
<?php } else { ?>
			
			$('.hepc_sample_stored_field').hide();

<?php } ?>
<?php if(count($patient_data) > 0 && $patient_data[0]->IsSVRSampleTransported == 1) {?>
			$('.hepc_sample_transported_field').show();
	<?php } else{ ?>
			$('.hepc_sample_transported_field').hide();
	<?php } ?>



			$("#svr_is_sample_stored").change(function(){
				if($(this).val() == 1)
				{
					$(".hepc_sample_stored_field").show();
					$('#svr_sample_storage_temp').prop('required',true); 
					$('#hepb_sample_stored_duration_more_less_than_day').prop('required',true);
					$('#hepb_sample_stored_duration').prop('required',true);
				}
				else
				{
					$(".hepc_sample_stored_field").hide();
					$('#svr_sample_storage_temp').prop('required',false); 
					$('#hepb_sample_stored_duration_more_less_than_day').prop('required',false);
					$('#hepb_sample_stored_duration').prop('required',false);

					$('#svr_sample_storage_temp').val('');
					$('#hepb_sample_stored_duration_more_less_than_day').val('');
					$('#hepb_sample_stored_duration').val('');
				}
			});

			$("#svr_is_sample_transported").change(function(){
				if($(this).val() == 1)
				{
					$(".hepc_sample_transported_field").show();
					$(".sample_accepted_fields").show();
					$('#svr_sample_transport_to').prop('required',true); 
					$('#svr_sample_transport_by_name').prop('required',true); 
					$('#svr_sample_transport_by_designation').prop('required',true);

					$('#svr_sample_receipt_date').prop('required',true); 
					$('#svr_sample_receieved_by_name').prop('required',true); 
					$('#svr_sample_receieved_by_designation').prop('required',true); 
				}
				else
				{
					$(".hepc_sample_transported_field").hide();
					$(".sample_accepted_fields").hide();


					$('#svr_sample_transport_to').prop('required',false); 
					$('#svr_sample_transport_by_name').prop('required',false); 
					$('#svr_sample_transport_by_designation').prop('required',false);
					 
					$('#svr_sample_receipt_date').prop('required',false); 
					$('#svr_sample_receieved_by_name').prop('required',false); 
					$('#svr_sample_receieved_by_designation').prop('required',false); 
					$('#svr_sample_receipt_date').val('');
					$('#svr_sample_receieved_by_name').val('');
					$('#svr_sample_receieved_by_designation').val('');

					$('#svr_sample_transport_temp').val('');
					$('#svr_sample_transport_date').val('');
					$('#svr_sample_transport_to').val('');
					$('#svr_sample_transport_by_name').val('');
					$('#svr_sample_transport_by_designation').val('');
					//$('#svr_sample_receieved_by_designation').val('');



				}
			});

		});
	</script>
<?php } else { ?>
	<script>
		$(document).ready(function(){
			$('input').prop('disabled', true);
			$('select').prop('disabled', true);
			$('textarea').prop('disabled', true);
		});


	</script>
	<?php } ?>