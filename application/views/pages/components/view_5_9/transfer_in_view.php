<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}
	
textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #A30A0C;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

/*#table_patient_list tbody tr:hover
{
	cursor: pointer;
}*/

.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #A30A0C !important;
	background-color: white !important;
	border : 1px solid #A30A0C;
}
.auto {cursor: auto;}
.fa-circle {
  color: white;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
</style>

<style>
    
    #dialog-confirm{
        display: none;
    }
    .ui-widget-overlay {
    background: #868686;
    opacity: .4;
    filter: Alpha(Opacity=50);
}
.ui-dialog .ui-dialog-buttonpane button {
    padding: 10px;
}
.ui-dialog .ui-dialog-titlebar-close {
   display: none;
}

.ui-widget-header {
border: 1px solid #e78f08;
background: #b32a2c url(images/ui-bg_gloss-wave_35_f6a828_500x100.png) 50% 50% repeat-x !important;
color: #fff;
font-weight: bold;
}
</style>

 <div id="dialog-confirm"  title="Patient transfer-in requests">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span> Are you sure you want to accept?</p>
</div>
<br/>
<br/>
<br/>
<?php $loginData = $this->session->userdata('loginData'); 
$visit = $this->uri->segment(2);
?>


<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="btn-group">


						<a class="btn btn-success" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/transferin/">Patient transfer-in requests</a>
						<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/transfer_out">Patient transfer-out requests</a>

</div>
</div>
</div>						
<!-- <form method="POST" name="search_form"> -->
				<?php
           $attributes = array(
              'name' => 'search_form',
               'autocomplete' => 'off',
            );
           echo form_open('',$attributes); ?>
<div class="row">
           <div class="col-md-10 col-md-offset-1">
		<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 6px; padding-bottom: 6px;">Patient transfer-in requests
		</h3>
	</div>
</div>

<div class="row">

	

	<div class="col-md-3 col-md-offset-1">
		
		<label for="">Search By</label>
		<select type="text" name="search_by" id="search_by" class="form-control input_fields" required>
			<option value="">Select</option>
			<option value="1" <?php if($this->input->post('search_by')==1) { echo 'selected';} ?>>UID/Contact No.</option>
			<option value="2" <?php if($this->input->post('search_by')==2) { echo 'selected';} ?>>Action</option>
			<option value="3" <?php if($this->input->post('search_by')==3) { echo 'selected';} ?>>Name</option>
		</select>
	</div>
	<div class="col-md-4 uid_field" style="display: none;">
		
		<label for="uid">UID/Contact No.</label>
		<input type="text" class="form-control input_fields" name="uid_contact" id="uid_contact" value="<?php if(isset($_POST['uid_contact'])) { echo $_POST['uid_contact'];} ?>" >
	</div>

	<div class="col-md-4 patname_field" style="display: none;">
		
		<label for="uid">Patient Name</label>
		<input type="text" class="form-control messagedata input_fields" name="pat_name" id="pat_name" maxlength="50" value="<?php if(isset($_POST['pat_name'])) { echo $_POST['pat_name'];} ?>"  >
	</div>

	<div class="col-md-4 status_field" style="display: none;">
		
		<label for="uid">Status</label>
		<select type="text" name="search_status" id="search_status" class="form-control input_fields" style="font-size: 14px !important;">
			<option value="0" <?php if($this->input->post('search_status')==0) { echo 'selected';} ?>>All</option>
			<option value="1" <?php if($this->input->post('search_status')==1) { echo 'selected';} ?>>Pending</option>
			<option value="2" <?php if($this->input->post('search_status')==2) { echo 'selected';} ?>>Accepted</option>
			
		</select>
	</div>
	<div class="col-md-1" style="padding-left: 0;">
		
		<label for="">&nbsp;</label>
		<button class="btn btn-block btn-success text-center" style="font-weight: 600; line-height: inherit; border-radius: 0;">SEARCH</button>
	</div>
</div>
		          <?php echo form_close(); ?>
<br/>
	<div class="row">
<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){  ?>
			

			<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				
				<?php } ?>
				</div>

<div class="row">
	<div class="col-md-10 col-md-offset-1">
		
		 <div class="text-right"><?php if(isset($offsetdata)) { echo ($offsetdata+1).'-'. (count($patient_list)+$offsetdata); } else { echo '0';} ?> <label for="total"> Of  <?php  if (isset($patient_listCount) ) echo ($patient_listCount); ?> records.</label></div>
		<table class="table table-striped table-bordered table-hover" id="table_patient_list" style="width:100%">

			
			<thead>
				<tr>
					<th>UID</th>
					<th>Name</th>
					<th>State</th>
					<th>District</th>
					<th>Facility</th>
					<th>Date(DD-MM-YY)</th>
					<th>Status HCV</th>
					<th>Status HBV</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(count($patient_list) > 0)
				{
					//echo "<pre>";
					//pr($patient_list);exit;

					foreach ($patient_list as $patient) {
						?>
						<tr>
							<td><?php //echo str_pad($patient->UID_Num, 6, '0', STR_PAD_LEFT);  
							echo $patient->UID_Prefix.'-'.str_pad($patient->UID_Num, 6, '0', STR_PAD_LEFT); ?></td>
							<td style="width: 20%;overflow: hidden;"><?php echo ($patient->FirstName); ?></td>
							
							<td><?php echo ucwords($patient->StateName); ?></td>
							<td><?php echo ucwords($patient->DistrictName); ?></td>
							<td><?php echo ucwords($patient->facility_short_name); ?></td>
							<td style="width: 8%;overflow: hidden;"><?php echo timeStampShow($patient->TransferRequestDatetime); ?></td>
							 <td><?php echo ucwords($patient->status); ?></td>
							 <td><?php echo ($patient->status_hbv); ?></td>
							<?php if($patient->id_mstfacility!=$patient->TransferToFacility){ ?>
							<td style="width: 6px;" align="center"><a onclick='Acceptpatdelete(<?php echo '"'.$patient->PatientGUID.'"'; ?>);' href="#<?php //echo base_url().'patientinfo/patienttransfer_accept/'.$patient->PatientGUID; ?>" class="Acceptpat" id="<?php echo $patient->PatientGUID; ?>">Accept</a></td>
							<?php } else{?>
							<td style="width: 6px;" align="center" style="color: #4d794e">Accepted</td>	
							<?php } ?>
							

							
						</tr>
						<?php 
					}
				}

				else
				{
					?>
					<?php 	if($this->input->post('search_by')==1) { ?>
					<tr>
						<td class="text-center auto" colspan=9 >No Patients in this UID/Contact No.</td>
					</tr>
				<?php } elseif($this->input->post('search_by')==2){ ?>
					<tr>
						<td class="text-center auto" colspan=9>No Patients in this Status.</td>
					</tr>
				<?php }else{ ?>
						<tr>
						<td class="text-center auto" colspan=9>No patients have been transferred to this facility.</td>
					</tr>
					<?php 
				} }
				?>
			</tbody>
		</table>

<div class="text-left">
Showing <?php if (isset($patient_list) && is_array($patient_list)) echo count($patient_list); ?> results
</div> 

<div class="text-right">
<?php if (isset($patient_list) && count($patient_list) >0 && $patient_list!='' && is_array($patient_list)) echo $page_links; ?>  
</div> 

		
	</div>
</div>
<br/><br/>



<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>


<script type="text/javascript" src="<?php echo site_url('application');?>/third_party/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('application');?>/third_party/js/jquery.dataTables.min.js"></script>



<script>


	/*$('.Acceptpat').click(function(){
      

   var txt;
  var r = confirm("Are you sure you want to accept?");
  if (r == true) {
    txt = "You pressed OK!";
  } else {
    txt = "You pressed Cancel!";
    return false;
  }

    }); */



function Acceptpatdelete(patientguid){
									
   		 $( "#dialog-confirm" ).dialog({
   		open : function() {
	    $("#dialog-confirm").closest("div[role='dialog']").css({top:100,left:'40%'});
		}, 	
      resizable: false,
      height: "auto",
      width: 400,
      modal: true,
      buttons: {
        "Yes": function() {
          window.location.replace('<?php echo base_url(); ?>patientinfo/patienttransfer_accept/'+patientguid);

        },
        "Cancel request": function() {
          $( this ).dialog( "close" );
          e.preventDefault();
        }
      }
 			 });
}			



$(document).ready(function(){

		$(".uid_field").hide();
		$(".status_field").hide();
		$(".patname_field").hide();
		$("#search_status").val('1');

		/*$('#table_patient_list tbody tr').click(function(){
			window.location = $(this).data('href');
		});*/

	<?php 	if($this->input->post('search_by')==1) { ?>
			$(".uid_field").show();
				$(".status_field").hide();
				$(".patname_field").hide();
				$("#search_by").prop('required', true);
				$("#uid_contact").prop('required', true);
				$("#search_status").prop('required', false);
				$("#pat_name").prop('required', false)
				$("#search_status").val('');

		<?php	}elseif($this->input->post('search_by')==2){ ?>

				$(".uid_field").hide();
				$(".status_field").show();
				$(".patname_field").hide();
				$("#search_by").prop('required', false);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', true);
				$("#pat_name").prop('required', false)
				$("#uid_contact").val('');
		<?php	}elseif($this->input->post('search_by')==3){ ?>
				//alert('eeeeee');
				$(".uid_field").hide();
				$(".status_field").hide();
				$(".patname_field").show();
				$("#pat_name").prop('required', true)
				$("#search_by").prop('required', false);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', false);
				$("#uid_contact").val('');
		<?php	}else {  ?>


				$(".uid_field").hide();
				$(".status_field").hide();
				$("#search_by").prop('required', true);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', false);
				$("#pat_name").prop('required', false)

		<?php } ?>

		$("#search_by").change(function(){

			if($(this).val() == 1)
			{
				$(".uid_field").show();
				$(".status_field").hide();
				$(".patname_field").hide();
				$("#search_by").prop('required', true);
				$("#uid_contact").prop('required', true);
				$("#search_status").prop('required', false);
				$("#pat_name").prop('required', false)
				$("#search_status").val('');
				$("#pat_name").val('');
			}
			else if($(this).val() == 2)
			{
				$(".uid_field").hide();
				$(".status_field").show();
				$(".patname_field").hide();
				$("#search_by").prop('required', false);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', true);
				$("#pat_name").prop('required', false)
				$("#uid_contact").val('');
				$("#pat_name").val('');
			}
			else if($(this).val() == 3)
			{
				$(".uid_field").hide();
				$(".status_field").hide();
				$(".patname_field").show();
				$("#pat_name").prop('required', true)
				$("#search_by").prop('required', false);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', false);
				$("#uid_contact").val('');
				$("#search_status").val('1');
			}
			else
			{
				$(".uid_field").hide();
				$(".status_field").hide();
				$("#search_by").prop('required', true);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', false);
				$("#pat_name").prop('required', false);

			}
		});
	});



</script>