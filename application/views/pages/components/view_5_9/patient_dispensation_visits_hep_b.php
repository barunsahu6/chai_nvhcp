<?php 
$loginData = $this->session->userdata('loginData');

$sql = "SELECT Dispense FROM `MSTRole` where RoleId = ".$loginData->RoleId;
$result = $this->db->query($sql)->result();
 //$visit = $this->uri->segment(4);
$visit_countval = count($visit_count);
$duration = $patient_data[0]->T_DurationValue/4;


$visit;
//echo $countdurationval= $visit_countval;
?>
<style>
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}

	.input_fields
	{
		height: 30px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	textarea
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}

	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}

	.form_buttons:focus
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}

	@media (min-width: 768px) {
		.row.equal {
			display: flex;
			flex-wrap: wrap;
		}
	}

	@media (max-width: 768px) {

		.input_fields
		{
			height: 40px !important;
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
		}

		.form_buttons
		{
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
			font-weight: 600 !important;
			padding: 8px 12px !important;
			border: 2px solid #A30A0C !important;

		}
		.form_buttons:hover
		{
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
			font-weight: 600 !important;
			padding: 8px 12px !important;
			border: 2px solid #A30A0C !important;
			background-color: #FFF;
			color: #A30A0C;

		}
	}

	.btn-default {
		color: #333 !important;
		background-color: #fff !important;
		border-color: #ccc !important;
	}
	.btn {
		display: inline-block;
		padding: 6px 12px;
		margin-bottom: 0;
		font-size: 14px;
		font-weight: 400;
		line-height: 2.4;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		-ms-touch-action: manipulation;
		touch-action: manipulation;
		cursor: pointer;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		background-image: none;
		border: 1px solid transparent;
		border-radius: 4px;
	}

	a.btn
	{
		text-decoration: none;
		color : #000;
		background-color: #A30A0C;
	}

	.btn-group .btn:hover
	{
		text-decoration: none !important;
		color: #000 !important;
		background-color: #CCC !important;
	}

	.btn-group .btn:hover
	{
		text-decoration: none !important;
		color: #000 !important;
		background-color: inherit !important;
	}

	a.active
	{
		color : #FFF !important;
		text-decoration: none;
		background-color: inherit !important;
	}

	#table_patient_list tbody tr:hover
	{
		cursor: pointer;
	}

	.btn-success
	{
		background-color: #A30A0C;
		color: #FFF !important;
		border : 1px solid #A30A0C;
	}

	.btn-success:hover
	{
		text-decoration: none !important;
		color: #A30A0C !important;
		background-color: white !important;
		border : 1px solid #A30A0C;
	}
	select[readonly] {
		background: #eee;
		pointer-events: none;
		touch-action: none;
	}
	.hasDatepicker[readonly] {
		background: #eee;
		pointer-events: none;
		touch-action: none;
	}
</style>

<br>
<div class="row equal">	
	<?php
	$attributes = array(
		'id' => 'patient_form',
		'name' => 'patient_form',
		'autocomplete' => 'off',
	);
	echo form_open('', $attributes); ?>
	<input type="hidden" name="fetch_uid" id="fetch_uid">

	<?php echo form_close(); ?>
	<!-- <input type="hidden" name="fetch_uid" id="fetch_uid"> -->

	<div class="col-lg-10 col-lg-offset-1">

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="btn-group">
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">1. Registration</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">2. Screening</a>

					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">3. Testing</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">4. HBV DNA</a>


					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/known_history/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">5. Known History</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">6. Prescription</a>
					<a class="btn btn-success" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">7. Dispensation</a>

				</div>
			</div>
		</div>

		<input type="hidden" name="patvisit" id="patvisit" value="<?php echo $visit; ?>">
		<!-- <form action="" method="POST" name="registration" id="registration"> -->
			<?php
			$attributes = array(
				'id' => 'registration',
				'name' => 'registration',
				'autocomplete' => 'off',
			);
			echo form_open('', $attributes); ?>

			<div class="row">
				<div class="col-md-12">
					<h4 class="text-center"><p>Patient Name - <strong><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FirstName)):''; ?></strong>  (<?php echo (count($patient_data) > 0)?$patient_data[0]->UID_Prefix:$uid_prefix; echo '-' .str_pad($patient_data[0]->UID_Num, 6, '0', STR_PAD_LEFT); ?>)<p></h4>
						<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Patient Dispensation Module</h3>
					</div>
				</div>
				<br>

				<div class="row">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>S No</th>
								<th>Patient Name</th>
								<th>Treatment Initiation Date</th>
								<th>Dispensation Number</th>
								<th>Dispensation place</th>
								<th>Pills Taken</th>
								<th>Next visit Date</th>								
							</tr>
						</thead>
						<tbody>
							<?php 
							$counter = 1; 
							if(count($tblpatientdispensationb_list) > 0){
							foreach($tblpatientdispensationb_list as $row){?>
								<tr>
								<td><?=$counter?></td>
								<td><?=$row->PatientGUID?></td>
								<td><?=$row->Treatment_Dt?></td>
								<td><?=$row->VisitNo?></td>
								<td><?=$row->DispensationPlace?></td>
								<td><?=$row->PillsTaken?></td>
								<td><?=$row->NextVisit?></td>
								
							</tr>
								<?php } }else{?>
									<tr><td colspan="7" class="text-center">No Data Found</tr>
									<?php } ?>
						</tbody>
					</table>

				</div>

				<hr style="border-top: 2px solid #000; margin-left: 30px; margin-right: 30px;">
				<br>

				<div class="row">					
					<div class="col-md-3">
						<label for="">Dispensation number <span class="text-danger">*</span></label>
						<input type="number" required name="VisitNo" id="VisitNo" class="input_fields form-control input2" value="<?php echo (isset($VisitNo))?$VisitNo:''; ?>">
						<br class="hidden-lg-*">
					</div>

					<div class="col-md-3">
						<label for="">Date Of Treatment Initiation <span class="text-danger">*</span></label>
						<input type="text" name="Treatment_Dt" id="Treatment_Dt" class="input_fields form-control hasCal dateInpt input2" required="" placeholder="dd-mm-yy">
						<br class="hidden-lg-*">
					</div>

				</div>
				<div class="row">
					<div class="col-md-3" >
						<label for="">Drug </label>
						<select class="form-control input_fields" id="RegimenPrescribed" name="RegimenPrescribed" required>
							<option value="">Select</option>
							<?php foreach($drug_list as $row){ ?>
								<option value="<?=$row->LookupCode?>" <?php echo (count($patient_data) > 0 && $patient_data[0]->Drug == $row->LookupCode)?'selected':''; ?>><?=$row->LookupValue?></option>
							<?php } ?>								
						</select>
					</div>
				
					<div class="col-md-3">
						<label for="">Place Of Dispensation</label>
						<select class="form-control input_fields" id="DispensationPlace" name="DispensationPlace">
							<option value="">Select</option>
							<?php error_reporting(0); if(count($patient_data) > 0 && !empty($patient_data[0]->ReferTo ) ) { ?>
								<option value="<?php  echo $ReferToFaci[0]->id_mstfacility; ?>" <?php if($patient_data[0]->PrescribingFacility == $ReferToFaci[0]->id_mstfacility){ echo "selected"; }  ?>><?php echo $ReferToFaci[0]->FacilityCode; ?></option>

							<?php } ?>
							<?php foreach ($place_of_dispensation as $row) { ?>
								<option  value="<?php echo $row->id_mstfacility; ?>"
									<?php 
									if($patient_data[0]->PrescribingFacility == null && $row->id_mstfacility == $loginData->id_mstfacility)
										echo "selected"; 
									else if ($patient_data[0]->PrescribingFacility != null && $patient_data[0]->PrescribingFacility == $row->id_mstfacility)
										echo "selected"; 
									else
										echo '';
									?>
									><?php echo $row->FacilityCode; ?></option>

								<?php } ?>
							</select>
						</div>	

						<div class="col-md-3">
							<label for="">Days Of Pills Dispensed <span class="text-danger">*</span></label>
							<input type="number" required name="PillsDaysDispensed" id="PillsDaysDispensed" class="input_fields form-control input2" value="28" onkeydown="return false" onkeyup="return false" readonly>
							<br class="hidden-lg-*">
						</div>					
					</div>
					<br>
					<div class="row">
						<div class="col-md-3 sofosbuvir_field" >
							<label for="">Pills To Be Taken As <span class="text-danger">*</span></label>
							<select class="form-control input_fields" id="PillsTaken" name="PillsTaken" required readonly>
								<option value="">Select</option>
								<?php foreach($pills_to_be_taken_list as $row){ ?>
									<option value="<?=$row->LookupCode?>" <?php echo (count($patient_data) > 0 && $patient_data[0]->PillsToBeTaken == $row->LookupCode)?'selected':''; ?>><?=$row->LookupValue?></option>
								<?php } ?>	
							</select>
						</div>	
					</div>
					<br>
					<div class="row pillsDiv" style="display: none;">

						<div class="col-md-3" >
							<label for="">Pills To Be Taken Daily <span class="text-danger">*</span></label>
							<select class="form-control input_fields" id="PillsTakenDaily" name="PillsTakenDaily" required>
								<option value="">Select</option>
								<option value="1">1</option>
								<option value="2">2</option>
							</select>
						</div>	

						<div class="col-md-3">
							<label for="">Pills To Be Dispensed <span class="text-danger">*</span></label>
							<input type="number" required="" name="PillsDispensed" id="PillsDispensed" class="input_fields form-control input2" value="<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->PrescribingDate):''; ?>" onkeydown="return false" onkeyup="return false" readonly>
							<br class="hidden-lg-*">
						</div>	


					</div>
					<br>
					<div class="row solutionDiv" style="display: none;">					

						<div class="col-md-3" >
							<label for="">Solution To Be Taken Daily <span class="text-danger">*</span></label>
							<select class="form-control input_fields" id="SolutionTakenDaily" name="SolutionTakenDaily" required>
								<option >Select</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>								
							</select>
						</div>	

						<div class="col-md-3">
							<label for="">Solutions Needed Daily <span class="text-danger">*</span></label>
							<input type="number" required name="PillsNeededDaily" id="PillsNeededDaily" class="input_fields form-control input2" value="<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->PrescribingDate):''; ?>" onkeydown="return false" onkeyup="return false" readonly>
							<br class="hidden-lg-*">
						</div>	

						<div class="col-md-3">
							<label for="">Solutions To Be Dispensed <span class="text-danger">*</span></label>
							<input type="number" required name="PillstoBeDispensed" id="PillstoBeDispensed" class="input_fields form-control input2" value="<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->PrescribingDate):''; ?>" onkeydown="return false" onkeyup="return false" readonly>
							<br class="hidden-lg-*">
						</div>

					</div>
					<br>
					<div class="row">						

						<div class="col-md-3">
							<label for="">Advised Next Visit Date <span class="text-danger">*</span></label>
							<input type="text" required="" name="NextVisit" id="NextVisit" class="input_fields form-control hasCal dateInpt input2" value="<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->PrescribingDate):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
							<br class="hidden-lg-*">
						</div>
					</div>
					<br>
					<input type="hidden" name="interruption_status" id="interruption_status" value="<?php  echo (count($patient_data) > 0)?$patient_data[0]->InterruptReason:'';  ?>">
					<div class="row">
						<div class="col-lg-3 col-md-2">
							<a class="btn btn-block btn-default form_buttons" href="<?php echo base_url('patientinfo_hep_b?p=1'); ?>" id="close" name="close" value="close">CLOSE</a>
						</div>
						<div class="col-lg-3 col-md-2">
							<button class="btn btn-block btn-default form_buttons" id="refresh" name="refresh" value="refresh">REFRESH</button>
						</div>
						<?php if($loginData->RoleId!=99){ ?>
							<div class="col-lg-3 col-md-2">
								<a href="" class="btn btn-block btn-default form_buttons" id="lock" name="lock" value="lock">LOCK</a>
							</div>
						<?php } else{?>
							<div class="col-lg-3 col-md-2">
								<a href="javascript:void(0)" class="btn btn-block btn-default form_buttons" onclick="openPopupUnlock()" id="" name="unlock" value="lock">LOCK</a>
							</div>
						<?php } ?>
						<?php if($result[0]->Dispense == 1) {?>
							<div class="col-lg-3 col-md-2">
								<button class="btn btn-block btn-success form_buttons" id="save" name="save" value="save">SAVE</button>
							</div>
						<?php } ?>
					</div>
					<input type="hidden" class="hasCal2 dateInpt input2" name="NextVisit_Dtn" id="NextVisit_Dtn" value="<?php echo timeStampShow($visit_detailsdate[0]->NextVisit_Dt); ?>">
					<input type="hidden" name="Next_Visitdtn" class="hasCal2 dateInpt input2"  id="Next_Visitdtn" value="<?php echo timeStampShow($patient_data[0]->Next_Visitdt); ?>">

					<br><br><br>
					<?php echo form_close(); ?>
					<div class="row" class="text-left">

						<div class="col-md-6 card well">
							<label class="col-sm-4"  style="text-align: left !important; line-height: 2.2 !important; ">Patient's Status -</label>
							<div class="col-sm-8" style="text-align: left !important; line-height: 2.2 !important; ">
								<label><?php echo (count($patient_status) > 0)?$patient_status[0]->status:''; ?></label>

							</div>
						</div>

						<div class="col-md-6 card well">
							<label class="col-sm-6" style="text-align: left !important; ">Patient's Interruption Status </label>
							<div class="col-sm-6">
								<select name="" id="type_val" onchange="openPopup()" class="btn" style="text-align: right!important;">
									<option value="">Select</option>
									<option value="1"  <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason != '')?'selected':''; ?> >Yes</option>
									<option value="0" <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason == '')?'selected':''; ?>>No</option>
								</select>

							</div>
						</div>

					</div>

					<!-- </form> -->
				</div>
			</div>


		</div>

		<br/><br/><br/>

		<br/><br/><br/>
		<!-- end unlock -->
		<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
		<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>

		<script>
			$(function(){
				var PillsTaken = $('#PillsTaken').val();
				if(PillsTaken == 1){	
					$(".solutionDiv").css("display", "none");	
					$(".pillsDiv").css("display", "block");		
					$('#SolutionTakenDaily').attr("disabled", true);
					$('#PillsTakenDaily').attr("disabled", false); 

					//calculation

					$('#PillsTakenDaily').change(function(){
						var PillsTakenDaily = $(this).val();
						var PillsDaysDispensed = $('#PillsDaysDispensed').val();
						var res = PillsDaysDispensed * PillsTakenDaily;
						$('#PillsDispensed').val(res);
					});
				}else{
					$(".pillsDiv").css("display", "none");
					$(".solutionDiv").css("display", "block");	
					$('#PillsTakenDaily').attr("disabled", true);
					$('#SolutionTakenDaily').attr("disabled", false); 

					$('#PillsTakenDaily').val();
					$('#PillsDispensed').val();

					//calculation

					$('#SolutionTakenDaily').change(function(){
						var PillsDaysDispensed = $('#PillsDaysDispensed').val();
						var SolutionTakenDaily = $(this).val();
						var res = (0.5/10)*SolutionTakenDaily/0.5;
						$('#PillsNeededDaily').val(res.toFixed(2));
						var solutionDispensed = PillsDaysDispensed*res;
						$('#PillstoBeDispensed').val(solutionDispensed.toFixed(2));
					});
				}
			});

			$('#PillsTaken').change(function(){
				var PillsTaken = $(this).val();
				
				if(PillsTaken == 1){	
				$(".solutionDiv").css("display", "none");	
					$(".pillsDiv").css("display", "block");			
					$('#SolutionTakenDaily').attr("disabled", true);
					$('#PillsTakenDaily').attr("disabled", false); 

					//calculation

					$('#PillsTakenDaily').change(function(){
						var PillsTakenDaily = $(this).val();
						var PillsDaysDispensed = $('#PillsDaysDispensed').val();
						var res = PillsDaysDispensed * PillsTakenDaily;
						$('#PillsDispensed').val(res);
					});
				}else{
					$(".pillsDiv").css("display", "none");
					$(".solutionDiv").css("display", "block");	
					$('#PillsTakenDaily').attr("disabled", true);
					$('#SolutionTakenDaily').attr("disabled", false); 

					$('#PillsTakenDaily').val();
					$('#PillsDispensed').val();

					//calculation

					$('#SolutionTakenDaily').change(function(){
						var PillsDaysDispensed = $('#PillsDaysDispensed').val();
						var SolutionTakenDaily = $(this).val();
						var res = (0.5/10)*SolutionTakenDaily/0.5;
						$('#PillsNeededDaily').val(res.toFixed(2));
						var solutionDispensed = PillsDaysDispensed*res;
						$('#PillstoBeDispensed').val(solutionDispensed.toFixed(2));
					});
				}
			});


			$("#visit_date" ).change(function( event ) {

				var date_of_prescribing_testsdate = '<?php echo $patient_data[0]->Next_Visitdt; ?>';
				var visit_date = $("#visit_date" ).val();

				if(date_of_prescribing_testsdate > visit_date ){

					$("#modal_header").text("Visit Date  greater than or equal to must be "+date_of_prescribing_testsdate);
					$("#modal_text").text("Please check dates");
					$("#visit_date" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;

				}
			});



//$( "#advised_visit_date" ).datepicker( "option", "disabled", true );
//$('#advised_visit_date').datepicker({minDate:-1,maxDate:-2}).attr('readonly','readonly'); 
$('.hasCal2').each(function() {
	ddate=$(this).val();
	yRange="1990:"+new Date().getFullYear();
	yr=$(this).attr('year-range');
	if(yr){
		yRange=yr;
	}
	
	$(this).datepicker({
		dateFormat: "dd-mm-yy",
		changeYear: true,
		changeMonth: true,
		yearRange: yRange,
           // maxDate: 0,
       });
	$(this).attr('placeholder','dd-mm-yy');
	if($(this).attr('noweekend')){
		$(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
	}
	$(this).datepicker( "setDate", ddate);
}); 



$('#btnCloseIt').click(function(){

	$('#type_val').val('0');
	$('#interruption_status').val('');

});

$('#type_val').change(function(){
	var type_val = $('#type_val').val();
	$('#interruption_status').val(type_val);

});


$('#btnSaveIt').click(function(e){

	e.preventDefault(); 
	$("#form-error").html('');

	var patvisit = $('#patvisit').val(); 
	var formData = new FormData($('#newModalForm')[0]);
	$('#btnSaveIt').prop('disabled',true);
	$.ajax({				    	
		url: '<?php echo base_url(); ?>patientinfo_hep_b/interruptionstatus_process/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
		type: 'POST',
		data: formData,
		dataType: 'json',
		async: false,
		cache: false,
		contentType: false,
		processData: false,
		success: function (data) { 

			if(data['status'] == 'true'){
				$("#form-error").html('Data has been successfully submitted');
				// redirectstatus
				if(data['interruptstage'] == 2)
				{

					alert('Patient interruption status has been successfully updated.Please update the SVR page when the patient has conducted the SVR test.');
					location.href='<?php echo base_url(); ?>patientinfo_hep_b/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
				}
				else if(data['interruptstage'] == 3)
				{
					alert('Patient interruption status has been successfully updated. SVR has not been recommended.');
					visit = 'eot';
					location.href='<?php echo base_url(); ?>patientinfo_hep_b/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/'+patvisit;
    					//location.href='<?php echo base_url(); ?>patientinfo_hep_b/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>eot';
    				}else{
    					setTimeout(function() {
    						location.href='<?php echo base_url(); ?>patientinfo_hep_b/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/'+patvisit;
    					}, 1000);
    				}

    			}else{
    				$("#form-error").html(data['message']);
    				$('#btnSaveIt').prop('disabled',false); 
    				return false;
    			}   
    		}        
    	}); 



}); 

</script>

<?php if(count($patient_data) > 0 && $visit_countval >= $visit || $patient_data[0]->NextVisitPurpose >98) {?>
	<script>

		$("#lock").click(function(){
			$("#modal_header").text("Contact admin ,for unlocking the record");
			$("#modal_text").text("Contact Admin");
			$("#multipurpose_modal").modal("show");
			return false;

		});
		
				/*Disable all input type="text" box*/
				//alert('<?php echo $patient_data[0]->MF5; ?>');
				$('#registration input[type="text"]').attr("readonly", true);
				$('#registration input[type="checkbox"]').attr("disabled", true);
				$('#registration input[type="date"]').attr("readonly", true);
				$("#save").attr("disabled", true);
				$("#refresh").attr("disabled", true);
				$('#registration select').attr('disabled', true);
				/*Disable textarea using id */
				$('#registration #txtAddress').prop("readonly", true);
				
			</script>
		<?php  } ?>


		<script>
			function onlyNumbersWithDot(e) {           
				var charCode;
				if (e.keyCode > 0) {
					charCode = e.which || e.keyCode;
				}
				else if (typeof (e.charCode) != "undefined") {
					charCode = e.which || e.keyCode;
				}
				if (charCode == 46)
					return true
				if (charCode > 31 && (charCode < 48 || charCode > 57))
					return false;
				return true;
			}
		</script>

		<script>


			$("#visit_date" ).change(function( event ) {

			// for 7 greater days
			<?php if($visit!=2){ ?>
				var date =$("#NextVisit_Dtn" ).datepicker('getDate'); 
			<?php } else{?>
				var date = $("#Next_Visitdtn" ).datepicker('getDate');
			//alert(date);
			<?php } ?>
			var visit_date = $("#visit_date" ).datepicker('getDate');
			//alert(visit_date+'/'+date);

			var remaningdays = 6;
			days = parseInt(remaningdays, 10);
			if(!isNaN(date.getTime())){
				date.setDate(date.getDate() + days);
			}

			// for 7 less days
			<?php if($visit!=2){ ?>

				var date1 = $("#NextVisit_Dtn" ).datepicker('getDate'); 
			<?php } else{?>
				var date1 = $("#Next_Visitdtn" ).datepicker('getDate');
			<?php }?>

			days1 = parseInt(remaningdays, 10);
			if(!isNaN(date1.getTime())){
				date1.setDate(date1.getDate());
			}
			<?php if($visit!=2){ ?>
				var d = $("#NextVisit_Dtn" ).val(); 
			<?php } else { ?>
				var d = $("#Next_Visitdtn" ).val(); 
			<?php } ?>

			if(date < visit_date || date1 > visit_date){

				$("#modal_header").text("Visit Date should be within 6 days from "+d);
				$("#modal_text").text("Please check dates");
				$("#visit_date" ).val('');
				$("#multipurpose_modal").modal("show");
				return false;
				
			}

			});	



			function pills_givencal()
			{

				var date = $("#visit_date").datepicker('getDate'); 
			//alert(date);
			<?php if($visit!=2){ ?>
				var datedb = $("#NextVisit_Dtn" ).datepicker('getDate'); 
			<?php } else { ?>
				var datedb = $("#Next_Visitdtn" ).datepicker('getDate');
			<?php  } ?>

			var pills_left =0;
			var pills_given = parseInt($('#pills_given').val());
			// alert(pills_given);
			var pills_left = $('#pills_left').val();
			if(pills_left == ''){
				var pills_left = 0;
			}else{
				var pills_left = parseInt(pills_left);
			}

			var pills_given1 = parseInt($('#pills_given').val());
			var pills_left1 = parseInt($('#pills_left').val());

			if(pills_left1 > pills_given1){

				$("#modal_header").text("Pills Left not greater then Pills Given");
				$("#modal_text").text("haemoglobin greater than zero");
				$('#pills_left').val('');
				$("#multipurpose_modal").modal("show");
				return false;
			}

			var minutes = 1000*60;
			var hours = minutes*60;
			var days = hours*24;

			var diff_date = Math.round((date - datedb)/days);
			

			var remaningpills = parseInt(pills_given)+ parseInt(pills_left);
			// alert(remaningpills);
			//var remaningpillsval = (remaningpills-3);
			var remaningpillsval = (remaningpills-0);
			days1 = parseInt(remaningpillsval, 10);
			if(diff_date>1){


				var pills_left1 = 31-(diff_date)-pills_left;
				
			}
			else{
				if(diff_date>0){
					var diff_date1 = diff_date-pills_left;
				}else{
					var diff_date1 = 0;
				}
				var pills_left1 = 31-pills_left-diff_date;
				
			}

			var remaningpills = parseInt(pills_given) + parseInt(pills_left);
			//var remaningpillsval = (remaningpills - 3);
			//var remaningpillsval = (remaningpills-3);
			var remaningpillsval = (remaningpills-3);
			//alert(remaningpillsval);
			var Adherence = ( ( pills_left1 ) * 100) / 28;
			days = parseInt(remaningpillsval, 10);

			if(!isNaN(date.getTime())){
				date.setDate(date.getDate() + days);


				if(Adherence>100){
					var Adherence = 100
				}else{
					var Adherence = Adherence;
				}
				if(date!='' && $('#pills_left').val()!=''){

					$('#adherence').val(Adherence.toFixed(2));
				}
				$("#advised_visit_date").val(date.toInputFormat());
				$("#advised_visit_datesvr").val();


			}

			/*Advised SVR Date start*/
			var remaningpillsval =  84;
			days = parseInt(remaningpillsval, 10);

			if(!isNaN(date.getTime())){
				date.setDate(date.getDate() + days);

				$('#advised_visit_datesvr').val(date.toInputFormat());
			}
			/*Advised SVR Date ens*/



			if(Adherence==100){
				$('.adherence_field').hide();
				$('#reason_low_adherence').prop('required',false);

			}else{
				$('.adherence_field').show();
				$('#reason_low_adherence').prop('required',true);
			}
		}

		Date.prototype.toInputFormat = function() {
			var yyyy = this.getFullYear().toString();
	        var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
	        var dd  = this.getDate().toString();
	        return (dd[1]?dd:"0"+dd[0]) + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + yyyy; // padding
	    };
	</script>