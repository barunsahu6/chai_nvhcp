<style>
#map { height: 400px; margin: 0; padding: 0; overflow: hidden; }
  .nicebox {
    position: absolute;
    text-align: center;
    font-family: "Roboto", "Arial", sans-serif;
    font-size: 13px;
    z-index: 5;
    box-shadow: 0 4px 6px -4px #333;
    padding: 5px 10px;
    background: rgb(150, 150, 150);
    background: linear-gradient(to bottom,rgba(255,255,255,1) 0%,rgba(245,245,245,1) 100%);
    border: rgb(197, 197, 197) 1px solid;
  }
  #controls {
    top: 10px;
    left: 110px;
    width: 360px;
    height: 45px;
  }
  #data-box {
    top: 10px;
    left: 500px;
    height: 45px;
    line-height: 45px;
    display: none;
  }
  #census-variable {
    width: 360px;
    height: 20px;
  }
  #legend { display: flex; display: -webkit-box; padding-top: 7px }
  .color-key {
    background: linear-gradient(to right,
      hsl(5, 69%, 54%) 0%,
      hsl(29, 71%, 51%) 17%,
      hsl(54, 74%, 47%) 33%,
      hsl(78, 76%, 44%) 50%,
      hsl(102, 78%, 41%) 67%,
      hsl(127, 81%, 37%) 83%,
      hsl(151, 83%, 34%) 100%);
    flex: 1;
    -webkit-box-flex: 1;
    margin: 0 5px;
    text-align: left;
    font-size: 1.0em;
    line-height: 1.0em;
  }
  #data-value { font-size: 2.0em; font-weight: bold }
  #data-label { font-size: 2.0em; font-weight: normal; padding-right: 10px; }
  #data-label:after { content: ':' }
  #data-caret { margin-left: -5px; display: none; font-size: 14px; width: 14px}


	.filter-div{
	margin-right: 2rem;
	}
	.row,.left-div,.right-div{
		background-color: #ffffff !important;
	}
.div_collapse{background-color:#ffffff;}
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}
.chart-legend_flex li span{
    display: inline-block;
    width: 14px;
    height: 14px;
    margin-right: 5px;
    border-radius: 7px;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    -webkit-border-radius: 14px;
    -moz-border-radius: 14px;
}
.chart-legend li span{
    display: inline-block;
    width: 14px;
    height: 14px;
    margin-right: 5px;
    border-radius: 7px;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    -webkit-border-radius: 14px;
    -moz-border-radius: 14px;
}
.chart-legend_flex ul{
	display: flex;
}
ul {
  list-style: none;
}

ul li::before {
  display: none; 
}
.side_btn {
            float: right;
            width: auto;
            height: auto;
        }

.chk {
            display: block;
            float: right;
            width: 350px;
            height: auto;
            position: absolute;
            right: -10px;
            top: 35px;
            background-color: #fbfbfb;
            border: solid 1px LightGray;
            z-index: 1000;
        }

        .side_ul {
            float: left;
            height: auto;
            width: auto;
        }
.input_fields
{
	height: 30px !important;
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}
h4,h5,label,input,p,span,button{
	font-family: 'Source Sans Pro';
	color: #505050;
}
.panel-title{
	font-family: 'Source Sans Pro';
	font-weight: 400;
	letter-spacing: .5px;
	font-size: 24px;
}
.sub-heading{
	font-family: 'Source Sans Pro';
	font-weight: 600;
	font-size: 18px;
}
@media (min-width: 720px){
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
	.container,.innerpadding{
	width: 76vw;
}
.pd-lr{padding-left: 18px; padding-right: 18px;}

.state_div
{
	position: relative;margin: auto;height: 365px;width: 100%;
}
.risk_factor_div
{
	position: relative;margin: auto;height: 375px;width: 100%;
}
.cascade_div
{
	position: relative;margin: auto;height: 300px;width: 100%;
}
.ageChart_div{
	position: relative;margin: auto;height: 296px;width: 100%;
}
.ageChart_divreg{
display: block;
   height: 307px;
   width: 486px;
}
#ageChart{
	width: 100%;
}
.cascade_scroll-x,.state_div,.district_scroll_x{
	padding-right: 2px;
}
.left-div{
	padding-left: 0px;

}
.right-div{
	padding-right: 0px;
}
.pie_chart_div{
position: relative;margin: auto;height: 256px;width: 100%;margin-left: -55px;
}.pie_chart_div_1{
position: relative;margin: auto;height: 288px;width: 100%;margin-left: -55px;
}
.pie_chart_div_spc{
	   position: relative;
    margin: auto;
    height: 267px;
    width: 100%;
    margin-left: -16px;
}

/*.pie_chart_div_1{
	position: relative;margin: auto;height: 220px;width: 100%;margin-left: -55px;
}*/
<?php if (count($treatment_Initiations_by_District)<=37) {
	echo '.district_div{position: relative;margin: auto;height: 350px;width: 100%;}';
}else{echo '.district_div{position: relative;margin: auto;height: 350px;width: 180vw;} .district_scroll_x{overflow-x: scroll;}';} ?>
.mar-right{padding: 0px;margin-right: -40px;}
.mar-left{padding: 0px;margin-left: -120px;}
.ml-20{margin-left: -20px;}
.mr-30{margin-right: 30px;}
.pie-chart_legend_margin{
margin-left: 80px;margin-right: -21px;
}
.top-margin{margin-top: -50px;padding-bottom: 3px;}
#vl-legend{margin-top: 36px;padding-bottom: 0px;}
#vl-legend_div{margin-left: 37px;margin-right: -37px;}
#successratiosvrtest-legend{margin-top: 35px;padding-bottom: 0px;}
.Age_wise_risk-legend_div{margin-left: 5.0em;margin-right: -5.1em;margin-top: -76px;}
.panel-title {
    margin-top: 0;
    margin-bottom: 0;
    font-size: 18px;
    color: #0c0c0c;
}
}

@media (max-width: 720px) {
	.mr-30{margin-right: 0px;}
	.panel-title{
    margin-top: 0;
    margin-bottom: 0;
    font-size: 16px;
}
	.state_scroll-x,.cascade_scroll-x{
		overflow-x: scroll;
	}
	.ml-20{margin-left: -20px;}
	.left-div,.right-div{
		padding-right: 0px;
		padding-left: 0px;
	}
.state_div
{
	position: relative;margin: auto;height: 70vh;width: 180vw;
}
<?php if (count($treatment_Initiations_by_District)<=37 && count($treatment_Initiations_by_District)>5) {
	echo '.district_div{position: relative;margin: auto;height: 60vh;width: 120vw;} .district_scroll_x{overflow-x: scroll;}';
}if (count($treatment_Initiations_by_District)<=5 && count($treatment_Initiations_by_District)>=1) {
	echo '.district_div{position: relative;margin: auto;height: 60vh;width: 100%;';
}else{echo '.district_div{position: relative;margin: auto;height: 60vh;width: 180vw;}.district_scroll_x{overflow-x: scroll;}';} ?>
.cascade_div
{
	position: relative;margin: auto;height: 60vh;width: 140vw;
}
.ageChart_div{
	position: relative;margin: auto;height: 60vh;width: 100%;
}
.risk_factor_div
{
	position: relative;margin: auto;height: 60vh;width:140vw;
}
.mar-right{padding: 0px;margin-right: 0px;}
.mar-left{padding: 0px;margin-left: 0px;}
.pie_chart_div{
	position: relative;margin: auto;height: 35vh;width: 100vw;
}
.pie_chart_div_1{
position: relative;margin: auto;height: 36vh;width: 100vw;
}
.risk_factor_ml-25{margin-left: -40px;}
.pie-chart_legend_margin{
margin-left: 84px;margin-right: -90px;margin-top: 5px;margin-left: -40px;
}
.top-margin{margin-top: 5px;margin-left: 100px;margin-right: -104px;}
.Age_wise_risk-legend_div{margin-left: 5.0em;margin-right: -5.1em;margin-top: 0px;}
#diseasevlpositive,#successratiosvrtest{margin-left: -30px;margin-top: 10px;}
}

	.input_fields
	{
		height: 30px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-family: 'Source Sans Pro';
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn,.btn1{
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	font-family: 'Source Sans Pro';
	line-height: 2;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
	font-family: 'Source Sans Pro';
}
a.btn1
{
	text-decoration: none;
	color : #FFFF;
	background-color: #1cacd7;
	font-family: 'Source Sans Pro';
}
.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	font-family: 'Source Sans Pro';
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
	border-radius: 0;
}
.col-md-offset-3,.col-lg-offset-3{
	margin-left: 22%;
}
.btn-success:hover
{
	text-decoration: none !important;
	color: #A30A0C !important;
	background-color: white !important;
	border : 1px solid #A30A0C;
	border-radius: 0;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
.panel-default>.panel-heading {
    background-color: #595959;
    cursor: pointer;
}

/*.events{
margin: 10px 20px;
}*/
.backcolors{
	background: #f4fafe82;
}


</style>

<?php $loginData = $this->session->userdata('loginData');
$filters = $this->session->userdata('filters');
//echo "<pre>"; print_r($geographical_distribution);
//pr($age_genderd);
if($loginData->user_type==1) { 
$select = '';
}else{
$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
$select4 = '';	
}
if(($loginData->user_type==2 || $loginData->user_type==3 || $loginData->user_type==1)&&($filters['id_mstfacility']!=='' || $filters['id_mstfacility']!==0)){ 
	if(!(isset($mtc_user[0]) ? $mtc_user[0]->is_Mtc==1 : '')  ){
$coloffset="col-lg-7 col-md-7 col-xs-12 col-sm-12 col-md-offset-3 col-lg-offset-3 col-sm-offset-0 col-xs-offset-0";
$coloffset_addhernce_per="col-lg-6 col-md-6 col-xs-12 col-sm-12 col-md-offset-3 col-lg-offset-3 col-sm-offset-0 col-xs-offset-0";
	}
	else{
		$coloffset_addhernce_per="col-lg-7 col-md-7 col-xs-12 col-sm-12 col-md-offset-3 col-lg-offset-3 col-sm-offset-0 col-xs-offset-0";
	$coloffset="col-lg-6 col-md-6 col-xs-12 col-sm-12";
}}
if(($loginData->user_type==1 || $loginData->user_type==3)&&($filters['id_mstfacility']=='' || $filters['id_mstfacility']==0)){
	$coloffset_addhernce_per="col-lg-6 col-md-6 col-xs-12 col-sm-12";

}


 ?>
<?php //pr($adherenceper); echo 'fff'.$adherenceper['COUNT']['4']->COUNT;exit(); ?>
<div class="container">
  <div class="row" style="padding: 0px;">
  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 left-div">

  <div class="card well events" style="background-color: #fff;border-bottom-width:5px;border-bottom-color:#35c4f9; text-align:center;">
    <div class="text-center " style="min-height: 53px;">
     <h3 style="margin: 5px;color: #000000;font-size:36px;font-weight: 500" class="count"><?php echo $cascade->initiatied_on_treatment; ?></h3>
      <h4 style="font-size: 14px;margin: 0px;color: #000000;letter-spacing: 1px;">Initiated on Treatment</h4>
    </div>
</div>

  	</div>

  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 right-div">

				<div class="card well events" style="background-color: #fff;border-bottom-width:5px;border-bottom-color:#ef6c00; text-align:center;height: 107px;">
				<div class="col-sm-12 col-md-1 pull-right text-right glyphicon glyphicon-info-sign" style="font-size: 14px; padding: 0px;"  data-toggle="tooltip" title="" data-original-title="Number of patients that have completed their treatment regimen (Pre-SVR)">
					
				</div>
				
				<div class="col-sm-12 col-md-11 text-center" style="min-height: 56px;">
				<h3 style="margin: 5px;color: #000000;font-size:36px;font-weight: 500;" class="count"> <?php echo $cascade->treatment_completed; ?></h3>
				<h4 style="font-size: 14px;margin: 0px;color: #000000;letter-spacing: 1px;">Treatment Completed</h4>
				</div>
				</div>

  	</div>
</div>
  	<div class="row">
  		<?php
  $attributes = array(
              'id' => 'filter_form',
              'name' => 'filter_form',
               'autocomplete' => 'false',
            );
           echo form_open('', $attributes); ?>
<div class="panel panel-default">
<div class="panel-body" style="padding-top: 5px;padding-bottom: 5px;">
<div class="row">
	<div class="col-md-2">
		<label for="">State</label>
		<select type="text" name="search_state" id="search_state" class="form-control input_fields" <?php  ?>  <?php echo $select2.$select3.$select4; ?>>
			<option value="">All States</option>
			<?php 
			foreach ($states as $state) {
				?>
				<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo $state->StateName; ?></option>
				<?php 
			}
			?>
		</select>
	</div>
	<div class="col-md-2">
		<label for="">District</label>
		<select type="text" name="input_district" id="input_district" class="form-control input_fields"  <?php echo $select.$select2.$select4; ?>>
			<option value="">All Districts</option>
			<?php 
			/*foreach ($districts as $district) {
				?>
				<option value="<?php echo $district->id_mstdistrict; ?>" <?php echo (count($default_districts)==1 && $default_districts[0]->id_mstdistrict == $district->id_mstdistrict)?'selected':''; ?>><?php echo ucwords(strtolower($district->DistrictName)); ?></option>
				<?php 
			}*/
			?>
		</select>
	</div>
	
	<div class="col-md-2">
		<label for="">Facility</label>
		<select type="text" name="mstfacilitylogin" id="mstfacilitylogin" class="form-control input_fields"  <?php echo $select.$select2; ?>>
			<option value="">All Facility</option>
			<?php 
			/*foreach ($facilities as $facility) {
				?>
				<option value="<?php echo $facility->id_mstfacility; ?>" <?php echo (count($default_facilities)==1 && $default_facilities[0]->id_mstfacility == $facility->id_mstfacility)?'selected':''; ?>><?php echo ucwords(strtolower($facility->facility_short_name)); ?></option>
				<?php 
			}*/
			?>
		</select>
	</div>
	<div class="col-md-2">
		<label for="">From Date</label>
		<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')){ echo $this->input->post('startdate');} else{ echo timeStampShow(date('2019-07-01'));}  ?>" required>
	</div>
	<div class="col-md-2">
		<label for="">To Date</label>
		<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')){echo $this->input->post('enddate');}else{echo timeStampShow(date('Y-m-d'));} ?>" required>
	</div>
	<div class="col-md-2">
		<label for="">&nbsp;</label>
		<button class="btn btn-block" style="background-color: #f4860c;color: white; line-height: 1.2; font-weight: 600;">SEARCH</button>
	</div>
</div>
<br>
 <?php echo form_close(); ?>
  	</div>
</div>
</div>

	<div class="row">
		

  	<div class="panel panel-default">
	<div class="panel-heading">
	<h4 class="text-center panel-title" style="color: white;">Viral Hepatitis Cascade of Care
</h4>
</div>
</div>
</div>
<div class="div_collapse">
<div class="row html2canvas_div" style="margin-top: -22px;">
		<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-11 col-sm-6 col-xs-10" style="margin-top: -15px;">

				<h4 class="sub-heading">Cascade of Care for HCV patients</h4>
				<div style="margin-left: -40px;">
		<div id="js-legend" class="chart-legend"></div></div>
	</div>
			<div class="col-md-1 col-sm-1 col-xs-1 pull-right mar-right" data-html2canvas-ignore="true">
  				
			<div class="col-md-4 col-sm-6 cpanel_wrapper">


				 <div class="side_btn">
                            <div class="side_ul">
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
<div id="divFilter" class="chk" style="width: 200px;">
                    <div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
                        <div class="col-sm-12">
                            <div class="form-group">
							<a id="cascadeChartLink" download="cascadeChart.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="downloadscreenedacross_national"></i></a>

                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                               <a  href='<?= base_url() ?>Dashboard/casecade_export_csv/excel' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
                            </div>
                        </div>
                    <br />
                </div>
            </div>
				
		</div>
		</div>
	</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 cascade_scroll-x">
				<div class="cascade_div">
				<canvas id="cascadeChart"></canvas></div>
			</div>
			
		</div>
	</div>
</div>

</div>


  	
  		
  		<!-- view only National login start-->
<?php  if ($loginData->user_type==1 && $filters['id_input_district']==0) { ?>
<?php if($filters['id_mstfacility']=="" OR $filters['id_mstfacility']==0){ ?>
<div class="row html2canvas_div">
<div class="panel panel-default">
	<!-- <div class="panel-heading">
	<h3 class="panel-title text-center" id="anti_hcv_state_heading" style="color: white;">Patients screened for Anti-HCV across states </h3>
	</div> -->
	<div class="panel-body">
			<div class="row">
				<div class="col-md-9 col-sm-6 col-xs-6" style="margin-top: -15px;">

				<h4 class="sub-heading" id="anti_hcv_state_heading">Patients screened for Anti-HCV across states</h4>
				<div style="margin-left: -40px;">
		<div id="js-legend1" class="chart-legend"></div></div>
			</div>
				 <div class="col-md-3 col-sm-6 col-xs-6 pull-right" data-html2canvas-ignore="true" style="padding: 0px;">
  				<div class="col-md-8 col-sm-6 col-xs-8 form-group" style="padding: 0px;">
					<select name="getmonthanalys" id="getmonthanalysnational" onchange="casecase_vl_ajax();" class="form-control">
					<option value="anti_hcv_screened" selected="">Anti-HCV Screened</option>
					<option value="anti_hcv_positive">Anti-HCV Positive</option>
					<option value="viral_load_tested">VL Screened</option>
					<option value="viral_load_detected" >VL Detected</option>
					<option value="initiatied_on_treatment">Initiated on Treatment</option>
					<option value="treatment_completed">Treatment Completed </option>
					<option value="svr_done">SVR Test Done</option>
					<option value="treatment_successful">Treatment Success</option>
					<option value="treatment_failure">Treatment Failure</option>
				</select>
			</div>
				<div class="col-md-3 col-sm-3 col-xs-4">
				
				<div class="side_btn">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="screenedacrossdistricts_nationalLink" download="Casecade_of_care_Across_State.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="downloadscreenedacross_national"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/treatment_Initiations_by_state_HCV_export/' class="btn1 btn-info btn-block" id="exl_screenedacrossdistricts" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


				<!-- <div class="text-right">
					<a id="screenedacrossdistricts_nationalLink" download="cascadeChart.jpeg" class="export_button"style="margin-right: 15px;" ><i class="fa fa-lg fa-download" id="downloadscreenedacross_national"></i></a>
				<a href='<?= base_url() ?>Dashboard/treatment_Initiations_by_state_HCV_export/' class="fa fa-file-excel-o fa-lg text-dark bg-success" style="margin-right: 10px;" id="exl_screenedacrossdistricts" title="Excel Download" ></a>
				</div> -->
			</div>
			</div>
		</div>
		 
		<div class="row">
			<div class="col-lg-12 col-md-12 state_scroll-x" style="padding-right: 0px;">
				<div class="state_div">
				<canvas id="screenedacrossdistricts_national"></canvas>
</div>
			</div>
			
		</div>
	</div>
</div>
</div>
<?php } }?>

<!-- view only state login start-->
<?php  if ($loginData->user_type==3 || $filters['id_input_district']!=0 ) { ?>

<?php if($filters['id_mstfacility']==""){ ?>
	<div class="row html2canvas_div">
<div class="panel panel-default">
	<!-- <div class="panel-heading">
	<h3 class="panel-title text-center" id="anti_hcv_dist_heading" style="color: white;">
	Patients screened for HCV across facilities 
	</h3>
	</div> -->
	<div class="panel-body">
			<div class="row">
				
				<div class="col-md-8 col-sm-5 col-xs-5" style="margin-top: -15px;">

				<h4 class="sub-heading" id="anti_hcv_dist_heading">Patients screened for HCV across facilities</h4>
				<div style="margin-left: -40px;">
		<div id="Screened_HCV_dist-legend" class="chart-legend"></div></div>
	</div>
				 <div class="col-md-3 col-sm-7 col-xs-7 pull-right" data-html2canvas-ignore="true">
  				<div class="col-md-8 col-xs-9 col-sm-9 form-group" style="padding: 0px;">
					<select name="getmonthanalys2" id="getmonthanalys2" onchange="hcv_screened_district();" class="form-control">
					<option value="anti_hcv_screened" selected="">Anti-HCV Screened</option>
					<option value="anti_hcv_positive">Anti-HCV Positive</option>
					<option value="viral_load_tested">VL Screened</option>
					<option value="viral_load_detected">VL Detected</option>
					<option value="initiatied_on_treatment">Initiated on Treatment</option>
					<option value="treatment_completed">Treatment Completed </option>
					<option value="svr_done">SVR Test Done</option>
					<option value="treatment_successful">Treatment Success</option>
					<option value="treatment_failure" >Treatment Failure</option>
				</select>
				</div>
				<div class="col-md-4 col-xs-3 col-sm-3 mar-right">


					<div class="side_btn" style="margin-right: 20px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="screenedacrossdistrictsLink" download="Casecade_of_care_Across_Districts.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="downloadscreenedacrossdistricts"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a  href='<?= base_url() ?>Dashboard/treatment_Initiations_by_District_HCV_export/excel' class="btn1 btn-info btn-block" id="exl_hcv_screened" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>
				<!-- <div class="text-right" style="margin-top: 5px;">
				
				
				
				<a id="screenedacrossdistrictsLink" download="cascadeChart.jpeg" class="export_button" style="margin-right: 15px;margin-top: 5px;"><i class="fa fa-lg fa-download" id="downloadscreenedacrossdistricts"></i></a>
				<a href='<?= base_url() ?>Dashboard/treatment_Initiations_by_District_HCV_export/excel' class="fa fa-file-excel-o fa-lg text-dark bg-success" id="exl_hcv_screened"style="margin-right: 10px;" title="Excel Download" ></a></div> -->
				</div>
			</div>
			
		
		
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 district_scroll_x">
				<div class="district_div">
				<canvas id="screenedacrossdistricts"></canvas>
			</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php } } ?>
<!-- view only state login End-->




  <!-- 	<div class="col-sm-12">
  		<h4>Heading</h4>
  	</div>

  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
  		graph3
  	</div>

  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
  		graph4
  	</div>

<div class="col-sm-12">
  		<h4>fgbfh</h4>
  		graph5
  	</div>

<div class="col-sm-12">
  		<h4>fgbfh</h4>
  		graph6
  	</div>

  	<div class="col-sm-12">
  		<h4>fgbfh</h4>
  		graph7
  	</div> -->

 <!--  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
  		graph10
  	</div>

  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
  		graph11
  	</div> -->


  		
  
 


  
  		
  		<!-- view only state login start-->
<?php  /*if ($loginData->user_type==3 || $filters['id_input_district']!=0) { ?>
<?php if($filters['id_mstfacility']=="" OR $filters['id_mstfacility']==0){ ?>
		<div class="row html2canvas_div">
<div class="panel panel-default">


	<div class="panel-body">
	<div class="row">

					<div class="col-md-9 col-sm-5 col-xs-5" style="margin-top: -15px;">

				<h4 class="sub-heading" id="head_hav_district">Screened for HAV across all facilities</h4>
				<div style="margin-left: -40px;">
		<div id="Screened_HAV_dist-legend" class="chart-legend"></div></div>
	</div>
				 <div class="col-md-3 col-sm-7 col-xs-7 pull-right" data-html2canvas-ignore="true">
  				<div class="col-md-8 col-xs-9 col-sm-9 form-group" style="padding: 0px;">
					<select name="getmonthanalys" id="getmonthanalys3" onchange="havscreened_district();" class="form-control">
					<option value="screened_for_hav" selected="">Screened for HAV</option>
					<option value="hav_positive_patients">HAV Positive Patients</option>
					<option value="patients_managed_at_facility">Patients managed at the facility</option>
					<option value="patients_referred_for_management">Patients referred for management</option>
					
				</select>
			</div>
				<div class="col-md-4 col-sm-6" style="padding: 0px;">
				
						<div class="side_btn" style="margin-right: 30px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="screenedacrossdistrictshavLink" download="Cascade_HAV_Patients_Across_Districts.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="screenedacrossdistrictshavdd"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/treatment_Initiations_by_District_HAV_export/excel' id="exl_hav_screened_dis" class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>

				
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12 col-md-12 district_scroll_x">
				<div class="district_div">
				<canvas id="screenedacrossdistrictshav"></canvas>
			</div>
			</div>
		</div>
	</div>
</div>
	</div>
<?php } }*/ ?>
<!-- view only state login End-->
  


  	
  		
  		<!-- view only state login start-->
<?php  /*if ($loginData->user_type==3 || $filters['id_input_district']!=0) { ?>
<?php if($filters['id_mstfacility']=="" OR $filters['id_mstfacility']==0){ 
	?>
	<div class="row html2canvas_div">
<div class="panel panel-default">
	

	<div class="panel-body">
		<div class="row">

				<div class="col-md-9 col-sm-5 col-xs-5" style="margin-top: -15px;">

				<h4 class="sub-heading" id="head_hev_district">Screened for HEV across all facilities</h4>
				<div style="margin-left: -40px;">
		<div id="Screened_HEV_dist-legend" class="chart-legend"></div></div>
	</div>
				 <div class="col-md-3 col-sm-7 col-xs-7 pull-right" data-html2canvas-ignore="true">
  				<div class="col-md-8 col-xs-9 col-sm-9 form-group" style="padding: 0px;">
					<select name="getmonthanalys" id="getmonthanalys4" onchange="hevscreened_district();" class="form-control">
					<option value="screened_for_hev" selected="">Screened for HEV</option>
					<option value="hev_positive_patients">HEV Positive Patients</option>
					<option value="patients_managed_at_facility_hev">Patients managed at the facility</option>
					<option value="patients_referred_for_management_hev">Patients referred for management</option>
					
				</select>
					</div>
				<div class="col-md-4 col-sm-3 col-xs-3" style="padding: 0px;">
				
						<div class="side_btn" style="margin-right: 30px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="screenedacrossdistrictshevLink" download="Cascade_HEV_Patients_Across_Districts.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="screenedacrossdistrictshevddev"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a  href='<?= base_url() ?>Dashboard/treatment_Initiations_by_District_HEV_export/excel' class="btn1 btn-info btn-block" id="exl_hev_screened_dis" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>
	


				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12 col-md-12 district_scroll_x">
				<div class="district_div">
				<canvas id="screenedacrossdistrictshev"></canvas>
			</div>
			</div>
		</div>
</div>
</div>
	</div>
<?php } }*/ ?>
  
</div>

	<div class="row">

				<!-- <h4 class="text-center" style="background-color: #464646; color: white; padding-top: 10px; padding-bottom: 10px;">HCV MONTH-ON-MONTH ANALYSIS
				</h4> -->

  		
<div class="panel panel-default">
	<div class="panel-heading">
		
				<h5 class="panel-title text-center"style="color: white;">HCV (Month-wise Indicators)</h5>
			 
			</div>
		</div>
	</div>
	<div class="div_collapse">
	<div class="row html2canvas_div" style="margin-top: -22px;">
		<div class="panel panel-default">
	<div class="panel-body">

					<div class="row">
						<div class="col-md-9 col-sm-4 col-xs-4" style="margin-top: -10px;">

									<h4 class="sub-heading" id="hcv_month_on_month">VL Detected</h4>
									<div style="margin-left: -40px;">
							<div id="month_on_month-legend" class="chart-legend"></div></div>
						</div>
				 <div class="col-md-3 col-sm-7 col-xs-8 pull-right" style="padding: 0px;" data-html2canvas-ignore="true">
  				<div class="col-md-8 col-sm-7 col-xs-8 form-group" style="padding: 0px;">
					<select name="getmonthanalys" id="getmonthanalys"  class="form-control" onchange="month_on_month_ajax();">
					<option value="anti_hcv_screened" selected="">Anti-HCV Screened</option>
					<option value="anti_hcv_positive">Anti-HCV Positive</option>
					<option value="viral_load_tested">VL Screened</option>
					<option value="viral_load_detected">VL Detected</option>
					<option value="initiatied_on_treatment">Initiated on Treatment</option>
					<option value="treatment_completed">Treatment Completed </option>
					<!-- <option value="">Expected Treatment Completions</option>
					<option value="">Actual Treatment Completions</option> -->
					<option value="svr_done">SVR Test Done</option>
					<option value="treatment_successful">Treatment Success</option>
					<option value="treatment_failure">Treatment Failure</option>
					</select>
					</div>
				<div class="col-md-4 col-sm-4 col-xs-4">

					<div class="side_btn" style="margin-right: 15px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="trendGraphLink" download="Month-On-Month.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="downloadCascade"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/trendviral_load_detected_export' class="btn1 btn-info btn-block" id="MONTH-ON-MONTH" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>



				<!-- <div class="text-right" style="margin-top: 5px;">
					
					<a id="trendGraphLink" download="trendGraph.jpeg" class="export_button"><i class="fa fa-download fa-lg" id="downloadCascade" style="margin-right: 20px; margin-top: 5px"></i></a>
					<a  href='<?= base_url() ?>Dashboard/trendviral_load_detected_export' class="fa fa-file-excel-o fa-lg text-dark bg-success" id="MONTH-ON-MONTH"  title="Excel Download" ></a>
					</div> -->
				</div>
			</div>
		</div>
		
		<div class="row">
			
			<div class="col-lg-12 col-md-12 cascade_scroll-x">
				<div class="cascade_div">
				<canvas id="trendGraph"></canvas>
			</div>
			</div>
		</div>
	</div>
</div>

  </div>

</div>

	<div class="row">
  			<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title text-center" style="color: white;">HCV (Stage of Disease and Regimen-wise Distribution) 
	</h5>
  	</div>	
  	</div>
</div>
<div class="div_collapse">
<div class="row" style="margin-top: -22px;">
  	<div class="<?php echo($coloffset); ?> left-div html2canvas_div">
  		<div class="panel panel-default">
			<div class="panel-body" style="padding-bottom: 0px;">
				<div class="row" style="padding-bottom: 0px;">
					<div class="col-xs-8 col-md-10" style="margin-top: -15px">
<h4 class="sub-heading">Stage of disease for VL positive patients</h4>
</div>
					<div class="col-xs-4 col-md-2" data-html2canvas-ignore="true">
						<!-- <a href="<?php echo site_url(); ?>chartreports_infinite_scroll/district_wise_age" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a> -->

							<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="diseasevlpositiveLink" download="Stage_of_disease_for_VL_positive_patients.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="downloadCascade"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a  href='<?= base_url() ?>Dashboard/cirrhotic_details_export/excel' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


						<!-- <div class="text-right">	
						<a id="diseasevlpositiveLink" download="ageChart.jpeg" class="export_button" style="margin-right: 20px;"><i class="fa fa-download fa-lg" id="downloadCascade"></i></a>
						<a href='<?= base_url() ?>Dashboard/cirrhotic_details_export/excel' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a>
											</div> -->
					</div>
				</div>
				<div class="row">

					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="overflow-x: hidden;margin-top: -10px;">
						<div class="pie_chart_div">
						<canvas id="diseasevlpositive"></canvas>
					</div>	
					</div>
					 <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-right top-margin" id="vl-legend_div">
						<div id="vl-legend" class="chart-legend pie-chart_legend_margin"></div>
							</div>
				</div>
			</div>
		</div>
  	</div>

 
  		<?php if(isset($mtc_user) || $loginData->user_type==3 || $loginData->user_type==1){ if((isset($mtc_user[0]) ? $mtc_user[0]->is_Mtc==1 : '')  ){ ?>
	 	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 right-div html2canvas_div">
		<div class="panel panel-default">
			<!-- <div class="panel-heading">
				<h5 class="panel-title text-center" style="color: white;">Regimen wise distribution</h5>
			</div> -->
			<div class="panel-body">

				<div class="row">
					<div class="col-xs-8 col-md-8" style="margin-top: -15px">
<h4 class="sub-heading"> Regimen-wise distribution</h4>
<div style="margin-left: -40px;">
		<div id="regimenWisedistribution-legend" class="chart-legend"></div></div>
	</div>
					<div class="col-xs-4 col-md-4" data-html2canvas-ignore="true">
						<!-- <a href="<?php echo site_url(); ?>chartreports_infinite_scroll/district_wise_gender" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a> -->

							<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="regimenWisedistributionLink" download="Regimen_Wise_Distribution.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="regimenWisedistributionv"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a  href='<?= base_url() ?>Dashboard/regimen_export/excel' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>

<!-- 
						<div class="text-right">
						<a id="regimenWisedistributionLink" download="donutchart.jpeg" class="export_button" style="margin-right: 20px;"><i class="fa fa-download fa-lg" id="regimenWisedistributionv"></i></a>
						<a href='<?= base_url() ?>Dashboard/regimen_export/excel' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a>
					</div> -->
					</div>
				</div>
				<div class="row">

					 <div class="col-lg-12 col-md-12 cascade_scroll-x">
					 	<div class="ageChart_divreg">
						<canvas id="regimenWisedistribution"></canvas>
					</div>
					</div>

				</div>
			</div>
		</div>
	</div>
<?php } } ?>

  	

</div>
</div>
  	<div class="row">
  		<div class="panel panel-default">
			<div class="panel-heading"><h5 class="panel-title text-center" style="color: white;">HCV (Treatment Success)
</h5>
</div>
</div>
  	</div>
<div class="div_collapse">
<div class="row" style="margin-top: -22px;">  
<div class="<?php echo ($coloffset); ?> left-div html2canvas_div">
  		<div class="panel panel-default">
			<!-- <div class="panel-heading">
				<h5 class="panel-title text-center" style="color: white;">Success rate of treatment (SVR tested)</h5>
			</div> -->
			<div class="panel-body">
				<div class="row" style="margin-top: -5px;">

					<div class="col-xs-8 col-md-8" style="margin-top: -10px">
					<h4 class="sub-heading">Success rate of treatment (SVR tested)</h4>
					<div style="margin-left: -40px;">
							<div id="svr_tested-legend" class="chart-legend"></div></div>
						</div>


					<div class="col-xs-4 col-md-4" data-html2canvas-ignore="true">
						<!-- <a href="<?php echo site_url(); ?>chartreports_infinite_scroll/district_wise_age" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a> -->
						
							<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="successratiosvrtestLink" download="Success_rate_of_treatment(SVR tested).jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="successratiosvrtestdown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a  href='<?= base_url() ?>Dashboard/cascade_SVR_Tested_export/excel' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />

						</div>
					</div>





<!-- 
						<div class="text-right">
						<a id="successratiosvrtestLink" download="ageChart.jpeg" class="export_button"style="margin-right: 20px;"><i class="fa fa-download fa-lg" id="successratiosvrtestdown"></i></a>
						<a href='<?= base_url() ?>Dashboard/cascade_SVR_Tested_export/excel' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a>
					</div> -->
					</div>
				</div>
				<div class="row">

					<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 cascade_scroll-x" style="overflow: hidden;">
						<div class="pie_chart_div">
						<canvas id="successratiosvrtest"></canvas>
					</div>
					</div>
					 <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-right top-margin">
					 	<div style="margin-left: 20px;">
						<div id="successratiosvrtest-legend" class="chart-legend pie-chart_legend_margin"></div>
					</div>
							</div>	
				</div>
			</div>
		</div>
  	</div>

  	
  		<?php if(isset($mtc_user) || $loginData->user_type==3 || $loginData->user_type==1){ if((isset($mtc_user[0]) ? $mtc_user[0]->is_Mtc==1 : '')  ){ ?>
	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 right-div html2canvas_div">
		<div class="panel panel-default">
			<!-- <div class="panel-heading">
				<h5 class="panel-title text-center" style="color: white;">Regimen wise success rate of treatment (SVR tested)</h5>
			</div> -->
			<div class="panel-body">
				<div class="row" style="margin-top: -5px;">

						<div class="col-xs-10 col-sm-10 col-md-11" style="margin-top: -10px">
					<h4 class="sub-heading">Regimen-wise success rate of treatment (SVR tested)</h4>
					<div style="margin-left: -40px;">
							<div id="svr_tested_rate-legend" class="chart-legend_flex"></div></div>
						</div>
			

					<div class="col-xs-2 col-sm-2 col-md-1" data-html2canvas-ignore="true">
						<!-- <a href="<?php echo site_url(); ?>chartreports_infinite_scroll/district_wise_gender" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a> -->
						
							<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="regimenWisetreatmentLink" download="Regimen_wise_success_rate_of_treatment(SVR tested).jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="regimenWisetreatmentdown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a  href='<?= base_url() ?>Dashboard/Success_Ratio_of_Treatment_export/' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>






						<!-- <div class="text-right">
						<a id="regimenWisetreatmentLink" download="donutchart.jpeg" class="export_button" style="margin-right: 20px;"><i class="fa fa-download fa-lg" id="regimenWisetreatmentdown"></i></a>
						<a href='<?= base_url() ?>Dashboard/Success_Ratio_of_Treatment_export/' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a>
											</div> -->
					</div>
				</div>
				<div class="row">

					 <div class="col-lg-12 col-md-12 cascade_scroll-x">
					 	<div class="ageChart_div">
						<canvas id="regimenWisetreatment"></canvas>
					</div>
					</div>

				</div>
			</div>
		</div>
		</div>
<?php } } ?>
  
  </div>

  
  		
  		<?php  if ($loginData->user_type==3 || $filters['id_input_district']!=0) { ?>
<?php if($filters['id_mstfacility']=="" OR $filters['id_mstfacility']==0){ ?>
		<div class="row html2canvas_div">
<div class="panel panel-default">
		<!-- <div class="panel-heading">
		<h5 class="panel-title text-center" style="color: white;">Success rate of treatment (SVR tested) across facilities
		</h5>
		</div> -->
	<div class="panel-body">
		<div class="row">
<div class="col-xs-11 col-sm-11 col-md-8" style="margin-top: -15px;">
					<h4 class="sub-heading">Success rate of treatment (SVR tested) across facilities</h4>
					<div style="margin-left: -40px;">
							<div id="svr_dist-legend" class="chart-legend_flex"></div></div>
						</div>
			<div class="col-xs-1 col-sm-1 col-md-4" data-html2canvas-ignore="true">
					
					<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="sucessratiosvrtestdisLink" download="Success_Rate_of_treatment(SVR tested)_across_facilities.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="sucessratiosvrtestdisdown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/treatment_by_Districtsvrsuc_export/' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


				<!-- <div class="text-right">
				<a id="sucessratiosvrtestdisLink" download="cascadeChart.jpeg" class="export_button" style="margin-right: 20px;"><i class="fa fa-lg fa-download" id="sucessratiosvrtestdisdown"></i></a>
				<a href='<?= base_url() ?>Dashboard/treatment_by_Districtsvrsuc_export/' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a>
					</div> -->
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 district_scroll_x">
				<div class="district_div">
				<canvas id="sucessratiosvrtestdis"></canvas>
			</div>
			</div>
		</div>
	</div>
</div>
	</div>

<?php } }?>
  
  		
  		
  		<!-- view only state login national-->
<?php  if ($loginData->user_type==1 && $filters['id_input_district']==0) { ?>
<?php if($filters['id_mstfacility']=="" OR $filters['id_mstfacility']==0){ ?>
	<div class="row html2canvas_div">
<div class="panel panel-default">
		<!-- <div class="panel-heading">
		<h5 class="panel-title text-center" style="color: white;">Success rate of treatment (SVR tested) across states 
		</h5>
		</div> -->
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-10 col-sm-10 col-md-8" style="margin-top: -15px;">
					<h4 class="sub-heading">Success rate of treatment (SVR tested) across states</h4>
					<div style="margin-left: -40px;">
							<div id="svr_state-legend" class="chart-legend_flex"></div></div>
						</div>
			<div class="col-xs-2 col-sm-2 col-md-4" data-html2canvas-ignore="true">
				
				<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="sucessratiosvrteststateLink" download="Success_Rate_of_treatment(SVR tested)_across_states.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="sucessratiosvrteststatedown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/treatment_by_statesvrsucess_export/excel' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


				<!-- <div class="text-right">
				<a id="sucessratiosvrteststateLink" download="cascadeChart.jpeg" class="export_button" style="margin-right: 20px;"><i class="fa fa-lg fa-download" id="sucessratiosvrteststatedown"></i></a>
				<a href='<?= base_url() ?>Dashboard/treatment_by_statesvrsucess_export/excel' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a>
					</div> -->
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 state_scroll-x">
				<div class="state_div">
				<canvas id="sucessratiosvrteststate"></canvas>
			</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php } }?>
<!-- view only national login End-->
  	
</div>
	<div class="row">
  		<!-- <h4 class="text-center" style="background-color: #464646; color: white; padding-top: 10px; padding-bottom: 10px;">HCV ADHERENCE AND LOST TO FOLLOW UP ANALYSIS
  		</h4> -->

<div class="panel panel-default">
	<div class="panel-heading">
		
				<h5 class="panel-title text-center"style="color: white;">HCV (Treatment Adherence and Lost to follow up)</h5>
			 
			</div>
		</div>
	</div>
			<!-- 
		<div class="panel-heading">
		<h5 class="panel-title text-center" style="color: white;">Lost to follow up</h5>
	</div> -->
	<div class="div_collapse">
	<div class="row html2canvas_div" style="margin-top: -22px;">
		<div class="panel panel-default">
	<div class="panel-body">
		
		<div class="row">

			<div class="col-md-9 col-sm-10 col-xs-10" style="margin-top: -15px;">

									<h4 class="sub-heading">Lost to follow up</h4>
									<div style="margin-left: -40px;">
							<div id="ltfu-legend" class="chart-legend_flex"></div></div>
						</div>

			<div class="col-xs-2 col-sm-2 col-md-3" data-html2canvas-ignore="true">
				
				<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="lossofFollowupLink" download="lossofFollowup.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="lossofFollowupDown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/lossofFollowup_HCV_export/excel' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


				<!-- <div class="text-right">
				<a id="lossofFollowupLink" download="lossofFollowup.jpeg" class="export_button" style="margin-right: 20px;"><i class="fa fa-lg fa-download" id="lossofFollowupDown"></i></a>
				<a href='<?= base_url() ?>Dashboard/lossofFollowup_HCV_export/excel' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a>
					</div> -->
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 cascade_scroll-x">
				<div class="cascade_div">
				<canvas id="lossofFollowup"></canvas>
			</div>
			</div>
		</div>
	</div>
</div>
  	</div>


  		
  		<!-- view only National login start-->
<?php  if ($loginData->user_type==1 && $filters['id_input_district']==0) { ?>
<?php if($filters['id_mstfacility']=="" OR $filters['id_mstfacility']==0){ ?>
	<!-- <div class="row html2canvas_div">
<div class="panel panel-default">
	 <div class="panel-heading">
	<h5 class="panel-title text-center" style="color: white;">Lost to follow up across states (in percentage)</h5>
	</div> -->
	<!-- <div class="panel-body">
		<div class="row">

			<div class="col-md-9 col-sm-10 col-xs-10" style="margin-top: -15px;">

									<h4 class="sub-heading">Lost to follow up across states (in percentage)</h4>
									<div style="margin-left: -40px;">
							<div id="ltfu_per-legend" class="chart-legend_flex"></div></div>
						</div>
			<div class="col-xs-2 col-sm-2 col-md-3" data-html2canvas-ignore="true">
				
				<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="lossfollowacrossstateLink" download="Lost_to_follow_up_across_states(in percentage).jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="lossfollowacrossstatedown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/lossfollowacross_State_export/' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


				<div class="text-right">
				<a id="lossfollowacrossstateLink" download="cascadeChart.jpeg" class="export_button" style="margin-right: 20px;"><i class="fa fa-lg fa-download" id="lossfollowacrossstatedown"></i></a>
				<a href='<?= base_url() ?>Dashboard/lossfollowacross_State_export/' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a>
					</div> -->
			<!--</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 state_scroll-x">
				<div class="state_div">
				<canvas id="lossfollowacrossstate"></canvas>
			</div>
			</div>
		</div>
	</div>
</div>
 	</div> -->
<?php } }?>
 

  
  		<!-- view only National login start-->
<?php  if ($loginData->user_type==1 && $filters['id_input_district']==0) { ?>
<?php if($filters['id_mstfacility']=="" OR $filters['id_mstfacility']==0){ ?>
		<div class="row html2canvas_div">
<div class="panel panel-default">
	<!-- <div class="panel-heading">
	<h5 class="panel-title text-center" style="color: white;">Lost to follow up across states (in numbers)</h5>
	</div> -->
	<div class="panel-body">
		<div class="row">
			
			<div class="col-md-9 col-sm-10 col-xs-10" style="margin-top: -15px;">

									<h4 class="sub-heading" id="ltfu_title">Lost to follow up across states (in numbers)</h4>
									<div style="margin-left: -40px;">
							<div id="ltfu_num-legend" class="chart-legend_flex"></div></div>
						</div>

			<div class="col-xs-2 col-sm-2 col-md-3" data-html2canvas-ignore="true">

		<div class="col-md-8 col-xs-9 col-sm-9 form-group" style="padding: 0px;">

		<select name="ltfu_across_state" id="ltfu_across_state" onchange="ltfu_change_state()" class="form-control">
		<option value="loss_of_follow_viral_load" selected="">Viral Load</option>
		<option value="loss_of_follow_1St_dispensation">1st Dispensation</option>
		<option value="loss_of_follow_2St_dispensation">2nd Dispensation</option>
		<option value="loss_of_follow_3St_dispensation">3rd Dispensation</option>
		<option value="loss_of_follow_4St_dispensation">4th Dispensation</option>
		<option value="loss_of_follow_5St_dispensation">5th Dispensation</option>
		<option value="loss_of_follow_6St_dispensation">6th Dispensation</option>
		<option value="loss_of_follow_SVR">SVR</option>

		</select>

		</div>

				
				<div class="side_btn">
						<div class="side_ul" style="margin-right: 10px;">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="lossfollowacrossdistnumLink" download="Lost_to_follow_up_across_states(in numbers).jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="lossfollowacrossdistnumdown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a id="lossfollowacrossdistnumLink" href='<?= base_url() ?>Dashboard/lossfollowacross_State_num_export/' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


			<!-- 	<div class="text-right">
			<a id="lossfollowacrossdistnumLink" download="cascadeChart.jpeg" class="export_button" style="margin-right: 20px;"><i class="fa fa-lg fa-download" id="lossfollowacrossdistnumdown"></i></a>
			<a href='<?= base_url() ?>Dashboard/lossfollowacross_State_num_export/' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a>
				</div> -->
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 state_scroll-x">
				<div class="state_div">
				<canvas id="lossfollowacrossstatetnum"></canvas>
			</div>
			</div>
		</div>
	</div>
</div>
 	</div>
<?php } }?>
<!-- view only National login End-->
 


	
  		<!-- view only state login start-->
<?php /* if ($loginData->user_type==3 || $filters['id_input_district']!=0) { ?>
<?php if($filters['id_mstfacility']=="" OR $filters['id_mstfacility']==0){ ?>
	<div class="row html2canvas_div">
<div class="panel panel-default">

	<div class="panel-body">
		<div class="row">
			<div class="col-md-9 col-sm-11 col-xs-11" style="margin-top: -15px;">

									<h4 class="sub-heading">Lost to follow up across facilities (in percentage)</h4>
									<div style="margin-left: -40px;">
							<div id="ltfu_per_dist-legend" class="chart-legend_flex"></div></div>
						</div>

			<div class="col-xs-1 col-sm-1 col-md-3" data-html2canvas-ignore="true">
				
				<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="lossfollowacrossdistLink" download="Lost_to_follow_up_across_facilities(in percentage).jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="lossfollowacrossdistdown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/lossfollowacrossdist_export/' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


			
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 district_scroll_x">
				<div class="district_div">
				<canvas id="lossfollowacrossdist"></canvas>
			</div>
		</div>
		</div>
	</div>
</div>
	</div>

<?php } }*/ ?>
<!-- view only state login End-->
  

  		
  		<!-- view only state login start-->
<?php  if ($loginData->user_type==3 || $filters['id_input_district']!=0) { ?>
<?php if($filters['id_mstfacility']=="" OR $filters['id_mstfacility']==0){ ?>
	<div class="row html2canvas_div">
<div class="panel panel-default">
	<!-- <div class="panel-heading">
	<h5 class="panel-title text-center" style="color: white;">Lost to follow up across facilities (in numbers)</h5>
	</div> -->
	<div class="panel-body">
		<div class="row">
		<div class="col-md-9 col-sm-11 col-xs-11" style="margin-top: -15px;">

									<h4 class="sub-heading" id="ltfi_title_dis">Lost to follow up across facilities (in numbers)</h4>
									<div style="margin-left: -40px;">
							<div id="ltfu_num_dist-legend" class="chart-legend_flex"></div></div>
						</div>

			<div class="col-xs-1 col-sm-1 col-md-3" data-html2canvas-ignore="true">

				<div class="col-md-8 col-xs-9 col-sm-9 form-group" style="padding: 0px;">

		<select name="ltfu_across_district" id="ltfu_across_district" onchange="ltfu_change_facility()" class="form-control">
		<option value="loss_of_follow_viral_load" selected="">Viral Load</option>
		<option value="loss_of_follow_1St_dispensation">1st Dispensation</option>
		<option value="loss_of_follow_2St_dispensation">2nd Dispensation</option>
		<option value="loss_of_follow_3St_dispensation">3rd Dispensation</option>
		<option value="loss_of_follow_4St_dispensation">4th Dispensation</option>
		<option value="loss_of_follow_5St_dispensation">5th Dispensation</option>
		<option value="loss_of_follow_6St_dispensation">6th Dispensation</option>
		<option value="loss_of_follow_SVR">SVR</option>

		</select>

		</div>

				<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="lossfollowacrossdistnumLink" download="Lost_to_follow_up_across_facilities(in numbers).jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="lossfollowacrossdistnumdown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/lossfollow_across_districts_export/' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


				<!-- <div class="text-right">
				<a id="lossfollowacrossdistnumLink" download="cascadeChart.jpeg" class="export_button" style="margin-right: 20px;"><i class="fa fa-lg fa-download" id="lossfollowacrossdistnumdown"></i></a>
				<a href='<?= base_url() ?>Dashboard/lossfollowacrossdist_export/' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a>
					</div> -->
			</div>
		</div>
		<div class="row">
		<div class="col-lg-12 col-md-12 district_scroll_x">
				<div class="district_div">
				<canvas id="lossfollowacrossdistnum"></canvas>
			</div>
			</div>
		</div>
	</div>
</div>
 	</div>
<?php } }?>
<!-- view only state login End-->
 
  	<div class="row">
<?php  if ($loginData->user_type==1 || $loginData->user_type==3 || $filters['id_input_district']!=0) { ?>

<?php if($filters['id_mstfacility']==""){ ?>

  	<!-- <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 left-div html2canvas_div">
  		<div class="panel panel-default">
			
			<div class="panel-body">
				<div class="row">

					<div class="col-md-10 col-sm-8 col-xs-8" style="margin-top: -15px;">

									<h4 class="sub-heading">Lost to follow up for special cases</h4>
									<div style="margin-left: -40px;">
							<div id="ltfu_spCase-legend" class="chart-legend"></div></div>
						</div>


					<div class="col-xs-4 col-md-2" data-html2canvas-ignore="true">
						

							<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="lossfollowspecialcaseLink" download="Lost_to_follow_up_for_special_cases.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="lossfollowspecialcasedown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a  href='<?= base_url() ?>Dashboard/lost_to_followup_special_case_export/' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


					</div>
				</div>
				<div class="row">

					<div class="col-lg-12 col-md-12 cascade_scroll-x">
						<div class="ageChart_div">
						<canvas id="lossfollowspecialcase"></canvas>
					</div>
					</div>	

				</div>
			</div>
		</div>
  	</div> -->

<?php }}?>

<div class="row">
  	<div class="col-lg-12 col-md-12 state_scroll-x html2canvas_div">
  		<div class="panel panel-default">
			<!-- <div class="panel-heading">
				<h5 class="panel-title text-center" style="color: white;">Adherence</h5>
			</div> -->
			<div class="panel-body">
				<div class="row">

					<div class="col-md-10 col-sm-8 col-xs-8" style="margin-top: -15px;">

									<h4 class="sub-heading">Adherence</h4>
									<div style="margin-left: -40px;">
							<div id="ltu_adherence-legend" class="chart-legend"></div></div>
						</div>
					<div class="col-xs-4 col-sm-4 col-md-2" data-html2canvas-ignore="true">

						
							<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="adherencepercentageLink" download="Adherence.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="adherencepercentagedown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a  href='<?= base_url() ?>Dashboard/adherencepercentage_export/' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


						
				</div>
				</div>
				<div class="row">

					 <div class="col-lg-12 col-md-12 cascade_scroll-x">
					 	<div class="ageChart_div">
						<canvas id="adherencepercentage"></canvas>
					</div>
					</div>

				</div>
			</div>
		</div>
  	</div>
</div>
</div>
  		
  		<!-- view only National login start-->
<?php  if ($loginData->user_type==1 && $filters['id_input_district']==0) { ?>

<?php if($filters['id_mstfacility']=="" OR $filters['id_mstfacility']==0){ ?>
	<div class="row html2canvas_div">
<div class="panel panel-default">
<!-- <div class="panel-heading">
<h5 class="panel-title text-center" id="head_Adherence_state"style="color: white;">Adherence across states for Regimen 1: SOF/DCV
</h5>
</div> -->
<div class="panel-body">
<div class="row">
	<div class="col-md-9 col-sm-5 col-xs-5" style="margin-top: -15px;">

									<h4 class="sub-heading" id="head_Adherence_state">Adherence across states for Regimen 1: SOF/DCV</h4>
									<div style="margin-left: -40px;">
							<div id="ltfu_AdherenceState-legend" class="chart-legend"></div></div>
						</div>
<div class="col-md-3 col-sm-7 col-xs-7 pull-right" style="padding: 0px;" data-html2canvas-ignore="true">
  <div class="col-md-8 col-xs-9 col-sm-9 form-group" style="padding: 0px;">

<select name="Adherence_across_state" id="Adherence_across_state" onchange="regimen_change_state()" class="form-control">
<option value="reg_wise_reg1_SOF_DCV" selected="">Regimen 1: SOF/DCV</option>
<option value="reg_wise_reg2_SOF_VEL">Regimen 2: SOF/VEL</option>
<option value="reg_wise_reg3_SOF_VEL_Ribavirin">Regimen 3a: SOF/VEL/Ribavirin</option>
<option value="reg_wise_reg4_SOF_VEL_24_weeks">Regimen 3b: SOF/VEL(24 weeks)</option>
<option value="reg_wise_others">Other Regimen</option>

</select>

</div>
<div class="col-md-4 col-xs-3 col-sm-3" style="padding: 0px;">
	
	<div class="side_btn mr-30">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="AdherenceStateLink" download="Adherence_across_states.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="AdherenceStatedown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/Adherence_state_export/' class="btn1 btn-info btn-block" id="regmen_state_exl" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


<!-- <div class="text-right" style="margin-top: 0px;">
<a id="AdherenceStateLink" download="cascadeChart.jpeg" class="export_button"><i class="fa fa-lg fa-download" id="AdherenceStatedown" style="margin-right: 20px; margin-top: 5px"></i></a>
<a href='<?= base_url() ?>Dashboard/Adherence_state_export/' class="fa fa-file-excel-o fa-lg text-dark bg-success" id="regmen_state_exl" title="Excel Download" style="margin-right: 15px; margin-top: 5px"></a>
</div> -->
</div>
</div>
</div>

<div class="row">
<div class="col-lg-12 col-md-12 state_scroll-x">
	<div class="state_div">
<canvas id="AdherenceState"></canvas>
</div>
</div>
</div>
</div>
</div>
	</div>
<?php } } ?>
<!-- view only National login End-->
  


  	
  		<!-- view only state login start-->
<?php  if ($loginData->user_type==3 || $filters['id_input_district']!=0) { ?>

<?php if($filters['id_mstfacility']==""){ ?>
		<div class="row html2canvas_div">
<div class="panel panel-default">
<!-- <div class="panel-heading">
<h5 class="panel-title text-center" id="head_Adherence_districts" style="color: white;">Adherence across facilities for Regimen 1: SOF/DCV
</h5>
</div> -->
<div class="panel-body">
<div class="row">
<div class="col-md-9 col-sm-5 col-xs-5" style="margin-top: -15px;">

									<h4 class="sub-heading" id="head_Adherence_districts">Adherence across facilities for Regimen 1: SOF/DCV</h4>
									<div style="margin-left: -40px;">
							<div id="ltfu_Adherencedist-legend" class="chart-legend"></div></div>
						</div>
<div class="col-md-3 col-sm-7 col-xs-7 pull-right" style="padding: 0px;" data-html2canvas-ignore="true">
  <div class="col-md-8 col-xs-9 col-sm-9 form-group" style="padding: 0px;">

<select name="Adherence_across_districts" id="Adherence_across_districts" onchange="regimen_change_district()" class="form-control">
<option value="reg_wise_reg1_SOF_DCV" selected="">Regimen 1: SOF/DCV</option>
<option value="reg_wise_reg2_SOF_VEL">Regimen 2: SOF/VEL</option>
<option value="reg_wise_reg3_SOF_VEL_Ribavirin">Regimen 3A: SOF/VEL/Ribavirin</option>
<option value="reg_wise_reg4_SOF_VEL_24_weeks">Regimen 3B: SOF/VEL(24 weeks)</option>
<option value="reg_wise_others">Other Regimen</option>

</select>
</div>

<div class="col-md-4 col-xs-3 col-sm-3" style="padding: 0px;">

	<div class="side_btn" style="margin-right: 20px">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: -5px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="AdherenceDistrictsLink" download="Adherence_across_facilities.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="AdherenceDistrictsdown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/AdherenceDistricts_export/' id="exl_adhernce_export" class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


<!-- <div class="text-right">
<a id="AdherenceDistrictsLink" download="cascadeChart.jpeg" class="export_button"><i class="fa fa-lg fa-download" id="AdherenceDistrictsdown"></i></a>
<a href='<?= base_url() ?>Dashboard/AdherenceDistricts_export/' id="exl_adhernce_export" class="fa fa-file-excel-o fa-lg text-dark bg-success"style="margin-right: -20px;" title="Excel Download" ></a>
</div> -->

</div>

</div>
</div>


<div class="row">
<div class="col-lg-12 col-md-12 district_scroll_x">
	<div class="district_div">
<canvas id="AdherenceDistricts"></canvas>
</div>
</div>
</div>
</div>
</div>
	</div>
<?php } } ?>
<!-- view only state login End-->

  
</div>

	<div class="row">
  		<!-- <h4 class="text-center" style="background-color: #464646; color: white; padding-top: 10px; padding-bottom: 10px;">HCV TURN AROUND TIME ANALYSIS Turnaround time
  		</h4> -->
<div class="panel panel-default">
	<div class="panel-heading">
	<h5 class="panel-title text-center" style="color: white;">HCV (Testing Turnaround Time)</h5>
	</div>
</div>
</div>
<div class="div_collapse">
	<div class="row html2canvas_div" style="margin-top: -22px;">

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">

			<div class="col-md-9 col-sm-10 col-xs-10" style="margin-top: -15px;">

									<h4 class="sub-heading">Turnaround time</h4>
									<div style="margin-left: -40px;">
							<div id="Turnaround-legend" class="chart-legend"></div></div>
						</div>

			<div class="col-xs-2 col-sm-2 col-md-3" data-html2canvas-ignore="true">
						
						<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="timeanalysisLink" download="Turnaround_time.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="timeanalysisdown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/Turn_Around_Time_Analysis_export/excel' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>

						<!-- <div class="text-right">
										<a id="timeanalysisLink" download="cascadeChart.jpeg" class="export_button" style="margin-right: 20px;"><i class="fa fa-lg fa-download" id="timeanalysisdown"></i></a>
										<a href='<?= base_url() ?>Dashboard/Turn_Around_Time_Analysis_export/excel' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a>
											</div> -->
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 cascade_scroll-x">
				<div class="cascade_div">
				<canvas id="timeanalysis"></canvas>
			</div>
			</div>
		</div>
	</div>
</div>
  	</div>


  	
  	<!-- 	view only National login start -->
<?php  if ($loginData->user_type==1 && $filters['id_input_district']==0) { ?>
<?php if($filters['id_mstfacility']==""){ ?>
<!-- hide bt client may be use in future -->
		<!-- <div class="row html2canvas_div">
		<div class="panel panel-default">
			<div class="panel-heading">
		<h5 class="panel-title text-center" style="color: white;">Turnaround time across states</h5>
			</div>
			<div class="panel-body">
		<div class="row">
				
				<div class="col-md-9 col-sm-8 col-xs-8" style="margin-top: -15px;">
		
									<h4 class="sub-heading">Turnaround time across states</h4>
									<div style="margin-left: -40px;">
							<div id="Turnaround_state-legend" class="chart-legend"></div></div>
						</div>
		
			<div class="col-xs-4 col-sm-4 col-md-3" data-html2canvas-ignore="true">
						
						<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="TurnAroundAnalysisStateLink" download="Turnaround_Time_across_states.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="TurnAroundAnalysisStatedown"></i></a>
		
								</div>
							</div>
		
							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/Time_Analysis_across_state_export/' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>
		
		
						<div class="text-right">
										<a id="TurnAroundAnalysisStateLink" download="cascadeChart.jpeg" class="export_button" style="margin-right: 20px;"><i class="fa fa-lg fa-download" id="TurnAroundAnalysisStatedown"></i></a>
										<a href='<?= base_url() ?>Dashboard/Time_Analysis_across_state_export/' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a>
											</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12 col-md-12 state_scroll-x">
				<div class="state_div">
				<canvas id="TurnAroundAnalysisState"></canvas>
			</div>
			</div>
		</div>
			</div>
		</div>
		 	</div> -->
<?php } } ?>
<!-- view only National login End-->
 


  		
  		<!-- view only state login start-->
<?php  /*if ($loginData->user_type==3 || $filters['id_input_district']!=0) { ?>
<?php if($filters['id_mstfacility']==""){ ?>
	<div class="row html2canvas_div">
<div class="panel panel-default">
	
	<div class="panel-body">
		<div class="row">
			<div class="col-md-9 col-sm-8 col-xs-8" style="margin-top: -15px;">

									<h4 class="sub-heading">Turnaround time across facilities</h4>
									<div style="margin-left: -40px;">
							<div id="Turnaround_dist-legend" class="chart-legend"></div></div>
						</div>

			<div class="col-xs-4 col-sm-4 col-md-3" data-html2canvas-ignore="true">
						<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="TurnAroundAnalysisdistLink" download="Turnaround_time_analysis_across_facilities.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="TurnAroundAnalysisdistdown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/Time_Analysis_across_districts_export/' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>

			</div>
		</div>

		<div class="row">
			<div class="col-lg-12 col-md-12 district_scroll_x">
				<div class="district_div">
				<canvas id="TurnAroundAnalysisdist"></canvas>
			</div>
			</div>
		</div>
	</div>
</div>
	</div>
<?php } }*/ ?>
</div>


  		<!-- <div class="row">
  			<div class="panel panel-default">
			<div class="panel-heading">
	
  		<h5 class="panel-title text-center" style="color: white;">Geographical Distribution
</h5>
</div>
</div>
</div>

<div class="div_collapse">

<div class="row">
		<div class="panel panel-default">
			
			<div class="panel-body">
				<?php if ($loginData->user_type!='2' && $filters['id_mstfacility']=="") {
					 $total = null;
					if(($loginData->user_type=='3' && $filters['id_mstfacility']=="") || ($loginData->user_type=='1' && $filters['id_mstfacility']=="" && $filters['id_search_state']!="")) { 
    			foreach ($map_state as $key => $value) {
       			 if ($total === null || $value->count > $total) {
            		$total = $value->count;
       				 }
   					 }
   					}
   					if(($loginData->user_type=='1' && $filters['id_mstfacility']=="" && $filters['id_search_state']=="")){
   						foreach ($map_national as $key => $value) {
       			 if ($total === null || $value->count > $total) {
            		$total = $value->count;
       				 }
   					 }
						
   					}
   					?> 
				<div class="row chart-legend_flex">
					<ul>
						<li style="width: 150px;">
							<span style="background-color: #E3F2FD"></span><span style="margin-left: 5px;width: 80%"><?php echo '0 -'.round($total/4);?></span>
						</li>
						<li style="width: 150px;">
							<span style="background-color:#90CAF9;"></span><span style="margin-left: 5px;width: 80%"><?php echo round($total/4).' - '.round($total/2);?></span>
						</li>
						<li style="width: 150px;">
							<span style="background-color:#42A5F5;"></span><span style="margin-left: 5px;width: 80%"><?php echo round($total/2).' - '.round($total*3/4);?></span>
						</li>
						<li style="width: 150px;"> 
							<span style="background-color: #0D47A1;"></span><span style="margin-left: 5px;width: 80%"><?php echo round($total*3/4).' - '.round($total);?></span>
						</li>
					</ul>
				</div>
			<?php }?>
				<div id="map"></div>
			</div>

  	</div>
</div>
</div> -->

  	 	<div class="row">
  		<!-- <h4 class="text-center" style="background-color: #464646; color: white; padding-top: 10px; padding-bottom: 10px;">HCV PATIENT DEMOGRAPHIC AND RISK FACTOR ANALYSIS FOR VL POSITIVE PATIENTS
  		</h4> -->
  		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title text-center" style="color: white;">HCV (Patient Risk Factor for Viral Load Positive Patients)</h5>
			</div>
			</div>
		</div>
		<div class="div_collapse">
		<div class="row html2canvas_div" style="margin-top: -22px;">
			<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">

					<div class="col-md-9 col-sm-8 col-xs-8" style="margin-top: -15px;">

									<h4 class="sub-heading">Risk factor analysis</h4>
									<div style="margin-left: -40px;">
							<div id="Risk_factor-legend" class="chart-legend"></div></div>
						</div>
				 <div class="col-xs-4 col-sm-4 col-md-3" data-html2canvas-ignore="true">
  				<!-- <div class="col-md-3 col-sm-3 form-group" style="padding: 0px;">
					<select name="rickfactoranygender" id="rickfactoranygender"  class="form-control">
						<?php foreach ($gender_details as  $value){ ?>

					<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
					
					<?php } ?>
				</select>
			</div> -->

			<!-- <div class="col-md-4 col-sm-3 form-group" style="padding: 0px;">
					<select name="rickfactoranyage" id="rickfactoranyage"  class="form-control">
						<?php //foreach ($gender_details as  $value){ ?>

					<option value="1"><?php echo '<10 Years'; ?></option>
					<option value="2"><?php echo '11-20 Years'; ?></option>
					<option value="3"><?php echo '21-30 Years'; ?></option>
					<option value="4"><?php echo '31-40 Years'; ?></option>
					<option value="5"><?php echo '41-50 Years'; ?></option>
					<option value="6"><?php echo '51-60 Years'; ?></option>
					<option value="7"><?php echo '>=60 Years'; ?></option>
					
					<?php //} ?>
				</select>
			</div> -->


				<div class="side_btn">
						<div class="side_ul" style="margin-right: 10px">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 5px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="FactorAnalysisLink" download="Risk_actor_analysis.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="downloadFactorAnalysis"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/Risk_Factor_Analysis/' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>

				<!-- <div class="text-right">
						<a href="<?php echo site_url(); ?>admin/chartreports/district_wise_regimen" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="FactorAnalysisLink" download="regimenChart.jpeg" class="export_button" style="margin-right: -15px;"><i class="fa fa-download fa-lg" id="downloadFactorAnalysis"></i></a>
						<a href='<?= base_url() ?>Dashboard/Risk_Factor_Analysis/' class="fa fa-file-excel-o fa-lg text-dark bg-success"style="margin-right: -20px;" title="Excel Download" ></a>
					</div> -->
			</div>
		</div>
		
				<div class="row">

					<div class="col-lg-12 col-md-12 cascade_scroll-x">
						<div class="risk_factor_div">
						<canvas id="FactorAnalysis" style="padding-right: 50px;"></canvas>
					</div>
					</div>
				</div>

			</div>
		</div>
  	</div>
  	<div class="row">
  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 left-div html2canvas_div">
  		<div class="panel panel-default">
			<!-- <div class="panel-heading">
				<h5 class="panel-title text-center" style="color: white;">Age wise risk factor analysis</h5>
			</div> -->
			<div class="panel-body" style="padding-bottom: 0px;">
				<div class="row">

				<div class="col-md-6 col-sm-4 col-xs-4" style="margin-top: -10px;">

									<h4 class="sub-heading">Age-wise risk factor analysis</h4>
						</div>
				 <div class="col-md-6 col-sm-7 col-xs-8 pull-right" style="padding: 0px;" data-html2canvas-ignore="true">
  				<div class="col-md-8 col-sm-7 col-xs-8 form-group" style="padding: 0px;">
  										<select name="rickfactorany2" id="rickfactorany2"  class="form-control" onchange="age_wise_Factor_ajax();">
  										<?php foreach ($side_effects_droupdown as  $value){ if(str_replace(' ', '', $value->LookupValue)=="Other(specify)")
							  {
							$LookupValue="Other";
							  }
							  else{
							  	$LookupValue=$value->LookupValue;
							  }?>
  										
  										<option value="<?php echo $value->LookupCode; ?>" ><?php echo $LookupValue; ?></option>
  										
  										<?php } ?>
  										</select>
  										</div>
				<div class="col-md-4 col-sm-4 col-xs-4">

							<div class="side_btn" style="margin-right: 20px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="agewisefactoranyLink" download="Age_wise_risk_factor_analysis.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="downloadagewisefactorany"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a  href='<?= base_url() ?>Dashboard/Age_wise_Risk_Factor_Analysis/<?php echo isset($side_effects[0]->LookupCode)?round($side_effects[0]->LookupCode):''; ?>' class="btn1 btn-info btn-block" style="margin-right: -15px;" id="exl_age_wise" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>



				<!-- <div class="text-right">
						
						<a id="agewisefactoranyLink" download="agewisefactorany.jpeg" class="export_button" style="margin-right: -15px;"><i class="fa fa-download fa-lg" id="downloadagewisefactorany"></i></a>
						<a href='<?= base_url() ?>Dashboard/Age_wise_Risk_Factor_Analysis/' class="fa fa-file-excel-o fa-lg text-dark bg-success"style="margin-right: -20px;" title="Excel Download" ></a>
						</div> -->
				</div>
			</div>
		</div>
		
				<div class="row">
					<div class="col-lg-11 col-md-11 cascade_scroll-x" style="overflow: hidden;">
						<div class="pie_chart_div risk_factor_ml-25" style="padding: 2px;">
						<canvas id="agewisefactorany" style="margin-left: 6px;"></canvas>
					</div>
					</div>
					 <div class="col-lg-8 col-sm-11 col-xs-11 pull-right top-margin" style="padding-bottom: 0px;">
					 	<div class="Age_wise_risk-legend_div">
						<div id="Age_wise_risk-legend" class="chart-legend pie-chart_legend_margin"></div>
					</div>
							</div>	
				</div>

			</div>
		</div>
  	</div>

  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 right-div html2canvas_div">
  		<div class="panel panel-default">
			<!-- <div class="panel-heading">
				<h5 class="panel-title text-center" style="color: white;">Gender wise risk factor analysis</h5>
			</div> -->
			<div class="panel-body">
				
				<div class="row">

				<div class="col-md-6 col-sm-4 col-xs-4" style="margin-top: -10px;">

									<h4 class="sub-heading">Gender-wise risk factor analysis</h4>
						</div>
				 <div class="col-md-6 col-sm-7 col-xs-8 pull-right" style="padding: 0px;" data-html2canvas-ignore="true">
  				<div class="col-md-8 col-sm-7 col-xs-8 form-group" style="padding: 0px;">
						<select name="rickfactorany1" id="rickfactorany1" onchange="gender_wise_Factor_ajax();" class="form-control">

						<?php 
						
						foreach ($side_effects_droupdown as  $value){
							if(str_replace(' ', '', $value->LookupValue)=="Other(specify)")
							  {
							$LookupValue="Other";
							  }
							  else{
							  	$LookupValue=$value->LookupValue;
							  }
							  ?>
						<option value="<?php echo $value->LookupCode; ?>" <?php  if($value->LookupCode==1){ echo 'selected';} ?>><?php echo $LookupValue; ?></option>
						
						<?php } ?>
						</select>
						</div> 
				<div class="col-md-4 col-sm-4 col-xs-4">

				
							<div class="side_btn" style="margin-right: 20px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="genderriskfactoranyLink" download="Gender_wise_risk_factor_analysis.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="downloadgenderriskfactorany"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/Gender_wise_Risk_Factor_Analysis/<?php echo isset($side_effects[0]->LookupCode)?round($side_effects[0]->LookupCode):''; ?>' class="btn1 btn-info btn-block" style="margin-right: -15px;" id="exl_gender_wise" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


				<!-- <div class="text-right">
						<a href="<?php echo site_url(); ?>admin/chartreports/district_wise_regimen" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="genderriskfactoranyLink" download="regimenChart.jpeg" class="export_button"style="margin-right: -15px;"><i class="fa fa-download fa-lg" id="downloadgenderriskfactorany"></i></a>
						
						<a href='<?= base_url() ?>Dashboard/Gender_wise_Risk_Factor_Analysis/' class="fa fa-file-excel-o fa-lg text-dark bg-success"style="margin-right: -20px;" title="Excel Download" ></a>
						</div> -->
				</div>
			</div>
		</div>
		
				<div class="row">

					<div class="col-lg-11 col-md-11 cascade_scroll-x" style="overflow: hidden;">
						<div class="pie_chart_div risk_factor_ml-25">
						<canvas id="genderriskfactorany" style="margin-left: 10px;"></canvas>
					</div>
					</div>
					 <div class="col-lg-8 col-sm-11 col-xs-11 pull-right top-margin">
					 	<div style="margin-left: 5.0em;margin-right: -5.1em;">
						<div id="gender_wise_risk-legend" class="chart-legend pie-chart_legend_margin"></div>
					</div>
							</div>	

				</div>

			</div>
  	</div>
</div>
</div>
</div>

	<div class="row">
  		
  		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title text-center" style="color: white;">HCV (Patient Demographic for Viral Load Positive Patients)</h5>
			</div>
			</div>
		</div>
		<div class="div_collapse">

<div class="row">
  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 left-div html2canvas_div">
  		<div class="panel panel-default">
			<!-- <div class="panel-heading">
				<h5 class="panel-title text-center" style="color: white;">Age wise distribution of VL detected patients</h5>
			</div> -->
			<div class="panel-body" style="padding-bottom: 0px;">
				<div class="row" >
						<div class="col-xs-8 col-md-10" style="margin-top: -15px">
						<h4 class="sub-heading">Distribution of VL detected patients</h4>
						</div>
					<div class="col-md-6 col-sm-7 col-xs-8 pull-right" style="padding: 0px;" data-html2canvas-ignore="true">


						<div class="col-md-8 col-sm-7 col-xs-8 form-group" style="padding: 0px;">
						<select name="agewisevl" id="agewisevl" onchange="gender_wise_agewisevl_ajax();" class="form-control">

						<option value="1" selected="">Age-wise</option>
						<option value="2" >Gender-wise</option>
						
						
						</select>
						</div> 



							<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="agewisedistributionLink" download="Age_wise_distribution_of_VL_detected_patients.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="downloadagewisedistribution"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
								<a href='<?= base_url() ?>Dashboard/Age_wise_distribution_VL_Detected_Patients/' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


					</div>
				</div>
				<div class="row">
					<div class="col-lg-11 col-md-11 cascade_scroll-x" style="overflow: hidden;">
						<div class="pie_chart_div_1" style="padding: 3px;">
						<canvas id="agewisedistribution" class="risk_factor_ml-25"></canvas>
					</div>
					</div>
					 <div class="col-lg-8 col-sm-11 col-xs-11 pull-right top-margin" style="padding-bottom: 0px;">
					 	<div class="Age_wise_risk-legend_div">
						<div id="Age_wise_vl-legend" class="chart-legend pie-chart_legend_margin"></div>
					</div>
							</div>	
				</div>

			</div>
		</div>
  	</div>


  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 right-div html2canvas_div">
  		<div class="panel panel-default">
			<!-- <div class="panel-heading">
				<h5 class="panel-title text-center" style="color: white;">Gender distribution screening vs Treatment initiations</h5>
			</div> -->
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-8 col-md-10" style="margin-top: -15px">
						<h4 class="sub-heading">Gender distribution screening vs Treatment initiations</h4>
							<div style="margin-left: -40px;">
							<div id="Gender_distribution-legend" class="chart-legend_flex"></div></div>
						</div>
					<div class="col-xs-4 col-md-2" data-html2canvas-ignore="true">
						
						
							<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="InitiationsLink" download="Gender_distribution_screening_vs_Treatment_initiations.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="downloadInitiations"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/TreatmentInitiations_export/' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


					<!-- 	<div class="text-right">
					<a href="<?php echo site_url(); ?>admin/chartreports/district_wise_regimen" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
					<a id="InitiationsLink" download="regimenChart.jpeg" class="export_button" style="margin-right: 20px;"><i class="fa fa-download fa-lg" id="downloadInitiations"></i></a>
					<a href='<?= base_url() ?>Dashboard/TreatmentInitiations_export/' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a>
										</div> -->
					</div>
				</div>
				<div class="row">

					<div class="col-lg-12 col-md-12 cascade_scroll-x">
						<div class="ageChart_div">
						<canvas id="TreatmentInitiations"></canvas>
					</div>
					</div>
				</div>

			</div>
		</div>
  	</div>
  </div>
</div>
<?php if(isset($mtc_user) || $loginData->user_type==3 || $loginData->user_type==1){ if((isset($mtc_user[0]) ? $mtc_user[0]->is_Mtc==1 : '')  ){ ?>
  	<div class="row">

  		<!-- <h4 class="text-center" style="background-color: #464646; color: white; padding-top: 10px; padding-bottom: 10px;">HCV SPECIAL CASES BY VL DETECTED 
  		</h4> -->
  		<div class="panel panel-default">
  	<div class="panel-heading">
				<h5 class="panel-title text-center" style="color: white;">HCV (Special Situations)</h5>
			</div>
		</div>
	</div>
	<div class="div_collapse">
			<div class="row html2canvas_div" style="margin-top: -22px;">
			<div class="panel panel-default">	
			<div class="panel-body">
				<div class="row">

					<div class="col-md-9 col-sm-8 col-xs-8" style="margin-top: -15px;">

									<h4 class="sub-heading">HCV Special Cases</h4>
								</div>
								<!-- <div style="margin-left: -40px;">
		<div id="spCasesPrevalence-legend" class="chart-legend"></div></div> -->


				 <div class="col-xs-4 col-sm-4 col-md-3" data-html2canvas-ignore="true">

						
							<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="spCasesPrevalenceLink" download="HCV_Special_Cases.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="regimenWisedistributionv"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a  href='<?= base_url() ?>Dashboard/spCasesPrevalence_export/' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>

						
						<!-- <div class="text-right">
						<a href="<?php echo site_url(); ?>admin/chartreports/district_wise_occupation" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="spCasesPrevalenceLink" download="agewisefactorany.jpeg" class="export_button"style="margin-right: 20px;"><i class="fa fa-download fa-lg" id="downloadspCasesPrevalence"></i></a>
						<a href='<?= base_url() ?>Dashboard/spCasesPrevalence_export/' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a>
											</div> -->
					</div>
				</div>
				<!-- <div class="row">
					<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 cascade_scroll-x" style="overflow: hidden;">
						<div class="pie_chart_div_spc">
						<canvas id="spCasesPrevalence" class="risk_factor_ml-25"></canvas>
						</div>
					</div>
					
				</div> -->

				<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 cascade_scroll-x">
				<div class="cascade_div">
				<canvas id="spCasesPrevalence"></canvas></div>
			</div>
			
		</div>



			</div>
		</div>
  	</div>
</div>
<?php } } ?>
</div>



<!-- *************************-->




<!--  -->

<!-- <div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title text-center">Gender Distribution Screening vs Treatment Initiations</h5>
			</div>
			<div class="panel-body">
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-md-12">
						<a href="<?php echo site_url(); ?>admin/chartreports/district_wise_regimen" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="InitiationsLink" download="regimenChart.jpeg" class="export_button"><i class="fa fa-download" id="downloadInitiations"></i></a>
					</div>
				</div>
				<div class="row">

					<div class="col-lg-12 col-md-12" style="padding : 20px">
						<canvas id="TreatmentInitiations" width="50vw" height="20vh"></canvas>
					</div>
				</div>

			</div>
		</div>
	</div>
</div> -->


<!--  -->

<br>

<!-- <script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAckMDhjh3pVCmbmf88f8qynPI4jKPNvyQ&callback=initMap">
</script> -->


<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQ_GviHVMBVFiVT6-H7I42kpwhADHEBz8"></script> -->
<script>

	$('[data-toggle="tooltip"]').tooltip();
	$(document).ready(function(){
		
		//$('#rickfactorany').val(2);
		$('#rickfactorany1').val(<?php echo isset($side_effects_droupdown[0]->LookupCode)?round($side_effects_droupdown[0]->LookupCode):'0' ?>);	
		$('#rickfactorany2').val(<?php echo isset($side_effects_droupdown[0]->LookupCode)?round($side_effects_droupdown[0]->LookupCode):'0'; ?>);
		$('#getmonthanalys').val('viral_load_detected');

		
		

		drawCascadeChart();
		//drawAgeWiseChart();
		//genderAreaChart();
		drawTrendChart();
		//drawdonutchart();
		drawdiseasevlpositiveChart();
		drawregimenWisedistributionChart();
		drawsuccessratiosvrtestChart();
		drawregimenWisetreatmentChart();
		drawlossofFollowupChart();
		drawadherencepercentageChart();
		drawtimeanalysisChart();
		drawtestefficienceChart();
		drawFactorAnalysisChart();
		drawagewisefactoranyChart();
		drawgenderriskfactoranyChart();
		drawagewisedistributionChart();
		<?php if(isset($mtc_user) || $loginData->user_type==3 || $loginData->user_type==1){ if((isset($mtc_user[0]) ? $mtc_user[0]->is_Mtc==1 : '')  ){ ?>
		drawspCasesPrevalenceChart();
	<?php } } ?>
		drawTreatmentInitiations();
	<?php if($filters['id_mstfacility']==""){ ?>
		drawscreenedacrossdistricts();
		//drawscreenedacrossdistrictshav();
		//drawscreenedacrossdistrictshev();
		drawsucessratiosvrtestdis();
		drawlossfollowacrossdist();
		drawlossfollowacrossdistnum();
		drawlossfollowspecialcase();
		drawAdherenceDistricts();
		//drawTurnAroundAnalysisdist();
		//drawMap();
		
/*National*/
		drawscreenedacrossdistricts_national();
		//drawscreenedacrossdistricts_hav_national();
		//drawscreenedacrossstateshev();
		drawsucessratiosvrteststate();
		//drawlossfollowacrossstate();
		drawlossfollowacrossstatetnum();
		drawAdherenceState();
		drawTurnAroundAnalysisState();
	<?php }?>
		   $('.chk').hide();
                    $(".side_ul").mouseover(function () {
                      
                           //$(this).('.chk').show(1000);
                        //$(this).('.chk').slideDown(1000);
                         var index = $(".side_ul").index(this);
                        $(".chk").eq(index).slideDown(1000);                			
                    });
                    $(".chk").mouseleave(function () {
                       
                            //
                             var index = $(".chk").index(this);
                         $(".chk").eq(index).slideUp(1000);
                         //$(".chk").eq(index).hide(1000);
                    });
                   $(document).scroll(function() {
 					 $(".chk").slideUp(1000);
});
                    $(".imagelink").mouseenter(function () {
                    	 var index = $(".imagelink").index(this);
                     var element=document.getElementsByClassName("html2canvas_div")[index];
        				// Global variable 
           				 var getCanvas;  
                			html2canvas(element, {x: window.scrollX,
							y: window.scrollY,
							width: element.innerWidth,
							height: element.innerHeight,
							allowTaint: true,
							scale:2,
							backgroundColor: "rgba(0,0,0,0)", removeContainer: true,
							logging: true,
                    		onrendered: function(canvas) {  
                        getCanvas = canvas; 
                        var url_base64jp = getCanvas.toDataURL('image/jpeg',1);
                         $(".imagelink").eq(index).attr("href", url_base64jp);
                        $("#loading_gif").show();
						    setTimeout(function() {
						       $("#loading_gif").hide();
						    },3000);
						
						 
                    } 
                }); 
                			}); 
$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 3000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
$(".div_collapse").hide();
$(".div_collapse").eq(0).show();

 $(".panel-heading").click(function () {
 	$(".div_collapse").toggle();
var index = $(".panel-heading").index(this);

if(index==0){
	$(".div_collapse").eq(0).toggle();
}
else{
	$(".div_collapse").eq(index).toggle();
	
}
$(".div_collapse").toggle();

//alert(index);
//$(".panel-body").toggle();
});
	});

	function drawCascadeChart()
	{
		var ctx  = $("#cascadeChart");

		var data = {
			labels: [["Anti-HCV"," Screened"],  ["Anti-HCV"," Positive"], ["Viral Load"," Tested"], ["Viral Load ","Detected"], ["Initiatied On"," Treatment"], ["Treatment"," Completed"],["SVR Done"],["Treatment ","Successful"],["Treatment ","Failure"]],
			datasets: [
			{
				label : ["No. of Patients"],
				backgroundColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				],
				/*borderColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				],
				borderWidth: 1,*/
				data: [
				<?php echo $cascade->anti_hcv_screened;  ?>,
				<?php echo $cascade->anti_hcv_positive;  ?>,
				<?php echo $cascade->viral_load_tested;  ?>,
				<?php echo $cascade->viral_load_detected;  ?>,
				<?php echo $cascade->initiatied_on_treatment;  ?>,
				<?php echo $cascade->treatment_completed;  ?>,
				<?php echo $cascade->svr_done;  ?>,
				<?php echo $cascade->treatment_successful;  ?>,
				<?php echo $cascade->treatment_failure;  ?>
				]
			},
			]
		};

		var options = {
			responsive:true,
			maintainAspectRatio:false,
			animation: {
			duration: 0,
			onComplete: function () {

				/*var url_base64jp = document.getElementById("cascadeChart").toDataURL("image/jpg");*/
				
			

			// render the value of the chart above the bar
			//imagebackground();
			var ctx = this.chart.ctx;

			ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
			ctx.fillStyle = this.chart.config.options.defaultFontColor;
			ctx.textAlign = 'center';
			ctx.textBaseline = 'bottom';
			this.data.datasets.forEach(function (dataset) {
			for (var i = 0; i < dataset.data.length; i++) {
			var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
			ctx.fillText(dataset.data[i], model.x, model.y - 5);
			}
			});

			/*var url_base64jp = document.getElementById("cascadeChart").toDataURL('image/jpeg', 1);
				//document.getElementById("cascadeChartLink").href=url_base64jp;*/
			}},
			hover: {
			mode: false
			},
			legend:false,
legendCallback: legendcallback,
				layout: {
				padding: {
				left: 0,
				right: 0,
				top: 20,
				bottom: 0
				}},cornerRadius: 5,plugins:{

				},
						tooltips: {
						enabled: false
						},events:[],
						scales: {
						yAxes: [{
						stacked: true,
						ticks: {
						beginAtZero:true,
						min:0,
						maxTicksLimit:8,
						suggestedMin:0,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},
						gridLines: {
						color: "rgba(0, 0, 0, 0)",
						drawBorder: false,
						display:false,
						}
						}],
						xAxes: [{
						barThickness : 30,
						labelAutoFit: true,    
						stacked: true,
						ticks: {
						beginAtZero:true,
						maxTicksLimit:9,
						},
						gridLines: {
						color: "rgba(0, 0, 0, 0)",
						drawBorder: false,
						display:false,
						}
						}]
						
						}
						};
		ctx.fillStyle  = "rgb(255,255,255,1)";
		var myBarChart = new Chart(ctx, { plugins: [plugin],
			type   : 'bar',
			data   : data,
			options: options,

		});	
		$('#js-legend').html(myBarChart.generateLegend());
		
}
$(document).ready(function(){

$("#search_state").trigger('change');
//alert('rff');

});
$('#search_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');
					//$("#mstfacilitylogin").trigger('change');

				},
				error : function(error)
				{
					//alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
					$('#mstfacilitylogin').html(data);
					
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});






function drawTrendChart()
{
	
	var ctx  = $("#trendGraph");
var getmonthanalys = $('#getmonthanalys').val();
var hdng_text = document.getElementById("hcv_month_on_month");
//alert(getmonthanalys);
hdng_text.innerHTML="VL Detected";
document.getElementById("MONTH-ON-MONTH").href='<?= base_url() ?>Dashboard/trendviral_load_detected_export/'+getmonthanalys;
var data = {

labels: [
<?php
foreach ($trendviral_load_detected as $row) {
echo "'".$row->MONTHNAME."-".$row->YEAR."',";
}?>
],
datasets: [
{
label : ["No. of Patients"],
backgroundColor: [
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
],
borderColor: [
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
],
borderWidth: 1,
data: [
//"3","2","1","7","0","0","2","1","5","4","0","6",
<?php
foreach ($trendviral_load_detected as $row) {
echo "\"".floor($row->COUNT)."\",";
//echo 4;
}
?>
]
}
]
};


var options = {
responsive:true,
maintainAspectRatio:false,
animation: {
duration: 0,
onComplete: function () {

// render the value of the chart above the bar
var ctx = this.chart.ctx;
ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
ctx.fillStyle = this.chart.config.options.defaultFontColor;
ctx.textAlign = 'center';
ctx.textBaseline = 'bottom';
this.data.datasets.forEach(function (dataset) {
for (var i = 0; i < dataset.data.length; i++) {
var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
ctx.fillText(dataset.data[i], model.x, model.y - 5);
}
});

}},
legend:false,
legendCallback:legendcallback,
cornerRadius:5,
        layout: {
padding: {
left: 0,
right: 0,
top: 25,
bottom: 0
}},
         tooltips: {
                        enabled: false
                        },events:[],
scales: {
yAxes: [{
stacked: true,
ticks: {
beginAtZero:true,
min:0,
suggestedMin:0,
maxTicksLimit:8,
callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
suggestedMax:100
},
gridLines: {
display:false,
color: "rgba(0, 0, 0, 0)",
}
}],
xAxes: [{
barThickness : 30,
stacked: true,
ticks: {
beginAtZero:true
},
gridLines: {
display:false,
color: "rgba(0, 0, 0, 0)",
}
}]

}
};

var myBarChart = new Chart(ctx, {plugins: [plugin],
type: 'bar',
data: data,
options: options
});
$('#month_on_month-legend').html(myBarChart.generateLegend());

}

//ajax month

function month_on_month_ajax()
{

var selct_hdng = document.getElementById("getmonthanalys");
var name=selct_hdng.options[selct_hdng.selectedIndex].value;
document.getElementById("MONTH-ON-MONTH").href='<?= base_url() ?>Dashboard/trendviral_load_detected_export/'+name;
       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/trendviral_load_detected_ajax/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',flg:'true'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
   
       var ctx  = $("#trendGraph");
        var returned = JSON.parse(data);
        maxval=0;
        var size= Object.keys(returned.trendchart).length;
      /* console.log(returned.trendchart[0].MONTHNAME);
       console.log(returned.trendchart[1].MONTHNAME);*/
        var i=0;
        labels=[];
        countIni=[];
        var str='';
        for(i=size-1;i>=0;i--){
        str="'"+i.toString()+"'";
        //console.log(str);
        //console.log(returned.trendchart[i].MONTHNAME);
        labels.push(returned.trendchart[i].MONTHNAME+"-"+returned.trendchart[i].YEAR);
        countIni.push(returned.trendchart[i].COUNT);
        }

          for (i=0; i<=countIni.length;i++){
    if (parseInt(countIni[i])>maxval) {
        maxval=parseInt(countIni[i]);
    }
}
    var data = {
labels: labels,
datasets: [
{
label : ["No. of Patients"],
backgroundColor: [
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
],
borderColor: [
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
'rgba(53, 196, 249)',
],
borderWidth: 1,
data:countIni,
}
]
};

var options = {
responsive:true,
maintainAspectRatio:false,
animation: {
duration: 0,
onComplete: function () {

// render the value of the chart above the bar
var ctx = this.chart.ctx;
ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
ctx.fillStyle = this.chart.config.options.defaultFontColor;
ctx.textAlign = 'center';
ctx.textBaseline = 'bottom';
this.data.datasets.forEach(function (dataset) {
for (var i = 0; i < dataset.data.length; i++) {
var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
ctx.fillText(dataset.data[i], model.x, model.y - 5);
}
});

}},
legend:false,
legendCallback:legendcallback,
cornerRadius:5,
        layout: {
padding: {
left: 0,
right: 0,
top: 25,
bottom: 0
}},
         tooltips: {
                        enabled: false
                        },events:[],
scales: {
yAxes: [{
stacked: true,
ticks: {
beginAtZero:true,
min:0,
suggestedMin:0,
maxTicksLimit:8,
callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
suggestedMax:100
},
gridLines: {
display:false,
color: "rgba(0, 0, 0, 0)",
}
}],
xAxes: [{
barThickness : 30,
stacked: true,
ticks: {
beginAtZero:true
},
gridLines: {
display:false,
color: "rgba(0, 0, 0, 0)",
}
}]

}
};

var myBarChart = new Chart(ctx, {plugins: [plugin],
type: 'bar',
data: data,
options: options
});  
}
//console.log(data);
});
       if(name=='anti_hcv_screened')
{

head_name='Anti HCV Screened';
}
else if(name=='anti_hcv_positive')
{

head_name='Anti HCV Positive';
}else if(name=='viral_load_tested')
{

head_name='Viral Load Tested';
}else if(name=='viral_load_detected')
{

head_name='Viral Load Detected';
}else if(name=='initiatied_on_treatment')
{

head_name='Initiatied On Treatment';
}else if(name=='treatment_completed')
{
head_name='Treatment Completed';
}else if(name=='svr_done')
{

head_name='Anti HCV Positive';
}else if(name=='treatment_successful')
{

head_name='Treatment Successful';
}else if(name=='treatment_failure')
{

head_name='Treatment Failure';
}
      var hdng_text = document.getElementById("hcv_month_on_month");
//alert(getmonthanalys);
hdng_text.innerHTML= head_name;

}


function drawdiseasevlpositiveChart()
{
	var ctx   = $("#diseasevlpositive");
	<?php $totalvlde_notded =  $cascade->viral_load_detected - ($cirrhotic_details->non_cirrhotic+$cirrhotic_details->compensated_cirrhotic+$cirrhotic_details->decompensated_cirrhotic); ?>
	//var total = <?php //echo ($cirrhotic_details->non_cirrhotic+$cirrhotic_details->compensated_cirrhotic+$cirrhotic_details->decompensated_cirrhotic+$cirrhotic_details->cirrhotic_status_notavaliable); ?>;
	var total = <?php echo ($cirrhotic_details->non_cirrhotic+$cirrhotic_details->compensated_cirrhotic+$cirrhotic_details->decompensated_cirrhotic+$totalvlde_notded); ?>;
	
	var data  = {
		labels: ["Non Cirrhotic", "Compensated Cirrhotic","Decompensated Cirrhotic","Status not available"],
		datasets: [
		{
			backgroundColor: [
			'#35c4f9',
			'#18bb4b',
			'#ffb35e',
			'#f4511e',
			],
			hoverBackgroundColor: [
			'#35c4f9',
			'#18bb4b',
			'#ffb35e',
			'#f4511e',
			],
			borderWidth: 1,
			data: 
			[
			<?php echo $cirrhotic_details->non_cirrhotic; ?>, 
			<?php echo $cirrhotic_details->compensated_cirrhotic; ?>,
			<?php echo $cirrhotic_details->decompensated_cirrhotic; ?>,
			<?php echo $totalvlde_notded; //$cirrhotic_details->totalvlde_notded; ?>]
		}
		]
	};

	var options = {
animation    : {
animateRotate: true,
duration     : 2000,
onComplete   : function(){
/*var url_base64jp = document.getElementById("diseasevlpositive").toDataURL("image/png");
document.getElementById("diseasevlpositiveLink").href=url_base64jp;*/
},
},
responsive:true,
maintainAspectRatio:false,
legend:false,
legendCallback:pie_legendcallback,
        layout: {
padding: {
left: 35,
right: 0,
top: 40,
bottom:30
}
},zoomOutPercentage:25,

events:[],cutoutPercentage:80,
plugins: {
doughnutlabel: {
        labels: [
          {
            text:total,
            font: {
              size: '30'
            }, color: '#505050',
          }]},
title: false,
outlabels: {
borderRadius: 0, // Border radius of Label
borderWidth: 1, // Thickness of border
color: 'white', // Font color
display: true,
lineWidth: 3, // Thickness of line between chart arc and Label
padding: 3,
stretch: 10, // The length between chart arc and Label
font: {
resizable: true,
minSize: 12,
maxSize: 18
},
text:function(context) {
var index = context.dataIndex;
var num=context.dataset.data[index] || 0;
var value = "%p.2 ("+(context.dataset.data[index])+")";
if(num==0||isNaN(num)){
return "";
}
else{
return value;
}

},
textAlign: "center"
}
},rotation: (-0.5*Math.PI) - (95/180 * Math.PI)
/*tooltips: {
//enabled: false,
callbacks: {
label: function(tooltipItem, data) {
var percentage = Math.round(((data.datasets[0].data[tooltipItem.index]/total)*100)*100)/100;
return data.datasets[0].data[tooltipItem.index]+" , "+percentage+"%";
}
}
}, showAllTooltips: true*/
};

/*var myBarChart = new Chart(ctx, {
type   : 'pie',
data   : data,
options: options
});*/

var chart2 = new Chart(ctx, {plugins:[roundedges,plugin],
type: "doughnut",
indexLabelPlacement: "outside",
showInLegend: true,
data: data,
options: options
  });
$('#vl-legend').html(chart2.generateLegend());
}


function drawregimenWisedistributionChart(){

	var ctx = $("#regimenWisedistribution");

	//var total = <?php  //echo  (isset($regimen[0]))?($regimen[0]->COUNT):'' +  (isset($regimen[1]))?($regimen[1]->COUNT):'' + (isset($regimen[2]))?($regimen[2]->COUNT):'' + (isset($regimen[3]))?($regimen[3]->COUNT):''; ?>;
	//var total = <?php //echo 8; ?>;

	/*var total = <?php echo ($regdistribution->regimenwisedistribution_reg1+ $regdistribution->regimenwisedistribution_reg2+$regdistribution->regimenwisedistribution_reg3+$regdistribution->regimenwisedistribution_reg24);?>;*/

	<?php $otherregimen = ($cascade->initiatied_on_treatment)-($regdistribution->regimenwisedistribution_reg1+$regdistribution->regimenwisedistribution_reg2+$regdistribution->regimenwisedistribution_reg3+$regdistribution->regimenwisedistribution_reg24); ?>

		var data = {
			labels:[["Initiated","on ","Treatment:","All", "Regimen"], ["Reg 1:","SOF + DCV"], ["\nReg 2:","SOF + VEL"], ["Reg 3A:","SOF+VEL+","Ribavirin"],["Reg 3B:","SOF+VEL","(24 weeks)"],["Other","Regimens"]],
				datasets: [
				{	label :["No of Patients"],
				data: [
				<?php echo $cascade->initiatied_on_treatment; ?>,
				<?php echo $regdistribution->regimenwisedistribution_reg1; ?>,
				<?php echo $regdistribution->regimenwisedistribution_reg2; ?>,
				<?php echo $regdistribution->regimenwisedistribution_reg3; ?>,
				<?php echo $regdistribution->regimenwisedistribution_reg24; ?>,
				<?php echo $otherregimen; ?>,			
				],
				backgroundColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				],
				hoverBackgroundColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				],
				/*borderColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				],
				borderWidth: 1,*/
			}]
		};
		var options = {
			responsive:true,
			maintainAspectRatio:false,
			animation: {
			duration: 0,
			onComplete: function () {
				
			// render the value of the chart above the bar
			var ctx = this.chart.ctx;
			ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
			ctx.fillStyle = this.chart.config.options.defaultFontColor;
			ctx.textAlign = 'center';
			ctx.textBaseline = 'bottom';
			this.data.datasets.forEach(function (dataset) {
			for (var i = 0; i < dataset.data.length; i++) {
			var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
			ctx.fillText(dataset.data[i], model.x, model.y - 5);
			}
			});
			}},

			legend:false,
			legendCallback:legendcallback,
			cornerRadius:5,
			layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 0
				}},
			/*tooltips: {
				enabled: false,
				callbacks: {
					label: function(tooltipItem) {

						var percentage = Math.round((tooltipItem.yLabel/total)*100);
						return tooltipItem.yLabel+" , "+percentage+"%"; 
					},
				}
			},*/events:[],

			scales : {

				xAxes: [{
barThickness : 30,
autoSkip:false,
offset: true,
stacked:true,
ticks: {
beginAtZero:true,
},
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
}
}],

				yAxes : [{
					gridLines: {
						color: "rgba(0, 0, 0, 0)",
						display:false,
					},ticks: {
						//stepSize:20,
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},
				}],
			},
			tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= value + ' %' %>",
		};
		var myBarChart = new Chart(ctx,{plugins: [plugin],
			type: 'bar',
			data: data,
			options: options
		});
		$('#regimenWisedistribution-legend').html(myBarChart.generateLegend());
}

/**/

function drawsuccessratiosvrtestChart()
{
	var ctx   = $("#successratiosvrtest");
	<?php $totalvaldata = $cascade->svr_done - ($cascade->treatment_successful+$cascade->treatment_failure); ?>
	var total = <?php echo $cascade->treatment_successful+$cascade->treatment_failure + $totalvaldata; ?>;
	
	var data  = {
		labels: ["Treatment Successful", "Treatment Failure","Status not available"],
		datasets: [
		{
			backgroundColor: [
			'#18bb4b',
			'#f4511e',
			'#35c4f9',
			
			],
			hoverBackgroundColor: [
			'#18bb4b',
			'#f4511e',
			'#35c4f9',
			
			],
			borderWidth: 1,
			data: 
			[
			<?php echo $cascade->treatment_successful; ?>, 
			<?php echo $cascade->treatment_failure; ?>,
			<?php echo  $totalvaldata; ?>]


		}
		]
	};

	var options = {
		responsive:true,
		maintainAspectRatio:false,
		animation    : {
			animateRotate: true,
			duration     : 2000,
		},
		/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 50
            }
        },*/
        legend:false,
        legendCallback:pie_legendcallback,
        zoomOutPercentage:5,
        layout: {
			padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 30
			}
		},cutoutPercentage:80,
		events:[],rotation: (-0.5*Math.PI) - (45/180 * Math.PI),

				plugins: {
					doughnutlabel: {
        labels: [
          {
            text:total,
            font: {
              size: '32'
            }, color: '#505050',
          }]},
				title: false,
				outlabels: {
				borderRadius: 0, // Border radius of Label
				borderWidth: 1, // Thickness of border
				color: 'white', // Font color
				display: true,
				lineWidth: 3, // Thickness of line between chart arc and Label
				padding: 3,
				stretch: 10, // The length between chart arc and Label
				font: {
				resizable: true,
				minSize: 12,
				maxSize: 18
				},
				text:function(context) {
				var index = context.dataIndex;
				var num=context.dataset.data[index] || 0;
				var value = "%p.2 ("+(context.dataset.data[index])+")";
				if(num==0||isNaN(num)){
					return "";	
				}
				else{
					return value;
				}
				
				},
				textAlign: "center"
				}
				},
		/*tooltips: {
			//enabled: false,
			callbacks: {
				label: function(tooltipItem, data) {
					var percentage = Math.round(((data.datasets[0].data[tooltipItem.index]/total)*100)*100)/100;
					return data.datasets[0].data[tooltipItem.index]+" , "+percentage+"%"; 
				}
			}
		}, showAllTooltips: true,
		events:[],
*/	};

	/*var myBarChart = new Chart(ctx, {
		type   : 'pie',
		data   : data,
		options: options
	});*/

	var chart2 = new Chart(ctx, {plugins:[roundedges,plugin],
    type: "doughnut",
    data: data,
    options: options
  });
	$('#successratiosvrtest-legend').html(chart2.generateLegend());
}

function drawregimenWisetreatmentChart(){


	var ctx = $("#regimenWisetreatment");

var data = {
labels:[/*["Initiated","on Treatment:","All Regimen"],*/ ["Reg 1: ","SOF + DCV"], ["\nReg 2: ","SOF + VEL"], ["Reg 3A:","SOF + VEL+","Ribavirin"],["Reg 3B:","SOF+VEL","(24 weeks)"],["Other","Regimens"]],
datasets: [
{ label: "Treatment Successful",
data: [
/*<?php //echo isset($regimensucess[0]) ? $regimensucess[0]->COUNT : '0'; ?>,*/
<?php echo  $regdistribution_svrtested->reg_pass1; ?>,
<?php echo  $regdistribution_svrtested->reg_pass2; ?>,
<?php echo  $regdistribution_svrtested->reg_pass3; ?>,
<?php echo  $regdistribution_svrtested->reg_pass24; ?>,
<?php echo  $regdistribution_svrtested->reg_sucess_other; ?>,
],
backgroundColor: [
'#18bb4b',
'#18bb4b',
'#18bb4b',
'#18bb4b',
'#18bb4b',


],
hoverBackgroundColor: [
'#18bb4b',
'#18bb4b',
'#18bb4b',
'#18bb4b',
'#18bb4b',
],datalabels: {
anchor: 'center',
align: 'center',
}
/*borderColor: [
'rgba(77,175,82)',
'rgba(77,175,82)',
'rgba(77,175,82)',
'rgba(77,175,82)',
'rgba(77,175,82)',
],
borderWidth: 1,*/
},
{
label: "Treatment Failure",
data : [
/*<?php if(isset($regimenfailure[0])?($regimenfailure[0]->COUNT):''==''){ echo '0';}else{ echo isset($regimenfailure[1])?($regimenfailure[1]->COUNT):'';}   ?>,*/
<?php echo  $regdistribution_svrtested->reg_fail1; ?>,
<?php echo  $regdistribution_svrtested->reg_fail2; ?>,
<?php echo  $regdistribution_svrtested->reg_fail3; ?>,
<?php echo  $regdistribution_svrtested->reg_fail24; ?>,
<?php echo  $regdistribution_svrtested->reg_fail_other; ?>,
],
backgroundColor: [
'#f4511e',
'#f4511e',
'#f4511e',
'#f4511e',
'#f4511e',
],
hoverBackgroundColor: [
'#f4511e',
'#f4511e',
'#f4511e',
'#f4511e',
'#f4511e',
],datalabels: {
anchor: 'center',
align: 'center',
}
/*borderColor: [
'rgba(239,83,80)',
'rgba(239,83,80)',
'rgba(239,83,80)',
'rgba(239,83,80)',
'rgba(239,83,80)',
],
borderWidth: 1,*/
}
]
};
var options = {
responsive:true,
maintainAspectRatio:false,
animation: {
duration: 2000,
onComplete: function () {
// render the value of the chart above the bar
/*var ctx = this.chart.ctx;
ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
ctx.fillStyle = this.chart.config.options.defaultFontColor;
ctx.textAlign = 'center';
ctx.textBaseline = 'bottom';
this.data.datasets.forEach(function (dataset) {
for (var i = 0; i < dataset.data.length; i++) {
var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
ctx.fillText(dataset.data[i], model.x, model.y - 5);
}*/
}
},
plugins:{
  stacked100: { enable: true,precision: 2 },
  /*labels:{
  render: function (args) {
      if(isNaN(args.percentage))
      {
      return "0%";
      }
      else{
      return args.percentage+'%';
      }
    }
  }*/
  datalabels: {
color: 'black',
font: {
       weight:'bold',
          size: 10,
        },
formatter: function(value, context) {
if(isNaN(context.dataset.data[context.dataIndex]))
{
return null;
}
else{
if (context.dataset.data[context.dataIndex]==0) {
return null;
}
else{


return context.dataset.data[context.dataIndex]+"%";
}
}

}, padding: {
bottom: 32}
//formatter: Math.round
},
 },
     
/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 25
            }
        },*/
        legend:false,
        legendCallback:legendcallback_per,
        cornerRadius:5,
        layout: {
padding: {
left: 0,
right: 0,
top: 10,
bottom: 0
}},
/*tooltips : {
enabled: false,
callbacks: {
label    : function(tooltipItem, data) {

var total_stack1 = data.datasets[0].data[0] + data.datasets[1].data[0];

var total_stack2 = data.datasets[0].data[1] + data.datasets[1].data[1];
var total_stack3 = data.datasets[0].data[2] + data.datasets[1].data[2];
var total_stack4 = data.datasets[0].data[3] + data.datasets[1].data[3];
var total_stack5 = data.datasets[0].data[4] + data.datasets[1].data[4];



if(tooltipItem.index == 0)
{

var percentage = Math.round(((tooltipItem.yLabel/total_stack1)*100)*100)/100;
return tooltipItem.yLabel+" , "+percentage+"%";
}
else if(tooltipItem.index == 1)
{

var percentage = Math.round(((tooltipItem.yLabel/total_stack2)*100)*100)/100;
return tooltipItem.yLabel+" , "+percentage+"%";
}
else if(tooltipItem.index == 2)
{

var percentage = Math.round(((tooltipItem.yLabel/total_stack3)*100)*100)/100;
return tooltipItem.yLabel+" , "+percentage+"%";
}
else if(tooltipItem.index == 3)
{

var percentage = Math.round(((tooltipItem.yLabel/total_stack4)*100)*100)/100;
return tooltipItem.yLabel+" , "+percentage+"%";
}
else if(tooltipItem.index == 4)
{

var percentage = Math.round(((tooltipItem.yLabel/total_stack5)*100)*100)/100;
return tooltipItem.yLabel+" , "+percentage+"%";
}
}
}
}*/events:[],
scales: {
yAxes: [{
stacked: true,
ticks: {

beginAtZero:true,
maxTicksLimit:8
},
/*ticks: {
steps : 10,
stepValue : 10,
max : 100,
callback: function(tick) {
return tick.toString() + '%';
},
},*/
gridLines: {
display:false,
color: "rgba(0, 0, 0, 0)",
},
 scaleLabel: {
        display: true,
        labelString: 'In Percentage (%)'
      },
}],
xAxes: [{
barThickness : 35,
stacked: true,
ticks: {
beginAtZero:true
},
gridLines: {
display:false,
color: "rgba(0, 0, 0, 0)",
}
}]

}
};
var myBarChart = new Chart(ctx,{plugins: [ChartDataLabels,plugin],
type   : 'bar',
data   : data,
options: options
});
$('#svr_tested_rate-legend').html(myBarChart.generateLegend());
}

function drawlossofFollowupChart(){

	var ctx = $("#lossofFollowup");
	var data = {
		labels: [
		["Viral Load"],
		["1st"," Dispensation"],
		["2nd ","Dispensation"],
		["3rd ","Dispensation"],
		["4th"," Dispensation"],
		["5th ","Dispensation"],
		["6th ","Dispensation"],
		["SVR"]
		],
		// for each stage
		datasets: [
		{	label: "Number of patients eligible",
		data: [
		<?php echo $eligible_stage->follow_viral_load; ?>,
		<?php echo $eligible_stage->follow_1St_dispensation; ?>,
		<?php echo $eligible_stage->follow_2St_dispensation; ?>,
		<?php echo $eligible_stage->follow_3St_dispensation; ?>,
		<?php echo $eligible_stage->follow_4St_dispensation; ?>,
		<?php echo $eligible_stage->follow_5St_dispensation; ?>,
		<?php echo $eligible_stage->follow_6St_dispensation; ?>,
		<?php echo $eligible_stage->follow_SVR; ?>,
		],
		backgroundColor: [
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		],
		hoverBackgroundColor: [
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		],
		/*borderColor: [
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		],
		borderWidth: 1,*/
	},
	{	
		//for each Stage
		label: "Number of patients LTFU ",
		data : [
		<?php echo $ltfu_stage->loss_of_follow_viral_load; ?>,
		<?php echo $ltfu_stage->loss_of_follow_1St_dispensation; ?>,
		<?php echo $ltfu_stage->loss_of_follow_2St_dispensation; ?>,
		<?php echo $ltfu_stage->loss_of_follow_3St_dispensation; ?>,
		<?php echo $ltfu_stage->loss_of_follow_4St_dispensation; ?>,
		<?php echo $ltfu_stage->loss_of_follow_5St_dispensation; ?>,
		<?php echo $ltfu_stage->loss_of_follow_6St_dispensation; ?>,
		<?php echo $ltfu_stage->loss_of_follow_SVR; ?>,
		],
		backgroundColor: [
		'rgba(229, 106, 84, 1)',
		'rgba(229, 106, 84, 1)',
		'rgba(229, 106, 84, 1)',
		'rgba(229, 106, 84, 1)',
		'rgba(229, 106, 84, 1)',
		'rgba(229, 106, 84, 1)',
		'rgba(229, 106, 84, 1)',
		'rgba(229, 106, 84, 1)',
		],
		hoverBackgroundColor: [
		'rgba(229, 106, 84, 1)',
		'rgba(229, 106, 84, 1)',
		'rgba(229, 106, 84, 1)',
		'rgba(229, 106, 84, 1)',
		'rgba(229, 106, 84, 1)',
		'rgba(229, 106, 84, 1)',
		'rgba(229, 106, 84, 1)',
		'rgba(229, 106, 84, 1)',
		],
		/*borderColor: [
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		],
		borderWidth: 1,*/
	}
	]
};

var myBarChart = new Chart(ctx,{plugins:[plugin],
	type   : 'bar',
	data   : data,
	options: {
		responsive:true,
		maintainAspectRatio:false,
		animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});
				}},
    barValueSpacing: 20,
    /*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 75
            }
        },*/
        legend:false,
        legendCallback:legendcallback_per,
        cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 0
				}},
            tooltips: {
                        enabled: false
                        },events:[],
    scales: {
        xAxes: [{
        	maxBarThickness : 30,
        	barPercentage: 0.7,
        categoryPercentage: .6,
            gridLines: {
            	display:false,
                color: "rgba(0, 0, 0, 0)",
            }
        }],
        yAxes: [{
        	ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},
            gridLines: {
            	display:false,
                color: "rgba(0, 0, 0, 0)",
            }
            
        }]
    }
  }
	/*options: {
		animation: {
				duration: 0,
				onComplete: function () {
				var cascade_url_base64jp = document.getElementById("lossofFollowup").toDataURL("image/png");
				document.getElementById("lossofFollowupLink").href=cascade_url_base64jp;
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'top';
				ctx.textBaseline = 'top';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});
				}},

    barValueSpacing: 20,
    legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 15
            }
        },
    scales: {
				yAxes: [{
					stacked: true,
					ticks: {
						beginAtZero:true
					},
					gridLines: {
						color: "rgba(0, 0, 0, 0)",
					}
				}],
				xAxes: [{
					barThickness : 60,
					stacked: true,
					ticks: {
						beginAtZero:true
					},
					gridLines: {
						color: "rgba(0, 0, 0, 0)",
					}
				}]

			}
  }*/
});
$('#ltfu-legend').html(myBarChart.generateLegend());


}

/**/
function drawlossfollowspecialcase()
	{
		var ctx  = $("#lossfollowspecialcase");
		var total = <?php echo $special_case->loss_to_follow_patient_referred_to_mtc + $special_case->loss_to_follow_patient_reporting_at_mtc + ($special_case->loss_to_follow_patient_referred_to_mtc-$special_case->loss_to_follow_patient_reporting_at_mtc); ?>;
		var data = {
			labels: [["Patient referred ","to MTC"],  ["Patient Initiatied On ","Treatment","1st ","Dispensation"], ["Lost to ","Follow up"],],
			datasets: [
			{
				label : ["No. of Patients"],
				backgroundColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				
				],
				/*borderColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				],
				borderWidth: 1,*/
				data: [
				<?php echo (count($special_case) > 0)?($special_case->loss_to_follow_patient_referred_to_mtc):'0'; ?>,
				<?php echo (count($special_case) > 0)?($special_case->loss_to_follow_patient_reporting_at_mtc):'0'; ?>,
				<?php echo (count($special_case) > 0)?($special_case->loss_to_follow_patient_referred_to_mtc-$special_case->loss_to_follow_patient_reporting_at_mtc):'0'; ?>,
				]
			},
			]
		};

		var options = {
		responsive:true,
		maintainAspectRatio:false,
			animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});
				}},
			/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 60
            }
        },*/
			legend: false,
			legendCallback:legendcallback,
			cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 0
				}},
			tooltips: {
				enabled: false,
		/*	callbacks: {
				label: function(tooltipItem, data) {
					var percentage = Math.round(((data.datasets[0].data[tooltipItem.index]/total)*100)*100)/100;
					return percentage+"%"; 
				}
			}*/
		},events:[],
			scales: {
				yAxes: [{
					stacked: true,
					ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},
					gridLines: {
						display:false,
						color: "rgba(0, 0, 0, 0)",
					}
				}],
				xAxes: [{
					barThickness : 30,
					stacked: true,
					ticks: {
						beginAtZero:true
					},
					gridLines: {
						display:false,
						color: "rgba(0, 0, 0, 0)",
					}
				}]

			}
		};

		ctx.fillStyle  = "rgb(255,255,255,1)";
		var myBarChart = new Chart(ctx, {plugins:[plugin],
			type   : 'bar',
			data   : data,
			options: options,

		});	
		$('#ltfu_spCase-legend').html(myBarChart.generateLegend());
	}


function drawadherencepercentageChart()
	{
		var ctx  = $("#adherencepercentage");

		var total = <?php echo ((isset($adherenceper[0])?($adherenceper[0]->COUNT):'')+(isset($adherenceper[1])?($adherenceper[1]->COUNT):'')+(isset($adherenceper[2])?($adherenceper[2]->COUNT):'')+(isset($adherenceper[3])?($adherenceper[3]->COUNT):'')+ (isset($adherenceper[4])?($adherenceper[4]->COUNT):'')); ?>
		//alert(total);
		var data = {
			labels: [["Reg 1: ","SOF + DCV"],  ["Reg 2: ","SOF + VEL"], ["Reg 3A:","SOF + VEL+","Ribavirin"], ["Reg 3B:","SOF+VEL","(24 weeks)"],["Other","Regimens"]],
			datasets: [
			{
				label : ["Adherence Percentage"],
				backgroundColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				],
				/*borderColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				],
				borderWidth: 1,*/
				data: [
				<?php echo isset($adherenceper[0])?round(($adherenceper[0]->COUNT),2):'0'; ?>,
				<?php echo isset($adherenceper[1])?round(($adherenceper[1]->COUNT),2):'0'; ?>,
				<?php echo isset($adherenceper[2])?round(($adherenceper[2]->COUNT),2):'0'; ?>,
				<?php echo isset($adherenceper[3])?round(($adherenceper[3]->COUNT),2):'0'; ?>,
				<?php echo isset($adherenceper[4])?round(($adherenceper[4]->COUNT),2):'0'; ?>,
				]
			},
			]
		};

	var options = {
responsive: true,
maintainAspectRatio:false,
       plugins: {
       labels: {
render: function (args) {  
 return args.value+"%";
}
 
  //Calculate percent
}},
animation: {
duration: 0,
onComplete: function () {
// render the value of the chart above the bar
/*var ctx = this.chart.ctx;
ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
ctx.fillStyle = this.chart.config.options.defaultFontColor;
ctx.textAlign = 'center';
ctx.textBaseline = 'bottom';
this.data.datasets.forEach(function (dataset) {
for (var i = 0; i < dataset.data.length; i++) {
var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
ctx.fillText(dataset.data[i], model.x, model.y - 5);
}
});*/
}},
/*legend: {
           display: true,
           position: 'bottom',
           labels: {
               boxWidth: 20,
               fontColor: '#111',
               padding: 45
           }
       },*/
       legend: false,
			legendCallback:legendcallback,
			cornerRadius:5,
       layout: {
padding: {
left: 0,
right: 0,
top: 25,
bottom: 0
}},
/*tooltips: {
enabled: false,
callbacks: {
label: function(tooltipItem, data) {
var percentage = Math.round(((data.datasets[0].data[tooltipItem.index]/total)*100)*100)/100;
return percentage+"%";
}
}
},*/events:[],
scales: {
yAxes: [{
stacked: true,
ticks: {//stepSize:20,
						beginAtZero:true,
						min: 0,
						max: 100,
						suggestedMax:100,
						maxTicksLimit:8,
						callback: function (ticks) {
							//alert(value);
							return ticks;
					},
						},scaleLabel: {
        display: true,
        labelString: 'In Percentage (%)'
      },

gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
}
}],
xAxes: [{
barThickness : 30,
stacked: true,
ticks: {
beginAtZero:true
},
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
}
}]

}
};

		ctx.fillStyle  = "rgb(255,255,255,1)";
		var myBarChart = new Chart(ctx, {plugins:[plugin],
			type   : 'bar',
			data   : data,
			options: options,

		});	
		$('#ltu_adherence-legend').html(myBarChart.generateLegend());
	}


function drawtimeanalysisChart(){

	var ctx = $("#timeanalysis");

	var data = {
		labels: [
		["Anti-HCV Result to ","Viral Load Test"],
		["VL Test to Delivery ","of Viral Load ","Result to Patient"],
		["Delivery of Result ","to Patient to ","Prescription of"," Baseline test"],
		["Prescription of ","Baseline testing to"," Date of Baseline Tests"],
		//["Date of Baseline ","Tests to Result of"," Baseline Tests"],
		["Result of Baseline"," Tests to Initiation"," of Treatment"],
		["Treatment Completion ","to SVR"]
		],
		datasets: [
		{	label: "Average Actual Days Taken",
		data: [
		<?php echo isset($tat_anti_hcv_result_to_viral[0]->Count)?round($tat_anti_hcv_result_to_viral[0]->Count):'0'; ?>,
		<?php echo isset($tat_viral_load_test_to_delivery[0]->Count)?round($tat_viral_load_test_to_delivery[0]->Count):'0'; ?>,
		<?php echo isset($tat_delivery_result_to_patient_prescription[0]->Count)?round($tat_delivery_result_to_patient_prescription[0]->Count):'0';?>,
		<?php echo isset($tat_prescription_baseline_testing[0]->Count)?round($tat_prescription_baseline_testing[0]->Count):'0'; ?>,
		/*<?php //echo isset($tat_date_of_baseline_tests_result[0]->Count)?round($tat_date_of_baseline_tests_result[0]->Count):'0'; ?>,*/
		<?php echo isset($tat_result_of_baseline_tests_to_initiation[0]->Count)?round($tat_result_of_baseline_tests_to_initiation[0]->Count):'0';  ?>,
		<?php echo isset($tat_treatment_completion_to_SVR[0]->Count)?round($tat_treatment_completion_to_SVR[0]->Count):'0';  ?>,
		],
		backgroundColor: [
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		],
		hoverBackgroundColor: [
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		],
		/*borderColor: [
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		],
		borderWidth: 1,*/
	},
	/*{	
		label: TAT+" TAT Median",
		data : [
		<?php//echo 0;//$anty_hcv_to_tesult->median; ?>,
		<?php //echo 0; ?>,
		<?php //echo 0; ?>,
		<?php //echo 0; ?>,
		<?php //echo 0; ?>,
		<?php //echo 0; ?>,
		<?php //echo 0; ?>,
		<?php //echo 0; ?>,
		],
		backgroundColor: [
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		],
		hoverBackgroundColor: [
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		],
		borderColor: [
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		],
		borderWidth: 1,
	}*/
	]
};

var myBarChart = new Chart(ctx,{plugins:[plugin],
	type   : 'bar',
	data   : data,
	options: {
		responsive:true,
		maintainAspectRatio:false,
		animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});
				}},
    barValueSpacing: 0,
    /*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 75
            }
        },*/
        legend: false,
			legendCallback:legendcallback_per,
			cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 0
				}},
            tooltips: {
                        enabled: false
                        },events:[],
     scales: {
        xAxes: [{
        	 barThickness:30,
            gridLines: {
            	display:false,
                color: "rgba(0, 0, 0, 0)",
            },
        }],
        yAxes: [{
        	ticks: {
						beginAtZero:true,
						min:0,
						//stepSize:20,
						maxTicksLimit:8,
						suggestedMin:1,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},
            gridLines: {
            	display:false,
                color: "rgba(0, 0, 0, 0)",
            }   
        }]
    }
  }
});
$('#Turnaround-legend').html(myBarChart.generateLegend());
}


function drawtestefficienceChart()
{
	var ctx   = $("#testefficience");
	
	var total = <?php echo $vl_samples_accepted_rejected->vl_samples_accepted + $vl_samples_accepted_rejected->vl_samples_rejected; ?>;
	
	var data  = {
		labels: [["Samples Accepted"], ["Samples  Rejected"]],
		datasets: [
		{
			backgroundColor: [
			'rgba(34,139,34)',
			'rgba(230, 0, 0)',
			
			],
			hoverBackgroundColor: [
			'rgba(34,139,34)',
			'rgba(230, 0, 0)',
			
			],
			borderWidth: 1,
			data: 
			[
			<?php echo $vl_samples_accepted_rejected->vl_samples_accepted; ?>, 
			<?php echo $vl_samples_accepted_rejected->vl_samples_rejected; ?>]
		}
		]
	};

	var options = {
		animation    : {
			animateRotate: true,
			duration     : 2000,
			onComplete   : function(){
		
			},
		},
		legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 15
            }
        },
		elements: {
					center: {
					text: total,
					color: '#FF6384', // Default is #000000
					fontStyle: 'Arial', // Default is Arial
					sidePadding: 12 // Defualt is 20 (as a percentage)
				}
			},

		tooltips: {
			callbacks: {
				label: function(tooltipItem, data) {
					var percentage = Math.round(((data.datasets[0].data[tooltipItem.index]/total)*100)*100)/100;
					return data.datasets[0].data[tooltipItem.index]+" , "+percentage+"%"; 
				}
			}
		},events:[],

	};

	/*var myBarChart = new Chart(ctx, {
		type   : 'pie',
		data   : data,
		options: options
	});*/

	var chart2 = new Chart(ctx, {
    type: "doughnut",
    data: data,
    options: options
  });
}


function drawFactorAnalysisChart()
{

	var ctx = $("#FactorAnalysis");
	
	var total = <?php $total=0;foreach ($side_effects as  $value){  $total+=$value->count;}
			 echo $total; ?>;

			 var maxtotal = <?php $maxtotal=0; foreach ($side_effects as   $key=>$val){ 
			 	if ($val->count > $maxtotal) {
        		$maxtotal = $val->count;
   				 } } echo $maxtotal; ?>;
			 

	var data = {
		labels: [
<?php foreach ($side_effects as  $value){ ?>
	

		"<?php echo $value->LookupValue; ?>",
<?php } ?>

		],
		datasets: [
		{	
			label : ["No. of Patients"],
			data: [
<?php foreach ($side_effects as  $value){ ?>
			"<?php echo $value->count; ?>",
			<?php }?>
			
			],
			backgroundColor: [
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			'rgba(53, 196, 249,1)',
			
			]
		}]
	};
	var options = {
	responsive:true,
	maintainAspectRatio:false,
	/*plugins:{
		labels:[{
			render:function(args){
				if(args.value==0 || total==0){
					return "0 (0%)";
				}
				else{
					var per=((args.value*100)/total).toFixed(2);
					if(per==0 || isNaN(per)){
					return "0 (0%)";
				}
					return args.value+" ("+per+"%)";
				}
			}
		}]
	},*/
	plugins:{
	datalabels: {
						color: '#505050',
						rotation:0,
						align:'end',
						anchor:'end',
						offset:2,
						 font: {
         weight:'400',
         //family:'Arial',
         family:'Helvetica',
          size: 12.4,
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0 (0%)";
							}
							else{
							var per=(context.dataset.data[context.dataIndex]*100)/total;
							if(per==null || isNaN(per)){
								return "0 (0%)";
							}
							else{
								return context.dataset.data[context.dataIndex]+"("+per.toFixed(2)+"%)";
							}
							}
							
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},
			animation: {
			duration: 0,
			onComplete: function () {
			
				
			
			// render the value of the chart above the bar
			var ctx = this.chart.ctx;
			ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
			ctx.fillStyle = this.chart.config.options.defaultFontColor;
			ctx.textAlign = 'center';
			ctx.textBaseline = 'bottom';
			/*this.data.datasets.forEach(function (dataset) {
			for (var i = 0; i < dataset.data.length; i++) {
			var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
			ctx.fillText(dataset.data[i], model.x, model.y - 5);
			}
			});*/
			
			}},

			/*legend: {
				display: true,
				position: 'bottom',
				labels: {
				boxWidth: 20,
				fontColor: '#111',
				padding: 45
				}
				},
        legend: {
				display: true,
				position: 'bottom',
				labels: {
				boxWidth: 20,
				fontColor: '#111',
				padding: 55
				}
				},*/
				legend: false,
				legendCallback:legendcallback,
				cornerRadius:5,
				layout: {
				padding: {
				left: -2,
				right: 40,
				top: 0,
				bottom: 0
				}},
			
			tooltips: {
				enabled: false,
				callbacks: {
					label: function(tooltipItem) {

						var percentage = Math.round((tooltipItem.yLabel/total)*100);
						return tooltipItem.yLabel+" , "+percentage+"%"; 
					},
				},
			},events:[],
				scales: {
				xAxes: [{
					stacked: true,
					maxBarThickness : 15,
					cutoutPercentage:1.0,
					barPercentage:1.0,
					ticks: {
						beginAtZero:true,
						min:0,
						maxTicksLimit:10,
						suggestedMin:1,
						//stepSize:20,
						max:maxtotal+500,
						 callback: function(value, index, values) {
	
                    if (Math.floor(value) === value) {
                    	if(value==total+500){
                    		return "";
                    	}
                        return value;
                    }
                },
						suggestedMax:100
					},
					gridLines: {
						display:false,
						color: "rgba(0, 0, 0, 0)",
					}
				}],
				yAxes: [{
					maxBarThickness : 15,
					cutoutPercentage:1.0,
					barPercentage:1.0,
					stacked: true,
					ticks :{
					autoSkip :false,
					
					},
					gridLines: {
						display:false,
						color: "rgba(0, 0, 0, 0)",
					}
				}]

			},
			tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= value + ' %' %>",
		};
	var myBarChart = new Chart(ctx,{plugins: [ChartDataLabels,plugin],
		type: 'horizontalBar',
		data: data,
		options: options
	});
	$('#Risk_factor-legend').html(myBarChart.generateLegend());
}






function drawagewisefactoranyChart()
{
var ctx = $("#agewisefactorany");

var total = <?php echo count($age_gpdata); ?>;

var data = {
labels: [

<?php foreach ($age_gpdata as  $value){ ?>
	
"<?php echo $value->LookupValue; ?>",

<?php } ?>
],
datasets: [
{
data: [
<?php foreach ($age_gpdata as  $value){ ?>

"<?php echo $value->count; ?>",

<?php } ?>

],
backgroundColor: [
			'#ffcece',
			'#745c97',
			'#4baea0',
			'#7fe7cc', 
			'#e56a54',
			'#ffb35e',
			'#69779b',
			'#CB4335',
			'#D4AC0D',
			'#283747',
			]
}]
};

var options = {
	responsive:true,
	maintainAspectRatio:false,
	animation    : {
			animateRotate: true,
			duration     : 2000,
		
		},
tooltips: false,
layout: {
padding: {
				left:0,
				right: 10,
				top: 10,
				bottom: 8
				},

},events:[],


elements: {
line: {
fill: false
},
point: {
hoverRadius: 7,
radius: 15
},
},rotation: (-0.5*Math.PI) - (65/180 * Math.PI),
plugins: {
title: false,
outlabels: {
   borderRadius: 0, // Border radius of Label
   borderWidth: 1, // Thickness of border
   color: 'white', // Font color
   display: true,
   lineWidth: 3, // Thickness of line between chart arc and Label
   padding: 3,
   stretch: 8, // The length between chart arc and Label
   font: {
resizable: true,
minSize: 12,
maxSize: 18
},
   text:function(context) {
				var index = context.dataIndex;
				var num=context.dataset.data[index] || 0;
				var value = "%p.2 ("+(context.dataset.data[index])+")";
				if(num==0||isNaN(num)){
					return "";	
				}
				else{
					return value;
				}
				
				},
   textAlign: "center"
}
},zoomOutPercentage: 10,
	/*legend: {
           display: true,
           position: 'bottom',
           labels: {
               boxWidth: 20,
               fontColor: '#111',
               padding: 10
           }
       },*/
       legend: false,
	   legendCallback:pie_legendcallback,
			
};
var myPieChart = new Chart(ctx,{plugins:[plugin],
type: 'outlabeledPie',
data: data,
options: options
});
$('#Age_wise_risk-legend').html(myPieChart.generateLegend());
}

/**/
function drawgenderriskfactoranyChart()
{

	var ctx = $("#genderriskfactorany");
	
	var total = <?php echo $risk_factor_analysis->gender_risk_factor_male+$risk_factor_analysis->gender_risk_factor_female+$risk_factor_analysis->gender_risk_factor_transgender; ?>;

	var data = {
		labels: [

		"Male", "Female", "Transgender",
		],
		datasets: [
		{
			data: [
<?php foreach ($age_gender as  $value){ ?>
			"<?php echo $value->count; ?>",
		
			<?php } ?>

			],
			backgroundColor: [
			'#745c97',
			'#ffcece',
			'#4baea0',
			'#7fe7cc', 
			'#e56a54',
			'#ffb35e',
			'#69779b',
			'#CB4335',
			'#D4AC0D',
			'#283747',
			]
		}]
	};
	var options = {
		responsive:true,
		maintainAspectRatio:false,
		animation    : {

			animateRotate: true,
			duration     : 2000,
		},
		/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 20
            }
        },*/
        legend:false,
        legendCallback:pie_legendcallback,

        rotation: (-0.5*Math.PI) - (65/180 * Math.PI),
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 20,
				bottom: 25
				}},
		 	plugins: {
				title: false,
				outlabels: {
				borderRadius: 0, // Border radius of Label
				borderWidth: 1, // Thickness of border
				color: 'white', // Font color
				display: true,
				lineWidth: 3, // Thickness of line between chart arc and Label
				padding: 3,
				stretch: 10, // The length between chart arc and Label
				font: {
				resizable: true,
				minSize: 12,
				maxSize: 18
				},
				text:function(context) {
				var index = context.dataIndex;
				var num=context.dataset.data[index] || 0;
				var value = "%p.2 ("+(context.dataset.data[index])+")";
				if(num==0||isNaN(num)){
					return "";	
				}
				else{
					return value;
				}
				
				},
				textAlign: "center"
				}
				},
       /*tooltips: {
			callbacks: {
				label: function(tooltipItem, data) {
					var percentage = Math.round(((data.datasets[0].data[tooltipItem.index]/total)*100)*100)/100;
					return data.datasets[0].data[tooltipItem.index]+" , "+percentage+"%"; 
				}
			}
		}, showAllTooltips: true,
		*/events:[],
	};
	var myPieChart = new Chart(ctx,{plugins:[plugin],
		type: 'pie',
		data: data,
		options: options
	});
	$('#gender_wise_risk-legend').html(myPieChart.generateLegend());
}






function drawagewisedistributionChart()
{

	$('.pie_chart_div_1').css("height","258px");
var ctx = $("#agewisedistribution");

var total = <?php echo count($age_vl_detected); ?>;

var data = {
labels: [

<?php foreach ($age_gpdatavlded as  $value){ ?>

"<?php echo $value->LookupValue; ?>",

<?php } ?>
],
datasets: [
{
data: [
<?php echo $age_vlcount->age_less_than_10; ?>,
<?php echo $age_vlcount->age_10_to_20; ?>,
<?php echo $age_vlcount->age_21_to_30; ?>,
<?php echo $age_vlcount->age_31_to_40; ?>,
<?php echo $age_vlcount->age_41_to_50; ?>,
<?php echo $age_vlcount->age_51_to_60; ?>,
<?php echo $age_vlcount->age_greater_than_60; ?>,
],
backgroundColor: [
'#ffcece',
'#745c97',
'#4baea0',
'#7fe7cc',
'#e56a54',
'#ffb35e',
'#69779b',
'#CB4335',
'#D4AC0D',
'#283747',
]
}]
};
var options = {
responsive:true,
maintainAspectRatio:false,
animation    : {
animateRotate: true,
duration     : 2000,
},
/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 17
            }
        },*/
        legend:false,
        legendCallback:pie_legendcallback,
        layout: {
padding: {
left: 18,
right: 8,
top: 40,
bottom: 5
}},

plugins: {
title: false,
outlabels: {
borderRadius: 0, // Border radius of Label
borderWidth: 1, // Thickness of border
color: 'white', // Font color
display: true,
lineWidth: 3, // Thickness of line between chart arc and Label
padding: 3,
stretch: 5, // The length between chart arc and Label
font: {
resizable: true,
minSize: 12,
maxSize: 18
},point: {
hoverRadius: 7,
radius: 12
},
text:function(context) {
var index = context.dataIndex;
var num=context.dataset.data[index] || 0;
var value = "%p.2 ("+(context.dataset.data[index])+")";
if(num==0||isNaN(num)){
return "";
}
else{
return value;
}

},
textAlign: "center"
}
},zoomOutPercentage:8,
/*tooltips: {
callbacks: {
label: function(tooltipItem, data) {
var percentage = Math.round(((data.datasets[0].data[tooltipItem.index]/total)*100)*100)/100;
return data.datasets[0].data[tooltipItem.index]+" , "+percentage+"%";
}
}
}, showAllTooltips: true,*/
events:[],
};
var myPieChart = new Chart(ctx,{plugins: [plugin],
type: 'pie',
data: data,
options: options
});
$('#Age_wise_vl-legend').html(myPieChart.generateLegend())
}

<?php if(isset($mtc_user) || $loginData->user_type==3 || $loginData->user_type==1){ if((isset($mtc_user[0]) ? $mtc_user[0]->is_Mtc==1 : '')  ){ ?>
function drawspCasesPrevalenceChart()
{

	var ctx = $("#spCasesPrevalence");
	
	<?php $cirrhosis_total = 3 + 6;?>

	var data = {
		labels: [

		["Treatment", "Experienced","Patients"],
		["People","Who Inject","Drugs"],
		["Persons with","Chronic Kidney", "Disease"],
		["Persons with", "HIV/HCV","Co-infection"],
		["Persons with","HBV/HCV", "Co-infection"],
		["Persons with","TB/HCV","Co-infection"],
		["Pregnant Women"],
		
		],
		datasets: [
		{	label :["No of Patients"],
			data: [


			"<?php echo isset($special_casedata_exp[0]->count)?($special_casedata_exp[0]->count):'0'; ?>",
			"<?php echo isset($special_casedata_durgs[0]->count)?($special_casedata_durgs[0]->count):'0'; ?>",
			"<?php echo isset($special_casedata_kidn[0]->count)?($special_casedata_kidn[0]->count):'0'; ?>",
			"<?php echo isset($special_casedata_hivhcv[0]->count)?($special_casedata_hivhcv[0]->count):'0'; ?>",
			"<?php echo isset($special_casedata_hbvhcv[0]->count)?($special_casedata_hbvhcv[0]->count):'0'; ?>",
			"<?php echo isset($special_casedata_hbvhcv[0]->count)?($special_casedata_hbvhcv[0]->count):'0'; ?>",
			"<?php echo isset($special_casedata_pregnant[0]->count)?($special_casedata_pregnant[0]->count):'0'; ?>",
		

			],
			backgroundColor: [
			'#ffcece',
			'#745c97',
			'#4baea0',
			'#7fe7cc', 
			'#e56a54',
			'#ffb35e',
			'#69779b',
			'#CB4335',
			'#D4AC0D',
			'#283747',
			]
		}]
	};
/*	var options = {
		animation : {
			animateRotate : true,
			duration : 2000,
			onComplete : function(){
				var url_base64jp = document.getElementById("spCasesPrevalence").toDataURL("image/png");
				document.getElementById("spCasesPrevalenceLink").href=url_base64jp;
			},
		},
*/

		var myBarChart = new Chart(ctx,{plugins:[plugin],
	type   : 'bar',
	data   : data,
	options: {
		responsive:true,
		maintainAspectRatio:false,
		animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});
				}},
    barValueSpacing: 0,
    /*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 75
            }
        },*/
        legend: false,
			legendCallback:legendcallback_per,
			cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 0
				}},
            tooltips: {
                        enabled: false
                        },events:[],
     scales: {
        xAxes: [{
        	 barThickness:30,
            gridLines: {
            	display:false,
                color: "rgba(0, 0, 0, 0)",
            },
        }],
        yAxes: [{
        	ticks: {
						beginAtZero:true,
						min:0,
						//stepSize:20,
						maxTicksLimit:8,
						suggestedMin:1,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},
            gridLines: {
            	display:false,
                color: "rgba(0, 0, 0, 0)",
            }   
        }]
    }
  }
});
$('#spCasesPrevalence-legend').html(myBarChart.generateLegend());

}


<?php  } }  ?>
function drawTreatmentInitiations(){

	var ctx = $("#TreatmentInitiations");

	var data = {
		labels: [
		"Anti-HCV Screened",
		"Viral Load Test",
		"Treatment Initiations",
		
		],
		datasets: [
		{	label: "Male",
		data: [
		<?php echo $scr_vs_tre_init->anti_scv_screened_male; ?>,
		<?php echo $scr_vs_tre_init->vl_test_male; ?>,
		<?php echo $scr_vs_tre_init->treatment_initiations_male; ?>,
		
		],
		backgroundColor: [
		'#745c97',
		'#745c97',
		'#745c97',
		'#745c97',
		'#745c97',
		'#745c97',
		],
		hoverBackgroundColor: [
		'rgba(0, 166, 156, 1)',
		'rgba(0, 166, 156, 1)',
		'rgba(0, 166, 156, 1)',
		'rgba(0, 166, 156, 1)',
		'rgba(0, 166, 156, 1)',
		'rgba(0, 166, 156, 1)',
		'rgba(0, 166, 156, 1)',
		],
		
		/*borderColor: [
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		'rgba(53, 196, 249,1)',
		],
		borderWidth: 1,*/
	},
	{	
		label: "Female",
		data : [
		<?php echo $scr_vs_tre_init->anti_scv_screened_female; ?>,
		<?php echo $scr_vs_tre_init->vl_test_female; ?>,
		<?php echo $scr_vs_tre_init->treatment_initiations_female; ?>,
		
		],
		backgroundColor: [
			'#ffcece',
			'#ffcece',
			'#ffcece',
			'#ffcece', 
			'#ffcece',
			'#ffcece',
			'#ffcece',
		
		],
		hoverBackgroundColor: [
			'#ffcece',
			'#ffcece',
			'#ffcece',
			'#ffcece', 
			'#ffcece',
			'#ffcece',
			'#ffcece',
		],
		/*borderColor: [
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		'rgba(255, 99, 132, 0.6)',
		],
		borderWidth: 1,*/
	},
	{	
		label: "Transgender",
		data : [
		<?php echo $scr_vs_tre_init->anti_scv_screened_transgender; ?>,
		<?php echo $scr_vs_tre_init->vl_test_transgender; ?>,
		<?php echo $scr_vs_tre_init->treatment_initiations_transgender; ?>,
		
		],
		backgroundColor: [
		'#4baea0',
		'#4baea0',
		'#4baea0',
		'#4baea0',
		'#4baea0',
		'#4baea0',
		'#4baea0',
		],
		hoverBackgroundColor: [
		'#4baea0',
		'#4baea0',
		'#4baea0',
		'#4baea0',
		'#4baea0',
		'#4baea0',
		'#4baea0',
		],
		/*borderColor: [
		'rgba(0, 111, 179,  0.6)',
		'rgba(0, 111, 179,  0.6)',
		'rgba(0, 111, 179,  0.6)',
		'rgba(0, 111, 179,  0.6)',
		'rgba(0, 111, 179,  0.6)',
		'rgba(0, 111, 179,  0.6)',
		'rgba(0, 111, 179,  0.6)',
		],
		borderWidth: 1,*/
	}
	]
};

var myBarChart = new Chart(ctx,{plugins:[plugin],
	type   : 'bar',
	data   : data,
	options: {
		responsive:true,
		maintainAspectRatio:false,
		animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});

				
				}},
    barValueSpacing: 20,
    /*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 15
            }
        },*/ 
        legend:false,
        legendCallback:legendcallback_per,
        cornerRadius:5,
        events:[],
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 0
				}},
    scales: {
        xAxes: [{
        	maxBarThickness : 30,
        	barPercentage: 0.7,
        	categoryPercentage: .6,
        	labelAutoFit: true,
            gridLines: {
            	display:false,
                color: "rgba(0, 0, 0, 0)",
            }
        }],
        yAxes: [{
        	ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},
            gridLines: {
            	display:false,
                color: "rgba(0, 0, 0, 0)",
            }   
        }]
    }
  },showAllTooltips:false
 
});
$('#Gender_distribution-legend').html(myBarChart.generateLegend());
}

<?php if($filters['id_mstfacility']==""){ ?>
function drawscreenedacrossdistricts(){

	var ctx  = $("#screenedacrossdistricts");
	
	var data = {
		labels: [
		<?php 
		foreach ($treatment_Initiations_by_District as $row) {
			echo "['".$row->hospital."'],";
		}
		?>
		],
		datasets: [
		{	
			label : ["No. of Patients"],
			backgroundColor: [
			<?php 
			foreach ($treatment_Initiations_by_District as $row) {
				echo "'".$row->color."',";
			}
			?>
			],
		
			data: [
			<?php 
			$max=null;
			foreach ($treatment_Initiations_by_District as $row) {
				echo floor($row->countIni).",";
						 if ($max == null || $row->countIni > $max) {
            		$max= $row->countIni;
       				 }

			}
			?>
			]
		}
		]
	};
var maxval=<?php echo $max ? $max : 0; ?>;
	var options = {
		responsive:true,
		maintainAspectRatio:false,
		animation: {
				duration: 0,
				onComplete: function () {
				/*var cascade_url_base64jp = document.getElementById("screenedacrossdistricts").toDataURL("image/png");
				document.getElementById("screenedacrossdistrictsLink").href=cascade_url_base64jp;*/
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});
				
				}},
	
		/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 15
            }
        },*/
        legend: false,
		legendCallback:legendcallback,
		cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 35,
				bottom: 10
				}},
            tooltips: {
                        enabled: false
                        },events:[],
		scales : {
			xAxes : [{
				<?php if(count($treatment_Initiations_by_District)<=10){
					echo 'barThickness:60,';
				}; ?>
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				},
				stacked: true,
ticks: {
beginAtZero:true,
autoSkip : false,
maxRotation:90,
minRotation:90
},
			}],
			yAxes : [{
				ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},

				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				}
			}]
		}
	};

	var myBarChart = new Chart(ctx, { plugins: [plugin],
		type: 'bar',
		data: data,
		options: options
	});
	$('#Screened_HCV_dist-legend').html(myBarChart.generateLegend());

}
<?php } ?>
<?php /*if($filters['id_mstfacility']==""){ ?>
function drawscreenedacrossdistrictshav()
{
	var ctx  = $("#screenedacrossdistrictshav");
	
	var data = {
		labels: [
		<?php 
		foreach ($treatment_Initiations_by_Districthav as $row) {
			echo "\"".$row->hospital."\",";
		}
		?>
		],
		datasets: [
		{	
			label : ["No. of Patients"],
			backgroundColor: [
			<?php 
			foreach ($treatment_Initiations_by_Districthav as $row) {
				echo "'".$row->color."',";
			}
			?>
			],
			
			data: [
			<?php 
			$max=0;
			foreach ($treatment_Initiations_by_Districthav as $row) {
				echo "\"".floor($row->countIni)."\",";
				 if ($max == null || $row->countIni > $max) {
            		$max= $row->countIni ? $row->countIni : 0;
       				 }

			}
			?>
			]
		}
		]
	};
var maxval=<?php echo $max ? $max : 0; ?>;

						
	var options = {
	responsive:true,
	maintainAspectRatio:false,
		animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});

				}},
	
		
        legend:false,
        legendCallback:legendcallback,
        cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 0
				}},
            tooltips: {
                        enabled: false
                        },events:[],
		scales : {
			xAxes : [{
				<?php if(count($treatment_Initiations_by_Districthav)<=10){
					echo 'barThickness:60,';
				};?>
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				},
				ticks :{
					autoSkip :false,
					maxRotation:90,
					minRotation:90
				}
			}],
			yAxes : [{
				ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},

				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				}
			}]
		}
	};

	var myBarChart = new Chart(ctx, { plugins: [plugin],
		type: 'bar',
		data: data,
		options: options
	});
	$('#Screened_HAV_dist-legend').html(myBarChart.generateLegend());
}
<?php }*/ ?>

<?php if($filters['id_mstfacility']==""){ ?>

/*function drawscreenedacrossdistrictshev()
{
	var ctx  = $("#screenedacrossdistrictshev");
	
	var data = {
		labels: [
		<?php 
		foreach ($treatment_Initiations_by_Districthev as $row) {
			echo "['".$row->hospital."'],";
		}
		?>
		],
		datasets: [
		{	
			label : ["No. of Patients"],
			backgroundColor: [
			<?php 
			foreach ($treatment_Initiations_by_Districthev as $row) {
				echo "'".$row->color."',";
			}
			?>
			],
			
			data: [
			<?php 
			$max=0;
			foreach ($treatment_Initiations_by_Districthev as $row) {
				echo floor($row->countIni).",";
				if ($max == null || $row->countIni > $max) {
            		$max= $row->countIni;
       				 }
			}
			?>
			]
		}
		]
	};
var maxval=<?php echo $max ? $max : 0; ?>;
	var options = {
	responsive:true,
	maintainAspectRatio:false,
		animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});

				}},
		barValueSpacing: 20,
		
        legend: false,
		legendCallback:legendcallback,
		cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 45,
				bottom: 0
				}},
            tooltips: {
                        enabled: false
                        },events:[],
		scales : {
			xAxes : [{
				<?php if(count($treatment_Initiations_by_Districthev)<=10){
					echo 'barThickness:60,';
				}; ?>
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				},
				stacked: true,
ticks: {
beginAtZero:true,
autoSkip : false,
maxRotation:90,
minRotation:90
},
			}],
			yAxes : [{
				ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						 max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},

				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				}
			}]
		}
	};

	var myBarChart = new Chart(ctx, { plugins: [plugin],
		type: 'bar',
		data: data,
		options: options
	});
	$('#Screened_HEV_dist-legend').html(myBarChart.generateLegend());
}*/

function drawsucessratiosvrtestdis(){
//Chart.plugins.register(ChartDataLabels);
var ctx  = $("#sucessratiosvrtestdis");
var data = {
labels: [
<?php
foreach ($treatment_by_Districtsvrsuc as $row) {
echo "\"".$row->hospital."\",";
}
?>
],
datasets: [
{ label: "Treatment Successful",
data: [
<?php
foreach ($treatment_by_Districtsvrsuc as $row) {
echo ceil($row->countIni).",";
}
?>
],
backgroundColor: [
<?php
foreach ($treatment_by_Districtsvrsuc as $row) {
echo "'".$row->color."',";
}
?>
],
hoverBackgroundColor: [
<?php
foreach ($treatment_by_Districtsvrsuc as $row) {
echo "'".$row->color."',";
}
?>
],datalabels: {
						anchor: 'center',
						align: 'center',
					}


},
{
label: "Treatment Failure",
data : [
<?php
foreach ($treatment_by_Districtsvrunsuc as $row) {
echo ceil($row->countIni).",";
}
?>
],
backgroundColor: [
<?php
foreach ($treatment_by_Districtsvrunsuc as $row) {
echo "'".$row->color."',";
}
?>
],
hoverBackgroundColor: [
<?php
foreach ($treatment_by_Districtsvrunsuc as $row) {
echo "'".$row->color."',";
}
?>
],datalabels: {
						anchor: 'center',
						align: 'center',
					}

}
]
};

var myBarChart = new Chart(ctx,{plugins:[plugin],
type   : 'bar',
data   : data,
 plugins: [ChartDataLabels,plugin],
options: {
plugins: {
          /*labels:{
 			render: function (args) {
      if(isNaN(args.percentage))
      {
      	return "0%";
      }
      else{
      	return args.percentage+'%';
      }
    }},*/
 	datalabels: {
						color: '#505050',
						rotation:360,
						 font: {
         
          size: 11,
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0%";
							}
							else{
							return context.dataset.data[context.dataIndex]+"%";
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							},	
stacked100: { enable: true,precision: 2 },

 },
 responsive:true,
maintainAspectRatio:false,
  animation: {
duration: 0,
onComplete: function () {
// render the value of the chart above the bar
/*var ctx = this.chart.ctx;
ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
ctx.fillStyle = this.chart.config.options.defaultFontColor;
ctx.textAlign = 'center';
ctx.textBaseline = 'bottom';
this.data.datasets.forEach(function (dataset) {
for (var i = 0; i < dataset.data.length; i++) {
var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
ctx.fillText(dataset.data[i], model.x, model.y - 5);
}
});*/
}},

    barValueSpacing: 20,
   /* legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 15
            }
        },*/
        legendCallback:legendcallback_per,
        legend:false,
        cornerRadius:5,
        layout: {
padding: {
left: 0,
right: 0,
top: 25,
bottom: 0
}},
            tooltips: {
                        enabled: false
                        },events:[],
    scales: {
yAxes: [{
stacked: true,
ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},scaleLabel: {
       display: true,
       labelString: 'In Percentage (%)',
     },
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
}
}],
xAxes: [{
<?php if(count($treatment_by_Districtsvrunsuc)<=10){
					echo 'barThickness:60,';
				}; ?>
stacked: true,
ticks: {
beginAtZero:true,
autoSkip : false,
maxRotation:90,
minRotation:90
},
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
}
}]

},
}
});
$('#svr_dist-legend').html(myBarChart.generateLegend());
}


function drawlossfollowacrossdist(){

//var ctx  = $("#lossfollowacrossdist");
var ctx  = $("#lossfollowacrossdist");
var total=<?php $temp=0;foreach ($lossupollowupacrossdistict as $row) {
$temp+=($row->countIni);}echo round($temp); ?>;
var data = {
labels: [
<?php
foreach ($lossupollowupacrossdistict as $row) {
echo "\"".$row->hospital."\",";
}
?>
],
datasets: [
{
label : ["LTFU is a percentage of total VL positive patients detected"],
backgroundColor: [
<?php
foreach ($lossupollowupacrossdistict as $row) {
echo "'".$row->color."',";
}
?>
],

data: [
<?php
foreach ($lossupollowupacrossdistict as $row) {
	if($temp>0){
		$pr=(($row->countIni)*100)/$temp;
	if($pr>0.01 && $pr<1){
		$per=round($pr,2);
	}
	else{
		$per=round($pr,2);
	}
	}
	else{
		$per=0;
	}

echo $per.",";
}
?>
]
}
]
};

var options = {

responsive: true,
maintainAspectRatio:false,
plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:1,
						 font: {
         weight:'400',
         //family:'Arial',
         family:'Helvetica',
          size: 12.4,
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0%";
							}
							else{
							return context.dataset.data[context.dataIndex]+"%";
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},
animation: {
duration: 0,
onComplete: function () {
// render the value of the chart above the bar
/*var ctx = this.chart.ctx;
ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
ctx.fillStyle = this.chart.config.options.defaultFontColor;
ctx.textAlign = 'center';
ctx.textBaseline = 'bottom';
this.data.datasets.forEach(function (dataset) {
for (var i = 0; i < dataset.data.length; i++) {
var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
ctx.fillText(dataset.data[i], model.x, model.y - 5);
}
});*/
}},

/*legend: {
           display: true,
           position: 'bottom',
           labels: {
               boxWidth: 20,
               fontColor: '#111',
               padding: 15
           }
       },*/
legend: false,
legendCallback:legendcallback,
cornerRadius:5,
       layout: {
padding: {
left: 0,
right: 0,
top: 35,
bottom: 0
}},
           tooltips: {
                       enabled: false
                       },events:[],
scales : {
xAxes : [{
	<?php if(count($lossupollowupacrossdistict)<=10){
					echo 'barThickness:40,';
				}; ?>
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
},
ticks: {
autoSkip :false,
maxRotation:90,
minRotation:90
      /* callback: function (value) {
         return value.toLocaleString('de-DE', {style:'percent'});
       },*/
     }
}],
yAxes : [{
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
},
ticks: {
						beginAtZero:true,
						min:0,
						max:100,
						stepSize:20,
						suggestedMin:1,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},scaleLabel: {
        display: true,
        labelString: 'In Percentage (%)'
      },

}]
}
};

var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
type: 'bar',
data: data,
options: options
});
$('#ltfu_per_dist-legend').html(myBarChart.generateLegend());
}




function drawlossfollowacrossdistnum(){

var ctx  = $("#lossfollowacrossdistnum");
	
	var data = {
		labels: [
		<?php 
		foreach ($lossupollowupacrossdistict as $row) {
			echo "\"".$row->hospital."\",";
		}
		?>
		],
		datasets: [
		{	
			label : ["No. of Patients"],
			backgroundColor: [
			<?php 
			foreach ($lossupollowupacrossdistict as $row) {
				echo "'".$row->color."',";
			}
			?>
			],
			
			data: [
			<?php 
			$max=0;
			foreach ($lossupollowupacrossdistict as $row) {
				echo "\"".floor($row->countIni)."\",";
				 if ($max == null || $row->countIni > $max) {
            		$max= $row->countIni;
       				 }
			}
			?>
			]
		}
		]
	};
var maxval=<?php echo $max ? $max : 0; ?>;

	var options = {
	responsive:true,
	maintainAspectRatio:false,
	plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:1,
						 font: {
         weight:'400',
         //family:'Arial',
         family:'Helvetica',
          size: 12.4,
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0";
							}
							else{
							return context.dataset.data[context.dataIndex];
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},
		animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				/*this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});
*/

				}},
		
		/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 45
            }
        },*/
        legend: false,
		legendCallback:legendcallback,
		cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 0
				}},
            tooltips: {
                        enabled: false
                        },events:[],
		scales : {
			xAxes : [{
				<?php if(count($lossupollowupacrossdistict)<=10){
					echo 'barThickness:60,';
				}; ?>
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				},
				ticks :{
					autoSkip :false,
					maxRotation:90,
					minRotation:90
				}
			}],
			yAxes : [{
				stacked:true,
				ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},

				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				}
			}]
		}
	};

	var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
		type: 'bar',
		data: data,
		options: options
	});
$('#ltfu_num_dist-legend').html(myBarChart.generateLegend());
}






function drawAdherenceDistricts(){

	
	var ctx  = $("#AdherenceDistricts");
var total=<?php $temp=0;foreach ($Adherenceaccross_district as $row) {
$temp+=($row->countIni);}echo round($temp); ?>;
var data = {
labels: [
<?php
foreach ($Adherenceaccross_district as $row) {
echo "['".$row->hospital."'],";
}
?>
],
datasets: [
{
label : ["No. of Patients"],
backgroundColor: [
<?php
foreach ($Adherenceaccross_district as $row) {
echo "'".$row->color."',";
}
?>
],

data: [
<?php
foreach ($Adherenceaccross_district as $row) {
if($temp>0){
		$pr=$row->countIni;
	if($pr>0.01 && $pr<1){
		$per=round($pr,2);
	}
	else{
		$per=round($pr,2);
	}
	}
	else{
		$per=0;
	}
echo $per.",";
}
?>
]
}
]
};

var options = {

responsive: true,
maintainAspectRatio:false,
 plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'normal',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0%";
							}
							else{
							return context.dataset.data[context.dataIndex]+"%";
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},animation: {
duration: 0,
onComplete: function () {
// render the value of the chart above the bar
/*var ctx = this.chart.ctx;
ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
ctx.fillStyle = this.chart.config.options.defaultFontColor;
ctx.textAlign = 'center';
ctx.textBaseline = 'bottom';
this.data.datasets.forEach(function (dataset) {
for (var i = 0; i < dataset.data.length; i++) {
var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
ctx.fillText(dataset.data[i], model.x, model.y - 5);
}
});*/
}},

/*legend: {
            display: false,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 15
            }
        },*/
        legend: false,
		legendCallback:legendcallback,
		cornerRadius:5,
        layout: {
padding: {
left: 0,
right: 0,
top: 45,
bottom: 0
}},
            tooltips: {
                        enabled: false
                        },events:[],
scales : {
xAxes : [{
	<?php if(count($Adherenceaccross_district)<=10){
					echo 'barThickness:60,';
				}; ?>
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
},
ticks: {
beginAtZero:true,
min:0,
suggestedMin:0,
autoSkip : false,
minRotation:90,
minRotation:90,
callback: function (value) {
return value.toLocaleString('de-DE', {style:'percent'});
},
}
}],
yAxes : [{
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
},
/*ticks: {
callback: function (value) {
return value.toLocaleString('de-DE', {style:'percent'});
},
}*/
ticks: {
						beginAtZero:true,
						min:0,
						max:100,
						stepSize:20,
						suggestedMin:1,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},scaleLabel: {
        display: true,
        labelString: 'In Percentage (%)'
      }

}]
}
};

var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
type: 'bar',
data: data,
options: options
});
$('#ltfu_Adherencedist-legend').html(myBarChart.generateLegend());
}


/*function drawTurnAroundAnalysisdist(){

	var ctx  = $("#TurnAroundAnalysisdist");
	
	var data = {
		labels: [
		<?php 
		foreach ($trunaroundtimeanalys_districts as $row) {
			echo "\"".$row->hospital."\",";
		}
		?>
		],
		datasets: [
		{	
			label : ["No. of Patients"],
			backgroundColor: [
			<?php 
			foreach ($trunaroundtimeanalys_districts as $row) {
				echo "'".$row->color."',";
			}
			?>
			],
			
			data: [
			<?php 
			foreach ($trunaroundtimeanalys_districts as $row) {
				echo "\"".floor($row->countIni)."\",";
			}
			?>
			]
		}
		]
	};

	var options = {
	responsive:true,
	maintainAspectRatio:false,
		animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});

				}},
	
		
        legend: false,
		legendCallback:legendcallback,
		cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 0
				}},
            tooltips: {
                        enabled: false
                        },events:[],
		scales : {
			xAxes : [{
				<?php if(count($trunaroundtimeanalys_districts)<=10){
					echo 'barThickness:60,';
				}; ?>
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				},
				ticks :{
					autoSkip :false,
					minRotation:90,
					maxRotation:90
				}
			}],
			yAxes : [{
				ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				}
			}]
		}
	};

	var myBarChart = new Chart(ctx, {plugins: [plugin],
		type: 'bar',
		data: data,
		options: options
	});
	$('#Turnaround_dist-legend').html(myBarChart.generateLegend());

}*/
function drawscreenedacrossdistricts_national(){
<?php  if ($loginData->user_type==1) { ?>
	
	var ctx  = $("#screenedacrossdistricts_national");
	var data = {
		labels: [
		<?php 
		foreach ($screened_antihcv_across_states as $row) {
			echo "['".$row->hospital."'],";
			
		}
		?> 
    
		],
		datasets: [
		{	
			label : ["No. of Patients"],
			backgroundColor: [
			<?php 
			foreach ($screened_antihcv_across_states as $row) {
				echo "'rgb(53, 196, 249)',";
			}
			?>
			],
			
			data: [
			<?php 
			$max=0;
			foreach ($screened_antihcv_across_states as $row) {
				echo floor($row->countIni).",";
			 if ($max === null || $row->countIni > $max) {
            		$max= $row->countIni;
       				 }
       				}
			?>
			]
		}
		]
	};
	var maxval=<?php echo $max ? $max : 0; ?>;
	var options = {
	responsive:true,
	maintainAspectRatio	:false,
	     plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'normal',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0";
							}
							else{
							return context.dataset.data[context.dataIndex];
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},
		animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				/*ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});*/

				}},
	
		legend:false,
		legendCallback:legendcallback, 
		cornerRadius: 5,  
        layout: {
				padding: {
				left: 0,
				right: 0,
				top:5,
				bottom: 20
				}}, tooltips: {
                        enabled: false
                        },events:[],
		scales : {
			xAxes : [{

				gridLines: {
					color: "rgba(0, 0, 0, 0)",
					display:false,
				},
				 ticks: {
          autoSkip: false,
          maxRotation: 90,
          minRotation: 90,
          beginAtZero:true,
		min:0,
		suggestedMin:1,
        }
			}],
			yAxes : [{
		
				stacked:true,
				ticks: {
						beginAtZero:true,
						min:0,
						maxTicksLimit:8,
						suggestedMin:1,
						max:(maxval+maxval/2)>100 ? (maxval+maxval/2) : 100,
            callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                    	if (value==maxval+maxval/2) {
                    		if(value>500){
								return "";
                    		}
                    		else{
                    			return "";
                    		}
                    		
                    	}
                    	else{
                    		return value;
                    	}
                   
                    }
                },
						suggestedMax:100
					},
				gridLines: {
					color: "rgba(0, 0, 0, 0)",
					display:false,
				}
			}]
		}
	};

	var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
		type: 'bar',
		data: data,
		options: options
	});
$('#js-legend1').html(myBarChart.generateLegend());
<?php } ?>
//document.getElementById('js-legend1').inerHTML = myBarChart.generateLegend();

}



function drawsucessratiosvrteststate(){


<?php  if ($loginData->user_type==1) { ?>
var ctx  = $("#sucessratiosvrteststate");
var data = {
labels: [
<?php
foreach ($treatment_by_statesvrsucess as $row) {
echo "\"".$row->hospital."\",";
}
?>
],
datasets: [
{ label: "Treatment Successful",
data: [
<?php
foreach ($treatment_by_statesvrsucess as $row) {
echo ceil($row->countIni).",";
}
?>
],datalabels: {
						anchor: 'center',
						align: 'center',
					},
backgroundColor: [
<?php
foreach ($treatment_by_statesvrsucess as $row) {
echo "'".$row->color."',";
}
?>
],
hoverBackgroundColor: [
<?php
foreach ($treatment_by_statesvrsucess as $row) {
echo "'".$row->color."',";
}
?>
],

},
{
label: "Treatment Failure",
data : [
<?php
foreach ($treatment_by_statesvrunsuc as $row) {
echo ceil($row->countIni).",";
}
?>
],datalabels: {
						anchor: 'center',
						align: 'center',
					},
backgroundColor: [
<?php
foreach ($treatment_by_statesvrunsuc as $row) {
echo "'".$row->color."',";
}
?>
],
hoverBackgroundColor: [
<?php
foreach ($treatment_by_statesvrunsuc as $row) {
echo "'".$row->color."',";
}
?>
],

}
]
};
var myBarChart = new Chart(ctx,{plugins: [ChartDataLabels,plugin],
type   : 'bar',
data   : data,

 options: {	
 	responsive:true,
    	maintainAspectRatio:false,
 	plugins:{
 		stacked100: { enable: true,precision: 2 },
 		/*labels:{
 			render: function (args) {
      if(isNaN(args.percentage))
      {
      	return "0%";
      }
      else{
      	return args.percentage+'%';
      }
    },position: 'border'
 		}*/
 		datalabels: {
						color: '#505050',
						rotation:270,
						//offset:8,
						 font: {
         
          size: 11,
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0% \t\s/n/n/n/n/n";
							}
							else{
							return context.dataset.data[context.dataIndex]+"%";
							}
							445555
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							},		
 },
    animation: {
    	
duration: 0,
},
    barValueSpacing: 20,
    /*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 15
            }
        },*/
      legend:false,
      legendCallback:legendcallback_per,
      cornerRadius:5,
        layout: {
padding: {
left: 0,
right: 0,
top: 10,
bottom: 0
}},
            /*tooltips: {
                        enabled: false
                        },*/events:[],
    scales: {
yAxes: [{
stacked: true,
ticks: {
	beginAtZero:true,
	min:0,
	stepValue:20,
	stepSize:20,
	steps:5,
	max:100,
        callback: function(tick) {

          return tick.toString() + '';
      }
        },
        scaleLabel: {
        display: true,
        labelString: 'In Percentage (%)'
      },
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)"
}
}],
xAxes: [{
barThickness: 20,
stacked: true,
ticks: {
autoSkip : false,
beginAtZero:true,
autoSkip: false,
maxRotation: 90,
minRotation: 90
},
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
}
}]

}
  }
			
});
$('#svr_state-legend').html(myBarChart.generateLegend());
<?php } ?>
}


<?php  } ?>
/*national*/
<?php if($filters['id_mstfacility']==""){ ?>
/*function drawlossfollowacrossstate(){

<?php  if ($loginData->user_type==1) { ?>
var ctx  = $("#lossfollowacrossstate");
var total=<?php $temp=0;foreach ($lossfollowacrossstate as $row) {
$temp+=$row->countIni;}echo round($temp); ?>;
var data = {
labels: [
<?php
foreach ($lossfollowacrossstate as $row) {
echo "\"".$row->hospital."\",";
}
?>
],
datasets: [
{
label : ["LTFU is a percentage of total VL positive patients detected"],
backgroundColor: [
<?php
foreach ($lossfollowacrossstate as $row) {
echo "'".$row->color."',";
}
?>
],

data: [
<?php
foreach ($lossfollowacrossstate as $row) {
	if($temp>0){
		$pr=($row->countIni*100)/$temp;
	if($pr>0.01 && $pr<1){
		$per=round($pr,2);
		$per=substr(strval($per), 1);
	}
	else{
		$per=round($pr,2);
	}
	}
	else{
		$per=0;
	}
echo $per.",";
}
?>
]
}
]
};

var options = {
responsive: true,
maintainAspectRatio:false,
      plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:1,
						 font: {
         weight:'400',
         //family:'Arial',
         family:'Helvetica',
          size: 12.4,
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0%";
							}
							else{
							return context.dataset.data[context.dataIndex]+"%";
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},
						legend: {
						display: true,
						position: 'bottom',
						labels: {
						boxWidth: 20,
						fontColor: '#111',
						padding: 15
						}
						},
						legend:false,
						legendCallback:legendcallback,
						cornerRadius:5,

						layout: {
						padding: {
						left: 0,
						right: 0,
						top: 30,
						bottom: 20
						}},
						tooltips: {
						enabled: false
						},events:[],
						scales : {
						xAxes : [{
						gridLines: {
							display:false,
						color: "rgba(0, 0, 0, 0)",
						},stacked:true,
						ticks: {
						autoSkip :false,
						suggestedMin:0,
						maxRotation: 90,
						minRotation: 90,
						callback: function (value) {
						return value.toLocaleString('de-DE', {style:'percent'});
						},
						}
						}],
						yAxes : [{
						gridLines: {
							display:false,
						color: "rgba(0, 0, 0, 0)",
						},
						scaleLabel: {
						display: true,
						labelString: 'In Percentage (%)'
						},
						stacked:true,
									ticks: {

									beginAtZero:true,
									stepSize:20,
									autoSkip:true,
									min: 0,
									max: 100,
									suggestedMax:100,
									suggestedMin:0,
									 callback: function(ticks) {
										return ticks;
								/*	if (Math.floor(value) === value) {
									var per=(value * 100/ total).toFixed(0);
									//alert(total);						
									if((per>90 && per<=99)){
									return "";
									}
									else{
									return per; // convert it to percentage
									}
									
									}*/
								/*	},
									},
						}]
						}
						};

var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
type: 'bar',
data: data,
options: options
});
$('#ltfu_per-legend').html(myBarChart.generateLegend());
<?php } ?>

}*/

function drawlossfollowacrossstatetnum(){
<?php  if ($loginData->user_type==1) { ?>
var ctx  = $("#lossfollowacrossstatetnum");
	
	var data = {
		labels: [
		<?php 
		foreach ($lossfollowacrossstate as $row) {
			echo "\"".$row->hospital."\",";
		}
		?>
		],
		datasets: [
		{	
			label : ["No. of Patients"],
			backgroundColor: [
			<?php 
			foreach ($lossfollowacrossstate as $row) {
				echo "'".$row->color."',";
			}
			?>
			],
			
			data: [
			<?php 
			$max=0;
			foreach ($lossfollowacrossstate as $row) {
				echo "\"".floor($row->countIni)."\",";
				 if ($max == null || $row->countIni > $max) {
            		$max= $row->countIni;
       				 }
			}
			?>
			]
		}
		]
	};
var maxval=<?php echo $max ? $max : 0; ?>;
	var options = {
	 responsive: true,
	 maintainAspectRatio:false,
     plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'500',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0";
							}
							else{
							return context.dataset.data[context.dataIndex];
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},
	
		
		/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 45
            }
        },*/
        legend:false,
      legendCallback:legendcallback_per,
      cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 10
				}},
            tooltips: {
                        enabled: false
                        },events:[],
		scales : {
			xAxes : [{
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				},
				ticks :{
					autoSkip :false,
						maxRotation: 90,
						minRotation: 90

					}
			}],
			yAxes : [{
			
				ticks: {
					
						beginAtZero:true,
						maxTicksLimit: 8,
						min:0,
						//max:this.max+100,
						suggestedMin:0,
						 max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				}
			}]
		}
	};

	var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
		type: 'bar',
		data: data,
		options: options
	});
	$('#ltfu_num-legend').html(myBarChart.generateLegend());
<?php } ?>
}


function drawAdherenceState(){

<?php  if ($loginData->user_type==1) { ?>
var total=<?php $temp=0;foreach ($Adherenceaccrossstate as $row) {
$temp+=($row->countIni);}echo $temp; ?>;
var ctx  = $("#AdherenceState");

var data = {
labels: [
<?php
foreach ($Adherenceaccrossstate as $row) {
echo "\"".$row->hospital."\",";
}
?>
],
datasets: [
{
/*label : ["No. of Patients"],*/
backgroundColor: [
<?php
foreach ($Adherenceaccrossstate as $row) {
echo "'".$row->color."',";
}
?>
],

data: [
<?php
foreach ($Adherenceaccrossstate as $row) {

echo $row->countIni.",";
}
?>
]
}
]
};

var options = {
responsive: true,
maintainAspectRatio:false,
     plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'500',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0%";
							}
							else{
							return context.dataset.data[context.dataIndex]+"%";
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},


						/*legend: {
						display: false,
						position: 'bottom',
						labels: {
						boxWidth: 20,
						fontColor: '#111',
						padding: 15
						}
						},*/
						legend: false,
						/*legendCallback:legendcallback,*/
						cornerRadius:5,
						layout: {
						padding: {
						left: 0,
						right: 0,
						top: 39,
						bottom: 18
						}},
						tooltips: {
						enabled: false
						},events:[],
						scales : {
						xAxes : [{
						gridLines: {
							display:false,
						color: "rgba(0, 0, 0, 0)",
						},
						ticks: {
						autoSkip : false,
						maxRotation: 90,
						minRotation: 90,
						callback: function (value) {
						return value.toLocaleString('de-DE', {style:'percent'});
						},
						}
						}],
						yAxes : [{

						gridLines: {
							display:false,
						color: "rgba(0, 0, 0, 0)",
						},
						scaleLabel: {
						display: true,
						labelString: 'In Percentage (%)'
						},
								/*ticks: {
						callback: function (value) {
						return value.toLocaleString('de-DE', {style:'percent'});
						},
						}*/
						ticks: {
							stepSize: 20,
						beginAtZero:true,
						min: 0,
						max: 100,
						callback: function (ticks) {
							
								return ticks;
						}
						}
						
						}]
						}
						};

var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
type: 'bar',
data: data,
options: options
});
/*$('#ltfu_AdherenceState-legend').html(myBarChart.generateLegend());*/
<?php } ?>

}
function drawTurnAroundAnalysisState(){
<?php  if ($loginData->user_type==1) { ?>
	var ctx  = $("#TurnAroundAnalysisState");
	
	var data = {
		labels: [
		<?php 
		foreach ($TurnAroundAnalysisState as $row) {
			echo "\"".$row->hospital."\",";
		}
		?>
		],
		datasets: [
		{	
			label : ["No. of Patients"],
			backgroundColor: [
			<?php 
			foreach ($TurnAroundAnalysisState as $row) {
				echo "'".$row->color."',";
			}
			?>
			],
			
			data: [
			<?php 
			foreach ($TurnAroundAnalysisState as $row) {
				echo floor($row->countIni).",";
			}
			?>
			]
		}
		]
	};

	var options = {
	responsive:true,
	maintainAspectRatio:false,
		animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});
				
				}},
	
		/*legend: {
            display: false,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 15
            }
        },*/
        legend: false,
			legendCallback:legendcallback,
			cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 15,
				bottom: 45
				}},
            tooltips: {
                        enabled: false
                        },events:[],
		scales : {
			xAxes : [{
				<?php if(count($TurnAroundAnalysisState)<=10){
					echo 'barThickness:30,';
				};?>
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				},
				ticks :{
					autoSkip :false,
					maxRotation: 90,
						minRotation: 90
				}
			}],
			yAxes : [{
				ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				}
			}]
		}
	};

	var myBarChart = new Chart(ctx, {plugins: [plugin],
		type: 'bar',
		data: data,
		options: options
	});
	$('#Turnaround_state-legend').html(myBarChart.generateLegend());
<?php } ?>
}

<?php } ?>

/*plugin code end*/
Chart.pluginService.register({
  beforeDraw: function (chart) {
    if (chart.config.options.elements.center) {
      //Get ctx from string
      var ctx = chart.chart.ctx;

      //Get options from the center object in options
      var centerConfig = chart.config.options.elements.center;
      var fontStyle = centerConfig.fontStyle || 'Arial';
      var txt = centerConfig.text;
      var color = centerConfig.color || '#000';
      var sidePadding = centerConfig.sidePadding || 20;
      var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
      //Start with a base font of 30px
      ctx.font = "55px " + fontStyle;

      //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
      var stringWidth = ctx.measureText(txt).width;
      var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

      // Find out how much the font can grow in width.
      var widthRatio = elementWidth / stringWidth;
      var newFontSize = Math.floor(30 * widthRatio);
      var elementHeight = (chart.innerRadius * 2);

      // Pick a new font size so it will not be larger than the height of label.
      var fontSizeToUse = Math.min(newFontSize, elementHeight);

      //Set font settings to draw it correctly.
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
      var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
      ctx.font = fontSizeToUse+"px " + fontStyle;
      ctx.fillStyle = color;

      //Draw text in center
      ctx.fillText(txt, centerX, centerY);
    }
  }
});

var roundedges={
            afterUpdate: function (chart) {
                    var a=chart.config.data.datasets.length -1;
                    for (let i in chart.config.data.datasets) {
                        for(var j = chart.config.data.datasets[i].data.length - 1; j>= 0;--j) { 
                            if (Number(j) == (chart.config.data.datasets[i].data.length - 1))
                                continue;
                            var arc = chart.getDatasetMeta(i).data[j];
                            arc.round = {
                                x: (chart.chartArea.left + chart.chartArea.right) / 2,
                                y: (chart.chartArea.top + chart.chartArea.bottom) / 2,
                                radius: chart.innerRadius + chart.radiusLength / 2 + (a * chart.radiusLength),
                                thickness: chart.radiusLength / 2 - 1,
                                backgroundColor: arc._model.backgroundColor
                            }
                        }
                        a--;
                    }
            },

            afterDraw: function (chart) {
                    var ctx = chart.chart.ctx;
                    for (let i in chart.config.data.datasets) {
                        for(var j = chart.config.data.datasets[i].data.length - 1; j>= 0;--j) { 
                            if (Number(j) == (chart.config.data.datasets[i].data.length - 1))
                                continue;
                            var arc = chart.getDatasetMeta(i).data[j];
                            var startAngle = (Math.PI / 2 - arc._view.startAngle);
                            var endAngle = Math.PI / 2 - arc._view.endAngle;

                            ctx.save();
                            ctx.translate(arc.round.x, arc.round.y);
                            console.log(arc.round.startAngle)
                            ctx.fillStyle = arc.round.backgroundColor;
                            ctx.beginPath();
                            ctx.arc(arc.round.radius * Math.sin(startAngle), arc.round.radius * Math.cos(startAngle), arc.round.thickness,0,2 * Math.PI);
                            ctx.arc(arc.round.radius * Math.sin(endAngle), arc.round.radius * Math.cos(endAngle), arc.round.thickness, 0, 2 * Math.PI);
                            ctx.closePath();
                            ctx.fill();
                            ctx.restore();
                        }
                    }
            },
        };

    /*Chart.plugins.register({
        afterDatasetsDraw: function(chartInstance, easing) {
            // To only draw at the end of animation, check for easing === 1
            var ctx = chartInstance.chart.ctx;

            chartInstance.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.getDatasetMeta(i);
                if (!meta.hidden) {
                    meta.data.forEach(function(element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = 'rgb(0, 0, 0)';

                        var fontSize = 16;
                        var fontStyle = 'normal';
                        var fontFamily = 'Helvetica Neue';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                        // Just naively convert to string for now
                        var dataString = dataset.data[index].toString();

                        // Make sure alignment settings are correct
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';

                        var padding = 15;
                        var position = element.tooltipPosition();
                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                    });
                }
            });
        }
    });*/

/*plugin code end*/

Chart.pluginService.register({
  beforeInit: function(chart) {
    var hasWrappedTicks = chart.config.data.labels.some(function(label) {
      return label.indexOf('\n') !== -1;
    });

    if (hasWrappedTicks) {
      // figure out how many lines we need - use fontsize as the height of one line
      var tickFontSize = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].ticks.fontSize, Chart.defaults.global.defaultFontSize);
      var maxLines = chart.config.data.labels.reduce(function(maxLines, label) {
        return Math.max(maxLines, label.split('\n').length);
      }, 0);
      var height = (tickFontSize + 2) * maxLines + (chart.options.scales.xAxes[0].ticks.padding || 0);

      // insert a dummy box at the bottom - to reserve space for the labels
      Chart.layoutService.addBox(chart, {
        draw: Chart.helpers.noop,
        isHorizontal: function() {
          return true;
        },
        update: function() {
          return {
            height: this.height
          };
        },
        height: height,
        options: {
          position: 'bottom',
          fullWidth: 1,
        }
      });

      // turn off x axis ticks since we are managing it ourselves
      chart.options = Chart.helpers.configMerge(chart.options, {
        scales: {
          xAxes: [{
            ticks: {
              display: false,
              // set the fontSize to 0 so that extra labels are not forced on the right side
              fontSize: 0
            }
          }]
        }
      });

      chart.hasWrappedTicks = {
        tickFontSize: tickFontSize
      };
    }
  },
  afterDraw: function(chart) {
    if (chart.hasWrappedTicks) {
      // draw the labels and we are done!
      chart.chart.ctx.save();
      var tickFontSize = chart.hasWrappedTicks.tickFontSize;
      var tickFontStyle = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].ticks.fontStyle, Chart.defaults.global.defaultFontStyle);
      var tickFontFamily = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].ticks.fontFamily, Chart.defaults.global.defaultFontFamily);
      var tickLabelFont = Chart.helpers.fontString(tickFontSize, tickFontStyle, tickFontFamily);
      chart.chart.ctx.font = tickLabelFont;
      chart.chart.ctx.textAlign = 'center';
      var tickFontColor = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].fontColor, Chart.defaults.global.defaultFontColor);
      chart.chart.ctx.fillStyle = tickFontColor;

      var meta = chart.getDatasetMeta(0);
      var xScale = chart.scales[meta.xAxisID];
      var yScale = chart.scales[meta.yAxisID];

      chart.config.data.labels.forEach(function(label, i) {
        label.split('\n').forEach(function(line, j) {
          chart.chart.ctx.fillText(line, xScale.getPixelForTick(i + 0.5), (chart.options.scales.xAxes[0].ticks.padding || 0) + yScale.getPixelForValue(yScale.min) +
            // move j lines down
            j * (chart.hasWrappedTicks.tickFontSize + 2));
        });
      });

      chart.chart.ctx.restore();
    }
  }
});

/*pie chart*/

Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.doughnut);

var helpers = Chart.helpers;
var defaults = Chart.defaults;

Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
	updateElement: function(arc, index, reset) {
    var _this = this;
    var chart = _this.chart,
        chartArea = chart.chartArea,
        opts = chart.options,
        animationOpts = opts.animation,
        arcOpts = opts.elements.arc,
        centerX = (chartArea.left + chartArea.right) / 2,
        centerY = (chartArea.top + chartArea.bottom) / 2,
        startAngle = opts.rotation, // non reset case handled later
        endAngle = opts.rotation, // non reset case handled later
        dataset = _this.getDataset(),
        circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
        innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
        outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
        custom = arc.custom || {},
        valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

    helpers.extend(arc, {
      // Utility
      _datasetIndex: _this.index,
      _index: index,

      // Desired view properties
      _model: {
        x: centerX + chart.offsetX,
        y: centerY + chart.offsetY,
        startAngle: startAngle,
        endAngle: endAngle,
        circumference: circumference,
        outerRadius: outerRadius,
        innerRadius: innerRadius,
        label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
      },

      draw: function () {
      	var ctx = this._chart.ctx,
						vm = this._view,
						sA = vm.startAngle,
						eA = vm.endAngle,
						opts = this._chart.config.options;
				
					var labelPos = this.tooltipPosition();
					var segmentLabel = vm.circumference / opts.circumference * 100;
					
					ctx.beginPath();
					
					ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
					ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);
					
					ctx.closePath();
					ctx.strokeStyle = vm.borderColor;
					ctx.lineWidth = vm.borderWidth;
					
					ctx.fillStyle = vm.backgroundColor;
					
					ctx.fill();
					ctx.lineJoin = 'bevel';
					
					if (vm.borderWidth) {
						ctx.stroke();
					}
					
					if (vm.circumference > 0.15) { // Trying to hide label when it doesn't fit in segment
						ctx.beginPath();
						ctx.font = helpers.fontString(opts.defaultFontSize, opts.defaultFontStyle, opts.defaultFontFamily);
						ctx.fillStyle = "#fff";
						ctx.textBaseline = "top";
						ctx.textAlign = "center";
            
            // Round percentage in a way that it always adds up to 100%
						ctx.fillText(segmentLabel.toFixed(0) + "%", labelPos.x, labelPos.y);
					}
      }
    });

    var model = arc._model;
    model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
    model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
    model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
    model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

    // Set correct angles if not resetting
    if (!reset || !animationOpts.animateRotate) {
      if (index === 0) {
        model.startAngle = opts.rotation;
      } else {
        model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
      }

      model.endAngle = model.startAngle + model.circumference;
    }

    arc.pivot();
  }
});

/*pie*/

Chart.pluginService.register({
  beforeRender: function(chart) {
    if (chart.config.options.showAllTooltips) {
      // create an array of tooltips
      // we can't use the chart tooltip because there is only one tooltip per chart
      chart.pluginTooltips = [];
      chart.config.data.datasets.forEach(function(dataset, i) {
        chart.getDatasetMeta(i).data.forEach(function(sector, j) {
          chart.pluginTooltips.push(new Chart.Tooltip({
            _chart: chart.chart,
            _chartInstance: chart,
            _data: chart.data,
            _options: chart.options.tooltips,
            _active: [sector]
          }, chart));
        });
      });

      // turn off normal tooltips
      chart.options.tooltips.enabled = false;
    }
  },
  afterDraw: function(chart, easing) {
    if (chart.config.options.showAllTooltips) {
      // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
      if (!chart.allTooltipsOnce) {
        if (easing !== 1)
          return;
        chart.allTooltipsOnce = true;
      }

      // turn on tooltips
      chart.options.tooltips.enabled = true;
      Chart.helpers.each(chart.pluginTooltips, function(tooltip) {
        tooltip.initialize();
        tooltip.update();
        // we don't actually need this since we are not animating tooltips
        tooltip.pivot();
        tooltip.transition(easing).draw();
      });
      chart.options.tooltips.enabled = false;
    }
  }
});
Chart.defaults.global.plugins= {
  
 labels: {
          render: () => {}
        }
};
Chart.plugins.unregister(ChartDataLabels);

/*Chart('pie').defaults.global.plugins= {
  
 labels: {
          render:'percentage'
        }
};
*/
/*var function imagebackground(){
*/
	/*var backgroundColor = 'white';
Chart.plugins.register({
    beforeDraw: function(c) {
        var ctx = c.chart.ctx;
        ctx.fillStyle = backgroundColor;
        ctx.fillRect(0, 0, c.chart.width, c.chart.height);
    }
});*/
/*}*/
var plugin = {beforeDraw: function(c) {
        var ctx = c.chart.ctx;
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, c.chart.width, c.chart.height);
    } };
  var legendcallback=function(chart) {
			
  var text = [];
  text.push('<ul class="' + chart.id + '-legend">');
  for (var i = 0; i < chart.data.datasets[0].label.length; i++) {
  	
    text.push('<li style="width:100%;"><span style="background-color:' + 
    chart.data.datasets[0].backgroundColor[i] + ';"></span><span style="margin-left:5px;width:70%;font-family:Source Sans Pro;font-weight:600;color:#505050;font-size:14px;">');
      if (chart.data.datasets[0].label[i]) {
      text.push(chart.data.datasets[0].label[i]);
    }
    text.push('</span></li>');
  }
  text.push('</ul>');
  return text.join("");
};
var pie_legendcallback=function(chart) {
			
  var text = [];
  text.push('<ul class="' + chart.id + '-legend">');
  for (var i = 0; i < chart.data.labels.length; i++) {
  	
    text.push('<li style="width:100%;padding-bottom: 5px;"><span style="background-color:' + 
    chart.data.datasets[0].backgroundColor[i] + ';"></span><span style="margin-left:5px;width: 80%;font-family:Source Sans Pro;font-weight:600;color:#505050;font-size:14px;">');
      if (chart.data.labels[i]) {
      text.push(chart.data.labels[i]);
    }
    text.push('</span></li>');
  }
  text.push('</ul>');
  return text.join("");
};
var legendcallback_per=function(chart) {
			
  var text = [];
  text.push('<ul class="' + chart.id + '-legend">');
  for (var i = 0; i < chart.data.datasets.length; i++) {
    text.push('<li style="width:45%;padding-bottom: 5px;"><span style="background-color:' + 
    chart.data.datasets[i].backgroundColor[i] + ';"></span><span style="margin-left:5px;width:77%;font-family:Source Sans Pro;font-weight:600;color:#505050;font-size:14px;">');
      if (chart.data.datasets[i].label) {
      text.push(chart.data.datasets[i].label);
    }
    text.push('</span></li>');
  }
  text.push('</ul>');
  return text.join("");
};
        /*ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
ctx.fillStyle = c.chart.config.options.defaultFontColor;
ctx.textAlign = 'center';
ctx.textBaseline = 'bottom';
c.data.datasets.forEach(function (dataset) {
for (var i = 0; i < dataset.data.length; i++) {
var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
ctx.fillText(dataset.data[i], model.x, model.y - 5);
}
    });*/
/*}
});*/






$('#getmonthanalys2search').click(function(){
  alert("The paragraph was clicked.");
});



function changehead(type)
{
	
	if(type=='national')
	{
	var selct_hdng = document.getElementById("getmonthanalysnational");
	var hdng_text = document.getElementById("anti_hcv_state_heading");
	var text=selct_hdng.options[selct_hdng.selectedIndex].value;
	var across='states';
}

	else if(type=='district')
	{
	var selct_hdng = document.getElementById("getmonthanalys2");
	var hdng_text = document.getElementById('anti_hcv_dist_heading');
	var text=selct_hdng.options[selct_hdng.selectedIndex].value;
	var across='districts';
	}
	if(text=="anti_hcv_screened")
	{
	hdng_text.innerHTML="Patients screened for Anti-HCV across "+across;
	}
	 else if(text=="anti_hcv_positive")
	{
hdng_text.innerHTML="Anti-HCV positive patients across "+across;
	}
	else if(text=="viral_load_tested")
	{
	hdng_text.innerHTML	="VL Screened patients across "+across;
	}
	else if(text=="viral_load_detected")
	{
	hdng_text.innerHTML	="VL Detected patients  across "+across;
	}
	else if(text=="initiatied_on_treatment")
	{
		hdng_text.innerHTML="Patients Initiated on Treatment across "+across;
	}
	else if(text=="treatment_completed")
	{
		hdng_text.innerHTML="Patients treatment completed across "+across;
	}
	else if(text=="svr_done")
	{
		hdng_text.innerHTML="Patients SVR test done across "+across;
	}
	else if(text=="treatment_successful")
	{
		hdng_text.innerHTML="Treatment success of patients across "+across;
	}
	else if(text=="treatment_failure")
	{
		hdng_text.innerHTML="Treatment failure of patients  across "+across;
	}
}


function changehead_screened(type)
{
	if(type=='HAV_national')
	{
	var selct_hdng = document.getElementById("getmonthanalys3");
	var hdng_text = document.getElementById("head_hav_national");
	var text=selct_hdng.options[selct_hdng.selectedIndex].value;
	var typetext='HAV';
	var across='states';
	var typesm='hav';
}
else if(type=='HEV_national')
{
	var selct_hdng = document.getElementById("getmonthanalys4state");
	var hdng_text = document.getElementById("head_hev_national");
	var text=selct_hdng.options[selct_hdng.selectedIndex].value;
	var typetext='HEV';
	var typesm='hev';
	var across='states';
}
else if(type=='HAV_district')
	{
	var selct_hdng = document.getElementById("getmonthanalys3");
	var hdng_text = document.getElementById("head_hav_district");
	var text=selct_hdng.options[selct_hdng.selectedIndex].value;
	var typetext='HAV';
	var typesm='hav';
	var across='All Facilities';
}
else if(type=='HEV_district')
{
	var selct_hdng = document.getElementById("getmonthanalys4");
	var hdng_text = document.getElementById("head_hev_district");
	var text=selct_hdng.options[selct_hdng.selectedIndex].value;
	var typetext='HEV';
	var typesm='hev';
	var across='All Facilities';
}
	if(text=="screened_for_"+typesm)
	{
	hdng_text.innerHTML="Screened for "+ typetext +" across "+across;
	}
	 else if(text==typesm+"_positive_patients")
	{
hdng_text.innerHTML=typetext +" positive patients across "+across;;
	}
	else if(text=="patients_managed_at_facility"||text=="patients_managed_at_facility_hev")
	{
	hdng_text.innerHTML	="Patients managed at the facility across "+across;
	}
	else if(text=="patients_referred_for_management"||text=="patients_referred_for_management_hev")
	{
	hdng_text.innerHTML	="Patients referred for management across "+across;
	}
}



function Adherence_across(type)
{
	var type1='';
if(type=='State')
{
	type1='states';
var selct_hdng = document.getElementById("Adherence_across_state");
var hdng_text = document.getElementById("head_Adherence_state");
var text=selct_hdng.options[selct_hdng.selectedIndex].value;
}

else if(type=='Districts')
{
	type1='districts';
var selct_hdng = document.getElementById("Adherence_across_districts");
var hdng_text = document.getElementById('head_Adherence_districts');
var text=selct_hdng.options[selct_hdng.selectedIndex].value;
}
if(text=="reg_wise_reg1_SOF_DCV")
{
hdng_text.innerHTML="Adherence across "+type1+" for Regimen 1: SOF/DCV";
}
else if(text=="reg_wise_reg2_SOF_VEL")
{
hdng_text.innerHTML="Adherence across "+type1+" for Regimen 2: SOF/VEL";
}
else if(text=="reg_wise_reg3_SOF_VEL_Ribavirin")
{
hdng_text.innerHTML ="Adherence acrosss "+type1+" Regimen 3a: SOF/VEL/Ribavirin";
}
else if(text=="reg_wise_reg4_SOF_VEL_24_weeks")
{
hdng_text.innerHTML ="Adherence across "+type1+" Regimen 3b: SOF/VEL(24 weeks)";
}
else if(text=="reg_wise_others")
{
hdng_text.innerHTML ="Adherence across "+type1+" (Other Regimen)";
}

}


function casecase_vl_ajax()
    {
    var selct_hdng = document.getElementById("getmonthanalysnational");
    var name=selct_hdng.options[selct_hdng.selectedIndex].value;
    document.getElementById("exl_screenedacrossdistricts").href='<?= base_url() ?>Dashboard/treatment_Initiations_by_state_HCV_export/'+name;
       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/casecase_vl_export/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',flg:'true'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
    //alert(data);
       var ctx  = $("#screenedacrossdistricts_national");
        var returned = JSON.parse(data);
        var maxval=0;
        var labels = returned.screened_antihcv_across_state.map(function(e) {
   return e.hospital;
});
         var countIni = returned.screened_antihcv_across_state.map(function(e) {
   return e.countIni;
});
          for (i=0; i<countIni.length;i++){
    if (parseInt(countIni[i])>=maxval) {
        maxval=parseInt(countIni[i]);
    }
}
         var color = returned.screened_antihcv_across_state.map(function(e) {
   return e.color;
});
//console.log(data);
var data = {
labels: labels,
datasets: [
{
label : ["No. of Patients"],
backgroundColor:color,
borderColor: color,
borderWidth: 1,
data:countIni,
}
]
};

var options = {
responsive:true,
cornerRadius: 5,
  plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'500',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0";
							}
							else{
							return context.dataset.data[context.dataIndex];
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},


legend: false,
           
        layout: {
padding: {
left: 0,
right: 0,
top: 5,
bottom: 20
}}, tooltips: {
                        enabled: false
                        },events:[],
scales : {
xAxes : [{
gridLines: {
color: "rgba(0, 0, 0, 0)",
display:false,
},
ticks: {
          autoSkip: false,
          maxRotation: 90,
          minRotation: 90
        }
}],
yAxes : [{
	ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},
gridLines: {
color: "rgba(0, 0, 0, 0)",
display:false,

}
}]
}
};

var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
type: 'bar',
data: data,
options: options
});
    }
  });
       changehead("national");
}


function havscreened()
{

var selct_hdng = document.getElementById("getmonthanalys3");
var name=selct_hdng.options[selct_hdng.selectedIndex].value;
document.getElementById("exl_hav_screened").href='<?= base_url() ?>Dashboard/treatment_Initiations_by_state_HAV_export/excel/'+name;
       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/havscreened_export/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',flg:'true'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
    //alert(data);
       var ctx  = $("#screenedacrossdistricts_hav_national");
        var returned = JSON.parse(data);
        var maxval=0;
        var labels = returned.treatment_Initiations_by_Districthav_state.map(function(e) {
  return e.hospital;
});
         var countIni = returned.treatment_Initiations_by_Districthav_state.map(function(e) {
   return e.countIni;
});
          for (i=0; i<=countIni.length;i++){
    if (parseInt(countIni[i])>maxval) {
        maxval=parseInt(countIni[i]);
    }
}
         var color = returned.treatment_Initiations_by_Districthav_state.map(function(e) {
   return e.color;
});
//console.log(data);
var data = {
labels: labels,
datasets: [
{
label : ["No. of Patients"],
backgroundColor:color,
data:countIni,
}
]
};

var options = {
	  plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'500',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0";
							}
							else{
							return context.dataset.data[context.dataIndex];
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},
responsive:true,
maintainAspectRatio:false,


legend:false,
		legendCallback:legendcallback,
		cornerRadius:5,
        layout: {
padding: {
left: 0,
right: 0,
top: 25,
bottom: 20
}}, tooltips: {
                        enabled: false
                        },events:[],
scales : {
xAxes : [{
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
},
ticks: {
          autoSkip: false,
          maxRotation: 90,
          minRotation: 90,
        }
}],
yAxes : [{
	ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
}
}]
}
};

var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
type: 'bar',
data: data,
options: options
});
$('#Screened_HAV_State-legend').html(myBarChart.generateLegend());
    }
  });
       changehead_screened('HAV_national')
       
}


function hevscreened()
{
var selct_hdng = document.getElementById("getmonthanalys4state");
var name=selct_hdng.options[selct_hdng.selectedIndex].value;
document.getElementById("exl_hev_screened").href='<?= base_url() ?>Dashboard/treatment_Initiations_by_statehev_export/excel/'+name;
       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/hevscreened_export/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',flg:'true'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
    //alert(data);
       var ctx  = $("#screenedacrossstateshev");
       var maxval=0;
        var returned = JSON.parse(data);
        var labels = returned.treatment_Initiations_by_statehev.map(function(e) {
   return e.hospital;
});
         var countIni = returned.treatment_Initiations_by_statehev.map(function(e) {
   return e.countIni;
});
         for (i=0; i<=countIni.length;i++){
    if (parseInt(countIni[i])>maxval) {
        maxval=parseInt(countIni[i]);
    }
}
         var color = returned.treatment_Initiations_by_statehev.map(function(e) {
   return e.color;
});
//console.log(data);
var data = {
labels: labels,
datasets: [
{
label : ["No. of Patients"],
backgroundColor:color,
borderColor: color,
borderWidth: 1,
data:countIni,
}
]
};

var options = {
responsive:true,
maintainAspectRatio:false,
  plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'500',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0";
							}
							else{
							return context.dataset.data[context.dataIndex];
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},
 legend:false,
        legendCallback:legendcallback,
        cornerRadius:5,
           
        layout: {
padding: {
left: 0,
right: 0,
top: 5,
bottom: 20
}}, tooltips: {
                        enabled: false
                        },events:[],
scales : {
xAxes : [{
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
},
ticks: {
          autoSkip: false,
          maxRotation: 90,
          minRotation: 90
        }
}],
yAxes : [{
	stacked:true,
	ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
}
}]
}
};

var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
type: 'bar',
data: data,
options: options
});
$('#Screened_HEV_State-legend').html(myBarChart.generateLegend());
    }
  });
       changehead_screened('HEV_national')
       
}

function regimen_change_state(){

var selct_hdng = document.getElementById("Adherence_across_state");
var name=selct_hdng.options[selct_hdng.selectedIndex].value;
document.getElementById("regmen_state_exl").href='<?= base_url() ?>Dashboard/Adherence_state_export/'+name;
       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/Adherenceaccrossstate_export/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',flg:'true'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
    //alert(data);
       var ctx  = $("#AdherenceState");
        var returned = JSON.parse(data);
       var total=0; 
       var per=0;
    returned.regimen.forEach(function(obj) { total=total+parseFloat(obj.countIni); });
    //console.log(total);
   
       
        var labels = returned.regimen.map(function(e) {
   return e.hospital;
});
                /*var countIni = returned.regimen.map(function(e) {
   var countvald = (e.countIni);
   return parseFloat(countvald).toFixed(1);
});*/
         var countIni = returned.regimen.map(function(e) {
         	if(total>0){
         	per=parseFloat(e.countIni);
         	if(per>0.01 && per<1){
         		per=per.toFixed(2);
	           per=per.toString().replace(/^[0.]+/, ".");
         	}
         	else if(per==0 || isNaN(per)){
         		per=0;
         	}
         	else{
         		per=per.toFixed(2);
         	}
}else{
	per=0;
}
   return per;
});
         var color = returned.regimen.map(function(e) {
   return e.color;
});
//console.log(data);
var data = {
labels: labels,
datasets: [
{
label : ["No. of Patients"],
backgroundColor:color,
borderColor: color,
borderWidth: 1,
data:countIni,
}
]
};

var options = {
responsive: true,
maintainAspectRatio:false,
  plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'500',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0%";
							}
							else{
							return context.dataset.data[context.dataIndex]+"%";
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},

						/*legend: {
						display: false,
						position: 'bottom',
						labels: {
						boxWidth: 20,
						fontColor: '#111',
						padding: 15
						}
						},*/
						legend: false,
						cornerRadius:5,
						layout: {
						padding: {
						left: 0,
						right: 0,
						top: 38,
						bottom: 18
						}},
						tooltips: {
						enabled: false
						},events:[],
						scales : {
						xAxes : [{
						gridLines: {
						color: "rgba(0, 0, 0, 0)",
						},
						ticks: {
						autoSkip : false,
						maxRotation: 90,
						minRotation: 90,
						callback: function (value) {
						return value.toLocaleString('de-DE', {style:'percent'});
						},
						}
						}],
						yAxes : [{
						gridLines: {
						color: "rgba(0, 0, 0, 0)",
						},
						/*ticks: {
						callback: function (value) {
						return value.toLocaleString('de-DE', {style:'percent'});
						},
						}*/
				ticks: {stepSize: 20,
						beginAtZero:true,
						min: 0,
						max: 100,
						callback: function (ticks) {
							return ticks;
					},
						},scaleLabel: {
        display: true,
        labelString: 'In Percentage (%)'
      },
						
						}]
						}
						};


var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
type: 'bar',
data: data,
options: options
});
    }
  });
   Adherence_across("State");  
}

/*state Code*/


function havscreened_district()
{
var selct_hdng = document.getElementById("getmonthanalys3");
var name=selct_hdng.options[selct_hdng.selectedIndex].value;
document.getElementById("exl_hav_screened_dis").href='<?= base_url() ?>Dashboard/treatment_Initiations_by_District_HAV_export/excel/'+name;

       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/havscreened_district_export/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',flg:'true'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
    //alert(data);
       var ctx  = $("#screenedacrossdistrictshav");
        var returned = JSON.parse(data);
        var maxval=0;
        var labels = returned.treatment_Initiations_by_Districthav.map(function(e) {
   return e.hospital;
});
         var countIni = returned.treatment_Initiations_by_Districthav.map(function(e) {
   return e.countIni;
});

          for (i=0; i<=countIni.length;i++){
    if (parseInt(countIni[i])>maxval) {
        maxval=parseInt(countIni[i]);
    }
}
         var color = returned.treatment_Initiations_by_Districthav.map(function(e) {
   return e.color;
});
//console.log(data);
var data = {
labels: labels,
datasets: [
{
label : ["No. of Patients"],
backgroundColor:color,
data:countIni,
}
]
};

var options = {
responsive:true,
maintainAspectRatio:false,
animation: {
duration: 0,
onComplete: function () {

// render the value of the chart above the bar
var ctx = this.chart.ctx;
ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
ctx.fillStyle = this.chart.config.options.defaultFontColor;
ctx.textAlign = 'center';
ctx.textBaseline = 'bottom';
this.data.datasets.forEach(function (dataset) {
for (var i = 0; i < dataset.data.length; i++) {
var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
ctx.fillText(dataset.data[i], model.x, model.y - 5);
}
});
}},

/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 35
            }
        },*/
        legend:false,
        legendCallback:legendcallback,
        cornerRadius:5,
        layout: {
padding: {
left: 0,
right: 0,
top: 25,
bottom: 0
}},
            tooltips: {
                        enabled: false
                        },events:[],
scales : {
xAxes : [{
	<?php if(count($treatment_Initiations_by_Districthav)<=10){
					echo 'barThickness:60,';
				};?>
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
},
ticks :{
autoSkip :false,
maxRotation:90,
minRotation:90
}
}],
yAxes : [{
	ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},

gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
}
}]
}
};

var myBarChart = new Chart(ctx, {plugins: [plugin],
type: 'bar',
data: data,
options: options
});
    }
  });
       changehead_screened('HAV_district')
       
}


function hevscreened_district()
{
var selct_hdng = document.getElementById("getmonthanalys4");
var name=selct_hdng.options[selct_hdng.selectedIndex].value;
document.getElementById("exl_hev_screened_dis").href='<?= base_url() ?>Dashboard/treatment_Initiations_by_District_HEV_export/excel/'+name;
       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/hevscreened_district_export/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',flg:'true'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
    //alert(data);
       var ctx  = $("#screenedacrossdistrictshev");
        var returned = JSON.parse(data);
        maxval=0;
        var labels = returned.treatment_Initiations_by_statehev.map(function(e) {
   return e.hospital;
});
         var countIni = returned.treatment_Initiations_by_statehev.map(function(e) {
   return e.countIni;
});

          for (i=0; i<=countIni.length;i++){
    if (parseInt(countIni[i])>maxval) {
        maxval=parseInt(countIni[i]);
    }
}
         var color = returned.treatment_Initiations_by_statehev.map(function(e) {
   return e.color;
});
//console.log(data);
var data = {
labels: labels,
datasets: [
{
label : ["No. of Patients"],
backgroundColor:color,
borderColor: color,
borderWidth: 1,
data:countIni,
}
]
};

var options = {
responsive:true,
maintainAspectRatio:false,
animation: {
duration: 0,
onComplete: function () {

// render the value of the chart above the bar
var ctx = this.chart.ctx;
ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
ctx.fillStyle = this.chart.config.options.defaultFontColor;
ctx.textAlign = 'center';
ctx.textBaseline = 'bottom';
this.data.datasets.forEach(function (dataset) {
for (var i = 0; i < dataset.data.length; i++) {
var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
ctx.fillText(dataset.data[i], model.x, model.y - 5);
}
});
}},

/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 15
            }
        },*/
        legend: false,
		legendCallback:legendcallback,
		cornerRadius:5,
        layout: {
padding: {
left: 0,
right: 0,
top: 25,
bottom: 0
}},
            tooltips: {
                        enabled: false
                        },events:[],
scales : {
xAxes : [{
	<?php if(count($treatment_Initiations_by_Districthev)<=10){
					echo 'barThickness:60,';
				}; ?>
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
},
ticks :{
autoSkip :false,
maxRotation:90,
minRotation:90
}
}],
yAxes : [{
	ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						 max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},

gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
}
}]
}
};

var myBarChart = new Chart(ctx, {plugins: [plugin],
type: 'bar',
data: data,
options: options
});
    }
  });
       changehead_screened('HEV_district');
       
}



function regimen_change_district(){
var selct_hdng = document.getElementById("Adherence_across_districts");
var name=selct_hdng.options[selct_hdng.selectedIndex].value;
document.getElementById("exl_adhernce_export").href='<?= base_url() ?>Dashboard/AdherenceDistricts_export/'+name;

       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/Adherenceaccross_district_export/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',flg:'true'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
    //alert(data);

       var ctx  = $("#AdherenceDistricts");
        var returned = JSON.parse(data);
        var total=0;
        var per=0;
    returned.regimen.forEach(function(obj) { total=total+parseInt(obj.countIni,10); });
    //console.log(total);
        var labels = returned.regimen.map(function(e) {
   return e.hospital;
});
         var countIni = returned.regimen.map(function(e) {
         	if(total>0){
   per=parseFloat(e.countIni);
         	if(per>0.01 && per<1){
         		per=per.toFixed(2);
         	}
         	else if(per==0 || isNaN(per)){
         		per=0;
         	}
         	else{
         		per=per.toFixed(2);
         	}
}else{
	per=0;
}
   return per;
});
         var color = returned.regimen.map(function(e) {
   return e.color;
});
//console.log(data);
var data = {
labels: labels,
datasets: [
{
label : ["No. of Patients"],
backgroundColor:color,
borderColor: color,
borderWidth: 1,
data:countIni,
}
]
};

var options = {
         plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'normal',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0%";
							}
							else{
							return context.dataset.data[context.dataIndex]+"%";
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},
							responsive: true,
							maintainAspectRatio:false,

/*legend: {
            display: false,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 15
            }
        },*/
        legend:false,
        legendCallback:legendcallback,
        cornerRadius:5,
        layout: {
padding: {
left: 0,
right: 0,
top: 45,
bottom: 0
}},
            tooltips: {
                        enabled: false
                        },events:[],
scales : {
xAxes : [{
		<?php if(count($Adherenceaccross_district)<=10){
					echo 'barThickness:60,';
				}; ?>
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
},
ticks: {
autoSkip : false,
callback: function (value) {
return value.toLocaleString('de-DE', {style:'percent'});
},
maxRotation:90,
minRotation:90
}
}],
yAxes : [{
	ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						stepSize:20,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},scaleLabel: {
        display: true,
        labelString: ' In Percentage (%)'
      },
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
},
/*ticks: {
callback: function (value) {
return value.toLocaleString('de-DE', {style:'percent'});
},
}*/

}]
}
};
var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
type: 'bar',
data: data,
options: options
});
    }
  });
   Adherence_across("Districts");  
}


function hcv_screened_district()
    {
    var selct_hdng = document.getElementById("getmonthanalys2");
    var name=selct_hdng.options[selct_hdng.selectedIndex].value;
    document.getElementById("exl_hcv_screened").href='<?= base_url() ?>Dashboard/treatment_Initiations_by_District_HCV_export/excel/'+name;
       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/hcv_screened_dis/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',flg:'true'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
    //alert(data);
       var ctx  = $("#screenedacrossdistricts");
        var returned = JSON.parse(data);
        var maxval=0;
        var labels = returned.screened_hcv_across_dis.map(function(e) {
   return e.hospital;
});
         var countIni = returned.screened_hcv_across_dis.map(function(e) {
   return e.countIni;
});

          for (i=0; i<=countIni.length;i++){
    if (parseInt(countIni[i])>maxval) {
        maxval=parseInt(countIni[i]);
    }
}
         var color = returned.screened_hcv_across_dis.map(function(e) {
   return e.color;
});
//console.log(data);
var data = {
labels: labels,
datasets: [
{
label : ["No. of Patients"],
backgroundColor:color,
data:countIni,
}
]
};

var options = {
responsive:true,
maintainAspectRatio:false,
animation: {
duration: 0,
onComplete: function () {
/*var cascade_url_base64jp = document.getElementById("screenedacrossdistricts").toDataURL("image/png");
document.getElementById("screenedacrossdistrictsLink").href=cascade_url_base64jp;*/
// render the value of the chart above the bar
var ctx = this.chart.ctx;
ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
ctx.fillStyle = this.chart.config.options.defaultFontColor;
ctx.textAlign = 'center';
ctx.textBaseline = 'bottom';
this.data.datasets.forEach(function (dataset) {
for (var i = 0; i < dataset.data.length; i++) {
var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
ctx.fillText(dataset.data[i], model.x, model.y - 5);
}
});
}},

/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 15
            }
        },*/
        legend: false,
			legendCallback:legendcallback,
			cornerRadius:5,
        layout: {
padding: {
left: 0,
right: 0,
top: 25,
bottom: 0
}},
            tooltips: {
                        enabled: false
                        },events:[],
scales : {
xAxes : [{
	<?php if(count($treatment_Initiations_by_District)<=10){
					echo 'barThickness:40,';
				}; ?>
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
},
stacked: true,
ticks: {
beginAtZero:true,
autoSkip : false,
maxRotation:90,
minRotation:90
},
}],
yAxes : [{
	ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},

gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
}
}]
}
};

var myBarChart = new Chart(ctx, { plugins: [plugin],
type: 'bar',
data: data,
options: options
});

    }
  });
       
       changehead("district");
}

function age_wise_Factor_ajax()
    {
    var selct_hdng = document.getElementById("rickfactorany2");
    var name=selct_hdng.options[selct_hdng.selectedIndex].value;
    document.getElementById("exl_age_wise").href='<?= base_url() ?>Dashboard/Age_wise_Risk_Factor_Analysis/'+name;
       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/age_wise_factor_export/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
    //alert(data);
       var ctx  = $("#agewisefactorany");
        var returned = JSON.parse(data);
        console.log(returned);
        var labels = returned.age_wise_factor.map(function(e) {
   return e.LookupValue;
});
         var countIni = returned.age_wise_factor.map(function(e) {
   return e.count;
});
//console.log(data);
var data = {
labels:labels,
datasets: [
{
data: countIni,
backgroundColor: [
			'#ffcece',
			'#745c97',
			'#4baea0',
			'#7fe7cc', 
			'#e56a54',
			'#ffb35e',
			'#69779b',
			'#CB4335',
			'#D4AC0D',
			'#283747',
			]
}]
};

var options = {
	responsive:true,
	maintainAspectRatio:false,
		animation    : {

			animateRotate: true,
			duration     : 2000,
		},
tooltips: false,
layout: {
padding: {
				left:0,
				right: 0,
				top: 10,
				bottom: 10
				},

},events:[],


elements: {
line: {
fill: false
},
point: {
hoverRadius: 7,
radius: 15
},
},rotation: (-0.5*Math.PI) - (65/180 * Math.PI),
plugins: {
title: false,
outlabels: {
   borderRadius: 0, // Border radius of Label
   borderWidth: 1, // Thickness of border
   color: 'white', // Font color
   display: true,
   lineWidth: 3, // Thickness of line between chart arc and Label
   padding: 3,
   stretch: 8, // The length between chart arc and Label
   font: {
resizable: true,
minSize: 12,
maxSize: 18
},
   text:function(context) {
				var index = context.dataIndex;
				var num=context.dataset.data[index] || 0;
				var value = "%p.2 ("+(context.dataset.data[index])+")";
				if(num==0||isNaN(num)){
					return "";	
				}
				else{
					return value;
				}
				
				},
   textAlign: "center"
}
},zoomOutPercentage: 10,
legend:false,
	/*legend: {
           display: true,
           position: 'bottom',
           labels: {
               boxWidth: 20,
               fontColor: '#111',
               padding: 10
           }
       },*/
};
var myPieChart = new Chart(ctx,{plugins:[plugin],
type: 'outlabeledPie',
data: data,
options: options
});
    }
  });
}
function gender_wise_Factor_ajax()
    {
    var selct_hdng = document.getElementById("rickfactorany1");
    var name=selct_hdng.options[selct_hdng.selectedIndex].value;
    document.getElementById("exl_gender_wise").href='<?= base_url() ?>Dashboard/Gender_wise_Risk_Factor_Analysis/'+name;
       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/gender_wise_factor_export/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
    //alert(data);
       var ctx  = $("#genderriskfactorany");
        var returned = JSON.parse(data);
        console.log(returned);
        var labels = returned.gender_wise_factor.map(function(e) {
   return e.LookupValue;
});
         var countIni = returned.gender_wise_factor.map(function(e) {
   return e.count;
});
//console.log(data);
var data = {
labels:labels,
datasets: [
{
data: countIni,
backgroundColor: [
			'#745c97',
			'#ffcece',
			'#4baea0',
			'#7fe7cc', 
			'#e56a54',
			'#ffb35e',
			'#69779b',
			'#CB4335',
			'#D4AC0D',
			'#283747',
			]
		}]
	};
	var options = {
		responsive:true,
		maintainAspectRatio:false,
		animation    : {

			animateRotate: true,
			duration     : 2000,
		},
		/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 20
            }
        },*/
        legend:false,
        legendCallback:pie_legendcallback,
        rotation: (-0.5*Math.PI) - (65/180 * Math.PI),
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 20,
				bottom: 25
				}},
		 	plugins: {
				title: false,
				outlabels: {
				borderRadius: 0, // Border radius of Label
				borderWidth: 1, // Thickness of border
				color: 'white', // Font color
				display: true,
				lineWidth: 3, // Thickness of line between chart arc and Label
				padding: 3,
				stretch: 10, // The length between chart arc and Label
				font: {
				resizable: true,
				minSize: 12,
				maxSize: 18
				},
				text:function(context) {
				var index = context.dataIndex;
				var num=context.dataset.data[index] || 0;
				var value = "%p.2 ("+(context.dataset.data[index])+")";
				if(num==0||isNaN(num)){
					return "";	
				}
				else{
					return value;
				}
				
				},
				textAlign: "center"
				}
				},
       /*tooltips: {
			callbacks: {
				label: function(tooltipItem, data) {
					var percentage = Math.round(((data.datasets[0].data[tooltipItem.index]/total)*100)*100)/100;
					return data.datasets[0].data[tooltipItem.index]+" , "+percentage+"%"; 
				}
			}
		}, showAllTooltips: true,
		*/events:[],
	};
	var myPieChart = new Chart(ctx,{plugins:[plugin],
		type: 'pie',
		data: data,
		options: options
	});
    }
  });
       $('#gender_wise_risk-legend').html(myPieChart.generateLegend());
}


// age wise ajax

function gender_wise_agewisevl_ajax()
    {

    var selct_hdng = document.getElementById("agewisevl");
    var name=selct_hdng.options[selct_hdng.selectedIndex].value;

    if(name==1){
    	drawagewisedistributionChart();

    }else{
   drawgenderriskfactoranyChartdata();
   
   }
}





function drawgenderriskfactoranyChartdata()
{

	$('.pie_chart_div_1').css("height","324px");
var ctx = $("#agewisedistribution");

<?php echo count($age_genderd); ?>;

var data = {
labels: [

"Male", "Female", "Transgender",
],
datasets: [
{
data: [

"<?php echo $age_genderd[0]->male; ?>",
"<?php echo $age_genderd[0]->female; ?>",
"<?php echo $age_genderd[0]->transgender; ?>",



],
backgroundColor: [
'#745c97',
'#ffcece',
'#4baea0',

]
}]
};
var options = {
		responsive:true,
		maintainAspectRatio:false,
		animation    : {

			animateRotate: true,
			duration     : 2000,
		},
		/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 20
            }
        },*/
        legend:false,
        legendCallback:pie_legendcallback,

        rotation: (-0.5*Math.PI) - (65/180 * Math.PI),
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 20,
				bottom: 25
				}},
		 	plugins: {
				title: false,
				outlabels: {
				borderRadius: 0, // Border radius of Label
				borderWidth: 1, // Thickness of border
				color: 'white', // Font color
				display: true,
				lineWidth: 3, // Thickness of line between chart arc and Label
				padding: 3,
				stretch: 10, // The length between chart arc and Label
				font: {
				resizable: true,
				minSize: 12,
				maxSize: 18
				},
				text:function(context) {
				var index = context.dataIndex;
				var num=context.dataset.data[index] || 0;
				var value = "%p.2 ("+(context.dataset.data[index])+")";
				if(num==0||isNaN(num)){
					return "";	
				}
				else{
					return value;
				}
				
				},
				textAlign: "center"
				}
				},zoomOutPercentage:15,
       /*tooltips: {
			callbacks: {
				label: function(tooltipItem, data) {
					var percentage = Math.round(((data.datasets[0].data[tooltipItem.index]/total)*100)*100)/100;
					return data.datasets[0].data[tooltipItem.index]+" , "+percentage+"%"; 
				}
			}
		}, showAllTooltips: true,
		*/events:[],
	};
var myPieChart = new Chart(ctx,{plugins:[plugin],
type: 'outlabeledPie',
data: data,
options: options
});
$('#Age_wise_vl-legend').html(myPieChart.generateLegend());
}


//LTFU Ajax


function changeheadhcv(type)
{
	
	if(type=='national')
	{
	var selct_hdng = document.getElementById("ltfu_across_state");
	var hdng_text = document.getElementById("ltfu_title");
	var text=selct_hdng.options[selct_hdng.selectedIndex].value;
	var across='states';
}

	else if(type=='district')
	{
	var selct_hdng = document.getElementById("ltfu_across_district");
	var hdng_text = document.getElementById('ltfu_title_dis');
	var text=selct_hdng.options[selct_hdng.selectedIndex].value;
	var across='districts';
	}
	if(text=="loss_of_follow_viral_load")
	{
	hdng_text.innerHTML="Lost to follow up across  "+across+" - Viral Load (in numbers) ";
	}
	 else if(text=="loss_of_follow_1St_dispensation")
	{
hdng_text.innerHTML="Lost to follow up across  "+across+" - 1st Dispensation (in numbers) ";
	}
	else if(text=="loss_of_follow_2St_dispensation")
	{
	hdng_text.innerHTML	="Lost to follow up across "+across+" - 2nd Dispensation (in numbers) ";
	}
	else if(text=="loss_of_follow_3St_dispensation")
	{
	hdng_text.innerHTML	="Lost to follow up across "+across+" - 3rd Dispensation (in numbers) ";
	}
	else if(text=="loss_of_follow_4St_dispensation")
	{
		hdng_text.innerHTML="Lost to follow up across "+across+" - 4th Dispensation (in numbers) ";
	}
	else if(text=="loss_of_follow_5St_dispensation")
	{
		hdng_text.innerHTML="Lost to follow up across "+across+" - 5th Dispensation (in numbers) ";
	}
	else if(text=="loss_of_follow_6St_dispensation")
	{
		hdng_text.innerHTML="Lost to follow up across "+across+" - 6th Dispensation (in numbers) ";
	}
	else if(text=="loss_of_follow_SVR")
	{
		hdng_text.innerHTML="Lost to follow up across "+across+" - SVR (in numbers) ";
	}
	
}


function ltfu_change_state()
    {
    var selct_hdng = document.getElementById("ltfu_across_state");
    var name=selct_hdng.options[selct_hdng.selectedIndex].value;
   // alert(name);
    document.getElementById("lossfollowacrossdistnumLink").href='<?= base_url() ?>Dashboard/lossfollowacross_State_num_export/'+name;
       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/ltfu_count_ajax/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',flg:'true'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
    //alert(data);
       var ctx  = $("#lossfollowacrossstatetnum");
        var returned = JSON.parse(data);
        var maxval=0;
        var labels = returned.ltfu_count_ajax.map(function(e) {
   return e.hospital;
});
         var countIni = returned.ltfu_count_ajax.map(function(e) {
   return e.countIni;
});
          for (i=0; i<countIni.length;i++){
    if (parseInt(countIni[i])>=maxval) {
        maxval=parseInt(countIni[i]);
    }
}
         var color = returned.ltfu_count_ajax.map(function(e) {
   return e.color;
});
//console.log(data);
var data = {
labels: labels,
datasets: [
{
label : ["No. of Patients"],
backgroundColor:color,
borderColor: color,
borderWidth: 1,
data:countIni,
}
]
};

var options = {
responsive:true,
cornerRadius: 5,
  plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'500',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0";
							}
							else{
							return context.dataset.data[context.dataIndex];
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},


legend: false,
           
        layout: {
padding: {
left: 0,
right: 0,
top: 5,
bottom: 20
}}, tooltips: {
                        enabled: false
                        },events:[],
scales : {
xAxes : [{
gridLines: {
color: "rgba(0, 0, 0, 0)",
display:false,
},
ticks: {
          autoSkip: false,
          maxRotation: 90,
          minRotation: 90
        }
}],
yAxes : [{
	ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},
gridLines: {
color: "rgba(0, 0, 0, 0)",
display:false,

}
}]
}
};

var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
type: 'bar',
data: data,
options: options
});
    }
  });
       changeheadhcv("national");
}

// Adhrance

function ltfu_change_facility()
    {
    var selct_hdng = document.getElementById("ltfu_across_district");
    var name=selct_hdng.options[selct_hdng.selectedIndex].value;
  // alert(name);
    document.getElementById("lossfollowacrossdistnumLink").href='<?= base_url() ?>Dashboard/lossfollowacross_State_num_export/'+name;
       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/ltfu_count_dis_ajax/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',flg:'true'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
    //alert(data);
       var ctx  = $("#lossfollowacrossdistnum");
        var returned = JSON.parse(data);
        var maxval=0;
        var labels = returned.ltfu_count_dis_ajax.map(function(e) {
   return e.hospital;
});
         var countIni = returned.ltfu_count_dis_ajax.map(function(e) {
   return e.countIni;
});
          for (i=0; i<countIni.length;i++){
    if (parseInt(countIni[i])>=maxval) {
        maxval=parseInt(countIni[i]);
    }
}
         var color = returned.ltfu_count_dis_ajax.map(function(e) {
   return e.color;
});
//console.log(data);
var data = {
labels: labels,
datasets: [
{
label : ["No. of Patients"],
backgroundColor:color,
borderColor: color,
borderWidth: 1,
data:countIni,
}
]
};

var options = {
responsive:true,
cornerRadius: 5,
  plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'500',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0";
							}
							else{
							return context.dataset.data[context.dataIndex];
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},


legend: false,
           
        layout: {
padding: {
left: 0,
right: 0,
top: 5,
bottom: 20
}}, tooltips: {
                        enabled: false
                        },events:[],
scales : {
xAxes : [{
gridLines: {
color: "rgba(0, 0, 0, 0)",
display:false,
},
ticks: {
          autoSkip: false,
          maxRotation: 90,
          minRotation: 90
        }
}],
yAxes : [{
	ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},
gridLines: {
color: "rgba(0, 0, 0, 0)",
display:false,

}
}]
}
};

var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
type: 'bar',
data: data,
options: options
});
    }
  });
       changeheadhcv("district");
}



</script>
