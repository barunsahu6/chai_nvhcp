<style>
#map { height: 400px; margin: 0; padding: 0; overflow: hidden; }
  .nicebox {
    position: absolute;
    text-align: center;
    font-family: "Roboto", "Arial", sans-serif;
    font-size: 13px;
    z-index: 5;
    box-shadow: 0 4px 6px -4px #333;
    padding: 5px 10px;
    background: rgb(150, 150, 150);
    background: linear-gradient(to bottom,rgba(255,255,255,1) 0%,rgba(245,245,245,1) 100%);
    border: rgb(197, 197, 197) 1px solid;
  }
  #controls {
    top: 10px;
    left: 110px;
    width: 360px;
    height: 45px;
  }
  #data-box {
    top: 10px;
    left: 500px;
    height: 45px;
    line-height: 45px;
    display: none;
  }
  #census-variable {
    width: 360px;
    height: 20px;
  }
  #legend { display: flex; display: -webkit-box; padding-top: 7px }
  .color-key {
    background: linear-gradient(to right,
      hsl(5, 69%, 54%) 0%,
      hsl(29, 71%, 51%) 17%,
      hsl(54, 74%, 47%) 33%,
      hsl(78, 76%, 44%) 50%,
      hsl(102, 78%, 41%) 67%,
      hsl(127, 81%, 37%) 83%,
      hsl(151, 83%, 34%) 100%);
    flex: 1;
    -webkit-box-flex: 1;
    margin: 0 5px;
    text-align: left;
    font-size: 1.0em;
    line-height: 1.0em;
  }
  #data-value { font-size: 2.0em; font-weight: bold }
  #data-label { font-size: 2.0em; font-weight: normal; padding-right: 10px; }
  #data-label:after { content: ':' }
  #data-caret { margin-left: -5px; display: none; font-size: 14px; width: 14px}


	.filter-div{
	margin-right: 2rem;
	}
	.row,.left-div,.right-div{
		background-color: #ffffff !important;
	}
.div_collapse{background-color:#ffffff;}
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}
.chart-legend_flex li span{
    display: inline-block;
    width: 14px;
    height: 14px;
    margin-right: 5px;
    border-radius: 7px;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    -webkit-border-radius: 14px;
    -moz-border-radius: 14px;
}
.chart-legend li span{
    display: inline-block;
    width: 14px;
    height: 14px;
    margin-right: 5px;
    border-radius: 7px;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    -webkit-border-radius: 14px;
    -moz-border-radius: 14px;
}
.chart-legend_flex ul{
	display: flex;
}
ul {
  list-style: none;
}

ul li::before {
  display: none; 
}
.side_btn {
            float: right;
            width: auto;
            height: auto;
        }

.chk {
            display: block;
            float: right;
            width: 350px;
            height: auto;
            position: absolute;
            right: -10px;
            top: 35px;
            background-color: #fbfbfb;
            border: solid 1px LightGray;
            z-index: 1000;
        }

        .side_ul {
            float: left;
            height: auto;
            width: auto;
        }
.input_fields
{
	height: 30px !important;
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}
h4,h5,label,input,p,span,button{
	font-family: 'Source Sans Pro';
	color: #505050;
}
.panel-title{
	font-family: 'Source Sans Pro';
	font-weight: 400;
	letter-spacing: .5px;
	font-size: 24px;
}
.sub-heading{
	font-family: 'Source Sans Pro';
	font-weight: 600;
	font-size: 18px;
}
@media (min-width: 720px){
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
	.container,.innerpadding{
	width: 76vw;
}
.pd-lr{padding-left: 18px; padding-right: 18px;}

.state_div
{
	position: relative;margin: auto;height: 365px;width: 100%;
}
.risk_factor_div
{
	position: relative;margin: auto;height: 375px;width: 100%;
}
.cascade_div
{
	position: relative;margin: auto;height: 300px;width: 100%;
}
.ageChart_div{
	position: relative;margin: auto;height: 260px;width: 100%;
}
#ageChart{
	width: 100%;
}
.cascade_scroll-x,.state_div,.district_scroll_x{
	padding-right: 2px;
}
.left-div{
	padding-left: 0px;

}
.right-div{
	padding-right: 0px;
}
.pie_chart_div{
	position: relative;margin: auto;height: 242px;width: 100%;margin-left: -55px;
}

.mar-right{padding: 0px;margin-right: -40px;}
.mar-left{padding: 0px;margin-left: -120px;}
.ml-20{margin-left: -20px;}
.mr-30{margin-right: 30px;}
.pie-chart_legend_margin{
margin-left: 80px;margin-right: -21px;
}
.top-margin{margin-top: -50px;padding-bottom: 3px;}
#vl-legend{margin-top: 36px;padding-bottom: 0px;}
#vl-legend_div{margin-left: 37px;margin-right: -37px;}
#successratiosvrtest-legend{margin-top: 35px;padding-bottom: 0px;}
.Age_wise_risk-legend_div{margin-left: 5.0em;margin-right: -5.1em;margin-top: -76px;}
.panel-title {
    margin-top: 0;
    margin-bottom: 0;
    font-size: 18px;
    color: #0c0c0c;
}
}

@media (max-width: 720px) {
	.mr-30{margin-right: 0px;}
	.panel-title{
    margin-top: 0;
    margin-bottom: 0;
    font-size: 16px;
}
	.state_scroll-x,.cascade_scroll-x{
		overflow-x: scroll;
	}
	.ml-20{margin-left: -20px;}
	.left-div,.right-div{
		padding-right: 0px;
		padding-left: 0px;
	}
.state_div
{
	position: relative;margin: auto;height: 70vh;width: 180vw;
}

.cascade_div
{
	position: relative;margin: auto;height: 60vh;width: 140vw;
}
.ageChart_div{
	position: relative;margin: auto;height: 60vh;width: 100%;
}
.risk_factor_div
{
	position: relative;margin: auto;height: 60vh;width:140vw;
}
.mar-right{padding: 0px;margin-right: 0px;}
.mar-left{padding: 0px;margin-left: 0px;}
.pie_chart_div{
	position: relative;margin: auto;height: 35vh;width: 100vw;
}
.risk_factor_ml-25{margin-left: -40px;}
.pie-chart_legend_margin{
margin-left: 84px;margin-right: -90px;margin-top: 5px;margin-left: -40px;
}
.top-margin{margin-top: 5px;margin-left: 100px;margin-right: -104px;}
.Age_wise_risk-legend_div{margin-left: 5.0em;margin-right: -5.1em;margin-top: 0px;}
#diseasevlpositive,#successratiosvrtest{margin-left: -30px;margin-top: 10px;}
}

	.input_fields
	{
		height: 30px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-family: 'Source Sans Pro';
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn,.btn1{
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	font-family: 'Source Sans Pro';
	line-height: 2;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
	font-family: 'Source Sans Pro';
}
a.btn1
{
	text-decoration: none;
	color : #FFFF;
	background-color: #1cacd7;
	font-family: 'Source Sans Pro';
}
.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	font-family: 'Source Sans Pro';
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
	border-radius: 0;
}
.col-md-offset-3,.col-lg-offset-3{
	margin-left: 22%;
}
.btn-success:hover
{
	text-decoration: none !important;
	color: #A30A0C !important;
	background-color: white !important;
	border : 1px solid #A30A0C;
	border-radius: 0;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
.panel-default>.panel-heading {
    background-color: #595959;
    cursor: pointer;
}

/*.events{
margin: 10px 20px;
}*/
.backcolors{
	background: #f4fafe82;
}

</style>

<?php $loginData = $this->session->userdata('loginData');
$filters = $this->session->userdata('filters');
//echo "<pre>"; print_r($geographical_distribution);

if($loginData->user_type==1) { 
$select = '';
}else{
$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
$select4 = '';	
}
if(($loginData->user_type==2 || $loginData->user_type==3 || $loginData->user_type==1)&&($filters['id_mstfacility']!=='' || $filters['id_mstfacility']!==0)){ 
	if(!(isset($mtc_user[0]) ? $mtc_user[0]->is_Mtc==1 : '')  ){
$coloffset="col-lg-7 col-md-7 col-xs-12 col-sm-12 col-md-offset-3 col-lg-offset-3 col-sm-offset-0 col-xs-offset-0";
$coloffset_addhernce_per="col-lg-6 col-md-6 col-xs-12 col-sm-12 col-md-offset-3 col-lg-offset-3 col-sm-offset-0 col-xs-offset-0";
	}
	else{
		$coloffset_addhernce_per="col-lg-7 col-md-7 col-xs-12 col-sm-12 col-md-offset-3 col-lg-offset-3 col-sm-offset-0 col-xs-offset-0";
	$coloffset="col-lg-6 col-md-6 col-xs-12 col-sm-12";
}}
if(($loginData->user_type==1 || $loginData->user_type==3)&&($filters['id_mstfacility']=='' || $filters['id_mstfacility']==0)){
	$coloffset_addhernce_per="col-lg-6 col-md-6 col-xs-12 col-sm-12";

}


 ?>
<?php //pr($adherenceper); echo 'fff'.$adherenceper['COUNT']['4']->COUNT;exit(); ?>
<div class="container">
 <!--  <div class="row" style="padding: 0px;">
  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 left-div">

  <div class="card well events" style="background-color: #fff;border-bottom-width:5px;border-bottom-color:#35c4f9; text-align:center;">
    <div class="text-center " style="min-height: 53px;">
     <h3 style="margin: 5px;color: #000000;font-size:36px;font-weight: 500" class="count"><?php //echo $cascade->initiatied_on_treatment; ?></h3>
      <h4 style="font-size: 14px;margin: 0px;color: #000000;letter-spacing: 1px;">Initiated on Treatment</h4>
    </div>
</div>

  	</div>

  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 right-div">

				<div class="card well events" style="background-color: #fff;border-bottom-width:5px;border-bottom-color:#ef6c00; text-align:center;height: 107px;">
				<div class="col-sm-12 col-md-1 pull-right text-right glyphicon glyphicon-info-sign" style="font-size: 14px; padding: 0px;"  data-toggle="tooltip" title="" data-original-title="Number of patients that have completed their treatment regimen (Pre-SVR)">
					
				</div>
				
				<div class="col-sm-12 col-md-11 text-center" style="min-height: 56px;">
				<h3 style="margin: 5px;color: #000000;font-size:36px;font-weight: 500;" class="count"> <?php //echo $cascade->treatment_completed; ?></h3>
				<h4 style="font-size: 14px;margin: 0px;color: #000000;letter-spacing: 1px;">Treatment Completed</h4>
				</div>
				</div>

  	</div>
</div> -->
  	<div class="row">
  		<?php
  $attributes = array(
              'id' => 'filter_form',
              'name' => 'filter_form',
               'autocomplete' => 'false',
            );
           echo form_open('', $attributes); ?>
<div class="panel panel-default">
<div class="panel-body" style="padding-top: 5px;padding-bottom: 5px;">
<div class="row">
	<div class="col-md-2">
		<label for="">State</label>
		<select type="text" name="search_state" id="search_state" class="form-control input_fields" <?php  ?>  <?php echo $select2.$select3.$select4; ?>>
			<option value="">All States</option>
			<?php 
			foreach ($states as $state) {
				?>
				<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo $state->StateName; ?></option>
				<?php 
			}
			?>
		</select>
	</div>
	<div class="col-md-2">
		<label for="">District</label>
		<select type="text" name="input_district" id="input_district" class="form-control input_fields"  <?php echo $select.$select2.$select4; ?>>
			<option value="">All Districts</option>
			<?php 
			/*foreach ($districts as $district) {
				?>
				<option value="<?php echo $district->id_mstdistrict; ?>" <?php echo (count($default_districts)==1 && $default_districts[0]->id_mstdistrict == $district->id_mstdistrict)?'selected':''; ?>><?php echo ucwords(strtolower($district->DistrictName)); ?></option>
				<?php 
			}*/
			?>
		</select>
	</div>
	
	<div class="col-md-2">
		<label for="">Facility</label>
		<select type="text" name="mstfacilitylogin" id="mstfacilitylogin" class="form-control input_fields"  <?php echo $select.$select2; ?>>
			<option value="">All Facility</option>
			<?php 
			/*foreach ($facilities as $facility) {
				?>
				<option value="<?php echo $facility->id_mstfacility; ?>" <?php echo (count($default_facilities)==1 && $default_facilities[0]->id_mstfacility == $facility->id_mstfacility)?'selected':''; ?>><?php echo ucwords(strtolower($facility->facility_short_name)); ?></option>
				<?php 
			}*/
			?>
		</select>
	</div>
	<div class="col-md-2">
		<label for="">From Date</label>
		<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')){ echo $this->input->post('startdate');} else{ echo timeStampShow(date('2019-07-01'));}  ?>" required>
	</div>
	<div class="col-md-2">
		<label for="">To Date</label>
		<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')){echo $this->input->post('enddate');}else{echo timeStampShow(date('Y-m-d'));} ?>" required>
	</div>
	<div class="col-md-2">
		<label for="">&nbsp;</label>
		<button class="btn btn-block" style="background-color: #f4860c;color: white; line-height: 1.2; font-weight: 600;">SEARCH</button>
	</div>
</div>
<br>
 <?php echo form_close(); ?>
  	</div>
</div>
</div>


<div class ="row">


<div class ="panel panel-default">
<div class ="panel-heading">
<h4 class  ="text-center panel-title" style="color: white;">HAV/HEV Cascade Of Care 
</h4>
</div>
</div>
</div>
<div class="row" style="margin-top: -22px;">
  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 left-div html2canvas_div">
  		<div class="panel panel-default">
			<!-- <div class="panel-heading">
				<h5 class="panel-title text-center" style="color: white;">Cascade of Care for HAV patients</h5>
			</div> -->
			<div class="panel-body">
				<div class="row">
					<div class="col-md-10 col-sm-8 col-xs-8" style="margin-top: -15px;">

									<h4 class="sub-heading">Cascade of Care for HAV patients</h4>
									<div style="margin-left: -40px;">
							<div id="agechart-legend" class="chart-legend"></div></div>
						</div>
					<div class="col-md-1 col-sm-4 col-xs-4 pull-right" data-html2canvas-ignore="true">
  				<!-- <div class="col-md-4 col-sm-3 form-group" style="padding: 0px;">
					<select name="havanygender" id="havanygender"  class="form-control">
						<?php foreach ($gender_details as  $value){ ?>

					<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
					
					<?php } ?>
				</select>
			</div>
 -->
			<!-- <div class="col-md-5 col-sm-4 form-group" style="padding: 0px;">
					<select name="havanyage" id="havanyage"  class="form-control">
						<?php //foreach ($gender_details as  $value){ ?>

					<option value="1"><?php echo '<10 Years'; ?></option>
					<option value="2"><?php echo '11-20 Years'; ?></option>
					<option value="3"><?php echo '21-30 Years'; ?></option>
					<option value="4"><?php echo '31-40 Years'; ?></option>
					<option value="5"><?php echo '41-50 Years'; ?></option>
					<option value="6"><?php echo '51-60 Years'; ?></option>
					<option value="7"><?php echo '>=60 Years'; ?></option>
					
					<?php //} ?>
				</select>
			</div> -->
			<div class="col-md-2 col-sm-6" style="padding: 0px;margin-top :5px;">
						
			<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 15px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="ageChartLink" download="Cascade_of_Care_for_HAV_patients.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="downloadCascade"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a  href='<?= base_url() ?>Dashboard/casecade_hav_export/excel' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>






<!-- 
						<div class="text-right">
						<a href="<?php echo site_url(); ?>chartreports_infinite_scroll/district_wise_age" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="ageChartLink" download="ageChart.jpeg" class="export_button"><i class="fa fa-download fa-lg" id="downloadCascade"></i></a>
						<a href='<?= base_url() ?>Dashboard/casecade_hav_export/excel' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a></div> -->
					</div>
				</div>
			</div>
				<div class="row">

					<div class="col-lg-12 col-md-12 cascade_scroll-x">
						<div class="ageChart_div">
						<canvas id="ageChart"></canvas>
					</div>
					</div>	

				</div>
			</div>
		</div>
  	</div>

  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 right-div html2canvas_div">
  		<div class="panel panel-default">
			<!-- <div class="panel-heading">
				<h5 class="panel-title text-center" style="color: white;">Cascade of Care for HEV patients</h5>
			</div> -->
			<div class="panel-body">
				<div class="row">
					<div class="col-md-10 col-sm-8 col-xs-8" style="margin-top: -15px;">

									<h4 class="sub-heading">Cascade of Care for HEV patients</h4>
									<div style="margin-left: -40px;">
							<div id="genderAreaChart-legend" class="chart-legend"></div></div>
						</div>
				<div class="col-md-2 col-sm-4 col-xs-4 pull-right mar-right" data-html2canvas-ignore="true">
  				<!-- <div class="col-md-4 col-sm-3 form-group" style="padding: 0px;">
					<select name="hevanygender" id="hevanygender"  class="form-control">
						<?php foreach ($gender_details as  $value){ ?>

					<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
					
					<?php } ?>
				</select>
			</div> -->
<!-- 
			<div class="col-md-5 col-sm-4 form-group" style="padding: 0px;">
					<select name="hev" id="hevanyage"  class="form-control">
						<?php //foreach ($gender_details as  $value){ ?>

					<option value="1"><?php echo '<10 Years'; ?></option>
					<option value="2"><?php echo '11-20 Years'; ?></option>
					<option value="3"><?php echo '21-30 Years'; ?></option>
					<option value="4"><?php echo '31-40 Years'; ?></option>
					<option value="5"><?php echo '41-50 Years'; ?></option>
					<option value="6"><?php echo '51-60 Years'; ?></option>
					<option value="7"><?php echo '>=60 Years'; ?></option>
					
					<?php //} ?>
				</select>
			</div> -->
			<div class="col-md-4 col-sm-6">
						<!-- <a href="<?php echo site_url(); ?>chartreports_infinite_scroll/district_wise_gender" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a> -->
						
	<div class="side_btn" style="margin-right: 10px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 15px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="genderAreaChartLink" download="Cascade_HEV_Patients.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="downloadCascade"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a  href='<?= base_url() ?>Dashboard/casecade_hev_export/excel' class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


<!-- 
						<div class="text-right">
						<a id="genderAreaChartLink" style="margin-right: 5px;" download="genderAreaChart.jpeg" class="export_button"><i class="fa fa-download fa-lg" id="downloadCascade"></i></a>
						<a href='<?= base_url() ?>Dashboard/casecade_hev_export/excel' class="fa fa-file-excel-o fa-lg text-dark bg-success" title="Excel Download" ></a></div> -->
					</div>
				</div>
			</div>
				<div class="row">

					<div class="col-lg-12 col-md-12 cascade_scroll-x">
						<div class="ageChart_div">
						<canvas id="genderAreaChart"></canvas>
					</div>
					</div>

				</div>
			</div>
		</div>
  	</div>
</div>
 <!--  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
  		graph10
  	</div>

  	<div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
  		graph11
  	</div> -->


  		
  <?php  if ($loginData->user_type==1 && $filters['id_input_district']==0) { ?>
<?php if($filters['id_mstfacility']=="" OR $filters['id_mstfacility']==0){ ?>
	<div class="row html2canvas_div">
<div class="panel panel-default">
	<!-- <div class="panel-heading">
	<h3 class="panel-title text-center" id="head_hav_national" style="color: white;">Screened for HAV across states</h3>
	</div> -->
	<div class="panel-body">
			<div class="row">
				<div class="col-md-9 col-sm-5 col-xs-5" style="margin-top: -15px;">

				<h4 class="sub-heading" id="head_hav_national">Screened for HAV across states</h4>
				<div style="margin-left: -40px;">
		<div id="Screened_HAV_State-legend" class="chart-legend"></div></div>
	</div>
				 <div class="col-md-3 col-sm-7 col-xs-7 pull-right" data-html2canvas-ignore="true">
  				<div class="col-md-8 col-xs-9 col-sm-9 form-group" style="padding: 0px;">
					<select name="getmonthanalys" id="getmonthanalys3" onchange="havscreened()" class="form-control">
					<option value="screened_for_hav" selected="">Screened for HAV</option>
					<option value="hav_positive_patients">HAV Positive Patients</option>
					<option value="patients_managed_at_facility">Patients managed at the facility</option>
					<option value="patients_referred_for_management">Patients referred for management</option>
					
				</select>
				</div>
				<div class="col-md-4 col-xs-3 col-sm-3">
					
					<div class="side_btn">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="screenedacrossdistricts_hav_nationalLink" download="Cascade_HAV_Patients_Across_State.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="downloadscreenedacross_hav_national"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/treatment_Initiations_by_state_HAV_export/excel' class="btn1 btn-info btn-block" id="exl_hav_screened" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


					<!-- <div class="text-right">
									<a id="screenedacrossdistricts_hav_nationalLink" download="cascadeChart.jpeg" class=""><i class="fa fa-lg fa-download" id="downloadscreenedacross_hav_national"></i></a> &nbsp;
									<a href='<?= base_url() ?>Dashboard/treatment_Initiations_by_state_HAV_export/excel' class="fa fa-file-excel-o fa-lg text-dark bg-success" id="exl_hav_screened"  title="Excel Download" ></a>
								</div> -->
				</div>
				
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12 col-md-12 state_scroll-x" style="padding-right: 0px;">
				<div class="state_div">
				<canvas id="screenedacrossdistricts_hav_national"></canvas>
			</div>
			</div>
		</div>
	</div>
</div>
	</div>
<?php } }?>
  

  
  		
  		<?php  if ($loginData->user_type==1 && $filters['id_input_district']==0) { ?>
<?php if($filters['id_mstfacility']=="" OR $filters['id_mstfacility']==0){ ?>
		<div class="row html2canvas_div">
<div class="panel panel-default">
	<!-- <div class="panel-heading">
	<h5 class="panel-title text-center" id="head_hev_national" style="color: white;">Screened for HEV across states</h5>
	</div> -->

	<div class="panel-body">

		<div class ="row">




		<div class="row">
			<div class="col-md-9 col-sm-5 col-xs-5" style="margin-top: -15px;">

				<h4 class="sub-heading" id="head_hev_national">Screened for HEV across states</h4>
				<div style="margin-left: -40px;">
		<div id="Screened_HEV_State-legend" class="chart-legend"></div></div>
	</div>
				<div class="col-md-3 col-sm-7 col-xs-7 pull-right" data-html2canvas-ignore="true">
  				<div class="col-md-8 col-xs-9 col-sm-9 form-group" style="padding: 0px;">
					<select name="getmonthanalys" id="getmonthanalys4state" onchange="hevscreened();" class="form-control">
					<option value="screened_for_hev" selected="">Screened for HEV</option>
					<option value="hev_positive_patients">HEV Positive Patients</option>
					<option value="patients_managed_at_facility_hev">Patients managed at the facility</option>
					<option value="patients_referred_for_management_hev">Patients referred for management</option>
					
				</select>
				</div>
				<div class="col-md-4 col-xs-3 col-sm-3">
				
				<div class="side_btn">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="screenedacrossstateshevLink" download="Cascade_HEV_Patients_Across_State.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="screenedacrossstateshevdown"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/treatment_Initiations_by_statehev_export/excel' class="btn1 btn-info btn-block" id="exl_hev_screened" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>


				<!-- <div class="text-right" style="margin-top: 5px;">
				<a id="screenedacrossstateshevLink" download="cascadeChart.jpeg" class="export_button"><i class="fa fa-lg fa-download" id="screenedacrossstateshevdown" style="margin-right: 20px; margin-top: 5px" ></i></a>
				<a href='<?= base_url() ?>Dashboard/treatment_Initiations_by_statehev_export/excel' class="fa fa-file-excel-o fa-lg text-dark bg-success" id="exl_hev_screened"  title="Excel Download" ></a>
							</div> -->
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 state_scroll-x" style="padding-right: 0px;">
				<div class="state_div">
				<canvas id="screenedacrossstateshev"></canvas>
			</div>
			</div>
		</div>
				</div>

</div>
 	</div>
<?php } }?>
 


  
  		
  		<!-- view only state login start-->
<?php  if ($loginData->user_type==3 || $filters['id_input_district']!=0) { ?>
<?php if($filters['id_mstfacility']=="" OR $filters['id_mstfacility']==0){ ?>
		<div class="row html2canvas_div">
<div class="panel panel-default">
<!-- 	<div class="panel-heading">
<h5 class="panel-title text-center" id="head_hav_district" style="color: white;">Screened for HAV across all facilities</h5>
</div> -->

	<div class="panel-body">
	<div class="row">

					<div class="col-md-9 col-sm-5 col-xs-5" style="margin-top: -15px;">

				<h4 class="sub-heading" id="head_hav_district">Screened for HAV across all facilities</h4>
				<div style="margin-left: -40px;">
		<div id="Screened_HAV_dist-legend" class="chart-legend"></div></div>
	</div>
				 <div class="col-md-3 col-sm-7 col-xs-7 pull-right" data-html2canvas-ignore="true">
  				<div class="col-md-8 col-xs-9 col-sm-9 form-group" style="padding: 0px;">
					<select name="getmonthanalys" id="getmonthanalys3" onchange="havscreened_district();" class="form-control">
					<option value="screened_for_hav" selected="">Screened for HAV</option>
					<option value="hav_positive_patients">HAV Positive Patients</option>
					<option value="patients_managed_at_facility">Patients managed at the facility</option>
					<option value="patients_referred_for_management">Patients referred for management</option>
					
				</select>
			</div>
				<div class="col-md-4 col-sm-6" style="padding: 0px;">
				
						<div class="side_btn" style="margin-right: 30px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="screenedacrossdistrictshavLink" download="Cascade_HAV_Patients_Across_Districts.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="screenedacrossdistrictshavdd"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a href='<?= base_url() ?>Dashboard/treatment_Initiations_by_District_HAV_export/excel' id="exl_hav_screened_dis" class="btn1 btn-info btn-block" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>

				<!-- <div class="text-right">
				<a id="screenedacrossdistrictshavLink" download="cascadeChart.jpeg" class="export_button"><i class="fa fa-lg fa-download" id="screenedacrossdistrictshavdd"></i></a>
				<a href='<?= base_url() ?>Dashboard/treatment_Initiations_by_District_HAV_export/excel' class="fa fa-file-excel-o fa-lg text-dark bg-success" style="margin-right:0px;" id="exl_hav_screened_dis"  title="Excel Download" ></a>
							</div> -->
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12 col-md-12 district_scroll_x">
				<div class="district_div">
				<canvas id="screenedacrossdistrictshav"></canvas>
			</div>
			</div>
		</div>
	</div>
</div>
	</div>
<?php } }?>
<!-- view only state login End-->
  


  	
  		
  		<!-- view only state login start-->
<?php  if ($loginData->user_type==3 || $filters['id_input_district']!=0) { ?>
<?php if($filters['id_mstfacility']=="" OR $filters['id_mstfacility']==0){ 
	?>
	<div class="row html2canvas_div">
<div class="panel panel-default">
	<!-- <div class="panel-heading">
	<h5 class="panel-title text-center" id="head_hev_district" style="color: white;">Screened for HEV across all facilities</h5>
	</div> -->

	<div class="panel-body">
		<div class="row">

				<div class="col-md-9 col-sm-5 col-xs-5" style="margin-top: -15px;">

				<h4 class="sub-heading" id="head_hev_district">Screened for HEV across all facilities</h4>
				<div style="margin-left: -40px;">
		<div id="Screened_HEV_dist-legend" class="chart-legend"></div></div>
	</div>
				 <div class="col-md-3 col-sm-7 col-xs-7 pull-right" data-html2canvas-ignore="true">
  				<div class="col-md-8 col-xs-9 col-sm-9 form-group" style="padding: 0px;">
					<select name="getmonthanalys" id="getmonthanalys4" onchange="hevscreened_district();" class="form-control">
					<option value="screened_for_hev" selected="">Screened for HEV</option>
					<option value="hev_positive_patients">HEV Positive Patients</option>
					<option value="patients_managed_at_facility_hev">Patients managed at the facility</option>
					<option value="patients_referred_for_management_hev">Patients referred for management</option>
					
				</select>
					</div>
				<div class="col-md-4 col-sm-3 col-xs-3" style="padding: 0px;">
				
						<div class="side_btn" style="margin-right: 30px;">
						<div class="side_ul">
							<ul class="list-unstyled">
								<li>
									<a href="javascript:void(0)" class="cpanel"><i class="fa fa-2x fa-download"></i></a>
								</li>
							</ul>
						</div>
					</div>
					<div id="divFilter" class="chk" style="width: 200px; margin-right: 25px;">
						<div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
							<div class="col-sm-12">
								<div class="form-group">
									<a id="screenedacrossdistrictshevLink" download="Cascade_HEV_Patients_Across_Districts.jpeg" class="btn1 btn-info btn-block imagelink" style="margin-right: -15px;" role="button">Export As Image &nbsp;<i class="fa fa-lg fa-image" id="screenedacrossdistrictshevddev"></i></a>

								</div>
							</div>

							<div class="col-sm-12">
								<div class="form-group">
									<a  href='<?= base_url() ?>Dashboard/treatment_Initiations_by_District_HEV_export/excel' class="btn1 btn-info btn-block" id="exl_hev_screened_dis" style="margin-right: -15px;" role="button">Export As Excel &nbsp;<i class="fa fa-lg fa-file-excel-o"></i></a>
								</div>
							</div>
							<br />
						</div>
					</div>
	





				<!-- <div class="text-right">
				<a id="screenedacrossdistrictshevLink" download="cascadeChart.jpeg" class="export_button"><i class="fa fa-lg fa-download" id="screenedacrossdistrictshevddev"></i></a>
				<a href='<?= base_url() ?>Dashboard/treatment_Initiations_by_District_HEV_export/excel' class="fa fa-file-excel-o fa-lg text-dark bg-success" id="exl_hev_screened_dis" style="margin-right: -20px;" title="Excel Download" ></a>
							</div> -->

				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12 col-md-12 district_scroll_x">
				<div class="district_div">
				<canvas id="screenedacrossdistrictshev"></canvas>
			</div>
			</div>
		</div>
</div>
</div>
	</div>
<?php } }?>
  
</div>

</div>








<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAckMDhjh3pVCmbmf88f8qynPI4jKPNvyQ&callback=initMap">
</script>

<script>

	$('[data-toggle="tooltip"]').tooltip();
	$(document).ready(function(){
		
	drawAgeWiseChart();
	genderAreaChart();

		<?php if($filters['id_mstfacility']==""){ ?>
		drawscreenedacrossdistrictshav();
		drawscreenedacrossdistrictshev();
		
		//drawMap();
		
/*National*/
		drawscreenedacrossdistricts_hav_national();
		drawscreenedacrossstateshev();
		
	<?php }?>


		   $('.chk').hide();
                    $(".side_ul").mouseover(function () {
                      
                           //$(this).('.chk').show(1000);
                        //$(this).('.chk').slideDown(1000);
                         var index = $(".side_ul").index(this);
                        $(".chk").eq(index).slideDown(1000);                			
                    });
                    $(".chk").mouseleave(function () {
                       
                            //
                             var index = $(".chk").index(this);
                         $(".chk").eq(index).slideUp(1000);
                         //$(".chk").eq(index).hide(1000);
                    });
                   $(document).scroll(function() {
 					 $(".chk").slideUp(1000);
});
                    $(".imagelink").mouseenter(function () {
                    	 var index = $(".imagelink").index(this);
                     var element=document.getElementsByClassName("html2canvas_div")[index];
        				// Global variable 
           				 var getCanvas;  
                			html2canvas(element, {x: window.scrollX,
							y: window.scrollY,
							width: element.innerWidth,
							height: element.innerHeight,
							allowTaint: true,
							scale:2,
							backgroundColor: "rgba(0,0,0,0)", removeContainer: true,
							logging: true,
                    		onrendered: function(canvas) {  
                        getCanvas = canvas; 
                        var url_base64jp = getCanvas.toDataURL('image/jpeg',1);
                         $(".imagelink").eq(index).attr("href", url_base64jp);
                        $("#loading_gif").show();
						    setTimeout(function() {
						       $("#loading_gif").hide();
						    },3000);
						
						 
                    } 
                }); 
                			}); 
$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 3000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});
$(".div_collapse").hide();
$(".div_collapse").eq(0).show();

 $(".panel-heading").click(function () {
 	$(".div_collapse").toggle();
var index = $(".panel-heading").index(this);

if(index==0){
	$(".div_collapse").eq(0).toggle();
}
else{
	$(".div_collapse").eq(index).toggle();
	
}
$(".div_collapse").toggle();

//alert(index);
//$(".panel-body").toggle();
});
	});



function drawAgeWiseChart()
{
	var ctx = $("#ageChart");

	
		//var total = <?php //echo $regimen[0]->COUNT +  $regimen[1]->COUNT +  $regimen[2]->COUNT +  $regimen[3]->COUNT; ?>;
		var total = <?php echo $cascade_hav_patients->screened_for_hav +  $cascade_hav_patients->hav_positive_patients + $cascade_hav_patients->patients_managed_at_facility +  $cascade_hav_patients->patients_referred_for_management; ?>;
		var data = {
			labels: [["Screened ","for HAV"], ["HAV Positive"," Patients"], ["Patients ","Managed at the"," facility "], ["Patients ","referred for"," Management"]],
			datasets: [
			{	
				label : ['No. of Patients'],
				data: [
				<?php echo $cascade_hav_patients->screened_for_hav; ?>,
				<?php echo $cascade_hav_patients->hav_positive_patients;  ?>,
				<?php echo $cascade_hav_patients->patients_managed_at_facility;  ?>,
				<?php echo $cascade_hav_patients->patients_referred_for_management;  ?>,
				],
				backgroundColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				],
				hoverBackgroundColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				],
				/*borderColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				],
				borderWidth: 1,*/
			}]
		};
		var options = {
			responsive:true,
			maintainAspectRatio:false,
			animation: {
			duration: 0,
			onComplete: function () {

				/*var url_base64jp = document.getElementById("ageChart").toDataURL("image/png");
				document.getElementById("ageChartLink").href=url_base64jp;*/
			
			// render the value of the chart above the bar
			var ctx = this.chart.ctx;
			ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
			ctx.fillStyle = this.chart.config.options.defaultFontColor;
			ctx.textAlign = 'center';
			ctx.textBaseline = 'bottom';
			this.data.datasets.forEach(function (dataset) {
			for (var i = 0; i < dataset.data.length; i++) {
			var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
			ctx.fillText(dataset.data[i], model.x, model.y - 5);
			}
			});
			}},

			legend: false,
			legendCallback:legendcallback,
			cornerRadius:5,
				layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 0
				}},
			
			tooltips: {
				enabled: false,
				callbacks: {
					label: function(tooltipItem) {

						var percentage = Math.round((tooltipItem.yLabel/total)*100);
						return tooltipItem.yLabel+" , "+percentage+"%"; 
					},
				},
			},events:[],
				scales: {
				yAxes: [{
					stacked: true,
					ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},
					gridLines: {
						display:false,
						color: "rgba(0, 0, 0, 0)",
					}
				}],
				xAxes: [{
					barThickness : 30,
					stacked: true,
					ticks: {
						beginAtZero:true
					},
					gridLines: {
						display:false,
						color: "rgba(0, 0, 0, 0)",
					}
				}]

			},
			tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= value + ' %' %>",
		};
		var myBarChart = new Chart(ctx,{plugins: [plugin],
			type: 'bar',
			data: data,
			options: options
		});
		$('#agechart-legend').html(myBarChart.generateLegend());
}


function genderAreaChart()
{

	var ctx = $("#genderAreaChart");

	//var total = <?php //echo $regimen[0]->COUNT +  $regimen[1]->COUNT +  $regimen[2]->COUNT +  $regimen[3]->COUNT; ?>;
	var total = <?php echo $cascade_hev_patients->screened_for_hev +  $cascade_hev_patients->hev_positive_patients +  $cascade_hev_patients->patients_managed_at_facility_hev +  $cascade_hev_patients->patients_referred_for_management_hev; ?>;
		var data = {
			labels: [["Screened ","for HEV"], ["HEV Positive"," Patients"], ["Patients ","Managed at"," the facility "], ["Patients referred "," for ","Management"]],
			datasets: [
			{	
				label :['No. of Patients'],
				data: [
				<?php echo $cascade_hev_patients->screened_for_hev; ?>,
				<?php echo $cascade_hev_patients->hev_positive_patients;  ?>,
				<?php echo $cascade_hev_patients->patients_managed_at_facility_hev;  ?>,
				<?php echo $cascade_hev_patients->patients_referred_for_management_hev;  ?>,		
				],
				backgroundColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				],
				hoverBackgroundColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				],
				/*borderColor: [
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				'rgba(53, 196, 249)',
				],
				borderWidth: 1,*/
			}]
		};
		var options = {
			responsive:true,
			maintainAspectRatio:false,
			animation: {
			duration: 0,
			onComplete: function () {
				/*var url_base64jp = document.getElementById("genderAreaChart").toDataURL("image/png");
				document.getElementById("genderAreaChartLink").href=url_base64jp;*/
			// render the value of the chart above the bar
			var ctx = this.chart.ctx;
			ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
			ctx.fillStyle = this.chart.config.options.defaultFontColor;
			ctx.textAlign = 'center';
			ctx.textBaseline = 'bottom';
			this.data.datasets.forEach(function (dataset) {
			for (var i = 0; i < dataset.data.length; i++) {
			var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
			ctx.fillText(dataset.data[i], model.x, model.y - 5);
			}
			});


			}},
			layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 0
				}},
			legend: false,
			legendCallback:legendcallback,
			cornerRadius:5,
			tooltips: {
				enabled: false,
				callbacks: {
					label: function(tooltipItem) {

						var percentage = Math.round((tooltipItem.yLabel/total)*100);
						return tooltipItem.yLabel+" , "+percentage+"%"; 
					},
				}
			},events:[],
			scales: {
				yAxes: [{
					stacked: true,
					ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						 callback: function(value, index, values) {
                    if (Math.floor(value) === value) {
                        return value;
                    }
                },
						suggestedMax:100
					},
					gridLines: {
						display:false,
						color: "rgba(0, 0, 0, 0)",
					}
				}],
				xAxes: [{
					barThickness : 30,
					stacked: true,
					ticks: {
                        autoSkip: false,
                        },
					gridLines: {
						display:false,
						color: "rgba(0, 0, 0, 0)",
					}
				}]

			},
			tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= value + ' %' %>",
		};
		var myBarChart = new Chart(ctx,{plugins: [plugin],
			type: 'bar',
			data: data,
			options: options
		});
		$('#genderAreaChart-legend').html(myBarChart.generateLegend());
}


function drawscreenedacrossdistricts_hav_national()
{

	<?php  if ($loginData->user_type==1) { ?>
	var ctx  = $("#screenedacrossdistricts_hav_national");
	
	var data = {
		labels: [
		<?php 
		foreach ($treatment_Initiations_by_states as $row) {
			echo "\"".$row->hospital."\",";
		}
		?>
		],
		datasets: [
		{	
			label : ["No. of Patients"],
			backgroundColor: [
			<?php 
			foreach ($treatment_Initiations_by_states as $row) {
				echo "'".$row->color."',";
			}
			?>
			],
			
			data: [
			<?php 
			$max=0;
			foreach ($treatment_Initiations_by_states as $row) {
				echo floor($row->countIni).",";
				 if ($max == null || $row->countIni > $max) {
            		$max= $row->countIni;
       				 }
			}
			?>
			]
		}
		]
	};
var maxval=<?php echo $max ? $max : 0; ?>;
	var options = {
	responsive:true,
	maintainAspectRatio:false,
	  plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'500',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0";
							}
							else{
							return context.dataset.data[context.dataIndex];
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},
		animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				/*ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});*/
				}},
	
		legend:false,
		legendCallback:legendcallback,
		cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 10
				}},
            tooltips: {
                        enabled: false
                        },events:[],
		scales : {
			xAxes : [{
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				},
				ticks: {
          autoSkip: false,
          maxRotation: 90,
          minRotation: 90
        },
			}],
			yAxes : [{
				ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						maxTicksLimit:8,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				}
			}]
		}
	};

	var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
		type: 'bar',
		data: data,
		options: options
	});
$('#Screened_HAV_State-legend').html(myBarChart.generateLegend());
<?php } ?>
}




<?php if($filters['id_mstfacility']==""){ ?>
function drawscreenedacrossdistrictshav()
{
	var ctx  = $("#screenedacrossdistrictshav");
	
	var data = {
		labels: [
		<?php 
		foreach ($treatment_Initiations_by_Districthav as $row) {
			echo "\"".$row->hospital."\",";
		}
		?>
		],
		datasets: [
		{	
			label : ["No. of Patients"],
			backgroundColor: [
			<?php 
			foreach ($treatment_Initiations_by_Districthav as $row) {
				echo "'".$row->color."',";
			}
			?>
			],
			
			data: [
			<?php 
			$max=0;
			foreach ($treatment_Initiations_by_Districthav as $row) {
				echo "\"".floor($row->countIni)."\",";
				 if ($max == null || $row->countIni > $max) {
            		$max= $row->countIni ? $row->countIni : 0;
       				 }

			}
			?>
			]
		}
		]
	};
var maxval=<?php echo $max ? $max : 0; ?>;

						
	var options = {
	responsive:true,
	maintainAspectRatio:false,
		animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});

				}},
	
		/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 35
            }
        },*/
        legend:false,
        legendCallback:legendcallback,
        cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 0
				}},
            tooltips: {
                        enabled: false
                        },events:[],
		scales : {
			xAxes : [{
				<?php if(count($treatment_Initiations_by_Districthav)<=10){
					echo 'barThickness:60,';
				};?>
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				},
				ticks :{
					autoSkip :false,
					maxRotation:90,
					minRotation:90
				}
			}],
			yAxes : [{
				ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},

				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				}
			}]
		}
	};

	var myBarChart = new Chart(ctx, { plugins: [plugin],
		type: 'bar',
		data: data,
		options: options
	});
	$('#Screened_HAV_dist-legend').html(myBarChart.generateLegend());
}
<?php } ?>

<?php if($filters['id_mstfacility']==""){ ?>

function drawscreenedacrossdistrictshev()
{
	var ctx  = $("#screenedacrossdistrictshev");
	
	var data = {
		labels: [
		<?php 
		foreach ($treatment_Initiations_by_Districthev as $row) {
			echo "['".$row->hospital."'],";
		}
		?>
		],
		datasets: [
		{	
			label : ["No. of Patients"],
			backgroundColor: [
			<?php 
			foreach ($treatment_Initiations_by_Districthev as $row) {
				echo "'".$row->color."',";
			}
			?>
			],
			
			data: [
			<?php 
			$max=0;
			foreach ($treatment_Initiations_by_Districthev as $row) {
				echo floor($row->countIni).",";
				if ($max == null || $row->countIni > $max) {
            		$max= $row->countIni;
       				 }
			}
			?>
			]
		}
		]
	};
var maxval=<?php echo $max ? $max : 0; ?>;
	var options = {
	responsive:true,
	maintainAspectRatio:false,
		animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});

				}},
		barValueSpacing: 20,
		/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 15
            }
        },*/
        legend: false,
		legendCallback:legendcallback,
		cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 45,
				bottom: 0
				}},
            tooltips: {
                        enabled: false
                        },events:[],
		scales : {
			xAxes : [{
				<?php if(count($treatment_Initiations_by_Districthev)<=10){
					echo 'barThickness:60,';
				}; ?>
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				},
				stacked: true,
ticks: {
beginAtZero:true,
autoSkip : false,
maxRotation:90,
minRotation:90
},
			}],
			yAxes : [{
				ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						 max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},

				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				}
			}]
		}
	};

	var myBarChart = new Chart(ctx, { plugins: [plugin],
		type: 'bar',
		data: data,
		options: options
	});
	$('#Screened_HEV_dist-legend').html(myBarChart.generateLegend());
}

<?php } ?>

function drawscreenedacrossstateshev()
{

	<?php  if ($loginData->user_type==1) { ?>
	var ctx  = $("#screenedacrossstateshev");
	
	var data = {
		labels: [
		<?php 
		foreach ($treatment_Initiations_by_statehev as $row) {
			echo "\"".$row->hospital."\",";
		}
		?>
		],
		datasets: [
		{	
			label : ["No. of Patients"],
			backgroundColor: [
			<?php 
			foreach ($treatment_Initiations_by_statehev as $row) {
				echo "'".$row->color."',";
			}
			?>
			],
			
			data: [
			<?php 
			$max=0;
			foreach ($treatment_Initiations_by_statehev as $row) {
				echo floor($row->countIni).",";
				if ($max == null || $row->countIni > $max) {
            		$max= $row->countIni;
       				 }
			}
			?>
			]
		}
		]
	};
	var maxval=<?php echo $max ? $max : 0; ?>;
	var options = {
	responsive:true,
	maintainAspectRatio:false,
	  plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'500',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0";
							}
							else{
							return context.dataset.data[context.dataIndex];
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},
		animation: {
				duration: 0,
				onComplete: function () {
				
				// render the value of the chart above the bar
				var ctx = this.chart.ctx;
				/*ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
				ctx.fillStyle = this.chart.config.options.defaultFontColor;
				ctx.textAlign = 'center';
				ctx.textBaseline = 'bottom';
				this.data.datasets.forEach(function (dataset) {
				for (var i = 0; i < dataset.data.length; i++) {
				var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
				ctx.fillText(dataset.data[i], model.x, model.y - 5);
				}
				});*/
				
				}},
	
		/*legend: {
            display: true,
            position: 'bottom',
            labels: {
                boxWidth: 20,
                fontColor: '#111',
                padding: 35
            }
        },*/
        legend:false,
        legendCallback:legendcallback,
        cornerRadius:5,
        layout: {
				padding: {
				left: 0,
				right: 0,
				top: 25,
				bottom: 20
				}},
            tooltips: {
                        enabled: false
                        },events:[],
		scales : {
			xAxes : [{
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				},
				ticks: {
          autoSkip: false,
          maxRotation: 90,
          minRotation: 90
        },
			}],
			yAxes : [{
				stacked:true,
				ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},
				gridLines: {
					display:false,
					color: "rgba(0, 0, 0, 0)",
				}
			}]
		}
	};

	var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
		type: 'bar',
		data: data,
		options: options
	});
	$('#Screened_HEV_State-legend').html(myBarChart.generateLegend());
<?php } ?>
}



function changehead_screened(type)
{
	if(type=='HAV_national')
	{
	var selct_hdng = document.getElementById("getmonthanalys3");
	var hdng_text = document.getElementById("head_hav_national");
	var text=selct_hdng.options[selct_hdng.selectedIndex].value;
	var typetext='HAV';
	var across='states';
	var typesm='hav';
}
else if(type=='HEV_national')
{
	var selct_hdng = document.getElementById("getmonthanalys4state");
	var hdng_text = document.getElementById("head_hev_national");
	var text=selct_hdng.options[selct_hdng.selectedIndex].value;
	var typetext='HEV';
	var typesm='hev';
	var across='states';
}
else if(type=='HAV_district')
	{
	var selct_hdng = document.getElementById("getmonthanalys3");
	var hdng_text = document.getElementById("head_hav_district");
	var text=selct_hdng.options[selct_hdng.selectedIndex].value;
	var typetext='HAV';
	var typesm='hav';
	var across='All Facilities';
}
else if(type=='HEV_district')
{
	var selct_hdng = document.getElementById("getmonthanalys4");
	var hdng_text = document.getElementById("head_hev_district");
	var text=selct_hdng.options[selct_hdng.selectedIndex].value;
	var typetext='HEV';
	var typesm='hev';
	var across='All Facilities';
}
	if(text=="screened_for_"+typesm)
	{
	hdng_text.innerHTML="Screened for "+ typetext +" across "+across;
	}
	 else if(text==typesm+"_positive_patients")
	{
hdng_text.innerHTML=typetext +" positive patients across "+across;;
	}
	else if(text=="patients_managed_at_facility"||text=="patients_managed_at_facility_hev")
	{
	hdng_text.innerHTML	="Patients managed at the facility across "+across;
	}
	else if(text=="patients_referred_for_management"||text=="patients_referred_for_management_hev")
	{
	hdng_text.innerHTML	="Patients referred for management across "+across;
	}
}




function havscreened()
{

var selct_hdng = document.getElementById("getmonthanalys3");
var name=selct_hdng.options[selct_hdng.selectedIndex].value;
document.getElementById("exl_hav_screened").href='<?= base_url() ?>Dashboard/treatment_Initiations_by_state_HAV_export/excel/'+name;
       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/havscreened_export/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',flg:'true'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
    //alert(data);
       var ctx  = $("#screenedacrossdistricts_hav_national");
        var returned = JSON.parse(data);
        var maxval=0;
        var labels = returned.treatment_Initiations_by_Districthav_state.map(function(e) {
  return e.hospital;
});
         var countIni = returned.treatment_Initiations_by_Districthav_state.map(function(e) {
   return e.countIni;
});
          for (i=0; i<=countIni.length;i++){
    if (parseInt(countIni[i])>maxval) {
        maxval=parseInt(countIni[i]);
    }
}
         var color = returned.treatment_Initiations_by_Districthav_state.map(function(e) {
   return e.color;
});
//console.log(data);
var data = {
labels: labels,
datasets: [
{
label : ["No. of Patients"],
backgroundColor:color,
data:countIni,
}
]
};

var options = {
	  plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'500',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0";
							}
							else{
							return context.dataset.data[context.dataIndex];
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},
responsive:true,
maintainAspectRatio:false,


legend:false,
		legendCallback:legendcallback,
		cornerRadius:5,
        layout: {
padding: {
left: 0,
right: 0,
top: 25,
bottom: 20
}}, tooltips: {
                        enabled: false
                        },events:[],
scales : {
xAxes : [{
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
},
ticks: {
          autoSkip: false,
          maxRotation: 90,
          minRotation: 90,
        }
}],
yAxes : [{
	ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						suggestedMax:100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
}
}]
}
};

var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
type: 'bar',
data: data,
options: options
});
$('#Screened_HAV_State-legend').html(myBarChart.generateLegend());
    }
  });
       changehead_screened('HAV_national')
       
}


function hevscreened()
{
var selct_hdng = document.getElementById("getmonthanalys4state");
var name=selct_hdng.options[selct_hdng.selectedIndex].value;
document.getElementById("exl_hev_screened").href='<?= base_url() ?>Dashboard/treatment_Initiations_by_statehev_export/excel/'+name;
       $.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Dashboard/hevscreened_export/", // <-- properly quote this line
    cache: false,
    data: {sel_id: name,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',flg:'true'}, // <-- provide the branch_id so it will be used as $_POST['branch_id']
    //dataType: 'JSON', // <-- add json datatype
    success: function(data) {
    //alert(data);
       var ctx  = $("#screenedacrossstateshev");
       var maxval=0;
        var returned = JSON.parse(data);
        var labels = returned.treatment_Initiations_by_statehev.map(function(e) {
   return e.hospital;
});
         var countIni = returned.treatment_Initiations_by_statehev.map(function(e) {
   return e.countIni;
});
         for (i=0; i<=countIni.length;i++){
    if (parseInt(countIni[i])>maxval) {
        maxval=parseInt(countIni[i]);
    }
}
         var color = returned.treatment_Initiations_by_statehev.map(function(e) {
   return e.color;
});
//console.log(data);
var data = {
labels: labels,
datasets: [
{
label : ["No. of Patients"],
backgroundColor:color,
borderColor: color,
borderWidth: 1,
data:countIni,
}
]
};

var options = {
responsive:true,
maintainAspectRatio:false,
  plugins: {
datalabels: {
						color: '#505050',
						rotation:270,
						align:'end',
						anchor:'end',
						offset:5,
						 font: {
         weight:'500',
         family:'Helvetica',
         size:'12',
        },
						formatter: function(value, context) {
							if(isNaN(context.dataset.data[context.dataIndex]))
							{
							return "0";
							}
							else{
							return context.dataset.data[context.dataIndex];
							}
							
							}, padding: {
							bottom: 32}
							//formatter: Math.round
							}},
 legend:false,
        legendCallback:legendcallback,
        cornerRadius:5,
           
        layout: {
padding: {
left: 0,
right: 0,
top: 5,
bottom: 20
}}, tooltips: {
                        enabled: false
                        },events:[],
scales : {
xAxes : [{
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
},
ticks: {
          autoSkip: false,
          maxRotation: 90,
          minRotation: 90
        }
}],
yAxes : [{
	stacked:true,
	ticks: {
						beginAtZero:true,
						min:0,
						suggestedMin:0,
						maxTicksLimit:8,
						max:(maxval+maxval/2)>100 ? maxval+maxval/2 : 100,
						callback: function(value, index, values) {
						if (Math.floor(value) === value) {
						if (value==maxval+maxval/2) {
						if(value>500){
						return "";
						}
						else{
						return "";
						}
						
						}
						else{
						return value;
						}
						
						}
						}
						},
gridLines: {
	display:false,
color: "rgba(0, 0, 0, 0)",
}
}]
}
};

var myBarChart = new Chart(ctx, {plugins: [ChartDataLabels,plugin],
type: 'bar',
data: data,
options: options
});
$('#Screened_HEV_State-legend').html(myBarChart.generateLegend());
    }
  });
       changehead_screened('HEV_national')
       
}



$(document).ready(function(){

$("#search_state").trigger('change');
//alert('rff');

});
$('#search_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');
					//$("#mstfacilitylogin").trigger('change');

				},
				error : function(error)
				{
					//alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
					$('#mstfacilitylogin').html(data);
					
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});



/*plugin code end*/
Chart.pluginService.register({
  beforeDraw: function (chart) {
    if (chart.config.options.elements.center) {
      //Get ctx from string
      var ctx = chart.chart.ctx;

      //Get options from the center object in options
      var centerConfig = chart.config.options.elements.center;
      var fontStyle = centerConfig.fontStyle || 'Arial';
      var txt = centerConfig.text;
      var color = centerConfig.color || '#000';
      var sidePadding = centerConfig.sidePadding || 20;
      var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
      //Start with a base font of 30px
      ctx.font = "55px " + fontStyle;

      //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
      var stringWidth = ctx.measureText(txt).width;
      var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

      // Find out how much the font can grow in width.
      var widthRatio = elementWidth / stringWidth;
      var newFontSize = Math.floor(30 * widthRatio);
      var elementHeight = (chart.innerRadius * 2);

      // Pick a new font size so it will not be larger than the height of label.
      var fontSizeToUse = Math.min(newFontSize, elementHeight);

      //Set font settings to draw it correctly.
      ctx.textAlign = 'center';
      ctx.textBaseline = 'middle';
      var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
      var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
      ctx.font = fontSizeToUse+"px " + fontStyle;
      ctx.fillStyle = color;

      //Draw text in center
      ctx.fillText(txt, centerX, centerY);
    }
  }
});

var roundedges={
            afterUpdate: function (chart) {
                    var a=chart.config.data.datasets.length -1;
                    for (let i in chart.config.data.datasets) {
                        for(var j = chart.config.data.datasets[i].data.length - 1; j>= 0;--j) { 
                            if (Number(j) == (chart.config.data.datasets[i].data.length - 1))
                                continue;
                            var arc = chart.getDatasetMeta(i).data[j];
                            arc.round = {
                                x: (chart.chartArea.left + chart.chartArea.right) / 2,
                                y: (chart.chartArea.top + chart.chartArea.bottom) / 2,
                                radius: chart.innerRadius + chart.radiusLength / 2 + (a * chart.radiusLength),
                                thickness: chart.radiusLength / 2 - 1,
                                backgroundColor: arc._model.backgroundColor
                            }
                        }
                        a--;
                    }
            },

            afterDraw: function (chart) {
                    var ctx = chart.chart.ctx;
                    for (let i in chart.config.data.datasets) {
                        for(var j = chart.config.data.datasets[i].data.length - 1; j>= 0;--j) { 
                            if (Number(j) == (chart.config.data.datasets[i].data.length - 1))
                                continue;
                            var arc = chart.getDatasetMeta(i).data[j];
                            var startAngle = (Math.PI / 2 - arc._view.startAngle);
                            var endAngle = Math.PI / 2 - arc._view.endAngle;

                            ctx.save();
                            ctx.translate(arc.round.x, arc.round.y);
                            console.log(arc.round.startAngle)
                            ctx.fillStyle = arc.round.backgroundColor;
                            ctx.beginPath();
                            ctx.arc(arc.round.radius * Math.sin(startAngle), arc.round.radius * Math.cos(startAngle), arc.round.thickness,0,2 * Math.PI);
                            ctx.arc(arc.round.radius * Math.sin(endAngle), arc.round.radius * Math.cos(endAngle), arc.round.thickness, 0, 2 * Math.PI);
                            ctx.closePath();
                            ctx.fill();
                            ctx.restore();
                        }
                    }
            },
        };

    /*Chart.plugins.register({
        afterDatasetsDraw: function(chartInstance, easing) {
            // To only draw at the end of animation, check for easing === 1
            var ctx = chartInstance.chart.ctx;

            chartInstance.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.getDatasetMeta(i);
                if (!meta.hidden) {
                    meta.data.forEach(function(element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = 'rgb(0, 0, 0)';

                        var fontSize = 16;
                        var fontStyle = 'normal';
                        var fontFamily = 'Helvetica Neue';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                        // Just naively convert to string for now
                        var dataString = dataset.data[index].toString();

                        // Make sure alignment settings are correct
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';

                        var padding = 15;
                        var position = element.tooltipPosition();
                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                    });
                }
            });
        }
    });*/

/*plugin code end*/

Chart.pluginService.register({
  beforeInit: function(chart) {
    var hasWrappedTicks = chart.config.data.labels.some(function(label) {
      return label.indexOf('\n') !== -1;
    });

    if (hasWrappedTicks) {
      // figure out how many lines we need - use fontsize as the height of one line
      var tickFontSize = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].ticks.fontSize, Chart.defaults.global.defaultFontSize);
      var maxLines = chart.config.data.labels.reduce(function(maxLines, label) {
        return Math.max(maxLines, label.split('\n').length);
      }, 0);
      var height = (tickFontSize + 2) * maxLines + (chart.options.scales.xAxes[0].ticks.padding || 0);

      // insert a dummy box at the bottom - to reserve space for the labels
      Chart.layoutService.addBox(chart, {
        draw: Chart.helpers.noop,
        isHorizontal: function() {
          return true;
        },
        update: function() {
          return {
            height: this.height
          };
        },
        height: height,
        options: {
          position: 'bottom',
          fullWidth: 1,
        }
      });

      // turn off x axis ticks since we are managing it ourselves
      chart.options = Chart.helpers.configMerge(chart.options, {
        scales: {
          xAxes: [{
            ticks: {
              display: false,
              // set the fontSize to 0 so that extra labels are not forced on the right side
              fontSize: 0
            }
          }]
        }
      });

      chart.hasWrappedTicks = {
        tickFontSize: tickFontSize
      };
    }
  },
  afterDraw: function(chart) {
    if (chart.hasWrappedTicks) {
      // draw the labels and we are done!
      chart.chart.ctx.save();
      var tickFontSize = chart.hasWrappedTicks.tickFontSize;
      var tickFontStyle = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].ticks.fontStyle, Chart.defaults.global.defaultFontStyle);
      var tickFontFamily = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].ticks.fontFamily, Chart.defaults.global.defaultFontFamily);
      var tickLabelFont = Chart.helpers.fontString(tickFontSize, tickFontStyle, tickFontFamily);
      chart.chart.ctx.font = tickLabelFont;
      chart.chart.ctx.textAlign = 'center';
      var tickFontColor = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].fontColor, Chart.defaults.global.defaultFontColor);
      chart.chart.ctx.fillStyle = tickFontColor;

      var meta = chart.getDatasetMeta(0);
      var xScale = chart.scales[meta.xAxisID];
      var yScale = chart.scales[meta.yAxisID];

      chart.config.data.labels.forEach(function(label, i) {
        label.split('\n').forEach(function(line, j) {
          chart.chart.ctx.fillText(line, xScale.getPixelForTick(i + 0.5), (chart.options.scales.xAxes[0].ticks.padding || 0) + yScale.getPixelForValue(yScale.min) +
            // move j lines down
            j * (chart.hasWrappedTicks.tickFontSize + 2));
        });
      });

      chart.chart.ctx.restore();
    }
  }
});

/*pie chart*/

Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.doughnut);

var helpers = Chart.helpers;
var defaults = Chart.defaults;

Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
	updateElement: function(arc, index, reset) {
    var _this = this;
    var chart = _this.chart,
        chartArea = chart.chartArea,
        opts = chart.options,
        animationOpts = opts.animation,
        arcOpts = opts.elements.arc,
        centerX = (chartArea.left + chartArea.right) / 2,
        centerY = (chartArea.top + chartArea.bottom) / 2,
        startAngle = opts.rotation, // non reset case handled later
        endAngle = opts.rotation, // non reset case handled later
        dataset = _this.getDataset(),
        circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
        innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
        outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
        custom = arc.custom || {},
        valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

    helpers.extend(arc, {
      // Utility
      _datasetIndex: _this.index,
      _index: index,

      // Desired view properties
      _model: {
        x: centerX + chart.offsetX,
        y: centerY + chart.offsetY,
        startAngle: startAngle,
        endAngle: endAngle,
        circumference: circumference,
        outerRadius: outerRadius,
        innerRadius: innerRadius,
        label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
      },

      draw: function () {
      	var ctx = this._chart.ctx,
						vm = this._view,
						sA = vm.startAngle,
						eA = vm.endAngle,
						opts = this._chart.config.options;
				
					var labelPos = this.tooltipPosition();
					var segmentLabel = vm.circumference / opts.circumference * 100;
					
					ctx.beginPath();
					
					ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
					ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);
					
					ctx.closePath();
					ctx.strokeStyle = vm.borderColor;
					ctx.lineWidth = vm.borderWidth;
					
					ctx.fillStyle = vm.backgroundColor;
					
					ctx.fill();
					ctx.lineJoin = 'bevel';
					
					if (vm.borderWidth) {
						ctx.stroke();
					}
					
					if (vm.circumference > 0.15) { // Trying to hide label when it doesn't fit in segment
						ctx.beginPath();
						ctx.font = helpers.fontString(opts.defaultFontSize, opts.defaultFontStyle, opts.defaultFontFamily);
						ctx.fillStyle = "#fff";
						ctx.textBaseline = "top";
						ctx.textAlign = "center";
            
            // Round percentage in a way that it always adds up to 100%
						ctx.fillText(segmentLabel.toFixed(0) + "%", labelPos.x, labelPos.y);
					}
      }
    });

    var model = arc._model;
    model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
    model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
    model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
    model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

    // Set correct angles if not resetting
    if (!reset || !animationOpts.animateRotate) {
      if (index === 0) {
        model.startAngle = opts.rotation;
      } else {
        model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
      }

      model.endAngle = model.startAngle + model.circumference;
    }

    arc.pivot();
  }
});

/*pie*/

Chart.pluginService.register({
  beforeRender: function(chart) {
    if (chart.config.options.showAllTooltips) {
      // create an array of tooltips
      // we can't use the chart tooltip because there is only one tooltip per chart
      chart.pluginTooltips = [];
      chart.config.data.datasets.forEach(function(dataset, i) {
        chart.getDatasetMeta(i).data.forEach(function(sector, j) {
          chart.pluginTooltips.push(new Chart.Tooltip({
            _chart: chart.chart,
            _chartInstance: chart,
            _data: chart.data,
            _options: chart.options.tooltips,
            _active: [sector]
          }, chart));
        });
      });

      // turn off normal tooltips
      chart.options.tooltips.enabled = false;
    }
  },
  afterDraw: function(chart, easing) {
    if (chart.config.options.showAllTooltips) {
      // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
      if (!chart.allTooltipsOnce) {
        if (easing !== 1)
          return;
        chart.allTooltipsOnce = true;
      }

      // turn on tooltips
      chart.options.tooltips.enabled = true;
      Chart.helpers.each(chart.pluginTooltips, function(tooltip) {
        tooltip.initialize();
        tooltip.update();
        // we don't actually need this since we are not animating tooltips
        tooltip.pivot();
        tooltip.transition(easing).draw();
      });
      chart.options.tooltips.enabled = false;
    }
  }
});
Chart.defaults.global.plugins= {
  
 labels: {
          render: () => {}
        }
};
Chart.plugins.unregister(ChartDataLabels);

/*Chart('pie').defaults.global.plugins= {
  
 labels: {
          render:'percentage'
        }
};
*/
/*var function imagebackground(){
*/
	/*var backgroundColor = 'white';
Chart.plugins.register({
    beforeDraw: function(c) {
        var ctx = c.chart.ctx;
        ctx.fillStyle = backgroundColor;
        ctx.fillRect(0, 0, c.chart.width, c.chart.height);
    }
});*/
/*}*/
var plugin = {beforeDraw: function(c) {
        var ctx = c.chart.ctx;
        ctx.fillStyle = 'white';
        ctx.fillRect(0, 0, c.chart.width, c.chart.height);
    } };
  var legendcallback=function(chart) {
			
  var text = [];
  text.push('<ul class="' + chart.id + '-legend">');
  for (var i = 0; i < chart.data.datasets[0].label.length; i++) {
  	
    text.push('<li style="width:100%;"><span style="background-color:' + 
    chart.data.datasets[0].backgroundColor[i] + ';"></span><span style="margin-left:5px;width:70%;font-family:Source Sans Pro;font-weight:600;color:#505050;font-size:14px;">');
      if (chart.data.datasets[0].label[i]) {
      text.push(chart.data.datasets[0].label[i]);
    }
    text.push('</span></li>');
  }
  text.push('</ul>');
  return text.join("");
};
var pie_legendcallback=function(chart) {
			
  var text = [];
  text.push('<ul class="' + chart.id + '-legend">');
  for (var i = 0; i < chart.data.labels.length; i++) {
  	
    text.push('<li style="width:100%;padding-bottom: 5px;"><span style="background-color:' + 
    chart.data.datasets[0].backgroundColor[i] + ';"></span><span style="margin-left:5px;width: 80%;font-family:Source Sans Pro;font-weight:600;color:#505050;font-size:14px;">');
      if (chart.data.labels[i]) {
      text.push(chart.data.labels[i]);
    }
    text.push('</span></li>');
  }
  text.push('</ul>');
  return text.join("");
};
var legendcallback_per=function(chart) {
			
  var text = [];
  text.push('<ul class="' + chart.id + '-legend">');
  for (var i = 0; i < chart.data.datasets.length; i++) {
    text.push('<li style="width:45%;padding-bottom: 5px;"><span style="background-color:' + 
    chart.data.datasets[i].backgroundColor[i] + ';"></span><span style="margin-left:5px;width:77%;font-family:Source Sans Pro;font-weight:600;color:#505050;font-size:14px;">');
      if (chart.data.datasets[i].label) {
      text.push(chart.data.datasets[i].label);
    }
    text.push('</span></li>');
  }
  text.push('</ul>');
  return text.join("");
};
        /*ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
ctx.fillStyle = c.chart.config.options.defaultFontColor;
ctx.textAlign = 'center';
ctx.textBaseline = 'bottom';
c.data.datasets.forEach(function (dataset) {
for (var i = 0; i < dataset.data.length; i++) {
var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
ctx.fillText(dataset.data[i], model.x, model.y - 5);
}
    });*/
/*}
});*/






$('#getmonthanalys2search').click(function(){
  alert("The paragraph was clicked.");
});

</script>

