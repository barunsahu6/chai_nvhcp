<?php 
$loginData = $this->session->userdata('loginData');
$sql = "SELECT TInitiate FROM `MSTRole` where RoleId = ".$loginData->RoleId;
$result = $this->db->query($sql)->result();
//echo 'fffff'.$fac_user[0]->is_Mtc;
?>
<style>
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}

	.input_fields
	{
		height: 30px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	textarea
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}

	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}

	.form_buttons:focus
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}

	@media (min-width: 768px) {
		.row.equal {
			display: flex;
			flex-wrap: wrap;
		}
	}

	@media (max-width: 768px) {

		.input_fields
		{
			height: 40px !important;
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
		}

		.form_buttons
		{
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
			font-weight: 600 !important;
			padding: 8px 12px !important;
			border: 2px solid #A30A0C !important;

		}
		.form_buttons:hover
		{
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
			font-weight: 600 !important;
			padding: 8px 12px !important;
			border: 2px solid #A30A0C !important;
			background-color: #FFF;
			color: #A30A0C;

		}
	}

	.btn-default {
		color: #333 !important;
		background-color: #fff !important;
		border-color: #ccc !important;
	}
	.btn {
		display: inline-block;
		padding: 6px 12px;
		margin-bottom: 0;
		font-size: 14px;
		font-weight: 400;
		line-height: 2.4;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		-ms-touch-action: manipulation;
		touch-action: manipulation;
		cursor: pointer;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		background-image: none;
		border: 1px solid transparent;
		border-radius: 4px;
	}

	a.btn
	{
		text-decoration: none;
		color : #000;
		background-color: #A30A0C;
	}

	.btn-group .btn:hover
	{
		text-decoration: none !important;
		color: #000 !important;
		background-color: #CCC !important;
	}

	.btn-group .btn:hover
	{
		text-decoration: none !important;
		color: #000 !important;
		background-color: inherit !important;
	}

	a.active
	{
		color : #FFF !important;
		text-decoration: none;
		background-color: inherit !important;
	}

	#table_patient_list tbody tr:hover
	{
		cursor: pointer;
	}

	.btn-success
	{
		background-color: #A30A0C;
		color: #FFF !important;
		border : 1px solid #A30A0C;
	}

	.btn-success:hover
	{
		text-decoration: none !important;
		color: #A30A0C !important;
		background-color: white !important;
		border : 1px solid #A30A0C;
	}
</style>

<br>
<?php //print_r($patient_data); ?>
<div class="row equal">

	<!-- <form action="" name="patient_form" id="patient_form" method="POST"> -->
		<?php
		$attributes = array(
			'id' => 'patient_form',
			'name' => 'patient_form',
			'autocomplete' => 'off',
		);
		echo form_open('', $attributes); ?>

		<input type="hidden" name="fetch_uid" id="fetch_uid">
		<?php echo form_close(); ?>
		<!-- <input type="hidden" name="fetch_uid" id="fetch_uid"> -->
		<!-- </form> -->
		<div class="col-lg-10 col-lg-offset-1">

			<div class="row">
				<div class="col-md-12 text-center">
					<div class="btn-group">
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">1. Registration</a>
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">2. Screening</a>
						
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">3. Testing</a>
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">4. HBV DNA</a>
						
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/known_history/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">5. Known History</a>
						<a class="btn btn-success" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">6. Prescription</a>
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">7. Dispensation</a>
						
					</div>
				</div>
			</div>

			<!-- <form action="" method="POST" name="registration" id="registration"> -->
				<?php
				$attributes = array(
					'id' => 'registration',
					'name' => 'registration',
					'autocomplete' => 'off',
				);
				echo form_open('', $attributes); ?>
				<div class="row">
					<div class="col-md-12" style="padding-right: 0;">
						<h4 class="text-center"><p>Patient Name - <strong><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FirstName)):''; ?></strong>  (<?php echo (count($patient_data) > 0)?$patient_data[0]->UID_Prefix:$uid_prefix; echo '-' .str_pad($patient_data[0]->UID_Num, 6, '0', STR_PAD_LEFT); ?>)<p></h4>
							<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Patient Prescription Module </h3>
						</div>
				<!-- <div class="col-md-2" style="padding-left: 0;">
					<h3><a href="<?php echo base_url(); ?>patientinfo_hep_b/patient_prescription_followup_visit/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>" class="btn btn-success btn-block" style="border-radius: 0; line-height: 2.2; font-weight: 500; font-weight: 600; padding-left: 5px; border : 2px solid #A30A0C;">Add FollowUp Visit</a></h3>
				</div> -->
			</div>
			<br>
			
			<div class="row">
				<div class="col-md-3">
					<label for="">Prescribing Facility <span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="prescribing_facility" name="prescribing_facility" required="">
						<option value="">Select</option>
						<?php /*foreach ($prescribing_facilities as $row) {?>
							<option value="<?php echo $row->id_mstfacility; ?>" 
								<?php 

								if($patient_data[0]->PrescribingFacility == null && $row->id_mstfacility == $loginData->id_mstfacility)
									echo "selected"; 
								else if ($patient_data[0]->PrescribingFacility != null && $patient_data[0]->PrescribingFacility == $row->id_mstfacility)
									echo "selected"; 
								else
									echo '';
								?>


								><?php echo $row->facility_short_name; ?></option>
							<?php }*/ ?>


							<?php if(count($patient_data) > 0 && !empty($patient_data[0]->ReferTo ) ) { ?>
								<option value="<?php echo $ReferToFaci[0]->id_mstfacility; ?>" <?php if($patient_data[0]->PrescribingFacility == $ReferToFaci[0]->id_mstfacility){ echo "selected"; }  ?>><?php echo $ReferToFaci[0]->FacilityCode; ?></option>

							<?php } ?>
							<?php foreach ($place_of_dispensation as $row) { ?>
								<option  value="<?php echo $row->id_mstfacility; ?>"
									<?php 
									if($patient_data[0]->PrescribingFacility == null && $row->id_mstfacility == $loginData->id_mstfacility)
										echo "selected"; 
									else if ($patient_data[0]->PrescribingFacility != null && $patient_data[0]->PrescribingFacility == $row->id_mstfacility)
										echo "selected"; 
									else
										echo '';
									?>
									><?php echo $row->FacilityCode; ?></option>

								<?php } ?>
							</select>
						</div>
						<div class="col-md-3">
							<label for="">Prescribing Doctor <span class="text-danger">*</span></label>
							<select class="form-control input_fields" id="prescribing_doctor" name="prescribing_doctor" required="">
								<option value="">Select</option>
								<?php foreach ($prescribing_doctor as $row) {?>
									<option <?php echo (count($patient_data) > 0 && $patient_data[0]->PrescribingDoctor == $row->id_mst_medical_specialists)?'selected':''; ?> value="<?php echo $row->id_mst_medical_specialists; ?>"><?php echo $row->name; ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-3 prescribing_doctor_other_field">
							<label for="">Prescribing Doctor Other <span class="text-danger">*</span></label>
							<input type="text" name="prescribing_doctor_other" id="prescribing_doctor_other" class="input_fields form-control messagedata" value="<?php echo (count($patient_data) > 0)?$patient_data[0]->PrescribingDoctorOther:''; ?>">
						</div>


					</div>
					<br>

					<div class="row" style="background-color: lightblue; padding-top: 15px; padding-bottom: 15px;">
						<div class="col-md-4" >
							<label for="">Drug </label>
							<select class="form-control input_fields" id="Drug" name="Drug" required>
								<option value="">Select</option>
								<?php foreach($drug_list as $row){ ?>
									<option value="<?=$row->LookupCode?>" <?php echo (count($patient_data) > 0 && $patient_data[0]->Drug == $row->LookupCode)?'selected':''; ?>><?=$row->LookupValue?></option>
								<?php } ?>								
							</select>
						</div>

						<div class="col-md-3 sofosbuvir_field" >
							<label for="">Pills to be taken as <span class="text-danger">*</span></label>
							<select class="form-control input_fields" id="PillsToBeTaken" name="PillsToBeTaken" required>
								<option value="">Select</option>
								<?php foreach($pills_to_be_taken_list as $row){ ?>
									<option value="<?=$row->LookupCode?>" <?php echo (count($patient_data) > 0 && $patient_data[0]->PillsToBeTaken == $row->LookupCode)?'selected':''; ?>><?=$row->LookupValue?></option>
								<?php } ?>	
							</select>
						</div>	

						<div class="col-md-4" >
							<a href="#" data-toggle="modal" data-target="#treatment_CHB_adults"><b>Treatment of CHB and their doses in adults  <i class="fa fa-eye" aria-hidden="true"></i></b></a><br><br>

							<a href="#" data-toggle="modal" data-target="#treatment_CHB_children"><b>Treatment of CHB and their doses in children  <i class="fa fa-eye" aria-hidden="true"></i></b></a>
						</div>					
					</div>
					<br>

					<div class="row">
						<div class="col-md-3 DurationShow">
							<label for="">Other Duration (Weeks)<span class="text-danger">*</span></label>
							<select class="form-control input_fields" id="durationother" name="durationother">
								<option value="">Select</option>
								<?php foreach ($duration as $row) {?>
									<option <?php echo (count($patient_data) > 0 && $patient_data[0]->T_DurationOther == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php } ?>

							</select>
						</div>
						<div class="col-md-3 DurationShow">
							<label for="">Reason<span class="text-danger">*</span></label>
							<input type="text" name="reason" id="reason" class="input_fields form-control" value="<?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->DurationReason)):''; ?>">
						</div>


						<div class="col-md-3">
							<label for="">Prescribing Date <span class="text-danger">*</span></label>
							<input type="text" required="" name="prescribing_date" id="prescribing_date" class="input_fields form-control hasCal dateInpt input2" value="<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->PrescribingDate):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
							<br class="hidden-lg-*">
						</div>
						<div class="col-md-3">
							<label for="">Place Of Dispensation</label>
							<select class="form-control input_fields" id="place_of_dispensation" name="place_of_dispensation">
								<option value="">Select</option>
								<?php error_reporting(0); if(count($patient_data) > 0 && !empty($patient_data[0]->ReferTo ) ) { ?>
									<option value="<?php  echo $ReferToFaci[0]->id_mstfacility; ?>" <?php if($patient_data[0]->PrescribingFacility == $ReferToFaci[0]->id_mstfacility){ echo "selected"; }  ?>><?php echo $ReferToFaci[0]->FacilityCode; ?></option>

								<?php } ?>
								<?php foreach ($place_of_dispensation as $row) { ?>
									<option  value="<?php echo $row->id_mstfacility; ?>"
										<?php 
										if($patient_data[0]->PrescribingFacility == null && $row->id_mstfacility == $loginData->id_mstfacility)
											echo "selected"; 
										else if ($patient_data[0]->PrescribingFacility != null && $patient_data[0]->PrescribingFacility == $row->id_mstfacility)
											echo "selected"; 
										else
											echo '';
										?>
										><?php echo $row->FacilityCode; ?></option>

									<?php } ?>


								</select>
							</div>
						</div>
						<br>
						<input type="hidden" name="interruption_status" id="interruption_status" value="<?php  echo (count($patient_data) > 0)?$patient_data[0]->InterruptReason:'';  ?>">
						<div class="row">
							<div class="col-lg-3 col-md-2">
								<a class="btn btn-block btn-default form_buttons" href="<?php echo base_url('patientinfo_hep_b?p=1'); ?>" id="close" name="close" value="close">CLOSE</a>
							</div>
							<div class="col-lg-3 col-md-2">
								<button class="btn btn-block btn-default form_buttons" id="refresh" name="refresh" value="refresh">REFRESH</button>
							</div>
							<?php if($loginData->RoleId!=99){ ?>
								<div class="col-lg-3 col-md-2">
									<a href="" class="btn btn-block btn-default form_buttons" id="lock" name="lock" value="lock">LOCK</a>
								</div>
							<?php } else{?>
								<div class="col-lg-3 col-md-2">
									<a href="javascript:void(0)" class="btn btn-block btn-default form_buttons" onclick="openPopupUnlock()" id="" name="unlock" value="lock">LOCK</a>
								</div>
							<?php } ?>
							<?php if($result[0]->TInitiate == 1) {?>
								<div class="col-lg-3 col-md-3">
									<button class="btn btn-block btn-success form_buttons" id="save" name="save" value="save">SAVE</button>
								</div>
							<?php } ?>
						</div>
						<input type="hidden" name="Prescribing_Dtn" id="Prescribing_Dtn" value="<?php echo timeStampShow($patient_cirrohosis_data[0]->Prescribing_Dt); ?>" class="hasCal dateInpt input2">
						<input type="hidden" name="ReferalDaten" id="ReferalDaten" value="<?php echo timeStampShow($patient_data[0]->ReferalDate); ?>" class="hasCal dateInpt input2">
						<br><br><br>
						<?php echo form_close(); ?>
						<div class="row" class="text-left">

							<div class="col-md-6 card well">
								<label class="col-sm-4"  style="text-align: left !important; line-height: 2.2 !important; ">Patient's Status -</label>
								<div class="col-sm-8" style="text-align: left !important; line-height: 2.2 !important; ">
									<label><?php echo (count($patient_status) > 0)?$patient_status[0]->status:''; ?></label>

								</div>
							</div>



							<div class="col-md-6 card well">
								<label class="col-sm-6" style="text-align: left !important; ">Patient's Interruption Status </label>
								<div class="col-sm-6">
									<select name="" id="type_val" onchange="openPopup()" class="btn" style="text-align: right!important;">
										<option value="">Select</option>
										<option value="1"  <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason != '')?'selected':''; ?> >Yes</option>
										<option value="0" <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason == '')?'selected':''; ?>>No</option>
									</select>

								</div>
							</div>

						</div>

						<!-- </form> -->
					</div>
				</div>



				<div class="modal fade" id="addMyModal" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">

								<h4 class="modal-title">Patient's Interruption Status</h4>
							</div>
							<span id='form-error' style='color:red'></span>
							<div class="modal-body">
								<!--   <form role="form" id="newModalForm" method="post"> -->
									<?php
									$attributes = array(
										'id' => 'newModalForm',
										'name' => 'newModalForm',
										'autocomplete' => 'off',
									);
									echo form_open('', $attributes); ?>


									<div class="form-group">
										<label class="control-label col-md-3" for="email">Reason:</label>
										<div class="col-md-9">
											<select class="form-control" id="resonval" name="resonval" required="required">
												<option value="">Select</option>
												<?php foreach ($InterruptReason as $key => $value) { ?>
													<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
												<?php } ?>
											</select> 
										</div>
									</div>
									<br/><br/>

									<div class="form-group resonfielddeath" >
										<label class="control-label col-md-3" for="email">Reason for death:</label>
										<div class="col-md-9">
											<select class="form-control" id="resonvaldeath" name="resonvaldeath">
												<option value="">Select</option>
												<?php foreach ($reason_death as $key => $value) { ?>
													<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>

												<?php } ?>
											</select> 
										</div>
									</div>
									<br/><br/>

									<div class="form-group resonfieldlfu">
										<label class="control-label col-md-3" for="email">Reason for LFU:</label>
										<div class="col-md-9">
											<select class="form-control" id="resonfluid" name="resonfluid">
												<option value="">Select</option>
												<?php foreach ($reason_flu as $key => $value) { ?>
													<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>

												<?php } ?>
											</select> 
										</div>
									</div>
									<div class="form-group resonfieldothers" >
										<label class="control-label col-md-3" for="email">Other(specify):</label>
										<div class="col-md-9">
											<input type="text" class="form-control" id="otherInterrupt" name="otherInterrupt">
										</div>
									</div>
									<br/><br/>

									<div class="modal-footer">
										<button type="submit" class="btn btn-success" id="btnSaveIt">Save</button>
										<button type="button" class="btn btn-default" id="btnCloseIt" data-dismiss="modal">Close</button>
									</div>
									<?php echo form_close(); ?>
									<!--  </form> -->
								</div>
							</div>
						</div>
					</div>
					<br/><br/><br/>


					<!-- Unlock box start-->
					<div class="modal fade" id="addMyModalUnbox" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">

									<h4 class="modal-title">Unlock Process</h4>
								</div>
								<span id='form-error' style='color:red'></span>
								<div class="modal-body">
									<!--   <form role="form" id="newModalForm" method="post"> -->
										<?php
										$attributes = array(
											'id' => 'unlockModalForm',
											'name' => 'unlockModalForm',
											'autocomplete' => 'off',
										);
										echo form_open('', $attributes); ?>


										<div class="form-group">
											<label class="control-label col-md-3" for="email">Reason:</label>
											<div class="col-md-9">
												<select class="form-control" id="unlockresonval" name="unlockresonval" required="required">
													<option value="">Select</option>
													<?php foreach ($unlockprocess as $key => $value) { ?>
														<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
													<?php } ?>
												</select> 
											</div>
										</div>
										<br/><br/>

										<div class="form-group unlockresonfieldothers" >
											<label class="control-label col-md-3" for="email">Other(specify):</label>
											<div class="col-md-9">
												<input type="text" class="form-control" id="unlockotherInterrupt" name="unlockotherInterrupt">
											</div>
										</div>
										<br/><br/>


										<div class="modal-footer">
											<button type="submit" class="btn btn-success" id="unlock">Save</button>
											<button type="button" class="btn btn-default" id="btnunlock" data-dismiss="modal">Close</button>
										</div>
										<?php echo form_close(); ?>
										<!--  </form> -->
									</div>
								</div>
							</div>
						</div>
						<!-- Modal -->
						<div id="treatment_CHB_adults" class="modal fade" role="dialog">
							<div class="modal-dialog">

								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Recommended drugs</h4>
									</div>
									<div class="modal-body">
										<h4>Recommended drugs for the treatment of CHB and their doses in adults</h4>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>S No</th>
													<th>Drug</th>
													<th>Dose</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>1</td>
													<td>Tenofovirdisoproxil fumarate (TDF)</td>
													<td>300 mg once daily</td>
												</tr>
												<tr>
													<td>2</td>
													<td>Entecavir(adult with compensated liver disease and lamivudine naive)</td>
													<td>0.5 mg once daily</td>
												</tr>
												<tr>
													<td>3</td>
													<td>Entecavir(adult with decompensated liver disease)</td>
													<td>1 mg once daily</td>
												</tr>
												<tr>
													<td>4</td>
													<td>Tenofoviralafenamide fumarate (TAF)</td>
													<td>25 mg once daily</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>

							</div>
						</div>

						<div id="treatment_CHB_children" class="modal fade" role="dialog">
							<div class="modal-dialog modal-lg">

								<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Recommended Drugs</h4>
									</div>
									<div class="modal-body">
										<h4>Recommended drugs for the treatment of CHB and their doses in children</h4>
										<table class="table table-bordered">
											<thead>
												<tr>
													<th>Drug</th>
													<th colspan="2">Dose</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Tenofovir (in children 12 years of age and older, and weighing at least 35kg)</td>
													<td colspan="2">300 mg once daily</td>
												</tr>
												<tr>
													<td rowspan="10">Entecavir (in children 2 years of age or older and weighing at least 10kg. the oral solution should be given to children with a body weight up to 30kg)</td>
													<td colspan="2"><b>Recommended once-daily dose of oral solution (mL)</b></td>
												</tr>
												<tr>
													<td>Body weight (kg)</td>
													<td>Treatment –naïve persons*</td>
												</tr>
												<tr>
													<td>10 to 11</td>
													<td>3</td>
												</tr>
												<tr>
													<td>>11 to 14</td>
													<td>4</td>
												</tr>
												<tr>
													<td>>14 to 17</td>
													<td>5</td>
												</tr>
												<tr>
													<td>> 17 to 20</td>
													<td>6</td>
												</tr>
												<tr>
													<td>>20 to 23</td>
													<td>7</td>
												</tr>
												<tr>
													<td>>23 to 26</td>
													<td>8</td>
												</tr>
												<tr>
													<td>>26 to 30</td>
													<td>9</td>
												</tr>
												<tr>
													<td>30</td>
													<td>10</td>
												</tr>
												
											</tbody>
										</table>
										<b>*Children with body weight more than 30 kg should 
										receive 10mL (0.5 mg) of oral solution or one 0.5 mg tablet once daily.</b>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>

							</div>
						</div>
						<br/><br/><br/>
						<!-- end unlock -->
						<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
						<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>

						<script>

							/*$('#regimen_prescribed').change(function(){
								var sofosbuvir = $('#sofosbuvir').val();
								$('#sofosbuvir').val(1);

							});*/



							$(document).ready(function(){

								var statusval			 = '<?php echo $patient_data[0]->MF6; ?>';
								var InterruptReason 	= '<?php echo $patient_data[0]->InterruptReason; ?>';

								if(statusval==0 && InterruptReason==1){

									$("#modal_header").text("Further entry is not allowed,as per treatment status...");
									$("#modal_text").text("Not allowed");
									$("#multipurpose_modal").modal("show");

									location.href='<?php echo base_url(); ?>patientinfo_hep_b/known_history/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';

								}
							});

							function openPopup() {
								var type_val = $('#type_val').val();
								if(type_val=='1'){
									$("#addMyModal").modal();
								}
							}

							$('.resonfielddeath').hide();
							$('.resonfieldlfu').hide();
							$('.resonfieldothers').hide();

							$('#btnCloseIt').click(function(){

								$('#type_val').val('0');
								$('#interruption_status').val('');

							});

							$('#type_val').change(function(){
								var type_val = $('#type_val').val();
								$('#interruption_status').val(type_val);

							});


							$('#btnSaveIt').click(function(e){

								e.preventDefault(); 
								$("#form-error").html('');


								var formData = new FormData($('#newModalForm')[0]);
								$('#btnSaveIt').prop('disabled',true);
								$.ajax({				    	
									url: '<?php echo base_url(); ?>patientinfo_hep_b/interruptionstatus_process/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
									type: 'POST',
									data: formData,
									dataType: 'json',
									async: false,
									cache: false,
									contentType: false,
									processData: false,
									success: function (data) { 

										if(data['status'] == 'true'){
											$("#form-error").html('Data has been successfully submitted');
											setTimeout(function() {
												location.href='<?php echo base_url(); ?>patientinfo_hep_b/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
											}, 1000);

										}else{
											$("#form-error").html(data['message']);
											$('#btnSaveIt').prop('disabled',false); 
											return false;
										}   
									}        
								}); 



							}); 


							function openPopupUnlock() {

								$("#addMyModalUnbox").modal();

							}

							$('.unlockresonfieldothers').hide();
							$('#unlockresonval').change(function(){
								var unlockresonval = $('#unlockresonval').val();
								/*	if(unlockresonval  ==4){
									$('.unlockresonfieldothers').show();
									}else{
									$('.unlockresonfieldothers').hide();	
								}*/
							});

							$('#resonval').change(function(){


								if($('#resonval').val() == '1'){

									$('.resonfielddeath').show();
									$('.resonfieldlfu').hide();
									$('.resonfieldothers').hide();
									$('#resonfluid').val('');

									$('#resonvaldeath').prop('required',true);
									$('#resonfluid').prop('required',false);
									$('#otherInterrupt').prop('required',false);

								}else if($('#resonval').val() == '2'){
									$('.resonfielddeath').hide();
									$('.resonfieldlfu').show();
									$('.resonfieldothers').hide();
									$('#resonvaldeath').val('');
									$('#resonvaldeath').prop('required',false);
									$('#resonfluid').prop('required',true);
									$('#otherInterrupt').prop('required',false);
								}else if($('#resonval').val() == '99'){

									$('.resonfieldothers').show();
									$('.resonfielddeath').hide();
									$('.resonfieldlfu').hide();
									$('#otherInterrupt').prop('required',true);
									$('#resonvaldeath').prop('required',false);
									$('#resonfluid').prop('required',false);
								}

								else{

									$('.resonfielddeath').hide();
									$('.resonfieldlfu').hide();
									$('.resonfieldothers').hide();
									$('#resonvaldeath').val('');
									$('#resonfluid').val('');
									$('#resonvaldeath').prop('required',false);
									$('#resonfluid').prop('required',false);
									$('#otherInterrupt').prop('required',false);
								}

							});

						</script>

						<?php if(count($patient_data) > 0 && $patient_data[0]->MF7 == 1) {?>
							<script>

								$("#lock").click(function(){
									$("#modal_header").text("Contact admin ,for unlocking the record");
									$("#modal_text").text("Contact Admin");
									$("#multipurpose_modal").modal("show");
									return false;

								});

								/*Unlock process start*/

								$("#unlock").click(function(e){
									/*var r = confirm("Do you need to unlock a data ?");
									if (r == true) {*/

										e.preventDefault(); 
										$("#form-error").html('');

										var formData      = new FormData($('#unlockModalForm')[0]);
										$.ajax({				    	
											url: '<?php echo base_url(); ?>Unlock/unlock_process_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
											type: 'POST',
											data: formData,
											dataType: 'json',
											async: false,
											cache: false,
											contentType: false,
											processData: false,
											success: function (data) { 

												if(data['status'] == 'true'){
													alert('Data has been successfully unlock');
													setTimeout(function() {
														location.href='<?php echo base_url(); ?>patientinfo_hep_b/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
													}, 1000);

												}else{
													$("#form-error").html(data['message']);

													return false;
												}   
											}        
										}); 


									});

								/*end unlock process*/

								/*Disable all input type="text" box*/

								$('#registration input[type="text"]').attr("readonly", true);
								$('#registration input[type="checkbox"]').attr("disabled", true);
								$('#registration input[type="date"]').attr("readonly", true);
								$("#save").attr("disabled", true);
								$("#refresh").attr("disabled", true);
								$('#registration select').attr('disabled', true);
								/*Disable textarea using id */
								$('#registration #txtAddress').prop("readonly", true);

							</script>
						<?php  } ?>

						<?php if($result[0]->TInitiate == 1) {?>
							<script>
								$(document).ready(function(){

									<?php 
									$error = $this->session->flashdata('error'); 

									if($error != null)
									{
										?>
										$("#multipurpose_modal").modal("show");
										<?php 
									}
									?>


									$("#refresh").click(function(e){
			    			// if(confirm('All further details will be deleted.Do you want to continue?')){ 
			    			$("input").val('');
			    			$("select").val('');
			    			$("textarea").val('');
			    			$("#input_district").html('<option>Select District</option>');
			    			$("#input_block").html('<option>Select Block</option>');

			    			e.preventDefault();
				// }
			});

				//$(".prescribing_doctor_other_field").hide();
				$(".sofosbuvir_field").hide();
				$(".daclatasvir_field").hide();
				$(".ribavrin_field").hide();
				$(".velpatasvir_field").hide();
				//$(".DurationShow").hide();

				$('#table_patient_list tbody tr').click(function(){
					window.location = $(this).data('href');
				});

				$("#prescribing_doctor").change(function(){
					if($(this).val() == 999)
					{
						$(".prescribing_doctor_other_field").show();
						$('#prescribing_doctor_other').prop('required',true);
					}
					else
					{
						$(".prescribing_doctor_other_field").hide();
						$('#prescribing_doctor_other').prop('required',false);
					}
				});

				<?php if(!empty($selected) && $patient_data[0]->T_Regimen==''){ ?>
					$(".sofosbuvir_field").show();
					$(".daclatasvir_field").show();
				<?php } ?>
				<?php if($patient_data[0]->T_Regimen=='' && $fac_user[0]->is_Mtc==0){ ?>
					$(".sofosbuvir_field").show();
					$(".daclatasvir_field").show();
				<?php } ?>
				<?php if($patient_data[0]->T_Regimen=='' && ($patient_data[0]->ChildScore == 5 || $patient_data[0]->ChildScore == 6)){ ?>
					$(".sofosbuvir_field").show();
					$(".velpatasvir_field").show();
				<?php } ?>	

				/*$("#regimen_prescribed").change(function(){

					if($(this).val() == 1)
					{
						$(".sofosbuvir_field").show();
						$(".daclatasvir_field").show();
						$(".ribavrin_field").hide();
						$(".velpatasvir_field").hide();

						$('#sofosbuvir').prop('required',true);
						$('#daclatasvir').prop('required',true);
						$('#velpatasvir').prop('required',false);
						$('#ribavrin').prop('required',false);
						$('#velpatasvir').val('');
						$('#daclatasvir').val('');
						$('#ribavrin').val('');

					}
					else if($(this).val() == 2)
					{
						$(".sofosbuvir_field").show();
						$(".velpatasvir_field").show();
						$(".daclatasvir_field").hide();
						$(".ribavrin_field").hide();

						$('#sofosbuvir').prop('required',true);
						$('#velpatasvir').prop('required',true);

						$('#ribavrin').prop('required',false);
						$('#daclatasvir').prop('required',false);
						$('#ribavrin').val('');
						$('#daclatasvir').val('');
						$('#velpatasvir').val('');
					}
					else if($(this).val() == 3)
					{
						$(".sofosbuvir_field").show();
						$(".ribavrin_field").show();
						$(".velpatasvir_field").show();
						$(".daclatasvir_field").hide();

						$('#sofosbuvir').prop('required',true);
						$('#velpatasvir').prop('required',true);
						$('#ribavrin').prop('required',true);

						$('#daclatasvir').prop('required',false);
						$('#daclatasvir').val('');
					}
					else
					{
						$(".sofosbuvir_field").hide();
						$(".daclatasvir_field").hide();
						$(".ribavrin_field").hide();
						$(".velpatasvir_field").hide();

						$('#sofosbuvir').prop('required',false);
						$('#velpatasvir').prop('required',false);
						$('#ribavrin').prop('required',false);
						$('#daclatasvir').prop('required',false);

						$('#sofosbuvir').val('');
						$('#velpatasvir').val('');
						$('#ribavrin').val('');
						$('#daclatasvir').val('');

					}
				});*/
			})
		</script>
	<?php } else { ?>
		<script>
			$(document).ready(function(){
				$('input').prop('disabled', true);
				$('select').prop('disabled', true);
				$('textarea').prop('disabled', true);
			});

			



		</script>
	<?php } ?>
	<script type="text/javascript">
		$(document).ready(function(){
			<?php if(count($patient_data) > 0 && $patient_data[0]->T_Regimen == 1) { //$patient_data[0]->RecommendedRegimen?>

				$(".sofosbuvir_field").show();
				$(".daclatasvir_field").show();

			<?php }?>

			<?php if(count($patient_data) > 0 && $patient_data[0]->T_Regimen == 2) {?>

				$(".sofosbuvir_field").show();
				$(".velpatasvir_field").show();

			<?php }?>
			<?php if(count($patient_data) > 0 && $patient_data[0]->T_Regimen == 3) {?>
					//alert(<?php echo $patient_data[0]->RecommendedRegimen; ?>);
					$(".sofosbuvir_field").show();
					$(".velpatasvir_field").show();
					$(".ribavrin_field").show();

				<?php }?>
			});

		<?php 
		if( count($patient_data) > 0 && $patient_data[0]->PrescribingDoctor == 999 ) {
			?>

			$(".prescribing_doctor_other_field").show();
			$('#prescribing_doctor_other').prop('required',true);
		<?php } else{ ?>

			$(".prescribing_doctor_other_field").hide();
		<?php	}  ?>

		$('.messagedata').keypress(function (e) {
			var regex = new RegExp(/^[a-zA-Z\s]+$/);
			var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (regex.test(str)) {
				return true;
			}
			else {
				e.preventDefault();
				return false;
			}
		});

		/*Max Date Today Code*/
		var today = new Date();
		var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!

			var yyyy = today.getFullYear();
			if(dd<10){
				dd='0'+dd;
			} 
			if(mm<10){
				mm='0'+mm;	
			}

			today = yyyy+'-'+mm+'-'+dd;
			document.getElementById("prescribing_date").setAttribute("max", today);

/*$("#prescribing_date" ).change(function( event ) {

var date_of_prescribing_testsdate = '<?php echo $patient_data[0]->T_DLL_01_VLC_Date; ?>';
var prescribing_date = $("#prescribing_date" ).val();



if(date_of_prescribing_testsdate > prescribing_date){

	$("#modal_header").text("Date  greater than or equal to Test Result Date");
					$("#modal_text").text("Please check dates");
					$("#prescribing_date" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

});
*/

$("#prescribing_date" ).change(function( event ) {

	var date_of_prescribing_testsdate = $("#Prescribing_Dtn").datepicker('getDate');
	var referal_doctor_date = $("#ReferalDaten").datepicker('getDate');
	var prescribing_date = $("#prescribing_date").datepicker('getDate');



	if(date_of_prescribing_testsdate > prescribing_date){

		$("#modal_header").text("Date cannot be before Last test result date");
		$("#modal_text").text("Please check dates");
		$("#prescribing_date").val('');
		$("#multipurpose_modal").modal("show");
		return false;

	}

	if(referal_doctor_date > prescribing_date){

		$("#modal_header").text("Date cannot be before Referal doctor date");
		$("#modal_text").text("Please check dates");
		$("#prescribing_date").val('');
		$("#multipurpose_modal").modal("show");
		return false;

	}

});


$("#duration").change(function(){
	if($(this).val() == 99)
	{
		$(".DurationShow").show();
		$("#durationother").val('');
		$('#durationother').prop('required',true); 
		$('#resion').prop('required',true); 
	}
	else
	{
		$(".DurationShow").hide();
		$('#durationother').prop('required',false); 
		$('#resion').prop('required',false); 


	}
});

<?php if($patient_data[0]->T_DurationValue == 12 || $patient_data[0]->T_DurationValue ==24 || $patient_data[0]->T_DurationValue ==''){  ?>
	$(".DurationShow").hide();
<?php } else{ ?>
	$(".DurationShow").show();

<?php  } ?>



</script>