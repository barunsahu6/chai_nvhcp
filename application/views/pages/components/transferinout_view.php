<div id="reportModal22">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				
				<h4 class="modal-title text-center">Patient Transfer out Module </h4>
			</div>
			<div class="modal-body text-center">
				<table class="table table-hover table-highlighted table-bordered">
					<thead>
							
							<th class="text-center">Patient Name - <strong><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FirstName)):''; ?></strong></th>
							<th class="text-center">NVHCP ID - <?php echo (count($patient_data) > 0)?$patient_data[0]->UID_Prefix:$uid_prefix; echo '-' .str_pad($patient_data[0]->UID_Num, 6, '0', STR_PAD_LEFT);  ?></th>
							
							
						</thead>
						
						</table>


						
   <?php $cname = $this->router->fetch_class(); ?>
    <div class="row">
     <?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){ ?>
			

			<div class="col-md-8 col-md-offset-2  col-xs-offset-1">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				
				<?php } ?>
			</div>
      <div class="modal-body">
      <!--   <form role="form" id="newModalForm" method="post"> -->
      	<?php
           $attributes = array(
              'id' => 'transferinoutForm',
              'name' => 'transferinoutForm',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>


				<div class="form-group">
				<label class="control-label col-md-3" for="email">State:</label>
				<div class="col-md-9">
				<select type="text" name="search_state" id="search_state" class="form-control input_fields" required >
			<option value="">All States</option>
			<?php 
			foreach ($states as $state) {
				?>
				<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo $state->StateName; ?></option>
				<?php 
			}
			?>
		</select>
				</div>
				</div>
<br/><br/><br/>

				<div class="form-group resonfielddeath" >
				<label class="control-label col-md-3" for="email">District</label>
				<div class="col-md-9">
				<select type="text" name="input_district" id="input_district" class="form-control input_fields"  required>
			<option value="">Select District</option>
			<?php 
			
			?>
		</select>
				</div>
				</div> 
				
		
<br/><br/>

<div class="form-group resonfielddeath" >
				<label class="control-label col-md-3" for="email">Facility</label>
				<div class="col-md-9">
				<select type="text" name="facility" id="mstfacilitylogin" class="form-control input_fields"  required>
			<option value="">All Facilities</option>
			<?php 
			
			?>
		</select>
		
				</div>
				</div>

<br/><br/>



<div class="form-group resonfielddeath" >
				<label class="control-label col-md-3" for="email">Reason for transfer</label>
				<div class="col-md-9">
				<select name="transfer_reason" id="transfer_reason" class="form-control input_fields"  required>
			<option value="">Select Reason</option>
			<?php foreach($TransferRegion as $value) { ?>

			<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>

			<?php } ?>
		</select>
				</div>
				</div> 		
<br/><br/>
<div class="form-group transfer_reason_otherv" style="display: none;">
				<label class="control-label col-md-3" for="email">Reason for transfer Other</label>
				<div class="col-md-9">
				<input  type="text" name="transfer_reason_other" id="transfer_reason_other" class="form-control input_fields"  required>
			
				</div>
				</div> 	

<br/><br/>










          <div class="modal-footer">

            <button type="submit" class="btn btn-success" id="save" name="save" value="save">Save</button>
            <a class="btn btn-success" href="<?php echo base_url(); ?><?php if($cname=='patientinfo'){ ?>patientinfo?p=1 <?php }else{ ?>patientinfo_hep_b?p=1 <?php } ?>">Cancel </a>
          
          </div>
           <?php echo form_close(); ?>
       <!--  </form> -->
      </div>
   
 


					</div>
					
				</div>

			</div>
		</div>


		<script>

	
$("#transfer_reason").change(function(){

	var transfer_reason = $('#transfer_reason').val();
	if(transfer_reason == 99){
		$('.transfer_reason_otherv').show();
		$("#transfer_reason_other").prop('required', true);
	}else{
		$('.transfer_reason_otherv').hide();
		$("#transfer_reason_other").prop('required', false);
	}


  });  

/*$(document).ready(function(){
$("#search_state").trigger('change');

$('[data-toggle="tooltip"]').tooltip();
});*/
$('#search_state').change(function(){
//alert($(this).val());
			$.ajax({
				url : "<?php echo base_url().'users/getDistrictstransfer/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');

				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilitiestransfer/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
					$('#mstfacilitylogin').html(data);
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});

</script>