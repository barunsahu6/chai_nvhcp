<style>
thead
{
	background-color: #085786;
	color: white;
}

.table-bordered>tbody>tr>td.data_td
{
	font-weight: 600;
	font-size: 15px;
	text-align: center;
	vertical-align: middle;
}
</style>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 5 !important;
	width: 100% !important;
	font-size: 14px !important;
	font-family: 'Source Sans Pro';
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
	.col-md-2{
		width: 16.33%;
		padding-left: 8px;
		padding-right: 8px;
	}
	.btn-width{
	width: 12%;
	margin-top: 20px;
}
.pd-15{
	padding-left: 15px;
}
.pb-20{
	padding-bottom: 20px;
}
.container{
	width: 76%;
}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 5 !important;
		width: 100% !important;
		font-size: 14px !important;
		font-family: 'Source Sans Pro';
	}


	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 13px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}

.table-bordered>thead>tr>th{
	vertical-align: middle;
	font-size: 13px;
	font-family: 'Source Sans Pro';
	background-color: #085786;
}
.table-bordered>tbody>tr>td{
	vertical-align: middle;
	font-size: 13px;
	color: #000000;
	font-family: 'Source Sans Pro';
}
</style>
<br>

<?php $loginData = $this->session->userdata('loginData');
//echo "<pre>"; print_r($loginData);

if($loginData->user_type==1) { 
$select = '';
}else{
$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
$select4 = '';	
}
 ?>
<div class="container">
<div class="row">
	
	<div class="col-md-12" style="padding: 0px;">
		<div class="panel panel-default" id="hep_b_progress_report">
					<div class="panel-heading" style="background-color: #333333;padding: 3px 0px;">
					<h4 class="text-center" style="background-color: #333333;color: white;font-family: 'Source Sans Pro';letter-spacing: .75px;">Progress on patients/persons benefitted (HBV)
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/excel_black.png" alt=""></a> -->
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/pdf_color.png" alt=""></a> -->
		</h4>
	</div>

<br>

<?php
           $attributes = array(
              'id' => 'filter_form',
              'name' => 'filter_form',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>
<div class="row pd-15 pb-20">
	<div class="col-md-2 col-sm-12">
		<label for="">State</label>
		<select type="text" name="search_state" id="search_state" class="form-control input_fields" required <?php echo $select2.$select3.$select4; ?>>
			<option value="0">All States</option>
			<?php 
			foreach ($states as $state) {
				?>
				<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo $state->StateName; ?></option>
				<?php 
			}
			?>
		</select>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">District</label>
		<select type="text" name="input_district" id="input_district" class="form-control input_fields"  <?php echo $select.$select2.$select4; ?>>
			<option value="">Select District</option>
			<?php 
			
			?>
		</select>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">Facility</label>
		<select type="text" name="facility" id="mstfacilitylogin" class="form-control input_fields"  <?php echo $select.$select2; ?>>
			<option value="">All Facilities</option>
			<?php 
			
			?>
		</select>
	</div>


	<div class="col-md-2 col-sm-12">
		<label for="">From Date</label>
		<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')) { echo $this->input->post('startdate'); }else{ echo timeStampShow(date('Y-01-01')); } ?>" required>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">To date</label>
		<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')) { echo $this->input->post('enddate'); }else{  echo timeStampShow(date('Y-m-d'));} ?>" required>
	</div>
<div class="col-lg-2 col-md-2 btn-width">
					<button type="submit" class="btn btn-block btn-success"  style="line-height: 1.2; font-weight: 600;">SEARCH</button>
				</div><span style="margin-top: 25px;margin-right: 25px; float: right;"><a href="javascript:void(0)"><i class="fa fa-2x fa-download" id="excel_button" onclick="tableToExcel('testTable', 'W3C Example Table','hep_b_progress.xls')" title="Excel Download"></i></a></span>
			</div>
 <?php echo form_close(); ?>
</div>
</div>
<div class="row">
	<div class="col-sm-12 col-md-12">	
		<table class="table table-bordered table-highlighted" id="testTable">

			<thead>
				<th class="text-center">Name of state/UT</th>
			<th class="text-center thclass" > Total&nbsp;number&nbsp;of persons screened for Hepatitis B </th>
				<th class="text-center thclass">Total&nbsp;number&nbsp;of persons found positive for Hepatitis B </th>
				<th class="text-center thclass" style="margin-top: 10px;">Total&nbsp;number&nbsp;of patients found eligible for treatment <br/></th>
				<th class="text-center thclass" style="margin-top: 10px;">Total&nbsp;number&nbsp;of  patients put on treatment<br/></th>
				<th colspan="4" class="text-center thclass">Immunization of health care workers (HCWs) </th>
				<th class="text-center thclass" style="margin-top: 10px;">Total no. of institutional deliveries<br/></th>
				<th class="text-center thclass" style="margin-top: 10px;">  No. of newborns received Hepatitis B birthdose<br/></th>

			</thead>
			<thead>

				<th colspan="5" style="background-color: #085786;color: white; " ></th>
				<th style="background-color: #085786;color: white; width: 150px !important;" class="text-center">Total no. of HCWs targetted</th>
				<th style="background-color: #085786;color: white; width: 150px !important;" class="text-center">HCWs given 1st dose</th>
				<th style="background-color: #085786;color: white; width: 150px !important;" class="text-center">HCWs given 2nd dose</th>
				<th style="background-color: #085786;color: white; width: 150px !important;" class="text-center">HCWs given 3rd dose</th>
				<th colspan="2" style="background-color: #085786;color: white; " ></th>
				
			</thead>
			<tbody>
				<?php $total_1 = 0;
					$total_2 = 0;
					$total_3 = 0;
					$total_4 = 0;
					$total_5=0;
					$total_6 = 0;
					$total_7 = 0;
					 if($hepatitis_b_screened){  foreach ($hepatitis_b_screened as  $value) { 
					 	$total_1+=$value->anti_hbs_screened;
					 	$total_2+=$value->anti_hbs_positive;
					 	$total_6+=$value->InitiatedOnTreatmentHEPB;
					 	$total_7+=$value->ContinuedOnTreatment;
					 	$total_3+=$value->dos1;
					 	$total_4+=$value->dos2;
					 	$total_5+=$value->dos3;
					
				}}	?>

				<tr style="background-color: #d5e4e6;color: black;">

					<td class="thclass text-center"><?php echo 'India'; ?></td>
					<td class="text-center data_td"><?php echo ($total_1); ?> </td>
				<td class="text-center data_td"><?php echo ($total_2); ?> </td>
					<td class="text-center data_td"><?php echo ($total_6); ?> </td>
					<td class="text-center data_td"><?php echo ($total_7); ?> </td>
					<td class="text-center data_td"><?php echo '-'; ?> </td>
				<td class="text-center data_td"><?php echo ($total_3); ?> </td>
				<td class="text-center data_td"><?php echo ($total_4); ?> </td>
				<td class="text-center data_td"><?php echo ($total_5); ?> </td>
					<td class="text-center data_td"><?php echo '-'; ?> </td>
					<td class=" text-center data_td"><?php echo '-'; ?> </td>		
				</tr>
					<?php if($hepatitis_b_screened){  foreach ($hepatitis_b_screened as  $value) { 	?>
				<tr>

					<td ><?php echo $value->StateName; ?></td>
					<td class="text-center data_td"><?php echo empty($value->anti_hbs_screened) ? '-' : $value->anti_hbs_screened; ?> </td>
				<td class="data_td text-center"><?php echo empty($value->anti_hbs_positive) ? '-' : $value->anti_hbs_positive; ?> </td>
					<td class=" text-center data_td"><?php echo empty($value->InitiatedOnTreatmentHEPB) ? '-' : $value->InitiatedOnTreatmentHEPB; ?>  </td>
					<td class=" text-center data_td"><?php echo empty($value->ContinuedOnTreatment) ? '-' : $value->ContinuedOnTreatment; ?>  </td>
					<td class=" text-center data_td"><?php echo '-'; ?> </td>
				<td class=" text-center data_td"><?php echo empty($value->dos1) ? '-' : $value->dos1; ?> </td>
				<td class=" text-center data_td"><?php echo empty($value->dos2) ? '-' : $value->dos2; ?> </td>
				<td class=" text-center data_td"><?php echo empty($value->dos3) ? '-' : $value->dos3; ?> </td>
					<td class=" text-center data_td"><?php echo '-'; ?> </td>
					<td class=" text-center data_td"><?php echo '-'; ?> </td>		
					
				</tr>

<?php } } ?>



				
			</tbody>
		
			
		</table>
	</div>
</div>
</div>
</div>
<br>
<br>
<br>
<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name,filename) {
    if (!table.nodeType) table = document.getElementById(table)
     var newtable=document.createElement("table");
            var newtable=document.createElement("table");
            var tbody =document.createElement("tbody");
             var row = document.createElement("tr");
             row.setAttribute("style", "background-color: black;color: white; font-family:Calibri;");
  				var cell1 = document.createElement("td");
  				cell1.colSpan=7;
  				cell1.innerHTML ="Data as on : - <?php echo date('jS \ F Y'); ?>";
  				row.appendChild(cell1);
 			 	tbody.appendChild(row);
 			 	newtable.appendChild(tbody);
             var clonedTable=newtable.cloneNode(true);
             var clonedTable1=table.cloneNode(true);
            clonedTable.appendChild(clonedTable1);
    var ctx = {worksheet: name || 'Worksheet', table: clonedTable.innerHTML}
     var link = document.createElement('a');
    link.download = filename;
    link.href = uri + base64(format(template, ctx));
    link.click();
     clonedTable.remove();
  }
})()
</script>
<script>
	
$(document).ready(function(){
$("#search_state").trigger('change');

$('[data-toggle="tooltip"]').tooltip();
});
$('#search_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');

				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
					$('#mstfacilitylogin').html(data);
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});

</script>