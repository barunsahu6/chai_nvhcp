<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}
	
textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #A30A0C;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

/*#table_patient_list tbody tr:hover
{
	cursor: pointer;
}
*/
.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #A30A0C !important;
	background-color: white !important;
	border : 1px solid #A30A0C;
}
.auto {cursor: auto;}
.fa-circle {
  color: white;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}

 .badge {
  position: absolute;
  top: 1px;
  right: -12px;
  padding: 5px 10px;
  border-radius: 50%;
  background-color: red;
  color: white;
}
.btn-success .badge {
    color: #101010;
    background-color: #fff;
}
</style>
 
<br/>
<br/>
<br/>
<?php $loginData = $this->session->userdata('loginData'); 


if($loginData->user_type==1) { 
$select = '';
}else{
$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
$select4 = '';	
}
 

$LookupCodeval = '3,4,5,6,7,8,9,10,11,17,18,19,20,21,22,23,24,25,26,27,28';
$query_params = [];
//echo "<pre>";print_r($loginData);
    if( ($loginData) && $loginData->user_type == '1' ){
        $sess_where = " 1";
      }
    elseif( ($loginData) && $loginData->user_type == '2' ){

        $sess_where = " Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
      }
    elseif( ($loginData) && $loginData->user_type == '3' ){ 
        $sess_where = " Session_StateID = '".$loginData->State_ID."'";
      }
    elseif( ($loginData) && $loginData->user_type == '4' ){ 
        $sess_where = " Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
      }

 $sql11 = "SELECT count(*) as count,p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID,p.TransferFromFacility,LookupCode,Mobile,d.DistrictName,p.Next_Visitdt,p.T_Initiation,p.Current_Visitdt,u.Contacted,u.ContactedRemark,u.CreatedOn from tblpatient p left join mstdistrict d on d.id_mstdistrict=p.District left join (SELECT * FROM ltfu_contact where Contacted=1) u on u.PatientGUID=p.PatientGUID left join (SELECT * FROM mstlookup where Flag =13 and LanguageID = 1) b on p.status = b.LookupCode WHERE   ".$sess_where." and HepC=1 and  T_Initiation IS not NULL and status in($LookupCodeval)  AND  u.PatientGUID is NULL AND DATEDIFF (NOW(),Next_Visitdt ) >=7";
     $patient_listcount = $this->db->query($sql11, $query_params)->result();

 $patient_listCount = $patient_listcount[0]->count;


 /*hepb*/

 $LookupCodevalhbv = '3,4,5,6,7,8,9,10,11,17,18,19,20,21,22,23,24,25,26,27,28';
$query_paramshbv = [];
//echo "<pre>";print_r($loginData);
		if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND p.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

$offset=0;
		/*Pahnation start*/
    $sql11b = "SELECT count(*) as count,p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID,p.TransferFromFacility,LookupCode,Mobile,d.DistrictName,dis.NextVisit as Next_Visitdt,dis.Treatment_Dt as T_Initiation,dis.Treatment_Dt as Current_Visitdt,u.Contacted,u.ContactedRemark,u.CreatedOn from tblpatient p inner join hepb_tblpatient hp on hp.PatientGUID=p.PatientGUID left join mstdistrict d on d.id_mstdistrict=p.District left join (SELECT * FROM ltfu_contact where Contacted=1) u on u.PatientGUID=p.PatientGUID LEFT JOIN (select * from tblpatientdispensationb  group by PatientGUID order by PatientID DESC) dis on hp.PatientGUID=dis.PatientGUID  left join (SELECT * FROM mstlookup where Flag =72 AND LanguageID = 1) b on hp.status = b.LookupCode WHERE  ".$sess_where." AND HbsAg = 1 AND dis.Treatment_Dt IS not NULL and DATEDIFF (NOW(),NextVisit ) >=7 AND  u.PatientGUID is NULL  AND (AntiHCV is null  OR (AntiHCV=1) ) and hp.Status=6";
		 $patient_listcounthbv = $this->db->query($sql11b, $query_paramshbv)->result();

$patient_listcounthbv = $patient_listcounthbv[0]->count;

//echo '<pre>'; print_r($loginData);exit();
  if(!empty($_GET['p'])){  if($_GET['p']==1) { ?>

<?php  } } ?>


<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="btn-group">


						<a class="btn btn-success" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>ltfuinfo/">LTFU Patient List HEP-C <span class="badge"><?php if($patient_listCount!=0){ echo $patient_listCount;}else { echo '';} ?></span> </a>

						<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>ltfuinfo/ltfuhbv">LTFU Patient List HEP-B <span class="badge"><?php if($patient_listcounthbv!=0){ echo $patient_listcounthbv;}else { echo '';} ?></span></a>


</div>
</div>
</div>	

<!-- <form method="POST" name="search_form"> -->
				<?php
           $attributes = array(
              'name' => 'search_form',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>
<div class="row">
           <div class="col-md-10 col-md-offset-1">
		<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 6px; padding-bottom: 6px;">LTFU Patient List HEP-C
		</h3>
	</div>
</div>
<div class="row">

	

	<div class="col-md-3 col-md-offset-1">
		
		<label for="">Search By</label>
		<select type="text" name="search_by" id="search_by" class="form-control input_fields" required>
			<option value="">Select</option>
			<option value="1" <?php if($this->input->post('search_by')==1) { echo 'selected';} ?>>UID/Contact No.</option>
			<option value="2" <?php if($this->input->post('search_by')==2) { echo 'selected';} ?>>Contacted</option>
			<option value="3" <?php if($this->input->post('search_by')==3) { echo 'selected';} ?>>Name</option>
		</select>
	</div>
	<div class="col-md-4 uid_field" style="display: none;">
		
		<label for="uid">UID/Contact No.</label>
		<input type="text" class="form-control input_fields" name="uid_contact" id="uid_contact" value="<?php if(isset($_POST['uid_contact'])) { echo $_POST['uid_contact'];} ?>" >
	</div>

	<div class="col-md-4 patname_field" style="display: none;">
		
		<label for="uid">Patient Name</label>
		<input type="text" class="form-control messagedata input_fields" name="pat_name" id="pat_name" maxlength="50" value="<?php if(isset($_POST['pat_name'])) { echo $_POST['pat_name'];} ?>"  >
	</div>

	<div class="col-md-4 status_field" style="display: none;">
		
		<label for="uid">Contacted?</label>
		<select type="text" name="search_status" id="search_status" class="form-control input_fields" style="font-size: 14px !important;">
			<option value="">Select Contacted?</option>
			<option value="1" <?php echo (count($this->input->post('search_status')) > 0 && $this->input->post('search_status') == 1)?'selected':''; ?>>Yes</option>
			<option value="2" <?php echo (count($this->input->post('search_status')) > 0 && $this->input->post('search_status') == 2)?'selected':''; ?>>No</option>
			<!-- <?php foreach ($status as $row) { ?>
				<option value="<?php echo $row->LookupCode; ?>" <?php if($row->LookupCode==$this->input->post('search_status')) { echo 'selected';} ?>><?php echo ucwords($row->LookupValue); ?></option>
			<?php } ?> -->
		</select>
	</div>
	<div class="col-md-1" style="padding-left: 0;">
		
		<label for="">&nbsp;</label>
		<button class="btn btn-block btn-success text-center" style="font-weight: 600; line-height: inherit; border-radius: 0;">SEARCH</button>
	</div>
</div>
		          <?php echo form_close(); ?>
<!-- </form> -->
<br>

<!-- <div class="row">
	<div class="col-md-6 col-md-offset-3 text-center">
		<h3>Patient List</h3>


	</div>
</div> -->

	<div class="row">
<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){  ?>
			

			<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				
				<?php } ?>
				</div>

<div class="row">
	<div class="col-md-10 col-md-offset-1">
		
		 <div class="text-right"><?php if(isset($offsetdata)) { echo ($offsetdata+1).'-'. (count($patient_list)+$offsetdata); } else { echo '0';} ?> <label for="total"> Of  <?php  if (isset($patient_listCounthcv) ) echo ($patient_listCounthcv); ?> records.</label></div>
		<table class="table table-striped table-bordered table-hover" id="table_patient_list" style="width:100%">

			
			<thead>
				<tr>
					<th>UID</th>
					<th>Name</th>
					<th>Contact no.</th>
					<th>District of the patient</th>
					<th>Last Visit Date</th>
					<th>Next Visit Date</th>
					<th>Last Status</th>
					<th>Contacted?</th>
					<th>Contact date</th>
					<th>Remarks</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(count($patient_list) > 0)
				{
					//echo "<pre>";
					//pr($patient_list);exit;

					foreach ($patient_list as $patient) {
						?>
						<tr>
							<td style="width: 100px;"><?php //echo str_pad($patient->UID_Num, 6, '0', STR_PAD_LEFT);  
							echo $patient->UID_Prefix.'-'.str_pad($patient->UID_Num, 6, '0', STR_PAD_LEFT); ?></td>
							<td ><?php echo ucwords($patient->FirstName); ?></td>
							<td><?php echo ($patient->Mobile); ?></td>
							<td style="width: 10px;"><?php echo ucwords($patient->DistrictName); ?></td>
							<td><?php   if($patient->Current_Visitdt=='0000-00-00'){ echo timeStampShow($patient->T_Initiation);} else{ echo timeStampShow($patient->Current_Visitdt);} ?></td>
							<td><?php echo timeStampShow($patient->Next_Visitdt); ?></td>
							<td><?php echo ($patient->status); ?></td>
							<td style="width: 6px;" ><input type="radio" name="Contacted_<?php echo $patient->PatientGUID; ?>" value="1" <?php echo (count($patient) > 0 && $patient->Contacted == 1)?'checked':''; ?>>Yes <br/><input type="radio" name="Contacted_<?php echo $patient->PatientGUID; ?>" value="2" <?php echo (count($patient) > 0 && $patient->Contacted == 2)?'checked':''; ?>>No</a></td>
							<td><?php echo timeStampShow($patient->CreatedOn); ?></td>
							<td align="center"><textarea style="border: none; border-color: transparent;" name="contactremarks" id="contactremarks_<?php echo $patient->PatientGUID; ?>"  rows="4" cols="20"><?php echo (count($patient) > 0)?ucwords(strtolower($patient->ContactedRemark)):''; ?></textarea></td>

							<td align="center"><button name="save" id="<?php echo $patient->PatientGUID; ?>" class="psave btn btn-success" title="Save"><i class="fa fa-floppy-o" aria-hidden="true" style="font-size: 12px;"></button></i>
</td>
						</tr>
						<?php 
					}
				}

				else
				{
					?>
					<?php 	if($this->input->post('search_by')==1) { ?>
					<tr data-href="#">
						<td class="text-center auto" colspan=11 >No Patients in this UID/Contact No.</td>
					</tr>
				<?php } elseif($this->input->post('search_by')==2){ ?>
					<tr data-href="#">
						<td class="text-center auto" colspan=11>No Patients in this Contacted.</td>
					</tr>
				<?php }else{ ?>
						<tr data-href="#">
						<td class="text-center auto" colspan=11>No Patients in this Name.</td>
					</tr>
					<?php 
				} }
				?>
			</tbody>
		</table>

<div class="text-left">
Showing <?php if (isset($patient_list) && is_array($patient_list)) echo count($patient_list); ?> results
</div> 

<div class="text-right">
<?php if (isset($patient_list) && count($patient_list) >0 && $patient_list!='' && is_array($patient_list)) echo $page_links; ?>  
</div> 

		
	</div>
</div>
<br/><br/>


  


<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>


<script type="text/javascript" src="<?php echo site_url('application');?>/third_party/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo site_url('application');?>/third_party/js/jquery.dataTables.min.js"></script>


<script>

	
	$('.messagedata').keypress(function (e) {
        var regex = new RegExp(/^[a-zA-Z\s]+$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            return false;
        }
    });

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
$("#search_state").trigger('change');


});

$('.psave').click(function(){
	var patid = $(this).attr("id");
//var Contacted =  $('input[name=Contacted_"+patid]:checked').val();

var Contacted = $('input[name=Contacted_' + patid + ']:radio:checked').val();
var contactremarks = $('#contactremarks_'+patid).val();

		
		
		if(Contacted==null){

			$("#modal_header").html('Please check Contacted Yes or No');
			$("#modal_text").html('Please check radio button yes or no');
			$("#multipurpose_modal").modal("show");
		return false;
		}
		if($('#contactremarks_'+patid).val().trim()==""){
			$("#modal_header").html('Please enter Remark');
			$("#modal_text").html('Please enter Remark');
			$("#multipurpose_modal").modal("show");
		return false;
		}


		var dataString = {
		"patid": patid,
		"Contacted": Contacted,
		"contactremarks": contactremarks
		};

 			$.ajax({				    	
			url: '<?php echo base_url(); ?>Ltfuinfo/ltfu_process/'+patid,
			data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
			type: 'GET',
			data: dataString,
			dataType: 'json',

			success: function (data) { 
				
			if(data['status'] == 'true'){
				
				//alert('Data has been successfully submitted');
				$("#modal_header").html('Data has been successfully submitted');
				$("#modal_text").html('LTFU Data has been successfully submitted');
				$("#multipurpose_modal").modal("show");
				setTimeout(function() {
    					location.href='<?php echo base_url(); ?>Ltfuinfo';
				}, 1000);

				}
				}        
				}); 



});


$('#search_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'POST',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');

				},
				error : function(error)
				{
					//alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
				
				method : 'POST',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
					$('#mstfacilitylogin').html(data);
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});

		/*$(document).ready(function() {
		$('#table_patient_list').DataTable({
		"pageLength":50
		});
		} );*/

	$(document).ready(function(){

		$(".uid_field").hide();
		$(".status_field").hide();
		$(".patname_field").hide();

		/*$('#table_patient_list tbody tr').click(function(){
			window.location = $(this).data('href');
		});*/

	<?php 	if($this->input->post('search_by')==1) { ?>
			$(".uid_field").show();
				$(".status_field").hide();
				$(".patname_field").hide();
				$("#search_by").prop('required', true);
				$("#uid_contact").prop('required', true);
				$("#search_status").prop('required', false);
				$("#pat_name").prop('required', false)
				$("#search_status").val('');

		<?php	}elseif($this->input->post('search_by')==2){ ?>

				$(".uid_field").hide();
				$(".status_field").show();
				$(".patname_field").hide();
				$("#search_by").prop('required', false);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', true);
				$("#pat_name").prop('required', false)
				$("#uid_contact").val('');
		<?php	}elseif($this->input->post('search_by')==3){ ?>
				//alert('eeeeee');
				$(".uid_field").hide();
				$(".status_field").hide();
				$(".patname_field").show();
				$("#pat_name").prop('required', true)
				$("#search_by").prop('required', false);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', false);
				$("#uid_contact").val('');
		<?php	}else {  ?>


				$(".uid_field").hide();
				$(".status_field").hide();
				$("#search_by").prop('required', true);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', false);
				$("#pat_name").prop('required', false)

		<?php } ?>

		$("#search_by").change(function(){

			if($(this).val() == 1)
			{
				$(".uid_field").show();
				$(".status_field").hide();
				$(".patname_field").hide();
				$("#search_by").prop('required', true);
				$("#uid_contact").prop('required', true);
				$("#search_status").prop('required', false);
				$("#pat_name").prop('required', false)
				$("#search_status").val('');
				$("#pat_name").val('');
			}
			else if($(this).val() == 2)
			{
				$(".uid_field").hide();
				$(".status_field").show();
				$(".patname_field").hide();
				$("#search_by").prop('required', false);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', true);
				$("#pat_name").prop('required', false)
				$("#uid_contact").val('');
				$("#pat_name").val('');
			}
			else if($(this).val() == 3)
			{
				$(".uid_field").hide();
				$(".status_field").hide();
				$(".patname_field").show();
				$("#pat_name").prop('required', true)
				$("#search_by").prop('required', false);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', false);
				$("#uid_contact").val('');
				$("#search_status").val('');
			}
			else
			{
				$(".uid_field").hide();
				$(".status_field").hide();
				$("#search_by").prop('required', true);
				$("#uid_contact").prop('required', false);
				$("#search_status").prop('required', false);
				$("#pat_name").prop('required', false)
			}
		});
	});


	
</script>