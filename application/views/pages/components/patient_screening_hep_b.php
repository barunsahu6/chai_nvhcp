<?php 
$loginData = $this->session->userdata('loginData');

$sql = "SELECT Screening FROM `MSTRole` where RoleId = ".$loginData->RoleId;
$result = $this->db->query($sql)->result();
?>

<style>
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}

	.input_fields
	{
		height: 30px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	textarea
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}

	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}

	.form_buttons:focus
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}

	@media (min-width: 768px) {
		.row.equal {
			display: flex;
			flex-wrap: wrap;
		}
	}

	@media (max-width: 768px) {

		.input_fields
		{
			height: 40px !important;
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
		}

		.form_buttons
		{
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
			font-weight: 600 !important;
			padding: 8px 12px !important;
			border: 2px solid #A30A0C !important;

		}
		.form_buttons:hover
		{
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
			font-weight: 600 !important;
			padding: 8px 12px !important;
			border: 2px solid #A30A0C !important;
			background-color: #FFF;
			color: #A30A0C;

		}
	}

	.btn-default {
		color: #333 !important;
		background-color: #fff !important;
		border-color: #ccc !important;
	}
	.btn {
		display: inline-block;
		padding: 6px 12px;
		margin-bottom: 0;
		font-size: 14px;
		font-weight: 400;
		line-height: 2.4;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		-ms-touch-action: manipulation;
		touch-action: manipulation;
		cursor: pointer;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		background-image: none;
		border: 1px solid transparent;
		border-radius: 4px;
	}

	a.btn
	{
		text-decoration: none;
		color : #000;
		background-color: #A30A0C;
	}

	.btn-group .btn:hover
	{
		text-decoration: none !important;
		color: #000 !important;
		background-color: #CCC !important;
	}

	.btn-group .btn:hover
	{
		text-decoration: none !important;
		color: #000 !important;
		background-color: inherit !important;
	}

	a.active
	{
		color : #FFF !important;
		text-decoration: none;
		background-color: inherit !important;
	}

	#table_patient_list tbody tr:hover
	{
		cursor: pointer;
	}

	.btn-success
	{
		background-color: #A30A0C;
		color: #FFF !important;
		border : 1px solid #A30A0C;
	}

	.btn-success:hover
	{
		text-decoration: none !important;
		color: #A30A0C !important;
		background-color: white !important;
		border : 1px solid #A30A0C;
	}


	input[type=date]::-webkit-inner-spin-button {
		-webkit-appearance: none;
		display: none;
	}
	/*updated:6/6/19*/


</style>


<style>
	select[readonly] {
		background: #eee;
		pointer-events: none;
		touch-action: none;
	}

</style>
<br>
<div class="row equal">
	<!-- <form action="" name="patient_form" id="patient_form" method="POST"> -->
		<?php
		$attributes = array(
			'id' => 'patient_form',
			'name' => 'patient_form',
			'autocomplete' => 'off',
		);
		echo form_open('', $attributes); ?>

		<input type="hidden" name="fetch_uid" id="fetch_uid">
		<?php echo form_close(); ?>
		<!-- <input type="hidden" name="fetch_uid" id="fetch_uid"> -->
		<!-- </form> -->
		<div class="col-lg-10 col-lg-offset-1">

			<div class="row">
				<div class="col-md-12 text-center">
					<div class="btn-group">
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">1. Registration</a>
						<a class="btn btn-success" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">2. Screening</a>

						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">3. Testing</a>

						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">4. HBV DNA</a>

						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/known_history/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">5. Known History</a>
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">6. Prescription</a>
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">7. Dispensation</a>
						
					</div>
				</div>
			</div>

			<!-- <form action="" method="POST" name="screening" id="screening"> -->
				<?php
				$attributes = array(
					'id' => 'screening',
					'name' => 'screening',
					'autocomplete' => 'off',
				);
				echo form_open('', $attributes); ?>
				<div class="row">
					<div class="col-md-12">
						<h4 class="text-center"><p>Patient Name - <strong><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FirstName)):''; ?></strong>  (<?php echo (count($patient_data) > 0)?$patient_data[0]->UID_Prefix:$uid_prefix; echo '-' .str_pad($patient_data[0]->UID_Num, 6, '0', STR_PAD_LEFT); ?>)<p> 
							<?php if (count($patient_data) > 0 && $patient_data[0]->AntiHCV == 1){ ?>
								<p class="text-right"> <a target="_blanck"  href="<?php echo base_url().'printdiv/index/'.$patient_data[0]->PatientGUID; ?>"><i class="fa fa-print" aria-hidden="true"></i></a></p>
							<?php } ?>

						</h4>
						<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Patient Screening Module  </h3>

					</div>
				</div>
				<br>

				<div class="row">
					<div class="col-md-2">
						<h4><b>TEST TYPES</b></h4>
					</div>
					<div class="col-md-2">
						<label class="checkbox-inline"><input type="checkbox" value="1" data-test="hav" class="virus_checkboxes" name="check_hav" id="check_hav" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->LgmAntiHAV == 1)?"checked":''; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">lgM Anti HAV</b></label>
					</div>
					<div class="col-md-2">
						<label class="checkbox-inline"><input type="checkbox" value="1" data-test="hbs" class="virus_checkboxes" name="check_hbs" id="check_hbs" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->HbsAg == 1)?"checked":''; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">HBsAg</b></label>
					</div>
				<!-- <div class="col-md-2">
					<label class="checkbox-inline"><input type="checkbox" value="1" data-test="hbc" class="virus_checkboxes" name="check_hbc" id="check_hbc" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->LgmAntiHBC == 1)?"checked":''; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">lgM Anti HBc</b></label>
				</div> -->
				<div class="col-md-2">
					<label class="checkbox-inline"><input type="checkbox" value="1" data-test="hcv" class="virus_checkboxes" name="check_hcv" id="check_hcv" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->AntiHCV == 1)?"checked":''; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Anti HCV</b></label>
				</div>
				<div class="col-md-2">
					<label class="checkbox-inline"><input type="checkbox" value="1" data-test="hev" class="virus_checkboxes" name="check_hev" id="check_hev" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->LgmAntiHEV == 1)?"checked":''; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">lgM Anti HEV</b></label>
				</div>
			</div>
			<hr>
			
			<!-- hav_fields -->

			<div class="row hav_fields">
				<div class="col-md-12">
					<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">Screening Details - lgM Anti HAV Testing</h4>
				</div>
			</div>
			<br class="hav_fields">
			<div class="hav_fields">
				<div class="row">
					<div class="col-md-3 col-md-offset-1">
						<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hav_rapid" name="hav_rapid" id="hav_rapid" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Rapid Diagnostic Test</b></label>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 col-md-offset-2">
						<label for="" id="hav_rapid_date_label">Date <span class="hav_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?"*":""; ?></span></label>
						<input type="text"  name="hav_rapid_date" id="hav_rapid_date" class="input_fields form-control hav_rapid_fields hasCal dateInpt input2"  <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?timeStampShow($patient_data[0]->HAVRapidDate):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>"  >
						<br class="hidden-lg-*">
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
						<label for="" id="hav_rapid_result_label">Result <span class="hav_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?"*":""; ?></span></label>
						<select name="hav_rapid_result" id="hav_rapid_result" class="form-control input_fields hav_rapid_fields"  <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?"required":"disabled"; ?>  >
							<option value="">Select</option>
							<?php foreach ($result_options as $row) {?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1 && $patient_data[0]->HAVRapidResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
							<?php } ?>
						</select>
						<br class="hidden-lg-*">
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
						<label for="" id="hav_rapid_place_of_test_label">Place Of Testing <span class="hav_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?"*":""; ?></span></label>
						<select class="form-control input_fields hav_rapid_fields place_of_testing" data-test="hav_rapid" id="hav_rapid_place_of_test" name="hav_rapid_place_of_test"  <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?"required":"disabled"; ?>>
							<option value="">Select</option>
							<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1 && $patient_data[0]->HAVRapidPlace == 1)?'selected':''; ?>>Govt. Lab</option>
							<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1 && $patient_data[0]->HAVRapidPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
						</select>
					</div>
				</div>
				<div class="row hav_rapid_other_lab_name govt_lab hav_rapid_govt_lab">
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
						<label for="" id="hav_rapid_lab_name_label">Lab Name <span class="hav_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?"*":""; ?></span></label>
						<select type="text" name="hav_rapid_lab_name" id="hav_rapid_lab_name" class="form-control input_fields hav_rapid_fields lab_names" data-test="hav_rapid"  <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1 && $patient_data[0]->HAVRapidPlace == 1)?"required":"disabled"; ?>>
							<option value="">Select</option>
							<?php 
							if($patient_data[0]->HAVRapidLabID!=""){
								foreach ($facility as $row) {
									?>
									<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1 && $patient_data[0]->HAVRapidLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
									<?php 
								}
							}else{

								foreach ($facility as $facilitiesdata) {
									?>
									<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
									<?php 
								}

							}
							?>

						</select>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 lab_name_others hav_rapid_lab_name_other">
						<label for="" id="hav_rapid_lab_name_other_label">Lab Name Other <span class="hav_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?"*":""; ?></span></label>
						<input type="text" name="hav_rapid_lab_name_other" id="hav_rapid_lab_name_other" class="input_fields form-control hav_rapid_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1 && $patient_data[0]->HAVRapidLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HAVRapid == 1)?$patient_data[0]->HAVRapidLabOther:''; ?>">
						<br class="hidden-lg-*">
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3 col-md-offset-1">
						<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hav_elisa" name="hav_elisa" id="hav_elisa" style="width: 20px; height: 20px;"<?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">ELISA Test</b></label>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 col-md-offset-2">
						<label for="">Date <span class="hav_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?"*":""; ?></span></label> 
						<input type="text" name="hav_elisa_date" id="hav_elisa_date" class="input_fields form-control hav_elisa_fields hasCal dateInpt input2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?timeStampShow($patient_data[0]->HAVElisaDate):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" >
						<br class="hidden-lg-*">
					</div>
					<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
						<label for="">Result <span class="hav_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?"*":""; ?></span></label> 
						<select name="hav_elisa_result" id="hav_elisa_result" class="form-control input_fields hav_elisa_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?"required":"disabled"; ?>>
							<option value="">Select</option>
							<?php foreach ($result_options as $row) {?>
								<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1 && $patient_data[0]->HAVElisaResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
							<?php } ?>
						</select>
						<br class="hidden-lg-*">
					</div>
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
						<label for="">Place Of Testing <span class="hav_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?"*":""; ?></span></label> 
						<select class="form-control input_fields hav_elisa_fields place_of_testing" data-test="hav_elisa" id="hav_elisa_place_of_test" name="hav_elisa_place_of_test" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?"required":"disabled"; ?>>
							<option value="">Select</option>
							<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1 && $patient_data[0]->HAVElisaPlace == 1)?'selected':''; ?>>Govt. Lab</option>
							<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1 && $patient_data[0]->HAVElisaPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
						</select>
					</div>
				</div>
				<div class="row hav_elisa_other_lab_name govt_lab hav_elisa_govt_lab">
					<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
						<label for="">Lab Name <span class="hav_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?"*":""; ?></span></label>
						<select type="text" name="hav_elisa_lab_name" id="hav_elisa_lab_name" class="form-control input_fields hav_elisa_fields lab_names" data-test="hav_elisa" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1 && $patient_data[0]->HAVElisaPlace == 1)?"required":"disabled"; ?>>
							<option value="">Select</option>
							<?php 
							if($patient_data[0]->HAVRapidLabID!=""){
								foreach ($facility as $row) {
									?>
									<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1 && $patient_data[0]->HAVElisaLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
									<?php 
								} } else{

									foreach ($facility as $facilitiesdata) {
										?>
										<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
										<?php 
									}

								}
								?>
							</select>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hav_elisa_lab_name_other lab_name_others hav_elisa_lab_name_other">
							<label for="">Lab Name Other <span class="hav_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?"*":""; ?></span></label>
							<input type="text" name="hav_elisa_lab_name_other" id="hav_elisa_lab_name_other" class="input_fields form-control hav_elisa_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1 && $patient_data[0]->HAVElisaLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HAVElisa == 1)?$patient_data[0]->HAVElisaLabOther:''; ?>">
							<br class="hidden-lg-*">
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3 col-md-offset-1">
							<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hav_other" name="hav_other" id="hav_other" style="width: 20px; height: 20px;"<?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Other Test</b></label>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
							<label for="">Test Name <span class="hav_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?"*":""; ?></span></label>
							<input autocomplete="anyrandomthing" type="text" name="hav_other_test_name" id="hav_other_test_name" class="input_fields form-control hav_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?$patient_data[0]->HAVOtherName:''; ?>">
							<br class="hidden-lg-*">
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
							<label for="">Date <span class="hav_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?"*":""; ?></span></label>
							<input type="text" name="hav_other_date" id="hav_other_date" class="input_fields form-control hav_other_fields hasCal dateInpt input2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?timeStampShow($patient_data[0]->HAVOtherDate):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" >
							<br class="hidden-lg-*">
						</div>
						<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
							<label for="">Result <span class="hav_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?"*":""; ?></span></label>
							<select name="hav_other_result" id="hav_other_result" class="form-control input_fields hav_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?"required":"disabled"; ?>>
								<option value="">Select</option>
								<?php foreach ($result_options as $row) {?>
									<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1 && $patient_data[0]->HAVOtherResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
								<?php } ?>
							</select>
							<br class="hidden-lg-*">
						</div>
						<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
							<label for="">Place Of Testing <span class="hav_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?"*":""; ?></span></label>
							<select class="form-control input_fields hav_other_fields place_of_testing" data-test="hav_other" id="hav_other_place_of_test" name="hav_other_place_of_test" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?"required":"disabled"; ?>>
								<option value="">Select</option>
								<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1 && $patient_data[0]->HAVOtherPlace == 1)?'selected':''; ?>>Govt. Lab</option>
								<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1 && $patient_data[0]->HAVOtherPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
							</select>
						</div>
					</div>
					<div class="row hav_other_other_lab_name govt_lab hav_other_govt_lab">
						<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
							<label for="">Lab Name <span class="hav_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?"*":""; ?></span></label>
							<select type="text" name="hav_other_lab_name" id="hav_other_lab_name" class="form-control input_fields hav_other_fields lab_names" data-test="hav_other" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1 && $patient_data[0]->HAVOtherPlace == 1)?"required":"disabled"; ?>>
								<option value="">Select</option>
								<?php 
								if($patient_data[0]->HAVRapidLabID!=""){
									foreach ($facility as $row) {
										?>
										<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1 && $patient_data[0]->HAVOtherLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
										<?php 
									} } else{

										foreach ($facility as $facilitiesdata) {
											?>
											<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
											<?php 
										}

									}
									?>
								</select>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hav_other_lab_name_other hav_other_lab_name_other lab_name_others">
								<label for="">Lab Name Other <span class="hav_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?"*":""; ?></span></label>
								<input type="text" name="hav_other_lab_name_other" id="hav_other_lab_name_other" class="input_fields form-control hav_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1 && $patient_data[0]->HAVOtherLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HAVOther == 1)?$patient_data[0]->HAVLabOther:''; ?>">
								<br class="hidden-lg-*">
							</div>
						</div>
						<br>
						<br>
						<!-- Start new section-->
						<div class="row hideshowposHAV">
							<div class="col-md-2">
								<h4><b></b></h4>
							</div>

							<div class="col-md-4">
								<label class="radio-inline"><input type="radio" value="1"  class="hav_rapid_fieldshav Refer_FacilityHAV" name="Refer_FacilityHAV" id="Refer_FacilityHAV" <?php echo (count($patient_data) > 0 && $patient_data[0]->Refer_FacilityHAV == 1)?"checked":''; ?> style="width: 20px; height: 20px;"><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Patient managed at the facility
								</b></label>
							</div>


							<div class="col-md-4">
								<label class="radio-inline"><input type="radio" value="2"  class="hav_rapid_fieldshav Refer_FacilityHAV" name="Refer_FacilityHAV" id="Refer_HigherFacilityHAV" <?php echo (count($patient_data) > 0 && $patient_data[0]->Refer_HigherFacilityHAV == 2)?"checked":''; ?> style="width: 20px; height: 20px;"><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Patient referred for management to higher facility
								</b></label>
							</div>
						</div>
						<br><br>
						<!-- end-->

					</div>

					<!-- hbs_fields -->

					<div class="row hbs_fields">
						<div class="col-md-12">
							<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">Screening Details - HBsAg Testing</h4>
						</div>
					</div>
					<br class="hbs_fields">
					<div class="hbs_fields">
						<div class="row">
							<div class="col-md-3 col-md-offset-1">
								<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hbs_rapid" name="hbs_rapid" id="hbs_rapid" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Rapid Diagnostic Test</b></label>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 col-md-offset-2">
								<label for="">Date <span class="hbs_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?"*":""; ?></span></label>
								<input type="text"   name="hbs_rapid_date" id="hbs_rapid_date" class="input_fields form-control hbs_rapid_fields hasCal dateInpt input2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?timeStampShow($patient_data[0]->HBSRapidDate):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" >
								<br class="hidden-lg-*">
							</div>
							<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
								<label for="">Result <span class="hbs_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?"*":""; ?></span></label>
								<select name="hbs_rapid_result" id="hbs_rapid_result" class="form-control input_fields hbs_rapid_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?"required":"disabled"; ?>>
									<option value="">Select</option>
									<?php foreach ($result_options as $row) {?>
										<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1 && $patient_data[0]->HBSRapidResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
									<?php } ?>
								</select>
								<br class="hidden-lg-*">
							</div>
							<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
								<label for="">Place Of Testing <span class="hbs_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?"*":""; ?></span></label>
								<select class="form-control input_fields hbs_rapid_fields place_of_testing" data-test="hbs_rapid" id="hbs_rapid_place_of_test" name="hbs_rapid_place_of_test" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?"required":"disabled"; ?>>
									<option value="">Select</option>
									<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1 && $patient_data[0]->HBSRapidPlace == 1)?'selected':''; ?>>Govt. Lab</option>
									<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1 && $patient_data[0]->HBSRapidPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
								</select>
							</div>
						</div>
						<div class="row hbs_rapid_other_lab_name govt_lab hbs_rapid_govt_lab">
							<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
								<label for="">Lab Name <span class="hbs_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?"*":""; ?></span></label>
								<select type="text" name="hbs_rapid_lab_name" id="hbs_rapid_lab_name" class="form-control input_fields hbs_rapid_fields lab_names" data-test="hbs_rapid" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1 && $patient_data[0]->HBSRapidPlace == 1)?"required":"disabled"; ?>>
									<option value="">Select</option>
									<?php 
									if($patient_data[0]->HAVRapidLabID!=""){
										foreach ($facility as $row) {
											?>
											<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1 && $patient_data[0]->HBSRapidLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
											<?php 
										} } else{

											foreach ($facility as $facilitiesdata) {
												?>
												<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
												<?php 
											}

										}
										?>
									</select>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 lab_name_others hbs_rapid_lab_name_other">
									<label for="">Lab Name Other <span class="hbs_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?"*":""; ?></span></label>
									<input type="text" name="hbs_rapid_lab_name_other" id="hbs_rapid_lab_name_other" class="input_fields form-control hbs_rapid_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1 && $patient_data[0]->HBSRapidLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBSRapid == 1)?$patient_data[0]->HBSRapidLabOther:''; ?>">
									<br class="hidden-lg-*">
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-3 col-md-offset-1">
									<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hbs_elisa" name="hbs_elisa" id="hbs_elisa" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">ELISA Test</b></label>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 col-md-offset-2">
									<label for="">Date <span class="hbs_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?"*":""; ?></span></label>
									<input type="text" name="hbs_elisa_date" id="hbs_elisa_date" class="input_fields form-control hbs_elisa_fields hasCal dateInpt input2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?timeStampShow($patient_data[0]->HBSElisaDate):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" >
									<br class="hidden-lg-*">
								</div>
								<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
									<label for="">Result <span class="hbs_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?"*":""; ?></span></label>
									<select name="hbs_elisa_result" id="hbs_elisa_result" class="form-control input_fields hbs_elisa_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?"required":"disabled"; ?>>
										<option value="">Select</option>
										<?php foreach ($result_options as $row) {?>
											<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1 && $patient_data[0]->HBSElisaResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
										<?php } ?>
									</select>
									<br class="hidden-lg-*">
								</div>
								<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
									<label for="">Place Of Testing <span class="hbs_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?"*":""; ?></span></label>
									<select class="form-control input_fields hbs_elisa_fields place_of_testing" data-test="hbs_elisa" id="hbs_elisa_place_of_test" name="hbs_elisa_place_of_test" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?"required":"disabled"; ?>>
										<option value="">Select</option>
										<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1 && $patient_data[0]->HBSElisaPlace == 1)?'selected':''; ?>>Govt. Lab</option>
										<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1 && $patient_data[0]->HBSElisaPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
									</select>
								</div>
							</div>
							<div class="row hbs_elisa_other_lab_name govt_lab hbs_elisa_govt_lab">
								<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
									<label for="">Lab Name <span class="hbs_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?"*":""; ?></span></label>
									<select type="text" name="hbs_elisa_lab_name" id="hbs_elisa_lab_name" class="form-control input_fields hbs_elisa_fields lab_names" data-test="hbs_elisa" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1 && $patient_data[0]->HBSElisaPlace == 1)?"required":"disabled"; ?>>
										<option value="">Select</option>
										<?php 
										if($patient_data[0]->HAVRapidLabID!=""){
											foreach ($facility as $row) {
												?>
												<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1 && $patient_data[0]->HBSElisaLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
												<?php 
											}
										} else{

											foreach ($facility as $facilitiesdata) {
												?>
												<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
												<?php 
											}

										}
										?>
									</select>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hbs_elisa_lab_name_other lab_name_others hbs_elisa_lab_name_other">
									<label for="">Lab Name Other <span class="hbs_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?"*":""; ?></span></label>
									<input type="text" name="hbs_elisa_lab_name_other" id="hbs_elisa_lab_name_other" class="input_fields form-control hbs_elisa_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1 && $patient_data[0]->HBSElisaLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBSElisa == 1)?$patient_data[0]->HBSElisaLabOther:''; ?>">
									<br class="hidden-lg-*">
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-3 col-md-offset-1">
									<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hbs_other" name="hbs_other" id="hbs_other" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Other Test</b></label>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
									<label for="">Test Name <span class="hbs_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?"*":""; ?></span></label>
									<input autocomplete="anyrandomthing" type="text" name="hbs_other_test_name" id="hbs_other_test_name" class="input_fields form-control hbs_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?$patient_data[0]->HBSOtherName:''; ?>">
									<br class="hidden-lg-*">
								</div>
								<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
									<label for="">Date <span class="hbs_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?"*":""; ?></span></label>
									<input type="text" name="hbs_other_date" id="hbs_other_date" class="input_fields form-control hbs_other_fields hasCal dateInpt input2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?timeStampShow($patient_data[0]->HBSOtherDate):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" >
									<br class="hidden-lg-*">
								</div>
								<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
									<label for="">Result <span class="hbs_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?"*":""; ?></span></label>
									<select name="hbs_other_result" id="hbs_other_result" class="form-control input_fields hbs_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?"required":"disabled"; ?>>
										<option value="">Select</option>
										<?php foreach ($result_options as $row) {?>
											<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1 && $patient_data[0]->HBSOtherResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
										<?php } ?>
									</select>
									<br class="hidden-lg-*">
								</div>
								<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
									<label for="">Place Of Testing <span class="hbs_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?"*":""; ?></span></label>
									<select class="form-control input_fields hbs_other_fields place_of_testing" data-test="hbs_other" id="hbs_other_place_of_test" name="hbs_other_place_of_test" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?"required":"disabled"; ?>>
										<option value="">Select</option>
										<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1 && $patient_data[0]->HBSOtherPlace == 1)?'selected':''; ?>>Govt. Lab</option>
										<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1 && $patient_data[0]->HBSOtherPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
									</select>
								</div>
							</div>
							<div class="row hbs_other_other_lab_name govt_lab hbs_other_govt_lab">
								<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
									<label for="">Lab Name <span class="hbs_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?"*":""; ?></span></label>
									<select type="text" name="hbs_other_lab_name" id="hbs_other_lab_name" class="form-control input_fields hbs_other_fields lab_names" data-test="hbs_other" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1 && $patient_data[0]->HBSOtherPlace == 1)?"required":"disabled"; ?>>
										<option value="">Select</option>
										<?php 
										if($patient_data[0]->HAVRapidLabID!=""){
											foreach ($facility as $row) {
												?>
												<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1 && $patient_data[0]->HBSOtherLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
												<?php 
											} }else{

												foreach ($facility as $facilitiesdata) {
													?>
													<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
													<?php 
												}

											}
											?>
										</select>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hbs_other_lab_name_other hbs_other_lab_name_other lab_name_others">
										<label for="">Lab Name Other <span class="hbs_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?"*":""; ?></span></label>
										<input type="text" name="hbs_other_lab_name_other" id="hbs_other_lab_name_other" class="input_fields form-control hbs_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1 && $patient_data[0]->HBSOtherLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBSOther == 1)?$patient_data[0]->HBSLabOther:''; ?>">
										<br class="hidden-lg-*">
									</div>
								</div>
								<br>
								<br>
							</div>

							<!-- hbc_fields -->

							<div class="row hbc_fields">
								<div class="col-md-12">
									<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">Screening Details - lgM Anti HBc Testing</h4>
								</div>
							</div>
							<br class="hbc_fields">
							<div class="hbc_fields">
								<div class="row">
									<div class="col-md-3 col-md-offset-1">
										<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hbc_rapid" name="hbc_rapid" id="hbc_rapid" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Rapid Diagnostic Test</b></label>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 col-md-offset-2">
										<label for="">Date <span class="hbc_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1)?"*":""; ?></span></label>
										<input type="text" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>"   name="hbc_rapid_date" id="hbc_rapid_date" class="input_fields form-control hbc_rapid_fields hasCal dateInpt input2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1)?timeStampShow($patient_data[0]->HBCRapidDate):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" >
										<br class="hidden-lg-*">
									</div>
									<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
										<label for="">Result <span class="hbc_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1)?"*":""; ?></span></label>
										<select name="hbc_rapid_result" id="hbc_rapid_result" class="form-control input_fields hbc_rapid_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1)?"required":"disabled"; ?>>
											<option value="">Select</option>
											<?php foreach ($result_options as $row) {?>
												<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1 && $patient_data[0]->HBCRapidResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
											<?php } ?>
										</select>
										<br class="hidden-lg-*">
									</div>
									<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
										<label for="">Place Of Testing <span class="hbc_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1)?"*":""; ?></span></label>
										<select class="form-control input_fields hbc_rapid_fields place_of_testing" data-test="hbc_rapid" id="hbc_rapid_place_of_test" name="hbc_rapid_place_of_test" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1)?"required":"disabled"; ?>>
											<option value="">Select</option>
											<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1 && $patient_data[0]->HBCRapidPlace == 1)?'selected':''; ?>>Govt. Lab</option>
											<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1 && $patient_data[0]->HBCRapidPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
										</select>
									</div>
								</div>
								<div class="row hbc_rapid_other_lab_name govt_lab hbc_rapid_govt_lab">
									<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
										<label for="">Lab Name <span class="hbc_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1)?"*":""; ?></span></label>
										<select type="text" name="hbc_rapid_lab_name" id="hbc_rapid_lab_name" class="form-control input_fields hbc_rapid_fields lab_names" data-test="hbc_rapid" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1 && $patient_data[0]->HBCRapidPlace == 1)?"required":"disabled"; ?>>
											<option value="">Select</option>
											<?php 
											if($patient_data[0]->HAVRapidLabID!=""){
												foreach ($facility as $row) {
													?>
													<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1 && $patient_data[0]->HBCRapidLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
													<?php 
												} }else{

													foreach ($facility as $facilitiesdata) {
														?>
														<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
														<?php 
													}

												}
												?>
											</select>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 lab_name_others hbc_rapid_lab_name_other">
											<label for="">Lab Name Other <span class="hbc_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1)?"*":""; ?></span></label>
											<input type="text" name="hbc_rapid_lab_name_other" id="hbc_rapid_lab_name_other" class="input_fields form-control hbc_rapid_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1 && $patient_data[0]->HBCRapidLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBCRapid == 1)?$patient_data[0]->HBCRapidLabOther:''; ?>">
											<br class="hidden-lg-*">
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-md-3 col-md-offset-1">
											<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hbc_elisa" name="hbc_elisa" id="hbc_elisa" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">ELISA Test</b></label>
										</div>
										<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 col-md-offset-2">
											<label for="">Date <span class="hbc_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1)?"*":""; ?></span></label>
											<input type="text" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>"  name="hbc_elisa_date" id="hbc_elisa_date" class="input_fields form-control hbc_elisa_fields hasCal dateInpt input2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1)?timeStampShow($patient_data[0]->HBCElisaDate):''; ?>">
											<br class="hidden-lg-*">
										</div>
										<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
											<label for="">Result <span class="hbc_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1)?"*":""; ?></span></label>
											<select name="hbc_elisa_result" id="hbc_elisa_result" class="form-control input_fields hbc_elisa_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1)?"required":"disabled"; ?>>
												<option value="">Select</option>
												<?php foreach ($result_options as $row) {?>
													<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1 && $patient_data[0]->HBCElisaResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
												<?php } ?>
											</select>
											<br class="hidden-lg-*">
										</div>
										<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
											<label for="">Place Of Testing <span class="hbc_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1)?"*":""; ?></span></label>
											<select class="form-control input_fields hbc_elisa_fields place_of_testing" data-test="hbc_elisa" id="hbc_elisa_place_of_test" name="hbc_elisa_place_of_test" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1)?"required":"disabled"; ?>>
												<option value="">Select</option>
												<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1 && $patient_data[0]->HBCElisaPlace == 1)?'selected':''; ?>>Govt. Lab</option>
												<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1 && $patient_data[0]->HBCElisaPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
											</select>
										</div>
									</div>
									<div class="row hbc_elisa_other_lab_name govt_lab hbc_elisa_govt_lab">
										<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
											<label for="">Lab Name <span class="hbc_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1)?"*":""; ?></span></label>
											<select type="text" name="hbc_elisa_lab_name" id="hbc_elisa_lab_name" class="form-control input_fields hbc_elisa_fields lab_names" data-test="hbc_elisa" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1 && $patient_data[0]->HBCElisaPlace == 1)?"required":"disabled"; ?>>
												<option value="">Select</option>
												<?php 
												if($patient_data[0]->HAVRapidLabID!=""){
													foreach ($facility as $row) {
														?>
														<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1 && $patient_data[0]->HBCElisaLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
														<?php 
													} }else{

														foreach ($facility as $facilitiesdata) {
															?>
															<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
															<?php 
														}

													}
													?>
												</select>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hbc_elisa_lab_name_other lab_name_others hbc_elisa_lab_name_other">
												<label for="">Lab Name Other <span class="hbc_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1)?"*":""; ?></span></label>
												<input type="text" name="hbc_elisa_lab_name_other" id="hbc_elisa_lab_name_other" class="input_fields form-control hbc_elisa_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1 && $patient_data[0]->HBCElisaLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBCElisa == 1)?$patient_data[0]->HBCElisaLabOther:''; ?>">
												<br class="hidden-lg-*">
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-md-3 col-md-offset-1">
												<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hbc_other" name="hbc_other" id="hbc_other" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Other Test</b></label>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
												<label for="">Test Name <span class="hbc_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?"*":""; ?></span></label>
												<input autocomplete="anyrandomthing" type="text" name="hbc_other_test_name" id="hbc_other_test_name" class="input_fields form-control hbc_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?$patient_data[0]->HBCOtherName:''; ?>">
												<br class="hidden-lg-*">
											</div>
											<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
												<label for="">Date <span class="hbc_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?"*":""; ?></span></label>
												<input type="text" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>"  name="hbc_other_date" id="hbc_other_date" class="input_fields form-control hbc_other_fields hasCal dateInpt input2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?timeStampShow($patient_data[0]->HBCOtherDate):''; ?>">
												<br class="hidden-lg-*">
											</div>
											<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
												<label for="">Result <span class="hbc_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?"*":""; ?></span></label>
												<select name="hbc_other_result" id="hbc_other_result" class="form-control input_fields hbc_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?"required":"disabled"; ?>>
													<option value="">Select</option>
													<?php foreach ($result_options as $row) {?>
														<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1 && $patient_data[0]->HBCOtherResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
													<?php } ?>
												</select>
												<br class="hidden-lg-*">
											</div>
											<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
												<label for="">Place Of Testing <span class="hbc_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?"*":""; ?></span></label>
												<select class="form-control input_fields hbc_other_fields place_of_testing" data-test="hbc_other" id="hbc_other_place_of_test" name="hbc_other_place_of_test" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?"required":"disabled"; ?>>
													<option value="">Select</option>
													<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1 && $patient_data[0]->HBCOtherPlace == 1)?'selected':''; ?>>Govt. Lab</option>
													<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1 && $patient_data[0]->HBCOtherPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
												</select>
											</div>
										</div>
										<div class="row hbc_other_other_lab_name govt_lab hbc_other_govt_lab">
											<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
												<label for="">Lab Name <span class="hbc_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?"*":""; ?></span></label>
												<select type="text" name="hbc_other_lab_name" id="hbc_other_lab_name" class="form-control input_fields hbc_other_fields lab_names" data-test="hbc_other" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1 && $patient_data[0]->HBCOtherPlace == 1)?"required":"disabled"; ?>>
													<option value="">Select</option>
													<?php 
													if($patient_data[0]->HAVRapidLabID!=""){
														foreach ($facility as $row) {
															?>
															<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1 && $patient_data[0]->HBCOtherLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
															<?php 
														} }else{

															foreach ($facility as $facilitiesdata) {
																?>
																<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
																<?php 
															}

														}
														?>
													</select>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hbc_other_lab_name_other hbc_other_lab_name_other lab_name_others">
													<label for="">Lab Name Other <span class="hbc_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?"*":""; ?></span></label>
													<input type="text" name="hbc_other_lab_name_other" id="hbc_other_lab_name_other" class="input_fields form-control hbc_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1 && $patient_data[0]->HBCOtherLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HBCOther == 1)?$patient_data[0]->HBCLabOther:''; ?>">
													<br class="hidden-lg-*">
												</div>
											</div>
											<br>
											<br>
										</div>

										<!-- hcv_fields -->

										<div class="row hcv_fields">
											<div class="col-md-12">
												<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">Screening Details - Anti HCV Testing</h4>
											</div>
										</div>
										<br class="hcv_fields">
										<div class="hcv_fields">
											<div class="row">
												<div class="col-md-3 col-md-offset-1">
													<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hcv_rapid" name="hcv_rapid" id="hcv_rapid" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Rapid Diagnostic Test</b></label>
												</div>
												<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 col-md-offset-2">
													<label for="">Date <span class="hcv_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?"*":""; ?></span></label>
													<input type="text" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>"  name="hcv_rapid_date" id="hcv_rapid_date" class="input_fields form-control hcv_rapid_fields hasCal dateInpt input2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?timeStampShow($patient_data[0]->HCVRapidDate):''; ?>">
													<br class="hidden-lg-*">
												</div>
												<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
													<label for="">Result <span class="hcv_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?"*":""; ?></span></label>
													<select name="hcv_rapid_result" id="hcv_rapid_result" class="form-control input_fields hcv_rapid_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?"required":"disabled"; ?>>
														<option value="">Select</option>
														<?php foreach ($result_options as $row) {?>
															<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1 && $patient_data[0]->HCVRapidResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
														<?php } ?>
													</select>
													<br class="hidden-lg-*">
												</div>
												<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
													<label for="">Place Of Testing <span class="hcv_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?"*":""; ?></span></label>
													<select class="form-control input_fields hcv_rapid_fields place_of_testing" data-test="hcv_rapid" id="hcv_rapid_place_of_test" name="hcv_rapid_place_of_test" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?"required":"disabled"; ?>>
														<option value="">Select</option>
														<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1 && $patient_data[0]->HCVRapidPlace == 1)?'selected':''; ?>>Govt. Lab</option>
														<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1 && $patient_data[0]->HCVRapidPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
													</select>
												</div>
											</div>
											<div class="row hcv_rapid_other_lab_name govt_lab hcv_rapid_govt_lab">
												<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
													<label for="">Lab Name <span class="hcv_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?"*":""; ?></span></label>
													<select type="text" name="hcv_rapid_lab_name" id="hcv_rapid_lab_name" class="form-control input_fields hcv_rapid_fields lab_names" data-test="hcv_rapid" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1 && $patient_data[0]->HCVRapidPlace == 1)?"required":"disabled"; ?>>
														<option value="">Select</option>
														<?php 
														if($patient_data[0]->HAVRapidLabID!=""){
															foreach ($facility as $row) {
																?>
																<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1 && $patient_data[0]->HCVRapidLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
																<?php 
															}
														}else{

															foreach ($facility as $facilitiesdata) {
																?>
																<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
																<?php 
															}

														}
														?>
													</select>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 lab_name_others hcv_rapid_lab_name_other">
													<label for="">Lab Name Other <span class="hcv_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?"*":""; ?></span></label>
													<input type="text" name="hcv_rapid_lab_name_other" id="hcv_rapid_lab_name_other" class="input_fields form-control hcv_rapid_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1 && $patient_data[0]->HCVRapidLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HCVRapid == 1)?$patient_data[0]->HCVRapidLabOther:''; ?>">
													<br class="hidden-lg-*">
												</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-3 col-md-offset-1">
													<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hcv_elisa" name="hcv_elisa" id="hcv_elisa" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">ELISA Test</b></label>
												</div>
												<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 col-md-offset-2">
													<label for="">Date <span class="hcv_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?"*":""; ?></span></label>
													<input type="text" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>"  name="hcv_elisa_date" id="hcv_elisa_date" class="input_fields form-control hcv_elisa_fields hasCal dateInpt input2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?timeStampShow($patient_data[0]->HCVElisaDate):''; ?>">
													<br class="hidden-lg-*">
												</div>
												<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
													<label for="">Result <span class="hcv_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?"*":""; ?></span></label>
													<select name="hcv_elisa_result" id="hcv_elisa_result" class="form-control input_fields hcv_elisa_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?"required":"disabled"; ?>>
														<option value="">Select</option>
														<?php foreach ($result_options as $row) {?>
															<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1 && $patient_data[0]->HCVElisaResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
														<?php } ?>
													</select>
													<br class="hidden-lg-*">
												</div>
												<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
													<label for="">Place Of Testing <span class="hcv_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?"*":""; ?></span></label>
													<select class="form-control input_fields hcv_elisa_fields place_of_testing" data-test="hcv_elisa" id="hcv_elisa_place_of_test" name="hcv_elisa_place_of_test" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?"required":"disabled"; ?>>
														<option value="">Select</option>
														<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1 && $patient_data[0]->HCVElisaPlace == 1)?'selected':''; ?>>Govt. Lab</option>
														<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1 && $patient_data[0]->HCVElisaPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
													</select>
												</div>
											</div>
											<div class="row hcv_elisa_other_lab_name govt_lab hcv_elisa_govt_lab">
												<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
													<label for="">Lab Name <span class="hcv_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?"*":""; ?></span></label>
													<select type="text" name="hcv_elisa_lab_name" id="hcv_elisa_lab_name" class="form-control input_fields hcv_elisa_fields lab_names" data-test="hcv_elisa" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1 && $patient_data[0]->HCVElisaPlace == 1)?"required":"disabled"; ?>>
														<option value="">Select</option>
														<?php 
														if($patient_data[0]->HAVRapidLabID!=""){
															foreach ($facility as $row) {
																?>
																<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1 && $patient_data[0]->HCVElisaLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
																<?php 
															}
														}else{

															foreach ($facility as $facilitiesdata) {
																?>
																<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
																<?php 
															}

														}
														?>
													</select>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hcv_elisa_lab_name_other lab_name_others hcv_elisa_lab_name_other">
													<label for="">Lab Name Other <span class="hcv_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?"*":""; ?></span></label>
													<input type="text" name="hcv_elisa_lab_name_other" id="hcv_elisa_lab_name_other" class="input_fields form-control hcv_elisa_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1 && $patient_data[0]->HCVElisaLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HCVElisa == 1)?$patient_data[0]->HCVElisaLabOther:''; ?>">
													<br class="hidden-lg-*">
												</div>
											</div>
											<br>
											<div class="row">
												<div class="col-md-3 col-md-offset-1">
													<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hcv_other" name="hcv_other" id="hcv_other" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Other Test</b></label>
												</div>
												<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
													<label for="">Test Name <span class="hcv_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?"*":""; ?></span></label>
													<input autocomplete="anyrandomthing" type="text" name="hcv_other_test_name" id="hcv_other_test_name" class="input_fields form-control hcv_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?$patient_data[0]->HCVOtherName:''; ?>">
													<br class="hidden-lg-*">
												</div>
												<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
													<label for="">Date <span class="hcv_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?"*":""; ?></span></label>
													<input type="text" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>"  name="hcv_other_date" id="hcv_other_date" class="input_fields form-control hcv_other_fields hasCal dateInpt input2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?timeStampShow($patient_data[0]->HCVOtherDate):''; ?>">
													<br class="hidden-lg-*">
												</div>
												<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
													<label for="">Result <span class="hcv_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?"*":""; ?></span></label>
													<select name="hcv_other_result" id="hcv_other_result" class="form-control input_fields hcv_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?"required":"disabled"; ?>>
														<option value="">Select</option>
														<?php foreach ($result_options as $row) {?>
															<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1 && $patient_data[0]->HCVOtherResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
														<?php } ?>
													</select>
													<br class="hidden-lg-*">
												</div>
												<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
													<label for="">Place Of Testing <span class="hcv_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?"*":""; ?></span></label>
													<select class="form-control input_fields hcv_other_fields place_of_testing" data-test="hcv_other" id="hcv_other_place_of_test" name="hcv_other_place_of_test" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?"required":"disabled"; ?>>
														<option value="">Select</option>
														<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1 && $patient_data[0]->HCVOtherPlace == 1)?'selected':''; ?>>Govt. Lab</option>
														<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1 && $patient_data[0]->HCVOtherPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
													</select>
												</div>
											</div>
											<div class="row hcv_other_other_lab_name govt_lab hcv_other_govt_lab">
												<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
													<label for="">Lab Name <span class="hcv_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?"*":""; ?></span></label>
													<select type="text" name="hcv_other_lab_name" id="hcv_other_lab_name" class="form-control input_fields hcv_other_fields lab_names" data-test="hcv_other" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1 && $patient_data[0]->HCVOtherPlace == 1)?"required":"disabled"; ?>>
														<option value="">Select</option>
														<?php 
														if($patient_data[0]->HAVRapidLabID!=""){
															foreach ($facility as $row) {
																?>
																<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1 && $patient_data[0]->HCVOtherLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
																<?php 
															}
														}else{

															foreach ($facility as $facilitiesdata) {
																?>
																<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
																<?php 
															}

														}
														?>
													</select>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hcv_other_lab_name_other hcv_other_lab_name_other lab_name_others">
													<label for="">Lab Name Other <span class="hcv_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?"*":""; ?></span></label>
													<input type="text" name="hcv_other_lab_name_other" id="hcv_other_lab_name_other" class="input_fields form-control hcv_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1 && $patient_data[0]->HCVOtherLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HCVOther == 1)?$patient_data[0]->HCVLabOther:''; ?>">
													<br class="hidden-lg-*">
												</div>
											</div>
											<br>
											<br>
										</div>

										<!-- hev_fields -->

										<div class="row hev_fields">
											<div class="col-md-12">
												<h4 style="border : 1px solid black; background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px; padding-left: 10px; margin: 0;">Screening Details - lgM Anti HEV Testing</h4>
											</div>
										</div>
										<br class="hev_fields">
										<div class="hev_fields">
											<div class="row">
												<div class="col-md-3 col-md-offset-1">
													<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hev_rapid" name="hev_rapid" id="hev_rapid" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Rapid Diagnostic Test</b></label>
												</div>
												<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 col-md-offset-2">
													<label for="">Date <span class="hev_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?"*":""; ?></span></label>
													<input type="text" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>"  name="hev_rapid_date" id="hev_rapid_date" class="input_fields form-control hev_rapid_fields hasCal dateInpt input2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?timeStampShow($patient_data[0]->HEVRapidDate):''; ?>">
													<br class="hidden-lg-*">
												</div>
												<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
													<label for="">Result <span class="hev_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?"*":""; ?></span></label>
													<select name="hev_rapid_result" id="hev_rapid_result" class="form-control input_fields hev_rapid_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?"required":"disabled"; ?>>
														<option value="">Select</option>
														<?php foreach ($result_options as $row) {?>
															<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1 && $patient_data[0]->HEVRapidResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
														<?php } ?>
													</select>
													<br class="hidden-lg-*">
												</div>
												<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
													<label for="">Place Of Testing <span class="hev_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?"*":""; ?></span></label>
													<select class="form-control input_fields hev_rapid_fields place_of_testing" data-test="hev_rapid" id="hev_rapid_place_of_test" name="hev_rapid_place_of_test" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?"required":"disabled"; ?>>
														<option value="">Select</option>
														<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1 && $patient_data[0]->HEVRapidPlace == 1)?'selected':''; ?>>Govt. Lab</option>
														<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1 && $patient_data[0]->HEVRapidPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
													</select>
												</div>
											</div>
											<div class="row hev_rapid_other_lab_name govt_lab hev_rapid_govt_lab">
												<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
													<label for="">Lab Name <span class="hev_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?"*":""; ?></span></label>
													<select type="text" name="hev_rapid_lab_name" id="hev_rapid_lab_name" class="form-control input_fields hev_rapid_fields lab_names" data-test="hev_rapid" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1 && $patient_data[0]->HEVRapidPlace == 1)?"required":"disabled"; ?>>
														<option value="">Select</option>
														<?php 
														if($patient_data[0]->HAVRapidLabID!=""){
															foreach ($facility as $row) {
																?>
																<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1 && $patient_data[0]->HEVRapidLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
																<?php 
															} }else{

																foreach ($facility as $facilitiesdata) {
																	?>
																	<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
																	<?php 
																}

															}
															?>
														</select>
													</div>
													<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 lab_name_others hev_rapid_lab_name_other">
														<label for="">Lab Name Other <span class="hev_rapid_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?"*":""; ?></span></label>
														<input type="text" name="hev_rapid_lab_name_other" id="hev_rapid_lab_name_other" class="input_fields form-control hev_rapid_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1 && $patient_data[0]->HEVRapidLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HEVRapid == 1)?$patient_data[0]->HEVRapidLabOther:''; ?>">
														<br class="hidden-lg-*">
													</div>
												</div>
												<br>
												<div class="row">
													<div class="col-md-3 col-md-offset-1">
														<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hev_elisa" name="hev_elisa" id="hev_elisa" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">ELISA Test</b></label>
													</div>
													<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 col-md-offset-2">
														<label for="">Date <span class="hev_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?"*":""; ?></span></label>
														<input type="text" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>"  name="hev_elisa_date" id="hev_elisa_date" class="input_fields form-control hev_elisa_fields hasCal dateInpt input2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?timeStampShow($patient_data[0]->HEVElisaDate):''; ?>">
														<br class="hidden-lg-*">
													</div>
													<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
														<label for="">Result <span class="hev_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?"*":""; ?></span></label></label>
														<select name="hev_elisa_result" id="hev_elisa_result" class="form-control input_fields hev_elisa_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?"required":"disabled"; ?>>
															<option value="">Select</option>
															<?php foreach ($result_options as $row) {?>
																<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1 && $patient_data[0]->HEVElisaResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
															<?php } ?>
														</select>
														<br class="hidden-lg-*">
													</div>
													<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
														<label for="">Place Of Testing <span class="hev_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?"*":""; ?></span></label></label>
														<select class="form-control input_fields hev_elisa_fields place_of_testing" data-test="hev_elisa" id="hev_elisa_place_of_test" name="hev_elisa_place_of_test" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?"required":"disabled"; ?>>
															<option value="">Select</option>
															<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1 && $patient_data[0]->HEVElisaPlace == 1)?'selected':''; ?>>Govt. Lab</option>
															<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1 && $patient_data[0]->HEVElisaPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
														</select>
													</div>
												</div>
												<div class="row hev_elisa_other_lab_name govt_lab hev_elisa_govt_lab">
													<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
														<label for="">Lab Name <span class="hev_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?"*":""; ?></span></label></label>
														<select type="text" name="hev_elisa_lab_name" id="hev_elisa_lab_name" class="form-control input_fields hev_elisa_fields lab_names" data-test="hev_elisa" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1 && $patient_data[0]->HEVElisaPlace == 1)?"required":"disabled"; ?>>
															<option value="">Select</option>
															<?php 
															if($patient_data[0]->HAVRapidLabID!=""){
																foreach ($facility as $row) {
																	?>
																	<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1 && $patient_data[0]->HEVElisaLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
																	<?php 
																} }else{

																	foreach ($facility as $facilitiesdata) {
																		?>
																		<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
																		<?php 
																	}

																}
																?>
															</select>
														</div>
														<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hev_elisa_lab_name_other lab_name_others hev_elisa_lab_name_other">
															<label for="">Lab Name Other <span class="hev_elisa_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?"*":""; ?></span></label></label>
															<input type="text" name="hev_elisa_lab_name_other" id="hev_elisa_lab_name_other" class="input_fields form-control hev_elisa_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1 && $patient_data[0]->HEVElisaLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HEVElisa == 1)?$patient_data[0]->HEVElisaLabOther:''; ?>">
															<br class="hidden-lg-*">
														</div>
													</div>
													<br>
													<div class="row">
														<div class="col-md-3 col-md-offset-1">
															<label class="checkbox-inline"><input type="checkbox" value="1" class="test_checkbox" data-test="hev_other" name="hev_other" id="hev_other" style="width: 20px; height: 20px;" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?"checked":""; ?>><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Other Test</b></label>
														</div>
														<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
															<label for="">Test Name <span class="hev_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?"*":""; ?></span></label></label>
															<input autocomplete="anyrandomthing" type="text" name="hev_other_test_name" id="hev_other_test_name" class="input_fields form-control hev_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?$patient_data[0]->HEVOtherName:''; ?>">
															<br class="hidden-lg-*">
														</div>
														<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
															<label for="">Date <span class="hev_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?"*":""; ?></span></label></label>
															<input type="text" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>"  name="hev_other_date" id="hev_other_date" class="input_fields form-control hev_other_fields hasCal dateInpt input2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?timeStampShow($patient_data[0]->HEVOtherDate):''; ?>">
															<br class="hidden-lg-*">
														</div>
														<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
															<label for="">Result <span class="hev_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?"*":""; ?></span></label></label>
															<select name="hev_other_result" id="hev_other_result" class="form-control input_fields hev_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?"required":"disabled"; ?>>
																<option value="">Select</option>
																<?php foreach ($result_options as $row) {?>
																	<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1 && $patient_data[0]->HEVOtherResult == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
																<?php } ?>
															</select>
															<br class="hidden-lg-*">
														</div>
														<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12">
															<label for="">Place Of Testing <span class="hev_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?"*":""; ?></span></label></label>
															<select class="form-control input_fields hev_other_fields place_of_testing" data-test="hev_other" id="hev_other_place_of_test" name="hev_other_place_of_test" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?"required":"disabled"; ?>>
																<option value="">Select</option>
																<option value="1" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1 && $patient_data[0]->HEVOtherPlace == 1)?'selected':''; ?>>Govt. Lab</option>
																<option value="2" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1 && $patient_data[0]->HEVOtherPlace == 2)?'selected':''; ?>>Private Lab-PPP</option>
															</select>
														</div>
													</div>
													<div class="row hev_other_other_lab_name govt_lab hev_other_govt_lab">
														<div class="col-lg-2 col-md-3 col-sm-6 col-xs-12 col-md-offset-6">
															<label for="">Lab Name <span class="hev_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?"*":""; ?></span></label></label>
															<select type="text" name="hev_other_lab_name" id="hev_other_lab_name" class="form-control input_fields hev_other_fields lab_names" data-test="hev_other" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1 && $patient_data[0]->HEVOtherPlace == 1)?"required":"disabled"; ?>>
																<option value="">Select</option>
																<?php 
																if($patient_data[0]->HAVRapidLabID!=""){
																	foreach ($facility as $row) {
																		?>
																		<option <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1 && $patient_data[0]->HEVOtherLabID == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
																		<?php 
																	}
																}else{

																	foreach ($facility as $facilitiesdata) {
																		?>
																		<option value="<?php echo $facilitiesdata->id_mstfacility; ?>" <?php if($default_facilities[0]->id_mstfacility == $facilitiesdata->id_mstfacility) { echo 'selected';} ?>><?php echo $facilitiesdata->FacilityCode; ?></option>
																		<?php 
																	}

																}
																?>
															</select>
														</div>
														<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hev_other_lab_name_other hev_other_lab_name_other lab_name_others">
															<label for="">Lab Name Other <span class="hev_other_required" style="color : red;"><?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?"*":""; ?></span></label></label>
															<input type="text" name="hev_other_lab_name_other" id="hev_other_lab_name_other" class="input_fields form-control hev_other_fields" <?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1 && $patient_data[0]->HEVOtherLabID == 999999)?"required":"disabled"; ?> value="<?php echo (count($patient_data) > 0 && $patient_data[0]->HEVOther == 1)?$patient_data[0]->HEVLabOther:''; ?>">
															<br class="hidden-lg-*">
														</div>



													</div>
													<br>
													<br>
													<!-- Start new section-->
													<div class="row hideshowposHEV">
														<div class="col-md-2">
															<h4><b></b></h4>
														</div>

														<div class="col-md-4">
															<label class="radio-inline"><input type="radio" value="1"  class="hev_rapid_fieldshev Refer_FacilityHEV" name="Refer_FacilityHEV" id="Refer_FacilityHEV" <?php echo (count($patient_data) > 0 && $patient_data[0]->Refer_FacilityHEV == 1)?"checked":''; ?> style="width: 20px; height: 20px;"><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Patient managed at the facility
															</b></label>
														</div>


														<div class="col-md-4">
															<label class="radio-inline"><input type="radio" value="2"  class="hev_rapid_fieldshev Refer_FacilityHEV" name="Refer_FacilityHEV" id="Refer_HigherFacilityHEV" <?php echo (count($patient_data) > 0 && $patient_data[0]->Refer_HigherFacilityHEV == 2)?"checked":''; ?> style="width: 20px; height: 20px;"><b style="padding-left: 10px; font-size: 13px; position: relative; top:10px;">Patient referred for management to higher facility
															</b></label>
														</div>
													</div>
													<br><br>
													<!-- end-->

												</div>
												<input type="hidden" name="interruption_status" id="interruption_status" value="<?php  echo (count($patient_data) > 0)?$patient_data[0]->InterruptReason:'';  ?>">
												<br>
												<div class="row">
													<div class="col-lg-3 col-md-2">
														<a class="btn btn-block btn-default form_buttons" href="<?php echo base_url('patientinfo_hep_b?p=1'); ?>" id="close" name="close" value="close">CLOSE</a>
													</div>
													<div class="col-lg-3 col-md-2">
														<button class="btn btn-block btn-default form_buttons" id="refresh" name="refresh" value="refresh">REFRESH</button>
													</div>
													<?php if($loginData->RoleId!=99){ ?>
														<div class="col-lg-3 col-md-2">
															<a href="" class="btn btn-block btn-default form_buttons" id="lock" name="lock" value="lock">LOCK</a>
														</div>
													<?php } else{?>
														<div class="col-lg-3 col-md-2">
															<a href="javascript:void(0)" class="btn btn-block btn-default form_buttons" onclick="openPopupUnlock()" id="" name="unlock" value="lock">LOCK</a>
														</div>
													<?php } ?>

													<?php if($result[0]->Screening == 1) {?>
														<div class="col-lg-3 col-md-2">
															<button class="btn btn-block btn-success form_buttons" id="save" name="save" value="save">SAVE</button>
														</div>
													<?php } ?>
												</div>
												<br><br><br>
												<?php echo form_close(); ?>
												<div class="row" class="text-left">

													<div class="col-md-6 card well">
														<label class="col-sm-4"  style="text-align: left !important; line-height: 2.2 !important; ">Patient's Status -</label>
														<div class="col-sm-8" style="text-align: left !important; line-height: 2.2 !important; ">
															<label><?php echo (count($patient_status) > 0)?$patient_status[0]->status:''; ?></label>

														</div>
													</div>



													<div class="col-md-6 card well">
														<label class="col-sm-6" style="text-align: left !important; ">Patient's Interruption Status </label>
														<div class="col-sm-6">
															<select name="" id="type_val" onchange="openPopup()" class="btn" style="text-align: right!important;">
																<option value="">Select</option>
																<option value="1"  <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason != '')?'selected':''; ?> >Yes</option>
																<option value="0" <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason == '')?'selected':''; ?>>No</option>
															</select>

														</div>
													</div>

												</div>



												<!-- </form> -->
											</div>
										</div>

										<br><br><br>

										<div class="modal fade" id="addMyModal" role="dialog">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">

														<h4 class="modal-title">Patient's Interruption Status</h4>
													</div>
													<span id='form-error' style='color:red'></span>
													<div class="modal-body">
														<!--   <form role="form" id="newModalForm" method="post"> -->
															<?php
															$attributes = array(
																'id' => 'newModalForm',
																'name' => 'newModalForm',
																'autocomplete' => 'off',
															);
															echo form_open('', $attributes); ?>


															<div class="form-group">
																<label class="control-label col-md-3" for="email">Reason:</label>
																<div class="col-md-9">
																	<select class="form-control" id="resonval" name="resonval" required="required">
																		<option value="">Select</option>
																		<?php foreach ($InterruptReason as $key => $value) { ?>
																			<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
																		<?php } ?>
																	</select> 
																</div>
															</div>
															<br/><br/>

															<div class="form-group resonfielddeath" >
																<label class="control-label col-md-3" for="email">Reason for death:</label>
																<div class="col-md-9">
																	<select class="form-control" id="resonvaldeath" name="resonvaldeath">
																		<option value="">Select</option>
																		<?php foreach ($reason_death as $key => $value) { ?>
																			<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>

																		<?php } ?>
																	</select> 
																</div>
															</div>
															<br/><br/>

															<div class="form-group resonfieldlfu">
																<label class="control-label col-md-3" for="email">Reason for LFU:</label>
																<div class="col-md-9">
																	<select class="form-control" id="resonfluid" name="resonfluid">
																		<option value="">Select</option>
																		<?php foreach ($reason_flu as $key => $value) { ?>
																			<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>

																		<?php } ?>
																	</select> 
																</div>
															</div>

															<div class="form-group resonfieldothers" >
																<label class="control-label col-md-3" for="email">Other(specify):</label>
																<div class="col-md-9">
																	<input type="text" class="form-control" id="otherInterrupt" name="otherInterrupt">
																</div>
															</div>
															<br/><br/>


															<div class="modal-footer">
																<button type="submit" class="btn btn-success" id="btnSaveIt">Save</button>
																<button type="button" class="btn btn-default" id="btnCloseIt" data-dismiss="modal">Close</button>
															</div>
															<?php echo form_close(); ?>
															<!--  </form> -->
														</div>
													</div>
												</div>
											</div>
											<br/><br/><br/>

											<!-- Unlock box start-->
											<div class="modal fade" id="addMyModalUnbox" role="dialog">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">

															<h4 class="modal-title">Unlock Process</h4>
														</div>
														<span id='form-error' style='color:red'></span>
														<div class="modal-body">
															<!--   <form role="form" id="newModalForm" method="post"> -->
																<?php
																$attributes = array(
																	'id' => 'unlockModalForm',
																	'name' => 'unlockModalForm',
																	'autocomplete' => 'off',
																);
																echo form_open('', $attributes); ?>


																<div class="form-group">
																	<label class="control-label col-md-3" for="email">Reason:</label>
																	<div class="col-md-9">
																		<select class="form-control" id="unlockresonval" name="unlockresonval" required="required">
																			<option value="">Select</option>
																			<?php foreach ($unlockprocess as $key => $value) { ?>
																				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
																			<?php } ?>
																		</select> 
																	</div>
																</div>
																<br/><br/>

																<div class="form-group unlockresonfieldothers" >
																	<label class="control-label col-md-3" for="email">Other(specify):</label>
																	<div class="col-md-9">
																		<input type="text" class="form-control" id="unlockotherInterrupt" name="unlockotherInterrupt">
																	</div>
																</div>
																<br/><br/>


																<div class="modal-footer">
																	<button type="submit" class="btn btn-success" id="unlock">Save</button>
																	<button type="button" class="btn btn-default" id="btnunlock" data-dismiss="modal">Close</button>
																</div>
																<?php echo form_close(); ?>
																<!--  </form> -->
															</div>
														</div>
													</div>
												</div>
												<br/><br/><br/>
												<!-- end unlock -->

												<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
												<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>

												<script>



	/*$('#hav_rapid_date').change(function(){
	var hav_rapid_date= $('#hav_rapid_date').val();	
	var todaydate = '<?php //echo Date('Y-m-d'); ?>';
	if(hav_rapid_date > todaydate){
	alert('eeeeeee');
	$('#hav_rapid_date').val('');	
	}
});*/


$('#hav_rapid').click(function(e){
	if(($('#hav_rapid').prop('checked'))==false && ($('#hav_elisa').prop('checked'))==false && ($('#hav_other').prop('checked'))==false){
		
		$('.hav_rapid_fieldshav').prop('disabled', true);
	}else{
		$('.hav_rapid_fieldshav').prop('disabled', false);
	}
});

$('#hav_elisa').click(function(e){
	if(($('#hav_rapid').prop('checked'))==false && ($('#hav_elisa').prop('checked'))==false && ($('#hav_other').prop('checked'))==false){
		
		$('.hav_rapid_fieldshav').prop('disabled', true);
	}else{
		$('.hav_rapid_fieldshav').prop('disabled', false);
	}
});

$('#hav_other').click(function(e){
	if(($('#hav_rapid').prop('checked'))==false && ($('#hav_elisa').prop('checked'))==false && ($('#hav_other').prop('checked'))==false){
		
		$('.hav_rapid_fieldshav').prop('disabled', true);
	}else{
		$('.hav_rapid_fieldshav').prop('disabled', false);
	}
});


<?php if(count($patient_data) > 0 && $patient_data[0]->Refer_FacilityHAV == 1 || $patient_data[0]->Refer_HigherFacilityHAV == 2) {?>
	$('.hideshowposHAV').show();
<?php } else{?>
	$('.hideshowposHAV').hide();
<?php } ?>

/*Anti HAV start*/

$('#hav_rapid_result').change(function(e){

	if($('#hav_rapid_result').val()==1 || $('#hav_elisa_result').val()==1 || $('#hav_other_result').val()==1){
		$('.hideshowposHAV').show();

		$('.Refer_FacilityHAV').prop('required',true);
	//$('#Refer_HigherFacilityHAV').prop('required',true);

}else{
	$('.Refer_FacilityHAV').prop('checked', false);
		//$('#Refer_HigherFacilityHAV').prop('checked', false);

		$('.hideshowposHAV').hide();
		$('#Refer_FacilityHAV').prop('required',false);
		$('#Refer_HigherFacilityHAV').prop('required',false);	
	}

});

$('#hav_elisa_result').change(function(e){

	if( $('#hav_elisa_result').val()==1 || $('#hav_rapid_result').val()==1 || $('#hav_other_result').val()==1){
		$('.hideshowposHAV').show();

	}else{
		$('.hideshowposHAV').hide();
		$('#Refer_FacilityHAV').prop('checked', false);
		$('#Refer_HigherFacilityHAV').prop('checked', false);	
	}

});

$('#hav_other_result').change(function(e){

	if($('#hav_other_result').val()==1 || $('#hav_elisa_result').val()==1 || $('#hav_rapid_result').val()==1){
		$('.hideshowposHAV').show();
	}else{
		$('#Refer_FacilityHAV').prop('checked', false);
		$('#Refer_HigherFacilityHAV').prop('checked', false);
		$('.hideshowposHAV').hide();	
	}

});

/*Anti HEV start*/


$('#hev_rapid').click(function(e){
	if(($('#hev_rapid').prop('checked'))==false && ($('#hev_elisa').prop('checked'))==false && ($('#hev_other').prop('checked'))==false){
		
		$('.hev_rapid_fieldshev').prop('disabled', true);
	}else{
		$('.hev_rapid_fieldshev').prop('disabled', false);
	}
});

$('#hev_elisa').click(function(e){
	if(($('#hev_rapid').prop('checked'))==false && ($('#hev_elisa').prop('checked'))==false && ($('#hev_other').prop('checked'))==false){
		
		$('.hev_rapid_fieldshev').prop('disabled', true);
	}else{
		$('.hev_rapid_fieldshev').prop('disabled', false);
	}
});

$('#hev_other').click(function(e){
	if(($('#hev_rapid').prop('checked'))==false && ($('#hev_elisa').prop('checked'))==false && ($('#hev_other').prop('checked'))==false){
		
		$('.hev_rapid_fieldshev').prop('disabled', true);
	}else{
		$('.hev_rapid_fieldshev').prop('disabled', false);
	}
});




<?php if(count($patient_data) > 0 && $patient_data[0]->Refer_FacilityHEV == 1 || $patient_data[0]->Refer_HigherFacilityHEV == 2) {?>
	$('.hideshowposHEV').show();
<?php } else{?>
	$('.hideshowposHEV').hide();
<?php } ?>


$('#hev_rapid_result').change(function(e){

	if($('#hev_rapid_result').val()==1 || $('#hev_elisa_result').val()==1 || $('#hev_other_result').val()==1){
		$('.hideshowposHEV').show();
		$('.Refer_FacilityHEV').prop('required',true);
	//$('#Refer_HigherFacilityHEV').prop('required',true);

}else{

	$('.Refer_FacilityHEV').prop('checked', false);
		//$('#Refer_HigherFacilityHEV').prop('checked', false);

		$('.hideshowposHEV').hide();	
	}

});

$('#hev_elisa_result').change(function(e){

	if( $('#hev_elisa_result').val()==1 || $('#hev_rapid_result').val()==1 || $('#hev_other_result').val()==1){
		$('.hideshowposHEV').show();
	}else{
		$('#Refer_FacilityHEV').prop('checked', false);
		$('#Refer_HigherFacilityHEV').prop('checked', false);
		$('.hideshowposHEV').hide();	
	}

});

$('#hev_other_result').change(function(e){

	if($('#hev_other_result').val()==1 || $('#hev_rapid_result').val()==1 || $('#hev_elisa_result').val()==1){
		$('.hideshowposHEV').show();
	}else{
		$('#Refer_FacilityHEV').prop('checked', false);
		$('#Refer_HigherFacilityHEV').prop('checked', false);
		$('.hideshowposHEV').hide();	
	}

});
/*End HEV*/



$(document).ready(function(){

	var statusval			 = '<?php echo $patient_data[0]->MF2; ?>';
	var InterruptReason 	= '<?php echo $patient_data[0]->InterruptReason; ?>';

	if(statusval==0 && InterruptReason==1){

		$("#modal_header").text("Further entry is not allowed,as per treatment status...");
		$("#modal_text").text("Not allowed");
		$("#multipurpose_modal").modal("show");

		location.href='<?php echo base_url(); ?>patientinfo_hep_b/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';

	}
});


$(document).ready(function(){
	$("#type_val").prop("disabled", false);
});


function openPopup() {
	var type_val = $('#type_val').val();
	if(type_val=='1'){
		$("#addMyModal").modal();
	}
}

$('.resonfielddeath').hide();
$('.resonfieldlfu').hide();
$('.resonfieldothers').hide();



$('#btnCloseIt').click(function(){

	$('#type_val').val('0');
	$('#interruption_status').val('');

});

$('#type_val').change(function(){
	var type_val = $('#type_val').val();
	$('#interruption_status').val(type_val);

});



$('#btnSaveIt').click(function(e){

	e.preventDefault(); 
	$("#form-error").html('');


	var formData = new FormData($('#newModalForm')[0]);
	$('#btnSaveIt').prop('disabled',true);
	$.ajax({				    	
		url: '<?php echo base_url(); ?>patientinfo_hep_b/interruptionstatus_process/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
		type: 'POST',
		data: formData,
		dataType: 'json',
		async: false,
		cache: false,
		contentType: false,
		processData: false,
		success: function (data) { 

			if(data['status'] == 'true'){
				$("#form-error").html('Data has been successfully submitted');
				setTimeout(function() {
					location.href='<?php echo base_url(); ?>patientinfo_hep_b/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
				}, 1000);

			}else{
				$("#form-error").html(data['message']);
				$('#btnSaveIt').prop('disabled',false); 
				return false;
			}   
		}        
	}); 



}); 

function openPopupUnlock() {

	$("#addMyModalUnbox").modal();

}

$('.unlockresonfieldothers').hide();
$('#unlockresonval').change(function(){
	var unlockresonval = $('#unlockresonval').val();
	if(unlockresonval  ==4){
		$('.unlockresonfieldothers').show();
	}else{
		$('.unlockresonfieldothers').hide();	
	}
});

$('#resonval').change(function(){


	if($('#resonval').val() == '1'){

		$('.resonfielddeath').show();
		$('.resonfieldlfu').hide();
		$('.resonfieldothers').hide();
		$('#resonfluid').val('');

		$('#resonvaldeath').prop('required',true);
		$('#resonfluid').prop('required',false);
		$('#otherInterrupt').prop('required',false);

	}else if($('#resonval').val() == '2'){
		$('.resonfielddeath').hide();
		$('.resonfieldlfu').show();
		$('.resonfieldothers').hide();
		$('#resonvaldeath').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',true);
		$('#otherInterrupt').prop('required',false);
	}else if($('#resonval').val() == '99'){

		$('.resonfieldothers').show();
		$('.resonfielddeath').hide();
		$('.resonfieldlfu').hide();
		$('#otherInterrupt').prop('required',true);
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',false);
	}

	else{

		$('.resonfielddeath').hide();
		$('.resonfieldlfu').hide();
		$('.resonfieldothers').hide();
		$('#resonvaldeath').val('');
		$('#resonfluid').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',false);
		$('#otherInterrupt').prop('required',false);
	}

});
</script>
<?php if(count($patient_data) > 0 && $patient_data[0]->T_DLL_01_VLC_Result == 1) {?>
	<script>
		$(document).ready(function(){

			$("#lock").click(function(){
				$("#modal_header").text("Contact admin ,for unlocking the record");
				$("#modal_text").text("Contact Admin");
				$("#multipurpose_modal").modal("show");
				return false;

			});

			/*Unlock process start*/

			$("#unlock").click(function(e){

				e.preventDefault(); 
				$("#form-error").html('');
	/*var r = confirm("Do you need to unlock a data ?");
	if (r == true) {*/

		var formData = new FormData($('#unlockModalForm')[0]);
		$.ajax({				    	
			url: '<?php echo base_url(); ?>Unlock/unlock_process_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
			type: 'POST',
			data: formData,
			dataType: 'json',
			async: false,
			cache: false,
			contentType: false,
			processData: false,
			success: function (data) { 
				
				if(data['status'] == 'true'){

					alert('Data has been successfully submitted');
					setTimeout(function() {
						location.href='<?php echo base_url(); ?>patientinfo_hep_b/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
					}, 1000);

				}else{
					$("#form-error").html(data['message']);

					return false;
				}   
			}        
		}); 


	});

			/*end unlock process*/

			/*Disable all input type="text" box*/
			$('#screening input[type="text"]').prop("disabled", true);
			$('#screening input[type="radio"]').prop("disabled", true);
			$('#screening input[type="checkbox"]').prop("disabled", true);
			$('#screening input[type="date"]').prop("readonly", true);
			$("#save").attr("disabled", "disabled");
			$("#refresh").attr("disabled", "disabled");
			$('#screening select').attr('disabled', true);
			/*Disable textarea using id */
			$('#screening #txtAddress').prop("readonly", true);
		});
	</script>
<?php  } ?>

<?php if($result[0]->Screening == 1) {?>
	<script>

		$(document).ready(function(){

			<?php 
			$error = $this->session->flashdata('error'); 

			if($error != null)
			{
				?>
				$("#multipurpose_modal").modal("show");
				<?php 
			}
			?>

			$("#refresh").click(function(e){
				if(confirm('All further details will be deleted.Do you want to continue?')){                 
					$("input").val('');
					$("select").val('');
					$("textarea").val('');
					e.preventDefault();
				}  
			});

			$(".govt_lab").hide();
			$(".lab_name_others").hide();

			<?php if(count($patient_data) == 0){ ?>
				$(".hav_fields").hide();
				$(".hbs_fields").hide();
				$(".hbc_fields").hide();
				$(".hcv_fields").hide();
				$(".hev_fields").hide();
				$(".govt_lab").hide();
				$(".lab_name_others").hide();
			<?php } 
			else
			{
				if($patient_data[0]->LgmAntiHAV == 0)
				{
					?>
					$(".hav_fields").hide();
					<?php 
				}
				else
				{
					?>
					$(".hav_fields").show();
					<?php 
					if ($patient_data[0]->HAVRapidPlace == 1)
					{
						?>
						$(".hav_rapid_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HAVRapidLabID == 999999)
					{
						?>
						$(".hav_rapid_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HAVElisaPlace == 1)
					{
						?>
						$(".hav_elisa_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HAVElisaLabID == 999999)
					{
						?>
						$(".hav_elisa_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HAVOtherPlace == 1)
					{
						?>
						$(".hav_other_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HAVOtherLabID == 999999)
					{
						?>
						$(".hav_other_lab_name_other").show();
						<?php 
					}
				}

				if($patient_data[0]->HbsAg == 0)
				{
					?>
					$(".hbs_fields").hide();
					<?php 
				}
				else
				{
					?>
					$(".hbs_fields").show();
					<?php 
					if ($patient_data[0]->HBSRapidPlace == 1)
					{
						?>
						$(".hbs_rapid_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HBSRapidLabID == 999999)
					{
						?>
						$(".hbs_rapid_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HBSElisaPlace == 1)
					{
						?>
						$(".hbs_elisa_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HBSElisaLabID == 999999)
					{
						?>
						$(".hbs_elisa_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HBSOtherPlace == 1)
					{
						?>
						$(".hbs_other_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HBSOtherLabID == 999999)
					{
						?>
						$(".hbs_other_lab_name_other").show();
						<?php 
					}
				}
				if($patient_data[0]->LgmAntiHBC == 0)
				{
					?>
					$(".hbc_fields").hide();
					<?php 
				}
				else
				{
					?>
					$(".hbc_fields").show();
					<?php 
					if ($patient_data[0]->HBCRapidPlace == 1)
					{
						?>
						$(".hbc_rapid_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HBCRapidLabID == 999999)
					{
						?>
						$(".hbc_rapid_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HBCElisaPlace == 1)
					{
						?>
						$(".hbc_elisa_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HBCElisaLabID == 999999)
					{
						?>
						$(".hbc_elisa_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HBCOtherPlace == 1)
					{
						?>
						$(".hbc_other_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HBCOtherLabID == 999999)
					{
						?>
						$(".hbc_other_lab_name_other").show();
						<?php 
					}
				}
				if($patient_data[0]->AntiHCV == 0)
				{
					?>
					$(".hcv_fields").hide();
					<?php 
				}
				else
				{
					?>
					$(".hcv_fields").show();
					<?php 
					if ($patient_data[0]->HCVRapidPlace == 1)
					{
						?>
						$(".hcv_rapid_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HCVRapidLabID == 999999)
					{
						?>
						$(".hcv_rapid_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HCVElisaPlace == 1)
					{
						?>
						$(".hcv_elisa_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HCVElisaLabID == 999999)
					{
						?>
						$(".hcv_elisa_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HCVOtherPlace == 1)
					{
						?>
						$(".hcv_other_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HCVOtherLabID == 999999)
					{
						?>
						$(".hcv_other_lab_name_other").show();
						<?php 
					}
				}
				if($patient_data[0]->LgmAntiHEV == 0)
				{
					?>
					$(".hev_fields").hide();
					<?php 
				}
				else
				{
					?>
					$(".hev_fields").show();
					<?php 
					if ($patient_data[0]->HEVRapidPlace == 1)
					{
						?>
						$(".hev_rapid_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HEVRapidLabID == 999999)
					{
						?>
						$(".hev_rapid_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HEVElisaPlace == 1)
					{
						?>
						$(".hev_elisa_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HEVElisaLabID == 999999)
					{
						?>
						$(".hev_elisa_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HEVOtherPlace == 1)
					{
						?>
						$(".hev_other_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HEVOtherLabID == 999999)
					{
						?>
						$(".hev_other_lab_name_other").show();
						<?php 
					}
				}
			}
			?>

			$(".virus_checkboxes").change(function(){
				if($('#check_'+$(this).data('test')).is(':checked'))
				{
					$('.'+$(this).data('test')+'_fields').show();
				}
				else
				{




					if($("#hav_rapid").is(":checked") && !$("#check_hav").is(":checked") )	{
						$('#hav_rapid').trigger('click');
					}
					if($("#hav_elisa").is(":checked") && !$("#check_hav").is(":checked") )	{
						$('#hav_elisa').trigger('click');
					}
					if($("#hav_other").is(":checked") && !$("#check_hav").is(":checked") )	{

						$('#hav_other').trigger('click');
					}




					if($("#hbs_rapid").is(":checked") && !$("#check_hbs").is(":checked"))	{

						$('#hbs_rapid').trigger('click');
					}
					if($("#hbs_elisa").is(":checked") && !$("#check_hbs").is(":checked"))	{
						$('#hbs_elisa').trigger('click');
					}
					if($("#hbs_other").is(":checked") && !$("#check_hbs").is(":checked"))	{
						$('#hbs_other').trigger('click');
					}

					if($("#hbc_rapid").is(":checked") && !$("#check_hbc").is(":checked"))	{

						$('#hbc_rapid').trigger('click');
					}
					if($("#hbc_elisa").is(":checked") && !$("#check_hbc").is(":checked"))	{

						$('#hbc_elisa').trigger('click');
					}
					if($("#hbc_other").is(":checked") && !$("#check_hbc").is(":checked"))	{

						$('#hbc_other').trigger('click');
					}


					if($("#hcv_rapid").is(":checked") && !$("#check_hcv").is(":checked"))	{

						$('#hcv_rapid').trigger('click');
					}
					if($("#hcv_elisa").is(":checked") && !$("#check_hcv").is(":checked") )	{

						$('#hcv_elisa').trigger('click');
					}
					if($("#hcv_other").is(":checked") && !$("#check_hcv").is(":checked") )	{

						$('#hcv_other').trigger('click');
					}


					if($("#hev_rapid").is(":checked") && !$("#check_hev").is(":checked") )	{

						$('#hev_rapid').trigger('click');
					}
					if($("#hev_elisa").is(":checked") && !$("#check_hev").is(":checked") )	{
						$('#hev_elisa').trigger('click');
					}
					if($("#hev_other").is(":checked") && !$("#check_hev").is(":checked"))	{

						$('#hev_other').trigger('click');
					}

					$('.'+$(this).data('test')+'_fields').hide();
				}
			});

			$(".test_checkbox").change(function(){

				if($(this).is(':checked'))
				{
					$('.'+$(this).data('test')+'_fields').prop('disabled', false);
					$('.'+$(this).data('test')+'_fields').prop('required', true);
					$('.'+$(this).data('test')+'_required').html('*');
				}
				else
				{
					$('.'+$(this).data('test')+'_fields').prop('disabled', true);

					$('.'+$(this).data('test')+'_fields').prop('required', false);
					$('.'+$(this).data('test')+'_required').html('');

					$('.'+$(this).data('test')+'_govt_lab').hide();
					$('.'+$(this).data('test')+'_lab_name_other').hide();
					$('#'+$(this).data('test')+'_lab_name').prop('required', false);
					$('#'+$(this).data('test')+'_lab_name_other').prop('required', false);

					$('#'+$(this).data('test')+'_date').val('');
					$('#'+$(this).data('test')+'_result').val('');
					$('#'+$(this).data('test')+'_place_of_test').val('');
					$('#'+$(this).data('test')+'_lab_name').val('');
					$('#'+$(this).data('test')+'_lab_name_other').val('');

					if($(this).data('test') == 'hav_other' || 
						$(this).data('test') == 'hbs_other' || 
						$(this).data('test') == 'hbc_other' || 
						$(this).data('test') == 'hcv_other' || 
						$(this).data('test') == 'hev_other')
					{
						$('#'+$(this).data('test')+'_test_name').val('');
					}
				}
			});

			$(".place_of_testing").change(function(){

				if($(this).val() == 1)
				{
					$('.'+$(this).data('test')+'_govt_lab').show();
					$('#'+$(this).data('test')+'_lab_name').prop('required', true);
					$('#'+$(this).data('test')+'_lab_name').prop('disabled', false);
					$('#'+$(this).data('test')+'_lab_name_other').prop('required', false);
				}
				else
				{
					$('.'+$(this).data('test')+'_govt_lab').hide();
					$('#'+$(this).data('test')+'_lab_name').val('');
					$('#'+$(this).data('test')+'_lab_name_other').val('');
					$('#'+$(this).data('test')+'_lab_name').prop('required', false);
					$('#'+$(this).data('test')+'_lab_name').prop('disabled', true);
					$('#'+$(this).data('test')+'_lab_name_other').prop('required', false);
					$('#'+$(this).data('test')+'_lab_name_other').prop('disabled', true);
					$('.'+$(this).data('test')+'_lab_name_other').hide();
				}
			});

			$(".lab_names").change(function(){

				if($(this).val() == 999999)
				{
					$('.'+$(this).data('test')+'_lab_name_other').show();
					$('#'+$(this).data('test')+'_lab_name_other').prop('required', true);
					$('#'+$(this).data('test')+'_lab_name_other').prop('disabled', false);
				}
				else
				{
					$('.'+$(this).data('test')+'_lab_name_other').hide();
					$('#'+$(this).data('test')+'_lab_name_other').val('');
					$('#'+$(this).data('test')+'_lab_name_other').prop('required', false);
					$('#'+$(this).data('test')+'_lab_name_other').prop('disabled', true);
				}
			});

			$("#screening").submit(function(e){

				if(
					!$("#check_hav").is(":checked") &&
					!$("#check_hbs").is(":checked") &&
					!$("#check_hbc").is(":checked") &&
					!$("#check_hcv").is(":checked") &&
					!$("#check_hev").is(":checked"))
				{
					$("#modal_header").text("Please Select a Test Type");
					$("#modal_text").text("Please fill details before submitting");
					$("#multipurpose_modal").modal("show");
					e.preventDefault();
				}

				if($("#check_hav").is(":checked") && !$("#hav_rapid").is(":checked") && !$("#hav_elisa").is(":checked") && !$("#hav_other").is(":checked"))
				{
					$("#modal_header").text("Please Select a Test From HAV");
					$("#modal_text").text("Please fill details before submitting");
					$("#multipurpose_modal").modal("show");
					e.preventDefault();
					return false;
				}

				if($("#check_hbs").is(":checked") && !$("#hbs_rapid").is(":checked") && !$("#hbs_elisa").is(":checked") && !$("#hbs_other").is(":checked"))
				{
					$("#modal_header").text("Please Select a Test From HBS");
					$("#modal_text").text("Please fill details before submitting");
					$("#multipurpose_modal").modal("show");
					e.preventDefault();
					return false;
				}

				if($("#check_hbc").is(":checked") && !$("#hbc_rapid").is(":checked") && !$("#hbc_elisa").is(":checked") && !$("#hbc_other").is(":checked"))
				{
					$("#modal_header").text("Please Select a Test From HBC");
					$("#modal_text").text("Please fill details before submitting");
					$("#multipurpose_modal").modal("show");
					e.preventDefault();
					return false;
				}

				if($("#check_hcv").is(":checked") && !$("#hcv_rapid").is(":checked") && !$("#hcv_elisa").is(":checked") && !$("#hcv_other").is(":checked"))
				{
					$("#modal_header").text("Please Select a Test From HCV");
					$("#modal_text").text("Please fill details before submitting");
					$("#multipurpose_modal").modal("show");
					e.preventDefault();
					return false;
				}

				if($("#check_hev").is(":checked") && !$("#hev_rapid").is(":checked") && !$("#hev_elisa").is(":checked") && !$("#hev_other").is(":checked"))
				{
					$("#modal_header").text("Please Select a Test From HEV");
					$("#modal_text").text("Please fill details before submitting");
					$("#multipurpose_modal").modal("show");
					e.preventDefault();
					return false;
				}
			});
		});
	</script>
<?php } else { ?>
	<script>
		$(document).ready(function(){
			$('input').prop('disabled', true);
			$('select').prop('disabled', true);

			<?php 
			$error = $this->session->flashdata('error'); 

			if($error != null)
			{
				?>
				$("#multipurpose_modal").modal("show");
				<?php 
			}
			?>

			$(".govt_lab").hide();
			$(".lab_name_others").hide();

			<?php if(count($patient_data) == 0){ ?>
				$(".hav_fields").hide();
				$(".hbs_fields").hide();
				$(".hbc_fields").hide();
				$(".hcv_fields").hide();
				$(".hev_fields").hide();
				$(".govt_lab").hide();
				$(".lab_name_others").hide();
			<?php } 
			else
			{
				if($patient_data[0]->LgmAntiHAV == 0)
				{
					?>
					$(".hav_fields").hide();
					<?php 
				}
				else
				{
					?>
					$(".hav_fields").show();
					<?php 
					if ($patient_data[0]->HAVRapidPlace == 1)
					{
						?>
						$(".hav_rapid_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HAVRapidLabID == 999999)
					{
						?>
						$(".hav_rapid_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HAVElisaPlace == 1)
					{
						?>
						$(".hav_elisa_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HAVElisaLabID == 999999)
					{
						?>
						$(".hav_elisa_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HAVOtherPlace == 1)
					{
						?>
						$(".hav_other_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HAVOtherLabID == 999999)
					{
						?>
						$(".hav_other_lab_name_other").show();
						<?php 
					}
				}

				if($patient_data[0]->HbsAg == 0)
				{
					?>
					$(".hbs_fields").hide();
					<?php 
				}
				else
				{
					?>
					$(".hbs_fields").show();
					<?php 
					if ($patient_data[0]->HBSRapidPlace == 1)
					{
						?>
						$(".hbs_rapid_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HBSRapidLabID == 999999)
					{
						?>
						$(".hbs_rapid_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HBSElisaPlace == 1)
					{
						?>
						$(".hbs_elisa_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HBSElisaLabID == 999999)
					{
						?>
						$(".hbs_elisa_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HBSOtherPlace == 1)
					{
						?>
						$(".hbs_other_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HBSOtherLabID == 999999)
					{
						?>
						$(".hbs_other_lab_name_other").show();
						<?php 
					}
				}
				if($patient_data[0]->LgmAntiHBC == 0)
				{
					?>
					$(".hbc_fields").hide();
					<?php 
				}
				else
				{
					?>
					$(".hbc_fields").show();
					<?php 
					if ($patient_data[0]->HBCRapidPlace == 1)
					{
						?>
						$(".hbc_rapid_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HBCRapidLabID == 999999)
					{
						?>
						$(".hbc_rapid_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HBCElisaPlace == 1)
					{
						?>
						$(".hbc_elisa_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HBCElisaLabID == 999999)
					{
						?>
						$(".hbc_elisa_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HBCOtherPlace == 1)
					{
						?>
						$(".hbc_other_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HBCOtherLabID == 999999)
					{
						?>
						$(".hbc_other_lab_name_other").show();
						<?php 
					}
				}
				if($patient_data[0]->AntiHCV == 0)
				{
					?>
					$(".hcv_fields").hide();
					<?php 
				}
				else
				{
					?>
					$(".hcv_fields").show();
					<?php 
					if ($patient_data[0]->HCVRapidPlace == 1)
					{
						?>
						$(".hcv_rapid_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HCVRapidLabID == 999999)
					{
						?>
						$(".hcv_rapid_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HCVElisaPlace == 1)
					{
						?>
						$(".hcv_elisa_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HCVElisaLabID == 999999)
					{
						?>
						$(".hcv_elisa_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HCVOtherPlace == 1)
					{
						?>
						$(".hcv_other_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HCVOtherLabID == 999999)
					{
						?>
						$(".hcv_other_lab_name_other").show();
						<?php 
					}
				}
				if($patient_data[0]->LgmAntiHEV == 0)
				{
					?>
					$(".hev_fields").hide();
					<?php 
				}
				else
				{
					?>
					$(".hev_fields").show();
					<?php 
					if ($patient_data[0]->HEVRapidPlace == 1)
					{
						?>
						$(".hev_rapid_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HEVRapidLabID == 999999)
					{
						?>
						$(".hev_rapid_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HEVElisaPlace == 1)
					{
						?>
						$(".hev_elisa_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HEVElisaLabID == 999999)
					{
						?>
						$(".hev_elisa_lab_name_other").show();
						<?php 
					}

					if ($patient_data[0]->HEVOtherPlace == 1)
					{
						?>
						$(".hev_other_govt_lab").show();
						<?php 
					}
					if ($patient_data[0]->HEVOtherLabID == 999999)
					{
						?>
						$(".hev_other_lab_name_other").show();
						<?php 
					}
				}
			}
			?>
		});


// $(document).ready(function(){
// <?php if(count($patient_data) > 0 && $patient_data[0]->IsSampleAccepted == 2) { ?>
// $("#modal_header").html('Further entry is not allowed, as per treatment status...');
// <?php } ?>


/*Max Date Today Code*/
$(function(){
	$('[type="date"]').prop('max', function(){
		return new Date().toJSON().split('T')[0];
	});
});


$(function(){
	$('[type="date"]').prop('max', function(){	
		return new Date().toJSON().split('T')[0];
	});
});

/*Max Date Today Code*/
var today = new Date();
var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!

			var yyyy = today.getFullYear();
			if(dd<10){
				dd='0'+dd;
			} 
			if(mm<10){
				mm='0'+mm;	
			}

			today = yyyy+'-'+mm+'-'+dd;
			document.getElementById("hav_rapid_date").setAttribute("max", today);
			document.getElementById("hbs_rapid_date").setAttribute("max", today);
			document.getElementById("hbc_rapid_date").setAttribute("max", today);
			document.getElementById("hcv_rapid_date").setAttribute("max", today);
			document.getElementById("hev_rapid_date").setAttribute("max", today);


		</script>
	<?php } ?>
