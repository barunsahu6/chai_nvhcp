<style>
thead
{
	background-color: #085786;
	color: white;
}

</style>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 5 !important;
	width: 100% !important;
	font-size: 14px !important;
	font-family: 'Source Sans Pro';
}
textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
	.col-md-2{
		width: 16.33%;
		padding-left: 8px;
		padding-right: 8px;
	}
	.btn-width{
	width: 12%;
	margin-top: 20px;
}
.pd-15{
	padding-left: 15px;
}
.pb-20{
	padding-bottom: 20px;
}
.container{
	width: 76%;
}
}

@media (max-width: 768px) {
.input_fields
	{
		height: 40px !important;
		border-radius: 5 !important;
		width: 100% !important;
		font-size: 14px !important;
		font-family: 'Source Sans Pro';
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 13px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
 .table-bordered>thead>tr>th{
	vertical-align: middle;
	font-size: 13px;
	font-family: 'Source Sans Pro';
	background-color: #085786;
}
.table-bordered>tbody>tr>td{
	vertical-align: middle;
	font-size: 13px;
	color: #000000;
	font-family: 'Source Sans Pro';
}
.table-bordered>tbody>tr>td.data_td
{
	font-weight: 600;
	font-size: 15px;
	text-align: center;
	vertical-align: middle;
}
</style>
<br>

<?php $loginData = $this->session->userdata('loginData');
//echo "<pre>"; print_r($loginData);

if($loginData->user_type==1) { 
$select = '';
}else{
$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
$select4 = '';	
}
 ?>
<div class="container">
<div class="row">
	<div class="panel panel-default" id="HMIS_Report_panel">
			<div class="panel-heading" style="background-color: #333333;padding: 3px 0px;">
					<h4 class="text-center" style="background-color: #333333;color: white;font-family: 'Source Sans Pro';letter-spacing: .75px;">HMIS Indicators for National Viral Hepatitis Control Program	
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/excel_black.png" alt=""></a> -->
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/pdf_color.png" alt=""></a> -->
	</h4>
</div>
<br>

<?php
           $attributes = array(
              'id' => 'filter_form',
              'name' => 'filter_form',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>
<div class="row" style="padding-left: 15px;">
	<div class="col-md-2 col-sm-12">
		<label for="">State</label>
		<select type="text" name="search_state" id="search_state" class="form-control input_fields" required <?php echo $select2.$select3.$select4; ?>>
			<option value="0">All States</option>
			<?php 
			foreach ($states as $state) {
				?>
				<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo $state->StateName; ?></option>
				<?php 
			}
			?>
		</select>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">District</label>
		<select type="text" name="input_district" id="input_district" class="form-control input_fields"  <?php echo $select.$select2.$select4; ?>>
			<option value="">Select District</option>
			<?php 
			
			?>
		</select>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">Facility</label>
		<select type="text" name="facility" id="mstfacilitylogin" class="form-control input_fields"  <?php echo $select.$select2; ?>>
			<option value="">Select Facility</option>
			<?php 
			
			?>
		</select>
	</div>


	<div class="col-md-2 col-sm-12">
		<label for="">From Date</label>
		<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')) { echo $this->input->post('startdate'); }else{ echo timeStampShow(date('Y-m-01')); } ?>" required>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">To date</label>
		<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')) { echo $this->input->post('enddate'); }else{  echo timeStampShow(date('Y-m-d'));} ?>" required>
	</div>
<div class="col-lg-2 col-md-2 col-sm-12 btn-width">
					<button type="submit" class="btn btn-block btn-success" style="line-height: 1.2; font-weight: 600;">SEARCH</button>
				</div><span style="margin-top: 25px;margin-right: 25px; float: right;"><a href="javascript:void(0)"><i class="fa fa-2x fa-download" id="excel_button" onclick="tableToExcel('testTable', 'W3C Example Table','HMIS_Report.xls')" title="Excel Download"></i></a></span>
			</div>
<!-- <div class="row">
    <div class="col-xs-2 pull-right ">
        <div class="text-right">
        	<label for="">&nbsp;</label>
            <button type="submit" class="btn btn-block btn-success" style="line-height: 1.2; font-weight: 600;">SEARCH</button>
        </div>
    </div>
</div> -->

 <?php echo form_close(); ?>
<br>
</div>
<div class="row">
	<div class="col-md-12 col-sm-12">

		<table class="table table-bordered table-highlighted" id="testTable">
			<thead>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';">S.No</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">Indicators </th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">&nbsp;&nbsp;&nbsp;SC&nbsp;&nbsp;&nbsp;</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">&nbsp;&nbsp;&nbsp;PHC&nbsp;&nbsp;&nbsp;</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">&nbsp;&nbsp;&nbsp;CHC&nbsp;&nbsp;&nbsp;</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">&nbsp;&nbsp;&nbsp;SDH&nbsp;&nbsp;&nbsp;</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">&nbsp;&nbsp;&nbsp;DH&nbsp;&nbsp;&nbsp;</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;">&nbsp;&nbsp;&nbsp;Others&nbsp;&nbsp;&nbsp;</th>
				
	<?php 
$dh_totalsc = $get_hepc_dh_others['hepadh'][0]->dh  + $get_hepc_dh_others['hepbdh'][0]->dh  + $get_hepc_dh_others['dh'][0]->dh  + $get_hepc_dh_others['hepedh'][0]->dh ;

$others_totalsc =  $get_hepc_dh_others['hepaothers'][0]->others + $get_hepc_dh_others['hepbothers'][0]->others +  $get_hepc_dh_others['others'][0]->others + $get_hepc_dh_others['hepeothers'][0]->others;


$dh_totalhepb =$get_hepc_positive_dh_others['hepadh'][0]->dh + $get_hepc_positive_dh_others['hepbdh'][0]->dh + $get_hepc_positive_dh_others['dh'][0]->dh + $get_hepc_positive_dh_others['hepedh'][0]->dh;

$others_totalhepb = $get_hepc_positive_dh_others['hepaothers'][0]->others + $get_hepc_positive_dh_others['hepbothers'][0]->others + $get_hepc_positive_dh_others['others'][0]->others + $get_hepc_positive_dh_others['hepeothers'][0]->others;

?>			
				
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>Total number of blood samples screened by ELISA/Rapid Other tests for viral hepatitis </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> <?php echo $dh_totalsc; ?> </td>
					<td class="data_td"> <?php echo $others_totalsc; ?> </td>
					
					
				</tr>

				<tr>
					<td>1a.</td>
					<td>Hepatitis A </td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td"><?php echo !empty($get_hepc_dh_others['hepadh'][0]->dh) ? $get_hepc_dh_others['hepadh'][0]->dh : "-"; ?></td>
					<td class="data_td"><?php echo !empty($get_hepc_dh_others['hepaothers'][0]->others) ? $get_hepc_dh_others['hepaothers'][0]->others : "-"; ?></td>
					
				</tr>
				<tr>
					<td>1b.</td>
					<td>Hepatitis B </td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td"><?php echo !empty($get_hepc_dh_others['hepbdh'][0]->dh) ? $get_hepc_dh_others['hepbdh'][0]->dh : "-"; ?></td>
					<td class="data_td"><?php echo !empty($get_hepc_dh_others['hepbothers'][0]->others) ? $get_hepc_dh_others['hepbothers'][0]->others : "-"; ?></td>
					
				</tr>
				<tr>
					<td>1c.</td>
					<td>Hepatitis C </td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td"><?php echo !empty($get_hepc_dh_others['dh'][0]->dh) ? $get_hepc_dh_others['dh'][0]->dh : "-";?></td>
					<td class="data_td"><?php echo !empty($get_hepc_dh_others['others'][0]->others) ? $get_hepc_dh_others['others'][0]->others : "-";?></td>
					
				</tr>
				<tr>
					<td>1d.</td>
					<td>Hepatitis E </td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td"><?php echo !empty($get_hepc_dh_others['hepedh'][0]->dh) ? $get_hepc_dh_others['hepedh'][0]->dh : "-"; ?></td>
					<td class="data_td"><?php echo !empty($get_hepc_dh_others['hepeothers'][0]->others) ? $get_hepc_dh_others['hepeothers'][0]->others : "-"; ?></td>
					
				</tr>

				<tr>
					<td>2.</td>
					<td>Total number of blood samples tested positive by ELISA/ Rapid /Other tests for viral hepatitis </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> <?php echo $dh_totalhepb; ?> </td>
					<td class="data_td"> <?php echo $others_totalhepb; ?> </td>
					
				</tr>

				<tr>
					<td>2a.</td>
					<td>Hepatitis A </td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td"><?php echo !empty($get_hepc_positive_dh_others['hepadh'][0]->dh) ? $get_hepc_positive_dh_others['hepadh'][0]->dh : "-"; ?></td>
					<td class="data_td"><?php echo !empty($get_hepc_positive_dh_others['hepaothers'][0]->others) ? $get_hepc_positive_dh_others['hepaothers'][0]->others : "-"; ?></td>
					
				</tr>

				<tr>
					<td>2b.</td>
					<td>Hepatitis B </td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td"><?php echo !empty($get_hepc_positive_dh_others['hepbdh'][0]->dh) ? $get_hepc_positive_dh_others['hepbdh'][0]->dh : "-"; ?></td>
					<td class="data_td"><?php echo !empty($get_hepc_positive_dh_others['hepbothers'][0]->others) ? $get_hepc_positive_dh_others['hepbothers'][0]->others : "-"; ?></td>
					
				</tr>
				<tr>
					<td>2c.</td>
					<td>Hepatitis C </td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td"><?php echo !empty($get_hepc_positive_dh_others['dh'][0]->dh) ? $get_hepc_positive_dh_others['dh'][0]->dh : "-";?></td>
					<td class="data_td"><?php echo !empty($get_hepc_positive_dh_others['others'][0]->others) ? $get_hepc_positive_dh_others['others'][0]->others : "-";?></td>
					
				</tr>
				<tr>
					<td>2d.</td>
					<td>Hepatitis E </td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td">-</td>
					<td class="data_td"><?php echo !empty($get_hepc_positive_dh_others['hepedh'][0]->dh) ? $get_hepc_positive_dh_others['hepedh'][0]->dh : "-"; ?></td>
					<td class="data_td"><?php echo !empty($get_hepc_positive_dh_others['hepeothers'][0]->others) ? $get_hepc_positive_dh_others['hepeothers'][0]->others : "-"; ?></td>
					
				</tr>

				<tr>
					<td>3.</td>
					<td>Total number of positive blood samples for Hepatitis C by screening test (ELISA/ Rapid /Other tests) confirmed by HCV RNA testing (Patients eligible for treatment for hepatitis C)  </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"><?php echo !empty($hims_hcv_vl_detected['dh'][0]->dh_viral_load_detected) ? $hims_hcv_vl_detected['dh'][0]->dh_viral_load_detected : "-"; ?></td>
					<td class="data_td"><?php echo !empty($hims_hcv_vl_detected['others'][0]->other_hcv_vl_detected) ? $hims_hcv_vl_detected['others'][0]->other_hcv_vl_detected : "-"; ?></td>
		
				</tr>

				<tr>
					<td>4.</td>
					<td>Total number of positive blood samples for hepatitis B by ELISA/ Rapid /Other tests tested for HBV DNA </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"><?php echo !empty($hep_b_confirmatory['dh'][0]->count) ? $hep_b_confirmatory['dh'][0]->count : "-"; ?></td>
					<td class="data_td"><?php echo !empty($hep_b_confirmatory['others'][0]->count) ? $hep_b_confirmatory['others'][0]->count : "-"; ?></td>				
				</tr>

				<tr>
					<td>5.</td>
					<td>Total number of patients put on treatment for Hepatitis C </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"><?php echo !empty($firstst_dispensation['dh'][0]->count) ? $firstst_dispensation['dh'][0]->count : " - "; ?></td>	
					<td class="data_td"><?php echo !empty($firstst_dispensation['others'][0]->count) ? $firstst_dispensation['others'][0]->count : " - "; ?></td>				
				</tr>
				<tr>
					<td>6.</td>
					<td>Total number of positive Hepatitis C patients who have completed treatment<div class="pull-right text-right glyphicon glyphicon-info-sign" style="font-size: 14px; padding: 0px;"  data-toggle="tooltip" title="" data-original-title="Number of patients that have completed their treatment regimen (Pre-SVR)"> </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"><?php echo !empty($etr_during['dh'][0]->count) ? $etr_during['dh'][0]->count : " - "; ?></td>
					<td class="data_td"><?php echo !empty($etr_during['others'][0]->count) ? $etr_during['others'][0]->count : " - "; ?></td>
					
	</tr>
				<tr>
					<td>7.</td>
					<td>Total number of patients cleared for HCV RNA on sustained virological response at 12 weeks (SVR12) </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"><?php echo !empty($svr_not_detected['dh'][0]->count) ? $svr_not_detected['dh'][0]->count : " - "; ?></td>
					<td class="data_td"><?php echo !empty($svr_not_detected['others'][0]->count) ? $svr_not_detected['others'][0]->count : " - "; ?></td>
		
				</tr>

				<tr>
					<td>8.</td>
					<td>Total number of patients found positive for HBsAg eligible for treatment for hepatitis B </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"><?php echo !empty($hbv_detected['dh'][0]->count) ? $hbv_detected['dh'][0]->count : " - "; ?></td>
					<td class="data_td"><?php echo !empty($hbv_detected['others'][0]->count) ? $hbv_detected['others'][0]->count : " - "; ?></td>
					
				</tr>

				<tr>
					<td>9.</td>
					<td>Total number of patients eligible for treatment for Hepatitis B put on treatment  </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"> - </td>
					<td class="data_td"><?php echo !empty($hbs_elegible['dh'][0]->count) ? $hbs_elegible['dh'][0]->count : " - "; ?></td>
					<td class="data_td"><?php echo !empty($hbs_elegible['others'][0]->count) ? $hbs_elegible['others'][0]->count : " - "; ?></td>
					
				</tr>
				
				
			</tbody>
		
			
		</table>
	</div>
</div>
</div>
<br>
<br>
<br>
<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name,filename) {
    if (!table.nodeType) table = document.getElementById(table)
    var newtable=document.createElement("table");
            var newtable=document.createElement("table");
            var tbody =document.createElement("tbody");
             var row = document.createElement("tr");
             row.setAttribute("style", "background-color: black;color: white; font-family:Calibri;");
  				var cell1 = document.createElement("td");
  				cell1.colSpan=7;
  				cell1.innerHTML ="Data as on : - <?php echo date('jS \ F Y'); ?>";
  				row.appendChild(cell1);
 			 	tbody.appendChild(row);
 			 	newtable.appendChild(tbody);
             var clonedTable=newtable.cloneNode(true);
             var clonedTable1=table.cloneNode(true);
            clonedTable.appendChild(clonedTable1);
    var ctx = {worksheet: name || 'Worksheet', table: clonedTable.innerHTML}
     var link = document.createElement('a');
    link.download = filename;
    link.href = uri + base64(format(template, ctx));
    link.click();
  }
})()
</script>
<script>
	
$(document).ready(function(){
$("#search_state").trigger('change');
$('[data-toggle="tooltip"]').tooltip();


});
$('#search_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');

				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
					$('#mstfacilitylogin').html(data);
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});

</script>