<?php 
$loginData = $this->session->userdata('loginData');

$sql = "SELECT Dispense FROM `MSTRole` where RoleId = ".$loginData->RoleId;
$result = $this->db->query($sql)->result();

 $visit_countval = count($visit_count);
 //echo $visit_countval .'>='. $visit;
$InterruptReasonCount = (count($patient_data) > 0 && $patient_data[0]->InterruptReason);
?>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #A30A0C !important;
	background-color: white !important;
	border : 1px solid #A30A0C;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
.hasDatepicker[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
</style>

<br>

<div class="row equal">
	<!-- <form action="" name="patient_form" id="patient_form" method="POST"> -->
				<!-- <form action="" method="POST" name="registration" id="registration"> -->
			<?php
           $attributes = array(
              'id' => 'patient_form',
              'name' => 'patient_form',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>

		<input type="hidden" name="fetch_uid" id="fetch_uid">
		          <?php echo form_close(); ?>
	<!-- </form> -->
	<div class="col-lg-10 col-lg-offset-1">

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="btn-group">
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">1. Registration</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">2. Screening</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">3. Viral Load</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">4. Testing</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/known_history/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">5. Known History</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">6. Prescription</a>
					<a class="btn btn-success" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">7. Dispensation</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">8. SVR</a>
				</div>
			</div>
		</div>

		<!-- <form action="" method="POST" name="registration" id="registration"> -->
						<?php
           $attributes = array(
              'id' => 'registration',
              'name' => 'registration',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>
			<div class="row">
				<div class="col-md-12">
					<h4 class="text-center"><p>Patient Name - <strong><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FirstName)):''; ?></strong>  (<?php echo (count($patient_data) > 0)?$patient_data[0]->UID_Prefix:$uid_prefix; echo '-' .str_pad($patient_data[0]->UID_Num, 6, '0', STR_PAD_LEFT); ?>)<p></h4>
					<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Patient Dispensation Module <?php echo set_hepc(); ?></h3>
				</div>
			</div>
			<br>
			
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="btn-group">
						<?php if(count($patient_data) > 0 and $patient_data[0]->T_DurationValue == 12){  ?>
						<a class="btn btn-success" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/1">1<sup>st</sup> Rx</a>
						<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/2">2<sup>nd</sup> Rx</a>
						<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/3">3<sup>rd</sup> Rx</a>
					<?php } ?>
						<?php if(count($patient_data) > 0 and $patient_data[0]->T_DurationValue == 24){  ?>
							<a class="btn btn-success" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/1">1<sup>st</sup> Rx</a>
						<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/2">2<sup>nd</sup> Rx</a>
						<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/3">3<sup>rd</sup> Rx</a>
							<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/<?php echo $patient_data[0]->PatientGUID; ?>/4">4<sup>th</sup> Rx</a>
							<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/<?php echo $patient_data[0]->PatientGUID; ?>/5">5<sup>th</sup> Rx</a>
							<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/<?php echo $patient_data[0]->PatientGUID; ?>/6">6<sup>th</sup> Rx</a>
						<?php } ?>
						<?php if(count($patient_data) > 0 and $patient_data[0]->T_DurationOther == 1 &&  $patient_data[0]->T_DurationValue == 99 ){  ?>
						<a class="btn btn-success" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/1">1<sup>st</sup> Rx</a>
						
					<?php } ?>
					<?php if(count($patient_data) > 0 and $patient_data[0]->T_DurationOther == 2 && $patient_data[0]->T_DurationValue == 99){  ?>
						<a class="btn btn-success" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/1">1<sup>st</sup> Rx</a>
						<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/2">2<sup>nd</sup> Rx</a>
						
					<?php } ?>
					<?php if(count($patient_data) > 0 and $patient_data[0]->T_DurationOther == 3){  ?>
						<a class="btn btn-success" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/1">1<sup>st</sup> Rx</a>
						<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/2">2<sup>nd</sup> Rx</a>
						<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/3">3<sup>rd</sup> Rx</a>
						<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/4">4<sup>rd</sup> Rx</a>
					<?php } ?>
					<?php if(count($patient_data) > 0 and $patient_data[0]->T_DurationOther == 4){  ?>
						<a class="btn btn-success" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/1">1<sup>st</sup> Rx</a>
						<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/2">2<sup>nd</sup> Rx</a>
						<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/3">3<sup>rd</sup> Rx</a>
						<a class="btn <?php echo ($visit == '4')?'btn-success':'btn-default'; ?>" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo $patient_data[0]->PatientGUID; ?>/4">4<sup>th</sup> Rx</a>
						<a class="btn <?php echo ($visit == '5')?'btn-success':'btn-default'; ?>" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo $patient_data[0]->PatientGUID; ?>/5">5<sup>th</sup> Rx</a>
					<?php } ?>
						<a class="btn btn-default" style="line-height: 1.4; font-size: 13px;" href="<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/eot">EoT</a>
					</div>
				</div>
			</div>
			<hr style="border-top: 2px solid #000; margin-left: 30px; margin-right: 30px;">
			<br>
			<div class="row">
			<!-- 	<div class="col-md-3">
					<label for="">Duration <span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="duration" name="duration" required="">
						<option value="">Select</option>
						<?php foreach ($duration as $row) {?>
							<option <?php echo (count($patient_data) > 0 && $patient_data[0]->T_DurationValue == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
						<?php } ?>
					</select>
				</div> -->

				<div class="col-md-3">
						<label for="">Total Regimen Duration(Weeks)</label>
						<select class="form-control input_fields" id="duration" name="duration" disabled="">
							<option value="">Select</option>
							<?php if($patient_data[0]->T_DurationValue == 12 || $patient_data[0]->T_DurationValue ==24){  ?>
							<option value="12" <?php echo (count($patient_data) > 0 && $patient_data[0]->T_DurationValue == 12)?'selected':''; ?>>12</option>
							<option value="24" <?php echo (count($patient_data) > 0 && $patient_data[0]->T_DurationValue == 24)?'selected':''; ?>>24</option>
						<?php } else { ?>
							<option value="99" <?php if($patient_data[0]->T_DurationValue != 12 || $patient_data[0]->T_DurationValue !=24){ echo 'selected';}else{ echo '';} ?>>Other</option>
						<?php  } ?>
						</select>
					</div>


					<div class="col-md-3 DurationShow">
						<label for="">Other Duration (Weeks)<span class="text-danger">*</span></label>
						<select class="form-control input_fields" id="durationother" name="durationother" disabled="">
							<option value="">Select</option>
							<?php foreach ($duration as $row) {?>
							<option <?php echo (count($patient_data) > 0 && $patient_data[0]->T_DurationOther == $row->LookupCode)?'selected':''; ?> value="<?php echo $row->LookupCode; ?>"><?php echo $row->LookupValue; ?></option>
						<?php } ?>

						</select>
					</div>

				<div class="col-md-3">
					<label for="">Date Of Treatment Initiation <span class="text-danger">*</span></label>
					<input type="text" name="date_treatment_initiation" id="date_treatment_initiation" class="input_fields form-control hasCal dateInpt input2"  value="<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->T_Initiation):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" required="" >
					<br class="hidden-lg-*">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4" style="background-color: lightblue; padding-top: 15px; padding-bottom: 15px;">
					<label for="">Regimen Prescribed <span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="regimen_prescribed" name="regimen_prescribed" required="" disabled="">
						<option value="">Select</option>
						<?php foreach ($regimen_prescribed as $row) {?>
							<option <?php echo (count($patient_data) > 0 && $patient_data[0]->T_Regimen == $row->id_mst_regimen)?'selected':''; ?> value="<?php echo $row->id_mst_regimen; ?>"><?php echo $row->regimen_name; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-3 sofosbuvir_field" style="background-color: lightblue; padding-top: 15px; padding-bottom: 15px;">
					<label for="">Sofosbuvir<span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="sofosbuvir" name="sofosbuvir" disabled="">
						<option value="">Select</option>
						<?php foreach ($mst_drug_strength as  $value) { ?>
							
							<option value="<?php  echo $value->id_mst_drug_strength; ?>" <?php if($regimen_drug_data[0]->id_mst_drug_strength ==  $value->id_mst_drug_strength ){ echo 'selected';}  ?>><?php  echo $value->strength; ?> mg</option>
							<?php } ?>
					</select>
				</div>
				<div class="col-md-3 daclatasvir_field" style="background-color: lightblue; padding-top: 15px; padding-bottom: 15px;">
					<label for="">Daclatasvir<span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="daclatasvir" name="daclatasvir" disabled="">
						<option value="">Select</option>
						<?php foreach ($mst_drug_Daclatasvir as  $value) { ?>
							
							<option value="<?php  echo $value->id_mst_drug_strength; ?>" <?php if($regimen_drug_data[1]->id_mst_drug_strength ==  $value->id_mst_drug_strength ){ echo 'selected';}  ?>><?php  echo $value->strength; ?> mg</option>
							<?php } ?>
					</select>
				</div>
				<div class="col-md-3 velpatasvir_field" style="background-color: lightblue; padding-top: 15px; padding-bottom: 15px;">
					<label for="">Velpatasvir<span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="velpatasvir" name="velpatasvir" disabled="">
						<option value="">Select</option>
						<?php foreach ($mst_drug_Velpatasvir as  $value) { ?>
							<option value="<?php  echo $value->id_mst_drug_strength; ?>" <?php if($regimen_drug_data[1]->id_mst_drug_strength ==  $value->id_mst_drug_strength ){ echo 'selected';}  ?>><?php  echo $value->strength; ?> mg</option>
						<?php  } ?>
					</select>
				</div>
				<div class="col-md-3 ledipasvir_field" style="background-color: lightblue; padding-top: 15px; padding-bottom: 15px;">
						<label for="">Ledipasvir </label>
						<select class="form-control input_fields" id="ledipasvir" name="sofosbuvir[]" disabled="">
							<option value="">Select</option> 
							<?php foreach ($mst_drug_Ledipasvir as  $value) { ?>
							<option value="<?php  echo $value->id_mst_drug_strength; ?>" <?php if(count($regimen_drug_data) >0){ if($regimen_drug_data[1]->id_mst_drug_strength ==  $value->id_mst_drug_strength ){ echo 'selected';} } ?>><?php  echo $value->strength; ?> mg</option>
						<?php  } ?>
						</select>
					</div>
				<div class="col-md-3 ribavrin_field" style="background-color: lightblue; padding-top: 15px; padding-bottom: 15px;">
					<label for="">Ribavrin<span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="ribavrin" name="ribavrin" disabled="">
						<option value="">Select</option>
							<?php foreach ($mst_drug_Ribavrin as  $value) { ?>
							<option value="<?php  echo $value->id_mst_drug_strength; ?>" <?php if($regimen_drug_data[2]->id_mst_drug_strength ==  $value->id_mst_drug_strength ){ echo 'selected';}  ?>><?php  echo $value->strength; ?> mg</option>
						<?php  } ?>
					</select>
				</div>
			</div>
			<br>
			<input type="hidden" name="interruption_status" id="interruption_status" value="<?php  echo (count($patient_data) > 0)?$patient_data[0]->InterruptReason:'';  ?>">
			<div class="row">
				<div class="col-md-3 new_place">
					<label for="">Place Of Dispensation<span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="place_of_dispensation" name="place_of_dispensation" required="" readonly>
						<option value="">Select</option>
						<?php error_reporting(0); if(count($patient_data) > 0 && !empty($patient_data[0]->ReferTo ) ) { ?>
							<option value="<?php  echo $ReferToFaci[0]->id_mstfacility; ?>" <?php if($patient_data[0]->PrescribingFacility == $ReferToFaci[0]->id_mstfacility){ echo "selected"; }  ?>><?php echo $ReferToFaci[0]->FacilityCode; ?></option>

						<?php } ?>
						<?php foreach ($place_of_dispensation as $row) {?>
							<option <?php echo (count($patient_data) > 0 && $patient_data[0]->DispensationPlace == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-3 new_dplace">
					<label for="">Place Of Dispensation<span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="place_of_dispensation" name="place_of_dispensation" required="">
						<option value="">Select</option>
						<?php error_reporting(0); if(count($patient_data) > 0 && !empty($patient_data[0]->ReferTo ) ) { ?>
							<option value="<?php  echo $ReferToFaci[0]->id_mstfacility; ?>" <?php if($patient_data[0]->PrescribingFacility == $ReferToFaci[0]->id_mstfacility){ echo "selected"; }  ?>><?php echo $ReferToFaci[0]->FacilityCode; ?></option>

						<?php } ?>
						<?php foreach ($place_of_dispensation as $row) {?>
							<option <?php echo (count($patient_data) > 0 && $patient_data[0]->DispensationPlace == $row->id_mstfacility)?'selected':''; ?> value="<?php echo $row->id_mstfacility; ?>"><?php echo $row->FacilityCode; ?><?php echo $row->name; ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-3">
					<label for="">Days of Pills Dispensed</label>
					<!-- <input type="text" name="pills_dispensed" id="pills_dispensed" class="input_fields form-control" value="<?php echo 28;//if($patient_data[0]->ETRComments == "") { echo $patient_data[0]->ETRComments; } else { echo '28';}   ?>" > -->
					<?php if($patient_data[0]->T_DurationValue==12){ ?>
					<select name="pills_dispensed" id="pills_dispensed" class="input_fields form-control" required="">
						<option value="28" <?php echo (count($patient_data) > 0 && $patient_data[0]->T_NoPillStart == 28)?'selected':''; ?>>28</option>
						<option value="56" <?php echo (count($patient_data) > 0 && $patient_data[0]->T_NoPillStart == 56)?'selected':''; ?>>56</option>
						<option value="84" <?php echo (count($patient_data) > 0 && $patient_data[0]->T_NoPillStart == 84)?'selected':''; ?>>84</option>
					</select>
				<?php  } if($patient_data[0]->T_DurationValue==24){ ?>
					<select name="pills_dispensed" id="pills_dispensed" class="input_fields form-control" required="">
						<option value="28" <?php echo (count($patient_data) > 0 && $patient_data[0]->T_NoPillStart == 28)?'selected':''; ?>>28</option>
						<option value="56" <?php echo (count($patient_data) > 0 && $patient_data[0]->T_NoPillStart == 56)?'selected':''; ?>>56</option>
						<option value="84" <?php echo (count($patient_data) > 0 && $patient_data[0]->T_NoPillStart == 84)?'selected':''; ?>>84</option>
						<!-- <option value="112">112</option>
						<option value="140">140</option>
						<option value="168">168</option> -->
					</select>
				<?php } ?>
				<?php  if($patient_data[0]->T_DurationValue==99 && $patient_data[0]->T_DurationOther==1){ ?>
					<select name="pills_dispensed" id="pills_dispensed" class="input_fields form-control" required="">
						<option value="28" <?php echo (count($patient_data) > 0 && $patient_data[0]->T_NoPillStart == 28)?'selected':''; ?>>28</option>
						
					</select>
				<?php  } ?>
			<?php   if($patient_data[0]->T_DurationValue==99 && $patient_data[0]->T_DurationOther==2){ ?>
					<select name="pills_dispensed" id="pills_dispensed" class="input_fields form-control" required="">
						<option value="28" <?php echo (count($patient_data) > 0 && $patient_data[0]->T_NoPillStart == 28)?'selected':''; ?>>28</option>
						<option value="56">56</option>
					</select>
				<?php  } ?>
					<?php   if($patient_data[0]->T_DurationValue==99 && $patient_data[0]->T_DurationOther==3){ ?>
					<select name="pills_dispensed" id="pills_dispensed" class="input_fields form-control" required="">
						<option value="28">28</option>
						<option value="56">56</option>
						<option value="84">84</option>
					</select>
				<?php  } ?>
			<?php   if($patient_data[0]->T_DurationValue==99 && $patient_data[0]->T_DurationOther==4){ ?>
					<select name="pills_dispensed" id="pills_dispensed" class="input_fields form-control" required="">
						<option value="28" <?php echo (count($patient_data) > 0 && $patient_data[0]->T_NoPillStart == 28)?'selected':''; ?>>28</option>
						<option value="56" <?php echo (count($patient_data) > 0 && $patient_data[0]->T_NoPillStart == 56)?'selected':''; ?>>56</option>
						<option value="84" <?php echo (count($patient_data) > 0 && $patient_data[0]->T_NoPillStart == 84)?'selected':''; ?>>84</option>
						<!-- <option value="112">112</option> -->
					</select>
				<?php  } ?>
					<br class="hidden-lg-*">
					<br class="hidden-lg-*">
				</div>
				<div class="col-md-3">
					<label for="">Advised Next Visit Date</label>
					<input type="text" name="advised_visit_date" id="advised_visit_date" class="input_fields form-control hasCal2 dateInpt input2" value="<?php //echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->AdvisedSVRDate):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" readonly>
					<br class="hidden-lg-*">
					<br class="hidden-lg-*">
				</div>
<!--  (EOT-1) Start -->
				<?php if(count($patient_data) > 0 and ($patient_data[0]->T_DurationValue == 12 && $visit==3 || ($patient_data[0]->T_DurationValue == 24 && $visit==6) || ($patient_data[0]->T_DurationOther == 1 && $visit==1) || ($patient_data[0]->T_DurationOther == 2 && $visit==2) || ($patient_data[0]->T_DurationOther == 3 && $visit==3) || ($patient_data[0]->T_DurationOther == 4 && $visit==4)) ){ ?>
			<div class="row">
				<div class="col-md-3">
					<label for="">Advised SVR Date</label>
					<input type="text" name="advised_visit_datesvr" readonly id="advised_visit_datesvr" class="input_fields form-control hasCal2 dateInpt input2" value="<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->AdvisedSVRDate):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>"">
					<br class="hidden-lg-*">
				</div>
			</div>
<?php } ?>
<!--  (EOT-1) end -->
			</div>
			<div class="row">
				<div class="col-md-12">
					<label for="">Comments</label>
					<textarea rows="5" name="comments" id="comments" class="form-control" style="border: 1px #CCC solid; width: 100%;" maxlength="500"><?php echo (count($patient_data) > 0)?$patient_data[0]->T_RmkDelay:''; ?></textarea>
					<br class="hidden-lg-*">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3 col-md-2">
					<a class="btn btn-block btn-default form_buttons" href="<?php echo base_url('patientinfo?p=1'); ?>" id="close" name="close" value="close">CLOSE</a>
				</div>
				<div class="col-lg-3 col-md-2">
					<button class="btn btn-block btn-default form_buttons" id="refresh" name="refresh" value="refresh">REFRESH</button>
				</div>
				<?php if($loginData->RoleId!=99){ ?>
				<div class="col-lg-3 col-md-2">
					<a href="" class="btn btn-block btn-default form_buttons" id="lock" name="lock" value="lock">LOCK</a>
				</div>
			<?php } else{?>
				<div class="col-lg-3 col-md-2">
					<a href="javascript:void(0)" class="btn btn-block btn-default form_buttons" onclick="openPopupUnlock()" id="" name="unlock" value="lock">LOCK</a>
				</div>
			<?php } ?>
				<?php if($result[0]->Dispense == 1 && $InterruptReasonCount!=1) {?>
					<div class="col-lg-3 col-md-2">
						<button class="btn btn-block btn-success form_buttons" id="save" name="save" value="save">SAVE</button>
					</div>
				<?php } ?>
				<?php if($InterruptReasonCount==1) {?>
										<div class="col-lg-3 col-md-2">
												<span class="btn btn-block btn-default form_buttons">This Patient is Interrupted.</span>
											</div>
										<?php } ?>
			</div> 
			<input type="hidden" class="hasCal dateInpt input2" name="PrescribingDaten" id="PrescribingDaten" value="<?php echo timeStampShow($patient_data[0]->PrescribingDate); ?>">
			<br><br><br>
			 <?php echo form_close(); ?>
			<div class="row" class="text-left">

					<div class="col-md-6 card well">
					 <label class="col-sm-4"  style="text-align: left !important; line-height: 2.2 !important; ">Patient's Status -</label>
					<div class="col-sm-8" style="text-align: left !important; line-height: 2.2 !important; ">
					<label><?php echo (count($patient_status) > 0)?$patient_status[0]->status:''; ?></label>

					</div>
				</div>

				

				<div class="col-md-6 card well">
					<label class="col-sm-6" style="text-align: left !important; ">Patient's Interruption Status </label>
					<div class="col-sm-6">
					<select name="" id="type_val" onchange="openPopup()" class="btn" style="text-align: right!important;">
						<option value="">Select</option>
						<option value="1"  <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason != '')?'selected':''; ?> >Yes</option>
						<option value="0" <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason == '')?'selected':''; ?>>No</option>
					</select>
				
				</div>
				</div>
			
			</div>

			      
		<!-- </form> -->
	</div>
</div>


<div class="modal fade" id="addMyModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title">Patient's Interruption Status</h4>
      </div>
      <span id='form-error' style='color:red'></span>
      <div class="modal-body">
      <!--   <form role="form" id="newModalForm" method="post"> -->
      	<?php
           $attributes = array(
              'id' => 'newModalForm',
              'name' => 'newModalForm',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>


				<div class="form-group">
				<label class="control-label col-md-3" for="email">Reason:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonval" name="resonval" required="required">
				<option value="">Select</option>
				<?php foreach ($InterruptReason as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>

				<div class="form-group resonfielddeath" >
				<label class="control-label col-md-3" for="email">Reason for death:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonvaldeath" name="resonvaldeath">
				<option value="">Select</option>
				<?php foreach ($reason_death as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
				
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>

				<div class="form-group resonfieldlfu">
				<label class="control-label col-md-3" for="email">Reason for LFU:</label>
				<div class="col-md-9">
				<select class="form-control" id="resonfluid" name="resonfluid">
				<option value="">Select</option>
				<?php foreach ($reason_flu as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
				
			<?php } ?>
				</select> 
				</div>
				</div>
<div class="form-group resonfieldothers" >
				<label class="control-label col-md-3" for="email">Other(specify):</label>
				<div class="col-md-9">
				<input type="text" class="form-control" id="otherInterrupt" name="otherInterrupt">
				</div>
				</div>
<br/><br/>
<?php if((count($patient_data) > 0) && timeStampShow($patient_data[0]->AdvisedSVRDate)!=''){ ?>
		<div class="svrRecommendedhs">
			<div class="form-group" >
<label>SVR Recommended</label>
			</div>
			<!-- <div class="control-label col-md-3">
				
				<input type="radio" name="interruption_stage" value="1">
				<label>ETR</label>
			</div> -->
			<div class="control-label col-md-3">
				<input type="radio" name="interruption_stage" value="2">
				<label>Yes</label>
			</div>

			<div class="control-label col-md-3">
				<input type="radio" name="interruption_stage" value="3">
				<label>No</label>
			</div>
			</div>
<?php } ?>
			<br/><br/>
		</div>

          <div class="modal-footer">
            <button type="submit" class="btn btn-success" id="btnSaveIt">Save</button>
            <button type="button" class="btn btn-default" id="btnCloseIt" data-dismiss="modal">Close</button>
          </div>
           <?php echo form_close(); ?>
       <!--  </form> -->
      </div>
    </div>
  </div>
</div>

<br/><br/><br/>


<!-- Unlock box start-->
<div class="modal fade" id="addMyModalUnbox" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title">Unlock Process</h4>
      </div>
      <span id='form-error' style='color:red'></span>
      <div class="modal-body">
      <!--   <form role="form" id="newModalForm" method="post"> -->
      	<?php
           $attributes = array(
              'id' => 'unlockModalForm',
              'name' => 'unlockModalForm',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>


				<div class="form-group">
				<label class="control-label col-md-3" for="email">Reason:</label>
				<div class="col-md-9">
				<select class="form-control" id="unlockresonval" name="unlockresonval" required="required">
					<option value="">Select</option>
				<?php foreach ($unlockprocess as $key => $value) { ?>
				<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
			<?php } ?>
				</select> 
				</div>
				</div>
<br/><br/>

<div class="form-group unlockresonfieldothers" >
				<label class="control-label col-md-3" for="email">Other(specify):</label>
				<div class="col-md-9">
				<input type="text" class="form-control" id="unlockotherInterrupt" name="unlockotherInterrupt">
				</div>
				</div>
<br/><br/>


          <div class="modal-footer">
            <button type="submit" class="btn btn-success" id="unlock">Save</button>
            <button type="button" class="btn btn-default" id="btnunlock" data-dismiss="modal">Close</button>
          </div>
           <?php echo form_close(); ?>
       <!--  </form> -->
      </div>
    </div>
  </div>
</div>
<br/><br/><br/>
<!-- end unlock -->
<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>

<script>
$('#advised_visit_date').datepicker({minDate:-1,maxDate:-2}).attr('readonly','readonly'); 
$('.hasCal2').each(function() {
        ddate=$(this).val();
        yRange="1990:"+new Date().getFullYear();
        yr=$(this).attr('year-range');
        if(yr){
            yRange=yr;
        }
        
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
            changeMonth: true,
            yearRange: yRange,
           // maxDate: 0,
        });
        $(this).attr('placeholder','dd-mm-yy');
        if($(this).attr('noweekend')){
            $(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
        }
        $(this).datepicker( "setDate", ddate);
    }); 
 


	//$('#advised_visit_date').datepicker({minDate:-1,maxDate:-2}).attr('readonly','readonly'); 
	function openPopup() {
		var type_val = $('#type_val').val();
		if(type_val=='1'){
    $("#addMyModal").modal();
		}
}

$('.resonfielddeath').hide();
$('.resonfieldlfu').hide();
$('.resonfieldothers').hide();

$('#btnCloseIt').click(function(){

$('#type_val').val('0');
$('#interruption_status').val('');

});

$('#type_val').change(function(){
var type_val = $('#type_val').val();
$('#interruption_status').val(type_val);

});


 $('#btnSaveIt').click(function(e){
      
      e.preventDefault(); 
      $("#form-error").html('');
     
        
	 var formData = new FormData($('#newModalForm')[0]);
	 $('#btnSaveIt').prop('disabled',true);
	 $.ajax({				    	
		url: '<?php echo base_url(); ?>patientinfo/interruptionstatus_process/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
		type: 'POST',
		data: formData,
		dataType: 'json',
		async: false,
        cache: false,
		contentType: false,
		processData: false,
		success: function (data) { 
				
			if(data['status'] == 'true'){
				$("#form-error").html('Data has been successfully submitted');
				// redirectstatus
				//alert(data['interruptstage']);
					if(data['interruptstage'] == 2)
					{

					alert('Patient interruption status has been successfully updated.Please update the SVR page when the patient has conducted the SVR test.');
					//return false;
    					location.href='<?php echo base_url(); ?>patientinfo/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
					}
					else if(data['interruptstage'] == 3)
					{
						alert('Patient interruption status has been successfully updated. SVR has not been recommended.');
						location.href='<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/1';
    					//location.href='<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/eot';
					}else{

						
						location.href='<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/1';
					}
				

			}else{
				$("#form-error").html(data['message']);
				$('#btnSaveIt').prop('disabled',false); 
				return false;
			}   
		}        
	 }); 
		
   

    }); 
 	 function openPopupUnlock() {
		
		$("#addMyModalUnbox").modal();
		
		}
		
		$('.unlockresonfieldothers').hide();
		$('#unlockresonval').change(function(){
		var unlockresonval = $('#unlockresonval').val();
	/*	if(unlockresonval  ==4){
		$('.unlockresonfieldothers').show();
		}else{
		$('.unlockresonfieldothers').hide();	
		}*/
		});

	$('#resonval').change(function(){
		

		if($('#resonval').val() == '1'){

		$('.resonfielddeath').show();
		$('.resonfieldlfu').hide();
		$('.resonfieldothers').hide();
		$('#resonfluid').val('');

		$('#resonvaldeath').prop('required',true);
		$('#resonfluid').prop('required',false);
		$('#otherInterrupt').prop('required',false);
		$('.svrRecommendedhs').hide();

	}else if($('#resonval').val() == '2'){
		$('.resonfielddeath').hide();
		$('.resonfieldlfu').show();
		$('.resonfieldothers').hide();
		$('#resonvaldeath').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',true);
		$('#otherInterrupt').prop('required',false);
		$('.svrRecommendedhs').show();

	}else if($('#resonval').val() == '99'){

		$('.resonfieldothers').show();
		$('.resonfielddeath').hide();
		$('.resonfieldlfu').hide();
		$('#otherInterrupt').prop('required',true);
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',false);
		$('.svrRecommendedhs').show();
	}

	else{

		$('.resonfielddeath').hide();
		$('.resonfieldlfu').hide();
		$('.resonfieldothers').hide();
		$('#resonvaldeath').val('');
		$('#resonfluid').val('');
		$('#resonvaldeath').prop('required',false);
		$('#resonfluid').prop('required',false);
		$('#otherInterrupt').prop('required',false);
		$('.svrRecommendedhs').show();
	}

});
</script>

			<?php if(count($patient_data) > 0 && $patient_data[0]->NextVisitPurpose > 3 ||  $visit_countval > 2) {?>
	<script>
			
			$("#lock").click(function(){
				$("#modal_header").text("Contact admin ,for unlocking the record");
					$("#modal_text").text("Contact Admin");
					$("#multipurpose_modal").modal("show");
					return false;

				});

			/*Unlock process start*/
				
	$("#unlock").click(function(e){
			e.preventDefault(); 
			$("#form-error").html('');

			var formData      = new FormData($('#unlockModalForm')[0]);
	 
		 $.ajax({				    	
			url: '<?php echo base_url(); ?>Unlock/unlock_process_dispensation_visit_1/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
			type: 'POST',
			data: formData,
			dataType: 'json',
			async: false,
	        cache: false,
			contentType: false,
			processData: false,
			success: function (data) { 
				
			if(data['status'] == 'true'){
				alert('Data has been successfully unlock');
				setTimeout(function() {
    					location.href='<?php echo base_url(); ?>patientinfo/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>/1';
				}, 1000);

				}else{
				$("#form-error").html(data['message']);
				
				return false;
				}   
				}        
				}); 
				
				
				});

				/*end unlock process*/

				/*Disable all input type="text" box*/
				//alert('<?php echo $patient_data[0]->MF5; ?>');
				$('#registration input[type="text"]').attr("readonly", true);
				$('#registration input[type="checkbox"]').attr("disabled", true);
				$('#registration input[type="date"]').attr("readonly", true);
				$("#save").attr("disabled", true);
				$("#refresh").attr("disabled", true);
				$('#registration select').attr('disabled', true);
				/*Disable textarea using id */
			$('#registration #txtAddress').prop("readonly", true);
		
		</script>
<?php  } ?>




<?php if($result[0]->Dispense == 1) {?>
	<script>
		
					$(document).ready(function(){
						$(".ledipasvir_field").hide();
			<?php if(count($patient_data) > 0 && $patient_data[0]->T_Regimen == 1) {?>
					
						$(".sofosbuvir_field").show();
					$(".daclatasvir_field").show();
					$(".ribavrin_field").hide();
					$(".velpatasvir_field").hide();
						
					<?php }?>

					<?php if(count($patient_data) > 0 && $patient_data[0]->T_Regimen == 2) {?>
					
						$(".sofosbuvir_field").show();
					$(".velpatasvir_field").show();
					$(".daclatasvir_field").hide();
					$(".ribavrin_field").hide();
					<?php }?>
					<?php if(count($patient_data) > 0 && $patient_data[0]->T_Regimen == 3) {?>
					//alert(<?php echo $patient_data[0]->RecommendedRegimen; ?>);
					$(".sofosbuvir_field").show();
					$(".ribavrin_field").show();
					$(".velpatasvir_field").show();
					$(".daclatasvir_field").hide();
						
					<?php } ?>
					<?php if(count($patient_data) > 0 && $patient_data[0]->T_Regimen == 4) {?>
					//alert(<?php echo $patient_data[0]->RecommendedRegimen; ?>);
						$(".sofosbuvir_field").show();
						$(".ribavrin_field").show();
						$(".daclatasvir_field").show();
						
					<?php }?>
					<?php if(count($patient_data) > 0 && $patient_data[0]->T_Regimen == 5) {?>
					//alert(<?php echo $patient_data[0]->RecommendedRegimen; ?>);
						$(".sofosbuvir_field").show();
						$(".ledipasvir_field").show();
						
					<?php }?>
					<?php if(count($patient_data) > 0 && $patient_data[0]->T_Regimen == 6) {?>
					//alert(<?php echo $patient_data[0]->RecommendedRegimen; ?>);
						$(".sofosbuvir_field").show();
						$(".ledipasvir_field").show();
						$(".ribavrin_field").show();
						
					<?php }?>

				});


					<?php if(count($patient_data) > 0 && $patient_data[0]->DispensationPlace == 9999999) {?>
				$(".new_dplace").show();
				$(".new_place").hide();
			<?php } else {?>
				$(".new_place").show();
				$(".new_dplace").hide();
			<?php } ?>


		$(document).ready(function(){

			<?php 
			$error = $this->session->flashdata('error'); 

			if($error != null)
			{
				?>
				$("#multipurpose_modal").modal("show");
				<?php 
			}
			?>

			$("#refresh").click(function(e){
				//location.reload();
				$("#date_treatment_initiation").val('');

			// if(confirm('All further details will be deleted.Do you want to continue?')){ 
				//$("input").val('');
				//$("select").val('');
				//$("textarea").val('');
				//$("#input_district").html('<option>Select District</option>');
				//$("#input_block").html('<option>Select Block</option>');

				e.preventDefault();
			// }
			});

			


			$("#regimen_prescribed").change(function(){

				if($(this).val() == 1)
				{
					$(".sofosbuvir_field").show();
					$(".daclatasvir_field").show();
					$(".ribavrin_field").hide();
					$(".velpatasvir_field").hide();

					$('#sofosbuvir').prop('required',true);
					$('#daclatasvir').prop('required',true);


				}
				else if($(this).val() == 2)
				{
					$(".sofosbuvir_field").show();
					$(".velpatasvir_field").show();
					$(".daclatasvir_field").hide();
					$(".ribavrin_field").hide();

					$('#sofosbuvir').prop('required',true);
					$('#velpatasvir').prop('required',true);

				}
				else if($(this).val() == 3)
				{
					$(".sofosbuvir_field").show();
					$(".ribavrin_field").show();
					$(".velpatasvir_field").show();
					$(".daclatasvir_field").hide();

					$('#sofosbuvir').prop('required',true);
					$('#velpatasvir').prop('required',true);
					$('#ribavrin').prop('required',true);
				
				}
				else
				{
					$(".sofosbuvir_field").hide();
					$(".daclatasvir_field").hide();
					$(".ribavrin_field").hide();
					$(".velpatasvir_field").hide();
				}
			});
		})
	</script>
<?php } else { ?>
	<script>
		$(document).ready(function(){
			$('input').prop('disabled', true);
			$('select').prop('disabled', true);
			$('textarea').prop('disabled', true);
		});
	</script>
	<?php } ?>
	<script>
		$(document).ready(function(){
			var date = $("#date_treatment_initiation").datepicker('getDate'),
		days = parseInt(25, 10);

		if(!isNaN(date.getTime())){
		date.setDate(date.getDate() + days);

		$("#advised_visit_date").val(date.toInputFormat());

		/*Advised SVR Date start*/
		var remaningpillsval =  84;
		days = parseInt(remaningpillsval, 10);

			if(!isNaN(date.getTime())){
			date.setDate(date.getDate() + days);

			$('#advised_visit_datesvr').val(date.toInputFormat());
		}
		/*Advised SVR Date ens*/

		} /*else {
		alert("Invalid Date");  
		}*/
		});




		$("#date_treatment_initiation").on("change", function(){

		var date =$("#date_treatment_initiation").datepicker('getDate');
		days = parseInt(25, 10);

		if(!isNaN(date.getTime())){
		date.setDate(date.getDate() + days);

		$("#advised_visit_date").val(date.toInputFormat());
		} /*else {
		alert("Invalid Date");  
		}*/

		/*Advised SVR Date start*/
		var remaningpillsval =  84;
		days = parseInt(remaningpillsval, 10);

			if(!isNaN(date.getTime())){
			date.setDate(date.getDate() + days);

			$('#advised_visit_datesvr').val(date.toInputFormat());
		}
		/*Advised SVR Date ens*/
		
		});

$(".sofosbuvir_field").hide();
			$(".daclatasvir_field").hide();
			$(".ribavrin_field").hide();
			$(".velpatasvir_field").hide();
$(document).ready(function(){
changepilsss();
});
	function changepilsss(){

		var durationcount = $('#duration').val();
			var pills_dispensedcount = $('#pills_dispensed').val();
if(durationcount == 12 && pills_dispensedcount > 84){

$("#modal_header").text("Days of Pills Dispensed less than or equal to 84 days");
					$("#modal_text").text("less than or equal to 84 days ");
					$("#pills_dispensed" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;

}

if(durationcount == 24 && pills_dispensedcount > 168){

$("#modal_header").text("Days of Pills Dispensed less than or equal to 84 days");
					$("#modal_text").text("less than or equal to 84 days ");
					$("#pills_dispensed" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;

}


		var date =$("#date_treatment_initiation").datepicker('getDate'),

		days = parseInt($("#pills_dispensed").val() - 3, 10);

		if(!isNaN(date.getTime())){
		date.setDate(date.getDate() + days);

		$("#advised_visit_date").val(date.toInputFormat());
		} /*else {
		alert("Invalid Date");  
		}*/
	}


		$("#pills_dispensed").on("change", function(){

			var durationcount = $('#duration').val();
			var pills_dispensedcount = $('#pills_dispensed').val();
if(durationcount == 12 && pills_dispensedcount > 84){

$("#modal_header").text("Days of Pills Dispensed less than or equal to 84 days");
					$("#modal_text").text("less than or equal to 84 days ");
					$("#pills_dispensed" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;

}

if(durationcount == 24 && pills_dispensedcount > 168){

$("#modal_header").text("Days of Pills Dispensed less than or equal to 84 days");
					$("#modal_text").text("less than or equal to 84 days ");
					$("#pills_dispensed" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;

}


		var date =$("#date_treatment_initiation").datepicker('getDate'),
		//days = parseInt($("#pills_dispensed").val(), 10);
		days = parseInt($("#pills_dispensed").val() - 3, 10);

		if(!isNaN(date.getTime())){
		date.setDate(date.getDate() + days);

		$("#advised_visit_date").val(date.toInputFormat());
		} /*else {
		alert("Invalid Date");  
		}*/
 	});



 Date.prototype.toInputFormat = function() {
       var yyyy = this.getFullYear().toString();
       var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
       var dd  = this.getDate().toString();
       return (dd[1]?dd:"0"+dd[0]) + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + yyyy; // padding
    };

/*Max Date Today Code*/
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!

			var yyyy = today.getFullYear();
			 if(dd<10){
			        dd='0'+dd;
			    } 
			    if(mm<10){
			        mm='0'+mm;	
			    }

			today = yyyy+'-'+mm+'-'+dd;
			document.getElementById("date_treatment_initiation").setAttribute("max", today);
		

$("#date_treatment_initiation" ).change(function( event ) {

var date_of_prescribing_testsdate = $("#PrescribingDaten" ).datepicker('getDate');
var date_treatment_initiation = $("#date_treatment_initiation" ).datepicker('getDate');
//alert(date_of_prescribing_testsdate);


if(date_of_prescribing_testsdate > date_treatment_initiation){

	$("#modal_header").text("Date Of Treatment Initiation greater than or equal to Prescribing Date");
					$("#modal_text").text("Please check dates");
					$("#date_treatment_initiation" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}

});



$("#duration").change(function(){
				if($(this).val() == 99)
				{
					$(".DurationShow").show();
					$("#durationother").val('');
				}
				else
				{
					$(".DurationShow").hide();

				}
			});
<?php if($patient_data[0]->T_DurationValue == 12 || $patient_data[0]->T_DurationValue ==24){  ?>
$(".DurationShow").hide();
<?php } else{ ?>
$(".DurationShow").show();

	<?php  } ?>

	
	</script>