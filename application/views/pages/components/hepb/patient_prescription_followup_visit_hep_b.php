<style>
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}

	.input_fields
	{
		height: 30px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	textarea
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}

	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}

	.form_buttons:focus
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}

	@media (min-width: 768px) {
		.row.equal {
			display: flex;
			flex-wrap: wrap;
		}
	}

	@media (max-width: 768px) {

		.input_fields
		{
			height: 40px !important;
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
		}

		.form_buttons
		{
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
			font-weight: 600 !important;
			padding: 8px 12px !important;
			border: 2px solid #A30A0C !important;

		}
		.form_buttons:hover
		{
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
			font-weight: 600 !important;
			padding: 8px 12px !important;
			border: 2px solid #A30A0C !important;
			background-color: #FFF;
			color: #A30A0C;

		}
	}

	.btn-default {
		color: #333 !important;
		background-color: #fff !important;
		border-color: #ccc !important;
	}
	.btn {
		display: inline-block;
		padding: 6px 12px;
		margin-bottom: 0;
		font-size: 14px;
		font-weight: 400;
		line-height: 2.4;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		-ms-touch-action: manipulation;
		touch-action: manipulation;
		cursor: pointer;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		background-image: none;
		border: 1px solid transparent;
		border-radius: 4px;
	}

	a.btn
	{
		text-decoration: none;
		color : #000;
		background-color: #A30A0C;
	}

	.btn-group .btn:hover
	{
		text-decoration: none !important;
		color: #000 !important;
		background-color: #CCC !important;
	}

	.btn-group .btn:hover
	{
		text-decoration: none !important;
		color: #000 !important;
		background-color: inherit !important;
	}

	a.active
	{
		color : #FFF !important;
		text-decoration: none;
		background-color: inherit !important;
	}

	#table_patient_list tbody tr:hover
	{
		cursor: pointer;
	}

	.btn-success
	{
		background-color: #A30A0C;
		color: #FFF !important;
		border : 1px solid #A30A0C;
	}

	.btn-success:hover
	{
		text-decoration: none !important;
		color: #A30A0C !important;
		background-color: white !important;
		border : 1px solid #A30A0C;
	}
</style>

<br>

<div class="row equal">

	<!-- <form action="" name="patient_form" id="patient_form" method="POST"> -->
		<?php
		$attributes = array(
			'id' => 'patient_form',
			'name' => 'patient_form',
			'autocomplete' => 'false',
		);
		echo form_open('', $attributes); ?>

		<input type="hidden" name="fetch_uid" id="fetch_uid">
		<?php echo form_close(); ?>
		<!-- <input type="hidden" name="fetch_uid" id="fetch_uid"> -->
		<!-- </form> -->
		<div class="col-lg-10 col-lg-offset-1">

			<div class="row">
				<div class="col-md-12 text-center">
					<div class="btn-group">
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">1. Registration</a>
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">2. Screening</a>
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">3. Baseline Tests</a>
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">4. Viral Load</a>

						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/known_history/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">5. Known History</a>
						<a class="btn btn-success" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">6. Prescription</a>
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">7. Dispensation</a>
						<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_svr/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">8. SVR</a>
					</div>
				</div>
			</div>

			<!-- <form action="" method="POST" name="registration" id="registration"> -->
				<?php
				$attributes = array(
					'id' => 'registration',
					'name' => 'registration',
					'autocomplete' => 'false',
				);
				echo form_open('', $attributes); ?>
				<div class="row">
					<div class="col-md-12">
						<h4 class="text-center"><p>Patient Name - <strong><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FirstName)):''; ?></strong>  (<?php echo (count($patient_data) > 0)?$patient_data[0]->UID_Prefix:$uid_prefix; echo '-' .str_pad($patient_data[0]->UID_Num, 6, '0', STR_PAD_LEFT); ?>)<p></h4>

						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Patient Precription FollowUp Visits (<?php echo set_hepd(); ?>)</h3>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-3">
							<h4 class="text-center">Follow Up Visits</h4>
							<table class="table table-hover table-bordered table-striped">
								<thead>
									<th>S.no</th>
									<th>Visit Date</th>
								</thead>
								<tbody>
									<?php foreach ($patient_tblpatientfu_data as $row) { ?>
										<tr>
											<td><?php echo $row->VisitNo; ?></td>
											<td><?php echo $row->VisitDate; ?></td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
						<div class="col-md-9" style="border-left: 1px solid #CCC;">
							<div class="row">
								<div class="col-md-3">
									<label for="">Follow Up Visit Date <span class="text-danger">*</span></label>
									<input type="date" name="visit_date" id="visit_date" class="input_fields form-control" required="">
									<br class="hidden-lg-*">
								</div>
								<div class="col-md-3 ">
									<label for="treating_doctor">Treating Doctor <span class="text-danger">*</span></label>
									<select class="form-control input_fields" id="treating_doctor" name="treating_doctor" required="">
										<option value="">Select</option>
										<?php foreach ($prescribing_doctor as $row) {?>
											<option <?php echo (count($patient_data) > 0 && $patient_data[0]->PrescribingDoctor == $row->id_mst_medical_specialists)?'selected':''; ?> value="<?php echo $row->id_mst_medical_specialists; ?>"><?php echo $row->name; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="col-md-3 prescribing_doctor_other_field">
									<label for="">Treating Doctor Other</label>
									<input type="text" name="treating_doctor_other" id="treating_doctor_other" class="input_fields form-control messagedata" >
									<br class="hidden-lg-*">
								</div>
								<div class="col-md-3">
									<label for="side_effect">Side Effects <span class="text-danger">*</span></label>
									<select class="form-control input_fields" id="side_effect" name="side_effect" required="">
										<option value="">Select</option>
										<?php foreach ($side_effects as $value) { ?>
											<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
										<?php	} ?>
									</select>
								</div>
								<div class="col-md-3 side_effect_other_fiels">
									<label for="">Side Effect Other</label>
									<input type="text" name="side_effect_other" id="side_effect_other" class="input_fields form-control">
									<br class="hidden-lg-*">
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label for="remarks">Other Remarks</label>
									<textarea rows="5" name="remarks" id="remarks" class="form-control" style="border: 1px #CCC solid; width: 100%;"></textarea>
									<br class="hidden-lg-*">
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<label for="referral">Patient Referral</label>
									<select class="form-control input_fields" id="referral" name="referral">
										<option value="">Select</option>
										<option value="1">Yes</option>
										<option value="2">No</option>
									</select>
								</div>
								<div class="col-md-3 referring_doctor_field">
									<label for="referring_doctor ">Referring Doctor</label>
									<select class="form-control input_fields" id="referring_doctor" name="referring_doctor">
										<option value="">Select</option>
										<?php foreach ($prescribing_doctor as $row) {?>
											<option <?php echo (count($patient_data) > 0 && $patient_data[0]->PrescribingDoctor == $row->id_mst_medical_specialists)?'selected':''; ?> value="<?php echo $row->id_mst_medical_specialists; ?>"><?php echo $row->name; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="col-md-3 referring_doctor_other_field">
									<label for="">Referring Doctor Other</label>
									<input type="text" name="referring_doctor_other" id="referring_doctor_other" class="input_fields form-control messagedata">
									<br class="hidden-lg-*">
								</div>
								<div class="col-md-3 referring_to_field">
									<label for="referring_to">Referring To</label>
									<select class="form-control input_fields" id="referring_to" name="referring_to">
										<option value="">Select</option>
										<?php foreach ($search_facilities as $facvalue) { ?>
											<option value="<?php echo $facvalue->id_mstfacility; ?>"><?php echo $facvalue->FacilityCode; ?></option>
										<?php	} ?>
									</select>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-lg-6 col-md-6 col-md-offset-3">
									<button class="btn btn-block btn-success form_buttons" id="save" name="save" value="save">SAVE</button>
								</div>
							</div>
						</div>
					</div>

					<br><br><br>
					<?php echo form_close(); ?>
					<!-- </form> -->
				</div>
			</div>


			<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
			<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>

			<script>

				$(document).ready(function(){

				})

				$('.messagedata').keypress(function (e) {
					var regex = new RegExp(/^[a-zA-Z\s]+$/);
					var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
					if (regex.test(str)) {
						return true;
					}
					else {
						e.preventDefault();
						return false;
					}
				});

				$(".prescribing_doctor_other_field").hide();

				$("#treating_doctor").change(function(){
					if($(this).val() == 999)
					{
						$(".prescribing_doctor_other_field").show();
						$('#treating_doctor_other').prop('required',true);
					}
					else
					{
						$(".prescribing_doctor_other_field").hide();
					}
				});

				$(".side_effect_other_fiels").hide();

				$("#side_effect").change(function(){
					if($(this).val() == 99)
					{
						$(".side_effect_other_fiels").show();
						$('#side_effect_other').prop('required',true);
					}
					else
					{
						$(".side_effect_other_fiels").hide();
					}
				});

				$(".referring_doctor_field").hide();
				$(".referring_doctor_other_field").hide();
				$(".referring_to_field").hide();

				$("#referral").change(function(){

					if($(this).val() == 1)
					{
						$(".referring_doctor_field").show();
						//$(".referring_doctor_other_field").show();
						$(".referring_to_field").show();

						$('#referring_doctor').prop('required',true);
						
						$('#referring_to').prop('required',true);
					}
					else
					{	
						$(".referring_doctor_field").hide();
						$(".referring_doctor_other_field").hide();
						$(".referring_to_field").hide();

						
					}
				});

				$("#referring_doctor").change(function(){
					if($(this).val() == 999)
					{
						$(".referring_doctor_other_field").show();
						$('#referring_doctor_other').prop('required',true);
					}
					else
					{
						$(".referring_doctor_other_field").hide();
					}
				});


			</script>