<?php 
$loginData = $this->session->userdata('loginData');

$sql = "SELECT Dispense FROM `MSTRole` where RoleId = ".$loginData->RoleId;
$result = $this->db->query($sql)->result();
 //$visit = $this->uri->segment(4);
$visit_countval = count($visit_count);
$duration = $patient_data[0]->T_DurationValue/4;


$visit;
//echo $countdurationval= $visit_countval;
//echo count($tblpatientdispensationb_list);

//echo  date('jS \ F Y');

 $InterruptReasonCount = (count($patient_data) > 0 && $patient_data[0]->InterruptReason);
?>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 14px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #A30A0C;
	color: #FFF !important;
	border : 1px solid #A30A0C;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #A30A0C !important;
	background-color: white !important;
	border : 1px solid #A30A0C;
}
select[readonly] {
	background: #eee;
	pointer-events: none;
	touch-action: none;
}
.hasDatepicker[readonly] {
	background: #eee;
	pointer-events: none;
	touch-action: none;
}

.hasDatepicker[readonly] {
    	background: #eee;
    	pointer-events: none;
    	touch-action: none;
    }
        .clearable__clear{
    	position: absolute;
    	right:32px; top:29px;
    	padding: 0 8px;
    	font-style: normal;
    	font-size: 1.2em;
    	user-select: none;
    	cursor: pointer;
    }

    .clearable__clear1{
    	position: absolute;
    	right:32px; top:33px;
    	padding: 0 8px;
    	font-style: normal;
    	font-size: 1.2em;
    	user-select: none;
    	cursor: pointer;
    }

</style>

<br>
<div class="row equal">	
	<?php
	$attributes = array(
		'id' => 'patient_form',
		'name' => 'patient_form',
		'autocomplete' => 'off',
	);
	echo form_open('', $attributes); ?>

	 <?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){ ?>
			<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				<?php } ?>
	<input type="hidden" name="fetch_uid" id="fetch_uid">

	<?php echo form_close(); ?>
	<!-- <input type="hidden" name="fetch_uid" id="fetch_uid"> -->

	<div class="col-lg-10 col-lg-offset-1">

		<div class="row">
			<div class="col-md-12 text-center">
				<div class="btn-group">
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_register/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">1. Registration</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_screening/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">2. Screening</a>

					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_testing/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">3. Baseline Tests</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_viral_load/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">4. HBV DNA</a>


					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/known_history/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">5. Known History</a>
					<a class="btn btn-default" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_prescription/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">6. Prescription</a>
					<a class="btn btn-success" href="<?php echo base_url(); ?>patientinfo_hep_b/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>">7. Dispensation</a>

				</div>
			</div>
		</div>

	<!-- 	<input type="hidden" name="patvisit" id="patvisit" value="<?php //echo $visit; ?>"> -->
		<!-- <form action="" method="POST" name="registration" id="registration"> -->
			<?php
			$attributes = array(
				'id' => 'registration',
				'name' => 'registration',
				'autocomplete' => 'off',
			);
			echo form_open('', $attributes); ?>

			<div class="row">
				
					<h4 class="text-center"><p>Patient Name - <strong><?php echo (count($patient_data) > 0)?ucwords(strtolower($patient_data[0]->FirstName)):''; ?></strong>  (<?php echo (count($patient_data) > 0)?$patient_data[0]->UID_Prefix:$uid_prefix; echo '-' .str_pad($patient_data[0]->UID_Num, 6, '0', STR_PAD_LEFT); ?>)<p></h4>

						<div class="col-md-6" style="padding-right: 0;">
							
							<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Patient Dispensation Module (<?php echo set_hepd(); ?>)</h3>
						</div>
						<div class="col-md-6" style="padding-left: 0;">
							<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Treatment Initiation Date - <?php echo timeStampShow($Treatment_Dt[0]->Treatment_Dt); ?> </h3>
						</div>

						
					
				</div>
				<br>

				<div class="row">
					<table class="table table-bordered" id="patient_table">
						<thead>
							<tr>
								<!-- <th>Patient Name</th> -->
								<th>Dispensation Number</th>
								<th>Visit Date</th>
								<!-- <th>Pills Taken as</th>
								<th>Pills Taken Daily</th> -->
								<th>Pills  Dispensed</th>
								<!-- <th>Solution to be taken daily (mL)</th>
								<th>Pills needed daily</th> -->
													
								<th>Advised Next visit Date</th>
								 <th>Place Of Dispensation</th>									
								<th>Action</th>								
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($tblpatientdispensationb_list) > 0){
								foreach($tblpatientdispensationb_list as $row){?>
									<tr>
										<!-- <td><?=$row->patientName?></td> -->
										<td><?=$row->VisitNo?></td>
										<td><?=timeStampShow($row->Treatment_Dt)?></td>
										<!-- <td><?=$row->PillsTakenValue?></td>
										<td><?=($row->PillsTakenDaily == 0 ? '--':$row->PillsTakenDaily)?></td> -->
										<td><?=($row->PillsDispensed == 0 ? '--':$row->PillsDispensed)?></td>
										<!-- <td><?=($row->SolutionTakenDaily == 0 ? '--':$row->SolutionTakenDaily)?></td>
										<td><?=($row->PillsNeededDaily == 0 ? '--':$row->PillsNeededDaily)?></td> -->
										<!-- <td><?=($row->PillstoBeDispensed == 0 ? '--':$row->PillstoBeDispensed)?></td> -->
										<td><?=timeStampShow($row->NextVisit)?></td>
										<td><?=$row->FacilityCode?></td>
										<td><?php if ($row->VisitNo == $VisitNo-1) { ?>
											<lable style="padding : 4px;" title="Edit"  class='editvisit' ><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></lable>
											<input type="hidden" name="PatientID" id="PatientID" value="<?php echo $row->PatientID; ?>">

											<input type="hidden" class="input_fields form-control hasCal2 dateInpt input2" name="NextVisitdate"  id="NextVisitdate" value="<?php echo (count($row) > 0)?timeStampShow($row->NextVisit):''; ?>" placeholder="dd-mm-yy" onkeydown="return false" onkeyup="return false">

											<?php }  ?> </td>

										</tr>
									<?php } }else{?>
										<tr><td colspan="12" class="text-center">No Data Found</tr>
										<?php } ?>
									</tbody>
								</table>

								<?php if(count($tblpatientdispensationb_list) == 0){ 

									$lablename = 'Date Of Treatment Initiation';
								}else{
									$lablename = 'Visit date';
								}
									?>
								

							</div>
							<div class="row">
								<div class="col-md-3">
									<lable class="btn btn-success" id="addnewvisit" name="addnewvisit" value="addnewvisit">Add New Visit</lable>
								</div>
							</div>
							<hr style="border-top: 2px solid #000; margin-left: 30px; margin-right: 30px;">
							<br>
							<div class="visitdiv">
								<div class="row">					
									<div class="col-md-3">
										<label for="">Dispensation number<span class="text-danger">*</span></label>
										<input type="text" required name="VisitNo" id="VisitNo" class="input_fields form-control" value="<?php echo (isset($VisitNo))?$VisitNo:''; ?>" readonly>
										<br class="hidden-lg-*">
									</div>
									
									<div class="col-md-3">
										<label for=""><?php echo $lablename; ?>  <span class="text-danger">*</span></label>
										<input type="text" name="Treatment_Dt" id="date_treatment_initiation" class="input_fields form-control hasCal dateInpt input2" value="<?php echo (count($patient_data) > 0)?timeStampShow($patient_data[0]->T_Initiation):''; ?>" placeholder="dd-mm-yy" onkeydown="return false" onkeyup="return false"  required="">
										<br class="hidden-lg-*">
									</div>

								</div>
								<div class="row">
									<div class="col-md-3" >
										<label for="">Regimen prescribed </label>
										<select class="form-control input_fields" id="RegimenPrescribed" name="RegimenPrescribed" required readonly>
											<option value="">Select</option>
											<?php foreach($mst_regimen_hepb as $row){ ?>
									<option value="<?=$row->id_mst_regimenhepb?>" <?php echo (count($patient_datahepb[0]->Drugs_Added) > 0 && $patient_datahepb[0]->Drugs_Added == $row->id_mst_regimenhepb)?'selected':''; ?>><?=$row->regimen_name?></option>
								<?php } ?>						
										</select>
									</div>

									<div class="col-md-3">
										<label for="">Place Of Dispensation</label>
										<select class="form-control input_fields" id="DispensationPlace" name="DispensationPlace" required="">
											<option value="">Select</option>
											<?php error_reporting(0); if(count($patient_data) > 0 && !empty($patient_data[0]->ReferTo ) ) { ?>
												<option value="<?php  echo $ReferToFaci[0]->id_mstfacility; ?>" <?php if($patient_data[0]->PrescribingFacility == $ReferToFaci[0]->id_mstfacility){ echo "selected"; }  ?>><?php echo $ReferToFaci[0]->FacilityCode; ?></option>

											<?php } ?>
											<?php foreach ($place_of_dispensation as $row) { ?>
												<option  value="<?php echo $row->id_mstfacility; ?>"
													<?php 
													if($patient_data[0]->PrescribingFacility == null && $row->id_mstfacility == $loginData->id_mstfacility)
														echo "selected"; 
													else if ($patient_data[0]->PrescribingFacility != null && $patient_data[0]->PrescribingFacility == $row->id_mstfacility)
														echo "selected"; 
													else
														echo '';
													?>
													><?php echo $row->FacilityCode; ?></option>

												<?php } ?>
											</select>
										</div>	

										<div class="col-md-3">
											<label for="">Days Of Pills Dispensed <span class="text-danger">*</span>
												<span style="margin-left: 20px; font-size: 14px;" class="glyphicon glyphicon-info-sign" data-toggle='tooltip' title="The drugs will be dispensed for 30 days for an initial 6-12 months. Once the treating doctor is confident that the patient has been stabilized, the drug dispensation can be done for up to 3 months."></span>
											</label>

											

											<!-- <input type="number" required name="PillsDaysDispensed" id="PillsDaysDispensed" class="input_fields form-control input2" value="28" onkeydown="return false" onkeyup="return false" readonly> -->

											<select  name="PillsDaysDispensed" id="PillsDaysDispensed" class="input_fields form-control input2" required="">
												<!--  <option value="">Select</option> -->
												<?php foreach ($pills_dispensed as $key => $value) {
												 ?>
												
												<option value="<?php echo $value->LookupCode; ?>" <?php if($patient_data[0]->PillsDispensed == $value->LookupCode){ echo "selected"; }  ?>><?php echo $value->LookupValue; ?></option>
												<?php } ?>
											</select>

											<br class="hidden-lg-*">
										</div>					
									</div>
									<br>
									<div class="row">
										<div class="col-md-3 sofosbuvir_field" >
											<label for="">Pills to be taken as <span class="text-danger">*</span></label>
											<select class="form-control input_fields" id="PillsTaken" name="PillsTaken" required readonly>
												<option value="">Select</option>
												<?php foreach($pills_to_be_taken_list as $row){ ?>
													<option value="<?=$row->LookupCode?>" <?php echo (count($patient_datahepb) > 0 && $patient_datahepb[0]->PillsToBeTaken == $row->LookupCode)?'selected':''; ?>><?=$row->LookupValue?></option>
												<?php } ?>	
											</select>
										</div>	

										<div class="col-md-3">
											<label for="">Pills To Be Dispensed <span class="text-danger">*</span></label>
											<input type="number" required="" name="PillsDispensed" id="PillsDispensed" class="input_fields form-control" value="<?php echo (count($patient_datahepb) > 0)?($patient_datahepb[0]->PillsDispensed):''; ?>"  readonly>
											<br class="hidden-lg-*">
										</div>	

									</div>
									<br>
									<div class="row pillsDiv" style="display: none;">

										<div class="col-md-3" style="display: none;">
											<label for="">Pills To Be Taken Daily <span class="text-danger">*</span></label>
											<input type="number" id="PillsTakenDaily" name="PillsTakenDaily" value="1"  >
											<!-- <select class="form-control input_fields" id="PillsTakenDaily" name="PillsTakenDaily" required>
												<option value="">Select</option>
												<option value="1" selected="">1</option>
												<option value="2">2</option>
											</select> -->
										</div>	

										


									</div>
									<br>
									<div class="row solutionDiv" style="display: none;">					

										<div class="col-md-3" >
											<label for="">Solution to be taken daily (mL) <span class="text-danger">*</span></label>
											<select class="form-control input_fields" id="SolutionTakenDaily" name="SolutionTakenDaily" required>
												<option >Select</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
												<option value="7">7</option>
												<option value="8">8</option>
												<option value="9">9</option>
												<option value="10">10</option>								
											</select>
										</div>	

										<div class="col-md-3">
											<label for="">Pills needed daily <span class="text-danger">*</span></label>
											<input type="number" required name="PillsNeededDaily" id="PillsNeededDaily" class="input_fields form-control input2" value="<?php //echo (count($patient_datahepb) > 0)?timeStampShow($patient_datahepb[0]->PrescribingDate):''; ?>"  readonly>
											<br class="hidden-lg-*">
										</div>	

										<div class="col-md-3">
											<label for="">Pills to be dispensed <span class="text-danger">*</span></label>
											<input type="number" required name="PillstoBeDispensed" id="PillstoBeDispensed" class="input_fields form-control input2" value="<?php //echo (count($patient_datahepb) > 0)?timeStampShow($patient_datahepb[0]->PrescribingDate):''; ?>"  readonly>
											<br class="hidden-lg-*">
										</div>

									</div>
									<br>
									<div class="row">
<?php if(count($tblpatientdispensationb_list) != 0){  ?>

			<div class="col-md-3 hidepillsdetails">
					<label for="">Pills left <span class="text-danger">*</span>
						<span style="margin-left: 20px; font-size: 14px;" class="glyphicon glyphicon-info-sign" data-toggle='tooltip' title="5 days buffer."></span>

					</label>
					<input type="text" name="pills_left" id="pills_left" required="" class="input_fields form-control" onkeypress="return onlyNumbersWithDot(event);" maxlength="2" value="<?php if(count($visit_details )>0 && $visit_details[0]->PillsLeft!=""){ echo $visit_details[0]->PillsLeft;} else{ echo 0;} ?>">
					<br class="hidden-lg-*">
				</div>

					<div class="col-md-3 hidepillsdetails">
					<label for="">Days of Pills Left</label>
					<input type="text" name="daypills_left" id="daypills_left" class="input_fields form-control" onkeypress="return onlyNumbersWithDot(event);" maxlength="2" value="<?php if(count($visit_details )>0 && $visit_details[0]->PillsLeft!=""){ echo $visit_details[0]->PillsLeft;} else{ echo 0;} ?>" readonly>
					<br class="hidden-lg-*">
				</div>


				<div class="col-md-3 hidepillsdetails" >
					<label for="">Adherence(%)</label>
					<input type="text" name="adherence" id="adherence" class="input_fields form-control" value="<?php if(count($visit_details )>0 && $visit_details[0]->Adherence!=""){ echo $visit_details[0]->Adherence;} ?>" readonly="">
					<br class="hidden-lg-*">
				</div>
				<div class="col-md-3 adherence_field hidepillsdetails">
					<label for="">Reason for Low Adherence<span class="text-danger">*</span></label>
					<select class="form-control input_fields" id="reason_low_adherence" name="reason_low_adherence">
						<option value="">Select</option>
						<?php foreach ($low_adherence as $row) {?>
							<option value="<?php echo $row->LookupCode; ?>"  <?php echo (count($visit_details) > 0 && $visit_details[0]->NAdherenceReason == $row->LookupCode)?'selected':''; ?>><?php echo $row->LookupValue; ?></option>
						<?php } ?>
					</select>
					<br>
				</div>
				<div class="col-md-3 reason_low_adherence_fields hidepillsdetails" >
					<label for="">Low Adherence Reason Other</label>
					<input type="text" name="reason_low_adherence_other" id="reason_low_adherence_other" class="input_fields form-control" value="<?php echo (count($visit_details) > 0)?$visit_details[0]->NAdherenceReasonOther:''; ?>">
					<br class="hidden-lg-*">
				</div>
			<?php } ?>
						

										<div class="col-md-3">
											<label for="">Advised Next Visit Date <span class="text-danger">*</span></label>
											<input type="text" required="" name="NextVisit" id="advised_visit_date" class="input_fields form-control hasCal dateInpt input2" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" readonly>
											<br class="hidden-lg-*">
										</div>
									</div>
<?php if(count($tblpatientdispensationb_list) != 0){  ?>
									<div class="row">
				<div class="col-md-4">
					<label>Presence of Side Effects</label><br/>

					<?php foreach ($side_effects as $row) { 

						 if(count($visit_details )>0 ){

						 	$SideEffectValue  = $visit_details[0]->SideEffectValue;
						 	$sideef = explode(',', $SideEffectValue);
						 	$checked = '';
									if (in_array($row->LookupCode, $sideef))
										$checked = 'checked';
						 }
						?>
						<label class="custom-control overflow-checkbox">
					<input type="checkbox" class="overflow-control-input" value="<?php echo $row->LookupCode; ?>" id="side_effects<?php echo $row->LookupCode; ?>" name="side_effects[]" <?php if(count($visit_details )>0 ){ echo $checked;} ?>>
					<span class="overflow-control-indicator"></span></label>

					<?php if($row->LookupCode==10){  ?>
					<span class="fill-control-description"><?php echo $row->LookupValue.'<br/>'; ?></span>
					<?php } else{?>
					<span class="fill-control-description"><?php echo $row->LookupValue; ?></span>
				<?php } ?>
					
					
					<?php } ?>

					<!-- <select class="form-control input_fields" id="side_effects" name="side_effects">
						<option value="">Select</option>
						<?php //foreach ($side_effects as $row) {?>
							<option value="<?php echo $row->LookupCode; ?>" <?php if(count($visit_details )>0 && $visit_details[0]->SideEffectValue == $row->LookupCode) { echo 'selected';} ?>><?php echo $row->LookupValue; ?></option>
						<?php //} ?>
					</select> -->
				</div>
				<div class="col-md-3 side_effects_other_field">
						<label for="">Other <span class="text-danger">*</span></label>
						<input type="text" name="side_effect_other" id="side_effect_other" class="input_fields form-control messagedata" value="<?php echo (count($visit_details) > 0)?$visit_details[0]->SideEffect:''; ?>">
					</div>

					</div>


<?php } ?>


									<br>
									<input type="hidden" name="interruption_status" id="interruption_status" value="<?php  echo (count($patient_data) > 0)?$patient_data[0]->InterruptReason:'';  ?>">
									<div class="row">
										<div class="col-lg-3 col-md-2">
											<a class="btn btn-block btn-default form_buttons" href="<?php echo base_url('patientinfo_hep_b?p=1'); ?>" id="close" name="close" value="close">CLOSE</a>
										</div>
										<div class="col-lg-3 col-md-2">
											<button class="btn btn-block btn-default form_buttons" id="refresh" name="refresh" value="refresh">REFRESH</button>
										</div>
										<?php if($loginData->RoleId!=99){ ?>
											<div class="col-lg-3 col-md-2">
												<a href="" class="btn btn-block btn-default form_buttons" id="lock" name="lock" value="lock">LOCK</a>
											</div>
										<?php } else{?>
											<div class="col-lg-3 col-md-2">
												<a href="javascript:void(0)" class="btn btn-block btn-default form_buttons" onclick="openPopupUnlock()" id="" name="unlock" value="lock">LOCK</a>
											</div>
										<?php } ?>
										<?php if($result[0]->Dispense == 1 && $InterruptReasonCount!=1) {?>
											<div class="col-lg-3 col-md-2">
												<button class="btn btn-block btn-success form_buttons" id="save" name="save" value="save">SAVE</button>
											</div>
										<?php } ?>
										<?php if($InterruptReasonCount==1) {?>
										<div class="col-lg-3 col-md-2">
												<span class="btn btn-block btn-default form_buttons">This Patient is Interrupted.</span>
											</div>
										<?php } ?>
									</div>
									<input type="hidden" class="hasCal dateInpt input2" name="PrescribingDaten" id="PrescribingDaten" value="<?php echo timeStampShow($patient_datahepb[0]->PrescribingDate); ?>">
<?php if(($VisitNo-1)==1){
	$visitnod = 'st';
}elseif(($VisitNo-1) ==2){
	$visitnod= 'nd';
}elseif(($VisitNo-1) ==3){
	$visitnod= 'rd';
} else{
	$visitnod= 'th';
}
	?>
									<br><br><br>
									<?php echo form_close(); ?>
									<div class="row" class="text-left">

										<div class="col-md-6 card well">
											<label class="col-sm-4"  style="text-align: left !important; line-height: 2.2 !important; ">Patient's Status -</label>
											<div class="col-sm-8" style="text-align: left !important; line-height: 2.2 !important; ">
												<label><?php echo "On Treatment - ".($VisitNo-1). $visitnod." Dispensation"; ?></label>

											</div>
										</div>

										<div class="col-md-6 card well">
											<label class="col-sm-6" style="text-align: left !important; ">Patient's Interruption Status </label>
											<div class="col-sm-6">
												<select name="" id="type_val" onchange="openPopup()" class="btn" style="text-align: right!important;">
													<option value="">Select</option>
													<option value="1"  <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason != '')?'selected':''; ?> >Yes</option>
													<option value="0" <?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason == '')?'selected':''; ?>>No</option>
												</select>

											</div>
										</div>

									</div>

									<!-- </form> -->
								</div>
							</div>
						</div>


					</div>

					<br/><br/><br/>


<div class="modal fade" id="addMyModal" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">

								<h4 class="modal-title">Patient's Interruption Status</h4>
							</div>
							<span id='form-error' style='color:red'></span>
							<div class="modal-body">
								<!--   <form role="form" id="newModalForm" method="post"> -->
									<?php
									$attributes = array(
										'id' => 'newModalForm',
										'name' => 'newModalForm',
										'autocomplete' => 'off',
									);
									echo form_open('', $attributes); ?>


									<div class="form-group">
										<label class="control-label col-md-3" for="email">Reason:</label>
										<div class="col-md-9">
											<select class="form-control" id="resonval" name="resonval" required="required">
												<option value="">Select</option>
												<?php foreach ($InterruptReason as $key => $value) { ?>
													<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>
												<?php } ?>
											</select> 
										</div>
									</div>
									<br/><br/>

									<div class="form-group resonfielddeath" >
										<label class="control-label col-md-3" for="email">Reason for death:</label>
										<div class="col-md-9">
											<select class="form-control" id="resonvaldeath" name="resonvaldeath">
												<option value="">Select</option>
												<?php foreach ($reason_death as $key => $value) { ?>
													<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>

												<?php } ?>
											</select> 
										</div>
									</div>
									<br/><br/>

									<div class="form-group resonfieldlfu">
										<label class="control-label col-md-3" for="email">Reason for LFU:</label>
										<div class="col-md-9">
											<select class="form-control" id="resonfluid" name="resonfluid">
												<option value="">Select</option>
												<?php foreach ($reason_flu as $key => $value) { ?>
													<option value="<?php echo $value->LookupCode; ?>"><?php echo $value->LookupValue; ?></option>

												<?php } ?>
											</select> 
										</div>
									</div>
									<div class="form-group resonfieldothers" >
										<label class="control-label col-md-3" for="email">Other(specify):</label>
										<div class="col-md-9">
											<input type="text" class="form-control" id="otherInterrupt" name="otherInterrupt">
										</div>
									</div>
									<br/><br/>

									<div class="modal-footer">
										<button type="submit" class="btn btn-success" id="btnSaveIt">Save</button>
										<button type="button" class="btn btn-default" id="btnCloseIt" data-dismiss="modal">Close</button>
									</div>
									<?php echo form_close(); ?>
									<!--  </form> -->
								</div>
							</div>
						</div>
					</div>
					<br/><br/><br/>

<input type="hidden" required="" name="prescribing_date" id="prescribing_date" class="input_fields form-control hasCal dateInpt input2" value="<?php echo (count($patient_datahepb) > 0)?timeStampShow($patient_datahepb[0]->PrescribingDate):''; ?>" onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
					<br/><br/><br/>
					<!-- end unlock -->
					<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/bootstrap-select.js"></script>
					<script type="text/javascript" src="<?php echo site_url('common_libs');?>/js/jquery.mask.js"></script>

					<script type="text/javascript">



function openPopup() {
		var type_val = $('#type_val').val();
		if(type_val=='1'){
			$("#addMyModal").modal();
		}
	}

$('.resonfielddeath').hide();
							$('.resonfieldlfu').hide();
							$('.resonfieldothers').hide();

							$('#btnCloseIt').click(function(){

								$('#type_val').val('0');
								$('#interruption_status').val('');

							});

							$('#type_val').change(function(){
								var type_val = $('#type_val').val();
								$('#interruption_status').val(type_val);

							});
$('#resonval').change(function(){


								if($('#resonval').val() == '1'){

									$('.resonfielddeath').show();
									$('.resonfieldlfu').hide();
									$('.resonfieldothers').hide();
									$('#resonfluid').val('');

									$('#resonvaldeath').prop('required',true);
									$('#resonfluid').prop('required',false);
									$('#otherInterrupt').prop('required',false);

								}else if($('#resonval').val() == '2'){
									$('.resonfielddeath').hide();
									$('.resonfieldlfu').show();
									$('.resonfieldothers').hide();
									$('#resonvaldeath').val('');
									$('#resonvaldeath').prop('required',false);
									$('#resonfluid').prop('required',true);
									$('#otherInterrupt').prop('required',false);
								}else if($('#resonval').val() == '99'){

									$('.resonfieldothers').show();
									$('.resonfielddeath').hide();
									$('.resonfieldlfu').hide();
									$('#otherInterrupt').prop('required',true);
									$('#resonvaldeath').prop('required',false);
									$('#resonfluid').prop('required',false);
								}

								else{

									$('.resonfielddeath').hide();
									$('.resonfieldlfu').hide();
									$('.resonfieldothers').hide();
									$('#resonvaldeath').val('');
									$('#resonfluid').val('');
									$('#resonvaldeath').prop('required',false);
									$('#resonfluid').prop('required',false);
									$('#otherInterrupt').prop('required',false);
								}

							});


$('[data-toggle="tooltip"]').tooltip();
							$(document).ready(function(){
							
							var PillsDaysDispensed = $('#PillsDaysDispensed').val();
							if(PillsDaysDispensed==1){
								$('#PillsDispensed').val(30);
							}
							else if(PillsDaysDispensed==2){
								$('#PillsDispensed').val(60);
							}else if(PillsDaysDispensed==3){
								$('#PillsDispensed').val(90);
							}else{
								$('#PillsDispensed').val('');
							}

						});


						$('#PillsDaysDispensed').change(function(){

							var PillsDaysDispensed = $('#PillsDaysDispensed').val();
							if(PillsDaysDispensed==1){
								$('#PillsDispensed').val(30);
							}
							else if(PillsDaysDispensed==2){
								$('#PillsDispensed').val(60);
							}else if(PillsDaysDispensed==3){
								$('#PillsDispensed').val(90);
							}else{
								$('#PillsDispensed').val('');
							}

						});

						$('.editvisit').click(function(e){
							$(".visitdiv").show()
							e.preventDefault();
							$('#PatientID').val();
							var dataString = {
								"PatientID": $('#PatientID').val(),
								"patientguid": '<?php echo $patient_datahepb[0]->PatientGUID; ?>'
							}; 
							$.ajax({    
								url: '<?php echo base_url(); ?>patientinfo_hep_b/get_last_visit_details',
								
								 data: {'PatientID': $('#PatientID').val(),"patientguid" : '<?php echo $patient_datahepb[0]->PatientGUID; ?>',<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'},
								type: 'GET',
								//data: dataString,
								dataType: 'json',
								
								success: function (data) {
									
									if(data.status == 'true'){
//$(".editVal").hide();
//console.log(fields);
var parsedData = JSON.parse(data.fields);
$("#VisitNo").val(parsedData.VisitNo);
$("#date_treatment_initiation").val(parsedData.Treatment_Dt);
$("#DispensationPlace").val(parsedData.DispensationPlace);
$("#PillsDaysDispensed").val(parsedData.PillsDaysDispensed);

//$("#PillsTaken").val(parsedData.PillsTaken);
$("#PillsTakenDaily").val(parsedData.PillsTakenDaily);
$("#PillsDispensed").val(parsedData.PillsDispensed);
$("#SolutionTakenDaily").val(parsedData.SolutionTakenDaily);
$("#PillsNeededDaily").val(parsedData.PillsNeededDaily);
$("#PillstoBeDispensed").val(parsedData.PillstoBeDispensed);
$("#advised_visit_date").val(parsedData.NextVisit);

$("#pills_left").val(parsedData.PIllsLeft);
$("#daypills_left").val(parsedData.PIllsLeft);
$("#adherence").val(parsedData.Adherence);



if(parsedData.PIllsLeft== ''){

	$('.hidepillsdetails').hide();
	$("#pills_left").val('0');
	$("#daypills_left").val('0');
}

if(parsedData.NAdherenceReasonOther!=''){
	$('.reason_low_adherence_fields').show();
	
$("#reason_low_adherence_other").val(parsedData.NAdherenceReasonOther);
}

if(parsedData.Adherence<100 && parsedData.Adherence!=''){
	$('.adherence_field').show();
	
$("#reason_low_adherence").val(parsedData.AdherenceReason);
}

if(parsedData.SideEffectValueOther!=''){
	$('.side_effects_other_field').show();
	
$("#side_effect_other").val(parsedData.SideEffectValueOther);
}

//$("#reason_low_adherence").val(parsedData.AdherenceReason);
if(parsedData.SideEffectValue!=null){
var side_effects = parsedData.SideEffectValue.split(",");
    $inputs = $('input[name^=side_effects]');
for (var j = 0; j < side_effects.length; j++) {
    $inputs.filter('[value=' + side_effects[j] + ']').attr('checked','checked');
}

}
/*var ciContact = parsedData.SideEffectValue.split(",");
for (var j = 0; j < ciContact.length; j++) {
    $('input[name^=ciContact][value=' + ciContact[j] + ']').attr('checked','checked');
}*/


}
}        
});
						}); 








						$("#addnewvisit").click(function(){
						// alert('asd');
						$(".visitdiv").show()
						location.reload(true);
						$("#VisitNo").val('');
						$("#date_treatment_initiation").val('');
						$("#DispensationPlace").val('');
						//$("#PillsTaken").val('');
						$("#PillsTakenDaily").val('');
						$("#PillsDispensed").val('');
						$("#SolutionTakenDaily").val('');
						$("#PillsNeededDaily").val('');
						$("#PillstoBeDispensed").val('');
						$("#advised_visit_date").val('');


					});
				</script>

				<script>					
					$(function(){
						
						//$('#patient_table').DataTable();						

						$("#date_treatment_initiation").on("change", function(){
							var date = $("#date_treatment_initiation").datepicker('getDate');	

							<?php if(count($tblpatientdispensationb_list) == 0){ ?>		
							var PillsDaysDispensed = parseInt($('#PillsDaysDispensed option:selected').text());	
							var totaldays = (PillsDaysDispensed - 5);			
							days = parseInt(totaldays, 10);
							if(!isNaN(date.getTime())){
								date.setDate(date.getDate() + days);
								$("#advised_visit_date").val(date.toInputFormat());
							} 
							<?php }  ?>
						});	

						$("#PillsDaysDispensed").on("change", function(){
							var date = $("#date_treatment_initiation").datepicker('getDate');	

							<?php if(count($tblpatientdispensationb_list) == 0){ ?>		
							var PillsDaysDispensed = parseInt($('#PillsDaysDispensed option:selected').text());	
							var totaldays = (PillsDaysDispensed - 5);			
							days = parseInt(totaldays, 10);
							if(!isNaN(date.getTime())){
								date.setDate(date.getDate() + days);
								$("#advised_visit_date").val(date.toInputFormat());
							} 
							<?php }  ?>
						});	


						$("#date_treatment_initiation" ).change(function( event ) {

							var date_of_prescribing_testsdate = $("#PrescribingDaten" ).datepicker('getDate');
							var date_treatment_initiation = $("#date_treatment_initiation" ).datepicker('getDate');

							var NextVisitdate = $('#NextVisitdate').datepicker('getDate');

							<?php if(count($tblpatientdispensationb_list) == 0){ ?>
//alert(date_of_prescribing_testsdate+'/'+date_treatment_initiation+'/'+NextVisitdate);
							if(date_of_prescribing_testsdate > date_treatment_initiation){

								$("#modal_header").text("Date Of Treatment Initiation greater than or equal to Prescribing Date");
								$("#modal_text").text("Please check dates");
								$("#date_treatment_initiation" ).val('');
								$("#multipurpose_modal").modal("show");
								return false;
							}

							if(NextVisitdate > date_treatment_initiation){

								$("#modal_header").text("Date Of Treatment Initiation greater than or equal to Prescribing Date");
								$("#modal_text").text("Please check dates");
								$("#date_treatment_initiation" ).val('');
								$("#multipurpose_modal").modal("show");
								return false;
							}

						<?php } ?>


						});



						var PillsTaken = $('#PillsTaken').val();
						if(PillsTaken == 1){	
							$(".solutionDiv").css("display", "none");	
							$(".pillsDiv").css("display", "block");		
							$('#SolutionTakenDaily').attr("disabled", true);
							$('#PillsTakenDaily').attr("disabled", false); 

					//calculation

					$('#PillsTakenDaily').change(function(){
						var PillsTakenDaily = $(this).val();
						var PillsDaysDispensed = $('#PillsDaysDispensed').val();
						var res = PillsDaysDispensed * PillsTakenDaily;
						$('#PillsDispensed').val(res);

					});
				}else{
					$(".pillsDiv").css("display", "none");
					$(".solutionDiv").css("display", "block");	
					$('#PillsTakenDaily').attr("disabled", true);
					$('#SolutionTakenDaily').attr("disabled", false); 

					$('#PillsTakenDaily').val();
					$('#PillsDispensed').val();

					//calculation

					$('#SolutionTakenDaily').change(function(){
						var PillsDaysDispensed = $('#PillsDaysDispensed').val();
						var SolutionTakenDaily = $(this).val();
						var res = (0.5/10)*SolutionTakenDaily/0.5;
						$('#PillsNeededDaily').val(res.toFixed(2));
						var solutionDispensed = PillsDaysDispensed*res;
						//$('#PillstoBeDispensed').val(solutionDispensed.toFixed(2));
						$('#PillstoBeDispensed').val(Math.ceil(solutionDispensed));

					});
				}
			});

					$('#PillsTaken').change(function(){
						var PillsTaken = $(this).val();

						if(PillsTaken == 1){	
							$(".solutionDiv").css("display", "none");	
							$(".pillsDiv").css("display", "block");			
							$('#SolutionTakenDaily').attr("disabled", true);
							$('#PillsTakenDaily').attr("disabled", false); 

					//calculation

					$('#PillsTakenDaily').change(function(){
						var PillsTakenDaily = $(this).val();
						var PillsDaysDispensed = $('#PillsDaysDispensed').val();
						var res = PillsDaysDispensed * PillsTakenDaily;
						$('#PillsDispensed').val(res);
					});
				}else{
					$(".pillsDiv").css("display", "none");
					$(".solutionDiv").css("display", "block");	
					$('#PillsTakenDaily').attr("disabled", true);
					$('#SolutionTakenDaily').attr("disabled", false); 

					$('#PillsTakenDaily').val();
					$('#PillsDispensed').val();

					//calculation

					$('#SolutionTakenDaily').change(function(){
						var PillsDaysDispensed = $('#PillsDaysDispensed').val();
						var SolutionTakenDaily = $(this).val();
						var res = (0.5/10)*SolutionTakenDaily/0.5;
						$('#PillsNeededDaily').val(res.toFixed(2));
						var solutionDispensed = PillsDaysDispensed*res;
						$('#PillstoBeDispensed').val(solutionDispensed.toFixed(2));
					});
				}
			});


					$("#date_treatment_initiation" ).change(function( event ) {

						var date_of_prescribing_testsdate = '<?php echo $patient_data[0]->Next_Visitdt; ?>';
						var visit_date = $("#date_treatment_initiation" ).val();

						if(date_of_prescribing_testsdate > visit_date ){

							$("#modal_header").text("Visit Date  greater than or equal to must be "+date_of_prescribing_testsdate);
							$("#modal_text").text("Please check dates");
							$("#date_treatment_initiation" ).val('');
							$("#multipurpose_modal").modal("show");
							return false;

						}
					});



//$( "#advised_visit_date" ).datepicker( "option", "disabled", true );
//$('#advised_visit_date').datepicker({minDate:-1,maxDate:-2}).attr('readonly','readonly'); 
$('.hasCal2').each(function() {
	ddate=$(this).val();
	yRange="1990:"+new Date().getFullYear();
	yr=$(this).attr('year-range');
	if(yr){
		yRange=yr;
	}
	
	$(this).datepicker({
		dateFormat: "dd-mm-yy",
		changeYear: true,
		changeMonth: true,
		yearRange: yRange,
           // maxDate: 0,
         });
	$(this).attr('placeholder','dd-mm-yy');
	if($(this).attr('noweekend')){
		$(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
	}
	$(this).datepicker( "setDate", ddate);
}); 

$(document).ready(function(){

			
			var InterruptReason 	= '<?php echo (count($patient_data) > 0 && $patient_data[0]->InterruptReason); ?>';

			if(InterruptReason==1){

				$("#modal_header").text("Further entry is not allowed,as per treatment status.");
				$("#modal_text").text("Not allowed");
				$("#multipurpose_modal").modal("show");

				
			}
		});

$('#btnCloseIt').click(function(){

	$('#type_val').val('0');
	$('#interruption_status').val('');

});

$('#type_val').change(function(){
	var type_val = $('#type_val').val();
	$('#interruption_status').val(type_val);

});


$('#btnSaveIt').click(function(e){

	e.preventDefault(); 
	$("#form-error").html('');

	var patvisit = $('#patvisit').val(); 
	var formData = new FormData($('#newModalForm')[0]);
	$('#btnSaveIt').prop('disabled',true);
	$.ajax({				    	
		url: '<?php echo base_url(); ?>patientinfo/interruptionstatus_process/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>',
			type: 'POST',
			data: formData,
			dataType: 'json',
			async: false,
			cache: false,
			contentType: false,
			processData: false,
		success: function (data) { 

			if(data['status'] == 'true'){
				$("#form-error").html('Data has been successfully submitted');
				// redirectstatus
				setTimeout(function() {
					location.href='<?php echo base_url(); ?>patientinfo_hep_b/patient_dispensation/<?php echo count($patient_data) > 0?$patient_data[0]->PatientGUID:''; ?>';
				}, 1000);
			
    			}else{
    				$("#form-error").html(data['message']);
    				$('#btnSaveIt').prop('disabled',false); 
    				return false;
    			}   
    		}        
    	}); 



}); 

</script>
<script>
	function onlyNumbersWithDot(e) {           
		var charCode;
		if (e.keyCode > 0) {
			charCode = e.which || e.keyCode;
		}
		else if (typeof (e.charCode) != "undefined") {
			charCode = e.which || e.keyCode;
		}
		if (charCode == 46)
			return true
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}
</script>

<script>


	$("#date_treatment_initiation" ).change(function( event ) {

			// for 7 greater days
			<?php if(count($tblpatientdispensationb_list) == 0){ ?>
				var date =$("#prescribing_date" ).datepicker('getDate'); 
				//alert(date);
			<?php } else{?>
				var date = $("#NextVisitdate" ).datepicker('getDate');
				//alert('ff');
			//alert(date);
		<?php } ?>
		var visit_date = $("#date_treatment_initiation" ).datepicker('getDate');
			//alert(visit_date+'/'+date);

			var remaningdays = 5;
			days = parseInt(remaningdays, 10);
			if(!isNaN(date.getTime())){
				date.setDate(date.getDate() + days);
			}

			// for 7 less days
			<?php if(count($tblpatientdispensationb_list) == 0){ ?>

				var date1 = $("#prescribing_date" ).datepicker('getDate'); 
			<?php } else{?>
				var date1 = $("#NextVisitdate" ).datepicker('getDate');
			<?php }?>

			days1 = parseInt(remaningdays, 10);
			
			if(!isNaN(date1.getTime())){
				date1.setDate(date1.getDate() - days1);
			}
			<?php if(count($tblpatientdispensationb_list) == 0){ ?>
				var d = $("#prescribing_date" ).val(); 
			<?php } else { ?>
				//alert('fff');
				var d = $("#NextVisitdate" ).val(); 
			<?php } ?>

			//if(date < visit_date || date1 > visit_date){
				//alert(date+'/'+days1+'/'+date1+'/'+visit_date);
				if(date1 > visit_date){

				<?php if(count($tblpatientdispensationb_list) == 0){ ?>
				$("#modal_header").text("Date Of Treatment Initiation greater than or equal to Prescribing Date");
				$("#modal_text").text("Please check dates");
				$("#date_treatment_initiation" ).val('');
				$("#multipurpose_modal").modal("show");
				return false;
			<?php 	}else{ ?>
					//$("#modal_header").text("Visit Date cannot be before " +d);
					$("#modal_header").text("Visit Date should be less than 6 days from " +d);
				$("#modal_text").text("Please check dates");
				$("#date_treatment_initiation" ).val('');
				$("#multipurpose_modal").modal("show");
				return false;
				<?php }  ?>
			}

		});	


	$("#pills_left" ).change(function() {
		
		pills_givencal();



		

	});

	$("#PillsDaysDispensed" ).change(function() {
		
		pills_givencal();

	});



	

	
		$("#date_treatment_initiation" ).change(function( event ) {
		
		pills_givencal();
	});

	function pills_givencal()
	{
		var pills_leftd = $("#pills_left").val();
		var PillsTakenDaily = $('#PillsTakenDaily').val();

		var PillsDaysDispensed = $('#PillsDaysDispensed').val();
		//alert(PillsDaysDispensed);
		if(PillsDaysDispensed==1){
			var countDispensed = 31;
		}else if(PillsDaysDispensed==2){
			var countDispensed = 61;
		}else if(PillsDaysDispensed==3){
			var countDispensed = 91;

		}
		if(PillsTakenDaily==1){
			var pillsleftdata= pills_leftd;
		}else{
			var pillsleftdata= pills_leftd/2;
		}
		$("#daypills_left" ).val(pillsleftdata);


		var date = $("#date_treatment_initiation").datepicker('getDate'); 
			//alert(date);
			<?php if(count($tblpatientdispensationb_list) == 0){ ?>
				var datedb = $("#prescribing_date" ).datepicker('getDate');
			<?php } else { ?>
				var datedb = $("#NextVisitdate" ).datepicker('getDate');
			<?php  } ?>

			var pills_left =0;
			var pills_given = parseInt($('#PillsDaysDispensed option:selected').text());
			 //alert(pills_given);
			var pills_left = $('#pills_left').val();
			if(pills_left == ''){
				var pills_left = 0;
			}else{
				var pills_left = parseInt(pills_left);
			}

			var pills_given1 = parseInt($('#PillsDaysDispensed option:selected').text());
			var pills_left1 = parseInt($('#pills_left').val());

			if(pills_left1 > pills_given1){

				$("#modal_header").text("Pills Left not greater then Pills Given");
				$("#modal_text").text("haemoglobin greater than zero");
				$('#pills_left').val('');
				$("#multipurpose_modal").modal("show");
				return false;
			}

			var minutes = 1000*60;
			var hours = minutes*60;
			var days = hours*24;

			var diff_date = Math.round((date - datedb)/days);
			
 //alert(pills_given1+'/'+pills_given);
			var remaningpills = parseInt(pills_given)+ parseInt(pills_left);
		 //alert(pills_given1);
			//var remaningpillsval = (remaningpills-3);
			var remaningpillsval = (remaningpills-0);
			days1 = parseInt(remaningpillsval, 10);
			if(diff_date>1){


				var pills_left1 = countDispensed-(diff_date)-pills_left;
				
			}
			else{
				if(diff_date>0){
					var diff_date1 = diff_date-pills_left;
				}else{
					var diff_date1 = 0;
				}
				var pills_left1 = countDispensed-pills_left-diff_date;
				
			}
//alert('/'+pills_left1);
			var remaningpills = parseInt(pills_given) + parseInt(pills_left);
			//alert(remaningpills);
			//var remaningpillsval = (remaningpills - 3);
			//var remaningpillsval = (remaningpills-3);
			var remaningpillsval = (remaningpills-3);
			//alert(remaningpillsval);
			//var Adherence = ( ( pills_left1 ) * 100) / 28;
			//alert(remaningpills+'/'+remaningpillsval);
			var Adherence = ( ( pills_left1+4 ) * 100) / pills_given1;
			days = parseInt(remaningpillsval, 10);

			if(!isNaN(date.getTime())){
				date.setDate(date.getDate() + days);


				if(Adherence>100){
					var Adherence = 100
				}else if(Adherence < 0){
					var Adherence = 0;
				}
				else{
					var Adherence = Adherence;
				}
				if(date!='' && $('#pills_left').val()!=''){

					$('#adherence').val(Adherence.toFixed(2));
				}
				$("#advised_visit_date").val(date.toInputFormat());
				$("#advised_visit_datesvr").val();


			}

			/*Advised SVR Date start*/
			var remaningpillsval =  84;
			days = parseInt(remaningpillsval, 10);

			if(!isNaN(date.getTime())){
				date.setDate(date.getDate() + days);

				$('#advised_visit_datesvr').val(date.toInputFormat());
			}
			/*Advised SVR Date ens*/



			if(Adherence==100){
				$('.adherence_field').hide();
				$('#reason_low_adherence').prop('required',false);

			}else{
				$('.adherence_field').show();
				$('#reason_low_adherence').prop('required',true);
			}
		}

		Date.prototype.toInputFormat = function() {
			var yyyy = this.getFullYear().toString();
	        var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
	        var dd  = this.getDate().toString();
	        return (dd[1]?dd:"0"+dd[0]) + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + yyyy; // padding
	      };
$(".adherence_field").hide();
$('.reason_low_adherence_fields').hide();
$("#adherence").change(function(){
				if($(this).val() < 100)
				{
					$(".adherence_field").show();
				}
				else
				{
					$(".adherence_field").hide();
				}
			});
$("#reason_low_adherence").change(function(){
	if($(this).val() == 99)
	{
		$('.reason_low_adherence_fields').show();
		$('#reason_low_adherence_other').val('');

	}
	else
	{
		
		$('.reason_low_adherence_fields').hide();
		
	}
});	     



		$("#side_effects99").click(function(){
					if($(this).val() == 99 && $(this).prop("checked") == true)
					{
						
						$(".side_effects_other_field").show();
						$('#side_effect_other').prop('required',true);
					}
					else
					{
						$(".side_effects_other_field").hide();
						$('#side_effect_other').val('');
						$('#side_effect_other').prop('required',false);
						
					}
				});
<?php if(count($visit_details) > 0 && !empty($visit_details[0]->SideEffect)) {?>
$(".side_effects_other_field").show();
<?php } else{?>
$(".side_effects_other_field").hide();
<?php  } ?>


$(document).ready(function(){
    $('#side_effects10').on('change', function(){        
        if($('#side_effects10:checked').length){
            //or $('.class3').prop({disabled: 'disabled', checked: false});
            $('#side_effects1').prop('disabled', true);
            $('#side_effects1').prop('checked', false);
            $('#side_effects2').prop('disabled', true);
            $('#side_effects2').prop('checked', false);
            $('#side_effects3').prop('disabled', true);
            $('#side_effects3').prop('checked', false);
            $('#side_effects4').prop('disabled', true);
            $('#side_effects4').prop('checked', false);
            $('#side_effects5').prop('disabled', true);
            $('#side_effects5').prop('checked', false);
            $('#side_effects6').prop('disabled', true);
            $('#side_effects6').prop('checked', false);
            $('#side_effects7').prop('disabled', true);
            $('#side_effects7').prop('checked', false);
            $('#side_effects8').prop('disabled', true);
            $('#side_effects8').prop('checked', false);
            $('#side_effects9').prop('disabled', true);
            $('#side_effects9').prop('checked', false);
            $('#side_effects99').prop('disabled', true);
            $('#side_effects99').prop('checked', false);
             $('#side_effect_other').prop('disabled', true);
            return;
        }

        $('#side_effects1').prop('disabled', false);
        $('#side_effects2').prop('disabled', false);
        $('#side_effects3').prop('disabled', false);
        $('#side_effects4').prop('disabled', false);
        $('#side_effects5').prop('disabled', false);
        $('#side_effects6').prop('disabled', false);
        $('#side_effects7').prop('disabled', false);
        $('#side_effects8').prop('disabled', false);
        $('#side_effects9').prop('disabled', false);
        $('#side_effects99').prop('disabled', false);
         $('#side_effect_other').prop('disabled', false);

    });

 if($('#side_effects10').prop("checked") == true){

 		$('#side_effects1').prop('disabled', true);
        $('#side_effects2').prop('disabled', true);
        $('#side_effects3').prop('disabled', true);
        $('#side_effects4').prop('disabled', true);
        $('#side_effects5').prop('disabled', true);
        $('#side_effects6').prop('disabled', true);
        $('#side_effects7').prop('disabled', true);
        $('#side_effects8').prop('disabled', true);
        $('#side_effects9').prop('disabled', true);
        $('#side_effects99').prop('disabled', true);
         $('#side_effect_other').prop('disabled', true);
         
        	
}  

}); 
	    </script>