<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>User Portal</title>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/bootstrap-select.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/styles_chai.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/datatable.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/jquery-ui.min.css"/>
  <script src="<?php echo site_url('common_libs');?>/js/jquery.min.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/jquery-ui.min.js"></script>
  
  <script src="<?php echo site_url('common_libs');?>/js/charts.min.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/chartsjs-plugin-deferred.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/bootstrap.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/velocity.min.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/velocity.ui.min.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/html2canvas.js"></script>

<link href="<?php echo site_url(); ?>assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css"/>

<!-- chat -->
<script src="<?php echo site_url('common_libs');?>/js/Chart.bundle.min.js"></script>
  
  <script src="<?php echo site_url('common_libs');?>/js/graph/Chart.bundle.min.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/graph/Chart.min.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/graph/chartjs-plugin-piechart-outlabels.js"></script>
  
  <script src="<?php echo site_url('common_libs');?>/js/index.js"></script>
<script src="<?php echo site_url('common_libs');?>/js/chartjs-plugin-labels.js"></script>
<script src="<?php echo site_url('common_libs');?>/js/graph/chartjs-plugin-doughnutlabel.js"></script>
 <script src="<?php echo site_url('common_libs');?>/js/graph/chartjs-plugin-datalabels.js"></script>
 <script src="<?php echo site_url('common_libs');?>/js/graph/Chart.roundedBarCharts.min.js"></script>

 <script src="<?php echo site_url('application');?>/third_party/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo site_url('application');?>/third_party/js/dataTables.bootstrap.min.js"></script>
<!-- <link href="<?php echo site_url(); ?>assets/css/jq_ui.css" rel="stylesheet" type="text/css"/> -->
<!-- <script src="<?php //echo site_url(); ?>assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> -->
<?php //echo $flag[0]->flag; ?>
  <style>

  .ui-datepicker .ui-datepicker-title select {
    font-weight: normal !important;
    color: #333 !important;
    font-size: 12px;
  
}
  .header_image
  {
   height: 79px;
    width: auto;
    margin-top: 14px;
    /*margin-left:10px;*/
  }
  .header_image1
  {
    height: 73px;
    width: auto;
    margin-top: 9px;
    /*margin-left:10px;*/
  }

  input.dateInpt {
    background: #fff url(<?php echo base_url(); ?>assets/img/calendar.gif) 98% 5px no-repeat;
    padding-right: 24px;
}
@media (min-width: 720px){
 #loading_gif{position: fixed; top: 50%;left: 43%; height: 140px; width: auto; z-index : 999;}
}
@media (max-width: 720px){
  #loading_gif{position: fixed; top: 50%;left: 30%; height: 140px; width: auto; z-index : 999;}
}

</style>
</head>

<?php if(isset($flag)){  if($flag[0]->flag==1) { ?>
<script>

$(function(){
  $('li').hide();
});

</script>
<?php } }  ?>


<script>


 <?php  if ($this->session->userdata('loginData') != NULL){ ?>
  var session_expiry = new Date(new Date().getTime() + <?php echo $this->config->item('sess_expiration') ?> * 60000);
  var page_head_interval_counter = setInterval(check_session_expiry, 1000);
  function check_session_expiry() 
  {
    var now = new Date().getTime();
    var distance = session_expiry - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    if (minutes < 5) 
    {
      clearInterval(page_head_interval_counter);
      var session_confirmation = confirm('Your session is about to expire in 5 minutes. If you want to coninue your session, press OK')
      if (session_confirmation == true) 
      {
        $.get('<?php echo site_url('ajax/session_reset') ?>', function(raw){
          session_expiry = new Date(new Date().getTime() + <?php echo $this->config->item('sess_expiration') ?> * 60000);
          page_head_interval_counter = setInterval(check_session_expiry, 1000);
        });
      }
    }
  }
<?php } ?>


  $(document)
  .ajaxStart(function () {
    $("#loading_gif").show();
  })
  .ajaxStop(function () {
    $("#loading_gif").hide();
  });

  $(document).ready(function(){
    $("#loading_gif").hide();
  });
</script>

<body id="top">
    <img id="loading_gif" src="<?php echo site_url('application');?>/third_party/images/spinner.gif" alt="loading_gif" />
  <div class="container-fluid">

      <div class="row">

        <!-- <div class="col-md-2  text-left col-xs-2">
        <img class="header_image" src="<?php //echo site_url('application/third_party');?>/images/moh_goi_logo.png" />
      </div> -->
      
   <!--       <div class="col-md-2 text-right col-xs-4">
        <img class="header_image" src="<?php //echo base_url('application/third_party');?>/images/logo_bw.png" />
      </div> 
       
      <div class="col-md-4 text-center col-xs-5">
        <h3 class="text-center">National Viral Hepatitis Control Program</h3>
      </div>
       <div class="d-block d-lg-none col-md-2  col-xs-4">
        <img class="header_image" src="<?php //echo base_url('application/third_party');?>/images/logo2.png" style="margin-top: 8px;" />
      </div> -->

      <div class="col-md-2 col-md-offset-2  col-xs-3">
        <img class="header_image" src="<?php echo base_url('application/third_party');?>/images/nhm_logo.png" />
      </div>
      <div class="col-md-5 text-center col-xs-5">
        <h3 class="text-center" style="margin-top: 32px;">National Viral Hepatitis Control Program</h3>
      </div>
      <div class="col-md-2 text-center col-xs-3">
        <img class="header_image1" src="<?php echo site_url('application/third_party');?>/images/logo2.png" />
      </div>
    </div>