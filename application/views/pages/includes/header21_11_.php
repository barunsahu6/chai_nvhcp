<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>User Portal</title>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/bootstrap-select.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/styles_chai.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/datatable.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/jquery-ui.min.css"/>
  <script src="<?php echo site_url('common_libs');?>/js/jquery.min.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/jquery-ui.min.js"></script>
  
  <script src="<?php echo site_url('common_libs');?>/js/charts.min.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/chartsjs-plugin-deferred.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/bootstrap.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/velocity.min.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/velocity.ui.min.js"></script>

<link href="<?php echo site_url(); ?>assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css"/>

<!-- chat -->
<script src="<?php echo site_url('common_libs');?>/js/Chart.bundle.min.js"></script>
  
  <script src="<?php echo site_url('common_libs');?>/js/graph/Chart.bundle.min.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/graph/Chart.min.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/graph/chartjs-plugin-piechart-outlabels.js"></script>

<script src="<?php echo site_url('common_libs');?>/js/chartjs-plugin-labels.js"></script>

<!-- <link href="<?php echo site_url(); ?>assets/css/jq_ui.css" rel="stylesheet" type="text/css"/> -->
<!-- <script src="<?php //echo site_url(); ?>assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> -->
<?php //echo $flag[0]->flag; ?>
  <style>

  .ui-datepicker .ui-datepicker-title select {
    font-weight: normal !important;
    color: #333 !important;
    font-size: 12px;
  
}
  .header_image
  {
    height: 85px;
    width: auto;
    /*margin-left:10px;*/
  }
  .header_image1
  {
    height: 73px;
    width: auto;
    margin-top: 9px;
    /*margin-left:10px;*/
  }

  input.dateInpt {
    background: #fff url(<?php echo base_url(); ?>assets/img/calendar.gif) 98% 5px no-repeat;
    padding-right: 24px;
}

</style>
</head>

<?php if(isset($flag)){  if($flag[0]->flag==1) { ?>
<script>

$(function(){
  $('li').hide();
});

</script>
<?php } }  ?>


<script>
  $(document)
  .ajaxStart(function () {
    $("#loading_gif").show();
  })
  .ajaxStop(function () {
    $("#loading_gif").hide();
  });

  $(document).ready(function(){
    $("#loading_gif").hide();
  });
</script>

<body id="top">
    <img id="loading_gif" src="<?php echo site_url('application');?>/third_party/images/spinner.gif" style="position: fixed; top : 40vh; left : 45vw; height: 100px; width: auto; z-index : 999;" alt="loading_gif" />
  <div class="container-fluid">

      <div class="row">

        <!-- <div class="col-md-2  text-left col-xs-2">
        <img class="header_image" src="<?php //echo site_url('application/third_party');?>/images/moh_goi_logo.png" />
      </div> -->
      
   <!--       <div class="col-md-2 text-right col-xs-4">
        <img class="header_image" src="<?php //echo base_url('application/third_party');?>/images/logo_bw.png" />
      </div> 
       
      <div class="col-md-4 text-center col-xs-5">
        <h3 class="text-center">National Viral Hepatitis Control Program</h3>
      </div>
       <div class="d-block d-lg-none col-md-2  col-xs-4">
        <img class="header_image" src="<?php //echo base_url('application/third_party');?>/images/logo2.png" style="margin-top: 8px;" />
      </div> -->

      <div class="col-md-2 col-md-offset-2  col-xs-3">
        <img class="header_image" src="<?php echo base_url('application/third_party');?>/images/logo_bw.png" />
      </div>
      <div class="col-md-5 text-center col-xs-5">
        <h3 class="text-center" style="margin-top: 32px;">National Viral Hepatitis Control Program</h3>
      </div>
      <div class="col-md-2 text-center col-xs-3">
        <img class="header_image1" src="<?php echo site_url('application/third_party');?>/images/logo2.png" />
      </div>
    </div>