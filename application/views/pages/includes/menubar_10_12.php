<?php
$userdata = $this->session->userdata('loginData');
//print_r($userdata);
?>

<style>
.navbar-nav > li:hover
{
  background-color: #e6c478; !important;
  color: black !important;
}

.navbar-inverse .navbar-nav > li > a:hover {
    color: black;
    background-color: #e6c478;
}

.active
{
   background-color: #e6c478; !important;
   color: black !important;
}
</style>
<div class="row" style="margin-top: 4px;">
  <nav class="navbar navbar-inverse fixed-menu" style="border: none;">
   <div class="container-fluid" style="background-color: black;">
      <div class="navbar-header">
     <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar" aria-expanded="false">
       <span class="sr-only">Toggle navigation</span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
     </button>
   </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
          <?php
             if($userdata->user_type == 2) 
             {
              ?>
           <li class="dropdown <?php echo ($this->uri->segment(1) == 'patientinfo')?'active':''; ?>"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo 'Patient Information'; ?> <span class="caret" ></span></a>
           <!--  <ul class="dropdown-menu" style="width : 100%;">
              <li><a href="<?php echo site_url('patientinfo')?>?p=1">Patient Registration</a></li>
              <li><a href="<?php echo site_url('patientinfo')?>?p=2">Test and Result</a></li>
              <li><a href="<?php echo site_url('patientinfo')?>?p=3">Treatment </a></li>
            </ul> -->
  <ul class="dropdown-menu">
            <?php if($userdata->user_type == 2) {?>
                  <li class="dropdown-submenu"><a tabindex="-1" href="#"><?php echo 'HEP-C'; ?></a>
                  <ul class="dropdown-menu" >
                 <li><a href="<?php echo site_url('patientinfo')?>?p=1">Patient Registration</a></li>
              <li><a href="<?php echo site_url('patientinfo')?>?p=2">Test and Result</a></li>
              <li><a href="<?php echo site_url('patientinfo')?>?p=3">Treatment </a></li>
                  </ul>
                </li>

                  <li class="dropdown-submenu"><a tabindex="-1" href="#"><?php echo 'HEP-B'; ?></a>
                  <ul class="dropdown-menu" >
                  <li><a href="<?php echo site_url('patientinfo_hep_b')?>?p=1">Patient Registration</a></li>
              <li><a href="<?php echo site_url('patientinfo_hep_b')?>?p=2">Test and Result</a></li>
              <li><a href="<?php echo site_url('patientinfo_hep_b')?>?p=3">Treatment </a></li>
                  </ul>
                </li>

              <?php } ?>
</ul>
          </li>



<?php } ?>

          <li class="<?php echo ($this->uri->segment(1) == 'dashboard')?'active':''; ?>"><a href="<?php echo site_url('dashboard');?>">Dashboard </a></li>
          <li class="<?php echo ($this->uri->segment(1) == 'reports')?'active':''; ?>"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo 'Reports'; ?> <span class="caret" ></span></a>
            <ul class="dropdown-menu">
                <li ><a href="<?php echo site_url('reports/monthly_report')?>">Monthly Report</a></li>
                 <li ><a href="<?php echo site_url('reports/lfu_report')?>">LFU Report</a></li>
                 <li ><a href="<?php echo site_url('reports/reasons_for_death')?>">Reasons for Death</a></li>
                  <li><a href="<?php echo site_url('Hepb_vaccination/index/Report')?>">Hepatitis B Vaccination Report</a></li>
                 <li ><a href="<?php echo site_url('reports/hmis_report')?>">HMIS</a></li>
                <!--  <li ><a href="<?php //echo site_url('reports/inventory_management')?>">Drug Stock Report</a></li> -->
                 <?php if($userdata->user_type != 1) {?>
                 <li ><a href="<?php echo site_url('linelist')?>">Line List</a></li>
                <?php } ?>

                <?php if($userdata->user_type == 1) {?>
                  <li class="dropdown-submenu"><a tabindex="-1" href="#"><?php echo 'Niti Aayog Report'; ?></a>
                  <ul class="dropdown-menu" >
                 <li ><a href="<?php echo site_url('reports/niti_aayog')?>">Consolidated Report</a></li>
                    <li ><a href="<?php echo site_url('reports/niti_aayog_national')?>">State Wise Report</a></li>
                  </ul>
                </li>

              <?php } ?>
            </ul>
          </li>
          <!-- <li class="<?php echo ($this->uri->segment(1) == 'patientinfo')?'active':''; ?>"><a href="<?php echo site_url('patientinfo')?>">Patient Information</a></li> -->

<?php   if( $userdata->user_type == 3) { ?>
  
 <li class="dropdown <?php echo ($this->uri->segment(1) == 'users' || $this->uri->segment(1) == 'facilities')?'active':''; ?>"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo 'Masters'; ?> <span class="caret" ></span></a>
            <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url('facilities')?>">Add Facilities</a></li>
               <li><a href="<?php echo site_url('users')?>">Manage Users</a></li>
               <li><a href="<?php echo site_url('linelist_masters')?>">Linelist Masters</a></li>

             <!--  <li><a href="<?php echo site_url('risk_factors')?>">Risk Factors</a></li>
              <li><a href="<?php echo site_url('drugs')?>">Drugs </a></li>
              <li><a href="<?php echo site_url('drug_strengths')?>">Drug Strengths </a></li>
              <li><a href="<?php echo site_url('regimen')?>">Manage Regimen </a></li>
              <li><a href="<?php echo site_url('regimen_rules')?>">Manage Regimen Rules </a></li>
            <li><a href="<?php echo site_url('lookup')?>">Manage Lookup </a></li> -->  
            </ul>
          </li>
          <?php } ?>
 <?php   if($userdata->user_type == 2 ){ ?>
          <li class="dropdown <?php echo ($this->uri->segment(1) == 'users')?'active':''; ?>" ><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo 'Masters'; ?> <span class="caret" ></span></a>
            <ul class="dropdown-menu">
              <?php //if($userdata->RoleId == '1'){ ?>
               <li><a href="<?php echo site_url('users')?>">Manage Users</a></li>
             <?php //} ?>
               <li><a href="<?php echo site_url('users/add_doctor')?>">Manage Doctors</a></li>
              <li><a href="<?php echo site_url('users/add_designation')?>">Sample Transporter Designation</a></li>
            
             
            </ul>
          </li>
<?php } ?>

          <?php   if($userdata->user_type == 1 ){ ?>
  
 <li class="dropdown <?php echo ($this->uri->segment(1) == 'users')?'active':''; ?> <?php if($this->uri->segment(1) == 'users' || $this->uri->segment(1) == 'facilities' || $this->uri->segment(1) == 'risk_factors' || $this->uri->segment(1) == 'drugs' || $this->uri->segment(1) == 'drug_strengths' || $this->uri->segment(1) == 'regimen' || $this->uri->segment(1) == 'regimen_rules' ||$this->uri->segment(1) == 'lookup'){ echo 'active'; }?>"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo 'Masters'; ?> <span class="caret" ></span></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo site_url('facilities')?>">Facilities</a></li> 
               <li><a href="<?php echo site_url('users')?>">Manage Users</a></li>
                <li ><a href="<?php echo site_url('users/eHRManagement')?>">EHR Management</a></li>
            
             <!--  <li><a href="<?php //echo site_url('risk_factors')?>">Risk Factors</a></li>
              <li><a href="<?php //echo site_url('drugs')?>">Drugs </a></li>
              <li><a href="<?php //echo site_url('drug_strengths')?>">Drug Strengths </a></li>
              <li><a href="<?php //echo site_url('regimen')?>">Manage Regimen </a></li>
              <li><a href="<?php //echo site_url('regimen_rules')?>">Manage Regimen Rules </a></li>
              <li><a href="<?php //echo site_url('lookup')?>">Manage Lookup </a></li> -->
            </ul>
          </li>
          <?php } ?>
<li><a href="http://nvhcp.gov.in/assets/NVHCPtest26112019).apk" download>Download Android App</a></li>
<li><a href="#" data-toggle="modal" data-target="#myModal">Helpdesk</li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <?php
             if($userdata->id_tblusers == 1) 
             {
              ?>
                    <li class="<?php echo ($this->uri->segment(1) == 'dashboard')?'active':''; ?>"><a href="<?php echo site_url('Admin');?>">login-log</a></li>
                   <?php } ?>
          <li class="dropdown "><?php echo ucfirst($userdata->Operator_Name); ?> <span class="caret" style="color:#000;"></span></a>
            <ul class="dropdown-menu" style="width : 100%;">
              <li><a href="<?php echo site_url('login/logout');?>">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</div>

 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center ">Helpdesk</h4>
        </div>
        <div class="modal-body bg-info">
<p class="text-center " style="font-size: 14px;"><b>Mobile No. - +91 7906842633 </b></p>
<p class="text-center" style="font-size: 14px;"> <b>Email Id - <a href="mailto:helpdesk.nvhcp@gmail.com" target="_top">helpdesk.nvhcp@gmail.com</a> </b></p>
<p class="text-center" style="font-size: 14px;"><b>Availability  - 10 AM TO 6 PM (Monday - Saturday)</b></p>


        </div>
        <div class="modal-footer ">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

