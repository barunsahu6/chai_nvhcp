<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Welcome To NVHCP</title>
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('application/third_party');?>/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('application/third_party');?>/css/styles_chi.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo site_url('application/third_party');?>/css/font-awesome.min.css"/>
	<script src='https://www.google.com/recaptcha/api.js'></script>

	<style>

	img {
		width : auto;
		/*height: 19vh !important;*/
	}

	.btn-black
	{
		color: white;
		font-weight : 600;
		background-color: black;
		border-radius : 0;
		border : none;
	}
	.btn-black:hover
	{
		color: black;
		font-weight : 600;
		background-color: white;
		border-radius : 0;
		border : 2px solid #000;
	}

	.input_fields
	{
		height: 40px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 20px !important;
	}
		.field-icon {
		float: right;
		margin-left: -25px;
		margin-top: -25px;
		position: relative;
		z-index: 2;
		}

		.container{
		padding-top:50px;
		margin: auto;
		}

</style>
</head>
<body>
	
	<br />
	<br />

	 <!-- <div class="row">
		<div class="col-md-2 col-md-offset-4 col-xs-6 text-center">
			<img src="<?php echo base_url('application/third_party');?>/images/logo_bw.png" />
		</div>
		<div class="col-md-2 col-xs-6 text-center">
			 <img src="<?php echo site_url('application/third_party');?>/images/moh_goi_logo.png" />
		</div>
	</div>  -->

	<div class="row">

		

<div class="col-md-2 col-md-offset-3 col-xs-6 text-right">
			<a href="http://nhm.gov.in" target="_blanck"><img src="<?php echo base_url('application/third_party');?>/images/nhm_logo.png" style="height: 85px;margin-top: 69px; margin-right: 74px" /></a>
		</div>
		
		<div class="col-md-2 col-xs-6 text-center">
			<a href="https://www.india.gov.in" target="_blanck"> <img src="<?php echo site_url('application/third_party');?>/images/moh_goi_logo.png" style="height: 188px;margin-top: -24px; margin-left: -44px; " /></a>
		</div>
		
		<div class="col-md-2 col-xs-6 text-center">
			<a href="<?php echo base_url(); ?>"> <img src="<?php echo site_url('application/third_party');?>/images/logo_nvhcp_2.png"   style="height: 81px;margin-top: 67px;     margin-left: 102px;"/></a>
		</div>

	</div>
	

	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<h3 class="text-center">Program Management System for NVHCP</h3>
		</div>

		<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){ ?>
			<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				<?php } ?>

			</div>



			<?php
			$attributes = array(
				'onsubmit' => 'return myFunction()',
				'id'       => 'myForm',
				'autocomplete' => 'off',
			);
			echo form_open(site_url('login/verifylogin'), $attributes); ?>		
			<input type="hidden" id="salt" value="<?php echo $salt ?>">

			<div class="row">
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<label for="username">Username</label>
					<input type="text" autocomplete="off" class="form-control input_fields" name="username" id="username"/>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<label for="username">Password</label>
					<input type="password" autocomplete="off" class="form-control input_fields" name="password" id="password"/>
					<!-- <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span> -->
				</div>
			</div>
			<br/>
			

			<!-- <div class="row">
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					
					<span id="image_captcha"><?php echo $captchaImg; ?></span><a href="javascript:void(0);" class="captcha-refresh" ><img height="30" width="30" src="<?php echo base_url().'assets/images/Refresh.png'; ?>"/></a><br/><br/>
					
                       <input type="text" name="captcha" value="" class="form-control" autocomplete="off"/>
                        
				</div>
			</div><br/> -->

			
			
			<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
		    <input type="checkbox" class="form-check-input" id="termsandcondition" name="termsandcondition">
		     <label class="form-check-label" for="exampleCheck1"><a href="#" data-toggle="modal" data-target="#myModal" style="color: #333">I accept the terms and conditions</a></label>
		  </div> <!-- data-toggle="modal" data-target="#myModal" -->
		 <!--  <div class="row">
				<div class="col-md-4 col-md-offset-5 col-xs-6 col-xs-offset-3">
					<hr />
					<div class="g-recaptcha" data-sitekey="6Le6j5cUAAAAAEPUqM3QDl8JIFc2ie2k9pD09PeF"></div>
				</div>
			</div> -->
			<br /><br/>
			<div class="row">
				<div class="col-md-2 col-md-offset-5 col-xs-6 col-xs-offset-3">
					<input type="submit" class="btn btn-block btn-black"  name="submit" value="Login"/>
				</div>
			</div>
			<?php echo form_close(); ?>
			<br />
			<br />

			 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Terms and conditions</h4>
        </div>
        <div class="modal-body">
<p>a.       I will make the entries myself.</p>

<p>b.       I will not intentionally enter data incorrectly.</p>

<p>c.       I will ensure the data which I have access to is kept confidential at all times.</p>

<p>d.       I will not share the data I have access to with anyone in any format.</p>

<p>e.       I will complete my work on time and diligently.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>


			<script type="text/javascript" src="<?php echo site_url('application/third_party');?>/js/jquery.min.js"></script>
			<script type="text/javascript" src="<?php echo site_url('application/third_party');?>/js/bootstrap.min.js"></script>
			<script src="<?php echo site_url('application/third_party');?>/js/crypto-js.min.js"></script>
 <script>
       $(document).ready(function(){
        
          $('.captcha-refresh').on('click', function(){
               $.get('<?php echo base_url().'login/refresh'; ?>', function(data){
                   $('#image_captcha').html(data);
               });
           });
       });
   </script>
			<script>

				$(".toggle-password").click(function() {

				$(this).toggleClass("fa-eye fa-eye-slash");
				var input = $($(this).attr("toggle"));
				if (input.attr('type') == 'password') {
				input.attr("type", "text");
				} else {
				input.attr("type", "password");
				}
				});

				function myFunction() 
				{           
					var password = $('#password').val();
					var salt = $('#salt').val();
					$('#password').val(compute_hashed_password(password, salt));
					return true;
				}

				function compute_hashed_password(password, salt) 
				{
					var first_pass = CryptoJS.SHA256(password).toString();
					var second_pass = CryptoJS.SHA256(first_pass.concat(salt)).toString();
					console.log(second_pass);
					return second_pass;
				}


			</script>
		</body>
		</html>