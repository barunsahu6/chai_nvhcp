<style type="text/css">
	.form-control
	{
		min-height: 35px;
	}
</style>
<div class="row main-div" style="min-height:448px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

		<div class="panel panel-default">
			<div class="panel-heading" style="color: white;background-color: #333333;">
				<h4 class="panel-title text-center" style="color: white;background-color: #333333;"><b>Edit State wise <?php echo ($State_Details->type==1 ? "Drugs " : ($State_Details->type==2 ? "Screening Kits" : " HBIG")) ?> Lead Time</b></h4>
			</div>

			<div class="panel panel-body">
				<?php  $attributes = array(
              'id' => 'AddStock_indent_form',
              'name' => 'AddStock_indent_form',
               'autocomplete' => 'off',
            );  echo form_open('State_lead_time/edit/'.$State_Details->id_mststate, $attributes); ?>
				
					<div class="panel-body">
						<label for="stateCd">State Code</label>
						<input type="text" class="form-control" id="stateCd" name="stateCd" placeholder="Enter  Sate Code" value="<?php echo $State_Details->StateCd; ?>" readonly>
					</div>

					<div class="panel-body">
						<label for="stateName">State Name</label>
						<input type="text" class="form-control" id="stateName" name="stateName" placeholder="Enter state Name" value="<?php echo $State_Details->StateName;?>" readonly>
					</div>

					<div class="panel-body">
						<label for="lead_time">Lead time (in months) from State warehouse to facility</label>
						<input type="number" class="form-control" id="lead_time" name="lead_time" placeholder="Enter Lead Time" value="<?php echo $State_Details->lead_time;?>">
					</div>

					<div style="text-align:center;">	<button type="submit" class="btn btn-warning mr-3" >Submit</button><a href="<?php echo site_url('State_lead_time/index/'.$State_Details->type.'');?>" class="btn btn-warning" style="margin-left: 10px;">Back</a></div>

				 <?php echo form_close(); ?>

			</div>
		</div>
	</div>
</div>