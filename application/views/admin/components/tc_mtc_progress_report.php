<style>
thead
{
	background-color: #085786;
	color: white;
}

.table-bordered>tbody>tr>td.data_td
{
	font-weight: 600;
	font-size: 15px;
	text-align: center;
	vertical-align: middle;
}
</style>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 5 !important;
	width: 100% !important;
	font-size: 14px !important;
	font-family: 'Source Sans Pro';
}
textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
	.col-md-2{
		width: 16.33%;
		padding-left: 8px;
		padding-right: 8px;
	}
	.btn-width{
	width: 12%;
	margin-top: 20px;
}
.pd-15{
	padding-left: 15px;
}
.pb-20{
	padding-bottom: 20px;
}
.container{
	width: 76%;
}
}

@media (max-width: 768px) {

.input_fields
	{
		height: 40px !important;
		border-radius: 5 !important;
		width: 100% !important;
		font-size: 14px !important;
		font-family: 'Source Sans Pro';
	}



	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 13px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
.table-bordered>thead>tr>th{
	vertical-align: middle;
	font-size: 13px;
	font-family: 'Source Sans Pro';
	background-color: #085786;
}
.table-bordered>tbody>tr>td{
	vertical-align: middle;
	font-size: 13px;
	color: #000000;
	font-family: 'Source Sans Pro';
}
</style>
</style>
<br>

<?php $loginData = $this->session->userdata('loginData');
//echo "<pre>"; print_r($loginData);

if($loginData->user_type==1) { 
$select = '';
}else{
$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
$select4 = '';	
}

$filters1 = $this->session->userdata('filters1'); 
//pr($filters1);
 ?>
<div class="container">
		<div class="panel panel-default" id="Progress_HCV_Report_Panel">
				<div class="panel-heading" style="background-color: #333333;padding: 3px 0px;">
					<h4 class="text-center" style="background-color: #333333;color: white;font-family: 'Source Sans Pro';letter-spacing: .75px;">Progress at State and District Levels - MTCs, TCs
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/excel_black.png" alt=""></a> -->
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/pdf_color.png" alt=""></a> -->
		</h4>
		<!-- <input type="button" onclick="printDiv('printableArea')" value="Print" /> -->
		
</div>

<?php
           $attributes = array(
              'id' => 'filter_form',
              'name' => 'filter_form',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>
	<div class="row" style="padding-right: 30px;padding-left: 30px;padding-top:10px;text-align: center;">
					<div class="col-md-3 form-group col-md-offset-2">
						<label for="">From Date</label>
						<input type="text" class="form-control hasCalreport dateInpt" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')){ echo $this->input->post('startdate');}else{ echo timeStampShow(date('2019-04-01'));}  ?>"  onkeydown="return false" onkeyup="return false" required style="text-align: left;height: 30px;font-size: 14px;">
					</div>
					<div class="col-md-3">
						<label for="">To Date</label>
						<input type="text" class="form-control hasCalreport dateInpt" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')){echo $this->input->post('enddate');}else{echo timeStampShow(date('Y-m-d'));} ?>" onkeydown="return false" onkeyup="return false" required style="text-align: left;height: 30px;font-size: 14px;">
					</div>
					<div class="col-md-2">
						<label for="">&nbsp;</label>
						<button class="btn btn-block btn-success" style="line-height: 1.2; font-weight: 500;">SEARCH</button>
					</div><span style="margin-top: 30px;margin-right: 7px; float: right;"><a href="javascript:void(0)"><i class="fa fa-2x fa-download" id="excel_button" onclick="tableToExcel('testTable', 'W3C Example','Progress_state_district_level.xls')" title="Excel Download"></i></a></span>



 <?php echo form_close(); ?>
<br>
</div>
</div>
<div class="row">
	<div class="col-sm-12 col-md-12">	
		<table class="table table-bordered table-highlighted" id="testTable">

			

			
			<thead>

				

				<th style="background-color: #085786;color: white; width: 150px !important;vertical-align: middle;" class="text-center">Name of State/UT</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center" colspan="2">Number of model treatment centres</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Total number of districts</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Number of districts with at least one TC</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center" colspan="2">Number of treatment centers</th>

				
			</thead>

				
			<thead>

				

				<th style="background-color: #085786;color: white; width: 150px !important;vertical-align: middle;" class="text-center"></th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Targets</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Progress</th>
				<th style="background-color: #085786;color: white; width: 150px !important;vertical-align: middle;" class="text-center"></th>
				<th style="background-color: #085786;color: white; width: 150px !important;vertical-align: middle;" class="text-center"></th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Targets</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Progress</th>
				
				
			</thead>


			<tbody>
				<?php $total_1 = 0;
					$total_2 = 0;
					$total_3 = 0;
					$total_4 = 0;
					$total_5 = 0;
					$total_6 = 0;
					 if($filtered_hfm_data_mtc){  foreach ($filtered_hfm_data_mtc as $key => $value) { 
					 		$total_1 += $value->target_mtc;
					$total_2 += $value->established;
					$total_3 += $filtered_hfm_data_tc[$key]->district;
					$total_4 +=$filtered_hfm_data_tc[$key]->district_with_tc;
					$total_6 += $filtered_hfm_data_tc[$key]->established;
					$total_5 += $filtered_hfm_data_tc[$key]->target_tc;
				 } } ?>
					<tr style="background-color: #d5e4e6;color: black;">

					<td class="thclass text-center"><?php echo 'India'; ?></td>
					<td class="thclass text-center data_td"><?php echo $total_1; ?> </td>
					<td class="thclass text-center data_td"><?php echo $total_2; ?> </td>
					<td class="thclass text-center data_td"><?php echo $total_3; ?></td>
					<td class="thclass text-center data_td" ><?php echo $total_4; ?> </td>
					<td class="thclass text-center data_td"><?php echo $total_5; ?> </td>
					<td class="thclass text-center data_td"><?php echo $total_6; ?> </td>
					
					
				</tr>
				<?php 
 					if($filtered_hfm_data_mtc){  foreach ($filtered_hfm_data_mtc as  $key => $value) { 

					
					?>
				<tr>

					<td ><?php echo $value->StateName; ?></td>
					<td class="text-center data_td"><?php if($value->target_mtc=='0') {echo '-';}else{ echo $value->target_mtc;} ?> </td>
					<td class="text-center data_td"><?php if($value->established=='0'){ echo '-';}else{ echo $value->established;} ?> </td>
					
					<td class="text-center data_td"><?php if($filtered_hfm_data_tc[$key]->district=='0'){ echo '-';}else{ echo $filtered_hfm_data_tc[$key]->district;} ?> </td>
					
					<td class="text-center data_td"><?php if($filtered_hfm_data_tc[$key]->district_with_tc=='0'){ echo '-';}else{ echo $filtered_hfm_data_tc[$key]->district_with_tc;} ?> </td>
						<td class="text-center data_td"><?php if($filtered_hfm_data_tc[$key]->target_tc=='0'){ echo '-';}else{ echo $filtered_hfm_data_tc[$key]->target_tc;} ?> </td>
					
					<td class="text-center data_td"><?php if($filtered_hfm_data_tc[$key]->established=='0'){ echo '-';}else{ echo $filtered_hfm_data_tc[$key]->established;} ?> </td>

				
					
				</tr>
					<?php  ?>

<?php } } ?>


				
			</tbody>
		
			
		</table>
	</div>
</div>
</div>
<br>
<br>
<br>
<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name,filename) {
    if (!table.nodeType) table = document.getElementById(table)
            var newtable=document.createElement("table");
            var tbody =document.createElement("tbody");
             var row = document.createElement("tr");
             row.setAttribute("style", "background-color: black;color: white; font-family:Calibri;");
  				var cell1 = document.createElement("td");
  				cell1.colSpan=7;
  				cell1.innerHTML ="Data as on : - <?php echo date('jS \ F Y'); ?>";
  				row.appendChild(cell1);
 			 	tbody.appendChild(row);
 			 	newtable.appendChild(tbody);
             var clonedTable=newtable.cloneNode(true);
             var clonedTable1=table.cloneNode(true);
            clonedTable.appendChild(clonedTable1);
    var ctx = {worksheet: name || 'Worksheet', table: clonedTable.innerHTML}
    //window.location.href = uri + base64(format(template, ctx))
     var link = document.createElement('a');
    link.download =filename;
    link.href = uri + base64(format(template, ctx));
    link.click();
     clonedTable.remove();
  }
})()
</script>
<script>

	
$(document).ready(function(){
$('[data-toggle="tooltip"]').tooltip();

$("#search_state").trigger('change');


});
$('#search_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');
					//$('#input_district').val($('#input_district').val());
				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
					$('#mstfacilitylogin').html(data);
					
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

</script>