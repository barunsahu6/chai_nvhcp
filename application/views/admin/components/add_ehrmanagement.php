	<style>
		a{
			cursor: pointer;
		}
		.set_height
		{
			max-height: 425px;
			overflow-y: scroll;

		}
		.panel-heading {
			padding: 1px 15px;
		}
		label
		{
			padding-top: 5px;
		}
		.form-horizontal .control-label{
			text-align: left;
		}
		select[readonly] {
			background: #eee;
			pointer-events: none;
			touch-action: none;
		}
	</style>
	<?php 

	$loginData = $this->session->userdata('loginData'); ?>
	<div class="row main-div" style="min-height:400px;">
		<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">

			<div class="panel panel-default">

				<div class="panel-heading">
					<h4 class="text-left" style="color : white;">EHR Code List <a href="<?php echo site_url("users/eHRManagement")?>" style="color: white; cursor: pointer;"><i class="fa fa-refresh pull-right"></i></a></h4>
				</div>
				<div class="panel-body set_height">
					<table id="data-table-command" class="table table-striped table-bordered table-vmiddle table-hover">
						<thead>
							<tr>
								<th class="text-center">NVHCP Label</th>
								<th class="text-center">EHR Standard Code</th>
								<th class="text-center">EHR Standard Ref.</th>
								<th class="text-center">Commands</th>
							</tr>

						</thead>
						<tbody>
							<?php if($ehr_code_list){ foreach($ehr_code_list as $row){ ?>
								<tr>

									<td class="text-center" style="padding-top: 16px;"><?php echo $row->nvhcp; ?></td>
									<td class="text-center" style="padding-top: 16px;"><?php echo $row->ehr_code; ?></td>
									<td class="text-center" style="padding-top: 16px;"><?php echo $row->nvhcp_stand_ref; ?></td>

									<td class="text-center">
										<?php if($loginData->RoleId == '1' || $loginData->RoleId == 0 || $loginData->id_tblusers==$row->id_ehr_management){ ?>
											<a href="<?php echo site_url('users/eHRManagement/'.$row->id_ehr_management);?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
											<a href="#<?php //echo site_url('users/delete_eHRManagement/'.$row->id_ehr_management);?>" class="usersdelete" id="<?php echo $row->id_ehr_management; ?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
										<?php } ?>
									</td>

								</tr>

							<?php } }?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
			<div class="panel panel-default">

				<div class="panel-heading">
					<h4 class="text-left" style="color: white;" id="form_head">Add EHR</h4>
				</div>
				<div class="row">
					<?php 
					$tr_msg= $this->session->flashdata('tr_msg');
					$er_msg= $this->session->flashdata('er_msg');
					if(!empty($tr_msg)){  ?>


						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						<?php } else if(!empty($er_msg)){ ?>
							<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>

							<?php } ?>
						</div>
						<div class="panel panel-body ">

							<?php
							$attributes = array(
								'id' => 'users',
								'name' => 'users',
								'autocomplete' => 'off',
							);
							echo form_open('', $attributes); ?>
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

							<div class="row">

								<div class="col-xs-2 text-left">
									<label for="full_name">NVHCP Label<span class="text-danger">*</span></label>
								</div>
								<div class="col-xs-4">
									<input type="text" class="form-control messagedata" id="nvhcp" name="nvhcp" placeholder="Enter NVHCP" value="<?=(!empty($ehr_code->nvhcp)?$ehr_code->nvhcp:'')?>" required>
								</div>

								<div class="col-xs-2 text-left">
									<label for="usertype">EHR Standard Code<span class="text-danger">*</span></label>
								</div>
								<div class="col-xs-4">
									<input type="text" class="form-control messagedata" id="ehr_code" name="ehr_code" placeholder="Enter EHR Code" min="0" value="<?=(!empty($ehr_code->ehr_code)?$ehr_code->ehr_code:'')?>" required>
								</div>

							</div>
							<br>

							<div class="row " >

								<div class="col-xs-2 text-left">
									<label for="usertype">EHR Standard Ref.<span class="text-danger">*</span></label>
								</div>
								<div class="col-xs-4">
									<input type="text" class="form-control messagedata" id="stand_ref" name="stand_ref" placeholder="Enter EHR Code" min="0" value="<?=(!empty($ehr_code->nvhcp_stand_ref)?$ehr_code->nvhcp_stand_ref:'')?>" required>
								</div>

								<!--div class="col-xs-2 text-left state_districts" >
									<label for="email">Status<span class="text-danger">*</span></label>
								</div>
								<div class="col-xs-4 state_districts">
									<select name="status" id="status" class="input_fields form-control" required="required">
										<option selected disabled>Select Status</option>
										<option value="1" <?php if(!empty($ehr_code->status)){ if($ehr_code->status == 1) { echo 'selected'; } } ?>>Active</option>
										<option value="2" <?php if(!empty($ehr_code->status)){ if($ehr_code->status == 2) { echo 'selected'; } } ?>>InActive</option>
									</select>
								</div-->


							</div>							

							<br>
							<?php if($loginData->RoleId == '1' || $loginData->RoleId == 0 || $loginData->id_tblusers==$id_user){ ?>
								<div class="row">
									<div class="col-xs-12 text-center">
										<button type="submit" class="btn btn-warning" id="submit_btn">Submit</button>
									</div>
								</div>
							<?php } ?>
							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
			</div>

<script>

		$(document).ready(function(){

		/*$(".glyphicon-trash").click(function(e){
			var ans = confirm("Are you sure you want to delete?");
			if(!ans)
			{
				e.preventDefault();
			}
		});*/
});

	$('.usersdelete').click(function(){

var idval = $(this).attr('id');
//alert(idval);
            $.ajax({
               url : "<?php echo base_url().'users/delete_eHRManagement/'; ?>"+idval,
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'POST',
                success : function(data)
                {
                   location.href= '<?php echo base_url().'users/eHRManagement'; ?>'; 
                },
                error : function(error)
                {
                    alert('Error Fetching Districts');
                }
            })
        });
</script>


