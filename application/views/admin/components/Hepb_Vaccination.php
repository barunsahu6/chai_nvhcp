<style>
	.dataTables_wrapper .dataTables_paginate .paginate_button {
    
    padding: 0px 0px 0px 1px;
   
    text-decoration: none !important; 
    cursor: pointer; 
 
}
.input_fields
{
	height: 30px !important;
	border-radius: 5 !important;
	width: 100% !important;
	font-size: 14px !important;
	font-family: 'Source Sans Pro';
}
.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
.active{
	background-color: #fff;
}
.pagination>.active>a:hover{
background-color: #337ab7;
color: #fff;
}
.dataTables_filter input { width: 50px; }
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.hide { display:none;}
	.show { display:none;}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
	.hideplus{
		display: none;
	}
	input,select,.form-control{
	height: 30px;
	}
	.panel-heading {
    	padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}@media screen and (min-width: 768px) {
	.btn-width{
		width: 13%;
	}
	.fa-2x{
		font-size: 2em;
	}
	.container{
		width: 76%;
	}
	.table-responsive{
		width: 103%;
	}
	.dataTables_wrapper .dataTables_filter input {
    margin-right: 10px;
}
}
.table-bordered>thead>tr>th{
	vertical-align: middle;
	font-size: 13px;
	font-family: 'Source Sans Pro';
	background-color: #085786;
}
.table-bordered>tbody>tr>td{
	vertical-align: middle;
	font-size: 13px;
	color: #000000;
	font-family: 'Source Sans Pro';
}
.table-bordered>tbody>tr>td.data_td
{
	font-weight: 600;
	font-size: 15px;
	text-align: center;
	vertical-align: middle;
}
</style>
<?php $loginData = $this->session->userdata('loginData');
$filters1=$this->session->userdata('filters1');
$filter_date = $this->session->userdata('hepb_repo_filter1');

/*
$logindata will check level of user (National User/State User/Facility User).
$filters1 is used to mark as selected from State/District/Facility/ Dropdowns.
$filter_date for date filter
*/

if($loginData->user_type==1) { 
	$select = '';
}else{
	$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
	$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
	$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
	$select4 = '';	
}
if (($loginData->user_type==2 && $Repo=="false") || $filters1['id_mstfacility'] !=''&& $Repo=="false"){ 
$excel_button_flag="'Healthcare_Worker_Hepatitis_B_Vaccination_Detailed_Report.xls',1";
}
else{
	$excel_button_flag="' Healthcare_Worker_Hepatitis_B_Vaccination_Summary_Report.xls',0";
}
 ?>
 <div class="container">
 <div class="row" style="margin-top: 20px;">

 
 			<div class="panel panel-default" id="hepB_vacc_Report_Panel">
				<div class="panel-heading" style="background-color: #333333;padding: 3px 0px;">
					<h4 class="text-center" style="background-color: #333333;color: white;font-family: 'Source Sans Pro';letter-spacing: .75px;" id="form_head">Healthcare Workers Hepatitis B Vaccination</h4>
 	</div>

<div class="row main-div">
	<!--Show only for facilty filter/facility login -->

<?php if ($loginData->user_type==2){ ?>
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px; margin-top: 20px;">

		<div class="col-md-6 col-md-offset-3 col-xs-offset-0" id="open_hepb_form_div">
			<a href="JavaScript:Void(0);" class="btn btn-block btn-success text-center" style="font-weight: 600; padding:12px 0px 12px 0px;background-color: #A30A0C; margin-top: -10px;" id="open_hepb_form"><i class="fa fa-plus" aria-hidden="true" id="plus"></i> 
				<span>ADD NEW HEPATITIS B VACCINATION</span></a>
			</div>
		</div>

<?php }?>



<div class="row" style="padding: 20px 35px 15px 40px;" id="filter_div">

	<?php
if($Repo=="true" && $loginData->user_type=="2"){
	$repoflag="Report";
}
else {
	$repoflag=NULL;
}

	$attributes = array(
		'id' => 'hepb_filter_form',
		'name' => 'hepb_filter_form',
		'autocomplete' => 'off',
	);
	echo form_open('Hepb_vaccination/index/'.$repoflag.'', $attributes); ?>

	<div class="row">
		<div class="col-md-2">
			<label for="">State</label>
			<select type="text" name="search_state" id="search_state" class="form-control input_fields" <?php  ?>  <?php echo $select2.$select3.$select4; ?>>
				<option value="">All States</option>
				<?php 
				foreach ($states as $state) {
					?>
					<option value="<?php echo $state->id_mststate; ?>"<?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} elseif($state->id_mststate ==$filters1['id_search_state']) { echo 'selected';}  elseif($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo $state->StateName; ?></option>
					<?php 
				}
				?>
			</select>
		</div>
		<div class="col-md-2">
			<label for="">District</label>
			<select type="text" name="input_district" id="input_district" class="form-control input_fields"  <?php echo $select.$select2.$select4; ?>>
				<option value="">All District</option>
				<?php 
				?>
			</select>
		</div>

		<div class="col-md-2">
			<label for="">Facility</label>
			<select type="text" name="mstfacilitylogin" id="mstfacilitylogin" class="form-control input_fields"  <?php echo $select.$select2; ?>>
				<option value="">All Facility</option>
				<?php 
				?>
			</select>
		</div>
		<div class="col-md-2">
			<label for="">From Date</label>
			<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')){ echo $this->input->post('startdate');} elseif($filter_date['Start_Date']!=NULL){echo timeStampShow($filter_date['Start_Date']);}else{ echo timeStampShow(date('Y-01-01'));}  ?>" required>
		</div>
		<div class="col-md-2">
			<label for="">To Date</label>
			<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')){echo $this->input->post('enddate');}elseif($filter_date['End_Date']!=NULL){echo timeStampShow($filter_date['End_Date']);}else{echo timeStampShow(date('Y-m-d'));} ?>" required>
		</div>
		<div class="col-md-2 btn-width col-sm-12">
			<label for="">&nbsp;</label>
			<button class="btn btn-block btn-success" name="submit" value="search" style="line-height: 1.2; font-weight: 600;height: 33px;">SEARCH</button>
		</div><span style="margin-top: 30px;margin-right: 15px; float: right;"><a href="javascript:void(0)"><i class="fa fa-2x fa-download" id="excel_button" onclick="tableToExcel('data-table-command', 'W3C Example Table',<?php echo $excel_button_flag;?>)" title="Excel Download"></i></a></span>


		<br>
		<?php echo form_close(); ?>
	</div>
</div>

</div>
</div>
</div>
<!--Detailed hepB vaccination Table -->

<!--Show only for facilty filter/facility login -->

<?php if (($loginData->user_type==2 && $Repo=="false") || $filters1['id_mstfacility'] !=''&& $Repo=="false"){ ?>
<!--Detailed Report Excel download At Facility Level -->

<div class="row text-center" style="padding: 0px;" id="hepb_report">

	<table id="data-table-command" class="table table-striped table-bordered table-vmiddle table-hover">
		<thead>
			<tr style="background-color: #085786;color: #fff">
				<th class="text-center">S.No.</th>
				<th class="text-center">Name</th>
				<th class="text-center">Father/husband's Name</th>
				<th class="text-center">Age</th>
				<th class="text-center">Sex</th>
				<th class="text-center">Designation</th>
				<th colspan="3"class="text-center">Hepatitis B Vaccination</th>
				<?php if ($loginData->user_type==2){ ?>
					<th class="text-center">Commands</th>
				<?php } else{ echo '<th class="text-center">Status</th>';} ?>
			</tr>
			<tr style="background-color: #085786;color:white;">
				<th colspan="6"class="text-center"></th>

				<th class="text-center">Date of Dose 1</th>
				<th class="text-center">Date of Dose 2</th>
				<th class="text-center">Date of Dose 3</th>
				<th class="text-center"></th>
			</tr>
		</thead>
		<tbody>
			<?php if(!empty($get_hepb_vacc)){
				foreach ($get_hepb_vacc as $key => $value) { 
					?>
					<tr>
						<td>
							<?php echo $key+1;?>
						</td>
						<td>
							<?php echo $value->worker_name; ?>
						</td>
						<td>
							<?php echo $value->father_or_husband;?>
						</td>
						<td>
							<?php echo $value->age; ?>
						</td>
						<td>
							<?php echo $value->Gender;?>
						</td>
						<td>
							<?php echo $value->designation_name;?>
						</td>
						<td nowrap>
							<?php echo timeStampShow($value->dos1); ?>
						</td>
						<?php if($value->dos2=='0000-00-00'){ ?>
							<td style="background-color: #ffcccb">Pending</td>
						<?php }else { ?> 
							<td nowrap>
								<?php echo timeStampShow($value->dos2); ?>
							</td>
						<?php }if($value->dos2=='0000-00-00'){?>
							<td nowrap style="background-color: #ffcccb">Pending</td>
						<?php }elseif($value->dos3=='0000-00-00' && $value->dos2!='0000-00-00') { ?> 
							<td style="background-color: #ffcccb">Pending</td>
						<?php }else { ?> 
							<td nowrap>
								<?php echo timeStampShow($value->dos3);} ?>
							</td>

							<?php if ($loginData->user_type==2){ if($value->is_locked=='0'){?>
								<td class="text-center">

									<a href="<?php echo site_url()."Hepb_vaccination/Add_Hepb_Vacc/".base64_encode($value->id_health_worker_hepb); ?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
									<?php if($value->dos2!='0000-00-00' && $value->dos3!='0000-00-00') {?>
										<a href="<?php echo site_url()."Hepb_vaccination/set_locked/".base64_encode($value->id_health_worker_hepb); ?>" style="padding : 4px;" title="Lock"><span class="fa fa-unlock" style="font-size : 15px; margin-top: 8px;"></span></a>
									</td>
								<?php }}else{
									echo "<td><span class='fa fa-lock' style='font-size : 15px; margin-top: 8px;''></span></td>";
								}}else{ if($value->is_locked=='0'){?>
									<td class="text-center">

									</span>
									<?php if($value->dos2!='0000-00-00' && $value->dos3!='0000-00-00') {?>
										<span class="fa fa-unlock" style="font-size : 15px; margin-top: 8px;"></span>
									</td>
								<?php }}else{
									echo "<td><span class='fa fa-lock' style='font-size : 15px; margin-top: 8px;''></span></td>";
								} }?>


							</tr>


						<?php }
					} else{
						?>
						<tr >
							<td colspan="10" class="text-center">
								NO RECORDS FOUND.
							</td>

						</tr>
					<?php } ?>



				</tbody>
			</table>
</div>
<!--Detailed HepB vaccination Table Ends Here -->
<!--Starting Add/Update HepB Vaccination Report Form (show Only at Facility Level )-->

<?php } if ($loginData->user_type==2) { ?>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3" id="addform" style="<?php if ($Repo=="false" && $ediflag==0) {
	echo "display: none;";
}?>">
	<div class="row">

		<div class="col-lg-12">

			<div class="panel panel-default" id="add_receipt_panel">

				<div class="panel-heading">
					<h4 class="text-center" style="color: white;" id="form_head"><?php if(isset($ediflag)){if($ediflag==0){ echo "Add Healthcare Worker's Hepatitis B Vaccination"; }else{ echo "Update Healthcare Worker's Hepatitis B Vaccination";}}else{ echo "Add Healthcare Worker's Hepatitis B Vaccination";} ?></h4>
				</div>
				<div class="row">
					<?php 
					$tr_msg= $this->session->flashdata('tr_msg');
					$er_msg= $this->session->flashdata('er_msg');
					if(!empty($tr_msg)){  ?>


						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						<?php } else if(!empty($er_msg)){ ?>
							<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>

							<?php } ?>
						</div>
						<div class="panel panel-body">
							<?php
							$attributes = array(
								'id' => 'Add_hepb_form',
								'name' => 'Add_hepb_form',
								'autocomplete' => 'off',
							);

							if(isset($get_hepb_deatails[0]->id_health_worker_hepb)){
								$hepb_id=$get_hepb_deatails[0]->id_health_worker_hepb;
							}
							else{
								$hepb_id=NULL;
							}
							echo form_open('Hepb_vaccination/Add_Hepb_Vacc/'.$hepb_id, $attributes); ?>
							<div class="row">
								<!-- <div class="form-group col-md-6" id="sn_div">
									<label for="sn">S.No</label>
									<input type="text" class="form-control" name="sn" id="sn" min="1"required value="<?php if($ediflag==0){ if(count($get_hepb_vacc)>0){echo count($get_hepb_vacc)+1;}else{ echo 1;}}elseif($ediflag==1){if(isset($sn)){echo $sn;}
								} ?>">
															</div> -->
							<div class="form-group col-md-6" id="batch_num_div">
								<label for="worker_name">Name<span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="worker_name" name="worker_name" placeholder="Healthcare Worker Name" required value="<?php if(isset($get_hepb_deatails[0]->worker_name)){ echo $get_hepb_deatails[0]->worker_name;}?>">

							</div>

							<div class="form-group col-md-6" id="batch_num_div">
								<label for="father_or_husband">Father/Husband's Name<span class="text-danger">*</span></label>
								<input type="text" class="form-control" id="father_or_husband" name="father_or_husband" placeholder="Father/Husband's Name" required value="<?php if(isset($get_hepb_deatails[0]->father_or_husband)){ echo $get_hepb_deatails[0]->father_or_husband;}?>">

							</div>	
						</div>
							<div class="row">
							<div class="col-lg-6 col-md-6">
								<label for="" id="age_label">Age (in years)<span class="text-danger">*</span></label>
								<input autocomplete="anyrandomthing" type="number" name="age" id="age" class="input_fields form-control" placeholder="Age" onchange="return isNumberKey(event)" required value="<?php if(isset($get_hepb_deatails[0]->age)){ echo $get_hepb_deatails[0]->age;}?>">
							</div>	
						

							<div class="col-lg-6 col-md-6">
								<label for="">Gender <span class="text-danger">*</span></label>
								<select type="text" name="gender" id="gender" class="form-control input_fields" required>
									<option value="">Select</option>
									<option value="1"<?php if(isset($get_hepb_deatails[0]->Gender)){if ($get_hepb_deatails[0]->Gender=='Male') {
										echo ' selected';
									}}?>>Male</option>
									<option value="2"<?php if(isset($get_hepb_deatails[0]->Gender)){if ($get_hepb_deatails[0]->Gender=='Female') {
										echo ' selected';
									}}?>>Female</option>
									<option value="3"<?php if(isset($get_hepb_deatails[0]->Gender)){if ($get_hepb_deatails[0]->Gender=='Transgender') {
										echo ' selected';
									}}?>>Transgender</option>
								</select>
							</div>
						</div>
				<div class="row">
							<div class="form-group col-md-6" id="batch_num_div">
								<label for="designation">Designation<span class="text-danger">*</span></label>
								<select type="text" name="designation" id="designation" class="form-control input_fields" required>
									<option value="">Select</option>
									<?php foreach ($designation_flag as $value) {?>
										<option value="<?php echo $value->LookupCode;?>"<?php if(isset($get_hepb_deatails[0]->designation_code)){if($get_hepb_deatails[0]->designation_code==$value->LookupCode){
											echo " selected";}}
										?> ><?php echo $value->LookupValue;?> </option>
									<?php }?>
								</select>
		<input type="text" class="form-control" id="designation_other" name="designation_other" placeholder="Please Specify Other" style="margin-top: 5px;" required value="<?php if(isset($get_hepb_deatails[0]->designation_name)){ echo $get_hepb_deatails[0]->designation_name;}?>">
							</div>

							<div class="form-group col-md-6">
								<label for="dos1">Date of Dose 1 <span class="text-danger">*</span></label>
								<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="Dose 1 Date" name="dos1" id="dos1" onchange="checkexpirydate()" required onkeydown="return false" onkeyup="return false" value="<?php if(isset($get_hepb_deatails[0]->dos1)){ echo timeStampShow($get_hepb_deatails[0]->dos1);} else{ echo date('d-m-Y');}//date('d-m-Y');}?>">
							</div>
								</div>
								<div class="row">
							<div class="form-group col-md-6">
								<label for="dos2">Date of Dose 2</label>
								<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="Dose 2 Date" name="dos2" id="dos2" onchange="checkexpirydate()" onkeydown="return false" onkeyup="return false" value="<?php if(isset($get_hepb_deatails[0]->dos2)){ echo timeStampShow($get_hepb_deatails[0]->dos2);}else{echo '';}?>">
							</div>
						
							<div class="form-group col-md-6">
								<label for="dos3">Date of Dose 3</label>
								<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="Dose 3 Date" name="dos3" id="dos3" onchange="checkexpirydate()" onkeydown="return false" onkeyup="return false" value="<?php if(isset($get_hepb_deatails[0]->dos3)){ echo timeStampShow($get_hepb_deatails[0]->dos3);}else{echo '';}?>">
							</div>

						</div>
						<br>

						<br>
						<div class="row">
							<div class="col-xs-12 text-center">
								<button type="submit" class="btn btn-warning" id="submit_btn" name="submit" value="save"><?php if(isset($ediflag)){if($ediflag==0){ echo "Save"; }else{ echo "Save";}}else{ echo "Save";} ?></button>
							</div>
						</div>
						<?php echo form_close();?>
						<br>
					</div>
				</div>
			</div>
		</div>

</div>
<!-- Add/Update form Ends Here(Only show for Facility Level) -->
<?php }

//Summary table show Hep_b report Gender wise at Facility/State/National Level

if ((($loginData->user_type==1 || $loginData->user_type==3)&&$filters1['id_mstfacility']=='')|| $Repo=="true"){

	if((($loginData->user_type==1) &&($filters1['id_search_state']==''))){

		$thname="State";

	}
	else if(($loginData->user_type==3 ||($filters1['id_search_state']!=''))|| $Repo=="true"){

		$thname="Facility";
	}

?>
<div class="row text-center table-responsive" style="padding-left: 0px;" id="hepb_report">

<!--Summary Report Excel download At Facility/State/National Level -->

<table id="data-table-command" class="table table-striped table-bordered table-vmiddle table-hover">
	<thead>
		<tr style="background-color: #085786;color: #fff">
			<th class="text-center">S.No.</th>
			<th nowrap class="text-center">
				<?php echo $thname;?>
			</th>
			<th colspan="3" class="text-center">Hepatitis B Vaccination</th>

		</tr>
		<tr style="background-color: #085786;color: #fff">
			<th colspan="2" class="text-center"></th>
			<th colspan="" class="text-center">Number of administrations of Dose 1</th>
			<th colspan="" class="text-center">Number of administrations of Dose 2</th>
			<th colspan="" class="text-center">Number of administrations of Dose 3</th>
		</tr>
		<!--  <tr style="background-color: #4e4e4e;color:white;">
					<th colspan="2"class="text-center"></th>
				
					<th class="text-center">Males</th>
					<th class="text-center">Females</th>
					<th class="text-center">Transgender</th>
					<th class="text-center">Total</th>
					<th class="text-center">Males</th>
					<th class="text-center">Females</th>
					<th class="text-center">Transgender</th>
					<th class="text-center">Total</th>
					<th class="text-center">Males</th>
					<th class="text-center">Females</th>
					<th class="text-center">Transgender</th>
					<th class="text-center">Total</th>
				</tr> -->
	</thead>
<tbody>

	
<?php if(!empty($get_hepb_vacc)){
	foreach ($get_hepb_vacc as $key => $value) { 
		if(($loginData->user_type==1) &&($filters1['id_search_state']=='')){
			$hospital= $value->StateName;
			$clickurl=base64_encode($value->id_mststate);
		}
		else if(($loginData->user_type==3 ||($filters1['id_search_state']!='')&& $filters1['id_mstfacility']=='')|| $Repo=="true"){
			$hospital= $value->hospital;
			$clickurl=base64_encode($value->id_mstfacility);
		}

	?>
	<tr data-href='<?php echo site_url()."Hepb_vaccination/get_facility_detailed_table/".$clickurl; ?>'>
		<td>
			<?php echo $key+1;?>
		</td>
		<td nowrap style="text-align: left;">
			<?php echo $hospital; ?>
		</td>
		<!-- <td>
			<?php echo $value->male_dos1 ? $value->male_dos1 : 0;?>
		</td>
		<td>
			<?php echo $value->female_dos1 ? $value->female_dos1 : 0; ?>
		</td>
		<td>
			<?php echo $value->transgender_dos1 ? $value->transgender_dos1 : 0;?>
		</td> -->

		<td class="text-center data_td">
			<?php echo (($value->male_dos1 ? $value->male_dos1 : 0)+($value->female_dos1 ? $value->female_dos1 : 0)+($value->transgender_dos1 ? $value->transgender_dos1 : 0)) > 0 ? (($value->male_dos1 ? $value->male_dos1 : 0)+($value->female_dos1 ? $value->female_dos1 : 0)+($value->transgender_dos1 ? $value->transgender_dos1 : 0)) : "-" ?>
				
		</td>
		<!-- <td>
			<?php echo $value->male_dos2 ? $value->male_dos2 : 0;?>
		</td>
		<td>
			<?php echo $value->female_dos2 ? $value->female_dos2 : 0; ?>
		</td>
		<td>
			<?php echo $value->transgender_dos2 ? $value->transgender_dos2 : 0;?>
		</td> -->
		<td class="text-center data_td">
			<?php echo (($value->male_dos2 ? $value->male_dos2 : 0)+($value->female_dos2 ? $value->female_dos2 : 0)+($value->transgender_dos2 ? $value->transgender_dos2 : 0)) > 0 ? (($value->male_dos2 ? $value->male_dos2 : 0)+($value->female_dos2 ? $value->female_dos2 : 0)+($value->transgender_dos2 ? $value->transgender_dos2 : 0)) : "-" ?>
			
		</td>
		<!-- <td>
			<?php echo $value->male_dos3 ? $value->male_dos3 : 0;?>
		</td>
		<td>
			<?php echo $value->female_dos3 ? $value->female_dos3 : 0; ?>
		</td>
		<td>
			<?php echo $value->transgender_dos3 ? $value->transgender_dos3 : 0;?>
		</td> -->
		<td class="text-center data_td"><?php echo (($value->male_dos3 ? $value->male_dos3 : 0)+($value->female_dos3 ? $value->female_dos3 : 0)+($value->transgender_dos3 ? $value->transgender_dos3 : 0)) > 0 ? (($value->male_dos3 ? $value->male_dos3 : 0)+($value->female_dos3 ? $value->female_dos3 : 0)+($value->transgender_dos3 ? $value->transgender_dos3 : 0)) : "-" ?>
			
		</td>
	</tr>


<?php }
} else{
?>
<tr >
	<td colspan="5" class="text-center">
		NO RECORDS FOUND.
	</td>

</tr>
<?php } ?>



</tbody>
</table>


</div>

<?php } ?>
</div>
<div class="overlay">
<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
</div>

<script type="text/javascript">


var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name,filename,flag) {
    if (!table.nodeType) table = document.getElementById(table)
    	 var newtable=document.createElement("table");
            var newtable=document.createElement("table");
            var tbody =document.createElement("tbody");
             var row = document.createElement("tr");
             row.setAttribute("style", "background-color: black;color: white; font-family:Calibri;");
  				var cell1 = document.createElement("td");
  				cell1.colSpan=5;
  				cell1.innerHTML ="Date as on : - <?php echo date('jS \ F Y'); ?>";
  				row.appendChild(cell1);
 			 	tbody.appendChild(row);
 			 	newtable.appendChild(tbody);
 			 	 var clonedTable1=newtable.cloneNode(true);
	var clonedTable = table.cloneNode(true);
	if(flag==1){
	var allRows = clonedTable.rows;
 	for (var i=0; i< allRows.length; i++) {
   allRows[i].deleteCell(-1); 
 	}
 }
 clonedTable1.appendChild(clonedTable);
    var ctx = {worksheet: name || 'Worksheet', table: clonedTable1.innerHTML}
    var link = document.createElement('a');
    link.download = filename;
    link.href = uri + base64(format(template, ctx));
    link.click();
  }
})()
</script>
<script type="text/javascript">

	function isNumberKey(evt)
	{
		var dataval =parseInt($('#age').val());
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;


		if(evt.srcElement.id == 'age' &&  dataval > 70 || dataval<=18)
		{
			$("#modal_header").html('Age must be less than 70 And greater or equal to 18');
			$("#modal_text").html('Please fill in appropriate Age');
			$('#age').val('');
			$("#multipurpose_modal").modal("show");
			return false;
		}

		return true;
	}
	$(document).ready(function(){
		<?php if($loginData->user_type==2){?>
			$("#designation").trigger('change');
			$( "#dos2" ).ready(function(){
				$( "#dos2" ).trigger("change");
			});
			$("#dos2").change(function(e){
		if($("#dos2").val()=='' || isNaN($("#dos2").datepicker('getDate'))){
	
				$("#dos3").attr("disabled",true);
			}
			else{
				$("#dos3").attr("disabled",false);
			}

			});
			$("#designation").on('ready change',function(e){
				var val=$("#designation").val();
				if(val=='99'){
					$("#designation_other").show();
					$("#designation_other").attr('required','required');
				}
				else{
					$("#designation_other").val('');
					$("#designation_other").hide();
					$("#designation_other").removeAttr('required');
				}
			});
			$(".fa-unlock").click(function(e){
				var ans = confirm("Are you sure you want to Lock?");
				if(!ans)
				{
					e.preventDefault();
				}
			}); <?php if ($Repo=="false") { ?>
				$('#open_hepb_form').click(function(){
					
					$('#hepb_report').slideToggle("slow");
					$('#filter_div').slideToggle("slow");
					
					$(this).find('span').text(function(i, text){
						return text === "VIEW HEPATITIS B VACCINATION REPORT" ? "ADD HEPATITIS B VACCINATION" : "VIEW HEPATITIS B VACCINATION REPORT";
					})
					$(this).find('i').toggleClass('fa-plus hideplus');
					$('#open_hepb_form_div').toggleClass('col-md-offset-4 col-md-offset-3');
					$('#open_hepb_form_div').toggleClass('col-md-5 col-md-4');
					$('#addform').toggleClass("show hide");
					$("#designation").trigger('change');
					$('#excel_button').slideToggle("slow");
				});
<?php } }?>
			<?php if ($ediflag==1) { ?>
				$('#excel_button').hide();
				$('#open_hepb_form_div').toggleClass('col-md-offset-4 col-md-offset-3');
				$('#open_hepb_form_div').toggleClass('col-md-5 col-md-4');
				$('#addform').show();
				$("#designation").trigger('change');

				$("#open_hepb_form").attr("href", "<?php echo site_url()."Hepb_vaccination/Add_Hepb_Vacc/";?>");
				$("#open_hepb_form").text("VIEW HEPATITIS B VACCINATION REPORT");
				$('#hepb_report').hide();
				$('#filter_div').hide();
			<?php }elseif ($ediflag==0) {?>
				$('#addform').css('display','none');
				 <?php if ($Repo=="true") {?>
					$("#open_hepb_form").text("ADD/VIEW HEPATITIS B VACCINATION REPORT");
				$("#open_hepb_form").attr("href", "<?php echo site_url()."Hepb_vaccination/index/";?>");
				
		<?php }else{ ?>
				$("#open_hepb_form").attr("href", "javascript:Void(0);");
				
				<?php }?>
				$('#hepb_report').show();
				$('#filter_div').show();
				$('#addform').css('display','none');
				
				<?php if(!empty($get_hepb_vacc)){ ?>
					$('#data-table-command').DataTable({
						"pagingType": "simple_numbers",
						<?php if($loginData->user_type==2 || $filters1['id_mstfacility']!='' ||$filters1['id_mstfacility']!=NULL){
						echo '"searching": true,
						language: {
        			search: "_INPUT_",
       	 			searchPlaceholder: "Search By Name"
    				},'; 
					} else{ echo '"searching": false,';}?>


						"lengthChange": false,
						"orderable": false,
						"pageLength": 10,
						
					}); <?php } ?>
				<?php }?>


				$('input:text').bind('cut copy paste', function(e) {
					e.preventDefault();
				});
				
				<?php  if (((($loginData->user_type==1 || $loginData->user_type==3)&&($filters1['id_search_state']!='' && $filters1['id_mstfacility']==''))||($loginData->user_type==3 && $filters1['id_mstfacility']==''))|| $loginData->user_type==2){ if($Repo=="true") {?>
					$('#data-table-command tr').click(function(){
						window.location = $(this).data('href');
					});  
					$('#data-table-command tr').css( 'cursor', 'pointer' );

					$('#data-table-command tr').css( 'cursor', 'hand' ); 
				<?php }}?>
				/*$("#sn").prop('disabled', true);*/

			});
 
	function checkexpirydate()
	{
		var Entry_Date = $('#dos1').datepicker('getDate');
		var Expiry_Date1 = $('#dos2').datepicker('getDate');
		var Expiry_Date2 = $('#dos3').datepicker('getDate');
		if ( Entry_Date >= Expiry_Date1 && Expiry_Date1!=null) { 
			$("#modal_header").text("Dose 2 Date cannot be before/Equal Dose 1 Date");
			$("#modal_text").text("Please check Dates");
			$("#dos2" ).val('');
			$("#multipurpose_modal").modal("show");
			$( "#dos2" ).trigger( "change" );
			return false;
		}
		else if (Expiry_Date1==null && (Expiry_Date2!=null)) { 
			$("#modal_header").text("Please update previous dosage date");
			$("#modal_text").text("Please check Dates");
			$("#dos3").val('');
			$("#multipurpose_modal").modal("show");
			return false;
		}
		else if ( (Expiry_Date2 <= Expiry_Date1 && Expiry_Date2!=null)){ 
			$("#modal_header").text("Dose 3 Date cannot be before/Equal to Dose 2 Date");
			$("#modal_text").text("Please check Dates");
			$("#dos3" ).val('');
			$("#multipurpose_modal").modal("show");
			return false;
		}

		else{
			return true;
		}
	}

	$('#worker_name,#father_or_husband,#designation_other').keypress(function (e) {
		var regex = new RegExp("^[a-zA-Z ]*$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test($.trim(str))) {
			var error_css = {"border":"1px solid skyblue"};
			$(this).css(error_css);
			return true;

		}
		else{
			e.preventDefault();
			return false;
		}

	});

	$('#submit_btn').click(function(e){
		var error_css = {"border":"1px solid red"};


		if($('#worker_name').val().trim() == ""){

			$("#worker_name").css(error_css);
			$("#worker_name").focus();
			e.preventDefault();
			return false;
		}

		if($('#father_or_husband').val().trim()==""){

			$("#father_or_husband").css(error_css);
			$("#father_or_husband").focus();
			e.preventDefault();
			return false;
		}
		if($('#designation').val()=='99'){

if($('#designation_other').val().trim()==""){

			$("#designation_other").css(error_css);
			$("#designation_other").focus();
			e.preventDefault();
			return false;
		}
	}
		if($('#age').val().trim==""){

			$("#age").css(error_css);
			$("#age").focus();
			e.preventDefault();
			return false;
		}

		if($('#gender').val()==""){

			$("#gender").css(error_css);
			$("#gender").focus();
			e.preventDefault();
			return false;
		}

		if($('#dos1').val()==""){

			$("#dos1").css(error_css);
			$("#dos1").focus();
			e.preventDefault();
			return false;
		}
		else{
			return true;
		}

	});

	$(document).ready(function(){

		$("#search_state").trigger('change');


});
	$('#search_state').change(function(){

		$.ajax({
			url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
			data : {
				<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
			},
			method : 'GET',
			success : function(data)
			{
//alert(data);
$('#input_district').html(data);
$("#input_district").trigger('change');


},
error : function(error)
{
//alert('Error Fetching Districts');
}
})
	});

	$('#input_district').change(function(){

		$.ajax({
			url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
data : {
				<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
			},
			method : 'GET',
			success : function(data)
			{
//alert(data);
if(data!="<option value=''>Select Facilities</option>"){
$('#mstfacilitylogin').html(data);

}
},
error : function(error)
{
//alert('Error Fetching Blocks');
}
})
	});

</script>


