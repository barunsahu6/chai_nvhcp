<style>
#table_inventory_list th 
{
text-align: center; 
vertical-align: middle;
}
td{
text-align: center; 
vertical-align: middle;
}
.item{
width: 140px !important;
}
.batch_num{
width: 100px !important;
}
#mtc,#tc,#hepc:hover {
    cursor: pointer;
}
a{
cursor: pointer;
}
.set_height
{
max-height: 425px;
overflow-y: scroll;

}
.panel-heading {
padding: 1px 15px;

}
label
{
padding-top: 5px;
}
.overlay{
z-index : -1;
width: 100%;
height: 100%;
background-color: rgba(0,0,0,0.5);
top : 0; 
left: 0; 
position: fixed; 
display : none;
}
.loading_gif{
width : 100px;
height : 100px;
top : 45%; 
left: 45%; 
position: absolute; 
}
.tbl_border{
border-style : hidden!important;
border: none;
padding: 0px !important;

}
.form-control{
border-radius: 0px;
width: 100%;
text-align:center;
}

.table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
    vertical-align: middle;
    width: 13.13%;
    text-align: center;
  font-family:  "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 12px;
    font-weight: 600;
}
@media (min-width: 768px) {
	.btn-width{
	width: 11%;
	margin-top: 20px;
}
.pd-15{
	padding-left: 15px;
}
.pb-20{
	padding-bottom: 20px;
}
.container{
	width: 76%;
}
}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData');
$filter= $this->session->userdata('hfm_search_filter');?>

<?php
$attributes = array(
'id' => 'hfm_repo_filter_form',
'name' => 'hfm_repo_filter_form',
'autocomplete' => 'false',
);
echo form_open('hfm_Dashboard/hfm_report', $attributes); ?>
<div class="container">
	<div class="row main-div" style="min-height:400px;">

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-default" id="Record_receipt_panel">
				<div class="panel-heading" style="background-color: #423e3e;">
					<h4 class="text-center" style="background-color: #423e3e;color: white;" id="Record_form_head">HFM REPORT</h4>
				</div>
				<div class="row" style="padding-right: 30px;padding-left: 30px;text-align: center;">
					<div class="col-md-3 form-group col-md-offset-2">
						<label for="">From Date</label>
						<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')){ echo $this->input->post('startdate');}else{ echo timeStampShow(date('Y-04-01'));}  ?>" onchange="checkdate();" onkeydown="return false" onkeyup="return false" required style="text-align: left;height: 30px;font-size: 14px;">
					</div>
					<div class="col-md-3">
						<label for="">To Date</label>
						<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')){echo $this->input->post('enddate');}else{echo timeStampShow(date('Y-m-d'));} ?>" onchange="checkdate();" onkeydown="return false" onkeyup="return false" required style="text-align: left;height: 30px;font-size: 14px;">
					</div>
					<div class="col-md-2">
						<label for="">&nbsp;</label>
						<button class="btn btn-block btn-warning" style="line-height: 1.2; font-weight: 500;">SEARCH</button>
					</div><span style="margin-top: 30px;margin-right: 7px; float: right;"><a href="javascript:void(0)"><i class="fa fa-2x fa-download" id="excel_button" onclick="tableToExcel('W3C Example Table','table_hfm_mtc_list','table_hfm_tc_list','table_hfm_hepc_list','hfm_report.xls')" title="Excel Download"></i></a></span>

				</div>
			</div>

			<div class="table-responsive">

				<table class="table table-striped table-bordered table-hover table-vmiddle" id="table_hfm_mtc_list">
					<thead>
						<tr>
							<td colspan="4" style="background-color:#088DA5;color: white;font-family:Georgia, serif; font-size: 18px; font-weight: 500"><?php echo '01/04/2019 - '.date('d/m/Y');?></td>
							<td colspan="3" style="background-color: #005565;color: white;font-family:Georgia, serif; font-size: 18px; font-weight: 500"><?php echo date('d/m/Y',strtotime($filter['startdate'])).' - '.date('d/m/Y',strtotime($filter['enddate']));?></td>

						</tr>
					</thead>			
					<thead>
						<tr style="background-color: #626563;color: white;font-size: 18px; font-weight: 500">
							<th colspan="7" style="text-align: left;font-size: 18px;" id="mtc">Model Treatment Centers</th>

						</tr>
						<tr style="background-color: #088DA5;color: white; font-size: 12px;font-weight: 500;letter-spacing: .5px;" class="mtc_row">
							<th colspan="">&nbsp;</th>
							<th>Target</th>
							<th style="text-align: center;">Number of MTCs established</th>
							<th style="text-align: center;">Number of MTCs<br> operational (i.e.<br> reporting patient entries)</th>
							<th style="background-color: #005565;text-align: center;">Target</th>
							<th style="background-color: #005565;text-align: center;">Number of MTCs established</th>
							<!-- <th>Number of Districts</th> -->
							<th style="background-color: #005565;text-align: center;">Number of MTCs<br> operational (i.e.<br> reporting patient entries)</th>
						</tr>
					</thead>
					<tbody>

						<?php if(!empty($static_hfm_data_mtc)){?>
							<tr class="mtc_row"><td colspan="" style="text-align: left;font-size: 13px;"><b>India</b></td>
								<?php 
								$target_mtc_sum_static=0;
								$established_mtc_sum_static=0;
								$facilty_with_patient_entry_sum_static=0;
								$target_mtc_sum_filtered=0;
								$established_mtc_sum_filtered=0;
								$facilty_with_patient_entry_sum_filtered=0;
								foreach($static_hfm_data_mtc as $key => $value){
									$target_mtc_sum_static+=$value->target_mtc;
									$established_mtc_sum_static+=$value->established_mtc;
									$facilty_with_patient_entry_sum_static+=$value->facilty_with_patient_entry;
									$target_mtc_sum_filtered+= $filtered_hfm_data_mtc[$key]->target_mtc;;
									$established_mtc_sum_filtered+=$filtered_hfm_data_mtc[$key]->established;
									$facilty_with_patient_entry_sum_filtered+=$filtered_hfm_data_mtc[$key]->facilty_with_patient_entry;
								}?>
								<td style="background-color: #ADD8E6"><b><?php echo $target_mtc_sum_static;?></b></td>
								<td style="background-color: #ADD8E6"><b><?php echo $established_mtc_sum_static;?></b></td>
								<td style="background-color: #ADD8E6"><b><?php echo $facilty_with_patient_entry_sum_static;?></b></td>
								<td style="background-color:#f1f1f1"><b><?php echo $target_mtc_sum_filtered;?></b></td>
								<td style="background-color: #f1f1f1"><b><?php echo $established_mtc_sum_filtered;?></b></td>
								<td style="background-color: #f1f1f1"><b><?php echo $facilty_with_patient_entry_sum_filtered;?></b></td>

							</tr>
							<?php foreach ($static_hfm_data_mtc as $key => $value) {
								?>
								<tr class="mtc_row">
									<td nowrap colspan="" style="text-align: left;"><?php echo ucwords(strtolower($value->StateName)); ?></td>
									<td style="background-color: #ADD8E6"><?php echo $value->target_mtc; ?></td>
									<td style="background-color: #ADD8E6"><?php echo $value->established_mtc; ?></td>
									<td style="background-color: #ADD8E6"><?php echo $value->facilty_with_patient_entry; ?></td>

									<td style="background-color:#f1f1f1"><?php echo $filtered_hfm_data_mtc[$key]->target_mtc; ?></td>
									<td style="background-color:#f1f1f1"><?php echo $filtered_hfm_data_mtc[$key]->established; ?></td>
									<td style="background-color:#f1f1f1"><?php echo $filtered_hfm_data_mtc[$key]->facilty_with_patient_entry; ?></td>

								</tr>
							<?php } }
							?>
						</tbody>
					</table>
				</div>
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover table-vmiddle" id="table_hfm_tc_list">
						<thead>
							<tr style="background-color: #626563;color: white;font-family:source sans pro; font-size: 18px; font-weight: 500">
								<th colspan="9" style="text-align: left;font-size: 18px;" id="tc">Treatment Centers</th>

							</tr>
							<tr style="background-color: #088DA5;color: white; font-size: 12px; font-weight: 500;letter-spacing: .5px;" class="tc_row">
								<th>&nbsp;</th>
								<th style="text-align: center;">Number of Districs</th>
								<th style="text-align: center;">Number of districts with TC</th>
								<th style="text-align: center;">Total Number of TCs established</th>
								<th style="text-align: center;">Number of TCs operational (i.e. reporting patient entries)</th>
								<th style="background-color: #005565;text-align: center;">Number of Districs</th>
								<th style="background-color: #005565;text-align: center;">Number of districts with TC</th>
								<th style="background-color: #005565;text-align: center;">Total Number of TCs established</th>
								<th style="background-color: #005565;text-align: center;">Number of TCs operational (i.e. reporting patient entries)</th>
							</tr>
						</thead>
						<tbody>
							<tr class="tc_row"><td colspan="" style="text-align: left;font-size: 13px;"><b>India</b></td>

								<?php if(!empty($static_hfm_data_tc)){ ?>
									<?php 
									$district_sum_static=0;
									$district_with_tc_sum_static=0;
									$established_tc_sum_static=0;
									$facilty_with_patient_entry_sum_static=0;
									$district_sum_filtered=0;
									$district_with_tc_sum_filtered=0;
									$established_tc_sum_filtered=0;
									$facilty_with_patient_entry_sum_filtered=0;
									foreach($static_hfm_data_tc as $key => $value){
										$district_sum_static+=$value->district;
										$district_with_tc_sum_static+=$value->district_with_tc;
										$established_tc_sum_static+=$value->established_tc;
										$facilty_with_patient_entry_sum_static+=$value->facilty_with_patient_entry;

										$district_sum_filtered+= $filtered_hfm_data_tc[$key]->district;
										$district_with_tc_sum_filtered+=$filtered_hfm_data_tc[$key]->district_with_tc;
										$established_tc_sum_filtered+=$filtered_hfm_data_tc[$key]->established;
										$facilty_with_patient_entry_sum_filtered+=$filtered_hfm_data_tc[$key]->facilty_with_patient_entry;

									}?>
									<td style="background-color: #ADD8E6"><b><?php echo $district_sum_static;?></b></td>
									<td style="background-color: #ADD8E6"><b><?php echo $district_with_tc_sum_static;?></b></td>
									<td style="background-color: #ADD8E6"><b><?php echo $established_tc_sum_static;?></b></td>
									<td style="background-color: #ADD8E6"><b><?php echo $facilty_with_patient_entry_sum_static;?></b></td>
									<td style="background-color:#f1f1f1"><b><?php echo $district_sum_filtered;?></b></td>
									<td style="background-color:#f1f1f1"><b><?php echo $district_with_tc_sum_filtered;?></b></td>
									<td style="background-color:#f1f1f1"><b><?php echo $established_tc_sum_filtered;?></b></td>
									<td style="background-color:#f1f1f1"><b><?php echo $facilty_with_patient_entry_sum_filtered;?></b></td>

								</tr>
								<?php foreach ($static_hfm_data_tc as $key => $value) {
									?>
									<tr  class="tc_row">
										<td nowrap style="text-align: left;"><?php echo ucwords(strtolower($value->StateName)); ?></td>
										<td style="background-color: #ADD8E6"><?php echo $value->district; ?></td>
										<td style="background-color: #ADD8E6"><?php echo $value->district_with_tc; ?></td>
										<td style="background-color: #ADD8E6"><?php echo $value->established_tc; ?></td>
										<td style="background-color: #ADD8E6"><?php echo $value->facilty_with_patient_entry; ?></td>

										<td style="background-color:#f1f1f1"><?php echo $filtered_hfm_data_tc[$key]->district; ?></td>
										<td style="background-color:#f1f1f1"><?php echo $filtered_hfm_data_tc[$key]->district_with_tc; ?></td>
										<td style="background-color:#f1f1f1"><?php echo $filtered_hfm_data_tc[$key]->established; ?></td>
										<td style="background-color:#f1f1f1"><?php echo $filtered_hfm_data_tc[$key]->facilty_with_patient_entry; ?></td>

									</tr>
								<?php }



							}?>


						</tbody>
					</table>
</div>
<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover table-vmiddle" id="table_hfm_hepc_list">
						<thead>
							<tr style="background-color: #626563;color: white; font-size: 18px; font-weight: 500">
								<th colspan="5" style="text-align: left;font-size: 18px;" id="hepc">Hepatitis C Treatment</th>

							</tr>
							<tr style="background-color: #088DA5;color: white; font-size: 12px; font-weight: 500;letter-spacing: .5px;" class="hepc_row">
								<th>&nbsp;</th>
								<th style="text-align: center;">Number of patients put on treatment</th>
								<th style="text-align: center;">Number of patients successfully treated</th>
								<th style="background-color: #005565;text-align: center;">Number of patients put on treatment</th>
								<th style="background-color: #005565;text-align: center;">Number of patients successfully treated</th>

							</tr>
						</thead>
						<tbody>
							<tr class="hepc_row"><td colspan="" style="text-align: left;font-size: 13px;"><b>India</b></td>

								<?php if(!empty($static_hepc_data)){ ?>
									<?php 
//pr($filtered_hepc_data);exit();
									$initiatied_on_treatment_sum_static=0;
									$treatment_successful_sum_static=0;

									$initiatied_on_treatment_sum_filtered=0;
									$treatment_successful_sum_filtered=0;

									foreach($static_hepc_data as $key => $value){
										$initiatied_on_treatment_sum_static+=$value['initiatied_on_treatment'];
										$treatment_successful_sum_static+=$value['treatment_successful'];

										$initiatied_on_treatment_sum_filtered+= $filtered_hepc_data[$key]['initiatied_on_treatment'];
										$treatment_successful_sum_filtered+=$filtered_hepc_data[$key]['treatment_successful'];


									}?>
									<td style="background-color: #ADD8E6"><b><?php echo $initiatied_on_treatment_sum_static;?></b></td>
									<td style="background-color: #ADD8E6"><b><?php echo $treatment_successful_sum_static;?></b></td>


									<td style="background-color:#f1f1f1"><b><?php echo $initiatied_on_treatment_sum_filtered;?></b></td>
									<td style="background-color:#f1f1f1"><b><?php echo $treatment_successful_sum_filtered;?></b></td>

								</tr>
								<?php foreach ($static_hepc_data as $key => $value) {
									?>
									<tr class="hepc_row">
										<td nowrap style="text-align: left;"><?php echo ucwords(strtolower($value['StateName'])); ?></td>
										<td style="background-color: #ADD8E6"><?php echo $value['initiatied_on_treatment']; ?></td>
										<td style="background-color: #ADD8E6"><?php echo $value['treatment_successful']; ?></td>


										<td style="background-color:#f1f1f1"><?php echo $filtered_hepc_data[$key]['initiatied_on_treatment']; ?></td>
										<td style="background-color:#f1f1f1"><?php echo $filtered_hepc_data[$key]['treatment_successful']; ?></td>


									</tr>
								<?php }



							}?>


						</tbody>
					</table>
				</div>	
			</div>
			<br>
		</div>
<div class="overlay">
<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
</div>
</div>
<script type="text/javascript">


var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(name,table1, table2, table3, filename) {
    if (!table1.nodeType) table1 = document.getElementById(table1)
    	 if (!table2.nodeType) table2 = document.getElementById(table2)
    	 	 if (!table3.nodeType) table3 = document.getElementById(table3)
    	 	 	var clonedTable =table1.cloneNode(true);
    	 	 var clonedTable2=table2.cloneNode(true);
    	 	  var clonedTable3=table3.cloneNode(true);
	clonedTable.appendChild(clonedTable2);
	clonedTable.appendChild(clonedTable3);
    var ctx = {worksheet: name || 'Worksheet', table: clonedTable.innerHTML}
    var link = document.createElement('a');
    link.download = filename;
    link.href = uri + base64(format(template, ctx));
    link.click();
  }
})()
</script>
<script type="text/javascript">
	$( document ).ready(function() {
   $(".tc_row").hide();
   $(".hepc_row").hide();
	$('#mtc').click(function(e){
		$(".mtc_row").toggle();
	});
	$('#tc').click(function(e){
		$(".tc_row").toggle();
	});
	$('#hepc').click(function(e){
		$(".hepc_row").toggle();
	});
});
</script>


