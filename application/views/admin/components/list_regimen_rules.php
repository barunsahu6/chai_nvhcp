<br>
<?php 
if($this->session->flashdata('msg'))
{
	echo '<div class="alert alert-success alert-dismissible show" role="alert">
	<p>'.$this->session->flashdata('msg').'
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</button></p>
	</div>';
}

if($this->session->flashdata('er_msg'))
{
	echo '<div class="alert alert-danger alert-dismissible show" role="alert">
	<p>'.$this->session->flashdata('er_msg').'
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</button></p>
	</div>';
}

?>

<section>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h3 class="text-center">Regimen Rules <a class="btn btn-warning pull-right" href="<?php echo site_url('regimen_rules/add');?>">Add Rule</a></h3>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<table class="table table-hover table-highlighted table-bordered">
				<thead>
					<th>Sno</th>
					<th>Cirrhosis</th>
					<th>Genotype</th>
					<th>Regimen</th>
					<th>Duration</th>
					<th>Commands</th>
				</thead>
				<tbody>
					
					<?php 
						$sno = 1;
						foreach ($rules as $rule) {
							?>
								<tr>
									<td><?php echo $sno; ?></td>
									<td><?php echo $rule->cirrhosis; ?></td>
									<td><?php echo $rule->genotype; ?></td>
									<td><?php echo $rule->regimen; ?></td>
									<td class="text-center"><?php echo $rule->duration; ?></td>
									<td><a href="<?php echo site_url('regimen_rules/edit/'.$rule->id_mst_regimen_rules);?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 14px; margin-top: 8px;"></span></a>
									<a href="<?php echo site_url('regimen_rules/delete/'.$rule->id_mst_regimen_rules);?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 14px; margin-top: 8px;"></span></a></td>
								</tr>
								<?php

							$sno++;
						}
					 ?>
				</tbody>
			</table>
		</div>
	</div>
</section>