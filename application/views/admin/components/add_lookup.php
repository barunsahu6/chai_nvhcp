	<style>
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.form-horizontal .control-label{
		text-align: left;
	}
</style>
<?php   $useriddv = $this->uri->segment(3);   $loginData = $this->session->userdata('loginData'); ?>
<div class="row main-div" style="min-height:400px;">
	<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">

		<div class="panel panel-default">

			<div class="panel-heading">
				<h4 class="text-left" style="color : white;">List <a href="#" style="color: white; cursor: pointer;"><i class="fa fa-plus-circle pull-right"></i></a></h4>
			</div>
			<div class="panel-body set_height">
				<table id="data-table-command" class="table table-striped table-bordered table-vmiddle table-hover">
					<thead>
						<tr>
							<th class="text-center">Value</th>
							<th class="text-center">Commands</th>
						</tr>

					</thead>
					<tbody>

						<?php if (!empty($lookup_list)) { ?>
							<?php foreach($lookup_list as $row){ ?>
								<tr>

									<td class="text-center" style="padding-top: 16px;"><?php echo $row->LookupValue; ?></td>

									<td class="text-center">
										<!-- <?php// if($loginData->user_type == '2' ){ ?> -->
											<button type="button" class="glyphicon glyphicon-pencil chnagelookup" data-toggle="modal" data-target="#myModal1" id="<?php echo $row->id_mstlookup; ?>"></button>

										<?php //} ?>
									</td>

								</tr>

							<?php } ?>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
		<div class="panel panel-default">

			<div class="panel-heading">
				<h4 class="text-left" style="color: white;" id="form_head">Add Lookup</h4>
			</div>
			<div class="row">
				<?php 
				$tr_msg= $this->session->flashdata('tr_msg');
				$er_msg= $this->session->flashdata('er_msg');
				if(!empty($tr_msg)){ ?>


					<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
						<div class="hpanel">
							<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<?php echo $this->session->flashdata('tr_msg');?>. </div>
							</div>
						</div>
					<?php } else if(!empty($er_msg)){ ?>
						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('er_msg');?>. </div>
								</div>
							</div>

						<?php } ?>
					</div>
					<div class="panel panel-body ">

						<?php
						$attributes = array(
							'id' => 'docutor',
							'name' => 'users',
							'autocomplete' => 'off',
						);
						echo form_open('', $attributes); ?>

						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
						<div class="row">

							<div class="col-xs-2 text-left">
								<label for="full_name">Lookup<span class="text-danger">*</span></label>
							</div>
							<div class="col-xs-4">
								<select name="LookupValue" class="form-control LookupValue" id="LookupValue" required="">
									<option value="91"<?php echo (trim($this->input->post('LookupValue')) == 91?"selected":"");?>>Source of patient</option>

									<option value="84"<?php echo (trim($this->input->post('LookupValue')) == 84?"selected":"");?>>Gender Type </option>

								</select>
							</div>

							<div class="col-xs-2">
								<button type="submit" name="Operation" value="1" class="btn btn-warning" id="search_id_val">Search</button>
							</div>

						</div>
						<br>


						<div class="AddLookup" id="AddLookup" >
							<div class="row">
								<div class="col-xs-2 text-left">
									<label for="lookup">Lookup Value</label>
									
								</div>
								<div class="col-xs-4">
									<input type="text" name="new_lookup" class="form-control" value="" id="new_lookupID">
								</div>
								<div class="col-xs-2">
									<button type="submit" name="Operation"  value="2" class="btn btn-success" id="OperationID" style="display: none;">Add New</button>
								</div>
							</div>
						</div>
						<?php echo form_close(); ?>

					</div>
				</div>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="myModal1" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <?php echo form_open('', array('id' => 'id_mstlookup')); ?>
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Change Lookup Value</h4>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		          <input type="hidden" name="id_mstlookup" id="id_mstlookupval" value="">
		          

		          <div class="col-md-6">
		            <span style="font-weight: bold;">Lookup Value</span><br>
		            <input type="text" name="old_lookup_value" id="old_lookup_value" class="form-control">
		          </div>
		        </div>
		      </div>
		      <div class="modal-footer">
		        <button id="submit" class="btn btn-success success" data-dismiss="modal">Change</button>
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		      <?php echo form_close(); ?>
		    </div>

		  </div>
		</div>

		<script type="text/javascript">

			$(document).ready(function(){
			$("#search_id_val").trigger("change");
			});

			$(document).ready(function() {
			    $("#LookupValue").change(function() {
			        // Do something here...
			       $("#search_id_val").trigger("click");
			    });

			    $('#new_lookupID').change(function(){
			    	var new_lookupID = $('#new_lookupID').val().trim();
			    	if(new_lookupID!=''){
			    	 $("#OperationID").css("display", "block");
			    	}else{
			    		$("#OperationID").css("display", "none");
			    	}

			    });

			});



		  $('.chnagelookup').click(function(e){
		    e.preventDefault();     
		    $("#id").val($(this).attr('id')); 
		    
		    $('#id_mstlookupval').val($(this).attr('id'));

		     var dataString = {
	"id_mstlookup": $(this).attr('id')
	};
	$.ajax({				    	
		url: "<?php echo site_url('lookup/get_lookupdata/'); ?>",
		type: 'GET',
		data: dataString,
		dataType: 'json',
		async: false,
		cache: false,
		success: function (data) { 
			
			if(data.status == 'true'){
				//$(".editVal").hide();

				var parsedData = JSON.parse(data.fields);
				$("#old_lookup_value").val(parsedData.LookupValue);
						
			} 	
	
		}        
	 }); 



		  });

		  $('#submit').click(function(e){
		              // alert('fsds');
		              e.preventDefault();
		              $("#form-error").html('');
		              var old_lookup_value = $('#old_lookup_value').val(); 
		              var id_mstlookup = $('#id_mstlookupval').val(); 
		              // alert(id_mstlookup);
		              var dataString = {
		                "old_lookup_value" : old_lookup_value,
		                "id_mstlookup" : id_mstlookup
		              };

		              if (old_lookup_value == "") 
		              {
		                alert('Please Add Lookup value for change');
		                exit();
		              }  
		              else {  

		              $.ajax({                        
		                url: "<?php echo site_url('lookup/lookup_update/'); ?>",
		                type: 'get',
		                data: dataString,
		                dataType: 'json',
		                async: false,
		                cache: false,
		                success: function (data) {
		                  if(data['status'] == 'true'){    

		                    alert('Data Changed Successfully');
		                    location.href='<?php echo base_url(); ?>lookup/add_lookup';
		                  }
		                }
		              });
		          }
		            });
		          </script>


		<!-- Data Table -->
		<script type="text/javascript">


			$('.usersdelete').click(function(){

				var idval = $(this).attr('id');

				$.ajax({
					url : "<?php echo base_url().'users/dodelete/'; ?>"+idval,
					data : {
						<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
					},
					method : 'POST',
					success : function(data)
					{
						location.href= '<?php echo base_url().'users/add_doctor/'; ?>'; 
					},
					error : function(error)
					{
						alert('Error Fetching Districts');
					}
				})
			});

			$(document).ready(function(){

				$(".glyphicon-trash").click(function(e){
					var ans = confirm("Are you sure you want to delete?");
					if(!ans)
					{
						e.preventDefault();
						return false;
					}
				});





				$("#usertype").change(function(){
					if($(this).val() == 1)
					{
						$(".facility_box").hide();
						$(".state_districts").hide();
						$(".state_districts_dis").hide();


						$("#id_mstfacility").removeAttr("required");
						$("#input_state").removeAttr("required");
						$("#input_district").removeAttr("required");
						$("#role_authentication").removeAttr("required");

						$("#id_mstfacility").val("");
						$("#input_state").val("");
						$("#input_district").val("");
						$("#role_authentication").val();
					}else if($(this).val() == 3){

						$(".state_districts").show();
						$(".state_districts_dis").hide();
						$(".facility_box").hide();
						$("#input_state").prop('required',true);
						$("#id_mstfacility").removeAttr("required");
						$("#input_district").removeAttr("required");
						$("#role_authentication").removeAttr("required");
				//$("#input_state").val("");
				$("#id_mstfacility").val("");
				$("#input_district").val("");

			}
			else if($(this).val() == 4){

				
				$(".state_districts").show();
				$(".state_districts_dis").show();
				$(".facility_box").hide();
				$("#input_state").prop('required',true);
				$("#input_district").prop('required',true);
				$("#role_authentication").prop('required',true);
				$("#id_mstfacility").removeAttr("required");
				//$("#input_state").val("");
				$("#id_mstfacility").val("");
				$("#input_district").val("");
				
				
			}
			else
			{
				$(".facility_box").show();
				$(".state_districts").show();
				$(".state_districts_dis").show();
				$("#id_mstfacility").prop('required',true);
				$("#input_state").prop('required',true);
				$("#input_district").prop('required',true);
				$("#role_authentication").prop("required",true);
			}
		});

				$("#submit_btn").click(function(e){

					if($("#password").val() != $("#repassword").val())
					{
						alert("Passswords do not match");
						e.preventDefault();
					}
			// if(edit_flag == 1)
			// {
			// 	var text = "Are you sure you want to Update this user";
			// }
			// else
			// {
			// 	var text = "Are you sure you want to Add this user";
			// }

			// var ans = confirm(text);
			// if(!ans)
			// {
			// 	e.preventDefault();
			// }
		});
			});

			function isNumberKey(evt)
			{
				var charCode = (evt.which) ? evt.which : event.keyCode
				if (charCode > 31 && (charCode < 48 || charCode > 57))
					return false;
			}
			$('.messagedata').keypress(function (e) {
				var regex = new RegExp(/^[a-zA-Z\s]+$/);
				var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
				if (regex.test(str)) {
					return true;
				}
				else {
					e.preventDefault();
					return false;
				}
			});

			$(document).ready(function(){
				$("#input_state").trigger('change');


			});

			$('#input_state').change(function(){

				$.ajax({
					url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
					data : {
						<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
					},
					method : 'POST',
					success : function(data)
					{
					//alert(data);
					$('#input_district').html(data);
					//$("#input_district").trigger('change');

				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
			});

			$('#input_district').change(function(){

				$.ajax({
					url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
					data : {
						<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
					},
					method : 'POST',
					success : function(data)
					{

						if(data!="<option value=''>Select Facilities</option>"){
							$('#id_mstfacility').html(data);
						}
					},
					error : function(error)
					{
					//alert('Error Fetching Blocks');
				}
			})
			});

		</script>


