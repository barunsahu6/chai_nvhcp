<style>
thead
{
	background-color: #085786;
	color: white;
}
.td_input{width: 100%;padding: 0px;border-radius: 2px;height: 40px;font-size:14px;font-weight: 600;border-width: 0px;border-style: none;}
.table-bordered>tbody>tr>td.data_td
{
	font-weight: 600;
	font-size: 15px;
	text-align: center;
	vertical-align: middle;
	background-color: #e4e4e4;
	border-color:#ffffff;
	border-width:1px;
	color:#000000;
}
.table-bordered>tbody>tr>td.data_td_late
{
	font-weight: 600;
	font-size: 15px;
	background-color: #ffffff;
	text-align: center;
	vertical-align: middle;
	padding: 0px;
}
.table-bordered>tbody>tr>td.total,.table-bordered>tbody>tr>td.td_total
{
	font-weight: 600;
	font-size: 16px;
	background-color: #ffffff;
	text-align: center;
	vertical-align: middle;
	padding: 0px;
}
</style>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 3 !important;
	width: 100% !important;
	font-size: 14px !important;
	font-family: 'Source Sans Pro';
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
	.col-md-2{
		width: 16.33%;
		padding-left: 8px;
		padding-right: 8px;
	}
	.btn-width{
	width: 12%;
	margin-top: 20px;
}
.pd-15{
	padding-left: 15px;
}
.pb-20{
	padding-bottom: 20px;
}
.container{
	width: 76%;
}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 4 !important;
		width: 100% !important;
		font-size: 14px !important;
		font-family: 'Source Sans Pro';
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 7px 12px;
	margin-bottom: 0;
	font-size: 13px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
 .table-bordered>thead>tr>th{
	vertical-align: middle;
	font-size: 13px;
}
.table-bordered>tbody>tr>td{
	vertical-align: middle;
	font-size: 13px;
	color: #000000;
}
</style>
</style>
<br>

<?php $loginData = $this->session->userdata('loginData');
//echo "<pre>"; print_r($loginData);

if($loginData->user_type==1) { 
$select = '';
}else{
$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
$select4 = '';	
}

$filters1 = $this->session->userdata('filters1'); 
//pr($filters1);
//echo $male->hcv_registered_cummulative;
 ?>
<div class="container">
		<div class="panel panel-default" id="Monthly_Report_Panel">
				<div class="panel-heading" style="background-color: #333333;padding: 3px 0px;">
					<h4 class="text-center" style="background-color: #333333;color: white; font-family: 'Source Sans Pro';letter-spacing: .75px;">Offline Monthly Records Entry (HCV)
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/excel_black.png" alt=""></a> -->
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/pdf_color.png" alt=""></a> -->
		</h4>
		<!-- <input type="button" onclick="printDiv('printableArea')" value="Print" /> -->
		
</div>

<?php
           $attributes = array(
              'id' => 'filter_form',
              'name' => 'filter_form',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>
<div class="row" style="padding: 5px 0px 5px 15px;">

	<div class="col-md-2 col-sm-12 col-md-offset-2">
		<label for="">State</label>
		<select type="text" name="search_state" id="search_state" class="form-control input_fields" required <?php echo $select2.$select3.$select4; ?>>
			<option value="0">All States</option>
			<?php 
			foreach ($states as $state) {
				?>
				<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo $state->StateName; ?></option>
				<?php 
			}
			?>
		</select>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">Year</label>
		<select name="year" id="year" required class="form-control input_fields" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2020', date('Y'));
										$flag=0;
										foreach($dates as $date){
												$year = $date;
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									elseif($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
															
								}
								?>
							</select>
	</div>

	<div class="col-md-2 col-sm-12">
		<label for="">Month</label>
		<select type="text" name="month" id="month" class="form-control input_fields" required>
<option value="">Select</option>
<?php
	$flag=0;
for ($i = 1; $i <=12;$i++) {
$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
if($this->input->post('month')==$i){
echo "<option value=".$i." selected>".$date_str ."</option>";
$flag=1;
}
elseif($i==date('m') && $flag==0){
	echo "<option value=".$i." selected>".$date_str ."</option>";
}
else{
	echo "<option value=".$i.">".$date_str ."</option>";

}
} ?>
</select>
	</div>

<div class="col-lg-2 col-md-2 col-sm-12 btn-width" style="margin-top: 20px;">
					<button type="submit" class="btn btn-block btn-success" id="search" name="search" value="search" style="line-height: 1.2; font-weight: 600;">SEARCH</button>
				</div>
				<!-- <?php //if (!((date('m')==$this->input->post('month') && date('Y')!=$this->input->post('year')) || (date('m')!=$this->input->post('month')&& date('d')>5))) {
					?> -->
					
				
				<span style="margin-top: 20px;margin-right: 25px; float: right;width: 80px;" id="span_save"><button type="submit" class="btn btn-block btn-success" id="save" name="save" value="save" style="line-height: 1.5; font-weight: 600;">Save</button></span>
				<!-- <?php // } ?> -->
			</div>

 
<br>
</div>
<div class="row" id="printableArea">

	<div class="col-md-12">
		<table class="table table-bordered table-highlighted" id="testTable" >
<tbody>
	<tr>
				<th style="background-color: #ffcccb;color: black; font-family:'Source Sans Pro';font-size: 14px;text-align: center;font-weight: 500" colspan="7">Please input data from <?php echo date("F-Y", strtotime( $filters1['startdate1'])); ?> to  <?php echo date("F-Y", strtotime($filters1['enddate'])); ?></th>
				
			</tr>
			<tr>
					<th style="background-color: #085786;color: white;">1</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';">Number of Hepatitis C infected people seeking care at the treatment center (Registering in Care)</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" nowrap="nowrap">Adult Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</tr>
			

				<tr style="font-family:'Source Sans Pro';" class="td_row">
					<td>1.1</td>
					<td style="padding-top: 0px;padding-bottom: 2px;">Cumulative number of persons registered in Hepatitis C care at the beginning of this month</td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_registered_cummulative[]" id="hcv_registered_cummulative_adult_male" value="<?php echo !empty($male->hcv_registered_cummulative)>0 ? $male->hcv_registered_cummulative : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_registered_cummulative[]" id="hcv_registered_cummulative_adult_female" value="<?php echo !empty($female->hcv_registered_cummulative)>0 ? $female->hcv_registered_cummulative : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_registered_cummulative[]" id="hcv_registered_cummulative_transgender" value="<?php echo !empty($transgender->hcv_registered_cummulative)>0 ? $transgender->hcv_registered_cummulative : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_registered_cummulative_children" id="hcv_registered_cummulative_children" value="<?php echo !empty($children->hcv_registered_cummulative)>0 ? $children->hcv_registered_cummulative : '0'; ?>"></td>
						<td class="total total_cummulative_reg"><?php echo "0"; ?></td>
				</tr>


				<tr style="font-family:'Source Sans Pro';" class="td_row">
					<td>1.2</td>
					<td nowrap="nowrap">Number of new persons registered in during this month</td>
						<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_registered[]" id="hcv_registered_adult_male" value="<?php echo !empty($male->hcv_registered)>0 ? $male->hcv_registered : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_registered[]" id="hcv_registered_adult_female" value="<?php echo !empty($female->hcv_registered)>0 ? $female->hcv_registered : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_registered[]" id="hcv_registered_transgender" value="<?php echo !empty($transgender->hcv_registered)>0 ? $transgender->hcv_registered : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_registered_children" id="hcv_registered_children" value="<?php echo !empty($children->hcv_registered)>0 ? $children->hcv_registered : '0'; ?>"></td>
						<td class="total total_reg"><?php echo "0"; ?></td>
				</tr>
				
				<tr style="font-family:'Source Sans Pro';">
					<td>1.3</td>
					<td>Cumulative number of persons registered at the end of this month</td>
						<td class="total_male_registered td_total">0</td>
					<td class="total_female_registered td_total">0</td>
					<td class="total_transgender_registered td_total">0</td>
					<td class="total_children_registered td_total">0</td>
					<td class="total_all_registered td_total">0</td>
				</tr>


			<tr>
				<th style="background-color: #085786;color: white; ">2</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';">Initiation of treatment</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" nowrap="nowrap">Adult Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

			</tr>
			
				<tr style="font-family:'Source Sans Pro';" class="td_row">
					<td >2.1</td>
					<td style="padding-top: 0px;padding-bottom: 2px;">Cumulative number of patients ever started on Treatment (Number at the beginning of this month)</td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_initiated_on_treatment_cumulative[]" id="cumulative_initiated_male" value="<?php echo !empty($male->hcv_treatment_start_cummulative)>0 ? $male->hcv_treatment_start_cummulative : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_initiated_on_treatment_cumulative[]" id="cumulative_initiated_female" value="<?php echo !empty($female->hcv_treatment_start_cummulative)>0 ? $female->hcv_treatment_start_cummulative : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_initiated_on_treatment_cumulative[]" id="cumulative_initiated_transgender" value="<?php echo !empty($transgender->hcv_treatment_start_cummulative)>0 ? $transgender->hcv_treatment_start_cummulative : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_initiated_on_treatment_cumulative_children" id="cumulative_initiated_children" value="<?php echo !empty($children->hcv_treatment_start_cummulative)>0 ? $children->hcv_treatment_start_cummulative : '0'; ?>"></td>
							<td class="total"><?php echo "0"; ?></td>
				</tr>
				<tr style="font-family:'Source Sans Pro';" class="td_row">
					<td>2.2</td>
					<td>Number of new patients started on treatment during this month</td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_initiated_on_treatment[]" id="initiated_male" value="<?php echo !empty($male->hcv_treatment_start)>0 ? $male->hcv_treatment_start : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_initiated_on_treatment[]" id="initiated_female" value="<?php echo !empty($female->hcv_treatment_start)>0 ? $female->hcv_treatment_start : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_initiated_on_treatment[]" id="initiated_transgender" value="<?php echo !empty($transgender->hcv_treatment_start)>0 ? $transgender->hcv_treatment_start : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_initiated_on_treatment_children" id="initiated_children" value="<?php echo !empty($children->hcv_treatment_start)>0 ? $children->hcv_treatment_start : '0'; ?>"></td>
							<td class="total"><?php echo "0"; ?></td>

				</tr>
				<tr style="font-family:'Source Sans Pro';" class="td_row">
					<td>2.3</td>
					<td>Number of patients on treatment "transferred in" during this month</td>


						<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_tranfered_in[]" id="transferred_in_male" value="<?php echo !empty($male->hcv_transferred_in)>0 ? $male->hcv_transferred_in : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_tranfered_in[]" id="transferred_in_female" value="<?php echo !empty($female->hcv_transferred_in)>0 ? $female->hcv_transferred_in : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_tranfered_in[]" id="transferred_in_transgender" value="<?php echo !empty($transgender->hcv_transferred_in)>0 ? $transgender->hcv_transferred_in : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_tranfered_in_children" id="transferred_in_children" value="<?php echo !empty($children->hcv_transferred_in)>0 ? $children->hcv_transferred_in : '0'; ?>"></td>
							<td class="total"><?php echo "0"; ?></td>

				</tr>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;">
					<td>2.4</td>
					<td>Cumulative number of patients ever received treatment (number at the end of this month) </td>

					<td class="td_total male_2">0</td>
					<td class="td_total female_2">0</td>
					<td class="td_total transgender_2">0</td>
					<td class="td_total children_2">0</td>
					<td class="td_total total_all_2">0</td>

					

				</tr>
		

			<tr>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';">3</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';">Treatment status (at the end of the month) out of all patients ever started on treatment</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" nowrap="nowrap">Adult Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>

			</tr>
			

				<tr style="font-family:'Source Sans Pro';" class="td_row">
					<td>3.1</td>
					<td>Cumulative number of patients who have completed treatment<div class="pull-right text-right glyphicon glyphicon-info-sign" style="font-size: 14px; padding: 0px;"  data-toggle="tooltip" title="" data-original-title="Number of patients that have completed their treatment regimen (Pre-SVR)"></div></td>

						<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_treatment_completed_cummulative[]" id="male_3_1" value="<?php echo !empty($male->hcv_treatment_completed_cummulative)>0 ? $male->hcv_treatment_completed_cummulative : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_treatment_completed_cummulative[]" id="female_3_1" value="<?php echo !empty($female->hcv_treatment_completed_cummulative)>0 ? $female->hcv_treatment_completed_cummulative : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_treatment_completed_cummulative[]" id="transgender_3_1" value="<?php echo !empty($transgender->hcv_treatment_completed_cummulative)>0 ? $transgender->hcv_treatment_completed_cummulative : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_treatment_completed_cummulative_children" id="children_3_1" value="<?php echo !empty($children->hcv_treatment_completed_cummulative)>0 ? $children->hcv_treatment_completed_cummulative : '0'; ?>" ></td>
							<td class="total total_3_1"><?php echo "0"; ?></td>
				</tr>

				<tr style="font-family:'Source Sans Pro';">
					<td>3.2</td>
					<td style="padding-top: 1px;padding-bottom: 2px;">Cumulative number of patients who are currently taking treatment<br>
					(3.2 = 2.4-(3.1+(3.3+3.5+3.8)))</td>
					<td class="td_total male_3_2"><?php echo "0"; ?></td>
					<td class="td_total female_3_2"><?php echo "0"; ?></td>
					<td class="td_total transgender_3_2"><?php echo "0"; ?></td>
					<td class="td_total children_3_2"><?php echo "0"; ?></td>
					<td class="td_total total_all_3_2"><?php echo "0"; ?></td>
				</tr>
				

				<tr style="font-family:'Source Sans Pro';" class="td_row">
					<td>3.3</td>
					<td>Cumulative Number of patients who "transferred out"</td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_tranfered_out[]" id="male_3_3" value="<?php echo !empty($male->hcv_transferred_out_cummulative)>0 ? $male->hcv_transferred_out_cummulative : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_tranfered_out[]" id="female_3_3" value="<?php echo !empty($female->hcv_transferred_out_cummulative)>0 ? $female->hcv_transferred_out_cummulative : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_tranfered_out[]" id="transgender_3_3" value="<?php echo !empty($transgender->hcv_transferred_out_cummulative)>0 ? $transgender->hcv_transferred_out_cummulative : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_tranfered_out_children" id="children_3_3" value="<?php echo !empty($children->hcv_transferred_out_cummulative)>0 ? $children->hcv_transferred_out_cummulative : '0'; ?>"></td>
							<td class="total total_3_3"><?php echo "0"; ?></td>
				</tr>
					<tr style="font-family:'Source Sans Pro';" class="td_row">
					<td>3.4</td>
					<td style="padding-top: 0px;padding-bottom: 2px;">The number of all patients whose treatment status in this month is “stopped treatment” due to side effects/medical reasons</td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_treatment_stopped[]" id="male_3_4" value="<?php echo !empty($male->hcv_treatment_stopped)>0 ? $male->hcv_treatment_stopped : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_treatment_stopped[]" id="female_3_4" value="<?php echo !empty($female->hcv_treatment_stopped)>0 ? $female->hcv_treatment_stopped : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_treatment_stopped[]" id="transgender_3_4" value="<?php echo !empty($transgender->hcv_treatment_stopped)>0 ? $transgender->hcv_treatment_stopped : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_treatment_stopped_children" id="children_3_4"  value="<?php echo !empty($children->hcv_treatment_stopped)>0 ? $children->hcv_treatment_stopped : '0'; ?>"></td>
							<td class="total total_3_4"><?php echo "0"; ?></td>
									</tr>
				<tr style="font-family:'Source Sans Pro';" class="td_row">
					<td>3.5</td>
					<td>Cumulative Number of patients who are lost to follow-up (LTFU)</td>
				<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_ltfu[]" id="male_3_5" value="<?php echo !empty($male->hcv_ltfu)>0 ? $male->hcv_ltfu : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_ltfu[]" id="female_3_5" value="<?php echo !empty($female->hcv_ltfu)>0 ? $female->hcv_ltfu : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_ltfu[]" id="transgender_3_5" value="<?php echo !empty($transgender->hcv_ltfu)>0 ? $transgender->hcv_ltfu : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_ltfu_children" id="children_3_5"  value="<?php echo !empty($children->hcv_ltfu)>0 ? $children->hcv_ltfu : '0'; ?>"></td>
							<td class="total total_3_5" ><?php echo "0"; ?></td>
				</tr>

				<tr style="font-family:'Source Sans Pro';" class="td_row">
					<td>3.6</td>
					<td style="padding-top: 0px;padding-bottom: 2px;">The number of patients who did not return to the facility and missed their doses in this month(defaulter)</td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_missed_doses[]" id="male_3_6" value="<?php echo !empty($male->hcv_missed_doses)>0 ? $male->hcv_missed_doses : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_missed_doses[]" id="female_3_6" value="<?php echo !empty($female->hcv_missed_doses)>0 ? $female->hcv_missed_doses : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_missed_doses[]" id="transgender_3_6" value="<?php echo !empty($transgender->hcv_missed_doses)>0 ? $transgender->hcv_missed_doses : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_missed_doses_children" id="children_3_6" value="<?php echo !empty($children->hcv_missed_doses)>0 ? $children->hcv_transferred_in : '0'; ?>"></td>
							<td class="total total_3_6"><?php echo "0"; ?></td>
				</tr>

				<tr style="font-family:'Source Sans Pro';" class="td_row">
					<td>3.7</td>
					<td style="padding-top: 0px;padding-bottom: 2px;">Total number of patients referred to higher center for further management</td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_reffered[]" id="male_3_7" value="<?php echo !empty($male->hcv_reffered)>0 ? $male->hcv_reffered : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_reffered[]" id="female_3_7" value="<?php echo !empty($female->hcv_reffered)>0 ? $female->hcv_reffered : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_reffered[]" id="transgender_3_7" value="<?php echo !empty($transgender->hcv_reffered)>0 ? $transgender->hcv_reffered : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_reffered_children" id="children_3_7" value="<?php echo !empty($children->hcv_reffered)>0 ? $children->hcv_reffered : '0'; ?>"></td>
							<td class="total total_3_7"><?php echo "0"; ?></td>
				</tr>

				<tr style="font-family:'Source Sans Pro';" class="td_row">
					<td>3.8</td>
					<td>Number of deaths reported</td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_death_reported[]" id="male_3_8" value="<?php echo !empty($male->hcv_death_reported)>0 ? $male->hcv_death_reported : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_death_reported[]" id="female_3_8" value="<?php echo !empty($female->hcv_death_reported)>0 ? $female->hcv_death_reported : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_death_reported[]" id="transgender_3_8" value="<?php echo !empty($transgender->hcv_death_reported)>0 ? $transgender->hcv_death_reported : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_death_reported_children" id="children_3_8" value="<?php echo !empty($children->hcv_death_reported)>0 ? $children->hcv_transferred_in : '0'; ?>"></td>
							<td class="total total_3_8"><?php echo "0"; ?></td>
				</tr>



		

			<tr>
				<th style="background-color: #085786;color:white ; font-family:'Source Sans Pro';">4</th>
				<th style="background-color: #085786;color:white ;font-family:'Source Sans Pro';">Sustained Virologic Response</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;" nowrap="nowrap">Adult Male</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;"nowrap="nowrap">Adult Female</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Transgender</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Children < 18 years</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			</tr>
		
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;" class="td_row">
					<td>4.1</td>
					<td style="padding-top: 2px;padding-bottom: 0px;">Number of patients who are eligible for SVR ( i.e have completed 12 weeks after end of treatment)have completed 12 weeks treatment</td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_eligible_for_svr[]" value="<?php echo !empty($male->hcv_eligible_for_svr)>0 ? $male->hcv_eligible_for_svr : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_eligible_for_svr[]" value="<?php echo !empty($female->hcv_eligible_for_svr)>0 ? $female->hcv_eligible_for_svr : '0'; ?>"
></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_eligible_for_svr[]" value="<?php echo !empty($transgender->hcv_eligible_for_svr)>0 ? $transgender->hcv_eligible_for_svr : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_eligible_for_svr_children" value="<?php echo !empty($children->hcv_eligible_for_svr)>0 ? $children->hcv_eligible_for_svr : '0'; ?>"></td>
							<td class="total"><?php echo "0"; ?></td>
				</tr>
				

				<tr style="font-family:'Source Sans Pro'; font-size: 12px;" class="td_row">
					<td>4.2</td>
					<td style="padding-top: 2px;padding-bottom: 0px;">Cumulative number of patients who have undergone SVR out of the eligible patient</td>
						<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_svr_done[]" value="<?php echo !empty($male->hcv_svr_undergone)>0 ? $male->hcv_svr_undergone : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_svr_done[]" value="<?php echo !empty($female->hcv_svr_undergone)>0 ? $female->hcv_svr_undergone : '0'; ?>"
></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_svr_done[]" value="<?php echo !empty($transgender->hcv_svr_undergone)>0 ? $transgender->hcv_svr_undergone : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_svr_done_children" value="<?php echo !empty($children->hcv_svr_undergone)>0 ? $children->hcv_svr_undergone : '0'; ?>"></td>
							<td class="total"><?php echo "0"; ?></td>	
				</tr>
				<tr style="font-family:'Source Sans Pro'; font-size: 12px;" class="td_row">
					<td>4.3</td>
					<td>Cumulative number of undetectable HCV RNA (out of 4.2)</td>

				<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_rna[]" value="<?php echo !empty($male->hcv_rna)>0 ? $male->hcv_rna : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_rna[]" value="<?php echo !empty($female->hcv_rna)>0 ? $female->hcv_rna : '0'; ?>"
></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_rna[]" value="<?php echo !empty($transgender->hcv_rna)>0 ? $transgender->hcv_rna : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_rna_children" value="<?php echo !empty($children->hcv_rna)>0 ? $children->hcv_rna : '0'; ?>"></td>
							<td class="total"><?php echo "0"; ?></td>
				</tr>
			</tbody>
		</table>
		<?php echo form_close(); ?>
	</div>
</div>
</div>
<br>
<br>
<br>
<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name,filename) {
    if (!table.nodeType) table = document.getElementById(table)
            var newtable=document.createElement("table");
            var tbody =document.createElement("tbody");
             var row = document.createElement("tr");
             row.setAttribute("style", "background-color: black;color: white; font-family:'Source Sans Pro';");
  				var cell1 = document.createElement("td");
  				cell1.colSpan=7;
  				cell1.innerHTML ="Date as on : - <?php echo date('jS \ F Y'); ?>";
  				row.appendChild(cell1);
 			 	tbody.appendChild(row);
 			 	newtable.appendChild(tbody);
             var clonedTable=newtable.cloneNode(true);
             var clonedTable1=table.cloneNode(true);
            clonedTable.appendChild(clonedTable1);
    var ctx = {worksheet: name || 'Worksheet', table: clonedTable.innerHTML}
    //window.location.href = uri + base64(format(template, ctx))
     var link = document.createElement('a');
    link.download =filename;
    link.href = uri + base64(format(template, ctx));
    link.click();
     clonedTable.remove();
  }
})()
</script>
<script>

	
$(document).ready(function(){
var td_input_val=0;
$('[data-toggle="tooltip"]').tooltip();
$(".td_input").attr('min',0);
$("#search_state").trigger('change');


$('.td_input').click(function(){
	var error_css = {'background-color':'rgb(227, 245, 251)'};
	$(this).css(error_css);
	td_input_val=$(this).val();
	if ($(this).val()==0) {
		$(this).val('');
	}
});
$('.td_input').blur(function(){
	var error_css = {'background-color':'rgb(255, 255, 255)'};
	if ($(this).val()==0 || $(this).val()=='') {
		$(this).val(0);
	}
	$(this).css(error_css);
});
	$(".td_input").on('keypress',function(evt){
								var charCode = (evt.which) ? evt.which : event.keyCode
								
								if ((charCode > 31 && (charCode < 48 || charCode > 57))){
									evt.preventDefault();
									return false;
								}
								
								var index = $(".td_input").index(this);
                        		if( $('.td_input').eq(index).val() < 0 || $('.td_input').eq(index).val() >999999)
								{
									//alert(index);
									$("#modal_header").html('Value cannot be less than 0 or greater than 1000000');
									$("#modal_text").html('Please fill in appropriate Value');
									$("#multipurpose_modal").modal("show");
									$('.td_input').eq(index).val('');
									$(".td_input").trigger('keyup'); 
									return false;
								}
								

    							/*var cell = $(this).closest('td');
								var cellIndex = cell[0].cellIndex;*/

								return true;

							});
$(".td_input").on('keyup change',function(evt){

	var index= $('#testTable .td_row').index($(this).closest('.td_row'));
    							var sum = 0;
   								$('#testTable .td_row').eq(index).find(".td_input").each(function(){
        						sum += +$(this).val();
    							});
    							$(".total").eq(index).html(sum);			
});


	$('#hcv_registered_cummulative_adult_male,#hcv_registered_adult_male').on('keyup change',function(evt){
		$('.total_male_registered').html(parseInt($('#hcv_registered_cummulative_adult_male').val() || 0)+parseInt($('#hcv_registered_adult_male').val() || 0));
			add_all();	

	});
	$('#hcv_registered_cummulative_adult_female,#hcv_registered_adult_female').on('keyup change',function(evt){
		$('.total_female_registered').html(parseInt($('#hcv_registered_cummulative_adult_female').val() || 0)+parseInt($('#hcv_registered_adult_female').val() || 0));

			add_all();

	});
	$('#hcv_registered_cummulative_transgender,#hcv_registered_transgender').on('keyup change',function(evt){
		$('.total_transgender_registered').html(parseInt($('#hcv_registered_cummulative_transgender').val() || 0)+parseInt($('#hcv_registered_transgender').val() || 0));

		add_all();

	});
	$('#hcv_registered_cummulative_children,#hcv_registered_children').on('keyup change',function(evt){
		$('.total_children_registered').html(parseInt($('#hcv_registered_cummulative_children').val() || 0)+parseInt($('#hcv_registered_children').val() || 0));

		add_all();
	});

/*2nd row total gender wise*/

	$('#cumulative_initiated_male,#initiated_male,#transferred_in_male').on('keyup',function(evt){
		//alert($(this).val());
		var total=parseInt($('#cumulative_initiated_male').val() || 0)+parseInt($('#initiated_male').val() || 0)+parseInt($('#transferred_in_male').val() || 0);
	
			$('.male_2').html(total);
			add_all_2();
			$('#male_3_1,#male_3_3,#male_3_5,#male_3_8').trigger('keyup');
	});
	$('#cumulative_initiated_female,#initiated_female,#transferred_in_female').on('keyup',function(evt){
			
		var total=parseInt($('#cumulative_initiated_female').val() || 0)+parseInt($('#initiated_female').val() || 0)+parseInt($('#transferred_in_female').val() || 0);	
			$('.female_2').html(total);
			add_all_2();
			$('#female_3_1,#female_3_3,#female_3_5,#female_3_8').trigger('keyup');
	});
	$('#cumulative_initiated_transgender,#initiated_transgender,#transferred_in_transgender').on('keyup',function(evt){
		var total=(parseInt($('#cumulative_initiated_transgender').val() || 0)+parseInt($('#initiated_transgender').val() || 0)+parseInt($('#transferred_in_transgender').val() || 0));
		
			$('.transgender_2').html(total);
			add_all_2();
			$('#transgender_3_1,#transgender_3_3,#transgender_3_5,#transgender_3_8').trigger('keyup');

	});
	$('#cumulative_initiated_children,#initiated_children,#transferred_in_children').on('keyup',function(evt){
		var total=(parseInt($('#cumulative_initiated_children').val() || 0)+parseInt($('#initiated_children').val() || 0)+parseInt($('#transferred_in_children').val() || 0));

		
			$('.children_2').html(total);
			add_all_2();
			$('#children_3_1,#children_3_3,#children_3_5,#children_3_8').trigger('keyup');


	});

/*end */

/*3rd total gender wise*/

	$('#male_3_1,#male_3_3,#male_3_5,#male_3_8').on('keyup',function(evt){
		var total=((parseInt($('#male_3_1').val() || 0)+(parseInt($('#male_3_3').val() || 0)+parseInt($('#male_3_5').val() || 0)+parseInt($('#male_3_8').val() || 0))));
		
		var male_2_4=parseInt($('.male_2').text());
	
		var diff=male_2_4-total;
		//alert(diff);
		if(diff<0){
			$("#modal_header").html('2.4 male cannot be less than 3.2 male');
			$("#modal_text").html('Please fill in appropriate data');
			$("#multipurpose_modal").modal("show");
			$(this).val('0');
			$('#male_3_1,#male_3_3,#male_3_5,#male_3_8').trigger('keyup');
			$(".td_input").trigger('change');
			return false;

		}
		else if(diff>0 || diff==0){
			$('.male_3_2').html(diff);
			add_all_3();
			
		}

	});
	$('#female_3_1,#female_3_3,#female_3_5,#female_3_8').on('keyup',function(evt){
		var total=((parseInt($('#female_3_1').val() || 0)+(parseInt($('#female_3_3').val() || 0)+parseInt($('#female_3_5').val() || 0)+parseInt($('#female_3_8').val() || 0))));

			var female_2_4=parseInt($('.female_2').text());
		var female_3_1=parseInt($('#female_3_1').val());
		var female_3_3=parseInt($('#female_3_3').val());
		/*var female_3_4=parseInt($('#female_3_4').val());*/
		var female_3_5=parseInt($('#female_3_5').val());
		/*var female_3_7=parseInt($('#female_3_7').val());*/
		var female_3_8=parseInt($('#female_3_8').val());
		if (total<0) {
			total=total*(-1);
		}
		var diff=female_2_4-total;
		if(diff<0){
			$("#modal_header").html('2.4 female cannot be less than 3.2 female');
			$("#modal_text").html('Please fill in appropriate data');
			$("#multipurpose_modal").modal("show");
			$(this).val('0');
			$('#female_3_1,#female_3_3,#female_3_5,#female_3_8').trigger('keyup')
			$(".td_input").trigger('change');
			return false;

		}
		else if(diff>0 || diff==0){
			$('.female_3_2').html(diff);
			add_all_3();
			
		}

	});
	$('#transgender_3_1,#transgender_3_3,#transgender_3_5,#transgender_3_8').on('keyup',function(evt){
		var total=((parseInt($('#transgender_3_1').val() || 0)+(parseInt($('#transgender_3_3').val() || 0)+parseInt($('#transgender_3_5').val() || 0)+parseInt($('#transgender_3_8').val() || 0))));

		var transgender_2_4=parseInt($('.transgender_2').text());
		var transgender_3_1=parseInt($('#transgender_3_1').val());
		var transgender_3_3=parseInt($('#transgender_3_3').val());
		/*var transgender_3_4=parseInt($('#transgender_3_4').val());*/
		var transgender_3_5=parseInt($('#transgender_3_5').val());
		/*var transgender_3_7=parseInt($('#transgender_3_7').val());*/
		var transgender_3_8=parseInt($('#transgender_3_8').val());
		if (total<0) {
			total=total*(-1);
		}
		var diff=transgender_2_4-total;
		
		if(diff<0){
			$("#modal_header").html('2.4 transgender cannot be less than 3.2 transgender');
			$("#modal_text").html('Please fill in appropriate data');
			$("#multipurpose_modal").modal("show");
			$(this).val('0');
			$('#transgender_3_1,#transgender_3_3,#transgender_3_5,#transgender_3_8').trigger('keyup');
			$(".td_input").trigger('change');
			return false;

		}
		else if(diff>0 || diff==0){
			$('.transgender_3_2').html(diff);
			add_all_3();
			
		}


	});
	$('#children_3_1,#children_3_3,#children_3_5,#children_3_8').on('keyup',function(evt){
		var total=((parseInt($('#children_3_1').val() || 0)+(parseInt($('#children_3_3').val() || 0)+parseInt($('#children_3_5').val() || 0)+parseInt($('#children_3_8').val() || 0))));

		var children_2_4=parseInt($('.children_2').text());
		var children_3_1=parseInt($('#children_3_1').val());
		var children_3_3=parseInt($('#children_3_3').val());
		/*var children_3_4=parseInt($('#children_3_4').val());*/
		var children_3_5=parseInt($('#children_3_5').val());
		/*var children_3_7=parseInt($('#children_3_7').val());*/
		var children_3_8=parseInt($('#children_3_8').val());
		if (total<0) {
			total=total*(-1);
		}
		var diff=children_2_4-total;
		if(diff<0){
			$("#modal_header").html('2.4 children cannot be less than 3.2 children');
			$("#modal_text").html('Please fill in appropriate data');
			$("#multipurpose_modal").modal("show");
			$(this).val('0');
			$('#children_3_1,#children_3_3,#children_3_5,#children_3_8').trigger('keyup');
			$(".td_input").trigger('change');
			return false;

		}
		else if(diff>0 || diff==0){
			$('.children_3_2').html(diff);
			add_all_3();
			
		}
	});

$('#year').change(function(){
	var count=0;
	<?php  $count=0; ?>;
	var options='<option value="">Select</option>';
	var currentYear = (new Date).getFullYear();
	if (currentYear==$('#year').val()) {
			<?php
		$flag=0;
		$count=date("m");
	for ($i = 1; $i <=$count;$i++) {
	$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
	if($this->input->post('month')==$i){ 	?>
	options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php $flag=1;
	}	
	elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php }
	else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	}
	else{
		<?php
		$flag=0;
		$count=12;
		for ($i = 1; $i <=$count;$i++) {
		$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
		if($this->input->post('month')==$i){ 	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php $flag=1;
		}	
		elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php }
		else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	
	}
	//alert(options);
$('#month').html(options);
$('#month').trigger('change');
});

$('#month').change(function(e){
var month=$('#month').val();
var year=$('#year').val();
//$clone = $('#span_save').clone( true )
var currentYear = (new Date).getFullYear();
var currentMonth=(new Date).getMonth()+1;
var currentdate = (new Date).getDate();
if (!((currentMonth==month && currentYear!=year) || (currentMonth!=month && currentdate>5))){
	$('#span_save').show();
	$('.td_input').prop("readonly", false);
}
else{
	$('#span_save').hide();
	$('.td_input').prop("readonly", true);
}
});

$("#year").trigger('change');
$(".td_input").trigger('change');
update_1_3();
update_2_4();
update_3_2();

});
$('#search_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');
					//$('#input_district').val($('#input_district').val());
				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
					$('#mstfacilitylogin').html(data);
					
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
function add_all(){
	$('.total_all_registered').html(parseInt($('.total_male_registered').html())+parseInt($('.total_female_registered').html())+parseInt($('.total_transgender_registered').html())+parseInt($('.total_children_registered').html()));
}
function add_all_2(){
	$('.total_all_2').html(parseInt($('.male_2').html())+parseInt($('.female_2').html())+parseInt($('.transgender_2').html())+parseInt($('.children_2').html()));
}
function add_all_3(){
	$('.total_all_3_2').html(parseInt($('.male_3_2').html())+parseInt($('.female_3_2').html())+parseInt($('.transgender_3_2').html())+parseInt($('.children_3_2').html()));
}

function update_3_2(){
$('#male_3_1,#male_3_3,#male_3_5,#male_3_8').trigger('keyup');
$('#female_3_1,#female_3_3,#female_3_5,#female_3_8').trigger('keyup');
$('#transgender_3_1,#transgender_3_3,#transgender_3_5,#transgender_3_8').trigger('keyup');
$('#children_3_1,#children_3_3,#children_3_5,#children_3_8').trigger('keyup');
}
function update_2_4(){
	$('#cumulative_initiated_male,#initiated_male,#transferred_in_male').trigger('keyup');
$('#cumulative_initiated_female,#initiated_female,#transferred_in_female').trigger('keyup');
$('#cumulative_initiated_transgender,#initiated_transgender,#transferred_in_transgender').trigger('keyup');
$('#cumulative_initiated_children,#initiated_children,#transferred_in_children').trigger('keyup');
}
function update_1_3(){
	$('#hcv_registered_cummulative_adult_male,#hcv_registered_adult_male').trigger('change');
$('#hcv_registered_cummulative_adult_female,#hcv_registered_adult_female').trigger('change');
$('#hcv_registered_cummulative_transgender,#hcv_registered_transgender').trigger('change');
$('#hcv_registered_cummulative_children,#hcv_registered_children').trigger('change');

}
function check_count(){
		var male_2=parseInt($('.male_2').html());
	var female_2=parseInt($('.female_2').html());
	var transgender_2=parseInt($('.transgender_2').html());
	var children_2=parseInt($('.children_2').html());
	var male_3_2=parseInt($('.male_3_2').html());
	var female_3_2=parseInt($('.female_3_2').html());
	var transgender_3_2=parseInt($('.transgender_3_2').html());
	var children_3_2=parseInt($('.children_3_2').html());
if (male_2<male_3_2 || female_2<female_3_2 || transgender_2< transgender_3_2 || children_2<children_3_2) {
	return 0;
}
else{
	return 1;
}
}
</script>