<style>
thead
{
	background-color: #085786;
	color: white;
}

.table-bordered>tbody>tr>td.data_td
{
	font-weight: 600;
	font-size: 15px;
	text-align: center;
	vertical-align: middle;
	background-color: #e4e4e4;
	border-color:#ffffff;
	border-width:1px;
	color:#000000;
}
.table-bordered>tbody>tr>td.data_td_late
{
	font-weight: 600;
	font-size: 16px;
	background-color: #ffffff;
}
</style>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 5 !important;
	width: 100% !important;
	font-size: 14px !important;
	font-family: 'Source Sans Pro';
}
textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
	.col-md-2{
		width: 16.33%;
		padding-left: 8px;
		padding-right: 8px;
	}
	.btn-width{
	width: 12%;
	margin-top: 20px;
}
.pd-15{
	padding-left: 15px;
}
.pb-20{
	padding-bottom: 20px;
}
.container{
	width: 100%;
}
}

@media (max-width: 768px) {

.input_fields
	{
		height: 40px !important;
		border-radius: 5 !important;
		width: 100% !important;
		font-size: 14px !important;
		font-family: 'Source Sans Pro';
	}



	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 13px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
.table-bordered>thead>tr>th{
	vertical-align: middle;
	font-size: 13px;
	font-family: 'Source Sans Pro';
	background-color: #085786;
}
.table-bordered>tbody>tr>td{
	vertical-align: middle;
	font-size: 13px;
	color: #000000;
	font-family: 'Source Sans Pro';
}
</style>
</style>
<br>

<?php $loginData = $this->session->userdata('loginData');
//echo "<pre>"; print_r($loginData);

if($loginData->user_type==1) { 
$select = '';
}else{
$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
$select4 = '';	
}

$filters1 = $this->session->userdata('filters1'); 
//pr($filters1);
 ?>
<div class="container">
		<div class="panel panel-default" id="Progress_HCV_Report_Panel">
				<div class="panel-heading" style="background-color: #333333;padding: 3px 0px;">
					<h4 class="text-center" style="background-color: #333333;color: white;font-family: 'Source Sans Pro';letter-spacing: .75px;">Offline Entry Comparision Report(HCV)
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/excel_black.png" alt=""></a> -->
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/pdf_color.png" alt=""></a> -->
		</h4>
		<!-- <input type="button" onclick="printDiv('printableArea')" value="Print" /> -->
		
</div>

<?php
           $attributes = array(
              'id' => 'filter_form',
              'name' => 'filter_form',
               'autocomplete' => 'off',
            );
           echo form_open('reports/comparision_offline_entry_report/', $attributes); ?>
<div class="row" style="padding-left: 15px; padding-top: 15px;">

	<div class="col-md-2 col-sm-12 col-md-offset-2">
		<label for="">State</label>
		<select type="text" name="search_state" id="search_state" class="form-control input_fields" required <?php echo $select2.$select3.$select4; ?>>
			<option value="0">All States</option>
			<?php 
			foreach ($states as $state) {
				?>
				<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo $state->StateName; ?></option>
				<?php 
			}
			?>
		</select>
	</div>
	<!-- <div class="col-md-2 col-sm-12">
		<label for="">District</label>
		<select type="text" name="input_district" id="input_district" class="form-control input_fields"   <?php echo $select.$select2.$select4; ?>>
			
	
			<?php foreach ($districts as  $value) { ?>
				<option value="<?php echo $value->id_mstdistrict; ?>" <?php if($this->input->post('input_district')==$value->id_mstdistrict) { echo 'selected';}  ?>><?php echo $value->DistrictName; ?></option>
			<?php } ?>
			<?php 
			
			?>
		</select>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">Facility</label>
		<select type="text" name="facility" id="mstfacilitylogin" class="form-control input_fields"  <?php echo $select.$select2; ?>>
			<option value="">Select Facility</option>
			<?php 
			
			?>
		</select>
	</div> -->


	<!-- <div class="col-md-2 col-sm-12">
		<label for="">From Date</label>
		<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')) { echo $this->input->post('startdate'); }else{ echo timeStampShow(date('Y-m-01')); } ?>" required>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">To date</label>
		<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')) { echo $this->input->post('enddate'); }else{  echo timeStampShow(date('Y-m-d'));} ?>" required>
	</div> -->
	<div class="col-md-2 col-sm-12">
		<label for="">Year</label>
		<select name="year" id="year" required class="form-control input_fields" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2020', date('Y'));
										$flag=0;
										foreach($dates as $date){
												$year = $date;
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									elseif($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
															
								}
								?>
							</select>
	</div>

	<div class="col-md-2 col-sm-12">
		<label for="">Month</label>
		<select type="text" name="month" id="month" class="form-control input_fields" required>
<option value="">Select</option>
<?php
	$flag=0;
for ($i = 3; $i <=12;$i++) {
$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
if($this->input->post('month')==$i){
echo "<option value=".$i." selected>".$date_str ."</option>";
$flag=1;
}
elseif($i==date('m') && $flag==0){
	echo "<option value=".$i." selected>".$date_str ."</option>";
}
else{
	echo "<option value=".$i.">".$date_str ."</option>";

}
} ?>
</select>
	</div>
<div class="col-lg-2 col-md-2 col-sm-12 btn-width" style="margin-top: 20px;">
					<button type="submit" class="btn btn-block btn-success" id="monthreport" style="line-height: 1.2; font-weight: 600;">SEARCH</button>
				</div><span style="margin-top: 25px;margin-right: 25px; float: right;"><a href="javascript:void(0)"><i class="fa fa-2x fa-download" id="excel_button" onclick="tableToExcel('testTable', 'W3C Example','Monthly_Report.xls')" title="Excel Download"></i></a></span>
			</div>


 <?php echo form_close(); ?>
<br>
</div>
<div class="row">
	<div class="col-sm-12 col-md-12">	
		<table class="table table-bordered table-highlighted" id="testTable">

			<thead>
					<th style="background-color: white;color: black;font-family:'Source Sans Pro';text-align: center;border:none;" colspan="1">&nbsp;</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;" colspan="5">Offline Monthly Entry </th>
				

				<th style="background-color: #01446b;color: white;font-family:'Source Sans Pro';text-align: center;" colspan="5">Regular Monthly Entry</th>

				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;" colspan="5">Late Monthly Entry </th>
				
			</thead>

			
			<thead>

				

				<th style="background-color: #085786;color: white; width: 150px !important;vertical-align: middle;" class="text-center">Name of State/UT</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">HCV Screened</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Anti-HCV positive</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">VL Detected</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Initiated on treatment</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Treatment Completed<div class="pull-right text-right glyphicon glyphicon-info-sign" style="font-size: 14px; padding: 0px;"  data-toggle="tooltip" title="" data-original-title="Number of patients that have completed their treatment regimen (Pre-SVR)">

</div></th>
				<th style="background-color: #01446b;color: white; width: 150px !important; vertical-align: middle;" class="text-center">HCV Screened</th>
				<th style="background-color: #01446b;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Anti-HCV positive</th>
				<th style="background-color: #01446b;color: white; width: 150px !important; vertical-align: middle;" class="text-center">VL Detected</th>
				<th style="background-color: #01446b;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Initiated on treatment</th>
				<th style="background-color: #01446b;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Treatment Completed<div class="pull-right text-right glyphicon glyphicon-info-sign" style="font-size: 14px; padding: 0px;"  data-toggle="tooltip" title="" data-original-title="Number of patients that have completed their treatment regimen (Pre-SVR)">

</div></th>
					<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">HCV Screened</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Anti-HCV positive</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">VL Detected</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Initiated on treatment</th>
				<th style="background-color: #085786;color: white; width: 150px !important; vertical-align: middle;" class="text-center">Treatment Completed<div class="pull-right text-right glyphicon glyphicon-info-sign" style="font-size: 14px; padding: 0px;"  data-toggle="tooltip" title="" data-original-title="Number of patients that have completed their treatment regimen (Pre-SVR)">

</div></th>
				
			</thead>
			<tbody>
				<?php $total_1 = 0;
					$total_2 = 0;
					$total_3 = 0; 
					$total_4 = 0;
					$total_5 = 0;
					$total1_1 = 0;
					$total1_2 = 0;
					$total1_3 = 0; 
					$total1_4 = 0;
					$total1_5 = 0;
					$total_reg_1 = 0;
					$total_reg_2 = 0;
					$total_reg_3 = 0;
					$total_reg_4 = 0;
					$total_reg_5 = 0;
					 if($nationaldata){  foreach ($nationaldata as $key=>$value) { 
					 		$total_1 += $value->hcv_screened;
					$total_2 += $value->hcv_positive;
					$total_3 += $value->eligible_for_treatment;
					$total_4 += $value->treatment_start;
					$total_5 += $value->treatment_completed;

					$total1_1 += $nationaldata1[$key]->anti_hcv_screened;
					$total1_2 += $nationaldata1[$key]->anti_hcv_positive;
					$total1_3 += $nationaldata1[$key]->viral_load_detected;
					$total1_4 += $nationaldata1[$key]->initiatied_on_treatment;
					$total1_5 += $nationaldata1[$key]->treatment_completed;

					$total_reg_1 += $hcv_screened[$key]->count;
					$total_reg_2 += $hcv_positive[$key]->count;
					$total_reg_3 += $hcv_vl_detected[$key]->count;
					$total_reg_4 += $ini_treatment[$key]->count;
					$total_reg_5 += $treatment_completed[$key]->count;
					
				 } } ?>
					<tr style="background-color: #d5e4e6;color: black;">

					<td class="thclass text-center"><?php echo 'India'; ?></td>
					<td class="thclass text-center data_td_late"><?php echo $total_1; ?> </td>
					<td class="thclass text-center data_td_late"><?php echo $total_2; ?> </td>
					<td class="thclass text-center data_td_late"><?php echo $total_3; ?></td>
					<td class="thclass text-center data_td_late" ><?php echo $total_4; ?> </td>
					<td class="thclass text-center data_td_late"><?php echo $total_5; ?> </td>

					<td class="thclass text-center data_td"><?php echo $total_reg_1; ?> </td>
					<td class="thclass text-center data_td"><?php echo $total_reg_2; ?> </td>
					<td class="thclass text-center data_td"><?php echo $total_reg_3; ?></td>
					<td class="thclass text-center data_td" ><?php echo $total_reg_4; ?> </td>
					<td class="thclass text-center data_td"><?php echo $total_reg_5; ?> </td>

					<td class="thclass text-center data_td_late"><?php echo $total1_1; ?> </td>
					<td class="thclass text-center data_td_late"><?php echo $total1_2; ?> </td>
					<td class="thclass text-center data_td_late"><?php echo $total1_3; ?></td>
					<td class="thclass text-center data_td_late" ><?php echo $total1_4; ?> </td>
					<td class="thclass text-center data_td_late"><?php echo $total1_5; ?> </td>
					
					
				</tr>
				<?php 
 					if($nationaldata){  foreach ($nationaldata as $key=>$value) { 

					
					?>
				<tr>

					<td ><?php echo $value->StateName; ?></td>
					<td class="text-center data_td_late"><?php if($value->hcv_screened=='0') {echo '-';}else{ echo $value->hcv_screened;} ?> </td>
					<td class="text-center data_td_late"><?php if($value->hcv_positive=='0'){ echo '-';}else{ echo $value->hcv_positive;} ?> </td>
					<td class="text-center data_td_late"><?php if($value->eligible_for_treatment=='0'){ echo '-';}else{ echo $value->eligible_for_treatment;} ?></td>
					<td class="text-center data_td_late" ><?php if($value->treatment_start=='0'){ echo '-'; } else{ echo $value->treatment_start;} ?> </td>
					<td class="text-center data_td_late"><?php if($value->treatment_completed=='0'){ echo '-';}else{ echo $value->treatment_completed;} ?> </td>


					<td class="text-center data_td"><?php if($hcv_screened[$key]->count=='0') {echo '-';}else{ echo $hcv_screened[$key]->count;} ?> </td>
					<td class="text-center data_td"><?php if($hcv_positive[$key]->count=='0'){ echo '-';}else{ echo $hcv_positive[$key]->count;} ?> </td>
					<td class="text-center data_td" ><?php if($hcv_vl_detected[$key]->count=='0'){ echo '-'; } else{ echo $hcv_vl_detected[$key]->count;} ?> </td>
					<td class="text-center data_td" ><?php if($ini_treatment[$key]->count=='0'){ echo '-'; } else{ echo $ini_treatment[$key]->count;} ?> </td>
					<td class="text-center data_td"><?php if($treatment_completed[$key]->count=='0'){ echo '-';}else{ echo $treatment_completed[$key]->count;} ?> </td>


						<td class="text-center data_td_late"><?php if($nationaldata1[$key]->anti_hcv_screened=='0') {echo '-';}else{ echo $nationaldata1[$key]->anti_hcv_screened;} ?> </td>
					<td class="text-center data_td_late"><?php if($nationaldata1[$key]->anti_hcv_positive=='0'){ echo '-';}else{ echo $nationaldata1[$key]->anti_hcv_positive;} ?> </td>
					<td class="text-center data_td_late"><?php if($nationaldata1[$key]->viral_load_detected=='0'){ echo '-';}else{ echo $nationaldata1[$key]->viral_load_detected;} ?></td>
					<td class="text-center data_td_late" ><?php if($nationaldata1[$key]->initiatied_on_treatment=='0'){ echo '-'; } else{ echo $nationaldata1[$key]->initiatied_on_treatment;} ?> </td>
					<td class="text-center data_td_late"><?php if($nationaldata1[$key]->treatment_completed=='0'){ echo '-';}else{ echo $nationaldata1[$key]->treatment_completed;} ?> </td>
					
				</tr>

<?php } } ?>


				
			</tbody>
		
			
		</table>
	</div>
</div>
</div>
<br>
<br>
<br>
<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name,filename) {
    if (!table.nodeType) table = document.getElementById(table)
            var newtable=document.createElement("table");
            var tbody =document.createElement("tbody");
             var row = document.createElement("tr");
             row.setAttribute("style", "background-color: black;color: white; font-family:Calibri;");
  				var cell1 = document.createElement("td");
  				cell1.colSpan=7;
  				cell1.innerHTML ="Data as on : - <?php echo date('jS \ F Y'); ?>";
  				row.appendChild(cell1);
 			 	tbody.appendChild(row);
 			 	newtable.appendChild(tbody);
             var clonedTable=newtable.cloneNode(true);
             var clonedTable1=table.cloneNode(true);
            clonedTable.appendChild(clonedTable1);
    var ctx = {worksheet: name || 'Worksheet', table: clonedTable.innerHTML}
    //window.location.href = uri + base64(format(template, ctx))
     var link = document.createElement('a');
    link.download =filename;
    link.href = uri + base64(format(template, ctx));
    link.click();
     clonedTable.remove();
  }
})()
</script>
<script>

	
$(document).ready(function(){
$('[data-toggle="tooltip"]').tooltip();

$("#search_state").trigger('change');
$('#year').change(function(){
	var count=0;
	<?php  $count=0; ?>;
	var options='<option value="">Select</option>';
	var currentYear = (new Date).getFullYear();
	if (currentYear==$('#year').val()) {
			<?php
		$flag=0;
		$count=date("m");
	for ($i = 1; $i <=$count;$i++) {
	$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
	if($this->input->post('month')==$i){ 	?>
	options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php $flag=1;
	}	
	elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php }
	else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	}
	else{
		<?php
		$flag=0;
		$count=12;
		for ($i = 1; $i <=$count;$i++) {
		$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
		if($this->input->post('month')==$i){ 	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php $flag=1;
		}	
		elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php }
		else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	
	}
	//alert(options);
$('#month').html(options);
});
$("#year").trigger('change');

});
$('#search_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');
					//$('#input_district').val($('#input_district').val());
				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
					$('#mstfacilitylogin').html(data);
					
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

</script>