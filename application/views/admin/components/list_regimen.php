<style type="text/css">

.drug_row
{
	margin-bottom: 10px;
}
</style>

<br>
<?php 
if($this->session->flashdata('msg'))
{
	echo '<div class="alert alert-success alert-dismissible show" role="alert">
	<p>'.$this->session->flashdata('msg').'
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</button></p>
	</div>';
}

if($this->session->flashdata('er_msg'))
{
	echo '<div class="alert alert-danger alert-dismissible show" role="alert">
	<p>'.$this->session->flashdata('er_msg').'
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</button></p>
	</div>';
}

?>

<div class="row main-div" style="min-height:448px;">
	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="text-center" style="color : white;">Regimen List <a href="<?php echo site_url("regimen/add")?>" style="color: white; cursor: pointer;"><i class="fa fa-plus-circle pull-right"  data-toggle="tooltip" title="Add Facility"></i></a></h4>
			</div>
			<div class="panel-body">
				<table id="data-table-command" class="table table-striped table-bordered table-vmiddle table-hover">
					<thead>
						<tr>
							<th class="text-center">Sno.</th>
							<th class="text-center">Regimen Name</th>
							<!-- <th class="text-center">Commands</th> -->
						</tr>

					</thead>
					<tbody id="table_regimen_list">
						<?php 
						$sno = 1;
						foreach ($regimens as $regimen) {
							?>
							<tr>
								<td class="text-center"><?php echo $sno; ?></td>
								<td class="text-left"><?php echo $regimen->regimen_name; ?></td>
								<!-- <td class="text-center">
									<a href="<?php echo site_url('regimen/index/'.$regimen->id_mst_regimen);?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
									<a href="<?php echo site_url('regimen/delete/'.$regimen->id_mst_regimen);?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
								</td> -->
							</tr>
							<?php 
							$sno++;
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="text-center" style="color : white;">Add Regimen</h4>
			</div>
			<div class="panel-body">
				<form method="post" action="regimen/add">
					
					<div class="row">
						<div class="col-md-2 col-md-offset-2">
							<label>Regimen Name</label>	
						</div>
						<div class="col-md-5">
							<?php
							 $d = ucfirst(substr($Regimenname[0]->regimen_name, 0,4));
							for ($n=0; $n<1; $n++) {
								$total =  ++$d;
							}							
							?>
												
							<input type="text" class="form-control" id="new_regimen" name="new_regimen" readonly value="<?php echo  $total; ?>: " required>
							<input type="hidden" id="hidden_new_regimen" value="<?php echo $total;?>">
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-4 col-md-offset-2">
							<a class="btn btn-primary" id="add_drug" onclick="add_row()"><i class="fa fa-plus"></i> Add Drug To Regimen</a>
						</div>
					</div>
					<br>
					<div id="drug_section"></div>

					<div class="row">
						<div class="col-md-12 text-center">
							<br>
							<input type="submit" name="" value="Submit" class="btn btn-warning">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	var drugs = 0;

	function add_row()
	{

		var drug_options = '<?php echo $drugs; ?>';
		if($('.drug_row').length > 4)
		{
			alert('You cannot add so many drugs to the regimen.');
		}
		else
		{
			$('#drug_section').append('<div class="row drug_row">\
				<div class="col-md-6 col-md-offset-2">\
				\
				<div class="row" style="border : 1px solid lightgray; padding: 10px;">\
				<div class="col-md-3">\
				<label>Drug</label>\
				</div>\
				<div class="col-md-6">\
				<select class="form-control drug" id="mySelect" onchange="myFunction()" name="drugs['+drugs+'][medicine]" required>\
				<option value="">--Select--</option>\
				'+drug_options+'\
				</select>\
				</div>\
				<div class="col-md-2" style="display:none;">\
				<label>Strength</label>\
				</div>\
				<div class="col-md-4" style="display:none;">\
				<select class="form-control strength" name="drug['+drugs+'][strength]">\
				<option value="">--Select--</option>\
				</select>\
				</div>\
				<div class="col-md-1 text-center">\
				<i class="fa fa-times fa-2x" onclick="remove_row(this)" style="cursor: pointer"></i>\
				</div>\
				</div>\
				</div>\
				</div>');

			drugs++;
			console.log(drugs);
		}

	}

	function remove_row(elem)
	{

		$(elem).closest('.drug_row').remove();
	}

	function fill_strength(elem)
	{
		$.ajax({
			method : 'POST',
			url : '<?php echo base_url(); ?>regimen/getStrengths/'+$(elem).val(),
		})
		.done(function(options){
			$(elem).parent().siblings().find('.strength').html(options)
		});
	}
</script>

<script>
function myFunction() 
{
	console.log('inside myFunction');
	// get the textbox content first 
	var str = $('#hidden_new_regimen').val();

	var drugArr = [];
	$('.drug').each(function(i,d){
		// str = str + ' + ' + $(d).find('option:selected').text();
		drugArr.push($(d).find('option:selected').text());
	});

	$('#new_regimen').val(str + ': ' + drugArr.join(' + '));
}
</script>