	<style>
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
    	padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
		.form-horizontal .control-label{
    text-align: left;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
</style>
<?php 

 $loginData = $this->session->userdata('loginData');
 //print_r($loginData); ?>
	<div class="row main-div" style="min-height:400px;">
		<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">

			<div class="panel panel-default">

				<div class="panel-heading">
					<h4 class="text-left" style="color : white;">Upload Excel List <a href="<?php echo site_url("users")?>" style="color: white; cursor: pointer;"><i class="fa fa-plus-circle pull-right"></i></a></h4>
				</div>
				<div class="panel-body set_height">
					<table id="data-table-command" class="table table-striped table-bordered table-vmiddle table-hover">
						<thead>
							<tr>
								<th class="text-center">File</th>
								<th class="text-center">Date</th>
								
							</tr>

						</thead>
						<tbody>
							<?php $i=0; foreach($filedata as $row){ $i++;?>
							<tr>

								<td class="text-center" style="padding-top: 16px;"><a href="<?php echo base_url(); ?><?php echo $row->excel_file; ?>" download>Files <?php echo $i; ?> </td>
									<td class="text-center" style="padding-top: 16px;"><?php echo $row->date; ?></td>
								
								</td>

							</tr>

							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
		<div class="panel panel-default">

		<div class="panel-heading">
			<h4 class="text-left" style="color: white;" id="form_head">Upload Excel file</h4>
		</div>
		<div class="row">
<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){  ?>
			

			<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				
				<?php } ?>
				</div>
		<div class="panel panel-body ">

							<?php
          /* $attributes = array(
              'id' => 'users',
              'name' => 'users',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes);*/ ?>
           <form  id="users" method="post" enctype="multipart/form-data">


			<div class="row">
					
				<div class="col-xs-4 text-left">
					<label for="full_name">Upload Excel file(Patient Information)<span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
					<input type="file" class="form-control messagedata" id="thumbnail_image" name="thumbnail_image"   required>
				</div>
				
					

<div class="row">
			
		
			</div>
<br>
				<?php //if($loginData->RoleId == '1' || $loginData->RoleId == 0 || $loginData->id_tblusers==$id_user){ ?>
				<div class="row">
					<div class="col-xs-12 text-center">
						<button type="submit" class="btn btn-warning" id="submit_btn">Submit</button>
					</div>
				</div>
			<?php //} ?>
			 <?php echo form_close(); ?>

		</div>
	</div>
</div>

	</div>

	


