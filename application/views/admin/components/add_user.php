<div class="row main-div" style="min-height:448px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="panel panel-default">

		<div class="panel-heading">
			<h4 class="panel-title text-center" style="color: white;"><b>Add New User</b></h4>
		</div>

		<div class="panel panel-body ">
			<form role="form" method="post" action="">
			
				<div class="panel-body">
					<label for="username">Username</label>
					<input type="text" class="form-control" id="username" name="username" placeholder="Username" value="" required>
				</div>


				<div class="panel-body">
					<label for="password">Password</label>
					<input type="password" class="form-control" id="password" name="password" placeholder="Password" value="" required>
				</div>

				<div class="panel-body">
					<label for="repassword">Re-type Password</label>
					<input type="password" class="form-control" id="repassword" name="repassword" placeholder="Re-type Password" value="" required>
				</div>

				<div class="panel-body">
					<label for="email">Email</label>
					<input type="email" class="form-control" id="email" name="email" placeholder="Email" value="" required>
				</div>

				<div class="panel-body">
					<label for="mobile">Mobile</label>
					<input type="mobile" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile" value="" required>
				</div>

				<div class="panel-body">
					<label for="id_mstfacility">Facility</label>
					<select class="selectpicker form-control" data-toggle="dropdown" id="id_mstfacility" name="id_mstfacility" required>
						<option value="">--Select--</option>
						<?php foreach($Facility as $row){?>
						<option value="<?php echo $row->id_mstfacility;?>" <?php echo (trim($this->input->post('id_mstfacility')) == $row->id_mstfacility?"selected":"");?>><?php echo $row->facility_short_name;?></option>
						<?php } ?>
					</select>

				</div>

				<div class="panel-body select">
					<label for="usertype">Select Usertype</label>
					<br>
					<select name="usertype" id="usertype" class="form-control" required>
						<option value="">--Select--</option>
						<option value="1">Admin</option>
						<option value="2">State User</option>
					</select>
				</div>

				<div style="text-align:center;">
					<button type="submit" class="btn btn-warning">Submit</button>
				</div>
			</form>

		</div>
	</div>
</div>
</div>