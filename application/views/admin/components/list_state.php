<?php 
$loginData = $this->session->userdata('loginData');
?>
<style type="text/css">
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: top;
		background-color: #085786 !important;
		color: white;
	}
	td{
		text-align: center; 
		vertical-align: middle;
	}
	table.dataTable thead tr {
  background-color: #085786 !important;
  color:white;
  height: 25px;
}
.form-control{
height: 35px;
}
</style>
<div class="row main-div" style="min-height:448px;">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default">

					<div class="panel-heading" style="color : white;background-color: #333333;">
						<h4 class="text-center" style="color : white;background-color: #333333;">State Wise Lead Time List </h4>
					</div><br>
					<div class="row" style="padding-top: 32px;">
					<?php 
					$tr_msg= $this->session->flashdata('tr_msg');
					$er_msg= $this->session->flashdata('er_msg');
					if(!empty($tr_msg)){  ?>
						

						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						<?php } else if(!empty($er_msg)){ ?>
							<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
							<?php } ?>
						</div>
					<div class="panel-body">
						<table id="data-table-command" class="table table-striped table-bordered table-vmiddle table-hover">
							<thead>
								<tr>
									<th class="text-center">State Name</th>
									<th class="text-center">Drugs Lead time in months</th>
									<th class="text-center">Screening kits Lead time in months</th>
									<th class="text-center">HBIG Lead time in months</th>
									<?php if($loginData->user_type == 1){ ?>
									<th class="text-center">Commands</th>
								<?php }?>
								</tr>

							</thead>
							<tbody>
								<?php foreach($state_list as $row){ ?>
								<tr>
									<td class="text-left"><?php echo $row->StateName; ?></td>
									<td class="text-center"><?php echo explode(",",$row->lead_time)[0]; ?></td>
									<td class="text-center"><?php echo explode(",",$row->lead_time)[1]; ?></td>
									<td class="text-center"><?php echo explode(",",$row->lead_time)[2]; ?></td>
									<?php if($loginData->user_type == 1){ ?>
									<td class="text-center">

										<a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModalCenter" onclick="get_lead_time_data('<?php echo $row->id_mststate; ?>');" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 20px; margin-top: 8px;"></span></a>
										<!-- <a href="<?php echo site_url('
										state/delete/'.$row->id_mststate);?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 20px; margin-top: 8px;"></span></a> -->
									</td>
									<?php } ?>
								</tr>

								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			</div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">Edit Lead Time</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <?php  $attributes = array(
              'id' => 'AddStock_indent_form',
              'name' => 'AddStock_indent_form',
               'autocomplete' => 'off',
            );  echo form_open('State_lead_time/edit', $attributes); ?>
				
					<div class="panel-body">
						<label for="stateCd">State Code</label>
						<input type="text" class="form-control" id="stateCd" name="stateCd" placeholder="Enter  Sate Code" readonly>
					</div>

					<div class="panel-body">
						<label for="stateName">State Name</label>
						<input type="text" class="form-control" id="stateName" name="stateName" placeholder="Enter state Name" readonly>
					</div>

					<div class="panel-body">
						<label for="lead_time">Drugs Lead time (in months) from State warehouse to facility</label>
						<input type="number" class="form-control" id="lead_time_drug" name="lead_time_drug" placeholder="Enter Lead Time">
					</div>			
      <div class="panel-body">
						<label for="lead_time">Screening kits Lead time (in months) from State warehouse to facility</label>
						<input type="number" class="form-control" id="lead_time_kit" name="lead_time_kit" placeholder="Enter Lead Time">
					</div>			
      <div class="panel-body">
						<label for="lead_time">HBIG Lead time (in months) from State warehouse to facility</label>
						<input type="number" class="form-control" id="lead_time_hbig" name="lead_time_hbig" placeholder="Enter Lead Time">
						<input type="hidden" id="id_mststate" name="id_mststate" placeholder="Enter Lead Time">
					</div>			
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
         <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
		<!-- Data Table -->
		<script type="text/javascript">
			$(document).ready(function(){
//Command Buttons
$("#data-table-command").dataTable();

});
function get_lead_time_data(id_mststate)
{
			$.ajax({
        url: "<?php echo site_url('State_lead_time/get_lead_details');?>",
        data: {id_mststate: id_mststate,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'}, // change this to send js object
        type: "GET",
        success: function(data){
           //document.write(data); just do not use document.write
            var returned = JSON.parse(data);
            $("#id_mststate").val(returned.State_Details.id_mststate);
            $("#lead_time_drug").val(returned.State_Details.lead_time.split(",")[0]);
            $("#lead_time_kit").val(returned.State_Details.lead_time.split(",")[1]);
            $("#lead_time_hbig").val(returned.State_Details.lead_time.split(",")[2]);
            $("#stateName").val(returned.State_Details.StateName);
            $("#stateCd").val(returned.State_Details.StateCd);
        }
      });
		}
</script>