<style>
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
    	padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData'); ?>
<!-- add/edit facility contact modal starts-->
<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Facility Contact</h4>
      </div>
      <div class="modal-body">
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Contact Name</label>
        		</div>
        		<div class="col-xs-12 col-lg-6">
					<input type="text" class="form-control" placeholder="Contact Name" name="contact_name" id="contact_name" required>
        		</div>
        	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="facility_contact_save">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- add/edit facility contact modal ends-->

	<div class="row main-div" style="min-height:400px;">
		<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">

			<div class="panel panel-default">

				<div class="panel-heading">
					<h4 class="text-center" style="color : white;">Facility List <a href="<?php echo site_url("facilities")?>" style="color: white; cursor: pointer;"><i class="fa fa-plus-circle pull-right"  data-toggle="tooltip" title="Add Facility"></i></a></h4>
				</div>
				<div class="panel-body set_height">
					<table id="data-table-command" class="table table-striped table-bordered table-vmiddle table-hover">
						<thead>
							<tr>
								<th class="text-center">Facility Name</th>
								<th class="text-center">Commands</th>
							</tr>

						</thead>
						<tbody id="table_facility_list">
							<?php foreach($facility_list as $row){ ?>
							<tr>

								<td class="text-center" style="padding-top: 16px;"><?php echo $row->facility_short_name; ?></td>
								<td class="text-center">
									<a href="<?php echo site_url('facilities/index/'.$row->id_mstfacility);?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
									<a href="#<?php //echo site_url('facilities/delete/'.$row->id_mstfacility);?>" class="usersdelete" id="<?php echo $row->id_mstfacility; ?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
								</td>

							</tr>

							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
		<div class="row">
			<div class="col-lg-12">

		
				
		<div class="panel panel-default">

		<div class="panel-heading">
			<h4 class="text-center" style="color: white;" id="form_head">Add Facility</h4>
		</div>
<div class="row">
<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){  ?>
			

			<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				
				<?php } ?>
</div>
		<div class="panel panel-body ">
			<?php
           $attributes = array(
              'id' => 'Facilityform',
              'name' => 'Facilityform',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

			<div class="row">
					
			
				<div class="col-xs-2 text-left">
				<label for="full_name">Facility Name <span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
					<input type="text" class="form-control" id="facility_short_name" name="facility_short_name" placeholder="(e.g. AIIMS Delhi, MAMC Delhi)" value="<?php if(isset($facility_details->facility_short_name)){ echo $facility_details->facility_short_name; }?>" required>
				</div>

					<div class="col-xs-2 text-left">
					<label for="facility_type">Facility Type <span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
				<select name="facility_type" id="facility_type" required class="form-control">
					<option value="">Select</option>
					<option value="DH" <?php if(isset($facility_details->FacilityType)) { if($facility_details->FacilityType == "DH"){ echo 'selected';} } ?>>District Hospital</option>
					<option value="GMC" <?php if(isset($facility_details->FacilityType)){ if($facility_details->FacilityType == "GMC"){ echo 'selected';} }?>>Govt. Medical College</option>
					<option value="SDH" <?php if(isset($facility_details->FacilityType)){ if($facility_details->FacilityType == "SDH"){ echo 'selected';} }?>>SDH</option>
					<option value="CHC" <?php if(isset($facility_details->FacilityType)){ if($facility_details->FacilityType == "CHC"){ echo 'selected';} }?>>CHC</option>
					<option value="PHC" <?php if(isset($facility_details->FacilityType)){ if($facility_details->FacilityType == "PHC"){ echo 'selected';} }?>>PHC</option>
					<option value="APHC" <?php if(isset($facility_details->FacilityType)){ if($facility_details->FacilityType == "APHC"){ echo 'selected';} }?>>APHC</option>

					<option value="HWC" <?php if(isset($facility_details->FacilityType)){ if($facility_details->FacilityType == "HWC"){ echo 'selected';} }?>>HWC</option>
					<option value="DIS" <?php if(isset($facility_details->FacilityType)){ if($facility_details->FacilityType == "DIS"){ echo 'selected';} }?>>Dispensary</option>
					<option value="U-PHC" <?php if(isset($facility_details->FacilityType)){ if($facility_details->FacilityType == "U-PHC"){ echo 'selected';} }?>>U-PHC</option>
					<option value="U-CHC" <?php if(isset($facility_details->FacilityType)){ if($facility_details->FacilityType == "U-CHC"){ echo 'selected';} }?>>U-CHC</option>
					<option value="TRS" <?php if(isset($facility_details->FacilityType)){ if($facility_details->FacilityType == "TRS"){ echo 'selected';} }?>>Tertiary Site</option>

					<?php  if($loginData->State_ID == 3 || $loginData->State_ID == 6 ){ ?>
					<option value="ART" <?php if(isset($facility_details->FacilityType)){ if($facility_details->FacilityType == "ART"){ echo 'selected';} }?>>ART Centre</option>
					<option value="OST" <?php if(isset($facility_details->FacilityType)){ if($facility_details->FacilityType == "OST"){ echo 'selected';} }?>>OST Centre</option>
					<option value="CJ" <?php if(isset($facility_details->FacilityType)){ if($facility_details->FacilityType == "CJ"){ echo 'selected';} }?>>Central Jail</option>
					<option value="SJ" <?php if(isset($facility_details->FacilityType)){ if($facility_details->FacilityType == "SJ"){ echo 'selected';} }?>>State Jail</option>
				<?php } ?>

				</select>
					
				</div>
				<input type="hidden" name="FacilityTypeNum" id="FacilityTypeNum" value="$facility_details->FacilityTypeNum">
			</div>

<br>
<div class="row">

	<div class="col-xs-2 text-left state_districts" >
					<label for="email">Service Delivery Level <span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4 state_districts">
					<select name="delivery_level" id="delivery_level" class="input_fields form-control" required="required">
								<option value="">Select</option>
								<option value="0" <?php if(isset($facility_details->is_Mtc)) { if($facility_details->is_Mtc == "0"){ echo 'selected';} } ?>>TC</option>
								<option value="1" <?php if(isset($facility_details->is_Mtc)) { if($facility_details->is_Mtc == "1"){ echo 'selected';} } ?>>MTC</option>
							</select>
				</div>

<div class="col-xs-2 text-left state_districts" >
					<label for="email">State<span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4 state_districts">
					<select name="input_state" id="input_state" class="input_fields form-control" required="required">
								<option value="">Select State</option>
								<?php 
								foreach ($states as $state) {
									?>
									<option value="<?php echo $state->id_mststate; ?>" <?php if(isset($facility_details->id_mststate)){ 
									if($facility_details->id_mststate == $state->id_mststate) { echo 'selected';} } elseif ($loginData->State_ID == $state->id_mststate) {
										echo 'selected';
									} ?>><?php echo ucwords(strtolower($state->StateName)); ?></option>
									<?php 
								}
								?>
							</select>
				</div>

	</div>
	<br>

			<div class="row">
					
						<div class="col-xs-2 text-left">
					<label for="full_name">District <span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
					<select name="district" id="district" class="form-control" required>
						<option value="">Select</option>
						<?php foreach ($district_list as $district) { ?>
							
							<option value="<?php echo $district->id_mstdistrict; ?>" <?php if(isset($facility_details->id_mstdistrict)){ if($district->id_mstdistrict == $facility_details->id_mstdistrict){ echo  'selected'; } }?> ><?php  echo $district->DistrictName; ?></option>
						<?php } ?>
					</select>
				</div>

				<div class="col-xs-2 text-left">
					<label for="parent_facility">Parent Facility <span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
					<select name="parent_facility" id="parent_facility" class="form-control" required>
						<option value="">Select</option>
					</select>
				</div>

				

			
			</div>
<input type="hidden" name="countoffacility" id="countoffacility">
<br>

<div class="row">
					
				<div class="col-xs-2 text-left">
					<label for="AddressLine1">Address 1:<span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
					<textarea class="form-control" style="width: 98%;" id="AddressLine1" name="AddressLine1" placeholder="Enter complete address" required><?php if(isset($facility_details->AddressLine1)){ echo $facility_details->AddressLine1; }?></textarea>
					<!-- <input type="text" class="form-control" style="width: 98%;" id="AddressLine1" name="AddressLine1" placeholder="Address" value="<?php //if(isset($facility_details->AddressLine1)){ echo $facility_details->AddressLine1; }?>"> -->
				</div>

				<div class="col-xs-2 text-left">
					<label for="AddressLine1">Address 2:<span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
					<textarea class="form-control" style="width: 98%;" id="AddressLine2" name="AddressLine2" placeholder="Enter complete address" required><?php if(isset($facility_details->AddressLine2)){ echo $facility_details->AddressLine2; }?></textarea>
					<!-- <input type="text" class="form-control" style="width: 98%;" id="AddressLine1" name="AddressLine1" placeholder="Address" value="<?php //if(isset($facility_details->AddressLine1)){ echo $facility_details->AddressLine1; }?>"> -->
				</div>


					
			</div>



<br>
<div class="row">
					

			<div class="col-xs-2 text-left">
					<label for="City">City <span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
					<input type="text" class="form-control" id="City" name="City" placeholder="City" value="<?php if(isset($facility_details->City)){ echo $facility_details->City; } ?>" required>
				</div>

				<div class="col-xs-2 text-left">
					<label for="PinCode">Pin <span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
				<input type="text" class="form-control" id="PinCode" maxlength="6" minlength="6" name="PinCode" placeholder="PIN" value="<?php if(isset($facility_details->PinCode)){ echo $facility_details->PinCode; } ?>"  onkeypress="return isNumberKey(event)" pattern="[1-9][0-9]{5}" required="">
				</div>


			</div>

<br>
<div class="row">


				<div class="col-xs-2 text-left">
				<label for="full_name">Nodal Person <span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
					<input type="text" class="form-control" id="nodal_person" name="nodal_person" placeholder="Nodal Person" value="<?php if(isset($facility_details->nodal_person)){ echo $facility_details->nodal_person; }?>" required>
				</div>

					
				<div class="col-xs-2 text-left">
					<label for="Phone1">Phone</label>
				</div>
				<div class="col-xs-4">
					<input type="text" class="form-control" id="Phone1" maxlength="10" minlength="10" name="Phone1" placeholder="Phone" value="<?php if(isset($facility_details->Phone1)){ echo $facility_details->Phone1;} ?>" onkeypress="return isNumberKey(event)">
				</div>
			
			</div>

<br>
<div class="row">
	<div class="col-xs-2 text-left">
					<label for="facility_code">Facility Code <span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
					<input type="text" class="form-control" id="facility_code" name="facility_code" placeholder="Facility Code" value="<?php if(isset($facility_details->FacilityCode)){ echo $facility_details->FacilityCode;} ?>" required readonly>
				</div>
	<div class="col-xs-2 text-left">
					<label for="email">Facility Email / Facility Nodal Officer Email</label>
				</div>
				<div class="col-xs-4">
				<input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php if(isset($facility_details->email)){ echo $facility_details->email; } ?>">
				</div>

	</div>			


				<div class="row">
					<div class="col-xs-12 text-center">
						<button type="submit" class="btn btn-warning" id="submit_btn">Add New Facility</button>
					</div>
				</div>
			</form>
<br>
<!-- 	<div class="row">
		<div class="col-lg-12">
			<table class="table table-hover table-striped table-bordered">
			<thead>
				<th class="text-left">Facility Contacts</th>
				<th class="text-center">Commands <span style="color: gray; cursor: pointer;font-size: 18px;
"><i class="fa fa-plus-circle pull-right" title="Add Facility Contact" data-toggle="tooltip" id="contact_modal"></i></span></th>
			</thead>
			<tbody id="facility_contact_body">
				<?php if(isset($facility_contact)){
					foreach ($facility_contact as $row) {
						?>
						 <tr>
						 <td class="text-center"><?php echo $row->Name; ?></td>
						 <td class="text-center">
							<a href="#" style="padding : 4px;" title="Edit" data-edit_contact_id = "<?php echo $row->id_mstfacilitycontacts; ?>"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
							<a href="#" style="padding : 4px;" title="Delete" data-delete_contact_id = "<?php echo $row->id_mstfacilitycontacts; ?>"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
						</td>
						 </tr>
						
					<?php  } }
				 ?>
			</tbody>
		</table>
		</div>
	</div> -->
		</div>
	</div>
</div>
</div>

</div>

	</div>
	<div class="overlay">
	<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
</div>

	<!-- Data Table -->
<script type="text/javascript">

$('.usersdelete').click(function(){

var idval = $(this).attr('id');

            $.ajax({
               url : "<?php echo base_url().'facilities/delete/'; ?>"+idval,
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'POST',
                success : function(data)
                {
                   //location.href= '<?php echo base_url().'facilities/'; ?>'; 
                },
                error : function(error)
                {
                    alert('Error Fetching Districts');
                }
            })
        });

function isNumberKey(evt)
	{
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;

	
		if(evt.srcElement.id == 'Phone1' && $('#Phone1').val() > 999999999)
		{
			$("#modal_header").html('Contact number must be 10 digits');
			$("#modal_text").html('Please fill in appropriate Contact Number');
			$("#multipurpose_modal").modal("show");
			return false;
		}
	
		if(evt.srcElement.id == 'PinCode' && $('#PinCode').val() > 99999)
		{
			$("#modal_header").html('PIN cannot be more than 6 digits');
			$("#modal_text").html('Please fill in appropriate PIN');
			$("#multipurpose_modal").modal("show");
			return false;
		}

		return true;
	}

$('#input_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#district').html(data);
					//$("#input_district").trigger('change');

				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

$('#district').change(function(){
createfacilityname();

});

$('#delivery_level').change(function(){
	if($('#district').val()!=""){
createfacilityname();
}
});

$('#facility_type').change(function(){
	if($('#district').val()!=""){
createfacilityname();
}
});


	function createfacilityname(){

	var delivery_level = $("#delivery_level option:selected").text();
var facility_type = $('#facility_type').val();
var districtname = $("#district option:selected").text();
var district = $('#district').val();
var delivery_levelid = $("#delivery_level").val();

$.ajax({
				url : "<?php echo base_url().'users/getDistrictscode/'; ?>"+district+'/'+delivery_levelid,
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				dataType: 'json',
				success : function(data)
				{
					if(data.status == 'true'){
						var parsedData = JSON.parse(data.fields);


	var fac_code = parsedData.DistrictCd;
	var mtctcval = parsedData.is_Mtc;
	if(mtctcval==0 && delivery_levelid==0){
		
		var totalismtcval= parseInt(mtctcval)+1;
	}else{
		
	var totalismtcval= parseInt(mtctcval);
}

	if(mtctcval==0 && delivery_levelid==1){
		
		var totalismtcval= parseInt(mtctcval)+1;
	}else{
		
	var totalismtcval= parseInt(mtctcval)+1;
}

if($("#delivery_level").val()!='' && $("#district").val()!='' && $("#facility_type").val()!=''){
$('#facility_code').val(fac_code+'-'+facility_type+'-'+delivery_level+totalismtcval);

$('#countoffacility').val(totalismtcval);
}


}
				},
				error : function(error)
				{
					//alert('Error Fetching Districts');
				}
			
			})


}



$('#facility_type').change(function(){
	var delivery_level = $("#delivery_level option:selected").text();
var facility_type = $('#facility_type').val();
var districtname = $("#district option:selected").text();
var fac_code = districtname.substring(0, 3);

if($("#delivery_level").val()!='' && $("#district").val()!='' && $("#facility_type").val()!=''){
$('#facility_code').val(fac_code+'-'+facility_type+'-'+delivery_level);
}
$.ajax({
               url : "<?php echo base_url().'facilities/facility_typenum'; ?>",
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',facilitytype : facility_type
				},
				method : 'GET',
                success : function(data)
                {
                    $('#FacilityTypeNum').val(data);
                    }
                });
if ($('#district').val()>0) 
$('#district').trigger('change');
});

$('#delivery_level').change(function(){
var delivery_level = $("#delivery_level option:selected").text();
var facility_type = $('#facility_type').val();
var districtname = $("#district option:selected").text();
var fac_code = districtname.substring(0, 3);

if($("#delivery_level").val()!='' && $("#district").val()!='' && $("#facility_type").val()!=''){
$('#facility_code').val(fac_code+'-'+facility_type+'-'+delivery_level);
}
});




		var contact_edit_flag = 0;
		var contact_id = 0;
		var edit_flag = <?php echo $edit_flag; ?>;
		//$(".alert").hide();
	$(document).ready(function(){

		$("#table_facility_list .glyphicon-trash").click(function(e){
			var ans = confirm("Are you sure you want to delete?");
			if(!ans)
			{
				e.preventDefault();
				return false;
			}
		});

		if(edit_flag == 1)
		{
			$("#form_head").text("Edit Facility");
			$("#submit_btn").text("Update Facility");
			$("#district").trigger("change");
			
		}

		$("#contact_modal").click(function(){

			if(edit_flag == 0)
			{
				$(".alert.alert-warning").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Please select facility to edit first');
				$(".alert.alert-warning").show();
			}
			else
			{
				contact_id = 0;
				contact_edit_flag = 0;
				$("#contact_name").val('');
				$("#myModal").modal('show');
			}
		});

		$('#myModal').on('shown.bs.modal', function () {
		    $("#contact_name").focus();
		});  

		//registerHandlers();


		/*$("#facility_contact_save").click(function(){

			$.ajax({
				url : "<?php //echo site_url(); ?>facilities/add_facility_contact_details/<?php echo $facility_details->id_mstfacility; ?>",
				method : "post",
				data : {
					"name" : $("#contact_name").val(),
					"contact_edit_flag" : contact_edit_flag,
					"contact_id" : contact_id,
				},
				success : function(data)
				{	
					$("#myModal").modal('hide');
					$(".alert").hide();
					var data_parsed = JSON.parse(data);
					console.log(data_parsed);
					// alert("Facility Contact Saved successfully");
					$("#facility_contact_body").html("");

					for (var i = 0; i < data_parsed.length; i++) {
						$("#facility_contact_body").append('<tr>\
						 <td class="text-center">'+data_parsed[i].Name+'</td>\
						 <td class="text-center">\
							<a href="#" style="padding : 4px;" title="Edit" data-edit_contact_id = "'+data_parsed[i].id_mstfacilitycontacts+'"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>\
							<a href="#" style="padding : 4px;" title="Delete" data-delete_contact_id = "'+data_parsed[i].id_mstfacilitycontacts+'"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>\
						</td>\
						 </tr>');
					}

					if(contact_edit_flag == 0)
					{
						$(".alert.alert-success").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> New Contact Added Successfully');
						$(".alert.alert-success").show();
					}
					else if(contact_edit_flag = 1)
					{
						$(".alert.alert-success").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> Contact Updated Successfully');
						$(".alert.alert-success").show();
					}

					registerHandlers();
				},
				error : function(error)
				{
					console.log('error ' + error);
					// alert("There was an error fetching this record.");
					if(contact_edit_flag == 0)
					{
						$(".alert.alert-danger").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> New Contact Add Failed');
						$(".alert.alert-danger").show();
					}
					else if(contact_edit_flag = 1)
					{
						$(".alert.alert-danger").html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> Update Contact Failed');
						$(".alert.alert-danger").show();
					}
				},
				beforeSend : function(){
					$(".overlay").css("z-index","999");
					$(".overlay").css("display","block");
				},
				complete : function(){
					$(".overlay").css("z-index","-1");
					$(".overlay").css("display","none");
				},
					});
		});*/	

$('#facility_short_name,#City').keypress(function (e) {
var regex = new RegExp("^[a-zA-Z ]*$");
var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
if (e.which === 32 && (this.value.length==0 || e.target.selectionStart==0)) {
          e.preventDefault();
          return false;
      }

else if (regex.test($.trim(str))) {
var error_css = {"border":"1px solid #c7c5c5"};
$(this).css(error_css);
return true;

}
else{
e.preventDefault();
return false;
}

});

$('#submit_btn').click(function(e){
var error_css = {"border":"1px solid red"};


if($('#facility_short_name').val().trim() == ""){

$("#facility_short_name").css(error_css);
$("#facility_short_name").focus();
e.preventDefault();
return false;
}

else if($('#City').val().trim()==""){

$("#City").css(error_css);
$("#City").focus();
e.preventDefault();
return false;
}

else if($('#PinCode').val().trim==""){

$("#PinCode").css(error_css);
$("#PinCode").focus();
e.preventDefault();
return false;
}
else{
return true;
}

});
	});

	$('#district').change(function(){

var val = $(this).val();
var facilitytype = $('#facility_type').val();
//alert(facilitytype);
if (facilitytype!='') {
            $.ajax({
               url : "<?php echo base_url().'facilities/get_facilies/'; ?>"+val,
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',facility_type : facilitytype
				},
				method : 'GET',
                success : function(data)
                {
                    var returned = JSON.parse(data);
                        //console.log(returned);
        				var id_mstfacility = returned.map(function(e) {
   						return e.id_mstfacility;
						});
						var facility_short_name = returned.map(function(e) {
   						return e.facility_short_name;
						});
                        var i;
                        var select=''
                        var opt='';
                        opt+='<option value="">--Select--</option>';
                        opt+='<option value="999">State</option>';
                        for(i=0; i<id_mstfacility.length; i++){
                        	
                        		select='';
                        
                            opt += '<option value="'+id_mstfacility[i]+'"'+select+'>'+facility_short_name[i]+'</option>';
                        }
                        //console.log(opt);
                        $('#parent_facility').html(opt); 
                        if (edit_flag==1) {
                        	$("#parent_facility").val("<?php if(isset($facility_details->parent_facility)){ echo $facility_details->parent_facility;}?>");
                        }
                },
                error : function(error)
                {
                    alert('Error Fetching Facilities');
                }
            })
        }
        else
        {
        	alert('Please select facility type');
        }
        });
</script>


