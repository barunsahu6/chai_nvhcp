	<link href="<?php echo site_url('common_libs');?>/css/bootstrap-multiselect.css"
	rel="stylesheet" type="text/css" />
	<script src="<?php echo site_url('common_libs');?>/js/bootstrap-multiselect.js"
	type="text/javascript"></script>
	<style>
		a{
			cursor: pointer;
		}
		.set_height
		{
			max-height: 425px;
			overflow-y: scroll;

		}
		.panel-heading {
			padding: 1px 15px;
		}
		label
		{
			padding-top: 5px;
		}
		.form-horizontal .control-label{
			text-align: left;
		}
		select[readonly] {
			background: #eee;
			pointer-events: none;
			touch-action: none;
		}
	</style>
	<?php 
//echo hash('sha256','NOTR@Kol-GMC-MTC');


	$loginData = $this->session->userdata('loginData');
 //print_r($loginData); ?>
 <div class="row main-div" style="min-height:400px;">
 	<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">

 		<div class="panel panel-default">

 			<div class="panel-heading">
 				<h4 class="text-left" style="color : white;">Template list<a href="<?php echo site_url("Linelist_masters")?>" style="color: white; cursor: pointer;"><i class="fa fa-plus-circle pull-right"></i></a></h4>
 			</div>
 			<div class="panel-body set_height">
 				<table id="data-table-command" class="table table-striped table-bordered table-vmiddle table-hover">
 					<thead>
 						<tr>
 							<th class="text-center">ID</th>
 							<th class="text-center">Template Name</th>
 							<th class="text-center">Action</th>
 						</tr>

 					</thead>
 					<tbody>
 						<?php $i=0; foreach ($tempname as $row) { $i++;?>
 							<tr>
 								<td><?php echo $i;?></td>
 								<td><?php echo $row->template_name;?></td>
 								<td class="text-center"><a href="javascript:;" onclick="myFunction(<?=$row->masterId?>)" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a></td>
 							</tr>
 						<?php } ?>
 					</tbody>
 				</table>
 			</div>
 		</div>
 	</div>

 	<div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
 		<div class="panel panel-default">

 			<div class="panel-heading">
 				<h4 class="text-left" style="color: white;" id="form_head">Line list Template</h4>
 			</div>
 			<div class="row">
 				<?php 
 				$tr_msg= $this->session->flashdata('tr_msg');
 				$er_msg= $this->session->flashdata('er_msg');
 				if(!empty($tr_msg)){  ?>


 					<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
 						<div class="hpanel">
 							<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
 								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
 								<?php echo $this->session->flashdata('tr_msg');?>. </div>
 							</div>
 						</div>
 					<?php } else if(!empty($er_msg)){ ?>
 						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
 							<div class="hpanel">
 								<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
 									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
 									<?php echo $this->session->flashdata('er_msg');?>. </div>
 								</div>
 							</div>

 						<?php } ?>
 					</div>
 					<div class="panel panel-body ">

 						<?php
 						$attributes = array(
 							'id' => 'linelistform',
 							'name' => 'linelistform',
 							'autocomplete' => 'off',
 						);
 						echo form_open('', $attributes); ?>
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
 						<div class="row">

 							<div class="col-xs-2 text-left">
 								<label for="full_name">Category Name<span class="text-danger">*</span></label>
 							</div>
 							<div class="col-xs-4">
 								<select class="form-control" multiple="multiple" required="required" name="Category" id="Category">
 									<?php foreach ($Category_list as $value) { ?>
 										<option value="<?php echo $value->category_Id;?>"><?php echo $value->Category_name;?></option>
 									<?php } ?>

 								</select>
 							</div>


 							<div class="col-xs-2 text-left">
 								<label for="full_name">Template Name<span class="text-danger">*</span></label>
 							</div>

 							<div class="col-xs-4">
 								<input type="text" class="form-control messagedata" required="required" name="Templatename" id="temp_name">
 							</div>
 						</div>

						<input type="hidden" name="tempidval" id="tempidval" value="">
 						<div class="panel-body " >

 							<div class="row showitem" >
 								<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 label_list_panel" style="display: none;">
 									<label id="tt">Label list</label>
 									<select class="label_list_show form-control" style="height: 300px;" multiple="multiple"  name="label" id="label">
 									<!-- <?php   foreach ($label_list as $value) { ?>
 										<option value="<?php echo $value->lcategoryId;?>"><?php echo $value->label_name;?></option>
 										<?php } ?> -->
 									</select>
 								</div>


 								<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 text-center label_list_panel" style="display: none; " style="margin-top: 40px;">
 									<div class="row vcenter"> 
 										<button  data-toggle="tooltip" style="margin-top: 145%;" Title ="Click here to add selected staff for recruitment team member." class ="btn btn-success" type="button" id="btnAdd">></button>
 									</div>
 									<div class="row" style="margin-top: 20px; "> 
 										<button data-toggle="tooltip"  Title ="Click here to remove staff from recruitment team member." class ="btn btn-danger"  type="button" id="btnRemove"><</button>
 									</div>
 								</div>


 								<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 label_list_panel2" style="display: none;" >
 									<label id="tt">Selected Label list</label>

 									<select class="form-control label_list_show2" multiple="multiple" style="height: 300px;"  name="label2[]" id="label2">
 									</select>
 								</div>

 							</div>
 						</div>

 						<div class="col-xs-2 text-left">
 							<label></label>
 							<input type="submit" name="submit" class="btn btn-success" style="margin-left: 305%;" value="Submit">
 						</div>

 					</div>

 					

 					<?php echo form_close(); ?>

 				</div>
 			</div>
 		</div>

 	</div>

 	<script type="text/javascript">
 		function myFunction(id)
 		{
 			$(".label_list_show2").empty();
 			 // alert(id);
 			 $('.label_list_panel2').attr('style', 'display:block');
 			$('#tempidval').val(id);

 			$.ajax({
 				url : '<?php echo site_url('Linelist_masters/label_categorylist');?>',
 				type : 'GET',
 				data: {id: id,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'},					 	
 				success : function(data){
 					
 					var arr = $.parseJSON(data); //convert to javascript array
 					$("#temp_name").val(arr[0].template_name);
 					$.each(arr,function(key,value){
 						// alert(value);
 						console.log(arr[key].label_name);
 						$(".label_list_show2").append(new Option(arr[key].label_name, arr[key].categoryId));
 					});

 				}
 			});       
 		}
 		$(function(){
 			$('#Category').change(function(event){
 				var cat_id = $(this).val();
 				$(".label_list_panel").removeAttr("style");
 				$(".label_list_panel2").removeAttr("style");
 					// alert(cat_id);
 					$.ajax({
 						url : '<?php echo site_url('Linelist_masters/label_namelist');?>',
 						type : 'GET',
 						data: {ids: cat_id,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'},					 	
 						success : function(show){
 							//console.log(show);
 							// alert(show);
 							$(".label_list_show").html(show);
 							// $(".label_list_panel2").html(show);

 						}
 					});                 
 				});

 		});

 function isNumberKey(evt)
	{
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	}
	$('.messagedata').keypress(function (e) {
        var regex = new RegExp(/^[a-zA-Z\s]+$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            return false;
        }
    });		
 	</script>


 	<script type="text/javascript">
 		$(function () {
 			$('#Category').multiselect({
 				includeSelectAllOption: true
 			});
 		});
 	</script>

 	<script type="text/javascript">
 		$(document).ready(function() {
 			$('#btnAdd').click(function(e) {
 				var selectedOpts = $('#label option:selected');
 				if (selectedOpts.length == 0) {
 					alert("Nothing to move.");
 					e.preventDefault();
 				}

 				$('#label2').append($(selectedOpts).clone());
 				$(selectedOpts).remove();
 				e.preventDefault();
 				updateIDs();
 			});

 			$('#btnRemove').click(function(e) {
 				var selectedOpts = $('#label2 option:selected');
 				if (selectedOpts.length == 0) {
 					alert("Nothing to move.");
 					e.preventDefault();
 				}

 				$('#label').append($(selectedOpts).clone());
 				$(selectedOpts).remove();
 				e.preventDefault();
 				updateIDs();
 			});
 		});
 	</script>