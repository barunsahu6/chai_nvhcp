	<style>
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
    	padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
		.form-horizontal .control-label{
    text-align: left;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
</style>
<?php 



echo hash('sha256','Mn@12345');	
//$this->security->xss_clean($RequestMethod);
 //$loginData->user_type;
  $useriddv = $this->uri->segment(3);  
 $loginData = $this->session->userdata('loginData');

 $sql = "SELECT id_tblusers FROM `tblusers` where id_tblusers = ? ";
$udata = $this->db->query($sql,[$loginData->id_tblusers])->result();

 //print_r($udata); ?>
	<div class="row main-div" style="min-height:400px;">
		<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">

			<div class="panel panel-default">

				<div class="panel-heading">
					<h4 class="text-left" style="color : white;">Users List <a href="<?php echo site_url("users")?>" style="color: white; cursor: pointer;"><i class="fa fa-plus-circle pull-right"></i></a></h4>
				</div>
				<div class="panel-body set_height">
					<table id="data-table-command" class="table table-striped table-bordered table-vmiddle table-hover">
						<thead>
							<tr>
								<th class="text-center">Operator Name</th>
								<th class="text-center">User Type</th>
								<th class="text-center">Commands</th>
							</tr>

						</thead>
						<tbody>
							<?php foreach($users_list as $row){ ?>

									<?php if($loginData->RoleId == '99' || $loginData->RoleId == 0 || $loginData->id_tblusers==$row->id_tblusers || $loginData->user_type==1){ ?>
							<tr>

								<td class="text-center" style="padding-top: 16px;"><?php echo $row->Operator_Name; ?></td>
								<td class="text-center" style="padding-top: 16px;">
								<?php 
									switch($row->user_type)
									{
										case 1 : echo "National User";
												break;
										case 2 : echo "Facility User";
												break;
										case 3 : echo "Admin (State User)";
												break;
										case 4 : echo "Districts User";
												break;
									}
										?>
										
									</td>
								<td class="text-center">
								
									<a href="<?php echo site_url('users/index/'.$row->id_tblusers);?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
									<?php if(($loginData->RoleId == '99' || $loginData->RoleId == 0 || $loginData->user_type==1) && $loginData->id_tblusers!= $row->id_tblusers){ ?>
									<a href="#<?php //echo site_url('users/delete/'.$row->id_tblusers);?>" class="usersdelete" id="<?php echo $row->id_tblusers; ?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash"  style="font-size : 15px; margin-top: 8px;"></span></a>
								<?php } ?>
								<?php } ?>
								</td>

							</tr>

							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
		<div class="panel panel-default">

		<div class="panel-heading">
			<h4 class="text-left" style="color: white;" id="form_head">Add User</h4>
		</div>
		<div class="row">
<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){  ?>
			

			<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				
				<?php } ?>
				</div>
		<div class="panel panel-body ">

							<?php
           $attributes = array(
           		'onsubmit' => 'return myFunction()',
              'id' => 'users',
              'name' => 'users',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

			<div class="row">
					
				<div class="col-xs-2 text-left">
					<label for="full_name">Full User Name<span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
					<input type="text" class="form-control messagedata" id="full_name" name="full_name" placeholder="First Name Last Name" value="<?php if(isset($user_details->Operator_Name)){ echo $user_details->Operator_Name;} ?>" required>
				</div>
				
				<div class="col-xs-2 text-left">
					<label for="usertype">User Type<span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
					<select name="usertype" id="usertype" class="form-control" required  <?php if($id_user!=''){ ?> readonly="" <?php } ?>>
						<option value="">--Select--</option>
						<?php  if($loginData->user_type == 1 ){ ?>
						<option value="1" <?php if(isset($user_details->user_type)){ echo (($user_details->user_type == 1)?"selected":"");} ?>>National User</option>
						<option value="3" <?php if(isset($user_details->user_type)){ echo (($user_details->user_type == 3)?"selected":"");} ?>>State User</option>
					<?php } elseif($loginData->RoleId == '99' || $loginData->user_type == 2){ ?>
						<option value="2" <?php if(isset($user_details->user_type)){ echo (($user_details->user_type == 2)?"selected":""); }?>>Facility User</option>
				<?php	}else{?>
						<option value="3" <?php if(isset($user_details->user_type)){ echo (($user_details->user_type == 3)?"selected":"");} ?>>State User</option>
						<option value="5" <?php if(isset($user_details->user_type)){ echo (($user_details->user_type == 5)?"selected":"");} ?>>Lab User</option>
						

						<option value="2" <?php if(isset($user_details->user_type)){ echo (($user_details->user_type == 2)?"selected":""); }?>>Facility User</option>

						<?php } ?>

					</select>
				</div>

			</div>
<br>
<!-- <option value="4" <?php //if(isset($user_details->user_type)){ echo (($user_details->user_type == 4)?"selected":""); }?>>District User</option> -->
<div class="row " >

						<div class="col-xs-2 text-left facility_box">
					<label for="usertype">User role authentication<span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4 facility_box">
					<select name="role_authentication" id="role_authentication" class="form-control" required <?php  if($loginData->user_type == 2 && $id_user!=''){ ?> readonly="" <?php } ?>>
						<option value="">--Select--</option>
						<?php foreach ($mst_role as $value) { ?>
						<option value="<?php echo $value->RoleId;  ?>" <?php if(isset($user_details->RoleId)){
							echo (($user_details->RoleId==$value->RoleId)?"selected":"");}?>><?php echo $value->Role;  ?></option>
					<?php } ?>
					</select>
				</div>

				<div class="col-xs-2 text-left state_districts" >
					<label for="email">State<span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4 state_districts">
					<select name="input_state" id="input_state" class="input_fields form-control" required="required">
								<option value="">Select State</option>
								<?php 
								foreach ($states as $state) {
									?>
									<option value="<?php echo $state->id_mststate; ?>" <?php if(isset($user_details->State_ID)){ 
									if($user_details->State_ID == $state->id_mststate) { echo 'selected';} } elseif ($loginData->State_ID == $state->id_mststate) {
										echo 'selected';
									} ?>><?php echo ucwords(strtolower($state->StateName)); ?></option>
									<?php 
								}
								?>
							</select>
				</div>
		
			</div>
			<br>
			<div class="row " >
			


				<div class="col-xs-2 text-left state_districts_dis" >
					<label for="mobile">District<span class="text-danger">*</span></label>
				</div><?php //echo "<pre>"; print_r($mstdistrict2); ?>
				<div class="col-xs-4 state_districts_dis" >
				<select  name="input_district" id="input_district" class="form-control input_fields" required="required">
						<option value="">Select District</option>
						<?php //if($edit_flag==1){ ?>
							<?php foreach ($mstdistrict2 as  $value) { ?>
							<option value="<?php echo $value->id_mstdistrict; ?>" <?php if(isset($user_details->DistrictID)){  echo (($user_details->DistrictID == $value->id_mstdistrict)?"selected":""); } ?>><?php echo ucwords(strtolower($value->DistrictName)); ?></option>
						<?php } //} ?>
					</select>
				</div>

						<div class="col-xs-2 text-left facility_box">
					<label for="id_mstfacility">Facility Name</label>
				</div>
				<div class="col-xs-4 facility_box">
					<select class="selectpicker form-control" data-toggle="dropdown" id="id_mstfacility" name="id_mstfacility" >
						<option value="0">Select</option>
						<?php foreach($Facility as $row){?>
						
						<option value="<?php echo $row->id_mstfacility;?>" <?php echo (($user_details->id_mstfacility == $row->id_mstfacility)?"selected":"");?>><?php echo $row->facility_short_name;?></option>
						<?php } ?>
					</select>
				</div>
				
			</div>


<br/>
<div class="row">
				

				<?php if($useriddv==''){ ?>
				<div class="col-xs-2 text-left">
					<label for="username">Username<span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
					<input type="text" <?php if($loginData->user_type != '1'){ ?> readonly="" <?php } ?> class="form-control" id="username" name="username" placeholder="Username" value="<?php //if(isset($user_details->username)){ echo $user_details->username; } ?>" <?php if($useriddv==''){ echo 'required';} ?> >
					
				</div>
			<?php  } ?>
	<div class="col-xs-2 text-left">
					<label for="email">Email</label>
				</div>
				<div class="col-xs-4">
					<input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php if(isset($user_details->Email)){  echo $user_details->Email;} ?>"  >
				</div>
		
			</div>
<br>
<?php if(empty($useriddv)){ 

$inputtype = 'text';
}else{
$inputtype = 'password';
	}
	?>


			<div class="row">
				<div class="col-xs-2 text-left">
					<label for="password">Password<span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
					<input  type=<?php echo '"'.$inputtype.'"'; ?> class="form-control" id="password" name="password" placeholder="Password" value="" required>
				</div>
				<div class="col-xs-2 text-left">
					<label for="repassword">Re-type Password<span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-4">
					<input type=<?php echo '"'.$inputtype.'"'; ?> class="form-control" id="repassword" name="repassword" placeholder="Re-type Password" value="" required>
				</div>
			</div>

<br>
			<!-- <div class="row">
			
				<div class="col-xs-2 text-left">
					<label for="mobile">Mobile</label>
				</div>
				<div class="col-xs-4">
					<input type="mobile" class="form-control" id="mobile" name="mobile" placeholder="Enter Mobile" value="<?php if(isset($user_details->Mobile)){  echo $user_details->Mobile;} ?>" maxlength='10' onkeypress="return isNumberKey(event)">
				</div>
			</div> -->
<br>
			
	<!-- <input type="hidden" name="updatah" id="updatah"> -->

<div class="row">
			
		
			</div>
<br>
				<?php if($loginData->RoleId == '99' || $loginData->RoleId == 0 || $loginData->id_tblusers==$id_user || $loginData->user_type==1){ ?>
				<div class="row">
					<div class="col-xs-12 text-center">
						<button type="submit" class="btn btn-warning" id="submit_btn">Add New User</button>
					</div>
				</div>
			<?php } ?>
			 <?php echo form_close(); ?>

		</div>
	</div>
</div>

	</div>

	<!-- Data Table -->
<script src="<?php echo site_url('application/third_party');?>/js/crypto-js.min.js"></script>
<script type="text/javascript">


$('.usersdelete').click(function(){

var idval = $(this).attr('id');

            $.ajax({
               url : "<?php echo base_url().'users/delete/'; ?>"+idval,
				data : {
					<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash() ;?>'
				},
				method : 'POST',
                success : function(data)
                {
                   location.href= '<?php echo base_url().'users/'; ?>'; 
                },
                error : function(error)
                {
                    alert('Error Fetching Districts');
                }
            })
        });


 function myFunction() 
        {           
          var password = $('#password').val();
          if(password!=''){
          //var salt = $('#salt').val();
          $('#password').val(compute_hashed_password(password));
          $('#repassword').val(compute_hashed_password(password));
          return true;
      }
        }

        function compute_hashed_password(password) 
        {
          var first_pass = CryptoJS.SHA256(password).toString();
          var second_pass = CryptoJS.SHA256(password).toString();
         // alert(first_pass+'/'+second_pass);
          //console.log(first_pass);
          return first_pass;
        }

/*$('#repassword').change(function(){
$('#updatah').val($('#repassword').val());
});

$('#password').change(function(){
$('#updatah').val($('#password').val());
});	*/	

$('#role_authentication').change(function(){
getusernamegen();

});	
$('#input_district').change(function(){
getusernamegen();

});	
$('#input_state').change(function(){
//getusernamegen();
});	
$('#id_mstfacility').change(function(){
getusernamegen();
});	

$('#usertype').change(function(){
adminuser();
getusernamegen();
});	
/*$('#input_state').change(function(){
//adminuser();
//getusernamegen();
});	*/

$('#input_state').change(function(){
//adminuser();if()
if($('#usertype').val()==3){
adminuser();
//alert($('#usertype').val());
}else{
getusernamegen();
//alert($('#usertype').val());
}
});	

function adminuser(){

		var input_state = $('#input_state').val();

			$.ajax({
				url : "<?php echo base_url().'users/getStateCodeAdmin/'; ?>"+input_state,
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				dataType: 'json',
				success : function(data)
				{
					if(data.status == 'true'){
						var parsedData = JSON.parse(data.fields);

//alert(parsedData.StateCd);
if($('#usertype').val()==3){
	var usertype_val = 'NOST@'+parsedData.StateCd;
	var password = parsedData.StateCd+parsedData.id_mststate;
	var repassword = parsedData.StateCd+parsedData.id_mststate;
	//alert(parsedData.StateCd);
}
if($('#usertype').val()==5){
	var usertype_val = 'LABST@'+parsedData.StateCd;
	var password = parsedData.StateCd+parsedData.id_mststate+1;
	var repassword = parsedData.StateCd+parsedData.id_mststate+1;
}			

//if($("#delivery_level").val()!='' && $("#district").val()!='' && $("#facility_type").val()!=''){
$('#username').val(usertype_val);
$('#password').val(password);
$('#repassword').val(repassword);
}
				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			
			})

}



	function getusernamegen(){

					var role=  $('#role_authentication').val();
					var input_state = $('#input_state').val();
					var role_tcmtc = $("#role_authentication option:selected").text();
					var id_mstfacility = $('#id_mstfacility').val();
					var id_mstdistrict = $('#input_district').val();
					var randval= '<?php echo  rand(10,99); ?>';
					//alert(input_state);
					 var dataString = {
	"unitid": $('#input_state').val()
	};
			$.ajax({
				url : "<?php echo base_url().'users/getStateCode/'; ?>"+input_state+'/'+id_mstdistrict+'/'+id_mstfacility,
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				dataType: 'json',
				success : function(data)
				{
					if(data.status == 'true'){
						var parsedData = JSON.parse(data.fields);

		console.log(parsedData);		
if(parsedData.is_Mtc==0){
	var usertype_val = 'TC';
	
}else{
	var usertype_val = 'MTC';
	
}
var mtctccount1111 = parseInt(parsedData.is_Mtcval);

if(parsedData.is_Mtcval==null){
	//alert('if'+mtctccount1111);
	var mtctccount = 1;
	
}else{
	//alert('else'+mtctccount1111);
	var mtctccount = parseInt(parsedData.is_Mtcval)+1;
	
}
<?php if($loginData->State_ID==3 || $loginData->State_ID==6){ ?>
var facilityCode = '-'+parsedData.FacilityType;
<?php }else {  ?>
var facilityCode = '';
<?php } ?>
if(parsedData.FacilityCode!=null){
var strval = parsedData.FacilityCode;
var resval = strval.split('-');
var resvalcount = resval[2];
}else{
	var resvalcount = '';
}
if(parsedData.DistrictCd!=null){
var DistrictCdval = parsedData.DistrictCd;
}else{
var DistrictCdval ='';
}
//var mtctccount = parseInt(parsedData.is_Mtcval)+1;
//alert(mtctccount);
if(role==1){
	var role_authval = 'PS@'+parsedData.DistrictCd+'-'+resvalcount+facilityCode;
	var password = parsedData.DistrictCd+'-'+resvalcount+randval;
	var repassword = parsedData.DistrictCd+'-'+resvalcount+randval;
}else if(role==10){
	var role_authval = 'DO@'+parsedData.DistrictCd+'-'+resvalcount+facilityCode;
	var password = parsedData.DistrictCd+'-'+resvalcount+randval;
	var repassword = parsedData.DistrictCd+'-'+resvalcount+randval;
}else if(role==9){
	var role_authval = 'LT@'+parsedData.DistrictCd+'-'+resvalcount+facilityCode;
	var password = parsedData.DistrictCd+'-'+resvalcount+randval;
	var repassword = parsedData.DistrictCd+'-'+resvalcount+randval;
}else if(role==7){
	var role_authval = 'PH@'+parsedData.DistrictCd+'-'+resvalcount+facilityCode;
	var password = parsedData.DistrictCd+'-'+resvalcount+randval;
	var repassword = parsedData.DistrictCd+'-'+resvalcount+randval;
}else if(role==8){
	var role_authval = 'MO@'+parsedData.DistrictCd+'-'+resvalcount+facilityCode;
	var password = parsedData.DistrictCd+'-'+resvalcount+randval;
	var repassword = parsedData.DistrictCd+'-'+resvalcount+randval;
}

else if(role==99){
	var role_authval = 'NOTR@'+parsedData.DistrictCd+'-'+resvalcount+facilityCode;
	var password = parsedData.DistrictCd+'-'+resvalcount+randval;
	var repassword = parsedData.DistrictCd+'-'+resvalcount+randval;
}

else if(role==12){
	var role_authval = 'NOLT@'+parsedData.DistrictCd+'-'+resvalcount+facilityCode;
	var password = parsedData.DistrictCd+'-'+resvalcount+randval;
	var repassword = parsedData.DistrictCd+'-'+resvalcount+randval;
}



if($("#usertype").val()=='2'  && $("#id_mstfacility").val()!=0){
$('#username').val(role_authval);

$('#password').val(password);
$('#repassword').val(repassword);

$('#updatah').val(password);
}
}
				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			
			})


}





	$(document).ready(function(){

		$(".glyphicon-trash").click(function(e){
			var ans = confirm("Are you sure you want to delete?");
			if(!ans)
			{
				e.preventDefault();
				return false;
			}
		});



		var edit_flag = <?php echo $edit_flag; ?>;
		if(edit_flag == 1)
		{
			$("#form_head").text("Edit User");
			$("#submit_btn").text("Update User");
			$("#password").removeAttr("required");
			$("#repassword").removeAttr("required");
			if($("#usertype").val() == 1)
			{
				$(".facility_box").hide();
				$(".state_districts").hide();
				$(".state_districts_dis").hide();
				$("#id_mstfacility").removeAttr("required");
				$("#input_state").removeAttr("required");
				$("#input_district").removeAttr("required");
				$("#role_authentication").removeAttr("required");
				
				
				$("#id_mstfacility").val('');
				$("#input_state").val("");
				$("#input_district").val("");
				$("#role_authentication").val("");
			}

			<?php if(isset($user_details->user_type)) { if($user_details->user_type == 3) { ?>

				$(".state_districts").show();
				$(".state_districts_dis").hide();
				$(".facility_box").hide();
				$("#input_state").prop('required',true);
				$("#id_mstfacility").removeAttr("required");
				$("#input_district").removeAttr("required");
				$("#role_authentication").removeAttr("required");
				//$("#input_state").val("");
				$("#id_mstfacility").val("");
				$("#input_district").val("");
				$("#role_authentication").val("");
	<?php } } ?>

	<?php if(isset($user_details->user_type)) { if($user_details->user_type == 4) { ?>

				$(".state_districts").show();
				$(".state_districts_dis").show();
				$(".facility_box").hide();
				$("#input_state").prop('required',true);
				$("#input_district").prop('required',true);
				$("#id_mstfacility").removeAttr("required");
				$("#role_authentication").prop('required',true);
				//$("#input_state").val("");
				$("#id_mstfacility").val("");
				$("#input_district").val("");
	<?php } } ?>

		}

		$("#usertype").change(function(){
			if($(this).val() == 1)
			{
				$(".facility_box").hide();
				$(".state_districts").hide();
				$(".state_districts_dis").hide();
				

				$("#id_mstfacility").removeAttr("required");
				$("#input_state").removeAttr("required");
				$("#input_district").removeAttr("required");
				$("#role_authentication").removeAttr("required");

				$("#id_mstfacility").val("");
				$("#input_state").val("");
				$("#input_district").val("");
				$("#role_authentication").val();
			}else if($(this).val() == 3){

				$(".state_districts").show();
				$(".state_districts_dis").hide();
				$(".facility_box").hide();
				$("#input_state").prop('required',true);
				$("#id_mstfacility").removeAttr("required");
				$("#input_district").removeAttr("required");
				$("#role_authentication").removeAttr("required");
				//$("#input_state").val("");
				$("#id_mstfacility").val("");
				$("#input_district").val("");

			}
			else if($(this).val() == 5){

				$(".state_districts").show();
				$(".state_districts_dis").hide();
				$(".facility_box").hide();
				$("#input_state").prop('required',true);
				$("#id_mstfacility").removeAttr("required");
				$("#input_district").removeAttr("required");
				$("#role_authentication").removeAttr("required");
				//$("#input_state").val("");
				$("#id_mstfacility").val("");
				$("#input_district").val("");

			}

			else if($(this).val() == 4){

				
				$(".state_districts").show();
				$(".state_districts_dis").show();
				$(".facility_box").hide();
				$("#input_state").prop('required',true);
				$("#input_district").prop('required',true);
				$("#role_authentication").prop('required',true);
				$("#id_mstfacility").removeAttr("required");
				//$("#input_state").val("");
				$("#id_mstfacility").val("");
				$("#input_district").val("");
				
				
			}
			else
			{
				$(".facility_box").show();
				$(".state_districts").show();
				$(".state_districts_dis").show();
				$("#id_mstfacility").prop('required',true);
				$("#input_state").prop('required',true);
				$("#input_district").prop('required',true);
				$("#role_authentication").prop("required",true);
			}
		});

		$("#submit_btn").click(function(e){

			if($("#password").val() != $("#repassword").val())
			{
				alert("Passwords do not match");
				e.preventDefault();
			}
			// if(edit_flag == 1)
			// {
			// 	var text = "Are you sure you want to Update this user";
			// }
			// else
			// {
			// 	var text = "Are you sure you want to Add this user";
			// }

			// var ans = confirm(text);
			// if(!ans)
			// {
			// 	e.preventDefault();
			// }
		});
	});

	$("#repassword" ).change(function( event ) {

	var password = $("#password").val();
	var repassword = $("#repassword").val();

	if(password != repassword){

	$("#modal_header").text("Password does not match Re-type password");
					$("#modal_text").text("Passwords do not match");
					$("#repassword" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}



});

		$("#password" ).change(function( event ) {

	var password = $("#password").val();
	var repassword = $("#repassword").val();
if(repassword!=''){
	if(password != repassword){

	$("#modal_header").text("Password does not match Re-type password");
					$("#modal_text").text("Passwords do not match");
					$("#password" ).val('');
					$("#multipurpose_modal").modal("show");
					return false;
	
}
}

/**/
<?php if($useriddv!=''){ ?>
		var password = $("#password").val();
		var pattern = /^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/;
		if(pattern.test(password)){
		return true;


    }else{

    	$('#message').show();

    	$('#message').html('');
    	 $("#password").val('');
		$("#modal_header").text("Enter minimum 8 chars with atleast 1 number, lower, upper & special(@#$%&) characters");
		$("#multipurpose_modal").modal("show");
		return false;

    }
<?php } ?>
/**/

});



	function isNumberKey(evt)
	{
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	}
	$('.messagedata').keypress(function (e) {
        var regex = new RegExp(/^[a-zA-Z\s]+$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            return false;
        }
    });

$(document).ready(function(){

	/*vikram*/
	$('#full_name').keypress(function (e) {
var regex = new RegExp("^[a-zA-Z ]*$");
var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
if (e.which === 32 && (this.value.length==0 || e.target.selectionStart==0)) {
           e.preventDefault();
           return false;
       }

else if (regex.test($.trim(str))) {
var error_css = {"border":"1px solid #c7c5c5"};
$(this).css(error_css);
return true;

}
else{
e.preventDefault();
return false;
}

});
$('#password,#repassword,#email').keypress(function (e) {
var regex = new RegExp("[^ ]*$");
var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
if (e.which === 32) {
           e.preventDefault();
           return false;
       }

else if (regex.test($.trim(str))) {
var error_css = {"border":"1px solid #c7c5c5"};
$(this).css(error_css);
return true;

}
else{
e.preventDefault();
return false;
}

});
$('#usertype,#input_district,#input_state,#role_authentication,#id_mstfacility').change(function (e) {
 if ($(this).val()!="") {
          var error_css = {"border":"1px solid #c7c5c5"};
$(this).css(error_css);
           return true;
       }

});
$('#submit_btn').click(function(e){
var error_css = {"border":"1px solid red"};
var edit_flag = <?php echo $edit_flag; ?>;

if($('#full_name').val().trim() == ""){

$("#full_name").css(error_css);
$("#full_name").focus();
e.preventDefault();
return false;
}
else if($('#usertype').val().trim() === ""){
$("#usertype").css(error_css);
$("#usertype").focus();
e.preventDefault();
return false;
}
else if($('#usertype').val().trim() === "2"){
if($('#role_authentication').val().trim() == ""){
$("#role_authentication").css(error_css);
$("#role_authentication").focus();
e.preventDefault();
return false;
}
else if($('#input_district').val().trim() == ""){
$("#input_district").css(error_css);
$("#input_district").focus();
e.preventDefault();
return false;
}
else if($('#id_mstfacility').val().trim() == ""){
$("#id_mstfacility").css(error_css);
$("#id_mstfacility").focus();
e.preventDefault();
return false;
}
}
else if($('#input_state').val().trim() == ""){
$("#input_state").css(error_css);
$("#input_state").focus();
e.preventDefault();
return false;
}
else if(edit_flag != 1)
{
if($('#password').val().trim()==""){

$("#password").css(error_css);
$("#password").focus();
e.preventDefault();
return false;
}

else if($('#repassword').val().trim==""){

$("#repassword").css(error_css);
$("#repassword").focus();
e.preventDefault();
return false;
}
}
else{
return true;
}

});
var ambit = $(document);

    // Disable Cut + Copy + Paste (input)
    ambit.on('copy paste cut', function (e) {
        e.preventDefault(); //disable cut,copy,paste
        return false;
    });


});

		$('#input_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					//$("#input_district").trigger('change');

				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){
			//alert($(this).val());
			$.ajax({
				url : "<?php echo base_url().'users/getFacilitiesname/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					
					if(data!="<option value=''>Select Facilities</option>"){
						
					$('#id_mstfacility').html(data);
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});

</script>


