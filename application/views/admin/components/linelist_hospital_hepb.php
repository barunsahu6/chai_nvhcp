<style>
	thead
	{
		background-color: #088DA5;
		color: white;
	}

	.data_td
	{
		font-weight: 600;
		font-size: 16px;
	}

	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}

	.input_fields
	{
		height: 30px !important;
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	textarea
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}

	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}

	.form_buttons:focus
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}

	@media (min-width: 768px) {
		.row.equal {
			display: flex;
			flex-wrap: wrap;
		}
	}

	@media (max-width: 768px) {

		.input_fields
		{
			height: 40px !important;
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
		}

		.form_buttons
		{
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
			font-weight: 600 !important;
			padding: 8px 12px !important;
			border: 2px solid #A30A0C !important;

		}
		.form_buttons:hover
		{
			border-radius: 0 !important;
			width: 100% !important;
			font-size: 15px !important;
			font-weight: 600 !important;
			padding: 8px 12px !important;
			border: 2px solid #A30A0C !important;
			background-color: #FFF;
			color: #A30A0C;

		}
	}

	.btn-default {
		color: #333 !important;
		background-color: #fff !important;
		border-color: #ccc !important;
	}
	.btn {
		display: inline-block;
		padding: 6px 12px;
		margin-bottom: 0;
		font-size: 14px;
		font-weight: 400;
		line-height: 2.4;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		-ms-touch-action: manipulation;
		touch-action: manipulation;
		cursor: pointer;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
		user-select: none;
		background-image: none;
		border: 1px solid transparent;
		border-radius: 4px;
	}

	a.btn
	{
		text-decoration: none;
		color : #000;
		background-color: #A30A0C;
	}

	.btn-group .btn:hover
	{
		text-decoration: none !important;
		color: #000 !important;
		background-color: #CCC !important;
	}

	.btn-group .btn:hover
	{
		text-decoration: none !important;
		color: #000 !important;
		background-color: inherit !important;
	}

	a.active
	{
		color : #FFF !important;
		text-decoration: none;
		background-color: inherit !important;
	}

	#table_patient_list tbody tr:hover
	{
		cursor: pointer;
	}

	.btn-success
	{
		background-color: #A30A0C;
		color: #FFF !important;
		border : 1px solid #A30A0C;
		border-radius: 0;
	}

	.btn-success:hover
	{
		text-decoration: none !important;
		color: #A30A0C !important;
		background-color: white !important;
		border : 1px solid #A30A0C;
		border-radius: 0;
	}
	select[readonly] {
		background: #eee;
		pointer-events: none;
		touch-action: none;
	}

	.pull-right {
  float: right !important;

}
</style>

<br>
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<h3 class="text-center" style="background-color: #484848; color: white; padding-top: 10px; padding-bottom: 10px;">Line List Hepb
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/excel_black.png" alt=""></a> -->
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/pdf_color.png" alt=""></a> -->
		</h3>
		<!-- <input type="button" onclick="printDiv('printableArea')" value="Print" /> -->
		

	</div>

</div>
<br>
<?php $loginData = $this->session->userdata('loginData');
//echo "<pre>"; print_r($loginData);

if($loginData->user_type==1) { 
	$select = '';
}else{
	$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
	$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
	$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
	$select4 = '';	
}
?>

<?php
$filterData = $this->session->userdata('filters1');
$attributes = array(
	'id' => 'filter_form',
	'name' => 'filter_form',
	'autocomplete' => 'off',
);
//echo $filterData['id_input_district'];die();
// echo '<pre>';
// print_r($facilities);die();
echo form_open('', $attributes); 
?>
<div class="row">

	<div class="col-md-2 col-md-offset-1">
		<label for="">State</label>
		<select type="text" name="search_state" id="search_state" class="form-control input_fields" required <?php echo $select2.$select3.$select4; ?>>
			<option value="">Select State</option>
			<?php 
			foreach ($states as $state) {
				?>
				<option value="<?php echo $state->id_mststate; ?>" <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';}elseif($filterData['id_search_state'] == $state->id_mststate){echo 'selected'; }else{ echo '';} ?>><?php echo ucwords(strtolower($state->StateName)); ?></option>
				<?php 
			}
			?>
		</select>
	</div>
	<div class="col-md-2">
		<label for="">District</label>
		<select type="text" name="input_district" id="input_district" class="form-control input_fields"  <?php echo $select.$select2.$select4; ?>>		 	
			<option value="" >Select District</option>
	
		<?php foreach ($districts as  $value) { ?>
				<option value="<?php echo $value->id_mstdistrict; ?>" <?php if($this->input->post('input_district')==$value->id_mstdistrict) { echo 'selected';}  ?>><?php echo $value->DistrictName; ?></option>
			<?php } ?>
				?>
			</select>
		</div>
		<div class="col-md-2">
			<label for="">Facility</label>
			<select type="text" name="facility" id="mstfacilitylogin" class="form-control input_fields"  <?php echo $select.$select2; ?>>
				<option value="">Select Facility</option>
				<?php 
				foreach($facilities as $facilitie){
					?><option value="<?=$facilitie->id_mstfacility?>" <?php if(isset($_POST['facility'])) { if($_POST['facility'] == $facilitie->id_mstfacility) { echo 'selected'; } } ?> ><?=$facilitie->hospital?></option><?php
				}
				?>
			</select>
		</div>


		<div class="col-md-2">
			<label for="">From Date</label>
			<input type="text"  class="form-control input_fields hasCal dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php  if(isset($_POST['startdate'])){echo timeStampShow($_POST['startdate']);}else{echo timeStampShow($startdateval[0]->startdate);} ?>" required autocomplete="off">
		</div>

		<div class="col-md-2 ">
			<label for="">To date</label>
			<input type="text" class="form-control input_fields hasCal dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if(isset($_POST['enddate'])){echo $_POST['enddate'];}else{echo timeStampShow(date('Y-m-d'));} ?>" required>
		</div>

		<!-- <div class="col-md-2 col-md-offset-1">
			<label for="" style="margin-top:10%">Template list</label>
			<select class="form-control input_fields" name="temp" id="temp">
				<option value="">--Select-All--</option>
				<?php foreach ($templist as  $value) { ?>

					<option value="<?php echo $value->masterId;?>" <?php if($this->input->post('temp')==$value->masterId) { echo 'selected';} ?>><?php echo $value->template_name;?></option>
				<?php } ?>
			</select>
		</div>
	 -->
	

	
		<div class="col-md-2 col-md-offset-1 pull-right">
			<button type="submit" class="btn btn-block btn-success" style="line-height: 1.2; font-weight: 600; margin-top:22%; margin-left: -126px;">SEARCH</button>
		</div>
	</div>


	<?php echo form_close(); ?>
	<br>
	<br>
	<br>
	<div class="row">
		<div class="col-xs-8 col-xs-offset-2">
			<table class="table table-striped table-bordered table-vmiddle table-hover">
				<thead>
					<th>Sno</th>
					<th>Hospital</th>
					<th>Patient Count</th>
					<th>Download Link</th>
				</thead>
				<tbody>
					<?php 
					error_reporting(0);
					$dir = APPPATH.'linelistsall';

				// Open a directory, and read its contents'
					if (is_dir($dir)){
						if ($dh = opendir($dir)){

							while (($file = readdir($dh)) !== false){

								$filedd[]=$file;
							}
							closedir($dh);
						}
					}
				//print_r($filedd);

					$dataexplode = explode('-', $filedd[2]);
				//print_r($dataexplode);

				$i=0; if(count($facilities)>1)	{ foreach ($facilities as $row) { $i++;
						echo '<tr>
						<td>'.$i.'</td>
						<td>'.$row->hospital.'</td>
						<td>'.$row->patients.'</td>
						<td><a href="'.site_url().'linelist_hepb/download_list_zipped/'.$row->id_mstfacility.'"><i class="fa fa-download"></i></a>';

						if(strtolower($dataexplode[0])==strtolower($row->hospital)){

							$filenameval = $filedd[2];
							echo '| <a href="'.site_url().'application/linelistsall/'.$filenameval .'" title="Download"><i class="fa fa-download"></i></a>';
							 //echo '| <a href="#" title="Download"><i class="fa fa-download"></i></a>';
						}
						'</td>
						</tr>';
					} }else{ 
					?>
<td colspan="5">No Data Found</td>
				<?php } ?>
				</tbody>
			</table>
		</div>
	</div>

	<script>

		$(document).ready(function(){
			$("#search_state").trigger('change');

		});
		$('#search_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');

				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
						$('#mstfacilitylogin').html(data);
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});
		function printDiv(divName) {
			var printContents = document.getElementById(divName).innerHTML;
			var originalContents = document.body.innerHTML;

			document.body.innerHTML = printContents;

			window.print();

			document.body.innerHTML = originalContents;
		}

	</script>