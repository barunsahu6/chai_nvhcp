	<style>
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
    	padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
</style>
	<div class="row main-div" style="min-height:400px;">
		<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">

			<div class="panel panel-default">

				<div class="panel-heading">
					<h4 class="text-center" style="color : white;">Risk Factor List <a href="<?php echo site_url("risk_factors")?>" style="color: white; cursor: pointer;"><i class="fa fa-plus-circle pull-right"></i></a></h4>
				</div>
				<div class="panel-body set_height">
					<table id="data-table-command" class="table table-striped table-bordered table-vmiddle table-hover">
						<thead>
							<tr>
								<th class="text-center">Risk Factor</th>
								<th class="text-center">Commands</th>
							</tr>

						</thead>
						<tbody>
							<?php foreach($risk_factors as $row){ ?>
							<tr>

								<td class="text-center" style="padding-top: 16px;"><?php echo $row->LookupValue; ?></td>
								
								<td class="text-center">
									<a href="<?php echo site_url('risk_factors/index/'.$row->id_mstlookup);?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
									<a href="<?php echo site_url('risk_factors/delete/'.$row->id_mstlookup);?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
								</td>

							</tr>

							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
		<div class="panel panel-default">

		<div class="panel-heading">
			<h4 class="text-center" style="color: white;" id="form_head">Add Risk Factor</h4>
		</div>

		<div class="panel panel-body ">
			<form role="form" method="post" action="">
			<div class="row">
					
				<div class="col-xs-2 text-center">
					<label for="risk_factor_text">Risk Factor</label>
				</div>
				<div class="col-xs-4">
					<input type="text" class="form-control" id="risk_factor_text" name="risk_factor_text" placeholder="Risk Factor" value="<?php if(isset($selected_risk_factors->LookupValue)){ echo $selected_risk_factors->LookupValue;} ?>" required>
				</div>
					<div class="col-xs-6 text-center">
						<button type="submit" class="btn btn-warning" id="submit_btn">Add New Risk Factor</button>
					</div>
			</div>

			</form>

		</div>
	</div>
</div>

	</div>

	<!-- Data Table -->
<script type="text/javascript">
	$(document).ready(function(){

		$(".glyphicon-trash").click(function(e){
			var ans = confirm("Are you sure you want to delete?");
			if(!ans)
			{
				e.preventDefault();
			}
		});

		var edit_flag = <?php echo $edit_flag; ?>;
		if(edit_flag == 1)
		{
			$("#form_head").text("Edit Risk Factor");
			$("#submit_btn").text("Update Risk Factor");
		}
	});
</script>


