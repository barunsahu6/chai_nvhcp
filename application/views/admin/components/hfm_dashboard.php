<style>
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: middle;
	}
	.table-bordered>tbody>tr>td.data_td,.table-bordered>tbody>tr>td.form-control
{
	font-weight: 600;
	font-size: 15px;
	text-align: center;
	vertical-align: middle;
}
	.item{
		width: 140px !important;
	}
	.batch_num{
		width: 100px !important;
	}
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	.tbl_border{
		border-style : hidden!important;
		border: none;
  		padding: 0px !important;

	}
	.form-control{
		border-radius: 5px;
		width: 100%;
		text-align:center;
		font-size: 14px;
	}
	.table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
    vertical-align: middle;
    width: 20%;
    text-align: center;
}
	@media (min-width: 768px){
.btn-width {
    width: 11.666667%;
}
.container{
	width: 75%;
}
}
.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
.table-bordered>thead>tr>th{
	vertical-align: middle;
	font-size: 13px;
	font-family: 'Source Sans Pro';
	background-color: #085786;
}
.table-bordered>tbody>tr>td{
	vertical-align: middle;
	font-size: 13px;
	color: #000000;
	font-family: 'Source Sans Pro';
}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData'); ?>
<div class="container">
<div class="row main-div" style="min-height:400px;">
	
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-default" id="Record_receipt_panel">
				<div class="panel-heading" style="background-color: #423e3e;">
					<h4 class="text-center" style="color: white;" id="Record_form_head">HFM Entry</h4>
				</div>
				<div class="row">
					<?php 
					$tr_msg= $this->session->flashdata('tr_msg');
					$er_msg= $this->session->flashdata('er_msg');
					if(!empty($tr_msg)){  ?>
						

						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						<?php } else if(!empty($er_msg)){ ?>
							<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
								
							<?php } ?>
						</div>
						<?php
						$attributes = array(
							'id' => 'hfm_filter_form',
							'name' => 'hfm_filter_form',
							'autocomplete' => 'false',
						);
  //pr($items);
  //print_r($states_hfm_data);
						echo form_open('', $attributes); ?>
						<div class="row" style="margin-left: 10px;margin-top: 20px; padding-right: 20px;">
							<div class="col-xs-12 col-sm-12 col-md-2 col-xs-offset-3">
								<label for="year">Financial Year <span class="text-danger">*</span></label>
							</div>
								<div class="col-md-2 col-sm-12 form-group">
									<select name="year" id="year" required class="form-control" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2019', date('Y'));
										$flag=0;
										foreach($dates as $date){

									if (date('m', strtotime($date)) < 4) {//Upto April
										$year = ($date-1) . '-' . $date;
									} else {//After April
										$year = $date . '-' . ($date + 1);
									}
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									elseif($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
									
								}
								?>
							</select>
						</div>
						<div class="col-md-2 col-lg-2 col-sm-12 btn-width">
							<button class="btn btn-block btn-success" name="submit" value="search" style="line-height: 1.2; font-weight: 600;height: 32px;">SEARCH</button>
						</div>
						<div class="col-md-2 col-lg-2 btn-width col-sm-12 pull-right" style="padding-bottom: 20px;">
							<button class="btn btn-block btn-success" name="submit" value="save" style="line-height: 1.2; font-weight: 600;height: 35px;">SAVE</button>
						</div>
					</div>
						
						</div>
	<?php	$attributes = array(
		'id' => 'hfm_add_form',
		'name' => 'hfm_add_form',
		'autocomplete' => 'off',
	);
	echo form_open('hfm_Dashboard/index/', $attributes); ?>
					<div class="table-responsive">
				
						<table class="table table-striped table-bordered table-hover table-vmiddle" id="table_inventory_list">

							
							<thead>
								<tr style="background-color: #088DA5;color: white;font-family:Georgia, serif; font-size: 14px; font-weight: 600">
									<th rowspan="2">State</th>
									<th colspan="2">MTC</th>
									<th colspan="3">TC</th>
								</tr>
							<tr style="background-color: #088DA5;color: white;font-family:Georgia, serif; font-size: 12px; font-weight: 600">
									<th>Target</th>
									<th>Established</th>
									<th>Target</th>
									<th>Number of Districts with TC</th>
									<th>Total&nbsp;number&nbsp;of&nbsp;TCs established</th>
									<!--<th colspan="2">Commands</th>-->
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($states_hfm_data)){
								foreach ($states_hfm_data as $key => $value) {
								?>
								<tr>
				 <td nowrap style="background-color: #ADD8E6; font-weight: 600;text-align: left;"><?php echo $value->StateName; ?><input type="hidden" class="form-control" name="id_mststate[]" value="<?php echo $value->id_mststate; ?>" /></td>
			<td class="tbl_border"><input type="number" class="form-control data_td" placeholder="Add Target" name="target[]" autocomplete="off" min="0" max="999" value="<?php echo $value->target_mtc; ?>" style="height: 40px; font-weight: 600;font-variant-numeric: tabular-nums;"/></td>
          <td class="tbl_border">
          	<input type="number" autocomplete="off" name="established[]" class="form-control data_td" placeholder="Add Established MTC" min="0" max="999" value="<?php echo $value->established_mtc; ?>" style="height: 40px;font-weight: 600;font-variant-numeric: tabular-nums;" /></td>
          	   <td class="tbl_border">
          	<input type="number" autocomplete="off" class="form-control data_td" name="target_tc[]" value="<?php echo $value->target_tc; ?>" min="0" max="999" placeholder="Target" style="height: 40px;font-weight: 600;font-variant-numeric: tabular-nums;" /></td>
          <td class="tbl_border">
          	<input type="number" autocomplete="off" class="form-control data_td" name="districts_with_tc[]" value="<?php echo $value->district_with_tc; ?>" min="0" max="999" placeholder="Districts with TC" style="height: 40px;font-weight: 600;font-variant-numeric: tabular-nums;" /></td>
          <td class="tbl_border">
          	<input type="number" class="form-control data_td" autocomplete="off" name="total_tc[]" placeholder="Total Facilities" min="0" max="999" value="<?php echo $value->tc_established; ?>" style="height: 40px;font-weight: 600;font-variant-numeric: tabular-nums;" /></td>
          		
      </tr>
  <?php }}?>
			</tbody>
								</table>
								<?php echo form_close(); ?>
								</div>	
							</div>
							<br>
						</div>
						<div class="overlay">
							<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
						</div>
			</div>
						<!-- Data Table -->
						<script type="text/javascript">
							$(".form-control").on('keyup',function(evt){
								var charCode = (evt.which) ? evt.which : event.keyCode
								if (charCode > 31 && (charCode < 48 || charCode > 57))
									return false;
								
								var index = $(".form-control").index(this);
								
                        		//$(".form-control").eq(index).slideDown(1000);
                        		if($('.form-control').eq(index).val() > 999 || $('.form-control').eq(index).val() < 0)
								{
									//alert(index);
									$("#modal_header").html('Value cannot be more than 3 digits or less than 0');
									$("#modal_text").html('Please fill in appropriate Value');
									$("#multipurpose_modal").modal("show");
									$('.form-control').eq(index).val('');
									return false;
								}

								return true;

							});
							
							
							$(document).ready(function(){
								$(".glyphicon-trash").click(function(e){
									var ans = confirm("Are you sure you want to save?");
									if(!ans)
									{
										e.preventDefault();
									}
								});
							});

						</script>


