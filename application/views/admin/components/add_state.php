<div class="row main-div" style="min-height:448px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title text-center" style="color: white;"><b>Add New State</b></h4>
			</div>

			<div class="panel panel-body">
				<form role="form" method="post" action="">
					
					<div class="panel-body">
						<label for="stateCd">State Code</label>
						<input type="text" class="form-control" id="stateCd" name="stateCd" placeholder="Enter  Sate Code" value="" required>
					</div>


					<div class="panel-body">
						<label for="stateName">State Name</label>
						<input type="text" class="form-control" id="stateName" name="stateName" placeholder="Enter state Name" value="" required>
					</div>

					<div class="panel-body">
						<label for="stateShortNm">State Short Name</label>
						<input type="text" class="form-control" id="stateShortNm" name="stateShortNm" placeholder="Enter State Short Name" value="" required>
					</div>

					<div style="text-align:center;">	<button type="submit" class="btn btn-warning">Submit</button></div>

				</form>

			</div>
		</div>
	</div>
</div>
