<section>
	<form method="POST" action="">
	<div class="row">
		<div class="col-md-12 text-center">
			<?php 
				if(isset($updating) && $updating == 1)
				{
					echo '<h3>Update Regimen Rule</h3>';
				}
				else
				{
					echo '<h3>Add Regimen Rule</h3>';
				}
			?>
		</div>
	</div>
	<br>
	<?php 
		if(isset($updating) && $updating == 1)
		{
	?>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<code>{Rule you are Editting}</code>
				<table class="table table-hover table-highlighted table-bordered">
					<thead>
						<th>Cirrhosis</th>
						<th>Genotype</th>
						<th>Regimen</th>
						<th>Duration</th>
					</thead>
					<tbody>
						<tr>
							<td><?php echo $rule_details[0]->cirrhosis; ?></td>
							<td><?php echo $rule_details[0]->genotype; ?></td>
							<td><?php echo $rule_details[0]->regimen; ?></td>
							<td class="text-center"><?php echo $rule_details[0]->duration; ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	<?php
		}
	 ?>
	<div class="row">
		<div class="col-md-4 col-md-offset-4 text-center">
			<code>New Rules</code>
		</div>
	</div>
	<br>
		<div class="row">
		<div class="col-md-4 col-md-offset-4">
			
			<div class="row">
				<div class="col-lg-4">
					<label>Cirrhosis</label>	
				</div>
				<div class="col-lg-8">
					<select class="form-control select" name="cirrhosis_status" id="cirrhosis_status" required>
						<option value="">--Select--</option>
						<option value="1" <?php echo (isset($updating) && $updating == 1 && $rule_details[0]->cirrhosis == 'Cirrhosis')?"selected":""; ?>>Cirrhosis</option>
						<option value="2" <?php echo (isset($updating) && $updating == 1 && $rule_details[0]->cirrhosis == 'No Cirrhosis')?"selected":""; ?>>No Cirrhosis</option>
					</select>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-4">
					<label>Genotype</label>	
				</div>
				<div class="col-lg-8">
					<select class="form-control select" name="genotype" id="genotype" required>
						<option value="">--Select--</option>
						<option value="0" <?php echo (isset($updating) && $updating == 1 && $rule_details[0]->genotype == 'Genotype 0')?"selected":""; ?>>Genotype 0</option>
						<option value="1" <?php echo (isset($updating) && $updating == 1 && $rule_details[0]->genotype == 'Genotype 1')?"selected":""; ?>>Genotype 1</option>
						<option value="2" <?php echo (isset($updating) && $updating == 1 && $rule_details[0]->genotype == 'Genotype 2')?"selected":""; ?>>Genotype 2</option>
						<option value="3" <?php echo (isset($updating) && $updating == 1 && $rule_details[0]->genotype == 'Genotype 3')?"selected":""; ?>>Genotype 3</option>
						<option value="4" <?php echo (isset($updating) && $updating == 1 && $rule_details[0]->genotype == 'Genotype 4')?"selected":""; ?>>Genotype 4</option>
						<option value="5" <?php echo (isset($updating) && $updating == 1 && $rule_details[0]->genotype == 'Genotype 5')?"selected":""; ?>>Genotype 5</option>
						<option value="6" <?php echo (isset($updating) && $updating == 1 && $rule_details[0]->genotype == 'Genotype 6')?"selected":""; ?>>Genotype 6</option>
					</select>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-4">
					<label>Regimen</label>	
				</div>
				<div class="col-lg-8">
					<select class="form-control select" name="regimen" id="regimen" required>
						<option value="">--Select--</option>
						<?php 
							foreach ($regimen as $reg) {
						 ?>
							<option value="<?php echo $reg->id_mst_regimen; ?>" <?php echo (isset($updating) && $updating == 1 && ($rule_details[0]->id_mst_regimen == $reg->id_mst_regimen))?"selected":""; ?>><?php echo $reg->regimen_name; ?></option>
						<?php 
							}
						 ?>
					</select>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-4">
					<label>Duration</label>	
				</div>
				<div class="col-lg-8">
					<input class="form-control" type="number" name="duration" id="duration" required value="<?php echo (isset($updating) && $updating == 1)?$rule_details[0]->duration:""; ?>">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-12 text-center">
					<button type="submit" class="btn btn-warning">Save</button>	
				</div>
			</div>
			<br>

		</div>
	</div>

	</form>
</section>