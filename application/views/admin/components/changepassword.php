	<style>
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
    	padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
		.form-horizontal .control-label{
    text-align: left;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
</style>

<script type="text/javascript" >

         history.pushState(null, null, location.href);
     window.onpopstate = function () {
          history.go(1);
      };
  </script>
<?php 
//echo hash('sha256','NOTR@Kol-GMC-MTC');
  $useriddv = $this->uri->segment(3);  
 $loginData = $this->session->userdata('loginData');
 //print_r($loginData); ?>
	<div class="row main-div" style="min-height:400px;">
		<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">

		<!-- 	<div class="panel panel-default">

				<div class="panel-heading">
					<h4 class="text-left" style="color : white;">Users List <a href="<?php echo site_url("users")?>" style="color: white; cursor: pointer;"><i class="fa fa-plus-circle pull-right"></i></a></h4>
				</div>
				<div class="panel-body set_height">
					
				</div>
			</div> -->
		</div>

		<div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
		<div class="panel panel-default">

		<div class="panel-heading">
			<h4 class="text-left" style="color: white;" id="form_head">Change Password</h4>
		</div>
		<div class="row">
<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){  ?>
			

			<div class="col-md-6 col-md-offset-4 col-xs-6 col-xs-offset-3">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-6 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				
				<?php } ?>
				</div>
		<div class="panel panel-body ">

							<?php
           $attributes = array(
           	'onsubmit' => 'return myFunction()',
              'id' => 'users',
              'name' => 'users',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes);  ?>




			<div class="row">

				<div class="col-xs-3 text-left">
					<label for="full_name">Full User Name<span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-6">
					<input type="text" class="form-control messagedata" id="full_name" name="full_name" placeholder="First Name Last Name" value="<?php if(isset($user_details[0]->Operator_Name)){ echo $user_details[0]->Operator_Name;} ?>"  readonly>
				</div>

			</div>
			<br/>
			<div class="row">
				<div class="col-xs-3 text-left">
					<label for="password">New Password<span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-6">
					<input type="password" class="form-control" id="password" name="password" placeholder="Password" value="" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>
				</div>
			</div>
				<br>
					<div class="row">
				<div class="col-xs-3 text-left">
					<label for="repassword">Retype New Password<span class="text-danger">*</span></label>
				</div>
				<div class="col-xs-6">
					<input type="password" class="form-control" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" id="repassword" name="repassword" placeholder="Re-type Password" value="" required>
				</div>
			</div>
<br>
	
<br>
<div id="message" style="display: none;">
  <h3>Password must contain the following:</h3>
  <p id="letter" class="invalid">Enter minimum 8 chars with atleast 1 number, lower, upper & special(@#$%&) characters</p>
</div>			

<div class="row">
			
		
			</div>
<br>
				<?php if($loginData->RoleId == '1' || $loginData->RoleId == 0 || $loginData->id_tblusers==$id_user){ ?>
				<div class="row">
					<div class="col-xs-12 text-center">
						<button type="submit" class="btn btn-warning" id="submit_btn">Change Password</button>
					</div>
				</div>
			<?php } ?>
			 <?php echo form_close(); ?>

		</div>
	</div>
</div>

	</div>

	<!-- Data Table -->
	<script src="<?php echo site_url('application/third_party');?>/js/crypto-js.min.js"></script>



<script type="text/javascript">

	$("#submit_btn").click(function(e){

		if($('#password').val().trim() == ""){

$("#password").css(error_css);
$("#password").focus();
e.preventDefault();
return false;
}
if($("#password").val() != $("#repassword").val())
			{
			$("#modal_header").text("Password does not match Re-type password");
			$("#modal_text").text("Passwords do not match");
			$("#repassword" ).val('');
			$("#multipurpose_modal").modal("show");
			return false;
		}
		


		var password = $("#password").val();
		var pattern = /^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/;
		if(pattern.test(password)){
		return true;


    }else{
    	$('#message').show();

    	$('#message').html('');

		$("#modal_header").text("Enter minimum 8 chars with atleast 1 number, lower, upper & special(@#$%&) characters");
		$("#multipurpose_modal").modal("show");
		return false;

    }

			if($("#password").val() != $("#repassword").val())
			{
				alert("Passswords do not match");
				e.preventDefault();
			}
			
		});
 function myFunction() 
        {           
          var password = $('#password').val();

          //var salt = $('#salt').val();
          $('#password').val(compute_hashed_password(password));
          $('#repassword').val(compute_hashed_password(password));
          return true;
        }

        function compute_hashed_password(password) 
        {
          var first_pass = CryptoJS.SHA256(password).toString();
          var second_pass = CryptoJS.SHA256(password).toString();
          //alert(first_pass+'/'+second_pass);
          //console.log(first_pass);
          return first_pass;
        }

		
	

	function isNumberKey(evt)
	{
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
	}
	$('.messagedata').keypress(function (e) {
        var regex = new RegExp(/^[a-zA-Z\s]+$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            return false;
        }
    });


		

		

</script>

<script type="text/javascript">

$(function(){
  $('li').hide();
});

</script>


</script>
