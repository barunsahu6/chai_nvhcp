<br>
<?php 
if($this->session->flashdata('msg'))
{
  echo '<div class="alert alert-success alert-dismissible show" role="alert">
  <p>'.$this->session->flashdata('msg').'
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
  </button></p>
  </div>';
}

if($this->session->flashdata('er_msg'))
{
  echo '<div class="alert alert-danger alert-dismissible show" role="alert">
  <p>'.$this->session->flashdata('er_msg').'
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
  </button></p>
  </div>';
}
?>

<div class="row main-div" style="min-height:448px;">
  <div class="col-md-4">
    <div class="panel panel-default">
      <div class="panel-heading">
      </div>
      <div class="panel-body">
        <form method="POST" action="">
          <div class="row">
            <div class="col-xs-4 text-center">
              <label for="usertype">Select LookUp</label>
            </div>
            <div class="col-xs-6">
              <select class="selectpicker form-control" data-toggle="dropdown" id="Flag" name="Flag" required>
                <option value="">--Select--</option>
                <option value="26">Risk Profile</option>  
                <option value="25">AntiHCV TestType</option>  
                <option value="34">SVR LFU</option> 
                <option value="35">LFU</option> 
                <option value="32">Interrupted</option> 
                <option value="28">Treatment duration</option>            
              </select>
            </div>
            <div class="col-xs-2">
              <button type="submit" name="submit" class="btn btn-warning">GO</button>
            </div>          
          </div>      
        </form>

      </div>
    </div>
    

    <div class="panel panel-default">

      <div class="panel-heading">
        <h4 class="text-center" style="color : white;">LookUp List <a href="<?php echo site_url("Lookup")?>" style="color: white; cursor: pointer;"><i class="fa fa-plus-circle pull-right"  data-toggle="tooltip" title="Add LookUp"></i></a></h4>
      </div>
      <div class="panel-body set_height">

        <table id="data-table-command" class="table table-striped table-bordered table-vmiddle table-hover">
          <thead>
            <tr>
              <th class="text-center">Sl No</th>
              <th class="text-center">LookUp</th>
              <th class="text-center">Commands</th>
            </tr>

          </thead>
          <tbody>
            <?php 
            if(count($list_lookup)>0){
            $count = 0;
            foreach($list_lookup as $row){ 
              $count +=1
              ?>
              <tr>
                <td class="text-center" style="padding-top: 16px;"><?php echo $count; ?></td>
                <td class="text-center" style="padding-top: 16px;"><?php echo $row->LookupValue; ?></td>
                <td class="text-center">
                  <a href="<?php echo site_url('Lookup/index/'.$row->id_mstlookup);?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
                  <a href="<?php echo site_url('Lookup/delete/'.$row->id_mstlookup);?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
                </td>

              </tr>

              <?php } } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
      <div class="panel panel-default">

        <div class="panel-heading">
          <h4 class="text-center" style="color: white;" id="form_head">Add LookUp</h4>
        </div>

        <div class="panel panel-body ">
          <form role="form" method="post" action="">
            <div class="row">

              <div class="col-xs-2 text-center">
                <label for="occupation">LookUp Name</label>
              </div>
                <div class="col-xs-4">
                <input type="text" class="form-control" id="lookup_name" name="lookup_name" placeholder="Enter LookUp name" value="<?php if(isset($selected_lookup->LookupValue)){ echo $selected_lookup->LookupValue;} ?>" required>
              </div>
              <div class="col-xs-6 text-center">
                <button type="submit" class="btn btn-warning" id="submit_btn">Add New LookUp</button>
              </div>
            </div>

          </form>

        </div>
      </div>
    </div>

  </div>
  <!-- Data Table -->
  <script type="text/javascript">
    $(document).ready(function(){

      $(".glyphicon-trash").click(function(e){
        var ans = confirm("Are you sure you want to delete?");
        if(!ans)
        {
          e.preventDefault();
        }
      });

      var edit_flag = <?php echo $edit_flag; ?>;
      if(edit_flag == 1)
      {
        $("#form_head").text("Edit Lookup Name");
        $("#submit_btn").text("Update LookUp Name");
      }
    });
  </script>