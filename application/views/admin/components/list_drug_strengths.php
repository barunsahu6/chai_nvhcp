<br>
<?php 
if($this->session->flashdata('msg'))
{
	echo '<div class="alert alert-success alert-dismissible show" role="alert">
	<p>'.$this->session->flashdata('msg').'
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</button></p>
	</div>';
}

if($this->session->flashdata('er_msg'))
{
	echo '<div class="alert alert-danger alert-dismissible show" role="alert">
	<p>'.$this->session->flashdata('er_msg').'
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</button></p>
	</div>';
}

?>

<div class="row main-div" style="min-height:448px;">
	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="text-center" style="color : white;">Drug List <a href="<?php echo site_url("drugs")?>" style="color: white; cursor: pointer;"><i class="fa fa-plus-circle pull-right"  data-toggle="tooltip" title="Add Drug"></i></a></h4>
			</div>
			<div class="panel-body">
				<table id="data-table-command" class="table table-striped table-bordered table-vmiddle table-hover">
						<thead>
							<tr>
								<th class="text-center">S.no</th>
								<th class="text-center">Drug Name</th>
								<th class="text-center">ABBR.</th>
								<!-- <th class="text-center">Buffer Stock</th> -->
							</tr>

						</thead>
						<tbody id="table_drug_list">
							<?php

							$sno = 1;

							foreach ($drugs as $drug) {
							
							?>
								
							 <tr style="cursor: pointer;" data-drug_id=<?php echo $drug->id_mst_drugs; ?> >
							 	<td class="text-center"><?php echo $sno; ?></td>
							 	<td class="text-left"><?php echo $drug->drug_name; ?></td>
							 	<td class="text-left"><?php echo $drug->drug_abb; ?></td>
							 	<!-- <td class="text-left"><?php //echo $drug->buffer_stock; ?></td> -->
							 </tr>
							<?php
							$sno++;
							}
							 ?>

						</tbody>
					</table>
			</div>
		</div>
	</div>

	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="text-center" style="color : white;">Add Strength</h4>
			</div>
			<div class="panel-body">
				<form method="POST" action="drug_strengths/add" onsubmit=" return validate();">

					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

					<div class="row">
						<!-- <div class="col-md-2 col-md-offset-3">
							<label>Drug Name</label>	
						</div> -->
						<div class="col-md-6 col-md-offset-3">
							<h4 id="drug_name" class="text-center">{Drug Name}</h4>
							<input type="hidden" name="id_mst_drugs" id="id_mst_drugs">
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<a class="btn btn-primary btn-block" onclick="addStrength(this)"><i class="fa fa-plus"></i> Add Strength</a>
						</div>
					</div>
					<br>
					<div class="row">
						
						<div class="col-md-6 col-md-offset-3">

						<table class="table table-bordered table-striped table-hover">
							<thead>
								<th>Sno</th>
								<th>Strength</th>
								<th>Unit</th>
								<th>Buffer Stock</th>
								<th>Commands</th>
							</thead>
							<tbody class="table_strengths">

							</tbody>	
						</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<input type="submit" class="btn btn-warning btn-block" name="" value="Save">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	var total_strengths = 0;
	var sno = 0;
		$(document).ready(function(){

		$("#table_drug_list tr").click(function(){
			var drug_id = $(this).data('drug_id');
			$("#drug_name").text('');
			$(".table_strengths").html('');
			
			$.ajax({
				url : "<?php echo base_url(); ?>drug_strengths/getstrengths/"+drug_id,
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : "GET"
			}).done(function(result){

				var drug_data = JSON.parse(result);

				total_strengths = drug_data['strength_details'].length;
				sno = drug_data['strength_details'].length;

				$("#drug_name").text(drug_data['drug_details'][0].drug_name);
				$("#id_mst_drugs").val(drug_data['drug_details'][0].id_mst_drugs);


				for(strength in drug_data['strength_details'])
				{
					$(".table_strengths").append('<tr>\
													<td class="text-center">'+(parseInt(strength)+1)+'</td>\
													<td class="text-center"><input type="text" class="form-control" name="" value="'+drug_data['strength_details'][strength].strength+'" readonly></td>\
													<td><input type="text" class="form-control" name="Unit" id="Unit" value="'+drug_data['strength_details'][strength].unit+'" readonly></td>\
													<td class="text-center">\
													<input type="number" class="form-control" name="buffer_stock"  value="'+drug_data['strength_details'][strength].buffer_stock+'" readonly></td>\
													</td>\
													<td class="text-center">\
														<a href="<?php echo base_url(); ?>drug_strengths/edit/" style="padding : 4px;">\
															<span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span>\
														</a>\
													</td>\
												</tr>');
				}

			});
		});
	});

	function addStrength()
	{	

		sno++;
		$(".table_strengths").append('<tr class="new_strength_row">\
													<td class="text-center">'+sno+'</td>\
													<td class="text-center"><input type="number" class="form-control" name="drug_strength['+sno+']" value="" required></td>\
													<td><input type="text" name="Unit['+sno+']" id="Unit"></td>\
													<td class="text-center"><input class="form-control" type="number" name="buffer_stock['+sno+']" id="buffer_stock['+sno+']" ></td>\
													<td class="text-center" style="cursor : pointer" onclick="remove_row(this)"><i class="fa fa-times fa-2x"></i></td>\
												</tr>');

	}

	function remove_row(elem)
	{
		$(elem).parent('tr').remove();
	}

	function validate()
	{

		if($("#drug_name").text() == '{Drug Name}')
		{

			alert('Please select a Dug First...');
			return false;
		}

		return true;

	}
</script>