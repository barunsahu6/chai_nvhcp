<style>
thead
{
	background-color: #085786;
	color: white;
}
.td_input{width: 100%;padding: 0px;border-radius: 2px;height: 40px;font-size:14px;font-weight: 600;border-width: 0px;border-style: none;}
.table-bordered>tbody>tr>td.data_td
{
	font-weight: 600;
	font-size: 15px;
	text-align: center;
	vertical-align: middle;
	background-color: #e4e4e4;
	border-color:#ffffff;
	border-width:1px;
	color:#000000;
}
.table-bordered>tbody>tr>td.data_td_late
{
	font-weight: 600;
	font-size: 15px;
	background-color: #ffffff;
	text-align: center;
	vertical-align: middle;
	padding: 0px;
}
.table-bordered>tbody>tr>td.total,.table-bordered>tbody>tr>td.td_total
{
	font-weight: 600;
	font-size: 16px;
	background-color: #ffffff;
	text-align: center;
	vertical-align: middle;
	padding: 0px;
}
</style>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 3 !important;
	width: 100% !important;
	font-size: 14px !important;
	font-family: 'Source Sans Pro';
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
	.col-md-2{
		width: 16.33%;
		padding-left: 8px;
		padding-right: 8px;
	}
	.btn-width{
	width: 12%;
	margin-top: 20px;
}
.pd-15{
	padding-left: 15px;
}
.pb-20{
	padding-bottom: 20px;
}
.container{
	width: 76%;
}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 4 !important;
		width: 100% !important;
		font-size: 14px !important;
		font-family: 'Source Sans Pro';
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 7px 12px;
	margin-bottom: 0;
	font-size: 13px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
 .table-bordered>thead>tr>th{
	vertical-align: middle;
	font-size: 13px;
}
.table-bordered>tbody>tr>td{
	vertical-align: middle;
	font-size: 13px;
	color: #000000;
}
</style>
</style>
<br>

<?php $loginData = $this->session->userdata('loginData');
//echo "<pre>"; print_r($loginData);

if($loginData->user_type==1) { 
$select = '';
}else{
$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
$select4 = '';	
}

$filters1 = $this->session->userdata('filters1'); 
//pr($filters1);
//echo $get_info_monthly_record->hcv_screened;
 ?>
<div class="container">
		<div class="panel panel-default" id="Monthly_Report_Panel">
				<div class="panel-heading" style="background-color: #333333;padding: 3px 0px;">
					<h4 class="text-center" style="background-color: #333333;color: white; font-family: 'Source Sans Pro';letter-spacing: .75px;">Offline Monthly Records Entry (HCV)
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/excel_black.png" alt=""></a> -->
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/pdf_color.png" alt=""></a> -->
		</h4>
		<!-- <input type="button" onclick="printDiv('printableArea')" value="Print" /> -->
		
</div>

<?php
           $attributes = array(
              'id' => 'filter_form',
              'name' => 'filter_form',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>
<div class="row" style="padding: 5px 0px 5px 15px;">

	<div class="col-md-2 col-sm-12 col-md-offset-2">
		<label for="">State</label>
		<select type="text" name="search_state" id="search_state" class="form-control input_fields" required <?php echo $select2.$select3.$select4; ?>>
			<option value="0">All States</option>
			<?php 
			foreach ($states as $state) {
				?>
				<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo $state->StateName; ?></option>
				<?php 
			}
			?>
		</select>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">Year</label>
		<select name="year" id="year" required class="form-control input_fields" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2020', date('Y'));
										$flag=0;
										foreach($dates as $date){
												$year = $date;
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									elseif($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
															
								}
								?>
							</select>
	</div>

	<div class="col-md-2 col-sm-12">
		<label for="">Month</label>
		<select type="text" name="month" id="month" class="form-control input_fields" required>
<option value="">Select</option>
<?php
	$flag=0;
for ($i = 1; $i <=12;$i++) {
$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
if($this->input->post('month')==$i){
echo "<option value=".$i." selected>".$date_str ."</option>";
$flag=1;
}
elseif($i==date('m') && $flag==0){
	echo "<option value=".$i." selected>".$date_str ."</option>";
}
else{
	echo "<option value=".$i.">".$date_str ."</option>";

}
} ?>
</select>
	</div>

<div class="col-lg-2 col-md-2 col-sm-12 btn-width" style="margin-top: 20px;">
					<button type="submit" class="btn btn-block btn-success" id="search" name="search" value="search" style="line-height: 1.2; font-weight: 600;">SEARCH</button>
				</div>
				<!-- <?php //if (!((date('m')==$this->input->post('month') && date('Y')!=$this->input->post('year')) || (date('m')!=$this->input->post('month')&& date('d')>5))) {
					?> -->
					
				
				<span style="margin-top: 20px;margin-right: 25px; float: right;width: 80px;" id="span_save"><button type="submit" class="btn btn-block btn-success" id="save" name="save" value="save" style="line-height: 1.5; font-weight: 600;">Save</button></span>
				<!-- <?php // } ?> -->
			</div>

 
<br>
</div>
<div class="row" id="printableArea">

	<div class="col-md-12">
		<table class="table table-bordered table-highlighted" id="testTable" >
<tbody>
	<!-- <tr>
				<th style="background-color: #ffcccb;color: black; font-family:'Source Sans Pro';font-size: 14px;text-align: center;font-weight: 500" colspan="7">Please input data from <?php echo date("F-Y", strtotime( $filters1['startdate1'])); ?> to  <?php echo date("F-Y", strtotime($filters1['enddate'])); ?></th>
				
			</tr> -->
			<tr>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';">Total number of persons screened for Hepatitis C</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Total number of persons found positive for Hepatitis C (Anti-HCV)</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Total number of patients found eligible for treatment</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Total number of patients put on treatment</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Total number of Hepatitis C patients completed treatment</th>
			</tr>
			

				<tr style="font-family:'Source Sans Pro';" class="td_row">
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_screened" id="hcv_screened" value="<?php echo !empty($get_info_monthly_record->hcv_screened)>0 ? $get_info_monthly_record->hcv_screened : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="hcv_positive" id="hcv_positive" value="<?php echo !empty($get_info_monthly_record->hcv_positive)>0 ? $get_info_monthly_record->hcv_positive : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="eligible_for_treatment" id="eligible_for_treatment" value="<?php echo !empty($get_info_monthly_record->eligible_for_treatment)>0 ? $get_info_monthly_record->eligible_for_treatment : '0'; ?>"></td>
					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="treatment_start" id="treatment_start" value="<?php echo !empty($get_info_monthly_record->treatment_start)>0 ? $get_info_monthly_record->treatment_start : '0'; ?>"></td>

					<td class="data_td_late"><input class="form-control text-center td_input" type="text" name="treatment_completed" id="treatment_completed" value="<?php echo !empty($get_info_monthly_record->treatment_completed)>0 ? $get_info_monthly_record->treatment_completed : '0'; ?>"></td>
						
				</tr>
			</tbody>
		</table>
		<?php echo form_close(); ?>
	</div>
</div>
</div>
<br>
<br>
<br>
<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name,filename) {
    if (!table.nodeType) table = document.getElementById(table)
            var newtable=document.createElement("table");
            var tbody =document.createElement("tbody");
             var row = document.createElement("tr");
             row.setAttribute("style", "background-color: black;color: white; font-family:'Source Sans Pro';");
  				var cell1 = document.createElement("td");
  				cell1.colSpan=7;
  				cell1.innerHTML ="Date as on : - <?php echo date('jS \ F Y'); ?>";
  				row.appendChild(cell1);
 			 	tbody.appendChild(row);
 			 	newtable.appendChild(tbody);
             var clonedTable=newtable.cloneNode(true);
             var clonedTable1=table.cloneNode(true);
            clonedTable.appendChild(clonedTable1);
    var ctx = {worksheet: name || 'Worksheet', table: clonedTable.innerHTML}
    //window.location.href = uri + base64(format(template, ctx))
     var link = document.createElement('a');
    link.download =filename;
    link.href = uri + base64(format(template, ctx));
    link.click();
     clonedTable.remove();
  }
})()
</script>
<script>

	
$(document).ready(function(){
var td_input_val=0;
$('[data-toggle="tooltip"]').tooltip();
$(".td_input").attr('min',0);
$("#search_state").trigger('change');


$('.td_input').click(function(){
	var error_css = {'background-color':'rgb(227, 245, 251)'};
	$(this).css(error_css);
	td_input_val=$(this).val();
	if ($(this).val()==0) {
		$(this).val('');
	}
});
$('.td_input').blur(function(){
	var error_css = {'background-color':'rgb(255, 255, 255)'};
	if ($(this).val()==0 || $(this).val()=='') {
		$(this).val(0);
	}
	$(this).css(error_css);
});
	$(".td_input").on('keypress',function(evt){
								var charCode = (evt.which) ? evt.which : event.keyCode
								
								if ((charCode > 31 && (charCode < 48 || charCode > 57))){
									evt.preventDefault();
									return false;
								}
								
								var index = $(".td_input").index(this);
                        		if( $('.td_input').eq(index).val() < 0 || $('.td_input').eq(index).val() >999999)
								{
									//alert(index);
									$("#modal_header").html('Value cannot be less than 0 or greater than 1000000');
									$("#modal_text").html('Please fill in appropriate Value');
									$("#multipurpose_modal").modal("show");
									$('.td_input').eq(index).val('');
									$(".td_input").trigger('keyup'); 
									return false;
								}
								

    							/*var cell = $(this).closest('td');
								var cellIndex = cell[0].cellIndex;*/

								return true;

							});



	


	

$('#year').change(function(){
	var count=0;
	<?php  $count=0; ?>;
	var options='<option value="">Select</option>';
	var currentYear = (new Date).getFullYear();
	if (currentYear==$('#year').val()) {
			<?php
		$flag=0;
		$count=date("m");
	for ($i = 1; $i <=$count;$i++) {
	$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
	if($this->input->post('month')==$i){ 	?>
	options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php $flag=1;
	}	
	elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php }
	else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	}
	else{
		<?php
		$flag=0;
		$count=12;
		for ($i = 1; $i <=$count;$i++) {
		$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
		if($this->input->post('month')==$i){ 	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php $flag=1;
		}	
		elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php }
		else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	
	}
	//alert(options);
$('#month').html(options);
$('#month').trigger('change');
});

$('#month').change(function(e){
var month=$('#month').val();
var year=$('#year').val();
//$clone = $('#span_save').clone( true )
var currentYear = (new Date).getFullYear();
var currentMonth=(new Date).getMonth()+1;
var currentdate = (new Date).getDate();
/*if (!((currentMonth==month && currentYear!=year) || (currentMonth!=month && currentdate>5))){
	
}
else{
	$('#span_save').hide();
	$('.td_input').prop("readonly", true);
	$('.td_input').off( "click");
}*/

if((currentMonth==month && currentYear==year)|| month==(currentMonth-1) && currentYear==year){
	$('#span_save').show();
	$('.td_input').prop("readonly", false);
	$('.td_input').on( "click");
}
else{
	$('#span_save').hide();
	$('.td_input').prop("readonly", true);
	$('.td_input').off( "click");
}

});

$("#year").trigger('change');
$(".td_input").trigger('change');

});
$('#search_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');
					//$('#input_district').val($('#input_district').val());
				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
					$('#mstfacilitylogin').html(data);
					
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

</script>