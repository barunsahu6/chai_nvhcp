<br>
<?php 
if($this->session->flashdata('msg'))
{
	echo '<div class="alert alert-success alert-dismissible show" role="alert">
	<p>'.$this->session->flashdata('msg').'
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</button></p>
	</div>';
}

if($this->session->flashdata('er_msg'))
{
	echo '<div class="alert alert-danger alert-dismissible show" role="alert">
	<p>'.$this->session->flashdata('er_msg').'
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
	</button></p>
	</div>';
}

?>
<style type="text/css">
	.form-control{
		height: 30px;
	}
</style>
<div class="row main-div" style="min-height:448px;">
	<div class="col-md-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="text-center" style="color : white;">Commodities List <a href="<?php echo site_url("drugs")?>" style="color: white; cursor: pointer;"><i class="fa fa-plus-circle pull-right"  data-toggle="tooltip" title="Add Commoditiy"></i></a></h4>
			</div>
			<div class="panel-body">
				<table id="data-table-command" class="table table-striped table-bordered table-vmiddle table-hover">
						<thead>
							<tr>
								<th class="text-center">S.no</th>
								<th class="text-center">Commodity Name</th>
								<th class="text-center">ABBR.</th>
								<th class="text-center">Commands</th>
							</tr>

						</thead>
						<tbody id="table_drug_list">
							<?php

							$sno = 1;

							foreach ($drugs as $drug) {
							
							?>
								
							 <tr>
							 	<td class="text-center"><?php echo $sno; ?></td>
							 	<td class="text-left"><?php echo $drug->drug_name; ?></td>
							 	<td class="text-left"><?php echo $drug->drug_abb; ?></td>
							 	<td class="text-center"><a href="javascript:void(0);" style="padding : 4px;" onclick="get_all_data('<?php echo $drug->id_mst_drugs; ?>')" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
							 		<a href="<?php echo site_url('drugs/delete/'.$drug->id_mst_drugs);?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a></td>
							 </tr>
							<?php
							$sno++;
							}
							 ?>

						</tbody>
					</table>
			</div>
		</div>
	</div>

	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="text-center" style="color : white;">Add Commodity</h4>
			</div>
			<div class="panel-body">
				<form method="POST" action="<?php echo (isset($drug_details))?'':'drugs/add'; ?>">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					<div class="row">
						<div class="col-md-2">
							<label>Commodity Name</label>	
						</div>
						<div class="col-md-4">
							<input type="text" class="form-control" id="drug_name_input" name="drug_name" value="<?php echo (isset($drug_details))?$drug_details[0]->drug_name:''; ?>">
						</div>
						<div class="col-md-2">
							<label>Commodity Abbreviation</label>
						</div>
						<div class="col-md-4">
							<input type="text" class="form-control" id="drug_abb" name="drug_abb" value="<?php echo (isset($drug_details))?$drug_details[0]->drug_abb:''; ?>">
						</div>
					</div>
					 <div class="row">
						<div class="col-md-2">
							<label>Commodity Type</label>	
						</div>
						<div class="col-md-4">
							<select name="type" id="type" required class="form-control" style="height: 30px;">
								<option>--Select--</option>
								<?php foreach ($item_mst_look as $value) { ?>

									<option value="<?php echo $value->LookupCode;?>" <?php if($this->input->post('type')==$value->LookupCode) { echo 'selected';}?>>
												<?php 
													echo $value->LookupValue;
												?></option>
									<?php } ?>
								</select>
						</div>
						<div class="col-md-2 category">
							<label>Commodity Category</label>
						</div>
						<div class="col-md-4 category">
							<select name="category" id="category" required class="form-control" style="height: 30px;">
								<option>--Select--</option>
								<?php foreach ($drug_type as $value) { ?>

									<option value="<?php echo $value->LookupCode;?>" <?php if($this->input->post('category')==$value->LookupCode) { echo 'selected';}?>>
												<?php 
													echo $value->LookupValue;
												?></option>
									<?php } ?>
								</select>
						</div>
					</div> 
					<br>
					<div class="row">
						<div class="col-md-2 col-md-offset-5">
							<input type="submit" class="btn btn-warning" name="" value="Save">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="text-center" style="color : white;">Add Strength</h4>
			</div>
			<div class="panel-body">
				 <?php  $attributes = array(
              'id' => 'drug_strengths_form',
              'name' => 'drug_strengths_form',
               'autocomplete' => 'off',
            ); 
             echo form_open('drugs/add_strength', $attributes); ?>
					<div class="row">
						<!-- <div class="col-md-2 col-md-offset-3">
							<label>Drug Name</label>	
						</div> -->
						<div class="col-md-6 col-md-offset-3">
							<h4 id="drug_name" class="text-center"></h4>
							<input type="hidden" name="id_mst_drugs" id="id_mst_drugs">
							<input type="hidden" name="type_val" id="type_val">
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<a class="btn btn-primary btn-block" onclick="addStrength(this)"><i class="fa fa-plus"></i> Add Strength</a>
						</div>
					</div>
					<br>
					<div class="row">
						
						<div class="col-md-6 col-md-offset-3">

						<table class="table table-bordered table-striped table-hover">
							<thead>
								<th>Sno</th>
								<th>Strength</th>
								<th>Unit</th>
								<th>Commands</th>
							</thead>
							<tbody class="table_strengths">

							</tbody>	
						</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<input type="submit" class="btn btn-warning btn-block" name="" value="Save">
						</div>
					</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	
	var total_strengths = 0;
	var sno = 0;
		$(document).ready(function(){

		
	});
function get_all_data(drug_id){
			//var drug_id = $(this).data('drug_id');
			$("#drug_name").text('');
			$(".table_strengths").html('');
			
			$.ajax({
				url : "<?php echo base_url(); ?>drugs/getstrengths/"+drug_id,
				method : "GET"
			}).done(function(result){

				var drug_data = JSON.parse(result);
				//console.log(drug_data['drug_details'][0].type);
				total_strengths = drug_data['strength_details'].length;
				sno = drug_data['strength_details'].length;

				$("#drug_name").text(drug_data['drug_details'][0].drug_name);
				$("#drug_name_input").val(drug_data['drug_details'][0].drug_name);
				$("#drug_abb").val(drug_data['drug_details'][0].drug_abb);
				$("#type").val(drug_data['drug_details'][0].type);
				$("#type_val").val(drug_data['drug_details'][0].type);
				$("#category").val(drug_data['drug_details'][0].category);
				$("#type").trigger('change');
				$("#id_mst_drugs").val(drug_data['drug_details'][0].id_mst_drugs);


				for(strength in drug_data['strength_details'])
				{
					$(".table_strengths").append('<tr>\
													<td class="text-center">'+(parseInt(strength)+1)+'</td>\
													<td class="text-center"><input type="text" class="form-control" name="drug_strength[]" value="'+drug_data['strength_details'][strength].strength+'" ></td>\
													<td>'+((drug_data['strength_details'][strength].type!=2) ? (drug_data['strength_details'][strength].unit) : "")+'</td>\
													<td></td>\
												</tr>');
				}

			});
		}
	function addStrength()
	{	
var type=$("#type").val();
var unit=(type==1 ? "mg" : (type==2 ? '' : "ml"));
		sno++;
		$(".table_strengths").append('<tr class="new_strength_row">\
													<td class="text-center">'+sno+'</td>\
													<td class="text-center"><input type="number" class="form-control" name="drug_strength[]" value="" required></td>\
													<td>'+unit+'</td>\
													<td class="text-center" style="cursor : pointer" onclick="remove_row(this)"><i class="fa fa-times fa-2x"></i></td>\
												</tr>');

	}

	function remove_row(elem)
	{
		$(elem).parent('tr').remove();
	}

	function validate()
	{

		if($("#drug_name").text() == '{Drug Name}')
		{

			alert('Please select a Dug First...');
			return false;
		}

		return true;

	}
	 $("#type").change(function() {
        $("#category").children('option').hide();
        if ($(this).val()==1) {
        	$("#category").children("option[value=1]").show();
        	$("#category").children("option[value=2]").show();
        }
        else if ($(this).val()==2) {

        	$("#category").children("option[value=3]").show();
        	$("#category").children("option[value=4]").show();
        	$("#category").children("option[value=5]").show();
        }
        else{
        	 $("#category").children('option').show();
        }
    });
</script>
