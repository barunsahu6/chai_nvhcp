<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('pages/includes/header'); ?>
<?php $this->load->view('pages/includes/menubar'); ?>
<?php $this->load->view('admin/components/'.$subview); ?>

<!-- multipurpose modal -->

<?php $error = $this->session->flashdata('error'); ?>
<div class="modal" data-easein="bounceIn" tabindex="-1" role="dialog" id="multipurpose_modal">
	<div class="modal-dialog" role="document" style="position: relative; top : 30vh;">
		<div class="modal-content">
			<div class="row">
				<!-- <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-xs-8 col-xs-offset-2"> -->
				<div class="col-lg-12">
					<div class="modal-header">
						<button class="btn btn-success btn-block form_buttons" href="" style="color: white;" data-dismiss="modal" aria-label="Close" id="modal_header"><?php echo isset($error)?$error['header']:'Information'; ?></button>

					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12 text-center">
								<i id="modal_text"><?php echo isset($error)?$error['message']:'Error Text'; ?></i>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<script>

		$('.hasCal').each(function() {
        ddate=$(this).val();
        yRange="1990:"+new Date().getFullYear();
        yr=$(this).attr('year-range');
        if(yr){
            yRange=yr;
        }
        
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
            changeMonth: true,
            yearRange: yRange,
            maxDate: 0,
        });
        $(this).attr('placeholder','dd-mm-yy');
        if($(this).attr('noweekend')){
            $(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
        }
        $(this).datepicker( "setDate", ddate);
    });
		
	$(".modal").each(function(l){$(this).on("show.bs.modal",function(l){var o=$(this).attr("data-easein");"shake"==o?$(".modal-dialog").velocity("callout."+o):"pulse"==o?$(".modal-dialog").velocity("callout."+o):"tada"==o?$(".modal-dialog").velocity("callout."+o):"flash"==o?$(".modal-dialog").velocity("callout."+o):"bounce"==o?$(".modal-dialog").velocity("callout."+o):"swing"==o?$(".modal-dialog").velocity("callout."+o):$(".modal-dialog").velocity("transition."+o)})});
</script>

<script type="text/javascript">
	
function checkdate(){
		var startdate = $('#startdate').datepicker('getDate');
		var enddate = $('#enddate').datepicker('getDate');
		if ( startdate >= enddate && startdate!=null) { 
			$("#modal_header").text("From Date cannot be before/Equal To Date");
			$("#modal_text").text("Please check Dates");
			$("#startdate" ).val('');
			$("#multipurpose_modal").modal("show");
			return false;
		}
		else{
			return true;
		}
	}

	$('.hasCal').each(function() {
		ddate=$(this).val();
		var expdate = new Date().getFullYear()+5;
		yRange="1990:"+ expdate;
		yr=$(this).attr('year-range');
		if(yr){
			yRange=yr;
		}

		$(this).datepicker({
			dateFormat: "dd-mm-yy",
			changeYear: true,
			changeMonth: true,
			yearRange: yRange,
//maxDate: 0,
});
		$(this).attr('placeholder','dd-mm-yy');
		if($(this).attr('noweekend')){
			$(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
		}
		$(this).datepicker( "setDate", ddate);
	});
	$('.hasCal_Receipt').each(function() {
		ddate=$(this).val();
		var expdate = new Date().getFullYear();
		yRange="1990:"+ expdate;
		yr=$(this).attr('year-range');
		if(yr){
			yRange=yr;
		}

		$(this).datepicker({
			dateFormat: "dd-mm-yy",
			changeYear: true,
			changeMonth: true,
			yearRange: yRange,
			maxDate: 0,
		});
		$(this).attr('placeholder','dd-mm-yy');
		if($(this).attr('noweekend')){
			$(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
		}
		$(this).datepicker( "setDate", ddate);
	});	
</script>


<?php $this->load->view('admin/includes/footer'); ?>