
    <style>
        .alert {
            position: absolute;
            padding: .75rem 1.25rem;
            padding-right: 1.25rem;
            margin-bottom: 1rem;
            border: 1px solid transparent;
            border-top-color: transparent;
            border-right-color: transparent;
            border-bottom-color: transparent;
            border-left-color: transparent;
            border-radius: .25rem;
            left: -15px;
            width: 72px;
            height: auto;
            top: 180px;
            z-index: 100;
        }

        .alert-info {
            color: #0c5460;
            background-color: transparent;
            border-color: transparent;
        }

        .alert-dismissible .close {
            padding: 0rem 0rem;
            background-color: #ccc;
            width: 82%;
        }

        .card {
            border: 0px solid;
        }

        .btn-outline-info:hover {
            color: #fff;
            background-color: #17a2b8 !important;
            border-color: #17a2b8;
        }

        .img-set {
            padding-top: 20px;
            width: 60%;
            margin-left: 20%;
        }

        .img-set2 {
            padding-top: 20px;
            width: 20%;
            margin-left: 40%;
        }

        .btn-outline-success {
            color: #ee82ee;
            border-color: #ee82ee;
        }

        .btn-outline-success:hover {
            color: #fff;
            background-color: #ee82ee;
            border-color: #ee82ee;
        }

        .cad {
            color: inherit !important;
        }








        .MultiCarousel {
            float: left;
            overflow: hidden;
            padding: 15px;
            width: 100%;
            position: relative;
        }

        .MultiCarousel .MultiCarousel-inner {
            transition: 1s ease all;
            float: left;
        }

        .MultiCarousel .MultiCarousel-inner .item {
            float: left;
        }

        .MultiCarousel .MultiCarousel-inner .item>div {
            padding: 10px;
            margin: 10px;
            /* background: #f1f1f1; */
            color: #666;
            box-shadow: 0px 0px 10px #ccc;
        }

        .MultiCarousel .leftLst,
        .MultiCarousel .rightLst {
            position: absolute;
            border-radius: 50%;
            top: calc(50% - 20px);
        }

        .MultiCarousel .leftLst {
            left: 0;
        }

        .MultiCarousel .rightLst {
            right: 0;
        }

        .MultiCarousel .leftLst.over,
        .MultiCarousel .rightLst.over {
            pointer-events: none;
            background: #ccc;
        }

        .slid-btn {
            color: #fff;
            text-decoration: underline !important;
            font-size: 12px;
            position: absolute;
            bottom: 15px;
        }

        .bg-success {
            background-color: #26a591 !important;
        }

        .bg-warning {
            background-color: #ff7918 !important;
        }






        .accordion .card-header {
            cursor: pointer;
        }

        .accordion.heading-right .card-title {
            position: absolute;
            right: 50px;
        }

        .accordion.indicator-plus .card-header:after {
            font-family: 'FontAwesome';
            content: "\f068";
            float: right;
        }

        .accordion.indicator-plus .card-header.collapsed:after {
            content: "\f067";
        }

        .accordion.indicator-plus-before.round-indicator .card-header:before {
            font-family: 'FontAwesome';
            font-size: 14pt;
            content: "\f056";
            margin-right: 10px;
        }

        .accordion.indicator-plus-before.round-indicator .card-header.collapsed:before {
            content: "\f055";
            color: #fbfbfb;
            float: right;
        }

        .accordion.indicator-plus-before .card-header:before {
            font-family: 'FontAwesome';
            content: "\f068";
            float: right;
        }

        .accordion.indicator-plus-before .card-header.collapsed:before {
            content: "\f067";
        }

        .accordion.indicator-chevron .card-header:after {
            font-family: 'FontAwesome';
            content: "\f078";
            float: right;
        }

        .accordion.indicator-chevron .card-header.collapsed:after {
            content: "\f077";
        }

        .accordion.background-none [class^="card"] {
            background: transparent;
        }

        .accordion.border-0 .card {
            border: 0;
        }

        .h-c {
            background-color: #ff7918 !important;
            color: #fff;
        }

        .cb-c {
            border: 1px solid #ddd;
            background-color: bisque;
            height: 180px;
            overflow: auto;
        }

        .cb-cc {
            height: 420px;
            overflow: auto;
            background-color: #00808021;
        }
    </style>

        <!-- Navbar end -->

        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                        <div class="accordion indicator-plus-before round-indicator" id="accordionH"
                            aria-multiselectable="true">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12"
                                    style="float: left;margin-bottom: 10px;">
                                    <div class="card-header collapsed h-c" role="tab" id="headingOneH"
                                        href="#collapseOneH" data-toggle="collapse" data-parent="#accordionH"
                                        aria-expanded="false" aria-controls="collapseOneH">
                                        <a class="card-title">PRINT</a>
                                    </div>
                                    <div class="collapse show" id="collapseOneH" role="tabpanel"
                                        aria-labelledby="headingOneH">
                                        <div class="card-body cb-c">
                                            <li><a href="<?php echo base_url(); ?>common_libs/iec/Hepatitis-B-and-C-prevention-Tatoo-Hindi-1.pdf" target="_blank" style="font-size:12px;">Hepatitis B and C prevention Tatoo Hindi 1</a></li>
                                             <li><a href="<?php echo base_url(); ?>common_libs/iec/Hepatitis-B-and-C-prevention-Tatoo-Hindi-2.pdf" target="_blank" style="font-size:12px;">Hepatitis B and C prevention Tatoo Hindi 2</a></li>
                                              <li><a href="<?php echo base_url(); ?>common_libs/iec/Hepatitis-C-Modes-of-Transmission.pdf" target="_blank" style="font-size:12px;">Hepatitis C Modes of Transmission</a></li>
                                              <li><a href="<?php echo base_url(); ?>common_libs/iec/Hepatitis-B-MTCT.pdf" target="_blank" style="font-size:12px;">Hepatitis B MTCT</a></li>
                                              <li><a href="<?php echo base_url(); ?>common_libs/iec/Hepatitis-B-and-C-prevention-Tatoo-English.pdf" target="_blank" style="font-size:12px;">Hepatitis B and C prevention Tatoo English</a></li>
                                        </div>
                                       
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12"
                                    style="float: left;margin-bottom: 10px;">
                                    <div class="card-header collapsed h-c" role="tab" id="headingTwoH"
                                        href="#collapseTwoH" data-toggle="collapse" data-parent="#accordionH"
                                        aria-expanded="false" aria-controls="collapseTwoH">
                                        <a class="card-title">AUDIO</a>
                                    </div>
                                    <div class="collapse show" id="collapseTwoH" role="tabpanel"
                                        aria-labelledby="headingTwoH">
                                        <div class="card-body cb-c">
                                            Content will be available soon !
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12"
                                    style="float: left;margin-bottom: 10px;">
                                    <div class="card-header collapsed h-c" role="tab" id="headingThreeH"
                                        href="#collapseThreeH" data-toggle="collapse" data-parent="#accordionH"
                                        aria-expanded="false" aria-controls="collapseThreeH">
                                        <a class="card-title">VIDEO</a>
                                    </div>
                                    <div class="collapse show" id="collapseThreeH" role="tabpanel"
                                        aria-labelledby="headingThreeH">
                                        <div class="card-body cb-c">
                                            Content will be available soon !
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12"
                                    style="float: left;margin-bottom: 10px;">
                                    <div class="card-header collapsed h-c" role="tab" id="headingfour"
                                        href="#collapsefour" data-toggle="collapse" data-parent="#accordionH"
                                        aria-expanded="false" aria-controls="collapsefour">
                                        <a class="card-title">TRAINING</a>
                                    </div>
                                    <div class="collapse show" id="collapsefour" role="tabpanel"
                                        aria-labelledby="headingThreeH">
                                        <div class="card-body cb-c">
                                            Content will be available soon !
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                        <div class="card-header" style="background-color: #008080 !important;color: #fff;">
                            <a class="card-title">NEWS & HIGHLIGHTS</a>
                        </div>
                        <div class="">
                            <div class="card-body cb-cc">
                                 <ul>
                                     <li><a href="http://pib.nic.in/newsite/PrintRelease.aspx?relid=181152" target="_blanck" style="font-size:12px;">Honourable Health Minister Launches National Viral Hepatitis Control Program </a></li>
                                  
                                     <li><a href="http://pib.nic.in/PressReleaseIframePage.aspx?PRID=1566140" target="_blanck" style="font-size:12px;">India rolls out National Viral Hepatitis Control Program</a></li>
                                  </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>



        <div class="row" style="box-shadow: 0px 0px 20px #ccc;margin-top: 12px;margin-bottom: 30px;padding: 20px 0px;">

            <div class="container">

                <div class="row text-center">
                    <div class="col-xl-12" style="border-bottom: 1px solid #eee;">
                        <h3>Related Links</h3>
                    </div>
                    <div class="rl">
                        <ul>
                            <li>
                                <a href="https://mohfw.gov.in/" target="_blanck"> Ministry of Health and Family Welfare,
                                    Govt. of India</a>
                            </li>
                            <li>
                                <a href="http://nhm.gov.in/" target="_blanck"> National Health Mission </a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>

        <div class="row ">

        </div>





        <div class="clearfix">
            <div class="googleImg"
                title="The translation (transliteration) is solely powered by Google and is not official translation of Ministry of External Affairs, India">
            </div>
            <div class="googleLang">
                <div id="google_translate_element"></div>
            </div>
        </div>


    </div>




    <script>
        $('.closeall').click(function (e) {
            e.preventDefault();
            $('.accordion .collapse.show').collapse('hide');
            //  $( '.accordion .collapse' ).collapse( 'show' );
            return false;
        });
        $('.openall').click(function (e) {
            e.preventDefault();
            $('.accordion .collapse').collapse('show');
            //  $( '.accordion .collapse.show' ).collapse( 'hide' );
            return false;
        });

        if (window.location.hash) {
            redirect(window.location.hash);
        }

        $('a[href^="#"]').on('click', function (e) {
            e.preventDefault();
            var a = document.createElement('a');
            a.href = this.href;
            redirect(a.hash);
            return false;
        });

        function redirect(hash) {
            // $( hash ).attr( 'aria-expanded', 'true' ).focus();
            // $( hash + '+div.collapse' ).addClass( 'show' ).attr( 'aria-expanded', 'true' );
            $(hash + '+div.collapse').collapse('show');

            // using this because of static nav bar space
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 60
            }, 10, function () {
                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        }

        document.documentElement.setAttribute("lang", "en");
        document.documentElement.removeAttribute("class");

        axe.run(function (err, results) {
            console.log(results.violations);
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            var itemsMainDiv = ('.MultiCarousel');
            var itemsDiv = ('.MultiCarousel-inner');
            var itemWidth = "";

            $('.leftLst, .rightLst').click(function () {
                var condition = $(this).hasClass("leftLst");
                if (condition)
                    click(0, this);
                else
                    click(1, this)
            });

            ResCarouselSize();
            hedName
            $(window).resize(function () {
                ResCarouselSize();
            });

            //this function define the size of the items
            function ResCarouselSize() {
                var incno = 0;
                var dataItems = ("data-items");
                var itemClass = ('.item');
                var id = 0;
                var btnParentSb = '';
                var itemsSplit = '';
                var sampwidth = $(itemsMainDiv).width();
                var bodyWidth = $('body').width();
                $(itemsDiv).each(function () {
                    id = id + 1;
                    var itemNumbers = $(this).find(itemClass).length;
                    btnParentSb = $(this).parent().attr(dataItems);
                    itemsSplit = btnParentSb.split(',');
                    $(this).parent().attr("id", "MultiCarousel" + id);


                    if (bodyWidth >= 1200) {
                        incno = itemsSplit[2];
                        itemWidth = sampwidth / incno;
                    }
                    else if (bodyWidth >= 992) {
                        incno = itemsSplit[2];
                        itemWidth = sampwidth / incno;
                    }
                    else if (bodyWidth >= 768) {
                        incno = itemsSplit[1];
                        itemWidth = sampwidth / incno;
                    }
                    else {
                        incno = itemsSplit[0];
                        itemWidth = sampwidth / incno;
                    }

                    $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
                    $(this).find(itemClass).each(function () {
                        $(this).outerWidth(itemWidth);
                    });

                    $(".leftLst").addClass("over");
                    $(".rightLst").removeClass("over");

                });
            }


            //this function used to move the items
            function ResCarousel(e, el, s) {
                var leftBtn = ('.leftLst');
                var rightBtn = ('.rightLst');
                var translateXval = '';
                var divStyle = $(el + ' ' + itemsDiv).css('transform');
                var values = divStyle.match(/-?[\d\.]+/g);
                var xds = Math.abs(values[4]);
                if (e == 0) {
                    translateXval = parseInt(xds) - parseInt(itemWidth * s);
                    $(el + ' ' + rightBtn).removeClass("over");

                    if (translateXval <= itemWidth / 2) {
                        translateXval = 0;
                        $(el + ' ' + leftBtn).addClass("over");
                    }
                }
                else if (e == 1) {
                    var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
                    translateXval = parseInt(xds) + parseInt(itemWidth * s);
                    $(el + ' ' + leftBtn).removeClass("over");

                    if (translateXval >= itemsCondition - itemWidth / 2) {
                        translateXval = itemsCondition;
                        $(el + ' ' + rightBtn).addClass("over");
                    }
                }
                $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
            }

            //It is used to get some elements from btn
            function click(ell, ee) {
                var Parent = "#" + $(ee).parent().attr("id");
                var slide = $(Parent).attr("data-slide");
                ResCarousel(ell, Parent, slide);
            }

        });</script>







</body>

</html>