  
    <style>
        .alert {
position: absolute;
padding: .75rem 1.25rem;
    padding-right: 1.25rem;
margin-bottom: 1rem;
border: 1px solid transparent;
    border-top-color: transparent;
    border-right-color: transparent;
    border-bottom-color: transparent;
    border-left-color: transparent;
border-radius: .25rem;
left: -15px;
width: 72px;
height: auto;
top: 180px;
z-index: 100;
}
.alert-info {
color: #0c5460;
background-color: transparent;
border-color: transparent;
}
.alert-dismissible .close {
padding: 0rem 0rem;
background-color: #ccc;
width: 82%;
}
.card {
border: 0px solid;
}
.btn-outline-info:hover {
    color: #fff;
    background-color: #17a2b8 !important;
    border-color: #17a2b8;
}
.img-set{
    padding-top: 20px;
    width: 60%;
    margin-left: 20%;
}
.img-set2{
    padding-top: 20px;
    width: 20%;
    margin-left: 40%;
}
.btn-outline-success {
    color: #ee82ee;
    border-color: #ee82ee;
}
.btn-outline-success:hover {
    color: #fff;
    background-color: #ee82ee;
    border-color: #ee82ee;
}
.cad{
    color: inherit !important;
}








.MultiCarousel
        {
            float: left;
            overflow: hidden;
            padding: 15px;
            width: 100%;
            position: relative;
        }
        .MultiCarousel .MultiCarousel-inner
        {
            transition: 1s ease all;
            float: left;
        }
        .MultiCarousel .MultiCarousel-inner .item
        {
            float: left;
        }
        .MultiCarousel .MultiCarousel-inner .item > div
        {
            padding: 10px;
            margin: 10px;
            /* background: #f1f1f1; */
            color: #666;
            box-shadow: 0px 0px 10px #ccc;
        }
        .MultiCarousel .leftLst, .MultiCarousel .rightLst
        {
            position: absolute;
            border-radius: 50%;
            top: calc(50% - 20px);
        }
        .MultiCarousel .leftLst
        {
            left: 0;
        }
        .MultiCarousel .rightLst
        {
            right: 0;
        }
        
        .MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over
        {
            pointer-events: none;
            background: #ccc;
        }
        .slid-btn{
            color: #fff;text-decoration: underline !important;font-size: 12px;position: absolute;bottom: 15px;
        }
        .bg-success {
    background-color: #26a591!important;
}
.bg-warning {
    background-color: #ff7918!important;
}
    </style>

        

<table class="table table-bordered">
 <tr >
                                <th colspan="6" class="text-center"> List of Model Treatment Centers and State Laboratories </th>
                            </tr>
           
            <hr>
                <table cellspacing=0 border=1>
                    <tr class="bg-warning text-white">
                        
                        <td style=min-width:50px>S.No.</td>
                        <td style=min-width:50px>State</td>
                        <td style=min-width:50px>No.</td>
                        <td style=min-width:50px>Name of MTCs</td>
                        <td style=min-width:50px>No.</td>
                        <td style=min-width:50px>State Labs</td>
                    </tr>
                    <tr>
                        
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Delhi</td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Maulana Azad Medical College & Lok Nayak Hospital, Delhi
</td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Maulana Azad Medical College & Lok Nayak Hospital, Delhi
</td>
                    </tr>
                    <tr>
                        
                        <td style=min-width:50px>2</td>
                        <td style=min-width:50px>Haryana</td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Pandit Bhagwat Dayal Sharma Post Graduate Institute of Medical Sciences, Rohtak
</td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Pandit Bhagwat Dayal Sharma Post Graduate Institute of Medical Sciences, Rohtak</td>
                    </tr>
                    <tr>
                        
                        <td style=min-width:50px>3</td>
                        <td style=min-width:50px>Himachal Pradesh</td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Indira Gandhi Medical College & Hospital, Shimla</td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Indira Gandhi Medical College & Hospital, Shimla</td>
                    </tr>
                    <tr>
                        
                        <td style=min-width:50px>4</td>
                        <td style=min-width:50px>Maharashtra</td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Lokmanya Tilak Municipal General Hospital, Sion, Mumbai</td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Lokmanya Tilak Municipal General Hospital, Sion, Mumbai</td>
                    </tr>
                    <tr>
                        
                        <td style=min-width:50px>5</td>
                        <td style=min-width:50px>Nagaland</td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Naga Hospital Authority, Kohima</td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Naga Hospital Authority, Kohima</td>
                    </tr>
                    <tr>
                        
                        <td style=min-width:50px>6</td>
                        <td style=min-width:50px>Punjab</td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Postgraduate Institute of Medical Education & Research (PGIMER), Chandigarh</td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Postgraduate Institute of Medical Education & Research (PGIMER), Chandigarh</td>
                    </tr>
                    <tr>
                        
                        <td style=min-width:50px>7</td>
                        <td style=min-width:50px>Rajasthan</td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Sawai Man Singh Medical College, Jaipur</td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Sawai Man Singh Medical College, Jaipur</td>
                    </tr>
                    <tr>
                        
                        <td style=min-width:50px>8</td>
                        <td style=min-width:50px>Tripura </td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Agartala Government Medical College, Agartala </td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Agartala Government Medical College, Agartala </td>
                    </tr>
                    <tr>
                        
                        <td style=min-width:50px>9</td>
                        <td style=min-width:50px>West Bengal</td>
                        <td style=min-width:50px>2</td>
                        <td style=min-width:50px>1.)Institute of Post Graduate Medical Education & Research (IPGMER), Kolkata ,<br/>
                        2. North Bengal Medical College, Siliguri </td>
                        <td style=min-width:50px>2</td>
                        <td style=min-width:50px>1.)Institute of Post Graduate Medical Education & Research (IPGMER), Kolkata ,<br/>
                        2. North Bengal Medical College, Siliguri</td>
                    </tr>

                     <tr>
                        
                        <td style=min-width:50px>10</td>
                        <td style=min-width:50px>Sikkim </td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>New STNM Multispeciality Hospital, Gangtok </td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>New STNM Multispeciality Hospital, Gangtok </td>
                    </tr>

                       <tr>
                        
                        <td style=min-width:50px>11</td>
                        <td style=min-width:50px>Jharkhand </td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Rajendra Institute of Medical Sciences, Ranchi </td>
                        <td style=min-width:50px>1</td>
                        <td style=min-width:50px>Rajendra Institute of Medical Sciences, Ranchi </td>
                    </tr>
                    <tr>

                          
                    <tr>
                   

                </table>
                <hr>
                    
                   
                                </body>
                            </html>
</table>

</div>
                <div class="row ">

                    </div>

            </div>





        </div>
       

  
    <script type="text/javascript">        $(document).ready(function () {
        var itemsMainDiv = ('.MultiCarousel');
        var itemsDiv = ('.MultiCarousel-inner');
        var itemWidth = "";

        $('.leftLst, .rightLst').click(function () {
            var condition = $(this).hasClass("leftLst");
            if (condition)
                click(0, this);
            else
                click(1, this)
        });

        ResCarouselSize();




        $(window).resize(function () {
            ResCarouselSize();
        });

        //this function define the size of the items
        function ResCarouselSize() {
            var incno = 0;
            var dataItems = ("data-items");
            var itemClass = ('.item');
            var id = 0;
            var btnParentSb = '';
            var itemsSplit = '';
            var sampwidth = $(itemsMainDiv).width();
            var bodyWidth = $('body').width();
            $(itemsDiv).each(function () {
                id = id + 1;
                var itemNumbers = $(this).find(itemClass).length;
                btnParentSb = $(this).parent().attr(dataItems);
                itemsSplit = btnParentSb.split(',');
                $(this).parent().attr("id", "MultiCarousel" + id);
               

                if (bodyWidth >= 1200) {
                    incno = itemsSplit[2];
                    itemWidth = sampwidth / incno;
                }
                else if (bodyWidth >= 992) {
                    incno = itemsSplit[2];
                    itemWidth = sampwidth / incno;
                }
                else if (bodyWidth >= 768) {
                    incno = itemsSplit[1];
                    itemWidth = sampwidth / incno;
                }
                else {
                    incno = itemsSplit[0];
                    itemWidth = sampwidth / incno;
                }

                $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
                $(this).find(itemClass).each(function () {
                    $(this).outerWidth(itemWidth);
                });

                $(".leftLst").addClass("over");
                $(".rightLst").removeClass("over");

            });
        }


        //this function used to move the items
        function ResCarousel(e, el, s) {
            var leftBtn = ('.leftLst');
            var rightBtn = ('.rightLst');
            var translateXval = '';
            var divStyle = $(el + ' ' + itemsDiv).css('transform');
            var values = divStyle.match(/-?[\d\.]+/g);
            var xds = Math.abs(values[4]);
            if (e == 0) {
                translateXval = parseInt(xds) - parseInt(itemWidth * s);
                $(el + ' ' + rightBtn).removeClass("over");

                if (translateXval <= itemWidth / 2) {
                    translateXval = 0;
                    $(el + ' ' + leftBtn).addClass("over");
                }
            }
            else if (e == 1) {
                var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
                translateXval = parseInt(xds) + parseInt(itemWidth * s);
                $(el + ' ' + leftBtn).removeClass("over");

                if (translateXval >= itemsCondition - itemWidth / 2) {
                    translateXval = itemsCondition;
                    $(el + ' ' + rightBtn).addClass("over");
                }
            }
            $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
        }

        //It is used to get some elements from btn
        function click(ell, ee) {
            var Parent = "#" + $(ee).parent().attr("id");
            var slide = $(Parent).attr("data-slide");
            ResCarousel(ell, Parent, slide);
        }

    });</script>

    
    <script>
        (function ($) {
            $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
                if (!$(this).next().hasClass('show')) {
                    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
                }
                var $subMenu = $(this).next(".dropdown-menu");
                $subMenu.toggleClass('show');

                $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                    $('.dropdown-submenu .show').removeClass("show");
                });

                return false;
            });
        })(jQuery)
    </script>
    <script>
        $(window).scroll(function () {
            $("#t-menu").stop().animate({ "marginTop": ($(window).scrollTop()) + "px", "marginLeft": ($(window).scrollLeft()) + "px" }, "slow");
        });
        var totaltext = "";
        for (var i = 0; i < 100; i++) {
            totaltext += "scroll!<br />";
        }
        $("#main_container").html();
    </script>
    <script type="text/javascript">
        var screenheight = window.innerHeight;
        // alert(screenheight - 114);
        var heightprop = screenheight - 72;
        document.getElementById('main_container').style.height = heightprop + 'px';
    </script>
    <script type="text/javascript">
        $('#hover')
            .mouseenter(function () {
                $('#divv').show(1000);
            })
            .click(function (e) {
                e.preventDefault();
                $('#divv').hide(1000);
            });
    </script>
    <script type="text/javascript">
        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > 81) {
                $('.fixed-menu').addClass('fixed-m');
                $('.l-m').addClass('l-mn');
                $('.bg-white').addClass('bg-success');

            } else {
                $('.fixed-menu').removeClass('fixed-m');
                $('.l-m').removeClass('l-mn');
                $('.bg-white').removeClass('bg-success');
            }
        });
    </script>
    

    

</body>

</html>