

    <style>
        .alert {
position: absolute;
padding: .75rem 1.25rem;
    padding-right: 1.25rem;
margin-bottom: 1rem;
border: 1px solid transparent;
    border-top-color: transparent;
    border-right-color: transparent;
    border-bottom-color: transparent;
    border-left-color: transparent;
border-radius: .25rem;
left: -15px;
width: 72px;
height: auto;
top: 180px;
z-index: 100;
}
.alert-info {
color: #0c5460;
background-color: transparent;
border-color: transparent;
}
.alert-dismissible .close {
padding: 0rem 0rem;
background-color: #ccc;
width: 82%;
}
.card {
border: 0px solid;
}
.btn-outline-info:hover {
    color: #fff;
    background-color: #17a2b8 !important;
    border-color: #17a2b8;
}
.img-set{
    padding-top: 20px;
    width: 60%;
    margin-left: 20%;
}
.img-set2{
    padding-top: 20px;
    width: 20%;
    margin-left: 40%;
}
.btn-outline-success {
    color: #ee82ee;
    border-color: #ee82ee;
}
.btn-outline-success:hover {
    color: #fff;
    background-color: #ee82ee;
    border-color: #ee82ee;
}
.cad{
    color: inherit !important;
}








.MultiCarousel
        {
            float: left;
            overflow: hidden;
            padding: 15px;
            width: 100%;
            position: relative;
        }
        .MultiCarousel .MultiCarousel-inner
        {
            transition: 1s ease all;
            float: left;
        }
        .MultiCarousel .MultiCarousel-inner .item
        {
            float: left;
        }
        .MultiCarousel .MultiCarousel-inner .item > div
        {
            padding: 10px;
            margin: 10px;
            /* background: #f1f1f1; */
            color: #666;
            box-shadow: 0px 0px 10px #ccc;
        }
        .MultiCarousel .leftLst, .MultiCarousel .rightLst
        {
            position: absolute;
            border-radius: 50%;
            top: calc(50% - 20px);
        }
        .MultiCarousel .leftLst
        {
            left: 0;
        }
        .MultiCarousel .rightLst
        {
            right: 0;
        }
        
        .MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over
        {
            pointer-events: none;
            background: #ccc;
        }
        .slid-btn{
            color: #fff;text-decoration: underline !important;font-size: 12px;position: absolute;bottom: 15px;
        }
        .bg-success {
    background-color: #26a591!important;
}
.bg-warning {
    background-color: #ff7918!important;
}

.carousel-indicators li {
    background-color: #222;
}
    </style>

         
        <!-- Navbar end -->
        
            <div class="row" >
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
                    <div class="card" style="box-shadow: 0px 2px 6px 4px #cccccc;">
                        <div class="card-header bg-warning text-white text-center" style="display: block !important">Important Links</div>
                        <div class="card-body" style="height:380px;overflow: auto;">
                            
                            <div class="card-ul">
                                <ul>
                                     <li><a href="<?php echo base_url(); ?>login" style="font-size:12px;">NVHCP Management Information System</a></li>
                                   <li><a href="<?php echo base_url(); ?>List-of-Model-Treatment-Centres" style="font-size:12px;">List of Model Treatment Centers and State Laboratories
</a></li>

 <li><a href="https://mohfw.gov.in/" target="_blank" style="font-size:12px;">Ministry of Health and Family Welfare
</a></li>

 <li><a href="https://nhm.gov.in/" target="_blank" style="font-size:12px;">National Health Mission
</a></li>
<li><a href="<?php echo base_url(); ?>common_libs/mistrainingmanual.pdf" target="_blank" style="font-size:12px;">NVHCP Management Information  System Training Manual</li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- d-none d-lg-block -->
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 ">
                    <!-- <img src="images/banner.png" style="width: 100%;"> -->
                    <div id="demo" class="carousel slide" data-ride="carousel">

                            <!-- Indicators -->
                            <ul class="carousel-indicators">
                              <li data-target="#demo" data-slide-to="0" class="active"></li>
                              <li data-target="#demo" data-slide-to="1"></li>
                              <li data-target="#demo" data-slide-to="2"></li>
                              <!-- <li data-target="#demo" data-slide-to="3"></li>
                               <li data-target="#demo" data-slide-to="4"></li>
                                <li data-target="#demo" data-slide-to="5"></li> -->
                            </ul>
                          
                            <!-- The slideshow -->
                            <div class="carousel-inner">

                                 <div class="carousel-item active">
                                <img src="<?php echo base_url(); ?>assets/images/b1.jpg" alt="Ticker2">
                              </div>
                                 <div class="carousel-item ">
                                <img src="<?php echo base_url(); ?>assets/images/b2.jpg" alt="Ticker4">
                              </div>

                                <div class="carousel-item ">
                                <img src="<?php echo base_url(); ?>assets/images/b3.jpg" alt="Ticker1">
                              </div>
                             <!-- <div class="carousel-item">
                                <img src="images/stamp picture 4.png" alt="Ticker3">
                              </div>
                              <div class="carousel-item">
                                <img src="images/GL-Front-page-collated-pic-2.jpg" alt="Ticker5">
                              </div>
                               <div class="carousel-item">
                                <img src="images/Slide_website_009.png" alt="Ticker3">
                              </div> -->

                              

                            </div>
                          
                            <!-- Left and right controls -->
                            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                              <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a class="carousel-control-next" href="#demo" data-slide="next">
                              <span class="carousel-control-next-icon"></span>
                            </a>
                          
                          </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
                    <div class="card" style="box-shadow: 0px 2px 6px 4px #cccccc;margin-top: 0px;">
                        <div class="card-header bg-warning text-white text-center" style="display: block !important"><br/></div>
                        <div class="card-body" style="height:380px;overflow: auto;">
                            
                            <div class="card-ul">
                                 <div class="card">
                   
                        
                        <div class="hon-img text-center" style="margin-top: -15px;">
                            <a href="http://loksabhaph.nic.in/Members/MemberBioprofile.aspx?mpsno=4776" target="_blank">
                            <img src="<?php echo site_url('assets/images/DrHarsh-Vardhan.jpg');?>" style="width: 125px;height: 115px;"></a>
                                    <p style="margin-bottom: 0px;font-size: 11px;">Dr. Harsh Vardhan<br></p>
                        </div>
                        <div class="hon-h text-center">Hon’ble Cabinet Minister</div>
                       <!--  <a style="text-align: right;width: 100%;font-size: 12px;padding-right: 15px;" href="http://loksabhaph.nic.in/Members/MemberBioprofile.aspx?mpsno=4776" target="_blank"> View Portfolio</a> -->
                        <hr>
                        <div class="hon-img text-center" style="margin-top: -13px;">
                            <a href="http://loksabhaph.nic.in/Members/MemberBioprofile.aspx?mpsno=4650" target="_blank">
                            <img  src="<?php echo site_url('assets/images/Shri_Ashwini_Kumar.jpg')?>" style="width: 125px;height: 115px;"></a>
                            <p style="margin-bottom: 0px;font-size: 11px;">Shri Ashwini Kumar Choubey<br></p>
                        </div>
                        <div class="hon-h text-center">Hon’ble Minister of State</div>
                        <!-- <a style="text-align: right;width: 100%;font-size: 12px;padding-right: 15px;"  href="http://loksabhaph.nic.in/Members/MemberBioprofile.aspx?mpsno=4650" target="_blank">View Portfolio</a> -->
                        
                        


                        
                </div> 
                           </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="row">
                <div class="col-sm-12" style="box-shadow: 0px 0px 20px #ccc;margin-top: 12px;">
                    
                 <h3 class="text-center" style="color: #26a591; margin-bottom: 0px;padding: 20px 0px;">   Latest From NVHCP
                    
                </h3>
                </div>
                <div class="container">

                      
                        <div class="row">
                                <div class="MultiCarousel" data-items="1,2,3" data-slide="1" id="MultiCarousel1"
                                    data-interval="500">
                                    <div class="MultiCarousel-inner" style="transform: translateX(0px); width: 1554px;">
                                        <div class="item col-xs-4">
                                            <div class="pad15">
                                                    <div class="card bg-success text-white">
                                                        <div class="card-header" style="display: block">
                                                            <h4 style="font-size: 16px;">Development of National Action Plan <br/><br/></h4></div>
                                                            <!-- <img class="card-img-top" src="images/girl.png" alt="Card image"> -->
                                                            
                                                            <!-- <div class="card-img-overlay"> -->
                                                                <div class="card-body" style="height: 245px;overflow: auto;">
                                                             
                                                            
                                                              <p class="card-text" style="font-size: 14px;     text-align: justify;">
                                                                    A National Steering committee was formed for development of National Action Plan for viral hepatitis in January 2017. The committee recommended to constitute working groups under various thematic areas related to viral hepatitis including surveillance of viral hepatitis, laboratory diagnosis, treatment, immunization, blood safety, injection safety and infection control, 
                                                              </p>
                                                              <a href="<?php echo base_url();?>Development" class="slid-btn">Read More..</a>
                                                            <!-- </div> -->
                                                          </div>
                                                        </div>
                                            </div>
                                        </div>

                                        <div class="item col-xs-4">
                                                <div class="pad15">
                                                        <div class="card bg-warning text-white">
                                                            <div class="card-header" style="display: block">
                                                                <h4 style="font-size: 16px;">Launch of National Viral Hepatitis Control Program</h4></div>
                                                                <!-- <img class="card-img-top" src="images/women.png" alt="Card image"> -->
                                                                
                                                                <!-- <div class="card-img-overlay"> -->
                                                                        <div class="card-body" style="height: 245px;overflow: auto;">
                                                                 
                                                                  <p class="card-text" style="font-size: 14px;     text-align: justify;">
                                                                        ‘National Viral Hepatitis Control Program’ was launched on 28th July, 2018 on the occasion of World Hepatitis Day by the hon’ble HFM with the aim to
to combat hepatitis and achieve countrywide elimination of Hepatitis C by 2030, achieve significant reduction in the infected population, morbidity and mortality associated with Hepatitis A,B, C and E.
                                                                  </p>
                                                                  <a href="<?php echo base_url(); ?>Launch-of-National-Viral-Hepatitis-Control-Program" class="slid-btn">Read More..</a>
                                                                <!-- </div> -->
                                                                </div>
                                                              </div>
                                                </div>
                                            </div>

                                            <div class="item col-xs-4">
                                                    <div class="pad15">
                                                            <div class="card bg-success text-white">
                                                                <div class="card-header" style="display: block;z-index: 10000;">
                                                                    <h4 style="font-size: 16px; " title="National Training of Trainers for Diagnosis and Management of Viral Hepatitis"> 
                                                                            National Training of Trainers for Diagnosis and Management.. 
                                                                        </h4></div>
                                                                    <!-- <img class="card-img-top" src="images/girl.png" alt="Card image">
                                                                    
                                                                    <div class="card-img-overlay"> -->
                                                                            <div class="card-body" style="height: 245px;overflow: auto;">
                                                                      <p class="card-text" style="font-size: 14px;     text-align: justify;" >
                                                                           Two rounds of national training of trainers were held to train the experts from across the country in diagnosis and management of viral hepatitis according to the standardized protocols and testing algorithms prepared by the technical resource group on laboratory and care support and treatment... 
                                                                      </p>
                                                                      <a href="<?php echo base_url(); ?>National-Training-of-Trainers" class="slid-btn">Read More..</a>
                                                                    </div>
                                                                  <!-- </div> -->
                                                                  </div>
                                                    </div>
                                                </div>

                                                <div class="item col-xs-4">
                                                        <div class="pad15">
                                                                <div class="card bg-warning text-white">
                                                                    <div class="card-header" style="display: block;z-index: 10000;">
                                                                        <h4 style="font-size: 16px;" title="Sensitization workshop for the State Nodal Officers-National Viral Hepatitis Control Program">Sensitization workshop for the State Nodal... </h4></div>
                                                                        <!-- <img class="card-img-top" src="images/women.png" alt="Card image">
                                                                        
                                                                        <div class="card-img-overlay"> -->
                                                                                <div class="card-body" style="height: 245px;overflow: auto;">
                                                                          <p class="card-text" style="font-size: 14px;"     text-align: justify;>
                                                                                A meeting of the state nodal officers of NVHCP of all the states was held on 6th-7th Sept., 2018 and 29-30th Jan., 2019 to sensitise them regarding the operational components of the for successful roll out of the program...
                                                                          </p>
                                                                          <a href="<?php echo base_url(); ?>Sensitization-workshop" class="slid-btn">Read More..</a>
                                                                        </div>
                                                                      <!-- </div> -->
                                                                      </div>
                                                        </div>
                                                    </div>

                                                 

                                                    
                                        
                                      
                                       
                                        
                                    </div>
                                    <button class="btn btn-primary leftLst over">
                                        &lt;</button>
                                    <button class="btn btn-primary rightLst">
                                        &gt;</button>
                                </div>
                            </div>
                        </div>


                        
                   
                </div>
 


<div class="row" style="box-shadow: 0px 0px 20px #ccc;margin-top: 12px;margin-bottom: 30px;padding: 20px 0px;">
 
        <div class="container">

            <div class="row text-center">
               <div class="col-xl-12" style="border-bottom: 1px solid #eee;"><h3>Related Links</h3></div>
               <div class="rl">
                   <ul>
                   <li>
                        <a href="https://mohfw.gov.in/" target="_blanck">  Ministry of Health and Family Welfare, Govt. of India</a>
                   </li>
                   <li>
                        <a  href="http://nhm.gov.in/" target="_blanck"> National Health Mission </a>
                   </li>
                </ul>
               </div> 
               
            </div>
        </div>
</div>
                
                <div class="row ">


                    </div>

            



 <div class="clearfix">
            <div class="googleImg" title="The translation (transliteration) is solely powered by Google and is not official translation of Ministry of External Affairs, India"></div>
            <div class="googleLang">
              <div id="google_translate_element"></div>
            </div>
          </div>


        </div>
       
      <script src="<?php echo site_url('assets/js')?>/jquery.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.3/umd/popper.min.js"></script> -->
    <script src="<?php echo site_url('assets/js')?>/bootstrap.min.js"></script>

  
    <script type="text/javascript">  

        $(document).ready(function () {
            
        var itemsMainDiv = ('.MultiCarousel');
        var itemsDiv = ('.MultiCarousel-inner');
        var itemWidth = "";

        $('.leftLst, .rightLst').click(function () {
            var condition = $(this).hasClass("leftLst");
            if (condition)
                click(0, this);
            else
                click(1, this)
        });

        ResCarouselSize();
        hedName
        $(window).resize(function () {
            ResCarouselSize();
        });

        //this function define the size of the items
        function ResCarouselSize() {
            var incno = 0;
            var dataItems = ("data-items");
            var itemClass = ('.item');
            var id = 0;
            var btnParentSb = '';
            var itemsSplit = '';
            var sampwidth = $(itemsMainDiv).width();
            var bodyWidth = $('body').width();
            $(itemsDiv).each(function () {
                id = id + 1;
                var itemNumbers = $(this).find(itemClass).length;
                btnParentSb = $(this).parent().attr(dataItems);
                itemsSplit = btnParentSb.split(',');
                $(this).parent().attr("id", "MultiCarousel" + id);
               

                if (bodyWidth >= 1200) {
                    incno = itemsSplit[2];
                    itemWidth = sampwidth / incno;
                }
                else if (bodyWidth >= 992) {
                    incno = itemsSplit[2];
                    itemWidth = sampwidth / incno;
                }
                else if (bodyWidth >= 768) {
                    incno = itemsSplit[1];
                    itemWidth = sampwidth / incno;
                }
                else {
                    incno = itemsSplit[0];
                    itemWidth = sampwidth / incno;
                }

                $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
                $(this).find(itemClass).each(function () {
                    $(this).outerWidth(itemWidth);
                });

                $(".leftLst").addClass("over");
                $(".rightLst").removeClass("over");

            });
        }


        //this function used to move the items
        function ResCarousel(e, el, s) {
            var leftBtn = ('.leftLst');
            var rightBtn = ('.rightLst');
            var translateXval = '';
            var divStyle = $(el + ' ' + itemsDiv).css('transform');
            var values = divStyle.match(/-?[\d\.]+/g);
            var xds = Math.abs(values[4]);
            if (e == 0) {
                translateXval = parseInt(xds) - parseInt(itemWidth * s);
                $(el + ' ' + rightBtn).removeClass("over");

                if (translateXval <= itemWidth / 2) {
                    translateXval = 0;
                    $(el + ' ' + leftBtn).addClass("over");
                }
            }
            else if (e == 1) {
                var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
                translateXval = parseInt(xds) + parseInt(itemWidth * s);
                $(el + ' ' + leftBtn).removeClass("over");

                if (translateXval >= itemsCondition - itemWidth / 2) {
                    translateXval = itemsCondition;
                    $(el + ' ' + rightBtn).addClass("over");
                }
            }
            $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
        }

        //It is used to get some elements from btn
        function click(ell, ee) {
            var Parent = "#" + $(ee).parent().attr("id");
            var slide = $(Parent).attr("data-slide");
            ResCarousel(ell, Parent, slide);
        }

    });</script>

    
   <!-- <script>
        (function ($) {
            $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
                if (!$(this).next().hasClass('show')) {
                    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
                }
                var $subMenu = $(this).next(".dropdown-menu");
                $subMenu.toggleClass('show');

                $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                    $('.dropdown-submenu .show').removeClass("show");
                });

                return false;
            });
        })(jQuery)
    </script>
     <script>
        $(window).scroll(function () {
            $("#t-menu").stop().animate({ "marginTop": ($(window).scrollTop()) + "px", "marginLeft": ($(window).scrollLeft()) + "px" }, "slow");
        });
        var totaltext = "";
        for (var i = 0; i < 100; i++) {
            totaltext += "scroll!<br />";
        }
        $("#main_container").html();
    </script> -->
    <!-- <script type="text/javascript">
        var screenheight = window.innerHeight;
        
        var heightprop = screenheight - 72;
        document.getElementById('main_container').style.height = heightprop + 'px';
    </script>
    <script type="text/javascript">
        $('#hover')
            .mouseenter(function () {
                $('#divv').show(1000);
            })
            .click(function (e) {
                e.preventDefault();
                $('#divv').hide(1000);
            });
    </script> -->
    <!-- <script type="text/javascript">
        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > 81) {
                $('.fixed-menu').addClass('fixed-m');
                $('.l-m').addClass('l-mn');
                $('.bg-white').addClass('bg-success');

            } else {
                $('.fixed-menu').removeClass('fixed-m');
                $('.l-m').removeClass('l-mn');
                $('.bg-white').removeClass('bg-success');
            }
        });
    </script> -->
    

    

</body>

</html>