

  <div class="container">
  <p class="text-center"><b>Details of State/UT Nodal Officers- NVHCP as on March '2021</b></p>
      
  <table class="table">
    <thead>
      <tr>
        <th>Sl. No.</th>
        <th>State/UT</th>
        <th>State Nodal Officer</th>
      </tr>
    </thead>
    <tbody>
      <tr>
            <td>1</td>
            <td>Andaman & Nicobar</td>
            <td>Dr. Omkar Singh</td>
        </tr>
        <tr>
            <td>2</td>
            <td>Andhra Pradesh</td>
            <td>Dr Neelima</td>
        </tr>
        <tr>
            <td>3</td>
            <td>Arunachal Pradesh</td>
            <td>Dr L  Jampa</td>
        </tr>
        <tr>
            <td>4</td>
            <td>Assam</td>
            <td>Dr Pranati Das</td>
        </tr>
        <tr>
            <td>5</td>
            <td>Bihar</td>
            <td>Dr Ragini  mishra</td>
        </tr>
        <tr>
            <td>6</td>
            <td>Chandigarh</td>
            <td>Dr Manjeet Trehan</td>
        </tr>
        <tr>
            <td>7</td>
            <td>Chhattisgarh</td>
            <td>Dr  Dharmendra Gahwai</td>
        </tr>
        <tr>
            <td>8</td>
            <td>D & N Haveli</td>
            <td>Dr Daulat Jhala</td>
        </tr>
        <tr>
            <td>9</td>
            <td>Daman & Diu</td>
            <td>Dr Makwana</td>
        </tr>
        <tr>
            <td>10</td>
            <td>Delhi</td>
            <td>To be provided by the state</td>
        </tr>
        <tr>
            <td>11</td>
            <td>Goa</td>
            <td>Dr Surekha Parulekar</td>
        </tr>
        <tr>
            <td>12</td>
            <td>Gujarat</td>
            <td>Dr. Amarnath Varma</td>
        </tr>
       
        <tr>
            <td>13</td>
            <td>Haryana</td>
            <td>Dr Praveen Verma</td>
        </tr>
        <tr>
            <td>14</td>
            <td>Himachal Pradesh</td>
            <td>Dr Vinod Mehta</td>
        </tr>
        <tr>
            <td>15</td>
            <td>Jammu & Kashmir</td>
            <td>Dr Om Kumar</td>
        </tr>
        <tr>
            <td>16</td>
            <td>Jharkhand</td>
            <td>Dr. Laxman Lal</td>
        </tr>
        <tr>
            <td>17</td>
            <td>Karnataka</td>
            <td>Dr Sridhar</td>
        </tr>
        <tr>
            <td>18</td>
            <td>Kerala</td>
            <td>Dr Sheela</td>
        </tr>
        <tr>
            <td>19</td>
            <td>Ladakh</td>
            <td>Dr Willayat Ali</td>
        </tr>
        <tr>
            <td>20</td>
            <td>Lakshdweep</td>
            <td>Dr  Haseena Bano</td>
        </tr>
        <tr>
            <td>21</td>
            <td>Madhya Pradesh</td>
            <td>Dr Shashi Thakur</td>
        </tr>
        <tr>
            <td>22</td>
            <td>Maharashtra</td>
            <td>Dr. Nitin Ambadekar</td>
        </tr>
        <tr>
            <td>23</td>
            <td>Manipur</td>
            <td>Dr. Rosie Raj Kumari</td>
        </tr>
        <tr>
            <td>24</td>
            <td>Meghalaya</td>
            <td>Dr.Nongbri</td>
        </tr>
        <tr>
            <td>25</td>
            <td>Mizoram</td>
            <td>Dr. Robert L Khawlhring</td>
        </tr>
        <tr>
            <td>26</td>
            <td>Nagaland</td>
            <td>Dr Vezokholu Theyo</td>
        </tr>
        <tr>
            <td>27</td>
            <td>Odisha</td>
            <td>Dr. Ashok Paikray</td>
        </tr>
        <tr>
            <td>28</td>
            <td>Puducherry</td>
            <td>Dr   J Ramesh</td>
        </tr>
        <tr>
            <td>29</td>
            <td>Punjab</td>
            <td>Dr Gagandeep Singh</td>
        </tr>
        <tr>
            <td>30</td>
            <td>Rajasthan</td>
            <td>Dr. Dholpuria</td>
        </tr>
        <tr>
            <td>31</td>
            <td>Sikkim</td>
            <td>Dr. Namita Subba</td>
        </tr>
        <tr>
            <td>32</td>
            <td>Tamilnadu</td>
            <td>Dr Narayanswamy</td>
        </tr>
        <tr>
            <td>33</td>
            <td>Telangana</td>
            <td>Dr Rajeev Raju</td>
        </tr>
        <tr>
            <td>34</td>
            <td>Tripura</td>
            <td>Dr Anirban</td>
        </tr>
        <tr>
            <td>35</td>
            <td>Uttar Pradesh</td>
            <td>Dr Vikasendu Agrawal</td>
        </tr>
        <tr>
            <td>36</td>
            <td>Uttarakhand</td>
            <td>Dr Mayank Badola</td>
        </tr>
        <tr>
            <td>37</td>
            <td>West Bengal</td>
            <td>Dr. Pallav Bhattacharya</td>
        </tr>
    </tbody>
  </table>
</div>