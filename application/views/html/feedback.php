
<style>
.alert {
    position: absolute;
    padding: .75rem 1.25rem;
    padding-right: 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-top-color: transparent;
    border-right-color: transparent;
    border-bottom-color: transparent;
    border-left-color: transparent;
    border-radius: .25rem;
    left: -15px;
    width: 72px;
    height: auto;
    top: 180px;
    z-index: 100;
}
.alert-info {
    color: #0c5460;
    background-color: transparent;
    border-color: transparent;
}
.alert-dismissible .close {
    padding: 0rem 0rem;
    background-color: #ccc;
    width: 82%;
}
.card {
    border: 0px solid;
}
.btn-outline-info:hover {
    color: #fff;
    background-color: #17a2b8 !important;
    border-color: #17a2b8;
}
.img-set{
    padding-top: 20px;
    width: 60%;
    margin-left: 20%;
}
.img-set2{
    padding-top: 20px;
    width: 20%;
    margin-left: 40%;
}
.btn-outline-success {
    color: #ee82ee;
    border-color: #ee82ee;
}
.btn-outline-success:hover {
    color: #fff;
    background-color: #ee82ee;
    border-color: #ee82ee;
}
.cad{
    color: inherit !important;
}








.MultiCarousel
{
    float: left;
    overflow: hidden;
    padding: 15px;
    width: 100%;
    position: relative;
}
.MultiCarousel .MultiCarousel-inner
{
    transition: 1s ease all;
    float: left;
}
.MultiCarousel .MultiCarousel-inner .item
{
    float: left;
}
.MultiCarousel .MultiCarousel-inner .item > div
{
    padding: 10px;
    margin: 10px;
    /* background: #f1f1f1; */
    color: #666;
    box-shadow: 0px 0px 10px #ccc;
}
.MultiCarousel .leftLst, .MultiCarousel .rightLst
{
    position: absolute;
    border-radius: 50%;
    top: calc(50% - 20px);
}
.MultiCarousel .leftLst
{
    left: 0;
}
.MultiCarousel .rightLst
{
    right: 0;
}

.MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over
{
    pointer-events: none;
    background: #ccc;
}
.slid-btn{
    color: #fff;text-decoration: underline !important;font-size: 12px;position: absolute;bottom: 15px;
}
.bg-success {
    background-color: #26a591!important;
}
.bg-warning {
    background-color: #ff7918!important;
}

/*alerts*/

.alert {
    position: absolute;
    padding: .75rem 1.25rem;
    padding-right: 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-top-color: transparent;
    border-right-color: transparent;
    border-bottom-color: transparent;
    border-left-color: transparent;
    border-radius: .25rem;
    left: -40px;
    width: 431px;
    height: auto;
    top: -98px;
    z-index: 100;
}

.alert-dismissible .close {
    padding: 0rem 0rem;
    background-color: #ccc;
    width: 4%;
}

.alert h4 {
  margin-top: 0;
  color: inherit;
}
.alert .alert-link {
  font-weight: bold;
}
.alert > p,
.alert > ul {
  margin-bottom: 0;
}
.alert > p + p {
  margin-top: 5px;
}
.alert-dismissable,
.alert-dismissible {
  padding-right: 35px;
}
.alert-dismissable .close,

.alert-success {
  background-color: #dff0d8;
  border-color: #d6e9c6;
  color: #3c763d;
}
.alert-success hr {
  border-top-color: #c9e2b3;
}
.alert-success .alert-link {
  color: #2b542c;
}
.alert-info {
  background-color: #d9edf7;
  border-color: #bce8f1;
  color: #31708f;
}
.alert-info hr {
  border-top-color: #a6e1ec;
}
.alert-info .alert-link {
  color: #245269;
}
.alert-warning {
  background-color: #fcf8e3;
  border-color: #faebcc;
  color: #8a6d3b;
}
.alert-warning hr {
  border-top-color: #f7e1b5;
}
.alert-warning .alert-link {
  color: #66512c;
}
.alert-danger {
  background-color: #f2dede;
  border-color: #ebccd1;
  color: #a94442;
}
.alert-danger hr {
  border-top-color: #e4b9c0;
}
.alert-danger .alert-link {
  color: #843534;
}
.table-hover>tbody>tr:hover {
    background-color: #EB984E;
}
</style>

<script src='https://www.google.com/recaptcha/api.js'></script>

<div class="container">
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-8 col-sm-12 m-auto">
            <h2 class="text-center">Feedback</h2>
            <div class="card" style="border: 1px solid #ccc">
                <div class="card-body">

                    <?php
                    $tr_msg = $this->session->flashdata('tr_msg');
                    $er_msg = $this->session->flashdata('er_msg');

                    if (!empty($tr_msg)) { ?>

                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="icon fa fa-check"></i>
                            <?php echo $tr_msg; ?>
                        </div>

                    <?php }elseif (!empty($er_msg)) { ?>

                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="icon fa fa-ban"></i>
                            <?php echo $er_msg; ?>
                        </div>

                    <?php } ?>

                    <?php
                    $attributes = array(
                      'id' => 'feedbackfid',
                      'name' => 'feedbackfid',
                      'autocomplete' => 'off',
                  );
                  echo form_open('', $attributes); ?>
                  <input type="hidden" name="token" value="<?php 
                  $_SESSION['token'] = uniqid();
                  echo $_SESSION['token'];
                  ?>">
                  <div class="form-group">
                      <label for="name">Name:</label>
                      <input type="text" autocomplete="anyrandomthing" class="form-control messagedata" id="name" placeholder="Enter name" required maxlength="50" name="name" required="">
                  </div>

                  <div class="form-group">
                      <label for="Subject">Subject:</label>
                      <input type="text" class="form-control" id="Subject" placeholder="Enter Subject" name="Subject" required="">
                  </div>
                  <div class="form-group">
                      <label for="email">Email:</label>
                      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" required="">
                  </div>
                  <div class="form-group">
                    <label for="category">Category:</label>
                    <select  name="category" id="category"  class="form-control">
                        <option value="" selected="selected">Select Category</option>
                        <option value="General">General</option>
                        <option value="Content">Content</option>
                    </select>
                </div>

                <div class="form-group">
                  <label for="Feedback">Feedback:</label>
                  <textarea class="form-control" placeholder="Enter Feedback" name="Feedback" id="Feedback" required=""></textarea>
              </div>

                <div class="row">
        <div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
          
          <span id="image_captcha"><?php echo $captchaImg; ?></span><a href="javascript:void(0);" class="captcha-refresh" ><img height="30" width="30" src="<?php echo base_url().'assets/images/Refresh.png'; ?>"/></a><br/><br/>
          
                       <input type="text" name="captcha" value="" class="form-control" autocomplete="off"/>
                        
        </div>
      </div><br/>

             
            <br>

            <div class="form-group">
                <button type="submit" class="btn btn-success" id="Submitfeedback">Submit</button>
            </div>
            <?php echo form_close(); ?>
        </div>

    </div>

</div>
</div>

</div>

<div class="row ">

</div>

</div>





</div>


<script src="<?php echo site_url('common_libs/js')?>/jquery.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.3/umd/popper.min.js"></script> -->
<script src="<?php echo site_url('common_libs/js')?>/bootstrap.min.js"></script>

<script>
       $(document).ready(function(){
        
          $('.captcha-refresh').on('click', function(){
               $.get('<?php echo base_url().'login/refresh'; ?>', function(data){
                   $('#image_captcha').html(data);
               });
           });
       });
   </script>
<script type="text/javascript">

    $('.messagedata').keypress(function (e) {
        var regex = new RegExp(/^[a-zA-Z\s]+$/);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        else {
            e.preventDefault();
            return false;
        }
    });

 </script>





</body>

</html>