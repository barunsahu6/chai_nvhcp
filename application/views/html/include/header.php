<!DOCTYPE html>
<html lang="en">

<head>
    <title>|| NVHCP ||</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo site_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/css/font-awesome.css')?>">
    <link rel="stylesheet" href="<?php echo site_url('assets/css/main_styles.css')?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css"> 
    </head>

<body data-spy="scroll" data-target="#myScrollspy" data-offset="1">
    <div class="container-fluid">
        <div class="row" style="background-color: #ffffff;color: #333;padding-bottom: 3px;">
            <div class="col">

                <div class="top-ul">
                    <ul>
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                         <li><a href="<?php echo base_url(); ?>feedback">Feedback</a></li>
                    </ul>
                </div>
            </div>
             <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 pull-right">
                                        <marquee direction="left" >
                                        Call- <a href="tel:1800 11 6666">1800 11 6666</a> for any questions or complaints
                                        </marquee>
                                       
                                    </div>
        </div>
        <div class="row" style="background-color: #e6c478;">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 d-none d-lg-block" style="padding-top: 5px;padding-bottom: 5px;">
               <img src="<?php echo site_url('assets/images') ?>/ministry_health.png" style="height: 70px;">
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-4 d-none d-lg-block text-center" style="padding-top: 5px;padding-bottom: 5px;">
                <a href="<?php echo base_url();?>"> <img src="<?php echo site_url('assets/images') ?>/mainlogo.png" style="height: 55px;margin-top: 8px;"></a>
               <!-- <h4 class="text-white" style="margin-top: 0px;">
                   <span style="float:left;width:auto;height:auto; text-align: left;">
                    National Viral Hepatitis Control Program
                <br>
               राष्ट्रीय वायरल हेपेटाइटिस नियंत्रण कार्यक्रम

            </span>
            <span style="float:left;width: 15%;height:auto;">
                    <img src="images/logo2.png" style="height: 70px;width: 100%;">
                </span>
                </h4>-->
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 d-none d-lg-block text-right" style="padding-top: 5px;padding-bottom: 5px;">
                <img src="<?php echo site_url('assets/images')?>/national_health.png" style="height: 70px;">
            </div>
        </div>

        <div class="row">
            <nav class="navbar navbar-expand-md bg-white navbar-dark">
                <!-- Brand -->
               <a class="navbar-brand d-block d-lg-none" href="#">
                     <img src="<?php echo site_url('assets/images')?>/reslogo1.png" style="height: 174px; margin-top: 8px;">
                </a>
              
                <!-- Toggler/collapsibe Button -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <!-- Navbar links -->
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav m-auto">
                        <li class="nav-item">
                          <a class="nav-link" href="<?php echo base_url(); ?>about_us">About Us</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="<?php echo base_url(); ?>Guidelines">Guidelines </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url(); ?>iec">IEC </a>
                          </li>
                        <li class="nav-item">
                          <a class="nav-link" href="<?php echo base_url(); ?>faqs">FAQs</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url(); ?>contact">Contact Us</a>
                          </li> 
                      </ul>
                </div> 
              </nav>
        </div>
    

