<!DOCTYPE html>
<html lang="en">

<head>
 
    <style>
        .alert {
position: absolute;
padding: .75rem 1.25rem;
    padding-right: 1.25rem;
margin-bottom: 1rem;
border: 1px solid transparent;
    border-top-color: transparent;
    border-right-color: transparent;
    border-bottom-color: transparent;
    border-left-color: transparent;
border-radius: .25rem;
left: -15px;
width: 72px;
height: auto;
top: 180px;
z-index: 100;
}
.alert-info {
color: #0c5460;
background-color: transparent;
border-color: transparent;
}
.alert-dismissible .close {
padding: 0rem 0rem;
background-color: #ccc;
width: 82%;
}
.card {
border: 0px solid;
}
.btn-outline-info:hover {
    color: #fff;
    background-color: #17a2b8 !important;
    border-color: #17a2b8;
}
.img-set{
    padding-top: 20px;
    width: 60%;
    margin-left: 20%;
}
.img-set2{
    padding-top: 20px;
    width: 20%;
    margin-left: 40%;
}
.btn-outline-success {
    color: #ee82ee;
    border-color: #ee82ee;
}
.btn-outline-success:hover {
    color: #fff;
    background-color: #ee82ee;
    border-color: #ee82ee;
}
.cad{
    color: inherit !important;
}
.nav-tabs .nav-link.active {
color: #17a2b8;
background-color: transparent;
border-color: #17a2b8 #17a2b8 #fff;
border-bottom: 4px solid #17a2b8;
}
.nav-tabs .nav-link {
border: 0px solid transparent;
color: #545454;
}
.nav-link {
    display: block;
    padding: 6px 10px;
}
.nav-tabs .nav-link:focus, .nav-tabs .nav-link:hover {
border-color: #17a2b8 #17a2b8 #17a2b8;
color: #17a2b8;
}







.MultiCarousel
        {
            float: left;
            overflow: hidden;
            padding: 15px;
            width: 100%;
            position: relative;
        }
        .MultiCarousel .MultiCarousel-inner
        {
            transition: 1s ease all;
            float: left;
        }
        .MultiCarousel .MultiCarousel-inner .item
        {
            float: left;
        }
        .MultiCarousel .MultiCarousel-inner .item > div
        {
            padding: 10px;
            margin: 10px;
            /* background: #f1f1f1; */
            color: #666;
            box-shadow: 0px 0px 10px #ccc;
        }
        .MultiCarousel .leftLst, .MultiCarousel .rightLst
        {
            position: absolute;
            border-radius: 50%;
            top: calc(50% - 20px);
        }
        .MultiCarousel .leftLst
        {
            left: 0;
        }
        .MultiCarousel .rightLst
        {
            right: 0;
        }
        
        .MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over
        {
            pointer-events: none;
            background: #ccc;
        }
        .slid-btn{
            color: #fff;text-decoration: underline !important;font-size: 12px;position: absolute;bottom: 15px;
        }
        .bg-success {
    background-color: #26a591!important;
}
.bg-warning {
    background-color: #ff7918!important;
}
    </style>
</head>

<body data-spy="scroll" data-target="#myScrollspy" data-offset="1">
    <div class="container-fluid">

         <div class="row">
                <div id="nav_div">
                        <div id="" class="">
                            <div class="row hid" style="background-color: #ffffff;color: #333;padding-bottom: 3px;margin: 0px;">
                                <div class="col-md-4" style="text-align: left;padding: 0px;">
            
                                    <div class="top-ul">
                                        <ul>
                                            <li><a href="index.php">Home</a></li>
                                             <li><a href="feedback.php">Feedback</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-8" style="text-align: right;margin-top: 3px;">
                                    <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12 pull-right">
                                        
                                        
                                            <div class="input-group">
                                             
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="row " style="margin: 0;background-color: #e6c478;">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 d-none d-lg-block" style="padding-top: 5px;padding-bottom: 5px;">
                                    <img src="images/ministry_health.png" style="height: 70px;">
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-4 d-none d-lg-block text-center" style="padding-top: 5px;padding-bottom: 5px;">
                                    <h4 class="text-white" style="margin-top: 0px;">
                                       <span style="float:left;width:auto;height:auto; text-align: left;">
                                        National Viral Hepatitis Control Program
                                    <br>
                                   राष्ट्रीय वायरल हेपेटाइटिस नियंत्रण कार्यक्रम

                                </span>
                                <span style="float:left;width: 15%;height:auto;">
                                        <img src="images/logo2.png" style="height: 70px;width: 100%;">
                                    </span>
                                    </h4>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 d-none d-lg-block text-right" style="padding-top: 5px;padding-bottom: 5px;">
                                    <img src="images/national_health.png" style="height: 70px;">
                                </div>
                            </div>
            
                            <div id="menu_area" class="menu-area">
                                <div class="container-fluid">
                                    <div class="row">
                                        <nav class="navbar navbar-light bg-white navbar-expand-lg mainmenu ">
                                            <a class="navbar-brand d-block d-lg-none" href="#">
                                                <img class="" src="images/national_health.png" style="height: 60px;">
                                            </a>
                                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                                                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                                <span class="navbar-toggler-icon"></span>
                                            </button>
            
                                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
            
                                                <img class="l-m" src="images/national_health.png" style="height: 40px;">
                                                <ul class="navbar-nav m-auto">
                                                    <!-- <li class="active"><a href="index.php?i_2" style="padding: 10px 10px 10px 10px;">Home
                                                            <span class="sr-only">(current)</span></a></li> -->
                                                     <li class="dropdown">
                                            <a class="" href="about_us.php" id="navbarDropdown" role="button"
                                                 aria-haspopup="true" aria-expanded="false">About Us
                                            </a>
                                            
                                        </li>
                                      
                                        
                                
                                        <li class="dropdown">
                                            <a class="dropdown"  href="Guidelines.php">Guidelines <b class="caret hidden-xs"></b></a>
                                        
                                        </li>
                                        
                                             <li><a href="faqs.php" style="padding: 10px 10px 10px 10px;">FAQs</a></li>
                                        <li><a href="contact.php" style="padding: 10px 10px 10px 10px;">Contact Us</a></li>
                                    </ul>
                                                <!-- <ul class="navbar-nav ml-auto">
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                                            <i class="fa fa-sign-in"></i> Login
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="#">Login</a>
                                                            <a class="dropdown-item" href="#">Sign Up</a>
                                                        </div>
                                                    </li>
                                                </ul> -->
                                            </div>
                                        </nav>
                                    </div>
            
            
                                </div>
                            </div>
            
            
                        </div>
                    </div>
        </div>
        <!-- Navbar end -->
        <div id="main_container">
            <div class="row" style="margin-top: 15px">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
                    <div class="card" style="box-shadow: 0px 2px 6px 4px #cccccc;">
                        <div class="card-header bg-warning text-white text-center" style="display: block !important">Important Links</div>
                        <div class="card-body" style="height:160px;overflow: auto;">
                            
                            <div class="card-ul">
                                <ul>
                                     <li><a href="#" style="font-size:12px;">NVHCP Management Information System</a></li>
                                   <li><a href="List-of-Model-Treatment-Centres.php" style="font-size:12px;">Proposed List of Model Treatment Centres and State Laboratories
</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="box-shadow: 0px 2px 6px 4px #cccccc;margin-top: 15px;">
                        <div class="card-header bg-warning text-white text-center" style="display: block !important">News & Highlights</div>
                        <div class="card-body" style="height:160px;overflow: auto;">
                            
                            <div class="card-ul">
                                <p class="text-justify">28th July 2018</p>
                               <ul>
                                   <li><a href="http://pib.nic.in/newsite/PrintRelease.aspx?relid=181152" target="_blanck" style="font-size:12px;"> Honourable Health Minister Shri J P Nadda Launches National Viral Hepatitis Control Program </a></li>
                               </ul>
                           </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                    <!-- <img src="images/banner.png" style="width: 100%;"> -->
                    <div id="demo" class="carousel slide" data-ride="carousel">

                            <!-- Indicators -->
                            <ul class="carousel-indicators">
                              <li data-target="#demo" data-slide-to="0" class="active"></li>
                              <li data-target="#demo" data-slide-to="1"></li>
                              <li data-target="#demo" data-slide-to="2"></li>
                              <li data-target="#demo" data-slide-to="3"></li>
                               <li data-target="#demo" data-slide-to="4"></li>
                                <li data-target="#demo" data-slide-to="5"></li>
                            </ul>
                          
                            <!-- The slideshow -->
                            <div class="carousel-inner">

                                 <div class="carousel-item active">
                                <img src="images/nvhcpbk.jpg" alt="Ticker2">
                              </div>
                                 <div class="carousel-item ">
                                <img src="images/ticker_1.jpg" alt="Ticker4">
                              </div>

                                <div class="carousel-item ">
                                <img src="images/ticker_2.jpg" alt="Ticker1">
                              </div>
                             <div class="carousel-item">
                                <img src="images/stamp picture 4.png" alt="Ticker3">
                              </div>
                              <div class="carousel-item">
                                <img src="images/GL-Front-page-collated-pic-2.jpg" alt="Ticker5">
                              </div>
                               <div class="carousel-item">
                                <img src="images/Slide_website_009.png" alt="Ticker3">
                              </div>

                              

                            </div>
                          
                            <!-- Left and right controls -->
                            <a class="carousel-control-prev" href="#demo" data-slide="prev">
                              <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a class="carousel-control-next" href="#demo" data-slide="next">
                              <span class="carousel-control-next-icon"></span>
                            </a>
                          
                          </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
                        <div class="card" style="box-shadow: 0px 2px 6px 4px #cccccc;">
                                <!-- <div class="card-header bg-warning text-white text-center" style="display: block !important">News & Highlights</div>
                                <div class="card-body" style="height: 385px;overflow: auto;">
                                        <div class="card-ul">
                                             <p class="text-justify" style="margin-left: -10px;color: #25a08c;font-size: 14px;font-weight: 600;">July 28 2018</p>
                                            <ul>
                                                <li><a href="http://pib.nic.in/newsite/PrintRelease.aspx?relid=181152" target="_blanck" style="font-size:12px;"> Honourable Health Minister Shri J P Nadda Launches National Viral Hepatitis Control Program</a></li>
                                            </ul>
                                        </div>
                                    </div> -->
                                    <table class="table  table-bordered">
                                        <tr>
                                            <th colspan="2" style="padding: 8px;">Hon’ble Cabinet Minister</th>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="text-center">
                                                <img src="images/cabinet-minister.jpg" style="width: 125px;height: 120px;">
                                                <p style="margin-bottom: 0px;">Shri Jagat Prakash Nadda</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="2" style="padding: 8px;">Hon’ble Minister of State</th>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <img src="images/mos.jpg" style="width: 100px;height: 90px;">
                                                <p style="margin-bottom: 0px;">Shri Ashwini Kumar Choubey</p>
                                            </td>
                                            <td class="text-center">
                                                <img src="images/patel.jpg" style="width: 100px;height: 90px;">
                                                <p  style="margin-bottom: 0px;">Smt. Anupriya Patel</p>
                                            </td>
                                        </tr>
                                    </table>
                            </div>
                </div>
               
            </div>
            <div class="row" style="margin-top: 15px;">
                <div class="col-sm-12" style="box-shadow: 0px 0px 20px #ccc;margin-top: 12px;">
                    
                 <h3 class="text-center" style="color: #26a591; margin-bottom: 0px;padding: 20px 0px;">   Latest From NVHCP
                    
                </h3>
                </div>
                <div class="container">

                      
                        <div class="row">
                                <div class="MultiCarousel" data-items="1,2,3" data-slide="1" id="MultiCarousel1"
                                    data-interval="500">
                                    <div class="MultiCarousel-inner" style="transform: translateX(0px); width: 1554px;">
                                        <div class="item col-xs-4">
                                            <div class="pad15">
                                                    <div class="card bg-success text-white">
                                                        <div class="card-header" style="display: block">
                                                            <h4 style="font-size: 16px;">Development of National Action Plan <br/><br/></h4></div>
                                                            <!-- <img class="card-img-top" src="images/girl.png" alt="Card image"> -->
                                                            
                                                            <!-- <div class="card-img-overlay"> -->
                                                                <div class="card-body" style="height: 245px;overflow: auto;">
                                                             
                                                            
                                                              <p class="card-text" style="font-size: 14px;     text-align: justify;">
                                                                    A National Steering committee was formed for development of National Action Plan for viral hepatitis in January 2017. The committee recommended to constitute working groups under various thematic areas related to viral hepatitis including surveillance of viral hepatitis, laboratory diagnosis, treatment, immunization, blood safety, injection safety and infection control, 
                                                              </p>
                                                              <a href="Development.php" class="slid-btn">Read More..</a>
                                                            <!-- </div> -->
                                                          </div>
                                                        </div>
                                            </div>
                                        </div>

                                        <div class="item col-xs-4">
                                                <div class="pad15">
                                                        <div class="card bg-warning text-white">
                                                            <div class="card-header" style="display: block">
                                                                <h4 style="font-size: 16px;">Launch of National Viral Hepatitis Control Program</h4></div>
                                                                <!-- <img class="card-img-top" src="images/women.png" alt="Card image"> -->
                                                                
                                                                <!-- <div class="card-img-overlay"> -->
                                                                        <div class="card-body" style="height: 245px;overflow: auto;">
                                                                 
                                                                  <p class="card-text" style="font-size: 14px;     text-align: justify;">
                                                                        ‘National Viral Hepatitis Control Program’ was launched on 28th July, 2018 on the occasion of World Hepatitis Day by the hon’ble HFM with the aim to
to combat hepatitis and achieve countrywide elimination of Hepatitis C by 2030, achieve significant reduction in the infected population, morbidity and mortality associated with Hepatitis A,B, C and E.
                                                                  </p>
                                                                  <a href="Launch-of-National-Viral-Hepatitis-Control-Program.php" class="slid-btn">Read More..</a>
                                                                <!-- </div> -->
                                                                </div>
                                                              </div>
                                                </div>
                                            </div>

                                            <div class="item col-xs-4">
                                                    <div class="pad15">
                                                            <div class="card bg-success text-white">
                                                                <div class="card-header" style="display: block;z-index: 10000;">
                                                                    <h4 style="font-size: 16px; " title="National Training of Trainers for Diagnosis and Management of Viral Hepatitis"> 
                                                                            National Training of Trainers for Diagnosis and Management.. 
                                                                        </h4></div>
                                                                    <!-- <img class="card-img-top" src="images/girl.png" alt="Card image">
                                                                    
                                                                    <div class="card-img-overlay"> -->
                                                                            <div class="card-body" style="height: 245px;overflow: auto;">
                                                                      <p class="card-text" style="font-size: 14px;     text-align: justify;" >
                                                                           Two rounds of national training of trainers were held to train the experts from across the country in diagnosis and management of viral hepatitis according to the standardized protocols and testing algorithms prepared by the technical resource group on laboratory and care support and treatment... 
                                                                      </p>
                                                                      <a href="National-Training-of-Trainers.php" class="slid-btn">Read More..</a>
                                                                    </div>
                                                                  <!-- </div> -->
                                                                  </div>
                                                    </div>
                                                </div>

                                                <div class="item col-xs-4">
                                                        <div class="pad15">
                                                                <div class="card bg-warning text-white">
                                                                    <div class="card-header" style="display: block;z-index: 10000;">
                                                                        <h4 style="font-size: 16px;" title="Sensitization workshop for the State Nodal Officers-National Viral Hepatitis Control Program">Sensitization workshop for the State Nodal... </h4></div>
                                                                        <!-- <img class="card-img-top" src="images/women.png" alt="Card image">
                                                                        
                                                                        <div class="card-img-overlay"> -->
                                                                                <div class="card-body" style="height: 245px;overflow: auto;">
                                                                          <p class="card-text" style="font-size: 14px;"     text-align: justify;>
                                                                                A meeting of the state nodal officers of NVHCP of all the states was held on 6th-7th Sept., 2018 and 29-30th Jan., 2019 to sensitise them regarding the operational components of the for successful roll out of the program...
                                                                          </p>
                                                                          <a href="Sensitization-workshop.php" class="slid-btn">Read More..</a>
                                                                        </div>
                                                                      <!-- </div> -->
                                                                      </div>
                                                        </div>
                                                    </div>

                                                 

                                                    
                                        
                                      
                                       
                                        
                                    </div>
                                    <button class="btn btn-primary leftLst over">
                                        &lt;</button>
                                    <button class="btn btn-primary rightLst">
                                        &gt;</button>
                                </div>
                            </div>
                        </div>


                        
                   
                </div>
 
  <div class="col-sm-12" style="box-shadow: 0px 0px 20px #ccc;margin-top: 12px;">
                    
                 <h3 class="text-center" style="color: #4e5453;; margin-bottom: 0px;padding: padding: 3px 0px; font-size: 16px;">  Related Links
                    
                </h3>
                </div>

<div class="row" style="box-shadow: 0px 0px 20px #ccc;margin-top: 12px;margin-bottom: 30px;padding: 20px 0px;">
 
        <div class="container">

            <div class="row text-center">
                <div class="col"> <a class="aa1" href="https://mohfw.gov.in/" target="_blanck" style="font-size: 12px;">  Ministry of Health and Family Welfare, Govt. of India</a></div>
                <div class="col"> <a class="aaa1" href="http://nhm.gov.in/" target="_blanck" style="font-size: 12px;"> National Health Mission <br/></a></div>
               
            </div>
        </div>
</div>
                
                <div class="row ">
 <?php  include 'include/footer.php';?>
                    </div>

            </div>



 <div class="clearfix">
            <div class="googleImg" title="The translation (transliteration) is solely powered by Google and is not official translation of Ministry of External Affairs, India"></div>
            <div class="googleLang">
              <div id="google_translate_element"></div>
            </div>
          </div>


        </div>
       
   
    <script src="js/jquery.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.3/umd/popper.min.js"></script> -->
    <script src="js/bootstrap.min.js"></script>
  
    <script type="text/javascript">        $(document).ready(function () {
        var itemsMainDiv = ('.MultiCarousel');
        var itemsDiv = ('.MultiCarousel-inner');
        var itemWidth = "";

        $('.leftLst, .rightLst').click(function () {
            var condition = $(this).hasClass("leftLst");
            if (condition)
                click(0, this);
            else
                click(1, this)
        });

        ResCarouselSize();
        hedName
        $(window).resize(function () {
            ResCarouselSize();
        });

        //this function define the size of the items
        function ResCarouselSize() {
            var incno = 0;
            var dataItems = ("data-items");
            var itemClass = ('.item');
            var id = 0;
            var btnParentSb = '';
            var itemsSplit = '';
            var sampwidth = $(itemsMainDiv).width();
            var bodyWidth = $('body').width();
            $(itemsDiv).each(function () {
                id = id + 1;
                var itemNumbers = $(this).find(itemClass).length;
                btnParentSb = $(this).parent().attr(dataItems);
                itemsSplit = btnParentSb.split(',');
                $(this).parent().attr("id", "MultiCarousel" + id);
               

                if (bodyWidth >= 1200) {
                    incno = itemsSplit[2];
                    itemWidth = sampwidth / incno;
                }
                else if (bodyWidth >= 992) {
                    incno = itemsSplit[2];
                    itemWidth = sampwidth / incno;
                }
                else if (bodyWidth >= 768) {
                    incno = itemsSplit[1];
                    itemWidth = sampwidth / incno;
                }
                else {
                    incno = itemsSplit[0];
                    itemWidth = sampwidth / incno;
                }

                $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
                $(this).find(itemClass).each(function () {
                    $(this).outerWidth(itemWidth);
                });

                $(".leftLst").addClass("over");
                $(".rightLst").removeClass("over");

            });
        }


        //this function used to move the items
        function ResCarousel(e, el, s) {
            var leftBtn = ('.leftLst');
            var rightBtn = ('.rightLst');
            var translateXval = '';
            var divStyle = $(el + ' ' + itemsDiv).css('transform');
            var values = divStyle.match(/-?[\d\.]+/g);
            var xds = Math.abs(values[4]);
            if (e == 0) {
                translateXval = parseInt(xds) - parseInt(itemWidth * s);
                $(el + ' ' + rightBtn).removeClass("over");

                if (translateXval <= itemWidth / 2) {
                    translateXval = 0;
                    $(el + ' ' + leftBtn).addClass("over");
                }
            }
            else if (e == 1) {
                var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
                translateXval = parseInt(xds) + parseInt(itemWidth * s);
                $(el + ' ' + leftBtn).removeClass("over");

                if (translateXval >= itemsCondition - itemWidth / 2) {
                    translateXval = itemsCondition;
                    $(el + ' ' + rightBtn).addClass("over");
                }
            }
            $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
        }

        //It is used to get some elements from btn
        function click(ell, ee) {
            var Parent = "#" + $(ee).parent().attr("id");
            var slide = $(Parent).attr("data-slide");
            ResCarousel(ell, Parent, slide);
        }

    });</script>

    
    <script>
        (function ($) {
            $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
                if (!$(this).next().hasClass('show')) {
                    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
                }
                var $subMenu = $(this).next(".dropdown-menu");
                $subMenu.toggleClass('show');

                $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                    $('.dropdown-submenu .show').removeClass("show");
                });

                return false;
            });
        })(jQuery)
    </script>
    <!-- <script>
        $(window).scroll(function () {
            $("#t-menu").stop().animate({ "marginTop": ($(window).scrollTop()) + "px", "marginLeft": ($(window).scrollLeft()) + "px" }, "slow");
        });
        var totaltext = "";
        for (var i = 0; i < 100; i++) {
            totaltext += "scroll!<br />";
        }
        $("#main_container").html();
    </script> -->
    <!-- <script type="text/javascript">
        var screenheight = window.innerHeight;
        
        var heightprop = screenheight - 72;
        document.getElementById('main_container').style.height = heightprop + 'px';
    </script>
    <script type="text/javascript">
        $('#hover')
            .mouseenter(function () {
                $('#divv').show(1000);
            })
            .click(function (e) {
                e.preventDefault();
                $('#divv').hide(1000);
            });
    </script> -->
    <!-- <script type="text/javascript">
        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > 81) {
                $('.fixed-menu').addClass('fixed-m');
                $('.l-m').addClass('l-mn');
                $('.bg-white').addClass('bg-success');

            } else {
                $('.fixed-menu').removeClass('fixed-m');
                $('.l-m').removeClass('l-mn');
                $('.bg-white').removeClass('bg-success');
            }
        });
    </script> -->
    

    

</body>

</html>