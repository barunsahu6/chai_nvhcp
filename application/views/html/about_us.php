
    <style>
        .alert {
position: absolute;
padding: .75rem 1.25rem;
    padding-right: 1.25rem;
margin-bottom: 1rem;
border: 1px solid transparent;
    border-top-color: transparent;
    border-right-color: transparent;
    border-bottom-color: transparent;
    border-left-color: transparent;
border-radius: .25rem;
left: -15px;
width: 72px;
height: auto;
top: 180px;
z-index: 100;
}
.alert-info {
color: #0c5460;
background-color: transparent;
border-color: transparent;
}
.alert-dismissible .close {
padding: 0rem 0rem;
background-color: #ccc;
width: 82%;
}
.card {
border: 0px solid;
}
.btn-outline-info:hover {
    color: #fff;
    background-color: #17a2b8 !important;
    border-color: #17a2b8;
}
.img-set{
    padding-top: 20px;
    width: 60%;
    margin-left: 20%;
}
.img-set2{
    padding-top: 20px;
    width: 20%;
    margin-left: 40%;
}
.btn-outline-success {
    color: #ee82ee;
    border-color: #ee82ee;
}
.btn-outline-success:hover {
    color: #fff;
    background-color: #ee82ee;
    border-color: #ee82ee;
}
.cad{
    color: inherit !important;
}



.MultiCarousel
        {
            float: left;
            overflow: hidden;
            padding: 15px;
            width: 100%;
            position: relative;
        }
        .MultiCarousel .MultiCarousel-inner
        {
            transition: 1s ease all;
            float: left;
        }
        .MultiCarousel .MultiCarousel-inner .item
        {
            float: left;
        }
        .MultiCarousel .MultiCarousel-inner .item > div
        {
            padding: 10px;
            margin: 10px;
            /* background: #f1f1f1; */
            color: #666;
            box-shadow: 0px 0px 10px #ccc;
        }
        .MultiCarousel .leftLst, .MultiCarousel .rightLst
        {
            position: absolute;
            border-radius: 50%;
            top: calc(50% - 20px);
        }
        .MultiCarousel .leftLst
        {
            left: 0;
        }
        .MultiCarousel .rightLst
        {
            right: 0;
        }
        
        .MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over
        {
            pointer-events: none;
            background: #ccc;
        }
        .slid-btn{
            color: #fff;text-decoration: underline !important;font-size: 12px;position: absolute;bottom: 15px;
        }
        .bg-success {
    background-color: #26a591!important;
}
.bg-warning {
    background-color: #ff7918!important;
}
    </style>


<div class="jumbotron">
  <h1>About Us</h1> 
  <br/>
  <h2>National Viral Hepatitis Control Program</h2>
  <p>Viral hepatitis is recognized as an important public health problem worldwide. According to WHO estimates, viral hepatitis caused 1.34 million deaths globally in 2015, a number comparable to deaths due to tuberculosis, worldwide. In India, it is estimated that there are 40 million people suffering from Hepatitis B and 6-12 million people suffering from Hepatitis C. </p> 
  <p>The aim of the program is to combat hepatitis and achieve countrywide elimination of Hepatitis C by 2030, achieve significant reduction in the infected population, morbidity and mortality associated with Hepatitis B and C viz. Cirrhosis and Hepato-cellular carcinoma (liver cancer) and reduce the risk, morbidity and mortality due to Hepatitis A and E.</p>
  <p>This program is in line with our global commitment towards achieving Sustainable development goal (SDG) goal 3; target 3.3 which aims to “By 2030, end the epidemics of AIDS, tuberculosis, malaria and neglected tropical diseases and combat hepatitis, water borne diseases and other communicable diseases” The Government of India is a signatory to the resolution 69.22 endorsed in the WHO Global Health Sector Strategy on Viral Hepatitis 2016-2021 at 69th WHA towards ending viral hepatitis by 2030.</p>
  <p>Integrating the intervention within the existing health systems framework under National Health Mission rather will further complement the efforts of increasing access to testing and management of viral hepatitis till health and wellness centre. </p>
  <p>The program proposes to offer free drugs and diagnostics for hepatitis B & C and management of hepatitis A and E.</p>
  <p>Coordination and collaboration with other national programs and schemes to provide a promotive, preventive and curative package of services will further augment government of India’s determined efforts towards achieving the goal. Adult vaccination for hepatitis B among key population and healthcare workers has also been initiated as a preventive measure under this initiative. A policy to use ‘Re-use prevention syringes’ to ensure injection safety has also been formulated. Another strategy under the program is on screening of hepatitis B in places where institutional delivery is less than 80% to ensure provision of birth dose hepatitis B vaccination and Hepatitis B immunoglobulin, if required</p>
  <p>MoHFW in partnership with the states will make all out efforts to work in this endeavour. Effective participation between government of India, state governments and other stakeholders will progressively strengthen the program and will remain the hallmark for achieving the program’s aim.</p>
  <p>Department of post has come out with a commemorative postage stamp which was released on the launch event. This is a good medium for spreading awareness about viral hepatitis in the public.</p>

  <h4>Aim</h4> 
<p>1. Combat hepatitis and achieve country wide elimination of Hepatitis C by 2030 </p>
<p>2. Achieve significant reduction in the infected population, morbidity and mortality associated with Hepatitis B and C viz. Cirrhosis and Hepato-cellular carcinoma (liver cancer) </p>
<p>3. Reduce the risk, morbidity and mortality due to Hepatitis A and E.</p>

<h4>Key Objectives</h4>
<p>1. Enhance community awareness on hepatitis and lay stress on preventive measures among general population especially high-risk groups and in hotspots.</p> 
<p>2. Provide early diagnosis and management of viral hepatitis at all levels of healthcare. </p>
<p>3. Develop standard diagnostic and treatment protocols for management of viral hepatitis and its complications. </p>
<p>4. Strengthen the existing infrastructure facilities, build capacities of existing human resource and raise additional human resources, where required, for providing comprehensive services for management of viral hepatitis and its complications in all districts of the country. </p>
<p>5. Develop linkages with the existing National programmes towards awareness, prevention, diagnosis and treatment for viral hepatitis. </p>
<p>6. Develop a web-based “Viral Hepatitis Information and Management System” to maintain a registry of persons affected with viral hepatitis and its sequelae.</p>

<h4>Components </h4>
<h6>The key components include: </h6>

<h6>1. Preventive component: This remains the cornerstone of the NVHCP. It will include </h6>
<ul>

<li><p> Awareness generation </p></li>
<li><p> Immunization of Hepatitis B (birth dose, high risk groups, health care workers) </p></li>
<li><p> Safety of blood and blood products d. Injection safety, safe socio-cultural practices </p></li>
<li><p> Safe drinking water, hygiene and sanitary toilets </p></li>
</ul>
<h6>2. Diagnosis and Treatment: </h6>
<ul>
<li><p>Screening of pregnant women for HBsAg to be done in areas where institutional deliveries are < 80% to ensure their referral for institutional delivery for birth dose Hepatitis B vaccination. </p></li>
<li><p> Free screening, diagnosis and treatment for both hepatitis B and C would be made available at all levels of health care in a phased manner. </p></li>
<li><p>Provision of linkages, including with private sector and not for profit institutions, for diagnosis and treatment. </p></li>
<li><p> Engagement with community/peer support to enhance and ensure adherence to treatment and demand generation. </p></li>
</ul>

<p>3. Monitoring and Evaluation, Surveillance and Research Effective linkages to the surveillance system would be established and operational research would be undertaken through Department of Health Research (DHR). Standardised M&E framework would be developed and an online web based system established. </p>

<h6>4. Training and capacity Building: </h6>
<p>This would be a continuous process and will be supported by NCDC, ILBS and state tertiary care institutes and coordinated by NVHCP. The hepatitis induction and update programs for all level of health care workers would be made available using both, the traditional cascade model of training through master trainers and various platforms available for enabling electronic, e-learning and e-courses.</p>
</ul>
<h4>Organizational Structure</h4>

<img src="<?php echo site_url('assets/images')?>/NVHCP_Orgstructure.PNG" width="60%">

</div>
                <div class="row ">
 
                    </div>

            </div>





        </div>
       
   

  
    <script type="text/javascript">        $(document).ready(function () {
        var itemsMainDiv = ('.MultiCarousel');
        var itemsDiv = ('.MultiCarousel-inner');
        var itemWidth = "";

        $('.leftLst, .rightLst').click(function () {
            var condition = $(this).hasClass("leftLst");
            if (condition)
                click(0, this);
            else
                click(1, this)
        });

        ResCarouselSize();




        $(window).resize(function () {
            ResCarouselSize();
        });

        //this function define the size of the items
        function ResCarouselSize() {
            var incno = 0;
            var dataItems = ("data-items");
            var itemClass = ('.item');
            var id = 0;
            var btnParentSb = '';
            var itemsSplit = '';
            var sampwidth = $(itemsMainDiv).width();
            var bodyWidth = $('body').width();
            $(itemsDiv).each(function () {
                id = id + 1;
                var itemNumbers = $(this).find(itemClass).length;
                btnParentSb = $(this).parent().attr(dataItems);
                itemsSplit = btnParentSb.split(',');
                $(this).parent().attr("id", "MultiCarousel" + id);
               

                if (bodyWidth >= 1200) {
                    incno = itemsSplit[2];
                    itemWidth = sampwidth / incno;
                }
                else if (bodyWidth >= 992) {
                    incno = itemsSplit[2];
                    itemWidth = sampwidth / incno;
                }
                else if (bodyWidth >= 768) {
                    incno = itemsSplit[1];
                    itemWidth = sampwidth / incno;
                }
                else {
                    incno = itemsSplit[0];
                    itemWidth = sampwidth / incno;
                }

                $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
                $(this).find(itemClass).each(function () {
                    $(this).outerWidth(itemWidth);
                });

                $(".leftLst").addClass("over");
                $(".rightLst").removeClass("over");

            });
        }


        //this function used to move the items
        function ResCarousel(e, el, s) {
            var leftBtn = ('.leftLst');
            var rightBtn = ('.rightLst');
            var translateXval = '';
            var divStyle = $(el + ' ' + itemsDiv).css('transform');
            var values = divStyle.match(/-?[\d\.]+/g);
            var xds = Math.abs(values[4]);
            if (e == 0) {
                translateXval = parseInt(xds) - parseInt(itemWidth * s);
                $(el + ' ' + rightBtn).removeClass("over");

                if (translateXval <= itemWidth / 2) {
                    translateXval = 0;
                    $(el + ' ' + leftBtn).addClass("over");
                }
            }
            else if (e == 1) {
                var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
                translateXval = parseInt(xds) + parseInt(itemWidth * s);
                $(el + ' ' + leftBtn).removeClass("over");

                if (translateXval >= itemsCondition - itemWidth / 2) {
                    translateXval = itemsCondition;
                    $(el + ' ' + rightBtn).addClass("over");
                }
            }
            $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
        }

        //It is used to get some elements from btn
        function click(ell, ee) {
            var Parent = "#" + $(ee).parent().attr("id");
            var slide = $(Parent).attr("data-slide");
            ResCarousel(ell, Parent, slide);
        }

    });</script>

    
    <script>
        (function ($) {
            $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
                if (!$(this).next().hasClass('show')) {
                    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
                }
                var $subMenu = $(this).next(".dropdown-menu");
                $subMenu.toggleClass('show');

                $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                    $('.dropdown-submenu .show').removeClass("show");
                });

                return false;
            });
        })(jQuery)
    </script>
   
    

    

</body>

</html>