
    <style>
        .alert {
position: absolute;
padding: .75rem 1.25rem;
    padding-right: 1.25rem;
margin-bottom: 1rem;
border: 1px solid transparent;
    border-top-color: transparent;
    border-right-color: transparent;
    border-bottom-color: transparent;
    border-left-color: transparent;
border-radius: .25rem;
left: -15px;
width: 72px;
height: auto;
top: 180px;
z-index: 100;
}
.alert-info {
color: #0c5460;
background-color: transparent;
border-color: transparent;
}
.alert-dismissible .close {
padding: 0rem 0rem;
background-color: #ccc;
width: 82%;
}
.card {
border: 0px solid;
}
.btn-outline-info:hover {
    color: #fff;
    background-color: #17a2b8 !important;
    border-color: #17a2b8;
}
.img-set{
    padding-top: 20px;
    width: 60%;
    margin-left: 20%;
}
.img-set2{
    padding-top: 20px;
    width: 20%;
    margin-left: 40%;
}
.btn-outline-success {
    color: #ee82ee;
    border-color: #ee82ee;
}
.btn-outline-success:hover {
    color: #fff;
    background-color: #ee82ee;
    border-color: #ee82ee;
}
.cad{
    color: inherit !important;
}








.MultiCarousel
        {
            float: left;
            overflow: hidden;
            padding: 15px;
            width: 100%;
            position: relative;
        }
        .MultiCarousel .MultiCarousel-inner
        {
            transition: 1s ease all;
            float: left;
        }
        .MultiCarousel .MultiCarousel-inner .item
        {
            float: left;
        }
        .MultiCarousel .MultiCarousel-inner .item > div
        {
            padding: 10px;
            margin: 10px;
            /* background: #f1f1f1; */
            color: #666;
            box-shadow: 0px 0px 10px #ccc;
        }
        .MultiCarousel .leftLst, .MultiCarousel .rightLst
        {
            position: absolute;
            border-radius: 50%;
            top: calc(50% - 20px);
        }
        .MultiCarousel .leftLst
        {
            left: 0;
        }
        .MultiCarousel .rightLst
        {
            right: 0;
        }
        
        .MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over
        {
            pointer-events: none;
            background: #ccc;
        }
        .slid-btn{
            color: #fff;text-decoration: underline !important;font-size: 12px;position: absolute;bottom: 15px;
        }
        .bg-success {
    background-color: #26a591!important;
}
.bg-warning {
    background-color: #ff7918!important;
}
    </style>



<div class="jumbotron">
  <h4>National Viral hepatitis Control Program Frequently Asked Questions</h4>
<br/>
<h4> 1)What is viral hepatitis? </h4>
<p>Acute viral hepatitis is a systemic infection affecting the liver predominantly. Almost all cases of acute viral hepatitis are caused by one of five viral agents: hepatitis A virus (HAV), hepatitis B virus (HBV), hepatitis C virus (HCV), the HBV-associated delta agent or hepatitis D virus (HDV) and hepatitis E virus (HEV). </p>

<h4>2)What are the symptoms of viral hepatitis?</h4>

<p>All types of viral hepatitis produce clinically similar illnesses. These range from asymptomatic and inapparent to fatal acute infections common to all types, on the one hand, to subclinical persistent infections, rapidly progressive chronic liver disease with cirrhosis and even hepato-cellular carcinoma, common to the blood-borne types (HBV, HCV and HDV),on the other.
</p>
<ul>
<li>Symptoms of anorexia, nausea and vomiting, fatigue, malaise, joint pain and muscle pain may precede the onset of jaundice (yellowing of eyes/skin) by 1-2 weeks.</li>
<li>A low-grade fever between 38° and 39°C (100-102°F) is more often present in hepatitis A and E than in hepatitis B or C. </li>
<li>Dark urine and clay-colored stools may be noticed by the patient from 1-5 days before the onset of clinical jaundice (yellow color of skin or sclera). </li>
<li>Mild weight loss (2.5-5 kg) is common. The liver becomes enlarged and tender and may be associated with right upper quadrant pain and discomfort.</li></ul>


<h4>3)Which virus type causes chronic viral hepatitis?</h4>

<p>Both hepatitis A and E are self-limited and do not cause chronic hepatitis. In contrast, the entire clinico-pathologic spectrum of chronic hepatitis occurs in patients with chronic viral hepatitis B and C as well as in patients with chronic hepatitis D superimposed on chronic hepatitis B.</p>

<h4>4)What is the incubation period of viral hepatitis?</h4>
<ul>
<li><p>HAV: 15-45 days, mean 30</p></li>
<li><p>HBV: 30-180 days, mean 60-90</p></li>
<li><p>HCV: 15-160 days, mean 50</p></li>
<li><p>HDV: 30-180 mean 60-90</p></li>
<li><p>HEV (days) 14-60, mean 40</p></li>
</ul>

<h4>5)Is vaccination for viral hepatitis available?</h4>

<p>There are vaccines to prevent hepatitis A and hepatitis B; however, there is no vaccine for hepatitis C.</p>

<p>Vaccination for hepatitis B is available and is a part of the Universal Immunization Program (UIP). The regimen for hepatitis B vaccine includes birth dose and dose at 6, 10 and 14 weeks. It is a part of pentavalent vaccine in the UIP.</p>
<br/>
<h3>Hepatitis B</h3>
<br/>
<h4>1)How is HBV transmitted?</h4>
<p>HBV is transmitted through activities that involve percutaneous (i.e., puncture through the skin) or mucosal contact with infectious blood or body fluids (e.g., semen, saliva), including</p>
<ul>
<li>Sex with an infected partner</li>
<li>Injection drug use that involves sharing needles, syringes, or drug-preparation equipment</li>
<li>Birth to an infected mother</li>
<li>Contact with blood or open sores of an infected person</li>
<li>Needle stick or sharp instrument exposures</li>
<li>Sharing items such as razors or toothbrushes with an infected person</li>
</ul>

<h4>2)What are the high risk groups for HBV infection?</h4>

<h5>The priority populations for testing will include but not limited to:</h5>
<ul>
<li><p>Household and sexual contacts of persons with hepatitis B</p>
<li><p>Babies born to mothers who are screened positive for the surface antigen for hepatitis B (HBsAg)</p>
<li><p>Persons who have used injection drugs</p>
<li><p>Persons with multiple sexual contacts or a history of sexually transmitted disease</p>
<li><p>Men who have sex with men</p>
<li><p>Blood/plasma/organ/tissue/semen donors</p>
<li><p>Persons with HCV or HIV infection</p>
<li><p>Hemodialysis patients</p>
<li><p>Pregnant women</p>
<li><p>Persons who are the source of blood or body fluids that would be an indication for post exposure prophylaxis</p>
<li><p>Persons who require immunosuppressive or cytotoxic therapy</p>
<li><p>Inmates of prisons and other closed settings</p>
</ul>

<h4>3)What should be done in case a woman with HBV gets pregnant?</h4>

<p>All pregnant women with HBV should be evaluated for the need of treatment for hepatitis B and any associated liver disease, and given advice about prevention of transmission. Only a proportion of those with hepatitis B virus infection (pregnant or otherwise) need treatment. </p>
<p>Hepatitis B in a pregnant woman is not a reason for considering termination of pregnancy. Similarly, the need for caesarean delivery should be decided based on obstetric indications, and not on the presence of HBV infection. Administration of hepatitis B vaccine to pregnant women with HBV provides no benefit either to the mother or the baby. The newborn should be administered hepatitis B vaccine along with hepatitis B immunoglobulin (0.5 ml or 100 international units, intramuscular) this should be done as soon after birth as possible (and within 12-24 hours) and in a limb other than the one in which hepatitis B vaccine has been administered.</p>


<h4>4)What are ways by which hepatitis B does not spread?</h4>

<p>HBV is not spread through food or water, sharing eating utensils, breastfeeding, hugging, kissing, hand holding, coughing, or sneezing.</p>

<h4>5)Is hepatitis B treatable? </h4>

<p>Antiviral agents active against HBV are available, and have been shown to suppress HBV replication, prevent progression to cirrhosis, and reduce the risk of HCC and liver-related deaths. However, currently available treatments fail to eradicate the virus in most of those treated, necessitating potentially lifelong treatment. The management for hepatitis B is now available free of cost under National Viral hepatitis Control Program.</p>

<h4>6)Can one donate blood if he/she is harboring hepatitis B infection?</h4>

<p>No</p>
<br/>
<h3>Hepatitis C</h3>
<br/>
<h4>1)What is hepatitis C?</h4>

<p>Hepatitis C is a liver infection caused by the hepatitis C virus. Hepatitis C can range from a mild illness lasting a few weeks to a serious, lifelong illness. Hepatitis C is often described as “acute,” meaning a new infection or “chronic,” meaning lifelong infection.</p>
<ul>
<li>Acute hepatitis C occurs within the first 6 months after someone is exposed to the hepatitis C virus. Hepatitis C can be a short-term illness, but acute infection can lead to a chronic infection.</li>
<li>Chronic hepatitis C can be a lifelong infection with the hepatitis C virus if left untreated. Left untreated, chronic hepatitis C can cause serious health problems, including liver damage, cirrhosis (scarring of the liver), liver cancer, and even death.</li> 
</ul>

<h4>2)How is hepatitis C virus (HCV) transmitted?</h4>

<p>HCV is transmitted primarily through parenteral exposures to infectious blood or body fluids that contain blood. Possible exposures include</p>

<ul>
<li>Injection drug use </li>
<li>Receipt of donated blood, blood products, and organs </li>
<li>Needle-stick injuries in health care settings</li>
<li>Birth to an HCV-infected mother</li>
<li>Sex with an HCV-infected person </li>
<li>Sharing personal items contaminated with infectious blood, such as razors or toothbrushes (also inefficient vectors of transmission)</li>
<li>Other health care procedures that involve invasive procedures, such as injections (usually recognized in the context of outbreaks)</li>
<li>Unregulated tattooing</li>
</ul>
<h4>3)Is hepatitis C treatable?</h4>

<p>Hepatitis C can be treated and the diagnostics and drugs are available free of cost for 12-24 weeks under the National Viral hepatitis Control Program</p>

<h4>4)If I have been cured of hepatitis C earlier, can I get infected again?</h4>

<p>Yes </p>

<h4>5)Which are the high risk groups for hepatitis C infection?</h4>

<h5>The priority populations for testing will include but not limited to:</h5>
<ul>
<li><p>People who inject drugs ( PWID)</p></li>
<li><p>Men who have sex with men</p></li>
<li><p>Female sex workers</p></li>
<li><p>People who received blood transfusion before routine testing for hepatitis C</p></li>
<li><p>People who need frequent blood transfusion, such as, thalessemic and dialysis patients</p></li>
<li><p>People living with HIV</p></li>
<li><p>Inmates of prisons and other closed settings</p></li>

</ul>
<br/>
<h4>6)Can one donate blood if he/she is harboring hepatitis C infection?</h4>

<p>No</p>

<h4>7)What are the symptoms of chronic hepatitis C?</h4>
<p>Most people with chronic hepatitis C virus infection do not have any symptoms or have general, symptoms such as tiredness and depression. Many people eventually develop severe liver damage and liver cancer. </p>

<h4>8)Is chronic hepatitis C serious?</h4>
<p>Chronic hepatitis C can be a serious disease resulting in long-term health problems, including liver damage, liver failure, liver cancer, or even death. </p>

<h4>9)When is the non-cirrhotic hepatitis C patient considered cured?</h4>

<p>HCV infection can be considered cured in non-cirrhotic patients who have achieved a Sustained Virological Response (SVR 12) after 12 weeks of completing the treatment. Thus no follow-up is required. </p>
<p>There is increased risk of re-infection (1-8%) following successful HCV among patients at high risk, such as PWIDs or men who have sex with men, etc. Thus the risk of re-infection should be explained to the patient in order to positively modify risk behavior. Following SVR 12, the monitoring for HCV re-infection should be recommended in these patients with ongoing risk behavior. If re-infection is identified during post-SVR follow-up, then retreatment is indicated.</p>
<br/>
<h3>Hepatitis A and Hepatitis E</h3>
<br/>
<h4>1)How are HAV and HEV transmitted?</h4>

<p>HAV and HEV are both spread by fecal-oral route. </p>

<h4>2)What is the treatment of hepatitis A?</h4>

<p>There is no role for antiviral drugs in therapy for HAV infection. Virtually all previously healthy patients with hepatitis A recover completely. Infection in the community is best prevented by improving social conditions especially overcrowding and poor sanitation.</p>

<h4>3)What is the treatment of hepatitis E?</h4>

<p>There is no specific treatment capable of altering the course of acute hepatitis E. The disease is usually self-limiting and recovers completely. In some cases, hospitalization is required (fulminant hepatitis and symptomatic pregnant women).</p>



</div>
                <div class="row ">

                    </div>

            </div>





        </div>
       
   

  
    <script type="text/javascript">        $(document).ready(function () {
        var itemsMainDiv = ('.MultiCarousel');
        var itemsDiv = ('.MultiCarousel-inner');
        var itemWidth = "";

        $('.leftLst, .rightLst').click(function () {
            var condition = $(this).hasClass("leftLst");
            if (condition)
                click(0, this);
            else
                click(1, this)
        });

        ResCarouselSize();




        $(window).resize(function () {
            ResCarouselSize();
        });

        //this function define the size of the items
        function ResCarouselSize() {
            var incno = 0;
            var dataItems = ("data-items");
            var itemClass = ('.item');
            var id = 0;
            var btnParentSb = '';
            var itemsSplit = '';
            var sampwidth = $(itemsMainDiv).width();
            var bodyWidth = $('body').width();
            $(itemsDiv).each(function () {
                id = id + 1;
                var itemNumbers = $(this).find(itemClass).length;
                btnParentSb = $(this).parent().attr(dataItems);
                itemsSplit = btnParentSb.split(',');
                $(this).parent().attr("id", "MultiCarousel" + id);
               

                if (bodyWidth >= 1200) {
                    incno = itemsSplit[2];
                    itemWidth = sampwidth / incno;
                }
                else if (bodyWidth >= 992) {
                    incno = itemsSplit[2];
                    itemWidth = sampwidth / incno;
                }
                else if (bodyWidth >= 768) {
                    incno = itemsSplit[1];
                    itemWidth = sampwidth / incno;
                }
                else {
                    incno = itemsSplit[0];
                    itemWidth = sampwidth / incno;
                }

                $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
                $(this).find(itemClass).each(function () {
                    $(this).outerWidth(itemWidth);
                });

                $(".leftLst").addClass("over");
                $(".rightLst").removeClass("over");

            });
        }


        //this function used to move the items
        function ResCarousel(e, el, s) {
            var leftBtn = ('.leftLst');
            var rightBtn = ('.rightLst');
            var translateXval = '';
            var divStyle = $(el + ' ' + itemsDiv).css('transform');
            var values = divStyle.match(/-?[\d\.]+/g);
            var xds = Math.abs(values[4]);
            if (e == 0) {
                translateXval = parseInt(xds) - parseInt(itemWidth * s);
                $(el + ' ' + rightBtn).removeClass("over");

                if (translateXval <= itemWidth / 2) {
                    translateXval = 0;
                    $(el + ' ' + leftBtn).addClass("over");
                }
            }
            else if (e == 1) {
                var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
                translateXval = parseInt(xds) + parseInt(itemWidth * s);
                $(el + ' ' + leftBtn).removeClass("over");

                if (translateXval >= itemsCondition - itemWidth / 2) {
                    translateXval = itemsCondition;
                    $(el + ' ' + rightBtn).addClass("over");
                }
            }
            $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
        }

        //It is used to get some elements from btn
        function click(ell, ee) {
            var Parent = "#" + $(ee).parent().attr("id");
            var slide = $(Parent).attr("data-slide");
            ResCarousel(ell, Parent, slide);
        }

    });</script>

    
    <script>
        (function ($) {
            $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
                if (!$(this).next().hasClass('show')) {
                    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
                }
                var $subMenu = $(this).next(".dropdown-menu");
                $subMenu.toggleClass('show');

                $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                    $('.dropdown-submenu .show').removeClass("show");
                });

                return false;
            });
        })(jQuery)
    </script>
    <script>
        $(window).scroll(function () {
            $("#t-menu").stop().animate({ "marginTop": ($(window).scrollTop()) + "px", "marginLeft": ($(window).scrollLeft()) + "px" }, "slow");
        });
        var totaltext = "";
        for (var i = 0; i < 100; i++) {
            totaltext += "scroll!<br />";
        }
        $("#main_container").html();
    </script>
    <script type="text/javascript">
        var screenheight = window.innerHeight;
        // alert(screenheight - 114);
        var heightprop = screenheight - 72;
        document.getElementById('main_container').style.height = heightprop + 'px';
    </script>
    <script type="text/javascript">
        $('#hover')
            .mouseenter(function () {
                $('#divv').show(1000);
            })
            .click(function (e) {
                e.preventDefault();
                $('#divv').hide(1000);
            });
    </script>
    <script type="text/javascript">
        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > 81) {
                $('.fixed-menu').addClass('fixed-m');
                $('.l-m').addClass('l-mn');
                $('.bg-white').addClass('bg-success');

            } else {
                $('.fixed-menu').removeClass('fixed-m');
                $('.l-m').removeClass('l-mn');
                $('.bg-white').removeClass('bg-success');
            }
        });
    </script>
    

    

</body>

</html>