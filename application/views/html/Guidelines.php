
    <style>
        .alert {
position: absolute;
padding: .75rem 1.25rem;
    padding-right: 1.25rem;
margin-bottom: 1rem;
border: 1px solid transparent;
    border-top-color: transparent;
    border-right-color: transparent;
    border-bottom-color: transparent;
    border-left-color: transparent;
border-radius: .25rem;
left: -15px;
width: 72px;
height: auto;
top: 180px;
z-index: 100;
}
.alert-info {
color: #0c5460;
background-color: transparent;
border-color: transparent;
}
.alert-dismissible .close {
padding: 0rem 0rem;
background-color: #ccc;
width: 82%;
}
.card {
border: 0px solid;
}
.btn-outline-info:hover {
    color: #fff;
    background-color: #17a2b8 !important;
    border-color: #17a2b8;
}
.img-set{
    padding-top: 20px;
    width: 60%;
    margin-left: 20%;
}
.img-set2{
    padding-top: 20px;
    width: 20%;
    margin-left: 40%;
}
.btn-outline-success {
    color: #ee82ee;
    border-color: #ee82ee;
}
.btn-outline-success:hover {
    color: #fff;
    background-color: #ee82ee;
    border-color: #ee82ee;
}
.cad{
    color: inherit !important;
}








.MultiCarousel
        {
            float: left;
            overflow: hidden;
            padding: 15px;
            width: 100%;
            position: relative;
        }
        .MultiCarousel .MultiCarousel-inner
        {
            transition: 1s ease all;
            float: left;
        }
        .MultiCarousel .MultiCarousel-inner .item
        {
            float: left;
        }
        .MultiCarousel .MultiCarousel-inner .item > div
        {
            padding: 10px;
            margin: 10px;
            /* background: #f1f1f1; */
            color: #666;
            box-shadow: 0px 0px 10px #ccc;
        }
        .MultiCarousel .leftLst, .MultiCarousel .rightLst
        {
            position: absolute;
            border-radius: 50%;
            top: calc(50% - 20px);
        }
        .MultiCarousel .leftLst
        {
            left: 0;
        }
        .MultiCarousel .rightLst
        {
            right: 0;
        }
        
        .MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over
        {
            pointer-events: none;
            background: #ccc;
        }
        .slid-btn{
            color: #fff;text-decoration: underline !important;font-size: 12px;position: absolute;bottom: 15px;
        }
        .bg-success {
    background-color: #26a591!important;
}
.bg-warning {
    background-color: #ff7918!important;
}
    </style>

        

<div class="jumbotron">
  <h1>Guidelines</h1> 
 <ul>
     <li><a href="<?php echo base_url();  ?>common_libs/diagnosis-management-viral-hepatitis.pdf" target="_blanck">National Guidelines for Diagnosis & Management of Viral Hepatitis</a></li>
     <li><a href="<?php echo base_url();  ?>common_libs/national-laboratory-guidelines.pdf"  target="_blanck">National Laboratory Guidelines for Testing of Viral Hepatitis</a></li>
     <li><a href="<?php echo base_url();  ?>common_libs/Operational_Guidelines.pdf"  target="_blanck">National Viral Hepatitis Control Program Operational Guidelines</a></li>
 <li><a href="<?php echo base_url();  ?>common_libs/TechnicalGuidelines_Low_resolution_27th June_2019.pdf"  target="_blanck">Technical Guidelines for Diagnosis & management of Hepatitis B</a></li>
 <li><a href="<?php echo base_url(); ?>common_libs/National Action Plan_Viral Hepatitis.pdf"  target="_blanck">National Action Plan - Combating Viral Hepatitis in India</a></li>
 
     
 </ul>
</div>
                <div class="row ">

                    </div>

            </div>





        </div>
       
   

  
    <script type="text/javascript">        $(document).ready(function () {
        var itemsMainDiv = ('.MultiCarousel');
        var itemsDiv = ('.MultiCarousel-inner');
        var itemWidth = "";

        $('.leftLst, .rightLst').click(function () {
            var condition = $(this).hasClass("leftLst");
            if (condition)
                click(0, this);
            else
                click(1, this)
        });

        ResCarouselSize();




        $(window).resize(function () {
            ResCarouselSize();
        });

        //this function define the size of the items
        function ResCarouselSize() {
            var incno = 0;
            var dataItems = ("data-items");
            var itemClass = ('.item');
            var id = 0;
            var btnParentSb = '';
            var itemsSplit = '';
            var sampwidth = $(itemsMainDiv).width();
            var bodyWidth = $('body').width();
            $(itemsDiv).each(function () {
                id = id + 1;
                var itemNumbers = $(this).find(itemClass).length;
                btnParentSb = $(this).parent().attr(dataItems);
                itemsSplit = btnParentSb.split(',');
                $(this).parent().attr("id", "MultiCarousel" + id);
               

                if (bodyWidth >= 1200) {
                    incno = itemsSplit[2];
                    itemWidth = sampwidth / incno;
                }
                else if (bodyWidth >= 992) {
                    incno = itemsSplit[2];
                    itemWidth = sampwidth / incno;
                }
                else if (bodyWidth >= 768) {
                    incno = itemsSplit[1];
                    itemWidth = sampwidth / incno;
                }
                else {
                    incno = itemsSplit[0];
                    itemWidth = sampwidth / incno;
                }

                $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
                $(this).find(itemClass).each(function () {
                    $(this).outerWidth(itemWidth);
                });

                $(".leftLst").addClass("over");
                $(".rightLst").removeClass("over");

            });
        }


        //this function used to move the items
        function ResCarousel(e, el, s) {
            var leftBtn = ('.leftLst');
            var rightBtn = ('.rightLst');
            var translateXval = '';
            var divStyle = $(el + ' ' + itemsDiv).css('transform');
            var values = divStyle.match(/-?[\d\.]+/g);
            var xds = Math.abs(values[4]);
            if (e == 0) {
                translateXval = parseInt(xds) - parseInt(itemWidth * s);
                $(el + ' ' + rightBtn).removeClass("over");

                if (translateXval <= itemWidth / 2) {
                    translateXval = 0;
                    $(el + ' ' + leftBtn).addClass("over");
                }
            }
            else if (e == 1) {
                var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
                translateXval = parseInt(xds) + parseInt(itemWidth * s);
                $(el + ' ' + leftBtn).removeClass("over");

                if (translateXval >= itemsCondition - itemWidth / 2) {
                    translateXval = itemsCondition;
                    $(el + ' ' + rightBtn).addClass("over");
                }
            }
            $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
        }

        //It is used to get some elements from btn
        function click(ell, ee) {
            var Parent = "#" + $(ee).parent().attr("id");
            var slide = $(Parent).attr("data-slide");
            ResCarousel(ell, Parent, slide);
        }

    });</script>

    
    <script>
        (function ($) {
            $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
                if (!$(this).next().hasClass('show')) {
                    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
                }
                var $subMenu = $(this).next(".dropdown-menu");
                $subMenu.toggleClass('show');

                $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                    $('.dropdown-submenu .show').removeClass("show");
                });

                return false;
            });
        })(jQuery)
    </script>
    <script>
        $(window).scroll(function () {
            $("#t-menu").stop().animate({ "marginTop": ($(window).scrollTop()) + "px", "marginLeft": ($(window).scrollLeft()) + "px" }, "slow");
        });
        var totaltext = "";
        for (var i = 0; i < 100; i++) {
            totaltext += "scroll!<br />";
        }
        $("#main_container").html();
    </script>
    <script type="text/javascript">
        var screenheight = window.innerHeight;
        // alert(screenheight - 114);
        var heightprop = screenheight - 72;
        document.getElementById('main_container').style.height = heightprop + 'px';
    </script>
    <script type="text/javascript">
        $('#hover')
            .mouseenter(function () {
                $('#divv').show(1000);
            })
            .click(function (e) {
                e.preventDefault();
                $('#divv').hide(1000);
            });
    </script>
    <script type="text/javascript">
        $(window).bind('scroll', function () {
            if ($(window).scrollTop() > 81) {
                $('.fixed-menu').addClass('fixed-m');
                $('.l-m').addClass('l-mn');
                $('.bg-white').addClass('bg-success');

            } else {
                $('.fixed-menu').removeClass('fixed-m');
                $('.l-m').removeClass('l-mn');
                $('.bg-white').removeClass('bg-success');
            }
        });
    </script>
    

    

</body>

</html>