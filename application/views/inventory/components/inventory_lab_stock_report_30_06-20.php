<style>
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: top;
		background-color: #085786;
		color: white;
	}
	td{
		text-align: center; 
		vertical-align: middle;
	}
	.item{
		width: 140px !important;
	}
	.batch_num{
		width: 100px !important;
	}
	a{
		cursor: pointer;
	}

	a:hover,a:focus {
		cursor: pointer;
		text-decoration: none;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	select[readonly] {
		background: #eee;
		pointer-events: none;
		touch-action: none;
	}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData');
$filters = $this->session->userdata('filters');
//echo "<pre>"; print_r($filters);

if($loginData->user_type==1) { 
	$select = '';
}else{
	$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
	$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
	$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
	$select4 = '';	
}
?>
<?php $loginData = $this->session->userdata('loginData');
$filters1=$this->session->userdata('filters1');
if ($loginData->user_type==2 || $filters1['id_mstfacility'] !=''){
	$colspan="17";
	$th="<th>Batch Number</th>";
	$item_text="Item";
}
else if (($loginData->user_type==1 || $loginData->user_type==3)&&$filters1['id_mstfacility']==''){
	$colspan="16";
	$th=" ";
	if (($loginData->user_type==1 || $loginData->user_type==3)|| $filters1['id_search_state'] !=''){
	$item_text="Facility";
}
}
if ($Repo_flg==1) {
	$show_text="(no. of bottles)";
}
elseif ($Repo_flg==2) {
	$show_text='(no. of screening tests)';
}
?>
<div class="row main-div" style="min-height:400px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xl-12">
		<div class="panel panel-default" id="Record_receipt_panel">
			<div class="panel-heading" style=";background: #333;">
				<h4 class="text-center" style="color: white;background: #333" id="Record_form_head"><?php if($Repo_flg==1){echo "Drug Stock Report";$option='<option value="drug">All Drugs</option>';}elseif($Repo_flg==2){ echo "Lab Stock Report";$option='<option value="Kit">All Screening Test Kits</option>';} ?></h4>
			</div>
			<div class="row">
				<?php 
				$tr_msg= $this->session->flashdata('tr_msg');
				$er_msg= $this->session->flashdata('er_msg');
				if(!empty($tr_msg)){  ?>


					<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
						<div class="hpanel">
							<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<?php echo $this->session->flashdata('tr_msg');?>. </div>
							</div>
						</div>
					<?php } else if(!empty($er_msg)){ ?>
						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('er_msg');?>. </div>
								</div>
							</div>

						<?php } ?>
					</div>
					<?php
					$attributes = array(
						'id' => 'Receipt_filter_form',
						'name' => 'Receipt_filter_form',
						'autocomplete' => 'false',
					);
//pr($items);
//print_r($receipt_details);
//pr($Closing_stock_Repo);
					echo form_open('', $attributes); ?>

					<div class="row" style="margin-left: 10px;">
						<!-- <div class="col-md-2">
							<label for="">State</label>
							<select type="text" name="search_state" id="search_state" class="form-control input_fields" <?php  ?>  <?php echo $select2.$select3.$select4; ?>>
								<option value="">All States</option>
								<?php 
								foreach ($states as $state) {
									?>
									<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo ucwords(strtolower($state->StateName)); ?></option>
									<?php 
								}
								?>
							</select>
						</div>
						<div class="col-md-2">
							<label for="">District</label>
							<select type="text" name="input_district" id="input_district" class="form-control input_fields"  <?php echo $select.$select2.$select4; ?>>
								<option value="">All District</option>
								<?php 
						/*foreach ($districts as $district) {
						?>
						<option value="<?php echo $district->id_mstdistrict; ?>" <?php echo (count($default_districts)==1 && $default_districts[0]->id_mstdistrict == $district->id_mstdistrict)?'selected':''; ?>><?php echo ucwords(strtolower($district->DistrictName)); ?></option>
						<?php 
						}*/
						?>
						</select>
						</div>
						
						<div class="col-md-2">
							<label for="">Facility</label>
							<select type="text" name="mstfacilitylogin" id="mstfacilitylogin" class="form-control input_fields"  <?php echo $select.$select2; ?>>
								<option value="">All Facility</option>
								<?php 
						/*foreach ($facilities as $facility) {
						?>
						<option value="<?php echo $facility->id_mstfacility; ?>" <?php echo (count($default_facilities)==1 && $default_facilities[0]->id_mstfacility == $facility->id_mstfacility)?'selected':''; ?>><?php echo ucwords(strtolower($facility->facility_short_name)); ?></option>
						<?php 
						}*/
						?>
						</select>
						</div> -->
<div class="col-xs-3 col-md-2">
	<label for="item_type">Item Name <span class="text-danger">*</span></label>
	<div class="form-group">
		<select name="item_type" id="item_type" required class="form-control">
			<?php echo $option; ?>
			<?php foreach ($items as $value) { ?>

				<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
					<?php if($value->type==2){echo $value->drug_name; } else{
						echo $value->drug_name." (".$value->strength.")";
					} ?></option>
				<?php } ?>
				<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
			</select>
		</div>
	</div>
	<div class="col-md-2">
		<label for="">Month</label>
		<select name="month" required class="form-control">
			<?php
			echo $curmonth = date("F");
			$flag=0;
			for($i = 1 ; $i <= 12; $i++)
			{
				$allmonth = date("F",mktime(0,0,0,$i,1,date("Y")))
				?>
				<option value="<?php
				echo $i;	
				?>"
				<?php 
				if($this->input->post('month')==$i){ echo 'selected';$flag=1;}
				if($curmonth==$allmonth && $flag==0)
				{
					echo 'selected';
				}
				?>
				>
				<?php
				echo date("F",mktime(0,0,0,$i,1,date("Y")));
//Close tag inside loop
				?>
			</option>
			<?php
		} ?>
	</select>
</div>
<div class="col-md-1" style="padding-left: 0px;">
	<label for="">Year</label>
	<select name="year" required class="form-control">

		<?php 
		$flag=0;
		for($i =2000; $i <= date('Y'); $i++){
			if($this->input->post('year')==$i){
				echo "<option value='$i' selected>$i</option>";
				$flag=1;
			}
			if($i==date('Y') && $flag==0){
				echo "<option value='$i' selected>$i</option>";
			}
			else{
				echo "<option value='$i'>$i</option>";
			}
		}
		?>
	</select>
</div>
<div class="col-md-1" style="padding-left: 0; margin-left: : -5px;width: 7.333333%">
	<label for="">&nbsp;</label>
	<button class="btn btn-block btn-warning" style="line-height: 1.2; font-weight: 600;">SEARCH</button>
</div>

<?php echo form_close(); ?>
</div>

</div>
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover" id="table_inventory_list">


	<thead>
		<tr>
			<th>Item</th>
			<th>Batch Num</th>
			<th>Opening Stock  <?php echo $show_text; ?></th>
			<th>Stock received from <br>warehouse <?php echo $show_text; ?></th>
			<th>Stock received  through a transfer-in from a facility <?php echo $show_text; ?></th>
			<th>Stock received from<br> unregistered sources <?php echo $show_text; ?></th>
			<th>Stock received from<br>  third party sources <?php echo $show_text; ?></th>
			<th>Total Receipts <br> <?php echo $show_text; ?></th>

			<th>Stock Expired(no. of screening tests)</th>
			<th>Stock rejected on receipt</th>
			<th>Stock returned due to stock failure	</th>
			<th>Stock utilized in own facility  (no. of screening tests performed and controls used)</th>
			<th>Stock transferred out <?php echo $show_text; ?></th>
			<th>Closing Stock <?php echo $show_text; ?>	</th>
			<th>Reorder point/Minimum Stock level that should be maintained (bottles)</th>
			<th>Maximum Stock that should be maintained (bottles)</th>
			<th>Reccomendation</th>
			<th>Action</th>


  

		</tr>
	</thead>
	<tbody>
<!-- </tbody>
</table> -->
<?php // echo "<pre>";/*print_r($inventory_details);*/
//$result=array_unique($inventory_details->);
if ($loginData->user_type==2 || $filters1['id_mstfacility'] !=''){
	$item_id=array();
	$data=array();
	$filtered_items=array();
	foreach ($Inventory_Repo as $key => $value) {
    $filtered_items[$key]['id_mst_drugs'] =$value->id_mst_drugs;
    $filtered_items[$key]['strength'] =$value->strength;
    /*$filtered_items['id_mst_drugs'] = $value->id_mst_drugs;
    $filtered_items['id_mst_drugs'] = $value->id_mst_drugs;*/
}
	foreach ($items as $key => $value) {
		foreach ($filtered_items as $key1 => $value1) {
			if($value->id_mst_drug_strength==$filtered_items[$key1]['id_mst_drugs']){
				$item_id[$key]['id_mst_drugs']= $value->id_mst_drug_strength;
				$item_id[$key]['drug_name']= $value->drug_name;
				$item_id[$key]['strength']= $filtered_items[$key1]['strength'];
			}
		}

	}
	$filtered_arr=array_unique($item_id,SORT_REGULAR);
//pr($filtered_arr);exit();
//pr($Inventory_Repo);
	?>
<!-- <table class="table table-striped table-bordered table-hover">
	<tbody> -->
		<?php if(!empty($Inventory_Repo)){
			foreach ($filtered_arr as $key=>$value_temp) { ?>
				<tr>
					<th colspan="<?php echo $colspan; ?>" class="info"style="border: none;background-color: #333;font-size: 16px;color:#fff;text-align: left;"><?php echo $value_temp['drug_name']." ".$value_temp['strength']." mg"; ?></th>
					<th class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus item_toggle" aria-hidden="true" id="<?php echo 'item'.$value_temp['id_mst_drugs']; ?>"></i></th></tr>
					<?php foreach ($Inventory_Repo as $key => $value) { 
						if($value_temp['id_mst_drugs']==$value->id_mst_drugs){
							?>
							<tr class="<?php echo 'item'.$value_temp['id_mst_drugs']; ?> item_row">
								<td class="item">
									<?php if($value->type==1){echo $value_temp['drug_name']."(".$value->strength.")";} elseif($value->type==2){echo $value_temp['drug_name'];} ?>
								</td>
							 <td>
									<?php echo $value->batch_num; ?>
								</td>
								<td>
									</td>
								<td >
									<?php if($value->warehouse_quantity_received==NULL){
										echo '-';
									}else{
										echo $value->warehouse_quantity_received;
									} ?>
								</td>
								<td>
									<?php if($value->quantity_received==NULL){
										echo '-';
									}else{
										echo $value->quantity_received; }?>
									</td>
									<td>
										<?php if($value->uslp_quantity_received==NULL){
											echo '-';
										}else{ echo $value->uslp_quantity_received;} ?>
									</td>
										<td>
										<?php if($value->tp_quantity_received==NULL){
											echo '-';
										}else{ echo $value->tp_quantity_received;} ?>
									</td>
									<td>
										<?php if($value->total_receipt==NULL){
											echo '-';
										}else{ echo $value->total_receipt; }?>
									</td>
									<td>
										<?php 
										if ($value->Expire_stock>0) {
											
										$total_expired=($value->Expire_stock-($value->returned_quantity+$value->total_rejected+$value->total_utilize+$value->total_transfer_out));

											echo $total_expired;

										}
										else{
											echo '-';
										}
										?>
									</td>
									<td>
										<?php 
											if($value->total_rejected==NULL){
											echo '-';
										}else{ echo $value->total_rejected; }

										?>
									</td>
									<td>
										<?php 
											if($value->returned_quantity==NULL){
											echo '-';
										}else{ echo $value->returned_quantity; }
											
										?>
									</td>
									<td>
										<?php if($value->total_utilize==NULL){
											echo '-';
										}else{echo $value->total_utilize; }?>
									</td>
									<td>
										<?php if($value->total_transfer_out==NULL){
											echo '-';
										}else{ echo $value->total_transfer_out; }?>
									</td>
									<td>
										<?php 
										$closing_stock=($value->total_receipt-($value->returned_quantity+$value->total_rejected+$value->total_utilize+$value->total_transfer_out+$total_expired));

										if($closing_stock==NULL){
											echo $closing_stock;
										}else{echo $closing_stock;} ?>
									</td>
									<td>
										
									</td>
										<td>
										
									</td>
										<td>
										
									</td>
								</tr>
<!-- <td class="text-center">
<a href="<?php echo site_url()."/inventory/AddReceipt/".$value->receipt_id;?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
<a href="<?php echo site_url()."/inventory/delete_receipt/".$value->receipt_id."/R";?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
</td>

</tr> -->
<?php } }}}else{ ?>
	<tr >
		<td colspan="<?php echo $colspan; ?>" class="text-center">
			NO RECORDS FOUND  BETWEEN SELECTED DATES.
		</td>

	</tr>
<?php } }

else if (($loginData->user_type==1 || $loginData->user_type==3)&&$filters1['id_mstfacility']==''){
	$item_id=array();
	$data=array();
	$filtered_items=array();
	foreach ($Inventory_Repo as $key => $value) {
    $filtered_items[$key]['id_mst_drugs'] =$value->id_mst_drugs;
    $filtered_items[$key]['strength'] =$value->strength;
    /*$filtered_items['id_mst_drugs'] = $value->id_mst_drugs;
    $filtered_items['id_mst_drugs'] = $value->id_mst_drugs;*/
}
	foreach ($items as $key => $value) {
		foreach ($filtered_items as $key1 => $value1) {
			if($value->id_mst_drug_strength==$filtered_items[$key1]['id_mst_drugs']){
				$item_id[$key]['id_mst_drugs']= $value->id_mst_drug_strength;
				$item_id[$key]['drug_name']= $value->drug_name;
				$item_id[$key]['strength']= $filtered_items[$key1]['strength'];
			}
		}

	}
	$filtered_arr=array_unique($item_id,SORT_REGULAR);
//pr(($filtered_items));
//pr($Inventory_Repo);exit();
	?>
<!-- <table class="table table-striped table-bordered table-hover">
	<tbody> -->
		<?php if(!empty($Inventory_Repo)){
			
			foreach ($filtered_arr as $key=>$value_temp) { 
				?>
				<tr>
					<td class="info"style="font-size: 12px;font: bold; text-align: left;"><?php echo $value_temp['drug_name']." ".$value_temp['strength']." mg"; ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					
					<td class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus item_toggle" aria-hidden="true" id="<?php echo 'item'.$value_temp['id_mst_drugs']; ?>"></i></td></tr>
					<?php foreach ($Inventory_Repo as $key => $value) { 
						if($value_temp['id_mst_drugs']==$value->id_mst_drugs){?>
							<tr class="<?php echo 'item'.$value_temp['id_mst_drugs']; ?>">
								<td class="item">

									<?php if(($loginData->user_type==1) &&($filters1['id_search_state']=='')){
										echo $value->StateName;
									}
									else if($loginData->user_type==3 ||($filters1['id_search_state']!='')){
										echo $value->hospital;
									}

									?>
								</td>
								<td>
									&nbsp;
								</td>
								<td>
									<?php echo '-' ?>
								</td>
								<td>
								<?php if($value->warehouse_quantity_received==NULL){
										echo '-';
									}else{
										echo $value->warehouse_quantity_received;
									} ?>
								</td>
								<td>
									<?php if($value->quantity_received==NULL){
										echo '-';
									}else{
										echo $value->quantity_received; }?>
									</td>
									<td>
										<?php if($value->uslp_quantity_received==NULL){
											echo '-';
										}else{ echo $value->uslp_quantity_received;} ?>
									</td>
										<td>
										<?php if($value->tp_quantity_received==NULL){
											echo '-';
										}else{ echo $value->tp_quantity_received;} ?>
									</td>
									<td>
										<?php if($value->total_receipt==NULL){
											echo '-';
										}else{ echo $value->total_receipt; }?>
									</td>
									<td>
										<?php 
											echo $value->Expired_stock;
										?>
									</td>
									<td>
										<?php 
											if($value->total_rejected==NULL){
											echo '-';
										}else{ echo $value->total_rejected; }

										?>
									</td>
									<td>
										<?php 
											if($value->returned_quantity==NULL){
											echo '-';
										}else{ echo $value->returned_quantity; }
											
										?>
									</td>
									<td>
										<?php if($value->total_utilize==NULL){
											echo '-';
										}else{echo $value->total_utilize; }?>
									</td>
									<td>
										<?php if($value->total_transfer_out==NULL){
											echo '-';
										}else{ echo $value->total_transfer_out; }?>
									</td>
									<td>
										<?php 
										$closing_stock=($value->total_receipt-($value->returned_quantity+$value->total_rejected+$value->total_utilize+$value->total_transfer_out+$value->Expired_stock));

										if($closing_stock==NULL){
											echo '-';
										}else{echo $closing_stock;} ?>
									</td>
									<td>
										
									</td>
										<td>
										
									</td>
										<td>
										</td>
										<?php if(($loginData->user_type==1) &&($filters1['id_search_state']=='')){?>
										<td class="info" style="border: none;text-align: right;"><a href="javascript:void(0);" onclick="get_facility_data($(this).parent().closest('tr').index(),'<?php echo $value->type;?>','<?php echo $value->id_mststate;?>','<?php echo $value->id_mst_drugs;?>');"class="fa fa-plus facility_td" aria-hidden="true" id="<?php echo $value->id_mst_drugs.'-'.$value->id_mststate; ?>"></a></td>
									<?php } 
									else if($loginData->user_type==3 ||($filters1['id_search_state']!='')){ ?>
										<td class="info" style="border: none;text-align: right;"><a href="javascript:void(0);" onclick="get_item_data($(this).parent().closest('tr').index(),'<?php echo $value->type;?>','<?php echo $value->id_mstfacility;?>','<?php echo $value->id_mst_drugs;?>');"class="fa fa-plus item_td" aria-hidden="true" id="<?php echo $value->id_mst_drugs.'-'.$value->id_mstfacility; ?>"></a></td>
									<?php } ?>
</tr>
									
									
<!-- <td class="text-center">
<a href="<?php //echo site_url()."/inventory/AddReceipt/".$value->receipt_id;?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
<a href="<?php //echo site_url()."/inventory/delete_receipt/".$value->receipt_id."/R";?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
</td>

</tr> -->
<?php } }}}else{ ?>
	<tr >
		<td colspan="<?php echo $colspan; ?>" class="text-center">
			NO RECORDS FOUND  BETWEEN SELECTED DATES.
		</td>

	</tr>
<?php } }?>

</tbody>
</table>
</div>
</div>
<br>
</div>
<div class="overlay">
	<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
</div>

<!-- Data Table -->
<script type="text/javascript">

	
	$(document).ready(function(){
		$(".glyphicon-trash").click(function(e){
			var ans = confirm("Are you sure you want to delete?");
			if(!ans)
			{
				e.preventDefault();
			}
		});
		$(".facility_td").click(function(e){
			$(this).toggleClass('fa-plus fa-minus');
			var abc=$(this).attr("id");
			$('.'+abc).toggle();
			if ($('.'+abc).is(':visible')==false) {
				
  				$('.item_row').hide();
			}
		});
		$(".item_toggle").click(function(e){
			$(this).toggleClass('fa-plus fa-minus');
			
			var abc=$(this).attr("id");
			<?php if($loginData->user_type!=2){?>
				
			$('.'+abc).toggle();

			if ($('.'+abc).is(':visible')==false && $('.item_row').is(':visible')==true) {
  				$('.item_row').hide();
			}
			
<?php }else{?>
		$('.'+abc).toggle();
	<?php }?>
		});
		


		
	});


		$(document).ready(function(){

		$("#search_state").trigger('change');
//alert('rff');

});
	$('#search_state').change(function(){

		$.ajax({
			url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
			data : {
				<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
			},
			method : 'GET',
			success : function(data)
			{
//alert(data);
$('#input_district').html(data);
$("#input_district").trigger('change');
//$("#mstfacilitylogin").trigger('change');

},
error : function(error)
{
//alert('Error Fetching Districts');
}
})
	});

	$('#input_district').change(function(){

		$.ajax({
			url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),

			method : 'GET',
			success : function(data)
			{
//alert(data);
if(data!="<option value=''>Select Facilities</option>"){
	$('#mstfacilitylogin').html(data);

}
},
error : function(error)
{
//alert('Error Fetching Blocks');
}
})
	});

function  get_facility_data(row_index,type,id_mststate,id_mst_drugs){

//alert(row_index);
var dataString = {
id_mst_drugs: id_mst_drugs,
id_mststate: id_mststate,
type: type,

};
$.ajax({    
url: '<?php echo base_url();?>Inventory_Reports/get_facility_data',
type: 'GET',
data: dataString,
dataType: 'json',
async: false,
cache: false,
success: function (data) {


var parsedData = JSON.parse(data.fields);
//console.log(parsedData);
var output = [];

for(var i=0; i<parsedData.length; i++){
var closing_stock=(parseInt((parsedData[i]['total_receipt'] || 0))-(parseInt(parsedData[i]['returned_quantity']|| 0)+parseInt(parsedData[i]['total_rejected']|| 0)+parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_transfer_out']|| 0)+parseInt(parsedData[i]['Expired_stock'] || 0)));
var facility=parsedData[i]['id_mstfacility'];

	str='<tr class="'+id_mst_drugs+'-'+id_mststate+' item'+id_mst_drugs+'">';
str+='<td>'+parsedData[i]['hospital'] +'</td> <td></td>'
	+' <td></td>'
	+' <td>'+parsedData[i]['warehouse_quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['uslp_quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['tp_quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['total_receipt'] +'</td>'
	+' <td>'+parsedData[i]['Expired_stock']+'</td>'
	+' <td>'+(parsedData[i]['total_rejected'] || "-") +'</td>'
	+' <td>'+(parsedData[i]['returned_quantity'] || "-") +'</td>'
	+' <td>'+parsedData[i]['total_utilize'] +'</td>'
	+' <td>'+parsedData[i]['total_transfer_out'] +'</td>'
	+' <td>'+closing_stock +'</td>'
	+' <td></td>'
	+' <td></td>'
	+' <td></td>'
	+' <td class="info" style="border: none;text-align: right;"><a class="fa fa-plus item_td"  href="javascript:void(0);" onclick="get_item_data($(this).parent().closest('+"'tr'"+').index(),'+"'"+type+"'"+','+"'"+facility+"'"+','+"'"+id_mst_drugs+"'"+');"  aria-hidden="true" id="'+id_mst_drugs+'-'+facility+'"></a></td>';
str+='</tr>';
output.push(str);
closing_stock=0;
}
 $('table > tbody > tr').eq(row_index).after(output);
 $('#'+id_mst_drugs+'-'+id_mststate).removeAttr('onclick');
 $('.'+id_mst_drugs+'-'+id_mststate).hide();
   }
});
}

function  get_item_data(row_index,type,id_mstfacility,id_mst_drugs){


var dataString = {
id_mst_drugs: id_mst_drugs,
id_mstfacility: id_mstfacility,
type: type,

};
$.ajax({    
url: '<?php echo base_url();?>Inventory_Reports/get_item_data',
type: 'GET',
data: dataString,
dataType: 'json',
async: false,
cache: false,
success: function (data) {


var parsedData = JSON.parse(data.fields);
//console.log(parsedData);
var output = [];

for(var i=0; i<parsedData.length; i++){
var closing_stock=(parseInt((parsedData[i]['total_receipt'] || 0))-(parseInt(parsedData[i]['returned_quantity']|| 0)+parseInt(parsedData[i]['total_rejected']|| 0)+parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_transfer_out']|| 0)));
var Expire_stock=(parseInt((parsedData[i]['Expire_stock'] || 0))-(parseInt(parsedData[i]['returned_quantity']|| 0)+parseInt(parsedData[i]['total_rejected']|| 0)+parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_transfer_out']|| 0)));
if (Expire_stock<0) {
	Expire_stock=0;
}
else{
	closing_stock=closing_stock-Expire_stock;
}
var facility=parsedData[i]['id_mstfacility'];

	str='<tr class="'+id_mst_drugs+'-'+id_mstfacility+' item_row">';
str+='<td></td> <td>'+parsedData[i]['batch_num'] +'</td>'
	+' <td></td>'
	+' <td>'+parsedData[i]['warehouse_quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['uslp_quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['tp_quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['total_receipt'] +'</td>'
	+' <td>'+Expire_stock+'</td>'
	+' <td>'+(parsedData[i]['total_rejected'] || "-") +'</td>'
	+' <td>'+(parsedData[i]['returned_quantity'] || "-") +'</td>'
	+' <td>'+parsedData[i]['total_utilize'] +'</td>'
	+' <td>'+parsedData[i]['total_transfer_out'] +'</td>'
	+' <td>'+closing_stock +'</td>'
	+' <td></td>'
	+' <td></td>'
	+' <td></td>'
	+' <td></td>'
str+='</tr>';
output.push(str);
closing_stock=0;
}
 $('table > tbody > tr').eq(row_index).after(output);
 $('#'+id_mst_drugs+'-'+id_mstfacility).removeAttr('onclick');
 $('.'+id_mst_drugs+'-'+id_mstfacility).hide();
//alert(output);
   }
});
}
$(document).on('click', ".item_td", function() {
	$(this).toggleClass('fa-plus fa-minus');
			var abc=$(this).attr("id");
			$('.'+abc).toggle();
	});
</script>


