<style>
thead
{
	background-color: #085786;
	color: white;
}

.table-bordered>tbody>tr>td.data_td
{
	font-weight: 600;
	font-size: 15px;
	text-align: center;
	vertical-align: middle;
}
.side_btn {
            float: right;
            width: 100%;
            height: auto;
        }

.chk {
            display: block;
            float: right;
            width: 350px;
            height: auto;
            position: absolute;
            right: -10px;
            background-color: #fbfbfb;
            border: solid 1px LightGray;
            z-index: 1000;
        }

        .side_ul {
            float: left;
            height: auto;
            width: 100%;
        }
</style>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 3 !important;
	width: 100% !important;
	font-size: 14px !important;
	font-family: 'Source Sans Pro';
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
	.col-md-2{
		width: 16.33%;
		padding-left: 8px;
		padding-right: 8px;
	}
	.btn-width{
	width: 12%;
	margin-top: 20px;
}
.pd-15{
	padding-left: 15px;
}
.pb-20{
	padding-bottom: 20px;
}
.container{
	width: 76%;
}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 4 !important;
		width: 100% !important;
		font-size: 14px !important;
		font-family: 'Source Sans Pro';
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn,.btn1 {
	display: inline-block;
	margin:10px 0px 10px 0px;
	font-size: 13px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #085786;
}
a.btn1
{
	text-decoration: none;
	color : #000;
	background-color: #006064;
	padding:6px 12px;
}
.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

/* .btn
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
} */

/* .btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
} */
.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
}

.btn:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
a.btn1:hover
{
	text-decoration: none;
	color : #000;
	background-color: #e47c09;
	padding:6px 12px;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
 .table-bordered>thead>tr>th{
	vertical-align: middle;
	font-size: 13px;
}
.table-bordered>tbody>tr>td{
	vertical-align: middle;
	font-size: 13px;
	color: #000000;
}
</style>
</style>
<br>

<?php $loginData = $this->session->userdata('loginData');
//echo "<pre>"; print_r($loginData);

if($loginData->user_type==1) { 
$select = '';
}else{
$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
$select4 = '';	
}

$filters1 = $this->session->userdata('filters1'); 
$userdata = $this->session->userdata('loginData');
//pr($userdata);
 ?>
<div class="container">
		
<div class="row">
	 <div class="col-md-4 col-sm-12 col-xs-12 col-lg-4 <?php if($userdata->user_type == 1) { ?> col-md-offset-2 <?php } ?>">
		<div class="panel panel-default" id="Monthly_Report_Panel">
				<div class="panel-heading" style="background-color: #333333;padding: 3px 0px;">
					<h4 class="text-center" style="background-color: #333333;color: white; font-family: 'Source Sans Pro';letter-spacing: .75px;">Stock Status Reports
		
		</h4>
		</div>
	<div class="panel-body">
		 <a class="btn btn-success btn-block" href="<?php echo site_url('Inventory_Reports/index/2')?>"> Laboratory Stock Status</a>
		 <a class="btn btn-success btn-block" href="<?php echo site_url('Inventory_Reports/index/1')?>">Drug Stock Status</a>
                 
        <div class="side_btn">
                            <div class="side_ul">
                                <ul class="list-unstyled">
                                    <li>
                                        <a class="btn btn-success btn-block" href="#">HBIG Stock Report</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                <div id="divFilter" class="chk" style="width: 300px;margin-top: 0px;">
                    <div class="row" style="margin-top: 15px; margin-left: 5px; margin-right: 5px">
                        <div class="col-sm-12">
                            <div class="form-group">
							 <a class="btn btn-success btn-block" href="<?php echo site_url('Inventory_Reports/index/4')?>">HBIG Stock Report (Self)</a>

                            </div>
                        </div>
                        <?php if($userdata->user_type == 2) { ?>
                        <div class="col-sm-12">
                            <div class="form-group">
                              <a class="btn btn-success btn-block" href="<?php echo site_url('Inventory_Reports/index/4/1')?>">HBIG Stock Report (Child Facilities)</a>
                            </div>
                        </div>
                    <?php } ?>
                    <br />
                </div>
            </div>
        <a class="btn btn-success btn-block" href="<?php echo site_url('Inventory_Reports/index/3')?>"> HBV Stock Status</a>
       
	               
	</div>
	</div>
	</div> 
	 <?php if($userdata->user_type != 1) {?>
	<div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
		<div class="panel panel-default" id="Monthly_Report_Panel">
				<div class="panel-heading" style="background-color: #333333;padding: 3px 0px;">
					<h4 class="text-center" style="background-color: #333333;color: white; font-family: 'Source Sans Pro';letter-spacing: .75px;">Indent Reports
		
		</h4>
		
</div>
<div class="panel-body">
		
                <?php if($userdata->user_type != 5) {?>
                	 <?php if($userdata->user_type == 2) {?>
                    	 <a class="btn1 btn-success btn-block" href="<?php echo site_url('Inventory_Reports/stock_indent_status')?>">Stock Indent Tracker</a>
                   <?php }?>
                    <?php if($userdata->user_type != 1) {?>
              <a class="btn1 btn-success btn-block" href="<?php echo site_url('Inventory_Reports/lab_drug_forecast/1')?>">Drug Indent Suggestion</a>
                    <a class="btn1 btn-success btn-block" href="<?php echo site_url('Inventory_Reports/lab_drug_forecast/2')?>"> Lab Indent Suggestion</a> 
                    <a class="btn1 btn-success btn-block" href="<?php echo site_url('Inventory_Reports/lab_drug_forecast/4')?>"> HBIG Indent Suggestion</a> 
                   
			<?php }}?>
            
</div>
	</div>
</div>
<?php } ?>
	<div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
	<div class="panel panel-default" id="Monthly_Report_Panel">
			<div class="panel-heading" style="background-color: #333333;padding: 3px 0px;">
				<h4 class="text-center" style="background-color: #333333;color: white; font-family: 'Source Sans Pro';letter-spacing: .75px;">Other Reports
	
	</h4></div>
	
	<div class="panel-body">
		<a class="btn1 btn-success btn-block" href="<?php echo site_url('Inventory_Reports/stock_expiry_report')?>">Stock Expiry Status </a>
                    <a class="btn1 btn-success btn-block" href="<?php echo site_url('Inventory_Reports/stock_rejection')?>">Stock Rejection </a> 
                    <a class="btn1 btn-success btn-block" href="<?php echo site_url('Inventory_Reports/stock_receipt_mismatch')?>">Stock Dispatch – Stock Receipt Mismatch</a>
                     <?php if($userdata->user_type == 2) {?>
                     	 <a class="btn1 btn-success btn-block" href="<?php echo site_url('Inventory_Reports/stock_reporting_mismatch')?>">IMS – MIS Stock Reporting Mismatch</a> 
                     	 <a class="btn1 btn-success btn-block" href="<?php echo site_url('Inventory_Reports/stock_out_instance_facility')?>">Stock Out Instances</a>
                      <?php }?>
                      	 <?php if($userdata->user_type == 3) {?>
                      	 	 <a class="btn1 btn-success btn-block" href="<?php echo site_url('Inventory_Reports/stock_reporting_mismatch_state_wise')?>">IMS – MIS Stock Reporting Mismatch</a> 
                      	 	 <a class="btn1 btn-success btn-block" href="<?php echo site_url('Inventory_Reports/stock_out_instance_state_wise')?>">Stock Out Instances </a>
                      <?php }?>
	</div>
	</div>
	</div> 
</div>

<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name,filename) {
    if (!table.nodeType) table = document.getElementById(table)
            var newtable=document.createElement("table");
            var tbody =document.createElement("tbody");
             var row = document.createElement("tr");
             row.setAttribute("style", "background-color: black;color: white; font-family:'Source Sans Pro';");
  				var cell1 = document.createElement("td");
  				cell1.colSpan=7;
  				cell1.innerHTML ="Date as on : - <?php echo date('jS \ F Y'); ?>";
  				row.appendChild(cell1);
 			 	tbody.appendChild(row);
 			 	newtable.appendChild(tbody);
             var clonedTable=newtable.cloneNode(true);
             var clonedTable1=table.cloneNode(true);
            clonedTable.appendChild(clonedTable1);
    var ctx = {worksheet: name || 'Worksheet', table: clonedTable.innerHTML}
    //window.location.href = uri + base64(format(template, ctx))
     var link = document.createElement('a');
    link.download =filename;
    link.href = uri + base64(format(template, ctx));
    link.click();
     clonedTable.remove();
  }
})()
</script>
<script>

	$(document).ready(function(){
		 $('.chk').hide();
                    $(".side_ul").mouseover(function () {
                      
                           //$(this).('.chk').show(1000);
                        //$(this).('.chk').slideDown(1000);
                        $(".chk").slideDown(1000);                      
                    });
                      $(".chk").mouseleave(function () {
                       
                            //
                             var index = $(".chk").index(this);
                         $(".chk").slideUp(1000);
                         //$(".chk").eq(index).hide(1000);
                    });
                   $(document).scroll(function() {
 					 $(".chk").slideUp(1000);
});

	});

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

</script>