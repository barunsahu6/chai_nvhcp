<style>
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
    	padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData'); ?>
<!-- add/edit facility contact modal starts-->
<!-- <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Facility Contact</h4>
      </div>
      <div class="modal-body">
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Contact Name</label>
        		</div>
        		<div class="col-xs-12 col-lg-6">
					<input type="text" class="form-control" placeholder="Contact Name" name="contact_name" id="contact_name" required>
        		</div>
        	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="facility_contact_save">Save changes</button>
      </div>
    </div>/.modal-content
  </div>/.modal-dialog
</div><! --><!--  /.modal -->

<!-- add/edit facility contact modal ends-->

	<div class="row main-div" style="min-height:400px;">
		<div class="row">
			 <div class="col-md-2 col-sm-10 pull-right" style="padding: 0px;">
  				<div class="col-md-10 col-sm-6 form-group" style="padding: 0px;">
				<a href="<?php echo base_url(); ?>inventory/index/T" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #A30A0C;" id="open_receipt">View Transfer Out Record</a>
			</div>
		</div>
	</div>
		<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
			<?php //include("left_nav_bar.php");
			$this->load->view("inventory/components/left_nav_bar"); ?>
		</div>

		<div class="col-lg-9 col-md-7 col-sm-12 col-xs-12">
		<div class="row">
			
			<div class="col-lg-12">

		
				
		<div class="panel panel-default" id="add_transfer_out_panel">

		<div class="panel-heading">
			<h4 class="text-center" style="color: white;" id="form_head"><?php if(isset($edit_flag)){if($edit_flag==0){ echo "Add Transfer Out Information"; }else{ echo "Transfer Out Information";}}else{ echo "Update Record";} ?></h4>
		</div>
<div class="row">
<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){  ?>
			

			<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				
				<?php } ?>
</div>
		<div class="panel panel-body">
			<?php
           $attributes = array(
              'id' => 'Add_transfer_out_form',
              'name' => 'Add_transfer_out_form',
               'autocomplete' => 'off',
            );
          // echo $receipt_details[0]->Receipt_Date;
           //print_r($receiptitems);exit();

           if(isset($receipt_details[0]->receipt_id)){
           	$receipt_id=$receipt_details[0]->receipt_id;
           }
           else{
           	$receipt_id=NULL;
           }
           echo form_open('Inventory/addtransferout/'.$receipt_id, $attributes); ?>
			<div class="row">
					
				<div class="col-xs-2 col-md-2 text-left" id="labelfor">
				<label for="item_type">Item Name <span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-md-4" id="item_div">
					<select name="item_type" id="item_type" required class="form-control">
						<option value="">--Select--</option>
					<?php foreach ($receiptitems as $value) { ?>
					<option value="<?php echo $value->id_mst_drugs; ?>" <?php if(isset($receipt_details[0]->id_mst_drugs)){ if($receipt_details[0]->id_mst_drugs==$value->id_mst_drugs) { echo 'selected';}}?>>
							<?php if($value->type==2){echo $value->name; } else{
								echo $value->name." (".$value->strength.")";
							} ?></option>
					<?php } ?>
					</select>
				</div>

					<div class="col-xs-2 col-md-2 text-left" id="labelforbatch1">
					<label for="batch_num">Batch Number<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-xs-4 col-md-4" id="batch_num_div1">
					<select name="batch_num" id="batch_num" required class="form-control" onchange="findRemaining();">
					<option value="">--Select--</option>
					<!-- <option value="999" <?php if(isset($receipt_details[0]->id_mst_drugs)){ if($receipt_details[0]->id_mst_drugs=='999') { echo 'selected';}}?>>other</option> -->
					</select>
					
				</div>
			</div>

<br>
<div class="row">

	<div class="col-xs-2 text-left" >
					<label for="date">Date of Transfer Out<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-xs-4 col-md-4">
					<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="Transfer Date" name="Entry_Date" id="Entry_Date" required onkeydown="return false" onkeyup="return false" value="<?php if(isset($receipt_details[0]->Entry_Date)){ echo timeStampShow($receipt_details[0]->Entry_Date);}else {echo timeStampShow(date('Y-m-d')); } ?>">
				</div>

<div class="col-xs-2 text-left" >
					<label for="quantity" id="quantitylbl">Quantity Transferred Out (Bottles/Kits)<span class="text-danger">*</span></label>
				</div>
				
				<div class="form-group col-xs-3">
					<input type="number" class="form-control" name="quantity" id="quantity" min="1" max="" required value="<?php if(isset($receipt_details[0]->quantity)){ echo $receipt_details[0]->quantity;}?>">
				</div><span class="text-danger" id="quantityRem"></span>

	</div>

			<div class="row">
				<div class="col-xs-2 text-left">
					<label for="from_to_type">Transferred Out To<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-xs-4">
					<select name="from_to_type" id="from_to_type" required class="form-control">
					<?php foreach ($source_name as $value) { ?>
						<option value="<?php echo $value->LookupCode;?>" <?php if(isset($receipt_details[0]->from_to_type)){ if($receipt_details[0]->from_to_type==$value->LookupCode) { echo 'selected';}}?>>
							<?php echo $value->LookupValue; ?></option>
					<?php } ?>
					</select>
				</div>
					
				<div class="col-xs-2 text-left">
					<label for="destination_name">Name<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-xs-4" id="source_div">
					<select name="destination_name" id="destination_name" required class="form-control">
					<option value="">--Select--</option>
					</select>
				</div>
				<div class="form-group col-xs-4" id="unrecognised_div">
					<input type="text" class="form-control" id="unrecognised" name="unrecognised" placeholder="Specify the Name" value="<?php if(isset($receipt_details[0]->unrecognised)){  echo $receipt_details[0]->unrecognised;}?>">
				</div>
			</div>

<br>
				<div class="row">
					<div class="col-xs-12 text-center">
						<button type="submit" class="btn btn-warning" id="submit_btn"><?php if(isset($edit_flag)){if($edit_flag==0){ echo "Add Transfer Out Information"; }else{ echo "Update Receipt";}}else{ echo "Update Receipt";} ?></button>
					</div>
				</div>
			<?php echo form_close();?>
<br>

		</div>
	</div>
</div>
</div>

</div>
<br>
</div>
	<div class="overlay">
	<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
</div>

	<!-- Data Table -->
<script type="text/javascript">

var findrem=[];

function isNumberKey(evt)
	{
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;

	
		if(evt.srcElement.id == 'Phone1' && $('#Phone1').val() > 999999999)
		{
			$("#modal_header").html('Contact number must be 10 digits');
			$("#modal_text").html('Please fill in appropriate Contact Number');
			$("#multipurpose_modal").modal("show");
			return false;
		}
	
		if(evt.srcElement.id == 'PinCode' && $('#PinCode').val() > 99999)
		{
			$("#modal_header").html('PIN cannot be more than 6 digits');
			$("#modal_text").html('Please fill in appropriate PIN');
			$("#multipurpose_modal").modal("show");
			return false;
		}

		return true;
	}
	
	
$(document).ready(function(){
 
            $('#item_type').change(function(){ 
               trasfer_out_source();
            }); 
            <?php if($edit_flag==1){?>
             $('#item_type').ready(function(){ 
               trasfer_out_source();
            }); <?php }else { ?>

            	 $('#item_type').on('load',function(){ 
               trasfer_out_source();
            });<?php } ?>
             $('#from_to_type').ready(function(){ 
        fill_source_name();        
});
             $('#source_div').show();
    		$('#unrecognised_div').hide();
    
    $('#from_to_type').change(function(){ 
        fill_source_name();        
});
        });

$('#quantity').change(function(){ 
           var rem_value=$('#quantityRem').html().split(" ");;
           var entered_val=$('#quantity').val(); 
         //alert(rem_value+"/"+entered_val);
           if(parseInt(entered_val)>parseInt(rem_value[1])){
           	alert('Entered value cannot be grater from Available value');
           	$('#quantity').val('');
           }   
            }); 
function trasfer_out_source(){
	var id=$('#item_type').val();
	var type_id=[];
	/*var fid=<?php if(isset($receipt_details[0]->receipt_id)){ echo $receipt_details[0]->receipt_id;}else{echo '0';}?>;*/
	if(id!=null){
                $.ajax({
                    url : "<?php echo site_url('inventory/get_batch_num');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                         console.log(data);
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
                        findrem=data;
        				var html = returned.batch_nums.map(function(e) {
   						return e.batch_num;
						});
						type_id= returned.batch_nums.map(function(e) {
   						return e.type; 
   					});
						if(type_id[0] == 1){
			$('#quantitylbl').html('');
			$('#quantitylbl').html('Quantity Transferred Out (Bottles)');
		}
		 if(type_id[0] == 2){
			$('#quantitylbl').html('');
			$('#quantitylbl').html('Quantity Transferred Out (Kits)');
		}
						//console.log(html);
						//alert(findrem);
                        var i;
                        var opt='';
                        opt="<option>----Select----</option>";
                        for(i=0; i<html.length; i++){
                            opt += '<option value="'+html[i]+'">'+html[i]+'</option>'; 
                        }
                        //console.log(opt);
                        $('#batch_num').html(opt);
 
                    }
                });
            }
                return false;
}	

function fill_source_name(){
var id=$('#from_to_type').val();
  var fid=<?php if(isset($receipt_details[0]->transfer_to)){ echo $receipt_details[0]->transfer_to;}else{echo '0';}?>;   
  if(id!=3){
$('#source_div').show();
            	$('#unrecognised_div').hide();
            	$('#source_name').attr('required','required');
    			$('#unrecognised').removeAttr('required');
}   

				if(id==1){
					var opt='';
					opt += '<option value="996">abc</option>';
					opt += '<option value="997">abcd</option>';
					opt+=  '<option value="998">abcde</option>';
					$('#destination_name').html(opt);
				}
                else if(id==2){
                $.ajax({
                    url : "<?php echo site_url('inventory/get_facilities');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
        				var id_mstfacility = returned.id_mstfacility.map(function(e) {
   						return e.id_mstfacility;
						});
						var facility_short_name = returned.id_mstfacility.map(function(e) {
   						return e.facility_short_name;
						});
                        var i;
                        var select=''
                        var opt='';
                        for(i=0; i<id_mstfacility.length; i++){
                        	if(id_mstfacility[i]==fid)
                        	{
                        		select='selected';
                        	}else{
                        		select='';
                        	}
                            opt += '<option value="'+id_mstfacility[i]+'"'+select+'>'+facility_short_name[i]+'</option>';
                        }
                        //console.log(opt);
                        $('#destination_name').html(opt);
 
                    }
                });
            }else if(id==3){

            	$('#source_div').hide();
            	$('#unrecognised_div').show();
            	$('#source_name').removeAttr('required');
    			$('#unrecognised').attr('required','required');

            }
                return false;

}
function findRemaining(){
	var id=$('#batch_num').val();
	var item_id=$('#item_type').val();

	var type_id=[];
	var strength=[];

                        var returned = JSON.parse(findrem);
        				type_id= returned.batch_nums.map(function(e) {
   						return e.type; 
   					});
   						strength= returned.batch_nums.map(function(e) {
   						return e.strength;
						});

		
                $.ajax({
                    url : "<?php echo site_url('inventory/get_rem_val/T');?>",
                    method : "POST",
                    data : {id: id,type: type_id[0]},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        console.log(data);
                        var returned = JSON.parse(data);
        				console.log(returned);
  						var rem  = returned.Rem_value.map(function(e) {
   						return e.Remaining;
						});
                    		if(type_id[0]==2){
                    		var test_per_kit=strength[0];
                    		var Remaining=parseInt(rem/test_per_kit);
                    		 $('#quantityRem').html("Avl: "+Remaining);
                    		}
   
                     else if(type_id[0]==1){

                     	 $('#quantityRem').html("Avl: "+rem);
                      }
                  if(rem==0 || Remaining==0){
                  	$('#quantityRem').hide();
                  }
                    }
                    
                    	

                });
	
                return false;
}	
</script>


