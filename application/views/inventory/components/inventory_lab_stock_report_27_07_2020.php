<style>
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: top;
		background-color: #085786;
		color: white;
		font-family: 'Source Sans Pro';
	}
	td{
		text-align: center; 
		vertical-align: middle;
	}
	.btn-width{
	width: 12%;
	margin-top: 30px;
}
.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
	.item{
		width: 140px !important;
	}
	.batch_num{
		width: 100px !important;
	}
	a{
		cursor: pointer;
	}

	a:hover,a:focus {
		cursor: pointer;
		text-decoration: none;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
		 font-family: 'Source Sans Pro';
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	select[readonly] {
		background: #eee;
		pointer-events: none;
		touch-action: none;
	}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData');
$filters = $this->session->userdata('filters');
//echo "<pre>"; print_r($filters);

if($loginData->user_type==1) { 
	$select = '';
}else{
	$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
	$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
	$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
	$select4 = '';	
}
?>
<?php $loginData = $this->session->userdata('loginData');
$filters1=$this->session->userdata('filters1');
if ($loginData->user_type==2 || $filters1['id_mstfacility'] !=''){
	$colspan="17";
	$th="<th>Batch Number</th>";
	$item_text="Item";
}
else if (($loginData->user_type==1 || $loginData->user_type==3)&&$filters1['id_mstfacility']==''){
	$colspan="16";
	$th=" ";
	if (($loginData->user_type==1 || $loginData->user_type==3)|| $filters1['id_search_state'] !=''){
	$item_text="Facility";
}
}
if ($Repo_flg==1) {
	$show_text="(no. of bottles)";
}
elseif ($Repo_flg==2) {
	$show_text='(no. of screening tests)';
}
?>
<div class="row main-div" style="min-height:400px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xl-12">
		<div class="panel panel-default" id="Record_receipt_panel">
			<div class="panel-heading" style=";background: #333;">
				<h4 class="text-center" style="color: white;background: #333" id="Record_form_head"><?php if($Repo_flg==1){echo "Drug Stock Report";$option='<option value="drug">All Drugs</option>';}elseif($Repo_flg==2){ echo "Lab Stock Report";$option='<option value="Kit">All Screening Test Kits</option>';} ?></h4>
			</div>
			<div class="row">
				<?php 
				$tr_msg= $this->session->flashdata('tr_msg');
				$er_msg= $this->session->flashdata('er_msg');
				if(!empty($tr_msg)){  ?>


					<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
						<div class="hpanel">
							<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<?php echo $this->session->flashdata('tr_msg');?>. </div>
							</div>
						</div>
					<?php } else if(!empty($er_msg)){ ?>
						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('er_msg');?>. </div>
								</div>
							</div>

						<?php } ?>
					</div>
					<?php
					$attributes = array(
						'id' => 'Receipt_filter_form',
						'name' => 'Receipt_filter_form',
						'autocomplete' => 'false',
					);
//pr($items);
//print_r($receipt_details);
//pr($Closing_stock_Repo);
					echo form_open('', $attributes); ?>

					<div class="row" style="margin-left: 10px;">
						<!-- <div class="col-md-2">
							<label for="">State</label>
							<select type="text" name="search_state" id="search_state" class="form-control input_fields" <?php  ?>  <?php echo $select2.$select3.$select4; ?>>
								<option value="">All States</option>
								<?php 
								foreach ($states as $state) {
									?>
									<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo ucwords(strtolower($state->StateName)); ?></option>
									<?php 
								}
								?>
							</select>
						</div>
						<div class="col-md-2">
							<label for="">District</label>
							<select type="text" name="input_district" id="input_district" class="form-control input_fields"  <?php echo $select.$select2.$select4; ?>>
								<option value="">All District</option>
								<?php 
						/*foreach ($districts as $district) {
						?>
						<option value="<?php echo $district->id_mstdistrict; ?>" <?php echo (count($default_districts)==1 && $default_districts[0]->id_mstdistrict == $district->id_mstdistrict)?'selected':''; ?>><?php echo ucwords(strtolower($district->DistrictName)); ?></option>
						<?php 
						}*/
						?>
						</select>
						</div>
						
						<div class="col-md-2">
							<label for="">Facility</label>
							<select type="text" name="mstfacilitylogin" id="mstfacilitylogin" class="form-control input_fields"  <?php echo $select.$select2; ?>>
								<option value="">All Facility</option>
								<?php 
						/*foreach ($facilities as $facility) {
						?>
						<option value="<?php echo $facility->id_mstfacility; ?>" <?php echo (count($default_facilities)==1 && $default_facilities[0]->id_mstfacility == $facility->id_mstfacility)?'selected':''; ?>><?php echo ucwords(strtolower($facility->facility_short_name)); ?></option>
						<?php 
						}*/
						?>
						</select>
					</div> -->
					<div class="col-xs-3 col-md-2">
						<label for="item_type">Item Name <span class="text-danger">*</span></label>
						<div class="form-group">
							<select name="item_type" id="item_type" required class="form-control" style="height: 30px;">
								<?php echo $option; ?>
								<?php foreach ($items as $value) { ?>

									<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
										<?php if($value->type==2){echo $value->drug_name; } else{
											echo $value->drug_name." (".$value->strength.")";
										} ?></option>
									<?php } ?>
									<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
								</select>
							</div>
						</div>
						<div class="col-md-2 col-sm-12">
							<label for="">Year</label>
							<select name="year" id="year" required class="form-control input_fields" style="height: 30px;">
								<option value="">Select</option>
								<?php
								$dates = range('2019', date('Y'));
								$flag=0;
								foreach($dates as $date){
									$year = $date;
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									elseif($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}

								}
								?>
							</select>
						</div>
						<div class="col-md-2 col-sm-12">
							<label for="">Month</label>
							<select type="text" name="month" id="month" class="form-control input_fields" required style="height: 30px;">
								<option value="">Select</option>
								<?php
								$flag=0;
								for ($i = 1; $i <=12;$i++) {
									$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
									if($this->input->post('month')==$i){
										echo "<option value=".$i." selected>".$date_str ."</option>";
										$flag=1;
									}
									elseif($i==date('m') && $flag==0){
										echo "<option value=".$i." selected>".$date_str ."</option>";
									}
									else{
										echo "<option value=".$i.">".$date_str ."</option>";

									}
								} ?>
							</select>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12 btn-width">
					<button type="submit" class="btn btn-block btn-success" id="monthreport" style="line-height: 1; font-weight: 600;font-size: 15px;height: 30px;">Search</button>
				</div><span style="margin-top: 25px;margin-right: 25px; float: right;"><a href="javascript:void(0)"><i class="fa fa-2x fa-download" id="excel_button" onclick="tableToExcel('testTable', 'W3C Example','Monthly_Report.xls')" title="Excel Download"></i></a></span>

						<?php echo form_close(); ?>
					</div>

</div>
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover" id="table_inventory_list">


	<thead>
		<tr>
			<th>Action</th>
			<th>Item</th>
			<th>Batch Num</th>
			<th>Opening Stock  <?php echo $show_text; ?></th>
			<th>Stock received from <br>warehouse <?php echo $show_text; ?></th>
			<th>Stock received  through a transfer-in from a facility <?php echo $show_text; ?></th>
			<th>Stock received from<br> unregistered sources <?php echo $show_text; ?></th>
			<th>Stock received from<br>  third party sources <?php echo $show_text; ?></th>
			<th>Total Receipts <br> <?php echo $show_text; ?></th>

			<th>Stock Expired(no. of screening tests)</th>
			<th>Stock rejected on receipt</th>
			<th>Stock returned due to stock failure	</th>
			<th>Stock utilized in own facility  (no. of screening tests performed and controls used)</th>
			<th>Stock transferred out <?php echo $show_text; ?></th>
			<th>Closing Stock <?php echo $show_text; ?>	</th>
			<th>Reorder point/Minimum Stock level that should be maintained (bottles)</th>
			<th>Maximum Stock that should be maintained (bottles)</th>
			<th>Reccomendation</th>
			


  

		</tr>
	</thead>
	<tbody>
<!-- </tbody>
</table> -->
<?php // echo "<pre>";/*print_r($inventory_details);*/
//$result=array_unique($inventory_details->);
if ($loginData->user_type==2 || $filters1['id_mstfacility'] !=''){
	$item_id=array();
	$data=array();
	$filtered_items=array();
	foreach ($Inventory_Repo as $key => $value) {
    $filtered_items[$key]['id_mst_drugs'] =$value->drug_name;
    $filtered_items[$key]['strength'] =$value->strength;
    /*$filtered_items['id_mst_drugs'] = $value->id_mst_drugs;
    $filtered_items['id_mst_drugs'] = $value->id_mst_drugs;*/
}
	foreach ($items as $key => $value) {
		foreach ($filtered_items as $key1 => $value1) {
			if($value->id_mst_drugs==$filtered_items[$key1]['id_mst_drugs']){
				$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
				$item_id[$key]['drug_name']= $value->drug_name;
				$item_id[$key]['strength']= $filtered_items[$key1]['strength'];
			}
		}

	}
	$filtered_arr=array_unique($item_id,SORT_REGULAR);
//pr($filtered_arr);
//pr($Inventory_Repo);
	?>
<!-- <table class="table table-striped table-bordered table-hover">
	<tbody> -->
		<?php if(!empty($Inventory_Repo)){
			foreach ($filtered_arr as $key=>$value_temp) { ?>
				<tr>
					<th class="info" style="border: none;background-color: #333;color:#fff;text-align: left;cursor: pointer;"><i class="fa fa-minus item_toggle" aria-hidden="true" id="<?php echo 'item'.$value_temp['id_mst_drugs']; ?>"></i></th><th class="info"style="border: none;background-color: #333;font-size: 16px;color:#fff;text-align: left;"><?php echo $value_temp['drug_name']; ?></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
			<th style="background: transparent;"></th>
					</tr>
					<?php foreach ($Inventory_Repo as $key => $value) { 
						if($value_temp['id_mst_drugs']==$value->drug_name){
							?>
							<tr class="<?php echo 'item'.$value_temp['id_mst_drugs']; ?> item_row">
								<td></td>
								<td class="item">
									<?php if($value->type==1){echo $value_temp['drug_name']."(".$value->strength.")";} elseif($value->type==2){echo $value_temp['drug_name'];} ?>
								</td>
							 <td>
									<?php echo $value->batch_num; ?>
								</td>
								<td>
										<?php 
										if ($value->Expire_stock1>0) {

						$total_expired=($value->Expire_stock1-($value->returned_quantity1+$value->total_rejected1+$value->total_utilize1+$value->total_transfer_out1));
					}
					else{
						$total_expired=0;
					}

					$opening_stock=($value->total_receipt1-($value->returned_quantity1+$value->total_rejected1+$value->total_utilize1+$value->total_transfer_out1+$total_expired));
										echo empty($opening_stock)> 0 ? "0" : $opening_stock;
										?>
									</td>
								<td >
									<?php if($value->warehouse_quantity_received==NULL){
										echo '-';
									}else{
										echo $value->warehouse_quantity_received;
									} ?>
								</td>
								<td>
									<?php if($value->quantity_received==NULL){
										echo '-';
									}else{
										echo $value->quantity_received; }?>
									</td>
									<td>
										<?php if($value->uslp_quantity_received==NULL){
											echo '-';
										}else{ echo $value->uslp_quantity_received;} ?>
									</td>
										<td>
										<?php if($value->tp_quantity_received==NULL){
											echo '-';
										}else{ echo $value->tp_quantity_received;} ?>
									</td>
									<td>
										<?php if($value->total_receipt==NULL){
											echo '-';
										}else{ echo $value->total_receipt; }?>
									</td>
									<td>
										<?php 
										if ($value->Expire_stock>0) {
											
										$total_expired=($value->Expire_stock-($value->returned_quantity+$value->total_rejected+$value->total_utilize+$value->total_transfer_out));

											echo $total_expired;

										}
										else{
											$total_expired=0;
											echo '-';
										}
										?>
									</td>
									<td>
										<?php 
											if($value->total_rejected==NULL){
											echo '-';
										}else{ echo $value->total_rejected; }

										?>
									</td>
									<td>
										<?php 
											if($value->returned_quantity==NULL){
											echo '-';
										}else{ echo $value->returned_quantity; }
											
										?>
									</td>
									<td>
										<?php if($value->total_utilize==NULL){
											echo '-';
										}else{echo $value->total_utilize; }?>
									</td>
									<td>
										<?php if($value->total_transfer_out==NULL){
											echo '-';
										}else{ echo $value->total_transfer_out; }?>
									</td>
									<td>
										<?php 
										$closing_stock=(($value->total_receipt +$opening_stock)-($value->returned_quantity+$value->total_rejected+$value->total_utilize+$value->total_transfer_out+$total_expired));

										if($closing_stock==NULL){
											echo $closing_stock;
										}else{echo $closing_stock;} ?>
									</td>
									<td>
										<?php 

											if ($value->Expire_stock2>0) {

											$total_expired2=($value->Expire_stock2-($value->returned_quantity2+$value->total_rejected2+$value->total_utilize2+$value->total_transfer_out2));
										}
										else{
											$total_expired2=0;
										}

					$opening_stock2=($value->total_receipt2-($value->returned_quantity2+$value->total_rejected2+$value->total_utilize2+$value->total_transfer_out2+$total_expired2));
						

										$P=((($closing_stock + $opening_stock + $opening_stock2)/3)*$value->lead_time) +(1+ ($value->buffer_stock/100));
										echo $P; ?>
										
									</td>
										<td>
										
									</td>
										<td>
											<?php 
										if($closing_stock==NULL || $closing_stock==0){
											echo 'Yes';
										}else{echo 'No';} ?>
									</td>
									<?php $opening_stock=0;
											$opening_stock2=0;
											$closing_stock=0; ?>
								</tr>
<!-- <td class="text-center">
<a href="<?php echo site_url()."/inventory/AddReceipt/".$value->receipt_id;?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
<a href="<?php echo site_url()."/inventory/delete_receipt/".$value->receipt_id."/R";?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
</td>

</tr> -->
<?php } }}}else{ ?>
	<tr >
		<td colspan="<?php echo $colspan; ?>" class="text-center">
			NO RECORDS FOUND  BETWEEN SELECTED DATES.
		</td>

	</tr>
<?php } }

else if (($loginData->user_type==1 || $loginData->user_type==3)&&$filters1['id_mstfacility']==''){
	$item_id=array();
	$data=array();
	$filtered_items=array();
	foreach ($Inventory_Repo as $key => $value) {
    $filtered_items[$key]['id_mst_drugs'] =$value->drug_name;
    $filtered_items[$key]['strength'] =$value->strength;
    /*$filtered_items['id_mst_drugs'] = $value->id_mst_drugs;
    $filtered_items['id_mst_drugs'] = $value->id_mst_drugs;*/
}
	foreach ($items as $key => $value) {
		foreach ($filtered_items as $key1 => $value1) {
			if($value->id_mst_drugs==$filtered_items[$key1]['id_mst_drugs']){
				$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
				if ($value->id_mst_drugs==8) {
								$item_id[$key]['drug_name']= $value->drug_name." ".$value1->strength." mg";
							}
							else{
								$item_id[$key]['drug_name']= $value->drug_name;
							}
				$item_id[$key]['strength']= $filtered_items[$key1]['strength'];
			}
		}

	}
	$filtered_arr=array_unique($item_id,SORT_REGULAR);
//pr(($filtered_items));
//pr($Inventory_Repo);exit();
	?>
<!-- <table class="table table-striped table-bordered table-hover">
	<tbody> -->
		<?php if(!empty($Inventory_Repo)){
			
			foreach ($filtered_arr as $key=>$value_temp) { 
				?>
				<tr>
						<td class="info" style="border: none;background-color: #ededed; color:black;text-align: right;font-weight: 700;"><i class="fa fa-minus item_toggle" style="cursor: pointer;" aria-hidden="true" id="<?php echo 'item'.$value_temp['id_mst_drugs']; ?>"></i></td>
					<td class="info"style="font-size: 12px;font: bold;font-weight: 700; text-align: left;"><?php echo $value_temp['drug_name']; ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					
				</tr>
					<?php foreach ($Inventory_Repo as $key => $value) { 
						if($value_temp['id_mst_drugs']==$value->drug_name){?>
							<tr class="<?php echo 'item'.$value_temp['id_mst_drugs']; ?>">
								<?php if(($loginData->user_type==1) &&($filters1['id_search_state']=='')){?>
										<td class="info" style="border: none;text-align: right;font-weight: 700;"><a href="javascript:void(0);" onclick="get_facility_data($(this).parent().closest('tr').index(),'<?php echo $value->type;?>','<?php echo $value->id_mststate;?>','<?php echo $value->id_mst_drugs;?>');"class="fa fa-plus facility_td" style="cursor: pointer;" aria-hidden="true" id="<?php echo $value->id_mst_drugs.'-'.$value->id_mststate; ?>"></a></td>
									<?php } 
									else if($loginData->user_type==3 ||($filters1['id_search_state']!='')){ ?>
										<td class="info" style="border: none;text-align: right;"><a href="javascript:void(0);" onclick="get_item_data($(this).parent().closest('tr').index(),'<?php echo $value->type;?>','<?php echo $value->id_mstfacility;?>','<?php echo $value->id_mst_drugs;?>');"class="fa fa-plus item_td" style="cursor: pointer;" aria-hidden="true" id="<?php echo $value->id_mst_drugs.'-'.$value->id_mstfacility; ?>"></a></td>
									<?php } ?>
								<td class="item">

									<?php if(($loginData->user_type==1) &&($filters1['id_search_state']=='')){
										echo $value->StateName;
									}
									else if($loginData->user_type==3 ||($filters1['id_search_state']!='')){
										echo $value->hospital;
									}

									?>
								</td>
								<td>
									&nbsp;
								</td>
									<td>
										<?php 
										if ($value->Expire_stock1>0) {

						$total_expired1=($value->Expire_stock1-($value->returned_quantity1+$value->total_rejected1+$value->total_utilize1+$value->total_transfer_out1));
					}
					else{
						$total_expired1=0;
					}

					$opening_stock=($value->total_receipt1-($value->returned_quantity1+$value->total_rejected1+$value->total_utilize1+$value->total_transfer_out1+$total_expired1));
										echo empty($opening_stock)> 0 ? "0" : $opening_stock;
										?>
									</td>
								<td>
								<?php if($value->warehouse_quantity_received==NULL){
										echo '-';
									}else{
										echo $value->warehouse_quantity_received;
									} ?>
								</td>
								<td>
									<?php if($value->quantity_received==NULL){
										echo '-';
									}else{
										echo $value->quantity_received; }?>
									</td>
									<td>
										<?php if($value->uslp_quantity_received==NULL){
											echo '-';
										}else{ echo $value->uslp_quantity_received;} ?>
									</td>
										<td>
										<?php if($value->tp_quantity_received==NULL){
											echo '-';
										}else{ echo $value->tp_quantity_received;} ?>
									</td>
									<td>
										<?php if($value->total_receipt==NULL){
											echo '-';
										}else{ echo $value->total_receipt; }?>
									</td>
									<td>
										<?php 
											if ($value->Expire_stock>0) {

						$total_expired=($value->Expire_stock-($value->returned_quantity+$value->total_rejected+$value->total_utilize+$value->total_transfer_out));
					}
					else{
						$total_expired=0;
					}
					echo $total_expired;
										?>
									</td>
									<td>
										<?php 
											if($value->total_rejected==NULL){
											echo '-';
										}else{ echo $value->total_rejected; }

										?>
									</td>
									<td>
										<?php 
											if($value->returned_quantity==NULL){
											echo '-';
										}else{ echo $value->returned_quantity; }
											
										?>
									</td>
									<td>
										<?php if($value->total_utilize==NULL){
											echo '-';
										}else{echo $value->total_utilize; }?>
									</td>
									<td>
										<?php if($value->total_transfer_out==NULL){
											echo '-';
										}else{ echo $value->total_transfer_out; }?>
									</td>
									<td>
										<?php 
										$closing_stock=(($value->total_receipt +$opening_stock)-($value->returned_quantity+$value->total_rejected+$value->total_utilize+$value->total_transfer_out+$total_expired));

										if($closing_stock==NULL){
											echo $closing_stock;
										}else{echo $closing_stock;} ?>
									</td>
									<td>
										<?php 

											if ($value->Expire_stock2>0) {

											$total_expired2=($value->Expire_stock2-($value->returned_quantity2+$value->total_rejected2+$value->total_utilize2+$value->total_transfer_out2));
										}
										else{
											$total_expired2=0;
										}

					$opening_stock2=($value->total_receipt2-($value->returned_quantity2+$value->total_rejected2+$value->total_utilize2+$value->total_transfer_out2+$total_expired2));
						

										$P=((($closing_stock + $opening_stock + $opening_stock2)/3)*$value->lead_time) +(1+ ($value->buffer_stock/100));
										echo $P; ?>
										
									</td>
										<td>
										
									</td>
										<td>
											<?php 
										if($closing_stock==NULL || $closing_stock==0){
											echo 'Yes';
										}else{echo 'No';} ?>
									</td>
								<?php $opening_stock=0;
											$opening_stock2=0;
											$closing_stock=0; ?>		
</tr>
									
									
<!-- <td class="text-center">
<a href="<?php //echo site_url()."/inventory/AddReceipt/".$value->receipt_id;?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
<a href="<?php //echo site_url()."/inventory/delete_receipt/".$value->receipt_id."/R";?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
</td>

</tr> -->
<?php } }}}else{ ?>
	<tr >
		<td colspan="<?php echo $colspan; ?>" class="text-center">
			NO RECORDS FOUND  BETWEEN SELECTED DATES.
		</td>

	</tr>
<?php } }?>

</tbody>
</table>
</div>
</div>
<br>
</div>
<div class="overlay">
	<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
</div>

<!-- Data Table -->
<script type="text/javascript">

	
	$(document).ready(function(){
		$(".glyphicon-trash").click(function(e){
			var ans = confirm("Are you sure you want to delete?");
			if(!ans)
			{
				e.preventDefault();
			}
		});
		$(".facility_td").click(function(e){
			$(this).toggleClass('fa-plus fa-minus');
			var abc=$(this).attr("id");
			$('.'+abc).toggle();
			if ($('.'+abc).is(':visible')==false) {
				
  				$('.item_row').hide();
			}
		});
		$(".item_toggle").click(function(e){
			$(this).toggleClass('fa-plus fa-minus');
			
			var abc=$(this).attr("id");
			<?php if($loginData->user_type!=2){?>
				
			$('.'+abc).toggle();

			if ($('.'+abc).is(':visible')==false && $('.item_row').is(':visible')==true) {
  				$('.item_row').hide();
			}
			
<?php }else{?>
		$('.'+abc).toggle();
	<?php }?>
		});
		


		
	});


		$(document).ready(function(){

		$("#search_state").trigger('change');
//alert('rff');

});
	$('#search_state').change(function(){

		$.ajax({
			url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
			data : {
				<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
			},
			method : 'GET',
			success : function(data)
			{
//alert(data);
$('#input_district').html(data);
$("#input_district").trigger('change');
//$("#mstfacilitylogin").trigger('change');

},
error : function(error)
{
//alert('Error Fetching Districts');
}
})
	});

	$('#input_district').change(function(){

		$.ajax({
			url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),

			method : 'GET',
			success : function(data)
			{
//alert(data);
if(data!="<option value=''>Select Facilities</option>"){
	$('#mstfacilitylogin').html(data);

}
},
error : function(error)
{
//alert('Error Fetching Blocks');
}
})
	});

function  get_facility_data(row_index,type,id_mststate,id_mst_drugs){

//alert(row_index);
var dataString = {
id_mst_drugs: id_mst_drugs,
id_mststate: id_mststate,
type: type,

};
$.ajax({    
url: '<?php echo base_url();?>Inventory_Reports/get_facility_data',
type: 'GET',
data: dataString,
dataType: 'json',
async: false,
cache: false,
success: function (data) {


var parsedData = JSON.parse(data.fields);
//console.log(parsedData);
var output = [];

for(var i=0; i<parsedData.length; i++){

var Expire_stock2=(parseInt((parsedData[i]['Expire_stock2'] || 0))-(parseInt(parsedData[i]['returned_quantity2']|| 0)+parseInt(parsedData[i]['total_rejected2']|| 0)+parseInt(parsedData[i]['total_utilize2']|| 0)+parseInt(parsedData[i]['total_transfer_out2']|| 0)));
	if (Expire_stock2<0) {
	Expire_stock2=0;
}
	var opening_stock2=(parseInt((parsedData[i]['total_receipt2'] || 0))-(parseInt(parsedData[i]['returned_quantity2']|| 0)+parseInt(parsedData[i]['total_rejected2']|| 0)+parseInt(parsedData[i]['total_utilize2']|| 0)+parseInt(parsedData[i]['total_transfer_out2']|| 0)+Expire_stock2));


	var Expire_stock1=(parseInt((parsedData[i]['Expire_stock1'] || 0))-(parseInt(parsedData[i]['returned_quantity1']|| 0)+parseInt(parsedData[i]['total_rejected1']|| 0)+parseInt(parsedData[i]['total_utilize1']|| 0)+parseInt(parsedData[i]['total_transfer_out1']|| 0)));
	if (Expire_stock1<0) {
	Expire_stock1=0;
}
	var opening_stock=(parseInt((parsedData[i]['total_receipt1'] || 0))-(parseInt(parsedData[i]['returned_quantity1']|| 0)+parseInt(parsedData[i]['total_rejected1']|| 0)+parseInt(parsedData[i]['total_utilize1']|| 0)+parseInt(parsedData[i]['total_transfer_out1']|| 0)+Expire_stock1));
var closing_stock=((parseInt(parsedData[i]['total_receipt'] || 0)+opening_stock)-(parseInt(parsedData[i]['returned_quantity']|| 0)+parseInt(parsedData[i]['total_rejected']|| 0)+parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_transfer_out']|| 0)));
var Expire_stock=(parseInt((parsedData[i]['Expire_stock'] || 0))-(parseInt(parsedData[i]['returned_quantity']|| 0)+parseInt(parsedData[i]['total_rejected']|| 0)+parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_transfer_out']|| 0)));
if (Expire_stock<0) {
	Expire_stock=0;
}
else{
	closing_stock=closing_stock-Expire_stock;
}
var rec='';
if (closing_stock==0) {
	rec='Yes';
}
else if (closing_stock>0) {
	rec='No';
}
var formula=(((closing_stock + opening_stock + opening_stock2)/3)*parseInt(parsedData[i]['lead_time'])) +(1+ (parseInt(parsedData[i]['buffer_stock'])/100));
var facility=parsedData[i]['id_mstfacility'];

	str='<tr class="'+id_mst_drugs+'-'+id_mststate+' item'+id_mst_drugs+'">';
str+=' <td class="info" style="border: none;text-align: right;"><a class="fa fa-plus item_td"  href="javascript:void(0);" onclick="get_item_data($(this).parent().closest('+"'tr'"+').index(),'+"'"+type+"'"+','+"'"+facility+"'"+','+"'"+id_mst_drugs+"'"+');"  aria-hidden="true" id="'+id_mst_drugs+'-'+facility+'"></a></td>'
	+'<td>'+parsedData[i]['hospital'] +'</td>'
	+' <td></td>'
	+' <td>'+opening_stock+'</td>'
	+' <td>'+parsedData[i]['warehouse_quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['uslp_quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['tp_quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['total_receipt'] +'</td>'
	+' <td>'+Expire_stock+'</td>'
	+' <td>'+(parsedData[i]['total_rejected'] || "-") +'</td>'
	+' <td>'+(parsedData[i]['returned_quantity'] || "-") +'</td>'
	+' <td>'+parsedData[i]['total_utilize'] +'</td>'
	+' <td>'+parsedData[i]['total_transfer_out'] +'</td>'
	+' <td>'+closing_stock +'</td>'
	+' <td>'+formula+'</td>'
	+' <td></td>'
	+' <td>'+rec+'</td>';
str+='</tr>';
output.push(str);
closing_stock=0;
}
 $('table > tbody > tr').eq(row_index).after(output);
 $('#'+id_mst_drugs+'-'+id_mststate).removeAttr('onclick');
 $('.'+id_mst_drugs+'-'+id_mststate).hide();
   }
});
}

function  get_item_data(row_index,type,id_mstfacility,id_mst_drugs){


var dataString = {
id_mst_drugs: id_mst_drugs,
id_mstfacility: id_mstfacility,
type: type,

};
$.ajax({    
url: '<?php echo base_url();?>Inventory_Reports/get_item_data',
type: 'GET',
data: dataString,
dataType: 'json',
async: false,
cache: false,
success: function (data) {


var parsedData = JSON.parse(data.fields);
//console.log(parsedData);
var output = [];

for(var i=0; i<parsedData.length; i++){
var Expire_stock2=(parseInt((parsedData[i]['Expire_stock2'] || 0))-(parseInt(parsedData[i]['returned_quantity2']|| 0)+parseInt(parsedData[i]['total_rejected2']|| 0)+parseInt(parsedData[i]['total_utilize2']|| 0)+parseInt(parsedData[i]['total_transfer_out2']|| 0)));
	if (Expire_stock2<0) {
	Expire_stock2=0;
}
	var opening_stock2=(parseInt((parsedData[i]['total_receipt2'] || 0))-(parseInt(parsedData[i]['returned_quantity2']|| 0)+parseInt(parsedData[i]['total_rejected2']|| 0)+parseInt(parsedData[i]['total_utilize2']|| 0)+parseInt(parsedData[i]['total_transfer_out2']|| 0)+Expire_stock2));


	var Expire_stock1=(parseInt((parsedData[i]['Expire_stock1'] || 0))-(parseInt(parsedData[i]['returned_quantity1']|| 0)+parseInt(parsedData[i]['total_rejected1']|| 0)+parseInt(parsedData[i]['total_utilize1']|| 0)+parseInt(parsedData[i]['total_transfer_out1']|| 0)));
	if (Expire_stock1<0) {
	Expire_stock1=0;
}
	var opening_stock=(parseInt((parsedData[i]['total_receipt1'] || 0))-(parseInt(parsedData[i]['returned_quantity1']|| 0)+parseInt(parsedData[i]['total_rejected1']|| 0)+parseInt(parsedData[i]['total_utilize1']|| 0)+parseInt(parsedData[i]['total_transfer_out1']|| 0)+Expire_stock1));
var closing_stock=((parseInt(parsedData[i]['total_receipt'] || 0)+opening_stock)-(parseInt(parsedData[i]['returned_quantity']|| 0)+parseInt(parsedData[i]['total_rejected']|| 0)+parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_transfer_out']|| 0)));
var Expire_stock=(parseInt((parsedData[i]['Expire_stock'] || 0))-(parseInt(parsedData[i]['returned_quantity']|| 0)+parseInt(parsedData[i]['total_rejected']|| 0)+parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_transfer_out']|| 0)));
if (Expire_stock<0) {
	Expire_stock=0;
}
else{
	closing_stock=closing_stock-Expire_stock;
}
var rec='';
if (closing_stock==0) {
	rec='Yes';
}
else if (closing_stock>0) {
	rec='No';
}
var formula=(((closing_stock + opening_stock + opening_stock2)/3)*parseInt(parsedData[i]['lead_time'])) +(1+ (parseInt(parsedData[i]['buffer_stock'])/100));
var facility=parsedData[i]['id_mstfacility'];
	str='<tr class="'+id_mst_drugs+'-'+id_mstfacility+' item_row">';
str+='<td></td><td></td> <td>'+parsedData[i]['batch_num'] +'</td>'
	+' <td>'+opening_stock+'</td>'
	+' <td>'+parsedData[i]['warehouse_quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['uslp_quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['tp_quantity_received'] +'</td>'
	+' <td>'+parsedData[i]['total_receipt'] +'</td>'
	+' <td>'+Expire_stock+'</td>'
	+' <td>'+(parsedData[i]['total_rejected'] || "-") +'</td>'
	+' <td>'+(parsedData[i]['returned_quantity'] || "-") +'</td>'
	+' <td>'+parsedData[i]['total_utilize'] +'</td>'
	+' <td>'+parsedData[i]['total_transfer_out'] +'</td>'
	+' <td>'+closing_stock +'</td>'
	+' <td>'+formula+'</td>'
	+' <td></td>'
	+' <td>'+rec+'</td>'
str+='</tr>';
output.push(str);
closing_stock=0;
opening_stock=0;
Expire_stock=0;
Expire_stock1=0;
}
 $('table > tbody > tr').eq(row_index).after(output);
 $('#'+id_mst_drugs+'-'+id_mstfacility).removeAttr('onclick');
 $('.'+id_mst_drugs+'-'+id_mstfacility).hide();
//alert(output);
   }
});
}
$(document).on('click', ".item_td", function() {
	$(this).toggleClass('fa-plus fa-minus');
			var abc=$(this).attr("id");
			$('.'+abc).toggle();
	});
$(document).ready(function(){

$('#year').change(function(){
	var count=0;
	<?php  $count=0; ?>;
	var options='<option value="">Select</option>';
	var currentYear = (new Date).getFullYear();
	if (currentYear==$('#year').val()) {
			<?php
		$flag=0;
		$count=date("m");
	for ($i = 1; $i <=$count;$i++) {
	$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
	if($this->input->post('month')==$i){ 	?>
	options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php $flag=1;
	}	
	elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php }
	else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	}
	else{
		<?php
		$flag=0;
		$count=12;
		for ($i = 1; $i <=$count;$i++) {
		$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
		if($this->input->post('month')==$i){ 	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php $flag=1;
		}	
		elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php }
		else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	
	}
	//alert(options);
$('#month').html(options);
});
$("#year").trigger('change');
});
</script>


