<style>
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: top;
		width: 7.14% !important;
	}
	td{
		text-align: center; 
		vertical-align: middle;
		width: 7.14% !important;
		word-break: break-all;
	}
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	input,nav,label,span{
		font-family: 'Source Sans Pro';

	}
	label{
		font-family: 'Source Sans Pro';
		font-size: 14px;
	}
	input,select,.form-control{
		height: 30px;
	}
	.modal-lg{
		width: 83%;
	}
	.pd-right-0{padding-right: 0px;}
	select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
</style>
<?php  //error_reporting(0);
//pr($receipt_items);
$loginData = $this->session->userdata('loginData'); ?>
<!-- add/edit facility contact modal starts-->
<div class="modal fade " tabindex="-1" role="dialog" id="myModal1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #333;color: white;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Stock Receipt</h4>
      </div>
      <div class="modal-body">
      	<?php
           $attributes = array(
              'id' => 'Add_Receipt_form',
              'name' => 'Add_Receipt_form',
               'autocomplete' => 'off',
            );
        
           echo form_open('Inventory/AddReceipt/', $attributes); ?>
        		<div class="row">
					<div class="col-md-7 col-sm-12">
						<div class="row">
<div class="col-xs-12 col-md-5 col-sm-12 text-left pd-right-0">
        			<label for="contact_name">Received From<span class="text-danger">*</span></label>
        		</div>
        		<div class="form-group col-md-6 col-sm-12">
					<select name="from_to_type" id="from_to_type" required class="form-control">
						<option value="">----Select----</option>
					<?php foreach ($source_name as $value) { ?>
					
						<option value="<?php echo $value->LookupCode;?>">
							<?php echo $value->LookupValue; ?></option>
					<?php } ?>
				</select>
        		</div>
</div>
<div class="row">
				<div class="col-xs-12 col-md-5 col-sm-12 pd-right-0">
					<label for="source_name">Name<span class="text-danger">*</span></label>
				</div>
				<div class="col-md-6 col-sm-12 form-group" id="source_div">
					<select name="source_name" id="source_name" required class="form-control">
					<option value="">--Select--</option>
					</select>
				</div>
				<div class="col-md-6 col-sm-12 form-group" id="unrecognised_div">
					<input type="text" class="form-control" id="unrecognised" name="unrecognised" placeholder="Specify the Name" value="<?php if(isset($receipt_details[0]->unrecognised)){  echo $receipt_details[0]->unrecognised;}?>">
				</div>
			</div>
							<div class="row">
						<div class="col-xs-12 col-md-5 col-sm-12 text-left pd-right-0">
				<label for="item_name">Item Name <span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-md-6 col-sm-12" id="item_div" tabindex="1">
					<select name="item_name" id="item_name" required class="form-control">
						<option value="0" <?php if($this->input->post('item_name')==null) { echo 'selected';}?>>-----Select-----</option>
										<!-- <?php foreach ($receipt_items as $value) { ?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_name')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php if($value->type==2){echo $value->drug_name; } else{
													echo $value->drug_name." ".$value->strength." mg";
												} ?></option>
											<?php } ?> -->
										</select>
				</div>
			</div>
	<div class="row">
<div class="col-xs-12 col-md-5 col-sm-12 text-left pd-right-0">
        			<label for="indent_num">Indent Number <span class="text-danger" id="indent_mark">*</span></label>
        		</div>
        		<div class="form-group col-md-6 col-sm-12" tabindex="2">
					<select name="indent_num" id="indent_num" required class="form-control">
						<option value="">----Select----</option>
					<!-- <?php //foreach ($indent_num as $value) { ?>
						<option value="<?php //echo $value->inventory_id;?>">
							<?php //echo $value->indent_num; ?></option>
					<?php// } ?> -->
				</select>
        		</div>
        	</div>
        		<div class="row">
        		<div class="col-xs-12 col-md-5 col-sm-12 text-left pd-right-0">
				<label for="approved_quantity">Quantity Indented /Approved (screening tests/bottle)<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-md-6 col-sm-12" id="item_div" style="border: none;">
					<input type="text" class="form-control" name="approved_quantity" id="approved_quantity" required disabled="disabled" value="N/A" >
				</div>
			</div>
				
	<div class="row">
		<div class="col-xs-12 col-md-5 col-sm-12 text-left pd-right-0">
				<label for="issue_num">Issue Number <span class="text-danger" id="issue_mark">*</span></label>
				</div>
				<div class="col-md-6 col-sm-12 form-group">
				<input type="text" class="form-control" id="issue_num" name="issue_num" placeholder="Issue Number" required >
				</div>
	</div>
		<div class="row">
			<div class="col-xs-12 col-md-5 col-sm-12 pd-right-0">
        			<label for="batch_num">Batch Number <span class="text-danger">*</span></label>
        		</div>
        		<div class="col-md-6 col-sm-12 form-group">
					<input type="text" class="form-control" id="batch_num" name="batch_num" placeholder="Batch  Number" readonly required="required">
        		</div>


		</div>

			
					</div>
					<div class="col-md-5 col-sm-12">
						<div class="row">
							<div class="col-xs-12 col-md-6 col-sm-12 text-left" id="labelfor">
								<label for="quantity_dispatched">Quantity of Stock dispatched (screening tests/bottle)<span class="text-danger">*</span></label>
							</div>
							<div class="form-group col-md-6 col-sm-12">
								<input type="text" class="form-control" name="quantity_dispatched" id="quantity_dispatched" required >
							</div>

						</div>

					<div class="row">
						<div class="col-xs-12 col-md-6 col-sm-12">
							<label for="contact_name">Date Of Receipt<span class="text-danger">*</span></label>
						</div>
						<div class="col-md-6 col-sm-12 form-group">
							<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="Receipt Date" name="Entry_Date" id="Entry_Date" onchange="checkexpirydate();" required onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" value="<?php echo date('d-m-Y'); ?>" >
						</div>

					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6 col-sm-12 text-left" id="labelfor">
							<label for="quantity_received">Quantity of Stock received (screening tests/bottle)<span class="text-danger">*</span></label>
						</div>
						<div class="form-group col-md-6 col-sm-12" id="item_div">
							<input type="text" class="form-control" name="quantity_received" id="quantity_received" required >
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6 col-sm-12">
							<label for="contact_name">Expiry Date<span class="text-danger">*</span></label>
						</div>
						<div class="col-md-6 col-sm-12 form-group">
							<input type="text" class="form-control hasCal dateInpt" placeholder="Expiry Date" name="Expiry_Date" id="Expiry_Date"  required onkeydown="return false" onkeyup="return false" onchange="checkexpirydate();">
						</div>  	
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6 col-sm-12 text-left" id="labelfor">
							<label for="Acceptance_Status">Stock Acceptance/ Rejection Status<span class="text-danger">*</span></label>
						</div>
						<div class="form-group col-md-6 col-sm-12" id="item_div">
							<select name="Acceptance_Status" id="Acceptance_Status" required class="form-control">
								<?php foreach ($Acceptance_Status as $value) { ?>
									<option value="<?php echo $value->LookupCode;?>">
										<?php echo $value->LookupValue; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-6 col-sm-12">
								<label for="contact_name">Reason for rejection<span class="text-danger recject_label">*</span></label>
							</div>
							<div class="col-md-6 col-sm-12 form-group">
								<select name="rejection_reason" id="rejection_reason" required class="form-control">
									<option value="">----Select----</option>
									<?php foreach ($reason_for_rejection as $value) { ?>

										<option value="<?php echo $value->LookupCode;?>">
											<?php echo $value->LookupValue; ?></option>
										<?php } ?>
									</select>
								</div>  

							</div>
							<div class="row">
								<div class="col-xs-12 col-md-6 col-sm-12 text-left" id="labelfor">
									<label for="quantity_rejected">Quantity of Stock Rejected<span class="text-danger recject_label">*</span></label>
								</div>
								<div class="form-group col-md-6 col-sm-12" id="item_div">
									<input type="text" class="form-control" name="quantity_rejected" id="quantity_rejected" required>
								</div>	
							</div>
						</div>
					</div>
			<div class="row" id="info_row">
				<h5 style="color: #a94442;text-align: center;font: bold" id="info_text">If you cannot find the item or the batch number in the dropdown, make sure you have recorded receipt of this item or raised an indent or stock currently available is greater than 0.</h5>
			</div>		
  </div>
  <input type="hidden" name="rem_quantity" id="rem_quantity">
<input type="hidden" id="inventory_id" name="inventory_id">
<input type="hidden" id="is_manual" name="is_manual">
<input type="hidden" id="rem_values" name="rem_values" value="">
<input type="hidden" id="indent_num_val" name="indent_num_val" value="">
<input type="hidden" id="item_name_val" name="item_name_val" value="">
<input type="hidden" id="approved_quantity_val" name="approved_quantity_val" value="">
<input type="hidden" id="batch_num_val" name="batch_num_val" value="">
<input type="hidden" id="issue_num_val" name="issue_num_val" value="">
<input type="hidden" id="from_to_type_val" name="from_to_type_val" value="">
<input type="hidden" id="source_name_val" name="source_name_val" value="">
<input type="hidden" id="unrecognised_val" name="unrecognised_val" value="">
<input type="hidden" id="indent_date_val" name="indent_date_val" value="">
<input type="hidden" id="quantity_rejected_val" name="quantity_rejected_val" value="">
<input type="hidden" id="rejection_reason_val" name="rejection_reason_val" value="">
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="save" value="save" id="submit_btn">Save</button>
      </div>
    </div>
        <?php echo form_close(); ?>
  </div>
</div><!--  /.modal -->

<!-- add/edit facility contact modal ends-->
<div class="row" style="padding-top: 20px;padding-bottom: 20px;">
	<div class="col-lg-10 text-center">
			<div class="col-md-4 col-md-offset-6" style="padding-right: 15px;">
			<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 15px;padding-bottom: 15px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i>  Add New Stock Receipt
</a>
		</div>
	</div>
</div>
<div class="row main-div" style="min-height:400px;">
	<div class="col-lg-2 col-xl-2 col-md-3 col-sm-12 col-xs-12 left_nav_bar">
			<?php //include("left_nav_bar.php");
			$this->load->view("inventory/components/left_nav_bar"); ?>
		</div>

		<div class="col-lg-10 col-xl-10 col-md-10 col-sm-12 col-xs-12 set_margin">
			<div class="panel panel-default" id="Record_receipt_panel" style="padding-bottom: 55px;">
				<div class="panel-heading" style=";background: #333;">
					<h4 class="text-center" style="color: white;background: #333" id="Record_form_head">Stock Receipt</h4>
				</div>
				<div class="row" style="padding-top: 32px;">
					<?php 
					$tr_msg= $this->session->flashdata('tr_msg');
					$er_msg= $this->session->flashdata('er_msg');
					if(!empty($tr_msg)){  ?>
						

						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						<?php } else if(!empty($er_msg)){ ?>
							<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
								
							<?php } ?>

							<!-- <div class="col-md-3 pull-right">
										<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 10px;padding-bottom: 10px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i>  Add New Stock Receipt
							</a>
									</div> -->
						</div>
				
						<?php
						$attributes = array(
							'id' => 'Receipt_filter_form',
							'name' => 'Receipt_filter_form',
							'autocomplete' => 'false',
						);
  //pr($items);
  //print_r($receipt_details);

						echo form_open('', $attributes); ?>
							<div class="row" style="padding-left: 10px;">
							<div class="col-xs-3 col-md-2">
								<label for="item_type">Item Name <span class="text-danger">*</span></label>
								<div class="form-group">
									<select name="item_type" id="item_type" required class="form-control">
										<option value="drugkit" <?php if($this->input->post('item_type')=='drugkit') { echo 'selected';}?>>All Kits and Drugs</option>
										<option value="Kit"<?php if($this->input->post('item_type')=='Kit') { echo 'selected';}?>>All Screening Test Kits</option>
										<option value="drug"<?php if($this->input->post('item_type')=='drug') { echo 'selected';}?>>All Drugs</option>
										
										<?php foreach ($items as $value) { ?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php if($value->type==2){echo $value->drug_name; } else{
													echo $value->drug_name." ".$value->strength." mg";
												} ?></option>
											<?php } ?>
											<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
										</select>
									</div>
								</div>
								
								
						
								<div class="col-md-2 col-sm-12 form-group">
									<label for="year">Financial Year <span class="text-danger">*</span></label>
									<select name="year" id="year" required class="form-control" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2018', date('Y'));
										$flag=0;
										foreach($dates as $date){

									if (date('m', strtotime($date)) < 4) {//Upto April
										$year = ($date-1) . '-' . $date;
									} else {//After April
										$year = $date . '-' . ($date + 1);
									}
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									else if($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
									
								}
								?>
							</select>
						</div>
								<div class="col-md-2 col-sm-12">
									<label for="">From Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')){ echo $this->input->post('startdate');} else{ echo date('01-04-Y');}  ?>" required style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 col-sm-12">
									<label for="">To Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')){echo $this->input->post('enddate');}else{echo timeStampShow(date('Y-m-d'));} ?>" required  style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 btn-width pull-right" style="padding-right: 30px;">
									<label for="">&nbsp;</label>
									<button class="btn btn-block" style="line-height: 1.2; font-weight: 600;background-color: #f4860c;border-color: #f4860c;color: white;">SEARCH</button>
								</div>

								<?php echo form_close(); ?>
							</div>
							
						</div>

						
							</div>
							<div class="row" style="padding-right: 30px;width:99%">
								<span class="pull-right">
								<dl>
									<dt class="red"></dt>
								    <dd>Incoming Stock Notification</dd>

								    <dt class="yellow"></dt>
								    <dd>Record Manual Entry</dd>

								     <dt class="green"></dt>
								    <dd>Stock Receipts Confirmed and Recorded</dd>
									

								    <dt class="orange"></dt>
								    <dd>Mismatch</dd>

								    
								</dl>
									</span>						
							</div>
							<div class="col-md-12 table-responsive">
								<table class="table table-striped table-bordered table-hover" id="table_inventory_list">

							
							<thead>
							<tr style="background-color: #085786; font-family: 'Source Sans Pro';color: white;font-size: 13px;">
									<th style='width: 4% !important;'>St.</th>
									<th>Item</th>
									<th>Indent Number</th>
									<th>Quantity Indented <br> | Approved <br>(screening tests/bottle)</th>
									<th>Received From</th>
									<th nowrap="nowrap">Issue No</th>
									<th>Batch Number</th>
									<th>Quantity of  <br>Stock dispatched  <br>(screening tests/bottle)</th>
									<th>Date of receipt</th>
									<th>Quantity of <br>Stock received<br> (screening tests/bottle)</th>
									<th>Expiry Date</th>
									<th>Stock Acceptance/  <br>Rejection Status</th>
									<th>Reason&nbsp;for rejection</th>
									<th>Quantity&nbsp;of  <br>Stock Rejected</th>
									 <th>Commands</th> 
								</tr>
							</thead>
							<tbody>
	
				<?php 
				$item_id=array();
				foreach ($items as $key => $value) {
					foreach ($inventory_detail_all as $value1) {
						if($value->id_mst_drugs==$value1->drug_name){
							$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
							$item_id[$key]['drug_name']= $value->drug_name;
						}
					}
					
				}
				$filtered_arr=array_unique($item_id,SORT_REGULAR);
				//pr($filtered_arr);exit();
				//pr($inventory_detail_all);exit();
				?>
				<!-- <table class="table table-striped table-bordered table-hover">
					<tbody> -->
						<?php if(!empty($inventory_detail_all)){
							foreach ($filtered_arr as $key=>$value_temp) { ?>
								<tr>
									<th colspan="14" class="info"style="border: none;background-color: #333;font-size: 16px;color:#fff;text-align: left;"><?php echo $value_temp['drug_name']; ?></th>
									<th class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus" aria-hidden="true" id="<?php echo 'head'.$value_temp['id_mst_drugs']; ?>"></i></th></tr>
									<?php foreach ($inventory_detail_all as  $value) { 
										if($value_temp['id_mst_drugs']==$value->drug_name){
											?>
											<tr class="<?php echo $value_temp['id_mst_drugs']; ?>">
												<td class="<?php if ($value->relocation_status==""){
														echo 'yellow_status';
												}
												elseif ($value->relocation_status==5){
														echo 'orange_status';
												}
												elseif ($value->relocation_status==3){
														echo 'green_status';
												}
												elseif(($value->relocation_status==1 || $value->relocation_status==2) &&($value->is_notification==1 || $value->is_notification=='1')){
														echo 'red_status';
												}
													
												?>" style='width: 4% !important;'><?php if(($value->relocation_status==1 || $value->relocation_status==2) &&($value->is_notification==0 || $value->is_notification=='0')){ ?><img style="width: 40px;" src="<?php echo site_url(); ?>/common_libs/images/new_alert3.png" alt="new alert"><?php }?></td>
												<td class="item">
													<?php if($value->type==1){echo $value_temp['drug_name']." ".$value->strength." mg";} elseif($value->type==2){echo $value_temp['drug_name'];} ?>
												</td>
												<td>
													<?php echo $value->indent_num; ?>
												</td>
												<td>
													<?php echo (!empty($value->show_quantity)>0 ? $value->show_quantity : '0') ." | ". (!empty($value->approved_quantity)>0 ? $value->approved_quantity : '0'); ?>
												</td>
												<td>
													<?php 
													if ($value->facility_short_name==NULL) {
													
													if ($value->from_to_type==3){
														echo "Unregistered Source";
													}
													else if ($value->from_to_type==4){
														echo "Third Party";
													}
													elseif ($value->from_to_type==1) {
														if ($value->source_name==996) {
															echo "abc";
														}
														if ($value->source_name==997) {
															echo "abcd";
														}
														if ($value->source_name==998) {
															echo "abcde";
														}
														
													}
													}
													else{
														echo $value->facility_short_name;
													}

													?>
												</td>
												<td>
													<?php echo $value->issue_num; ?>
												</td>
												<td>
													<?php echo $value->batch_num; ?>
												</td>
												<td>
													<?php echo $value->quantity_dispatched; ?>
												</td>
												<td nowrap>
													<?php echo timeStampShow($value->Entry_Date); ?>
												</td>
												<td>
													<?php echo $value->quantity_received; ?>
												</td>
												<!-- <td>
													<?php if ($value->quantity_screening_tests==0) {
														echo "N/A";
													}else{
														echo $value->quantity_screening_tests;
													}?>
												</td> -->
												<td nowrap>
													<?php echo timeStampShow($value->Expiry_Date); ?>
												</td>
												<td>

													<?php foreach ($Acceptance_Status as $Acceptance) {
														# code...
													 if($value->Acceptance_Status==$Acceptance->LookupCode) { echo $Acceptance->LookupValue;}} ?>
												</td>
												<td>
													<!-- <?php echo $value->rejection_reason;
													 ?> -->
													 <?php if($value->rejection_reason==0 || $value->rejection_reason=="" || $value->rejection_reason==NULL){
													 	if ($value->relocation_status<=2 && $value->status=='n') {
													 		 echo "";
													 	}
													 	else{
													 		 echo "N/A";
													 	}
													}else{
													 	foreach ($reason_for_rejection as $rejection) {
														# code...
													 if($value->rejection_reason==$rejection->LookupCode) { echo $rejection->LookupValue;}}
													 } ?>
												</td>
												<td>
													<?php if ($value->relocation_status<=2 && $value->status=='n') {
													 		 echo "";
													 	}
													 	else{
													 	
													 	echo !empty($value->quantity_rejected)>0 ? $value->quantity_rejected :"N/A" ; }?>
												</td>
												<!-- <td>
													<?php //echo !empty($value->rejection_reason)>0 ? ; ?>
												</td> -->
												 <td class="text-center">
												<?php if ( $value->relocation_status<=2 && $value->status=='n') {?>
													<a href="" style="padding : 4px;" title="Edit" data-toggle="modal" data-target="#myModal1" onclick='get_receipt_data(<?php echo $value->inventory_id ?>,"<?php echo $value->status; ?>");'><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;" ></span></a>
													<?php } ?>
													<!-- <?php //if ($value->status=='m') {?>
													<a href="<?php //echo site_url()."/inventory/delete_receipt/".$value->inventory_id."/R";?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
													<?php //} ?> -->
												</td>
												
											</tr>
										<?php } }}}else{ ?>
											<tr >
												<td colspan="14" class="text-center">
													NO RECORDS FOUND  BETWEEN SELECTED DATES.
												</td>

											</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
							<br>
						</div>
						<div class="overlay">
							<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
						</div>

						<!-- Data Table -->
						<script type="text/javascript">

						
	$('#from_to_type').ready(function(){ 
        fill_source_name();        
});
    $('#from_to_type').change(function(){ 
        fill_source_name();        
});
 $("#modal_close,#close_icon").on('click ',function(evt){
	 empty_all_input();
	$('#Add_Receipt_form').find("input[type=text],textarea,select").val("");
	$('.modal-title').html('Add Stock Receipt');

});
$("#myModal1").on('hide.bs.modal', function(){
   // alert('The modal is about to be hidden.');
   $('.modal-title').html('Add Stock Receipt');
 $('#Add_Receipt_form').find("input[type=text],textarea").val("");
 empty_all_input();
  $('#Entry_Date').val('')
  });

function empty_all_input(){
	$("#rem_quantity").val('');
	$("#inventory_id").val('');
	$("#rem_values").val('');
	$("#indent_num_val").val('');
	$("#batch_num_val").val('');
	$("#issue_num_val").val('');
	$("#from_to_type_val").val('');
	$("#source_name_val").val('');
	$("#unrecognised_val").val('');
	$("#indent_date_val").val('');
	$("#indent_num").val('');
	$("#from_to_type").val('');
	$('#Acceptance_Status').removeAttr('readonly','readonly');
	$("#Acceptance_Status").val(1);
	$("#Acceptance_Status").trigger("change");
	$("#quantity_rejected_val").val('');
    $("#rejection_reason_val").val('');
    $("#is_manual").val('');
    $("#item_name_val").val('');
    $("#item_name").val('');
    $("#source_name").val('');
    $('#info_row').show();
    $('#info_text').show();

    $('#item_name,#indent_num,#issue_num,#batch_num,#quantity_dispatched,#from_to_type,#source_name,#request_date').removeAttr('readonly','readonly');


}
$("#item_name,#from_to_type").on('keypress change',function(evt){

	genrate_batch_issue(0);
});
$("#indent_num").on('change',function(evt){
if ($('#indent_num').val()=='') {
	$('#approved_quantity').val('N/A');
	}
	else{
	var quantity=($('#approved_quantity_val').val()).toString();
	var quantity_arr=quantity.split(',');
	var index=($("#indent_num")[0].selectedIndex);
	
	var val=quantity_arr[index-1];
	$('#approved_quantity').val(val);
	}
	
});
$("#Entry_Date").on('change',function(evt){

	genrate_batch_issue(1);
});
function genrate_batch_issue(is_date_clicked){
	var item=$('#item_name').val();
	var issue_num=$('#issue_num_val').val();

	var EntryDate1       =$('#Entry_Date').val();
	var parts            = EntryDate1.split('-');
	var date             =parts[2] + parts[1] +  parts[0];
	var get_entry_date   =new Date(parts[1] +"-"+parts[0]+"-"+parts[2]);
	var batch_num_val    =$('#batch_num_val').val();
	

	<?php foreach ($items as $value){ ?>
		<?php
		if ($value->type==1) {
			$strength=$value->strength;
		}
		else{
			$strength='';
		}
		?>

		if (item==<?php echo $value->id_mst_drug_strength; ?>) {
			if (is_date_clicked==0) {
				if ($('#from_to_type').val()==1 || $('#from_to_type').val()==2) {  


					if (issue_num!="") {
						//alert(issue_num);	
						abb_arr=issue_num.split("/");
						abb_val=abb_arr[1];
						if (abb_val!='<?php echo $value->drug_abb.$strength ?>') {
							var issue='SI/'+'<?php echo $value->drug_abb.$strength; ?>/';
							$("#issue_num").val(issue);
						}
					}
					if(issue_num=="" || issue_num==null || $("#issue_num").val()=="") {
						
						var issue='SI/'+'<?php echo $value->drug_abb.$strength ?>/';
						$("#issue_num").val(issue);
					}
					if (batch_num_val!="" && $('#from_to_type_val').val()==1) {

						$("#batch_num").val(batch_num_val);
					}
					else if (batch_num_val!="" && $('#from_to_type_val').val()==2) {
						$("#batch_num").val(batch_num_val);
						
					}
					else{
						$("#batch_num").val('');
					}
				}

			}
			if ($('#from_to_type').val()==3 || $('#from_to_type').val()==4) {
					if (batch_num_val    !="") {
						var batchnumparts    = batch_num_val.split('/');
						var entry_date_year  =batchnumparts[4].substring(0, 4);
						var entry_date_month =batchnumparts[4].substring(4, 6);
						var entry_date_day   =batchnumparts[4].substring(6, 8);
						entry_date_val       =new Date(entry_date_month+"-"+entry_date_day+"-"+entry_date_year);
					}	
				if (is_date_clicked==0) {
					if ($("#issue_num_val").val()!=null && $('#from_to_type_val').val()==3) {

						$("#issue_num").val($("#issue_num_val").val());
					}
					else if ($("#issue_num_val").val()!=null && $('#from_to_type_val').val()==4) {
						$("#issue_num").val($("#issue_num_val").val());
					}
					else{
						$("#issue_num").val('');
					}
				}
				var batch_num='BN/';
	$.ajax({
	type: 'GET',
   url: "<?php echo base_url(); ?>Inventory/get_batch_sequence", // <-- properly quote this line
   cache: false,
   async : true,
   data: {drug_name:item, Entry_Date: EntryDate1, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'}, 
   success: function(data) {
   	var returned = JSON.parse(data);
   	console.log(returned);
     //var issue_count = returned.item_sequence.map(function(e) {return e.issue_count;});
     var batch_count = returned.item_sequence.map(function(e) {return e.batch_count;});
   //issue_num=issue_num+'<?php echo $value->drug_abb."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(issue_count)+1);
    //$('#issue_num').val(batch_count);   



    if (batch_num_val!="" && ('<?php echo $value->drug_abb.$strength; ?>'==batchnumparts[2])) {
    	if (get_entry_date<entry_date_val && ('<?php echo $value->drug_abb.$strength; ?>'==batchnumparts[2]) || (get_entry_date>entry_date_val && ('<?php echo $value->drug_abb.$strength; ?>'==batchnumparts[2]))) {
    		if ($('#from_to_type').val()==3) {
    			batch_num+='USLP/';
    			batch_num=batch_num+'<?php echo $value->drug_abb.$strength."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(batch_count)+1);
    		}

    		else if ($('#from_to_type').val()==4) {
    			batch_num+='TP/';
    			batch_num=batch_num+'<?php echo $value->drug_abb.$strength."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(batch_count)+1);
    		}
    		
    	}
    	else{
    		if ($('#from_to_type').val()==3) {
    			batchnumparts[1]="USLP";
    			batch_num=batchnumparts.join("/");
    			
    		}
    		else if($('#from_to_type').val()==4) {
    			batchnumparts[1]="TP";
    			batch_num=batchnumparts.join("/");
    			
    		}
    		
    	}
    }
    else if (batch_num_val!="" && ('<?php echo $value->drug_abb; ?>'!=batchnumparts[2])){
    	if ($('#from_to_type').val()==3) {
    		batch_num+='USLP/';
    		batch_num=batch_num+'<?php echo $value->drug_abb.$strength."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(batch_count)+1);
    	}

    	else if ($('#from_to_type').val()==4) {
    		batch_num+='TP/';
    		batch_num=batch_num+'<?php echo $value->drug_abb.$strength."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(batch_count)+1);
    	}
    }
    else{
    	if ($('#from_to_type').val()==3) {
    		batch_num+='USLP/';
    		batch_num=batch_num+'<?php echo $value->drug_abb.$strength."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(batch_count)+1);
    	}

    	else if ($('#from_to_type').val()==4) {
    		batch_num+='TP/';
    		batch_num=batch_num+'<?php echo $value->drug_abb.$strength."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(batch_count)+1);
    	}
    	
    }
    //alert(batch_num);
    $('#batch_num').val(batch_num); 
    
}
});

			}

		}	

	<?php } ?>
}
function fill_source_name(){
var id=$('#from_to_type').val();

if(id!=3 || id!=4){
$('#source_div').show();
            	$('#unrecognised_div').hide();
            	$('#source_name').attr('required','required');
            	$('#issue_nume').attr('required','required');
            	$('#indent_num,#quantity_dispatched,#Acceptance_Status').removeAttr('disabled');
            	$('#issue_mark').show();
            	$('#info_text').show();
            	$('#indent_mark').show();

    			$('#unrecognised').removeAttr('required');
}
 /* var fid=<?php if(isset($receipt_details[0]->source_name)){ echo $receipt_details[0]->source_name;}else{echo '0';}?>; */     
				if(id==1){
					var opt='';
					opt += '<option value="996">abc</option>';
					opt += '<option value="997">abcd</option>';
					opt+=  '<option value="998">abcde</option>';
					$('#source_name').html(opt);
					$('#source_name').val($('#source_name_val').val());
					$('#batch_num').removeAttr('readonly');
				}
                else if(id==2){
                $.ajax({
                    url : "<?php echo site_url('inventory/get_facilities');?>",
                    method : "GET",
                   data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
                        //console.log(returned);
        				var id_mstfacility = returned.id_mstfacility.map(function(e) {
   						return e.id_mstfacility;
						});
						var facility_short_name = returned.id_mstfacility.map(function(e) {
   						return e.facility_short_name;
						});
                        var i;
                        var select=''
                        var opt='';
                        opt+='<option value="">----Select----</option>'
                        for(i=0; i<id_mstfacility.length; i++){
                        	
                        		select='';
                        
                            opt += '<option value="'+id_mstfacility[i]+'"'+select+'>'+facility_short_name[i]+'</option>';
                        }
                        //console.log(opt);
                        $('#source_name').html(opt);
 						$('#source_name').val($('#source_name_val').val());
                    }
                });
                $('#batch_num').removeAttr('readonly');
            }else if(id==3 || id==4){

            	$('#source_div').hide();
            	$('#unrecognised_div').show();
            	$('#source_name').removeAttr('required');
            	$('#issue_num').removeAttr('required');
            	$('#issue_mark').hide();
    			$('#unrecognised').attr('required','required');
    			$('#batch_num').attr('readonly','readonly');
    			$('#indent_num,#quantity_dispatched,#Acceptance_Status').attr('disabled','true');
    			$('#indent_num').attr('disabled','true');
    			$('#indent_mark').hide();
    			$('#info_text').hide();
    			$('#unrecognised').val($('#unrecognised_val').val());
    		
            }
            else if(id==''){
            	$('#batch_num').attr('readonly','readonly');
            }
              //$('#batch_num').val($('#batch_num_val').val());
         if ($('#is_manual').val()=='n') {
         	$('#batch_num').attr('readonly','readonly');
         }
         else{
         	if(id==3 || id==4){
         	$('#batch_num').attr('readonly','readonly');
         }
         else{
         	 $('#batch_num').removeAttr('readonly');
         }
         }
         if (id!=null || id!='') {
         	 get_rem_item_name();
         }
        

}   
$('#Acceptance_Status').ready(function(){
checkrejction();
}); 
$('#Acceptance_Status').change(function(){
checkrejction();
});
function checkrejction(){
	var idval = $('#Acceptance_Status').val();
//alert(idval);
if(idval=='1')
{
	$('#rejection_reason').removeAttr('required');
	$('#rejection_reason').attr('disabled','disabled');
	$('#quantity_rejected').attr('disabled','disabled');
	$('#quantity_rejected').val('N/A');
	$('#rejection_reason').val('');
	$('#quantity_rejected').removeAttr('required');
	$('.recject_label').hide();
	$('#rejection_reason option[value=""]').text("N/A");
}
else if(idval=='2' || idval=='3')
{
	
	$('#rejection_reason').attr('required','required');
	$('#rejection_reason').removeAttr('disabled','disabled');
	$('#quantity_rejected').attr('required','required');
	$('#quantity_rejected').removeAttr('disabled','disabled');
	if ($('#quantity_rejected_val').val()!='' || $('#quantity_rejected_val').val()!=null || $('#quantity_rejected_val').val()!=0) {
		//alert('hi');
		$('#quantity_rejected').val($('#quantity_rejected_val').val());
		if ($('#rejection_reason_val').val()!=null) {
		$('#rejection_reason').val($('#rejection_reason_val').val());
		}
		else{
			$('#rejection_reason').val('');
			
		}

		$('#rejection_reason option[value=""]').text("----Select----");
	}

	else{
		$('#quantity_rejected').val('');
		$('#rejection_reason').val('');
		$('#rejection_reason option[value=""]').text("----Select----");
	}
	
	$('.recject_label').show();
	//alert($('#rejection_reason_val').val());
	

}
}
						
			
							$(document).ready(function(){
								$(".glyphicon-trash").click(function(e){
									var ans = confirm("Are you sure you want to delete?");
									if(!ans)
									{
										e.preventDefault();
									}
								});
								<?php foreach ($filtered_arr as $key=>$value_temp) {
									$elemnt_id= ".".$value_temp['id_mst_drugs'];?> 
									$("<?php echo $elemnt_id; ?>").show();
									$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").click(function(){
										if($("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").hasClass('fa-minus')){
											$("<?php echo $elemnt_id; ?>").hide();
											$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-minus').addClass("fa-plus");
										}
										else{
											$("<?php echo $elemnt_id; ?>").show();
											$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-plus').addClass("fa-minus");
										}
										
									});
									$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").css('cursor', 'pointer');
								<?php }?>
							});
function checkexpirydate()
	{
		var Entry_Date = $('#Entry_Date').datepicker('getDate');
		var Expiry_Date = $('#Expiry_Date').datepicker('getDate');
   		//alert(Receipt_Date+'/'+Expiry_Date);
        if ( Entry_Date > Expiry_Date && Expiry_Date!=null) { 
        	/*$("#modal_header").text("Expiry date cannot be before Receipt Date");
			$("#modal_text").text("Please check dates");
			$("#Expiry_Date" ).val('');
			$("#multipurpose_modal").modal("show");*/
			/*return false;*/
			$('#Acceptance_Status').val(2);
			checkrejction();
			$('#Acceptance_Status').attr('readonly','readonly');
			
		
        }
 		else{
 			checkrejction();
 			$('#Acceptance_Status').removeAttr('readonly','readonly');
 			 return true;
 		}
       
    }
$('#issue_num,#batch_num').keyup(function (e) {
   /* var regex = new RegExp("^[a-zA-Z0-9{2}]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test($.trim(str))) {
        return true;
    }
    else{
    	e.preventDefault();
    return false;
    }*/
     $(this).val($(this).val().toUpperCase());

});
$('#unrecognised').keypress(function (e) {

   var regex = new RegExp("^[a-zA-Z]+$");
			var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (regex.test(str)) {
				return true;
			}
			else
			{
			e.preventDefault();
			return false;
			}

});
/*$('#batch_num').on('keypress',function(e){
	 var regex = new RegExp("^[a-zA-Z0-9 ]+$");
			var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (regex.test(str)) {
				return true;
			}
			else
			{
			e.preventDefault();
			return false;
			}

});*/

$("#quantity_rejected,#quantity_received,#quantity_dispatched,#approved_quantity").on('keypress keyup',function(evt){

								var charCode = (evt.which) ? evt.which : event.keyCode
								
								if ((charCode > 31 && (charCode < 48 || charCode > 57)) || parseInt($(this).val()+ evt.key)>10000){
									evt.preventDefault();
									return false;
								}
								
								if($(this).val() <=0 && $(this).val()!='')
								{
									$("#modal_header").html('Quantity must be greater than 0');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$(this).val('');
									return false;
								}
								
							});

$("#quantity_dispatched").on('keyup',function(evt){
								var error_css = {"border":"1px solid red"};
								var from_to_type=$('#from_to_type').val();
								
								if($('#item_name').val().trim() == ""){
		
									$("#modal_header").text("Please Select Item");
									$(this).val('');
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#item_name").css(error_css);
									$("#item_name").focus();
									evt.preventDefault();
									return false;
								}
								else if (from_to_type==1 || from_to_type==2) {
								 if($('#indent_num').val().trim() == ""){

									$("#modal_header").text("Please Select Indent Number");
									$(this).val('');
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#indent_num").css(error_css);
									$("#indent_num").focus();
									evt.preventDefault();
									return false;
								}
							}
								/*else{
								var index=($("#indent_num")[0].selectedIndex);
								var indent_values=$('#rem_quantity').val().toString();
								var indent_arr=indent_values.split(',');
								var indent_quantity=parseInt(indent_arr[index-1]);
								if(parseInt(($(this).val()))>indent_quantity)
								{
									$("#modal_header").html('Dispatched Quantity must be less than Remaining indent Quantity Remaining : '+indent_quantity);
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$(this).val('');
									return false;
								}
								}*/

								if(parseInt(($(this).val()) < parseInt($('#quantity_received').val()) && $('#quantity_received').val()!='') || (parseInt($(this).val())< parseInt($('#quantity_rejected').val()) && $('#quantity_rejected').val()!=''))
								{
									$("#modal_header").html('Dispatched Quantity must be greater than Received Quantity or Rejected Quantity');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$('#quantity_received').val('');
									if($("#Acceptance_Status").val()!=1){
										$('#quantity_rejected').val('');
									}
									
									return false;
								}
								
								
							});
$("#quantity_received").on('keyup',function(evt){
var error_css = {"border":"1px solid red"};
if ($('#from_to_type').val()==1 || $('#from_to_type').val()==2) {
								if ($('#quantity_dispatched').val()=="") {
									$("#modal_header").html('Dispatched Quantity must be greater than Received Quantity');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$("#quantity_dispatched").css(error_css);
									$("#quantity_dispatched").focus();
									$('#quantity_received').val('');
								}
								
								else if(parseInt($(this).val()) > parseInt($('#quantity_dispatched').val()))
								{
									$("#modal_header").html('Dispatched Quantity must be greater than Received Quantity');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$('#quantity_received').val('');
									
									return false;
								}
								 if(parseInt($(this).val()) < parseInt($('#quantity_rejected').val()))
								{
									$("#modal_header").html('Rejected Quantity cannot be greater than Quantity Received');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
								
									if($("#Acceptance_Status").val()!=1){
										$('#quantity_rejected').val('');
									}
									
									return false;
								}

								}
								
							});
$("#quantity_rejected").on('keyup',function(evt){
								
								if(parseInt($(this).val()) > parseInt($('#quantity_dispatched').val()))
								{
									$("#modal_header").html('Rejected Quantity cannot be greater than Quantity Dispatched');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$('#quantity_received').val('');
									
									return false;
								}
								if(parseInt($(this).val()) > parseInt($('#quantity_received').val()))
								{
									$("#modal_header").html('Rejected Quantity cannot be greater than Quantity Received');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
								
									if($("#Acceptance_Status").val()!=1){
										$('#quantity_rejected').val('');
									}
									
									return false;
								}
								
								
							});
/*$('#indent_num').change(function () {
	

	
});*/
$('#item_name').change(function () {
	if ($('#from_to_type').val()!=3 || $('#item_name').val()!=4) {
		get_indent_num();
	}
	

	
});
function get_indent_num(){
	var item=$('#item_name').val();
	var is_manual=$('#is_manual').val();
	$('#item_name option[value=""]').text("----Select----");
	var inventoryid=$('#inventory_id').val();
	var type_id=[];
	if(item!=null){
                $.ajax({
                    url : "<?php echo site_url('inventory/get_indent_num');?>",
                    method : "GET",
                    data : {item_name: item,is_edit: is_manual,inventory_id:inventoryid, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                         console.log(data);
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
        				var html = returned.get_indent_num.map(function(e) {
   						return e.indent_num;
						});
						var rem = returned.get_indent_num.map(function(e) {
   						return e.rem;
						});
						var approved_quantity = returned.get_indent_num.map(function(e) {
   						return e.approved_quantity;
						});

						 $('#rem_quantity').val(rem);
						
						 var indent_date_val = returned.get_indent_num.map(function(e) {
   						return e.indent_date;
						});
						  $('#indent_date_val').val(indent_date_val);
						  $('#approved_quantity_val').val(approved_quantity);

						/// alert(approved_quantity);
						//console.log(returned);
						/*type_id = returned.batch_nums.map(function(e) {
   						return e.type;
						});
						//alert(To_Date);

						if(type_id == 1){
			$('#quantityRemlbl').html('');
			$('#quantityRemlbl').html('Quantity of bottles available');
		}
		 if(type_id == 2){
			//console.log('hi');
			$('#quantityRemlbl').html('');
			$('#quantityRemlbl').html('Number of screening tests available');
		}*/
                        var i;
                        var opt='';
                        opt +='<option selected value="">-----Select-----</option>';
                        for(i=0; i<html.length; i++){

								
								select='';
                        
                        	opt += '<option value="'+html[i]+'"'+select+'>'+html[i]+'</option>'; 	
                   
                            
                        }
                        //alert(opt);
                        $('#indent_num').html(opt);
                    $('#indent_num').val($('#indent_num_val').val());
                    $('#indent_num').trigger('change');
 						
                    }
                });
           }
                return false;
}	

 $('#item_name,#quantity_rejected,#quantity_received,#quantity_dispatched,#approved_quantity,#rejection_reason,#from_to_type,#unrecognised,#source_name,#indent_num,#batch_num,#Expiry_Date,#Entry_Date').on('change click keypress',function(e){
						
	var error_css = {"border":"1px solid #c7c5c5"};
			$(this).css(error_css);
	return true;

});
$('#submit_btn').click(function(e){
		var error_css = {"border":"1px solid red"};
//alert($('#item_name').has('option').length);
		/*if($('#item_name').val() == "" && $('#item_name').has('option').length ==1) {
				$("#modal_header").html('If item is not showing then please genrate indent request for this item');
									$("#modal_text").html('Please genrate indent request');
									$("#multipurpose_modal").modal("show");
									return false;
		}*/
		//alert($('#item_name').val());
		if($('#item_name').val() == null){

			$("#item_name").css(error_css);
			$("#item_name").focus();
			e.preventDefault();
			return false;
		}

		 
		/* if($('#approved_quantity').val().trim()=="" || parseInt($('#approved_quantity').val())==0){

			$("#approved_quantity").css(error_css);
			$("#approved_quantity").focus();
			e.preventDefault();
			return false;
		}*/
 if($('#from_to_type').val().trim() == ""){

			$("#from_to_type").css(error_css);
			$("#from_to_type").focus();
			e.preventDefault();
			return false;
		}
 if($('#from_to_type').val().trim() == "1" || $('#from_to_type').val().trim() == "2"){
	if($('#source_name').val().trim() == ""){

			$("#source_name").css(error_css);
			$("#source_name").focus();
			e.preventDefault();
			return false;
		}
		var issue_arr=$('#issue_num').val().split("/");
		if (issue_arr.length<5) {
			$("#modal_header").html('Ask issuing facility or warehouse for complete issue number');
									$("#modal_text").html('Please fill in Correct Issue Number');
									$("#multipurpose_modal").modal("show");
									return false;
		}
		if($('#issue_num').val().trim() == ""){

			$("#issue_num").css(error_css);
			$("#issue_num").focus();
			e.preventDefault();
			return false;
		}
		if($('#indent_num').val() == ""){

			$("#indent_num").css(error_css);
			$("#indent_num").focus();
			e.preventDefault();
			return false;
		}
		if($('#quantity_dispatched').val().trim()=="" || parseInt($('#quantity_dispatched').val())==0){

			$("#quantity_dispatched").css(error_css);
			$("#quantity_dispatched").focus();
			e.preventDefault();
			return false;
		}
		if($('#Acceptance_Status').val().trim() == "2" || $('#Acceptance_Status').val() != "1"){
		 if($('#quantity_rejected').val().trim()=="" || parseInt($('#quantity_rejected').val())==0){

			$("#quantity_rejected").css(error_css);
			$("#quantity_rejected").focus();
			e.preventDefault();
			return false;
		}
	}

}
 if($('#from_to_type').val().trim() == "3" || $('#from_to_type').val().trim() == "4"){
	
	if($('#unrecognised').val().trim() == ""){

			$("#unrecognised").css(error_css);
			$("#unrecognised").focus();
			e.preventDefault();
			return false;
		}
}

		 
		 if($('#batch_num').val().trim() == ""){

			$("#batch_num").css(error_css);
			$("#batch_num").focus();
			e.preventDefault();
			return false;
		}

		
			
 if($('#Entry_Date').val().trim()==""){

			$("#Entry_Date").css(error_css);
			$("#Entry_Date").focus();
			e.preventDefault();
			return false;
		}
 
 if($('#Expiry_Date').val().trim()==""){

			$("#Expiry_Date").css(error_css);
			$("#Expiry_Date").focus();
			e.preventDefault();
			return false;
		}	
		 if($('#quantity_received').val().trim()=="" || parseInt($('#quantity_received').val())==0){

			$("#quantity_received").css(error_css);
			$("#quantity_received").focus();
			e.preventDefault();
			return false;
		}	
		

return true;

	});
$("#Add_Receipt_form").submit(function() {
            $(this).submit(function() {
                return false;
            });
            return true;
        });
			
function get_receipt_data(inventory_id,status){
$('#inventory_id').val(inventory_id);
$('#is_manual').val(status);
$('.modal-title').html('Update Receipt');
//alert($('#inventory_id').val());
$.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Inventory/edit_inventory_data", // <-- properly quote this line
    cache: false,
    async : true,
    data: {inventory_id:inventory_id,inv_remark:status, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'}, 
    success: function(data) {
        var returned = JSON.parse(data);
        console.log(returned);
    var id_mst_drugs = returned.inventory_details.map(function(e) { return e.id_mst_drugs; });

     var indent_num = returned.inventory_details.map(function(e) { return e.indent_num; });
      var issue_num = returned.inventory_details.map(function(e) { return e.issue_num; });
     var quantity_dispatched = returned.inventory_details.map(function(e) { return e.quantity_dispatched; });
      var quantity_received = returned.inventory_details.map(function(e) { return e.quantity_received; });

        var quantity_rejected = returned.inventory_details.map(function(e) { return e.quantity_rejected; });
 var quantity = returned.inventory_details.map(function(e) { return e.quantity; });
 var  approved_quantity = returned.inventory_details.map(function(e) { return e.approved_quantity; });
 if (approved_quantity=='' || approved_quantity==0) {
 	approved_quantity=quantity;

 }


       var from_to_type = returned.inventory_details.map(function(e) { return e.from_to_type; });

      

      var Entry_Date = returned.inventory_details.map(function(e) { return e.Entry_Date; });

        var Expiry_Date = returned.inventory_details.map(function(e) { return e.Expiry_Date; });

       var Acceptance_Status = returned.inventory_details.map(function(e) { return e.Acceptance_Status || 1; });

       var batch_num = returned.inventory_details.map(function(e) { return e.batch_num; });

  
       if (status=='n') {
       	  var source_name = returned.inventory_details.map(function(e) { return e.transfer_to; });
       }
     else{
     	  var source_name = returned.inventory_details.map(function(e) { return e.source_name; });
     	 
     }

       var unrecognised = returned.inventory_details.map(function(e) { return e.unrecognised; });
       var rejection_reason=returned.inventory_details.map(function(e) { return e.rejection_reason; });
		//rejection_reason=rejection_reason>0 ? rejection_reason :" ";
var date1='';
var date2='';
if(Entry_Date!=null && Entry_Date!='0000-00-00' && Entry_Date!='')  {
		var parts = Entry_Date[0].split('-');
 		 date1=parts[2] + "-"+ parts[1] + "-" + parts[0];
}

      
if(Expiry_Date!=null  && Expiry_Date!='0000-00-00' && Expiry_Date!=''){
		 var parts = Expiry_Date[0].split('-');
 		 date2=parts[2] + "-"+ parts[1] + "-" + parts[0];
}
 	if (Entry_Date=='0000-00-00') {
	 date1='';
 	}	
 	if (Expiry_Date=='0000-00-00') {
 		 date2='';
 	}
	

	$("#batch_num_val").val(batch_num);
 	$("#indent_num_val").val(indent_num);
 	$("#item_name_val").val(id_mst_drugs);
 	$("#source_name_val").val(source_name);
  $("#quantity_rejected_val").val(quantity_rejected);
   $("#from_to_type").val(from_to_type);
    $("#from_to_type_val").val(from_to_type);
    //alert(source_name);
       $("#rejection_reason_val").val(rejection_reason);
 	$("#issue_num").val(issue_num);
 	$("#issue_num_val").val(issue_num);
 	$("#approved_quantity").val(approved_quantity);

 	 $("#Entry_Date").val(date1);
 	
 	$("#unrecognised_val").val(unrecognised);
    
 	$('#from_to_type').trigger('change');
 	$("#indent_num").val(indent_num);
    $("#quantity_received").val(quantity_received);
	$("#quantity_dispatched").val(quantity_dispatched);
    $("#Expiry_Date").val(date2);
    $("#Acceptance_Status").val(Acceptance_Status);
     
          $("#Acceptance_Status").trigger('change');

  
    //$("#relocation_remark").val(relocation_remark);
    //$("#issue_num").val('123');
}
});
if (status=='n') {
	$('#item_name,#indent_num,#issue_num,#batch_num,#quantity_dispatched,#from_to_type,#source_name,#request_date').attr('readonly','readonly');
	$('#info_row').hide();
}
else{
	$('#item_name,#indent_num,#issue_num,#batch_num,#quantity_dispatched,#from_to_type,#source_name,#request_date').removeAttr('readonly','readonly');
	$('#info_row').show();
}
}

$('#Entry_Date').on('change',function(e){
var error_css = {"border":"1px solid red"};
var from_to_type=$('#from_to_type').val();
if (from_to_type==1 || from_to_type==2) {
if($('#item_name').val().trim() == ""){
		
									$("#modal_header").text("Please Select Item");
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#item_name").css(error_css);
									$("#item_name").focus();
									e.preventDefault();
									return false;
								}
	else if($('#indent_num').val().trim() == ""){

									$("#modal_header").text("Please Select indent Number");
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#indent_num").css(error_css);
									$("#indent_num").focus();
									e.preventDefault();
									return false;
								}
					else{			
					var Entry_Date =($('#Entry_Date').datepicker('getDate'));
					var last_Date=($('#indent_date_val').val()).toString();
					var date_arr=last_Date.split(',');
					var index=($("#indent_num")[0].selectedIndex);
					var parts=date_arr[index-1].split('-');
					var indent_date=new Date(parts[1] + '-' + parts[2] + '-' + parts[0]);
					//alert(to_date);
					// alert(entrydate+"**"+Entry_Date);
								if (Entry_Date < indent_date) { 
									$("#modal_header").text("Entry date cannot be before Indent Date("+(parts[2] + '-' + parts[1] + '-' + parts[0])+")");
									$("#modal_text").text("Please check dates");
									$("#Entry_Date" ).val('');
									$("#multipurpose_modal").modal("show");
									return false;
								}
								else{
									return true;
								}
							}
						}
});
$('#batch_num').on('blur',function(evt){
	if ($('#batch_num').val()!=null) {
		var batchnum=$('#batch_num').val();
		  $.ajax({
                    url : "<?php echo site_url('inventory/check_batch_num');?>",
                    method : "GET",
                   data : {
					batch_num: batchnum,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
                        console.log(returned);
        				var batch_num_count = returned.batch_num.map(function(e) {
   						return e.batch_count;
						});
						if ($('#batch_num_val').val()=="" && batch_num_count[0]>0) {
						$("#modal_header").text("Duplicate Batch Number");
									$("#modal_text").text("Please Fill Correct Batch Number");
									$("#batch_num" ).val('');
									$("#multipurpose_modal").modal("show");
									return false;
						}
						else if ($('#batch_num_val').val()!=batchnum && batch_num_count[0]>0) {
							$("#modal_header").text("Duplicate Batch Number");
									$("#modal_text").text("Please Fill Correct Batch Number");
									$("#batch_num" ).val('');
									$("#multipurpose_modal").modal("show");
									return false;
						}
                       }
                });
	}

});
function get_rem_item_name(){

var ismanual=$('#is_manual').val();
var from_to_type=$('#from_to_type').val();
var itemname=$('#item_name_val').val();
	var inventoryid=$('#inventory_id').val();
                $.ajax({
                    url : "<?php echo site_url('inventory/get_rem_indent_item');?>",
                    method : "GET",
                    data : {inventory_id: inventoryid,is_manual:ismanual,item_name:itemname, from_type:from_to_type,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'},
                    //async : true,
                   //dataType : 'json',
                    success: function(data){
                         //console.log(data);
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
        				var id_mst_drug = returned.get_rem_items.map(function(e) {
   						return e.id_mst_drug_strength;
						});
						var drug_name = returned.get_rem_items.map(function(e) {
   						return e.drug_name;
						});
						var strength = returned.get_rem_items.map(function(e) {
   						return e.strength;
						});
						 var type = returned.get_rem_items.map(function(e) {
   						return e.type;
						});
                        var i;
                        var opt='';
                        opt +='<option value="" selected>----Select----</option>';
                        for(i=0; i<id_mst_drug.length; i++){

								
								select='';

                        if (type[i]==1) {
                        	if (id_mst_drug[i]==15) {
                        		opt += '<option value="'+id_mst_drug[i]+'"'+select+'>Sofusbuvir 400 mg + Velpatasvir 100 mg </option>'; 
                        	}
                        	else{
                        		opt += '<option value="'+id_mst_drug[i]+'"'+select+'>'+drug_name[i]+' '+strength[i]+' mg</option>'; 
                        	}
                        	
                        }
                 	else if(type[i]==2){
                 		opt += '<option value="'+id_mst_drug[i]+'"'+select+'>'+drug_name[i]+'</option>'; 
                 	}	
                            
                        }
                        //console.log(opt);
                        $('#item_name').html(opt);
                         $("#item_name").val($("#item_name_val").val());
                         $('#item_name').trigger('change');

                    }
                });
          
						
}
	</script>


