<style>
thead
{
	background-color: #085786;
	color: white;
}
.td_input{width: 100%;padding: 0px;border-radius: 2px;height: 40px;font-size:14px;font-weight: 600;border-width: 0px;border-style: none;}
.table-bordered>tbody>tr>td.data_td
{
	font-weight: 600;
	font-size: 15px;
	text-align: center;
	vertical-align: middle;
	background-color: #e4e4e4;
	border-color:#ffffff;
	border-width:1px;
	color:#000000;
}
.table-bordered>tbody>tr>td
{
	padding-top: 1px;
    padding-right: 8px;
    padding-bottom: 1px;
    padding-left: 8px;
    text-align: center;
	vertical-align: middle;
}
.table-bordered>tbody>tr>td.data_td_late
{
	font-weight: 600;
	font-size: 15px;
	background-color: #ffffff;
	text-align: center;
	vertical-align: middle;
	padding: 0px;
}
.table-bordered>tbody>tr>td.total,.table-bordered>tbody>tr>td.td_total
{
	font-weight: 600;
	font-size: 16px;
	background-color: #ffffff;
	text-align: center;
	vertical-align: middle;
	padding: 0px;
}
</style>
<style>
.loading_gif{
	width : 100px;
	height : 100px;
	top : 45%; 
	left: 45%; 
	position: absolute; 
}

.input_fields
{
	height: 30px !important;
	border-radius: 3 !important;
	width: 100% !important;
	font-size: 14px !important;
	font-family: 'Source Sans Pro';
}

textarea
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
}

.form_buttons
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;

}

.form_buttons:hover
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

.form_buttons:focus
{
	border-radius: 0 !important;
	width: 100% !important;
	font-size: 15px !important;
	font-weight: 600 !important;
	padding: 8px 12px !important;
	border: 2px solid #A30A0C !important;
	background-color: #FFF;
	color: #A30A0C;

}

@media (min-width: 768px) {
	.row.equal {
		display: flex;
		flex-wrap: wrap;
	}
	.col-md-2{
		width: 16.33%;
		padding-left: 8px;
		padding-right: 8px;
	}
	.btn-width{
	width: 12%;
	margin-top: 20px;
}
.pd-15{
	padding-left: 15px;
}
.pb-20{
	padding-bottom: 20px;
}
.container{
	width: 76%;
}
}

@media (max-width: 768px) {

	.input_fields
	{
		height: 40px !important;
		border-radius: 4 !important;
		width: 100% !important;
		font-size: 14px !important;
		font-family: 'Source Sans Pro';
	}

	.form_buttons
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;

	}
	.form_buttons:hover
	{
		border-radius: 0 !important;
		width: 100% !important;
		font-size: 15px !important;
		font-weight: 600 !important;
		padding: 8px 12px !important;
		border: 2px solid #A30A0C !important;
		background-color: #FFF;
		color: #A30A0C;

	}
}

.btn-default {
	color: #333 !important;
	background-color: #fff !important;
	border-color: #ccc !important;
}
.btn {
	display: inline-block;
	padding: 7px 12px;
	margin-bottom: 0;
	font-size: 13px;
	font-weight: 400;
	line-height: 2.4;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	-ms-touch-action: manipulation;
	touch-action: manipulation;
	cursor: pointer;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	background-image: none;
	border: 1px solid transparent;
	border-radius: 4px;
}

a.btn
{
	text-decoration: none;
	color : #000;
	background-color: #A30A0C;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: #CCC !important;
}

.btn-group .btn:hover
{
	text-decoration: none !important;
	color: #000 !important;
	background-color: inherit !important;
}

a.active
{
	color : #FFF !important;
	text-decoration: none;
	background-color: inherit !important;
}

#table_patient_list tbody tr:hover
{
	cursor: pointer;
}

.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
select[readonly] {
  background: #eee;
  pointer-events: none;
  touch-action: none;
}
 .table-bordered>thead>tr>th{
	vertical-align: middle;
	font-size: 13px;
}
.table-bordered>tbody>tr>td{
	vertical-align: middle;
	font-size: 13px;
	color: #000000;
}
</style>
</style>
<br>

<?php $loginData = $this->session->userdata('loginData');
//echo "<pre>"; print_r($loginData);

if($loginData->user_type==1) { 
$select = '';
}else{
$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
$select4 = '';	
}

$filters1 = $this->session->userdata('filters1'); 
//pr($filters1);
//echo $get_info_monthly_record->hcv_screened;
 ?>
<div class="container">
		<div class="panel panel-default" id="Monthly_Report_Panel">
				<div class="panel-heading" style="background-color: #333333;padding: 3px 0px;">
					<h4 class="text-center" style="background-color: #333333;color: white; font-family: 'Source Sans Pro';letter-spacing: .75px;">IMS – MIS Stock Reporting Mismatch
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/excel_black.png" alt=""></a> -->
			<!-- <a href="" class="pull-right" style="margin-right: 10px;"><img width="25px" height="auto" src="<?php echo base_url(); ?>application/third_party/images/pdf_color.png" alt=""></a> -->
		</h4>
		<!-- <input type="button" onclick="printDiv('printableArea')" value="Print" /> -->
		
</div>
<div class="row" style="padding-top: 32px;">
					<?php 
					$tr_msg= $this->session->flashdata('tr_msg');
					$er_msg= $this->session->flashdata('er_msg');
					if(!empty($tr_msg)){  ?>
						

						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						<?php } else if(!empty($er_msg)){ ?>
							<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
								
							<?php } ?>
		<!-- <div class="col-md-3 pull-right" style="padding-right: 15px;">
			<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 10px;padding-bottom: 10px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i> Raise Indent Request
		</a>
		</div> -->
						</div>
<?php
           $attributes = array(
              'id' => 'filter_form',
              'name' => 'filter_form',
               'autocomplete' => 'off',
            );
           echo form_open('', $attributes); ?>
<div class="row" style="padding: 5px 0px 5px 15px;">

	<div class="col-md-2 col-sm-12 ">
		<label for="">State</label>
		<select type="text" name="search_state" id="search_state" class="form-control input_fields" required <?php echo $select2.$select3.$select4; ?>>
			<option value="0">All States</option>
			<?php 
			foreach ($states as $state) {
				?>
				<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo $state->StateName; ?></option>
				<?php 
			}
			?>
		</select>
	</div>
	<div class="col-xs-3 col-md-2">
	<label for="item_type">Item Name <span class="text-danger">*</span></label>
	<div class="form-group">
		<select name="item_type" id="item_type" required class="form-control input_fields">
										<?php inventory_options($this->input->post('item_type')); ?>
										
										<?php foreach ($items as $value) { ?>
											<?php if($value->type!=3) {?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php
												echo ($value->type==2) ? $value->drug_name : $value->drug_name.' '.$value->strength.' '.$value->unit ; ?></option>
											<?php } }?>
											<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
										</select>
		</div>
	</div>
	<div class="col-md-2 col-sm-12">
		<label for="">Year</label>
		<select name="year" id="year" required class="form-control input_fields" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2020', date('Y'));
										$flag=0;
										foreach($dates as $date){
												$year = $date;
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									elseif($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
															
								}
								?>
							</select>
	</div>

	<div class="col-md-2 col-sm-12">
		<label for="">Month</label>
		<select type="text" name="month" id="month" class="form-control input_fields" required>
<option value="">Select</option>
<?php
	$flag=0;
for ($i = 1; $i <=12;$i++) {
$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
if($this->input->post('month')==$i){
echo "<option value=".$i." selected>".$date_str ."</option>";
$flag=1;
}
elseif($i==date('m') && $flag==0){
	echo "<option value=".$i." selected>".$date_str ."</option>";
}
else{
	echo "<option value=".$i.">".$date_str ."</option>";

}
} ?>
</select>
	</div>

<div class="col-lg-2 col-md-2 col-sm-12 btn-width" style="margin-top: 20px;">
					<button type="submit" class="btn btn-block btn-success" id="search" name="search" value="search" style="line-height: 1.2; font-weight: 600;">SEARCH</button>
				</div>
				<!-- <?php //if (!((date('m')==$this->input->post('month') && date('Y')!=$this->input->post('year')) || (date('m')!=$this->input->post('month')&& date('d')>5))) {
					?> -->
					
				
				<span style="margin-top: 20px;margin-right: 25px; float: right;width: 80px;" id="span_save"><button type="submit" class="btn btn-block btn-success" id="save" name="save" value="save" style="line-height: 1.5; font-weight: 600;">Save</button></span>
				<!-- <?php // } ?> -->
			</div>

 
<br>
</div>
<div class="row" id="printableArea">

	<div class="col-md-12">
		<table class="table table-bordered table-highlighted" id="testTable" >
<tbody>
	 <tr>
				<th style="background-color: #ffcccb;color: black; font-family:'Source Sans Pro';font-size: 14px;text-align: center;font-weight: 500" colspan="5">Please review this report by the 5th of each month</th>
				
			</tr>
			<tr>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';">Item</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Stock Utilization as per MIS </th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Stock Utilization Entered</th>
				<th style="background-color: #085786;color: white; font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Gap in data entered</th>
				<th style="background-color: #085786;color: white;font-family:'Source Sans Pro';text-align: center;vertical-align: middle;">Remarks(Enter remarks if blank*)</th>
			</tr>
			

				
<!-- <table class="table table-striped table-bordered table-hover">
	<tbody> -->
		<?php if(!empty($Inventory_Repo)){
			foreach ($filtered_arr as $key=>$value_temp) { ?>
				<tr>
					<th colspan="4" class="info"style="border: none;background-color: #333;font-size: 16px;color:#fff;text-align: left;"><?php echo ($value_temp->drug_abb!='SOF400VEL') ? $value_temp->drug_name : $value_temp->drug_name.' '.$value_temp->strength.' '.$value_temp->unit ; ?></th>
					<th class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus item_toggle" aria-hidden="true" id="<?php echo 'head'.$value_temp->id_mst_drugs; ?>"></i></th></tr>
					<?php foreach ($Inventory_Repo as $key => $value) { 
						if($value_temp->id_mst_drugs==$value->id_mst_drugs){
							?>
							<tr class="<?php echo 'item'.$value_temp->id_mst_drugs; ?> item_row">
					 <td nowrap style="background-color: #ADD8E6; text-align: left;"><input type="hidden" class="form-control" name="id_mst_drug_strength[]" value="<?php echo $value->id_mst_drug_strength; ?>" /><?php echo ($value->type!=2) ? $value->drug_name.' '.$value->strength.' '.$value->unit : $value->drug_name; ?><input type="hidden" class="form-control" name="type[]" value="<?php echo $value->type; ?>" /></td>
												<td>
													<?php 
													if ($value->type==1) {
														$mis= ($value->visits*28);
														echo $mis;

													}
													else{
															$mis= ($value->visits);
															echo $mis;
													}
													 ?>
													 <input type="hidden" class="form-control" name="mis[]" value="<?php echo $mis; ?>" />
												</td>
												<td>
													<?php echo $value->dispensed_quantity+$value->control_used; ?>
														 <input type="hidden"  class="form-control" name="utilize[]" value="<?php echo $value->dispensed_quantity+$value->control_used; ?>" />
												</td>
												<?php 
													if ($value->type==1) {
														$gap= ($value->visits*28)-($value->dispensed_quantity+$value->control_used);
													}
													else{
														$gap=  ($value->visits)-($value->dispensed_quantity+$value->control_used);
													} ?>
												<td title="<?php if ($gap>0){
															echo 'No. of tests/dispensation recorded in the MIS exceeed utilization information entered manually. Please review';
												}
												else{
													echo 'Utilization information entered in the MIS for selected period is incomplete. Please update missing entries';
												} ?>
													
												">
													
														
													<?php echo $gap;

													?>
													<input type="hidden" class="form-control" name="gap[]" value="<?php echo $gap; ?>" />
												</td>
												<td>
													<input class="form-control text-center td_input" type="text" name="remark[]" placeholder="Enter Remarks Here" id="remark[]" value="<?php echo $value->gap_remark; ?>">
												</td>
								</tr>
<!-- <td class="text-center">
<a href="<?php echo site_url()."/inventory/AddReceipt/".$value->receipt_id;?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
<a href="<?php echo site_url()."/inventory/delete_receipt/".$value->receipt_id."/R";?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
</td>

</tr> -->
<?php } } } ?><tr><td colspan="5" style="text-align: left;">In the case of kits, stock utilization of kits as per MIS patient data might not match than stock utilization reported as per the IMS if any of the following two conditions are true:-<br>
<ol>
<li>In the MIS, you have only entered patient records of positive patients but accounted for utilization of all kits in the IMS</li>
<li>You have initiated patients identified positive in screening camps in the MIS but accounted for the utilization of screening kit used for this patient under the category of ‘screening camps’ in the Utilization Module of the IMS. Since, this report only compares utilization records of commodities used for ‘routine dispensation in the facility’ there will be a gap.</li></ol>
If any of these conditions are true for you, please justify the gap by using the ‘Remarks’ button above</td> </tr><?php }else{ ?>
	<tr >
		<td colspan="5" class="text-center">
			NO RECORDS FOUND  BETWEEN SELECTED DATES.
		</td>

	</tr>
<?php } ?>
			</tbody>
		</table>
		<?php echo form_close(); ?>
	</div>
</div>
</div>
<br>
<br>
<br>
<script type="text/javascript">
var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns=""><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name,filename) {
    if (!table.nodeType) table = document.getElementById(table)
            var newtable=document.createElement("table");
            var tbody =document.createElement("tbody");
             var row = document.createElement("tr");
             row.setAttribute("style", "background-color: black;color: white; font-family:'Source Sans Pro';");
  				var cell1 = document.createElement("td");
  				cell1.colSpan=7;
  				cell1.innerHTML ="Date as on : - <?php echo date('jS \ F Y'); ?>";
  				row.appendChild(cell1);
 			 	tbody.appendChild(row);
 			 	newtable.appendChild(tbody);
             var clonedTable=newtable.cloneNode(true);
             var clonedTable1=table.cloneNode(true);
            clonedTable.appendChild(clonedTable1);
    var ctx = {worksheet: name || 'Worksheet', table: clonedTable.innerHTML}
    //window.location.href = uri + base64(format(template, ctx))
     var link = document.createElement('a');
    link.download =filename;
    link.href = uri + base64(format(template, ctx));
    link.click();
     clonedTable.remove();
  }
})()
</script>
<script>

	
$(document).ready(function(){
$('[data-toggle="tooltip"]').tooltip();
$("#search_state").trigger('change');


$('.td_input').click(function(){
	var error_css = {'background-color':'rgb(227, 245, 251)'};
	$(this).css(error_css);
	td_input_val=$(this).val();
});
$('.td_input').blur(function(){
	var error_css = {'background-color':'rgb(255, 255, 255)'};
	$(this).css(error_css);
});

$('#year').change(function(){
	var count=0;
	<?php  $count=0; ?>;
	var options='<option value="">Select</option>';
	var currentYear = (new Date).getFullYear();
	if (currentYear==$('#year').val()) {
			<?php
		$flag=0;
		$count=date("m");
	for ($i = 1; $i <=$count;$i++) {
	$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
	if($this->input->post('month')==$i){ 	?>
	options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php $flag=1;
	}	
	elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php }
	else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	}
	else{
		<?php
		$flag=0;
		$count=12;
		for ($i = 1; $i <=$count;$i++) {
		$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
		if($this->input->post('month')==$i){ 	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php $flag=1;
		}	
		elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php }
		else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	
	}
	//alert(options);
$('#month').html(options);
$('#month').trigger('change');
});

$('#month').change(function(e){
var month=$('#month').val();
var year=$('#year').val();
//$clone = $('#span_save').clone( true )
var currentYear = (new Date).getFullYear();
var currentMonth=(new Date).getMonth()+1;
var currentdate = (new Date).getDate();
//alert(currentdate);
/*if (!((currentMonth==month && currentYear!=year) || (currentMonth!=month && currentdate>5))){
	
}
else{
	$('#span_save').hide();
	$('.td_input').prop("readonly", true);
	$('.td_input').off( "click");
}*/

if((currentMonth==month && currentYear==year)|| (month==(currentMonth-1) && currentYear==year && currentdate<=5)){
	$('#span_save').show();
	$('.td_input').prop("readonly", false);
	$('.td_input').on( "click");
}
else{
	$('#span_save').hide();
	$('.td_input').prop("readonly", true);
	$('.td_input').off( "click");
}

});

$("#year").trigger('change');
$(".td_input").trigger('change');

});
$('#search_state').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					$('#input_district').html(data);
					$("#input_district").trigger('change');
					//$('#input_district').val($('#input_district').val());
				},
				error : function(error)
				{
					alert('Error Fetching Districts');
				}
			})
		});

		$('#input_district').change(function(){

			$.ajax({
				url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),
				data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
				method : 'GET',
				success : function(data)
				{
					//alert(data);
					if(data!="<option value=''>Select Facilities</option>"){
					$('#mstfacilitylogin').html(data);
					
					}
				},
				error : function(error)
				{
					//alert('Error Fetching Blocks');
				}
			})
		});
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

</script>