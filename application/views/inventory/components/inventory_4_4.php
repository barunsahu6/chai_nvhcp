<style>
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: middle;
	}
	td{
		text-align: center; 
		vertical-align: middle;
	}
	.item{
		width: 140px !important;
	}
	.batch_num{
		width: 100px !important;
	}
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	input,nav,label,span{
		font-family: 'Source Sans Pro';

	}
	label{
		font-family: 'Source Sans Pro';
		font-size: 14px;
	}
	input,select,.form-control{
		height: 30px;
	}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData'); ?>
<!-- add/edit facility contact modal starts-->
<div class="modal fade " tabindex="-1" role="dialog" id="myModal1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #333;color: white;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Stock Reciept</h4>
      </div>
      <div class="modal-body">
      	<?php
           $attributes = array(
              'id' => 'Add_Receipt_form',
              'name' => 'Add_Receipt_form',
               'autocomplete' => 'off',
            );
          // echo $receipt_details[0]->Receipt_Date;
           //echo $receipt_details[0]->rejection;
           //print_r($edit_flag);
          // print_r($source_name);
           if(isset($receipt_details[0]->receipt_id)){
           	$receipt_id=$receipt_details[0]->receipt_id;
           }
           else{
           	$receipt_id=NULL;
           }
           echo form_open('Inventory/AddReceipt/'.$receipt_id, $attributes); ?>
        		<div class="row">
					
				<div class="col-xs-12 col-md-2 col-sm-12 text-left col-md-offset-1" id="labelfor">
				<label for="item_type">Item Name <span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-md-3 col-sm-12" id="item_div">
					<select name="item_type" id="item_type" required class="form-control">
					<?php foreach ($items as $value) { ?>
					<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if(isset($receipt_details[0]->id_mst_drugs)){ if($receipt_details[0]->id_mst_drugs==$value->id_mst_drug) { echo 'selected';}}?>>
							<?php if($value->type==2){echo $value->drug_name; } else{
								echo $value->drug_name." (".$value->strength.")";
							} ?></option>
					<?php } ?>
					<!-- <option value="999" <?php if(isset($receipt_details[0]->id_mst_drugs)){ if($receipt_details[0]->id_mst_drugs=='999') { echo 'selected';}}?>>other</option> -->
					</select>
				</div>
				<div class="col-xs-12 col-md-2 col-sm-12">
        			<label for="contact_name">Indent Number <span class="text-danger">*</span></label>
        		</div>
        		<div class="col-md-3 col-sm-12 form-group">
					<input type="text" class="form-control" id="batch_num" name="batch_num" placeholder="Indent Number" pattern="^[a-zA-Z0-9]+$" required onkeyup="this.value = this.value.toUpperCase();" value="<?php if(isset($receipt_details[0]->batch_num)){ echo $receipt_details[0]->batch_num;}?>" disabled="true">
        		</div>
        	</div>
        	<div class="row">
					
				<div class="col-xs-12 col-md-3 col-sm-12 text-left" id="labelfor">
				<label for="item_type">Quantity Indented /Approved (screening tests/bottle)<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-md-3 col-sm-12" id="item_div">
					<input type="number" class="form-control" name="quantity" min="1" max="10000" required value="<?php if(isset($receipt_details[0]->quantity)){ echo $receipt_details[0]->quantity;}?>">
				</div>
				<div class="col-xs-12 col-md-2 col-sm-12">
        			<label for="contact_name">Recieved From<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-md-3 col-sm-12 form-group">
					<select name="from_to_type" id="from_to_type" required class="form-control">

					<?php foreach ($source_name as $value) { ?>
						<option value="<?php echo $value->LookupCode;?>" <?php if(isset($receipt_details[0]->from_to_type)){ if($receipt_details[0]->from_to_type==$value->LookupCode) { echo 'selected';}}?>>
							<?php echo $value->LookupValue; ?></option>
					<?php } ?>
				</select>
        		</div>
        	</div>

			<div class="row">
					
				<div class="col-xs-12 col-md-2 col-sm-12 text-left col-md-offset-1" id="labelfor">
				<label for="item_type">Issue Number <span class="text-danger">*</span></label>
				</div>
				<div class="col-md-3 col-sm-12 form-group">
				<input type="text" class="form-control" id="batch_num" name="batch_num" placeholder="Indent Number" pattern="^[a-zA-Z0-9]+$" required onkeyup="this.value = this.value.toUpperCase();" value="<?php if(isset($receipt_details[0]->batch_num)){ echo $receipt_details[0]->batch_num;}?>" disabled="true">
				</div>
				<div class="col-xs-12 col-md-2 col-sm-12">
        			<label for="contact_name">Batch Number <span class="text-danger">*</span></label>
        		</div>
        		<div class="col-md-3 col-sm-12 form-group">
					<input type="text" class="form-control" id="batch_num" name="batch_num" placeholder="Indent Number" pattern="^[a-zA-Z0-9]+$" required onkeyup="this.value = this.value.toUpperCase();" value="<?php if(isset($receipt_details[0]->batch_num)){ echo $receipt_details[0]->batch_num;}?>" disabled="true">
        		</div>
        	</div>
<div class="row">
					
				<div class="col-xs-12 col-md-3 col-sm-12 text-left" id="labelfor">
				<label for="item_type">Quantity of Stock dispatched (screening tests/bottle)<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-md-3 col-sm-12" id="item_div">
					<input type="number" class="form-control" name="quantity" min="1" max="10000" required value="<?php if(isset($receipt_details[0]->quantity)){ echo $receipt_details[0]->quantity;}?>">
				</div>
				<div class="col-xs-12 col-md-2 col-sm-12">
        			<label for="contact_name">Date Of Receipt<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-md-3 col-sm-12 form-group">
					<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="Receipt Date" name="Entry_Date" id="Entry_Date" value="<?php if(isset($receipt_details[0]->Entry_Date)){ echo timeStampShow($receipt_details[0]->Entry_Date);} else {echo timeStampShow(date('Y-m-d')); } ?>" required onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" onchange="checkexpirydate()">
        		</div>
        	</div>

      <div class="row">
					
				<div class="col-xs-12 col-md-3 col-sm-12 text-left" id="labelfor">
				<label for="item_type">Quantity of Stock received (screening tests/bottle)<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-md-3 col-sm-12" id="item_div">
					<input type="number" class="form-control" name="quantity" min="1" max="10000" required value="<?php if(isset($receipt_details[0]->quantity)){ echo $receipt_details[0]->quantity;}?>">
				</div>
				<div class="col-xs-12 col-md-2 col-sm-12">
        			<label for="contact_name">Expiry Date<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-md-3 col-sm-12 form-group">
					<input type="text" class="form-control hasCal dateInpt" placeholder="Expiry Date" name="Expiry_Date" id="Expiry_Date" onchange="checkexpirydate()" required onkeydown="return false" onkeyup="return false" value="<?php if(isset($receipt_details[0]->Expiry_Date)){ echo timeStampShow($receipt_details[0]->Expiry_Date);} ?>">
        	</div>  	
      </div>
      <div class="row">
					
				<div class="col-xs-12 col-md-3 col-sm-12 text-left" id="labelfor">
				<label for="item_type">Stock Acceptance/Rejection Status<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-md-3 col-sm-12" id="item_div">
					<select name="Acceptance_Status" id="Acceptance_Status" required class="form-control">
					<?php foreach ($Acceptance_Status as $value) { ?>
						<option value="<?php echo $value->LookupCode;?>" <?php if(isset($receipt_details[0]->Acceptance_Status)){ if($receipt_details[0]->Acceptance_Status==$value->LookupCode) { echo 'selected';}}?>>
							<?php echo $value->LookupValue; ?></option>
					<?php } ?>
					</select>
				</div>
				<div class="col-xs-12 col-md-2 col-sm-12">
        			<label for="contact_name">Reason for rejection<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-md-3 col-sm-12 form-group">
					<select name="from_to_type" id="from_to_type" required class="form-control">

					<?php foreach ($source_name as $value) { ?>
						<option value="<?php echo $value->LookupCode;?>" <?php if(isset($receipt_details[0]->from_to_type)){ if($receipt_details[0]->from_to_type==$value->LookupCode) { echo 'selected';}}?>>
							<?php echo $value->LookupValue; ?></option>
					<?php } ?>
				</select>
        	</div>  	
      </div>
      <div class="row">
					
				<div class="col-xs-12 col-md-3 col-sm-12 text-left" id="labelfor">
				<label for="item_type">Quantity of Stock received (screening tests/bottle)<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-md-3 col-sm-12" id="item_div">
					<input type="number" class="form-control" name="quantity" min="1" max="10000" required value="<?php if(isset($receipt_details[0]->quantity)){ echo $receipt_details[0]->quantity;}?>">
				</div>
      </div>
  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="facility_contact_save">Save changes</button>
      </div>
    </div>
  </div>
</div><!--  /.modal -->

<!-- add/edit facility contact modal ends-->

<div class="row main-div" style="min-height:400px;">
	<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12" style="padding: 0;">
			<?php //include("left_nav_bar.php");
			$this->load->view("inventory/components/left_nav_bar"); ?>
		</div>

		<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
			<div class="panel panel-default" id="Record_receipt_panel">
				<div class="panel-heading" style=";background: #333;">
					<h4 class="text-center" style="color: white;background: #333" id="Record_form_head">Stock Reciept</h4>
				</div>
				<div class="row">
					<?php 
					$tr_msg= $this->session->flashdata('tr_msg');
					$er_msg= $this->session->flashdata('er_msg');
					if(!empty($tr_msg)){  ?>
						

						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						<?php } else if(!empty($er_msg)){ ?>
							<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
								
							<?php } ?>
						</div>
						<div class="row">
	<div class="col-lg-12" style="margin-bottom: 20px; margin-top: 20px;">
		<div class="col-md-3 col-md-offset-5">
			<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 10px;padding-bottom: 10px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i>  Add New Stock Receipt
</a>
		</div>
	</div>
</div>
						<?php
						$attributes = array(
							'id' => 'Receipt_filter_form',
							'name' => 'Receipt_filter_form',
							'autocomplete' => 'false',
						);
  //pr($items);
  //print_r($receipt_details);

						echo form_open('', $attributes); ?>
						<div class="row">
							<div class="col-xs-3 col-md-2 col-md-offset-1">
								<label for="item_type">Item Name <span class="text-danger">*</span></label>
								<div class="form-group">
									<select name="item_type" id="item_type" required class="form-control">
										<option value="Kit">All Screening Test Kits</option>
										<option value="drug">All Drugs</option>
										<option value="drugkit" <?php if($this->input->post('item_type')=='drugkit') { echo 'selected';}?> selected>All Kits and Drugs</option>
										<?php foreach ($items as $value) { ?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php if($value->type==2){echo $value->drug_name; } else{
													echo $value->drug_name." (".$value->strength.")";
												} ?></option>
											<?php } ?>
											<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
										</select>
									</div>
								</div>
								
								
						
								<div class="col-md-2 col-sm-12 form-group">
									<label for="year">Financial Year <span class="text-danger">*</span></label>
									<select name="year" id="year" required class="form-control" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2016', date('Y'));
										$flag=0;
										foreach($dates as $date){

									if (date('m', strtotime($date)) <= 4) {//Upto April
										$year = ($date-1) . '-' . $date;
									} else {//After April
										$year = $date . '-' . ($date + 1);
									}
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									if($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
									
								}
								?>
							</select>
						</div>
								<div class="col-md-2 col-sm-12">
									<label for="">From Date</label>
									<input type="text" class="form-control input_fields hasCal_Receipt dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')){ echo $this->input->post('startdate');} else{ echo timeStampShow(date("Y-m-d", mktime(0, 0, 0, date("m")-1, 1)));}  ?>" onchange="checkenddate();" required style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 col-sm-12">
									<label for="">To Date</label>
									<input type="text" class="form-control input_fields hasCal_Receipt dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')){echo $this->input->post('enddate');}else{echo timeStampShow(date('Y-m-d'));} ?>" required onchange="checkenddate();" style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 btn-width">
									<label for="">&nbsp;</label>
									<button class="btn btn-block" style="line-height: 1.2; font-weight: 600;background-color: #f4860c;border-color: #f4860c;color: white;">SEARCH</button>
								</div>

								<?php echo form_close(); ?>
							</div>
							
						</div>

						<table class="table table-striped table-bordered table-hover" id="table_inventory_list">

							
							<thead>
							<tr style="background-color: #085786; font-family: 'Source Sans Pro';color: white;font-size: 13px;">
									<th>Item</th>
									<th>Indent Number</th>
									<th>Quantity Indented /Approved (screening tests/bottle)</th>
									<th>Received From</th>
									<th>Issue No</th>
									<th>Batch Number</th>
									<th>Quantity of Stock dispatched (screening tests/bottle)</th>
									<th>Date of receipt</th>
									<th>Quantity of <br>Stock received<br> (screening tests/bottle)</th>
									<th>Expiry Date</th>
									<th>Stock Acceptance/Rejection Status</th>
									<th>Reason for rejection</th>
									<th>Quantity of Stock Rejected</th>
									<th>Commands</th>
								</tr>
							</thead>
							<tbody>
			<!-- </tbody>
			</table> -->
				<?php // echo "<pre>";/*print_r($inventory_details);*/
				//$result=array_unique($inventory_details->);
				$item_id=array();
				foreach ($items as $key => $value) {
					foreach ($inventory_details as $value1) {
						if($value->id_mst_drugs==$value1->drug_name){
							$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
							$item_id[$key]['drug_name']= $value->drug_name;
						}
					}
					
				}
				$filtered_arr=array_unique($item_id,SORT_REGULAR);
				//pr($filtered_arr);exit();
				//pr($inventory_details);
				?>
				<!-- <table class="table table-striped table-bordered table-hover">
					<tbody> -->
						<?php if(!empty($inventory_details)){
							foreach ($filtered_arr as $key=>$value_temp) { ?>
								<tr>
									<th colspan="9" class="info"style="border: none;background-color: #088da5;font-size: 16px;color:#fff;text-align: left;"><?php echo $value_temp['drug_name']; ?></th>
									<th class="info" style="border: none;background-color: #088da5;color:#fff;text-align: right;"><i class="fa fa-minus" aria-hidden="true" id="<?php echo 'head'.$value_temp['id_mst_drugs']; ?>"></i></th></tr>
									<?php foreach ($inventory_details as  $value) { 
										if($value_temp['id_mst_drugs']==$value->drug_name){
											?>
											<tr class="<?php echo $value_temp['id_mst_drugs']; ?>">
												<td class="item">
													<?php if($value->type==1){echo $value_temp['drug_name']."(".$value->strength.")";} elseif($value->type==2){echo $value_temp['drug_name'];} ?>
												</td>
												<td>
													<?php echo $value->batch_num; ?>
												</td>
												<td>
													<?php if ($value->facility_short_name==NULL){
														echo $value->unrecognised."<br>(Unregistered Source)";
													}
													else{
														echo $value->facility_short_name;
													}

													?>
												</td>
												<td nowrap>
													<?php echo timeStampShow($value->Entry_Date); ?>
												</td>
												<td>
													<?php echo $value->quantity; ?>
												</td>
												<td>
													<?php if ($value->quantity_screening_tests==0) {
														echo "N/A";
													}else{
														echo $value->quantity_screening_tests;
													}?>
												</td>
												<td nowrap>
													<?php echo timeStampShow($value->Expiry_Date); ?>
												</td>
												<td>

													<?php foreach ($Acceptance_Status as $Acceptance) {
														# code...
													 if($value->Acceptance_Status==$Acceptance->LookupCode) { echo $Acceptance->LookupValue;}} ?>
												</td>
												<td>
													<?php echo $value->rejection; ?>
												</td>
												<td class="text-center">
													<a href="<?php echo site_url()."/inventory/AddReceipt/".$value->receipt_id;?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
													<a href="<?php echo site_url()."/inventory/delete_receipt/".$value->receipt_id."/R";?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
												</td>

											</tr>
										<?php } }}}else{ ?>
											<tr >
												<td colspan="10" class="text-center">
													NO RECORDS FOUND  BETWEEN SELECTED DATES.
												</td>

											</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
							<br>
						</div>
						<div class="overlay">
							<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
						</div>

						<!-- Data Table -->
						<script type="text/javascript">

							function isNumberKey(evt)
							{
								var charCode = (evt.which) ? evt.which : event.keyCode
								if (charCode > 31 && (charCode < 48 || charCode > 57))
									return false;

								
								if(evt.srcElement.id == 'Phone1' && $('#Phone1').val() > 999999999)
								{
									$("#modal_header").html('Contact number must be 10 digits');
									$("#modal_text").html('Please fill in appropriate Contact Number');
									$("#multipurpose_modal").modal("show");
									return false;
								}
								
								if(evt.srcElement.id == 'PinCode' && $('#PinCode').val() > 99999)
								{
									$("#modal_header").html('PIN cannot be more than 6 digits');
									$("#modal_text").html('Please fill in appropriate PIN');
									$("#multipurpose_modal").modal("show");
									return false;
								}

								return true;
							}

							$('#Acceptance_Status').change(function(){
								var idval = $('#Acceptance_Status').val();
//alert(idval);
if(idval=='yes')
{
	$('#rejection').removeAttr('required');
	$('#rejection').attr('disabled','disabled');
}
else if(idval=='no')
{
	
	$('#rejection').attr('required','required');
	$('#rejection').removeAttr('disabled','disabled');
	//$('#rejection').val('N/A');

}
});

							function checkenddate()
							{
								var startdate = $('#startdate').datepicker('getDate');
								var enddate = $('#enddate').datepicker('getDate');
								if ( startdate > enddate && enddate!=null) { 
									$("#modal_header").text("To date cannot be before From Date");
									$("#modal_text").text("Please check dates");
									$("#enddate" ).val('');
									$("#multipurpose_modal").modal("show");
									return false;
								}
								else{
									return true;
								}
								
							}
							$('#batch_num').keypress(function (e) {
								var regex = new RegExp("^[a-zA-Z0-9]+$");
								var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
								if (regex.test(str)) {
									return true;
								}

								e.preventDefault();
								return false;
							});
							$(document).ready(function(){
								$(".glyphicon-trash").click(function(e){
									var ans = confirm("Are you sure you want to delete?");
									if(!ans)
									{
										e.preventDefault();
									}
								});
								<?php foreach ($filtered_arr as $key=>$value_temp) {
									$elemnt_id= ".".$value_temp['id_mst_drugs'];?> 
									$("<?php echo $elemnt_id; ?>").show();
									$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").click(function(){
										if($("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").hasClass('fa-minus')){
											$("<?php echo $elemnt_id; ?>").hide();
											$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-minus').addClass("fa-plus");
										}
										else{
											$("<?php echo $elemnt_id; ?>").show();
											$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-plus').addClass("fa-minus");
										}
										
									});
									$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").css('cursor', 'pointer');
								<?php }?>
							});
function checkexpirydate()
	{
		var Entry_Date = $('#Entry_Date').datepicker('getDate');
		var Expiry_Date = $('#Expiry_Date').datepicker('getDate');
   		//alert(Receipt_Date+'/'+Expiry_Date);
        if ( Entry_Date > Expiry_Date && Expiry_Date!=null) { 
        	$("#modal_header").text("Expiry date cannot be before Receipt Date");
			$("#modal_text").text("Please check dates");
			$("#Expiry_Date" ).val('');
			$("#multipurpose_modal").modal("show");
			return false;
        }
 		else{
 			 return true;
 		}
       
    }
$('#batch_num').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9{2}]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test($.trim(str))) {
        return true;
    }
    else{
    	e.preventDefault();
    return false;
    }
    
});
						</script>


