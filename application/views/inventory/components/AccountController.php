<?php

	namespace FES\CLART\Modules\Account;

	use FES\CLART\Modules\Account\Model\Account;
	use FES\CLART\Modules\Account\Model\Role;

	use FES\CLART\Modules\Home\Model\AppUser;

	use FES\CLART\Modules\ClartForm\Model\ClartForm;

	use FES\Framework\Classes\DataTable;

	use GuzzleHttp\Client;

	class AccountController {

		protected $Model;
		protected $App;
		protected $Columns;

		protected $RoleColumns;
		protected $RoleModel;

		protected $Log;

		protected $ModuleUri;

		public function init($app) {
			$this->App = $app;
			$this->App->getTheme()->setModule(basename(__DIR__));
			$this->Model = new Account();

			$this->RoleModel = new Role();

			$this->Log = $this->App->getLog();
			$this->Request = $this->App->getRequest();
            $this->Session = $this->App->getSession();

			$routes = $this->App->getRoutes();

			if(isset($routes['parent']) && in_array($routes['parent'], array('login', 'check-email', 'check-availability'))) {
                $this->App->setRoutes('auth', false);
			}

			$this->ModuleUri = $this->App->getTheme()->getModuleUri();

			$this->Columns = array(
				'id' => 'skip', 
				'strName' => 'Name', 
				'strMobile' => 'Mobile',
				'strRole' => 'Role',
				'accountStatus' => 'Status',
				'dtiLastLogin' => 'Last Login',
				'' => 'skip'
			);

			$this->RoleColumns = array(
				'id' => 'skip', 
				'strRole' => 'Name', 
				'txtDescription' => 'Description', 
				'' => 'skip'
			);
		}

		public function login() {

			$client = new Client();

            $response = $client->request('POST', 'http://apps.fes.org.in/clart/api/check-update2', [
                'form_params' => array()
			]);
			
			$content = $response->getBody()->getContents(); //_e($content); exit;
            
            $decoded = json_decode($content, true); //pr($decoded);

			if($response->getStatusCode() === 200) {
				$body = array(
					'app' => $decoded
				);
	
				$this->App->setBodyData($body);
			}

			$this->App->getTheme()->setLibraries(array('validate'));
			$this->App->display('login', 'main-login-page');
		}

		public function postLogin() {

			$request = $this->App->getRequest();
			$session = $this->App->getSession();
			$log = $this->App->getLog();
			
			$username = $request->post('username');
			$password = $request->post('password');
			$passwordHash = _hash($password); //echo $passwordHash; exit;

			$loginData = $this->Model->chkLogin($username, $passwordHash); //pr($loginData);

			if ($loginData == 404) {
				$session->add('ERROR', 'Login failed; Invalid username or password');
				_locate(SITE_URL);
			} else {

				$this->Model->getAccessRights($loginData['role_id']);

				if($this->Model->checkAccess('access-user', 'login')) {

					$session->id('MAIN');
					$session->add('USERID', $loginData['id']);
					$session->add('NAME', $loginData['full_name']);
					$session->add('ROLEID', $loginData['role_id']);

					$page = SITE_URL . 'account/dashboard';
					if(defined('DASHBOARD_URI')) $page = SITE_URL . DASHBOARD_URI;

					$this->Model->updateLastLoginDate($loginData['id']);

					$session->add('MSG', "You have been successfully logged in");

					$log->write($loginData['id'], $loginData['full_name'].' has been successfully logged in', 0);

				} else {

					$session->add('ERROR', "ERROR! You don't have rights to login");
					$page = SITE_URL;

				}

				_locate($page);
			}
		}

		public function dashboard() {

			$appUserModel = new AppUser;

			$filters = array(
				'resultset' => 'count'
			);

			$appUserCount = $appUserModel->getAppUsersByFilter($filters);

			$clartModel = new ClartForm;

			$filters = array(
				'resultset' => 'count'
			);

			$totalCount = $clartModel->getClartFormsByFilter($filters);
			
			$filters['where'] = array('month(main.dtiSubmit)' => date('m'), 'year(main.dtiSubmit)' => date('Y'));
			$monthCount = $clartModel->getClartFormsByFilter($filters);

			$filters['where'] = array('date(main.dtiSubmit)' => date('Y-m-d'));
			$todayCount = $clartModel->getClartFormsByFilter($filters);

			$filters['resultset'] = 'data';
			$filters['fields'] = array('count(main.id) as total_rows', "au.tiStatus as app_status");
			$filters['group'] = array('au.tiStatus');
			$appCount = $clartModel->getClartFormsByFilter($filters); //pr($appCount);

			$appStatusCount = array();
			if($appCount != 404) {
				foreach($appCount as $ac) {
					$status = $ac['app_status'];
					$appStatusCount[$status] = $ac['total_rows'];
				}
			}

			$body = array(
				'app_users' => $appUserCount,
				'total_forms' => $totalCount,
				'month_forms' => $monthCount,
				'app_count' => $appStatusCount,
				'today_forms' => $todayCount
			);

			$this->App->getTheme()->setLibraries(array('morris', 'raphael'));
			$this->App->setBodyData($body);
			$this->App->display('dashboard');
		}

		public function viewList() {

			if(!$this->App->checkAccess('account', 'list')) {
				$this->App->displayError('unauthorized');
			}

			$this->App->getTheme()->setLibraries(array('datatable', 'dt-buttons', 'dt-ajax-init'));
			$this->App->setBodyData(array('columns' => $this->Columns));
			$this->App->display('list', 'list-layout');
		}

		public function roles() {

			if(!$this->App->checkAccess('role', 'list')) {
				$this->App->displayError('unauthorized');
			}

			$this->App->getTheme()->setLibraries(array('datatable', 'dt-buttons', 'dt-ajax-init'));
			$this->App->setBodyData(array('columns' => $this->RoleColumns));
			$this->App->display('roles', 'list-layout');
		}

		public function ajaxAccounts() {

			$columns = array_keys($this->Columns);

			$dataTable = new DataTable();
			$dataTable->setColumns($columns);
			$dataTable->setRequest($this->Request);
			
			$filters = array(
				'resultset' => 'count',
				'not' => array('main.id' => $this->Session->value('USERID'))
			);
			
			if($this->App->checkAccess('account', 'list-assigned')) {
				$filters['where']['main.idCreated'] = $this->Session->value('USERID');
			}

            /* Total data set length */
            $accountCount = $this->Model->getAccountsByFilter($filters);

			$likeArray = $dataTable->filterQuery();
			$filters['like'] = $likeArray;
                
            /* Data set length after filtering */
			$accountFiltered = $this->Model->getAccountsByFilter($filters);
			
			/*
			* Paging
			*/
			if ( $this->Request->post('length') > 0 ) {
				$this->Model->begin = $this->Request->check('start') ? $this->Request->post('start') : 0;
				$this->Model->perpage = $this->Request->post('length');
			}

			$options = array(
				'count' => $accountCount,
				'filtered' => $accountFiltered
			);

			if($accountFiltered > 0) {
				$filters = array(
					'resultset' => 'data',
					'order' => array('main.strName'),
					'like' => $likeArray,
					'not' => array('main.id' => $this->Session->value('USERID'))
				);

				$sOrder = $dataTable->orderData();

				if(sizeof($sOrder) > 0) {
					$filters['order'] = $sOrder;
				}

				$options['data'] = $this->Model->getAccountsByFilter($filters);

				if($this->App->checkAccess('account', 'edit')) {
					$options['links'][] = array(
						'url' => 'edit-account?id={id}',
						'class' => 'fa fa-edit',
						'title' => 'Edit Account'
					);
				}

				if($this->App->checkAccess('account', 'disable')) {
					$options['links'][] = array(
						'url' => 'change-status?id={id}',
						'class' => 'fa fa-times link-confirm',
						'confirm' => "Are you really want to {status} '{name}'?",
						'title' => '',
						'column' => 'strName',
						'status' => 'enmStatus'
					);
				}
			}

			$dataTable->ajaxDataTable($options);
		}

		public function logout() {

			$log = $this->App->getLog();

			$log->write($this->Session->value('USERID'), $this->Session->value('NAME').' has been successfully logged out');
			
			$this->Session->remove('USERID');
			$this->Session->remove('EMAIL');
			$this->Session->remove('NAME');

			$this->Session->destroy();
			
			_locate(SITE_URL);
		}

		public function newAccount() {

			if(!$this->App->checkAccess('account', 'add')) {
				$this->App->displayError('unauthorized');
			}

			$filters = array(
				'resultset' => 'data',
				'order' => array('main.strRole')
			);

			$roles = $this->RoleModel->getRolesByFilter($filters);

			$body = array(
				'roles' => $roles,
				'action' => 'add'
			);

			$this->App->getTheme()->setLibraries(array('validate'));
			$this->App->setBodyData($body);
			$this->App->display('add');
		}

		public function editAccount() {

			if(!$this->App->checkAccess('account', 'edit')) {
				$this->App->displayError('unauthorized');
			}

			$filters = array(
				'resultset' => 'data',
				'order' => array('main.strRole')
			);

			$roles = $this->RoleModel->getRolesByFilter($filters);

			$filters = array(
				'resultset' => 'row',
				'where' => array('main.id' => $this->Request->get('id'))
			);

			$account = $this->Model->getAccountsByFilter($filters);

			if($account == 404) {
				$this->App->displayError();
			}

			if($this->App->checkAccess('account', 'list-assigned') && $account['idCreated'] != $this->Session->value('USERID')) {
				$this->App->displayError('unauthorized');
			}

			$body = array(
				'account' => $account,
				'roles' => $roles,
				'action' => 'edit'
			);

			$this->App->getTheme()->setLibraries(array('validate'));
			$this->App->setBodyData($body);
			$this->App->display('add');
		}

		public function postAccount() {

			$this->Request->setValue('user', $this->Session->value('USERID'));

			switch($this->Request->post('action')) {
				case 'add':
					$accountId = $this->Model->addAccount($this->Request->post());
					$msg = "<strong>" . $this->Request->post('fullname') . "</strong> has been successfully added";
					break;
				case 'profile':
				case 'edit':
					$accountId = $this->Model->editAccount($this->Request->post());
					$msg = "<strong>" . $this->Request->post('fullname') . "</strong> has been successfully updated";
					break;
				default:
					$this->App->displayError();
					break;
			}

			if ($accountId !== false) {
				$this->Session->add('MSG', $msg);
				$this->App->getLog()->write($accountId, strip_tags($msg));
			} else {
				$error = "ERROR! Please contact system administrator";
				$this->Session->add('ERROR', $error);
				$this->App->getLog()->write($accountId, $error, 'error');
			}

			$page = $this->Request->check('r') ? $this->Request->post('r') : $this->ModuleUri . "view-list";
			_locate($page);
		}

		public function changeStatus() {

			if(!$this->App->checkAccess('account', 'disable')) {
				$this->App->displayError('unauthorized');
			}

			$filters = array(
				'resultset' => 'row',
				'where' => array('main.id' => $this->Request->get('id'))
			);

			$account = $this->Model->getAccountsByFilter($filters);

			if($account == 404) {
				$this->App->displayError();
			}

			if($this->App->checkAccess('account', 'list-assigned') && $account['idCreated'] != $this->Session->value('USERID')) {
				$this->App->displayError('unauthorized');
			}

			$statusData = array(
				'id' => $this->Request->get('id'),
				'status' => $account['enmStatus'],
				'user' => $this->Session->value('USERID')
			);
			$result = $this->Model->updateStatus($statusData);

			if($result === false) {
				$this->App->displayError(500);
			}

			$this->Session->add('MSG', $account['strName'].' has been successfully '.($account['enmStatus'] == 0 ? 'enabled' : 'disabled'));
			_locate($this->ModuleUri . "view-list");
		}

		public function ajaxRoles() {

			$columns = array_keys($this->RoleColumns);

			$dataTable = new DataTable();
			$dataTable->setColumns($columns);
			$dataTable->setRequest($this->Request);
			
			$filters = array(
                'resultset' => 'count'
            );

            /* Total data set length */
            $roleCount = $this->RoleModel->getRolesByFilter($filters);

			$likeArray = $dataTable->filterQuery();
			$filters['like'] = $likeArray;
                
            /* Data set length after filtering */
            $roleFiltered = $this->RoleModel->getRolesByFilter($filters);

			$options = array(
				'count' => $roleCount,
				'filtered' => $roleFiltered
			);

			/*
			* Paging
			*/
			if ( $this->Request->post('length') > 0 ) {
				$this->RoleModel->begin = $this->Request->check('start') ? $this->Request->post('start') : 0;
				$this->RoleModel->perpage = $this->Request->post('length');
			}

			if($roleFiltered > 0) {
				$filters = array(
					'resultset' => 'data',
					'order' => array('main.strRole'),
					'like' => $likeArray
				);

				$sOrder = $dataTable->orderData();

				if(sizeof($sOrder) > 0) {
					$filters['order'] = $sOrder;
				}

				$options['data'] = $this->RoleModel->getRolesByFilter($filters);

				if($this->App->checkAccess('role', 'assign')) {
					$options['links'][] = array(
						'url' => 'permissions?id={id}',
						'class' => 'fa fa-check-square',
						'title' => 'Assign Rights'
					);
				}
			}

			$dataTable->ajaxDataTable($options);
		}

		public function ajaxCheckEmail() {
			if($this->Request->check('email', 'get')){
				$filters = array(
					'resultset' => 'count',
					'where' => array('strEmail' => $this->Request->get('email'))
				);
	
				$accountCount = $this->Model->getAccountsByFilter($filters);
				if($accountCount == 0) {
					die("true");
				}else{
					die("false");
				}
			}
		}

		public function postCheckAvailability() {
			if($this->Request->check('email', 'get')){
				$filters = array(
					'resultset' => 'count',
					'where' => array('strEmail' => $this->Request->get('email'))
				);
	
				$accountCount = $this->Model->getAccountsByFilter($filters);
				if($accountCount == 0) {
					die("false");
				}else{
					die("true");
				}
			}
		}

		public function permissions() {

			if(!$this->App->checkAccess('role', 'assign')) {
				$this->App->displayError('unauthorized');
			}

			$filters = array(
				'resultset' => 'row',
				'where' => array('id' => $this->Request->get('id'))
			);

			$role = $this->RoleModel->getRolesByFilter($filters);

			$modules = array(
				'account' => array(
					'title' => 'Account', 
					'action' => array('add', 'edit', 'list', 'list-assigned', 'disable', 'login', 
									  'dashboard', 'delete', 'reset-password')
				),
				'role' => array(
					'title' => 'System Role', 
					'action' => array('add', 'list', 'edit', 'assign')
				),
				'clart' => array(
					'title' => 'CLART Forms',
					'action' => array('list', 'export', 'delete-form', 'multi-delete')
				),
				'app-user' => array(
					'title' => 'App Users',
					'action' => array('list', 'export')
				),
				'sync' => array(
					'title' => 'Sync Data',
					'action' => array('check-forms')
				)
			);
			
			$actions = array(
				'add' => 'Add',
				'edit' => 'Edit',
				'delete' => 'Delete',
				'list' => 'List',
				'list-all' => 'List All',
				'list-assigned' => 'List Created by User',
				'assign' => 'Assign',
				'disable' => 'Disable',
				'login' => 'Login',
				'dashboard' => 'Dashboard',
				'reset-password' => 'Reset Password',
				'export' => 'Export',

				'delete-form' => 'Delete Form',
				'multi-delete' => 'Delete Multiple Form',

				'check-forms' => 'Check Forms'
			);

			$body = array(
				'role' => $role,
				'modules' => $modules,
				'actions' => $actions
			);

			$this->App->getTheme()->setLibraries(array('validate', 'datatable'));
			$this->App->setBodyData($body);
			$this->App->display('assign');
		}

		public function postAssign() {

			$this->Request->setValue('user', $this->Session->value('USERID'));
			$update = $this->RoleModel->updateRolePermissions($this->Request->post());

			if ($update !== false) {
				$msg = 'Roles has been successfully updated';
				$this->Session->add('MSG', $msg);
				$this->App->getLog()->write($this->Session->value('USERID'), strip_tags($msg));
			} else {
				$error = "ERROR! Please contact system administrator";
				$this->Session->add('ERROR', $error);
				$this->App->getLog()->write($this->Session->value('USERID'), $error, 'error');
			}
		
			_locate($this->ModuleUri."roles");
		}

		public function resetPassword() {

			$filters = array(
				'resultset' => 'row',
				'where' => array("main.id" => $this->App->getSession()->value('USERID'))
			);

			$account = $this->Model->getAccountsByFilter($filters);

			$body = array(
				'action' => 'add',
				'account' => $account
			);

			$this->App->getTheme()->setLibraries(array('validate'));
			$this->App->setBodyData($body);
			$this->App->display('reset-password');
		}

		public function postResetPassword() {

			$filters = array(
				'resultset' => 'row',
				'where' => array("main.id" => $this->App->getSession()->value('USERID'))
			);

			$account = $this->Model->getAccountsByFilter($filters); //pr($account);

			$error = '';
			if(_hash($this->Request->post('current_password')) != $account['strPassword']) {
				$error = 'Provided current password is wrong';
			} else if(!$this->Request->check('password')) {
				$error = 'Please enter new password';
			} else if(strlen($this->Request->post('password')) < 8 || strlen($this->Request->post('password')) > 16) {
				$error = 'Password must be 8 to 16 chars long';
			} else if($this->Request->post('password') != $this->Request->post('confirm_password')) {
				$error = 'Password and Confirm Password must be same';
			}
			
			if(!empty($error)) {
				$this->Session->add('ERROR', $error);
				_locate('referer');
			}
			
			$password = _hash($this->Request->post('password'));
			$data = array(
				'password' => $password, 
				'id' => $account['id']
			);

			$result = $this->Model->resetPassword($data); //var_dump($result); exit;

			if($result === false) {
				$this->Session->add('ERROR', 'ERROR! Please contact system administrator');
				_locate('referer');
			}
			
			$this->Session->add('MSG', 'Your password has been successfully reset. You can login to your account now using new password.');
			_locate(SITE_URL);
		}
	}