<style>
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
    	padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData'); ?>
<!-- add/edit facility contact modal starts-->
<!-- <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Facility Contact</h4>
      </div>
      <div class="modal-body">
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Contact Name</label>
        		</div>
        		<div class="col-xs-12 col-lg-6">
					<input type="text" class="form-control" placeholder="Contact Name" name="contact_name" id="contact_name" required>
        		</div>
        	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="facility_contact_save">Save changes</button>
      </div>
    </div>/.modal-content
  </div>/.modal-dialog
</div><! --><!--  /.modal -->

<!-- add/edit facility contact modal ends-->

	<div class="row main-div" style="min-height:400px;">
		<div class="row">
			 <div class="col-md-2 col-sm-10 pull-right" style="padding: 0px;">
  				<div class="col-md-10 col-sm-6 form-group" style="padding: 0px;">
				<a href="<?php echo base_url(); ?>inventory/index/U" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #A30A0C;" id="open_receipt">View Utilization Record</a>
			</div>
		</div>
	</div>
		<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
			<?php //include("left_nav_bar.php");
			$this->load->view("inventory/components/left_nav_bar"); ?>
		</div>

		<div class="col-lg-9 col-md-7 col-sm-12 col-xs-12">
		<div class="row">
			
			<div class="col-lg-12">

		
				
		<div class="panel panel-default" id="add_transfer_out_panel">

		<div class="panel-heading">
			<h4 class="text-center" style="color: white;" id="form_head"><?php if(isset($edit_flag)){if($edit_flag==0){ echo "Add Utilization Information"; }else{ echo "Update Utilization Information";}}else{ echo "Update Utilization Information";} ?></h4>
		</div>
<div class="row">
<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){  ?>
			

			<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				
				<?php } ?>
</div>
		<div class="panel panel-body">
			<?php
           $attributes = array(
              'id' => 'Add_utilization_form',
              'name' => 'Add_utilization_form',
               'autocomplete' => 'off',
            );
          // echo $receipt_details[0]->Receipt_Date;
           //print_r($edit_flag);
           if(isset($receipt_details[0]->receipt_id)){
           	$receipt_id=$receipt_details[0]->receipt_id;
           }
           else{
           	$receipt_id=NULL;
           }
           echo form_open('Inventory/AddUtilization/'.$receipt_id, $attributes); ?>
			<div class="row">
					
				<div class="col-xs-2 col-md-2 text-left" id="labelfor">
				<label for="item_type">Item Name <span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-md-4" id="item_div">
					<select name="item_type" id="item_type" required class="form-control">
						<option>--Select--</option>
					<?php foreach ($receiptitems as $value) { ?>
					<option value="<?php echo $value->id_mst_drugs; ?>" <?php if(isset($receipt_details[0]->id_mst_drugs)){ if($receipt_details[0]->id_mst_drugs==$value->id_mst_drugs) { echo 'selected';}}?>>
							<?php if($value->type==2){echo $value->name; } else{
								echo $value->name." (".$value->strength.")";
							} ?></option>
					<?php } ?>
					</select>
				</div>

					<div class="col-xs-2 col-md-2 text-left" id="labelforbatch1">
					<label for="batch_num">Batch Number<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-xs-4 col-md-4" id="batch_num_div1">
					<select name="batch_num" id="batch_num" required class="form-control">
					<option>--Select--</option>
					<!-- <option value="999" <?php if(isset($receipt_details[0]->id_mst_drugs)){ if($receipt_details[0]->id_mst_drugs=='999') { echo 'selected';}}?>>other</option> -->
					</select>
					
				</div>
			</div>

<br>
<div class="row">

	<div class="col-xs-2 text-left" >
					<label for="date">From Date<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-xs-4 col-md-4">
					<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="Transfer Date" name="Entry_Date" id="Entry_Date" required onchange="checkdates();"onkeydown="return false" onkeyup="return false" value="<?php if(isset($receipt_details[0]->Entry_Date)){ echo timeStampShow($receipt_details[0]->Entry_Date);} ?>">
				</div>

						<div class="col-xs-2 text-left">
					<label for="Expiry_Date">To Date <span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-xs-4">
					<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="Expiry Date" name="Expiry_Date" id="Expiry_Date" onchange="checkdates();" required onkeydown="return false" onkeyup="return false" value="<?php if(isset($receipt_details[0]->Expiry_Date)){ echo timeStampShow($receipt_details[0]->Expiry_Date);} else {echo timeStampShow(date('Y-m-d')); }?>">
				</div>
</div>
	<br>
<div class="row">
<div class="col-xs-2 text-left" >
					<label for="quantityRem" id="quantityRemlbl"><?php if(isset($receipt_details[0]->type)){ if($receipt_details[0]->type==1){echo "Quantity of bottles Available";} elseif ($receipt_details[0]->type==2) {
						echo "Number of screening tests Available";
					}}else{echo "Quantity of bottles available (drugs)/Number of screening tests Available (kits)";}?><span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-xs-4">
					<input type="number" class="form-control" name="quantityRem" min="1" id="quantityRem" required value="<?php if(isset($receipt_details[0]->Avaiable_quantity)){ echo $receipt_details[0]->Avaiable_quantity;}?>" readonly>
				</div>					
<div class="col-xs-2 text-left" >
					<label for="quantity" id="quantitylbl"><?php if(isset($receipt_details[0]->type)){ if($receipt_details[0]->type==1){echo "Number of drug bottles Dispensed";} elseif ($receipt_details[0]->type==2) {
						echo "Number of Screening tests Performed";
					}}else{echo "Number of drug bottles dispensed/screening tests performed";}?><span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-xs-4">
					<input type="number" class="form-control" name="quantity" min="1" max="" required id="quantity" value="<?php if(isset($receipt_details[0]->quantity)){if($receipt_details[0]->quantity==0){echo $receipt_details[0]->quantity_screening_tests;} else{echo $receipt_details[0]->quantity;}}?>">
				</div>
	</div>
	<br>

	<div class="row">
					<div class="col-xs-12 text-center">
						<button type="submit" class="btn btn-warning" id="submit_btn"><?php if(isset($edit_flag)){if($edit_flag==0){ echo "Add Utilization Information"; }else{ echo "Update Utilization Record";}}else{ echo "Update Receipt";} ?></button>
					</div>
				</div>
			<?php echo form_close();?>
<br>

		</div>
	</div>
</div>
</div>

</div>
<br>
</div>
	<div class="overlay">
	<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
</div>

	<!-- Data Table -->
<script type="text/javascript">
	
	var findrem=[];


function isNumberKey(evt)
	{
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;

	
		if(evt.srcElement.id == 'Phone1' && $('#Phone1').val() > 999999999)
		{
			$("#modal_header").html('Contact number must be 10 digits');
			$("#modal_text").html('Please fill in appropriate Contact Number');
			$("#multipurpose_modal").modal("show");
			return false;
		}
	
		if(evt.srcElement.id == 'PinCode' && $('#PinCode').val() > 99999)
		{
			$("#modal_header").html('PIN cannot be more than 6 digits');
			$("#modal_text").html('Please fill in appropriate PIN');
			$("#multipurpose_modal").modal("show");
			return false;
		}

		return true;
	}
	
	
$(document).ready(function(){
 
            $('#item_type').change(function(){ 
               trasfer_out_source();
            }); 
            <?php if($edit_flag==1){?>
             $('#item_type').ready(function(){ 
               trasfer_out_source();
            }); <?php }else { ?>

            	 $('#item_type').on('load',function(){ 
               trasfer_out_source();
            });<?php } ?>
     
       
        });
 $('#batch_num').change(function(){ 
               findRemaining();
            }); 
 $('#quantity').change(function(){ 
           var rem_value=$('#quantityRem').val();
           var entered_val=$('#quantity').val(); 
          // alert(rem_value+"/"+entered_val);
           if(parseInt(entered_val)>parseInt(rem_value)){
           	alert('Entered value cannot be grater from Available value');
           	$('#quantity').val('');
           }   
            }); 
function trasfer_out_source(){
	var id=$('#item_type').val();
	var parm = 'U';
	var type_id=[];
	var b_num=<?php if(isset($receipt_details[0]->batch_num)){ echo "'".$receipt_details[0]->batch_num."'";}else{echo '0';}?>;
	if(id!=null){
                $.ajax({
                    url : "<?php echo site_url('inventory/get_batch_num');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                        // console.log(data);
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
                        findrem=data;
        				var html = returned.batch_nums.map(function(e) {
   						return e.batch_num;
						});
						var To_Date = returned.batch_nums.map(function(e) {
   						return e.To_Date;
						});
						type_id= returned.batch_nums.map(function(e) {
   						return e.type;
						});
						//alert(To_Date);
                        if(type_id[0] == 1){
			$('#quantityRemlbl').html('');
			$('#quantityRemlbl').html('Quantity of bottles available');
			$('#quantitylbl').html('');
			$('#quantitylbl').html('Number of drug bottles dispensed');
		}
		 if(type_id[0] == 2){
			//console.log('hi');
			$('#quantityRemlbl').html('');
			$('#quantityRemlbl').html('Number of screening tests available');
			$('#quantitylbl').html('');
			$('#quantitylbl').html('Number of Screening tests performed');
		}

                        var i;
                        var opt='';
                        opt +='<option>-----Select-------</option>';
                        for(i=0; i<html.length; i++){

								if(html[i]==b_num)
								{
								select='selected';
								alert(select);
								}else{
								select='';
								}
                        
                        	opt += '<option value="'+html[i]+'"'+select+'>'+html[i]+'</option>'; 	
                   
                            
                        }
                        //console.log(opt);
                        $('#batch_num').html(opt);
                    
 						
                    }
                });
           }
                return false;
}	
function findRemaining(){
	var id=$('#batch_num').val();
	var item_id=$('#item_type').val();

	var type_id=[];
                        var returned = JSON.parse(findrem);
        				type_id= returned.batch_nums.map(function(e) {
   						return e.type;
						});
					
						fill_date();

		
                $.ajax({
                    url : "<?php echo site_url('inventory/get_rem_val');?>",
                    method : "POST",
                    data : {id: id,type: type_id[0]},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        console.log(data);
                        var returned = JSON.parse(data);
        				console.log(returned);
  						var rem  = returned.Rem_value.map(function(e) {
   						return e.Remaining;
						});
                    $('#quantityRem').val(rem);
                    }

                });
                return false;
}	
function fill_date(){
	var id=$('#batch_num').val();
		 $.ajax({
                    url : "<?php echo site_url('inventory/get_maxdate_val');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        //console.log(data);
                        var returned = JSON.parse(data);
        				console.log(returned);
  						var Last_date= returned.Last_date.map(function(e) {
   						return e.Last_Date;
						});
						//alert(Last_date);
                    $('#Entry_Date').val(Last_date);
                    if((isNaN(Last_date))){
                    		$('#Entry_Date').attr('disabled','disabled');
                    }
                    else{
                    	$('#Entry_Date').removeAttr('disabled');
                    }
                    }

                });
}
function checkdates()
	{
		var Entry_Date = $('#Entry_Date').datepicker('getDate');
		var Expiry_Date = $('#Expiry_Date').datepicker('getDate');
   		//alert(Entry_Date+'/'+Expiry_Date);
        if ( Entry_Date > Expiry_Date && Expiry_Date!=null) { 
        	$("#modal_header").text("To date cannot be before From Date");
			$("#modal_text").text("Please check dates");
			$("#Expiry_Date" ).val('');
			$("#multipurpose_modal").modal("show");
			return false;
        }
        if ( Entry_Date-Expiry_Date==0 && Expiry_Date!=null) { 
        	$("#modal_header").text("From Date and To Date cannot be same");
			$("#modal_text").text("Please check dates");
			$("#Expiry_Date" ).val('');
			$("#Entry_Date" ).val('');
			fill_date();
			$("#multipurpose_modal").modal("show");
			return false;
        }
 		else{
 			 return true;
 		}
       
    }

</script>


