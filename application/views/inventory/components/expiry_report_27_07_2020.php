<style>
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: top;
		background-color: #085786;
		color: white;
	}
	td{
		text-align: center; 
		vertical-align: middle;
	}
	.btn-width{
	width: 12%;
	margin-top: 30px;
}
	.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
	.item{
		width: 140px !important;
	}
	.batch_num{
		width: 100px !important;
	}
	a{
		cursor: pointer;
	}

	a:hover,a:focus {
		cursor: pointer;
		text-decoration: none;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	select[readonly] {
		background: #eee;
		pointer-events: none;
		touch-action: none;
	}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData');
$filters = $this->session->userdata('filters');
//echo "<pre>"; print_r($filters);

if($loginData->user_type==1) { 
	$select = '';
}else{
	$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
	$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
	$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
	$select4 = '';	
}
?>
<?php $loginData = $this->session->userdata('loginData');
$filters1=$this->session->userdata('filters1');
if ($loginData->user_type==2 || $filters1['id_mstfacility'] !=''){
	$colspan="17";
	$th="<th>Batch Number</th>";
	$item_text="Item";
}
else if (($loginData->user_type==1 || $loginData->user_type==3)&&$filters1['id_mstfacility']==''){
	$colspan="16";
	$th=" ";
	if (($loginData->user_type==1 || $loginData->user_type==3)|| $filters1['id_search_state'] !=''){
	$item_text="Facility";
}
}
?>
<div class="row main-div" style="min-height:400px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xl-12">
		<div class="panel panel-default" id="Record_receipt_panel">
			<div class="panel-heading" style=";background: #333;">
				<h4 class="text-center" style="color: white;background: #333" id="Record_form_head">Stock Expiry Report</h4>
			</div>
			<div class="row">
				<?php 
				$tr_msg= $this->session->flashdata('tr_msg');
				$er_msg= $this->session->flashdata('er_msg');
				if(!empty($tr_msg)){  ?>


					<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
						<div class="hpanel">
							<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<?php echo $this->session->flashdata('tr_msg');?>. </div>
							</div>
						</div>
					<?php } else if(!empty($er_msg)){ ?>
						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('er_msg');?>. </div>
								</div>
							</div>

						<?php } ?>
					</div>
					<?php
					$attributes = array(
						'id' => 'Receipt_filter_form',
						'name' => 'Receipt_filter_form',
						'autocomplete' => 'false',
					);
//pr($items);
//print_r($receipt_details);
//pr($Closing_stock_Repo);
					echo form_open('', $attributes); ?>

					<div class="row" style="margin-left: 10px;">
						<!-- <div class="col-md-2">
							<label for="">State</label>
							<select type="text" name="search_state" id="search_state" class="form-control input_fields" <?php  ?>  <?php echo $select2.$select3.$select4; ?>>
								<option value="">All States</option>
								<?php 
								foreach ($states as $state) {
									?>
									<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo ucwords(strtolower($state->StateName)); ?></option>
									<?php 
								}
								?>
							</select>
						</div>
						<div class="col-md-2">
							<label for="">District</label>
							<select type="text" name="input_district" id="input_district" class="form-control input_fields"  <?php echo $select.$select2.$select4; ?>>
								<option value="">All District</option>
								<?php 
						/*foreach ($districts as $district) {
						?>
						<option value="<?php echo $district->id_mstdistrict; ?>" <?php echo (count($default_districts)==1 && $default_districts[0]->id_mstdistrict == $district->id_mstdistrict)?'selected':''; ?>><?php echo ucwords(strtolower($district->DistrictName)); ?></option>
						<?php 
						}*/
						?>
						</select>
						</div>
						
						<div class="col-md-2">
							<label for="">Facility</label>
							<select type="text" name="mstfacilitylogin" id="mstfacilitylogin" class="form-control input_fields"  <?php echo $select.$select2; ?>>
								<option value="">All Facility</option>
								<?php 
						/*foreach ($facilities as $facility) {
						?>
						<option value="<?php echo $facility->id_mstfacility; ?>" <?php echo (count($default_facilities)==1 && $default_facilities[0]->id_mstfacility == $facility->id_mstfacility)?'selected':''; ?>><?php echo ucwords(strtolower($facility->facility_short_name)); ?></option>
						<?php 
						}*/
						?>
						</select>
						</div> -->
<div class="col-xs-3 col-md-2">
	<label for="item_type">Item Name <span class="text-danger">*</span></label>
	<div class="form-group">
		<select name="item_type" id="item_type" required class="form-control">
										<option value="drugkit" <?php if($this->input->post('item_type')=='drugkit') { echo 'selected';}?>>All Kits and Drugs</option>
										<option value="Kit"<?php if($this->input->post('item_type')=='Kit') { echo 'selected';}?>>All Screening Test Kits</option>
										<option value="drug"<?php if($this->input->post('item_type')=='drug') { echo 'selected';}?>>All Drugs</option>
										
										<?php foreach ($items as $value) { ?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php if($value->type==2){echo $value->drug_name; } else{
													echo $value->drug_name." ".$value->strength." mg";
												} ?></option>
											<?php } ?>
											<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
										</select>
		</div>
	</div>
	<div class="col-md-2 col-sm-12">
									<label for="">From Date</label>
									<select name="expiry_filter" id="expiry_filter" required class="form-control">
										<option value="">---All---</option>
										
										<?php foreach ($Expire_filter as $value) { ?>
											<option value="<?php echo $value->LookupCode;?>" <?php if($this->input->post('expiry_filter')==$value->LookupCode) { echo 'selected';}?>>
           							<?php echo $value->LookupValue; ?></option>
											<?php } ?>
											<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
										</select>
									</div>
<div class="col-lg-2 col-md-2 col-sm-12 btn-width">
					<button type="submit" class="btn btn-block btn-success" id="monthreport" style="line-height: 1; font-weight: 600;font-size: 15px;height: 30px;">Search</button>
				</div><span style="margin-top: 25px;margin-right: 25px; float: right;"><a href="javascript:void(0)"><i class="fa fa-2x fa-download" id="excel_button" onclick="tableToExcel('testTable', 'W3C Example','Monthly_Report.xls')" title="Excel Download"></i></a></span>

<?php echo form_close(); ?>
</div>

</div>

<table class="table table-striped table-bordered table-hover" id="table_inventory_list">


	<thead>
		<tr>
			<th>Item</th>
			<th>Batch Num</th>
			<th>Date of Receipt</th>
			<th>Expiry Date</th>
			<th>Quantity of <br>bottles/screening<br> tests available</th>
			<th>Expiry Status</th>
			<th>Stock of drugs/screening <br>tests required for<br> six months based <br>on the existing  stock</th>
			<th>Reccomendation</th>
			<th>If no action taken,<br> stock that will expire</th>
			<th>Action</th>


  

		</tr>
	</thead>
	<tbody>
<!-- </tbody>
</table> -->
<?php // echo "<pre>";/*print_r($inventory_details);*/
//$result=array_unique($inventory_details->);
if ($loginData->user_type==2 || $filters1['id_mstfacility'] !=''){
	$item_id=array();
	$data=array();
	$filtered_items=array();
	foreach ($Inventory_Repo as $key => $value) {
    $filtered_items[$key]['id_mst_drugs'] =$value->drug_name;
    $filtered_items[$key]['strength'] =$value->strength;
    /*$filtered_items['id_mst_drugs'] = $value->id_mst_drugs;
    $filtered_items['id_mst_drugs'] = $value->id_mst_drugs;*/
}
	foreach ($items as $key => $value) {
		foreach ($filtered_items as $key1 => $value1) {
			if($value->id_mst_drugs==$filtered_items[$key1]['id_mst_drugs']){
				$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
				$item_id[$key]['drug_name']= $value->drug_name;
				$item_id[$key]['strength']= $filtered_items[$key1]['strength'];
			}
		}

	}
	$filtered_arr=array_unique($item_id,SORT_REGULAR);
//pr($filtered_arr);exit();
//pr($inventory_details);
	?>
<!-- <table class="table table-striped table-bordered table-hover">
	<tbody> -->
		<?php if(!empty($Inventory_Repo)){
			foreach ($filtered_arr as $key=>$value_temp) { ?>
				<tr>
					<th colspan="9" class="info"style="border: none;background-color: #333;font-size: 16px;color:#fff;text-align: left;"><?php echo $value_temp['drug_name']; ?></th>
					<th class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus item_toggle" aria-hidden="true" id="<?php echo 'head'.$value_temp['id_mst_drugs']; ?>"></i></th></tr>
					<?php foreach ($Inventory_Repo as $key => $value) { 
						if($value_temp['id_mst_drugs']==$value->drug_name){
							?>
							<tr class="<?php echo 'item'.$value_temp['id_mst_drugs']; ?> item_row">
								<td class="item">
									<?php if($value->type==1){echo $value_temp['drug_name']." ".$value->strength." mg";} elseif($value->type==2){echo $value_temp['drug_name'];} ?>
								</td>
							 <td>
									<?php echo $value->batch_num; ?>
								</td>
								<td nowrap>
													<?php echo timeStampShow($value->Entry_Date); ?>
												</td>
								<td>
									<?php echo timeStampShow($value->Entry_Date);?>
									</td>
									<td>
										<?php 
										$closing_stock=($value->total_receipt-($value->returned_quantity+$value->total_rejected+$value->total_utilize+$value->total_transfer_out+$value->Expire_stock));

										if($closing_stock==NULL){
											echo '-';
										}else{echo $closing_stock;} ?>
									</td>
									<?php 
									if($value->days>180){
										echo '<td style="background-color:green;">Okay</td>';
										}
										else if($value->days<=180 && $value->days>0){
										echo '<td style="background-color:yellow;">Near to Expiry</td>';
										}
										else if($value->days<=0){
										echo '<td style="background-color:red;">Expired</td>';
										}
										?>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
								</tr>
<!-- <td class="text-center">
<a href="<?php echo site_url()."/inventory/AddReceipt/".$value->receipt_id;?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
<a href="<?php echo site_url()."/inventory/delete_receipt/".$value->receipt_id."/R";?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
</td>

</tr> -->
<?php } }}}else{ ?>
	<tr >
		<td colspan="<?php echo $colspan; ?>" class="text-center">
			NO RECORDS FOUND  BETWEEN SELECTED DATES.
		</td>

	</tr>
<?php } }

else if (($loginData->user_type==1 || $loginData->user_type==3)&&$filters1['id_mstfacility']==''){
	$item_id=array();
	$data=array();
	$filtered_items=array();
	foreach ($Inventory_Repo as $key => $value) {
    $filtered_items[$key]['id_mst_drugs'] =$value->drug_name;
    $filtered_items[$key]['strength'] =$value->strength;
    /*$filtered_items['id_mst_drugs'] = $value->id_mst_drugs;
    $filtered_items['id_mst_drugs'] = $value->id_mst_drugs;*/
}
	foreach ($items as $key => $value) {
		foreach ($filtered_items as $key1 => $value1) {
			if($value->id_mst_drugs==$filtered_items[$key1]['id_mst_drugs']){
				$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
				$item_id[$key]['drug_name']= $value->drug_name;
				$item_id[$key]['strength']= $filtered_items[$key1]['strength'];
			}
		}

	}
	$filtered_arr=array_unique($item_id,SORT_REGULAR);
//pr(($filtered_items));
//pr($Inventory_Repo);exit();
	?>
<!-- <table class="table table-striped table-bordered table-hover">
	<tbody> -->
		<?php if(!empty($Inventory_Repo)){
			
			foreach ($filtered_arr as $key=>$value_temp) { 
				?>
				<tr>
					<td class="info"style="font-size: 12px;font: bold; text-align: left;"><?php echo $value_temp['drug_name']; ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				
					
					<td class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus item_toggle" aria-hidden="true" id="<?php echo 'item'.$value_temp['id_mst_drugs']; ?>"></i></td></tr>
					<?php foreach ($Inventory_Repo as $key => $value) { 
						if($value_temp['id_mst_drugs']==$value->drug_name){?>
							<tr class="<?php echo 'item'.$value_temp['id_mst_drugs']; ?>">
								<td class="item">

									<?php if(($loginData->user_type==1) &&($filters1['id_search_state']=='')){
										echo $value->StateName;
									}
									else if($loginData->user_type==3 ||($filters1['id_search_state']!='')){
										echo $value->hospital;
									}

									?>
								</td>
								<td>
									&nbsp;
								</td>
								<td>
									<?php echo '-' ?>
								</td>
								
								<td>
									<?php
										echo '-';
									?>
									</td>
									<td>
										<?php 
										$total_expired=0;
									
										$closing_stock=($value->total_receipt-($value->returned_quantity+$value->total_rejected+$value->total_utilize+$value->total_transfer_out+$value->Expired_stock));

										if($closing_stock==NULL){
											echo $closing_stock;
										}else{echo $closing_stock;} ?>
									</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								
										<?php if(($loginData->user_type==1) &&($filters1['id_search_state']=='')){?>
										<td class="info" style="border: none;text-align: right;"><a href="javascript:void(0);" onclick="get_facility_data($(this).parent().closest('tr').index(),'<?php echo $value->type;?>','<?php echo $value->id_mststate;?>','<?php echo $value->id_mst_drugs;?>');"class="fa fa-plus facility_td" aria-hidden="true" id="<?php echo $value->id_mst_drugs.'-'.$value->id_mststate; ?>"></a></td>
									<?php } 
									else if($loginData->user_type==3 ||($filters1['id_search_state']!='')){ ?>
										<td class="info" style="border: none;text-align: right;"><a href="javascript:void(0);" onclick="get_item_data($(this).parent().closest('tr').index(),'<?php echo $value->type;?>','<?php echo $value->id_mstfacility;?>','<?php echo $value->id_mst_drugs;?>');"class="fa fa-plus item_td" aria-hidden="true" id="<?php echo $value->id_mst_drugs.'-'.$value->id_mstfacility; ?>"></a></td>
									<?php } ?>

									</tr>
									
<!-- <td class="text-center">
<a href="<?php //echo site_url()."/inventory/AddReceipt/".$value->receipt_id;?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
<a href="<?php //echo site_url()."/inventory/delete_receipt/".$value->receipt_id."/R";?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
</td>

</tr> -->
<?php } }}}else{ ?>
	<tr >
		<td colspan="<?php echo $colspan; ?>" class="text-center">
			NO RECORDS FOUND  BETWEEN SELECTED DATES.
		</td>

	</tr>
<?php } }?>

</tbody>
</table>
</div>
<br>
</div>
<div class="overlay">
	<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
</div>

<!-- Data Table -->
<script type="text/javascript">

	
	$(document).ready(function(){
		$(".glyphicon-trash").click(function(e){
			var ans = confirm("Are you sure you want to delete?");
			if(!ans)
			{
				e.preventDefault();
			}
		});
		$(".facility_td").click(function(e){
			$(this).toggleClass('fa-plus fa-minus');
			var abc=$(this).attr("id");
			$('.'+abc).toggle();
			if ($('.'+abc).is(':visible')==false) {
				
  				$('.item_row').hide();
			}
		});
		$(".item_toggle").click(function(e){
			$(this).toggleClass('fa-plus fa-minus');
			
			
			<?php if($loginData->user_type!=2){?>
				var abc=$(this).attr("id");
			$('.'+abc).toggle();

			if ($('.'+abc).is(':visible')==false && $('.item_row').is(':visible')==true) {
  				$('.item_row').hide();
			}
			
<?php }else{?>
		$('.item_row').toggle();
	<?php }?>
		});
		


		
	});


		$(document).ready(function(){

		$("#search_state").trigger('change');
//alert('rff');

});
	$('#search_state').change(function(){

		$.ajax({
			url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
			data : {
				<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
			},
			method : 'GET',
			success : function(data)
			{
//alert(data);
$('#input_district').html(data);
$("#input_district").trigger('change');
//$("#mstfacilitylogin").trigger('change');

},
error : function(error)
{
//alert('Error Fetching Districts');
}
})
	});

	$('#input_district').change(function(){

		$.ajax({
			url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),

			method : 'GET',
			success : function(data)
			{
//alert(data);
if(data!="<option value=''>Select Facilities</option>"){
	$('#mstfacilitylogin').html(data);

}
},
error : function(error)
{
//alert('Error Fetching Blocks');
}
})
	});

function  get_facility_data(row_index,type,id_mststate,id_mst_drugs){

//alert(row_index);
var dataString = {
id_mst_drugs: id_mst_drugs,
id_mststate: id_mststate,
type: type,

};
$.ajax({    
url: '<?php echo base_url();?>Inventory_Reports/get_stock_expiry_data',
type: 'GET',
data: dataString,
dataType: 'json',
async: false,
cache: false,
success: function (data) {


var parsedData = JSON.parse(data.fields);
//console.log(parsedData);
var output = [];

for(var i=0; i<parsedData.length; i++){
var closing_stock=(parseInt((parsedData[i]['total_receipt'] || 0))-(parseInt(parsedData[i]['returned_quantity']|| 0)+parseInt(parsedData[i]['total_rejected']|| 0)+parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_transfer_out']|| 0)+parseInt(parsedData[i]['Expired_stock'] || 0)));
var facility=parsedData[i]['id_mstfacility'];

	str='<tr class="'+id_mst_drugs+'-'+id_mststate+' item'+id_mst_drugs+'">';
str+='<td>'+parsedData[i]['hospital'] +'</td> <td></td>'
	+' <td></td>'
	+' <td></td>'
	+' <td>'+closing_stock+'</td>'
	+' <td></td>'
	+' <td></td>'
	+' <td></td>'
	+' <td></td>'
	+' <td class="info" style="border: none;text-align: right;"><a class="fa fa-plus item_td"  href="javascript:void(0);" onclick="get_item_data($(this).parent().closest('+"'tr'"+').index(),'+"'"+type+"'"+','+"'"+facility+"'"+','+"'"+id_mst_drugs+"'"+');"  aria-hidden="true" id="'+id_mst_drugs+'-'+facility+'"></a></td>';
str+='</tr>';
output.push(str);
closing_stock=0;
}
 $('table > tbody > tr').eq(row_index).after(output);
 $('#'+id_mst_drugs+'-'+id_mststate).removeAttr('onclick');
 $('.'+id_mst_drugs+'-'+id_mststate).hide();
   }
});
}

function  get_item_data(row_index,type,id_mstfacility,id_mst_drugs){


var dataString = {
id_mst_drugs: id_mst_drugs,
id_mstfacility: id_mstfacility,
type: type,

};
$.ajax({    
url: '<?php echo base_url();?>Inventory_Reports/get_item_expiry_report',
type: 'GET',
data: dataString,
dataType: 'json',
async: false,
cache: false,
success: function (data) {


var parsedData = JSON.parse(data.fields);
console.log(parsedData);
var output = [];

for(var i=0; i<parsedData.length; i++){
var closing_stock=(parseInt((parsedData[i]['total_receipt'] || 0))-(parseInt(parsedData[i]['returned_quantity']|| 0)+parseInt(parsedData[i]['total_rejected']|| 0)+parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_transfer_out']|| 0)));
var Expire_stock=(parseInt((parsedData[i]['Expire_stock'] || 0))-(parseInt(parsedData[i]['returned_quantity']|| 0)+parseInt(parsedData[i]['total_rejected']|| 0)+parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_transfer_out']|| 0)));
var status='';
if(parsedData[i]['days']>180){
status=	'<td style="background-color:green;">Okay</td>';
}
else if(parsedData[i]['days']<=180 && parsedData[i]['days']>0){
status=	'<td style="background-color:yellow;">Near to Expiry</td>';
}
else if(parsedData[i]['days']<=0){
status=	'<td style="background-color:red;">Expired</td>';
}
if (Expire_stock<0) {
	Expire_stock=0;
}
else{
	closing_stock=closing_stock-Expire_stock;
}
var facility=parsedData[i]['id_mstfacility'];
var EntryDate       =parsedData[i]['Entry_Date'];
	var parts            = EntryDate.split('-');
	var date             =parts[2] + '-'+ parts[1] + '-'+   parts[0];
	var ExpiryDate       =parsedData[i]['Expiry_Date'];
	var parts            = ExpiryDate.split('-');
	var date1             =parts[2] + '-'+ parts[1] + '-'+   parts[0];

	str='<tr class="'+id_mst_drugs+'-'+id_mstfacility+' item_row">';
str+='<td></td> <td>'+parsedData[i]['batch_num'] +'</td>'
	+' <td nowrap>'+date +'</td>'
	+' <td nowrap>'+date1 +'</td>'
	+'<td>'+closing_stock+'</td>'
	+status
	+'<td></td>'
	+'<td></td>'	
	+'<td></td>'
	+'<td></td>'
str+='</tr>';
output.push(str);
closing_stock=0;
}
 $('table > tbody > tr').eq(row_index).after(output);
 $('#'+id_mst_drugs+'-'+id_mstfacility).removeAttr('onclick');
 $('.'+id_mst_drugs+'-'+id_mstfacility).hide();
//alert(output);
   }
});
}
$(document).on('click', ".item_td", function() {
	$(this).toggleClass('fa-plus fa-minus');
			var abc=$(this).attr("id");
			$('.'+abc).toggle();
	});
</script>


