<style>
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.form-control{
		height: 30px;
		font-size: 13px;
	}
	.panel-heading {
    	padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData'); ?>
<!-- add/edit facility contact modal starts-->
<!-- <div class="modal fade" tabindex="-1" role="dialog" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Facility Contact</h4>
      </div>
      <div class="modal-body">
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Contact Name</label>
        		</div>
        		<div class="col-xs-12 col-lg-6">
					<input type="text" class="form-control" placeholder="Contact Name" name="contact_name" id="contact_name" required>
        		</div>
        	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="facility_contact_save">Save changes</button>
      </div>
    </div>/.modal-content
  </div>/.modal-dialog
</div><! --><!--  /.modal -->

<!-- add/edit facility contact modal ends-->

	<div class="row main-div" style="min-height:400px;">
		<div class="row" style="width: 90%">
			 <div class="col-md-2 col-sm-8 pull-right" style="padding: 0px;">
  				<div class="col-md-10 col-sm-6 form-group" style="padding: 0px;">
				<a href="<?php echo base_url(); ?>inventory" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #A30A0C;" id="open_receipt">VIEW Stock Indent</a>
			</div>
		</div>
	</div>
		<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
			<?php //include("left_nav_bar.php");
			$this->load->view("inventory/components/left_nav_bar"); ?>
		</div>

		<div class="col-lg-9 col-md-7 col-sm-12 col-xs-12">
		<div class="row">
			
			<div class="col-lg-10 ">

		
				
		<div class="panel panel-default" id="add_receipt_panel">

		<div class="panel-heading" style="background-color: #333333;font-family: 'Source Sans Pro'">
			<h4 class="text-center" style="color: white;" id="form_head"><?php if(isset($edit_flag)){if($edit_flag==0){ echo "Add Stock Indent"; }else{ echo "Update Receipt";}}else{ echo "Update Receipt";} ?></h4>
		</div>
<div class="row">
<?php 
		$tr_msg= $this->session->flashdata('tr_msg');
		$er_msg= $this->session->flashdata('er_msg');
		if(!empty($tr_msg)){  ?>
			

			<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
				<div class="hpanel">
					<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo $this->session->flashdata('tr_msg');?>. </div>
					</div>
				</div>
			<?php } else if(!empty($er_msg)){ ?>
				<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
					<div class="hpanel">
						<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<?php echo $this->session->flashdata('er_msg');?>. </div>
						</div>
					</div>
				
				<?php } ?>
</div>
		<div class="panel panel-body">
			<?php
           $attributes = array(
              'id' => 'AddStock_indent_form',
              'name' => 'AddStock_indent_form',
               'autocomplete' => 'off',
            );
          // echo $receipt_details[0]->Receipt_Date;
           //echo $receipt_details[0]->rejection;
           //print_r($edit_flag);
          // print_r($source_name);
           if(isset($receipt_details[0]->receipt_id)){
           	$receipt_id=$receipt_details[0]->receipt_id;
           }
           else{
           	$receipt_id=NULL;
           }
           echo form_open('Inventory/AddStock_indent/'.$receipt_id, $attributes); ?>
			<div class="row">
					
				<div class="col-xs-2 col-md-2 text-left" id="labelfor">
				<label for="item_type">Item Name <span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-md-4" id="item_div">
					<select name="item_type" id="item_type" required class="form-control">
					<?php foreach ($items as $value) { ?>
					<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if(isset($receipt_details[0]->id_mst_drugs)){ if($receipt_details[0]->id_mst_drugs==$value->id_mst_drug) { echo 'selected';}}?>>
							<?php if($value->type==2){echo $value->drug_name; } else{
								echo $value->drug_name." (".$value->strength.")";
							} ?></option>
					<?php } ?>
					<!-- <option value="999" <?php if(isset($receipt_details[0]->id_mst_drugs)){ if($receipt_details[0]->id_mst_drugs=='999') { echo 'selected';}}?>>other</option> -->
					</select>
				</div>

				<div class="form-group col-md-2" id="other_div" style="padding: 0px">
					<input type="text" class="form-control" id="other" name="other" placeholder="Specify the Name" value="<?php if(isset($receipt_details[0]->id_mst_drugs)){ if($receipt_details[0]->id_mst_drugs=='999') {  echo $receipt_details[0]->drug_name;}}?>">
				</div>
			<div id="other_div1">
				<div class="col-xs-1 col-md-1 text-left" id="label_type"style="padding: 0px;    width: 5.33333333%;">
				<label for="drug_type">Type<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-md-2 col-xs-6" style="padding: 0px">
					
					<select class="form-control" id="drug_type" name="drug_type">
						<?php foreach ($item_type as $value) { ?>
						<option value="<?php echo $value->LookupCode;?>" <?php if(isset($receipt_details[0]->type)){ if($receipt_details[0]->type==$value->LookupCode) { echo 'selected';}}?>>
							<?php echo $value->LookupValue; ?></option>
					<?php } ?>
					</select>
				</div>
			</div>

					<div class="col-xs-2 text-left state_districts" >
					<label for="quantity">Quantity of Stock received (Bottles/Kits)<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-xs-4">
					<input type="number" class="form-control" name="quantity" min="1" max="10000" required value="<?php if(isset($receipt_details[0]->quantity)){ echo $receipt_details[0]->quantity;}?>">
				</div>

			</div>

<br>
<div class="row">

	<div class="col-xs-2 text-left" >
					<label for="date">Date of placing indent<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-xs-4">
					<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="Receipt Date" name="Entry_Date" id="Entry_Date" value="<?php if(isset($receipt_details[0]->Entry_Date)){ echo timeStampShow($receipt_details[0]->Entry_Date);} else {echo timeStampShow(date('Y-m-d')); } ?>" required onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>" onchange="checkexpirydate()">
				</div>
<div class="col-xs-2 col-md-2 text-left" id="labelforbatch">
					<label for="batch_num">Indent Number <span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-xs-4 col-md-4" id="batch_num_div">
					<input type="text" class="form-control" id="batch_num" name="batch_num" placeholder="Indent Number" pattern="^[a-zA-Z0-9]+$" required onkeyup="this.value = this.value.toUpperCase();" value="<?php if(isset($receipt_details[0]->batch_num)){ echo $receipt_details[0]->batch_num;}?>" disabled="true">
					
				</div>

	</div>
<br>

<br>
<div class="row">
					
				<div class="col-xs-2 text-left">
					<label for="rejection">Remarks</label>
				</div>
				<div class="form-group col-xs-4">
				<textarea rows="4" cols="50" class="form-control" id="remark" name="rejection"><?php if(isset($receipt_details[0]->rejection)){ echo $receipt_details[0]->rejection;} ?>
</textarea>
				</div>
			</div>


<br>
				<div class="row">
					<div class="col-xs-12 text-center">
						<button type="submit" class="btn btn-warning" id="submit_btn"><?php if(isset($edit_flag)){if($edit_flag==0){ echo "Save"; }else{ echo "Update";}}else{ echo "Update";} ?></button>
					</div>
				</div>
			<?php echo form_close();?>
<br>
<!-- 	<div class="row">
		<div class="col-lg-12">
			<table class="table table-hover table-striped table-bordered">
			<thead>
				<th class="text-left">Facility Contacts</th>
				<th class="text-center">Commands <span style="color: gray; cursor: pointer;font-size: 18px;
"><i class="fa fa-plus-circle pull-right" title="Add Facility Contact" data-toggle="tooltip" id="contact_modal"></i></span></th>
			</thead>
			<tbody id="facility_contact_body">
				<?php if(isset($facility_contact)){
					foreach ($facility_contact as $row) {
						?>
						 <tr>
						 <td class="text-center"><?php echo $row->Name; ?></td>
						 <td class="text-center">
							<a href="#" style="padding : 4px;" title="Edit" data-edit_contact_id = "<?php echo $row->id_mstfacilitycontacts; ?>"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
							<a href="#" style="padding : 4px;" title="Delete" data-delete_contact_id = "<?php echo $row->id_mstfacilitycontacts; ?>"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
						</td>
						 </tr>
						
					<?php  } }
				 ?>
			</tbody>
		</table>
		</div>
	</div> -->
		</div>
	</div>
</div>
</div>

</div>
<br>
</div>
	<div class="overlay">
	<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
</div>

	<!-- Data Table -->
<script type="text/javascript">

function isNumberKey(evt)
	{
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;

	
		if(evt.srcElement.id == 'Phone1' && $('#Phone1').val() > 999999999)
		{
			$("#modal_header").html('Contact number must be 10 digits');
			$("#modal_text").html('Please fill in appropriate Contact Number');
			$("#multipurpose_modal").modal("show");
			return false;
		}
	
		if(evt.srcElement.id == 'PinCode' && $('#PinCode').val() > 99999)
		{
			$("#modal_header").html('PIN cannot be more than 6 digits');
			$("#modal_text").html('Please fill in appropriate PIN');
			$("#multipurpose_modal").modal("show");
			return false;
		}

		return true;
	}
	$(document).ready(function(){
		<?php if(isset($receipt_details[0]->id_mst_drugs)){
			if($receipt_details[0]->id_mst_drugs==999){?>
				show_hide_other_div();
			<?php }else{ ?>
				 $("#other_div").hide();
   				$("#other_div1").hide();
   				$('#other').removeAttr('required');
    			$('#drug_type').removeAttr('required');<?php }} else{ ?>
				 $("#other_div").hide();
   				$("#other_div1").hide();
   				$('#other').removeAttr('required');
    			$('#drug_type').removeAttr('required');
			<?php }
		?>
 
    $('input:text').bind('cut copy paste', function(e) {
    e.preventDefault();
});
    $('#source_div').show();
    $('#unrecognised_div').hide();

    $('#from_to_type').ready(function(){ 
        fill_source_name();        
});
    $('#from_to_type').change(function(){ 
        fill_source_name();        
});
    checkrejction();
});

function fill_source_name(){
var id=$('#from_to_type').val();
if(id!=3){
$('#source_div').show();
            	$('#unrecognised_div').hide();
            	$('#source_name').attr('required','required');
    			$('#unrecognised').removeAttr('required');
}
  var fid=<?php if(isset($receipt_details[0]->source_name)){ echo $receipt_details[0]->source_name;}else{echo '0';}?>;      
				if(id==1){
					var opt='';
					opt += '<option value="996">abc</option>';
					opt += '<option value="997">abcd</option>';
					opt+=  '<option value="998">abcde</option>';
					$('#source_name').html(opt);
				}
                else if(id==2){
                $.ajax({
                    url : "<?php echo site_url('inventory/get_facilities');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
        				var id_mstfacility = returned.id_mstfacility.map(function(e) {
   						return e.id_mstfacility;
						});
						var facility_short_name = returned.id_mstfacility.map(function(e) {
   						return e.facility_short_name;
						});
                        var i;
                        var select=''
                        var opt='';
                        for(i=0; i<id_mstfacility.length; i++){
                        	if(id_mstfacility[i]==fid)
                        	{
                        		select='selected';
                        	}else{
                        		select='';
                        	}
                            opt += '<option value="'+id_mstfacility[i]+'"'+select+'>'+facility_short_name[i]+'</option>';
                        }
                        //console.log(opt);
                        $('#source_name').html(opt);
 						
                    }
                });
            }else if(id==3){

            	$('#source_div').hide();
            	$('#unrecognised_div').show();
            	$('#source_name').removeAttr('required');
    			$('#unrecognised').attr('required','required');

            }
                return false;
            

}

	function show_hide_other_div()
	{
		var idval = $('#item_type').val();
//alert(idval);
if(idval=='999')
{
	$("#other_div").show();
$("#other_div1").show();
	/*$( "#labelfor" ).removeClass( "col-xs-2 col-md-2" ).addClass( "col-xs-2 col-md-1" );*/
	$( "#item_div" ).removeClass( "col-xs-4 col-md-4" ).addClass( "col-xs-2 col-md-2" );
	$( "#batch_num_div" ).removeClass( "col-xs-4 col-md-4" ).addClass( "col-xs-2 col-md-2" );
	$( "#batch_num_div" ).css({'padding-right':'0','margin-left':'-40px'});
	$( "#labelforbatch" ).css({'padding-right':'0','width': '15.33333333%'});
	$('#other').attr('required','required');
	$('#drug_type').attr('required','required');
}
else {
	$( "#item_div" ).removeClass( "col-xs-2 col-md-2" ).addClass( "col-xs-4 col-md-4" );
	$( "#labelfor" ).removeClass( "col-xs-2 col-md-1" ).addClass( "col-xs-2 col-md-2" );
	$( "#batch_num_div" ).removeClass( "col-xs-2 col-md-2" ).addClass( "col-xs-4 col-md-4");
	$( "#batch_num_div" ).css({'padding-right':'','margin-left':''});
	$( "#labelforbatch" ).css({'padding-right':'','width':''});
 $("#other_div").hide();
 $("#other_div1").hide();
  $('#other').removeAttr('required');
    $('#drug_type').removeAttr('required');
}
	}


$('#item_type').change(function(){
show_hide_other_div();
});
$('#Acceptance_Status').change(function(){
checkrejction();
});
function checkrejction(){
	var idval = $('#Acceptance_Status').val();
//alert(idval);
if(idval=='1')
{
	$('#rejection').removeAttr('required');
	$('#rejection').attr('disabled','disabled');
}
else if(idval=='2')
{
	
	$('#rejection').attr('required','required');
	$('#rejection').removeAttr('disabled','disabled');
	//$('#rejection').val('N/A');

}
}
	function checkexpirydate()
	{
		var Entry_Date = $('#Entry_Date').datepicker('getDate');
		var Expiry_Date = $('#Expiry_Date').datepicker('getDate');
   		//alert(Receipt_Date+'/'+Expiry_Date);
        if ( Entry_Date > Expiry_Date && Expiry_Date!=null) { 
        	$("#modal_header").text("Expiry date cannot be before Receipt Date");
			$("#modal_text").text("Please check dates");
			$("#Expiry_Date" ).val('');
			$("#multipurpose_modal").modal("show");
			return false;
        }
 		else{
 			 return true;
 		}
       
    }
$('#batch_num').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9{2}]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test($.trim(str))) {
        return true;
    }
    else{
    	e.preventDefault();
    return false;
    }
    
});
</script>


