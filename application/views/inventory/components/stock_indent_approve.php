<style>
	.isDisabled {
  color: currentColor;
  cursor: not-allowed;
  opacity: 0.5;
  text-decoration: none;
   pointer-events: none;
}
	
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: top;
	}
	#pending_indent>tbody>tr>td
	{
		text-align: center; 
		vertical-align: top;
		word-break: break-all;
		width: 13%;
	}
	td{
		text-align: center; 
		vertical-align: middle;

		
	}
	.table>thead>tr>th{
		vertical-align: top;
	}
	.pending_table>thead>tr>th{
		vertical-align: top;
		word-break: break-all;
	}
	.pending_table>thead>tr>td{
		vertical-align: middle;
		text-align: center;
		word-break: break-all;
	}
	.btn-width{
	width: 12%;
}
	
	.batch_num{
		width: 100px !important;
	}
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : 9999;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
		z-index: 9999 !important;
	}
	input,nav,label,span{
		font-family: 'Source Sans Pro';

	}
	label{
		font-family: 'Source Sans Pro';
		font-size: 14px;
	}
	input,select,.form-control{
		height: 30px;
	}
	.table-bordered>tbody>tr>td{
		height: 45;
	}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData'); ?>
<!-- add/edit facility contact modal starts-->
<div class="modal fade" tabindex="-1" role="dialog" id="myModal1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #333;color: white;">
      	<!-- <img id="loading_gif1" src="<?php echo site_url('application');?>/third_party/images/spinner.gif" alt="loading_gif" /> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_icon" style="color: white;"><span aria-hidden="true" style="color: white;">&times;</span></button>
        <h4 class="modal-title">Approve/Reject Stock Indent Received</h4>
      </div>
      <div class="modal-body">
      	<?php  $attributes = array(
              'id' => 'AddStock_indent_form',
              'name' => 'AddStock_indent_form',
               'autocomplete' => 'off',
            );
           if(isset($inventory_details[0]->inventory_id)){
           	$inventory_id=$inventory_details[0]->inventory_id;
           }
           else{
           	$inventory_id=NULL;
           }
           echo form_open('Inventory/approved_indent/'.$inventory_id, $attributes); ?>
        	
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Total Quantity Indented (kits/bottle/vials)<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<input type="text" class="form-control" name="quantity" id="quantity" required disabled="disabled">
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Quantity Approved (kits/bottle/vials)<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<input type="text" class="form-control" name="approved_quantity" id="approved_quantity" required>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Quantity Rejected (kits/bottle/vials)<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<input type="text" class="form-control" name="quantity_rejected" id="quantity_rejected" required>
        		</div>
        	</div>
        	<div class="row" id="remark_rej" style="display: none;">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Rejected Remark<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<textarea type="text" class="form-control" name="sno_remark" id="sno_remark" ></textarea> 
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Request warehouse to fulfill indent<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<select name="warehouse_name" id="warehouse_name" required class="form-control">
					<option value="">--Select--</option>
					</select>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Warehouse quanitity fulfillment (kits/bottle/vials)<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<input type="text" class="form-control" name="warehouse_request_quantity" id="warehouse_request_quantity" required>
        		</div>
        	</div>
        	<div class="row">
        	<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Request facility transfer from<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<select name="facility_name" id="facility_name" required class="form-control">
					<option value="">--Select--</option>
					</select>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Transfer request quantity (kits/bottle/vials)<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<input type="text" class="form-control" name="requested_quantity" id="requested_quantity" required>
        		</div>
        	</div>
        	<input type="hidden" id="inventory_id" name="inventory_id" value="">
        	<input type="hidden" id="source_name_val" name="source_name_val" value="">
        	<input type="hidden" id="warehouse_val" name="warehouse_val" value="">
        	<!-- <div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Indent Number <span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
        						<input type="text" class="form-control" id="indent_num" name="indent_num" placeholder="Indent Number" readonly>
        		</div>
        		
        	</div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="modal_close">Close</button>
        <button type="submit" class="btn btn-primary" id="submit_btn" name="save" value="save">Save</button>
      </div>
      <input type="hidden" name="already_relocated_quantity" id="already_relocated_quantity" disabled="true">
      <input type="hidden" name="fix_requested_quantity" id="fix_requested_quantity" disabled="true">
      <input type="hidden" name="fix_warehouse_requested_quantity" id="fix_warehouse_requested_quantity" disabled="true">
      <input type="hidden" name="indent_num" id="indent_num">
      <input type="hidden" name="is_temp" id="is_temp">
      <?php echo form_close(); ?>
    </div>
  </div>
</div>

<!-- add/edit facility contact modal ends-->
<!-- <div class="row" style="padding-top: 20px;padding-bottom: 20px;">
	<div class="col-lg-10 text-center">
			<div class="col-md-4 col-md-offset-6" style="padding-right: 15px;">
			<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 15px;padding-bottom: 15px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i> Raise Indent Request
		</a>
		</div>
	</div>
</div> -->

<div class="row main-div" style="margin-top: 1em;margin-bottom: 1em">
	<!-- <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12" style="padding: 0;">
			<?php //include("left_nav_bar.php");
			//$this->load->view("inventory/components/left_nav_bar"); ?>
		</div> -->
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-default" id="Record_receipt_panel">
				<div class="panel-heading" style=";background: #333;">
					<h4 class="text-center" style="color: white;background: #333" id="Record_form_head">Designated Officer Module</h4>
				</div>
				<div class="row">
					<?php 
					$tr_msg= $this->session->flashdata('tr_msg');
					$er_msg= $this->session->flashdata('er_msg');
					if(!empty($tr_msg)){  ?>
						

						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						<?php } else if(!empty($er_msg)){ ?>
							<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
								
							<?php } ?>
		<!-- <div class="col-md-3 pull-right" style="padding-right: 15px;">
			<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 10px;padding-bottom: 10px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i> Raise Indent Request
		</a>
		</div> -->
						</div>
						
						<?php
						$attributes = array(
							'id' => 'Receipt_filter_form',
							'name' => 'Receipt_filter_form',
							'autocomplete' => 'off',
						);
  //pr($get_facilities);
  //print_r($inventory_details);

						echo form_open('', $attributes); ?>
						<div class="row" style="padding-left: 10px;">
							<div class="col-xs-3 col-md-2 form-group">
								<label for="item_type">Facility<span class="text-danger">*</span></label>
					<select name="id_mstfacility" id="id_mstfacility" class="form-control">
					<option value="">--Select All--</option>
					<?php foreach ($get_facilities as $value) { ?>
											<option value="<?php echo $value->id_mstfacility;?>" <?php if($this->input->post('id_mstfacility')==$value->id_mstfacility) { echo 'selected';}?>>
												<?php 
													echo $value->facility_short_name;
											?></option>
											<?php } ?>
					</select>
        		</div>
							<div class="col-xs-3 col-md-2">
								<label for="item_type">Item Name <span class="text-danger">*</span></label>
								<div class="form-group">
									<select name="item_type" id="item_type" required class="form-control">
										<?php inventory_options($this->input->post('item_type')); ?>
										
										<?php foreach ($items as $value) { ?>
											<?php if($value->type!=3) {?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php
												echo ($value->type==2) ? $value->drug_name : $value->drug_name.' '.$value->strength.' '.$value->unit ; ?></option>
											<?php } }?>
											<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
										</select>
									</div>
								</div>
								
								
						
								<div class="col-md-2 col-sm-12 form-group">
									<label for="year">Financial Year <span class="text-danger">*</span></label>
									<select name="year" id="year" required class="form-control" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2018', date('Y'));
										$flag=0;
										foreach($dates as $date){

									if (date('m', strtotime($date)) < 4) {//Upto April
										$year = ($date-1) . '-' . $date;
									} else {//After April
										$year = $date . '-' . ($date + 1);
									}
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									else if($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
									
								}
								?>
							</select>
						</div>
								<div class="col-md-2 col-sm-12">
									<label for="">From Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')){ echo $this->input->post('startdate');} else{ echo $start_date;}  ?>" required style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 col-sm-12">
									<label for="">To Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')){echo $this->input->post('enddate');}else{echo timeStampShow(date('Y-m-d'));} ?>" required  style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 btn-width pull-right" style="padding-right: 30px;">
									<label for="">&nbsp;</label>
									<button type="submit" id="search_btn" name="search" value="search" class="btn btn-block" style="line-height: 1.2; font-weight: 600;background-color: #f4860c;border-color: #f4860c;color: white;">SEARCH</button>
								</div>

								<?php echo form_close(); ?>
							</div>
							
						</div>
						<div class="panel panel-default" id="pending_table_panel">
							<div class="panel-body">
				<div class="col-md-12 table-responsive">
						<table class="table table-striped table-bordered table-hover pending_table" id="pending_indent">

							
							<thead style="background-color: #085786; font-family: 'Source Sans Pro';color: white;font-size: 13px; text-align: center;">
								<tr>
									<th class="text-center">Item</th>
									<th class="text-center">Indent Number</th>
									<th class="text-center">Date of placing indent</th>
									<th class="text-center">Facility</th>
									<th class="text-center">Quantity Indented (kits/bottle/vials)</th>
									<th class="text-center">Status</th>
									<th class="text-center">Remarks</th>
									<th class="text-center">Approve/Reject Indent</th>
									</tr>
							</thead>
							<tbody>
						<?php if(!empty($inventory_detail_all)){
							foreach ($filtered_arr as $key=>$value_temp) { ?>
								<tr>
									<td class="info" colspan="7" style="border: none;background-color: #333;font-size: 16px;color:#fff;text-align: left;"><?php echo ($value_temp->drug_abb!='SOF400VEL') ? $value_temp->drug_name : $value_temp->drug_name.' '.$value_temp->strength.' '.$value_temp->unit ; ?></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus" aria-hidden="true" id="<?php echo 'head'.$value_temp->id_mst_drugs; ?>"></i></td></tr>
									<?php foreach ($inventory_detail_all as  $value) { 
										if($value_temp->id_mst_drugs==$value->drug_id){
											?>
											<tr class="<?php echo $value_temp->id_mst_drugs; ?>">
												<td class="item">
												<?php echo ($value->type!=2) ? $value->drug.' '.$value->strength.' '.$value->unit : $value->drug; ?>
												</td>
												<td nowrap>
													<?php echo $value->indent_num; ?>
												</td>
					
												<td nowrap>
													<?php echo timeStampShow($value->indent_date); ?>
												</td>
												<td>
													<?php echo $value->facility_short_name; ?>
												</td>
												<td>
													<?php echo $value->quantity; ?>
												</td>
													<td class="<?php if ($value->relocation_status==1){
														echo 'yellow_status facility';
												}
												elseif ($value->relocation_status==2){
														echo 'orange_status facility';
												}
												elseif ($value->relocation_status==3){
														echo 'green_status facility';
												}
												elseif ($value->relocation_status==4){
														echo 'red_status facility';
												}
												elseif ($value->relocation_status==5){
														echo 'darkYellow_status facility';
												}
													elseif ($value->relocation_status==6){
														echo 'darkBlue_status facility';
												}
													
												?>">

													<?php foreach ($relocation_status as $relocation) {
														# code...
													 if($value->relocation_status==$relocation->LookupCode) { echo $relocation->LookupValue;break;}} ?>
												</td>
												<td>
													<?php echo $value->indent_remark; ?>
												</td>
												<td nowrap>
													<?php if ((($value->indent_accept_date==NULL || $value->indent_accept_date=='' ||  $value->indent_accept_date=='0000-00-00') && ($value->indent_status==0 || $value->indent_status>0))) { ?>
													<a href="#" class="btn" style="background-color: #085786;color: white" data-toggle="modal" data-target="#myModal1" onclick='get_indent_data(<?php echo $value->inventory_id ?>);' role="button">Respond to  Request</a>
												<?php } ?>
												<?php if ((($value->indent_accept_date!=NULL || $value->indent_accept_date!='') &&($value->indent_status==0 || $value->relocated_quantity==0 || $value->relocated_quantity==NULL || $value->current_status==2))) { ?>
													<a href="#" class="btn" style="background-color: #085786;color: white" data-toggle="modal" data-target="#myModal1" onclick='get_indent_data(<?php echo $value->inventory_id ?>);' role="button">Edit Request</a>
												<?php } ?>
												</td>

											</tr>
										<?php } }}}else{ ?>
											<tr>
												<td class="text-center" colspan='8'>
													NO RECORDS FOUND  BETWEEN SELECTED DATES
												</td>
												<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									
									
											</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
						</div>
							<br>
						</div>
		<div class="row" style="padding-left: 1.2%;">
				<span><button class="show_hide_legend btn" style="line-height: 1.2; font-weight: 600;background-color: #f4860c;border-color: #f4860c;color: white;">Show/Hide Legend</button></span>
					<span class="legends">
				<dl>
				    <dt class="yellow"></dt>
				    <dd>Order dispatched to facility</dd>

				    <dt class="orange"></dt>
				    <dd>Partial order dispatched to facility</dd>

				     <dt class="green"></dt>
				    <dd>Order received by facility</dd>

				    <dt class="darkBlue_status"></dt>
					 <dd>Order partially received on receipt by facility</dd>

					 <dt class="darkYellow_status"></dt>
				    <dd>Order partially rejected on receipt by facility</dd> 
<br>
				    <dt class="red"></dt>
				    <dd>Order rejected on receipt by facility</dd>

				    
				    
				</dl>
					</span>						
			</div>
			<div class="row" style="padding-right: 10px;">
				<div class="col-lg-12 table-responsive">

					<table class="table table-striped table-bordered table-hover" id="indent_status">
						<thead><tr>
									<th colspan="27" class="info main_header"style="border: none;background-color: #545454;font-size: 16px;color:#fff;text-align: left;">Indents dispatch status<span style="margin-left: 20px;"><input style="margin: 0px;height: 13px;" type="checkbox" class="form-group" id="warehouse" name="warehouse" value="warehouse">
<label for="warehouse">Show warehouse dispatch status</label></span><span style="margin-left: 20px;"><input style="margin: 0px;height: 13px;" type="checkbox" id="facility" name="facility" value="facility">
<label for="facility">Show facility dispatch status</label></span></th>
</tr>

								</thead>
								<thead>
									<tr>
									<th colspan="8" class="info"style="border: none;background-color: #4A708B;font-size: 16px;color:#fff;text-align: left;">Indents Details</th>
									<th colspan="9" class="info warehouse"style="border: none;background-color: #085786;font-size: 16px;color:#fff;text-align: left;">Warehouse dispatch status (kits/bottle/vials)</th>
									<th colspan="10" class="info facility"style="border: none;background-color: #19457b;font-size: 16px;color:#fff;text-align: left;">Facility dispatch status (kits/bottle/vials)</th>
								</tr>
								</thead>
							
							<thead style="background-color: #085786; font-family: 'Source Sans Pro';color: white;font-size: 13px; text-align: center;">
									<tr>
									<th class="text-center">Item</th>
									<th class="text-center">Indent Number</th>
									<th class="text-center">Date of placing indent</th>
									<th class="text-center">Facility</th>
									<th class="text-center">Approved | Quantity Indented (screening tests/bottle)</th>
									<th class="text-center">Date of approval</th>
									<th class="text-center">Indent fulfillment request sent to</th>
									<th class="text-center">S.No. (indent fulfilled)</th>
									<th class="text-center warehouse" style="background: #01446b;">Warehouse quantity fulfilled (kits/bottle/vials)</th>
									<th class="text-center warehouse" style="background: #01446b;">Warehouse total quantity requested (kits/bottle/vials)</th>
									<th class="text-center warehouse" style="background: #01446b;">Warehouse indent fulfillment quantity pending (kits/bottle/vials)</th>
									<th class="text-center warehouse" style="background: #01446b;">Issue no</th>
									<th class="text-center warehouse" style="background: #01446b;">Batch No</th>
									<th class="text-center warehouse" style="background: #01446b;">Warehouse dispatch status update</th>
									<th class="text-center warehouse" style="background: #01446b;">Warehouse dispatch status update date</th>
									<th class="text-center warehouse" style="background: #01446b;">Quantity of Stock Rejected by receiving Facility</th>
									<th class="text-center warehouse" style="background: #01446b;">Reason for rejection</th>

									<th class="text-center facility" style="background: #19457b;">Facility quantity fulfilled (kits/bottle/vials)</th>
									<th class="text-center facility" style="background: #19457b;">Facility total quantity requested (kits/bottle/vials)</th>
									<th class="text-center facility" style="background: #19457b;">Facility indent fulfillment quantity pending (kits/bottle/vials)</th>
									<th class="text-center facility" style="background: #19457b;">Issue no</th>
									<th class="text-center facility" style="background: #19457b;">Batch No</th>
									<th class="text-center facility" style="background: #19457b;">Facility dispatch status update</th>
									<th class="text-center facility" style="background: #19457b;">Facility dispatch status update date</th>
									<th class="text-center facility" style="background: #19457b;">Quantity of stock received by facility</th>
									<th class="text-center facility" style="background: #19457b;">Quantity of Stock Rejected by receiving Facility</th>
									<th class="text-center facility" style="background: #19457b;">Reason for rejection</th>
									</tr>
							</thead>
							<tbody>
			<!-- </tbody>
			</table> -->
				<?php // echo "<pre>";/*print_r($inventory_details);*/
				//$result=array_unique($inventory_details->);
				$item_id=array();
				foreach ($items as $key => $value) {
					foreach ($indent_all_details as $value1) {
						if($value->id_mst_drugs==$value1->drug_name){
							$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
							$item_id[$key]['drug_name']= $value->drug_name;
						}
					}
					
				}
				$filtered_arr1=array_unique($item_id,SORT_REGULAR);
				//pr($indent_details);
				//pr($indent_all_details);
				?>
				<!-- <table class="table table-striped table-bordered table-hover">
					<tbody> -->
						<?php if(!empty($indent_details)){
							foreach ($filtered_arr1 as $key=>$value_temp) { ?>

							
								<tr>
									<td colspan="26" class="info itemname"style="border: none;background-color: #333;font-size: 16px;color:#fff;text-align: left;"><?php echo $value_temp['drug_name']; ?></td>
									<td class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus" aria-hidden="true" id="<?php echo 'head1_'.$value_temp['id_mst_drugs']; ?>"></i></td></tr>
									<?php foreach ($indent_details as  $value) { 
										$count=1;
										foreach ($indent_all_details as $value_all) {
										
								if ($value->indent_num==$value_all->indent_num) {
					
									
										if($value_temp['id_mst_drugs']==$value->drug_name){
												if ($count==1) {
											?>
											<tr class="<?php echo 'item'.$value_temp['id_mst_drugs']; ?>">
												<td class="item">
													<?php if($value->type==1){echo $value_temp['drug_name']." ".$value->strength." mg";} elseif($value->type==2){echo $value_temp['drug_name'];} ?>
												</td>
												<td>
													<?php echo $value->indent_num; ?>
												</td>
					
												<td nowrap>
													<?php echo timeStampShow($value->indent_date); ?>
												</td>
												<td>
													
													<?php echo $value->facility_short_name; ?>
												</td>
												<td>
													<?php echo $value->approved_quantity.'|'.$value->quantity; ?>
												</td>
												<td nowrap="nowrap">
													<?php echo timeStampShow($value->indent_accept_date); ?>
												</td>
												<td>
													<?php 
													$source_name='';
													$req_places='';
													/*if ($value->source_name==996) {
															$source_name= " abc";
														}
														elseif ($value->source_name==997) {
															$source_name =" abcd";
														}
														elseif ($value->source_name==998) {
															$source_name= " abcde";
														}*/
														if (($value->req_facility!='' || $value->req_facility!=NULL) && $source_name!='') {
															$req_places=$value->req_facility." & ".$source_name;
														}
														elseif (($value->req_facility=='' || $value->req_facility==NULL) && $source_name!='') {
															$req_places=$source_name;
														}
														elseif (($value->req_facility!='' || $value->req_facility!=NULL) && $source_name=='') {
															$req_places=$value->req_facility;
														}
														echo $req_places;
														 ?>
												</td>
												<?php }else{?>
													<td colspan="7">
													
												</td>
												<?php }	
												 ?>
													<td nowrap>
													<?php echo $count; ?>
												</td>
					<?php if ($value_all->warehouse_request_quantity==0 || $value_all->warehouse_request_quantity==NULL || $value_all->warehouse_request_quantity=='') {
						echo '<td class="warehouse" colspan="9" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">&nbsp;</td>';
					}else{?>
												<td class="warehouse" nowrap style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													<?php echo ""; ?>
												</td>
												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													<?php echo  !empty($value_all->warehouse_request_quantity)>0 ? $value_all->warehouse_request_quantity : ''; ?>
												</td>
												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													<?php echo  !empty($value_all->warehouse_request_quantity)>0 ? $value_all->warehouse_request_quantity : ''; ?>
												</td>
												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													
												</td>
												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													
												</td>
												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													
												</td>
												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													
												</td>

												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													<?php  $value_all->relocated_quantity; ?>
												</td>
												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													<?php  $value_all->requested_quantity; ?>
												</td>
											<?php }?>
												<td class="facility">
													<?php echo ($value_all->relocated_quantity); ?>
												</td>
												<td class="facility">
													<?php if ($value_all->pending_quantity==0) {
														echo $value_all->requested_quantity;
													}
													else{
														echo $value_all->pending_quantity;
													} ?>
												</td>
												<td class="facility">
													<?php if ($value_all->pending_quantity==0) {
														echo ($value_all->requested_quantity-$value_all->relocated_quantity);
													}
													else{
														echo ($value_all->pending_quantity-$value_all->relocated_quantity);
													} ?>
												</td>
												<td class="facility">
													<?php echo $value_all->issue_num; ?>
												</td>
												<td class="facility">
													<?php echo $value_all->batch_num; ?>
												</td>
												<td class="<?php if ($value_all->relocation_status==1){
														echo 'yellow_status facility';
												}
												elseif ($value_all->relocation_status==2){
														echo 'orange_status facility';
												}
												elseif ($value_all->relocation_status==3){
														echo 'green_status facility';
												}
												elseif ($value_all->relocation_status==4){
														echo 'red_status facility';
												}
												elseif ($value_all->relocation_status==5){
														echo 'darkYellow_status facility';
												}
												elseif ($value_all->relocation_status==6){
														echo 'darkBlue_status facility';
												}
													
												?>">

													<?php foreach ($relocation_status as $relocation) {
														# code...
													 if($value_all->relocation_status==$relocation->LookupCode) { echo $relocation->LookupValue;break;}} ?>
												</td>
												<td class="facility" nowrap="nowrap"> 	<?php echo timeStampShow($value_all->dispatch_date); ?>
													
												</td>
												
												<td class="facility">
													<?php echo !empty($value_all->quantity_received)>0 ? $value_all->quantity_received :"N/A" ; ?>
												</td>
												<td class="facility">
													<?php 
														if (!empty($value_all->quantity_received)>0) {
															echo !empty($value_all->quantity_rejected)>0 ? $value_all->quantity_rejected :"0" ;
														}
														else{
															echo !empty($value_all->quantity_rejected)>0 ? $value_all->quantity_rejected :"N/A" ; 
														}
													?>
												</td>
												<td class="facility">
													 <?php if($value_all->rejection_reason==0 || $value_all->rejection_reason=="" || $value_all->rejection_reason==NULL){
													 echo "N/A";}else{
													 	foreach ($reason_for_rejection as $rejection) {
														# code...
													 if($value_all->rejection_reason==$rejection->LookupCode) { echo $rejection->LookupValue;}}
													 } ?>
												</td>
											</tr>
										<?php }$count=$count+1;}}}}}else{ ?>
											<tr >
												<td colspan="18" class="text-center">
													NO RECORDS FOUND  BETWEEN SELECTED DATES
												</td>

											</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
							<br>
						</div>
	</div>

</div>

						<div class="overlay">
							 <img class="loading_gif" src="<?php echo site_url('application');?>/third_party/images/spinner.gif" alt="loading_gif" />
						</div>
				<!-- Data Table -->
						<script type="text/javascript">

							
							$("#quantity,#quantity_rejected,#requested_quantity,#approved_quantity,#warehouse_request_quantity").on('keypress',function(evt){

								var charCode = (evt.which) ? evt.which : event.keyCode
								
								if ((charCode > 31 && (charCode < 48 || charCode > 57)) || parseInt($(this).val()+ evt.key)>10000){

									evt.preventDefault();
									return false;
								}
								if($(this).val() <=0 && $(this).val()!='')
								{
									$("#modal_header").html('Quantity must be greater than 0');
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									$(this).val('');
									return false;
								}
								return true;
							});
$("#approved_quantity").on('keyup',function(evt){

					

								if(parseInt($(this).val()) >parseInt($('#quantity').val()))
								{
									$("#modal_header").html('Approved Quantity must be less than Indented Quantity');
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									$('#approved_quantity').val('');
									return false;
								}


								if ($('#approved_quantity').val()=='') {
									$('#quantity_rejected').val('');
									$('#quantity_rejected').removeAttr('readonly','readonly');
								}

								var quantity=parseInt($('#quantity').val())-(parseInt($('#approved_quantity').val()));
								//alert(quantity);
								 if (quantity>0) {
									$('#quantity_rejected').val(quantity);
									$('#quantity_rejected').attr('readonly','readonly');
									$('#warehouse_request_quantity,#warehouse_name,#facility_name,#requested_quantity').removeAttr('disabled');
									$('#warehouse_request_quantity,#warehouse_name,#facility_name,#requested_quantity').removeClass('isDisabled');
								}
								else  if (quantity<=0) {
									$('#quantity_rejected').val(0);
									$('#quantity_rejected').attr('readonly','readonly');
									$('#warehouse_request_quantity,#warehouse_name,#facility_name,#requested_quantity').removeAttr('disabled');
								}
								
					
						if ($('#fix_requested_quantity').val()!='' && parseInt($('#fix_requested_quantity').val())!=0) {
							$("#requested_quantity").val($('#fix_requested_quantity').val());
								$('#facility_name').addClass('isDisabled');
								//$('#facility_name').attr('disabled','true');
						}
						else if (parseInt($('#approved_quantity').val())==0) {
							$('#facility_name').addClass('isDisabled');
							$('#facility_name').attr('disabled','true');
						}
						else{
							if (parseInt($('#approved_quantity').val())==parseInt($('#warehouse_request_quantity').val())){
										$('#facility_name').addClass('isDisabled');
										$('#facility_name').attr('disabled','true');
										$('#facility_name').val('');
										$('#requested_quantity').val('');
							}
							else{
							$('#facility_name,#requested_quantity').removeClass('isDisabled');
							$('#facility_name,#requested_quantity').removeAttr('disabled');
							$('#requested_quantity').val('');
						}
						}
							if ($('#fix_warehouse_requested_quantity').val()!='' && parseInt($('#fix_warehouse_requested_quantity').val())!=0) {
							$("#warehouse_request_quantity").val($('#fix_warehouse_requested_quantity').val());
								$('#warehouse_name').addClass('isDisabled');
								//$('#warehouse_name').attr('disabled','true');
						}
						else if (parseInt($('#approved_quantity').val())==0) {
							$('#warehouse_name').addClass('isDisabled');
							$('#warehouse_name').attr('disabled','true');
						}
						else{
							if (parseInt($('#approved_quantity').val())==parseInt($('#requested_quantity').val())){
										$('#warehouse_name,#warehouse_request_quantity').addClass('isDisabled');
										$('#warehouse_name,#warehouse_request_quantity').attr('disabled','true');
										$('#warehouse_name').val('');
										$('#warehouse_request_quantity').val('');
							}
							else{
							$('#warehouse_name').removeClass('isDisabled');
							$('#warehouse_name').removeAttr('disabled');
							$('#warehouse_request_quantity').val('');
						}
						}
						 if (parseInt($('#approved_quantity').val())==0 && ($('#already_relocated_quantity').val()=='' || parseInt($('#already_relocated_quantity').val())==0)) {
							$('#warehouse_request_quantity,#warehouse_name,#facility_name,#requested_quantity').attr('disabled','true');
							$('#warehouse_request_quantity,#warehouse_name,#facility_name,#requested_quantity').addClass('isDisabled');
							$('#warehouse_request_quantity,#warehouse_name,#facility_name,#requested_quantity').val('');
							var error_css = {"border":"1px solid #c7c5c5"};
									$('#warehouse_request_quantity,#warehouse_name,#facility_name,#requested_quantity').css(error_css);
						}
								return true;
							});
$("#approved_quantity").on('change',function(evt){

						if(parseInt($(this).val() || 0) <parseInt($('#already_relocated_quantity').val()))
								{
									$("#modal_header").html('Approved Quantity must be greater than or equal to Quantity already Relocated : ('+$('#already_relocated_quantity').val()+')');
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									$('#approved_quantity').val($('#already_relocated_quantity').val());
									$('#approved_quantity').trigger('keyup');
									return false;
								}
								check_reject();
							});
$("#quantity_rejected").on('keyup',function(evt){
							

								/*if ($('#approved_quantity').val()=='') {
									$("#modal_header").html("Approved Quantity can not be empty");
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									$('#warehouse_request_quantity').val('');
								}*/
								if(parseInt($(this).val()) >parseInt($('#quantity').val()))
								{
									$("#modal_header").html('Rejected Quantity must be less than Indented Quantity');
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									$('#quantity_rejected').val('');
									$('#approved_quantity').val('');
									return false;
								}
								else{
									var quantity=parseInt($('#quantity').val() || 0)-(parseInt($('#quantity_rejected').val() || 0));
									//alert($('#quantity_rejected').val());
									$('#approved_quantity').val(quantity);
								}
								if ((parseInt($('#approved_quantity').val()))<parseInt($('#quantity').val())) {
								var rem=parseInt($('#quantity').val())-(parseInt($('#approved_quantity').val() || 0)+parseInt($('#quantity_rejected').val()));
								if (rem<0) {
									$("#modal_header").html('Sum of Approved and Rejected Quantity Should be less than or eqaul to Indented Quantity');
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									$('#quantity_rejected').val('');
									var rem=parseInt($('#quantity').val())-(parseInt($('#approved_quantity').val())+parseInt($('#quantity_rejected').val() || 0));

									$('#quantity_rejected').val(rem);
									return false;
								}
								
							}
							
							
								
								if (parseInt($('#approved_quantity').val())==0) {
									$('#warehouse_request_quantity,#warehouse_name,#facility_name,#requested_quantity').attr('disabled','true');
									var error_css = {"border":"1px solid #c7c5c5"};
									$('#warehouse_request_quantity,#warehouse_name,#facility_name,#requested_quantity').css(error_css);
						}
						else{
							$('#warehouse_request_quantity,#warehouse_name,#facility_name,#requested_quantity').removeAttr('disabled');
						}
						if ($(this).val()=='') {
							$(this).val(0);
						}
						if ($('#fix_requested_quantity').val()!='' || parseInt($('#fix_requested_quantity').val())!=0) {
							$("#requested_quantity").val($('#fix_requested_quantity').val());
						}
						if ($('#fix_warehouse_requested_quantity').val()!='' && parseInt($('#fix_warehouse_requested_quantity').val())!=0) {
							$("#warehouse_request_quantity").val($('#fix_warehouse_requested_quantity').val());
								
						}
								return true;
							});
$("#quantity_rejected").on('change',function(evt){
$("#approved_quantity").trigger('change');
});
$("#warehouse_request_quantity").on('keyup',function(evt){

						
								if ($('#approved_quantity').val()=='')
								 {
										$("#modal_header").html("Approved Quantity can not be empty");
										$("#modal_text").html('Please fill in appropriate quantity');
										$("#multipurpose_modal").modal("show");
										$('#warehouse_request_quantity').val('');
											}
								 if(parseInt($(this).val()) >parseInt($('#approved_quantity').val()))
								{
									$("#modal_header").html('Warehouse Requested Quantity must be less than Approved Quantity');
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									$('#warehouse_request_quantity').val('');
									$('#requested_quantity').val('');
									return false;
								}
								if ($('#warehouse_request_quantity').val()!='') {
								var quantity=(parseInt($('#approved_quantity').val())-parseInt($('#warehouse_request_quantity').val()|| 0));
								 if (quantity>0) {
									if ($('#fix_requested_quantity').val()=='' || parseInt($('#fix_requested_quantity').val())==0) {
										$('#requested_quantity').val(quantity);
										$('#requested_quantity,#facility_name').removeAttr('disabled');
										$('#requested_quantity').attr('readonly','readonly');
									}
									else{
										if (quantity<parseInt($('#fix_requested_quantity').val())) {
											$("#modal_header").html('Requested Quantity must be greter than or equal to Quantity already Relocated by Facility: ('+$('#fix_requested_quantity').val()+")");
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									if ($('#fix_warehouse_requested_quantity').val()=='' || parseInt($('#fix_warehouse_requested_quantity').val())==0) {
										var quantity2=(parseInt($('#approved_quantity').val())-parseInt($('#fix_requested_quantity').val()));
									$('#warehouse_request_quantity').val(quantity2);
								}
								else{
									$('#warehouse_request_quantity').val($('#fix_warehouse_requested_quantity').val());
								}
										}
										else{
											$('#requested_quantity').val(quantity);
											$('#requested_quantity').removeAttr('readonly','readonly');
										}
										
									}
									
									
								}
								else if (quantity==0) {
									if ($('#fix_requested_quantity').val()!='' && parseInt($('#fix_requested_quantity').val())!=0) {
										$("#modal_header").html('Requested Quantity must be greter than or equal to Quantity already Relocated by Facility : ('+$('#fix_requested_quantity').val()+")");
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									if ($('#fix_warehouse_requested_quantity').val()=='' || parseInt($('#fix_warehouse_requested_quantity').val())==0) {
										var quantity2=(parseInt($('#approved_quantity').val())-parseInt($('#fix_requested_quantity').val()));
									$('#warehouse_request_quantity').val(quantity2);

									}
									else{
									$('#warehouse_request_quantity').val($('#fix_warehouse_requested_quantity').val());
								}
								}
									else
									{
										$('#requested_quantity').val(quantity);
										$('#requested_quantity,#facility_name').attr('disabled','true');
										var error_css = {"border":"1px solid #c7c5c5"};
										$('#requested_quantity,#facility_name').css(error_css);
										$('#facility_name').val('');
									}
								}
							}
							else if ($('#warehouse_request_quantity').val()=='') {
								$('#requested_quantity').val('');
								$('#requested_quantity').removeAttr('readonly','readonly');
							}
															return true;
							});
$("#requested_quantity").on('keyup',function(evt){

								/*if(parseInt($(this).val()) >parseInt($('#quantity').val()))
								{
									$("#modal_header").html('Transfer requested quantity should be less than approved quantity');
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									$('#requested_quantity').val('');
									return false;
								}*/
								
								if ($('#approved_quantity').val()=='') {
									$("#modal_header").html("Approved Quantity can not be empty");
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									$('#requested_quantity').val('');
								}
								if(parseInt($(this).val()) >parseInt($('#approved_quantity').val()))
								{
									//alert($(this).val());
									$("#modal_header").html('Transfer requested quantity should be less than approved quantity');
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									$('#requested_quantity').val('');
									return false;
								}
								else if (parseInt($('#requested_quantity').val())>0) {
									var quantity=(parseInt($('#approved_quantity').val())-(parseInt($('#requested_quantity').val()|| 0)));
									if (quantity>0) {
										
										if ($('#fix_warehouse_requested_quantity').val()=='' || parseInt($('#fix_warehouse_requested_quantity').val())==0) {
											$('#warehouse_request_quantity').val(quantity);
											$('#warehouse_request_quantity,#warehouse_name').removeAttr('disabled');
										}
										else{
											if (quantity<parseInt($('#fix_warehouse_requested_quantity').val())) {
												$("#modal_header").html('Warehouse Requested Quantity must be greater than or equal to already Requested Quantity: ('+$('#fix_warehouse_requested_quantity').val()+")");
												$("#modal_text").html('Please fill in appropriate quantity');
												$("#multipurpose_modal").modal("show");
												if ($('#fix_requested_quantity').val()=='' || parseInt($('#fix_requested_quantity').val())==0) {
													var quantity2=(parseInt($('#approved_quantity').val())-parseInt($('#fix_warehouse_requested_quantity').val()));
													$('#requested_quantity').val(quantity2);
													$('#warehouse_request_quantity').removeAttr('disabled');
												}
												else{
													$('#requested_quantity').val($('#fix_requested_quantity').val());
													$('#warehouse_request_quantity').attr('readonly','readonly');
												}
											}
											else{
												$('#warehouse_request_quantity').val(quantity);
											}
											
										}
										
									}
									else if (quantity==0) {
										if ($('#fix_warehouse_requested_quantity').val()!='' && parseInt($('#fix_warehouse_requested_quantity').val())!=0) {
										$("#modal_header").html('Warehouse Requested Quantity must be greter than or eqaul to Quantity already Relocated by Warehouse: ('+$('#fix_warehouse_requested_quantity').val()+")");
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									if ($('#fix_requested_quantity').val()=='' || parseInt($('#fix_requested_quantity').val())==0) {
										var quantity2=(parseInt($('#approved_quantity').val())-parseInt($('#fix_warehouse_requested_quantity').val()));
									$('#requested_quantity').val(quantity2);
									$('#warehouse_request_quantity').val($('#fix_warehouse_requested_quantity').val());

									}
									else{
									$('#requested_quantity').val($('#fix_requested_quantity').val());
									$('#warehouse_request_quantity').val($('#fix_warehouse_requested_quantity').val());
								}
								}
										else{
										$('#warehouse_request_quantity').val(quantity);
										var error_css = {"border":"1px solid #c7c5c5"};
										
										$('#warehouse_request_quantity,#warehouse_name').attr('disabled','true');
										$('#warehouse_request_quantity,#warehouse_name').css(error_css);
										$('#warehouse_name').val('');
									}
																	
								}
							}
								else if ($('#requested_quantity').val()=='') {
									$('#warehouse_request_quantity').val('');
									$('#warehouse_request_quantity').removeAttr('readonly','readonly');
								}
								

								return true;
							});
$("#requested_quantity,#warehouse_request_quantity").on('change',function(evt){
								if((parseInt($("#warehouse_request_quantity").val())+parseInt($("#requested_quantity").val())) <parseInt($('#already_relocated_quantity').val()))
								{
									$("#modal_header").html('Total Requested Quantity must be greater than relocated quantity: ('+$('#already_relocated_quantity').val()+')');
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									$(this).val('');
									$("#requested_quantity").val($("#fix_requested_quantity").val());
									return false;
								}
								if(parseInt($("#requested_quantity").val() || 0)<parseInt($('#fix_requested_quantity').val()))
								{
									$("#modal_header").html('Total Requested Quantity must be greater than or equal to Quantity already Relocated by Facility: ('+$('#fix_requested_quantity').val()+')');
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									$("#requested_quantity").val($("#fix_requested_quantity").val());
									$("#requested_quantity").trigger('keyup');
									$("#warehouse_request_quantity").removeAttr('readonly');
									return false;
								}
								if(parseInt($("#warehouse_request_quantity").val() || 0)<parseInt($('#fix_warehouse_requested_quantity').val() || 0))
								{
									$("#modal_header").html('Total Requested Quantity must be greater than or equal to  Quantity already Relocated by Warehouse: ('+$('#fix_warehouse_requested_quantity').val()+')');
									$("#modal_text").html('Please fill in appropriate quantity');
									$("#multipurpose_modal").modal("show");
									$("#warehouse_request_quantity").val($("#fix_warehouse_requested_quantity").val());
									$("#warehouse_request_quantity").trigger('keyup');
									$("#requested_quantity").removeAttr('readonly');
									return false;
								}
								return true;
							});
function get_indent_data(inventory_id){
 $(".loading_gif").show();
$('#inventory_id').val(inventory_id);
$('.modal-title').html('Update Stock Indent Received');
fill_source_name();

//alert($('#inventory_id').val());
$.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Inventory/get_indent_quantity", // <-- properly quote this line
    //cache: false,
   // async : true,
    data: {inventory_id:inventory_id, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'}, 
    success: function(data) {
    	
        var returned = JSON.parse(data);
    var id_mst_drugs = returned.inventory_details.map(function(e) { return e.id_mst_drugs; });
    var indent_num = returned.inventory_details.map(function(e) { return e.indent_num; });
     var quantity = returned.inventory_details.map(function(e) { return e.quantity; });
     var id_mstfacility = returned.inventory_details.map(function(e) { return e.id_mstfacility; });
     var transfer_to = returned.inventory_details.map(function(e) { return e.transfer_to; });
     var source_name = returned.inventory_details.map(function(e) { return e.source_name; });
     var quantity_rejected = returned.inventory_details.map(function(e) { return e.quantity_rejected_by_state; });
     var warehouse_request_quantity = returned.inventory_details.map(function(e) { return e.warehouse_request_quantity; });
     var requested_quantity = returned.inventory_details.map(function(e) { return e.relocated; });
     var approved_quantity = returned.inventory_details.map(function(e) { return e.approved; });
     var relocated_quantity = returned.inventory_details.map(function(e) { return e.relocated_quantity; });
     var relocation_status = returned.inventory_details.map(function(e) { return e.relocation_status; });
     var sno_remark = returned.inventory_details.map(function(e) { return e.sno_remark; });
     var is_temp = returned.inventory_details.map(function(e) { return e.is_temp; });
     $("#facility_name option[value='"+id_mstfacility[0]+"']").remove();
    $("#quantity").val(quantity);
    $("#sno_remark").val(sno_remark);
    
    $("#is_temp").val(is_temp);
    $("#indent_num").val(indent_num);
   //alert(transfer_to[0]);
    $("#approved_quantity").val(approved_quantity);
       $("#quantity_rejected").val(quantity_rejected);
     $("#source_name_val").val(transfer_to);
     $("#warehouse_val").val(source_name);
  
     // $("#quantity_rejected").trigger('keyup');
  if (approved_quantity!='' || approved_quantity!=0) {
  	//alert('hiiiii');
    	 $("#approved_quantity").trigger('keyup');
    }
    else if (quantity_rejected!='' || quantity_rejected!=0) {
    	//alert('hi');
    	 $("#quantity_rejected").trigger('keyup');
    	 
    }
    if (quantity_rejected!='' && quantity_rejected>0) {
    	$("#remark_rej").show();
     }
     else
     {
     	$("#remark_rej").hide();
     }
    $("#warehouse_request_quantity").val(warehouse_request_quantity);
    
    $("#requested_quantity").val(requested_quantity);
   
    if (((requested_quantity!=0 || requested_quantity!='0') && requested_quantity!='')) {
    	//alert(requested_quantity);
    	$("#requested_quantity").removeAttr('readonly');
    	 $("#requested_quantity").trigger('keyup');
    }
   if (((warehouse_request_quantity!=0 || warehouse_request_quantity!='0') && warehouse_request_quantity!='')) {
    	//alert(warehouse_request_quantity);
    	$("#warehouse_request_quantity").removeAttr('readonly');
    	 $("#warehouse_request_quantity").trigger('keyup');
    }
     $("#warehouse_name").val(source_name[0]);
    $("#facility_name").val(transfer_to[0]);
   if (approved_quantity!='0' || approved_quantity!=0) {
    	$("#approved_quantity").removeAttr('readonly');
    }
    if (quantity_rejected!='0' || quantity_rejected!=0) {
    	$("#quantity_rejected").removeAttr('readonly');
    }
  
    if (relocation_status[0]==null) {
    	$('#approved_quantity,#quantity_rejected,#facility_name,#warehouse_name,#requested_quantity,#warehouse_request_quantity').removeAttr('readonly');
    	$('#approved_quantity,#quantity_rejected,#facility_name,#warehouse_name,#requested_quantity,#warehouse_request_quantity').removeClass('isDisabled');
    }

    else if ((relocation_status[0]>0 || relocation_status[0]!='0' || relocation_status[0]!==null || relocation_status[0]!=='' || relocation_status[0]!=='null')) {
    	 if ((parseInt(quantity)==parseInt(approved_quantity || 0)) && ((relocated_quantity!=0 || relocated_quantity!='0') && relocated_quantity!='')) {
    	$('#approved_quantity,#quantity_rejected,#facility_name,#warehouse_name,#requested_quantity,#warehouse_request_quantity').addClass('isDisabled');
    	$('#approved_quantity,#quantity_rejected,#facility_name,#warehouse_name,#requested_quantity,#warehouse_request_quantity').attr('readonly','readonly');
    	}
    	else{
    		
    		$('#already_relocated_quantity').val(approved_quantity);
    		$("#fix_requested_quantity").val(requested_quantity);
    		$("#fix_warehouse_requested_quantity").val(warehouse_request_quantity);
    		if (($('#fix_requested_quantity').val()!='' && parseInt($('#fix_requested_quantity').val())!=0) && ($('#fix_warehouse_requested_quantity').val()!='' && parseInt($('#fix_warehouse_requested_quantity').val())!=0))
    		 {
				$('#facility_name,#warehouse_name').addClass('isDisabled');
				$('#facility_name,#warehouse_name').attr('readonly','readonly');
			}
			else if ($('#fix_requested_quantity').val()!='' && parseInt($('#fix_requested_quantity').val())!=0)
    		 {
				$('#facility_name').addClass('isDisabled');
				$('#facility_name').attr('readonly','readonly');
			}
			else if ($('#fix_warehouse_requested_quantity').val()!='' && parseInt($('#fix_warehouse_requested_quantity').val())!=0)
    		 {
				$('#warehouse_name').addClass('isDisabled');
				$('#warehouse_name').attr('readonly','readonly');
			}
    	}
    }
}
});
 setTimeout(function() {
						       $(".loading_gif").hide();
						    },3000);
 $("#facility_name").val($("#source_name_val").val());
}
$("#modal_close,#close_icon").on('click ',function(evt){
	$('#inventory_id').val('');
	  $('#indent_num_val').val('');
	  $('#already_relocated_quantity,#fix_warehouse_requested_quantity,#fix_requested_quantity').val('');
	$('.modal-title').html('Add Stock Indent Received');
   
 $('#AddStock_indent_form').find("input[type=text],textarea,select").val("");
 $('#AddStock_indent_form').find("input[type=text],textarea,select").removeAttr('readonly');


});
$("#myModal1").on('hide.bs.modal', function(){
   // alert('The modal is about to be hidden.');
   $('#inventory_id').val('');
   $('#indent_num_val').val('');
   $('#already_relocated_quantity,#fix_warehouse_requested_quantity,#fix_requested_quantity').val('');
 $('#AddStock_indent_form').find("input[type=text],textarea,select").val("");
  $('#indent_date').val('<?php echo date("d-m-Y"); ?>');
  });

$('select,input[type=text]').on('change click keypress',function(e){
						
	var error_css = {"border":"1px solid #c7c5c5"};
	$(this).css({"border":""});
			$(this).css(error_css);
	return true;

});
$('#submit_btn').click(function(e){
		var error_css = {"border":"1px solid red"};
		var error_free=true;
		var quantity1=(parseInt($('#approved_quantity').val())-(parseInt($('#requested_quantity').val())|| 0));
		var quantity2=(parseInt($('#approved_quantity').val())-(parseInt($('#warehouse_request_quantity').val())|| 0));
		if($('#approved_quantity').val().trim()==""){

			$("#approved_quantity").css(error_css);
			$("#approved_quantity").focus();
			e.preventDefault();
			error_free= false;
		}
		else if($('#quantity_rejected').val().trim()==""){

			$("#quantity_rejected").css(error_css);
			$("#quantity_rejected").focus();
			e.preventDefault();
			error_free= false;
		}

	else if (quantity1>0) {
		
		 if($('#warehouse_name').val().trim()==""){

			$("#warehouse_name").css(error_css);
			$("#warehouse_name").focus();
			e.preventDefault();
			error_free= false;
		}
		else if($('#warehouse_request_quantity').val().trim()==""){

			$("#warehouse_request_quantity").css(error_css);
			$("#warehouse_request_quantity").focus();
			e.preventDefault();
			error_free= false;
		}
		
	}

		  if (quantity2>0) {
		 if($('#requested_quantity').val().trim()==""){

			$("#requested_quantity").css(error_css);
			$("#requested_quantity").focus();
			e.preventDefault();
			error_free= false;
		}
		if($('#facility_name').val().trim()==""){

			$("#facility_name").css(error_css);
			$("#facility_name").focus();
			e.preventDefault();
			error_free= false;
		}
	}

if (!error_free){
		e.preventDefault(); 
		return error_free;
	}
	else{
		
		return true;

	}

	});

 $("#AddStock_indent_form").submit(function() {
            $(this).submit(function() {
                return false;
            });
            return true;
        });
 $("#facility,#warehouse").click(function() {
 	var classname=$(this).val();
 	
           if ( $(this).is(':checked') === true ) {
           $('.'+classname).show();
           }
       
           else{
           	$('.'+classname).hide();
           }
           if ( ($('#facility').is(':checked') === false ) &&($('#warehouse').is(':checked') === false )) {
            	$('.main_header').attr('colspan',8);
           		$('.itemname').attr('colspan',7);
            }
            else if ( ($('#facility').is(':checked') === true ) &&($('#warehouse').is(':checked') === false )) {
            	$('.main_header').attr('colspan',19);
           		$('.itemname').attr('colspan',17);
            } 
            else if ( ($('#facility').is(':checked') === false ) &&($('#warehouse').is(':checked') === true )) {
            	$('.main_header').attr('colspan',17);
           		$('.itemname').attr('colspan',16);
            }
            else if ( ($('#facility').is(':checked') === true ) &&($('#warehouse').is(':checked') === true )) {
            	$('.main_header').attr('colspan',27);
           		$('.itemname').attr('colspan',26);
            }
        });

							$(document).ready(function(){
								fill_source_name();
								$('#facility').attr('checked','checked');
								$('#warehouse').attr('checked','checked');
								/*$('#pending_indent').DataTable({"pagingType": "simple_numbers",
						"searching": false,

						"lengthChange": false,
						"orderable": false,
						"pageLength": 5}
						
					);*/	/*$('#indent_status').DataTable({"pagingType": "simple_numbers",
						"searching": false,

						"lengthChange": false,
						"orderable": false,
						"pageLength": 5}
						
					);*/<?php foreach ($filtered_arr as $key=>$value_temp) {
									$elemnt_id= ".".$value_temp->id_mst_drugs;?> 
									$("<?php echo $elemnt_id; ?>").show();
									$("<?php echo '#head'.$value_temp->id_mst_drugs; ?>").click(function(){
										if($("<?php echo '#head'.$value_temp->id_mst_drugs; ?>").hasClass('fa-minus')){
											$("<?php echo $elemnt_id; ?>").hide();
											$("<?php echo '#head'.$value_temp->id_mst_drugs; ?>").removeClass('fa-minus').addClass("fa-plus");
										}
										else{
											$("<?php echo $elemnt_id; ?>").show();
											$("<?php echo '#head'.$value_temp->id_mst_drugs; ?>").removeClass('fa-plus').addClass("fa-minus");
										}
										
									});
									$("<?php echo '#head'.$value_temp->id_mst_drugs; ?>").css('cursor', 'pointer');
								<?php }?>
								<?php foreach ($filtered_arr1 as $key=>$value_temp) {
									$elemnt_id= ".item".$value_temp['id_mst_drugs'];?> 
									$("<?php echo $elemnt_id; ?>").show();
									$("<?php echo '#head1_'.$value_temp['id_mst_drugs'] ?>").click(function(){
										if($("<?php echo '#head1_'.$value_temp['id_mst_drugs'] ?>").hasClass('fa-minus')){
											$("<?php echo $elemnt_id; ?>").hide();
											$("<?php echo '#head1_'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-minus').addClass("fa-plus");
										}
										else{
											$("<?php echo $elemnt_id; ?>").show();
											$("<?php echo '#head1_'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-plus').addClass("fa-minus");
										}
										
									});
									$("<?php echo '#head1_'.$value_temp['id_mst_drugs'] ?>").css('cursor', 'pointer');
								<?php }?>
							});

							
			function delete_indent_data(inventory_id){
									
									   		 $( "#dialog-confirm" ).dialog({
									   		open : function() {
										    $("#dialog-confirm").closest("div[role='dialog']").css({top:100,left:'40%'});
											}, 	
									      resizable: false,
									      height: "auto",
									      width: 400,
									      modal: true,
									      buttons: {
									        "Cancel request": function() {
									          window.location.replace('<?php echo base_url(); ?>Inventory/delete_receipt/'+inventory_id+'/I');

									        },
									        "Don't Cancel request": function() {
									          $( this ).dialog( "close" );
									          e.preventDefault();
									        }
									      }
									 			 });
								};	

function fill_source_name(){
					/*var opt='<option value="">----Select----</option>';
					opt += '<option value="996">abc</option>';
					opt += '<option value="997">abcd</option>';
					opt+=  '<option value="998">abcde</option>';
					$('#warehouse_name').html(opt);*/
					$.ajax({
                    url : "<?php echo site_url('inventory/get_facilities');?>",
                    method : "GET",
                   data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>',is_warehouse:2
				},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
                        //console.log(returned);
        				var id_mstfacility = returned.id_mstfacility.map(function(e) {
   						return e.id_mstfacility;
						});
						var facility_short_name = returned.id_mstfacility.map(function(e) {
   						return e.facility_short_name;
						});
                        var is_warehouse = returned.id_mstfacility.map(function(e) {
   						return e.is_warehouse;
						});
                        var i;
                        var select=''
                        var opt='';
                        var select1=''
                        var opt1='';
                        opt+='<option value="">----Select----</option>'
                        for(i=0; i<id_mstfacility.length; i++){
                        	
                        		select='';
                        		select1='';
                        if (is_warehouse[i]==1)
                            opt1 += '<option value="'+id_mstfacility[i]+'"'+select1+'>'+facility_short_name[i]+'</option>';
                        else
                        opt += '<option value="'+id_mstfacility[i]+'"'+select+'>'+facility_short_name[i]+'</option>';
                        }
                        //console.log(opt);
                        $('#facility_name').html(opt);
                        $('#warehouse_name').html(opt1);
 						$('#facility_name').val($('#source_name_val').val());
                    }
                });
					}	
function check_reject()
{
	var val=$('#quantity_rejected').val();
	//alert(val);
	if (val>0 && val!='') {
		$('#remark_rej').show();
	}
	else
	{
		$('#remark_rej').hide();
	}
}					

						</script>


