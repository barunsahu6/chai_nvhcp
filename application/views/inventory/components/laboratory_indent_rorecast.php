<style>
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: top;
		background-color: #085786;
		color: white;
	}
	td{
		text-align: center; 
		vertical-align: middle;
	}
	.btn-width{
	width: 12%;
	margin-top: 30px;
}
.btn-success
{
	background-color: #f4860c;
	color: #FFF !important;
	border : 1px solid #f4860c;
	border-radius: 5px;
}

.btn-success:hover
{
	text-decoration: none !important;
	color: #FFF !important;
	background-color: #e47c09 !important;
	border : 1px solid #e47c09;
	border-radius: 5;
}
		text-align: center; 
		vertical-align: middle;
	}
	.item{
		width: 140px !important;
	}
	.batch_num{
		width: 100px !important;
	}
	a{
		cursor: pointer;
	}

	a:hover,a:focus {
		cursor: pointer;
		text-decoration: none;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	select[readonly] {
		background: #eee;
		pointer-events: none;
		touch-action: none;
	}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData');
$filters = $this->session->userdata('filters');
//echo "<pre>"; print_r($filters);

if($loginData->user_type==1) { 
	$select = '';
}else{
	$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
	$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
	$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
	$select4 = '';	
}
?>
<?php $loginData = $this->session->userdata('loginData');
$filters1=$this->session->userdata('filters1');
if ($loginData->user_type==2 || $filters1['id_mstfacility'] !=''){
	$colspan="17";
	$th="<th>Batch Number</th>";
	$item_text="Item";
}
else if (($loginData->user_type==1 || $loginData->user_type==3)&&$filters1['id_mstfacility']==''){
	$colspan="16";
	$th=" ";
	if (($loginData->user_type==1 || $loginData->user_type==3)|| $filters1['id_search_state'] !=''){
	$item_text="Facility";
}
}
if ($Repo_flg==1) {
	$show_text="(no. of bottles)";
}
elseif ($Repo_flg==2) {
	$show_text='(no. of screening tests)';
}
?>
<div class="row main-div" style="min-height:400px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xl-12">
		<div class="panel panel-default" id="Record_receipt_panel">
			<div class="panel-heading" style=";background: #333;">
				<h4 class="text-center" style="color: white;background: #333" id="Record_form_head">  <?php if($Repo_flg==4){$option='';echo "HBIG Indent Suggestion";}elseif($Repo_flg==2){ $option='<option value="Kit">All Screening Test Kits</option><option value="RDTKit">RDT Kits</option>
										<option value="ELISAKit">ELISA Kits</option>
										<option value="VLKit">Viral Load Kits</option>';echo "Lab Indent Suggestion";} ?></h4>
			</div>
			<div class="row">
				<?php 
				$tr_msg= $this->session->flashdata('tr_msg');
				$er_msg= $this->session->flashdata('er_msg');
				if(!empty($tr_msg)){  ?>


					<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
						<div class="hpanel">
							<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<?php echo $this->session->flashdata('tr_msg');?>. </div>
							</div>
						</div>
					<?php } else if(!empty($er_msg)){ ?>
						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('er_msg');?>. </div>
								</div>
							</div>

						<?php } ?>
					</div>
					<?php
					$attributes = array(
						'id' => 'Receipt_filter_form',
						'name' => 'Receipt_filter_form',
						'autocomplete' => 'false',
					);
//pr($items);
//print_r($receipt_details);
//pr($Closing_stock_Repo);
					echo form_open('', $attributes); ?>

					<div class="row" style="margin-left: 10px;">
						
<div class="col-xs-3 col-md-2">
	<label for="item_type">Item Name <span class="text-danger">*</span></label>
	<div class="form-group">
		<select name="item_type" id="item_type" required class="form-control">
			<?php echo $option; ?>
			<?php foreach ($items as $value) { ?>

				<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php if($value->type==2){echo $value->drug_name; } elseif($value->type==3 || $value->type==4){echo $value->drug_name." ".(float)$value->strength." ml";} else{
													echo $value->drug_name." ".(float)$value->strength." mg";
												} ?></option>
				<?php } ?>
				<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
			</select>
		</div>
	</div>
	<div class="col-md-2 col-sm-12">
							<label for="">Year</label>
							<select name="year" id="year" required class="form-control input_fields" style="height: 30px;">
								<option value="">Select</option>
								<?php
								$dates = range('2019', date('Y'));
								$flag=0;
								foreach($dates as $date){
									$year = $date;
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									elseif($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}

								}
								?>
							</select>
						</div>
						<div class="col-md-2 col-sm-12">
							<label for="">Month</label>
							<select type="text" name="month" id="month" class="form-control input_fields" required style="height: 30px;">
								<option value="">Select</option>
								<?php
								$flag=0;
								for ($i = 1; $i <=12;$i++) {
									$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
									if($this->input->post('month')==$i){
										echo "<option value=".$i." selected>".$date_str ."</option>";
										$flag=1;
									}
									elseif($i==date('m') && $flag==0){
										echo "<option value=".$i." selected>".$date_str ."</option>";
									}
									else{
										echo "<option value=".$i.">".$date_str ."</option>";

									}
								} ?>
							</select>
						</div>
						<div class="col-xs-3 col-md-2">
						<label for="item_type">Report Type</label>
						<div class="form-group">
							<select name="child_report" id="child_report" required class="form-control" style="height: 30px;">
								<?php foreach ($report_type as $value) { ?>

									<option value="<?php echo $value->LookupCode;?>" <?php if($this->input->post('child_report')==$value->LookupCode) { echo 'selected';}?>>
												<?php 
													echo $value->LookupValue;
												?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="col-lg-2 col-md-2 col-sm-12 btn-width">
					<button type="submit" class="btn btn-block btn-success" id="monthreport" style="line-height: 1; font-weight: 600;font-size: 15px;height: 30px;">Search</button>
				</div><span style="margin-top: 25px;margin-right: 25px; float: right;"><a href="javascript:void(0)"><i class="fa fa-2x fa-download" id="excel_button" onclick="tableToExcel('testTable', 'W3C Example','Monthly_Report.xls')" title="Excel Download"></i></a></span>


<?php echo form_close(); ?>
</div>

</div>

<table class="table table-striped table-bordered table-hover" id="table_inventory_list">


	<thead>
		<tr><td colspan="9" style="background-color: #ffcccb;color: black; font-family:'Source Sans Pro';font-size: 14px;text-align: center;font-weight: 500"> This report is valid only if the data entry is complete and correct</td></tr>
		<tr>
			<th>Item</th>
			<th>Opening Balance</th>
			<th>Average Monthly Consumption</th>
			<th>Closing Balance</th>
			<th>Near to expiry stock before  <?php echo date('t-m-Y', strtotime(date($filters1['startdate']).'+1 months')); ?></th>
			<th>Months of stock at hand</th>
			<th>Minimum suggested indent quantity</th>
			<th>Maximum suggested indent quantity</th>
			<th>Action</th>


  

		</tr>
	</thead>
	<tbody>
<!-- </tbody>
</table> -->
<?php // echo "<pre>";/*print_r($inventory_details);*/
//$result=array_unique($inventory_details->);
if (($loginData->user_type==2 && $filters1['child_report']==NULL) || $filters1['id_mstfacility'] !=''){
	$item_id=array();
	$data=array();
	$filtered_items=array();
	foreach ($Inventory_Repo as $key => $value) {
    $filtered_items[$key]['id_mst_drugs'] =$value->drug_name;
    $filtered_items[$key]['strength'] =$value->strength;
    /*$filtered_items['id_mst_drugs'] = $value->id_mst_drugs;
    $filtered_items['id_mst_drugs'] = $value->id_mst_drugs;*/
}
	foreach ($items as $key => $value) {
		foreach ($filtered_items as $key1 => $value1) {
			if($value->id_mst_drugs==$filtered_items[$key1]['id_mst_drugs']){
				$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
				if ($value->drug_abb=='SOF400VEL') {
								$item_id[$key]['drug_name']= $value->drug_name." ".$value1['strength']." ".$value->unit;
							}
							else{
								$item_id[$key]['drug_name']= $value->drug_name;
							}
				$item_id[$key]['strength']= $filtered_items[$key1]['strength'];
			}
		}

	}
	$filtered_arr=array_unique($item_id,SORT_REGULAR);
//pr($filtered_arr);exit();
//pr($inventory_details);
	?>
<!-- <table class="table table-striped table-bordered table-hover">
	<tbody> -->
		<?php if(!empty($Inventory_Repo)){
			foreach ($filtered_arr as $key=>$value_temp) { ?>
				<tr>
					<th colspan="8" class="info"style="border: none;background-color: #333;font-size: 16px;color:#fff;text-align: left;"><?php echo $value_temp['drug_name']; ?></th>
					<th class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus" aria-hidden="true" id="<?php echo 'head'.$value_temp['id_mst_drugs']; ?>"></i></th></tr>
					<?php foreach ($Inventory_Repo as $key => $value) { 
						if($value_temp['id_mst_drugs']==$value->drug_name){
							?>
							<tr class="<?php echo 'item'.$value_temp['id_mst_drugs']; ?>">
								<td class="item">
									<?php if($value->type==1){echo $value_temp['drug_name']." ".$value->strength." mg";} elseif($value->type==2){echo $value_temp['drug_name'];} ?>
								</td>
							<td>
										<?php 
										if ($value->Expire_stock1>0) {

						$total_expired=($value->Expire_stock1-($value->returned_quantity1+$value->total_rejected1+$value->total_utilize1+$value->total_transfer_out1));
					}
					else{
						$total_expired=0;
					}

					$opening_stock=($value->total_receipt1-($value->returned_quantity1+$value->total_rejected1+$value->total_utilize1+$value->total_transfer_out1+$total_expired));
										echo floor($opening_stock); ?>
										
									</td>
										<td>
										<?php  $avgr=floor(($value->total_utilize+$value->total_utilize1)/2);
										echo $avgr;  ?>
									</td>
									<td>
										<?php 
										$closing_stock=(($value->total_receipt +$opening_stock)-($value->returned_quantity+$value->total_rejected+$value->total_utilize+$value->total_transfer_out+$total_expired));
										echo $closing_stock;
											 ?>
									</td>
									<td><?php $Expire_stock_next=($value->Expire_stock_next>0) ? ($value->Expire_stock_next-$closing_stock) : '' ; 
									echo $Expire_stock_next; ?></td>
									<td>
										<?php 
										$stock_forcast_next=NULL;
												if ($avgr>0) {
												$stock_forcast_next=floor(($closing_stock-((!empty($Expire_stock_next)>0) ? $Expire_stock_next : 0))/$avgr);

												echo floor($stock_forcast_next);

										}
										 ?>
									</td>
									<td>
										<?php if (floor($stock_forcast_next)<=$value->lead_time) {
										 	echo floor(($avgr*$value->lead_time)*((1+$value->buffer_stock/100)))-$value->quantity;
										 } ?>	
									</td>
									<td>
										<?php if (floor($stock_forcast_next)<=$value->lead_time) {
										 	echo floor(($avgr*$value->lead_time)*((1+$value->buffer_stock/100)))+floor($avgr*$value->lead_time)-$value->quantity;
										 } ?>	
									</td>
									<td></td>
								</tr>

<?php } }}}else{ ?>
	<tr >
		<td colspan="<?php echo $colspan; ?>" class="text-center">
			NO RECORDS FOUND  BETWEEN SELECTED DATES
		</td>

	</tr>
<?php } }

else if (($loginData->user_type==1 || $loginData->user_type==3 ||  ($loginData->user_type==2 && $filters1['child_report']==1))&&$filters1['id_mstfacility']==''){
		$item_id=array();
	$data=array();
	$filtered_items=array();
	foreach ($Inventory_Repo as $key => $value) {
    $filtered_items[$key]['id_mst_drugs'] =$value->drug_name;
    $filtered_items[$key]['strength'] =$value->strength;
   
}
	foreach ($items as $key => $value) {
		foreach ($filtered_items as $key1 => $value1) {
			if($value->id_mst_drugs==$filtered_items[$key1]['id_mst_drugs']){
				$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
					if ($value->drug_abb=='SOF400VEL') {
								$item_id[$key]['drug_name']= $value->drug_name." ".$value1['strength']." ".$value->unit;
							}
							else{
								$item_id[$key]['drug_name']= $value->drug_name;
							}
				$item_id[$key]['strength']= $filtered_items[$key1]['strength'];
			}
		}

	}
	$filtered_arr=array_unique($item_id,SORT_REGULAR);

	?>

		<?php if(!empty($Inventory_Repo)){
			
			foreach ($filtered_arr as $key=>$value_temp) { 
				?>
				<tr>
					<td class="info"style="font-size: 12px;font: bold; text-align: left;"><?php echo $value_temp['drug_name']; ?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					
					<td class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus item_toggle" aria-hidden="true" id="<?php echo 'item'.$value_temp['id_mst_drugs']; ?>"></i></td></tr>
					<?php  foreach ($Inventory_Repo as $key => $value) { 
						if($value_temp['id_mst_drugs']==$value->drug_name){  

				

							?>
							<tr class="<?php echo 'item'.$value_temp['id_mst_drugs']; ?>">
								<td class="item">

									<?php if(($loginData->user_type==1) &&($filters1['id_search_state']=='')){
										echo $value->StateName;
									}
									else if($loginData->user_type==3 ||($filters1['id_search_state']!='')){
										echo $value->hospital;
									}

									?>
								</td>
								<td>
										<?php 
										if ($value->Expire_stock1>0) {

						$total_expired=($value->Expire_stock1-($value->returned_quantity1+$value->total_rejected1+$value->total_utilize1+$value->total_transfer_out1));
					}
					else{
						$total_expired=0;
					}

					$opening_stock=($value->total_receipt1-($value->returned_quantity1+$value->total_rejected1+$value->total_utilize1+$value->total_transfer_out1+$total_expired));
										echo floor($opening_stock); ?>
										
									</td>
										<td>
										<?php  $avgr=floor(($value->total_utilize+$value->total_utilize1)/2);
										echo $avgr; ?>

									</td>
									<td>
										<?php 
										$closing_stock=(($value->total_receipt +$opening_stock)-($value->returned_quantity+$value->total_rejected+$value->total_utilize+$value->total_transfer_out+$total_expired));
										echo $closing_stock;
											 ?>
									</td>
										<td><?php echo  ($value->Expire_stock_next>0) ? ($value->Expire_stock_next-$closing_stock) : '' ; ?></td>
									<td>
										<?php 
										$stock_forcast_next=NULL;
												if ($avgr>0) {
												$stock_forcast_next=floor(($closing_stock-((!empty($Expire_stock_next)>0) ? $Expire_stock_next : 0))/$avgr);

												echo floor($stock_forcast_next);

										}
										 ?>
									</td>
								<td>
										<?php if (floor($stock_forcast_next)<=$value->lead_time) {
										 	echo floor(($avgr*$value->lead_time)*((1+$value->buffer_stock/100)))-$value->quantity;
										 } ?>	
									</td>
									<td>
										<?php if (floor($stock_forcast_next)<=$value->lead_time) {
										 	echo floor(($avgr*$value->lead_time)*((1+$value->buffer_stock/100)))+floor($avgr*$value->lead_time)-$value->quantity;
										 } ?>	
									</td>
									
									<?php if(($loginData->user_type==1) &&($filters1['id_search_state']=='')){?>
										<td class="info" style="border: none;text-align: right;"><a href="javascript:void(0);" onclick="get_facility_data($(this).parent().closest('tr').index(),'<?php echo $value->type;?>','<?php echo $value->id_mststate;?>','<?php echo $value->id_mst_drugs;?>');"class="fa fa-plus facility_td" aria-hidden="true" id="<?php echo $value->id_mst_drugs.'-'.$value->id_mststate; ?>"></a></td>
									<?php } 
									else if($loginData->user_type==3 ||($filters1['id_search_state']!='')){ ?>
										<td></td>
										<!--<td class="info" style="border: none;text-align: right;"><a href="javascript:void(0);" onclick="get_item_data($(this).parent().closest('tr').index(),'<?php echo $value->type;?>','<?php echo $value->id_mstfacility;?>','<?php echo $value->id_mst_drugs;?>');"class="fa fa-plus item_td" aria-hidden="true" id="<?php echo $value->id_mst_drugs.'-'.$value->id_mstfacility; ?>"></a></td>-->
									<?php } ?>

									

<?php } }}}else{ ?>
	<tr >
		<td colspan="<?php echo $colspan; ?>" class="text-center">
			NO RECORDS FOUND  BETWEEN SELECTED DATES
		</td>

	</tr>
<?php } }?>

</tbody>
</table>
</div>
<br>
</div>
<div class="overlay">
	<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
</div>

<!-- Data Table -->
<script type="text/javascript">

	
	$(document).ready(function(){
		$(".glyphicon-trash").click(function(e){
			var ans = confirm("Are you sure you want to delete?");
			if(!ans)
			{
				e.preventDefault();
			}
		});
		$(".facility_td").click(function(e){
			$(this).toggleClass('fa-plus fa-minus');
			var abc=$(this).attr("id");
			$('.'+abc).toggle();
			if ($('.'+abc).is(':visible')==false) {
				
  				$('.item_row').hide();
			}
		});
		$(".item_toggle").click(function(e){
			$(this).toggleClass('fa-plus fa-minus');
			
			var abc=$(this).attr("id");
			<?php if($loginData->user_type!=2){?>
				
			$('.'+abc).toggle();

			if ($('.'+abc).is(':visible')==false && $('.item_row').is(':visible')==true) {
  				$('.item_row').hide();
			}
			
<?php }else{?>
		$('.'+abc).toggle();
	<?php }?>
		});
		


		
	});

		$(document).ready(function(){

		$("#search_state").trigger('change');
//alert('rff');

});
	$('#search_state').change(function(){

		$.ajax({
			url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
			data : {
				<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
			},
			method : 'GET',
			success : function(data)
			{
//alert(data);
$('#input_district').html(data);
$("#input_district").trigger('change');
//$("#mstfacilitylogin").trigger('change');

},
error : function(error)
{
//alert('Error Fetching Districts');
}
})
	});

	$('#input_district').change(function(){

		$.ajax({
			url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),

			method : 'GET',
			success : function(data)
			{
//alert(data);
if(data!="<option value=''>Select Facilities</option>"){
	$('#mstfacilitylogin').html(data);

}
},
error : function(error)
{
//alert('Error Fetching Blocks');
}
})
	});

function  get_facility_data(row_index,type,id_mststate,id_mst_drugs){

//alert(row_index);
var dataString = {
id_mst_drugs: id_mst_drugs,
id_mststate: id_mststate,
type: type,

};
console.log(dataString);
$.ajax({    
url: '<?php echo base_url();?>Inventory_Reports/get_facility_forcast_data',
type: 'GET',
data: dataString,
dataType: 'json',
async: false,
cache: false,
success: function (data) {


var parsedData = JSON.parse(data.fields);
//console.log(parsedData);
var output = [];

for(var i=0; i<parsedData.length; i++){

var Expire_stock2=(parseInt((parsedData[i]['Expire_stock2'] || 0))-(parseInt(parsedData[i]['returned_quantity2']|| 0)+parseInt(parsedData[i]['total_rejected2']|| 0)+parseInt(parsedData[i]['total_utilize2']|| 0)+parseInt(parsedData[i]['total_transfer_out2']|| 0)));
	if (Expire_stock2<0) {
	Expire_stock2=0;
}
	var opening_stock2=(parseInt((parsedData[i]['total_receipt2'] || 0))-(parseInt(parsedData[i]['returned_quantity2']|| 0)+parseInt(parsedData[i]['total_rejected2']|| 0)+parseInt(parsedData[i]['total_utilize2']|| 0)+parseInt(parsedData[i]['total_transfer_out2']|| 0)+Expire_stock2));


	var Expire_stock1=(parseInt((parsedData[i]['Expire_stock1'] || 0))-(parseInt(parsedData[i]['returned_quantity1']|| 0)+parseInt(parsedData[i]['total_rejected1']|| 0)+parseInt(parsedData[i]['total_utilize1']|| 0)+parseInt(parsedData[i]['total_transfer_out1']|| 0)));
	if (Expire_stock1<0) {
	Expire_stock1=0;
}
	var opening_stock=(parseInt((parsedData[i]['total_receipt1'] || 0))-(parseInt(parsedData[i]['returned_quantity1']|| 0)+parseInt(parsedData[i]['total_rejected1']|| 0)+parseInt(parsedData[i]['total_utilize1']|| 0)+parseInt(parsedData[i]['total_transfer_out1']|| 0)+Expire_stock1));
var closing_stock=((parseInt(parsedData[i]['total_receipt'] || 0)+opening_stock)-(parseInt(parsedData[i]['returned_quantity']|| 0)+parseInt(parsedData[i]['total_rejected']|| 0)+parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_transfer_out']|| 0)));
var Expire_stock=(parseInt((parsedData[i]['Expire_stock'] || 0))-(parseInt(parsedData[i]['returned_quantity']|| 0)+parseInt(parsedData[i]['total_rejected']|| 0)+parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_transfer_out']|| 0)));
if (Expire_stock<0) {
	Expire_stock=0;
}
else{
	closing_stock=closing_stock-Expire_stock;
}
var Expire_stock_next=0;
if (parsedData[i]['Expire_stock_next']>0 || parsedData[i]['Expire_stock_next']!='' || parsedData[i]['Expire_stock_next']!=null) {
	Expire_stock_next=(parsedData[i]['Expire_stock_next'])-closing_stock;

}
var stock_forcast_next='';
var avgr='';
var minimum='';
var maximum='';
avgr=Math.round((parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_utilize1'] || 0)+parseInt(parsedData[i]['total_utilize_pwomen']|| 0)+parseInt(parsedData[i]['total_utilize_pwomen1']|| 0))/2);
if (avgr>0)
stock_forcast_next=Math.round((closing_stock-Expire_stock_next)/avgr);
if (Math.round(stock_forcast_next)<=parseInt(parsedData[i]['lead_time'])) {
minimum=Math.round((avgr*parseInt(parsedData[i]['lead_time']))*((1+parseInt(parsedData[i]['buffer_stock'])/100)));
						
maximum=Math.round((avgr*parseInt(parsedData[i]['lead_time']))*((1+parseInt(parsedData[i]['buffer_stock'])/100)))+Math.round(avgr*parseInt(parsedData[i]['lead_time']));
}
var facility=parsedData[i]['id_mstfacility'];
	str='<tr class="'+id_mst_drugs+'-'+id_mststate+' item'+id_mst_drugs+'">';
str+='<td>'+parsedData[i]['hospital'] +'</td>'
	+' <td>'+opening_stock+'</td>'
	+' <td>'+avgr+'</td>'
	+' <td>'+closing_stock+'</td>'
	+' <td>'+Expire_stock_next+'</td>'
	+' <td>'+Math.round(stock_forcast_next)+'</td>'
	+' <td>'+minimum+'</td>'
	+' <td>'+maximum+'</td>'
	+' <td class="info" style="border: none;text-align: right;"><a class="fa fa-plus item_td"  href="javascript:void(0);" onclick="get_item_data($(this).parent().closest('+"'tr'"+').index(),'+"'"+type+"'"+','+"'"+facility+"'"+','+"'"+id_mst_drugs+"'"+');"  aria-hidden="true" id="'+id_mst_drugs+'-'+facility+'"></a></td>';
str+='</tr>';
output.push(str);
closing_stock=0;
}
 $('table > tbody > tr').eq(row_index).after(output);
 $('#'+id_mst_drugs+'-'+id_mststate).removeAttr('onclick');
 $('.'+id_mst_drugs+'-'+id_mststate).hide();
   }
});
}
function  get_item_data(row_index,type,id_mstfacility,id_mst_drugs){


var dataString = {
id_mst_drugs: id_mst_drugs,
id_mstfacility: id_mstfacility,
type: type,

};
$.ajax({    
url: '<?php echo base_url();?>Inventory_Reports/get_item_forcast_data',
type: 'GET',
data: dataString,
dataType: 'json',
async: false,
cache: false,
success: function (data) {


var parsedData = JSON.parse(data.fields);
console.log(parsedData);
var output = [];

for(var i=0; i<parsedData.length; i++){
var Expire_stock2=(parseInt((parsedData[i]['Expire_stock2'] || 0))-(parseInt(parsedData[i]['returned_quantity2']|| 0)+parseInt(parsedData[i]['total_rejected2']|| 0)+parseInt(parsedData[i]['total_utilize2']|| 0)+parseInt(parsedData[i]['total_transfer_out2']|| 0)));
	if (Expire_stock2<0) {
	Expire_stock2=0;
}
	var opening_stock2=(parseInt((parsedData[i]['total_receipt2'] || 0))-(parseInt(parsedData[i]['returned_quantity2']|| 0)+parseInt(parsedData[i]['total_rejected2']|| 0)+parseInt(parsedData[i]['total_utilize2']|| 0)+parseInt(parsedData[i]['total_transfer_out2']|| 0)+Expire_stock2));


	var Expire_stock1=(parseInt((parsedData[i]['Expire_stock1'] || 0))-(parseInt(parsedData[i]['returned_quantity1']|| 0)+parseInt(parsedData[i]['total_rejected1']|| 0)+parseInt(parsedData[i]['total_utilize1']|| 0)+parseInt(parsedData[i]['total_transfer_out1']|| 0)));
	if (Expire_stock1<0) {
	Expire_stock1=0;
}
	var opening_stock=(parseInt((parsedData[i]['total_receipt1'] || 0))-(parseInt(parsedData[i]['returned_quantity1']|| 0)+parseInt(parsedData[i]['total_rejected1']|| 0)+parseInt(parsedData[i]['total_utilize1']|| 0)+parseInt(parsedData[i]['total_transfer_out1']|| 0)+Expire_stock1));
var closing_stock=((parseInt(parsedData[i]['total_receipt'] || 0)+opening_stock)-(parseInt(parsedData[i]['returned_quantity']|| 0)+parseInt(parsedData[i]['total_rejected']|| 0)+parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_transfer_out']|| 0)));
var Expire_stock=(parseInt((parsedData[i]['Expire_stock'] || 0))-(parseInt(parsedData[i]['returned_quantity']|| 0)+parseInt(parsedData[i]['total_rejected']|| 0)+parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_transfer_out']|| 0)));
if (Expire_stock<0) {
	Expire_stock=0;
}
else{
	closing_stock=closing_stock-Expire_stock;
}
var Expire_stock_next=0;
if (parsedData[i]['Expire_stock_next']>0 || parsedData[i]['Expire_stock_next']!='' || parsedData[i]['Expire_stock_next']!=null) {
	Expire_stock_next=(parsedData[i]['Expire_stock_next'])-closing_stock;

}
var stock_forcast_next='';
var avgr='';
var minimum='';
var maximum='';
avgr=Math.round((parseInt(parsedData[i]['total_utilize']|| 0)+parseInt(parsedData[i]['total_utilize1']|| 0)+parseInt(parsedData[i]['total_utilize_pwomen']|| 0)+parseInt(parsedData[i]['total_utilize_pwomen1']|| 0))/2);
if (avgr>0)
stock_forcast_next=Math.round((closing_stock-Expire_stock_next)/avgr);
if (Math.round(stock_forcast_next)<=parseInt(parsedData[i]['lead_time'])) {
minimum=Math.round((avgr*parseInt(parsedData[i]['lead_time']))*((1+parseInt(parsedData[i]['buffer_stock'])/100)));
						
maximum=Math.round((avgr*parseInt(parsedData[i]['lead_time']))*((1+parseInt(parsedData[i]['buffer_stock'])/100)))+Math.round(avgr*parseInt(parsedData[i]['lead_time']));
}
var facility=parsedData[i]['id_mstfacility'];
	str='<tr class="'+id_mst_drugs+'-'+id_mstfacility+' item_row">';
str+='<td></td>'
	+' <td>'+opening_stock+'</td>'
	+' <td>'+avgr+'</td>'
	+' <td>'+closing_stock+'</td>'
	+' <td>'+Expire_stock_next+'</td>'
	+' <td>'+Math.round(stock_forcast_next)+'</td>'
	+' <td>'+minimum+'</td>'
	+' <td>'+maximum+'</td>';
str+='</tr>';
output.push(str);
closing_stock=0;
opening_stock=0;
Expire_stock=0;
Expire_stock1=0;
}
 $('table > tbody > tr').eq(row_index).after(output);
 $('#'+id_mst_drugs+'-'+id_mstfacility).removeAttr('onclick');
 $('.'+id_mst_drugs+'-'+id_mstfacility).hide();
//alert(output);
   }
});
}
$(document).on('click', ".item_td", function() {
	$(this).toggleClass('fa-plus fa-minus');
			var abc=$(this).attr("id");
			$('.'+abc).toggle();
	});
$(document).ready(function(){

$('#year').change(function(){
	var count=0;
	<?php  $count=0; ?>;
	var options='<option value="">Select</option>';
	var currentYear = (new Date).getFullYear();
	if (currentYear==$('#year').val()) {
			<?php
		$flag=0;
		$count=date("m");
	for ($i = 1; $i <=$count;$i++) {
	$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
	if($this->input->post('month')==$i){ 	?>
	options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php $flag=1;
	}	
	elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
	<?php }
	else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	}
	else{
		<?php
		$flag=0;
		$count=12;
		for ($i = 1; $i <=$count;$i++) {
		$date_str = date("M", mktime(0, 0, 0, $i, 10)); 
		if($this->input->post('month')==$i){ 	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php $flag=1;
		}	
		elseif($i==date('m') && $flag==0){	?>
		options+='<?php echo "<option value=".$i." selected>".$date_str ."</option>"; ?>';
		<?php }
		else{ ?>
		options+='<?php echo "<option value=".$i.">".$date_str ."</option>"; ?>'; 
<?php
		}
	} ?>
	
	}
	//alert(options);
$('#month').html(options);
});
$("#year").trigger('change');
});
</script>



