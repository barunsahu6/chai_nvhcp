<style>
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: top;
		width: 8.33% !important;
	}
	td{
		text-align: center; 
		vertical-align: middle;
		width: 8.33% !important;
		word-break: break-all;
	}

	.batch_num{
		width: 100px !important;
	}
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	input,nav,label,span{
		font-family: 'Source Sans Pro';

	}
	label{
		font-family: 'Source Sans Pro';
		font-size: 14px;
	}
	input,select,.form-control{
		height: 30px;
	}
	.modal-lg{
		width: 87%;
	}
	.pd-right-0{padding-right: 0px;}
	.pd-left-0{padding-left: 0px;}

	select[readonly] option, select[readonly] optgroup {
    display: none;
}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData'); ?>
<!-- add/edit facility contact modal starts-->
<div class="modal fade " tabindex="-1" role="dialog" id="myModal1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #333;color: white;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Relocation</h4>
      </div>
      <div class="modal-body">
      	<?php
           $attributes = array(
              'id' => 'Add_Relocation_form',
              'name' => 'Add_Relocation_form',
               'autocomplete' => 'off',
            );
        
           echo form_open('Inventory/relocation/', $attributes); ?>
        		<div class="row">
					<div class="col-md-6 col-sm-12">
							<div class="row">
						<div class="col-xs-12 col-md-5 col-sm-12 text-left" id="labelfor">
				<label for="item_name">Item Name <span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-md-7 col-sm-12 pd-left-0" id="item_div" tabindex="1">
					<select name="item_name" id="item_name" required class="form-control">
						<!-- <option value="">-----Select-----</option>
										<?php foreach ($items as $value) { ?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_name')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php if($value->type==2){echo $value->drug_name; } else{
													echo $value->drug_name." (".$value->strength.")";
												} ?></option>
											<?php } ?> -->
										</select>
				</div>
			</div>
			<div class="row">
<div class="col-xs-12 col-md-5 col-sm-12 text-left">
        			<label for="contact_name">Relocation to Facility/ State Warehouse<span class="text-danger">*</span></label>
        		</div>
        		<div class="form-group col-md-7 pd-left-0 col-sm-12">
					<select name="from_to_type" id="from_to_type" required class="form-control">
						<option value="">----Select----</option>
					<?php foreach ($source_name as $value) { 
					if($value->LookupCode== '1' || $value->LookupCode== '2'){ ?>
						<option value="<?php echo $value->LookupCode;?>">
							<?php echo $value->LookupValue; ?></option>
					<?php } }?>
				</select>
        		</div>
</div>
<div class="row">
				<div class="col-xs-12 col-md-5 col-sm-12">
					<label for="source_name">Name<span class="text-danger">*</span></label>
				</div>
				<div class="col-md-7 col-sm-12 form-group pd-left-0" id="source_div">
					<select name="source_name" id="source_name" required class="form-control">
					<option value="">--Select--</option>
					</select>
				</div>
				<div class="col-md-6 col-sm-12 form-group" id="unrecognised_div">
					<input type="text" class="form-control" id="unrecognised" name="unrecognised" placeholder="Specify the Name" value="<?php if(isset($receipt_details[0]->unrecognised)){  echo $receipt_details[0]->unrecognised;}?>">
				</div>
			</div>
	<div class="row">
<div class="col-xs-12 col-md-5 col-sm-12 text-left">
        			<label for="indent_num">Indent Number <span class="text-danger">*</span></label>
        		</div>
        			<div class="form-group col-xs-12 col-md-7 pd-left-0 col-sm-12">
        		<select name="indent_num" id="indent_num" required class="form-control">
           					<option value=''>----Select----</option>
           				</select>
        		</div>
        	</div>
        	<div class="row">
           			
           			<div class="col-xs-12 col-md-5 text-left" >
           				<label for="batch_num">Batch Num<span class="text-danger">*</span></label>
           			</div>
           			<div class="form-group col-xs-12 col-md-7 pd-left-0 col-sm-12">
           				<select name="batch_num" id="batch_num" required class="form-control">
           					<option value=''>----Select----</option>
           				</select>
           			</div>
           		</div>
        		<div class="row">
        		<div class="col-xs-12 col-md-5 col-sm-12 text-left" id="labelfor">
				<label for="requested_quantity">Quantity requested to be relocated (screening tests/bottle)<span class="text-danger">*</span></label>
				</div>
				<div class="form-group col-md-7 col-sm-12 pd-left-0" id="item_div" style="border: none;">
					<input type="text" class="form-control" name="requested_quantity" id="requested_quantity" required >
				</div>
			</div>
				
			
					</div>
					<div class="col-md-6 col-sm-12">

						<div class="row">
						<div class="col-xs-12 col-md-6 col-sm-12">
							<label for="contact_name">Date on which relocation request received<span class="text-danger">*</span></label>
						</div>
						<div class="col-md-6 col-sm-12 form-group">
							<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="Receipt Date" name="request_date" id="request_date" required onkeydown="return false" onkeyup="return false" max="<?php echo date('d-m-Y');?>" onchange="checkexpirydate()">
						</div>

					</div>
						<div class="row">
						<div class="col-xs-12 col-md-6 col-sm-12">
							<label for="contact_name">Date of Dispatch<span class="text-danger">*</span></label>
						</div>
						<div class="col-md-6 col-sm-12 form-group">
							<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="Expiry Date" name="dispatch_date" id="dispatch_date" onchange="checkexpirydate()" max="<?php echo date('d-m-Y');?>" required onkeydown="return false" onkeyup="return false" >
						</div>  	
					</div>
						<div class="row">
							<div class="col-xs-12 col-md-6 col-sm-12 text-left" id="labelfor">
								<label for="relocated_quantity">Quantity Relocated (screening tests/bottle)<span class="text-danger">*</span></label>
							</div>
							<div class="form-group col-md-6 col-sm-12">
								<input type="text" class="form-control" name="relocated_quantity" id="relocated_quantity" required >
							</div>

						</div>
							<div class="row">
							<div class="col-xs-12 col-md-6 col-sm-12 text-left" id="labelfor">
								<label for="not_relocated_quantity">Quantity Not Relocated (screening tests/bottle)<span class="text-danger">*</span></label>
							</div>
							<div class="form-group col-md-6 col-sm-12">
								<input type="text" class="form-control" name="not_relocated_quantity" id="not_relocated_quantity" readonly required >
							</div>

						</div>	
						<!-- <div class="row">
							<div class="col-xs-12 col-md-6 col-sm-12">
								<label for="relocation_status">Status of relocation<span class="text-danger"></span></label>
							</div>
							<div class="col-md-6 col-sm-12 form-group">
								<input type="text" class="form-control" id="relocation_status" name="relocation_status" placeholder="Relocation Status" readonly>
							</div>
						
						
						</div> -->
						
							<div class="row">
								<div class="col-xs-12 col-md-6 col-sm-12 text-left" id="labelfor">
									<label for="issue_num">Issue Number <span class="text-danger">*</span></label>
								</div>
								<div class="col-md-6 col-sm-12 form-group">
									<input type="text" class="form-control" id="issue_num" name="issue_num" placeholder="Issue Number" required readonly>
								</div>
							</div>
							<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Remarks<span class="text-danger" id="remark_label">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<textarea class="form-control" id="relocation_remark" name="relocation_remark">
</textarea>
        		</div>
        	</div>
							
						</div>
					</div>
					<div class="row">
				<h5 id="bottom_note" style="color: #a94442;text-align: center;font: bold">If you cannot find the item or the batch number in the dropdown, make sure you have recorded receipt of this item or raised an indent or stock currently available is greater than 0.</h5>
			</div>		
  </div>
<input type="hidden" name="rem_quantity" id="rem_quantity">
<input type="hidden" name="quantity" id="quantity">
<input type="hidden" name="quantity_val" id="quantity_val">
<input type="hidden" name="indent_date" id="indent_date">
<input type="hidden" name="indent_date_val" id="indent_date_val">
<input type="hidden" id="inventory_id" name="inventory_id" value="">
<input type="hidden" id="rem_values" name="rem_values" value="">
<input type="hidden" id="indent_num_val" name="indent_num_val" value="">
<input type="hidden" id="item_name_val" name="item_name_val" value="">
<input type="hidden" id="batch_num_val" name="batch_num_val" value="">
<input type="hidden" id="issue_num_val" name="issue_num_val">
<input type="hidden" id="is_temp" name="is_temp">
<input type="hidden" id="transfer_to_val" name="transfer_to_val" value="">
<input type="hidden" id="unrecognised_val" name="unrecognised_val" value="">
<input type="hidden" id="Entry_Date_val" name="Entry_Date_val" value="">
<input type="hidden" id="is_manual" name="is_manual" value="">
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" name="save" value="save" id="submit_btn">Save</button>
      </div>
    </div>
        <?php echo form_close(); ?>
  </div>
</div><!--  /.modal -->

<!-- add/edit facility contact modal ends-->
<div class="row" style="padding-top: 20px;padding-bottom: 20px;">
	<div class="col-lg-10 text-center">
			<div class="col-md-4 col-md-offset-6" style="padding-right: 15px;">
			<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 15px;padding-bottom: 15px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i>  Add New Relocation
</a>
		</div>
	</div>
</div>
<div class="row main-div" style="min-height:400px;">
	<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12 left_nav_bar">
			<?php //include("left_nav_bar.php");
			$this->load->view("inventory/components/left_nav_bar"); ?>
		</div>

		<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 set_margin">
			<div class="panel panel-default" id="Record_receipt_panel" style="padding-bottom: 55px;">
				<div class="panel-heading" style=";background: #333;">
					<h4 class="text-center" style="color: white;background: #333" id="Record_form_head">Relocation</h4>
				</div>
				<div class="row" style="padding-top: 32px;">
					<?php 
					$tr_msg= $this->session->flashdata('tr_msg');
					$er_msg= $this->session->flashdata('er_msg');
					if(!empty($tr_msg)){  ?>
						

						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						<?php } else if(!empty($er_msg)){ ?>
							<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
								
							<?php } ?>

							<!-- <div class="col-md-3 pull-right">
										<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 10px;padding-bottom: 10px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i>  Add New Relocation
							</a>
									</div> -->
						</div>
				
						<?php
						$attributes = array(
							'id' => 'Receipt_filter_form',
							'name' => 'Receipt_filter_form',
							'autocomplete' => 'false',
						);
  //pr($items);
  //print_r($receipt_details);

						echo form_open('', $attributes); ?>
							<div class="row" style="padding-left: 10px;">
							<div class="col-xs-3 col-md-2">
								<label for="item_type">Item Name <span class="text-danger">*</span></label>
								<div class="form-group">
									<select name="item_type" id="item_type" required class="form-control">
										<option value="drugkit" <?php if($this->input->post('item_type')=='drugkit') { echo 'selected';}?>>All Kits and Drugs</option>
										<option value="Kit"<?php if($this->input->post('item_type')=='Kit') { echo 'selected';}?>>All Screening Test Kits</option>
										<option value="drug"<?php if($this->input->post('item_type')=='drug') { echo 'selected';}?>>All Drugs</option>
										
										<?php foreach ($items as $value) { ?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php if($value->type==2){echo $value->drug_name; } else{
													echo $value->drug_name." ".$value->strength." mg";
												} ?></option>
											<?php } ?>
											<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
										</select>
									</div>
								</div>
								
								
						
								<div class="col-md-2 col-sm-12 form-group">
									<label for="year">Financial Year <span class="text-danger">*</span></label>
									<select name="year" id="year" required class="form-control" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2018', date('Y'));
										$flag=0;
										foreach($dates as $date){

									if (date('m', strtotime($date)) < 4) {//Upto April
										$year = ($date-1) . '-' . $date;
									} else {//After April
										$year = $date . '-' . ($date + 1);
									}
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									else if($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
									
								}
								?>
							</select>
						</div>
								<div class="col-md-2 col-sm-12">
									<label for="">From Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')){ echo $this->input->post('startdate');} else{ echo date('01-04-Y');}  ?>" required style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 col-sm-12">
									<label for="">To Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')){echo $this->input->post('enddate');}else{echo timeStampShow(date('Y-m-d'));} ?>" required  style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 btn-width pull-right" style="padding-right: 30px;">
									<label for="">&nbsp;</label>
									<button class="btn btn-block" style="line-height: 1.2; font-weight: 600;background-color: #f4860c;border-color: #f4860c;color: white;">SEARCH</button>
								</div>

								<?php echo form_close(); ?>
							</div>
							
						</div>

						
							</div>
							<div class="row" style="margin-right: 10px;margin-left: 5px;">
								
								<div class="col-md-9">
								
								<dl>
									<dt class="red"></dt>
								    <dd>New Relocation Request</dd>
								    <dt class="yellow"></dt>
								    <dd>Order dispatched to facility</dd>

								    <dt class="orange"></dt>
								    <dd>Partial order dispatched to facility</dd>

								     <dt class="green"></dt>
								    <dd>Order received by facility</dd>

								    
								</dl>
									
									</div>					
							</div>
							<div class="col-md-12 table-responsive">
								<table class="table table-striped table-bordered table-hover" id="table_inventory_list">

							
							<thead>
							<tr style="background-color: #085786; font-family: 'Source Sans Pro';color: white;font-size: 13px;">
									<th style="width: 5px !important;'">St.</td>
									<th>Item</th>
									<th>Indent Number</th>
									<th>Quantity to be <br>relocated (screening <br>tests/bottle)</th>
									<th>Relocation to <br>Facility/ State<br> Warehouse</th>
									<th>Date on which <br>relocation request<br> received</th>
									<th>Date of Dispatch</th>
									<th>Quantity <br>Relocated (screening <br>tests/bottle)</th>
									<th>Batch Number</th>
									<th>Status of relocation</th>
									<th>Issue No</th>
									<th>Remarks</th>
									<th>Commands</th>
								</tr>
							</thead>
							<tbody>
								
			
				<?php 
				$item_id=array();
				foreach ($items as $key => $value) {
					foreach ($inventory_detail_all as $value1) {
						if($value->id_mst_drugs==$value1->drug_name){
							$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
							if ($value->id_mst_drugs==8) {
								$item_id[$key]['drug_name']= $value->drug_name." ".$value1->strength." mg";
							}
							else{
								$item_id[$key]['drug_name']= $value->drug_name;
							}
						}
					}
					
				}
				$filtered_arr=array_unique($item_id,SORT_REGULAR);
				//pr($filtered_arr);exit();
				//pr($relocation_items);
				?>
				<!-- <table class="table table-striped table-bordered table-hover">
					<tbody> -->
						<?php if(!empty($inventory_detail_all)){
							foreach ($filtered_arr as $key=>$value_temp) { ?>
								<tr>
									<th colspan="12" class="info"style="border: none;background-color: #333;font-size: 16px;color:#fff;text-align: left;"><?php echo $value_temp['drug_name']; ?></th>
									<th class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus" aria-hidden="true" id="<?php echo 'head'.$value_temp['id_mst_drugs']; ?>"></i></th></tr>
									<?php foreach ($inventory_detail_all as  $value) { 
										if($value_temp['id_mst_drugs']==$value->drug_name){
											?>
											<tr class="<?php echo $value_temp['id_mst_drugs']; ?>" style="background-color:<?php if ($value->is_temp=='1') {
												echo '#ADD8E6';
											} ?>">
												<td class="<?php if ($value->is_temp=='0'){
													if (($value->relocation_status=='' || $value->relocation_status==NULL || $value->relocation_status==0) && ($value->is_notification==1 || $value->is_notification=='1')){
														echo 'red_status';
														}
														elseif ($value->relocation_status==1){
														echo 'yellow_status';
												}
												elseif ($value->relocation_status==2){
														echo 'orange_status';
												}
												elseif ($value->relocation_status==3){
														echo 'green_status';
												}
											}
												?>"style="width: 5px !important;"><?php if((($value->relocation_status=='' || $value->relocation_status==NULL) && ($value->is_notification==0 || $value->is_notification=='0')) || $value->is_temp=='1'){ ?><img style="width: 40px;" src="<?php echo site_url(); ?>/common_libs/images/new_alert3.png" alt="new alert"><?php }?></td>
												<td class="item">
													<?php if($value->type==1){echo $value_temp['drug_name']."(".$value->strength.")";} elseif($value->type==2){echo $value_temp['drug_name'];} ?>
												</td>
												<td>
													<?php echo $value->indent_num; ?>
												</td>
												<td>
													<?php echo $value->requested_quantity; ?>
												</td>
											<td>
													<?php 
													if ($value->facility_short_name==NULL) {
													
													if ($value->from_to_type==3){
														echo "Unregistered Source";
													}
													else if ($value->from_to_type==4){
														echo "Third Party";
													}
													elseif ($value->from_to_type==1) {
														if ($value->source_name==996) {
															echo "abc";
														}
														if ($value->source_name==997) {
															echo "abcd";
														}
														if ($value->source_name==998) {
															echo "abcde";
														}
														
													}
													}
													else{
														echo $value->facility_short_name;
													}

													?>
												</td>
												<td nowrap>
													<?php echo timeStampShow($value->request_date); ?>
												</td>
												<td nowrap>
													<?php echo timeStampShow($value->dispatch_date); ?>
												</td>
											<td>
													<?php echo $value->relocated_quantity; ?>
												</td>
												<td>
													<?php echo $value->batch_num; ?>
												</td>
												
												<td class=<?php 

													if ($value->is_temp=='0'){

												if ($value->relocation_status==1){
														echo 'yellow_status';
												}
												elseif ($value->relocation_status==2){
														echo 'orange_status';
												}
												elseif ($value->relocation_status==3){
														echo 'green_status';
												}
												
												}
												/*else{

												}	*/
												?>>

													<?php foreach ($relocation_status as $relocation) {
														# code...
													 if($value->relocation_status==$relocation->LookupCode) { echo $relocation->LookupValue;}} ?>
												</td>
													<td>
													<?php echo $value->issue_num; ?>
												</td>
												
													<td>
													<?php echo $value->relocation_remark; ?>
												</td>
												
											
												<td class="text-center">
													<?php if ( (($value->relocation_status<=2 and $value->status=='m') || (($value->relocation_status=='' || $value->relocation_status==NULL || $value->relocation_status==0) and $value->status=='n')) || $value->is_temp=='1') { ?>
													
													<a href="" style="padding : 4px;" title="Edit" data-toggle="modal" data-target="#myModal1" onclick='get_relocation_data(<?php echo $value->inventory_id ?>,"<?php echo $value->status; ?>");'><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;" ></span></a>
												<?php } ?>
														 <?php if ($value->is_temp=='1') {?> 
													 <a href="<?php echo site_url()."/inventory/delete_receipt/".$value->inventory_id."/L";?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
												 <?php }  ?> 
												</td>
												
											</tr>
										<?php } }}}else{ ?>
											<tr >
												<td colspan="13" class="text-center">
													NO RECORDS FOUND  BETWEEN SELECTED DATES.
												</td>

											</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
							<br>
						</div>
						<div class="overlay">
							<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
						</div>

						<!-- Data Table -->
						<script type="text/javascript">

						
	$('#from_to_type').ready(function(){ 
        fill_source_name();        
});
    $('#from_to_type').change(function(){ 
        fill_source_name();        
});
$("#item_name,#dispatch_date,#from_to_type").on('keypress change',function(evt){

var item=$('#item_name').val();

    			//alert(item);
var EntryDate1=$('#dispatch_date').val();
 var parts = EntryDate1.split('-');
 var date=parts[2] + parts[1] +  parts[0];
var issue_num='SI/';
//var batch_num='BN/';


var issue_num_val=$('#issue_num_val').val();
if (issue_num_val!="") {
 var issueparts = issue_num_val.split('/');
 var issue_date_year=issueparts[3].substring(0, 4);
  var issue_date_month=issueparts[3].substring(4, 6);
   var issue_date_day=issueparts[3].substring(6, 8);
   issue_date_val=new Date(issue_date_month+"-"+issue_date_day+"-"+issue_date_year);
}
<?php foreach ($items as $value){ ?>
if (item==<?php echo $value->id_mst_drug_strength; ?>) {
	<?php
		if ($value->type==1) {
			$strength=$value->strength;
		}
		else{
			$strength='';
		}

		?>
			
	$.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Inventory/get_issue_sequence", // <-- properly quote this line
    cache: false,
    async : true,
    data: {drug_name:'<?php echo $value->id_mst_drugs; ?>', dispatch_date: EntryDate1, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'}, 
    success: function(data) {
        var returned = JSON.parse(data);
        //console.log(returned);
     var issue_count = returned.item_sequence.map(function(e) {return e.issue_count;});
    // var batch_count = returned.item_sequence.map(function(e) {return e.batch_count;});
   //issue_num=issue_num+'<?php echo $value->drug_abb."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(issue_count)+1);

    if (issue_num_val!="" && ('<?php echo $value->drug_abb.$strength; ?>'==issueparts[1])) {
    if (($('#dispatch_date').datepicker('getDate'))<issue_date_val || (($('#dispatch_date').datepicker('getDate'))>issue_date_val )) {
     	 issue_num=issue_num+'<?php echo $value->drug_abb.$strength."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(issue_count)+1);
     }
    else {
    	if (($('#dispatch_date').datepicker('getDate'))!=issue_date_val) {
    		issue_num=issue_num+'<?php echo $value->drug_abb.$strength."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(issue_count)+1);
    	}
    	else{
    		issue_num=$("#issue_num_val").val();
    	}
     	 
     	 //alert($("#issue_num_val").val()+"hi");
     }
     }
     else if (issue_num_val!="" && ('<?php echo $value->drug_abb.$strength; ?>'!=issueparts[1])) {
     	 issue_num=issue_num+'<?php echo $value->drug_abb."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(issue_count)+1);
     	 //alert(issue_num+"bye");
     }
     else{
     	issue_num=issue_num+'<?php echo $value->drug_abb.$strength."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(issue_count)+1);
     	//alert(issue_num+"hii");
     }



    $('#issue_num').val(issue_num);   
  /*  if ($('#from_to_type').val()==3) {
    	//batch_num+='USLP/';
    	//batch_num=batch_num+'<?php echo $value->drug_abb."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(batch_count)+1);
    	//$('#batch_num').val(batch_num);
    }

    else if ($('#from_to_type').val()==4) {
    	batch_num+='TP/';
    	batch_num=batch_num+'<?php echo $value->drug_abb."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(batch_count)+1);
    	$('#batch_num').val(batch_num);
    }
    else{
    	$('#batch_num').val('');
    }*/

}
});
	

}
	
<?php } ?>
	
});
 $("#modal_close,#close_icon").on('click ',function(evt){
	empty_all_input();

});
$("#myModal1").on('hide.bs.modal', function(){
   // alert('The modal is about to be hidden.');
   empty_all_input();
   
  });
function empty_all_input(){
	$('.modal-title').html('Add Stock Relocation');
  $('#dispatch_date').val('<?php echo date("d-m-Y"); ?>');
	$("#rem_quantity").val('');
	$("#inventory_id").val('');
	$("#rem_values").val('');
	$("#indent_num_val").val('');
	$("#batch_num_val").val('');
	$("#issue_num_val").val('');
	$("#from_to_type_val").val('');
	$("#source_name_val").val('');
	$("#unrecognised_val").val('');
	$("#indent_date_val").val('');
	$("#indent_num").val('');
	$("#from_to_type").val('');
	$("#quantity_rejected_val").val('');
	$("#item_name_val").val('');
    $("#rejection_reason_val").val('');
    $("#is_manual").val('');
    $('#submit_btn,#relocation_remark').removeAttr('disabled');
    $('#remark_label').hide();
    get_rem_item_name();
     $('#Add_Relocation_form').find("input[type=text],textarea,select").val("");
     $('select,#request_date,#requested_quantity').removeClass( "isDisabled");
    $('#item_name,#indent_num,#requested_quantity,#from_to_type,#source_name,#request_date').removeAttr('readonly','readonly');

}
function get_relocation_data(inventory_id,status){
$('#inventory_id').val(inventory_id);
$('#is_manual').val(status);
//alert(status);
var is_temp=[];
$('.modal-title').html('Update Stock Relocation ');
//alert($('#inventory_id').val());
$.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Inventory/edit_inventory_data", // <-- properly quote this line
    cache: false,
    async : false,
    data: {inventory_id:inventory_id,relocation_remark:status, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'}, 
    success: function(data) {
        var returned = JSON.parse(data);
        console.log(returned);
    var id_mst_drugs = returned.inventory_details.map(function(e) { return e.id_mst_drugs; });

     var indent_num = returned.inventory_details.map(function(e) { return e.indent_num; });
      var issue_num = returned.inventory_details.map(function(e) { return e.issue_num; });
      var relocated_quantity = returned.inventory_details.map(function(e) { return e.relocated_quantity; });
       var from_to_type = returned.inventory_details.map(function(e) { return e.from_to_type; });
 var requested_quantity = returned.inventory_details.map(function(e) { return e.requested_quantity; });
        var control_used = returned.inventory_details.map(function(e) { return e.control_used; });

      var request_date = returned.inventory_details.map(function(e) { return e.request_date; });

        var dispatch_date = returned.inventory_details.map(function(e) { return e.dispatch_date; });

       var relocation_remark = returned.inventory_details.map(function(e) { return e.relocation_remark; });

       var batch_num = returned.inventory_details.map(function(e) { return e.batch_num; });
       var indent_accept_date = returned.inventory_details.map(function(e) { return e.indent_accept_date; });
        is_temp = returned.inventory_details.map(function(e) { return e.is_temp; });

       if (status=='n' || (status=='m' && (indent_accept_date=='' || indent_accept_date==null || indent_accept_date=='0000-00-00'))) {
       	  var transfer_to = returned.inventory_details.map(function(e) { return e.id_mstfacility; });
       	 // alert(transfer_to);
       }
     else{
     	  var transfer_to = returned.inventory_details.map(function(e) { return e.transfer_to; });
     }

       var unrecognised = returned.inventory_details.map(function(e) { return e.unrecognised; });
 if (requested_quantity==0 || requested_quantity==null || requested_quantity=='') {
       	  	 var requested_quantity = returned.inventory_details.map(function(e) { return e.pending_quantity; });
       	  }

var date1='';
var date2='';
//alert(dispatch_date);
if(request_date!=''){
		 var parts = request_date[0].split('-');
 		var date1=parts[2] + "-"+ parts[1] + "-" + parts[0];
}
      
if(dispatch_date!=''){
		 var parts = dispatch_date[0].split('-');
 		var date2=parts[2] + "-"+ parts[1] + "-" + parts[0];
}
 if (dispatch_date=='0000-00-00') {
 date2='';
 }     
if (request_date=='0000-00-00') {
 		 date1='';
 }     
 		
	

	$("#batch_num_val").val(batch_num);
 	$("#indent_num_val").val(indent_num);
 	$("#issue_num_val").val(issue_num);
 	$("#is_temp").val(is_temp);
 	$("#dispatch_date").val(date2);
 	$("#transfer_to_val").val(transfer_to);
 	$("#unrecognised_val").val(unrecognised);

    $("#item_name_val").val(id_mst_drugs);
 	get_rem_item_name();
 	$("#indent_num").val(indent_num);
 	//$('#indent_num').trigger('change');
    $("#relocated_quantity").val(relocated_quantity);
	$("#requested_quantity").val(requested_quantity);
	$("#from_to_type").val(from_to_type);
    $("#from_to_type").trigger('change');
    $("#request_date").val(date1);
    $("#relocation_remark").val(relocation_remark);
    //$("#issue_num").val('123');
//alert(from_to_type);
}
});
if (status=='n') {
	$('#item_name,#indent_num,#requested_quantity,#from_to_type,#source_name,#request_date').attr('readonly','readonly');
	$('#item_name,#indent_num,#requested_quantity,#from_to_type,#source_name,#request_date').addClass('isDisabled');

}
else{
	$('#item_name,#indent_num,#requested_quantity,#from_to_type,#source_name,#request_date').removeAttr('readonly','readonly');
	$('#item_name,#indent_num,#requested_quantity,#from_to_type,#source_name,#request_date').removeClass('isDisabled');
}
//alert(is_temp[0]);
if (is_temp[0]=='1') {
	$('#item_name,#indent_num,#from_to_type,#source_name').attr('readonly','readonly');
	$('#item_name,#indent_num,#from_to_type,#source_name').addClass('isDisabled');

}

}
function fill_source_name(){
var id=$('#from_to_type').val();

if(id!=3 || id!=4){
$('#source_div').show();
            	$('#unrecognised_div').hide();
            	$('#source_name').attr('required','required');
    			$('#unrecognised').removeAttr('required');
}
 /* var fid=<?php if(isset($receipt_details[0]->source_name)){ echo $receipt_details[0]->source_name;}else{echo '0';}?>; */     
				if(id==1){
					var opt='';
					opt += '<option value="996">abc</option>';
					opt += '<option value="997">abcd</option>';
					opt+=  '<option value="998">abcde</option>';
					$('#source_name').html(opt);
					$('#source_name').val($('#transfer_to_val').val());
					//$('#batch_num').removeAttr('readonly');
				}
                else if(id==2){
                $.ajax({
                    url : "<?php echo site_url('inventory/get_facilities');?>",
                    method : "GET",
                   data : {
					<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
				},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
                        //console.log(returned);
        				var id_mstfacility = returned.id_mstfacility.map(function(e) {
   						return e.id_mstfacility;
						});
						var facility_short_name = returned.id_mstfacility.map(function(e) {
   						return e.facility_short_name;
						});
                        var i;
                        var select=''
                        var opt='';
                        opt+='<option value="">----Select----</option>'
                        for(i=0; i<id_mstfacility.length; i++){
                        	
                        		select='';
                        
                            opt += '<option value="'+id_mstfacility[i]+'"'+select+'>'+facility_short_name[i]+'</option>';
                        }
                        //console.log(opt);
                        $('#source_name').html(opt);
 						$('#source_name').val($('#transfer_to_val').val());
 						get_indent_num();
                    }
                });
              //  $('#batch_num').removeAttr('readonly');
            }else if(id==3 || id==4){

            	$('#source_div').hide();
            	$('#unrecognised_div').show();
            	$('#source_name').removeAttr('required');
    			$('#unrecognised').attr('required','required');
    			$('#unrecognised').val($('#unrecognised_val').val());
    			//$('#batch_num').attr('readonly','readonly');
    			
    			
            }
          
            

}   
		
							$(document).ready(function(){
								get_rem_item_name();
								$('#remark_label').hide();
								$(".glyphicon-trash").click(function(e){
									var ans = confirm("Are you sure you want to delete?");
									if(!ans)
									{
										e.preventDefault();
									}
								});
								<?php foreach ($filtered_arr as $key=>$value_temp) {
									$elemnt_id= ".".$value_temp['id_mst_drugs'];?> 
									$("<?php echo $elemnt_id; ?>").show();
									$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").click(function(){
										if($("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").hasClass('fa-minus')){
											$("<?php echo $elemnt_id; ?>").hide();
											$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-minus').addClass("fa-plus");
										}
										else{
											$("<?php echo $elemnt_id; ?>").show();
											$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-plus').addClass("fa-minus");
										}
										
									});
									$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").css('cursor', 'pointer');
								<?php }?>
							});
function checkexpirydate()
	{
		var request_date = $('#request_date').datepicker('getDate');
		var dispatch_date = $('#dispatch_date').datepicker('getDate');
   		//alert(Receipt_Date+'/'+dispatch_date);
        if ( request_date > dispatch_date && dispatch_date!=null) { 
        	$("#modal_header").text("Dispatch date cannot be before Request Date");
			$("#modal_text").text("Please check dates");
			$("#dispatch_date" ).val('');
			$("#multipurpose_modal").modal("show");
			return false;
        }
 		else{
 			 return true;
 		}
       
    }
/*$('#indent_num').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9{2}]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test($.trim(str))) {
        return true;
    }
    else{
    	e.preventDefault();
    return false;
    }
    
});*/
/*$('#batch_num').on('keypress keyup',function(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
								
								if ((charCode > 31 && (charCode < 48 || charCode > 57))){
									evt.preventDefault();
									return false;
								}

});*/

$("#relocated_quantity,#requested_quantity").on('keypress keyup',function(evt){

								var charCode = (evt.which) ? evt.which : event.keyCode
								
								if ((charCode > 31 && (charCode < 48 || charCode > 57)) || parseInt($(this).val()+ evt.key)>10000){
									evt.preventDefault();
									return false;
								}
								
								if($(this).val() <=0 && $(this).val()!='')
								{
									$("#modal_header").html('Quantity must be greater than 0');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$(this).val('');
									return false;
								}
								
							});
$("#requested_quantity").on('keyup',function(evt){

							if ($('#indent_num').val()!='') {
								if($(this).val()>parseInt($('#quantity').val()))
								{
									$("#modal_header").html('Requested Quantity must be lesser than indented quantity: ('+$('#quantity').val()+')');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$(this).val('');
									return false;
								}
								if ($("#requested_quantity").val()!="" && $("#relocated_quantity").val()!='') {
									not_relocated_quantity=parseInt($("#requested_quantity").val())-parseInt($("#relocated_quantity").val());
									$("#not_relocated_quantity").val(not_relocated_quantity);
								}
								}
							});
$("#relocated_quantity").on('keyup',function(evt){
								var error_css = {"border":"1px solid red"};
								if($('#item_name').val().trim() == ""){
		
									$("#modal_header").text("Please Select Item");
									$(this).val('');
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#item_name").css(error_css);
									$("#item_name").focus();
									evt.preventDefault();
									return false;
								}
								else if($('#indent_num').val().trim() == ""){

									$("#modal_header").text("Please Select Indent Num");
									$(this).val('');
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#indent_num").css(error_css);
									$("#indent_num").focus();
									evt.preventDefault();
									return false;
								}
								else if($('#batch_num').val().trim() == ""){

									$("#modal_header").text("Please Select Batch Num");
									$(this).val('');
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#batch_num").css(error_css);
									$("#batch_num").focus();
									evt.preventDefault();
									return false;
								}
								else{
								var values=($('#rem_values').val()).toString();
									var values_arr=values.split(',');
									var index=($("#batch_num")[0].selectedIndex);
									var quantity=parseInt(values_arr[index-1]);
								if(parseInt(($(this).val()))>quantity)
								{
									$("#modal_header").html('Relocated Quantity must be less than Remaining Quantity : '+quantity);
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$(this).val('');
									return false;
								}
								else if(parseInt(($(this).val()))>parseInt(($('#requested_quantity').val())))
								{
									$("#modal_header").html('Relocated Quantity is greater than Requested Quantity');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$(this).val('');
									$('#relocation_remark').attr('required','required');
									//$('#relocation_remark').attr("placeholder", "Add Remark for relocating Extra Stock");

									//$('#remark_label').show();
									return false;
								}
								 if(parseInt(($(this).val()))<parseInt(($('#requested_quantity').val())))
								{
									
									$('#relocation_remark').removeAttr('required','required');
									//$('#remark_label').hide();
									
								}
								if ($("#requested_quantity").val()!="" && $("#relocated_quantity").val()!='') {
									not_relocated_quantity=parseInt($("#requested_quantity").val())-parseInt($("#relocated_quantity").val());
									$("#not_relocated_quantity").val(not_relocated_quantity);
								}
								}

							
								
							});
$('#source_name').change(function () {
		
		get_indent_num();
	
	
});
function get_indent_num(){


	var from_to_type=$('#from_to_type').val();
	var item=$('#item_name').val() || $('#item_name_val').val();
	var source_name=$('#source_name').val();
	var inventoryid=$('#inventory_id').val();
	var ismanual=$('#is_manual').val();

	if(item!=null && ismanual=='n'){
                $.ajax({
                    url : "<?php echo site_url('inventory/get_indent_num_relocation');?>",
                    method : "GET",
                    data : {item_name: item,inventory_id:inventoryid, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                         console.log(data);
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
        				var html = returned.get_indent_num.map(function(e) {
   						return e.indent_num;
						});
						/*var rem = returned.get_indent_num.map(function(e) {
   						return e.rem;
						});
						 $('#rem_quantity').val(rem);*/
						 //alert($('#rem_quantity').val());
						//console.log(returned);
						/*type_id = returned.batch_nums.map(function(e) {
   						return e.type;
						});
						//alert(To_Date);

						if(type_id == 1){
			$('#quantityRemlbl').html('');
			$('#quantityRemlbl').html('Quantity of bottles available');
		}
		 if(type_id == 2){
			//console.log('hi');
			$('#quantityRemlbl').html('');
			$('#quantityRemlbl').html('Number of screening tests available');
		}*/
                        var i;
                        var opt='';
                        opt +='<option value="">-----Select-----</option>';
                        for(i=0; i<html.length; i++){

								
								select='';
                        
                        	opt += '<option value="'+html[i]+'"'+select+'>'+html[i]+'</option>'; 	
                   
                            
                        }
                        //console.log(opt);
                        $('#indent_num').html(opt);
                        $('#indent_num').val($("#indent_num_val").val());
                    	  
 						
                    }
                });
           }
           else if(item!=null && (ismanual=='m' || ismanual=='')){
           		$.ajax({
                    url : "<?php echo site_url('inventory/get_manual_indent_num');?>",
                    method : "GET",
                    data : {item_name: item,inventory_id:inventoryid,id_mstfacility:source_name, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                         console.log(data);
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
        				var html = returned.get_indent_num.map(function(e) {
   						return e.indent_num;
						});
						var quantity = returned.get_indent_num.map(function(e) {
   						return e.quantity;
						});
						var indent_date = returned.get_indent_num.map(function(e) {
   						return e.indent_date;
						});
						  $('#quantity_val').val(quantity);
						  $('#indent_date_val').val(indent_date);
                        var i;
                        var opt='';
                        opt +='<option value="">-----Select-----</option>';
                        for(i=0; i<html.length; i++){

								
								select='';
                        
                        	opt += '<option value="'+html[i]+'"'+select+'>'+html[i]+'</option>'; 	
                   
                            
                        }
                        //console.log(opt);
                        $('#indent_num').html(opt);
                        $('#indent_num').val($("#indent_num_val").val());
                    	  
 						
                    }
                });
           }
       
             
}	
 $('#item_name,#relocated_quantity,#requested_quantity,#rejection_reason,#from_to_type,#unrecognised,#source_name,#indent_num,#batch_num,#dispatch_date').on('change click keypress',function(e){
						
	var error_css = {"border":"1px solid #c7c5c5"};
			$(this).css(error_css);
	return true;

});
$('#submit_btn').click(function(e){
		var error_css = {"border":"1px solid red"};


		if($('#item_name').val().trim() == ""){

			$("#item_name").css(error_css);
			$("#item_name").focus();
			e.preventDefault();
			return false;
		}
if ($('#is_manual').val()!='n') {
		 var indent_num=$('#issue_num').val().split("/");
		if (indent_num.length<5) {
			$("#modal_header").html('Ask Request facility or warehouse for complete indent number');
									$("#modal_text").html('Please fill in Correct indent Number');
									$("#multipurpose_modal").modal("show");
									$("#indent_num").css(error_css);
									$("#indent_num").focus();
									return false;
		}
		if($('#indent_num').val().trim() == ""){

			$("#indent_num").css(error_css);
			$("#indent_num").focus();
			e.preventDefault();
			return false;
		}
}
		 if(parseInt($('#requested_quantity').val())<parseInt($('#relocated_quantity').val())){

			if($('#relocation_remark').val().trim() == ""){

			$("#relocation_remark").css(error_css);
			$("#relocation_remark").focus();
			e.preventDefault();
			return false;
		}
		}
 if($('#from_to_type').val().trim() == ""){

			$("#from_to_type").css(error_css);
			$("#from_to_type").focus();
			e.preventDefault();
			return false;
		}
 if($('#from_to_type').val().trim() == "1" || $('#from_to_type').val().trim() == "2"){
	if($('#source_name').val().trim() == ""){

			$("#source_name").css(error_css);
			$("#source_name").focus();
			e.preventDefault();
			return false;
		}
}
 if($('#from_to_type').val().trim() == "3" || $('#from_to_type').val().trim() == "4"){
	
	if($('#unrecognised').val().trim() == ""){

			$("#unrecognised").css(error_css);
			$("#unrecognised").focus();
			e.preventDefault();
			return false;
		}
}

		 if($('#batch_num').val().trim() == ""){

			$("#batch_num").css(error_css);
			$("#batch_num").focus();
			e.preventDefault();
			return false;
		}

		if($('#relocated_quantity').val().trim()=="" || parseInt($('#relocated_quantity').val())==0){

			$("#relocated_quantity").css(error_css);
			$("#relocated_quantity").focus();
			e.preventDefault();
			return false;
		}
			
 if($('#request_date').val().trim()==""){

			$("#request_date").css(error_css);
			$("#request_date").focus();
			e.preventDefault();
			return false;
		}
 
 if($('#dispatch_date').val().trim()==""){

			$("#dispatch_date").css(error_css);
			$("#dispatch_date").focus();
			e.preventDefault();
			return false;
		}	
return true;
	});
$("#Add_Relocation_form").submit(function() {
            $(this).submit(function() {
                return false;
            });
            return true;
        });
				
$('#item_name').change(function () {
	
get_indent_num();
	var item=$('#item_name').val();
	var inventoryid=$('#inventory_id').val();
	///var indentnum=$('#indent_num').val() || $('#indent_num_val').val();
	//alert(indentnum);
	var type_id=[];
	if(item!=null){
                $.ajax({
                    url : "<?php echo site_url('inventory/get_data_for_utilize');?>",
                    method : "GET",
                    data : {item_name: item,inventory_id:inventoryid ,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                        console.log(data);
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
        				var html = returned.get_batch_num.map(function(e) {
   						return e.batch_num;
						});
						var rem_values = returned.get_batch_num.map(function(e) {
   						return e.rem;
						});
						var Entry_Date = returned.get_batch_num.map(function(e) {
   						return e.Entry_Date;
						});
						$('#Entry_Date_val').val(Entry_Date);
                        var i;
                        var opt='';
                        opt +='<option value="">----Select----</option>';
                        for(i=0; i<html.length; i++){

								
								select='';
                        
                        	opt += '<option value="'+html[i]+'"'+select+'>'+html[i]+'</option>'; 	
                   
                            
                        }
                        //console.log(opt);
                        $('#batch_num').html(opt);
                   //$('#batch_num').trigger('change');

 						$("#batch_num").val($('#batch_num_val').val());
 						if ($("#is_manual").val()=='n' || $("#is_manual").val()=='m') {
 						if(html.length==0){
                   		$("#modal_header").text("You cannot record any relocation as quantity of stock available as per records is 0. If the quantity of stock currently available is greater than 0, please correct enteries made  in other modules against this item.");
								$("#modal_text").text("Please check records");
									$("#multipurpose_modal").modal("show");
									$('#submit_btn').attr('disabled','true');
                   }
                   }
						$('#rem_values').val(rem_values);

                    }
                });
           }
                return false;
});		
		$('#batch_num').change(function () {
								if ($('#batch_num').val()!=''){
									var values=($('#rem_values').val()).toString();
									var values_arr=values.split(',');
									var index=($("#batch_num")[0].selectedIndex);
									var quantity=parseInt(values_arr[index-1]);
									$('#available_quantity').val(quantity);
								
								}	
								});	
$('#dispatch_date').on('change',function(e){
var error_css = {"border":"1px solid red"};
var To_Date =($('#to_Date').datepicker('getDate'));
if($('#item_name').val().trim() == ""){
		
									$("#modal_header").text("Please Select Item");
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#item_name").css(error_css);
									$("#item_name").focus();
									e.preventDefault();
									return false;
								}
		else if($('#indent_num').val().trim() == ""){

									$("#modal_header").text("Please Select Batch Num");
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#indent_num").css(error_css);
									$("#indent_num").focus();
									e.preventDefault();
									return false;
								}												
	else if($('#batch_num').val().trim() == ""){

									$("#modal_header").text("Please Select Batch Num");
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#batch_num").css(error_css);
									$("#batch_num").focus();
									e.preventDefault();
									return false;
								}

					else{	
					 $('#indent_num').trigger('change');		
					var dispatch_date =($('#dispatch_date').datepicker('getDate'));
					var last_Date=($('#Entry_Date_val').val()).toString();
					var date_arr=last_Date.split(',');
					var index=($("#batch_num")[0].selectedIndex);
					var parts=date_arr[index-1].split('-');
					var Entry_Date=new Date(parts[1] + '-' + parts[2] + '-' + parts[0]);
					//alert(Entry_Date);
					var indent_date=new Date($('#indent_date').val());
					// alert(entrydate+"**"+dispatch_date);
								if (dispatch_date < Entry_Date) { 
									$("#modal_header").text("Dispatch Date cannot be before Receipt Date("+(parts[2] + '-' + parts[1] + '-' + parts[0])+")");
									$("#modal_text").text("Please check dates");
									$("#dispatch_date" ).val('');
									$("#multipurpose_modal").modal("show");
									return false;
								}
								if (dispatch_date < indent_date) { 
									$("#modal_header").text("Request Date cannot be before indent Date("+$('#indent_date').val()+")");
									$("#modal_text").text("Please check dates");
									$("#dispatch_date" ).val('');
									$("#multipurpose_modal").modal("show");
									return false;
								}
								else{
									return true;
								}
							}
});			
$('#request_date').on('change',function(e){
var error_css = {"border":"1px solid red"};
var To_Date =($('#to_Date').datepicker('getDate'));
if($('#item_name').val().trim() == ""){
		
									$("#modal_header").text("Please Select Item");
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#item_name").css(error_css);
									$("#item_name").focus();
									e.preventDefault();
									return false;
								}
		else if($('#indent_num').val().trim() == ""){

									$("#modal_header").text("Please Select Batch Num");
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#indent_num").css(error_css);
									$("#indent_num").focus();
									e.preventDefault();
									return false;
								}												
	else if($('#batch_num').val().trim() == ""){

									$("#modal_header").text("Please Select Batch Num");
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#batch_num").css(error_css);
									$("#batch_num").focus();
									e.preventDefault();
									return false;
								}

					else{		
					 $('#indent_num').trigger('change');	
					var request_date =($('#request_date').datepicker('getDate'));
					var indent_date=new Date($('#indent_date').val());
					var last_Date=($('#indent_date').val()).toString();
					var date_arr=last_Date.split(',');
					var index=($("#indent_num")[0].selectedIndex);
					var parts=date_arr[index-1].split('-');
					//alert($('#indent_date').val());
								if (request_date < indent_date) { 
									$("#modal_header").text("Request Date cannot be before indent Date("+(parts[1] + '-' + parts[0] + '-' + parts[2])+")");
									$("#modal_text").text("Please check dates");
									$("#request_date" ).val('');
									$("#multipurpose_modal").modal("show");
									return false;
								}
								else{
									return true;
								}
							}
});					
function get_rem_item_name(){

var ismanual=$('#is_manual').val();
var itemname=$('#item_name_val').val();
	var inventoryid=$('#inventory_id').val();
                $.ajax({
                    url : "<?php echo site_url('inventory/get_rem_item_name');?>",
                    method : "GET",
                    data : {inventory_id: inventoryid,is_manual:ismanual,item_name:itemname, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                         console.log(data);
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
        				var id_mst_drug = returned.get_rem_items.map(function(e) {
   						return e.id_mst_drug_strength;
						});
						var drug_name = returned.get_rem_items.map(function(e) {
   						return e.drug_name;
						});
						var strength = returned.get_rem_items.map(function(e) {
   						return e.strength;
						});
						 var type = returned.get_rem_items.map(function(e) {
   						return e.type;
						});
                        var i;
                        var opt='';
                        opt +='<option value="" selected>----Select----</option>';
                        for(i=0; i<id_mst_drug.length; i++){

								
								select='';
                        if (type[i]==1) {
                        	
                        		opt += '<option value="'+id_mst_drug[i]+'"'+select+'>'+drug_name[i]+' '+strength[i]+' mg</option>'; 
                       
                        }
                 	else if(type[i]==2){
                 		opt += '<option value="'+id_mst_drug[i]+'"'+select+'>'+drug_name[i]+'</option>'; 
                 	}	
                            
                        }
                        if(id_mst_drug.length==0 && ismanual!=''){
                        	 $.ajax({
                    url : "<?php echo site_url('inventory/get_rem_item_name');?>",
                    method : "GET",
                    data : {item_name:itemname,is_manual:'n',<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                         console.log(data);
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
        				var id_mst_drug = returned.get_rem_items.map(function(e) {
   						return e.id_mst_drug_strength;
						});
						var drug_name = returned.get_rem_items.map(function(e) {
   						return e.drug_name;
						});
						var strength = returned.get_rem_items.map(function(e) {
   						return e.strength;
						});
						 var type = returned.get_rem_items.map(function(e) {
   						return e.type;
						});
                        var i;
                        var opt='';
                        opt +='<option value="" selected>----Select----</option>';
                        for(i=0; i<id_mst_drug.length; i++){

								
								select='';
                        if (type[i]==1) {
                        	
                        		opt += '<option value="'+id_mst_drug[i]+'"'+select+'>'+drug_name[i]+' '+strength[i]+' mg</option>'; 
                       
                        }
                 	else if(type[i]==2){
                 		opt += '<option value="'+id_mst_drug[i]+'"'+select+'>'+drug_name[i]+'</option>'; 
                 	}	
                            
                        }
                         $('#item_name').html(opt);
                         $("#item_name").val($("#item_name_val").val());
                         $('#item_name').trigger('change');
                   		
								}
   					});
                   }

                   else{
                   	$('#submit_btn').removeAttr('disabled');
                   }
                        //console.log(opt);
                        $('#item_name').html(opt);
                         $("#item_name").val($("#item_name_val").val());
                         $('#item_name').trigger('change');

                    }
                });
          
						
}			
$("#indent_num").on('change',function(evt){
if ($('#indent_num').val()!='') {
	var quantity=($('#quantity_val').val()).toString();
	var quantity_arr=quantity.split(',');
	var index=($("#indent_num")[0].selectedIndex);
	
	var val=quantity_arr[index-1];
	$('#quantity').val(val);
	}
					var last_Date=($('#indent_date_val').val()).toString();
					//alert(last_Date);
					var date_arr=last_Date.split(',');
					var parts=date_arr[index-1].split('-');
					var indent_date_val=parts[1] + '-' + parts[2] + '-' + parts[0];
					$('#indent_date').val(indent_date_val);
						//alert($('#indent_date').val());		
});																				
						</script>


