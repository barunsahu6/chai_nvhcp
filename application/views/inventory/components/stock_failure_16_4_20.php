<style>
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: middle;
	}
	td{
		text-align: center; 
		vertical-align: middle;
	}
	.btn-width{
	width: 12%;
}
	.item{
		width: 140px !important;
	}
	.batch_num{
		width: 100px !important;
	}
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	input,nav,label,span{
		font-family: 'Source Sans Pro';

	}
	label{
		font-family: 'Source Sans Pro';
		font-size: 14px;
	}
	input,select,.form-control{
		height: 30px;
	}
	
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData'); ?>
<!-- add/edit facility contact modal starts-->
<div class="modal fade" tabindex="-1" role="dialog" id="myModal1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #333;color: white;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_icon" style="color: white;"><span aria-hidden="true" style="color: white;">&times;</span></button>
        <h4 class="modal-title">Add Stock Failure</h4>
      </div>
      <div class="modal-body">
      	<?php  $attributes = array(
              'id' => 'stock_failure_form',
              'name' => 'stock_failure_form',
               'autocomplete' => 'off',
            );
          
           echo form_open('Inventory/stock_failure/', $attributes); ?>
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Item Name<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<select name="item_name" id="item_name" required class="form-control">
						<option value="">-----Select-----</option>
										<?php foreach ($failure_items as $value) { ?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_name')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php if($value->type==2){echo $value->drug_name; } else{
													echo $value->drug_name." (".$value->strength.")";
												} ?></option>
											<?php } ?>
										</select>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="batch_num">Batch Num<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<select name="batch_num" id="batch_num" required class="form-control">
						<option value=''>----Select----</option>
				</select>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="returned_quantity">Quantity of stock returned due to stock failure (screening tests/bottle)<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<input type="text" class="form-control" name="returned_quantity" id="returned_quantity" required>
        		</div>
        	</div>
        		
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Date stock return initiated<span class="text-danger">*</span></label>
        		</div>

        		<div class="col-xs-12 col-lg-6 form-group">
					<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="Date" name="failure_date" id="failure_date" value="<?php echo timeStampShow(date('Y-m-d')); ?>" required onkeydown="return false" onkeyup="return false" max="<?php echo date('Y-m-d');?>">
        		</div>
        	</div>

        	       <div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="failure_issue">Describe issue faced<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
        			<textarea rows="4" cols="50" class="form-control" id="failure_issue" name="failure_issue" required>
        			</textarea>
        		</div>
        	</div>
        		 <input type="hidden" id="inventory_id" name="inventory_id" value="">
        		 <input type="hidden" id="quantity_received" name="quantity_received" value="" disabled="disabled">
        		  <input type="hidden" id="Entry_Date" name="Entry_Date" value="" disabled="disabled">
        		  <input type="hidden" id="batch_num_val" name="batch_num_val" value="" disabled="disabled">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="modal_close">Close</button>
        <button type="submit" class="btn btn-primary" id="submit_btn" name="save" value="save">Save</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>

<!-- add/edit facility contact modal ends-->


<div class="row main-div" style="min-height:400px;margin-top: 1em;">
	<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12" style="padding: 0;">
			<?php //include("left_nav_bar.php");
			$this->load->view("inventory/components/left_nav_bar"); ?>
		</div>

		<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
			<div class="panel panel-default" id="Record_receipt_panel">
				<div class="panel-heading" style=";background: #333;">
					<h4 class="text-center" style="color: white;background: #333" id="Record_form_head">Stock Failure</h4>
				</div>
				<div class="row">
					<?php 
					$tr_msg= $this->session->flashdata('tr_msg');
					$er_msg= $this->session->flashdata('er_msg');
					if(!empty($tr_msg)){  ?>
						

						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						<?php } else if(!empty($er_msg)){ ?>
							<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
								
							<?php } ?>
							<div class="col-md-3 pull-right">
			<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 10px;padding-bottom: 10px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i> Add Stock Failure
</a>
		</div>
						</div>
						<?php
						$attributes = array(
							'id' => 'Receipt_filter_form',
							'name' => 'Receipt_filter_form',
							'autocomplete' => 'off',
						);
  //pr($items);
  //print_r($inventory_details);

						echo form_open('', $attributes); ?>
							<div class="row" style="padding-left: 10px;">
							<div class="col-xs-3 col-md-2">
								<label for="item_type">Item Name <span class="text-danger">*</span></label>
								<div class="form-group">
									<select name="item_type" id="item_type" required class="form-control">
										<option value="drugkit" <?php if($this->input->post('item_type')=='drugkit') { echo 'selected';}?>>All Kits and Drugs</option>
										<option value="Kit"<?php if($this->input->post('item_type')=='Kit') { echo 'selected';}?>>All Screening Test Kits</option>
										<option value="drug"<?php if($this->input->post('item_type')=='drug') { echo 'selected';}?>>All Drugs</option>
										
										<?php foreach ($items as $value) { ?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php if($value->type==2){echo $value->drug_name; } else{
													echo $value->drug_name." (".$value->strength.")";
												} ?></option>
											<?php } ?>
											<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
										</select>
									</div>
								</div>
								
								
						
								<div class="col-md-2 col-sm-12 form-group">
									<label for="year">Financial Year <span class="text-danger">*</span></label>
									<select name="year" id="year" required class="form-control" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2018', date('Y'));
										$flag=0;
										foreach($dates as $date){

									if (date('m', strtotime($date)) < 4) {//Upto April
										$year = ($date-1) . '-' . $date;
									} else {//After April
										$year = $date . '-' . ($date + 1);
									}
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									else if($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
									
								}
								?>
							</select>
						</div>
								<div class="col-md-2 col-sm-12">
									<label for="">From Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')){ echo $this->input->post('startdate');} else{ echo timeStampShow(date("Y-m-d", mktime(0, 0, 0, date("m"), 1)));}  ?>" required style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 col-sm-12">
									<label for="">To Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')){echo $this->input->post('enddate');}else{echo timeStampShow(date('Y-m-d'));} ?>" required  style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 btn-width pull-right" style="padding-right: 30px;">
									<label for="">&nbsp;</label>
									<button class="btn btn-block" style="line-height: 1.2; font-weight: 600;background-color: #f4860c;border-color: #f4860c;color: white;">SEARCH</button>
								</div>

								<?php echo form_close(); ?>
							</div>
							
						</div>

						<table class="table table-striped table-bordered table-hover" id="table_inventory_list">

							
							<thead>
								<tr style="background-color: #085786; font-family: 'Source Sans Pro';color: white;font-size: 13px;">
									<th>Item</th>
									<th>Batch no.</th>
									<th>Quantity of stock returned<br> due to stock failure <br>(screening tests/bottle)</th>

									<th>Describe issue faced</th>
									<th>Date stock return initiated</th>
									<th>Stock return status</th>
									<th>Commands</th>
								</tr>
							</thead>
							<tbody>
			<!-- </tbody>
			</table> -->
				<?php // echo "<pre>";/*print_r($inventory_details);*/
				//$result=array_unique($inventory_details->);
				$item_id=array();
				foreach ($items as $key => $value) {
					foreach ($inventory_detail_all as $value1) {
						if($value->id_mst_drugs==$value1->drug_name){
							$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
							$item_id[$key]['drug_name']= $value->drug_name;
						}
					}
					
				}
				$filtered_arr=array_unique($item_id,SORT_REGULAR);
				//pr($filtered_arr);exit();
				//pr($inventory_details);
				?>
				<!-- <table class="table table-striped table-bordered table-hover">
					<tbody> -->
						<?php if(!empty($inventory_detail_all)){
							foreach ($filtered_arr as $key=>$value_temp) { ?>
								<tr>
									<th colspan="6" class="info"style="border: none;background-color: #333;font-size: 16px;color:#fff;text-align: left;"><?php echo $value_temp['drug_name']; ?></th>
									<th class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus" aria-hidden="true" id="<?php echo 'head'.$value_temp['id_mst_drugs']; ?>"></i></th></tr>
									<?php foreach ($inventory_detail_all as  $value) { 
										if($value_temp['id_mst_drugs']==$value->drug_name){
											?>
											<tr class="<?php echo $value_temp['id_mst_drugs']; ?>">
												<td class="item">
													<?php if($value->type==1){echo $value_temp['drug_name']."(".$value->strength.")";} elseif($value->type==2){echo $value_temp['drug_name'];} ?>
												</td>
												<td>
													<?php echo $value->batch_num; ?>
												</td>
												<td>
													<?php if($value->type==1){echo $value->returned_quantity;} elseif($value->type==2){echo $$value->quantity_screening_tests;} ?>
												</td>
												<td>
													<?php echo $value->failure_issue; ?>
												</td>
												<td nowrap>
													<?php echo timeStampShow($value->failure_date); ?>
												</td>
												
											<td nowrap>
													<?php echo ""; ?>
												</td>
												
												<td class="text-center">
													<?php if($value->indent_status==0){?>
													<a href="" style="padding : 4px;" title="Edit" data-toggle="modal" data-target="#myModal1" onclick='get_failture_data(<?php echo $value->inventory_id ?>);'><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;" ></span></a>
													<a href="#" onclick='delete_indent_data(<?php echo $value->inventory_id ?>);'style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
												<?php }?>
												</td>

											</tr>
										<?php } }}}else{ ?>
											<tr >
												<td colspan="10" class="text-center">
													NO RECORDS FOUND  BETWEEN SELECTED DATES.
												</td>

											</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
							<br>
						</div>
						<div class="overlay">
							<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
						</div>
				<!-- Data Table -->
						<script type="text/javascript">

							
							$("#returned_quantity").on('keypress keyup',function(evt){

								var charCode = (evt.which) ? evt.which : event.keyCode
								
								if ((charCode > 31 && (charCode < 48 || charCode > 57))){
									evt.preventDefault();
									return false;
								}
								
								if(parseInt($('#returned_quantity').val()) <=0 && $('#returned_quantity').val()!='')
								{
									$("#modal_header").html('Quantity must be greater than 0');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$('#returned_quantity').val('');
									return false;
								}
								
							});
$("#returned_quantity").on('keyup',function(evt){
	var error_css = {"border":"1px solid red"};
								if($('#item_name').val().trim() == ""){
		
									$("#modal_header").text("Please Select Item");
									$(this).val('');
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#item_name").css(error_css);
									$("#item_name").focus();
									evt.preventDefault();
									return false;
								}
								else if($('#batch_num').val().trim() == ""){

									$("#modal_header").text("Please Select Batch Num");
									$(this).val('');
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#batch_num").css(error_css);
									$("#batch_num").focus();
									evt.preventDefault();
									return false;
								}
								else{
									var values=($('#quantity_received').val()).toString();
									var values_arr=values.split(',');
									var index=($("#batch_num")[0].selectedIndex);
									var quantity_received=parseInt(values_arr[index-1]);
									if(parseInt($('#returned_quantity').val())> quantity_received)
								{
									$("#modal_header").html('Return Quantity must be less than Received Quantity');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$('#returned_quantity').val('');
									return false;
								}
								
								}
								if (parseInt($(this).val())>10000){
									evt.preventDefault();
									return false;
								}

				
});							
function get_failture_data(inventory_id){
$('#inventory_id').val(inventory_id);
$('.modal-title').html('Update Stock Failure ');
//alert($('#inventory_id').val());
$.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Inventory/edit_inventory_data", // <-- properly quote this line
    cache: false,
    async : true,
    data: {inventory_id:inventory_id, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'}, 
    success: function(data) {
        var returned = JSON.parse(data);
        console.log(returned);
    var id_mst_drugs = returned.inventory_details.map(function(e) { return e.id_mst_drugs; });
     var returned_quantity = returned.inventory_details.map(function(e) { return e.returned_quantity; });
      var failure_date = returned.inventory_details.map(function(e) { return e.failure_date; });

       var failure_issue = returned.inventory_details.map(function(e) { return e.failure_issue; });

       var batch_num = returned.inventory_details.map(function(e) { return e.batch_num; });
//alert(batch_num);
       var parts = failure_date[0].split('-');
 		var date=parts[2] + "-"+ parts[1] + "-" + parts[0];

    $("#item_name").val(id_mst_drugs);
 	$('#item_name').trigger('change');
    $("#returned_quantity").val(returned_quantity);

    $("#failure_date").val(date);
 	$("#batch_num_val").val(batch_num);
    $("#failure_issue").html(failure_issue);

}
});
}
$("#modal_close,#close_icon").on('click ',function(evt){
	$('#inventory_id').val('');
	$('.modal-title').html('Add Stock Failure');

});
$('#item_name,#returned_quantity,#failure_date,#failure_issue,#batch_num').on('change click keypress',function(e){
						
	var error_css = {"border":"1px solid #c7c5c5"};
			$(this).css(error_css);
	return true;

});
$('#failure_date').on('change',function(e){
var error_css = {"border":"1px solid red"};
if($('#item_name').val().trim() == ""){
		
									$("#modal_header").text("Please Select Item");
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#item_name").css(error_css);
									$("#item_name").focus();
									e.preventDefault();
									return false;
								}
	else if($('#batch_num').val().trim() == ""){

									$("#modal_header").text("Please Select Batch Num");
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#batch_num").css(error_css);
									$("#batch_num").focus();
									e.preventDefault();
									return false;
								}
					else{			
					var returned_date =($('#failure_date').datepicker('getDate'));
					var Entrydate1=($('#Entry_Date').val()).toString();
					var date_arr=Entrydate1.split(',');
					var index=($("#batch_num")[0].selectedIndex);
					var parts=date_arr[index-1].split('-');
					var entrydate=new Date(parts[1] + '-' + parts[2] + '-' + parts[0]);
					// alert(entrydate+"**"+returned_date);
								if (returned_date < entrydate && entrydate!=null) { 
									$("#modal_header").text("Stock Return date cannot be before From Receipt Date("+(parts[2] + '-' + parts[1] + '-' + parts[0])+")");
									$("#modal_text").text("Please check dates");
									$("#failure_date" ).val('<?php echo date("d-m-Y")?>');
									$("#multipurpose_modal").modal("show");
									return false;
								}
								 
								else{
									return true;
								}
							}
});

 $("#myModal1").on('hide.bs.modal', function(){
   // alert('The modal is about to be hidden.');
   
 $('#stock_failure_form').find("input[type=text],input[type=hidden], textarea,select").val("");
  $('#failure_date').val('<?php echo date("d-m-Y"); ?>')
  });
$('#submit_btn').click(function(e){
		var error_css = {"border":"1px solid red"};
//alert($('#batch_num').val());

		if($('#item_name').val().trim() == ""){

			$("#item_name").css(error_css);
			$("#item_name").focus();
			e.preventDefault();
			return false;
		}
if($('#batch_num').val().trim() == ""){

			$("#batch_num").css(error_css);
			$("#batch_num").focus();
			e.preventDefault();
			return false;
		}
		 if($('#returned_quantity').val().trim()=="" || parseInt($('#returned_quantity').val())==0){

			$("#returned_quantity").css(error_css);
			$("#returned_quantity").focus();
			e.preventDefault();
			return false;
		}
 if($('#failure_date').val().trim()==""){

			$("#failure_date").css(error_css);
			$("#failure_date").focus();
			e.preventDefault();
			return false;
		}
		if($('#failure_issue').val().trim()==""){

			$("#failure_issue").css(error_css);
			$("#failure_issue").focus();
			e.preventDefault();
			return false;
		}
		
			return true;

	});
$("#stock_failure_form").submit(function() {
            $(this).submit(function() {
                return false;
            });
            return true;
        });
							$(document).ready(function(){
								
								<?php foreach ($filtered_arr as $key=>$value_temp) {
									$elemnt_id= ".".$value_temp['id_mst_drugs'];?> 
									$("<?php echo $elemnt_id; ?>").show();
									$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").click(function(){
										if($("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").hasClass('fa-minus')){
											$("<?php echo $elemnt_id; ?>").hide();
											$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-minus').addClass("fa-plus");
										}
										else{
											$("<?php echo $elemnt_id; ?>").show();
											$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-plus').addClass("fa-minus");
										}
										
									});
									$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").css('cursor', 'pointer');
								<?php }?>
							});

							
			function delete_indent_data(inventory_id){
									
									   		 $( "#dialog-confirm" ).dialog({
									   		open : function() {
										    $("#dialog-confirm").closest("div[role='dialog']").css({top:100,left:'40%'});
											}, 	
									      resizable: false,
									      height: "auto",
									      width: 400,
									      modal: true,
									      buttons: {
									        "Cancel request": function() {
									          window.location.replace('<?php echo base_url(); ?>Inventory/delete_receipt/'+inventory_id+'/I');

									        },
									        "Don't Cancel request": function() {
									          $( this ).dialog( "close" );
									          e.preventDefault();
									        }
									      }
									 			 });
								};		
		$('#item_name').change(function () {
	

	var item=$('#item_name').val();
	var type_id=[];
	if(item!=null){
                $.ajax({
                    url : "<?php echo site_url('inventory/get_batch_num');?>",
                    method : "GET",
                    data : {item_name: item, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                        // console.log(data);
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
        				var html = returned.get_batch_num.map(function(e) {
   						return e.batch_num;
						});
						var Entry_Date = returned.get_batch_num.map(function(e) {
   						return e.Entry_Date;
						});
						var quantity_received = returned.get_batch_num.map(function(e) {
   						return e.quantity_received;
						});

						$('#Entry_Date').val(Entry_Date);
						$('#quantity_received').val(quantity_received);
                        var i;
                        var opt='';
                        opt +='<option value="">----Select----</option>';
                        for(i=0; i<html.length; i++){

								
								select='';
                        
                        	opt += '<option value="'+html[i]+'"'+select+'>'+html[i]+'</option>'; 	
                   
                            
                        }
                        //console.log(opt);
                        $('#batch_num').html(opt);
                   $('#batch_num').trigger('change');
 						$("#batch_num").val($('#batch_num_val').val());
                    }
                });
           }
                return false;
});								
						</script>


