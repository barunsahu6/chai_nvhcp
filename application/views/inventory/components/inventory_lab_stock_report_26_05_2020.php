<style>
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: middle;
	}
	td{
		text-align: center; 
		vertical-align: middle;
	}
	.item{
		width: 140px !important;
	}
	.batch_num{
		width: 100px !important;
	}
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	select[readonly] {
		background: #eee;
		pointer-events: none;
		touch-action: none;
	}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData');
$filters = $this->session->userdata('filters');
//echo "<pre>"; print_r($filters);

if($loginData->user_type==1) { 
	$select = '';
}else{
	$select = '';	
}if ($loginData->user_type==2 ) {
	$select2 = 'readonly';
}else{
	$select2 = '';
}if ($loginData->user_type==3) {
	$select3 = 'readonly';
}else{
	$select3 = '';
}if ($loginData->user_type==4) {
	$select4 = 'readonly';
}else{
	$select4 = '';	
}
?>
<?php $loginData = $this->session->userdata('loginData');
$filters1=$this->session->userdata('filters1');
if ($loginData->user_type==2 || $filters1['id_mstfacility'] !=''){
	$colspan="14";
	$th="<th>Batch Number</th>";
}
else if (($loginData->user_type==1 || $loginData->user_type==3)&&$filters1['id_mstfacility']==''){
	$colspan="13";
	$th=" ";
}
?>
<div class="row main-div" style="min-height:400px;">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-xl-12">
		<div class="panel panel-default" id="Record_receipt_panel">
			<div class="panel-heading">
				<h4 class="text-center" style="color: white;" id="Record_form_head"><?php if($Repo_flg==1){echo "Drug Stock Report";$option='<option value="drug">All Drugs</option>';}elseif($Repo_flg==2){ echo "Lab Stock Report";$option='<option value="Kit">All Screening Test Kits</option>';} ?></h4>
			</div>
			<div class="row">
				<?php 
				$tr_msg= $this->session->flashdata('tr_msg');
				$er_msg= $this->session->flashdata('er_msg');
				if(!empty($tr_msg)){  ?>


					<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
						<div class="hpanel">
							<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<?php echo $this->session->flashdata('tr_msg');?>. </div>
							</div>
						</div>
					<?php } else if(!empty($er_msg)){ ?>
						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('er_msg');?>. </div>
								</div>
							</div>

						<?php } ?>
					</div>
					<?php
					$attributes = array(
						'id' => 'Receipt_filter_form',
						'name' => 'Receipt_filter_form',
						'autocomplete' => 'false',
					);
//pr($items);
//print_r($receipt_details);
//pr($Closing_stock_Repo);
					echo form_open('', $attributes); ?>

					<div class="row" style="margin-left: 10px;">
						<div class="col-md-2">
							<label for="">State</label>
							<select type="text" name="search_state" id="search_state" class="form-control input_fields" <?php  ?>  <?php echo $select2.$select3.$select4; ?>>
								<option value="">All States</option>
								<?php 
								foreach ($states as $state) {
									?>
									<option value="<?php echo $state->id_mststate; ?>" <?php if($this->input->post('search_state')==$state->id_mststate) { echo 'selected';} ?> <?php if($state->id_mststate == $loginData->State_ID) { echo 'selected';} ?>><?php echo ucwords(strtolower($state->StateName)); ?></option>
									<?php 
								}
								?>
							</select>
						</div>
						<div class="col-md-2">
							<label for="">District</label>
							<select type="text" name="input_district" id="input_district" class="form-control input_fields"  <?php echo $select.$select2.$select4; ?>>
								<option value="">All District</option>
								<?php 
/*foreach ($districts as $district) {
?>
<option value="<?php echo $district->id_mstdistrict; ?>" <?php echo (count($default_districts)==1 && $default_districts[0]->id_mstdistrict == $district->id_mstdistrict)?'selected':''; ?>><?php echo ucwords(strtolower($district->DistrictName)); ?></option>
<?php 
}*/
?>
</select>
</div>

<div class="col-md-2">
	<label for="">Facility</label>
	<select type="text" name="mstfacilitylogin" id="mstfacilitylogin" class="form-control input_fields"  <?php echo $select.$select2; ?>>
		<option value="">All Facility</option>
		<?php 
/*foreach ($facilities as $facility) {
?>
<option value="<?php echo $facility->id_mstfacility; ?>" <?php echo (count($default_facilities)==1 && $default_facilities[0]->id_mstfacility == $facility->id_mstfacility)?'selected':''; ?>><?php echo ucwords(strtolower($facility->facility_short_name)); ?></option>
<?php 
}*/
?>
</select>
</div>
<div class="col-xs-3 col-md-2">
	<label for="item_type">Item Name <span class="text-danger">*</span></label>
	<div class="form-group">
		<select name="item_type" id="item_type" required class="form-control">
			<?php echo $option; ?>
			<?php foreach ($items as $value) { ?>

				<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
					<?php if($value->type==2){echo $value->drug_name; } else{
						echo $value->drug_name." (".$value->strength.")";
					} ?></option>
				<?php } ?>
				<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
			</select>
		</div>
	</div>
	<div class="col-md-2">
		<label for="">Month</label>
		<select name="month" required class="form-control">
			<?php
			echo $curmonth = date("F");
			$flag=0;
			for($i = 1 ; $i <= 12; $i++)
			{
				$allmonth = date("F",mktime(0,0,0,$i,1,date("Y")))
				?>
				<option value="<?php
				echo $i;	
				?>"
				<?php 
				if($this->input->post('month')==$i){ echo 'selected';$flag=1;}
				if($curmonth==$allmonth && $flag==0)
				{
					echo 'selected';
				}
				?>
				>
				<?php
				echo date("F",mktime(0,0,0,$i,1,date("Y")));
//Close tag inside loop
				?>
			</option>
			<?php
		} ?>
	</select>
</div>
<div class="col-md-1" style="padding-left: 0px;">
	<label for="">Year</label>
	<select name="year" required class="form-control">

		<?php 
		$flag=0;
		for($i =2000; $i <= date('Y'); $i++){
			if($this->input->post('year')==$i){
				echo "<option value='$i' selected>$i</option>";
				$flag=1;
			}
			if($i==date('Y') && $flag==0){
				echo "<option value='$i' selected>$i</option>";
			}
			else{
				echo "<option value='$i'>$i</option>";
			}
		}
		?>
	</select>
</div>
<div class="col-md-1" style="padding-left: 0; margin-left: : -5px;width: 7.333333%">
	<label for="">&nbsp;</label>
	<button class="btn btn-block btn-warning" style="line-height: 1.2; font-weight: 600;">SEARCH</button>
</div>

<?php echo form_close(); ?>
</div>

</div>

<table class="table table-striped table-bordered table-hover" id="table_inventory_list">


	<thead>
		<tr>
			<th>Item</th>
			<?php echo $th; ?>
			<th>Opening Stock at the <br> beginning of the month (no. of screening tests)</th>
			<th>Stock received from <br>warehouse (no. of screening tests)</th>
			<th>Stock received  through <br>relocation (no. of screening tests)</th>
			<th>Stock received from<br> unregistered sources (no. of screening tests)</th>
			<th>Total Receipts <br> (no. of screening tests)</th>
			<th>Stock utilized in own <br> facility (no. of screening tests)</th>
			<th>Stock transferred out <br> (no. of screening tests)</th>

			<th>Stock Expired(no. of screening tests)</th>
			<th>Stock rejected on receipt</th>
			<th>Stock Wasted (no. of screening tests)</th>
			<th>Stock Missing(no. of screening tests)</th>
			<th>Closing Stock</th>
		</tr>
	</thead>
	<tbody>
<!-- </tbody>
</table> -->
<?php // echo "<pre>";/*print_r($inventory_details);*/
//$result=array_unique($inventory_details->);
if ($loginData->user_type==2 || $filters1['id_mstfacility'] !=''){
	$item_id=array();
	foreach ($items as $key => $value) {
		foreach ($Inventory_Repo as $value1) {
			if($value->id_mst_drugs==$value1->drug_name){
				$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
				$item_id[$key]['drug_name']= $value->drug_name;
			}
		}

	}
	$filtered_arr=array_unique($item_id,SORT_REGULAR);
//pr($filtered_arr);exit();
//pr($inventory_details);
	?>
<!-- <table class="table table-striped table-bordered table-hover">
	<tbody> -->
		<?php if(!empty($Inventory_Repo)){
			foreach ($filtered_arr as $key=>$value_temp) { ?>
				<tr>
					<th colspan="<?php echo $colspan-1; ?>" class="info"style="border: none;background-color: #088da5;font-size: 16px;color:#fff;text-align: left;"><?php echo $value_temp['drug_name']; ?></th>
					<th class="info" style="border: none;background-color: #088da5;color:#fff;text-align: right;"><i class="fa fa-minus" aria-hidden="true" id="<?php echo 'head'.$value_temp['id_mst_drugs']; ?>"></i></th></tr>
					<?php foreach ($Inventory_Repo as $key => $value) { 
						if($value_temp['id_mst_drugs']==$value->drug_name){
							?>
							<tr class="<?php echo $value_temp['id_mst_drugs']; ?>">
								<td class="item">
									<?php if($value->type==1){echo $value_temp['drug_name']."(".$value->strength.")";} elseif($value->type==2){echo $value_temp['drug_name'];} ?>
								</td>
								<td>
									<?php echo $value->batch_num; ?>
								</td>
								<td>
									<?php if($Closing_stock_Repo!=NULL){

										$close_stock=$Closing_stock_Repo[$key]->Rec-($Closing_stock_Repo[$key]->tr+$Closing_stock_Repo[$key]->Uti+$Closing_stock_Repo[$key]->waste);
										if ($close_stock==NULL) {
											echo '0';
										}
										else{
											echo $close_stock;
										}
									} else{
										echo '0';

									}?>
								</td>
								<td >
									<?php if($value->rec_from_warehouse==NULL){
										echo '-';
									}else{
										echo $value->rec_from_warehouse;
									} ?>
								</td>
								<td>
									<?php if($value->rec_from_facility==NULL){
										echo '-';
									}else{
										echo $value->rec_from_facility; }?>
									</td>
									<td>
										<?php if($value->rec_from_un==NULL){
											echo '-';
										}else{ echo $value->rec_from_un;} ?>
									</td>
									<td>
										<?php if($value->Rec==NULL){
											echo '-';
										}else{ echo $value->Rec; }?>
									</td>
									<td>
										<?php if($value->Uti==NULL){
											echo '-';
										}else{ echo $value->Uti; }?>
									</td>
									<td>
										<?php if($value->tr==NULL){
											echo '-';
										}else{echo $value->tr; }?>
									</td>
									<td>
										<?php if($value->expired==NULL){
											echo '-';
										}else{echo $value->expired; }?>
									</td>
									<td>
										<?php if($value->reject==NULL){
											echo '-';
										}else{echo $value->reject; }?>
									</td>
									<td>
										<?php if($value->waste==NULL){
											echo '-';
										}else{ echo $value->waste; }?>
									</td>
									<td>
										<?php if($value->missing==NULL){
											echo '-';
										}else{echo $value->missing;} ?>
									</td>
									<td>
										<?php $close_stock=$value->Rec-($value->tr+$value->Uti+$value->waste);
										if ($close_stock==NULL) {
											echo '0';
										}
										else{
											echo $close_stock;
										} ?>
									</td>
<!-- <td class="text-center">
<a href="<?php echo site_url()."/inventory/AddReceipt/".$value->receipt_id;?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
<a href="<?php echo site_url()."/inventory/delete_receipt/".$value->receipt_id."/R";?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
</td>

</tr> -->
<?php } }}}else{ ?>
	<tr >
		<td colspan="<?php echo $colspan; ?>" class="text-center">
			NO RECORDS FOUND  BETWEEN SELECTED DATES.
		</td>

	</tr>
<?php } }

else if (($loginData->user_type==1 || $loginData->user_type==3)&&$filters1['id_mstfacility']==''){
	$item_id=array();
	foreach ($items as $key => $value) {
		foreach ($Inventory_Repo as $value1) {
			if($value->id_mst_drugs==$value1->drug_name){
				$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
				$item_id[$key]['drug_name']= $value->drug_name;
			}
		}

	}
	$filtered_arr=array_unique($item_id,SORT_REGULAR);
//pr($filtered_arr);exit();
//pr($inventory_details);
	?>
<!-- <table class="table table-striped table-bordered table-hover">
	<tbody> -->
		<?php if(!empty($Inventory_Repo)){
			foreach ($filtered_arr as $key=>$value_temp) { ?>
				<tr>
					<th colspan="<?php echo $colspan-1; ?>" class="info"style="border: none;background-color: #088da5;font-size: 16px;color:#fff;text-align: left;"><?php echo $value_temp['drug_name']; ?></th>
					<th class="info" style="border: none;background-color: #088da5;color:#fff;text-align: right;"><i class="fa fa-minus" aria-hidden="true" id="<?php echo 'head'.$value_temp['id_mst_drugs']; ?>"></i></th></tr>
					<?php foreach ($Inventory_Repo as $key => $value) { 
						if($value_temp['id_mst_drugs']==$value->drug_name){
							?>
							<tr class="<?php echo $value_temp['id_mst_drugs']; ?>">
								<td class="item">

									<?php if(($loginData->user_type==1) &&($filters1['id_search_state']=='')){
										echo $value->StateName;
									}
									else if($loginData->user_type==3 ||($filters1['id_search_state']!='')){
										echo $value->hospital;
									}

									?>
								</td>
								<td>
									<?php if($Closing_stock_Repo!=NULL){

										$close_stock=$Closing_stock_Repo[$key]->Rec-($Closing_stock_Repo[$key]->tr+$Closing_stock_Repo[$key]->Uti+$Closing_stock_Repo[$key]->waste);
										if ($close_stock==NULL) {
											echo '0';
										}
										else{
											echo $close_stock;
										}
									} else{
										echo '0';

									}?>
								</td>
								<td >
									<?php if($value->rec_from_warehouse==NULL){
										echo '-';
									}else{
										echo $value->rec_from_warehouse;
									} ?>
								</td>
								<td>
									<?php if($value->rec_from_facility==NULL){
										echo '-';
									}else{
										echo $value->rec_from_facility; }?>
									</td>
									<td>
										<?php if($value->rec_from_un==NULL){
											echo '-';
										}else{ echo $value->rec_from_un; }?>
									</td>
									<td>
										<?php if($value->Rec==NULL){
											echo '-';
										}else{ echo $value->Rec; }?>
									</td>
									<td>
										<?php if($value->Uti==NULL || $value->Uti==0){
											echo '-';
										}else{ echo $value->Uti; }?>
									</td>
									<td>
										<?php if($value->tr==NULL){
											echo '-';
										}else{echo $value->tr; }?>
									</td>
									<td>
										<?php if($value->expired==NULL){
											echo '-';
										}else{echo $value->expired; }?>
									</td>
									<td>
										<?php if($value->reject==NULL){
											echo '-';
										}else{echo $value->reject; }?>
									</td>
									<td>
										<?php if($value->waste==NULL || $value->waste==0){
											echo '-';
										}else{ echo $value->waste; }?>
									</td>
									<td>
										<?php if($value->missing==NULL || $value->missing==0){
											echo '-';
										}else{echo $value->missing;} ?>
									</td>
									<td>
										<?php $close_stock=$value->Rec-($value->tr+$value->Uti+$value->waste);
										if ($close_stock==NULL) {
											echo '0';
										}
										else{
											echo $close_stock;
										} ?>
									</td>
<!-- <td class="text-center">
<a href="<?php echo site_url()."/inventory/AddReceipt/".$value->receipt_id;?>" style="padding : 4px;" title="Edit"><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;"></span></a>
<a href="<?php echo site_url()."/inventory/delete_receipt/".$value->receipt_id."/R";?>" style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
</td>

</tr> -->
<?php } }}}else{ ?>
	<tr >
		<td colspan="<?php echo $colspan; ?>" class="text-center">
			NO RECORDS FOUND  BETWEEN SELECTED DATES.
		</td>

	</tr>
<?php } }?>

</tbody>
</table>
</div>
<br>
</div>
<div class="overlay">
	<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
</div>

<!-- Data Table -->
<script type="text/javascript">

	function isNumberKey(evt)
	{
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;


		if(evt.srcElement.id == 'Phone1' && $('#Phone1').val() > 999999999)
		{
			$("#modal_header").html('Contact number must be 10 digits');
			$("#modal_text").html('Please fill in appropriate Contact Number');
			$("#multipurpose_modal").modal("show");
			return false;
		}

		if(evt.srcElement.id == 'PinCode' && $('#PinCode').val() > 99999)
		{
			$("#modal_header").html('PIN cannot be more than 6 digits');
			$("#modal_text").html('Please fill in appropriate PIN');
			$("#multipurpose_modal").modal("show");
			return false;
		}

		return true;
	}

	$('#Acceptance_Status').change(function(){
		var idval = $('#Acceptance_Status').val();
//alert(idval);
if(idval=='yes')
{
	$('#rejection').removeAttr('required');
	$('#rejection').attr('disabled','disabled');
}
else if(idval=='no')
{

	$('#rejection').attr('required','required');
	$('#rejection').removeAttr('disabled','disabled');
//$('#rejection').val('N/A');

}
});

	function checkenddate()
	{
		var startdate = $('#startdate').datepicker('getDate');
		var enddate = $('#enddate').datepicker('getDate');
		if ( startdate > enddate && enddate!=null) { 
			$("#modal_header").text("To date cannot be before From Date");
			$("#modal_text").text("Please check dates");
			$("#enddate" ).val('');
			$("#multipurpose_modal").modal("show");
			return false;
		}
		else{
			return true;
		}

	}
	$('#batch_num').keypress(function (e) {
		var regex = new RegExp("^[a-zA-Z0-9]+$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}

		e.preventDefault();
		return false;
	});
	$(document).ready(function(){
		$(".glyphicon-trash").click(function(e){
			var ans = confirm("Are you sure you want to delete?");
			if(!ans)
			{
				e.preventDefault();
			}
		});
		<?php foreach ($filtered_arr as $key=>$value_temp) {
			$elemnt_id= ".".$value_temp['id_mst_drugs'];?> 
			$("<?php echo $elemnt_id; ?>").show();
			$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").click(function(){
				if($("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").hasClass('fa-minus')){
					$("<?php echo $elemnt_id; ?>").hide();
					$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-minus').addClass("fa-plus");
				}
				else{
					$("<?php echo $elemnt_id; ?>").show();
					$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-plus').addClass("fa-minus");
				}

			});
			$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").css('cursor', 'pointer');
		<?php }?>
	});

	$(document).ready(function(){

		$("#search_state").trigger('change');
//alert('rff');

});
	$('#search_state').change(function(){

		$.ajax({
			url : "<?php echo base_url().'users/getDistricts/'; ?>"+$(this).val(),
			data : {
				<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'
			},
			method : 'POST',
			success : function(data)
			{
//alert(data);
$('#input_district').html(data);
$("#input_district").trigger('change');
//$("#mstfacilitylogin").trigger('change');

},
error : function(error)
{
//alert('Error Fetching Districts');
}
})
	});

	$('#input_district').change(function(){

		$.ajax({
			url : "<?php echo base_url().'users/getFacilities/'; ?>"+$(this).val(),

			method : 'POST',
			success : function(data)
			{
//alert(data);
if(data!="<option value=''>Select Facilities</option>"){
	$('#mstfacilitylogin').html(data);

}
},
error : function(error)
{
//alert('Error Fetching Blocks');
}
})
	});


</script>


