<style>
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: top;
	}
	td{
		text-align: center; 
		vertical-align: middle;
		
	}
	.table>thead>tr>th{
		vertical-align: top;
	}
	.pending_table>thead>tr>th{
		vertical-align: top;
		word-break: break-all;
	}
	.pending_table>thead>tr>td{
		vertical-align: middle;
		text-align: center;
		word-break: break-all;
	}
	.btn-width{
	width: 12%;
}
	
	.batch_num{
		width: 100px !important;
	}
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	input,nav,label,span{
		font-family: 'Source Sans Pro';

	}
	label{
		font-family: 'Source Sans Pro';
		font-size: 14px;
	}
	input,select,.form-control{
		height: 30px;
	}
	.table-bordered>tbody>tr>td{
		height: 45;
	}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData'); ?>
<!-- add/edit facility contact modal starts-->


<!-- add/edit facility contact modal ends-->
<!-- <div class="row" style="padding-top: 20px;padding-bottom: 20px;">
	<div class="col-lg-10 text-center">
			<div class="col-md-4 col-md-offset-6" style="padding-right: 15px;">
			<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 15px;padding-bottom: 15px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i> Raise Indent Request
		</a>
		</div>
	</div>
</div> -->

<div class="row main-div" style="margin-top: 1em;margin-bottom: 1em">
	<!-- <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12" style="padding: 0;">
			<?php //include("left_nav_bar.php");
			//$this->load->view("inventory/components/left_nav_bar"); ?>
		</div> -->
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-default" id="Record_receipt_panel">
				<div class="panel-heading" style=";background: #333;">
					<h4 class="text-center" style="color: white;background: #333" id="Record_form_head">Stock Indent Tracker</h4>
				</div>
				<div class="row">
					<?php 
					$tr_msg= $this->session->flashdata('tr_msg');
					$er_msg= $this->session->flashdata('er_msg');
					if(!empty($tr_msg)){  ?>
						

						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						<?php } else if(!empty($er_msg)){ ?>
							<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
								
							<?php } ?>
		<!-- <div class="col-md-3 pull-right" style="padding-right: 15px;">
			<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 10px;padding-bottom: 10px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i> Raise Indent Request
		</a>
		</div> -->
						</div>
						
						<?php
						$attributes = array(
							'id' => 'Receipt_filter_form',
							'name' => 'Receipt_filter_form',
							'autocomplete' => 'off',
						);
  //pr($get_facilities);
  //print_r($inventory_details);

						echo form_open('', $attributes); ?>
						<div class="row" style="padding-left: 10px;">
							<div class="col-xs-3 col-md-2">
								<label for="item_type">Item Name <span class="text-danger">*</span></label>
								<div class="form-group">
									<select name="item_type" id="item_type" required class="form-control">
										<?php inventory_options($this->input->post('item_type')); ?>
										
										<?php foreach ($items as $value) { ?>
											<?php if($value->type!=3) {?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php
												echo ($value->type==2) ? $value->drug_name : $value->drug_name.' '.$value->strength.' '.$value->unit ; ?></option>
											<?php } }?>
											<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
										</select>
									</div>
								</div>
								
								
						
								<div class="col-md-2 col-sm-12 form-group">
									<label for="year">Financial Year <span class="text-danger">*</span></label>
									<select name="year" id="year" required class="form-control" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2018', date('Y'));
										$flag=0;
										foreach($dates as $date){

									if (date('m', strtotime($date)) < 4) {//Upto April
										$year = ($date-1) . '-' . $date;
									} else {//After April
										$year = $date . '-' . ($date + 1);
									}
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									else if($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
									
								}
								?>
							</select>
						</div>
								<div class="col-md-2 col-sm-12">
									<label for="">From Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')){ echo $this->input->post('startdate');} else{ echo date('01-04-Y');}  ?>" required style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 col-sm-12">
									<label for="">To Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')){echo $this->input->post('enddate');}else{echo timeStampShow(date('Y-m-d'));} ?>" required  style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 btn-width pull-right" style="padding-right: 30px;">
									<label for="">&nbsp;</label>
									<button type="submit" id="search_btn" name="search" value="search" class="btn btn-block" style="line-height: 1.2; font-weight: 600;background-color: #f4860c;border-color: #f4860c;color: white;">SEARCH</button>
								</div>

								<?php echo form_close(); ?>
							</div>
							
						</div>
					
						</div>
							<br>
						</div>
		<div class="row" style="padding-right: 08%;">
				<span class="pull-right">
				<dl>
				    <dt class="yellow"></dt>
				    <dd>Order dispatched to facility</dd>

				    <dt class="orange"></dt>
				    <dd>Partial order dispatched to facility</dd>

				     <dt class="green"></dt>
				    <dd>Order received by facility</dd>

				    <dt class="red"></dt>
				    <dd>Order rejected on receipt by facility</dd>
				    <dt class="darkYellow_status"></dt>
				    <dd>Order partially rejected on receipt by facility</dd>
				</dl>
					</span>						
			</div>
			<div class="row" style="padding-right: 10px;">
				<div class="col-lg-12 table-responsive">

					<table class="table table-striped table-bordered table-hover" id="indent_status">
						<thead><tr>
									<th colspan="27" class="info main_header"style="border: none;background-color: #545454;font-size: 16px;color:#fff;text-align: left;">Indents dispatch status<span style="margin-left: 20px;"><input style="margin: 0px;height: 13px;" type="checkbox" class="form-group" id="warehouse" name="warehouse" value="warehouse">
<label for="warehouse">Show warehouse dispatch status</label></span><span style="margin-left: 20px;"><input style="margin: 0px;height: 13px;" type="checkbox" id="facility" name="facility" value="facility">
<label for="facility">Show facility dispatch status</label></span></th>
</tr>

								</thead>
								<thead>
									<tr>
									<th colspan="8" class="info"style="border: none;background-color: #4A708B;font-size: 16px;color:#fff;text-align: left;">Indents Details</th>
									<th colspan="9" class="info warehouse"style="border: none;background-color: #085786;font-size: 16px;color:#fff;text-align: left;">Warehouse dispatch status (kits/bottle)</th>
									<th colspan="10" class="info facility"style="border: none;background-color: #4A708B;font-size: 16px;color:#fff;text-align: left;">Facility dispatch status (kits/bottle)</th>
								</tr>
								</thead>
							
							<thead style="background-color: #085786; font-family: 'Source Sans Pro';color: white;font-size: 13px; text-align: center;">
									<tr>
									<th class="text-center">Item</th>
									<th class="text-center">Indent Number</th>
									<th class="text-center">Date of placing indent</th>
									<th class="text-center">Facility</th>
									<th class="text-center">Approved | Quantity Indented (screening tests/bottle)</th>
									<th class="text-center">Date of approval</th>
									<th class="text-center">Indent fulfillment request sent to</th>
									<th class="text-center">S.No. (indent fulfilled)</th>
									<th class="text-center warehouse" style="background: #01446b;">Warehouse quantity fulfilled (kits/bottle/vials)</th>
									<th class="text-center warehouse" style="background: #01446b;">Warehouse total quantity requested (kits/bottle/vials)</th>
									<th class="text-center warehouse" style="background: #01446b;">Warehouse indent fulfillment quantity pending (kits/bottle/vials)</th>
									<th class="text-center warehouse" style="background: #01446b;">Issue no</th>
									<th class="text-center warehouse" style="background: #01446b;">Batch No</th>
									<th class="text-center warehouse" style="background: #01446b;">Warehouse dispatch status update</th>
									<th class="text-center warehouse" style="background: #01446b;">Warehouse dispatch status update date</th>
									<th class="text-center warehouse" style="background: #01446b;">Quantity of Stock Rejected by receiving Facility</th>
									<th class="text-center warehouse" style="background: #01446b;">Reason for rejection</th>

									<th class="text-center facility">Facility quantity fulfilled (kits/bottle/vials)</th>
									<th class="text-center facility">Facility total quantity requested (kits/bottle/vials)</th>
									<th class="text-center facility">Facility indent fulfillment quantity pending (kits/bottle/vials)</th>
									<th class="text-center facility">Issue no</th>
									<th class="text-center facility">Batch No</th>
									<th class="text-center facility">Facility dispatch status update</th>
									<th class="text-center facility">Facility dispatch status update date</th>
									<th class="text-center facility">Quantity of stock received by facility</th>
									<th class="text-center facility">Quantity of Stock Rejected by receiving Facility</th>
									<th class="text-center facility">Reason for rejection</th>
									</tr>
							</thead>
							<tbody>
			<!-- </tbody>
			</table> -->
				<?php // echo "<pre>";/*print_r($inventory_details);*/
				//$result=array_unique($inventory_details->);
				$item_id=array();
				foreach ($items as $key => $value) {
					foreach ($indent_details as $value1) {
						if($value->id_mst_drugs==$value1->drug_name){
							$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
							$item_id[$key]['drug_name']= $value->drug_name;
						}
					}
					
				}
				$filtered_arr=array_unique($item_id,SORT_REGULAR);
				//pr($filtered_arr);exit();
				//pr($inventory_detail_all);
				?>
				<!-- <table class="table table-striped table-bordered table-hover">
					<tbody> -->
						<?php if(!empty($indent_details)){
							foreach ($filtered_arr as $key=>$value_temp) { ?>

							
								<tr>
									<td colspan="26" class="info itemname"style="border: none;background-color: #333;font-size: 16px;color:#fff;text-align: left;"><?php echo $value_temp['drug_name']; ?></td>
									<td class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus" aria-hidden="true" id="<?php echo 'head'.$value_temp['id_mst_drugs']; ?>"></i></td></tr>
									<?php foreach ($indent_details as  $value) { 
										$count=1;
										foreach ($indent_all_details as $value_all) {
										
								if ($value->inventory_id==$value_all->refrence_id) {
					
									
										if($value_temp['id_mst_drugs']==$value->drug_name){
												if ($count==1) {
											?>
											<tr class="<?php echo $value_temp['id_mst_drugs']; ?>">
												<td class="item">
													<?php if($value->type==1){echo $value_temp['drug_name']." ".$value->strength." mg";} elseif($value->type==2){echo $value_temp['drug_name'];} ?>
												</td>
												<td>
													<?php echo $value->indent_num; ?>
												</td>
					
												<td nowrap>
													<?php echo timeStampShow($value->indent_date); ?>
												</td>
												<td>
													
													<?php echo $value->facility_short_name; ?>
												</td>
												<td>
													<?php echo $value->approved_quantity.'|'.$value->quantity; ?>
												</td>
												<td nowrap="nowrap">
													<?php echo timeStampShow($value->indent_accept_date); ?>
												</td>
												<td>
													<?php 
													$source_name='';
													$req_places='';
													if ($value->source_name==996) {
															$source_name= " abc";
														}
														elseif ($value->source_name==997) {
															$source_name =" abcd";
														}
														elseif ($value->source_name==998) {
															$source_name= " abcde";
														}
														if (($value->req_facility!='' || $value->req_facility!=NULL) && $source_name!='') {
															$req_places=$value->req_facility." & ".$source_name;
														}
														elseif (($value->req_facility=='' || $value->req_facility==NULL) && $source_name!='') {
															$req_places=$source_name;
														}
														elseif (($value->req_facility!='' || $value->req_facility!=NULL) && $source_name=='') {
															$req_places=$value->req_facility;
														}
														echo $req_places;
														 ?>
												</td>
												<?php }else{?>
													<td colspan="7">
													
												</td>
												<?php }	
												 ?>
													<td nowrap>
													<?php echo $count; ?>
												</td>
					<?php if ($value_all->warehouse_request_quantity==0 || $value_all->warehouse_request_quantity==NULL || $value_all->warehouse_request_quantity=='') {
						echo '<td class="warehouse" colspan="9" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">&nbsp;</td>';
					}else{?>
												<td class="warehouse" nowrap style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													<?php echo ""; ?>
												</td>
												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													<?php echo  !empty($value_all->warehouse_request_quantity)>0 ? $value_all->warehouse_request_quantity : ''; ?>
												</td>
												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													<?php echo  !empty($value_all->warehouse_request_quantity)>0 ? $value_all->warehouse_request_quantity : ''; ?>
												</td>
												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													
												</td>
												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													
												</td>
												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													
												</td>
												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													
												</td>

												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													<?php  $value_all->relocated_quantity; ?>
												</td>
												<td class="warehouse" style="background-color: #e4e4e4;border-width: 1px;border-color: white;">
													<?php  $value_all->requested_quantity; ?>
												</td>
											<?php }?>
												<td class="facility">
													<?php echo ($value_all->relocated_quantity); ?>
												</td>
												<td class="facility">
													<?php if ($value_all->pending_quantity==0) {
														echo $value_all->requested_quantity;
													}
													else{
														echo $value_all->pending_quantity;
													} ?>
												</td>
												<td class="facility">
													<?php if ($value_all->pending_quantity==0) {
														echo ($value_all->requested_quantity-$value_all->relocated_quantity);
													}
													else{
														echo ($value_all->pending_quantity-$value_all->relocated_quantity);
													} ?>
												</td>
												<td class="facility">
													<?php echo $value_all->issue_num; ?>
												</td>
												<td class="facility">
													<?php echo $value_all->batch_num; ?>
												</td>
												<td class="<?php if ($value->relocation_status==1){
														echo 'yellow_status facility';
												}
												elseif ($value->relocation_status==2){
														echo 'orange_status facility';
												}
												elseif ($value->relocation_status==3){
														echo 'green_status facility';
												}
												elseif ($value->relocation_status==4){
														echo 'red_status facility';
												}
												elseif ($value->relocation_status==5){
														echo 'darkYellow_status facility';
												}
													
												?>">

													<?php foreach ($relocation_status as $relocation) {
														# code...
													 if($value->relocation_status==$relocation->LookupCode) { echo $relocation->LookupValue;}} ?>
												</td>
												<td class="facility" nowrap="nowrap"> 	<?php echo timeStampShow($value_all->dispatch_date); ?>
													
												</td>
												
												<td class="facility">
													<?php echo !empty($value_all->quantity_received)>0 ? $value_all->quantity_received :"N/A" ; ?>
												</td>
												<td class="facility">
													<?php echo !empty($value_all->quantity_rejected)>0 ? $value_all->quantity_rejected :"N/A" ; ?>
												</td>
												<td class="facility">
													 <?php if($value_all->rejection_reason==0 || $value_all->rejection_reason=="" || $value_all->rejection_reason==NULL){
													 echo "N/A";}else{
													 	foreach ($reason_for_rejection as $rejection) {
														# code...
													 if($value_all->rejection_reason==$rejection->LookupCode) { echo $rejection->LookupValue;}}
													 } ?>
												</td>
											</tr>
										<?php }$count=$count+1;}}}}}else{ ?>
											<tr >
												<td colspan="27" class="text-center">
													NO RECORDS FOUND  BETWEEN SELECTED DATES.
												</td>

											</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
							<br>
						</div>
	</div>

</div>

						<div class="overlay">
							<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
						</div>
				<!-- Data Table -->
						<script type="text/javascript">

							
 $("#facility,#warehouse").click(function() {
 	var classname=$(this).val();
 	
           if ( $(this).is(':checked') === true ) {
           $('.'+classname).show();
           }
       
           else{
           	$('.'+classname).hide();
           }
           if ( ($('#facility').is(':checked') === false ) &&($('#warehouse').is(':checked') === false )) {
            	$('.main_header').attr('colspan',8);
           		$('.itemname').attr('colspan',7);
            }
            else if ( ($('#facility').is(':checked') === true ) &&($('#warehouse').is(':checked') === false )) {
            	$('.main_header').attr('colspan',19);
           		$('.itemname').attr('colspan',17);
            } 
            else if ( ($('#facility').is(':checked') === false ) &&($('#warehouse').is(':checked') === true )) {
            	$('.main_header').attr('colspan',17);
           		$('.itemname').attr('colspan',16);
            }
            else if ( ($('#facility').is(':checked') === true ) &&($('#warehouse').is(':checked') === true )) {
            	$('.main_header').attr('colspan',27);
           		$('.itemname').attr('colspan',26);
            }
        });

							$(document).ready(function(){
								$('#facility').attr('checked','checked');
								$('#warehouse').attr('checked','checked');
								/*$('#pending_indent').DataTable({"pagingType": "simple_numbers",
						"searching": false,

						"lengthChange": false,
						"orderable": false,
						"pageLength": 5}
						
					);*/	/*$('#indent_status').DataTable({"pagingType": "simple_numbers",
						"searching": false,

						"lengthChange": false,
						"orderable": false,
						"pageLength": 5}
						
					);*/
								<?php foreach ($filtered_arr as $key=>$value_temp) {
									$elemnt_id= ".".$value_temp['id_mst_drugs'];?> 
									$("<?php echo $elemnt_id; ?>").show();
									$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").click(function(){
										if($("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").hasClass('fa-minus')){
											$("<?php echo $elemnt_id; ?>").hide();
											$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-minus').addClass("fa-plus");
										}
										else{
											$("<?php echo $elemnt_id; ?>").show();
											$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-plus').addClass("fa-minus");
										}
										
									});
									$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").css('cursor', 'pointer');
								<?php }?>
							});

							
			
						</script>


