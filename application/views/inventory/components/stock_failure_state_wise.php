<style>
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: top;
	}
	td{
		text-align: center; 
		vertical-align: middle;
	}
	.btn-width{
	width: 12%;
}
	.item{
		width: 140px !important;
	}
	.batch_num{
		width: 100px !important;
	}
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	input,nav,label,span{
		font-family: 'Source Sans Pro';

	}
	label{
		font-family: 'Source Sans Pro';
		font-size: 14px;
	}
	input,select,.form-control{
		height: 30px;
	}
	.modal-dialog{
	width: 700;
	}
	.pd-left-0{
		padding-left: 0px;
	}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData'); ?>
<!-- add/edit facility contact modal starts-->


<!-- add/edit facility contact modal ends-->
<!-- <div class="row" style="padding-top: 20px;padding-bottom: 20px;">
	<div class="col-lg-9 text-center">
			<div class="col-md-4 col-md-offset-6" style="padding-right: 15px;">
			<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 15px;padding-bottom: 15px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i> Add Stock Failure
							</a>
		</div>
	</div>
</div> -->

<div class="row main-div" style="min-height:400px;margin-top: 1em;">
	<!-- <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12" style="padding: 0;">
			<?php //include("left_nav_bar.php");
			//$this->load->view("inventory/components/left_nav_bar"); ?>
		</div>
	 -->
		<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
			<div class="panel panel-default" id="Record_receipt_panel">
				<div class="panel-heading" style=";background: #333;">
					<h4 class="text-center" style="color: white;background: #333" id="Record_form_head">Stock Failure</h4>
				</div>
				<div class="row">
					<?php 
					$tr_msg= $this->session->flashdata('tr_msg');
					$er_msg= $this->session->flashdata('er_msg');
					if(!empty($tr_msg)){  ?>
						

						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						<?php } else if(!empty($er_msg)){ ?>
							<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
								
							<?php } ?>
							<!-- <div class="col-md-3 pull-right">
										<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 10px;padding-bottom: 10px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i> Add Stock Failure
							</a>
									</div> -->
						</div>
						<?php
						$attributes = array(
							'id' => 'Receipt_filter_form',
							'name' => 'Receipt_filter_form',
							'autocomplete' => 'off',
						);
  //pr($items);
  //print_r($inventory_details);

						echo form_open('', $attributes); ?>
							<div class="row" style="padding-left: 10px;">
							<div class="col-xs-3 col-md-2">
								<label for="item_type">Item Name <span class="text-danger">*</span></label>
								<div class="form-group">
									<select name="item_type" id="item_type" required class="form-control">
										<?php inventory_options($this->input->post('item_type')); ?>
										
										<?php foreach ($items as $value) { ?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php if($value->type==2){echo $value->drug_name; } else{
													echo $value->drug_name." ".$value->strength." mg";
												} ?></option>
											<?php } ?>
											<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
										</select>
									</div>
								</div>
								
								
						
								<div class="col-md-2 col-sm-12 form-group">
									<label for="year">Financial Year <span class="text-danger">*</span></label>
									<select name="year" id="year" required class="form-control" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2018', date('Y'));
										$flag=0;
										foreach($dates as $date){

									if (date('m', strtotime($date)) < 4) {//Upto April
										$year = ($date-1) . '-' . $date;
									} else {//After April
										$year = $date . '-' . ($date + 1);
									}
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									else if($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
									
								}
								?>
							</select>
						</div>
								<div class="col-md-2 col-sm-12">
									<label for="">From Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')){ echo $this->input->post('startdate');} else{ echo date('01-04-Y');}  ?>" required style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 col-sm-12">
									<label for="">To Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')){echo $this->input->post('enddate');}else{echo timeStampShow(date('Y-m-d'));} ?>" required  style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 btn-width pull-right" style="padding-right: 30px;">
									<label for="">&nbsp;</label>
									<button class="btn btn-block" style="line-height: 1.2; font-weight: 600;background-color: #f4860c;border-color: #f4860c;color: white;">SEARCH</button>
								</div>

								<?php echo form_close(); ?>
							</div>
							
						</div>

						<table class="table table-striped table-bordered table-hover" id="table_inventory_list">

							
							<thead>
								<tr style="background-color: #085786; font-family: 'Source Sans Pro';color: white;font-size: 13px;">
									<th>Item</th>
									<th>Batch no.</th>
									<th>Quantity of stock returned<br> due to stock failure <br>(screening tests/bottle)</th>

									<th>Describe issue faced</th>
									<th>Date stock return initiated</th>
									<th>Stock return status</th>
									<th>Remark by Warehouse</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
			<!-- </tbody>
			</table> -->
						<?php if(!empty($inventory_detail_all)){
							foreach ($filtered_arr as $key=>$value_temp) { ?>
								<tr>
									<th colspan="7" class="info"style="border: none;background-color: #333;font-size: 16px;color:#fff;text-align: left;"><?php echo ($value_temp->drug_abb!='SOF400VEL') ? $value_temp->drug_name : $value_temp->drug_name.' '.$value_temp->strength.' '.$value_temp->unit ; ?></th>
									<th class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus" aria-hidden="true" id="<?php echo 'head'.$value_temp->id_mst_drugs; ?>"></i></th></tr>
									<?php foreach ($inventory_detail_all as  $value) { 
										if($value_temp->id_mst_drugs==$value->id_mst_drugs){
											?>
											<tr class="<?php echo $value_temp->id_mst_drugs; ?>">
												<td class="item">
													<?php echo ($value->type!=2) ? $value->drug_name.' '.$value->strength.' '.$value->unit : $value->drug_name; ?>
												</td>
												<td>
													<?php echo $value->batch_num; ?>
												</td>
												<td>
													<?php /*if($value->type==1){echo $value->returned_quantity;} elseif($value->type==2){echo $value->quantity_screening_tests;}*/
													echo $value->returned_quantity; ?>
												</td>
												<td>
													<?php echo $value->failure_issue; ?>
												</td>
												<td nowrap>
													<?php echo timeStampShow($value->failure_date); ?>
												</td>
												
											<td nowrap>
													<?php echo ""; ?>
												</td>
												<td nowrap>
													<?php echo ""; ?>
												</td>
												
												 <td class="text-center">
													<?php if($value->indent_status==0){?>
													<a href="" style="padding : 4px;" title="Edit" data-toggle="modal" data-target="#myModal1" onclick='update_fail_status(<?php echo $value->inventory_id ?>,1);'><span class="glyphicon glyphicon-ok" style="font-size : 15px; margin-top: 8px;" ></span></a>
													<a href="#" onclick='update_fail_status(<?php echo $value->inventory_id ?>,2);'style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-remove" style="font-size : 15px; margin-top: 8px;"></span></a>
												<?php }?>
												</td> 

											</tr>
										<?php } }}}else{ ?>
											<tr >
												<td colspan="10" class="text-center">
													NO RECORDS FOUND  BETWEEN SELECTED DATES.
												</td>

											</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
							<br>
						</div>
						<div class="overlay">
							<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
						</div>
				<!-- Data Table -->
						<script type="text/javascript">

		$( document ).ready(function() {
   		get_rem_item_name();
			});	
							
							$(document).ready(function(){
								
								<?php foreach ($filtered_arr as $key=>$value_temp) {
									$elemnt_id= ".".$value_temp->id_mst_drugs;?> 
									$("<?php echo $elemnt_id; ?>").show();
									$("<?php echo '#head'.$value_temp->id_mst_drugs ?>").click(function(){
										if($("<?php echo '#head'.$value_temp->id_mst_drugs ?>").hasClass('fa-minus')){
											$("<?php echo $elemnt_id; ?>").hide();
											$("<?php echo '#head'.$value_temp->id_mst_drugs ?>").removeClass('fa-minus').addClass("fa-plus");
										}
										else{
											$("<?php echo $elemnt_id; ?>").show();
											$("<?php echo '#head'.$value_temp->id_mst_drugs; ?>").removeClass('fa-plus').addClass("fa-minus");
										}
										
									});
									$("<?php echo '#head'.$value_temp->id_mst_drugs; ?>").css('cursor', 'pointer');
								<?php }?>
							});

			function update_fail_status(inventory_id,status){
				var main_title="";
						if(status==2){
							$('#sub_head_confirm').text('Failure request will be cancelled. Are you sure?');
							main_title="Delete failure request";
							var button_options={
									        "Cancel request": function() {
									          window.location.replace('<?php echo base_url(); ?>Inventory/update_fail_status/'+inventory_id+'/'+status);

									        },
									        "Don't Cancel request": function() {
									          $( this ).dialog( "close" );
									          e.preventDefault();
									        }
									      };			
						}
						else{
							$('#sub_head_confirm').text('Failure request will be Accepted. Are you sure?');
							main_title="Accept failure request";
							var button_options={
									        "Accept request": function() {
									          window.location.replace('<?php echo base_url(); ?>Inventory/update_fail_status/'+inventory_id+'/'+status);

									        },
									        "Don't Accept request": function() {
									          $( this ).dialog( "close" );
									          e.preventDefault();
									        }
									      };	
						}			   		 $( "#dialog-confirm" ).dialog({
									   		open : function() {
										    $("#dialog-confirm").closest("div[role='dialog']").css({top:100,left:'40%'});
											}, 	
									      resizable: false,
									      height: "auto",
									      title:main_title,
									      width: 400,
									      modal: true,
									      buttons: button_options
									 			 });
								};				
						</script>


