<style>
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: middle;
	}
	td{
		text-align: center; 
		vertical-align: middle;
	}
	.btn-width{
	width: 12%;
}
	.item{ 
		width: 140px !important;
	}
	.batch_num{
		width: 100px !important;
	}
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	input,nav,label,span{
		font-family: 'Source Sans Pro';

	}
	label{
		font-family: 'Source Sans Pro';
		font-size: 14px;
	}
	input,select,.form-control{
		height: 30px;
	}
	.modal-lg{
		width: 1100px;
	}
	.pd-right-0{padding-right: 0px;}
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData'); ?>
<!-- add/edit facility contact modal starts-->
<div class="modal fade" tabindex="-1" role="dialog" id="myModal1">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #333;color: white;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_icon" style="color: white;"><span aria-hidden="true" style="color: white;">&times;</span></button>
        <h4 class="modal-title">Add Utilization/Dispensation</h4>
      </div>
      <div class="modal-body">
      	<?php  $attributes = array(
              'id' => 'stock_utilization_form',
              'name' => 'stock_utilization_form',
               'autocomplete' => 'off',
            );
          
           echo form_open('Inventory/stock_utilization/', $attributes); ?>
           <div class="row">
           	<div class="col-md-7 col-sm-12">
           		<div class="row">
           			<div class="col-xs-12 col-md-5 text-left" >
           				<label for="contact_name">Item Name<span class="text-danger">*</span></label>
           			</div>
           			<div class="col-xs-12 col-lg-7 col-md-6 form-group">
           				<select name="item_name" id="item_name" required class="form-control">
           					<option value="">-----Select-----</option>
           					
           					</select>
           				</div>
        	
           		</div>
           		<div class="row">
           			
           			<div class="col-xs-12 col-md-5 text-left" >
           				<label for="batch_num">Batch Num<span class="text-danger">*</span></label>
           			</div>
           			<div class="form-group col-xs-12 col-md-7">
           				<select name="batch_num" id="batch_num" required class="form-control">
           					<option value=''>----Select----</option>
           				</select>
           			</div>
           		</div>
           		<div class="row">
           			<div class="col-xs-12 col-md-5 col-sm-12 text-left">
           				<label for="approved_quantity" id="approved_quantity_label">Quantity of stock currently available (screening tests/bottle)<span class="text-danger">*</span></label>
           			</div>
           			<div class="form-group col-md-7 col-sm-12" id="item_div">
           				<input type="text" class="form-control" name="available_quantity" id="available_quantity" readonly>
           			</div>
           		</div>
           		<div class="row">
           			<div class="col-xs-12 col-md-5 text-left" >
           				<label for="date">From Date<span class="text-danger">*</span></label>
           			</div>
           			<div class="form-group col-xs-12 col-md-7">
           				<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="From Date" name="from_Date" id="from_Date" onchange="checkenddate();" required onkeydown="return false" onkeyup="return false" >
           			</div>
           		</div>
           		<div class="row">
           			<div class="col-xs-12 col-md-5 text-left" >
           				<label for="Expiry_Date">To Date <span class="text-danger">*</span></label>
           			</div>
           			<div class="form-group col-xs-12 col-md-7">
           				<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="To Date" name="to_Date" id="to_Date" onchange="checkenddate();" required onkeydown="return false" onkeyup="return false">
           			</div>
           		</div>
           	</div>
           	<div class="col-md-5 col-sm-12">
           		<div class="row">
           			<div class="col-xs-12 col-md-7 col-sm-12 text-left" id="labelfor">
           				<label for="dispensed_quantity" id="dispensed_quantity_label">Number of drug bottles dispensed/ screening tests dispensed (including repeat tests)<span class="text-danger">*</span></label>
           			</div>
           			<div class="form-group col-md-5 col-sm-12">
           				<input type="text" class="form-control" name="dispensed_quantity" id="dispensed_quantity" required >
           			</div>
           		</div>
           		<div class="row">
           			<div class="col-xs-12 col-md-7 col-sm-12 text-left" id="labelfor">
           				<label for="approved_quantity">Number of screening tests used for repeat tests<span class="text-danger type_label">*</span></label>
           			</div>
           			<div class="form-group col-md-5 col-sm-12">
           				<input type="text" class="form-control" name="repeat_quantity" id="repeat_quantity" required >
           			</div>
           		</div>
           		<div class="row">
           			<div class="col-xs-12 col-md-7 col-sm-12 text-left">
           				<label for="control_used">Number of controls used (screening tests)<span class="text-danger type_label">*</span></label>
           			</div>
           			<div class="form-group col-md-5 col-sm-12">
           				<input type="text" class="form-control" name="control_used" id="control_used" required >
           			</div>
           		</div>
           		<div class="row">
           			<div class="col-xs-12 col-md-7 col-sm-12 text-left">
           				<label for="approved_quantity">Purpose of utilization<span class="text-danger">*</span></label>
           			</div>
           			<div class="form-group col-md-5 col-sm-12" id="item_div">
           				<select name="utilization_purpose" id="utilization_purpose" required class="form-control">
           					<option value="">----Select----</option>
           					<?php foreach ($utilization_purpose as $value) { ?>

           						<option value="<?php echo $value->LookupCode;?>">
           							<?php echo $value->LookupValue; ?></option>
           						<?php } ?>
           					</select>
           				</div>
           			</div>
           		</div>

           	</div>
           	<div class="row">
				<h5 style="color: #a94442;text-align: center;font: bold">If you cannot find the item in the dropdown or the batch number, make sure you have recorded receipt of this item or raised an indent.</h5>
			</div>	
        		 <input type="hidden" id="inventory_id" name="inventory_id" value="">
        		 <input type="hidden" id="last_to_date" name="last_to_date" value="">
        		  <input type="hidden" id="last_to_date_flags" name="last_to_date_flags" value="">
        		  <input type="hidden" id="rem_values" name="rem_values" value="">
        		  <input type="hidden" id="type_val" name="type_val" value="">
        		  <input type="hidden" id="batch_num_val" name="batch_num_val" value="">
        		    <input type="hidden" id="item_name_val" name="item_name_val" value="">
        		     <input type="hidden" id="repeat_quantity_val" name="repeat_quantity_val" value="">
        		    <input type="hidden" id="control_used_val" name="control_used_val" value="">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="modal_close">Close</button>
        <button type="submit" class="btn btn-primary" id="submit_btn" name="save" value="save">Save</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>

<!-- add/edit facility contact modal ends-->
<div class="row" style="padding-top: 20px;padding-bottom: 20px;">
	<div class="col-lg-10 text-center">
			<div class="col-md-4 col-md-offset-6" style="padding-right: 15px;">
			<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 15px;padding-bottom: 15px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i> Add Utilization/Dispensation
							</a>
		</div>
	</div>
</div>

<div class="row main-div" style="min-height:400px;margin-top: 1em;">
	<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12" style="padding: 0;">
			<?php //include("left_nav_bar.php");
			$this->load->view("inventory/components/left_nav_bar"); ?>
		</div>

		<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
			<div class="panel panel-default" id="Record_receipt_panel">
				<div class="panel-heading" style=";background: #333;">
					<h4 class="text-center" style="color: white;background: #333" id="Record_form_head"> Utilization/Dispensation</h4>
				</div>
				<div class="row">
					<?php 
					$tr_msg= $this->session->flashdata('tr_msg');
					$er_msg= $this->session->flashdata('er_msg');
					if(!empty($tr_msg)){  ?>
						

						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						<?php } else if(!empty($er_msg)){ ?>
							<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
								
							<?php } ?>
							<!-- <div class="col-md-3 pull-right">
										<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 10px;padding-bottom: 10px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i> Add Utilization/Dispensation
							</a>
									</div> -->
						</div>
						
						<?php
						$attributes = array(
							'id' => 'Receipt_filter_form',
							'name' => 'Receipt_filter_form',
							'autocomplete' => 'off',
						);
  //pr($items);
 // print_r($inventory_detail_all);
						//echo ($value->from_Date);

						echo form_open('', $attributes); ?>
					<div class="row" style="padding-left: 10px;">
							<div class="col-xs-3 col-md-2">
								<label for="item_type">Item Name <span class="text-danger">*</span></label>
								<div class="form-group">
									<select name="item_type" id="item_type" required class="form-control">
										<option value="drugkit" <?php if($this->input->post('item_type')=='drugkit') { echo 'selected';}?>>All Kits and Drugs</option>
										<option value="Kit"<?php if($this->input->post('item_type')=='Kit') { echo 'selected';}?>>All Screening Test Kits</option>
										<option value="drug"<?php if($this->input->post('item_type')=='drug') { echo 'selected';}?>>All Drugs</option>
										
										<?php foreach ($items as $value) { ?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php if($value->type==2){echo $value->drug_name; } else{
													echo $value->drug_name." ".$value->strength." mg";
												} ?></option>
											<?php } ?>
											<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
										</select>
									</div>
								</div>
								
								
						
								<div class="col-md-2 col-sm-12 form-group">
									<label for="year">Financial Year <span class="text-danger">*</span></label>
									<select name="year" id="year" required class="form-control" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2018', date('Y'));
										$flag=0;
										foreach($dates as $date){

									if (date('m', strtotime($date)) < 4) {//Upto April
										$year = ($date-1) . '-' . $date;
									} else {//After April
										$year = $date . '-' . ($date + 1);
									}
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									else if($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
									
								}
								?>
							</select>
						</div>
								<div class="col-md-2 col-sm-12">
									<label for="">From Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')){ echo $this->input->post('startdate');} else{ echo date('01-04-Y');}  ?>" required style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 col-sm-12">
									<label for="">To Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')){echo $this->input->post('enddate');}else{echo timeStampShow(date('Y-m-d'));} ?>" required  style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 col-sm-12">
										<label for="">Utilization Recorded</label>
           				<select name="utilization_filter" id="utilization_filter" required class="form-control">
           					<?php foreach ($utilization_filter as $value) { ?>

           						<option value="<?php echo $value->LookupCode;?>" <?php if($this->input->post('utilization_filter')==$value->LookupCode) { echo 'selected';}?>>
           							<?php echo $value->LookupValue; ?></option>
           						<?php } ?>
           					</select>
           				</div>
								<div class="col-md-2 btn-width pull-right" style="padding-right: 30px;">
									<label for="">&nbsp;</label>
									<button class="btn btn-block" style="line-height: 1.2; font-weight: 600;background-color: #f4860c;border-color: #f4860c;color: white;">SEARCH</button>
								</div>

								<?php echo form_close(); ?>
							</div>
							
						</div>

						<table class="table table-striped table-bordered table-hover" id="table_inventory_list">

							
							<thead>
								<tr style="background-color: #085786; font-family: 'Source Sans Pro';color: white;font-size: 13px;">
									<th>Item</th>
									<th>Batch no.</th>
									<th>Quantity of stock currently available (screening tests/bottle)</th>
									<th>From Date (Beginning of the day)</th>
									<th>To Date (End of the day)</th>
									<th>Number of drug bottles dispensed/screening tests dispensed (including repeat tests)</th>
									<th>Number of screening tests used for repeat tests</th>
									<th>Number of controls used (screening tests)</th>
									<th>Total Utilization (screening tests/bottle)</th>
									<th>Purpose of utilization</th>
									<th>Commands</th>
								</tr>
							</thead>
							<tbody>
			<!-- </tbody>
			</table> -->
				<?php // echo "<pre>";/*print_r($inventory_details);*/
				//$result=array_unique($inventory_details->);
				$item_id=array();
				foreach ($utilize_items as $key => $value) {
					foreach ($inventory_detail_all as $value1) {
						if($value->id_mst_drugs==$value1->drug_name){
							$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
							$item_id[$key]['drug_name']= $value->drug_name;
						}
					}
					
				}
				$filtered_arr=array_unique($item_id,SORT_REGULAR);
				//pr($filtered_arr);exit();
				//pr($inventory_detail_all);
				?>
				<!-- <table class="table table-striped table-bordered table-hover">
					<tbody> -->
						<?php if(!empty($inventory_detail_all)){
							foreach ($filtered_arr as $key=>$value_temp) { ?>
								<tr>
									<th colspan="10" class="info"style="border: none;background-color: #333;font-size: 16px;color:#fff;text-align: left;"><?php echo $value_temp['drug_name']; ?></th>
									<th class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus" aria-hidden="true" id="<?php echo 'head'.$value_temp['id_mst_drugs']; ?>"></i></th></tr>
									<?php foreach ($inventory_detail_all as  $value) { 
										if($value_temp['id_mst_drugs']==$value->drug_name){
											?>
											<tr class="<?php echo $value_temp['id_mst_drugs']; ?>">
												<td class="item">
													<?php if($value->type==1){echo $value_temp['drug_name']."(".$value->strength.")";} elseif($value->type==2){echo $value_temp['drug_name'];} ?>
												</td>
												<td>
													<?php echo $value->batch_num; ?>
												</td>
												<td>
													<?php echo $value->available_quantity; ?>
												</td>
												<td nowrap>
													<?php if ($value->from_Date=='0000-00-00' || $value->from_Date==NULL) {
															echo "N/A";
													}else{
														echo timeStampShow($value->from_Date);
													}?>
												</td>
												<td nowrap>
													<?php if ($value->to_Date=='0000-00-00' || $value->to_Date==NULL) {
															echo "N/A";
													}else{
														echo timeStampShow($value->to_Date);
													}?>
												</td>
											<td>
													<?php echo !empty($value->dispensed_quantity)> 0 ? $value->dispensed_quantity: "N/A"; ?>
												</td>
												<td>
													<?php echo !empty($value->repeat_quantity)> 0 ? $value->repeat_quantity : "N/A"; ?>
												</td>
													<td>
													<?php echo !empty($value->control_used)> 0 ? $value->control_used : "N/A"; ?>
												</td>
												<td>

													<?php 
														$total=$value->dispensed_quantity+$value->control_used;
													echo !empty($total)> 0 ? ($total) : "N/A"; ?>
												</td>
												<td>
												<?php 
												if ($value->utilization_purpose!="" || $value->utilization_purpose!=0) {
												foreach ($utilization_purpose as $utilization) {
													 if($value->utilization_purpose==$utilization->LookupCode) { echo $utilization->LookupValue;}}}else{
													 	echo "N/A";
													 } ?>
													</td>
												<td class="text-center">
													<?php if ($value->is_utilized==1) { ?>
													<a href="" style="padding : 4px;" title="Edit" data-toggle="modal" data-target="#myModal1" onclick='get_utilization_data(<?php echo $value->inventory_id ?>);'><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;" ></span></a>
													<a href="#" onclick='delete_indent_data(<?php echo $value->inventory_id ?>);'style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
												</td>
												<?php }?>
											</tr>
										<?php } }}}else{ ?>
											<tr >
												<td colspan="11" class="text-center">
													NO RECORDS FOUND  BETWEEN SELECTED DATES.
												</td>

											</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
							<br>
						</div>
						<div class="overlay">
							<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
						</div>
				<!-- Data Table -->
						<script type="text/javascript">
$( document ).ready(function() {
   		get_rem_item_name();
			});	
							
							$("#dispensed_quantity,#repeat_quantity,#control_used").on('keypress keyup',function(evt){

								var charCode = (evt.which) ? evt.which : event.keyCode
								
								if ((charCode > 31 && (charCode < 48 || charCode > 57)) || parseInt($(this).val()+ evt.key)>10000){
									evt.preventDefault();
									return false;
								}
								
								if(parseInt($(this).val()) <=0 && $(this).val()!='')
								{
									$("#modal_header").html('Quantity must be greater than 0');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$(this).val('');
									return false;
								}
								
							});

$("#dispensed_quantity").on('keyup',function(evt){

					var error_css = {"border":"1px solid red"};
					if($('#item_name').val().trim() == ""){
		
									$("#modal_header").text("Please Select Item");
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#item_name").css(error_css);
									$("#item_name").focus();
									$("#dispensed_quantity").val('');
									e.preventDefault();
									return false;
								}
					else if($('#batch_num').val().trim() == ""){

									$("#modal_header").text("Please Select Batch Num");
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#batch_num").css(error_css);
									$("#batch_num").focus();
									$("#dispensed_quantity").val('');
									e.preventDefault();
									return false;
								}
								else if((parseInt($(this).val()) >parseInt($('#available_quantity').val()))&&$('#available_quantity').val()!='')
								{
									$("#modal_header").html('Dispensed Quantity must be greater than Available Quantity');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$(this).val('');
									return false;
								}
							var rem_quantity=parseInt($('#available_quantity').val())-parseInt($('#dispensed_quantity').val())-parseInt($('#control_used').val());
								if(rem_quantity<0)
								{
									$("#modal_header").html(' Quantity must be less than Rem Quantity');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$("#repeat_quantity").val('');
									$("#control_used").val('');
									return false;
								}
								if((parseInt($(this).val()) <=parseInt($('#control_used').val())&&$('#control_used').val()!='') || (parseInt($(this).val()) <=parseInt($('#repeat_quantity').val())&&$('#repeat_quantity').val()!=''))
								{
									$("#modal_header").html('Quantity must be greater than control used/Repeat Quantity');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$("#control_used").val('');
									$("#repeat_quantity").val('');
									return false;
								}
							});

$("#repeat_quantity,#control_used").on('keyup',function(evt){

	var rem_quantity=parseInt($('#available_quantity').val())-parseInt($('#dispensed_quantity').val())-parseInt($('#control_used').val());
	//alert(rem_quantity);

				 if((parseInt($(this).val()) >=parseInt($('#dispensed_quantity').val()))&&$('#available_quantity').val()!='')
								{
									$("#modal_header").html(' Quantity must be less than Dispensed Quantity');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$(this).val('');
									return false;
								}
						if(rem_quantity<0)
								{
									$("#modal_header").html(' Quantity must be less than Rem Quantity after despensed Quantity');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$(this).val('');
									return false;
								}
							});
function get_utilization_data(inventory_id){
$('#inventory_id').val(inventory_id);
$('.modal-title').html('Update Utilization/Dispensation ');
//alert($('#inventory_id').val());
$.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Inventory/edit_inventory_data", // <-- properly quote this line
    cache: false,
    async : true,
    data: {inventory_id:inventory_id, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'}, 
    success: function(data) {
        var returned = JSON.parse(data);
        //console.log(returned);
    var id_mst_drugs = returned.inventory_details.map(function(e) { return e.id_mst_drugs; });
     var dispensed_quantity = returned.inventory_details.map(function(e) { return e.dispensed_quantity; });
      var available_quantity = returned.inventory_details.map(function(e) { return e.available_quantity; });
       var repeat_quantity = returned.inventory_details.map(function(e) { return e.repeat_quantity; });

        var control_used = returned.inventory_details.map(function(e) { return e.control_used; });

      var from_Date = returned.inventory_details.map(function(e) { return e.from_Date; });

        var to_Date = returned.inventory_details.map(function(e) { return e.to_Date; });

       var utilization_purpose = returned.inventory_details.map(function(e) { return e.utilization_purpose; });

       var batch_num = returned.inventory_details.map(function(e) { return e.batch_num; });
//alert(batch_num);
       var parts = from_Date[0].split('-');
 		var date1=parts[2] + "-"+ parts[1] + "-" + parts[0];

 		 var parts = to_Date[0].split('-');
 		var date2=parts[2] + "-"+ parts[1] + "-" + parts[0];

 	 $("#item_name_val").val(id_mst_drugs);
    $("#batch_num_val").val(batch_num);
    get_rem_item_name();
    $("#dispensed_quantity").val(dispensed_quantity);
     $("#available_quantity").val(available_quantity);
	$("#repeat_quantity_val").val(repeat_quantity);
	$("#control_used_val").val(control_used);
    $("#from_Date").val(date1);
     $("#to_Date").val(date2);
    $("#utilization_purpose").val(utilization_purpose);

}
});
}

$('#item_name,#dispensed_quantity,#repeat_quantity,#control_used,#from_Date,#to_Date,#utilization_purpose,#batch_num').on('change click keypress',function(e){
						
	var error_css = {"border":"1px solid #c7c5c5"};
			$(this).css(error_css);
	return true;

});
$('#from_Date').on('change',function(e){
var error_css = {"border":"1px solid red"};
var To_Date =($('#to_Date').datepicker('getDate'));
if($('#item_name').val().trim() == ""){
		
									$("#modal_header").text("Please Select Item");
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#item_name").css(error_css);
									$("#item_name").focus();
									e.preventDefault();
									return false;
								}
	else if($('#batch_num').val().trim() == ""){

									$("#modal_header").text("Please Select Batch Num");
									//$("#modal_text").text("Please check dates");
									$("#multipurpose_modal").modal("show");
									$("#batch_num").css(error_css);
									$("#batch_num").focus();
									e.preventDefault();
									return false;
								}
		else if (from_Date <To_Date && To_Date!='') { 
									$("#modal_header").text("From date cannot be before To Date");
									$("#modal_text").text("Please check dates");
									$("#to_Date" ).val('');
									$("#multipurpose_modal").modal("show");
									return false;
								}						
					else{			
					var from_Date =($('#from_Date').datepicker('getDate'));
					var last_Date=($('#last_to_date').val()).toString();
					var last_Date_flags=($('#last_to_date_flags').val()).toString();
					var flag_arr=last_Date_flags.split(',');
					var date_arr=last_Date.split(',');
					var index=($("#batch_num")[0].selectedIndex);
					var parts=date_arr[index-1].split('-');
					var flag_value=flag_arr[index-1];
					var to_date=new Date(parts[1] + '-' + parts[2] + '-' + parts[0]);
					//alert(to_date);
					// alert(entrydate+"**"+from_Date);
								if (from_Date < to_date && flag_value==0) { 
									$("#modal_header").text("From date cannot be before ("+(parts[2] + '-' + parts[1] + '-' + parts[0])+")");
									$("#modal_text").text("Please check dates");
									$("#from_Date" ).val('');
									$("#multipurpose_modal").modal("show");
									return false;
								}
								else if (from_Date <= to_date && flag_value==1) { 
									$("#modal_header").text("From date cannot be before and equal to ("+(parts[2] + '-' + parts[1] + '-' + parts[0])+")");
									$("#modal_text").text("Please check dates");
									 $("#from_Date" ).val('');
									$("#multipurpose_modal").modal("show");
									return false;
								}

								else{
									return true;
								}
							}
});
$("#modal_close,#close_icon").on('click ',function(evt){
	empty_inputs();

});
 $("#myModal1").on('hide.bs.modal', function(){
 	empty_inputs();

  });
 function empty_inputs(){
 	$('.modal-title').html('Add Utilization/Dispensation');
 	 $('#stock_utilization_form').find("input[type=text],textarea,select").val("");
 	$('#inventory_id').val('');
 	$('#last_to_date').val('');
 	$('#last_to_date_flags').val('');
 	$('#rem_values').val('');
 	$('#type_val').val('');
 	$('#batch_num_val').val('');
 	$('#item_name_val').val('');
 	$('#repeat_quantity_val').val('');
 	$('#control_used_val').val('');
 	$('#utilization_purpose').val(2);
 }
$('#submit_btn').click(function(e){
		var error_css = {"border":"1px solid red"};
//alert($('#batch_num').val());

		if($('#item_name').val().trim() == ""){

			$("#item_name").css(error_css);
			$("#item_name").focus();
			e.preventDefault();
			return false;
		}
if($('#batch_num').val().trim() == ""){

			$("#batch_num").css(error_css);
			$("#batch_num").focus();
			e.preventDefault();
			return false;
		}
		 
 if($('#from_Date').val().trim()==""){

			$("#from_Date").css(error_css);
			$("#from_Date").focus();
			e.preventDefault();
			return false;
		}
if($('#to_Date').val().trim()==""){

			$("#to_Date").css(error_css);
			$("#to_Date").focus();
			e.preventDefault();
			return false;
		}
if($('#dispensed_quantity').val().trim()=="" || parseInt($('#dispensed_quantity').val())==0){

			$("#dispensed_quantity").css(error_css);
			$("#dispensed_quantity").focus();
			e.preventDefault();
			return false;
		}	
	if ($("#type_val").val()==2) {		
if($('#repeat_quantity').val().trim()=="" || parseInt($('#repeat_quantity').val())==0){

			$("#repeat_quantity").css(error_css);
			$("#repeat_quantity").focus();
			e.preventDefault();repeat_quantity
			return false;
		}	
if($('#control_used').val().trim()=="" || parseInt($('#control_used').val())==0){

			$("#control_used").css(error_css);
			$("#control_used").focus();
			e.preventDefault();repeat_quantity
			return false;
		}		
		}				
		if($('#utilization_purpose').val().trim()==""){

			$("#utilization_purpose").css(error_css);
			$("#utilization_purpose").focus();
			e.preventDefault();
			return false;
		}
		
			return true;

	});
$("#stock_utilization_form").submit(function() {
            $(this).submit(function() {
                return false;
            });
            return true;
        });
							$(document).ready(function(){
								$('#utilization_purpose').val(2);
								<?php foreach ($filtered_arr as $key=>$value_temp) {
									$elemnt_id= ".".$value_temp['id_mst_drugs'];?> 
									$("<?php echo $elemnt_id; ?>").show();
									$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").click(function(){
										if($("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").hasClass('fa-minus')){
											$("<?php echo $elemnt_id; ?>").hide();
											$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-minus').addClass("fa-plus");
										}
										else{
											$("<?php echo $elemnt_id; ?>").show();
											$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-plus').addClass("fa-minus");
										}
										
									});
									$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").css('cursor', 'pointer');
								<?php }?>
							});

							function checkenddate()
							{
								var startdate = $('#from_Date').datepicker('getDate');
								var enddate = $('#to_Date').datepicker('getDate');
								if ( startdate > enddate && enddate!=null) { 
									$("#modal_header").text("To date cannot be before From Date");
									$("#modal_text").text("Please check dates");
									$("#to_Date" ).val('');
									$("#multipurpose_modal").modal("show");
									return false;
								}
								else{
									return true;
								}
								
							}
							
			function delete_indent_data(inventory_id){
									
									   		 $( "#dialog-confirm" ).dialog({
									   		open : function() {
										    $("#dialog-confirm").closest("div[role='dialog']").css({top:100,left_nav_bar:'40%'});
											}, 	
									      resizable: false,
									      height: "auto",
									      width: 400,
									      modal: true,
									      buttons: {
									        "Delete ": function() {
									          window.location.replace('<?php echo base_url(); ?>Inventory/delete_receipt/'+inventory_id+'/U');

									        },
									        "Don't Delete ": function() {
									          $( this ).dialog( "close" );
									          e.preventDefault();
									        }
									      }
									 			 });
									   		 $("#dialog-confirm").dialog("option", "title", "Delete Stock Utilization Data");
								};		
		$('#item_name').change(function () {
	

	var item=$('#item_name').val();
	var inventoryid=$('#inventory_id').val();
	if(item!=null){
                $.ajax({
                    url : "<?php echo site_url('inventory/get_data_for_utilize');?>",
                    method : "GET",
                    data : {item_name: item,inventory_id: inventoryid,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                        console.log(data);
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
        				var html = returned.get_batch_num.map(function(e) {
   						return e.batch_num;
						});
						var last_to_date = returned.get_batch_num.map(function(e) {
   						return e.to_Date;
						});
						var rem_values = returned.get_batch_num.map(function(e) {
   						return e.rem;
						});
						var last_to_date_flags = returned.get_batch_num.map(function(e) {
   						return e.to_Date_flag;
						});
						var type_val = returned.get_batch_num.map(function(e) {
   						return e.type;
						});

						$("#type_val").val(type_val[0]);
						
						if(type_val[0]=='1')
						{
							$('#repeat_quantity').removeAttr('required');
							$('#repeat_quantity').attr('disabled','disabled');
							$('#control_used').removeAttr('required');
							$('#control_used').attr('disabled','disabled');
							$('#repeat_quantity').val('');
							$('#control_used').val('');
							$('.type_label').hide();
							$('#dispensed_quantity_label').text('Number of drug bottle dispensed');
							$('#approved_quantity_label').text('Quantity of stock currently available (bottle)');

						}
						else if(type_val[0]=='2')
						{
							
							$('#repeat_quantity').attr('required','required');
							$('#repeat_quantity').removeAttr('disabled','disabled');
							$('#control_used').attr('required','required');
							$('#control_used').removeAttr('disabled','disabled');
							$('#repeat_quantity').val('');
							$('#control_used').val('');
							$('.type_label').show();
							$('#dispensed_quantity_label').text('Number of screening tests dispensed');
							$('#approved_quantity_label').text('Quantity of stock currently available (screening tests');

						}
                        var i;
                        var opt='';
                        opt +='<option value="">----Select----</option>';
                        for(i=0; i<html.length; i++){

								
								select='';
                        
                        	opt += '<option value="'+html[i]+'"'+select+'>'+html[i]+'</option>'; 	
                   
                            
                        }
                        //console.log(opt);
                        $('#batch_num').html(opt);
                   		$('#batch_num').trigger('change');
 						$("#batch_num").val($('#batch_num_val').val());
 						  $('#last_to_date').val(last_to_date);
						$('#rem_values').val(rem_values);
						$('#last_to_date_flags').val(last_to_date_flags);
						$("#repeat_quantity").val($("#repeat_quantity_val").val());
						$("#control_used").val($("#control_used_val").val());
                    }
                });
           }
                return false;
});		
		$('#batch_num').change(function () {
								if ($('#batch_num').val()!=''){
									var values=($('#rem_values').val()).toString();
									var values_arr=values.split(',');
									var index=($("#batch_num")[0].selectedIndex);
									var quantity=parseInt(values_arr[index-1]);
									$('#available_quantity').val(quantity);
								
								}	
								});	
function get_rem_item_name(){


	var inventoryid=$('#inventory_id').val();
                $.ajax({
                    url : "<?php echo site_url('inventory/get_rem_item_name');?>",
                    method : "GET",
                    data : {inventory_id: inventoryid, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'},
                    async : true,
                   //dataType : 'json',
                    success: function(data){
                         console.log(data);
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
        				var id_mst_drug = returned.get_rem_items.map(function(e) {
   						return e.id_mst_drug_strength;
						});
						var drug_name = returned.get_rem_items.map(function(e) {
   						return e.drug_name;
						});
						var strength = returned.get_rem_items.map(function(e) {
   						return e.strength;
						});
						 var type = returned.get_rem_items.map(function(e) {
   						return e.type;
						});
                        var i;
                        var opt='';
                        opt +='<option value="">----Select----</option>';
                        for(i=0; i<id_mst_drug.length; i++){

								
								select='';
                        if (type[i]==1) {
                        	opt += '<option value="'+id_mst_drug[i]+'"'+select+'>'+drug_name[i]+' '+strength[i]+' mg</option>'; 
                        }
                 	else if(type[i]==2){
                 		opt += '<option value="'+id_mst_drug[i]+'"'+select+'>'+drug_name[i]+'</option>'; 
                 	}
                   
                            
                        }
                        //console.log(opt);
                        $('#item_name').html(opt);
                         $("#item_name").val($("#item_name_val").val());
                         $('#item_name').trigger('change');
                    }
                });
          
						
}																			
						</script>


