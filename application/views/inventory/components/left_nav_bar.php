<style>
	a.fa-globe {
  position: relative;
  font-size: 2em;
  color: grey;
  cursor: pointer;
}
span.fa-comment {
  position: absolute;
  font-size: 0.6em;
  top: -4px;
  color: red;
  right: -4px;
}
span.num {
  position: absolute;
  font-size: 0.3em;
  top: 1px;
  color: #fff;
  right: 2px;
}
.isDisabled {
  color: currentColor;
  cursor: not-allowed;
  opacity: 0.5;
  text-decoration: none;
   pointer-events: none;
}
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	input,nav,label,span{
		font-family: 'Source Sans Pro';

	}
	input,select{
		height: 30px;
	}
	.panel-heading {
    	padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	.active11
{
   background-color: #f4860c; !important;
   color: #ffff !important;
}
.active11 a
{
   background-color: #f4860c; !important;
   color: #ffff !important;
   font-weight: bold;
}
td a{
	color: black;
	font-weight: bold;
}



	/*Remove rounded coners*/

	nav.sidebar.navbar {
		border-radius: 0px;
		background-color: #333;
	}

	nav.sidebar, .main{
		-webkit-transition: margin 200ms ease-out;
	    -moz-transition: margin 200ms ease-out;
	    -o-transition: margin 200ms ease-out;
	    transition: margin 200ms ease-out;
	}

	/* Add gap to nav and right windows.*/

	/* .....NavBar: Icon only with coloring/layout.....*/

	/*small/medium side display*/
	@media (min-width: 768px) {

		/*Allow main to be next to Nav*/
		
		/*lets nav bar to be showed on mouseover*/
		

		/*Center Brand*/
		nav.sidebar.navbar.sidebar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand {
			margin-left: 0px;
		}
		/*Center Brand*/
		nav.sidebar .navbar-brand, nav.sidebar .navbar-header{
			text-align: center;
			width: 100%;
			margin-left: 0px;
		}

		/*Center Icons*/
		nav.sidebar a{
			padding-right: 13px;
		}

		/*adds border top to first nav box */
		nav.sidebar .navbar-nav > li:first-child{
			border-top: 1px #e5e5e5 solid;
		}

		/*adds border to bottom nav boxes*/
		nav.sidebar .navbar-nav > li{
			border-bottom: 1px #e5e5e5 solid;
		}

		/* Colors/style dropdown box*/
		nav.sidebar .navbar-nav .open .dropdown-menu {
			position: static;
			float: none;
			width: auto;
			margin-top: 0;
			background-color: transparent;
			border: 0;
			-webkit-box-shadow: none;
			box-shadow: none;
		}

		/*allows nav box to use 100% width*/
		nav.sidebar .navbar-collapse, nav.sidebar .container-fluid{
			padding: 0 0px 0 0px;
		}

		/*colors dropdown box text */
		.navbar-inverse .navbar-nav .open .dropdown-menu>li>a {
			color: #777;
		}

		/*gives sidebar width/height*/
		nav.sidebar{
			width: 200px;
			height: 100%;
			margin-left: -160px;
			float: left;
			/* z-index: 8000; */
			margin-bottom: 0px;
		}

		/*give sidebar 100% width;*/
		nav.sidebar li {
			width: 100%;
		}

		/* Move nav to full on mouse over*/
		nav.sidebar:hover{
			margin-left: 0px;
		}
		/*for hiden things when navbar hidden*/
		.forAnimate{
			opacity: 0;
		}
	}

	/* .....NavBar: Fully showing nav bar..... */

	@media (min-width: 1330px) {

		/*Allow main to be next to Nav*/
	

		/*Show all nav*/
		nav.sidebar{
			margin-left: 0px;
			float: left;
		}
		/*Show hidden items on nav*/
		nav.sidebar .forAnimate{
			opacity: 1;
		}
	}

	nav.sidebar .navbar-nav .open .dropdown-menu>li>a:hover, nav.sidebar .navbar-nav .open .dropdown-menu>li>a:focus {
		color: #CCC;
		background-color: transparent;
	}

	nav:hover .forAnimate{
		opacity: 1;
	}
	section{
		padding-left: 15px;
	}
</style>

				<!-- <div class="panel-heading">
					<h4 class="text-center" style="color : white;">Inventory</h4>
				</div>
				<div class="panel-body">
					<table id="data-table-command" class="table table-bordered table-hover">
						<tbody id="table_facility_list">
							<tr>
				
								<td class="text-center <?php echo ($this->uri->segment(3) == 'R' )?'active11':''; ?>" style="padding-top: 16px;"><a href="<?php echo base_url(); ?>inventory/index/R">Receipt</a></td>
							</tr>
								<tr>
								<td class="text-center <?php echo ($this->uri->segment(3) == 'T')?'active11':''; ?>" style="padding-top: 16px;"><a href="<?php echo base_url(); ?>inventory/index/T">Transfer Out</a></td>
								</tr>
								<tr>
								<td class="text-center <?php echo ($this->uri->segment(3) == 'U')?'active11':''; ?>" style="padding-top: 16px;"><a href="<?php echo base_url(); ?>inventory/index/U">Utilization/Dispensation</a></td>
								</tr>
								<tr>
								<td class="text-center <?php echo ($this->uri->segment(3) == 'W')?'active11':''; ?>" style="padding-top: 16px;"><a href="<?php echo base_url(); ?>inventory/index/W">Wastage/Missing</a></td>
								</tr>
								<tr>
								<td class="text-center" style="padding-top: 16px;">Stock Reorder</td>
							</tr>
						</tbody>
					</table>
				</div> -->

				<nav class="navbar navbar-inverse sidebar" role="navigation" style="margin-left: 10px;">
    <div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
			<ul class="nav navbar-nav">
		<li class="<?php echo ($this->uri->segment(3) == 'I' )?'active11':''; ?>"><a href="<?php echo base_url(); ?>inventory/index/I">Stock Indent<!-- <span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-envelope"></span><span class="num" style="font-size:16px; color: white">2</span> --></a></li>


		<li class="<?php echo ($this->uri->segment(3) == 'R' )?'active11':''; ?>"><a href="<?php echo base_url(); ?>inventory/index/R">Stock Receipt <?php if (!empty($new_recipt_notification[0]->count)) {?><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-envelope"></span><span class="num" style="font-size:16px; color: white"><?php echo $new_recipt_notification[0]->count; ?></span><?php }?><?php if (!empty($new_recipt_notification[0]->is_new)) {?><span style="font-size:16px;margin-left: 0px;" class="hidden-xs"><img style="width: 40px;" src="<?php echo site_url(); ?>/common_libs/images/new-badge2.png" alt="new alert"></span><?php }?></a></li>

		<li class="<?php echo ($this->uri->segment(3) == 'F' )?'active11':''; ?>"><a href="<?php echo base_url(); ?>inventory/index/F">Stock Failure<!-- <span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-envelope"></span><span class="num" style="font-size:16px; color: white">2</span> --></a></li>

		<li class="<?php echo ($this->uri->segment(3) == 'U' )?'active11':''; ?>"><a href="<?php echo base_url(); ?>inventory/index/U">Utilization/Dispensation<!-- <span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-envelope"></span><span class="num" style="font-size:16px; color: white">2</span> --></a></li>
		
				<li class="<?php echo ($this->uri->segment(3) == 'L' )?'active11':''; ?>"><a href="<?php echo base_url(); ?>inventory/index/L">Stock Relocation<?php if (!empty($new_relocation_request[0]->count)) {?><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-envelope"></span><span class="num" style="font-size:16px; color: white"><?php echo $new_relocation_request[0]->count; ?></span><?php }?><?php if (!empty($new_relocation_request[0]->is_new)) { ?><span style="font-size:16px;margin-left: 0px;" class="hidden-xs"><img style="width: 40px;" src="<?php echo site_url(); ?>/common_libs/images/new-badge2.png" alt="new alert"></span><?php } ?></a></li>
				<!-- <li class="<?php //echo ($this->uri->segment(2) == 'approved_indent' )?'active11':''; ?>"><a href="<?php // echo base_url(); ?>inventory/approved_indent">Designated Officer Module<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-envelope"></span><span class="num" style="font-size:16px; color: white">2</span></a></li> -->
			</ul>
		</div>
	</div>
</nav>

<script type="text/javascript">
	function htmlbodyHeightUpdate(){
		var height3 = $( window ).height()
		var height1 = $('.nav').height()+50
		height2 = $('.main').height()
		if(height2 > height3){
			$('html').height(Math.max(height1,height3,height2)+10);
			$('body').height(Math.max(height1,height3,height2)+10);
		}
		else
		{
			$('html').height(Math.max(height1,height3,height2));
			$('body').height(Math.max(height1,height3,height2));
		}
		
	}
	$(document).ready(function () {
		htmlbodyHeightUpdate()
		$( window ).resize(function() {
			htmlbodyHeightUpdate()
		});
		$( window ).scroll(function() {
			height2 = $('.main').height()
  			htmlbodyHeightUpdate()
		});
	});
</script>