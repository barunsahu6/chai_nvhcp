<style>
	#table_inventory_list th 
	{
		text-align: center; 
		vertical-align: middle;
	}
	td{
		text-align: center; 
		vertical-align: middle;
	}
	.btn-width{
	width: 12%;
}
	.item{
		width: 140px !important;
	}
	.batch_num{
		width: 100px !important;
	}
	a{
		cursor: pointer;
	}
	.set_height
	{
		max-height: 425px;
		overflow-y: scroll;

	}
	.panel-heading {
		padding: 1px 15px;
	}
	label
	{
		padding-top: 5px;
	}
	.overlay{
		z-index : -1;
		width: 100%;
		height: 100%;
		background-color: rgba(0,0,0,0.5);
		top : 0; 
		left: 0; 
		position: fixed; 
		display : none;
	}
	.loading_gif{
		width : 100px;
		height : 100px;
		top : 45%; 
		left: 45%; 
		position: absolute; 
	}
	input,nav,label,span{
		font-family: 'Source Sans Pro';

	}
	label{
		font-family: 'Source Sans Pro';
		font-size: 14px;
	}
	input,select,.form-control{
		height: 30px;
	}
	
</style>
<?php  //error_reporting(0);
$loginData = $this->session->userdata('loginData'); ?>
<!-- add/edit facility contact modal starts-->
<div class="modal fade" tabindex="-1" role="dialog" id="myModal1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #333;color: white;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_icon" style="color: white;"><span aria-hidden="true" style="color: white;">&times;</span></button>
        <h4 class="modal-title">Add Stock Indent Request</h4>
      </div>
      <div class="modal-body">
      	<?php  $attributes = array(
              'id' => 'AddStock_indent_form',
              'name' => 'AddStock_indent_form',
               'autocomplete' => 'off',
            );
           if(isset($inventory_details[0]->inventory_id)){
           	$inventory_id=$inventory_details[0]->inventory_id;
           }
           else{
           	$inventory_id=NULL;
           }
           echo form_open('Inventory/Stock_indent/'.$inventory_id, $attributes); ?>
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Item Name<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<select name="item_name" id="item_name" required class="form-control">
						<option value="">-----Select-----</option>
										<?php foreach ($items as $value) { ?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_name')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php if($value->type==2){echo $value->drug_name; } else{
													echo $value->drug_name." (".$value->strength.")";
												} ?></option>
											<?php } ?>
										</select>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Quantity Indented (screening tests/bottle)<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<input type="text" class="form-control" name="quantity" id="quantity" required>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Date of placing indent<span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<input type="text" class="form-control hasCal_Receipt dateInpt" placeholder="Receipt Date" name="indent_date" id="indent_date" value="<?php echo date('d-m-Y') ?>" required onkeydown="return false" onkeyup="return false" max="<?php echo date('d-m-Y');?>">
        		</div>
        	</div>
        	
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Indent Number <span class="text-danger">*</span></label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<input type="text" class="form-control" id="indent_num" name="indent_num" placeholder="Indent Number" readonly>
        		</div>
        		 <input type="hidden" id="inventory_id" name="inventory_id" value="">
        		 <input type="hidden" id="indent_num_val" name="indent_num_val" value="">
        	</div>
        	<div class="row">
        		<div class="col-xs-12 col-lg-6">
        			<label for="contact_name">Remarks</label>
        		</div>
        		<div class="col-xs-12 col-lg-6 form-group">
					<textarea class="form-control" id="indent_remark" name="indent_remark">
</textarea>
        		</div>
        	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="modal_close">Close</button>
        <button type="submit" class="btn btn-primary" id="submit_btn" name="save" value="save">Save</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>

<!-- add/edit facility contact modal ends-->


<div class="row main-div" style="min-height:400px;margin-top: 1em">
	<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12" style="padding: 0;">
			<?php //include("left_nav_bar.php");
			$this->load->view("inventory/components/left_nav_bar"); ?>
		</div>

		<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
			<div class="panel panel-default" id="Record_receipt_panel">
				<div class="panel-heading" style=";background: #333;">
					<h4 class="text-center" style="color: white;background: #333" id="Record_form_head">Stock Indent</h4>
				</div>
				<div class="row">
					<?php 
					$tr_msg= $this->session->flashdata('tr_msg');
					$er_msg= $this->session->flashdata('er_msg');
					if(!empty($tr_msg)){  ?>
						

						<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
							<div class="hpanel">
								<div class="alert alert-success alert-dismissable alert1"> <i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
									<?php echo $this->session->flashdata('tr_msg');?>. </div>
								</div>
							</div>
						<?php } else if(!empty($er_msg)){ ?>
							<div class="col-md-4 col-md-offset-4 col-xs-6 col-xs-offset-3">
								<div class="hpanel">
									<div class="alert alert-danger alert-dismissable alert1"> <i class="fa fa-check"></i>
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										<?php echo $this->session->flashdata('er_msg');?>. </div>
									</div>
								</div>
								
							<?php } ?>
		<div class="col-md-3 pull-right" style="padding-right: 15px;">
			<a href="#" class="btn btn-block btn-success text-center" style="font-weight: 600; background-color: #f4860c;padding-top: 10px;padding-bottom: 10px;" id="open_receipt" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus" aria-hidden="true"></i> Raise Indent Request
</a>
		</div>
						</div>
						
						<?php
						$attributes = array(
							'id' => 'Receipt_filter_form',
							'name' => 'Receipt_filter_form',
							'autocomplete' => 'off',
						);
  //pr($items);
  //print_r($inventory_details);

						echo form_open('', $attributes); ?>
						<div class="row" style="padding-left: 10px;">
							<div class="col-xs-3 col-md-2">
								<label for="item_type">Item Name <span class="text-danger">*</span></label>
								<div class="form-group">
									<select name="item_type" id="item_type" required class="form-control">
										<option value="drugkit" <?php if($this->input->post('item_type')=='drugkit') { echo 'selected';}?>>All Kits and Drugs</option>
										<option value="Kit"<?php if($this->input->post('item_type')=='Kit') { echo 'selected';}?>>All Screening Test Kits</option>
										<option value="drug"<?php if($this->input->post('item_type')=='drug') { echo 'selected';}?>>All Drugs</option>
										
										<?php foreach ($items as $value) { ?>
											<option value="<?php echo $value->id_mst_drug_strength;?>" <?php if($this->input->post('item_type')==$value->id_mst_drug_strength) { echo 'selected';}?>>
												<?php if($value->type==2){echo $value->drug_name; } else{
													echo $value->drug_name." (".$value->strength.")";
												} ?></option>
											<?php } ?>
											<!-- <option value="999"<?php if($this->input->post('item_type')=='999') { echo 'selected';}?>>other</option> -->
										</select>
									</div>
								</div>
								
								
						
								<div class="col-md-2 col-sm-12 form-group">
									<label for="year">Financial Year <span class="text-danger">*</span></label>
									<select name="year" id="year" required class="form-control" style="height: 30px;">
										<option value="">Select</option>
										<?php
										$dates = range('2018', date('Y'));
										$flag=0;
										foreach($dates as $date){

									if (date('m', strtotime($date)) < 4) {//Upto April
										$year = ($date-1) . '-' . $date;
									} else {//After April
										$year = $date . '-' . ($date + 1);
									}
									if($this->input->post('year')==$year){
										echo "<option value='$year' selected>$year</option>";
										$flag=1;
									}
									else if($date==date('Y') && $flag==0){
										echo "<option value='$year' selected>$year</option>";
									}
									else{
										echo "<option value='$year'>$year</option>";
									}
									
								}
								?>
							</select>
						</div>
								<div class="col-md-2 col-sm-12">
									<label for="">From Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="From Date" name="startdate" id="startdate" value="<?php if($this->input->post('startdate')){ echo $this->input->post('startdate');} else{ echo timeStampShow(date("Y-m-d", mktime(0, 0, 0, date("m"), 1)));}  ?>" required style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 col-sm-12">
									<label for="">To Date</label>
									<input type="text" class="form-control input_fields hasCalreport dateInpt input2" placeholder="To Date" name="enddate" id="enddate" value="<?php if($this->input->post('enddate')){echo $this->input->post('enddate');}else{echo timeStampShow(date('Y-m-d'));} ?>" required  style="font-size: 14px;font-family: 'Source Sans Pro'">
								</div>
								<div class="col-md-2 btn-width pull-right" style="padding-right: 30px;">
									<label for="">&nbsp;</label>
									<button class="btn btn-block" style="line-height: 1.2; font-weight: 600;background-color: #f4860c;border-color: #f4860c;color: white;">SEARCH</button>
								</div>

								<?php echo form_close(); ?>
							</div>
							
						</div>

						<table class="table table-striped table-bordered table-hover" id="table_inventory_list">

							
							<thead>
								<tr style="background-color: #085786; font-family: 'Source Sans Pro';color: white;font-size: 13px;">
									<th>Item</th>
									<th>Indent no.</th>
									<th>Quantity Indented <br>(screening tests/bottle)</th>
									<th>Date of placing indent</th>
									<th>Remarks</th>
									<th>Indent Quantity Approved</th>
									<th>Date of Indent Acceptance/Rejection</th>
									<th>Commands</th>
								</tr>
							</thead>
							<tbody>
			<!-- </tbody>
			</table> -->
				<?php // echo "<pre>";/*print_r($inventory_details);*/
				//$result=array_unique($inventory_details->);
				$item_id=array();
				foreach ($indent_items as $key => $value) {
					foreach ($inventory_detail_all as $value1) {
						if($value->id_mst_drugs==$value1->drug_name){
							$item_id[$key]['id_mst_drugs']= $value->id_mst_drugs;
							$item_id[$key]['drug_name']= $value->drug_name;
						}
					}
					
				}
				$filtered_arr=array_unique($item_id,SORT_REGULAR);
				//pr($filtered_arr);exit();
				//pr($inventory_detail_all);
				?>
				<!-- <table class="table table-striped table-bordered table-hover">
					<tbody> -->
						<?php if(!empty($inventory_detail_all)){
							foreach ($filtered_arr as $key=>$value_temp) { ?>
								<tr>
									<th colspan="7" class="info"style="border: none;background-color: #333;font-size: 16px;color:#fff;text-align: left;"><?php echo $value_temp['drug_name']; ?></th>
									<th class="info" style="border: none;background-color: #333;color:#fff;text-align: right;"><i class="fa fa-minus" aria-hidden="true" id="<?php echo 'head'.$value_temp['id_mst_drugs']; ?>"></i></th></tr>
									<?php foreach ($inventory_detail_all as  $value) { 
										if($value_temp['id_mst_drugs']==$value->drug_name){
											?>
											<tr class="<?php echo $value_temp['id_mst_drugs']; ?>">
												<td class="item">
													<?php if($value->type==1){echo $value_temp['drug_name']."(".$value->strength.")";} elseif($value->type==2){echo $value_temp['drug_name'];} ?>
												</td>
												<td>
													<?php echo $value->indent_num; ?>
												</td>
												<td>
													<?php echo $value->quantity; ?>
												</td>
												<td nowrap>
													<?php echo timeStampShow($value->indent_date); ?>
												</td>
												<td>
													<?php echo $value->indent_remark; ?>
												</td>
												<td>
													<?php echo !empty($value->approved_quantity)>0 ? $value->approved_quantity : "pending" ; ?>
												</td>
												<td nowrap>
													<?php echo timeStampShow($value->indent_AcceptReject_date); ?>
												</td>
												<td class="text-center">
													<?php if($value->indent_status==0){?>
													<a href="" style="padding : 4px;" title="Edit" data-toggle="modal" data-target="#myModal1" onclick='get_indent_data(<?php echo $value->inventory_id ?>);'><span class="glyphicon glyphicon-pencil" style="font-size : 15px; margin-top: 8px;" ></span></a>
													<a href="#" onclick='delete_indent_data(<?php echo $value->inventory_id ?>);'style="padding : 4px;" title="Delete"><span class="glyphicon glyphicon-trash" style="font-size : 15px; margin-top: 8px;"></span></a>
												<?php }?>
												</td>

											</tr>
										<?php } }}}else{ ?>
											<tr >
												<td colspan="10" class="text-center">
													NO RECORDS FOUND  BETWEEN SELECTED DATES.
												</td>

											</tr>
										<?php }?>
									</tbody>
								</table>
							</div>
							<br>
						</div>
						<div class="overlay">
							<img src="<?php echo site_url(); ?>/common_libs/images/spinner.gif" alt="loading gif" class="loading_gif">
						</div>
				<!-- Data Table -->
						<script type="text/javascript">

							
							$("#quantity").on('keypress keyup',function(evt){

								var charCode = (evt.which) ? evt.which : event.keyCode
								
								if ((charCode > 31 && (charCode < 48 || charCode > 57)) || parseInt($(this).val()+ evt.key)>10000){

									evt.preventDefault();
									return false;
								}
								if($('#quantity').val() <=0 && $('#quantity').val()!='')
								{
									$("#modal_header").html('Quantity must be greater than 0');
									$("#modal_text").html('Please fill in appropriate Quantity');
									$("#multipurpose_modal").modal("show");
									$('#quantity').val('');
									return false;
								}
								return true;
							});

function get_indent_data(inventory_id){
$('#inventory_id').val(inventory_id);
$('.modal-title').html('Update Stock Indent Request');
//alert($('#inventory_id').val());
$.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Inventory/edit_inventory_data", // <-- properly quote this line
    cache: false,
    async : true,
    data: {inventory_id:inventory_id, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'}, 
    success: function(data) {
        var returned = JSON.parse(data);
        console.log(returned);
    var id_mst_drugs = returned.inventory_details.map(function(e) { return e.id_mst_drugs; });
     var quantity = returned.inventory_details.map(function(e) { return e.quantity; });
      var indent_date = returned.inventory_details.map(function(e) { return e.indent_date; });

       var indent_remark = returned.inventory_details.map(function(e) { return e.indent_remark; });

       var indent_num = returned.inventory_details.map(function(e) { return e.indent_num; });

       var parts = indent_date[0].split('-');
 		var date=parts[2] + "-"+ parts[1] + "-" + parts[0];

    $("#item_name").val(id_mst_drugs);

    $("#quantity").val(quantity);

    $("#indent_date").val(date);

    $("#indent_remark").val(indent_remark);

    $("#indent_num").val(indent_num);

    $("#indent_num_val").val(indent_num);
      
}
});
}
$("#modal_close,#close_icon").on('click ',function(evt){
	$('#inventory_id').val('');
	$('.modal-title').html('Add Stock Indent Request');
   
 $('#AddStock_indent_form').find("input[type=text],input[type=hidden], textarea,select").val("");
  $('#indent_date').val('<?php echo date("d-m-Y"); ?>');
});
$("#myModal1").on('hide.bs.modal', function(){
   // alert('The modal is about to be hidden.');
   
 $('#AddStock_indent_form').find("input[type=text],input[type=hidden], textarea,select").val("");
  $('#indent_date').val('<?php echo date("d-m-Y"); ?>');
  });
$("#item_name,#indent_date").on('keypress change',function(evt){
var item=$('#item_name').val();
var indentdate=$('#indent_date').val();
var indent_num_val=$('#indent_num_val').val();
if (indent_num_val!="") {
 var indentparts = indent_num_val.split('/');
 var indent_date_year=indentparts[3].substring(0, 4);
  var indent_date_month=indentparts[3].substring(4, 6);
   var indent_date_day=indentparts[3].substring(6, 8);
   indent_date_val=new Date(indent_date_month+"-"+indent_date_day+"-"+indent_date_year);
}

 var parts = indentdate.split('-');
 var date=parts[2] + parts[1] +  parts[0];
var str='SO/';
<?php foreach ($items as $value){ ?>
if (item==<?php echo $value->id_mst_drug_strength; ?>) {
	$.ajax({
    type: 'GET',
   url: "<?php echo base_url(); ?>Inventory/get_sequence_no", // <-- properly quote this line
    cache: false,
    async : true,
    data: {drug_name:'<?php echo $value->id_mst_drugs; ?>', indent_date: indentdate, <?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'}, 
    success: function(data) {
        var returned = JSON.parse(data);
        //console.log(returned);
     var counts = returned.item_sequence.map(function(e) {
   return e.count;
  
});
    if (indent_num_val!="" && ('<?php echo $value->drug_abb; ?>'==indentparts[1])) {
    if (($('#indent_date').datepicker('getDate'))<indent_date_val && ('<?php echo $value->drug_abb; ?>'==indentparts[1]) || (($('#indent_date').datepicker('getDate'))>indent_date_val && ('<?php echo $value->drug_abb; ?>'==indentparts[1]))) {
     	 str=str+'<?php echo $value->drug_abb."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(counts)+1);
     }
    else {
     	 str=$("#indent_num_val").val();
     }
     }
     else if (indent_num_val!="" && ('<?php echo $value->drug_abb; ?>'!=indentparts[1])) {
     	 str=str+'<?php echo $value->drug_abb."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(counts)+1);
     }
     else{
     	str=str+'<?php echo $value->drug_abb."/".$FacilityCode[0]->FacilityCode."/"; ?>'+date+'/'+(parseInt(counts)+1);
     }
    $('#indent_num').val(str);   
}
});
	/*str=str+'<?php echo $value->drug_abb."/".$FacilityCode[0]->FacilityCode."/".date("Y").date("m").date("d"); ?>/';*/

}

 
	
<?php } ?>


});
$('#item_name,#quantity,#indent_date').on('change click keypress',function(e){
						
	var error_css = {"border":"1px solid #c7c5c5"};
			$(this).css(error_css);
	return true;

});
$('#submit_btn').click(function(e){
		var error_css = {"border":"1px solid red"};
		var error_free=true;

		if($('#item_name').val().trim() == ""){

			$("#item_name").css(error_css);
			$("#item_name").focus();
			e.preventDefault();
			error_free= false;
		}

		else if($('#quantity').val().trim()=="" || parseInt($('#quantity').val())==0){

			$("#quantity").css(error_css);
			$("#quantity").focus();
			e.preventDefault();
			error_free= false;
		}
 else if($('#indent_date').val().trim()==""){

			$("#indent_date").css(error_css);
			$("#indent_date").focus();
			e.preventDefault();
			error_free= false;
		}

		 else if($('#indent_num').val().trim==""){

			$("#indent_num").css(error_css);
			//$("#indent_num").focus();
			e.preventDefault();
			error_free= false;
		}
if (!error_free){
		e.preventDefault(); 
		return error_free;
	}
	else{
		
		return true;

	}

	});

 $("#AddStock_indent_form").submit(function() {
            $(this).submit(function() {
                return false;
            });
            return true;
        });

							$(document).ready(function(){
								
								<?php foreach ($filtered_arr as $key=>$value_temp) {
									$elemnt_id= ".".$value_temp['id_mst_drugs'];?> 
									$("<?php echo $elemnt_id; ?>").show();
									$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").click(function(){
										if($("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").hasClass('fa-minus')){
											$("<?php echo $elemnt_id; ?>").hide();
											$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-minus').addClass("fa-plus");
										}
										else{
											$("<?php echo $elemnt_id; ?>").show();
											$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").removeClass('fa-plus').addClass("fa-minus");
										}
										
									});
									$("<?php echo '#head'.$value_temp['id_mst_drugs'] ?>").css('cursor', 'pointer');
								<?php }?>
							});

							
			function delete_indent_data(inventory_id){
									
									   		 $( "#dialog-confirm" ).dialog({
									   		open : function() {
										    $("#dialog-confirm").closest("div[role='dialog']").css({top:100,left:'40%'});
											}, 	
									      resizable: false,
									      height: "auto",
									      width: 400,
									      modal: true,
									      buttons: {
									        "Cancel request": function() {
									          window.location.replace('<?php echo base_url(); ?>Inventory/delete_receipt/'+inventory_id+'/I');

									        },
									        "Don't Cancel request": function() {
									          $( this ).dialog( "close" );
									          e.preventDefault();
									        }
									      }
									 			 });
								};				
						</script>


