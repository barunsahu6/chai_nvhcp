<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('pages/includes/header'); ?>
<?php $this->load->view('pages/includes/menubar'); ?>
<?php $this->load->view('inventory/components/'.$subview); ?>

<!-- multipurpose modal -->
<style>
    
    #dialog-confirm{
        display: none;
    }
    .ui-widget-overlay {
    background: #868686;
    opacity: .4;
    filter: Alpha(Opacity=50);
}
.ui-dialog .ui-dialog-buttonpane button {
    padding: 10px;
}
.ui-dialog .ui-dialog-titlebar-close {
   display: none;
}
option{
        background: #f3f3f3;
        color:#000;
    }
   #modal_header{
    white-space: pre-wrap;
   }
   .table-bordered>tbody>tr>th{
    vertical-align: top;
   text-align: center;
  }
</style>
<?php $error = $this->session->flashdata('error'); ?>
<div class="modal" data-easein="bounceIn" tabindex="-1" role="dialog" id="multipurpose_modal">
	<div class="modal-dialog" role="document" style="position: relative; top : 30vh;">
		<div class="modal-content">
			<div class="row">
				<!-- <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-xs-8 col-xs-offset-2"> -->
				<div class="col-lg-12">
					<div class="modal-header">
						<button class="btn btn-warning btn-block form_buttons" href="" style="color: white;padding-bottom: 15px;padding-top: 15px;" data-dismiss="modal" aria-label="Close" id="modal_header"><?php echo isset($error)?$error['header']:'Information'; ?></button>

					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12 text-center">
								<i id="modal_text"><?php echo isset($error)?$error['message']:'Error Text'; ?></i>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<div id="dialog-confirm" title="Delete Stock Indent">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span> Your request will be cancelled. Are you sure?</p>
</div>
        
<script>
	$(".modal").each(function(l){$(this).on("show.bs.modal",function(l){var o=$(this).attr("data-easein");"shake"==o?$(".modal-dialog").velocity("callout."+o):"pulse"==o?$(".modal-dialog").velocity("callout."+o):"tada"==o?$(".modal-dialog").velocity("callout."+o):"flash"==o?$(".modal-dialog").velocity("callout."+o):"bounce"==o?$(".modal-dialog").velocity("callout."+o):"swing"==o?$(".modal-dialog").velocity("callout."+o):$(".modal-dialog").velocity("transition."+o)})});
</script>
<script type="text/javascript">
	$('.hasCal').each(function() {
        ddate=$(this).val();
        var expdate = new Date().getFullYear()+5;
        yRange="1990:"+ expdate;
        yr=$(this).attr('year-range');
        if(yr){
            yRange=yr;
        }
        
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
            changeMonth: true,
            yearRange: yRange,
            //maxDate: 0,
        });
        $(this).attr('placeholder','dd-mm-yy');
        if($(this).attr('noweekend')){
            $(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
        }
        $(this).datepicker( "setDate", ddate);
    });
$('.hasCal_Receipt').each(function() {
        ddate=$(this).val();
        var expdate = new Date().getFullYear();
        yRange="1990:"+ expdate;
        yr=$(this).attr('year-range');
        if(yr){
            yRange=yr;
        }
        
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
            changeMonth: true,
            yearRange: yRange,
            maxDate: 0,
        });
        $(this).attr('placeholder','dd-mm-yy');
        if($(this).attr('noweekend')){
            $(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
        }
        $(this).datepicker( "setDate", ddate);
    });
$('.hasCalreport').each(function() {
        ddate=$(this).val();
        yRange="2018:"+new Date().getFullYear();

        yr=$(this).attr('year-range');
        if(yr){
            yRange=yr;
        }
        
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
            changeMonth: true,
            yearRange: yRange,
            minDate:new Date(2018, 3, 1),
            maxDate: 0,
        });
        $(this).attr('placeholder','dd-mm-yy');
        if($(this).attr('noweekend')){
            $(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
        }
        $(this).datepicker( "setDate", ddate);
    });
$(document).ready(function () {
    //Disable cut copy paste
    $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });
  
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
   var startval=$('#startdate').val();
        var endval=$('#enddate').val();
         $('#startdate,#enddate').change(function() {
    var startdate = $('#startdate').datepicker('getDate');
    var enddate = $('#enddate').datepicker('getDate');
    if ( startdate > enddate && startdate!=null && enddate!=null) { 
      $("#modal_header").text("From Date cannot be before To Date");
      $("#modal_text").text("Please check Dates");
      $(this).val('');
      if ($('#startdate').val()=='') {
        $('#startdate').val(startval);
      }
      else if ($('#enddate').val()=='') {
        $('#enddate').val(endval);
      }
      $("#multipurpose_modal").modal("show");
      return false;
    }
    else{
      return true;
    }
  });
         //valid_min_date();
         $('#year').change(function() {
             var year_arr=$('#year').val().split('-');
           
            
            if(new Date().getFullYear()!=year_arr[0]){
                 $('#startdate,#enddate').datepicker( "option", "minDate",new Date(year_arr[0], 3, 1));
                 $('#startdate,#enddate').datepicker( "option", "maxDate",new Date(year_arr[1], 2, 31));
                  $('#startdate').val('01-04-'+year_arr[0]);
                 $('#enddate').val('31-03-'+year_arr[1]);
            }
            else{
                $('#startdate').val('<?php echo date("01-04-Y"); ?>');
                 $('#enddate').val('<?php echo date("d-m-Y");?>');
            }
         });
     
        $("textarea").prop('maxlength', '100');
      
});

</script>
<?php $this->load->view('admin/includes/footer'); ?>