<?php
$userdata = $this->session->userdata('loginData');
?>

<style>
.navbar-nav > li:hover
{
  background-color: white !important;
  color: black !important;
}

.navbar-inverse .navbar-nav > li > a:hover {
    color: black;
    background-color: white;
}

.active
{
   background-color: white !important;
   color: black !important;
}
</style>
<div class="row" style="margin-top: 4px;">
  <nav class="navbar navbar-inverse fixed-menu" style="border: none;">
    <div class="container-fluid" style="background-color: black;">
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
          <li class="<?php echo ($this->uri->segment(1) == 'dashboard')?'active':''; ?>"><a href="<?php echo site_url('dashboard');?>">Dashboard </a></li>
          <li class="<?php echo ($this->uri->segment(1) == 'reports' && $this->uri->segment(2) == 'monthly_report')?'active':''; ?>"><a href="<?php echo site_url('reports/monthly_report')?>">Monthly Report</a></li>

          <!-- <li class="<?php echo ($this->uri->segment(1) == 'patientinfo')?'active':''; ?>"><a href="<?php echo site_url('patientinfo')?>">Patient Information</a></li> -->

           <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo 'Patient Information'; ?> <span class="caret" ></span></a>
            <ul class="dropdown-menu" style="width : 100%;">
              <li><a href="<?php echo site_url('patientinfo')?>?p=1">Patient Registration</a></li>
              <li><a href="<?php echo site_url('patientinfo')?>?p=2">Test and Result</a></li>
              <li><a href="<?php echo site_url('patientinfo')?>?p=3">Treatment </a></li>
            </ul>
          </li>


        </ul>
        <ul class="nav navbar-nav navbar-right">
          <?php
             if($userdata->id_tblusers == 1) 
             {
              ?>
                    <li class="<?php echo ($this->uri->segment(1) == 'dashboard')?'active':''; ?>"><a href="<?php echo site_url('Admin');?>">login-log</a></li>
                   <?php } ?>
          <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo ucfirst($userdata->Operator_Name); ?> <span class="caret" style="color:#000;"></span></a>
            <ul class="dropdown-menu" style="width : 100%;">
              <li><a href="<?php echo site_url('login/logout');?>">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</div>