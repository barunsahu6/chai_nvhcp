<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>User Portal</title>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/bootstrap-select.min.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/styles_chai.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/datatable.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo site_url('common_libs');?>/css/jquery-ui.min.css"/>
  <script src="<?php echo site_url('common_libs');?>/js/jquery.min.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/jquery-ui.min.js"></script>
  <script src="<?php echo site_url('application');?>/third_party/js/datatable.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/charts.min.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/chartsjs-plugin-deferred.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/bootstrap.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/velocity.min.js"></script>
  <script src="<?php echo site_url('common_libs');?>/js/velocity.ui.min.js"></script>
  
  <style>
  .header_image
  {
    height: 85px;
    width: auto;
  }
  .ui-datepicker .ui-datepicker-title select {
    font-weight: normal !important;
    color: #333 !important;
    font-size: 12px;
  }
  input.dateInpt {
    background: #fff url(<?php echo base_url(); ?>assets/img/calendar.gif) 98% 5px no-repeat;
    padding-right: 24px;
}

</style>
</head>
<script>
  $(document)
  .ajaxStart(function () {
    $("#loading_gif").show();
  })
  .ajaxStop(function () {
    $("#loading_gif").hide();
  });

  $(document).ready(function(){
    $("#loading_gif").hide();
  });
</script>

<body id="top">
  <img id="loading_gif" src="<?php echo site_url('application');?>/third_party/images/spinner.gif" style="position: fixed; top : 40vh; left : 45vw; height: 100px; width: auto; z-index : 999;" alt="loading_gif" />
  <div class="container-fluid">

    <div class="row">
      <div class="col-md-2 col-md-offset-2 text-center col-xs-3">
        <img class="header_image" src="<?php echo base_url('application/third_party');?>/images/logo_bw.png" />
      </div>
      <div class="col-md-4 text-center col-xs-5">
        <h3 class="text-center">National Viral Hepatitis Control Program</h3>
      </div>
      <div class="col-md-2 text-center col-xs-3">
        <img class="header_image" src="<?php echo site_url('application/third_party');?>/images/moh_goi_logo.png" />
      </div>
    </div>