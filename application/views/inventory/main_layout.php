<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('pages/includes/header'); ?>
<?php $this->load->view('pages/includes/menubar'); ?>
<?php $this->load->view('inventory/components/'.$subview); ?>

<!-- multipurpose modal -->
<style>
  .dataTables_wrapper .dataTables_paginate .paginate_button {
    
    padding: 0px 0px 0px 1px;
   
    text-decoration: none !important; 
    cursor: pointer; 
 
}

  .model-lg{
    width: 87% !important;
  }
.active{
  background-color: #fff;
}
.pagination>.active>a:hover{
background-color: #337ab7;
color: #fff;
}
.dataTables_filter input { width: 50px; }
  a{
    cursor: pointer;
  }
  .dataTables_filter {
display: none;
}
    #bottom_note,#info_text{
      padding-left: 15%;
      padding-right: 15%;
    }
    #dialog-confirm{
        display: none;
    }
    .ui-widget-overlay {
    background: #868686;
    opacity: .4;
    filter: Alpha(Opacity=50);
}
.ui-dialog .ui-dialog-buttonpane button {
    padding: 10px;
}
.ui-dialog .ui-dialog-titlebar-close {
   display: none;
}
option{
        background: #f3f3f3;
        color:#000;
    }
   #modal_header{
    white-space: pre-wrap;
   }
   .table-bordered>tbody>tr>th{
    vertical-align: top;
   text-align: center;
  }
  .left_nav_bar{
 padding-top: 0;
 padding-left: 7px;
 padding-right: 0px
 padding-bottom:0px;
  }
  dl dt{
    display: inline-block;
    width: 20px;
    height: 20px;
    vertical-align: middle;
}
dl dd{
    display: inline-block;
    margin: 0px 10px;
    padding-bottom: 0;
    vertical-align: middle;
}
dl dt.red,.red_status{
    background: #e9635e;
}
dl dt.green,.green_status{
    background: #87c830;
}
dl dt.yellow,.yellow_status{
    background: #ffd400;
}
dl dt.purple,.purple_status{
    background: #b19cd9;
}
dl dt.orange,.orange_status{
    background: #fe7e0f;
}
dl dt.darkYellow_status,.darkYellow_status{
    background: #ef9b0f;
}
dl dt.darkBlue_status,.darkBlue_status{
    background: #218C8D;
}
dl dt.manual,.manual{
    background: #2460A7FF;
}
dl dt.auto,.auto{
    background: #643E46FF;
}
dl dt.auto_pending,.auto_pending{
    background: #643E46FF;
}
input: required: invalid {
    outline: none;
    border: none;
}
.item{
    width:auto;
    text-align: left;
    word-break: normal;
  }
  @media (min-width: 768px) and (max-width: 1400px) {
  .set_margin{
    width: 83.1%;
    }
    

}
  @media (min-width: 1920px ) {
   
    .left_nav_bar{
      width: 12.666667%;
    }
.set_margin{
    width: 86.5%;
    }
}
.tooltip {
  position: relative;
  display: contents;
  border-bottom: 1px dotted black;
  font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
    color: blue;
    cursor: pointer;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: 120px;
  background-color: black;
  color: #fff;
  text-align: center;
  border-radius: 6px;
  padding: 5px 0;
  
  /* Position the tooltip */
  position: relative;
  z-index: 1;
  top: 100%;
  left: 50%;
  margin-left: -60px;
  display: inline-grid;
  z-index: 999;
  font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 14px;
    text-decoration: none;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}

</style>
<?php $error = $this->session->flashdata('error'); ?>
<div class="modal" data-easein="bounceIn" tabindex="-1" role="dialog" id="multipurpose_modal">
	<div class="modal-dialog" role="document" style="position: relative; top : 30vh;">
		<div class="modal-content">
			<div class="row">
				<!-- <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-xs-8 col-xs-offset-2"> -->
				<div class="col-lg-12">
					<div class="modal-header">
						<button class="btn btn-warning btn-block form_buttons" href="" style="color: white;padding-bottom: 15px;padding-top: 15px;" data-dismiss="modal" aria-label="Close" id="modal_header"><?php echo isset($error)?$error['header']:'Information'; ?></button>

					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12 text-center">
								<i id="modal_text"><?php echo isset($error)?$error['message']:'Error Text'; ?></i>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<div class="modal" data-easein="bounceIn" tabindex="-1" role="dialog" id="copy_modal">
  <div class="modal-dialog" role="document" style="position: relative; top : 10vh;">
    <div class="modal-content">
      <div class="row">
        <!-- <div class="col-lg-4 col-lg-offset-4 col-md-offset-4 col-md-4 col-xs-8 col-xs-offset-2"> -->
        <div class="col-lg-12">
          <div class="modal-header">
           <h4>Consignee Details</h4>

          </div>
          <div class="modal-body" id="copy_div">
            <div class="row">
              <div class="col-md-12 text-center">
                <div class="row">
                  <div class="col-md-6" style="text-align:left;"><label style="text-align:left;">Nodal Person:</label></div>
                  <div class="col-md-6" style="text-align:left;"><label id="nodal_person"></label></div>
                </div>
                <div class="row">
                  <div class="col-md-6" style="text-align:left;"><label style="text-align:left;">Contact Number:</label></div>
                  <div class="col-md-6" style="text-align:left;"><label id="phone1"></label></div>
                </div>
                 <div class="row">
                  <div class="col-md-6" style="text-align:left;"><label style="text-align:left;">Email Id:</label></div>
                  <div class="col-md-6" style="text-align:left;"><label id="email"></label></div>
                </div>
               <div class="row">
                  <div class="col-md-6" style="text-align:left;"><label style="text-align:left;">Institution:</label></div>
                  <div class="col-md-6" style="text-align:left;"><label></label></div>
                </div>
                 <div class="row">
                  <div class="col-md-6" style="text-align:left;"><label style="text-align:left;">State:</label></div>
                  <div class="col-md-6" style="text-align:left;"><label id="StateName"></label></div>
                </div>
               <div class="row">
                  <div class="col-md-6" style="text-align:left;"><label style="text-align:left;">District:</label></div>
                  <div class="col-md-6" style="text-align:left;"><label id="DistrictName"></label></div>
                </div>
               <div class="row">
                  <div class="col-md-6" style="text-align:left;"><label style="text-align:left;">Address line 1:</label></div>
                  <div class="col-md-6" style="text-align:left;"><label id="AddressLine1"></label></div>
                </div>
               <div class="row">
                  <div class="col-md-6" style="text-align:left;"><label style="text-align:left;">Address line 2:</label></div>
                  <div class="col-md-6" style="text-align:left;"><label id="AddressLine2"></label></div>
                </div>
              <div class="row">
                  <div class="col-md-6" style="text-align:left;"><label style="text-align:left;">Pincode:</label></div>
                  <div class="col-md-6" style="text-align:left;"><label id="PinCode"></label></div>
                </div>
              </div>
            </div>
          </div>
           <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="copy_to_clipboard();">Copy to clipboard</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
        </div>
      </div>

    </div>
  </div>
</div>
<div id="dialog-confirm" title="Delete Stock Indent">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span> <span id="sub_head_confirm">Your request will be cancelled. Are you sure?</span></p>
</div>
   <textarea id="myInput" style="display: none;"></textarea>      
<script>
	$(".modal").each(function(l){$(this).on("show.bs.modal",function(l){var o=$(this).attr("data-easein");"shake"==o?$(".modal-dialog").velocity("callout."+o):"pulse"==o?$(".modal-dialog").velocity("callout."+o):"tada"==o?$(".modal-dialog").velocity("callout."+o):"flash"==o?$(".modal-dialog").velocity("callout."+o):"bounce"==o?$(".modal-dialog").velocity("callout."+o):"swing"==o?$(".modal-dialog").velocity("callout."+o):$(".modal-dialog").velocity("transition."+o)})});
</script>
<script type="text/javascript">
	$('.hasCal').each(function() {
        ddate=$(this).val();
        var expdate = new Date().getFullYear()+5;
        yRange="1990:"+ expdate;
        yr=$(this).attr('year-range');
        if(yr){
            yRange=yr;
        }
        
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
            changeMonth: true,
            yearRange: yRange,
            //maxDate: 0,
        });
        $(this).attr('placeholder','dd-mm-yy');
        if($(this).attr('noweekend')){
            $(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
        }
        $(this).datepicker( "setDate", ddate);
    });
$('.hasCal_Receipt').each(function() {
        ddate=$(this).val();
        var expdate = new Date().getFullYear();
        yRange="1990:"+ expdate;
        yr=$(this).attr('year-range');
        if(yr){
            yRange=yr;
        }
        
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
            changeMonth: true,
            yearRange: yRange,
            maxDate: 0,
        });
        $(this).attr('placeholder','dd-mm-yy');
        if($(this).attr('noweekend')){
            $(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
        }
        $(this).datepicker( "setDate", ddate);
    });
$('.hasCalreport').each(function() {
        ddate=$(this).val();
        yRange="2018:"+new Date().getFullYear();

        yr=$(this).attr('year-range');
        if(yr){
            yRange=yr;
        }
        
        $(this).datepicker({
            dateFormat: "dd-mm-yy",
            changeYear: true,
            changeMonth: true,
            yearRange: yRange,
            minDate:new Date(2018, 3, 1),
            maxDate: 0,
        });
        $(this).attr('placeholder','dd-mm-yy');
        if($(this).attr('noweekend')){
            $(this).datepicker( "option", "beforeShowDay", jQuery.datepicker.noWeekends);
        }
        $(this).datepicker( "setDate", ddate);
    });
$(document).ready(function () {
    //Disable cut copy paste
   /* $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
    });*/
  
    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });
     $(".legends").hide();
   var startval=$('#startdate').val();
        var endval=$('#enddate').val();
         $('#startdate,#enddate').change(function() {
    var startdate = $('#startdate').datepicker('getDate');
    var enddate = $('#enddate').datepicker('getDate');
    if ( startdate > enddate && startdate!=null && enddate!=null) { 
      $("#modal_header").text("From Date cannot be before To Date");
      $("#modal_text").text("Please check Dates");
      $(this).val('');
      if ($('#startdate').val()=='') {
        $('#startdate').val(startval);
      }
      else if ($('#enddate').val()=='') {
        $('#enddate').val(endval);
      }
      $("#multipurpose_modal").modal("show");
      return false;
    }
    else{
      return true;
    }
  });
         //valid_min_date();
         $('.show_hide_legend').click(function() {
             $(".legends").toggle("slide", { direction: "right" }, 1000);
            /* if($('.legends').css("display")=='none' || $('.legends').css("visibility")=='hidden')
             {
              alert('hi');
             }
             else{
              alert('bye');
             }*/
           });
             $('#year').change(function() {
             var year_arr=$('#year').val().split('-');
           
            
            if(new Date().getFullYear()!=year_arr[0]){
                 $('#startdate,#enddate').datepicker( "option", "minDate",new Date(year_arr[0], 3, 1));
                 $('#startdate,#enddate').datepicker( "option", "maxDate",new Date(year_arr[1], 2, 31));
                  $('#startdate').val('01-04-'+year_arr[0]);
                 $('#enddate').val('31-03-'+year_arr[1]);
            }
            else{
                $('#startdate').val('<?php echo date("01-04-Y"); ?>');
                 $('#enddate').val('<?php echo date("d-m-Y");?>');
            }
         });
     
        $("textarea").prop('maxlength', '100');
      
});
function get_cons_details(facility)
{
   $.ajax({
                    url : "<?php echo site_url('inventory/get_cons_details');?>",
                    method : "GET",
                    data : {facility_id: facility,<?php echo $this->security->get_csrf_token_name() ?>: '<?php echo $this->security->get_csrf_hash() ?>'},
                    //async : true,
                   //dataType : 'json',
                    success: function(data){
                         //console.log(data);
                        var html = '';
                        var i;
                        var returned = JSON.parse(data);
                        var nodal_person = returned.data.map(function(e) {
                      return e.nodal_person;
                    }); 
                        var phone1 = returned.data.map(function(e) {
                      return e.Phone1;
                    }); 
                    var email = returned.data.map(function(e) {
                      return e.email;
                    });
                     var PinCode = returned.data.map(function(e) {
                      return e.PinCode;
                    }); 
                     var AddressLine1 = returned.data.map(function(e) {
                      return e.AddressLine1;
                    });
                     var AddressLine2 = returned.data.map(function(e) {
                      return e.AddressLine2;
                    });
                       var DistrictName = returned.data.map(function(e) {
                      return e.DistrictName;
                    });
                     var StateName = returned.data.map(function(e) {
                      return e.StateName;
                    });
                     $('#PinCode').text(PinCode);
                     $('#AddressLine1').text(AddressLine1);
                     $('#AddressLine2').text(AddressLine2);
                     $('#DistrictName').text(DistrictName);
                     $('#StateName').text(StateName);
                     $('#email').text(email);
                     $('#phone1').text(phone1);
                     $('#nodal_person').text(nodal_person);
                      $('#copy_modal').modal('show');
                      }

                    });
}
function copy_to_clipboard() {
  var str = document.querySelector("#copy_div").textContent;
$('#myInput').val(str);
 var textarea = document.createElement('textarea');
  textarea.textContent = str.toString();
  document.body.appendChild(textarea);

  var selection = document.getSelection();
  var range = document.createRange();
//  range.selectNodeContents(textarea);
  range.selectNode(textarea);
  selection.removeAllRanges();
  selection.addRange(range);

  console.log('copy success', document.execCommand('copy'));
  selection.removeAllRanges();

  document.body.removeChild(textarea);
  alert("Copied the text: " );
}</script>
<?php $this->load->view('admin/includes/footer'); ?>