<!-- <?php // echo "<pre>"; print_r($facility_calloutput); exit; ?> -->
	<style>
	.patient_list_row:hover
	{
		cursor: pointer;
	}
	.panel-heading:hover
	{
		cursor: pointer;
	}
	.panel-title
	{
		color : white;
	}
	.checkbox
	{
		margin-top : 0;
		display : inline;
	}
	input[type="checkbox"]
	{
		margin: 1px 0 0;
	}
	input[type="radio"]
	{
		margin: 1px 0 0;
	}
	.navbar-right .dropdown-menu {
		right: -2px;
		left: auto;
	}
	.radio
	{
		display: inline;
		margin-right: 20px;
	}
	.visit_circle
	{
		width : 35px;
		height: 35px;
		border-radius: 50%;
		cursor: pointer;
	}
	.visit_circle p
	{
		color: white;
		font-size: 10px;
		padding-top: 11px;
	}
	.green
	{
		background-color: green;
	}
	.gray
	{
		background-color: gray;
	}
	.missed_app_hidden_trs:hover{
		cursor: pointer;
	}
	.follow_up_hidden_trs:hover{
		cursor: pointer;
	}
	.treatment_completed_hidden_trs:hover{
		cursor: pointer;
	}

	.excel_icon
	{
		width: 30px;
		height: auto;
		cursor: pointer;
	}
	canvas
	{
		background-color: white;
	}
	#googft-mapCanvas {
		height: 400px;
		margin: 0;
		padding: 0;
		width: 500px;
	}
</style>
<br>
<div class="row">
	<div class="col-xs-12 text-center">
		<code>Overall Dashboard</code>
	</div>
</div>
<br>
<div class="row">
	<form action="" method="post" id="filter_form">
		<div class="col-lg-1 col-md-1 col-lg-offset-1">
			<label for="facility">Hospital</label>
		</div>
		<div class="col-lg-2 col-md-2">
			<select name="facility" id="facility" class="selectpicker form-control">
				<?php 
				$loginData = $this->session->userdata('loginData');
				if($loginData->id_mstfacility == 0 )
				{
					echo '<option value="0">All</option>';
				}
				foreach ($facilities as $facility) {
					$selected = "";
					if($facilityid == $facility->id_mstfacility)
					{
						$selected = "selected";
					}
					else
					{
						$selected = "";
					}
					echo "<option ".$selected." value='".$facility->id_mstfacility."'>".$facility->hospital."</option>";
				}
				?>
			</select>
		</div>

		<div class="col-lg-1 col-md-1" style="padding-right: 0;">
			<label for="startdate">From Date:</label>
		</div>
		<div class="col-lg-2 col-md-2"  style="padding-right: 0;">
			<input type="date" name="startdate" id="startdate" class="form-control" value="<?php echo $start_date; ?>">
		</div>

		<div class="col-lg-1 col-md-1">
			<label for="enddate">To Date:</label>
		</div>
		<div class="col-lg-2 col-md-2"  style="padding-right: 0;">
			<input type="date" name="enddate" id="enddate" class="form-control" value="<?php echo $end_date; ?>">
		</div>

		<div class="col-lg-1 col-md-2">
			<input type="submit" name="submitbtn" id="submitbtn" class="btn btn-primary">
			<input type="hidden" name="export_type" id="export_type" value="">
			<input type="hidden" name="export_report" id="export_report" value="">
		</div>
	</form>
</div>
<br>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<h3 class="text-center">Patient Cascade</h3>
				<a href="<?php echo site_url(); ?>admin/chartreports/cascade" class="list_page_icon_big"><i class="fa fa-2x fa-list-ul"></i></a>
				<a id="cascadeChartLink" download="cascadeChart.png" class="export_button" style="top : 20px;"><i class="fa fa-2x fa-download" id="downloadCascade"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12" style="padding : 20px">
				<canvas id="cascadeChart" width="100vw" height="29vh"></canvas>
			</div>
		</div>
	</div>
</div>
<br>

<div class="row">
	<div class="col-lg-6 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title">Cirrhosis/No-Cirrhosis Report</h5>
			</div>
			<div class="panel-body">
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-md-12">
						<a href="<?php echo site_url(); ?>admin/chartreports/district_wise_cirrhosis_no_cirrhosis" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="cirrhosisChartLink" download="cirrhosisChart.png" class="export_button"><i class="fa fa-download" id="downloadCascade"></i></a>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12" style="padding : 20px">
						<canvas id="cirrhosisChart" width="50vw" height="20vh"></canvas>
					</div>
				</div>

			</div>
		</div>
	</div>
	<div class="col-lg-6 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title">Genotype Report</h5>
			</div>
			<div class="panel-body">
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-md-12">
						<a href="<?php echo site_url(); ?>admin/chartreports/district_wise_genotype" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="genotypeChartLink" download="genotypeChart.png" class="export_button"><i class="fa fa-download" id="downloadCascade"></i></a>
					</div>
				</div>
				<div class="row">

					<div class="col-lg-12 col-md-12" style="padding : 20px">
						<canvas id="genotypeChart" width="50vw" height="20vh"></canvas>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-6 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title">Age Wise Report</h5>
			</div>
			<div class="panel-body">
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-md-12">
						<a href="<?php echo site_url(); ?>admin/chartreports/district_wise_age" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="ageChartLink" download="ageChart.png" class="export_button"><i class="fa fa-download" id="downloadCascade"></i></a>
					</div>
				</div>
				<div class="row">

					<div class="col-lg-12 col-md-12" style="padding : 20px">
						<canvas id="ageChart" width="100vw" height="62vh"></canvas>
					</div>	

				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title">Gender Area Report</h5>
			</div>
			<div class="panel-body">
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-md-12">
						<a href="<?php echo site_url(); ?>admin/chartreports/district_wise_gender" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="genderAreaChartLink" download="genderAreaChart.png" class="export_button"><i class="fa fa-download" id="downloadCascade"></i></a>
					</div>
				</div>
				<div class="row">

					<div class="col-lg-12 col-md-12" style="padding : 20px">
						<canvas id="genderAreaChart" width="100vw" height="62vh"></canvas>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>


<div class="panel panel-default">
	<div class="panel-heading">
		<h5 class="panel-title">Treatment Completed Summary</h5>
	</div>
	<div class="panel-body">
		<table class="table table-striped table-bordered table-vmiddle table-hover">
			<thead>	
				<th style="width: 35px;"></th>
				<th class="text-center" style="width: 300px;">Hospital</th>
				<th class="text-center"># patients Completed Treatment</th>
				<th class="text-center"># patients Due for SVR</th>
				<th class="text-center"># patients with SVR Results</th>
				<th class="text-center"># patients with SVR Achieved</th>
				<?php 
				if($loginData->id_mstfacility == 0 && $facilityid == 0)
				{
					echo '<th class="text-center" id="excel_treatment_completed"><img class="excel_icon" src="'.site_url().'common_libs/images/excel_icon.jpeg" alt="excel_download_logo"></th>';
				}
				?>
			</thead>
			<tbody>
				<?php 
				echo '<tr>
				<td></td>
				<td class="text-center">'.$agg_treatment_completed->hospital.'</td>
				<td class="text-center">'.$agg_treatment_completed->CompletedTreatment.'</td>
				<td class="text-center">'.$agg_due_svr[0]->count.'</td>
				<td class="text-center">'.$cumulative_svr[0]->count.'</td>
				<td class="text-center">'.$cumulative_svr[1]->count.'</td>
				';
				if($loginData->id_mstfacility == 0 && $facilityid == 0)
				{
					echo '<th class="text-center" id="show_treatment_completed_district_table"><i class="fa fa-plus" style="font-size: 20px;
					color: rgb(128,128,128);" id="show_treatment_completed_button" aria-hidden="true"></i></th></tr>';
				}
				else{
					echo '</tr>';
				}
				if($loginData->id_mstfacility == 0 && $facilityid == 0)
				{
					$counter = 0;
					foreach ($res_treatment_completed_all_district as $row) {

						echo '<tr class="treatment_completed_hidden_trs">
						<td class="text-center">'.$row->id_mstfacility.'</td>
						<td class="text-center">'.$row->hospital.'</td>
						<td class="text-center">'.$row->CompletedTreatment.'</td>
						<td class="text-center">'.$all_district_due_svr[$counter]->count.'</td>
						<td class="text-center">'.$res_lal_svr_all_districts[$counter]->count.'</td>
						<td class="text-center">'.$res_lal_svr_all_districts[$counter+25]->count.'</td>';

						if($loginData->id_mstfacility == 0)
						{
							echo '<td class="text-center"></td>';
						}
						echo '</tr>';
						$counter++;
					}
				}
				?>

			</tbody>
		</table>
		<?php if($loginData->id_mstfacility == 0){ ?>
		<table class="table table-striped table-bordered table-vmiddle table-hover" id="aggregate_treatment_completed_district_table">
			<tbody class="text-center">
				<?php 

				$site_url = site_url();
				
				?>
			</tbody>
		</table>
		<?php } ?>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h5 class="panel-title">Missed Appointment Summary</h5>
	</div>
	<div class="panel-body">
		<table class="table table-striped table-bordered table-vmiddle table-hover">
			<thead>
				<th style="width: 35px;"></th>
				<th class="text-center" style="width: 300px;">Hospital</th>
				<th class="text-center"># patients for follow up visits</th>
				<th class="text-center"># patients for SVR</th>
				<th class="text-center"># patients for treatment initiation</th>
				<th class="text-center"># patients for Confirmatory RNA</th>

				<?php 
				if($loginData->id_mstfacility == 0 && $facilityid == 0)
				{
					echo '<th class="text-center" id="excel_missed_appointment"><img class="excel_icon" src="'.site_url().'common_libs/images/excel_icon.jpeg" alt="excel_download_logo"></th>';
				}
				?>
			</thead>
			<tbody>
				<?php 
				echo '<tr>	
				<td></td>
				<td class="text-center">'.$agg_missed_app->hospital.'</td>
				<td class="text-center">'.$agg_missed_app->Followup.'</td>
				<td class="text-center">'.$agg_missed_app->SVR.'</td>
				<td class="text-center">'.$agg_missed_app->TreatmentInitiation.'</td>
				<td class="text-center">'.$agg_missed_app->ConfirmatoryPending.'</td>';
				if($loginData->id_mstfacility == 0 && $facilityid == 0)
				{
					echo '<th class="text-center" id="show_missed_appointment_district_table"><i class="fa fa-plus" style="font-size: 20px;
					color: rgb(128,128,128);" id="show_missed_appointment_button" aria-hidden="true"></i></th></tr>';
				}
				else{
					echo '</tr>';
				}
				?>
				<?php 
				if($loginData->id_mstfacility == 0 && $facilityid == 0)
				{
					foreach ($res_aggregate_missed_all_district as $row) {

						echo '<tr class="missed_app_hidden_trs">
						<td class="text-center">'.$row->id_mstfacility.'</td>
						<td class="text-center">'.$row->hospital.'</td>
						<td class="text-center">'.$row->Followup.'</td>
						<td class="text-center">'.$row->SVR.'</td>
						<td class="text-center">'.$row->TreatmentInitiation.'</td>
						<td class="text-center">'.$row->ConfirmatoryPending.'</td>';

						if($loginData->id_mstfacility == 0)
						{
							echo '<td class="text-center"></td>';
						}
						echo '</tr>';
					}
				}
				?>
			</tbody>
		</table>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h5 class="panel-title">Loss To Follow Up Summary</h5>
	</div>
	<div class="panel-body">
		<table class="table table-striped table-bordered table-vmiddle table-hover">
			<thead>
				<th style="width: 35px;"></th>
				<th class="text-center" style="width: 300px;">Hospital</th>
				<th class="text-center"># patients for follow up visits</th>
				<th class="text-center"># patients for SVR</th>
				<th class="text-center"># patients for treatment initiation</th>
				<th class="text-center"># patients for Confirmatory RNA</th>

				<?php 
				if($loginData->id_mstfacility == 0 && $facilityid == 0)
				{
					echo '<th class="text-center" id="excel_loss_to_followup"><img class="excel_icon" src="'.site_url().'common_libs/images/excel_icon.jpeg" alt="excel_download_logo"></th>';
				}
				?>
			</thead>
			<tbody>
				<?php 
				echo '<tr>	
				<td></td>
				<td class="text-center">'.$agg_loss_to_followup->hospital.'</td>
				<td class="text-center">'.$agg_loss_to_followup->Followup.'</td>
				<td class="text-center">'.$agg_loss_to_followup->SVR.'</td>
				<td class="text-center">'.$agg_loss_to_followup->TreatmentInitiation.'</td>
				<td class="text-center">'.$agg_loss_to_followup->ConfirmatoryPending.'</td>';
				if($loginData->id_mstfacility == 0 && $facilityid == 0)
				{
					echo '<th class="text-center" id="show_loss_to_followup_district_table"><i class="fa fa-plus" style="font-size: 20px;
					color: rgb(128,128,128);" id="show_loss_to_followup_button" aria-hidden="true"></i></th></tr>';
				}
				else{
					echo '</tr>';
				}
				?>
				<?php 
				if($loginData->id_mstfacility == 0 && $facilityid == 0)
				{
					foreach ($res_aggregate_loss_to_followup_all_district as $row) {

						echo '<tr class="loss_to_followup_hidden_trs">
						<td class="text-center">'.$row->id_mstfacility.'</td>
						<td class="text-center">'.$row->hospital.'</td>
						<td class="text-center">'.$row->Followup.'</td>
						<td class="text-center">'.$row->SVR.'</td>
						<td class="text-center">'.$row->TreatmentInitiation.'</td>
						<td class="text-center">'.$row->ConfirmatoryPending.'</td>';

						if($loginData->id_mstfacility == 0)
						{
							echo '<td class="text-center"></td>';
						}
						echo '</tr>';
					}
				}
				?>
			</tbody>
		</table>
	</div>
</div>



<div class="panel panel-default">
	<div class="panel-heading">
		<h5 class="panel-title">Follow Up Summary</h5>
	</div>
	<div class="panel-body">
		<table class="table table-striped table-bordered table-vmiddle table-hover">
			<thead>
				<th style="width: 35px;"></th>
				<th class="text-center" style="width: 300px;">Hospital</th>
				<th class="text-center"># patients for follow up visits</th>
				<th class="text-center"># patients for SVR</th>
				<th class="text-center"># patients for treatment initiation</th>
				<th class="text-center"># patients for Confirmatory RNA</th>

				<?php 
				if($loginData->id_mstfacility == 0 && $facilityid == 0)
				{
					echo '<th class="text-center" id="excel_follow_up"><img class="excel_icon" src="'.site_url().'common_libs/images/excel_icon.jpeg" alt="excel_download_logo"></th>';
				}
				?>
			</thead>
			<tbody>
				<?php 
				echo '<tr>
				<td></td>
				<td class="text-center">'.$agg_follow_up->hospital.'</td>
				<td class="text-center">'.$agg_follow_up->Followup.'</td>
				<td class="text-center">'.$agg_follow_up->SVR.'</td>
				<td class="text-center">'.$agg_follow_up->TreatmentInitiation.'</td>
				<td class="text-center">'.$agg_follow_up->ConfirmatoryPending.'</td>';
				if($loginData->id_mstfacility == 0 && $facilityid == 0)
				{
					echo '<th class="text-center" id="show_follow_up_district_table"><i class="fa fa-plus" style="font-size: 20px;
					color: rgb(128,128,128);" id="show_follow_up_button" aria-hidden="true"></i></th></tr>';
				}
				else{
					echo '</tr>';
				}
				if($loginData->id_mstfacility == 0 && $facilityid == 0)
				{
					foreach ($res_follow_up_all_district as $row) {

						echo '<tr class="follow_up_hidden_trs">
						<td class="text-center">'.$row->id_mstfacility.'</td>
						<td class="text-center">'.$row->hospital.'</td>
						<td class="text-center">'.$row->Followup.'</td>
						<td class="text-center">'.$row->SVR.'</td>
						<td class="text-center">'.$row->TreatmentInitiation.'</td>
						<td class="text-center">'.$row->ConfirmatoryPending.'</td>';

						if($loginData->id_mstfacility == 0)
						{
							echo '<td class="text-center"></td>';
						}
						echo '</tr>';
					}
				}
				?>

			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div class="col-lg-6 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title">Adherence</h5>
			</div>
			<div class="panel-body">
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-md-12">
						<a href="<?php echo site_url(); ?>admin/chartreports/district_wise_adherence" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="adherenceChartLink" download="adherenceChart.png" class="export_button"><i class="fa fa-download" id="downloadCascade"></i></a>
					</div>
				</div>
				<div class="row">

					<div class="col-lg-12 col-md-12" style="padding : 20px">
						<canvas id="adherenceChart" width="100vw" height="62vh"></canvas>
					</div>	

				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title">Treatment Success Ratio</h5>
			</div>
			<div class="panel-body" style="padding-bottom: 84px;">
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-md-12">
						<a href="<?php echo site_url(); ?>admin/chartreports/treatmentsuccessful" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="treatmentSuccessChartLink" download="treatmentSuccessChart.png" class="export_button"><i class="fa fa-download" id="downloadCascade"></i></a>
					</div>
				</div>
				<div class="row">

					<div class="col-lg-12 col-md-12" style="padding : 20px">
						<canvas id="treatmentSuccessChart" width="50vw" height="25vh"></canvas>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-lg-6 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title">Risk Factor</h5>
			</div>
			<div class="panel-body">
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-md-12">
						<a href="<?php echo site_url(); ?>admin/chartreports/district_wise_risk" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="riskFactorChartLink" download="riskFactorChart.png" class="export_button"><i class="fa fa-download" id="downloadCascade"></i></a>
					</div>
				</div>
				<div class="row">
					<br>
					<div class="col-lg-12 col-md-12">
						<canvas width="100vw" height="62vh" id="riskFactorChart"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title">Turn Around Time report</h5>
			</div>
			<div class="panel-body">
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-md-12">
						<a href="<?php echo site_url(); ?>admin/chartreports/tat" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="TATChartLink" download="TATChart.png" class="export_button"><i class="fa fa-download" id="downloadCascade"></i></a>
					</div>
				</div>
				<div class="row">

					<div class="col-lg-12 col-md-12" style="padding : 20px">
						<canvas id="TATChart" width="100vw" height="59vh"></canvas>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>


<div class="panel">
	<div class="panel-heading">
		<h5 class="panel-title">Trend</h5>
	</div>
	<div class="panel-body">
		<div class="row" style="margin-bottom: 10px;">
			<div class="col-xs-12 col-md-12">
				<a href="<?php echo site_url(); ?>admin/chartreports/district_wise_trend" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
				<a id="trendGraphLink" download="trendGraph.png" class="export_button"><i class="fa fa-download" id="downloadCascade"></i></a>
			</div>
		</div>
		<div class="row">
			<h3 class="text-center">Treatment Initiations by District</h3>
			<br>
			<div class="col-lg-12 col-md-12">
				<canvas width="100vw" height="25vh" id="trendGraph"></canvas>
			</div>
		</div>
	</div>
</div>
<br>
<div class="row">
	
	<div class="col-lg-6 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title">District wise map of patients initiated on treatment</h5>
			</div>
			<div class="panel-body">
				<div id="googft-mapCanvas"></div>
			</div>
		</div>
	</div>
	<div class="col-lg-6 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title">Monthly patient load</h5>
			</div>
			<div class="panel-body">
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-md-12">
						<a href="<?php echo site_url(); ?>admin/chartreports/monthly" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="monthlyChartLink" download="monthlyChart.png" class="export_button"><i class="fa fa-download" id="downloadCascade"></i></a>
					</div>
				</div>
				<div class="row">
					<br>
					<div class="col-lg-12 col-md-12">
						<canvas width="100vw" height="62vh" id="monthlyChart"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br>

<div class="row">
	<div class="col-lg-6 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title">Regimen Chart</h5>
			</div>
			<div class="panel-body">
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-md-12">
						<a href="<?php echo site_url(); ?>admin/chartreports/district_wise_regimen" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="regimenChartLink" download="regimenChart.png" class="export_button"><i class="fa fa-download" id="downloadCascade"></i></a>
					</div>
				</div>
				<div class="row">

					<div class="col-lg-12 col-md-12" style="padding : 20px">
						<canvas id="regimenChart" width="50vw" height="20vh"></canvas>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="col-lg-6 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5 class="panel-title">Occupation Report</h5>
			</div>
			<div class="panel-body">
				<div class="row" style="margin-bottom: 10px;">
					<div class="col-xs-12 col-md-12">
						<a href="<?php echo site_url(); ?>admin/chartreports/district_wise_occupation" class="list_page_icon_small"><i class="fa fa-list-ul"></i></a>
						<a id="occupationChartLink" download="occupationChart.png" class="export_button"><i class="fa fa-download" id="downloadCascade"></i></a>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12" style="padding : 20px">
						<canvas id="occupationChart" width="50vw" height="20vh"></canvas>
					</div>
				</div>

			</div>
		</div>
	</div>

</div>
<br>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<h3 class="text-center">Follow-up call outputs for interrupted patients</h3>
				<a href="<?php echo site_url(); ?>admin/chartreports/facility_cascade" class="list_page_icon_big"><i class="fa fa-2x fa-list-ul"></i></a>
				<a id="facility_cascade_link" download="FacilityCascade.png" class="export_button" style="top : 20px;"><i class="fa fa-2x fa-download" id="download_facility_cascade"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12" style="padding : 20px">
				<canvas id="FacilityCascade" width="100vw" height="29vh"></canvas>
			</div>
		</div>
	</div>
</div>
<!-- <div class="row">
	
	<div class="col-lg-12">
		<div id="candle_graph" style="height: 300px; width: 100%;"></div>
	</div>
</div>
<br> -->


<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDAe8IMsH0rGsaCxephRUAO5sLsDpQ-djA"></script>
<!-- <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script> -->
 <!-- <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data = google.visualization.arrayToDataTable([

    	<?php 
    	foreach ($candle as $row) {
    		if($row->gen == 2)
    		{
    			echo "['Gen:".$row->gen."', ".$row->min.", ".$row->q1.", ".$row->q3.",".$row->max."],";
    		}
    		else
    		{
    			echo "['Gen:".$row->gen."', ".$row->min.", ".$row->q1.", ".$row->q3.",".$row->max."],";
    		}
    	}

    	 ?>
      // ['Mon', 20, 28, 38, 45],
      // ['Tue', 31, 38, 55, 66],
      // ['Wed', 50, 55, 77, 80],
      // ['Thu', 77, 77, 66, 50],
      // ['Fri', 68, 66, 22, 15],
      // ['Fri', 68, 66, 22, 15],
      // ['Mon', 20, 28, 38, 45],
      // ['Tue', 31, 38, 55, 66],
      // ['Wed', 50, 55, 77, 80],
      // ['Thu', 77, 77, 66, 50],
      // ['Fri', 68, 66, 22, 15],
      // ['Fri', 68, 66, 22, 15],
      // ['Mon', 20, 28, 38, 45],
      // ['Tue', 31, 38, 55, 66],
      // ['Wed', 50, 55, 77, 80],
      // ['Thu', 77, 77, 66, 50],
      // ['Fri', 68, 66, 22, 15],
      // ['Fri', 68, 66, 22, 15],
      // ['Mon', 20, 28, 38, 45],
      // ['Tue', 31, 38, 55, 66],
      // ['Wed', 50, 55, 77, 80],
      // ['Thu', 77, 77, 66, 50],
      // ['Fri', 68, 66, 22, 15],
      // ['Fri', 68, 66, 22, 15],
      // ['Mon', 20, 28, 38, 45],
      // ['Tue', 31, 38, 55, 66],
      // ['Wed', 50, 55, 77, 80],
      // ['Thu', 77, 77, 66, 50],
      // ['Fri', 68, 66, 22, 15],
      // ['Fri', 68, 66, 22, 15],
      // ['Mon', 20, 28, 38, 45],
      // ['Tue', 31, 38, 55, 66],
      // ['Wed', 50, 55, 77, 80],
      // ['Thu', 77, 77, 66, 50],
      // ['Fri', 68, 66, 22, 15],
      // ['Fri', 68, 66, 22, 15],
      // Treat first row as data as well.
    ], true);

    var options = {
      legend:'none'
    };

    var chart = new google.visualization.CandlestickChart(document.getElementById('candle_graph'));

    chart.draw(data, options);
  }
</script> -->

<script>

	$(document).ready(function(){

		$(".missed_app_hidden_trs").hide();
		$(".follow_up_hidden_trs").hide();
		$(".treatment_completed_hidden_trs").hide();
		$(".loss_to_followup_hidden_trs").hide();
		drawCascadeChart();
		drawCirrhosisChart();
		drawGenotypeChart();
		drawAgeWiseChart();
		genderAreaChart();
		drawTrendChart();
		drawAdherenceChart();
		drawTreatmentSuccessChart();
		drawRiskFactorChart();
		drawTATChart();
		drawMonthlyChart();
		drawMap();
		drawRegimentypeChart();
		drawOccupationChart();
		drawFacilityCascade();

		function drawMap(){

			google.maps.event.addDomListener(window, 'load', initialize);

			Chart.plugins.register({
				beforeDraw: function(chartInstance) {
					var ctx = chartInstance.chart.ctx;
					ctx.fillStyle = "white";
					ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
				}
			});
		}

		function initialize() {

			var map_data_str = '<?php echo $map_data; ?>';			
			var map_data     = JSON.parse(map_data_str);
		// console.log(map_data);

		var green  = [];
		var blue   = [];
		var yellow = [];
		var red    = [];

		for(facility of map_data)
		{
			if(facility.patients >=0 && facility.patients < 1000)
			{
				green.push(parseInt(facility.id_mstdistrict));
			}
			else 
				if(facility.patients >=1000 && facility.patients < 2500)
				{
					blue.push(parseInt(facility.id_mstdistrict));
				}
				else 
					if(facility.patients >=2500 && facility.patients < 5000)
					{
						yellow.push(parseInt(facility.id_mstdistrict));
					}
					else 
						if(facility.patients >=5000)
						{
							red.push(parseInt(facility.id_mstdistrict));
						}
					}


					cGREEN  = "#DBE5F1";
					cBLUE   = "#5bc0de";
					cYELLOW = "#538ED5";
					cRED    = "#0F253F";

					google.maps.visualRefresh = true;
					var isMobile = (navigator.userAgent.toLowerCase().indexOf('android') > -1) ||
					(navigator.userAgent.match(/(iPod|iPhone|iPad|BlackBerry|Windows Phone|iemobile)/));
					if (isMobile) {
						var viewport = document.querySelector("meta[name=viewport]");
						viewport.setAttribute('content', 'initial-scale=1.0, user-scalable=no');
					}

					var mapDiv = document.getElementById('googft-mapCanvas');
					mapDiv.style.width = isMobile ? '100%' : '100%';
					mapDiv.style.height = isMobile ? '100%' : '500';
					var map = new google.maps.Map(mapDiv, {
						center: new google.maps.LatLng(31, 75.45),
						zoom: 7,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});

					layer = new google.maps.FusionTablesLayer({
						map: map,
						heatmap: { enabled: false },
						query: {
							select: "Shape_Area",
							from: "1Kw23GfciC669U98-ek3vYktPEakKmV8uncXfoUjv",
        // where: ""
      },
      options: {
      	styleId: 2,
      	templateId: 2,
      	styles : [
      	{
                            //                            where: "ID='" + val + "'",
                            where: "id_mstdistrict in (" + green + ")",
                            polygonOptions:
                            {
                            	fillColor: cGREEN,

                            	fillOpacity: "0.6"

                            }
                          },
                          {
                            //                            where: "ID='" + val + "'",
                            where: "id_mstdistrict in (" + blue + ")",
                            polygonOptions:
                            {
                            	fillColor: cBLUE,

                            	fillOpacity: "0.6"

                            }
                          },
                          {
                            //                            where: "ID='" + val + "'",
                            where: "id_mstdistrict in (" + yellow + ")",
                            polygonOptions:
                            {
                            	fillColor: cYELLOW,

                            	fillOpacity: "0.6"

                            }
                          },
                          {
                            //                            where: "ID='" + val + "'",
                            where: "id_mstdistrict in (" + red + ")",
                            polygonOptions:
                            {
                            	fillColor: cRED,

                            	fillOpacity: "0.6"

                            }
                          },
                          ],

                        }
                      });

					layer.addListener('click', function(e){

						$.ajax({
							url : '<?php echo site_url('admin/Ajax/getInitiationByDistrict/');?>'+e.row.id_mstdistrict.value,
							async : false,
							success : function(raw)
							{
				// console.log(raw);
				e.infoWindowHtml = raw;
			}
		});

					});

					if (isMobile) {
						var legend = document.getElementById('googft-legend');
						var legendOpenButton = document.getElementById('googft-legend-open');
						var legendCloseButton = document.getElementById('googft-legend-close');
						legend.style.display = 'none';
						legendOpenButton.style.display = 'block';
						legendCloseButton.style.display = 'block';
						legendOpenButton.onclick = function() {
							legend.style.display = 'block';
							legendOpenButton.style.display = 'none';
						}
						legendCloseButton.onclick = function() {
							legend.style.display = 'none';
							legendOpenButton.style.display = 'block';
						}
					}

					var legend = document.createElement('div');
					legend.id = 'legend';

		// ADDED
		legend.style.padding = '10px';
		legend.style.backgroundColor = 'white';
		legend.style.borderStyle = 'solid';
		legend.style.borderWidth = '1px';
		legend.style.textAlign = 'left';

		var content = [];
		content.push('<h3>Legend</h3>');
		// CHANGED
		//content.push('<p><div class="color red"></div>No</p>');
		//content.push('<p><div class="color green"></div>Yes</p>');
		content.push('<p><p style="background-color: #DBE5F1;">&nbsp;&nbsp;&nbsp;&nbsp;</p>0 - 1000</p>');
		content.push('<p><p style="background-color: #5bc0de;">&nbsp;&nbsp;&nbsp;&nbsp;</p>1000 - 2500</p>');
		content.push('<p><p style="background-color: #538ED5;">&nbsp;&nbsp;&nbsp;&nbsp;</p>2500 - 5000</p>');
		content.push('<p><p style="background-color: #0F253F;">&nbsp;&nbsp;&nbsp;&nbsp;</p>> 5000</p>');

		legend.innerHTML = content.join('');
		legend.index = 1;
		map.controls[google.maps.ControlPosition.LEFT_TOP].push(legend);

	}


	$("#show_missed_appointment_district_table").click(function(){
		if($(".missed_app_hidden_trs").css('display') == 'table-row')
		{
			$("#show_missed_appointment_button").addClass('fa-plus').removeClass('fa-minus');
			$(".missed_app_hidden_trs").fadeOut();
		}
		else{

			$(".missed_app_hidden_trs").fadeIn();
			$("#show_missed_appointment_button").addClass('fa-minus').removeClass('fa-plus');
		}
	});

	$("#show_follow_up_district_table").click(function(){
		if($(".follow_up_hidden_trs").css('display') == 'table-row')
		{
			$("#show_follow_up_button").addClass('fa-plus').removeClass('fa-minus');
			$(".follow_up_hidden_trs").fadeOut();
		}
		else{

			$(".follow_up_hidden_trs").fadeIn();
			$("#show_follow_up_button").addClass('fa-minus').removeClass('fa-plus');
		}
	});

	$("#show_treatment_completed_district_table").click(function(){
		if($(".treatment_completed_hidden_trs").css('display') == 'table-row')
		{
			$("#show_treatment_completed_button").addClass('fa-plus').removeClass('fa-minus');
			$(".treatment_completed_hidden_trs").fadeOut();
		}
		else{

			$(".treatment_completed_hidden_trs").fadeIn();
			$("#show_treatment_completed_button").addClass('fa-minus').removeClass('fa-plus');
		}
	});

	$("#show_loss_to_followup_button").click(function(){
		if($(".loss_to_followup_hidden_trs").css('display') == 'table-row')
		{
			$("#show_loss_to_followup_button").addClass('fa-plus').removeClass('fa-minus');
			$(".loss_to_followup_hidden_trs").fadeOut();
		}
		else{

			$(".loss_to_followup_hidden_trs").fadeIn();
			$("#show_loss_to_followup_button").addClass('fa-minus').removeClass('fa-plus');
		}
	});

	$(".missed_app_hidden_trs").click(function(){
		var district_id = $(this).closest('tr').children('td:first').text();
		document.location.href = "<?php echo site_url(); ?>admin/reports/missed_appointment/"+district_id;
	});

	$(".follow_up_hidden_trs").click(function(){
		var district_id = $(this).closest('tr').children('td:first').text();
		document.location.href = "<?php echo site_url(); ?>admin/reports/patients_to_be_followed_up/"+district_id;
	});

	$(".treatment_completed_hidden_trs").click(function(){
		var district_id = $(this).closest('tr').children('td:first').text();
		document.location.href = "<?php echo site_url(); ?>admin/reports/treatment_completed_list/"+district_id;
	});

	$(".loss_to_followup_hidden_trs").click(function(){
		var district_id = $(this).closest('tr').children('td:first').text();
		document.location.href = "<?php echo site_url(); ?>admin/reports/loss_to_followup_list/"+district_id;
	});

	$("#excel_treatment_completed").click(function(){
		$("#export_type").val('excel');
		$("#export_report").val('treatment_completed');
		$("#filter_form").submit();
		$("#export_type").val('');
		$("#export_report").val('');
	});

	$("#excel_missed_appointment").click(function(){
		$("#export_type").val('excel');
		$("#export_report").val('missed_appointment');
		$("#filter_form").submit();
		$("#export_type").val('');
		$("#export_report").val('');
	});

	$("#excel_follow_up").click(function(){
		$("#export_type").val('excel');
		$("#export_report").val('follow_up');
		$("#filter_form").submit();
		$("#export_type").val('');
		$("#export_report").val('');
	});
	
	$("#excel_loss_to_followup").click(function(){
		$("#export_type").val('excel');
		$("#export_report").val('loss_to_followup');
		$("#filter_form").submit();
		$("#export_type").val('');
		$("#export_report").val('');
	});

});


function drawCirrhosisChart()
{

	var ctx = $("#cirrhosisChart");
	
	<?php $cirrhosis_total = $cirrhosis_data[0]->cnt + $cirrhosis_data[1]->cnt;?>

	var data = {
		labels: [
		"Cirrhosis - <?php echo round(($cirrhosis_data[0]->cnt/$cirrhosis_total)*100,2); ?> %",
		"No-Cirrhosis - <?php echo round(($cirrhosis_data[1]->cnt/$cirrhosis_total)*100,2); ?> %",
		],
		datasets: [
		{
			data: [<?php echo $cirrhosis_data[0]->cnt; ?>, <?php echo $cirrhosis_data[1]->cnt; ?>],
			backgroundColor: [
			'rgba(255,46,46,0.8)',
			'rgba(54, 162, 235, 0.8)',
			],
			hoverBackgroundColor: [
			'rgba(255,46,46,0.8)',
			'rgba(54, 162, 235, 0.8)',
			],
		}]
	};
	var options = {
		animation : {
			animateRotate : true,
			duration : 2000,
			onComplete : function(){
				var url_base64jp = document.getElementById("cirrhosisChart").toDataURL("image/png");
				document.getElementById("cirrhosisChartLink").href=url_base64jp;
			},
		}
	};
	var myPieChart = new Chart(ctx,{
		type: 'pie',
		data: data,
		options: options
	});
}

function drawGenotypeChart()
{

	var ctx = $("#genotypeChart");
	
	var total = <?php echo $genotype->gen1 +  $genotype->gen2 +  $genotype->gen3 +  $genotype->gen4 +  $genotype->gen5 +  $genotype->gen6; ?>;
	var data = {
		labels: [
		"Genotype 1",
		"Genotype 2",
		"Genotype 3",
		"Genotype 4",
		"Genotype 5",
		"Genotype 6",
		],
		datasets: [
		{	
			data: [
			<?php echo ($genotype->gen1 == null)?"0":$genotype->gen1; ?>,
			<?php echo ($genotype->gen2 == null)?"0":$genotype->gen2; ?>,
			<?php echo ($genotype->gen3 == null)?"0":$genotype->gen3; ?>,
			<?php echo ($genotype->gen4 == null)?"0":$genotype->gen4; ?>,
			<?php echo ($genotype->gen5 == null)?"0":$genotype->gen5; ?>,
			<?php echo ($genotype->gen6 == null)?"0":$genotype->gen6; ?>
			],
			backgroundColor: [
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			],
			hoverBackgroundColor: [
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			],
			borderColor: [
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			],
			borderWidth: 1,
		}]
	};
	var options = {
		animation : {
			animateRotate : true,
			duration : 2000,
			onComplete : function(){
				var cascade_url_base64jp = document.getElementById("genotypeChart").toDataURL("image/png");
				document.getElementById("genotypeChartLink").href=cascade_url_base64jp;
			},
		},
		legend: {
			display: false
		},
		tooltips: {
			callbacks: {
				label: function(tooltipItem) {

					var percentage = Math.round((tooltipItem.yLabel/total)*100);
					return tooltipItem.yLabel+" , "+percentage+"%"; 
				},
			}
		},
		scales : {

			xAxes : [{
				gridLines: {
					color: "rgba(0, 0, 0, 0)",
				}
			}],
			yAxes : [{
				gridLines: {
					color: "rgba(0, 0, 0, 0)",
				}
			}],
		},
		tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= value + ' %' %>",
	};
	var myBarChart = new Chart(ctx,{
		type: 'bar',
		data: data,
		options: options
	});
}

function drawRegimentypeChart()
{

	var ctx = $("#regimenChart");
	
	var total = <?php echo $regimen[0]->count +  $regimen[1]->count +  $regimen[2]->count +  $regimen[3]->count ; ?>;
	var data = {
		labels: [
		"Reg1: Sof + Ledi",
		"Reg2: Sof + Ledi + Riba",
		"Reg3: Sof + Dac",
		"Reg4: Sof + Dac + Riba",
		],
		datasets: [
		{	
			data: [
			<?php echo ($regimen[0]->count == null)?"0":$regimen[0]->count; ?>,
			<?php echo ($regimen[1]->count == null)?"0":$regimen[1]->count; ?>,
			<?php echo ($regimen[2]->count == null)?"0":$regimen[2]->count; ?>,
			<?php echo ($regimen[3]->count == null)?"0":$regimen[3]->count; ?>,
			],
			backgroundColor: [
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			],
			hoverBackgroundColor: [
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			],
			borderColor: [
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			],
			borderWidth: 1,
		}]
	};
	var options = {
		animation : {
			animateRotate : true,
			duration : 2000,
			onComplete : function(){
				var cascade_url_base64jp = document.getElementById("regimenChart").toDataURL("image/png");
				document.getElementById("regimenChartLink").href=cascade_url_base64jp;
			},
		},
		legend: {
			display: false
		},
		tooltips: {
			callbacks: {
				label: function(tooltipItem) {

					var percentage = Math.round((tooltipItem.yLabel/total)*100);
					return tooltipItem.yLabel+" , "+percentage+"%"; 
				},
			}
		},
		scales : {

			xAxes : [{
				gridLines: {
					color: "rgba(0, 0, 0, 0)",
				}
			}],
			yAxes : [{
				gridLines: {
					color: "rgba(0, 0, 0, 0)",
				}
			}],
		},
		tooltipTemplate: "<%if (label){%><%=label %>: <%}%><%= value + ' %' %>",
	};
	var myBarChart = new Chart(ctx,{
		type: 'bar',
		data: data,
		options: options
	});
}


function drawAgeWiseChart()
{
	var ctx = $("#ageChart");

	var data = {
		labels: ["<10 Years", "10-20 Years", "20-30 Years", "30-40 Years", "40-50 Years", "50-60 Years", ">=60 Years"],
		datasets: [
		{
			backgroundColor: [
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			],
			borderColor: [
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			],
			borderWidth: 1,
			data: [
			<?php echo $age_data[0]->Patients; ?>, 
			<?php echo $age_data[1]->Patients; ?>,
			<?php echo $age_data[2]->Patients; ?>,
			<?php echo $age_data[3]->Patients; ?>,
			<?php echo $age_data[4]->Patients; ?>,
			<?php echo $age_data[5]->Patients; ?>,
			<?php echo $age_data[6]->Patients; ?>,]
		}
		]
	};
	var total = <?php echo $age_data[0]->Patients +   $age_data[1]->Patients +  $age_data[2]->Patients +  $age_data[3]->Patients +  $age_data[4]->Patients +  $age_data[5]->Patients +  $age_data[6]->Patients; ?>;
	var options = {
		animation : {
			animateRotate : true,
			duration : 2000,
			onComplete : function(){
				var cascade_url_base64jp = document.getElementById("ageChart").toDataURL("image/png");
				document.getElementById("ageChartLink").href=cascade_url_base64jp;
			},
		},
		legend: {
			display: false
		},
		tooltips: {
			callbacks: {
				label: function(tooltipItem) {
					var percentage = Math.round((tooltipItem.yLabel/total)*100);
					return tooltipItem.yLabel+" , "+percentage+"%"; 
				}
			}
		},
		scales : {
			xAxes : [{
				ticks : {
					autoSkip: false,
					maxRotation: 0,
					minRotation: 0
				},
				gridLines: {
					color: "rgba(0, 0, 0, 0)",
				}
			}],
			yAxes : [{
				gridLines: {
					color: "rgba(0, 0, 0, 0)",
				}
			}]
		}
	};

	var myBarChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options
	});
}

function drawFacilityCascade(){
	var ctx = $("#FacilityCascade");
	var data = {
		labels: ["<?php echo $facility_calloutput[0]->LookupValue; ?>","<?php echo $facility_calloutput[1]->LookupValue; ?>","<?php echo $facility_calloutput[2]->LookupValue; ?>","<?php echo $facility_calloutput[3]->LookupValue; ?>","<?php echo $facility_calloutput[4]->LookupValue; ?>","<?php echo $facility_calloutput[5]->LookupValue; ?>","<?php echo $facility_calloutput[6]->LookupValue; ?>","<?php echo $facility_calloutput[7]->LookupValue; ?>","<?php echo $facility_calloutput[8]->LookupValue; ?>","<?php echo $facility_calloutput[9]->LookupValue; ?>"],
		datasets: [{
			label : "Actuals",
			backgroundColor: [
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
                // 'rgba(54, 162, 235,  0.6)',
                ],
                borderColor: [
                'rgba(54, 162, 235,  0.6)',
                'rgba(54, 162, 235,  0.6)',
                'rgba(54, 162, 235,  0.6)',
                'rgba(54, 162, 235,  0.6)',
                'rgba(54, 162, 235,  0.6)',
                'rgba(54, 162, 235,  0.6)',
                // 'rgba(54, 162, 235,  0.6)',
                ],
                borderWidth: 1,
                data: [
                <?php foreach ($facility_calloutput as $row) {
                	echo $row->count_output.',';
                } ?>
                ]
              }]
            }

            var options = {
            	animation : {
            		animateRotate : true,
            		duration : 2000,
            		onComplete : function(){
            			var cascade_url_base64jp = document.getElementById("FacilityCascade").toDataURL("image/png");
            			document.getElementById("facility_cascade_link").href=cascade_url_base64jp;
            		},
            	},
            	legend: {
            		display: false,
            	},
            	tooltips: {
            		callbacks: {
            			label: function(tooltipItem) {
            				return tooltipItem.yLabel;
            			}
            		}
            	},

            	scales: {
            		yAxes: [{
            			stacked: true,
            			ticks: {
            				beginAtZero:true
            			},
            			gridLines: {
            				color: "rgba(0, 0, 0, 0)",
            			}
            		}],
            		xAxes: [{
            			barThickness : 70,
            			stacked: true,
            			ticks: {
            				beginAtZero:true
            			},
            			gridLines: {
            				color: "rgba(0, 0, 0, 0)",
            			}
            		}]
            	}
            };

            ctx.fillStyle = "rgb(255,255,255,1)";
            var myBarChart = new Chart(ctx, {
            	type: 'bar',
            	data: data,
            	options: options,

            });	

          }

          function drawCascadeChart()
          {

          	var ctx = $("#cascadeChart");
          	var data = {
          		labels: ["Antibody Positive",  "Chronic HCV", "Initiated on Treatment", "Treatment Completed", "SVR Test Done", "Treatment Successful"],
          		datasets: [
          		{
          			label : "Actuals",
          			backgroundColor: [
          			'rgba(54, 162, 235,  0.6)',
          			'rgba(54, 162, 235,  0.6)',
          			'rgba(54, 162, 235,  0.6)',
          			'rgba(54, 162, 235,  0.6)',
          			'rgba(54, 162, 235,  0.6)',
          			'rgba(54, 162, 235,  0.6)',
                // 'rgba(54, 162, 235,  0.6)',
                ],
                borderColor: [
                'rgba(54, 162, 235,  0.6)',
                'rgba(54, 162, 235,  0.6)',
                'rgba(54, 162, 235,  0.6)',
                'rgba(54, 162, 235,  0.6)',
                'rgba(54, 162, 235,  0.6)',
                'rgba(54, 162, 235,  0.6)',
                // 'rgba(54, 162, 235,  0.6)',
                ],
                borderWidth: 1,
                data: [
                <?php echo $cascade_row1->AntibodyPositive; ?>,
            // <?php echo $cascade_row1->Confirmatory; ?>, 
            <?php echo $cascade_row1->ChronicHIV; ?>,
            <?php echo $cascade_row1->TreatmentInitiated; ?>,
            <?php echo $cascade_row1->TreatmentCompleted; ?>,
            // <?php echo $cascade_row1->SVR; ?>,
            // <?php echo $cascade_row1->TreatmentSuccessful; ?>,
            <?php echo $cumulative_svr[0]->count; ?>,
            <?php echo $cumulative_svr[1]->count; ?>,
            ]
          },
        
        ]
      };

      var options = {
      	animation : {
      		animateRotate : true,
      		duration : 2000,
      		onComplete : function(){
      			var cascade_url_base64jp = document.getElementById("cascadeChart").toDataURL("image/png");
      			document.getElementById("cascadeChartLink").href=cascade_url_base64jp;
      		},
      	},
      	legend: {
      		display: false,
      	},
      	tooltips: {
      		callbacks: {
      			label: function(tooltipItem) {
      				return tooltipItem.yLabel;
      			}
      		}
      	},
	    
	    scales: {
	    	yAxes: [{
	    		stacked: true,
	    		ticks: {
	    			beginAtZero:true
	    		},
	    		gridLines: {
	    			color: "rgba(0, 0, 0, 0)",
	    		}
	    	}],
	    	xAxes: [{
	    		barThickness : 70,
	    		stacked: true,
	    		ticks: {
	    			beginAtZero:true
	    		},
	    		gridLines: {
	    			color: "rgba(0, 0, 0, 0)",
	    		}
	    	}]

	    }
	  };

	  ctx.fillStyle = "rgb(255,255,255,1)";
	  var myBarChart = new Chart(ctx, {
	  	type: 'bar',
	  	data: data,
	  	options: options,

	  });	
	}

	function genderAreaChart()
	{

		var ctx = $("#genderAreaChart");

		var data = {
			labels: [
			"Urban",
			"Rural",
			],
			datasets: [
			{	label: "Male",
			data: [
			<?php echo ($gender_area_array[0]->Patients == null)?"0":$gender_area_array[0]->Patients; ?>,
			<?php echo ($gender_area_array[1]->Patients == null)?"0":$gender_area_array[1]->Patients; ?>,
			],
			backgroundColor: [
			'rgba(54, 162, 235, 0.6)',
			'rgba(54, 162, 235, 0.6)',
			],
			hoverBackgroundColor: [
			'rgba(54, 162, 235, 0.6)',
			'rgba(54, 162, 235, 0.6)',
			],
			borderColor: [
			'rgba(54, 162, 235, 0.6)',
			'rgba(54, 162, 235, 0.6)',
			],
			borderWidth: 1,
		},
		{	
			label : "Female",
			data : [
			<?php echo ($gender_area_array[2]->Patients == null)?"0":$gender_area_array[2]->Patients; ?>,
			<?php echo ($gender_area_array[3]->Patients == null)?"0":$gender_area_array[3]->Patients; ?>,
			],
			backgroundColor: [
			'rgba(255, 99, 132, 0.6)',
			'rgba(255, 99, 132, 0.6)',
			],
			hoverBackgroundColor: [
			'rgba(255, 99, 132, 0.6)',
			'rgba(255, 99, 132, 0.6)',
			],
			borderColor: [
			'rgba(255, 99, 132, 0.6)',
			'rgba(255, 99, 132, 0.6)',
			],
			borderWidth: 1,
		},
		{	
			label : "Transgender",
			data : [
			<?php echo ($gender_area_array[4]->Patients == null)?"0":$gender_area_array[4]->Patients; ?>,
			<?php echo ($gender_area_array[5]->Patients == null)?"0":$gender_area_array[5]->Patients; ?>,
			],
			backgroundColor: [
			'rgba(255, 206, 86, 0.6)',
			'rgba(255, 206, 86, 0.6)',
			],
			hoverBackgroundColor: [
			'rgba(255, 206, 86, 0.6)',
			'rgba(255, 206, 86, 0.6)',
			],
			borderColor: [
			'rgba(255, 206, 86, 0.6)',
			'rgba(255, 206, 86, 0.6)',
			],
			borderWidth: 1,
		}
		]
	};
	var options = {
		animation : {
			animateRotate : true,
			duration : 2000,
			onComplete : function(){
				var cascade_url_base64jp = document.getElementById("genderAreaChart").toDataURL("image/png");
				document.getElementById("genderAreaChartLink").href=cascade_url_base64jp;
			},
		},
		legend: {
			display: true
		},
		tooltips: {
			callbacks: {
				label: function(tooltipItem, data) {

					var total_stack1 = data.datasets[0].data[0] + data.datasets[1].data[0] + data.datasets[2].data[0];

					var total_stack2 = data.datasets[0].data[1] + data.datasets[1].data[1] + data.datasets[2].data[1];

					if(tooltipItem.index == 0)
					{

						var percentage = Math.round(((tooltipItem.yLabel/total_stack1)*100)*100)/100;
						return tooltipItem.yLabel+" , "+percentage+"%"; 
					}
					else if(tooltipItem.index == 1)
					{

						var percentage = Math.round(((tooltipItem.yLabel/total_stack2)*100)*100)/100;
						return tooltipItem.yLabel+" , "+percentage+"%"; 
					}
				}
			}
		},
		scales: {
			yAxes: [{
				stacked: true,
				ticks: {
					beginAtZero:true
				},
				gridLines: {
					color: "rgba(0, 0, 0, 0)",
				}
			}],
			xAxes: [{
				barThickness : 70,
				stacked: true,
				ticks: {
					beginAtZero:true
				},
				gridLines: {
					color: "rgba(0, 0, 0, 0)",
				}
			}]
			
		}
	};
	var myBarChart = new Chart(ctx,{
		type: 'bar',
		data: data,
		options: options
	});
}

function drawTrendChart()
{
	var ctx = $("#trendGraph");

	var data = {
		labels: [
		<?php 
		foreach ($trend_data as $row) {
			echo "\"".$row->hospital."\",";
		}
		?>
		],
		datasets: [
		{
			backgroundColor: [
			<?php 
			foreach ($trend_data as $row) {
				echo "'".$row->color."',";
			}
			?>
			],
			borderColor: [
			<?php 
			foreach ($trend_data as $row) {
				echo "'".$row->color."',";
			}
			?>
			],
			borderWidth: 1,
			data: [
			<?php 
			foreach ($trend_data as $row) {
				echo "\"".floor($row->countIni)."\",";
			}
			?>
			]
		}
		]
	};

	var options = {
		animation : {
			animateRotate : true,
			duration : 2000,
			onComplete : function(){
				var cascade_url_base64jp = document.getElementById("trendGraph").toDataURL("image/png");
				document.getElementById("trendGraphLink").href=cascade_url_base64jp;
			},
		},
		legend: {
			display: false
		},
		scales : {
			xAxes : [{
				gridLines: {
					color: "rgba(0, 0, 0, 0)",
				}
			}],
			yAxes : [{
				gridLines: {
					color: "rgba(0, 0, 0, 0)",
				}
			}]
		}
	};

	var myBarChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options
	});
}

function drawAdherenceChart()
{
	var ctx = $("#adherenceChart");

	var data = {
		labels: ["Visit 2", 
		"Visit 3", 
		"Visit 4", 
		"Visit 5", 
		"Visit 6", 
		"Visit 7",],
		datasets: [
		{
			backgroundColor: [
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			],
			borderColor: [
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			'rgba(54, 162, 235,  0.6)',
			],
			borderWidth: 1,
			data: [
			<?php echo round($adherence[0]->v2, 2); ?>, 
			<?php echo round($adherence[0]->v3, 2); ?>,
			<?php echo round($adherence[0]->v4, 2); ?>,
			<?php echo round($adherence[0]->v5, 2); ?>,
			<?php echo round($adherence[0]->v6, 2); ?>,
			<?php echo round($adherence[0]->v7, 2); ?>,]
		},
        // {
        // 	label : "test",
        // 	type : "line",
        // 	data : 	[
        // 	<?php echo round($adherence[0]->adh_overall, 2); ?>,
        // 	<?php echo round($adherence[0]->adh_overall, 2); ?>,
        // 	<?php echo round($adherence[0]->adh_overall, 2); ?>,
        // 	<?php echo round($adherence[0]->adh_overall, 2); ?>,
        // 	<?php echo round($adherence[0]->adh_overall, 2); ?>,
        // 	<?php echo round($adherence[0]->adh_overall, 2); ?>,
        // 	],
        // 	fill: false,
        //     borderColor: 'rgba(255,46,46,0.8)',
        //     backgroundColor: 'rgba(255,46,46,0.8)',
        //     pointBorderColor: 'rgba(255,46,46,0.8)',
        //     pointBackgroundColor: 'rgba(255,46,46,0.8)',
        //     pointHoverBackgroundColor: 'rgba(255,46,46,0.8)',
        //     pointHoverBorderColor: 'rgba(255,46,46,0.8)',
        //     pointRadius : 0

        // }
        ]
      };

      var options = {
      	animation : {
      		animateRotate : true,
      		duration : 2000,
      		onComplete : function(){
      			var cascade_url_base64jp = document.getElementById("adherenceChart").toDataURL("image/png");
      			document.getElementById("adherenceChartLink").href=cascade_url_base64jp;
      		},
      	},
      	legend: {
      		display: false
      	},
      	tooltips: {
      		callbacks: {
      			label: function(tooltipItem) {
      				return tooltipItem.yLabel;
      			}
      		}
      	},
      	scales : {
      		xAxes : [{
      			ticks : {
      				autoSkip: false,
      				maxRotation: 0,
      				minRotation: 0
      			},
      			gridLines: {
      				color: "rgba(0, 0, 0, 0)",
      			}
      		}],
      		yAxes : [{
      			gridLines: {
      				color: "rgba(0, 0, 0, 0)",
      			}
      		}],
      	}
      };

      var myBarChart = new Chart(ctx, {
      	type: 'bar',
      	data: data,
      	options: options
      });
    }

    function drawTreatmentSuccessChart()
    {
    	var ctx = $("#treatmentSuccessChart");

    	var total = <?php echo $cumulative_svr[0]->count; ?>;

    	var data = {
    		labels: ["Treatment Successful", "Treatment Unsuccessful"],
    		datasets: [
    		{
    			backgroundColor: [
    			'rgba(54, 162, 235, 0.8)',
    			'rgba(255,46,46,0.8)',
    			],
    			hoverBackgroundColor: [
    			'rgba(54, 162, 235, 0.8)',
    			'rgba(255,46,46,0.8)',
    			],
    			borderWidth: 1,
    			data: 
            // [
            // <?php echo $treatment_success[0]->SVRSuccessful; ?>, 
            // <?php echo ($treatment_success[0]->SVR - $treatment_success[0]->SVRSuccessful); ?>]
            [
            <?php echo $cumulative_svr[1]->count; ?>, 
            <?php echo ($cumulative_svr[0]->count - $cumulative_svr[1]->count); ?>]
          }
          ]
        };

        var options = {
        	animation : {
        		animateRotate : true,
        		duration : 2000,
        		onComplete : function(){
        			var url_base64jp = document.getElementById("treatmentSuccessChart").toDataURL("image/png");
        			document.getElementById("treatmentSuccessChartLink").href=url_base64jp;
        		},
        	},
        	tooltips: {
        		callbacks: {
        			label: function(tooltipItem, data) {
        				var percentage = Math.round(((data.datasets[0].data[tooltipItem.index]/total)*100)*100)/100;
        				return data.datasets[0].data[tooltipItem.index]+" , "+percentage+"%"; 
        			}
        		}
        	},
        };

        var myBarChart = new Chart(ctx, {
        	type: 'pie',
        	data: data,
        	options: options
        });
      }


      function drawRiskFactorChart()
      {
      	var ctx = $("#riskFactorChart");

      	var total = <?php echo $riskfactor[0]->count +  $riskfactor[1]->count +  $riskfactor[2]->count +  $riskfactor[3]->count +  $riskfactor[4]->count +  $riskfactor[5]->count +  $riskfactor[6]->count; ?>;

      	var data = {
      		labels: [
      		["Unsafe", "Injection Use"], 
      		["IVDU"], 
      		["Unprotected","Sexual 	Practice"], 
      		["Surgery"], 
      		["Dental"], 
      		["Others"],
      		["Not Available"]
      		],
      		datasets: [
      		{
      			backgroundColor: [
      			'rgba(54, 162, 235,  0.6)',
      			'rgba(54, 162, 235,  0.6)',
      			'rgba(54, 162, 235,  0.6)',
      			'rgba(54, 162, 235,  0.6)',
      			'rgba(54, 162, 235,  0.6)',
      			'rgba(54, 162, 235,  0.6)',
      			'rgba(54, 162, 235,  0.6)',
      			],
      			borderColor: [
      			'rgba(54, 162, 235,  0.6)',
      			'rgba(54, 162, 235,  0.6)',
      			'rgba(54, 162, 235,  0.6)',
      			'rgba(54, 162, 235,  0.6)',
      			'rgba(54, 162, 235,  0.6)',
      			'rgba(54, 162, 235,  0.6)',
      			'rgba(54, 162, 235,  0.6)',
      			],
      			borderWidth: 1,
      			data: [
      			<?php echo ($riskfactor[0]->count != null)?$riskfactor[0]->count:'0'; ?>, 
      			<?php echo ($riskfactor[1]->count != null)?$riskfactor[1]->count:'0'; ?>,
      			<?php echo ($riskfactor[2]->count != null)?$riskfactor[2]->count:'0'; ?>,
      			<?php echo ($riskfactor[3]->count != null)?$riskfactor[3]->count:'0'; ?>,
      			<?php echo ($riskfactor[4]->count != null)?$riskfactor[4]->count:'0'; ?>,
      			<?php echo ($riskfactor[5]->count != null)?$riskfactor[5]->count:'0'; ?>,
      			<?php echo ($riskfactor[6]->count != null)?$riskfactor[6]->count:'0'; ?>,
      			]
      		}
      		]
      	};

      	var options = {
      		animation : {
      			animateRotate : true,
      			duration : 2000,
      			onComplete : function(){
      				var cascade_url_base64jp = document.getElementById("riskFactorChart").toDataURL("image/png");
      				document.getElementById("riskFactorChartLink").href=cascade_url_base64jp;
      			},
      		},
      		legend: {
      			display: false
      		},
      		tooltips: {
      			callbacks: {
      				label: function(tooltipItem) {
      					var percentage = Math.round(((tooltipItem.yLabel/total)*100)*100)/100;
      					return tooltipItem.yLabel+" , "+percentage+"%"; 
      				}
      			}
      		},
      		scales : {
      			xAxes : [{
      				ticks : {
      					suggestedMin : 0,
      					beginAtZero : true,
      					autoSkip: false,
      					maxRotation: 0,
      					minRotation: 0
      				},
      				gridLines: {
      					color: "rgba(0, 0, 0, 0)",
      				}
      			}],
      			yAxes : [{
      				ticks : {
      					suggestedMin : 0,
      					beginAtZero : true,
      				},
      				gridLines: {
      					color: "rgba(0, 0, 0, 0)",
      				}
      			}]
      		}
      	};

      	var myBarChart = new Chart(ctx, {
      		type: 'bar',
      		data: data,
      		options: options
      	});
      }

      function drawTATChart()
      {
      	var ctx = $("#TATChart");

      	var data = {
      		labels: [
      		"Cirrhosis",
      		"No-Cirrhosis",
      		],
      		datasets: [
      		{	label: "Antibody Test to HCV Viral Load",
      		data: [
      		<?php echo ($tat[0]->one == null)?"0":round($tat[0]->one); ?>,
      		<?php echo ($tat[1]->one == null)?"0":round($tat[1]->one); ?>,
      		],
      		backgroundColor: [
      		'rgba(54, 162, 235, 0.6)',
      		'rgba(54, 162, 235, 0.6)',
      		],
      		hoverBackgroundColor: [
      		'rgba(54, 162, 235, 0.6)',
      		'rgba(54, 162, 235, 0.6)',
      		],
      		borderColor: [
      		'rgba(54, 162, 235, 0.6)',
      		'rgba(54, 162, 235, 0.6)',
      		],
      		borderWidth: 1,
      	},
      	{	
      		label : "HCV Viral Load To Treatment Initiation",
      		data : [
      		<?php echo ($tat[0]->two == null)?"0":round($tat[0]->two); ?>,
      		<?php echo ($tat[1]->two == null)?"0":round($tat[1]->two); ?>,
      		],
      		backgroundColor: [
      		'rgba(255, 99, 132, 0.6)',
      		'rgba(255, 99, 132, 0.6)',
      		],
      		hoverBackgroundColor: [
      		'rgba(255, 99, 132, 0.6)',
      		'rgba(255, 99, 132, 0.6)',
      		],
      		borderColor: [
      		'rgba(255, 99, 132, 0.6)',
      		'rgba(255, 99, 132, 0.6)',
      		],
      		borderWidth: 1,
      	},
      	{	
      		label : "Initiation To Completion",
      		data : [
      		<?php echo ($tat[0]->three == null)?"0":round($tat[0]->three); ?>,
      		<?php echo ($tat[1]->three == null)?"0":round($tat[1]->three); ?>,
      		],
      		backgroundColor: [
      		'rgba(255, 206, 86, 0.6)',
      		'rgba(255, 206, 86, 0.6)',
      		],
      		hoverBackgroundColor: [
      		'rgba(255, 206, 86, 0.6)',
      		'rgba(255, 206, 86, 0.6)',
      		],
      		borderColor: [
      		'rgba(255, 206, 86, 0.6)',
      		'rgba(255, 206, 86, 0.6)',
      		],
      		borderWidth: 1,
      	},
      	{	
      		label : "Completion To SVR Test",
      		data : [
      		<?php echo ($tat[0]->four == null)?"0":round($tat[0]->four); ?>,
      		<?php echo ($tat[1]->four == null)?"0":round($tat[1]->four); ?>,
      		],
      		backgroundColor: [
      		'rgba(186,214,31, 0.8)',
      		'rgba(186,214,31, 0.8)',
      		],
      		hoverBackgroundColor: [
      		'rgba(186,214,31, 0.8)',
      		'rgba(186,214,31, 0.8)',
      		],
      		borderColor: [
      		'rgba(186,214,31, 0.8)',
      		'rgba(186,214,31, 0.8)',
      		],
      		borderWidth: 1,
      	}
      	]
      };
      var options = {
      	animation : {
      		animateRotate : true,
      		duration : 2000,
      		onComplete : function(){
      			var cascade_url_base64jp = document.getElementById("TATChart").toDataURL("image/png");
      			document.getElementById("TATChartLink").href=cascade_url_base64jp;
      		},
      	},
      	legend: {
      		display: true
      	},
      	tooltips: {
      		callbacks: {
      			label: function(tooltipItem) {
      				return tooltipItem.xLabel;
      			}
      		}
      	},
      	scales: {
      		yAxes: [{
      			barThickness : 50,
      			stacked: true,
      			ticks: {
      				beginAtZero:true
      			},
      			gridLines: {
      				color: "rgba(0, 0, 0, 0)",
      			}
      		}],
      		xAxes: [{
      			barThickness : 50,
      			stacked: true,
      			ticks: {
      				beginAtZero:true
      			},
      			gridLines: {
      				color: "rgba(0, 0, 0, 0)",
      			}
      		}]

      	}
      };
      var myBarChart = new Chart(ctx,{
      	type: 'horizontalBar',
      	data: data,
      	options: options
      });
    }

    function drawMonthlyChart()
    {
    	var ctx = $("#monthlyChart");

    	var data = {
    		labels: [

    		<?php 
    		foreach ($monthly as $row) {
    			echo "'".$row->monthname."-".$row->year."',";
    		}
    		?>
    		],
    		datasets: [
    		{
    			label: "My First dataset",
    			fill: false,
    			lineTension: 0.1,
    			backgroundColor: "rgba(75,192,192,0.4)",
    			borderColor: "rgba(75,192,192,1)",
    			borderCapStyle: 'butt',
    			borderDash: [],
    			borderDashOffset: 0.0,
    			borderJoinStyle: 'miter',
    			pointBorderColor: "rgba(75,192,192,1)",
    			pointBackgroundColor: "#fff",
    			pointBorderWidth: 1,
    			pointHoverRadius: 5,
    			pointHoverBackgroundColor: "rgba(75,192,192,1)",
    			pointHoverBorderColor: "rgba(220,220,220,1)",
    			pointHoverBorderWidth: 2,
    			pointRadius: 5,
    			pointHitRadius: 10,
    			spanGaps: false,
    			data: [
    			<?php 
    			foreach ($monthly as $row) {
    				echo $row->count.",";
    			}
    			?>
    			]
    		}
    		]
    	};

    	var options = {
    		animation : {
    			animateRotate : true,
    			duration : 2000,
    			onComplete : function(){
    				var cascade_url_base64jp = document.getElementById("monthlyChart").toDataURL("image/png");
    				document.getElementById("monthlyChartLink").href=cascade_url_base64jp;
    			},
    		},
    		legend: {
    			display: false
    		},
    		tooltips: {
    			callbacks: {
    				label: function(tooltipItem) {
    					return tooltipItem.yLabel;
    				}
    			}
    		},
    		scales : {
    			xAxes : [{
    				ticks : {
    					suggestedMin : 0,
    					beginAtZero : true,
    					autoSkip: false,
					  // maxRotation: 0,
       //    			  minRotation: 0
     },
     gridLines: {
     	color: "rgba(0, 0, 0, 0)",
     }
   }],
   yAxes : [{
   	ticks : {
   		suggestedMin : 0,
   		beginAtZero : true,
   	},
   	gridLines: {
   		color: "rgba(0, 0, 0, 0)",
   	}
   }]
 }
};

var myLineChart = new Chart(ctx, {
	type: 'line',
	data: data,
	options: options
});
}

function drawOccupationChart()
{

	var ctx = $("#occupationChart");
	
	<?php $cirrhosis_total = $cirrhosis_data[0]->cnt + $cirrhosis_data[1]->cnt;?>

	var data = {
		labels: [
		<?php 

		foreach ($occupation as $row) {
			echo '"'.$row->occupation.'",';
		}
		?>
		],
		datasets: [
		{
			data: [

			<?php 

			foreach ($occupation as $row) {
				echo $row->count.',';
			}
			?>

			],
			backgroundColor: [
			'#F5B7B1',
			'#BB8FCE',
			'#7FB3D5',
			'#76D7C4',
			'#F7DC6F',
			'#F0B27A',
			'#2E86C1',
			'#CB4335',
			'#D4AC0D',
			'#283747',
			]
		}]
	};
	var options = {
		animation : {
			animateRotate : true,
			duration : 2000,
			onComplete : function(){
				var url_base64jp = document.getElementById("occupationChart").toDataURL("image/png");
				document.getElementById("occupationChartLink").href=url_base64jp;
			},
		}
	};
	var myPieChart = new Chart(ctx,{
		type: 'pie',
		data: data,
		options: options
	});
}
</script>