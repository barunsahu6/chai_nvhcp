<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Printdiv_hep_b extends CI_Controller {
	public function __construct()
	{
		parent::__construct();	
		$this->load->helper('common');
		$this->load->model('Common_Model');
		$this->load->model('Log4php_model');
		$loginData = $this->session->userdata('loginData');	

		//pr($loginData);
		if($loginData == null)
		{
			redirect('login');
		}
		if($loginData->user_type != 2){
			$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login');	
		}
		error_reporting(0);
	}
	public function index($patientguid = null){
		$loginData = $this->session->userdata('loginData');

		//echo $_SERVER['DOCUMENT_ROOT'];
		 $sql = "SELECT * FROM `hepb_tblpatient` WHERE PatientGUID = ?";
		$result = $this->db->query($sql,[$patientguid])->result();
		
		// pr($result); die();


		$content['patient_data'] = $result;
		$stateID = $result[0]->State;
		$district_Code = $result[0]->District;
		$BlockCode = $result[0]->Block;

		$sql = "SELECT * FROM `mstlookup` where flag = 3 and LanguageID = 1";
		$content['risk_factor'] = $this->db->query($sql)->result();

		if($stateID != null){
			$sql = "select StateName from mststate WHERE id_mststate = ?";
			$content['states'] = $this->db->query($sql,[$stateID])->result();
		}

		if($district_Code != null && $stateID != null){
			$sql = "select DistrictName from mstdistrict WHERE id_mstdistrict = ? AND id_mststate = ?";
			$content['district'] = $this->db->query($sql,[$district_Code,$stateID])->result();
		}

		if($district_Code != null && $stateID != null && $BlockCode != null){
			$sql = "select BlockName from mstblock WHERE id_mststate = ? AND id_mstdistrict = ? AND id_mstblock = ?";
			$content['block'] = $this->db->query($sql,[$stateID,$district_Code,$BlockCode])->result();
		}

		$sql = "SELECT * FROM `tblpatientcirrohosis` WHERE PatientGUID = ?";
		$patient_cirrohosis_data = $this->db->query($sql,[$patientguid])->result();

		$content['patient_cirrohosis_data'] = $patient_cirrohosis_data;

		$id_mstfacilit = $result[0]->PreviousTreatingHospital;

		if($id_mstfacilit != null OR $id_mstfacilit != 0){
			$sql = "SELECT FacilityCode FROM mstfacility WHERE id_mstfacility = ? ";
			$content['FacilityCode'] = $this->db->query($sql,[$id_mstfacilit])->result();
		}

		$id_mst_past_regimen = $result[0]->PastRegimen;

		if($id_mst_past_regimen != null OR $id_mst_past_regimen != 0){
			$sql = "SELECT regimen_name FROM `mst_past_regimen` where id_mst_past_regimen= ? AND is_deleted = 0";
			$content['past_regimen'] = $this->db->query($sql,[$id_mst_past_regimen])->result();
		}

		$LookupCode = $result[0]->PreviousDuration;

		if($LookupCode != null OR $LookupCode != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 28 and LanguageID = 1 AND LookupCode = ? ";
			$content['previous_duration'] = $this->db->query($sql,[$LookupCode])->result();
		}

		$weeks_LookupCode = $result[0]->NWeeksCompleted;

		if($weeks_LookupCode != null OR $weeks_LookupCode != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 28 and LanguageID = 1 AND LookupCode = ? ";
			$content['weeks'] = $this->db->query($sql,[$weeks_LookupCode])->result();
		}

		$ART_Regimen_LookupCode = $result[0]->ART_Regimen;

		if($ART_Regimen_LookupCode != null OR $ART_Regimen_LookupCode != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 42 and LanguageID = 1 AND LookupCode = ? ";
			$content['hiv_regimen'] = $this->db->query($sql,[$ART_Regimen_LookupCode])->result();
		}

		//renal_ckd
		$CKDStage_LookupCode = $result[0]->CKDStage;

		if($CKDStage_LookupCode != null OR $CKDStage_LookupCode != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 30 and LanguageID = 1 AND LookupCode = ? ";
			$content['renal_ckd'] = $this->db->query($sql,[$CKDStage_LookupCode])->result();
		}

		//referring_doctor

		$id_mst_medical_specialists = $result[0]->ReferingDoctor;

		$sql = "SELECT name FROM mst_medical_specialists WHERE id_mst_medical_specialists = ?";
		$content['referring_doctor'] = $this->db->query($sql,[$id_mst_medical_specialists])->result();

		//referred to			

		$sql = "SELECT facility_short_name FROM mstfacility where id_mstfacility = ? ";
		$content['referred_to'] = $this->db->query($sql,[$result[0]->ReferTo])->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 68 AND LanguageID = 1 ORDER BY SEQUENCE";
		$content['ailments_list'] = $this->db->query($sql)->result();

		$sql1 = "SELECT * FROM `tblriskprofile_hepb` WHERE PatientGUID = ?";
		$content['patient_tblRiskProfile_data'] = $this->db->query($sql1,[$patientguid])->result();

		$sql = "SELECT PrescribingFacility,id_mstfacility FROM tblpatient where patientguid = ? ";
		$content['dispensationdata'] = $this->db->query($sql,[[$patientguid]])->result();

		$sql = "SELECT facility_short_name FROM mstfacility where id_mstfacility = ? ";
		$content['dispensationdata_val'] = $this->db->query($sql,[$content['dispensationdata'][0]->id_mstfacility])->result();

		//prescribing_doctor
		$sql = "SELECT name FROM mst_medical_specialists where id_mst_medical_specialists = ?";
		$content['prescribing_doctor'] = $this->db->query($sql,[$result[0]->PrescribingDoctor])->result();
		//NAdherence Reason
// pr($result); die();
		
		//regimen_prescribed
		$regimen_prescribed_LookupCode = $result[0]->T_Regimen;

		$sql = "select * from mst_regimen_hepb where is_deleted = 0  AND id_mst_regimenhepb=  ".$result[0]->Drugs_Added;
		$content['regimen_prescribed'] =$this->Common_Model->query_data($sql);
		// print_r($result[0]->Drug_Dosage); die();
		if ($result[0]->Drug_Dosage != "") {	
		$sql1 = "SELECT * FROM `mst_drug_strength_hepb` where id_mst_drug_strengthhepb = ".$result[0]->Drug_Dosage;
		$content['mst_drug_strength_hepb'] = $this->db->query($sql1)->result();  
		}
		// pr($content['mst_drug_strength_hepb']); die();

		
		$sqlsic1 = "SELECT * FROM tblpatient_regimen_drug_data WHERE patientguid = ?";
		$regimen_drug_data = $this->db->query($sqlsic1,[$patientguid])->result();
		if(count($regimen_drug_data) > 0){
			$content['regimen_drug_data'] = $regimen_drug_data;
			
			// echo '<pre>';
			// print_r($regimen_drug_data);die();

			//Sofosbuvir
			$id_mst_drug = $regimen_drug_data[0]->id_mst_drugs;
			$sql = "SELECT strength FROM `mst_drug_strength` where id_mst_drug_strength = ?";
			$content['Sofosbuvir_mst_drug_strength'] = $this->db->query($sql, [$id_mst_drug])->result();

			//Daclatasvir
			$id_mst_drug_strength = $regimen_drug_data[1]->id_mst_drug_strength;

			$sql = "SELECT strength FROM `mst_drug_strength` where id_mst_drug_strength = ?";
			$content['Daclatasvir_mst_drug_strength'] = $this->db->query($sql, [$id_mst_drug_strength])->result();

			//Velpatasvir

			$id_mst_drug_strength = $regimen_drug_data[1]->id_mst_drug_strength;

			$sql = "SELECT strength FROM `mst_drug_strength` where id_mst_drug_strength = ?";
			$content['Velpatasvir_mst_drug_strength'] = $this->db->query($sql, [$id_mst_drug_strength])->result();

			// print_r($content['Velpatasvir_mst_drug_strength']);die();

			//Ribavrin

			$id_mst_drug_strength = $regimen_drug_data[2]->id_mst_drug_strength;

			$sql = "SELECT strength FROM `mst_drug_strength` where id_mst_drug_strength = ?";
			$content['Ribavrin_mst_drug_strength'] = $this->db->query($sql, [$id_mst_drug_strength])->result();

		}
		$sql = "SELECT * FROM `mstlookup` where Flag = 39 and LanguageID = 1";
		$content['sample_transported_to_options'] = $this->db->query($sql)->result();
		//Dispensation information
		$sql = "SELECT * FROM tblpatientvisit WHERE PatientGUID = ?";
		$Dispensation_res = $this->db->query($sql,[$patientguid])->result();
		$content['dispensation_result'] = $Dispensation_res;

		$sql = "SELECT m.LookupValue FROM tblriskprofile_hepb r inner join mstlookup m 
		on ((find_in_set(m.LookupCode,r.RiskID)))   WHERE m.flag = 68 and r.PatientGUID = ? ";
		$content['risk_name'] = $this->db->query($sql,[$patientguid])->result();

		//patient svr result
		$SVR12W_Result_LookupCode = $result[0]->SVR12W_Result;

		if($SVR12W_Result_LookupCode != null OR $SVR12W_Result_LookupCode != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 5 and LanguageID = 1 AND LookupCode = ? ";
			$content['svr_result'] = $this->db->query($sql,[$SVR12W_Result_LookupCode])->result();
		}
		
		$InterruptReason = $result[0]->InterruptReason;
		$DeathReason = $result[0]->DeathReason;
		$LFUReason = $result[0]->LFUReason;
		$InterruptToStage = $result[0]->InterruptToStage;

		if($InterruptReason != null OR $InterruptReason != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 48 and LanguageID = 1 AND LookupCode = ? ";
			$content['InterruptReason'] = $this->db->query($sql,[$InterruptReason])->result();
		}

		if($DeathReason != null OR $DeathReason != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 48 and LanguageID = 1 AND LookupCode = ? ";
			$content['DeathReason'] = $this->db->query($sql,[$DeathReason])->result();
		}
if($content['patient_data'][0]->id_mstfacility!=''){
		 $sql = "SELECT * FROM mstfacility where id_mstfacility = ".$content['patient_data'][0]->id_mstfacility." ";
		$content['oncenter'] = $this->db->query($sql)->result();
	}
if($content['patient_data'][0]->ReferTo!=''){
		 $sql = "SELECT * FROM mstfacility where id_mstfacility = ".$content['patient_data'][0]->ReferTo." ";
		$content['referred_to'] = $this->db->query($sql)->result();
}



/*New Code for this start from here...
//check ....
*/
//function baseline test data start from here...
$sql = "SELECT * FROM `hepb_followup` WHERE PatientGUID = ?";
$patient_datahepb = $this->db->query($sql,[$patientguid])->result();
$content['patient_datahepb']        = $patient_datahepb;

$sql = "SELECT p.*,d.NextVisit FROM hepb_tblpatient p  left join tblpatientdispensationb d on p.PatientGUID=d.PatientGUID WHERE p.PatientGUID = ?";
$patient_datahepb_knownhistory = $this->db->query($sql,[$patientguid])->result();
$content['patient_datahepb_knownhistory']        = $patient_datahepb_knownhistory;

$sql = "SELECT BVLSampleCollectionDate, IsBVLSampleStored, BVLStorageTemp, Storage_days_hrs, Storageduration, IsBVLSampleTransported, BVLTransportTemp, IsBVLSampleTransported, BVLTransportDate, BVLLabID, VLLabID, BVLLabID_Other, BVLTransporterName, BVLResultRemarks, BVLRecieptDate, BVLReceiverName, BStorage_days_hrs, BStorageduration, T_DLL_01_BVLCount,  (case when IsBSampleAccepted=1 then 'Yes' else 'NO' end ) as IsBSampleAccepted,(case  when T_DLL_01_BVLC_Result = 1 then 'Detected' else 'Not Detected' end) as T_DLL_01_BVLC_Result,Visit_No,T_DLL_01_BVLC_Date,PatientGUID FROM `tblpatientvlsamplelist` WHERE PatientGUID = ?  order by Visit_No desc";
$tblpatientvlsamplelistlist = $this->db->query($sql,[$patientguid])->result();

$content['tblpatientvlsamplelistlist']        = $tblpatientvlsamplelistlist;

$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ? and id_mstfacility = ".$loginData->id_mstfacility;
$patient_data = $this->db->query($sql,[$patientguid])->result();
$content['patient_data']   = $patient_data;
// pr($content['patient_datahepb_knownhistory']);


$query = "SELECT a.*,b.LookupValue as PillsTakenValue,c.FirstName as patientName,f.FacilityCode from tblpatientdispensationb a 
left join mstlookup b ON a.PillsTaken = b.LookupCode AND b.Flag = 64 AND b.LanguageID = 1
left join tblpatient c ON a.PatientGUID = c.PatientGUID
left join mstfacility f ON a.DispensationPlace=f.id_mstfacility
where a.PatientGUID = ? ORDER BY a.VisitNo DESC";

$content['tblpatientdispensationb_list'] = $this->db->query($query, [$patientguid])->result();

// echo "<pre>";
// print_r($content['patient_datahepb'] ); die();
		// echo '<pre>';

		// print_r($Dispensation_res);die();

		// echo $side_effect = $Dispensation_res[0]->SideEffectValue;

		// $sql = "SELECT LookupValue FROM `mstlookup` l inner join tblpatient p on l.LookupCode=p.  where flag = 44 and LanguageID = 1 AND LookupCode in($side_effect)";
		// $content['side_effect_data'] = $this->db->query($sql)->result();

		// echo '<pre>';print_r($result);die();
		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Print','Print Patientinfo');
		$content['subview'] = 'printdiv1_hep_b';
		$this->load->view('pages/main_layout', $content);
	}

public function knownhistory_print($patientguid = null){

		 $sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$result = $this->db->query($sql,[$patientguid])->result();
		$content['patient_data'] = $result;

		 $sql = "SELECT * FROM `mst_medical_specialists` WHERE id_mst_medical_specialists = ?";
		$content['doctor_name'] = $this->db->query($sql,[$content['patient_data'][0]->ReferingDoctor])->result();

		 $sql = "SELECT * FROM mstfacility where id_mstfacility = ".$content['patient_data'][0]->ReferTo." ";
		$content['referred_to'] = $this->db->query($sql)->result();

		//print_r($content['doctor_name']);

		$content['subview'] = 'knownhistory_print';
		$this->load->view('pages/main_layout', $content);
}

}