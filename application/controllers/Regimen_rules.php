<?php

defined('BASEPATH') or exit ('no direct access');

class Regimen_rules extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		$query = "SELECT id_mst_regimen_rules, CASE
					    cirrhosis WHEN 1 THEN 'Cirrhosis' WHEN 2 THEN 'No Cirrhosis' ELSE ''
					END AS cirrhosis,
					CONCAT('Genotype ', genotype) AS genotype,
					b.regimen_name AS regimen,
					duration
					FROM
					    `mst_regimen_rules` a
					INNER JOIN mst_regimen b ON
					    a.id_mst_regimen = b.id_mst_regimen where a.is_deleted = 0";
		$content['rules'] = $this->db->query($query)->result();

		$content['subview'] = 'list_regimen_rules';
		$this->load->view('admin/main_layout', $content);
	}

	public function add()
	{	
		$Request_Method = $this->input->server('REQUEST_METHOD');

		if($Request_Method == 'POST')
		{
			// print_r($_POST); die();

			$insert_array = array(
				"cirrhosis"      => $this->input->post('cirrhosis_status'),
				"genotype"       => $this->input->post('genotype'),
				"id_mst_regimen" => $this->input->post('regimen'),
				"duration"       => $this->input->post('duration'),
			);

			$result = $this->db->insert('mst_regimen_rules', $insert_array);

			if($result == 1)
			{
				$this->session->set_flashdata('msg', 'Regimen Rule Added Succesfully');
			}			
			else
			{
				$this->session->set_flashdata('er_msg', 'Error in Adding Regimen Rule');
			}
			redirect('regimen_rules');
		}

		$this->db->where('is_deleted', 0);
		$content['regimen'] = $this->db->get('mst_regimen')->result();

		$content['subview'] = 'add_regimen_rule';
		$this->load->view('admin/main_layout', $content);
	}

	public function delete($id_mst_regimen_rules = null)
	{
		$query = "update mst_regimen_rules set is_deleted = 1 where id_mst_regimen_rules = ".$id_mst_regimen_rules;
		$result = $this->db->query($query);

		if($result == 1)
		{
			$this->session->set_flashdata('msg', 'Regimen Rule Deleted Succesfully');
		}			
		else
		{
			$this->session->set_flashdata('er_msg', 'Error in Deleting Regimen Rule');
		}
		redirect('regimen_rules');

	} 

	public function edit($id_mst_regimen_rules = null)
	{

		$Request_Method = $this->input->server('REQUEST_METHOD');

		if($Request_Method == 'POST')
		{
			// print_r($_POST); die();

			$update_array = array(
				"cirrhosis"      => $this->input->post('cirrhosis_status'),
				"genotype"       => $this->input->post('genotype'),
				"id_mst_regimen" => $this->input->post('regimen'),
				"duration"       => $this->input->post('duration'),
			);

			$result = $this->db->update('mst_regimen_rules', $update_array, array("id_mst_regimen_rules" => $id_mst_regimen_rules));

			if($result == 1)
			{
				$this->session->set_flashdata('msg', 'Regimen Rule Updated Succesfully');
			}			
			else
			{
				$this->session->set_flashdata('er_msg', 'Error in Updating Regimen Rule');
			}
			redirect('regimen_rules');
		}


		$query = "SELECT id_mst_regimen_rules, CASE
					    cirrhosis WHEN 1 THEN 'Cirrhosis' WHEN 2 THEN 'No Cirrhosis' ELSE ''
					END AS cirrhosis,
					CONCAT('Genotype ', genotype) AS genotype,
					b.regimen_name AS regimen,
					duration,
					a.id_mst_regimen
					FROM
					    `mst_regimen_rules` a
					INNER JOIN mst_regimen b ON
					    a.id_mst_regimen = b.id_mst_regimen where a.is_deleted = 0 and id_mst_regimen_rules = ".$id_mst_regimen_rules;
		$content['rule_details'] = $this->db->query($query)->result();

		$this->db->where('is_deleted', 0);
		$content['regimen'] = $this->db->get('mst_regimen')->result();

		$content['updating'] = 1;

		$content['subview'] = 'add_regimen_rule';
		$this->load->view('admin/main_layout', $content);
	}
}