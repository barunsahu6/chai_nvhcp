<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Linelist_update_hepb extends CI_Controller {

	private $patientFilters;

	public function __construct()
	{
		parent::__construct();

		set_time_limit(100000);
		ini_set('memory_limit', '100000M');
		ini_set('upload_max_filesize', '300000M');
		ini_set('post_max_size', '200000M');
		ini_set('max_input_time', 100000);
		ini_set('max_execution_time', 0);
		ini_set('memory_limit','-1');

		$this->load->model('Common_Model');
		$this->load->model('Patient_Model');
		$this->load->helper('common');

	}
	public function index()
	{
		echo "Nothing data found";
	}

	/*Linelist Update start*/

	public function update_linelistMulti($Session_StateID = NULL , $id_mstfacility = NULL ,$FacilityType = NULL )
	{



		if($Session_StateID == NULL){

			$SessionStateID = "AND 1";exit;
			$stateId = "AND 1";
		}else{
			$SessionStateID = "AND l.Session_StateID IN( '".$Session_StateID."')";
			$stateId = " id_mststate IN( ".$Session_StateID.")";
			$SessionstateId = " Session_StateID IN( ".$Session_StateID.")";
		}
		if($FacilityType == NULL){
			$FacilityType    = "AND 1";
		}else{
			$FacilityType    = "AND FacilityType='".$FacilityType."' "; 
		}
		if($id_mstfacility == NULL || $id_mstfacility == 0){

			$id_mstfacility  = "AND 1";
			$id_mstfacilityn = "AND 1";
			$id_mstfacilitynPat = "AND 1";
		}else{
			$id_mstfacilityl  = "AND l.id_mstfacility in (".$id_mstfacility.")";
			$id_mstfacilityn  = "AND f.id_mstfacility in (".$id_mstfacility.")";

			$id_mstfacilitynPat  = "AND id_mstfacility in (".$id_mstfacility.")";
		}


		if($Session_StateID != NULL){



  //echo  $sql = "select id_mstfacility,id_mststate from mstfacility where  ".$stateId." ".$id_mstfacilityn." ".$FacilityType."  ";exit;
			$sql = "SELECT f.id_mstfacility,id_mststate from mstfacility f INNER JOIN (SELECT DISTINCT id_mstfacility from tblpatient WHERE ".$SessionstateId." ".$id_mstfacilitynPat.") p ON f.id_mstfacility=p.id_mstfacility where  ".$stateId." ".$id_mstfacilityn." ".$FacilityType."  ";

			$res = $this->Common_Model->query_data($sql);
				//echo date("d");exit();
			foreach ($res as $row) {


		  //$query = "delete l.* FROM linelist l inner join `tblpatient` p on l.patientguid = p.patientguid where ((p.UpdatedOn >l.UpdatedOn) OR (p.UpdatedOn >=l.UpdatedOn and p.updatedon >=date_add(NOW(),INTERVAL-1 DAY))) and l.id_mstfacility= ".$row->id_mstfacility."  "; 

				$query = "delete l.* FROM linelist l inner join `tblpatient` p on l.patientguid = p.patientguid INNER JOIN mstfacility f on f.id_mstfacility=p.id_mstfacility where ((p.UpdatedOn >l.UpdatedOn) OR (p.UpdatedOn >=l.UpdatedOn and p.updatedon >=date_add(NOW(),INTERVAL-10 DAY))) and l.id_mstfacility= ".$row->id_mstfacility."  "; 
				$this->db->query($query);



				$insert_array = array(
					"StateID"            => $row->id_mststate,
					"id_mstfacility" => $row->id_mstfacility,
					"StartTime"		=>date("Y-m-d H:i:s")
				);

				$this->Common_Model->insert_data('line_list_tracker',$insert_array);


				$query = "INSERT INTO linelist_hepb(
				ID,
				id_mstfacility,
				Session_StateID,
				Session_DistrictID,
				PatientGUID,
				UID_NUM,
				OPD_ID,
				NVHCP_ID,
				Patient_Type,
				NAME,
				Agebetween0and1Year,
				Age,
				Gender,
				Relation,
				Relation_Name,
				Address,
				State,
				District,
				Block_Ward,
				Other_addres,
				Village,
				Pincode,
				MobileNo,
				ConsentforReceivingCommunication,
				RiskFactors,
				Other,
				HbsAg,
				HBSRapid,
				HBSSampleCollectionDate,
				HBSRapidDate,
				HBSRapidResult,
				HBSRapidPlace,
				HBSRapidLabName,
				HBSRapidOtherLabName,
				HBSElisa,
				HBSElisaDate,
				HBSElisaResult,
				HBSElisaPlace,
				HBSELISALabName,
				HBSELISAOtherLabName,
				HBSOther,
				HBSOtherName,
				HBSOtherDate,
				HBSOtherResult,
				HBSOtherPlace,
				HBSLabName,
				HBSOtherLabName,
				VLBHepB,
				VLBSampleCollectionDate,
				VLBIsSampleStored,
				VLBISSampleTransported,
				VLBRemarks,
				VLDBIsSampleAccepted,
				VLDBTestResultDate,
				VLDBResult,
				VLDBViralLoad,
				TreatmentRecommended,
				VLDReasonForRejection,
				VLDBRemarks,
				DateofPrescribingTests,
				Dateofissueoflastinvestigationreport,
				APRI,
				APRIScore,
				FIB4,
				FIB4Score,
				ComplicatedUncomplicated,
				VaricealBleed,
				Ascites,
				Encephalopathy,
				ChildPughScore,
				SeverityofHEPB,
				PersistentlyElevatedALTlevels,
				RiskFactor,
				HIV_ARTRegimen,
				Renal_CKDStage,
				ReasonForPrescribingRibavarin,
				LastMenstrualPeriod,
				Pregnant,
				ExpectedDateofDelivery,
				Referred,
				ReferringDoctor,
				ReferringDoctorOther,
				ReferredTo,
				DATE,
				Observations,
				PrescribingDoctor,
				RegimenPrescribed_2,
				TDF,
				TAF,
				Encetavir,
				PrescribingDate,
				PlaceOfDispensation,
				Dispensationnumber,
				Visitdate,
				Regimenprescribed,
				Daysofpillsdispensed,
				Advisednextvisitdate,
				Dispensationnumber_2,
				Visitdate_2,
				Regimenprescribed_3,
				Daysofpillsdispensed_2,
				Advisednextvisitdate_2,
				Adherence,
				InterruptionStatus,
				InterruptionOthers,
				date_of_patient_registration
			)
			SELECT NULL,
			p.id_mstfacility,
			p.Session_StateID,
			p.Session_DistrictID,
			p.PatientGUID,
			p.UID_Num,
			p.OPD_Id,
			p.UID_Prefix,
			p.PatientType,
			p.FirstName,
			CASE WHEN p.IsAgeMonths = 1 THEN 'Yes' ELSE 'No'
			END AS IsAgeMonths,
			p.Age,
			gender.LookupValue AS gender_look,
			p.Relation,
			p.FatherHusband,
			p.Add1,
			s.StateName,
			d.DistrictName,
			b.BlockName,
			NULL,
			p.VillageTown,
			p.PIN,
			p.Mobile,
			CASE WHEN p.IsSMSConsent = 1 THEN 'Yes' ELSE 'No'
			END AS IsSMSConsent,
			GROUP_CONCAT(',', aa.Lookupvalue) as risk,
			NULL,
			CASE WHEN p.HbsAg = 1 THEN 'Yes' ELSE 'No'
			END AS HbsAg,
			CASE WHEN p.HBSRapid = 1 THEN 'Yes' ELSE 'No'
			END AS HBSRapid,
			p.HepB_RapidCollectionDate,
			p.date_of_patient_registration,
			HBSRapidResult.LookupValue as HBSRapidResult,
			CASE WHEN p.HBSRapidPlace = 1 THEN 'Govt. Lab' ELSE 'Private Lab-PPP'
			END AS HBSRapidPlace,
			p.HBSRapidLabID,
			p.HBSRapidLabOther,
			CASE WHEN p.HBSElisa = 1 THEN 'Yes' ELSE 'No'
			END AS HBSElisa,
			p.HBSElisaDate,
			HBSElisaResult.LookupValue as HBSElisaResult,
			CASE WHEN p.HBSElisaPlace = 1 THEN 'Govt. Lab' ELSE 'Private Lab-PPP'
			END AS HBSElisaPlace,
			p.HBSElisaLabID,
			p.HBSElisaLabOther,
			CASE WHEN p.HBSOther = 1 THEN 'Yes' ELSE 'No'
			END AS HBSOther,
			p.HBSOtherName,
			p.HBSOtherDate,
			HBSOtherResult.LookupValue as HBSOtherResult,
			CASE WHEN p.HBSOtherPlace = 1 THEN 'Govt. Lab' ELSE 'Private Lab-PPP'
			END AS HBSOtherPlace,
			p.HBSOtherLabID,
			p.HBSLabOther,
			CASE WHEN p.VLHepB = 1 THEN 'Yes' ELSE 'No'
			END AS VLHepB,
			p.VLSampleCollectionDate,
			CASE WHEN p.IsVLSampleStored = 1 THEN 'Yes' ELSE 'No'
			END AS IsVLSampleStored,
			CASE WHEN p.IsVLSampleTransported = 1 THEN 'Yes' ELSE 'No'
			END AS IsVLSampleTransported,
			p.VLSCRemarks,
			CASE WHEN p.IsSampleAccepted = 1 THEN 'Yes' ELSE 'No'
			END AS IsSampleAccepted,
			p.T_DLL_01_VLC_Date,
			result.LookupValue AS T_DLL_01_VLC_Result,
			p.T_DLL_01_VLCount,
			CASE WHEN p.TreatmentRecommended = 1 THEN 'Yes' ELSE 'No'
			END AS TreatmentRecommended,
			p.BRejectionReason,
			p.BVLResultRemarks,
			pp.Prescribing_Dt,
			pp.LastTest_Dt,
			CASE WHEN pp.APRI = 1 THEN 'Yes' ELSE 'No'
			END AS APRI,
			pp.APRI_Score,
			CASE WHEN pp.FIB4 = 1 THEN 'Yes' ELSE 'No'
			END AS FIB4,
			pp.FIB4_FIB4,
			CASE WHEN pp.V1_Cirrhosis = 1 THEN 'Complicated' ELSE 'Uncomplicated'
			END AS V1_Cirrhosis,
			Cirr_VaricealBleed.LookupValue AS Cirr_VaricealBleed,
			Cirr_Ascites.LookupValue AS Cirr_Ascites,
			Cirr_Ascites.LookupValue AS Cirr_Encephalopathy,
			pp.ChildScore,
			CASE WHEN pp.Result = 1 THEN 'Compensated Cirrhosis' ELSE 'Decompensated Cirrhosis'
			END AS Result,
			CASE WHEN pp.Elevated_ALT_Level = 1 THEN 'Indecisive' WHEN pp.Elevated_ALT_Level = 2 THEN 'Yes' ELSE 'No'
			END AS Elevated_ALT_Level,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			pp.PrescribingDoctor,
			pp.Drugs_Added,
			NULL,
			NULL,
			NULL,
			pp.PrescribingDate,
			pp.PrescribingFacility,
			dis.VisitNo,
			dis.Treatment_Dt,
			NULL,
			dis.PillsDispensed,
			dis.NextVisit,
			dis2.VisitNo,
			dis2.Treatment_Dt,
			NULL,
			dis2.PillsDispensed,
			dis2.NextVisit,
			NULL,
			NULL,
			NULL,
			p.date_of_patient_registration
			FROM
			tblpatient p
			LEFT JOIN mststate s ON
			p.Session_StateID = s.id_mststate
			LEFT JOIN mstdistrict d ON
			p.Session_StateID = d.id_mststate AND p.Session_DistrictID = d.id_mstdistrict
			LEFT JOIN mstblock b ON
			p.Session_StateID = b.id_mststate AND p.Session_DistrictID = b.id_mstdistrict AND p.Block = b.id_mstblock
			LEFT JOIN mstfacility f ON
			p.id_mstfacility = f.id_mstfacility
			LEFT JOIN(
			SELECT
        *
			FROM
			mstlookup
			WHERE
			Flag = 1 AND LanguageID = 1
		) AS gender
		ON
		p.Gender = gender.LookupCode
		LEFT JOIN hepb_tblpatient pp ON
		p.PatientGUID = pp.PatientGUID
		LEFT JOIN(
		SELECT
        *
		FROM
		mstlookup
		WHERE
		Flag = 4 AND LanguageID = 1
	) AS HBSRapidResult
	ON
	p.HBSRapidResult = HBSRapidResult.LookupCode
			LEFT JOIN(
			SELECT
	        *
			FROM
			mstlookup
			WHERE
			Flag = 4 AND LanguageID = 1
		) AS HBSElisaResult
		ON
		p.HBSElisaResult = HBSElisaResult.LookupCode
				LEFT JOIN(
				SELECT
		        *
				FROM
				mstlookup
				WHERE
				Flag = 4 AND LanguageID = 1
			) AS HBSOtherResult
			ON
			p.HBSOtherResult = HBSOtherResult.LookupCode
	LEFT JOIN(
	SELECT
        *
	FROM
	mstlookup
	WHERE
	Flag = 5 AND LanguageID = 1
) result
ON
p.T_DLL_01_VLC_Result = result.LookupCode
LEFT JOIN(
SELECT
        *
FROM
`mstlookup`
WHERE
flag = 8 AND LanguageID = 1
) Cirr_VaricealBleed
ON
pp.Cirr_VaricealBleed = Cirr_VaricealBleed.LookupCode
LEFT JOIN(
SELECT
        *
FROM
`mstlookup`
WHERE
flag = 7 AND LanguageID = 1
) Cirr_Ascites
ON
pp.Cirr_Ascites = Cirr_Ascites.LookupCode
LEFT JOIN(
SELECT
        *
FROM
mstlookup
WHERE
Flag = 3 AND LanguageID = 1
) msrisk
ON
p.Risk = msrisk.LookupCode
LEFT JOIN(
SELECT
a.*,
b.LookupValue AS PillsTakenValue,
c.FirstName AS patientName,
f.FacilityCode
FROM
tblpatientdispensationb a
LEFT JOIN mstlookup b ON
a.PillsTaken = b.LookupCode AND b.Flag = 64 AND b.LanguageID = 1
LEFT JOIN tblpatient c ON
a.PatientGUID = c.PatientGUID
LEFT JOIN mstfacility f ON
a.DispensationPlace = f.id_mstfacility
WHERE
VisitNo = 1
) dis
ON
p.PatientGUID = dis.PatientGUID
LEFT JOIN(
SELECT
a.*,
b.LookupValue AS PillsTakenValue,
c.FirstName AS patientName,
f.FacilityCode
FROM
tblpatientdispensationb a
LEFT JOIN mstlookup b ON
a.PillsTaken = b.LookupCode AND b.Flag = 64 AND b.LanguageID = 1
LEFT JOIN tblpatient c ON
a.PatientGUID = c.PatientGUID
LEFT JOIN mstfacility f ON
a.DispensationPlace = f.id_mstfacility
WHERE
VisitNo = 2
) dis2
ON
p.PatientGUID = dis2.PatientGUID
LEFT JOIN (SELECT * from mstlookup where Flag=3 AND LanguageID=1) as aa 
ON FIND_IN_SET(aa.LookupCode, p.Risk)
LEFT JOIN(
SELECT
PatientGUID AS pguid
FROM
linelist
) b
ON
p.PatientGUID = b.pguid
WHERE
b.pguid IS NULL AND p.id_mstfacility = ".$row->id_mstfacility."
GROUP BY
p.id_tblpatient";
$this->db->query($query);


$update_array = array(
	"EndTime"         => date('Y-m-d H:i:s'),
	"Status"		=>'Success');
$array = array('StateID' => $row->id_mststate, 'id_mstfacility' => $row->id_mstfacility, 'STR_TO_DATE(`StartTime`,"%Y-%m-%d")' => date('Y-m-d'));
$this->db->where($array);
$this->db->update("line_list_tracker", $update_array);


}

}

}



/*End linelist update*/





}