<?php

defined('BASEPATH') or exit('Direct script access not allaowed');

Class Regimen extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index($id_mst_regimen = null)
	{

		if($id_mst_regimen != null)
		{
			$query = "select * from mst_regimen where id_mst_regimen = ".$id_mst_regimen;
			$result = $this->db->query($query)->result();

			$content['regimen_details'] = $result;
		}

		$query = "SELECT * FROM mst_regimen where is_deleted = 0";
		$result = $this->db->query($query)->result();

		$content['regimens'] = $result;


		$query = "SELECT * FROM mst_regimen where is_deleted = 0 ORDER BY regimen_name DESC";
		$result = $this->db->query($query)->result();
		$content['Regimenname'] = $result;
		// print_r($content['Regimenname']); die();

		$query = "SELECT * FROM mst_drugs where is_deleted = 0";
		$result = $this->db->query($query)->result();

		$drug_options = '';

		foreach ($result as $drug) {
			$drug_options .= '<option value="'.$drug->id_mst_drugs.'">'.$drug->drug_name.'</options>';
		}
		$content['drugs'] = $drug_options;


		$content['subview'] = 'list_regimen';
		$this->load->view('admin/main_layout', $content);
	}

	public function add($id =NULL)
	{

	$Request_Method = $this->input->server('REQUEST_METHOD');

		if($Request_Method == 'POST')
		{	

			$drugs_in_reg = $this->input->post('drugs'); 
			// print_r($drugs_in_reg);   die();           

			$regimen_name = $this->input->post('new_regimen');
           
			$this->db->where('trim(lcase(regimen_name))', trim(strtolower($regimen_name)));
			$result = $this->db->get('mst_regimen')->result();

			if(count($result) > 0)
			{
				$this->session->set_flashdata('er_msg', 'A Regimen with the same name already exists! Please create a different Regimen');

				redirect('regimen');
			}
			
			$insert_array = array(
				// "regimen_name" => trim(strtolower($regimen_name)),
				"regimen_name" => $regimen_name,
				"is_deleted"   => 0,
			);
			$this->db->insert('mst_regimen', $insert_array);
			$new_regimen_id = $this->db->insert_id();

			
			// print_r($drugs_in_reg);
			// die();

			foreach ($drugs_in_reg as $drug) {

				$regimen_drug_insert_array = array(
					"id_mst_regimen"       => $new_regimen_id,
					"id_mst_drug"          => $drug['medicine'],
					// "id_mst_drug_strength" => $drug['strength'],
				);
				$this->db->insert('mst_regimen_drugs', $regimen_drug_insert_array);
			}

			$this->session->set_flashdata('msg', 'Successfully Added Regimen');
		}

		redirect('regimen');
	}

	// public function edit()
	// {

	// }

	// public function delete($id_mst_regimen = null)
	// {	

	// 	$this->db->where('id_mst_regimen', $id_mst_regimen);
	// 	$this->db->where('is_deleted', 0);
	// 	$result = $this->db->get('mst_regimen')->result();

	// 	if(count($result) > 0)
	// 	{

	// 		$query = "update mst_regimen set is_deleted = 1 where id_mst_regimen = ".$id_mst_regimen;
	// 		$result = $this->db->query($query);

	// 		if($result == 1)
	// 		{
	// 			$this->session->set_flashdata('msg', 'Regimen Deleted Successful');
	// 		}
	// 	}

	// 	$query = "SELECT * FROM mst_regimen where is_deleted = 0";
	// 	$result = $this->db->query($query)->result();

	// 	$content['regimens'] = $result;

	// 	redirect('admin/regimen');
	// }

	public function getStrengths($drug = null)
	{
		$query = 'SELECT * FROM `mst_drug_strength` where id_mst_drug = '.$drug;
		$result = $this->db->query($query)->result();

		$strength_option = '<option value="">--Select--</option>'; 

		foreach ($result as $strength) {
			
			$strength_option .= '<option value="'.$strength->id_mst_drug_strength.'">'.$strength->strength.'</option>';	
		}

		echo $strength_option;
	}

}