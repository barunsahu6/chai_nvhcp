<?php

defined('BASEPATH') or exit('Direct script access not allaowed');

Class Drugsbuffer extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$query = "SELECT * FROM `mst_drugs` where is_deleted = 0 group by type";
		$content['drugs'] = $this->db->query($query)->result();
		$query = "SELECT * FROM `mstlookup` where Flag='90' and LookupCode!=3";
		$content['types'] = $this->db->query($query)->result();
		//pr($content['types']);
		$content['subview'] = 'list_drug_buffer';
		$this->load->view('admin/main_layout', $content);
	}
public function add_buffer()
	{	
		//echo "string";exit();
		$Request_Method = $this->input->server('REQUEST_METHOD');
		$loginData = $this->session->userdata('loginData');
		if($Request_Method == "POST")
		{

			$type = $this->input->post('type');
			if ($type>0) {
			$buffer_stocks    = $this->input->post('buffer_stock');
			$id_mst_drug_strength    = $this->input->post('id_mst_drug_strength');
			//pr($id_mst_drug_strength);exit();
			$result = true;
			
			foreach ($buffer_stocks as $key=>$buffer_stock) {
					$updateArray = array(
				"buffer_stock" => $buffer_stock,
				"updateBy" => $loginData->id_tblusers,
				 "updatedOn" => date('Y-m-d')
				);
			$where = "(id_mst_drug_strength=".$id_mst_drug_strength[$key].")";
			$this->db->where($where);
			$this->db->update('mst_drug_strength', $updateArray);
			$result=true;
				
				if($result)
				{
					$this->session->set_flashdata('msg', 'Strengths Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('er_msg', 'Error in adding Strengths');
				}


			}
			
			redirect('Drugsbuffer');
		}
		else{
			$this->session->set_flashdata('er_msg', 'Please Select Commodity');
			redirect('Drugsbuffer');
		}
	}
	}	
public function getbuffer($id_mst_drugs = null)
	{	

		$query = "SELECT * FROM mst_drugs where is_deleted=0 and type = ".$id_mst_drugs;
		$drug_details = $this->db->query($query)->result();
		
		$query = "SELECT s.*,d.*,ifnull(s.unit,'') as unit,CAST(s.strength as char)*1 as strength FROM mst_drug_strength s inner join mst_drugs d on s.id_mst_drug=d.id_mst_drugs where s.is_deleted=0 and d.type = ".$id_mst_drugs;
		$strength_details = $this->db->query($query)->result();

		echo json_encode(array("drug_details" => $drug_details, "strength_details" => $strength_details));
	}
}