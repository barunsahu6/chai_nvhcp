<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Lookup extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    
    $this->load->model('Common_Model');
    $this->load->model('Patient_Model');
    
    // $loginData = $this->session->userdata('loginData');
    // if($loginData->user_type != 1){
    //   $this->session->set_flashdata('er_msg','You are not logged-in, please login again to continue');
    //   redirect('login');  
    // }
  }

  public function index($id_mstlookup = null)
  { 
    $content['edit_flag'] = 0;

    $RequestMethod = $this->input->server('REQUEST_METHOD');

    if($RequestMethod == "POST")
    {
      $this->session->set_userdata('Flag', $this->input->post('Flag'));

      if($id_mstlookup != null)
      {
        $updateArray = array(
          'LookupValue'   => $this->input->post('lookup_name'),
        );
        $this->db->where('id_mstlookup', $id_mstlookup);
        $this->db->update('mstlookup',$updateArray);
        redirect("lookup");
      }
      else
      {
        $sql_lookupcode = "SELECT max(LookupCode) as maxcode FROM `mstlookup` where Flag = '".$this->session->userdata('Flag')."'";
        $res = $this->Common_Model->query_data($sql_lookupcode)[0];

        $sql_lookupcode = "SELECT max(Sequence) as maxsequence FROM `mstlookup`";
        $res_sequence = $this->Common_Model->query_data($sql_lookupcode)[0];

        $insertArray = array(
          'LookUpType'            => "",
          'Flag'                  => $this->session->userdata('Flag'),
          'LookupCode'            => $res->maxcode + 1,
          'LookupValue'           => $this->input->post('lookup_name'),
          'Sequence'              => $res_sequence->maxsequence + 1,
          'LanguageID'            => 1,
        );
        $this->db->insert('mstlookup', $insertArray);
      }
    }

    if($id_mstlookup != null)
    {
      $sql_details = "select * from mstlookup where id_mstlookup ='".$id_mstlookup."' ";
      $res_details = $this->Common_Model->query_data($sql_details)[0];
      $content['selected_lookup'] = $res_details;
      $content['edit_flag'] = 1;
    }

    $sql = "SELECT * FROM `mstlookup` where Flag = '".$this->session->userdata('Flag')."'";
    $res = $this->Common_Model->query_data($sql);
    $content['list_lookup'] = $res;

    $content['subview'] = 'list_lookup';
    $this->load->view('admin/main_layout', $content);
  }

  // public function delete($id_mstlookup = null)
  // {
  //  $sql = "delete from mstlookup where id_mstlookup = $id_mstlookup";

  //  $this->db->query($sql);

  //  redirect("admin/lookup");
  // }

  public function add_lookup()
  {
    // print_r($id_mstlookup); die();
    $RequestMethod = $this->input->server('REQUEST_METHOD');

    $query = "SELECT * FROM mstlookup WHERE Flag = 91";
    $content['lookup_list'] = $this->db->query($query)->result();

    if($RequestMethod == "POST")
    {
          $Operation = $this->input->post('Operation');
          $LookupValue = $this->input->post('LookupValue');
          $new_lookup = $this->input->post('new_lookup');
      // print_r($LookupValue); die();
          $query = "SELECT * FROM mstlookup WHERE Flag = $LookupValue";
          $content['lookup_list'] = $this->db->query($query)->result();
          // print_r($content['lookup_list']); 
          if ($Operation == 2) {
            $query = "SELECT max(LookupCode)+1 as LookupCode FROM mstlookup WHERE Flag = $LookupValue and LookupCode != 99";
            $LookupCode = $this->db->query($query)->result()[0]->LookupCode;

            $query = "SELECT max(Sequence)+1 as Sequence FROM mstlookup WHERE Flag = $LookupValue";
            $Sequence = $this->db->query($query)->result()[0]->Sequence;

            $insertArray = array(
              'Flag'        => $LookupValue,
              'LookupValue' => $new_lookup,
              'LookupCode'  => $LookupCode,
              'Sequence'    => $Sequence,
              'LanguageID'  => 1
            );
            $this->db->insert('mstlookup', $insertArray);
        // print_r($insertArray); die();
            redirect('Lookup/add_lookup');
          }
      }

      $content['subview'] = "add_lookup";
      $this->load->view('admin/main_layout', $content);
    }


    public function lookup_update()
    {
      if($this->input->get()){ 
        $arr = array();
        $id_mstlookup   = $this->input->get('id_mstlookup');
        $old_lookup_value   = $this->input->get('old_lookup_value');
        // print_r($UserID); die();
        $updateArr = array(
            'LookupValue' => $old_lookup_value
        );
        $this->db->WHERE('id_mstlookup', $id_mstlookup);
        $data = $this->db->update('mstlookup', $updateArr);

        

        if($data>0){
          $arr['status'] = 'true';
          $arr['fields'] = 'Successfully';
        }else{
          $arr['status'] = 'false';
          $arr['fields'] = '';
        }

        echo json_encode($arr);
      }
  }

public function get_lookupdata(){

  if($this->input->get()){
        
        $arr = array();
        $id = $this->input->get('id_mstlookup');
        $data = array(
          'id' => $id
        );


     $query = "SELECT * FROM mstlookup WHERE id_mstlookup = $id";
    $data = $this->db->query($query)->result();
    

        if($data){
          $arr['status'] = 'true';
          $arr['fields'] = json_encode($data[0]);
        }else{
          $arr['status'] = 'false';
          $arr['fields'] = '';
        }
        
        echo json_encode($arr);
      }         

}


}