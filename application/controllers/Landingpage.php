<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landingpage extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('captcha');
		
	}
	public function index()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/index');		
		$this->load->view('html/include/footer');
	}

	public function about_us()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/about_us');
		$this->load->view('html/include/footer');
	}


	public function contact()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/contact');
		$this->load->view('html/include/footer');
	}
	public function equipment_drugskits(){

		$this->load->view('html/include/header');
		$this->load->view('html/equipment_drugskits');
		$this->load->view('html/include/footer');
	}

	public function Training_Manual(){

		$this->load->view('html/include/header');
		$this->load->view('html/training_manual');
		$this->load->view('html/include/footer');
	}


	public function feedback()
	{	

		$this->load->library('Phpmailer');
		$RequestMethod = $this->input->server('REQUEST_METHOD');
		


		if($RequestMethod == 'POST')
		{

			

			 $captcha_insert = $this->security->xss_clean($this->input->post('captcha'));
          $contain_sess_captcha = $this->session->userdata('valuecaptchaCode');
            
            if ($captcha_insert === $contain_sess_captcha) {
            	
            }else{
            	$this->session->set_flashdata('er_msg', 'Please enter valid captcha');
			redirect(current_url());
            }



			

			$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[50]|min_length[1]|xss_clean');
				$this->form_validation->set_rules('Subject', 'Subject', 'trim|required|max_length[50]|min_length[1]|xss_clean');
				$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[100]|min_length[3]|xss_clean');
				$this->form_validation->set_rules('Feedback', 'Feedback', 'trim|required|max_length[200]|min_length[5]|xss_clean');
				

				if ($this->form_validation->run() == FALSE) {

					$arr['status'] = 'false';
					 $this->session->set_flashdata("tr_msg",validation_errors());
				}else{

			$insert_array['name'] = $this->security->xss_clean($this->input->post('name'));
			$insert_array['subject'] = $this->security->xss_clean($this->input->post('Subject'));
			$insert_array['email'] = $this->security->xss_clean($this->input->post('email'));			
			$insert_array['category'] = $this->security->xss_clean($this->input->post('category'));
			$insert_array['feedback'] = $this->security->xss_clean($this->input->post('Feedback'));

			$this->db->insert('feedback', $insert_array);
			
			$to = "nvhcpfeedback2019@gmail.com";
			$subject = "Feedback Email";
			$message = "
			<html>
			<head>
			<title>Feedback NVHCP</title>
			</head>
			<body>
			<p>User Details</p>
			<table>
			<tr>
			<th>Name</th>
			<th>Subject</th>
			<th>Email</th>
			<th>Category</th>
			<th>Feedback</th>
			</tr>
			<tr>
			<td>'".$insert_array['name']."'</td>
			<td>'".$insert_array['subject']."'</td>
			<td>'".$insert_array['email']."'</td>
			<td>'".$insert_array['category']."'</td>
			<td>'".$insert_array['feedback']."'</td>
			</tr>
			</table>
			</body>
			</html>
			";


			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
			$headers .= 'From: nvhcpfeedback2019@gmail.com' . "\r\n";
//$headers .= 'Cc: barunkrsahu@gmail.com' . "\r\n";

			mail($to,$subject,$message,$headers);

			$this->session->set_flashdata('tr_msg', 'Thank you for your feedback. We will get back to you shortly');
			//redirect('feedback', 'refresh');
			redirect(current_url());

}
		}

		/*captcha*/
         $config = array(
            'img_url' => base_url() . 'image_for_captcha/',
            'img_path' => 'image_for_captcha/',
           'font_path' => base_url() . 'fount/Verdana.ttf',
			 'font_size'     => 16,
			'img_width' => 130,
			'img_height' => 40,
			'word_length'   => 5,
			'expiration' => 7200,
			 'img_id'        => 'Imageid',
        	'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
			 'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
        )
        );
        // pr($config);exit();
        $captcha = create_captcha($config);
        //pr($captcha);
        $this->session->unset_userdata('valuecaptchaCode');
        $this->session->set_userdata('valuecaptchaCode', $captcha['word']);
        $content['captchaImg'] = $captcha['image'];
        /*end captcha*/

		$this->load->view('html/include/header');	
		$this->load->view('html/feedback',$content);
		$this->load->view('html/include/footer');
	}
	

	public function development()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/Development');
		$this->load->view('html/include/footer');
	}


	public function Guidelines()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/Guidelines');
		$this->load->view('html/include/footer');
	}


	public function iec()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/iec');
		$this->load->view('html/include/footer');
	}

	public function term()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/term');
		$this->load->view('html/include/footer');
	}

	public function welcome1()
	{	
		//$this->load->view('html/include/header');
		$this->load->view('html/welcome1');
		//$this->load->view('html/include/footer');
	}

	public function welcome()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/welcome');
		$this->load->view('html/include/footer');
	}



	public function Launch_of_National()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/Launch-of-National-Viral-Hepatitis-Control-Program');
		$this->load->view('html/include/footer');
	}

	public function List_of_Model()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/List-of-Model-Treatment_Centres');
		$this->load->view('html/include/footer');
	}	


	public function landingpage_national()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/National-Training-of-Trainers');
		$this->load->view('html/include/footer');
	}
	

	public function policies()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/policies');
		$this->load->view('html/include/footer');
	}

	public function rrrrrrrrrrrrrrrr()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/rrrrrrrrrrrrrrrr');
		$this->load->view('html/include/footer');
	}


	public function Sensitization_workshop()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/Sensitization-workshop');
		$this->load->view('html/include/footer');
	}

	public function index23()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/index_23');
		$this->load->view('html/include/footer');
	}	

	public function faqs()
	{	
		$this->load->view('html/include/header');
		$this->load->view('html/faqs');
		$this->load->view('html/include/footer');
	}

	public function abc()
	{	
		
		
	}

	public function List_of_Nodal_officers(){

		$this->load->view('html/include/header');
		$this->load->view('html/List_of_Nodal_officers');
		$this->load->view('html/include/footer');
	}


}
