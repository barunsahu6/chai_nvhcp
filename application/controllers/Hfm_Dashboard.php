<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hfm_Dashboard extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Common_Model');
		$this->load->helper('common');
		$this->load->model('Hfm_Dashboard_Model');
//error_reporting(0);
		$loginData = $this->session->userdata('loginData');

		if($loginData == null)
		{
			redirect('login');
		}
	}



	public function index($ids = null)
	{
		//error_reporting(0);
		$loginData = $this->session->userdata('loginData');
	//echo "<pre>";print_r($loginData);exit();
if( ($loginData) && $loginData->user_type != '1' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_wherep = " 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' ";
			}


		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

		if($REQUEST_METHOD == 'POST')
		{
			if ($this->security->xss_clean($this->input->post('submit')=='save')) {
				
				$id_mststate=$this->security->xss_clean($this->input->post('id_mststate'));
				$target=$this->security->xss_clean($this->input->post('target'));
				$target_tc=$this->security->xss_clean($this->input->post('target_tc'));
				//pr($target);exit();
				$mtc_established=$this->security->xss_clean($this->input->post('established'));
				$districts_with_tc=$this->security->xss_clean($this->input->post('districts_with_tc'));
				$tc_established=$this->security->xss_clean($this->input->post('total_tc'));
				//pr($tc_established);exit();
				$filter['year']= $this->security->xss_clean($this->input->post('year'));
				foreach ($id_mststate as $key => $value) {
					$where=array('id_mststate'=>$value,
								'financial_year'=>$filter['year'],);
					$this->db->where($where);

					$data=array(
					'target_mtc' => isset($target[$key]) ? $target[$key] : 0,
					'target_tc' => isset($target_tc[$key]) ? $target_tc[$key] : 0,
					'established_mtc' =>  isset($mtc_established[$key]) ? $mtc_established[$key] : 0,	
					'tc_established'=>isset($tc_established[$key]) ? $tc_established[$key] : 0,
					'district_with_tc' =>isset($districts_with_tc[$key]) ? $districts_with_tc[$key] : 0,
					'updated_on' =>date('Y-m-d'),
					'updated_by' =>$loginData->id_tblusers,
					 );

					$this->db->update('tbl_hfm_report',$data);
					
				}
				$this->session->set_flashdata('tr_msg','Information Updated Successfully');
					//pr($data);exit();
					$this->session->set_userdata('hfm_filter', $filter);
				$content['states_hfm_data']=$this->Hfm_Dashboard_Model->get_hfm_report_data();
					redirect('hfm_dashboard');

			}
			elseif ($this->security->xss_clean($this->input->post('submit')=='search')) {
			$filter['year']= $this->security->xss_clean($this->input->post('year'));
				$this->session->set_userdata('hfm_filter', $filter);
			$content['states_hfm_data']=$this->Hfm_Dashboard_Model->get_hfm_report_data();
			}

		}
		else
		{	
		if (date('m', strtotime(date('Y-m-d'))) < 4) {
			$filter['year'] =(date('Y')-1)."-".date("Y");
		}
		else{
			$filter['year'] =(date('Y'))."-".(date("Y")+1);
		}
		$sql="Select * from tbl_hfm_report where financial_year=?";
		$hfm_data=$this->db->query($sql,[$filter['year']])->result();
		//pr($hfm_data);exit();
		if(empty($hfm_data)){
			$hfm_repo_insert=$this->Hfm_Dashboard_Model->get_mtc_tc_info_first_time_only();
			foreach ($hfm_repo_insert as $value) {


				$data = array('id_mststate' =>$value->id_mststate ,
					'established_mtc' => $value->mtc_established,	
					'district_with_tc' =>$value->districts_with_tc,
					'tc_established'=>$value->tc_established,
					'id_tblusers' =>$loginData->id_tblusers,
					'financial_year'=>$filter['year'],

				);
				$this->db->insert('tbl_hfm_report',$data);

			}
			$this->session->set_userdata('hfm_filter', $filter);
			$content['states_hfm_data']=$this->Hfm_Dashboard_Model->get_hfm_report_data();


		}
		else{
			$this->session->set_userdata('hfm_filter', $filter);
			$content['states_hfm_data']=$this->Hfm_Dashboard_Model->get_hfm_report_data();
		}

		}

		$content['subview'] = 'hfm_dashboard';
		$this->load->view('admin/main_layout', $content);
	}

public function hfm_report(){
	
	$loginData = $this->session->userdata('loginData');
	if( ($loginData) && $loginData->user_type != '1' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

		if($REQUEST_METHOD == 'POST')
		{
			 $filter['startdate']=$this->security->xss_clean(timestampD($this->input->post('startdate')));
			$filter['enddate1']=$this->security->xss_clean(timestampD($this->input->post('enddate')));
			$filter['enddate']        = $this->security->xss_clean(date($this->input->post('enddate')));
			if ($filter['enddate']==date('d-m-Y')) {
			$filter['enddate']        = $this->security->xss_clean(timestampD(timeStampShow('-1 day',$filter['enddate'])));
			}
			else{
			$filter['enddate']        = $this->security->xss_clean(timestampD($this->input->post('enddate')));
			}

			

		}
		else{
			$filter['startdate']=date('2019-04-01');
			$filter['enddate']=timestampD(timeStampShow('-1 day',date('Y-m-d')));
			$filter['enddate1']=date('Y-m-d');
		}
		if (date('m', strtotime(date($filter['startdate']))) < 4) {
	$filter['year1'] =date('Y',strtotime("-1 year",strtotime($filter['startdate'])))."-".date("Y", strtotime($filter['startdate']));
			}
		else{
			$filter['year1'] =date('Y',strtotime($filter['startdate']))."-".date("Y",strtotime("+1 year",strtotime($filter['startdate'])));
		}
		if (date('m', strtotime(date($filter['enddate']))) < 4) {
			$filter['year2'] =date('Y',strtotime("-1 year",strtotime($filter['enddate'])))."-".date("Y", strtotime($filter['enddate']));
			}
		else{
			$filter['year2'] =date('Y',strtotime($filter['enddate']))."-".date("Y",strtotime("+1 year",strtotime($filter['enddate'])));
		}

		$this->session->set_userdata('hfm_search_filter', $filter);
		$content['static_hfm_data_mtc']=$this->Hfm_Dashboard_Model->hfm_report_summary(1);
		$content['filtered_hfm_data_mtc']=$this->Hfm_Dashboard_Model->filtered_hfm_report(1);
		$content['static_hfm_data_tc']=$this->Hfm_Dashboard_Model->hfm_report_summary(0);
		$content['filtered_hfm_data_tc']=$this->Hfm_Dashboard_Model->filtered_hfm_report(0);
		$content['static_hepc_data']=$this->Hfm_Dashboard_Model->hepatitis_treatment();
		$content['filtered_hepc_data']=$this->Hfm_Dashboard_Model->hepatitis_treatment(1);

		$content['subview'] = 'hfm_report';
		$this->load->view('admin/main_layout', $content);
}	
public function tc_mtc_progress_reports(){
	$loginData = $this->session->userdata('loginData');
	if( ($loginData) && $loginData->user_type != '1' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

		if($REQUEST_METHOD == 'POST')
		{
			 $filter['startdate']=$this->security->xss_clean(timestampD($this->input->post('startdate')));
			$filter['enddate1']=$this->security->xss_clean(timestampD($this->input->post('enddate')));
			$filter['enddate']        = $this->security->xss_clean(date($this->input->post('enddate')));
			if ($filter['enddate']==date('d-m-Y')) {
			$filter['enddate']        = $this->security->xss_clean(timestampD(timeStampShow('-1 day',$filter['enddate'])));
			}
			else{
			$filter['enddate']        = $this->security->xss_clean(timestampD($this->input->post('enddate')));
			}

			

		}
		else{
			$filter['startdate']=date('2019-04-01');
			$filter['enddate']=timestampD(timeStampShow('-1 day',date('Y-m-d')));
			$filter['enddate1']=date('Y-m-d');
		}
		if (date('m', strtotime(date($filter['startdate']))) < 4) {
	$filter['year1'] =date('Y',strtotime("-1 year",strtotime($filter['startdate'])))."-".date("Y", strtotime($filter['startdate']));
			}
		else{
			$filter['year1'] =date('Y',strtotime($filter['startdate']))."-".date("Y",strtotime("+1 year",strtotime($filter['startdate'])));
		}
		if (date('m', strtotime(date($filter['enddate']))) < 4) {
			$filter['year2'] =date('Y',strtotime("-1 year",strtotime($filter['enddate'])))."-".date("Y", strtotime($filter['enddate']));
			}
		else{
			$filter['year2'] =date('Y',strtotime($filter['enddate']))."-".date("Y",strtotime("+1 year",strtotime($filter['enddate'])));
		}

		$this->session->set_userdata('hfm_search_filter', $filter);
		$content['filtered_hfm_data_mtc']=$this->Hfm_Dashboard_Model->filtered_hfm_report(1);
		$content['filtered_hfm_data_tc']=$this->Hfm_Dashboard_Model->filtered_hfm_report(0);
		$content['subview'] = 'tc_mtc_progress_report';
		$this->load->view('admin/main_layout', $content);
}	
}