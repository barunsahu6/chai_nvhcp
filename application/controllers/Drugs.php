<?php

defined('BASEPATH') or exit('Direct script access not allaowed');

Class Drugs extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('common');
		$this->load->model('Common_Model');
	}

	public function index()
	{
		$query = "SELECT * FROM `mst_drugs` where is_deleted = 0";
		$content['drugs'] = $this->db->query($query)->result();
		$query="SELECT * FROM `mstlookup` WHERE flag='58'";
		$content['item_mst_look']=$this->db->query($query)->result();
		$query="SELECT * FROM `mstlookup` WHERE flag='92'";
		$content['drug_type']=$this->db->query($query)->result();
		//print_r($content['item_mst_look']);
		$content['subview'] = 'list_drugs';
		$this->load->view('admin/main_layout', $content);
	}

	public function add()
	{	
		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST')
		{

			$this->db->where('lcase(drug_name)', strtolower($this->input->post('drug_name')));
			$result = $this->db->get('mst_drugs')->result();

			if(count($result) > 0)
			{
				$this->session->set_flashdata('er_msg', 'A Drug With This Name Already Exists, Please Choose a different name');
			}
			else
			{
				$insert_array = array(
						"drug_name"  => $this->input->post('drug_name'),
						"is_deleted" => 0,
						"type" => $this->input->post('type'),
						"category" => $this->input->post('category'),
						"drug_abb"   => strtoupper($this->input->post('drug_abb')),
					);

				$this->db->insert('mst_drugs', $insert_array);
				
				$this->session->set_flashdata('msg', 'Drug Added Successfully');
			}
		}

		redirect('drugs');
	}

	public function edit($id_mst_drugs = null)
	{	
		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST')
		{	
			$update_array = array(
						"drug_name"  => $this->input->post('drug_name'),
						"is_deleted" => 0,
						"lead_time" => $this->input->post('lead_time'),
						"drug_abb"   => strtoupper($this->input->post('drug_abb')),
					);

				$result = $this->db->update('mst_drugs', $update_array, array("id_mst_drugs" => $id_mst_drugs));
				// echo $id_mst_drugs;
				// echo $result;
			// print_r($update_array); die();
				$this->session->set_flashdata('msg', 'Drug Updated Successfully');

				redirect('drugs');
		}

		$query = "select * from mst_drugs where is_deleted = 0 and id_mst_drugs = ".$id_mst_drugs;
		$result = $this->db->query($query)->result();

		$content['drug_details'] = $result;
		//prd($content['drug_details']);
		$query = "SELECT * FROM `mst_drugs` where is_deleted = 0";
		$content['drugs'] = $this->db->query($query)->result();
		$query="SELECT * FROM `mstlookup` WHERE flag='58'";
		$filter['item_mst_look']=$this->db->query($query)->result();
		$query="SELECT * FROM `mstlookup` WHERE flag='92'";
		$filter['drug_type']=$this->db->query($query)->result();

		$content['subview'] = 'list_drugs';
		$this->load->view('admin/main_layout', $content);
	}

	public function delete($id_mst_drugs = null)
	{	
		$query = "update mst_drugs set is_deleted = 1 where id_mst_drugs = ".$id_mst_drugs;
		$this->db->query($query);

		$this->session->set_flashdata('msg', 'Drug Deleted Successfully');

		redirect('drugs');
	}
public function add_strength()
	{	
		//echo "string";exit();
		$Request_Method = $this->input->server('REQUEST_METHOD');
		$loginData = $this->session->userdata('loginData');
		if($Request_Method == "POST")
		{

			$id_mst_drugs = $this->input->post('id_mst_drugs');
			if ($id_mst_drugs>0) {
				$where = "(id_mst_drug=".$id_mst_drugs.")";
			$this->db->where($where);
			$this->db->update('mst_drug_strength', array('is_deleted'=>1));
			$strengths    = $this->input->post('drug_strength');
			//pr($strengths);exit();
			$type    = $this->input->post('type_val');
			//pr($type);exit();
			$unit=($type==1 ? "mg" : ($type==2 ? NULL : "ml"));
			$buffer_stock    = $this->input->post('buffer_stock');

			$result = true;
			
			foreach ($strengths as $strength) {

				
				$query="SELECT count(id_mst_drug_strength) as count FROM `mst_drug_strength` where strength=? and id_mst_drug=?";
						$count=$this->db->query($query,[$strength,$id_mst_drugs])->result();
						//echo $count[0]->count;exit();
				if($count[0]->count>0){
					$updateArray = array(
				"id_mst_drug" => $id_mst_drugs,
				"strength"    => $strength,
				"unit"		  => $unit,
				"is_deleted"    => 0,
				"updateBy" => $loginData->id_tblusers,
				 "updatedOn" => date('Y-m-d')
				);
			$where = "(strength='".$strength."' and id_mst_drug=".$id_mst_drugs.")";
			$this->db->where($where);
			$this->db->update('mst_drug_strength', $updateArray);
			$result=true;
						}
						else{
							// /echo "hello";exit();
							$insert_array = array(
					"id_mst_drug" => $id_mst_drugs,
					"strength"    => $strength,
					"unit"		  => $unit,
					"buffer_stock"=> $buffer_stock,
					"is_deleted"    => 0,
				   "createBy" => $loginData->id_tblusers,
				   "createdOn" => date('Y-m-d'),
				);
							//pr($insert_array);exit();
							$result = $this->db->insert('mst_drug_strength', $insert_array);
						}
				

				if($result)
				{
					$this->session->set_flashdata('msg', 'Strengths Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('er_msg', 'Error in adding Strengths');
				}


			}
			
			redirect('drugs');
		}
		else{
			$this->session->set_flashdata('er_msg', 'Please Add/Select Commodity before its Strength');
			redirect('drugs');
		}
	}
	}	
public function getStrengths($id_mst_drugs = null)
	{	

		$query = "SELECT * FROM mst_drugs where is_deleted=0 and id_mst_drugs = ".$id_mst_drugs;
		$drug_details = $this->db->query($query)->result();
		
		$query = "SELECT * FROM mst_drug_strength where is_deleted=0 and id_mst_drug = ".$id_mst_drugs;
		$strength_details = $this->db->query($query)->result();

		echo json_encode(array("drug_details" => $drug_details, "strength_details" => $strength_details));
	}
}