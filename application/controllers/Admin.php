<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends CI_Controller {

    private $patientFilters;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_Model');

        $loginData = $this->session->userdata('loginData');
        if($loginData == null)
        {
            redirect('login');
        }

        $this->patientFilters = $this->session->userdata('patientFilters');
    }

    public function index()
    {
        $loginData = $this->session->userdata('loginData');

        $sql = "SELECT * FROM tbl_login_log";   
        $content['admin_details'] = $this->Common_Model->query_data($sql);
        $content['subview'] = 'admin_details';
        $this->load->view('pages/main_layout', $content);

    }
}

//End of file