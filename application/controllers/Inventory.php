<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$loginData = $this->session->userdata('loginData');
		$this->load->helper('common');
		$this->load->model('Common_Model');
		
		$this->load->library('form_validation');
		$this->load->model('Inventory_Model');
		if($loginData == null)
		{
			redirect('login');
		}
	}

	public function index($flag=NULL)
	{
		// die('heh');
		$loginData = $this->session->userdata('loginData');
		$query="SELECT parent_facility as approving_facility FROM `mstfacility` WHERE id_mstfacility=?";
			$approving_facility=$this->db->query($query,[$loginData->id_mstfacility])->result()[0];
		$this->session->set_userdata("approving_facility",$approving_facility);
		$content['new_recipt_notification'] =$this->Inventory_Model->count_new_receipt();
		$content['new_relocation_request'] =$this->Inventory_Model->count_new_relocation_request();
		$query="SELECT inventory_lock as lock_days,bare_minimum_stock FROM `mstappstateconfiguration` WHERE id_tblappstateconfiguration=?";
			$content['inventory_lock']=$this->db->query($query,[1])->result();
			//pr($content['inventory_lock']);

		if( ($loginData) && $loginData->user_type != '2' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
			$query="SELECT * FROM `mstlookup` WHERE flag='58'";
			$filter['item_mst_look']=$this->db->query($query)->result();
			//pr($filter['item_mst_look']);
		if($REQUEST_METHOD == 'POST')
		{
			$filter['Start_Date'] = timeStamp($this->input->post('startdate'));
			$filter['End_Date'] = timeStamp($this->input->post('enddate'));
			$filter['year'] = $this->input->post('year');
			 $filter['item_type']=$this->input->post('item_type');
			 if ($flag=='U') {
			 	 $filter['utilization_filter']=$this->input->post('utilization_filter');
			 }
		}	
		else
		{
			$filter['Start_Date'] = timeStamp(timeStampShow(date('Y-01-01')));
			$filter['End_Date'] = timeStamp(timeStampShow(date('Y-m-d')));
			$filter['item_type']='';
			 $year = date("Y",strtotime($filter['Start_Date']));
		 $year1 = date("Y",strtotime($filter['End_Date']));
		
		if (date('m', strtotime(($filter['End_Date']))) < 4) {
			 $filter['year'] =($year-1)."-".$year1;
			  $content['start_date']=date("01-04-".($year-1));
		}
		else{
			 if ($year==$year1) {
		 	$year1+=1;
		 }
			 $filter['year'] =($year)."-".$year1;
			 $content['start_date']=date("01-04-".$year);
			 //die("hi");
		}
		$filter['Start_Date']=$content['start_date'];
			
		if ($flag=='U') {
			 	 $filter['utilization_filter']=3;
			 }
		}
		$content['items']=$this->Inventory_Model->get_items();

		//
		//echo date("01-04-".$year)."  ";
		//echo $filter['year'];
		//echo $filter['End_Date'];
		$this->session->set_userdata('invfilter',$filter);	
		if($flag=='R'){	
		$query="SELECT FacilityCode FROM `mstfacility` WHERE id_mstfacility=?";	
		$content['FacilityCode']=$this->db->query($query,[$loginData->id_mstfacility])->result();	
			$content['inventory_detail_all'] =$this->Inventory_Model->get_receiving_data();
			$content['filtered_arr'] =$this->Inventory_Model->get_receiving_data('group');
			//pr($content['inventory_detail_all']);
			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
			$content['Acceptance_Status']=$this->db->query($query,['76'])->result();

			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
			$content['source_name']=$this->db->query($query,['77'])->result();

			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
			$content['reason_for_rejection']=$this->db->query($query,['78'])->result();

			$query="SELECT inventory_id,indent_num FROM `tbl_inventory` where flag=?";
						$content['indent_num']=$this->db->query($query,['I'])->result();

				$is_notification = "((((id_mstfacility =".$loginData->id_mstfacility." and Flag='I' and is_temp='0') or (transfer_to =".$loginData->id_mstfacility." and Flag='L')) AND is_deleted='0'))";
				$notification_data=array('is_notification' => '1');
				$this->db->where($is_notification);
				$this->db->update('tbl_inventory',$notification_data);
			/*$query="SELECT * FROM `tbl_inventory` where flag=? and transfer_to=? and relocation_status=? and is_deleted=?";*/
						$content['new_request']=$this->Inventory_Model->get_new_request();
				//pr($content['inventory_detail_all']);	
			$content['subview']           = "inventory";
			$this->load->view('inventory/main_layout', $content);
		}
		else if($flag=='L'){		
			$query="SELECT FacilityCode FROM `mstfacility` WHERE id_mstfacility=?";	
		$content['FacilityCode']=$this->db->query($query,[$loginData->id_mstfacility])->result();	
			$content['inventory_detail_all'] =$this->Inventory_Model->get_relocation_data($flag);
			//pr($content['inventory_detail_all']);
			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['Acceptance_Status']=$this->db->query($query,['76'])->result();

			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['source_name']=$this->db->query($query,['77'])->result();

			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['reason_for_rejection']=$this->db->query($query,['78'])->result();

			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['relocation_status']=$this->db->query($query,['80'])->result();			

			$query="SELECT inventory_id,indent_num FROM `tbl_inventory` where flag=?";
						$content['indent_num']=$this->db->query($query,['I'])->result();
				$content['relocation_items']=$this->Inventory_Model->get_items(NULL,NULL,'L');
				//$content['new_request']=$this->Inventory_Model->get_new_relocation_request();
				//pr($content['inventory_detail_all']);exit();
				$is_notification = "((transfer_to ='".$loginData->id_mstfacility."' and Flag ='I' and is_deleted='0' and (relocation_status is null or relocation_status=0) and ((indent_status is not null or indent_status is null or indent_status=0) and (indent_accept_date is not null or indent_accept_date!='0000-00-00'))) || (transfer_to ='".$loginData->id_mstfacility."' and (Flag ='R' or Flag='I') and is_temp='1' and is_deleted='0'))";
				$notification_data=array('is_notification' => '1');
				$this->db->where($is_notification);
				$this->db->update('tbl_inventory',$notification_data);
			$content['subview']           = "relocation";
			$this->load->view('inventory/main_layout', $content);
		}
		else if($flag=='U'){		
			$query="SELECT FacilityCode FROM `mstfacility` WHERE id_mstfacility=?";	
			$content['FacilityCode']=$this->db->query($query,[$loginData->id_mstfacility])->result();
			$query="SELECT d.type FROM `mst_drugs` d left join `mst_drug_strength` s on d.id_mst_drugs=s.id_mst_drug WHERE (id_mst_drug_strength=?)";	
			$content['type']=$this->db->query($query,[$filter['item_type']])->result();
			//pr($content['type']);
			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=? and Sequence!=3";
			$content['utilization_purpose']=$this->db->query($query,['79'])->result();
			//pr($content['utilization_purpose']);
			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
			$content['utilization_purpose_hbv']=$this->db->query($query,['79'])->result();

		$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['utilization_filter']=$this->db->query($query,['82'])->result();
		
		 	 $content['inventory_detail_drugkit'] =$this->Inventory_Model->get_no_utilization_data($filter['utilization_filter'],NULL,'i.type in(1,2)');
		 	 $content['inventory_detail_hbv'] =$this->Inventory_Model->get_no_utilization_data($filter['utilization_filter'],NULL,'i.type=3 ');
		 	 $content['inventory_detail_hbig'] =$this->Inventory_Model->get_no_utilization_data($filter['utilization_filter'],NULL,'i.type=4 ');
		 	 $content['filtered_arr'] =$this->Inventory_Model->get_no_utilization_data($filter['utilization_filter'],'group',' 1');
		 	// $content['abcd'] =$this->Inventory_Model->get_inventory_details('U');
		
		//pr($content['inventory_detail_hbv']);

		$content['subview']           = "stock_utilization";
		$this->load->view('inventory/main_layout', $content);
		}
else if($flag=='W'){		
			$content['inventory_details'] =$this->Inventory_Model->get_inventory_details($flag);
			$content['subview']           = "Wastage_Missing_Report";
			$this->load->view('inventory/main_layout', $content);
		}
		else if($flag=='I'){	
		$query="SELECT FacilityCode FROM `mstfacility` WHERE id_mstfacility=?";	
		$content['FacilityCode']=$this->db->query($query,[$loginData->id_mstfacility])->result();
		$content['inventory_detail_all'] =$this->Inventory_Model->get_indent_data();
		//pr($content['inventory_detail_all']);exit();
		$content['filtered_arr']=$this->Inventory_Model->get_indent_data('group');
		
		$content['Inventory_Repo']=$this->Inventory_Model->get_lab_indent_forcast();
		$content['filtered_arr1']=$this->Inventory_Model->get_lab_indent_forcast(NULL,NULL,NULL,'group');
		//pr($content['Inventory_Repo']);
		$content['subview']           = "stock_indent_report";
		$this->load->view('inventory/main_layout', $content);
		}
		else if($flag=='F'){	
		$query="SELECT FacilityCode FROM `mstfacility` WHERE id_mstfacility=?";	
		$content['FacilityCode']=$this->db->query($query,[$loginData->id_mstfacility])->result();
		$content['inventory_detail_all'] =$this->Inventory_Model->get_inventory_details($flag);
		//pr($content['inventory_detail_all']);exit();
		$content['filtered_arr']=$this->Inventory_Model->get_inventory_details($flag,'group');
		//pr($content['failure_items']);
		$content['subview']           = "stock_failure";
		$this->load->view('inventory/main_layout', $content);
		}
		else{
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			
		}
	}
public function Stock_indent(){
		//echo $inventory_id;
						$loginData = $this->session->userdata('loginData');
						if( ($loginData) && $loginData->user_type != '2' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
						$this->load->library('form_validation');
						$this->load->model('Inventory_Model');
						//echo "<pre>";print_r($loginData);
						
						if( ($loginData) && $loginData->user_type == '1' ){
						$sess_where = " 1";
						}
						elseif( ($loginData) && $loginData->user_type == '2' ){
						
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
						}
						elseif( ($loginData) && $loginData->user_type == '3' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
						}
						elseif( ($loginData) && $loginData->user_type == '4' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
						}
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						$State_ID=$loginData->State_ID;
						$DistrictID=$loginData->DistrictID;
						$id_mstfacility=$loginData->id_mstfacility;
						if($loginData->State_ID==''||$loginData->State_ID==NULL){
						 $State_ID=0;
						}
						if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
						$DistrictID=0;
						}
						if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
						$id_mstfacility=0;
						}
						//print_r($loginData);
						if ($this->security->xss_clean($this->input->post('save'))=='save') {

						$item_id=$this->security->xss_clean($this->input->post('item_name'));

						$inventory_id=$this->security->xss_clean(strip_tags($this->input->post('inventory_id')));
						 $this->form_validation->set_rules('quantity', 'quantity', 'trim|required|numeric|xss_clean|greater_than[0]');
						  $this->form_validation->set_rules('item_name', 'Item Name', 'trim|required|numeric|xss_clean');
					 $this->form_validation->set_rules('indent_num', 'indent Number', 'trim|required|xss_clean'); 
					 $this->form_validation->set_rules('indent_remark', 'indent Remark', 'trim|max_length[100]|xss_clean');

					if ($this->form_validation->run() == FALSE) {

						$arr['status'] = 'false';
						$this->session->set_flashdata("tr_msg",validation_errors());
						//echo("arg1");
						redirect('Inventory/index/I');
					}else{
						$item_name_ar=$this->Inventory_Model->get_items($item_id);
						
						$item_name=$item_name_ar[0]->id_mst_drugs;
						$item_type=$item_name_ar[0]->type;

						if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$item_name_ar[0]->strength*$this->security->xss_clean(strip_tags($this->input->post('quantity')));
						}
						else{
						
						$screening_tests=0;
						}
						$insert_array = array(  
						'id_mst_drugs'  =>$item_id,
						'drug_name'  =>$item_name,
						'type'=>$item_type,
						'indent_date'=>$this->security->xss_clean(strip_tags(timeStamp($this->input->post('indent_date')))),
						'indent_num'=>$this->security->xss_clean(strip_tags($this->input->post('indent_num'))),
						'indent_remark'=>$this->security->xss_clean(strip_tags($this->input->post('indent_remark'))),
						'quantity'     => $this->security->xss_clean(strip_tags($this->input->post('quantity'))),
						'quantity_screening_tests'     => $screening_tests, 
						'id_mststate'   =>  $State_ID,
						'id_mstdistrict' => $DistrictID,
						'id_mstfacility'     => $id_mstfacility,
						'approving_facility'     => $this->session->userdata('approving_facility')->approving_facility,
						'indent_status'=>0,
						'from_to_type'=>2,
						'flag'=>	'I',
						'is_deleted'=>'0',
						'current_status'=>1
						);
						
						//pr($data);
						//echo $inventory_id;exit;
						if ($inventory_id!=NULL) {
						$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->where('inventory_id',$inventory_id);
						$this->db->update('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Indent updated Successfully');
						redirect('Inventory/index/I');
						}
						else{
						$array2 = array("id_tblusers" => $loginData->id_tblusers, "CreatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->insert('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Indent Recorded Successfully');
						redirect('Inventory/index/I');
						}	
						}
					}
					//redirect('Inventory/index/I');
				}
			
	}
function get_rem_item_name(){
	$inventory_id=$this->input->get('inventory_id',TRUE);
	$is_manual=$this->input->get('is_manual',TRUE);
	$item_name=$this->input->get('item_name',TRUE);
	$loginData = $this->session->userdata('loginData');
	if ($is_manual==null or $is_manual=='' || $is_manual=='m') {

	if ($inventory_id!="" || $inventory_id!=NULL) {
		//echo 'if';
		$data['get_rem_items']=$this->Inventory_Model->get_items($item_name);
	}
	else{
		//echo 'else';
		$data['get_rem_items']=$this->Inventory_Model->get_rem_item_name();
	}
}
elseif ($is_manual=='n') {
$data['get_rem_items']=$this->Inventory_Model->get_items($item_name);
}
echo json_encode($data);
						
}
function get_rem_indent_item(){

	$inventory_id=$this->input->get('inventory_id',TRUE);
	$is_manual=$this->input->get('is_manual',TRUE);
	$item_name=$this->input->get('item_name',TRUE);
	$from_to_type=$this->input->get('from_type',TRUE);
	$loginData = $this->session->userdata('loginData');
	

	if ($from_to_type==1 || $from_to_type==2) {
		$data['get_rem_items']=$this->Inventory_Model->get_rem_indent_item($is_manual,$from_to_type);
	}
	elseif ($from_to_type==3 || $from_to_type==4) {
		$data['get_rem_items']['data']=$this->Inventory_Model->get_items(NULL,NULL,NULL,$from_to_type);
	}
	else{
		$data['get_rem_items']=array();
	}

echo json_encode($data);
						
}

function get_sequence_no($drug_name=NULL,$indent_date=NULL){
	$drug_name_val = $this->input->get('drug_name',TRUE);
	$indent_date1 =timeStamp($this->input->get('indent_date',TRUE));
	$loginData = $this->session->userdata('loginData');
	$query="SELECT IFNULL(count(id_mst_drugs),0) as count FROM tbl_inventory WHERE id_mst_drugs=? and indent_date=? and id_mstfacility=? and flag=? and is_deleted=? and is_temp='0'";
							$data['item_sequence']=$this->db->query($query,[$drug_name_val,$indent_date1,$loginData->id_mstfacility,'I','0'])->result();
	echo json_encode($data);						
}

function get_batch_sequence(){
	$drug_name_val = $this->input->get('drug_name',TRUE);
	$Entry_Date =timeStamp($this->input->get('Entry_Date',TRUE));
	$loginData = $this->session->userdata('loginData');
	$query="SELECT IFNULL(count(issue_num),0) as issue_count,IFNULL(count(batch_num),0) as batch_count FROM tbl_inventory WHERE id_mst_drugs=? and Entry_Date=? and id_mstfacility=? and flag=? and is_deleted=? and is_temp='0'";
							$data['item_sequence']=$this->db->query($query,[$drug_name_val,$Entry_Date,$loginData->id_mstfacility,'R','0'])->result();
	echo json_encode($data);						
}
function get_issue_sequence(){
	$drug_name_val = $this->input->get('drug_name',TRUE);
	$dispatch_date =timeStamp($this->input->get('dispatch_date',TRUE));
	$inventory_id =($this->input->get('inventory_id',TRUE));
	$is_manual =($this->input->get('is_manual',TRUE));
	$condition='And 1';
	if (($inventory_id!='' || $inventory_id!=NULL) &&($is_manual=='m' || $is_manual=='')) {
		$condition=" AND inventory_id!=".$inventory_id."";
	}
	else{
		$condition='And 1';
	}
	$loginData = $this->session->userdata('loginData');
	$query="SELECT IFNULL(count(issue_num),0) as issue_count FROM tbl_inventory WHERE drug_name=? and dispatch_date=? AND ((id_mstfacility=? and flag=?) OR (Flag=? and transfer_to=? and (indent_status>0 or indent_status is not null))) and is_deleted=? and is_temp='0' ".$condition."";
							$data['item_sequence']=$this->db->query($query,[$drug_name_val,$dispatch_date,$loginData->id_mstfacility,'L','I',$loginData->id_mstfacility,'0'])->result();
	echo json_encode($data);						
}
function check_batch_num(){
	$batch_num = $this->input->get('batch_num',TRUE);
	//$Entry_Date =timeStamp($this->input->get('Entry_Date',TRUE));
	$loginData = $this->session->userdata('loginData');
	$query="SELECT IFNULL(count(batch_num),0) as batch_count FROM tbl_inventory WHERE batch_num=? and ((flag=? and id_mstfacility=?) OR (flag=? and relocation_status<3 and id_mstfacility=?)) and is_deleted=? and is_temp='0' ";
							$data['batch_num']=$this->db->query($query,[$batch_num,'R',$loginData->id_mstfacility,'I',$loginData->id_mstfacility,'0'])->result();
	echo json_encode($data);						
}
function get_indent_num(){
	$item_name = $this->input->get('item_name',TRUE);
	$inventory_id=$this->input->get('inventory_id',TRUE);
	$is_edit=$this->input->get('is_edit',TRUE);
	$loginData = $this->session->userdata('loginData');
	if ($inventory_id!="" || $inventory_id!=NULL) {
		$qry="and inventory_id!=?";
		$arr=['0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility];
	}
	else{
		$qry="";
		$arr=['0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility];
	}
	if ($is_edit=='' || $is_edit=='m') {
		$rem= "rem>0";
	}
	else{
		$rem='1 ';
	}
	$query="SELECT i1.rem,i1.type,i1.indent_num,i2.indent_date,i2.approved_quantity FROM 
(SELECT Flag,inventory_id,indent_num FROM tbl_inventory WHERE (Flag='I' OR Flag='R') AND is_deleted =? AND id_mst_drugs=? and id_mstfacility=?) AS p
LEFT JOIN 
(SELECT SUM((case when ((approved_quantity is null or approved_quantity=0) AND (quantity_rejected_by_state IS NULL OR quantity_rejected_by_state=0)) then ifnull(quantity,0) ELSE approved_quantity END-(case when is_temp='0' then ifnull(quantity_received,0) else 0 end))+(case when is_temp='0' then ifnull(quantity_rejected,0) else 0 end))as rem,type,indent_num,inventory_id from tbl_inventory where is_deleted =?  AND id_mst_drugs=? and id_mstfacility=? GROUP BY indent_num
) AS i1
 ON p.indent_num=i1.indent_num AND p.inventory_id=i1.inventory_id
LEFT JOIN 
					(SELECT indent_date,indent_num,sum(case when (approved_quantity is NULL OR approved_quantity=0) then quantity else approved_quantity END) as approved_quantity from tbl_inventory where is_deleted =?  AND id_mst_drugs=? and id_mstfacility=? and Flag='I' GROUP BY indent_num) AS i2
			ON p.indent_num=i2.indent_num where ".$rem." And i1.indent_num is not null";

							$data['get_indent_num']=$this->db->query($query,$arr)->result();
							echo json_encode($data);
						
}
function get_indent_num_relocation(){
	$item_name = $this->input->get('item_name',TRUE);
	$inventory_id=$this->input->get('inventory_id',TRUE);
	$loginData = $this->session->userdata('loginData');
	if ($inventory_id!="" || $inventory_id!=NULL) {
		$qry="and inventory_id!=?";
		$arr=['0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility,$inventory_id,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility];
	}
	else{
		$qry="";
		$arr=['0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility];
	}

	$query="SELECT inv.* FROM (
SELECT p.type,p.indent_num,i2.indent_date,i2.approved_quantity,p.refrence_id,(p.requested_quantity-ifnull(i1.relocated,0)) AS rem,p.transfer_to FROM 
 (SELECT type,Flag,inventory_id,indent_num,refrence_id,requested_quantity,transfer_to FROM tbl_inventory WHERE (Flag='I' OR Flag='L') AND is_deleted =? and is_temp='0' AND id_mst_drugs=? AND (id_mstfacility=? OR transfer_to=?)) AS p
LEFT JOIN 
(SELECT Flag,inventory_id,type,indent_num,refrence_id,sum(relocated_quantity) AS relocated FROM tbl_inventory WHERE (Flag='I' OR Flag='L') AND is_deleted =? and is_temp='0' AND id_mst_drugs=? and(id_mstfacility=? OR transfer_to=?) ".$qry." GROUP BY indent_num) AS i1
ON 
p.indent_num=i1.indent_num
LEFT JOIN 
				(SELECT indent_accept_date as indent_date,indent_num,transfer_to,approved_quantity,inventory_id from tbl_inventory where is_deleted =? and is_temp='0' AND id_mst_drugs=? and transfer_to=? and Flag='I' GROUP BY batch_num) AS i2
			ON p.indent_num=i2.indent_num) AS inv
where inv.rem>0 and inv.transfer_to=? GROUP BY inv.refrence_id";

							$data['get_indent_num']=$this->db->query($query,$arr)->result();
							echo json_encode($data);
						
}
function get_manual_indent_num(){
	$item_name = $this->input->get('item_name',TRUE);
	$facility = $this->input->get('id_mstfacility',TRUE);
	$inventory_id=$this->input->get('inventory_id',TRUE);
	$loginData = $this->session->userdata('loginData');
	if ($inventory_id!="" || $inventory_id!=NULL) {
		$qry="";
	}
	else{
		$qry="AND i3.rem>0";
		
	}
		
		$arr=[$facility,$facility,'0',$item_name,'0',$item_name,$facility,'0',$item_name];

	$query="SELECT i2.*,i3.rem FROM 
				(SELECT indent_num,inventory_id FROM tbl_inventory i WHERE (((Flag='I') and id_mstfacility=?) or((Flag='R' or Flag='L')and transfer_to=?)) AND is_deleted=? AND i.id_mst_drugs=? and (i.indent_accept_date is NULL or i.indent_accept_date='0000-00-00') GROUP BY indent_num) AS i1
				LEFT JOIN 
				(SELECT i.id_mst_drugs,i.inventory_id,i.indent_num,i.indent_date,i.quantity,i.drug_name,i.type,i.indent_status,s.strength FROM `tbl_inventory` i INNER JOIN `mst_drug_strength` s on i.id_mst_drugs=s.id_mst_drug_strength where i.is_deleted=? AND i.id_mst_drugs=? AND i.id_mstfacility=? AND i.Flag='I' and (i.pending_quantity=0 or i.pending_quantity is null) and (i.indent_accept_date is NULL or i.indent_accept_date='0000-00-00') ORDER BY i.indent_date DESC) AS i2
				ON i1.indent_num=i2.indent_num
				LEFT JOIN 
				(SELECT SUM((case when ((approved_quantity is null or approved_quantity=0) AND (quantity_rejected_by_state IS NULL OR quantity_rejected_by_state=0)) then ifnull(quantity,0) ELSE approved_quantity END)-((case when is_temp='0' then ifnull(quantity_received,0) else 0 end)+(case when is_temp='0' then ifnull(quantity_rejected,0) else 0 end)+(case when relocation_status<3 AND flag='L' then relocated_quantity ELSE 0 END )))as rem,type,indent_num,inventory_id from tbl_inventory where is_deleted =? AND id_mst_drugs=? GROUP BY indent_num) AS i3
 ON i1.indent_num=i3.indent_num
				WHERE i2.inventory_id IS NOT NULL ".$qry."";

							$data['get_indent_num']=$this->db->query($query,$arr)->result();
							echo json_encode($data);
						
}
function get_batch_num(){
	$item_name = $this->input->get('item_name',TRUE);
	$inventory_id=$this->input->get('inventory_id',TRUE);
	$loginData = $this->session->userdata('loginData');
	if ($inventory_id!="" || $inventory_id!=NULL) {
		$qry="and inventory_id!=?";
		$arr=['0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility,$loginData->id_mstfacility,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility,$inventory_id,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility];
	}
	else{
		$qry="";
		$arr=['0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility,$loginData->id_mstfacility,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility];
	}
	 $query="SELECT i1.rem,TYPE,i1.batch_num,i2.Entry_Date FROM 
				(SELECT Flag,inventory_id,batch_num,id_mst_drugs FROM tbl_inventory WHERE is_deleted =? and is_temp='0' AND id_mst_drugs=? and (((Flag='R' OR FLag='F' OR FLag='U' OR Flag='L' or Flag='I') AND id_mstfacility=?) or (relocated_quantity!=0 or relocated_quantity is not null and Flag='I' AND transfer_to=?))) AS p
			LEFT JOIN 
				(SELECT SUM(IFNULL((case when id_mstfacility=? then quantity_received ELSE 0 END ),0)-(ifnull(quantity_rejected,0)+ifnull(dispensed_quantity,0)+ifnull(control_used,0)+ifnull(total_ml_wast_vials_used,0)+ifnull(returned_quantity,0)+ IFNULL((case when (transfer_to=? and Flag='I') then relocated_quantity when (id_mstfacility=? and Flag='L') then relocated_quantity ELSE 0 END),0))) as rem,type,batch_num,inventory_id,id_mst_drugs from tbl_inventory where is_deleted=? and is_temp='0' AND id_mst_drugs=? and  (id_mstfacility=? or transfer_to=?)".$qry." GROUP BY batch_num ) AS i1
			ON p.inventory_id=i1.inventory_id
			LEFT JOIN 
				(SELECT inventory_id,batch_num,Entry_Date from tbl_inventory where is_deleted =? and is_temp='0' AND id_mst_drugs=? and ((id_mstfacility=? and Flag='R') or (Flag='I' and id_mstfacility=?)) GROUP BY batch_num ) AS i2
			ON p.batch_num=i2.batch_num
 			WHERE i1.rem>0 AND i2.Entry_Date>DATE_ADD(Now(), INTERVAL-6 MONTH)";
							$data['get_batch_num']=$this->db->query($query,$arr)->result();
			echo json_encode($data);				
}
function edit_inventory_data(){
	$loginData = $this->session->userdata('loginData');
	$inventory_id = $this->input->get('inventory_id',TRUE);
	$relocation_remark = $this->input->get('relocation_remark',TRUE);
	$inv_remark = $this->input->get('inv_remark',TRUE);
	if ($relocation_remark=='n' || $inv_remark=='n') {
		$facility="(transfer_to=? or id_mstfacility=".$loginData->id_mstfacility.")";
	}
	else{
		$facility="id_mstfacility=?";
	}
	
	$query="SELECT * FROM tbl_inventory where is_deleted=? AND inventory_id=?";
							$data['inventory_details']=$this->db->query($query,['0',$inventory_id])->result();
	echo json_encode($data);						
}
function get_indent_quantity(){
	$inventory_id = $this->input->get('inventory_id',TRUE);
	$loginData = $this->session->userdata('loginData');
	$query="SELECT i1.*,case when i1.quantity_dispatched IS NOT NULL AND i1.quantity_dispatched>0 AND i1.is_temp='1' then i2.quantity_dispatched when (i2.relocated is NULL || i2.relocated=0) then i1.requested_quantity when (i2.relocated<i1.requested_quantity and relocation_status is NULL or relocation_status=0) then i1.requested_quantity ELSE i2.relocated END AS relocated,case when (i1.approved_quantity is NULL || i1.approved_quantity=0) then (case when i2.quantity_dispatched IS NOT NULL AND i2.quantity_dispatched>0 AND i1.is_temp='1' then i2.quantity_dispatched ELSE relocated END) ELSE i1.approved_quantity END AS approved FROM (SELECT * FROM tbl_inventory where is_deleted=? AND inventory_id=? and id_mststate=?)AS i1
LEFT JOIN 
(SELECT SUM(relocated_quantity) AS relocated,SUM(quantity_dispatched) AS quantity_dispatched,indent_num,id_mstfacility FROM tbl_inventory where is_deleted=? AND  id_mststate=? GROUP BY indent_num) AS i2
ON i1.indent_num=i2.indent_num AND i1.id_mstfacility=i2.id_mstfacility";
							$data['inventory_details']=$this->db->query($query,['0',$inventory_id,$loginData->State_ID,'0',$loginData->State_ID])->result();
	echo json_encode($data);						
}

function get_data_for_utilize(){
	$item_name = $this->input->get('item_name',TRUE);
	$indent_num = $this->input->get('indent_num',TRUE);
	$inventory_id = $this->input->get('inventory_id',TRUE);
	$loginData = $this->session->userdata('loginData');
	if ($indent_num!='') {

		if ($inventory_id!="") {

			$arr=[$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$indent_num,$loginData->id_mstfacility,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility,$inventory_id,'0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility];
		$inventory_id_qry=" AND inventory_id!=?";
	}
	else{
		$arr=[$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$indent_num,$loginData->id_mstfacility,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility];
		$inventory_id_qry="";
	}

		
		$indent_qry=" AND indent_num=? ";
		
	}
	else{
		if ($inventory_id!="") {
		$arr=[$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility,$inventory_id,'0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility];
		$inventory_id_qry=" AND inventory_id!=?";
	}
	else{
		$arr=[$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,$loginData->id_mstfacility];
		$inventory_id_qry="";
	}
		
		$indent_qry="";
		
	}
	$query="SELECT i1.rem,i1.batch_num,i3.to_Date,i4.Entry_Date,i4.Expiry_Date,i4.type,i5.LookupValue as days,case when i2.to_Date IS NULL then '0' ELSE '1' end AS to_Date_flag FROM 
				(SELECT Flag,inventory_id,batch_num FROM tbl_inventory WHERE (((Flag='R' OR FLag='F' OR FLag='U' OR Flag='L' or Flag='I') AND id_mstfacility=?) or (relocated_quantity!=0 or relocated_quantity is not null and Flag='I' AND transfer_to=?)) AND is_deleted =? and is_temp='0' AND id_mst_drugs=? ".$indent_qry.") AS p
			LEFT JOIN 
				(SELECT SUM(IFNULL((case when id_mstfacility=? then quantity_received ELSE 0 END ),0)-(ifnull(quantity_rejected,0)+ifnull(dispensed_quantity,0)+ifnull(control_used,0)+ifnull(total_ml_wast_vials_used,0)+ifnull(returned_quantity,0)+ IFNULL((case when (transfer_to=? and Flag='I') then relocated_quantity when (id_mstfacility=? and Flag='L') then relocated_quantity ELSE 0 END),0))) as rem,type,batch_num,inventory_id from tbl_inventory where is_deleted =? and is_temp='0' AND id_mst_drugs=? and (id_mstfacility=? or transfer_to=?)".$inventory_id_qry." GROUP BY batch_num ) AS i1
			ON p.inventory_id=i1.inventory_id
			LEFT JOIN 
				(SELECT to_Date,inventory_id,batch_num from tbl_inventory where is_deleted =? and is_temp='0' AND id_mst_drugs=? and id_mstfacility=? and Flag='U' GROUP BY batch_num ) AS i2
			ON p.batch_num=i2.batch_num
			LEFT JOIN 
				(SELECT inventory_id,batch_num,MAX(GREATEST(COALESCE((to_Date), 0),COALESCE((Entry_Date), 0))) AS to_Date from tbl_inventory where is_deleted =? and is_temp='0' AND id_mst_drugs=? and id_mstfacility=? and ((Flag='U' or Flag='R' or Flag='I')) GROUP BY batch_num ) AS i3
			ON p.batch_num=i3.batch_num
			LEFT JOIN 
				(SELECT inventory_id,batch_num,Entry_Date,Expiry_Date,type from tbl_inventory where is_deleted =? and is_temp='0' AND id_mst_drugs=? and ((id_mstfacility=? and Flag='R') or (Flag='I' and id_mstfacility=?)) GROUP BY batch_num ) AS i4
			ON p.batch_num=i4.batch_num
			LEFT JOIN 
				(SELECT LookupValue,LookupCode from mstlookup where Flag='85') AS i5
			ON i4.type=i5.LookupCode
 			WHERE i1.rem>0";
							$data['get_batch_num']=$this->db->query($query,$arr)->result();
	echo json_encode($data);						
}
function delete_receipt($inventory_id=NULL,$flag=NULL){

$this->Inventory_Model->delete_receipt($inventory_id);
if($flag=='R'){
	redirect('Inventory');
}
else if($flag=='L'){
	redirect('Inventory/index/L');
}
else if($flag=='U'){
	redirect('Inventory/index/U');
}

else if($flag=='F'){
	redirect('Inventory/index/F');
}
else if($flag=='I'){
	redirect('Inventory/index/I');
}
}

	public function AddReceipt($inventory_id=NULL){
		//echo $inventory_id;
						$loginData = $this->session->userdata('loginData');
						//echo "<pre>";print_r($loginData);
						if( ($loginData) && $loginData->user_type != '2' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
						if( ($loginData) && $loginData->user_type == '1' ){
						$sess_where = " 1";
						}
						elseif( ($loginData) && $loginData->user_type == '2' ){
						
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
						}
						elseif( ($loginData) && $loginData->user_type == '3' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
						}
						elseif( ($loginData) && $loginData->user_type == '4' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
						}
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						$State_ID=$loginData->State_ID;
						$DistrictID=$loginData->DistrictID;
						$id_mstfacility=$loginData->id_mstfacility;
						if($loginData->State_ID==''||$loginData->State_ID==NULL){
						 $State_ID=0;
						}
						if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
						$DistrictID=0;
						}
						if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
						$id_mstfacility=0;
						}
						if ($this->security->xss_clean($this->input->post('save'))=='save') {

							$this->form_validation->set_rules('item_name', 'Item Name', 'trim|required|numeric|xss_clean');
					

					$this->form_validation->set_rules('indent_num', 'Indent Number', 'trim|xss_clean');
					
					$this->form_validation->set_rules('batch_num', 'Batch Number', 'trim|required|xss_clean');

					$this->form_validation->set_rules('issue_num', 'Issue Number', 'trim|xss_clean');
					
					$this->form_validation->set_rules('Expiry_Date', 'Expiry Date', 'trim|required|xss_clean');
					
					$this->form_validation->set_rules('quantity_dispatched', 'Dispatch quantity', 'trim|xss_clean|numeric|greater_than[0]');
					
					 
					 $this->form_validation->set_rules('from_to_type', 'Facility/State Warehouse', 'trim|required|numeric|xss_clean');
 
							
					$this->form_validation->set_rules('relocation_remark', 'Remark', 'trim|xss_clean');  
					if ($this->form_validation->run() == FALSE) {

						$arr['status'] = 'false';
						$this->session->set_flashdata("tr_msg",validation_errors());
						//echo("arg1");
						redirect('Inventory/index/R');
					}else{

						$item_id=$this->security->xss_clean($this->input->post('item_name'));
						$inventory_id=$this->security->xss_clean(strip_tags($this->input->post('inventory_id')));
						$item_name_ar=$this->Inventory_Model->get_items($item_id);
						$from_to_type=$this->security->xss_clean($this->input->post('from_to_type'));
						$quantity_dispatched=$this->security->xss_clean(strip_tags($this->input->post('quantity_dispatched')));
						$quantity_received=$this->security->xss_clean(strip_tags($this->input->post('quantity_received')));
						$quantity_rejected=$this->security->xss_clean(strip_tags($this->input->post('quantity_rejected')));
						
						$item_name=$item_name_ar[0]->id_mst_drugs;
						$item_type=$item_name_ar[0]->type;
						
						$is_manual=$this->security->xss_clean(strip_tags($this->input->post('is_manual')));
	
						if ($quantity_rejected>0 && $quantity_rejected!=NULL) {
							if ($quantity_rejected==$quantity_dispatched) {
								$status=4;
							}
							else if ($quantity_rejected<$quantity_dispatched) {
								$status=6;
							}
						}
						else if ($quantity_rejected==0) {
							if ($quantity_received==$quantity_dispatched) {
								$status=3;
							}
							else if ($quantity_received<$quantity_dispatched) {
								$status=6;
							}

						}
						else if ($quantity_received>0 && $quantity_rejected>0) {
								$status=5;
							}
						if ($is_manual=='n') {
							$update_arr = array(
											'Entry_Date' => timeStamp($this->security->xss_clean(strip_tags($this->input->post('Entry_Date')))),
											'Expiry_Date' => timeStamp($this->security->xss_clean(strip_tags($this->input->post('Expiry_Date')))),
											'quantity_received' => $this->security->xss_clean(strip_tags($this->input->post('quantity_received'))),
											'not_quantity_received' => $this->security->xss_clean(strip_tags($this->input->post('not_quantity_received'))),
											'Acceptance_Status' => $this->security->xss_clean($this->input->post('Acceptance_Status')),
											'rejection_reason' => $this->security->xss_clean(strip_tags($this->input->post('rejection_reason'))),
											'quantity_rejected' => $this->security->xss_clean(strip_tags($this->input->post('quantity_rejected'))),
											'indent_status'=>$status,
											'relocation_status'=>$status,
											'current_status'=>4
																	);
						
						$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						$data = array_merge($update_arr, $array2);
						//pr($data);exit();
						$this->db->where('inventory_id',$inventory_id);
						$this->db->update('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Receipt Recorded Successfully');
						redirect('Inventory/index/R');
						}



						/*if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$item_name_ar[0]->strength*$this->input->post('quantity');
						}
						else{
						
						$screening_tests=0;
						}*/
						elseif($is_manual=='m' || $is_manual=='') {
					
						if($from_to_type==3 || $from_to_type==4){
							 $unrecognised=$this->security->xss_clean($this->input->post('unrecognised'));
							$source_name=NULL;
							$indent_num==NULL;
							$transfer_to=NULL;
							$indent_date=NULL;
							$is_temp='0';
							$is_notification='1';
						}
						else if($from_to_type==1){
							$source_name=$this->security->xss_clean($this->input->post('source_name'));
							$unrecognised=NULL;
							$transfer_to=NULL;
							$indent_num          = $this->security->xss_clean(strip_tags($this->input->post('indent_num')));
							$indent_date=timeStamp($this->security->xss_clean(strip_tags($this->input->post('indent_date'))));
							$is_temp='0';
							$is_notification='1';
						}
						else if($from_to_type==2){
							$source_name=NULL;
							$unrecognised=NULL;
							$transfer_to=$this->security->xss_clean($this->input->post('source_name'));
							$indent_num = $this->security->xss_clean(strip_tags($this->input->post('indent_num')));
							$indent_date=($this->security->xss_clean(strip_tags($this->input->post('indent_date_val'))));
							$is_temp='1';
							$is_notification='0';
							$query='Select count(inventory_id) as count,inventory_id,quantity_received,relocated_quantity from tbl_inventory where indent_num=? and flag=? and is_deleted=? and id_mstfacility=?';
							$is_first_time=$this->db->query($query,[$indent_num,'I','0',$loginData->id_mstfacility])->result();
							//pr($is_first_time);exit();
							if ($is_first_time[0]->count==1 && ($is_first_time[0]->quantity_received==NULL || $is_first_time[0]->quantity_received=='' || $is_first_time[0]->quantity_received==0 || $is_first_time[0]->relocated_quantity==NULL || $is_first_time[0]->relocated_quantity=='' || $is_first_time[0]->relocated_quantity==0)) {
								$update_arr         = array(   
								'batch_num'           => $this->security->xss_clean(strip_tags($this->input->post('batch_num'))),  
								'issue_num'           => $this->security->xss_clean(strip_tags($this->input->post('issue_num'))),  
								'Entry_Date'          => timeStamp($this->security->xss_clean(strip_tags($this->input->post('Entry_Date')))),
								/*'approved_quantity'   => $this->security->xss_clean(strip_tags($this->input->post('approved_quantity')),*/
								'quantity_dispatched' => $this->security->xss_clean(strip_tags($this->input->post('quantity_dispatched'))),
								'quantity_received'   => $this->security->xss_clean(strip_tags($this->input->post('quantity_received'))),
								'not_quantity_received' => $this->security->xss_clean(strip_tags($this->input->post('not_quantity_received'))),
								'quantity_rejected'   => $this->security->xss_clean(strip_tags($this->input->post('quantity_rejected'))),
								'Expiry_Date'         => timeStamp($this->security->xss_clean(strip_tags($this->input->post('Expiry_Date')))),  
								'from_to_type'        => $this->security->xss_clean($this->input->post('from_to_type')),  
								'source_name'         => $source_name,
								'unrecognised'        =>$unrecognised,
								'transfer_to'		  =>$transfer_to,
								'Acceptance_Status'   => $this->security->xss_clean($this->input->post('Acceptance_Status')),  
								'rejection_reason'    => $this->security->xss_clean($this->input->post('rejection_reason')),  
								'indent_status'=>$status,
								'relocation_status'=>$status,
								'is_temp'=>$is_temp,
								'is_notification'=>$is_notification,
								'approving_facility'     => $this->session->userdata('approving_facility')->approving_facility,
								'current_status'=>4
								);
								$this->db->where('inventory_id',$is_first_time[0]->inventory_id);
								$this->db->update('tbl_inventory',$update_arr);
								$this->session->set_flashdata('tr_msg','Stock Receipt Added Successfully');
								redirect('Inventory/index/R');
							}
						}
						$insert_array         = array(  
						'id_mst_drugs'        =>$item_id,
						'drug_name'           =>$item_name,
						'type'                =>$item_type,
						'indent_num'          => $indent_num,  
						'indent_date'          => $indent_date,  
						'batch_num'           => $this->security->xss_clean(strip_tags($this->input->post('batch_num'))),  
						'issue_num'           => $this->security->xss_clean(strip_tags($this->input->post('issue_num'))),  
						'Entry_Date'          => timeStamp($this->security->xss_clean(strip_tags($this->input->post('Entry_Date')))),
						'approved_quantity'   => $this->security->xss_clean(strip_tags($this->input->post('approved_quantity'))),
						'quantity_dispatched' => $this->security->xss_clean(strip_tags($this->input->post('quantity_dispatched'))),
						'quantity_received'   => $this->security->xss_clean(strip_tags($this->input->post('quantity_received'))),
						'not_quantity_received' => $this->security->xss_clean(strip_tags($this->input->post('not_quantity_received'))),
						'quantity_rejected'   => $this->security->xss_clean(strip_tags($this->input->post('quantity_rejected'))),
						'Expiry_Date'         => timeStamp($this->security->xss_clean(strip_tags($this->input->post('Expiry_Date')))),  
						'from_to_type'        => $this->security->xss_clean($this->input->post('from_to_type')),  
						'source_name'         => $source_name,
						'unrecognised'        =>$unrecognised,
						'transfer_to'		  =>$transfer_to,
						'Acceptance_Status'   => $this->security->xss_clean($this->input->post('Acceptance_Status')),  
						'rejection_reason'    => $this->security->xss_clean($this->input->post('rejection_reason')),  
						'id_mststate'         =>  $State_ID,
						'id_mstdistrict'      => $DistrictID,
						'id_mstfacility'      => $id_mstfacility,
						'flag'                =>	'R',
						'approving_facility'     => $this->session->userdata('approving_facility')->approving_facility,
						'is_deleted'          =>'0', 
						'indent_status'=>$status,
						'relocation_status'=>$status,
						'is_temp'=>$is_temp,
						'is_notification'=>$is_notification,
						'current_status'=>4
						);
						
						//pr($insert_array);exit();
						//echo $inventory_id;exit;
						if ($inventory_id!=NULL) {
						$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->where('inventory_id',$inventory_id);
						$this->db->update('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Receipt Updated Successfully');
						redirect('Inventory/index/R');
						}
						else{
						$array2 = array("id_tblusers" => $loginData->id_tblusers, "CreatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->insert('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Receipt Added Successfully');
						redirect('Inventory/index/R');
						}	
							}
					}
				}
				}
			
	}

public function stock_failure(){
		//echo $inventory_id;
						$loginData = $this->session->userdata('loginData');
						$this->load->library('form_validation');
						$this->load->model('Inventory_Model');
						//echo "<pre>";print_r($loginData);
						if( ($loginData) && $loginData->user_type != '2' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
						if( ($loginData) && $loginData->user_type == '1' ){
						$sess_where = " 1";
						}
						elseif( ($loginData) && $loginData->user_type == '2' ){
						
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
						}
						elseif( ($loginData) && $loginData->user_type == '3' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
						}
						elseif( ($loginData) && $loginData->user_type == '4' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
						}
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						$State_ID=$loginData->State_ID;
						$DistrictID=$loginData->DistrictID;
						$id_mstfacility=$loginData->id_mstfacility;
						if($loginData->State_ID==''||$loginData->State_ID==NULL){
						 $State_ID=0;
						}
						if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
						$DistrictID=0;
						}
						if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
						$id_mstfacility=0;
						}
						//print_r($loginData);
						if ($this->security->xss_clean($this->input->post('save'))=='save') {

						$item_id=$this->security->xss_clean($this->input->post('item_name'));

						$inventory_id=$this->security->xss_clean(strip_tags($this->input->post('inventory_id')));
						 $this->form_validation->set_rules('returned_quantity', 'returned quantity', 'trim|required|numeric|xss_clean|greater_than[0]');
						  $this->form_validation->set_rules('item_name', 'Item Name', 'trim|required|numeric|xss_clean');
					 $this->form_validation->set_rules('batch_num', 'Batch Number', 'trim|required|xss_clean');
					  $this->form_validation->set_rules('failure_issue', 'Failure Issue', 'trim|required|max_length[100]|xss_clean');
					  $this->form_validation->set_rules('failure_date', 'Failure Date', 'trim|required|xss_clean');

					if ($this->form_validation->run() == FALSE) {

						$arr['status'] = 'false';
						$this->session->set_flashdata("tr_msg",validation_errors());
						//echo("arg1");
						redirect('Inventory/index/F');
					}else{
						$item_name_ar=$this->Inventory_Model->get_items($item_id);
						
						$item_name=$item_name_ar[0]->id_mst_drugs;
						$item_type=$item_name_ar[0]->type;

						if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$item_name_ar[0]->strength*$this->security->xss_clean(strip_tags($this->input->post('quantity')));
						}
						else{
						
						$screening_tests=0;
						}
						$batch_num=$this->security->xss_clean(strip_tags($this->input->post('batch_num')));
						$query="SELECT DISTINCT indent_num FROM tbl_inventory WHERE batch_num=? AND indent_num IS NOT NULL";
						$get_indent_num=$this->db->query($query,[$batch_num])->result();
						$insert_array = array(  
						'id_mst_drugs'  =>$item_id,
						'drug_name'  =>$item_name,
						'indent_num' =>$get_indent_num[0]->indent_num,
						'type'=>$item_type,
						'failure_date'=>$this->security->xss_clean(strip_tags(timeStamp($this->input->post('failure_date')))),
						'batch_num'=>$this->security->xss_clean(strip_tags($this->input->post('batch_num'))),
						'failure_issue'=>$this->security->xss_clean(strip_tags($this->input->post('failure_issue'))),
						'returned_quantity'     => $this->security->xss_clean(strip_tags($this->input->post('returned_quantity'))),
						'quantity_screening_tests'     => $screening_tests, 
						'id_mststate'   =>  $State_ID,
						'id_mstdistrict' => $DistrictID,
						'id_mstfacility'     => $id_mstfacility,
						'failure_accept_status'     =>0,
						'approving_facility'     => $this->session->userdata('approving_facility')->approving_facility,
						'indent_status'=>0,
						'flag'=>	'F',
						'is_deleted'=>'0'
						);
						
						//pr($data);
						//echo $inventory_id;exit;
						if ($inventory_id!=NULL) {
						$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->where('inventory_id',$inventory_id);
						$this->db->update('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Failure Updated Successfully');
						redirect('Inventory/index/F');
						}
						else{
						$array2 = array("id_tblusers" => $loginData->id_tblusers, "CreatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->insert('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Failure Recorded Successfully');
						redirect('Inventory/index/F');
						}	
						}
					}
				}
			
	}

public function stock_utilization(){
		//echo $inventory_id;
						$loginData = $this->session->userdata('loginData');
						$this->load->library('form_validation');
						$this->load->model('Inventory_Model');
						//echo "<pre>";print_r($loginData);
						if( ($loginData) && $loginData->user_type != '2' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
						if( ($loginData) && $loginData->user_type == '1' ){
						$sess_where = " 1";
						}
						elseif( ($loginData) && $loginData->user_type == '2' ){
						
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
						}
						elseif( ($loginData) && $loginData->user_type == '3' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
						}
						elseif( ($loginData) && $loginData->user_type == '4' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
						}
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						$State_ID=$loginData->State_ID;
						$DistrictID=$loginData->DistrictID;
						$id_mstfacility=$loginData->id_mstfacility;
						if($loginData->State_ID==''||$loginData->State_ID==NULL){
						 $State_ID=0;
						}
						if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
						$DistrictID=0;
						}
						if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
						$id_mstfacility=0;
						}
						//print_r($loginData);
						if ($this->security->xss_clean($this->input->post('save'))=='save') {

						$item_id=$this->security->xss_clean($this->input->post('item_name'));
						$item_name_ar=$this->Inventory_Model->get_items($item_id);
						
						$item_name=$item_name_ar[0]->id_mst_drugs;
						$item_type=$item_name_ar[0]->type;
						$inventory_id=$this->security->xss_clean(strip_tags($this->input->post('inventory_id')));
						 $this->form_validation->set_rules('item_name', 'Item Name', 'trim|required|numeric|xss_clean');
					 $this->form_validation->set_rules('batch_num', 'Batch Number', 'trim|required|xss_clean');
					$this->form_validation->set_rules('from_Date', 'From Date', 'trim|required|xss_clean');
					  $this->form_validation->set_rules('to_Date', 'To Date', 'trim|required|xss_clean');
						 //$this->form_validation->set_rules('dispensed_quantity', 'Dispensed quantity', 'trim|required|numeric|xss_clean|greater_than[0]');
						  if ($item_type!=3 && $item_type!=4) {

						  $this->form_validation->set_rules('dispensed_quantity', 'Dispensed quantity', 'trim|required|numeric|xss_clean|greater_than[0]');
						}
						  if ($item_type==2) {
						  	$this->form_validation->set_rules('repeat_quantity', 'Repeat quantity', 'trim|required|numeric|xss_clean|greater_than[0]');
						  $this->form_validation->set_rules('control_used', 'Number of Control Used', 'trim|required|numeric|xss_clean|greater_than[0]');
						  }
						  
						  
						 
					  $this->form_validation->set_rules('utilization_purpose', 'Purpose of Utilization', 'trim|required|xss_clean');
					

					if ($this->form_validation->run() == FALSE) {

						$arr['status'] = 'false';
						$this->session->set_flashdata("tr_msg",validation_errors());
						//echo("arg1");
						redirect('Inventory/index/U');
					}else{
						

						if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$item_name_ar[0]->strength*$this->security->xss_clean(strip_tags($this->input->post('quantity')));
						}
						else{
						
						$screening_tests=0;
						}
						$batch_num=$this->security->xss_clean(strip_tags($this->input->post('batch_num')));
						$query="SELECT DISTINCT indent_num FROM tbl_inventory WHERE batch_num=? AND indent_num IS NOT NULL";
						$get_indent_num=$this->db->query($query,[$batch_num])->result();
						// echo $get_indent_num[0]->indent_num;exit;

						if ($item_type==3) {
							$dispensed_quantity = $this->security->xss_clean(strip_tags($this->input->post('total_ml_vaccine')));
						}
						else{
							$dispensed_quantity = $this->security->xss_clean(strip_tags($this->input->post('dispensed_quantity')));
						}
						$insert_array = array(  
						'id_mst_drugs'  =>$item_id,
						'drug_name'  =>$item_name,
						'type'=>$item_type,
						'indent_num'=>$get_indent_num[0]->indent_num,
						'batch_num'=>$this->security->xss_clean(strip_tags($this->input->post('batch_num'))),
						'from_Date'=>$this->security->xss_clean(strip_tags(timeStamp($this->input->post('from_Date')))),
						'to_Date'=>$this->security->xss_clean(strip_tags(timeStamp($this->input->post('to_Date')))),

						'dispensed_quantity'     => $dispensed_quantity,

						'repeat_quantity'     => $this->security->xss_clean(strip_tags($this->input->post('repeat_quantity'))),
						'control_used'     => $this->security->xss_clean(strip_tags($this->input->post('control_used'))),
						'available_quantity'   => $this->security->xss_clean(strip_tags($this->input->post('available_quantity'))),
						'utilization_purpose'     => $this->security->xss_clean($this->input->post('utilization_purpose')),
						'number_ofdoes_healthcare'     => $this->security->xss_clean($this->input->post('number_ofdoes_healthcare')),
						'num_of_infant_doese'     => $this->security->xss_clean($this->input->post('num_of_infant_doese')),
						'number_of_adult_doses'     => $this->security->xss_clean($this->input->post('number_of_adult_doses')),
						'total_vials_used'     => $this->security->xss_clean($this->input->post('total_vials_used')),
						'total_ml_wast_vials_used'     => $this->security->xss_clean($this->input->post('total_ml_wast_vials_used')),
						'quantity_screening_tests'     => $screening_tests, 
						'id_mststate'   =>  $State_ID,
						'id_mstdistrict' => $DistrictID,
						'id_mstfacility'     => $id_mstfacility,
						'approving_facility'     => $this->session->userdata('approving_facility')->approving_facility,
						'indent_status'=>0,
						'flag'=>	'U',
						'is_deleted'=>'0'
						);
						
						//pr($data);
						
						if ($inventory_id!=NULL) {
						$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->where('inventory_id',$inventory_id);
						$this->db->update('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Utilization Updated Successfully');
						redirect('Inventory/index/U');
						}
						else{
						$array2 = array("id_tblusers" => $loginData->id_tblusers, "CreatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->insert('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Utilization Recorded Successfully');
						redirect('Inventory/index/U');
						}	
						}
					}
				}
			
	}
public function relocation($inventory_id=NULL){
		//echo $inventory_id;
		$this->load->library('form_validation');
		$loginData = $this->session->userdata('loginData');
						//echo "<pre>";print_r($loginData);
		if( ($loginData) && $loginData->user_type != '2' ){
			$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = " 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){
		
			$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
		}
						
		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		
		if($REQUEST_METHOD == 'POST')
		{
				$loginData = $this->session->userdata('loginData');
				$State_ID=$loginData->State_ID;
				$DistrictID=$loginData->DistrictID;
				$id_mstfacility=$loginData->id_mstfacility;
				if($loginData->State_ID==''||$loginData->State_ID==NULL){
					 $State_ID=0;
				}
				if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
					$DistrictID=0;
				}
				if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
					$id_mstfacility=0;
				}
				if ($this->security->xss_clean($this->input->post('save'))=='save') {
			 
					$this->form_validation->set_rules('item_name', 'Item Name', 'trim|required|numeric|xss_clean');
					

					$this->form_validation->set_rules('indent_num', 'Indent Number', 'trim|required|xss_clean');
					
					$this->form_validation->set_rules('batch_num', 'Batch Number', 'trim|required|xss_clean');

					$this->form_validation->set_rules('issue_num', 'Issue Number', 'trim|required|xss_clean');
					
					$this->form_validation->set_rules('request_date', 'Request Date', 'trim|required|xss_clean');
					
					$this->form_validation->set_rules('dispatch_date', 'Dispatch Date', 'trim|required|xss_clean');
					
					$this->form_validation->set_rules('relocated_quantity', 'Relocated quantity', 'trim|required|numeric|xss_clean|greater_than[0]');
					 
					 $this->form_validation->set_rules('from_to_type', 'Facility/State Warehouse', 'trim|required|numeric|xss_clean');

					$this->form_validation->set_rules('requested_quantity', 'Request quantity', 'trim|required|numeric|xss_clean|greater_than[0]');  
							
					$this->form_validation->set_rules('relocation_remark', 'Remark', 'trim|xss_clean');  
					$this->form_validation->set_rules('is_temp', 'is_temp', 'trim|xss_clean');  
					if ($this->form_validation->run() == FALSE) {

						$arr['status'] = 'false';
						$this->session->set_flashdata("tr_msg",validation_errors());
						//echo("arg1");
						redirect('Inventory/index/L');
					}else{
						$item_id=$this->security->xss_clean($this->input->post('item_name'));
						$item_name_ar=$this->Inventory_Model->get_items($item_id);
						$from_to_type=$this->security->xss_clean($this->input->post('from_to_type'));
						$source_name=$this->security->xss_clean($this->input->post('source_name'));
						$inventory_id=$this->security->xss_clean(strip_tags($this->input->post('inventory_id')));
						$is_temp=$this->security->xss_clean(strip_tags($this->input->post('is_temp')));
						$item_name=$item_name_ar[0]->id_mst_drugs;
						$item_type=$item_name_ar[0]->type;

						$batch_num=$this->security->xss_clean(strip_tags($this->input->post('batch_num')));
						$indent_num=$this->security->xss_clean(strip_tags($this->input->post('indent_num')));
						$is_manual=$this->security->xss_clean(strip_tags($this->input->post('is_manual')));
						$requested_quantity=$this->security->xss_clean(strip_tags($this->input->post('requested_quantity')));
						$relocated_quantity=$this->security->xss_clean(strip_tags($this->input->post('relocated_quantity')));
						$relocation_status=NULL;
						$indent_status=NULL;
						$not_quantity_received=NULL;
						$Rem_value=$requested_quantity-$relocated_quantity;
						if ($Rem_value==0) {
							$indent_status=1;
							$relocation_status=1;
						}
						else if ($Rem_value>0) {
							$indent_status=2;
							$relocation_status=2;
						}
						else if ($Rem_value<0) {

							$relocation_status=1;
						}
						$quantity_dispatched=$this->security->xss_clean(strip_tags($this->input->post('relocated_quantity')));
							$quantity_received=$this->security->xss_clean(strip_tags($this->input->post('quantity_received')));
						if ($quantity_received!='' && $quantity_received>0) {
							
							$not_quantity_received=$quantity_dispatched-$quantity_received;
							if ($quantity_received>=$quantity_dispatched) {
								$status=3;
							}
							else if ($quantity_received<$quantity_dispatched) {
								$status=6;
							}
							$indent_status=$status;
							$relocation_status=$status;
						}
						$query="SELECT Expiry_Date FROM tbl_inventory WHERE batch_num=? AND batch_num IS NOT NULL and flag=? group by batch_num";
						$get_expiry_date=$this->db->query($query,[$batch_num,'R'])->result();

						if ($is_manual=='n') {
						$query="SELECT COUNT(i.inventory_id) as count,i.refrence_id,SUM((IFNULL(i.requested_quantity,0)-IFNULL(i.relocated_quantity,0)))as rem,requested_quantity FROM tbl_inventory i WHERE indent_num=? AND indent_status IS NOT NULL and flag=? and transfer_to=?";
						$get_data_relocation=$this->db->query($query,[$indent_num,'I',$loginData->id_mstfacility])->result();
						if ($get_data_relocation[0]->count==1) {
							$pending_quantity=0;
							$requested_quantity=$get_data_relocation[0]->requested_quantity;
						}
						else if ($get_data_relocation[0]->count>1) {
							$pending_quantity=$Rem_value;
							$requested_quantity=0;

						}
						$refrence_id=$get_data_relocation[0]->refrence_id;
						$update_arr = array('batch_num' => $batch_num,
											'dispatch_date' => timeStamp($this->security->xss_clean(strip_tags($this->input->post('dispatch_date')))),
											'Expiry_Date'        =>$get_expiry_date[0]->Expiry_Date,
											'relocated_quantity' => $relocated_quantity,
											'not_relocated_quantity' => $this->security->xss_clean(strip_tags($this->input->post('not_relocated_quantity'))),
											'quantity_dispatched' => $relocated_quantity,
											'issue_num' => $this->security->xss_clean(strip_tags($this->input->post('issue_num'))),
											'relocation_remark' => $this->security->xss_clean(strip_tags($this->input->post('relocation_remark'))),
											'relocation_status' => $relocation_status,
											'indent_status' => $indent_status,
											'is_notification'=>'0',
											'not_quantity_received'=>$not_quantity_received,
											'current_status'=>3
											);
						$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						$data = array_merge($update_arr, $array2);
						
						if ($Rem_value<=0 && $inventory_id!=NULL) {

						$this->db->where('inventory_id',$inventory_id);
						$this->db->update('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Relocation Added Successfully');
						redirect('Inventory/index/L');
							
						}
						else if ($Rem_value>0 && $inventory_id!=NULL) {
							$pending_quantity=$Rem_value;
							$query="SELECT * FROM tbl_inventory i WHERE flag=? and inventory_id=? and is_temp='0'";
							$get_insert_array=$this->db->query($query,['I',$inventory_id])->result();
							$get_insert_array[0]->inventory_id=NULL;
							$get_insert_array[0]->quantity=NULL;
							$get_insert_array[0]->approved_quantity=NULL;
							$get_insert_array[0]->requested_quantity=0;
							$get_insert_array[0]->Entry_Date=NULL;
							$get_insert_array[0]->quantity_received=NULL;
							$get_insert_array[0]->not_quantity_received=NULL;
							$get_insert_array[0]->quantity_rejected=NULL;
							$get_insert_array[0]->quantity_dispatched=NULL;
							$get_insert_array[0]->pending_quantity=$pending_quantity;
							$get_insert_array[0]->id_tblusers=$loginData->id_tblusers;
							$get_insert_array[0]->CreatedOn=date('Y-m-d');
							$get_insert_array[0]->indent_status= $indent_status;
							$get_insert_array[0]->relocation_status= $relocation_status;
							$get_insert_array[0]->updateBy= NULL;
							$get_insert_array[0]->updatedOn= NULL;
							$get_insert_array[0]->dispatch_date= NULL;
							$get_insert_array[0]->issue_num= NULL;
							$get_insert_array[0]->relocation_remark= NULL;
							$get_insert_array[0]->batch_num= NULL;
							$get_insert_array[0]->relocated_quantity= NULL;
							$get_insert_array[0]->relocation_status= NULL;
							$get_insert_array[0]->current_status=NULL;

							
							$this->db->where('inventory_id',$inventory_id);
							$this->db->update('tbl_inventory',$data);

							$query="SELECT SUM((IFNULL(i.requested_quantity,0)-IFNULL(i.relocated_quantity,0)))as rem,requested_quantity FROM tbl_inventory i WHERE indent_num=? AND indent_status IS NOT NULL and flag=? and transfer_to=?";
						$get_rem=$this->db->query($query,[$indent_num,'I',$loginData->id_mstfacility])->result();
						if ($get_rem[0]->rem>0) {
								$get_insert_array[0]->pending_quantity=$get_rem[0]->rem;
								$set_insert_array_values=$get_insert_array[0];
							$this->db->insert('tbl_inventory',$set_insert_array_values);
						}
						
						
						
						$this->session->set_flashdata('tr_msg','Relocation Updated Successfully');
						redirect('Inventory/index/L');
						}
					}
					elseif ($is_manual=='m' || $is_manual=='') {
						$flag='L';
						$refrence_id=NULL;
						$query="SELECT count(inventory_id) as count,updatedOn,inventory_id FROM tbl_inventory i WHERE indent_num=? and (id_mstfacility=? AND flag='I' ) and is_deleted='0'";
						$get_inventory_id=$this->db->query($query,[$indent_num,$source_name])->result();
						/*if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$item_name_ar[0]->strength*$this->input->post('quantity');
						}
						else{
						
						$screening_tests=0;
						}*/
						/*if($this->input->post('from_to_type')==3){
						  $unrecognised=$this->input->post('unrecognised');
							$source_name=NULL;
							$transfer_to=NULL;
						}
						else{
							$source_name=$this->input->post('source_name');
							$unrecognised=NULL;
							$transfer_to=$this->input->post('destination_name');
						}*/
						if($from_to_type==3 || $from_to_type==4){
							 $unrecognised=$this->security->xss_clean($this->input->post('unrecognised'));
							$transfer_to=NULL;
						}
						else{
							$transfer_to=$this->security->xss_clean($this->input->post('source_name'));
							$unrecognised=NULL;
						}
						if ($is_temp=='1') {
							$flag='R';
							$transfer_to=$id_mstfacility;
							$quantity_dispatched=$this->security->xss_clean(strip_tags($this->input->post('relocated_quantity')));
							$quantity_received=$this->security->xss_clean(strip_tags($this->input->post('quantity_received')));
							$not_quantity_received=$quantity_dispatched-$quantity_received;
							if ($quantity_received>=$quantity_dispatched) {
								$status=3;
							}
							else if ($quantity_received<$quantity_dispatched) {
								$status=5;
							}
						}
						$insert_array         = array(  
						'id_mst_drugs'        =>$item_id,
						'refrence_id'        =>$refrence_id,
						'drug_name'           =>$item_name,
						'type'                =>$item_type,
						'indent_num'          => $this->security->xss_clean(strip_tags($this->input->post('indent_num'))),  
						'batch_num'           => $this->security->xss_clean(strip_tags($this->input->post('batch_num'))),  
						'issue_num'           => $this->security->xss_clean(strip_tags($this->input->post('issue_num'))),  
						'requested_quantity'          => $requested_quantity,
						'from_to_type'        => $this->security->xss_clean($this->input->post('from_to_type')),  
						'transfer_to'         => $transfer_to,
						'unrecognised'        =>$unrecognised,
						'Expiry_Date'        =>$get_expiry_date[0]->Expiry_Date,
						'request_date'         => timeStamp($this->security->xss_clean(strip_tags($this->input->post('request_date')))),  
						'dispatch_date'         => timeStamp($this->security->xss_clean(strip_tags($this->input->post('dispatch_date')))),  
						'relocated_quantity' => $this->security->xss_clean(strip_tags($this->input->post('relocated_quantity'))),
						'not_relocated_quantity' => $this->security->xss_clean(strip_tags($this->input->post('not_relocated_quantity'))),
						'quantity_dispatched' => $this->security->xss_clean(strip_tags($this->input->post('relocated_quantity'))),
						'relocation_remark'    => $this->security->xss_clean(strip_tags($this->input->post('relocation_remark'))),  
						'flag'                =>	$flag,
						'is_deleted'          =>'0', 
						'is_notification'=>'0',
						'is_temp'			=>'0',
						'current_status'=>3
						);
						//print_r($insert_array);exit();
						/*echo "<pre>";
						*/
					    // pr($get_inventory_id);exit();
						if ($get_inventory_id[0]->inventory_id!=NULL && $get_inventory_id[0]->inventory_id!='' && (($get_inventory_id[0]->count==1 && ($get_inventory_id[0]->updatedOn==NULL || $get_inventory_id[0]->updatedOn=='' || $get_inventory_id[0]->updatedOn=='0000-00-00')) || $get_inventory_id[0]->inventory_id==$inventory_id)) {
							$update_array         = array(  
						'refrence_id'        =>$get_inventory_id[0]->inventory_id,
						'batch_num'           => $this->security->xss_clean(strip_tags($this->input->post('batch_num'))),  
						'issue_num'           => $this->security->xss_clean(strip_tags($this->input->post('issue_num'))),  
						'requested_quantity'          => $requested_quantity,
						'from_to_type'        => $this->security->xss_clean($this->input->post('from_to_type')),  
						'transfer_to'         => $loginData->id_mstfacility,
						'unrecognised'        =>$unrecognised,
						'Expiry_Date'        =>$get_expiry_date[0]->Expiry_Date,
						'request_date'         => timeStamp($this->security->xss_clean(strip_tags($this->input->post('request_date')))),  
						'dispatch_date'         => timeStamp($this->security->xss_clean(strip_tags($this->input->post('dispatch_date')))),  
						'relocated_quantity' => $this->security->xss_clean(strip_tags($this->input->post('relocated_quantity'))),
						'not_relocated_quantity' => $this->security->xss_clean(strip_tags($this->input->post('not_relocated_quantity'))),
						'quantity_dispatched' => $this->security->xss_clean(strip_tags($this->input->post('relocated_quantity'))),
						'relocation_remark'    => $this->security->xss_clean(strip_tags($this->input->post('relocation_remark'))),  
						'is_notification'=>'0',
						'is_temp'	=>'0',
						'current_status'=>3
						);
							if ($is_temp=='1') {
								$array2 = array('not_quantity_received'=>$not_quantity_received,'indent_status' => $status,'relocation_status' => $status,"updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						}
						else{
								$array2 = array('indent_status' => $indent_status,'relocation_status' => $relocation_status,"updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
							
											
						}
							$data = array_merge($update_array, $array2);
						$this->db->where('inventory_id',$get_inventory_id[0]->inventory_id);
						$this->db->update('tbl_inventory',$data);
						
						$this->session->set_flashdata('tr_msg','Relocation Updated Successfully');
						redirect('Inventory/index/L');
						}
						else{
						echo 'hi';exit();
						if ($inventory_id!=NULL) {
						if ($is_temp=='0' || $is_temp=='') {
							$array2=array('id_mststate'         =>  $State_ID,
										'id_mstdistrict'      => $DistrictID,
										'id_mstfacility'      => $id_mstfacility,
										"updateBy" => $loginData->id_tblusers,
										 "updatedOn" => date('Y-m-d'),
										 'relocation_status'     => $relocation_status);
						}
						else{
								$array2 = array('not_quantity_received'=>$not_quantity_received,'indent_status' => $status,'relocation_status' => $status,"updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						}
						$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->where('inventory_id',$inventory_id);
						$this->db->update('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Relocation Updated Successfully');
						redirect('Inventory/index/L');
						}
						else{
							$array2=array('id_mststate'         =>  $State_ID,
										'id_mstdistrict'      => $DistrictID,
										'id_mstfacility'      => $id_mstfacility,
										"id_tblusers" => $loginData->id_tblusers, 
										"CreatedOn" => date('Y-m-d'),
										 'relocation_status' => $relocation_status,
											'indent_status' => $indent_status
										);
						$data = array_merge($insert_array, $array2);
						$this->db->insert('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Relocation Added Successfully');
						redirect('Inventory/index/L');
						}	
						}
					}
					}
					}
				}
			
	}
public function approved_indent(){
		$loginData = $this->session->userdata('loginData');
		if( ($loginData) && $loginData->user_type != '3' && $loginData->user_type != '2' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
		$this->load->library('form_validation');
		$this->load->model('Inventory_Model');
		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
			$query="SELECT * FROM `mstlookup` WHERE flag='58'";
			$filter['item_mst_look']=$this->db->query($query)->result();
			//pr($filter['item_mst_look']);
		
						
						//echo "<pre>";print_r($loginData);
						
						if( ($loginData) && $loginData->user_type == '1' ){
						$sess_where = " 1";
						}
						elseif( ($loginData) && $loginData->user_type == '2' ){
						
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
						}
						elseif( ($loginData) && $loginData->user_type == '3' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
						}
						elseif( ($loginData) && $loginData->user_type == '4' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
						}
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						$State_ID=$loginData->State_ID;
						$DistrictID=$loginData->DistrictID;
						$id_mstfacility=$loginData->id_mstfacility;
						if($loginData->State_ID==''||$loginData->State_ID==NULL){
						 $State_ID=0;
						}
						if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
						$DistrictID=0;
						}
						if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
						$id_mstfacility=0;
						}
						if ($this->security->xss_clean($this->input->post('search'))=='search') {
						
			$filter['Start_Date'] = timeStamp($this->input->post('startdate'));
			$filter['End_Date'] = timeStamp($this->input->post('enddate'));
			$filter['year'] = $this->security->xss_clean($this->input->post('year'));
			 $filter['item_type']=$this->security->xss_clean($this->input->post('item_type'));
			 $filter['id_mstfacility']=$this->security->xss_clean($this->input->post('id_mstfacility'));
						}
						elseif ($this->security->xss_clean($this->input->post('save'))=='save') {

						$item_id=$this->security->xss_clean($this->input->post('item_name'));

						$inventory_id=$this->security->xss_clean(strip_tags($this->input->post('inventory_id')));
						 $this->form_validation->set_rules('approved_quantity', 'approved_quantity', 'trim|required|numeric|xss_clean');
						  $this->form_validation->set_rules('quantity_rejected', 'rejected quantity ', 'trim|required|numeric|xss_clean');
						   $this->form_validation->set_rules('requested_quantity', 'transfer requested quantity', 'trim|numeric|xss_clean');
						    $this->form_validation->set_rules('warehouse_request_quantity', 'warehouse request quantity', 'trim|numeric|xss_clean');
						     $this->form_validation->set_rules('warehouse_name', 'warehouse name', 'trim|xss_clean');
						     $this->form_validation->set_rules('facility_name', 'facility name', 'trim|xss_clean');
						      $this->form_validation->set_rules('sno_remark', 'indent Remark', 'trim|max_length[100]|xss_clean');
						

					if ($this->form_validation->run() == FALSE) {

						$arr['status'] = 'false';
						$this->session->set_flashdata("tr_msg",validation_errors());
						//echo("arg1");
						redirect('Inventory/approved_indent/');
					}else{

							$transfer_to=$this->security->xss_clean($this->input->post('facility_name'));
							$indent_num=$this->security->xss_clean(strip_tags($this->input->post('indent_num')));
							$requested_quantity=$this->security->xss_clean(strip_tags($this->input->post('requested_quantity')));
							$quantity_rejected_by_state=$this->security->xss_clean(strip_tags($this->input->post('quantity_rejected_by_state')));
							$approved_quantity=$this->security->xss_clean(strip_tags($this->input->post('approved_quantity')));
							$is_temp=$this->security->xss_clean(strip_tags($this->input->post('is_temp')));
							$sno_remark=$this->security->xss_clean(strip_tags($this->input->post('sno_remark')));
							
						$query="SELECT count(i.inventory_id) as count,i.inventory_id,SUM(IFNULL(i.relocated_quantity,0)) as rem FROM tbl_inventory i WHERE i.indent_num=? AND i.indent_status IS NOT NULL and i.transfer_to=? and i.is_deleted=?";
						$get_data=$this->db->query($query,[$indent_num,$transfer_to,'0'])->result();
						if ($get_data[0]->rem<$requested_quantity && $get_data[0]->rem!=0)
						{
							/*print_r($get_data[0]->rem);
						echo " hi ".$requested_quantity;
						exit();*/
							$is_notification='1';
						}
						else{
							/*print_r($get_data[0]->rem);
						echo " bye  ".$requested_quantity;
						exit();*/
							$is_notification='0';
						}
						$update_array = array(  
						'refrence_id'=>$inventory_id,
						'approved_quantity'     => $this->security->xss_clean(strip_tags($this->input->post('approved_quantity'))),
						'quantity_rejected_by_state'     => $this->security->xss_clean(strip_tags($this->input->post('quantity_rejected'))),
						'sno_remark'     => $this->security->xss_clean(strip_tags($this->input->post('sno_remark'))),
						'requested_quantity'     => $this->security->xss_clean(strip_tags($this->input->post('requested_quantity'))),
						'warehouse_request_quantity'     => $this->security->xss_clean(strip_tags($this->input->post('warehouse_request_quantity'))),
						'source_name'     => $this->security->xss_clean($this->input->post('warehouse_name')),
						'transfer_to'     => $this->security->xss_clean($this->input->post('facility_name')),
						'indent_accept_date'=>date('Y-m-d'),
						'request_date'=>date('Y-m-d'),
						'is_notification'=>$is_notification,
						'is_temp'=>'0',
						'current_status'=>2
						
						);
						if ($get_data[0]->rem<$requested_quantity && $inventory_id!=NULL && $get_data[0]->rem!=0) {
							$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
							$data = array_merge($update_array, $array2);
							$this->db->where('inventory_id',$inventory_id);
							$this->db->update('tbl_inventory',$data);
							$pending_quantity=$requested_quantity-$get_data[0]->rem;
							$query="SELECT * FROM tbl_inventory i WHERE flag=? and inventory_id=?";
							$get_insert_array=$this->db->query($query,['I',$inventory_id])->result();
							//pr($get_insert_array);exit();
							$get_insert_array[0]->inventory_id=NULL;
							$get_insert_array[0]->refrence_id=$inventory_id;
							$get_insert_array[0]->quantity=NULL;
							$get_insert_array[0]->requested_quantity=0;
							$get_insert_array[0]->pending_quantity=$pending_quantity;
							$get_insert_array[0]->id_tblusers=$loginData->id_tblusers;
							$get_insert_array[0]->CreatedOn=date('Y-m-d');
							$get_insert_array[0]->indent_status=NULL;
							$get_insert_array[0]->relocation_status= NULL;
							$get_insert_array[0]->Entry_Date= NULL;
							$get_insert_array[0]->Expiry_Date= NULL;
							$get_insert_array[0]->quantity_dispatched= NULL;
							$get_insert_array[0]->relocated_quantity= NULL;
							$get_insert_array[0]->not_relocated_quantity= NULL;
							$get_insert_array[0]->not_quantity_received= NULL;
							$get_insert_array[0]->quantity_rejected= NULL;
							$get_insert_array[0]->quantity_received= NULL;
							$get_insert_array[0]->warehouse_request_quantity= NULL;
							$get_insert_array[0]->source_name= NULL;
							$get_insert_array[0]->updateBy= NULL;
							$get_insert_array[0]->updatedOn= NULL;
							$get_insert_array[0]->dispatch_date= NULL;
							$get_insert_array[0]->issue_num= NULL;
							$get_insert_array[0]->relocation_remark= NULL;
							$get_insert_array[0]->batch_num= NULL;
							$get_insert_array[0]->relocated_quantity= NULL;
							$get_insert_array[0]->relocation_status= NULL;
							$get_insert_array[0]->transfer_to= $transfer_to;
							$get_insert_array[0]->is_notification= '0';
							$get_insert_array[0]->is_temp= '0';
							$get_insert_array[0]->current_status= 2;

							$set_insert_array_values=$get_insert_array[0];
							$this->db->insert('tbl_inventory',$set_insert_array_values);
							$this->session->set_flashdata('tr_msg','Stock Indent Approved Successfully');
						redirect('Inventory/approved_indent');
						}
						///pr($update_array);exit();
						//echo $inventory_id;exit;
						else if ($inventory_id!=NULL) {

							/*if ($is_temp=='1' && $approved_quantity==0) {
								$update_arr         = array(   
								'batch_num'           => NULL,  
								'issue_num'           => NULL,  
								'Entry_Date'          => NULL,
								/*'approved_quantity'   => $this->security->xss_clean($this->input->post('approved_quantity')),
								'quantity_dispatched' => NULL,
								'quantity_received'   => NULL,
								'not_quantity_received' => NULL,
								'quantity_rejected'   => NULL,
								'Expiry_Date'         =>NULL,  
								'from_to_type'=>2,
								'source_name'         => NULL,
								'unrecognised'        =>NULL,
								'transfer_to'		  =>NULL,
								'Acceptance_Status'   => NULL,  
								'rejection_reason'    => NULL,  
								'indent_status'=>NULL,
								'relocation_status'=>NULL,
								'is_temp'=>'0',
								'is_notification'=>'1',
								);
								$this->db->where('inventory_id',$inventory_id);
								$this->db->update('tbl_inventory',$update_arr);
							}*/


						$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						$data = array_merge($update_array, $array2);
						$this->db->where('inventory_id',$inventory_id);
						$this->db->update('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Indent Approved Successfully');
						redirect('Inventory/approved_indent');
						}	
						}
					}
					
				}
				
					else
		{
			$filter['Start_Date'] = timeStamp(timeStampShow(date('Y-01-01')));
			$filter['End_Date'] = timeStamp(timeStampShow(date('Y-m-d')));
			$filter['item_type']='';
			$filter['id_mstfacility']='';
		}
		$year = date("Y",strtotime($filter['Start_Date']));
		 $year1 = date("Y",strtotime($filter['End_Date']));
		
		if (date('m', strtotime(($filter['Start_Date']))) < 4) {
			 $filter['year'] =($year-1)."-".$year1;
			  $content['start_date']=date("01-04-".($year-1));
		}
		else{
			 if ($year==$year1) {
		 	$year1+=1;
		 }
			 $filter['year'] =($year)."-".$year1;
			 $content['start_date']=date("01-04-".$year);
		}
		$filter['Start_Date']=$content['start_date'];
		$content['items']=$this->Inventory_Model->get_items();
		
		$this->session->set_userdata('invfilter',$filter);	
	
		
		/*$query="SELECT FacilityCode FROM `mstfacility` WHERE id_mstfacility=?";	
		$content['FacilityCode']=$this->db->query($query,[$loginData->id_mstfacility])->result();*/
			$query="SELECT inventory_lock as lock_days FROM `mstappstateconfiguration` WHERE id_tblappstateconfiguration=?";
			$content['inventory_lock']=$this->db->query($query,[1])->result();
		 $content['get_facilities'] = $this->Inventory_Model->get_all_facilities();
		$content['inventory_detail_all'] =$this->Inventory_Model->get_pending_indent(NULL,NULL,1);

		$content['filtered_arr'] =$this->Inventory_Model->get_pending_indent(NULL,'group',1);
		
		$content['indent_details'] =$this->Inventory_Model->get_indent_status(NULL,1);
		$content['indent_all_details'] =$this->Inventory_Model->get_all_indent_data(NULL,1);
		//pr($content['filtered_arr']);
		$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['relocation_status']=$this->db->query($query,['80'])->result();
						$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['reason_for_rejection']=$this->db->query($query,['78'])->result();

		$content['indent_items']=$this->Inventory_Model->get_items(NULL,NULL,'I');
		if($loginData->user_type==3)
		$content['subview']           = "stock_indent_approve";
		if($loginData->user_type==2)
			$content['subview']           = "stock_indent_approve_facility";
		$this->load->view('inventory/main_layout', $content);
			
	}
function AddTransferOut($inventory_id=NULL){
		//echo $inventory_id;
							$loginData = $this->session->userdata('loginData');
							//echo "<pre>";print_r($loginData);
							
							if( ($loginData) && $loginData->user_type == '1' ){
							$sess_where = " 1";
							}
							elseif( ($loginData) && $loginData->user_type == '2' ){
							
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
							}
							elseif( ($loginData) && $loginData->user_type == '3' ){ 
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
							}
							elseif( ($loginData) && $loginData->user_type == '4' ){ 
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
							}
							
							$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
							
							if($REQUEST_METHOD == 'POST')
							{
							$loginData = $this->session->userdata('loginData');
							$State_ID=$loginData->State_ID;
							$DistrictID=$loginData->DistrictID;
							$id_mstfacility=$loginData->id_mstfacility;
							if($loginData->State_ID==''||$loginData->State_ID==NULL){
							$State_ID=0;
							}
							if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
							$DistrictID=0;
							}
							if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
							$id_mstfacility=0;
							}
							//print_r($loginData);
							$item_id=$this->input->post('item_type');
							$item_name_ar=$this->Inventory_Model->get_items($item_id);
							
							if($item_id!=999)
							{
							$item_id=$this->input->post('item_type');
							$item_name=$item_name_ar[0]->id_mst_drugs;
							$item_type=$item_name_ar[0]->type;
							//echo $item_name[0]->drug_name;exit();
							}
							else{
							$item_id=$this->input->post('item_type');
							$batch_num   = $this->input->post('batch_num');
							$get_item_details=$this->Inventory_Model->get_receipt_in_transfer_out($item_id,$batch_num);
							//print_r($item_name_ar);
							$item_name=$get_item_details[0]->drug_name;
							$item_type=$get_item_details[0]->type;
							}
							if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$item_name_ar[0]->strength*$this->input->post('quantity');
						}
						else{
						
						$screening_tests=0;
						}
						if($this->input->post('from_to_type')==3){
						  $unrecognised=$this->input->post('unrecognised');
							$source_name=NULL;
							$transfer_to=NULL;
						}
						else{
							$source_name=$this->input->post('source_name');
							$unrecognised=NULL;
							$transfer_to=$this->input->post('destination_name');
						}
							$data = array(  
							'id_mst_drugs'  =>$item_id,
							'drug_name'  =>$item_name,
							'type'=>$item_type,
							'batch_num'   => $this->input->post('batch_num'),  
							'Entry_Date' => timeStamp($this->input->post('Entry_Date')),
							'quantity'     => $this->input->post('quantity'), 
							'quantity_screening_tests'     => $screening_tests,
							/*  'Expiry_Date'  => timeStamp($this->input->post('Expiry_Date')),*/  
							'from_to_type'   => $this->input->post('from_to_type'),  
							'source_name' => $source_name,
							'unrecognised'=>$unrecognised,
							/* 'Acceptance_Status'     => $this->input->post('Acceptance_Status'),*/  
							/* 'rejection'  => $this->input->post('rejection'), */  
							'id_mststate'   =>  $State_ID,
							'id_mstdistrict' => $DistrictID,
							'id_mstfacility'     => $id_mstfacility, 
							'transfer_to'=> $transfer_to, 
							'id_tblusers'  => $loginData->id_tblusers ,
							'flag'=>	'T',
							'is_deleted'=>'0'
							);
							
							//print_r($data);
							//echo $inventory_id;exit;
							if ($inventory_id!=NULL) {
							$this->db->where('inventory_id',$inventory_id);
							$this->db->update('tbl_inventory',$data);
							$this->session->set_flashdata('tr_msg','Receipt Updated Successfully');
							redirect('Inventory/AddTransferOut');
							}
							else{
							$this->db->insert('tbl_inventory',$data);
							$this->session->set_flashdata('tr_msg','Transfer Out Information Added Successfully');
							redirect('Inventory/AddTransferOut');
							}	
							}
							if($inventory_id!=NULL){
							//echo $inventory_id;
							$res_details = $this->Inventory_Model->edit_inventory_receipt($inventory_id);
							$content['receipt_details'] = $res_details;
							$content['edit_flag'] = 1;
							//print_r($res_details);
							}
							else{
							$content['edit_flag'] = 0;
							}
							$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag='58'";
							$content['item_type']=$this->db->query($query)->result();
							$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag='59'";
							$content['source_name']=$this->db->query($query)->result();
							$content['receiptitems']=$this->Inventory_Model->get_receipt_in_transfer_out();
							$content['mst_drug']=$this->Inventory_Model->get_items();
							$content['subview'] = "add_transfer_out";
							$this->load->view('inventory/main_layout', $content);
}
/*function get_batch_num(){
	$this->load->model('Inventory_Model');
        $id_mst_drugs = $this->input->post('id',TRUE);
        $data['batch_nums'] = $this->Inventory_Model->get_receipt_in_transfer_out($id_mst_drugs);
        echo json_encode($data);
    }*/

function get_facilities($state_warehouse=NULL)
{
		$is_warehouse = $this->input->get('is_warehouse',TRUE);
		//pr($is_warehouse);exit();
	 $this->load->model('Inventory_Model');
        $data['id_mstfacility'] = $this->Inventory_Model->get_all_facilities($is_warehouse);
        echo json_encode($data);
}  
function AddUtilization($inventory_id=NULL){
	$loginData = $this->session->userdata('loginData');
							//echo "<pre>";print_r($loginData);
							
							if( ($loginData) && $loginData->user_type == '1' ){
							$sess_where = " 1";
							}
							elseif( ($loginData) && $loginData->user_type == '2' ){
							
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
							}
							elseif( ($loginData) && $loginData->user_type == '3' ){ 
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
							}
							elseif( ($loginData) && $loginData->user_type == '4' ){ 
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
							}
							
							$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
							
							if($REQUEST_METHOD == 'POST')
							{
							$loginData = $this->session->userdata('loginData');
							$State_ID=$loginData->State_ID;
							$DistrictID=$loginData->DistrictID;
							$id_mstfacility=$loginData->id_mstfacility;
							if($loginData->State_ID==''||$loginData->State_ID==NULL){
							$State_ID=0;
							}
							if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
							$DistrictID=0;
							}
							if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
							$id_mstfacility=0;
							}
							//print_r($loginData);
							$item_id=$this->input->post('item_type');
							$item_name_ar=$this->Inventory_Model->get_items($item_id);
							
							if($item_id!=999)
							{
							$item_id=$this->input->post('item_type');
							$item_name=$item_name_ar[0]->id_mst_drugs;
							$item_type=$item_name_ar[0]->type;
							//echo $item_name[0]->drug_name;exit();
							}
							else{
							$item_id=$this->input->post('item_type');
							$batch_num   = $this->input->post('batch_num');
							$get_item_details=$this->Inventory_Model->get_receipt_in_transfer_out($item_id,$batch_num);
							//print_r($get_item_details);
							$item_name=$get_item_details[0]->drug_name;
							$item_type=$get_item_details[0]->type;
							}
							if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$this->input->post('quantity');
						$quantity=0;
						}
						else{
						
						$screening_tests=0;
						$quantity=$this->input->post('quantity');
					}
					
							$data = array(  
							'id_mst_drugs'  =>$item_id,
							'drug_name'  =>$item_name,
							'type'=>$item_type,
							'batch_num'   => $this->input->post('batch_num'),  
							'Entry_Date' => timeStamp($this->input->post('Entry_Date')),
							'quantity'     => $quantity,
							
							'quantity_screening_tests'     => $screening_tests,
							'Expiry_Date'  => timeStamp($this->input->post('Expiry_Date')), 
			
							'id_mststate'   =>  $State_ID,
							'id_mstdistrict' => $DistrictID,
							'id_mstfacility'     => $id_mstfacility, 
							'transfer_to'=> $transfer_to, 
							'id_tblusers'  => $loginData->id_tblusers ,
							'flag'=>	'U',
							'is_deleted'=>'0'
							);
							
							//print_r($data);
							//echo $inventory_id;exit;
							if ($inventory_id!=NULL) {
							$this->db->where('inventory_id',$inventory_id);
							$this->db->update('tbl_inventory',$data);
							$this->session->set_flashdata('tr_msg','Utilization Information Updated Successfully');
							redirect('Inventory/index/U');
							}
							else{
							$this->db->insert('tbl_inventory',$data);
							$lastid=$this->db->insert_id();

							$avaiable=array('Avaiable_quantity'=>$this->input->post('quantityRem'));
							$this->db->where('inventory_id',$lastid);
							$this->db->update('tbl_inventory',$avaiable);


							$this->session->set_flashdata('tr_msg','Utilization Information Added Successfully');
							redirect('Inventory/AddUtilization');
							}	
							}
							if($inventory_id!=NULL){
							//echo $inventory_id;
							$res_details = $this->Inventory_Model->edit_inventory_receipt($inventory_id);
							$content['receipt_details'] = $res_details;
							$content['edit_flag'] = 1;
							//print_r($res_details);
							}
							else{
							$content['edit_flag'] = 0;
							}
							$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag='58'";
							$content['item_type']=$this->db->query($query)->result();
							$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag='59'";
							$content['source_name']=$this->db->query($query)->result();
							$content['receiptitems']=$this->Inventory_Model->get_receipt_in_transfer_out();
	$content['subview'] = "Add_Utilization";
	$this->load->view('inventory/main_layout', $content);
}
function AddWastageMissing($inventory_id=NULL){
	$loginData = $this->session->userdata('loginData');
							//echo "<pre>";print_r($loginData);
							
							if( ($loginData) && $loginData->user_type == '1' ){
							$sess_where = " 1";
							}
							elseif( ($loginData) && $loginData->user_type == '2' ){
							
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
							}
							elseif( ($loginData) && $loginData->user_type == '3' ){ 
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
							}
							elseif( ($loginData) && $loginData->user_type == '4' ){ 
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
							}
							
							$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
							
							if($REQUEST_METHOD == 'POST')
							{
							$loginData = $this->session->userdata('loginData');
							$State_ID=$loginData->State_ID;
							$DistrictID=$loginData->DistrictID;
							$id_mstfacility=$loginData->id_mstfacility;
							if($loginData->State_ID==''||$loginData->State_ID==NULL){
							$State_ID=0;
							}
							if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
							$DistrictID=0;
							}
							if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
							$id_mstfacility=0;
							}
							//print_r($loginData);
							$item_id=$this->input->post('item_type');
							$item_name_ar=$this->Inventory_Model->get_items($item_id);
							
							if($item_id!=999)
							{
							$item_id=$this->input->post('item_type');
							$item_name=$item_name_ar[0]->id_mst_drugs;
							$item_type=$item_name_ar[0]->type;
							//echo $item_name[0]->drug_name;exit();
							}
							else{
							$item_id=$this->input->post('item_type');
							$batch_num   = $this->input->post('batch_num');
							$get_item_details=$this->Inventory_Model->get_receipt_in_transfer_out($item_id,$batch_num);
							//print_r($item_name_ar);
							$item_name=$get_item_details[0]->drug_name;
							$item_type=$get_item_details[0]->type;
							}
							if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$this->input->post('quantity');
						$quantity=0;
						}
						else{
						
						$screening_tests=0;
						$quantity=$this->input->post('quantity');
						}
					
							$data = array(  
							'id_mst_drugs'  =>$item_id,
							'drug_name'  =>$item_name,
							'type'=>$item_type,
							'batch_num'   => $this->input->post('batch_num'),  
							'Entry_Date' => timeStamp($this->input->post('Entry_Date')),
							'quantity'     => $quantity,
							
							'quantity_screening_tests'     => $screening_tests,
							'rejection'  => $this->input->post('rejection'), 
							'waste_type'=>$this->input->post('waste_type'),
							'id_mststate'   =>  $State_ID,
							'id_mstdistrict' => $DistrictID,
							'id_mstfacility'     => $id_mstfacility,  
							'id_tblusers'  => $loginData->id_tblusers ,
							'flag'=>	'W',
							'is_deleted'=>'0' 
							);
							
							//print_r($data);
							//echo $inventory_id;exit;
							if ($inventory_id!=NULL) {
							$this->db->where('inventory_id',$inventory_id);
							$this->db->update('tbl_inventory',$data);
							$this->session->set_flashdata('tr_msg','Wastage Missing Information Updated Successfully');
							redirect('Inventory/AddWastageMissing');
							}
							else{
							$this->db->insert('tbl_inventory',$data);
							$lastid=$this->db->insert_id();

							$avaiable=array('Avaiable_quantity'=>$this->input->post('quantityRem'));
							$this->db->where('inventory_id',$lastid);
							$this->db->update('tbl_inventory',$avaiable);


							$this->session->set_flashdata('tr_msg','Wastage Missing Information Added Successfully');
							redirect('Inventory/AddWastageMissing');
							}	
							}
							if($inventory_id!=NULL){
							//echo $inventory_id;
							$res_details = $this->Inventory_Model->edit_inventory_receipt($inventory_id);
							$content['receipt_details'] = $res_details;
							$content['edit_flag'] = 1;
							//print_r($res_details);
							}
							else{
							$content['edit_flag'] = 0;
							}
							$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag='58'";
							$content['item_type']=$this->db->query($query)->result();
						$query1="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag='60'";
						$content['Waste_type']=$this->db->query($query1)->result();
						//print_r($content['Waste_type']);
						$content['receiptitems']=$this->Inventory_Model->get_receipt_in_transfer_out();
	$content['subview'] = "Add_Wastage_Missing";
	$this->load->view('inventory/main_layout', $content);
}
function get_rem_val(){
        $batch_num = $this->input->post('id',TRUE);
        $type = $this->input->post('type',TRUE);
        $data['Rem_value'] = $this->Inventory_Model->Remaining_stock($batch_num,$type);	
        
        //print_r($data);die();
        echo json_encode($data);
    }  
    function get_maxdate_val(){
        $batch_num = $this->input->post('id',TRUE);
        $data['Last_date'] = $this->Inventory_Model->getmaxdate($batch_num);
        //print_r($data);die();
        echo json_encode($data);
    }  
    public function get_failure_report()
	{
		// die('heh');
		$loginData = $this->session->userdata('loginData');
		if( ($loginData) && $loginData->user_type != '3' && $loginData->user_type != '2' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
			$query="SELECT * FROM `mstlookup` WHERE flag='58'";
			$filter['item_mst_look']=$this->db->query($query)->result();
			//pr($filter['item_mst_look']);
		if($REQUEST_METHOD == 'POST')
		{
			$filter['Start_Date'] = timeStamp($this->input->post('startdate'));
			$filter['End_Date'] = timeStamp($this->input->post('enddate'));
			$filter['year'] = $this->input->post('year');
			 $filter['item_type']=$this->input->post('item_type');
			 if ($flag=='U') {
			 	 $filter['utilization_filter']=$this->input->post('utilization_filter');
			 }
		}	
		else
		{
			$filter['Start_Date'] = timeStamp(timeStampShow(date('Y-01-01')));
			$filter['End_Date'] = timeStamp(timeStampShow(date('Y-m-d')));
			$filter['item_type']='';
			 $year = date("Y",strtotime($filter['Start_Date']));
		 $year1 = date("Y",strtotime($filter['End_Date']));
		
		if (date('m', strtotime(($filter['Start_Date']))) < 4) {
			 $filter['year'] =($year-1)."-".$year1;
			  $content['start_date']=date("01-04-".($year-1));
		}
		else{
			 if ($year==$year1) {
		 	$year1+=1;
		 }
			 $filter['year'] =($year)."-".$year1;
			 $content['start_date']=date("01-04-".$year);
		}
		$filter['Start_Date']=$content['start_date'];
		}
		$content['items']=$this->Inventory_Model->get_items();
		
		$this->session->set_userdata('invfilter',$filter);	
		
		$content['inventory_detail_all'] =$this->Inventory_Model->get_inventory_details('F',NULL,1);
		$content['filtered_arr'] =$this->Inventory_Model->get_inventory_details('F','group',1);
		$content['fail_edit']=1;
		//pr($content['filtered_arr']);exit();
		$content['failure_items']=$this->Inventory_Model->get_items(NULL,NULL,'F');

		$content['subview']           = "stock_failure_state_wise";
		$this->load->view('inventory/main_layout', $content);
	}	
function check_pending_notification(){
	$indent_num = $this->input->get('indent_num',TRUE);
	$loginData = $this->session->userdata('loginData');
	$query="SELECT i2.count,i3.count1 FROM
				(SELECT indent_num FROM tbl_inventory i WHERE i.is_deleted='0' and i.indent_num='".$indent_num."' and i.flag='I' GROUP BY i.indent_num) AS i1
			LEFT JOIN 
				(SELECT COUNT(i.inventory_id) as COUNT,indent_num FROM tbl_inventory i WHERE ((i.Flag='I' and i.id_mstfacility='".$loginData->id_mstfacility."') or (i.Flag='L' and i.transfer_to='".$loginData->id_mstfacility."') ) AND i.relocation_status<3 AND i.relocation_status>0 and i.is_temp='0' and i.is_deleted='0' and i.indent_num='".$indent_num."' GROUP BY i.indent_num) AS i2
			ON i1.indent_num=i2.indent_num
			LEFT JOIN  
			(SELECT COUNT(i.inventory_id) AS count1,indent_num FROM tbl_inventory i WHERE (i.Flag='I' AND i.id_mstfacility='".$loginData->id_mstfacility."') AND i.is_deleted='0' and i.indent_num='".$indent_num."' AND (i.indent_accept_date IS NOT NULL OR i.indent_accept_date!='0000-00-00') AND (i.transfer_to IS NOT NULL OR i.transfer_to>0) AND (i.relocation_status IS NULL OR i.relocation_status=0)) AS i3
			ON i1.indent_num=i3.indent_num";
							$data['check_indent_num']=$this->db->query($query)->result();
	echo json_encode($data);						
}	
function delete_temp_receipt($inventory_id,$flag,$flag1)
	{
		if ($flag=='I') {
			$update_arr         = array(   
								'batch_num'           => NULL,  
								'issue_num'           => NULL,  
								'Entry_Date'          => NULL,
								/*'approved_quantity'   => $this->security->xss_clean($this->input->post('approved_quantity')),*/
								'quantity_dispatched' => NULL,
								'quantity_received'   => NULL,
								'not_quantity_received' => NULL,
								'quantity_rejected'   => NULL,
								'Expiry_Date'         =>NULL,  
								'from_to_type'=>2,
								'source_name'         => NULL,
								'unrecognised'        =>NULL,
								'transfer_to'		  =>NULL,
								'Acceptance_Status'   => NULL,  
								'rejection_reason'    => NULL,  
								'indent_status'=>NULL,
								'relocation_status'=>NULL,
								'is_temp'=>'0',
								'is_notification'=>'1',
								);
								$this->db->where('inventory_id',$inventory_id);
								$this->db->update('tbl_inventory',$update_arr);
								$this->session->set_flashdata('tr_msg','Stock Deleted Successfully');
								redirect('Inventory/index/'.$flag1);
		}
		else{
			$this->Inventory_Model->delete_receipt($inventory_id);
			$this->session->set_flashdata('tr_msg','Stock Deleted Successfully');
			redirect('Inventory/index/'.$flag1);
		}
	}
function get_cons_details()
{
	$id_mstfacility = $this->input->get('facility_id',TRUE);
	$query="SELECT m.*,s.StateName,d.DistrictName FROM `mstfacility` m INNER join mststate s on m.id_mststate=s.id_mststate INNER join mstdistrict d on m.id_mstdistrict=d.id_mstdistrict WHERE id_mstfacility=?";	
		$data['data']=$this->db->query($query,[$id_mstfacility])->result();
	echo json_encode($data);
}
function get_treatment_alert()
{
	$data=$this->Inventory_Model->get_treatment_alert([1,13,14]);
	pr($data);exit();
}
function update_fail_status($inventory_id,$status)
{
	$update_arr         = array(   
								'failure_accept_status'           => $status,
								);
								$this->db->where('inventory_id',$inventory_id);
								$this->db->update('tbl_inventory',$update_arr);
								$this->session->set_flashdata('tr_msg','Request  Updated Successfully');
			redirect('Inventory/get_failure_report/');
}
}
