<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class State extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Common_Model');

		$loginData = $this->session->userdata('loginData');
		if($loginData->user_type != 1){
			$this->session->set_flashdata('er_msg','You are not logged-in, please login again to continue');
			redirect('login');	
		}
	}

	public function index()
	{
		$query = "select * from mststate";
		$content['state_list'] = $this->Common_Model->query_data($query);

		$content['subview'] = "list_state";
		$this->load->view('admin/main_layout', $content);
	}

	public function add()
	{	
		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == "POST")
		{
			$insertArray = array(
				'StateCd' => $this->input->post('stateCd'),
				'StateName' => $this->input->post('stateName'),
				'StateShortNm'	=>	$this->input->post('stateShortNm'),
				'LanguageID' =>1,
				);

			$this->Common_Model->insert_data('mststate', $insertArray);
			$this->session->set_flashdata('tr_msg', 'Successfully added State');
			redirect('admin/state');
		}

		$content['subview'] = 'add_state';
		$this->load->view('admin/main_layout', $content);
	}

	public function edit($id = null)
	{	

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == "POST")
		{
			$updateArray = array(
				'StateCd' => $this->input->post('stateCd'),
				'StateName' => $this->input->post('stateName'),
				'StateShortNm'	=>	$this->input->post('stateShortNm'),
				'LanguageID' =>1,
				);

			$this->Common_Model->update_data('mststate', $updateArray, 'id_mststate', $id);
			$this->session->set_flashdata('tr_msg', 'Successfully updated State');
			redirect('admin/state');
		}


		$sql = "select * from mststate where id_mststate = $id";

		$content['State_Details'] = $this->Common_Model->query_data($sql)[0];
		$content['subview'] = 'edit_state';

		$this->load->view('admin/main_layout', $content);
	}

	public function delete($id = null)
	{
		$sql = "delete from mststate where id_mststate=$id ";

		$this->db->query($sql);
		$this->session->set_flashdata('tr_msg' ,"State Deleted Successfully");

		redirect("admin/state");
	}
}