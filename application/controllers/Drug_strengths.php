<?php

defined('BASEPATH') or exit('Direct script access not allaowed');

Class Drug_strengths extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$query = "SELECT * FROM `mst_drugs` where is_deleted = 0";
		$content['drugs'] = $this->db->query($query)->result();
		
		$content['subview'] = 'list_drug_strengths';
		$this->load->view('admin/main_layout', $content);
	}

	public function add()
	{	
		//echo "string";exit();
		$Request_Method = $this->input->server('REQUEST_METHOD');
		$loginData = $this->session->userdata('loginData');
		if($Request_Method == "POST")
		{

			$id_mst_drugs = $this->input->post('id_mst_drugs');
			if ($id_mst_drugs>0) {
				$where = "(id_mst_drug=".$id_mst_drugs.")";
			$this->db->where($where);
			$this->db->update('mst_drug_strength', array('is_deleted'=>1));
			$strengths    = $this->input->post('drug_strength');
			//pr($strengths);exit();
			$Unit    = $this->input->post('Unit');
			$buffer_stock    = $this->input->post('buffer_stock');

			$result = true;
			
			foreach ($strengths as $strength) {

				
				$query="SELECT count(id_mst_drug_strength) as count FROM `mst_drug_strength` where strength=? and id_mst_drug=?";
						$count=$this->db->query($query,[$strength,$id_mst_drugs])->result();
						//echo $count[0]->count;exit();
				if($count[0]->count>0){
					$updateArray = array(
				"id_mst_drug" => $id_mst_drugs,
				"strength"    => $strength,
				"Unit"		  => $Unit,
				"is_deleted"    => 0,
				"updateBy" => $loginData->id_tblusers,
				 "updatedOn" => date('Y-m-d')
				);
			$where = "(strength='".$strength."' and id_mst_drug=".$id_mst_drugs.")";
			$this->db->where($where);
			$this->db->update('mst_drug_strength', $updateArray);
			$result=true;
						}
						else{
							$insert_array = array(
					"id_mst_drug" => $id_mst_drugs,
					"strength"    => $strength,
					"Unit"		  => $Unit,
					"buffer_stock"=> $buffer_stock,
					"is_deleted"    => 0,
				   "createBy" => $loginData->id_tblusers,
				   "createdOn" => date('Y-m-d')
				);
							$result = $this->db->insert('mst_drug_strength', $insert_array);
						}
				

				if($result)
				{
					$this->session->set_flashdata('msg', 'Strengths Added Successfully');
				}
				else
				{
					$this->session->set_flashdata('er_msg', 'Error in adding Strengths');
				}


			}
			
			redirect('drugs');
		}
		else{
			$this->session->set_flashdata('er_msg', 'Please Add/Select Commodity before its Strength');
			redirect('drugs');
		}
	}
	}

	public function edit($id_mst_drugs = null)
	{	
	}

	public function delete($id_mst_drugs = null)
	{	
	}

	public function getStrengths($id_mst_drugs = null)
	{	

		$query = "SELECT * FROM mst_drugs where is_deleted=0 and id_mst_drugs = ".$id_mst_drugs;
		$drug_details = $this->db->query($query)->result();
		
		$query = "SELECT * FROM mst_drug_strength where is_deleted=0 and id_mst_drug = ".$id_mst_drugs;
		$strength_details = $this->db->query($query)->result();

		echo json_encode(array("drug_details" => $drug_details, "strength_details" => $strength_details));
	}

}