<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class State_lead_time extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Common_Model');

		$loginData = $this->session->userdata('loginData');
		if($loginData->user_type != 1 && $loginData->user_type != 3){
			$this->session->set_flashdata('er_msg','You are not logged-in, please login again to continue');
			redirect('login');	
		}

	}

	public function index()
	{
		$query = "select l.id_mststate,group_concat(l.lead_time order by l.type) as lead_time,s.stateName as StateName,s.stateShortNm,s.StateCd from mst_lead_time l inner join mststate s on l.id_mststate=s.id_mststate where l.is_deleted=0 group by l.id_mststate";
		$content['state_list'] = $this->db->query($query)->result();
	//pr($content['state_list']);
		$content['subview'] = "list_state";
		$this->load->view('admin/main_layout', $content);
		
	}

	/*public function add()
	{	
		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == "POST")
		{
			$insertArray = array(
				'StateCd' => $this->input->post('stateCd'),
				'StateName' => $this->input->post('stateName'),
				'StateShortNm'	=>	$this->input->post('stateShortNm'),
				'LanguageID' =>1,
				);

			$this->Common_Model->insert_data('mststate', $insertArray);
			$this->session->set_flashdata('tr_msg', 'Successfully added State');
			redirect('admin/state');
		}

		$content['subview'] = 'add_state';
		$this->load->view('admin/main_layout', $content);
	}
*/
	public function edit($id = null)
	{	

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == "POST")
		{
			$updateArray = array(
				'lead_time'	=>	$this->input->post('lead_time_drug'),
				);
			$where = "(type = 1 and id_mststate =".$this->input->post('id_mststate').")";
			$this->db->where($where);
			$this->db->update('mst_lead_time ', $updateArray);

			$updateArray = array(
				'lead_time'	=>	$this->input->post('lead_time_kit'),
				);
			$where = "(type = 2 and id_mststate =".$this->input->post('id_mststate').")";
			$this->db->where($where);
			$this->db->update('mst_lead_time ', $updateArray);

			$updateArray = array(
				'lead_time'	=>	$this->input->post('lead_time_hbig'),
				);
			$where = "(type = 4 and id_mststate =".$this->input->post('id_mststate').")";
			$this->db->where($where);
			$this->db->update('mst_lead_time', $updateArray); 
			



			$this->session->set_flashdata('tr_msg', 'Lead Time Updated Successfully');
			redirect('State_lead_time');
		}
		$content['subview'] = 'list_state';

		$this->load->view('admin/main_layout', $content);
	}

	public function delete($id = null)
	{
		$sql = "update mst_lead_time set is_deleted=1 where id_mst_lead_time=$id ";

		$this->db->query($sql);
		$this->session->set_flashdata('tr_msg' ,"State Deleted Successfully");

		redirect("State_lead_time");
	}
	function get_lead_details()
{
	$id_mststate = $this->input->get('id_mststate',TRUE);
	$query = "select l.id_mststate,group_concat(l.lead_time order by l.type) as lead_time,s.stateName as StateName,s.stateShortNm,s.StateCd from mst_lead_time l inner join mststate s on l.id_mststate=s.id_mststate where l.is_deleted=0 and l.id_mststate=? group by l.id_mststate";
		$data['State_Details'] = $this->db->query($query,[$id_mststate])->result()[0];	
	echo json_encode($data);
}
}