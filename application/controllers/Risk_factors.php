<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Risk_factors extends CI_Controller {

	public function __construct()
{
		parent::__construct();
		
		$this->load->model('Common_Model');
		$this->load->model('Patient_Model');
		
		$loginData = $this->session->userdata('loginData');
		if($loginData->user_type != 1){
			$this->session->set_flashdata('er_msg','You are not logged-in, please login again to continue');
			redirect('login');	
		}
	}


	public function index($id_mstlookup = null)
	{	
		$content['edit_flag'] = 0;

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == "POST")
		{
			if($id_mstlookup != null)
				{
					$updateArray = array(
						'LookupValue'       	        => $this->input->post('risk_factor_text'),
						);

					$this->Common_Model->update_data('mstlookup', $updateArray, 'id_mstlookup', $id_mstlookup);
					redirect("risk_factors");
				}
			else
			{
				$sql_lookupcode = "SELECT max(LookupCode) as maxcode FROM `mstlookup` where Flag = 3 and LanguageID = 1";
				$res = $this->Common_Model->query_data($sql_lookupcode)[0];

				$sql_lookupcode = "SELECT max(Sequence) as maxsequence FROM `mstlookup`";
				$res_sequence = $this->Common_Model->query_data($sql_lookupcode)[0];


				$insertArray = array(
				'LookUpType'		 		     => "",
				'Flag'       	                 => 3,
				'LookupCode' 				     => $res->maxcode + 1,
				'LookupValue'					 =>	$this->input->post('risk_factor_text'),
				'Sequence'						 => $res_sequence->maxsequence + 1,
				'LanguageID'				     => 1,
				);

			$this->Common_Model->insert_data('mstlookup', $insertArray);
			}
		}

		if($id_mstlookup != null)
		{
			$sql_details = "select * from mstlookup where id_mstlookup = ".$id_mstlookup;
			$res_details = $this->Common_Model->query_data($sql_details)[0];
			// print_r($res_details); die();
			$content['selected_risk_factors'] = $res_details;
			$content['edit_flag'] = 1;
		}

		$sql = "SELECT * FROM `mstlookup` where Flag = 3 and LanguageID = 1";
		$res = $this->Common_Model->query_data($sql);

		$content['risk_factors'] = $res;

		$content['subview'] = 'risk_factors';
		$this->load->view('admin/main_layout', $content);
	}

	public function delete($id_mstlookup = null)
	{
		$sql = "delete from mstlookup where id_mstlookup = $id_mstlookup";

		$this->db->query($sql);

		redirect("risk_factors");
	}

}