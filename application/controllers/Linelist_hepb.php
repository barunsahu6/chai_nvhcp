<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Linelist_hepb extends CI_Controller
{
    private $sess_where = '1';
    private $sess_mstdistrict ='1';
    private $sess_mstfacility ='1';
    private $filter = '1';
    private $filter1 = '1';
    public function __construct()
    {        
      parent::__construct(); 
      $this->load->model('Common_Model');
      $this->load->model('Patient_Model');
      $this->load->helper('common');
     set_time_limit(100000);
        ini_set('memory_limit', '100000M');
        ini_set('upload_max_filesize', '300000M');
        ini_set('post_max_size', '200000M');
        ini_set('max_input_time', 100000);
        ini_set('max_execution_time', 0);
        ini_set('memory_limit','-1');
      $loginData = $this->session->userdata('loginData');
      $sessiontemp =   $this->session->userdata('templatename'); 
    $tempval = $this->input->post('temp');

      $filters1data =   $this->session->userdata('filters1'); 
      //pr($filters1data);
      if($sessiontemp==''){
        $sessivaldata = 1;
      }else{
        $sessivaldata =   $sessiontemp;
      }

    if($filters1data['startdate']==''){
     $startdate = date('Y-07-01');  
  }else{

    $startdate =timeStampD($filters1data['startdate']);
  }
  if($filters1data['enddate']==''){

      $enddate = date('Y-m-d');
  }else{
     $enddate = timeStampD($filters1data['enddate']);
  }

        //Login filter query
      if( ($loginData) && $loginData->user_type == '1' ){
          $sess_where = '1';
          $sess_mstdistrict ='1';
          $sess_mstfacility ='1';

          $this->filter = " ( p.date_of_patient_registration between '".($startdate)."' AND '".($enddate)."' ||  p.HBSRapidDate between '".($startdate)."' AND '".($enddate)."' || p.HBSElisaDate between '".($startdate)."' AND '".($enddate)."' || p.HBSOtherDate between '".($startdate)."' AND '".($enddate)."' ) ";

          //$this->filter1 = " p1.CreatedOn between '".($startdate)."' and '".($enddate)."'";

          $this->filter1 = " ( p1.date_of_patient_registration between '".($startdate)."' AND '".($enddate)."' ||  p1.HBSRapidDate between '".($startdate)."' AND '".($enddate)."' || p1.HBSElisaDate between '".($startdate)."' AND '".($enddate)."' || p1.HBSOtherDate between '".($startdate)."' AND '".($enddate)."'  )";

         // $this->datefilter = "AND p.CreatedOn between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."' ";

         $this->datefilter = "AND ( p.HBSRapidDate between '".
         ($startdate)."' AND '".
         ($enddate)."' || p.HBSElisaDate between '".
         ($startdate)."' AND '".
         ($enddate)."' || p.HBSOtherDate between '".
         ($startdate)."' AND '".
         ($enddate)."'  || p.date_of_patient_registration between '".($startdate)."' AND '".($enddate)."'  )";
      }
      elseif( ($loginData) && $loginData->user_type == '2' ){

          $this->sess_where = " id_mststate = '".$loginData->State_ID."'";

          $this->sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

          $this->sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

          $this->filter = " p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."' AND p.id_mstfacility='".$loginData->id_mstfacility."' ";
          $this->filter1 = " p1.Session_StateID = '".$loginData->State_ID."' AND p1.Session_DistrictID ='".$loginData->DistrictID."' AND p1.id_mstfacility='".$loginData->id_mstfacility."' ";
           //$this->datefilter = "AND p.CreatedOn between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."' ";
           $this->datefilter = "AND ( p.date_of_patient_registration between '".($startdate)."' AND '".($enddate)."' ||  p.HBSRapidDate between '".($startdate)."' AND '".($enddate)."' || p.HBSElisaDate between '".($startdate)."' AND '".($enddate)."' || p.HBSOtherDate between '".($startdate)."' AND '".($enddate)."'   )";
      }
      elseif( ($loginData) && $loginData->user_type == '3' ){ 
          $this->sess_where = " id_mststate = '".$loginData->State_ID."'";
          $this->sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
          $this->sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";

          $this->filter = " p.Session_StateID = '".$loginData->State_ID."'";
          $this->filter1 = " p1.Session_StateID = '".$loginData->State_ID."'";
          // $this->datefilter = "AND p.CreatedOn between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."' ";
            $this->datefilter = "AND ( p.date_of_patient_registration between '".($startdate)."' AND '".($enddate)."' ||  p.HBSRapidDate between '".($startdate)."' AND '".($enddate)."' || p.HBSElisaDate between '".($startdate)."' AND '".($enddate)."' || p.HBSOtherDate between '".($startdate)."' AND '".($enddate)."'   )";
      }

       elseif( ($loginData) && $loginData->user_type == '5' ){ 
          $this->sess_where = " id_mststate = '".$loginData->State_ID."'";
          $this->sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
          $this->sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";

          $this->filter = " p.Session_StateID = '".$loginData->State_ID."'";
          $this->filter1 = " p1.Session_StateID = '".$loginData->State_ID."'";
          // $this->datefilter = "AND p.CreatedOn between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."' ";
            $this->datefilter = "AND ( p.date_of_patient_registration between '".($startdate)."' AND '".($enddate)."' ||  p.HBSRapidDate between '".($startdate)."' AND '".($enddate)."' || p.HBSElisaDate between '".($startdate)."' AND '".($enddate)."' || p.HBSOtherDate between '".($startdate)."' AND '".($enddate)."'  )";
      }
      elseif( ($loginData) && $loginData->user_type == '4' ){ 
          $this->sess_where       = " id_mststate = '".$loginData->State_ID."'";
          $this->sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
          $this->sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";

          $this->filter = " AND p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."'";
          $this->filter1 = " AND p1.Session_StateID = '".$loginData->State_ID."' AND p1.Session_DistrictID ='".$loginData->DistrictID."'";
          // $this->datefilter = "AND p.CreatedOn between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."' ";
           $this->datefilter = "AND ( p.date_of_patient_registration between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."' ||  p.HBSRapidDate between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."' || p.HBSElisaDate between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."' || p.HBSOtherDate between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."'   )";
      }
      if($loginData->user_type == 1){
        $this->session->set_flashdata('er_msg','You are not logged-in, please login again to continue');
        redirect('login');
    }
    if ($this->session->userdata('loginData') == null) {
     redirect('login');
 }
//echo '//'.$sessiontemp.'||'. $tempval.' and'. $sessiontemp1;exit();
 if ($sessivaldata==1)
 {
  //echo 'ddddd';exit();
     $this->header = array( 
                   'PatientGUID',
                     'UID_NUM',
                     'OPD/IPD ',     
                     'NVHCP ID',                 
                     'Patient Type',
                     'Name',
                     'Age between 0 and 1 Year',
                     'Age (in years)',
                     'Gender',
                     'Relation',
                     'Relation Name',  
                     'Home & Street Address',                    
                     'State',
                     'District',
                     'Block/Ward',
                     'Other',
                     'Village/Town/City',            
                     'Pincode',
                     'Mobile No.',
                     'Consent for Receiving Communication',
                     'Risk Factors',
                     'Other',
                     'HbsAg',
                     'HBSRapid',
                     'HBS Sample Collection Date',
                     'HBSRapidDate',
                     'HBSRapidResult',
                     'HBSRapidPlace',
                     'HBSRapidLab Name',
                     'HBSRapidOther Lab Name',
                     'HBSElisa',
                     'HBSElisaDate',
                     'HBSElisaResult',
                     'HBSElisaPlace',
                     'HBSELISALab Name',
                     'HBSELISAOther Lab Name',
                     'HBSOther',
                     'HBSOtherName',
                     'HBSOtherDate',
                     'HBSOtherResult',
                     'HBSOtherPlace',
                     'HBSLab Name',
                     'HBSOtherLab Name',
                     'VLB HepB',
                     'VLB SampleCollectionDate',
                     'VLB Is Sample Stored',
                     'VLB IS Sample Transported',
                     'VLB Remarks',
                     'VLDB Is Sample Accepted',
                     'VLDB Test Result Date',
                     'VLDB Result',
                     'VLDB Viral Load',
                     'Treatment Recommended',
                     'VLDB Reason For Rejection',
                     'VLDB Remarks',
                     'Date of Prescribing Tests',
                     'Date of issue of last investigation report',
                     'APRI',
                     'APRI Score',
                     'FIB 4',
                     'FIB 4 Score',
                     'Complicated/Uncomplicated',
                     'Variceal Bleed',
                     'Ascites',
                     'Encephalopathy',
                     'Child Pugh Score',
                     'Severity of HEP-B',
                     'Persistently Elevated ALT levels',
                     'Risk Factor',
                     'HIV/ART Regimen',
                     'Renal/CKD Stage',
                     'Reason For Prescribing Ribavarin',
                     'Last Menstrual Period',
                     'Pregnant',
                     'Expected Date of Delivery',
                     'Referred',
                     'Referring Doctor',
                     'Referring Doctor Other',
                     'Referred To',
                     'Date',
                     'Observations',
                     'Prescribing Doctor',
                     'Regimen Prescribed',
                     'TDF',
                     'TAF',
                     'Encetavir',
                     'Prescribing Date',
                     'Place Of Dispensation',
                     'Dispensation number',
                     'Visit date',
                     'Regimen prescribed',
                     'Days of pills dispensed',
                     'Advised next visit date',
                     'Dispensation number',
                     'Visit date',
                     'Regimen prescribed',
                     'Days of pills dispensed',
                     'Advised next visit date',
                     'Adherence %',
                     'Interruption Status',
                     'Interruption Others'
                 );
}else 
{

    $query = "SELECT ct.label_name FROM linelist_template tmp 
    LEFT JOIN linelist_category ct ON tmp.categoryId=ct.lcategoryId 
    WHERE tmp.masterId = ".$sessiontemp."";

    $db_nameval= [];
    $resdata = $this->db->query($query)->result();  
    
    foreach($resdata as $row){
      $db_nameval[] =  $row->label_name; 


  }
  $this->header = $db_nameval;
}

$this->folder_path = FCPATH . "application" . DIRECTORY_SEPARATOR . "linelists" . DIRECTORY_SEPARATOR;

$this->load->helper('file');
delete_files($this->folder_path);
}

public function index()
{
    //echo 'sdfdsfsd';exit();
     // die($this->filter);
   $loginData = $this->session->userdata('loginData');
        //echo "<pre>";print_r($loginData);

  

            if( ($loginData) && $loginData->user_type == '1' ){
                $sess_where = " 1";
                $sess_wherep = " 1";
            }
        elseif( ($loginData) && $loginData->user_type == '2' ){

                $sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND p.id_mstfacility='".$loginData->id_mstfacility."'";
                 $sess_wherep = " p1.Session_StateID = '".$loginData->State_ID."' AND p1.id_mstfacility='".$loginData->id_mstfacility."'";
            }
        elseif( ($loginData) && $loginData->user_type == '3' ){ 
                $sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
                $sess_wherep = " p1.Session_StateID = '".$loginData->State_ID."'";
            }

              elseif( ($loginData) && $loginData->user_type == '5' ){ 
                $sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
                $sess_wherep = " p1.Session_StateID = '".$loginData->State_ID."'";
            }
        elseif( ($loginData) && $loginData->user_type == '4' ){ 
                $sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
                $sess_wherep = "and  p1.Session_StateID = '".$loginData->State_ID."' ";
            }
      $sql= "select min(CreatedOn) as startdate from linelist p where ".$sess_where." and CreatedOn is not null and CreatedOn!='0000-00-00'";  
     $content['startdateval'] = $this->db->query($sql)->result();  
     $content['startdateval'][0]->startdate;  
    $REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

     if($REQUEST_METHOD == 'POST')
        {
            
            $filters1['id_search_state'] = $this->input->post('search_state');
            $filters1['id_input_district'] = $this->input->post('input_district');
            $filters1['id_mstfacility'] = $this->input->post('facility');   
            $filters1['startdate']      = timeStamp($this->input->post('startdate'));   
            $filters1['enddate']        = date($this->input->post('enddate'));
            //$filters1['enddate1']        = timeStamp($this->input->post('enddate'));
            if ($filters1['enddate']==date('d-m-Y')) {
                 $filters1['enddate']        = timeStamp(timeStampShow('-1 day',$filters1['enddate']));
            }
            else{
                $filters1['enddate']        = timeStamp($this->input->post('enddate'));
            }
            
        }
        else
        {   
             $filters1['id_search_state'] = 0;
            $filters1['id_input_district'] = 0;
            $filters1['id_mstfacility'] = 0;    
             $filters1['startdate']      = $content['startdateval'][0]->startdate;  
            $filters1['enddate']        = timeStamp(timeStampShow('-1 day',date('Y-m-d')));
            //$filters1['enddate1']        = date('Y-m-d');
            //pr($content['filters1']);exit();
        }
//pr($filters1);
         $this->session->set_userdata('filters1', $filters1);

    if($REQUEST_METHOD == 'POST')
    {
        $id_search_state = $this->input->post('search_state');
        //$id_input_district = $this->input->post('input_district');
       
        if($this->input->post('input_district')==''){
             $id_input_district =  "AND 1 "; 
        }else{
             $id_input_district =  "AND p.Session_DistrictID = ".$this->input->post('input_district').""; 
        }

        if($this->input->post('facility')==''){
             $id_mstfacility =  "AND 1 "; 
        }else{
             $id_mstfacility =  "AND p.id_mstfacility = ".$this->input->post('facility').""; 
        }

        $startdate = timeStamp($this->input->post('startdate'));
        $enddate = timeStamp($this->input->post('enddate'));
        $tempname = $this->input->post('temp'); 

            $filters1['id_search_state'] = $this->input->post('search_state') ;
            //$filters1['id_input_district'] = $this->input->post('input_district');
            //$filters1['id_mstfacility'] = $this->input->post('facility');  

             if($this->input->post('input_district')==''){
            $filters1['id_input_district'] =  "AND 1 "; 
        }else{
             $filters1['id_input_district'] =  "AND p.Session_DistrictID = ".$this->input->post('input_district')." "; 
        }

            if($this->input->post('facility')==''){
            $filters1['id_mstfacility'] =  "AND 1 "; 
        }else{
             $filters1['id_mstfacility'] =  "AND p.id_mstfacility = ".$this->input->post('facility')." "; 
        }

            $filters1['startdate']      = timeStamp($this->input->post('startdate'));   
            $filters1['enddate']        = timeStamp($this->input->post('enddate'));  



if($tempname!=''){
        $PostFilter = "p.Session_StateID = '".$id_search_state."' ".$id_input_district."  ".$id_mstfacility." AND ( p.date_of_patient_registration between '".($startdate)."' AND '".($enddate)."' ||  p.HBSRapidDate between '".($startdate)."' AND '".($enddate)."' || p.HBSElisaDate between '".($startdate)."' AND '".($enddate)."' || p.HBSOtherDate between '".($startdate)."' AND '".($enddate)."' ) AND '".($tempname)."'  ";
    }else{
        $PostFilter = "p.Session_StateID = '".$id_search_state."'  ".$id_input_district."  ".$id_mstfacility." AND ( p.date_of_patient_registration between '".($startdate)."' AND '".($enddate)."' || p.HBSRapidDate between '".($startdate)."' AND '".($enddate)."' || p.HBSElisaDate between '".($startdate)."' AND '".($enddate)."' || p.HBSOtherDate between '".($startdate)."' AND '".($enddate)."'  )   ";
    }
     $filter1 = " and ( p1.date_of_patient_registration between '".($startdate)."' AND '".($enddate)."' || p1.HBSRapidDate between '".($startdate)."' AND '".($enddate)."' || p1.HBSElisaDate between '".($startdate)."' AND '".($enddate)."' || p1.HBSOtherDate between '".($startdate)."' AND '".($enddate)."'  )"; 

        $this->session->set_userdata('templatename',$tempname);
    }
    else
    {   
            $filters1['id_search_state'] = 0 ;
            $filters1['id_input_district'] = 0;
            $filters1['id_mstfacility'] = 0;    
            $filters1['startdate']      = $content['startdateval'][0]->startdate;   
            $filters1['enddate']        = date('d-m-Y');  
              $startdate = $content['startdateval'][0]->startdate;  
              $enddate = date('Y-m-d');
              $tempname = '';

      $filter = " ( p.date_of_patient_registration between '".($startdate)."' AND '".($enddate)."' || p.HBSRapidDate between '".($startdate)."' AND '".($enddate)."' || p.HBSElisaDate between '".($startdate)."' AND '".($enddate)."' || p.HBSOtherDate between '".($startdate)."' AND '".($enddate)."'  )";           
      $filter1 = " and  ( p1.date_of_patient_registration between '".($startdate)."' AND '".($enddate)."' || p1.HBSRapidDate between '".timeStampD($startdate)."' AND '".timeStampD($enddate)."' || p1.HBSElisaDate between '".timeStampD($startdate)."' AND '".timeStampD($enddate)."' || p1.HBSOtherDate between '".timeStampD($startdate)."' AND '".timeStampD($enddate)."'  )"; 

      // $PostFilter = " p.CreatedOn between '".($startdate)."' AND '".($enddate)."'   ";

        $PostFilter = " ( p.date_of_patient_registration between '".($startdate)."' AND '".($enddate)."' || p.HBSRapidDate between '".($startdate)."' AND '".($enddate)."' || p.HBSElisaDate between '".($startdate)."' AND '".($enddate)."' || p.HBSOtherDate between '".($startdate)."' AND '".($enddate)."'  ) ";

       $this->session->unset_userdata('templatename');
       //echo 'gdfgf'; pr($filters1); die;        
  }

  


  /*$content['start_date'] = date('Y-m-d');
  $content['end_date']   = date('Y-m-d');*/

     $sql = "select 
          * 
  from 
  ( 
  SELECT 
  f.id_mstfacility, 
  f.facility_short_name AS hospital, 
  COUNT(PatientGUID) AS 'patients' 
  FROM linelist_hepb p 
  INNER JOIN mstfacility f 
  ON p.id_mstfacility = f.id_mstfacility 
  WHERE  ".$sess_where." and ".(isset($PostFilter)?$PostFilter:$this->filter)." 
  GROUP BY f.facility_short_name 
  UNION 
  SELECT 
  0, 'All' AS hospital, 
  COUNT(PatientGUID) AS 'patients' 
  FROM linelist_hepb p1 where ".$sess_wherep."   ".$filter1." 
) a ORDER BY a.id_mstfacility";
 

$res = $this->Common_Model->query_data($sql);
//pr($res);

$sql = "select * from mststate where ".$this->sess_where."";
$content['states'] = $this->db->query($sql)->result();

$sql = "select * from mstdistrict where ".$this->sess_mstdistrict."";
$content['districts'] = $this->db->query($sql)->result();

$sql = "select * from mstfacility where  ".$this->sess_mstfacility."";
$content['facilities'] = $this->db->query($sql)->result();  

$sql = "select * from linelist_master where userId = ".$loginData->id_tblusers."";
$content['templist'] = $this->db->query($sql)->result();


$content['facilities'] = $res;
$content['subview'] = 'linelist_hospital_hepb';
$this->load->view('admin/main_layout', $content);
}

public function getlist($id_mstfacility = null)
{
    ini_set('memory_limit', '-1');
    if ($id_mstfacility > 0) {

        $sql_facility_name = "select facilitycode from mstfacility where id_mstfacility = " . $id_mstfacility;
        $facility_name = $this->Common_Model->query_data($sql_facility_name);
        $facility = $facility_name[0]->facilitycode;
        $facility_where = "where id_mstfacility = " . $id_mstfacility;
    } else {
        $facility_where = "";
        $facility = "ALL";
    }

        // $sql_total_recs = "select count(*) as count from tblpatient where id_mstfacility = ".$id_mstfacility;
        // $res = $this->Common_Model->query_data($sql_total_recs);
        // $total_recs = $res[0]->count;

    $filename = $facility . '_' . date('Y-m-d') . ".csv";
    header("Content-Disposition: attachment; filename=\"$filename\"");
    header("Content-Type: text/csv");

    $out = fopen("php://output", 'w');
    $flag = false;

    $sql_line_list = "select 
    UID,TreatingHospital,Name,Age,Gender,RiskFactors,FathersHusbandsName,Address,Villagetown,Area,Block,District,Pincode,ContactNo,SampleCollectionDateforAntiHCVTest,AntiHCVTestDate,ResultReceivedDatebyPatient,RESULTOFANTIHCVTESTINGELISA,SampleCollectionDateforVL,SampleCollectioncenternameforVL,ViralLoadTestDate,BaselineViralLoad,ViralLoadStatus,DateofArrivaltoHospital,MedicalSpecialist,CirrhosisNoCirrhosisSTatus,ClinicalUSGtestdate,ClinicalUSGresult,Fibroscantestdate,LSMValueinkPa,Fibroscanresult,APRItestdate,AST,ASTNormal,APRIScore,FIB4testdate,ALT,FIB4Score,Albumin,Bilurubin,INR,Baseline1_val,Visit2_1_val,Visit3_1_val,Visit4_1_val,Visit5_1_val,Visit6_1_val,Visit7_1,Baseline2_val,Visit2_2_val,Visit3_2_val,Visit4_2_val,Visit5_2_val,Visit6_2_val,Visit7_2_val,DecompensatedCirrhosisResult,DecompensatedCirrhosisTestdate,Encephalopathy,Ascites,VaricealBleed,ChildScore,Genotype,GenotypeTestDate,Treatmentinitiationdate,Regimen,Duration,PillsDispensed,AdvisedNextVisitDate,Visit2_2,PillsLeft_2,AdvisedNextVisitDate_2,Adherence_2,Visit3_3,PillsLeft_3,AdvisedNextVisitDate_3,Adherence_3,Visit4_4,PillsLeft_4,AdvisedNextVisitDate_4,Adherence_4,Visit5_5,PillsLeft_5,AdvisedNextVisitDate_5,Adherence_5,Visit6_6,PillsLeft_6,AdvisedNextVisitDate_6,Adherence_6,Visit7_7,PillsLeft_7,AdvisedNextVisitDate_7,Adherence_7,ETRTestDate,ETRViralLoad,ETRTestResult,SVRTestDate,SVRViralLoad,SVRTestResult,DrugSideEffect,DrugCompliance,PatientStatus,TreatmentStatus,ReasonforTreatmentFailure,TreatmentCardType,Remarks,Durationoftrt_from_private,
    category,jailno,UploadedOn,lal_svr_date,lal_svr_result,lal_viral_load,
    AltContact,history_of_hcv,uid_serial_number,previous_treating_hospital,previous_treatment_outcome,occupation,occupation_other,hiv,ckd,diabetes,hypertension,hbv,call_output,cumulative_svr,
    cumulative_svr_viral_load,cumulative_svr_outcome
    from linelist " . $facility_where;

        // echo $sql_line_list; die();
        // ini_set('display_errors', 1);

    $content['line_list'] = $this->Common_Model->query_data($sql_line_list);

    if (count($content['line_list']) > 0) {
        foreach ($content['line_list'] as $row) {
                // print_r($row); die();
            if (!$flag) {
                    // display field/column names as first row
                    // $firstline = array_map(array($this,'map_colnames'), array_keys((array)$row));
                fputcsv($out, $this->header, ',', '"');
                $flag = true;
            }
            foreach ((array)$row as $value) {
                if ($value == null) {
                    $value = '';
                }
            }
            fputcsv($out, array_values((array)$row), ',', '"');
        }
    }
        // }
    fclose($out);
    exit();
}

public function download_list_zipped($id_mstfacility = 0)
{
    ini_set('memory_limit', '-1');

    if ($id_mstfacility > 0) {

        $query = "select facilitycode from mstfacility where id_mstfacility = " . $id_mstfacility;
        $result = $this->db->query($query)->result();
        $filename = $result[0]->facilitycode . "-" . date('Y-m-d-His');
        $facility_where = " ".$this->filter." and p.id_mstfacility = " . $id_mstfacility;

    } else {
        $facility_where = " ".$this->filter." ";
        $filename = "ALL-" . date('Y-m-d-His');
    }
    //echo $filename;
    //die($facility_where);
       $query = "select count(*) as count from linelist_hepb p WHERE " . $facility_where .' ' .$this->datefilter;
    // $query = "select count(*) as count from linelist_hepb p WHERE 1";
       // echo $query; die();
    $count_res = $this->db->query($query)->result();
    $count = $count_res[0]->count;
    $offset = 0;
    $limit = 2000;
     $file_path = $this->folder_path . $filename;
//echo $offset.'-'.$count;
    while ($offset < $count) {
        if ($offset == 0) {

            $file = fopen($file_path . '.csv', "w");            
            if (!$file) {
                die('cant open file handler');
            }
            fputcsv($file, $this->header);
        } else {
            $file = fopen($file_path . ".csv", "a");
        }

        $result = $this->get_line_list_result($facility_where, $limit, $offset);  

        foreach ($result as $row) {
            fputcsv($file, (array)$row);
        }

        fclose($file);
        $offset += $limit;
    }

    $this->load->library('zip');

    if (!$this->zip->read_file($file_path . '.csv')) {
      echo "cant read file";
      die();
  } else {
      header('Content-Encoding: UTF-8');
      $this->zip->download($filename . '.zip');
  }
}

public function get_line_list_result($filter_facility = "", $limit = 5000, $offset = 0)
{
  $sessiontemp =   $this->session->userdata('templatename'); 
  ini_set('memory_limit', '-1');

  if ($sessiontemp == '')
  {

      $query = "SELECT `PatientGUID`, `UID_NUM`, `OPD_ID`, `NVHCP_ID`, `Patient_Type`, `Name`, `Agebetween0and1Year`, `Age`, `Gender`, `Relation`, `Relation_Name`, `Address`, `State`, `District`, `Block_Ward`, `Other_addres`, `Village`, `Pincode`, `MobileNo`, `ConsentforReceivingCommunication`, `RiskFactors`, `Other`, `HbsAg`, `HBSRapid`, `HBSSampleCollectionDate`, `HBSRapidDate`, `HBSRapidResult`, `HBSRapidPlace`, `HBSRapidLabName`, `HBSRapidOtherLabName`, `HBSElisa`, `HBSElisaDate`, `HBSElisaResult`, `HBSElisaPlace`, `HBSELISALabName`, `HBSELISAOtherLabName`, `HBSOther`, `HBSOtherName`, `HBSOtherDate`, `HBSOtherResult`, `HBSOtherPlace`, `HBSLabName`, `HBSOtherLabName`, `VLBHepB`, `VLBSampleCollectionDate`, `VLBIsSampleStored`, `VLBISSampleTransported`, `VLBRemarks`, `VLDBIsSampleAccepted`, `VLDBTestResultDate`, `VLDBResult`, `VLDBViralLoad`, `TreatmentRecommended`, `VLDReasonForRejection`, `VLDBRemarks`, `DateofPrescribingTests`, `Dateofissueoflastinvestigationreport`, `APRI`, `APRIScore`, `FIB4`, `FIB4Score`, `ComplicatedUncomplicated`, `VaricealBleed`, `Ascites`, `Encephalopathy`, `ChildPughScore`, `SeverityofHEPB`, `PersistentlyElevatedALTlevels`, `RiskFactor`, `HIV_ARTRegimen`, `Renal_CKDStage`, `ReasonForPrescribingRibavarin`, `LastMenstrualPeriod`, `Pregnant`, `ExpectedDateofDelivery`, `Referred`, `ReferringDoctor`, `ReferringDoctorOther`, `ReferredTo`, `Date`, `Observations`, `PrescribingDoctor`, `RegimenPrescribed_2`, `TDF`, `TAF`, `Encetavir`, `PrescribingDate`, `PlaceOfDispensation`, `Dispensationnumber`, `Visitdate`, `Regimenprescribed`, `Daysofpillsdispensed`, `Advisednextvisitdate`, `Dispensationnumber_2`, `Visitdate_2`, `Regimenprescribed_3`, `Daysofpillsdispensed_2`, `Advisednextvisitdate_2`, `Adherence`, `InterruptionStatus`, `InterruptionOthers` FROM `linelist_hepb` p WHERE " . $filter_facility . "  ".$this->datefilter." ";

    // die($query);
    return $this->db->query($query)->result();


}else 
{
   $query = "SELECT ct.label_name,ct.db_name FROM linelist_template tmp 
   LEFT JOIN linelist_category ct ON tmp.categoryId=ct.lcategoryId 
   WHERE tmp.masterId  = ".$sessiontemp."";

   $db_name= array();
   $resdata = $this->db->query($query)->result();  

   foreach($resdata as $row){
    $db_nameval[] =  $row->db_name;
    $db_namevalval = implode(" ", $db_nameval);

}
 $query = "SELECT
".rtrim($db_namevalval,',')." 
from linelist p WHERE  " . $filter_facility . "  ".$this->datefilter." 
    limit $limit 
    offset $offset";

return $this->db->query($query)->result();
}


}

}
