<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Linelist_update extends CI_Controller {

	private $patientFilters;

	public function __construct()
	{
		parent::__construct();

		set_time_limit(100000);
        ini_set('memory_limit', '100000M');
        ini_set('upload_max_filesize', '300000M');
        ini_set('post_max_size', '200000M');
        ini_set('max_input_time', 100000);
        ini_set('max_execution_time', 0);
        ini_set('memory_limit','-1');
        
		$this->load->model('Common_Model');
		$this->load->model('Patient_Model');
		$this->load->helper('common');

	}
	public function index()
	{
		echo "Nothing data found";
	}

	/*Linelist Update start*/

	public function update_linelistMulti($Session_StateID = NULL , $id_mstfacility = NULL ,$FacilityType = NULL )
	{



	if($Session_StateID == NULL){

		$SessionStateID = "AND 1";exit;
		$stateId = "AND 1";
	}else{
		$SessionStateID = "AND l.Session_StateID IN( '".$Session_StateID."')";
		 $stateId = " id_mststate IN( ".$Session_StateID.")";
		 $SessionstateId = " Session_StateID IN( ".$Session_StateID.")";
	}
	if($FacilityType == NULL){
		$FacilityType    = "AND 1";
	}else{
		$FacilityType    = "AND FacilityType='".$FacilityType."' "; 
	}
	if($id_mstfacility == NULL || $id_mstfacility == 0){

		$id_mstfacility  = "AND 1";
		$id_mstfacilityn = "AND 1";
		$id_mstfacilitynPat = "AND 1";
	}else{
		$id_mstfacilityl  = "AND l.id_mstfacility in (".$id_mstfacility.")";
		$id_mstfacilityn  = "AND f.id_mstfacility in (".$id_mstfacility.")";

		$id_mstfacilitynPat  = "AND id_mstfacility in (".$id_mstfacility.")";
	}


	if($Session_StateID != NULL){



  //echo  $sql = "select id_mstfacility,id_mststate from mstfacility where  ".$stateId." ".$id_mstfacilityn." ".$FacilityType."  ";exit;
		 $sql = "SELECT f.id_mstfacility,id_mststate from mstfacility f INNER JOIN (SELECT DISTINCT id_mstfacility from tblpatient WHERE ".$SessionstateId." ".$id_mstfacilitynPat.") p ON f.id_mstfacility=p.id_mstfacility where  ".$stateId." ".$id_mstfacilityn." ".$FacilityType."  ";

				$res = $this->Common_Model->query_data($sql);
				//echo date("d");exit();
				foreach ($res as $row) {
 

		  //$query = "delete l.* FROM linelist l inner join `tblpatient` p on l.patientguid = p.patientguid where ((p.UpdatedOn >l.UpdatedOn) OR (p.UpdatedOn >=l.UpdatedOn and p.updatedon >=date_add(NOW(),INTERVAL-1 DAY))) and l.id_mstfacility= ".$row->id_mstfacility."  "; 

  $query = "delete l.* FROM linelist l inner join `tblpatient` p on l.patientguid = p.patientguid INNER JOIN mstfacility f on f.id_mstfacility=p.id_mstfacility where ((p.UpdatedOn >l.UpdatedOn) OR (p.UpdatedOn >=l.UpdatedOn and p.updatedon >=date_add(NOW(),INTERVAL-10 DAY))) and l.id_mstfacility= ".$row->id_mstfacility."  "; 
		$this->db->query($query);

		

					$insert_array = array(
				"StateID"            => $row->id_mststate,
				"id_mstfacility" => $row->id_mstfacility,
				"StartTime"		=>date("Y-m-d H:i:s")
			);

			$this->Common_Model->insert_data('line_list_tracker',$insert_array);


	 $query = "
		Insert into linelist 
		(
		
		id_tblpatient,
		id_mstfacility,
		Session_StateID,
		Session_DistrictID,
		linelist_synced,
		Serial,
		PatientGUID,
		OPD_Id,
		UID_Prefix,
		UID_Num,
		gender_look,
		FatherHusband,
		Add1,
		PIN,
		Mobile,
		date_of_patient_registration,
		T_DLL_01_VLC_Date,
		T_DLL_01_VLCount,
		T_DLL_01_VLC_Result,
		V1_Haemoglobin,
		V1_Albumin,
		V1_Bilrubin,
		V1_INR,
		V1_Cirrhosis,
		Cirr_Encephalopathy,
		Cirr_Ascites,
		Cirr_VaricealBleed,
		ChildScore,
		Result,
		T_Initiation,
		T_RmkDelay,
		T_NoPillStart,
		ETR_HCVViralLoad_Dt,
		SVR12W_HCVViralLoad_Dt,
		SVR12W_HCVViralLoadCount,
		patientAdherence,
		Age,
		FirstName,
		T_DurationValue,
		Pregnant,
		DeliveryDt,
		V1_Platelets,
		CreatedOn,
		CreatedBy,
		UpdatedOn,
		UpdatedBy,
		UploadedOn,
		UploadedBy,
		IsUploaded,
		IsDeleted,
		AdvisedSVRDate,
		ETR_PillsLeft,
		p_Next_Visitdt,
		ETRComments,
		SVRComments,    
		PatientType,
		Weight,
		CKDStage,
		ETRDoctor,
		IsSMSConsent,
		OtherRisk,
		ETRDoctorOther,
		SVRDoctorOther,
		VLSampleCollectionDate,
		VLRecieptDate,
		Relation,
		AntiHCV,
		HCVRapid,
		HCVRapidDate,
		HCVRapidResult,
		HCVRapidPlace,
		HCVRapidLabOther,
		HCVElisa,
		HCVElisaDate,
		HCVElisaResult,
		HCVElisaPlace,
		HCVElisaLabOther,
		HCVOther,
		HCVOtherName,
		HCVOtherDate,
		HCVOtherResult,
		HCVOtherPlace,
		HCVLabOther,
		HBSRapid,
		HBSRapidDate,
		HBSRapidResult,
		HBSRapidPlace,
		HBSRapidLabOther,
		HBSElisa,
		HBSElisaDate,
		HBSElisaResult,
		HBSElisaPlace,
		HBSElisaLabOther,
		HBSOther,
		HBSOtherName,
		HBSOtherDate,
		HBSOtherResult,
		HBSOtherPlace,
		HBSLabOther,
		V1_Creatinine,
		V1_EGFR,
		IsReferal,
		ReferalDate,
		PrescribingDate,
		ReferingDoctorOther,
		PrescribingDoctorOther,
		IsAgeMonths,
		SCRemarks,
		IsVLSampleTransported,
		VLTransportTemp,
		VLSCRemarks,
		VLResultRemarks,
		VLHepB,
		VLHepC,
		ALT,
		AST,
		AST_ULN,
		patientNAdherenceReason,
		patientNAdherenceReasonOther,
		LMP,
		DurationReason,
		BlockOther,
		IsSVRSampleAccepted,
		SVRRejectionReasonOther,
		VillageTown,
		StateName,
		DistrictName,
		BlockName,
		rg_name,
		ch_Prescribing_Dtt,
		ch_LastTest_Dt,
		ch_Clinical_US,
		ch_Clinical_US_Dt,
		ch_Fibroscan,
		ch_Fibroscan_Dt,
		ch_Fibroscan_LSM,
		ch_APRI_Score,
		ch_FIB4_FIB4,
		v1_visit_Dt,
		v1_PillsLeft,
		v1_Adherence,
		v1_NextVisit_Dt,
		v1_Comments,
		v1_Doctor,
		v1_DoctorOther,
		v1_NAdherenceReason,
		v1_NAdherenceReasonOther,
		v2_visit_Dt,
		v2_PillsLeft,
		v2_Adherence,
		v2_NextVisit_Dt,
		v2_Comments,
		v2_Doctor,
		v2_DoctorOther,
		v2_NAdherenceReason,
		v2_NAdherenceReasonOther,
		v3_visit_Dt,
		v3_PillsLeft,
		v3_Adherence,
		v3_NextVisit_Dt,
		v3_Comments,
		v3_Doctor,
		v3_DoctorOther,
		v3_NAdherenceReason,
		v3_NAdherenceReasonOther,
		v4_visit_Dt,
		v4_PillsLeft,
		v4_Adherence,
		v4_NextVisit_Dt,
		v4_Comments,
		v4_Doctor,
		v4_DoctorOther,
		v4_NAdherenceReason,
		v4_NAdherenceReasonOther,
		v5_visit_Dt,
		v5_PillsLeft,
		v5_Adherence,
		v5_NextVisit_Dt,
		v5_Comments,
		v5_Doctor,
		v5_DoctorOther,
		v5_NAdherenceReason,
		v5_NAdherenceReasonOther,
		drug_strength,
		Daclat_strength,
		Velpat_strength,
		ribpat_strength,
		look_LookupValue,
		ribavarin_lookupvalue,
		prescribing_name,
		dispensation_facicode,
		hiv_look,
		referd_doc,
		faci_facility_short_name,
		msrisk_lookup,
		otherdw_look,
		svrr_look,
		svrd_name,
		result_lookup,
		rejection_look,
		hbs_labd,
		hbs_labe,
		hbs_labf,
		hcv_labg,
		hcv_labh,
		hcv_labi,
		interruption_status,
		interruption_others,
		 PastUID,
       PastFacility) (select 
		
		 null,
    p.id_mstfacility,
    p.Session_StateID,
    p.Session_DistrictID,
    p.linelist_synced,
    p.Serial,
    p.PatientGUID,
    p.OPD_Id,
    p.UID_Prefix,
    lpad(p.UID_Num,6,0) as UID_Num,
    gender.LookupValue as gender_look,
    p.FatherHusband,
    p.Add1,
    p.PIN,
    p.Mobile,
    p.date_of_patient_registration,
    p.T_DLL_01_VLC_Date,
    p.T_DLL_01_VLCount,
    p.T_DLL_01_VLC_Result,
    p.V1_Haemoglobin,
    p.V1_Albumin,
    p.V1_Bilrubin,
    p.V1_INR,
    p.V1_Cirrhosis,
    p.Cirr_Encephalopathy,
    p.Cirr_Ascites,
    p.Cirr_VaricealBleed,
    p.ChildScore,
    p.Result,
    p.T_Initiation,
    p.T_RmkDelay,
    p.T_NoPillStart,
    p.ETR_HCVViralLoad_Dt,
    p.SVR12W_HCVViralLoad_Dt,
    p.SVR12W_HCVViralLoadCount,
    p.Adherence AS patientAdherence,
    p.Age,
    p.FirstName,
    p.T_DurationValue,
    p.Pregnant,
    p.DeliveryDt,
    p.V1_Platelets,
    p.CreatedOn,
    p.CreatedBy,
    p.UpdatedOn,
    p.UpdatedBy,
    p.UploadedOn,
    p.UploadedBy,
    p.IsUploaded,
    p.IsDeleted,
    p.AdvisedSVRDate,
    p.ETR_PillsLeft,
    p.Next_Visitdt as p_Next_Visitdt,
    p.ETRComments,
    p.SVRComments,
    p.PatientType,
    p.Weight,
    p.CKDStage,
    p.ETRDoctor,
    p.IsSMSConsent,
    p.OtherRisk,
    p.ETRDoctorOther,
    p.SVRDoctorOther,
    p.VLSampleCollectionDate,
    p.VLRecieptDate,
    p.Relation,
    p.AntiHCV,
    p.HCVRapid,
    p.HCVRapidDate,
    p.HCVRapidResult,
    p.HCVRapidPlace,
    p.HCVRapidLabOther,
    p.HCVElisa,
    p.HCVElisaDate,
    p.HCVElisaResult,
    p.HCVElisaPlace,
    p.HCVElisaLabOther,
    p.HCVOther,
    p.HCVOtherName,
    p.HCVOtherDate,
    p.HCVOtherResult,
    p.HCVOtherPlace,
    p.HCVLabOther,
    p.HBSRapid,
    p.HBSRapidDate,
    p.HBSRapidResult,
    p.HBSRapidPlace,
    p.HBSRapidLabOther,
    p.HBSElisa,
    p.HBSElisaDate,
    p.HBSElisaResult,
    p.HBSElisaPlace,
    p.HBSElisaLabOther,
    p.HBSOther,
    p.HBSOtherName,
    p.HBSOtherDate,
    p.HBSOtherResult,
    p.HBSOtherPlace,
    p.HBSLabOther,
    p.V1_Creatinine,
    p.V1_EGFR,
    p.IsReferal,
    p.ReferalDate,
    p.PrescribingDate,
    p.ReferingDoctorOther,
    p.PrescribingDoctorOther,
    p.IsAgeMonths,
    p.SCRemarks,
    p.IsVLSampleTransported,
    p.VLTransportTemp,
    p.VLSCRemarks,
    p.VLResultRemarks,
    p.VLHepB,
    p.VLHepC,
    p.ALT,
    p.AST,
    p.AST_ULN,
    p.NAdherenceReason AS patientNAdherenceReason,
    p.NAdherenceReasonOther AS patientNAdherenceReasonOther,
    p.LMP,
    p.DurationReason,
    p.BlockOther,
    p.IsSVRSampleAccepted,
    p.SVRRejectionReasonOther,
    p.VillageTown,
    s.StateName,
    d.DistrictName,
    b.BlockName,
    r.regimen_name as rg_name,
    ch.Prescribing_Dt as ch_Prescribing_Dtt,
    ch.LastTest_Dt as ch_LastTest_Dt,
    ch.Clinical_US as ch_Clinical_US,
    ch.Clinical_US_Dt as ch_Clinical_US_Dt,
    ch.Fibroscan as ch_Fibroscan,
    ch.Fibroscan_Dt as ch_Fibroscan_Dt,
    ch.Fibroscan_LSM as ch_Fibroscan_LSM,
    ch.APRI_Score as ch_APRI_Score,
    ch.FIB4_FIB4 as ch_FIB4_FIB4,
    v1.Visit_Dt as v1_visit_Dt,
    v1.PillsLeft as v1_PillsLeft,
    v1.Adherence as v1_Adherence,
    v1.NextVisit_Dt as v1_NextVisit_Dt,
    v1.Comments as v1_Comments,
    v1.Doctor as v1_Doctor,
    v1.DoctorOther as v1_DoctorOther,
    v1.NAdherenceReason as v1_NAdherenceReason,
    v1.NAdherenceReasonOther as v1_NAdherenceReasonOther,
    v2.Visit_Dt as v2_visit_Dt,
    v2.PillsLeft as v2_PillsLeft,
    v2.Adherence as v2_Adherence,
    v2.NextVisit_Dt as v2_NextVisit_Dt,
    v2.Comments as v2_Comments,
    v2.Doctor as v2_Doctor,
    v2.DoctorOther as v2_DoctorOther,
    v2.NAdherenceReason as v2_NAdherenceReason,
    v2.NAdherenceReasonOther as v2_NAdherenceReasonOther,
    v3.Visit_Dt as v3_visit_Dt,
    v3.PillsLeft as v3_PillsLeft,
    v3.Adherence as v3_Adherence,
    v3.NextVisit_Dt as v3_NextVisit_Dt,
    v3.Comments as v3_Comments,
    v3.Doctor as v3_Doctor,
    v3.DoctorOther as v3_DoctorOther,
    v3.NAdherenceReason as v3_NAdherenceReason,
    v3.NAdherenceReasonOther as v3_NAdherenceReasonOther,
    v4.Visit_Dt as v4_visit_Dt,
    v4.PillsLeft as v4_PillsLeft,
    v4.Adherence as v4_Adherence,
    v4.NextVisit_Dt as v4_NextVisit_Dt,
    v4.Comments as v4_Comments,
    v4.Doctor as v4_Doctor,
    v4.DoctorOther as v4_DoctorOther,
    v4.NAdherenceReason as v4_NAdherenceReason,
    v4.NAdherenceReasonOther as v4_NAdherenceReasonOther,
    v5.Visit_Dt as v5_visit_Dt,
    v5.PillsLeft as v5_PillsLeft,
    v5.Adherence as v5_Adherence,
    v5.NextVisit_Dt as v5_NextVisit_Dt,
    v5.Comments as v5_Comments,
    v5.Doctor as v5_Doctor,
    v5.DoctorOther as v5_DoctorOther,
    v5.NAdherenceReason as v5_NAdherenceReason,
    v5.NAdherenceReasonOther as v5_NAdherenceReasonOther,
    drugst.strength as drug_strength,
    Daclat.strength as Daclat_strength,
    Velpat.strength as Velpat_strength,
    ribpat.strength as ribpat_strength,
    GROUP_CONCAT(distinct look.LookupValue) as look_LookupValue,
    rpres.LookupValue as ribavarin_lookupvalue,
    mdspe.name as prescribing_name,
    facility.FacilityCode as dispensation_facicode,
    hivpres.LookupValue as hiv_look,
    mdical.name as referd_doc,
    refto.facility_short_name as faci_facility_short_name,
    GROUP_CONCAT(Distinct msrisk.LookupValue) as msrisk_lookup,
    otherdw.LookupValue as otherdw_look,
    svrr.LookupValue as svrr_look,
    svrd.name as svrd_name,
    result.LookupValue as result_lookup,
    svrrr.LookupValue as rejection_look,
    labd.FacilityCode as hbs_labd,
    labe.FacilityCode as hbs_labe,
    labf.FacilityCode as hbs_labf,
    labg.FacilityCode as hcv_labg,
    labh.FacilityCode as hcv_labh,
    labi.FacilityCode as hcv_labi,
     CASE WHEN p.InterruptReason = 1 THEN 'Death' WHEN p.InterruptReason = 2 THEN 'Loss to followup' WHEN p.InterruptReason = 99 THEN 'Others' END AS interruption_status,InterruptReasonOther,
     p.PastUID,
     pastf.facility_short_name
FROM
    tblpatient p
LEFT JOIN mststate s ON
    p.Session_StateID = s.id_mststate
LEFT JOIN mstdistrict d ON
    p.Session_StateID = d.id_mststate AND p.Session_DistrictID = d.id_mstdistrict
LEFT JOIN mstblock b ON
    p.Session_StateID = b.id_mststate AND p.Session_DistrictID = b.id_mstdistrict AND p.Block = b.id_mstblock
LEFT JOIN mstfacility f ON
    p.id_mstfacility = f.id_mstfacility
LEFT JOIN mst_medical_specialists m ON
    p.id_mst_medical_specialists = m.id_mst_medical_specialists
LEFT JOIN tblpatientcirrohosis ch ON
    p.PatientGUID = ch.PatientGUID
LEFT JOIN mst_regimen r ON
    p.T_Regimen = r.id_mst_regimen
LEFT JOIN (SELECT * from mstlookup where Flag=1 AND LanguageID=1) as gender 
ON p.Gender = gender.LookupCode 
LEFT JOIN tblpatientvisit pv ON
    p.PatientGUID = pv.PatientGUID
LEFT JOIN (SELECT * from mstlookup where Flag=10 AND LanguageID=1) as regimn 
ON p.T_Regimen = regimn.LookupCode 
LEFT JOIN (SELECT rd.*,ma1.strength from tblpatient_regimen_drug_data rd inner JOIN mst_drug_strength  ma1 ON rd.id_mst_drugs=ma1.id_mst_drug AND rd.id_mst_drug_strength=ma1.id_mst_drug_strength AND ma1.id_mst_drug=1
)drugst ON p.PatientGUID=drugst.patientguid AND pv.VisitNo=drugst.visit_no
LEFT JOIN (SELECT rd.*,ma1.strength from tblpatient_regimen_drug_data rd inner JOIN mst_drug_strength  ma1 ON rd.id_mst_drugs=ma1.id_mst_drug AND rd.id_mst_drug_strength=ma1.id_mst_drug_strength AND ma1.id_mst_drug=5
)Velpat ON p.PatientGUID=Velpat.patientguid AND pv.visitno=Velpat.visit_no
LEFT JOIN (SELECT rd.*,ma1.strength from tblpatient_regimen_drug_data rd inner JOIN mst_drug_strength  ma1 ON rd.id_mst_drugs=ma1.id_mst_drug AND rd.id_mst_drug_strength=ma1.id_mst_drug_strength AND ma1.id_mst_drug=3
)ribpat ON p.PatientGUID=ribpat.patientguid AND pv.visitno=ribpat.visit_no
LEFT JOIN (SELECT rd.*,ma1.strength from tblpatient_regimen_drug_data rd inner JOIN mst_drug_strength  ma1 ON rd.id_mst_drugs=ma1.id_mst_drug AND rd.id_mst_drug_strength=ma1.id_mst_drug_strength AND ma1.id_mst_drug=4
)Daclat ON p.PatientGUID=Daclat.patientguid AND pv.visitno=Daclat.visit_no
LEFT JOIN (SELECT * from mstlookup where Flag=3 AND LanguageID=1) as aa 
ON FIND_IN_SET(aa.LookupCode, p.Risk)
LEFT JOIN tblpatientvisit v1 on v1.PatientGUID=p.PatientGUID and v1.VisitNo=2
LEFT JOIN tblpatientvisit v2 on v2.PatientGUID=p.PatientGUID and v2.VisitNo=3
LEFT JOIN tblpatientvisit v3 ON v3.PatientGUID=p.PatientGUID and v3.VisitNo=4
LEFT JOIN tblpatientvisit v4 ON v4.PatientGUID=p.PatientGUID and v4.VisitNo=5
LEFT JOIN tblpatientvisit v5 ON v5.PatientGUID=p.PatientGUID and v5.VisitNo=6
left join tblRiskProfile tblrisk on p.PatientGUID = tblrisk.PatientGUID
left join (SELECT * from mstlookup where flag=26 AND LanguageID=1) look on tblrisk.RiskID = look.LookupCode 
left join (SELECT * from mstlookup where Flag=43 AND LanguageID=1) rpres on p.Ribavirin = rpres.LookupCode
left join mst_medical_specialists mdspe on p.PrescribingDoctor = mdspe.id_mst_medical_specialists
left join mstfacility facility on p.PrescribingFacility = facility.id_mstfacility
left join (SELECT * from mstlookup where Flag=42 AND LanguageID=1) hivpres on p.ART_Regimen = hivpres.LookupCode
left join mst_medical_specialists mdical on p.ReferingDoctor = mdical.id_mst_medical_specialists
left join mstfacility refto on p.ReferTo = refto.id_mstfacility
left join (SELECT * from mstlookup where Flag=3 AND LanguageID=1) msrisk on  p.Risk=msrisk.LookupCode
left join mstfacility laba on p.HAVRapidLabID=laba.id_mstfacility
left join (SELECT * from mstlookup where Flag=14 AND LanguageID=1) as otherdw on p.T_DurationOther=otherdw.LookupCode 
left join (SELECT * from mstlookup where Flag=45 AND LanguageID=1) svrr on p.SVRRejectionReason=svrr.LookupCode
left join (SELECT * from mstlookup where Flag=5 AND LanguageID=1) result on p.SVR12W_Result=result.LookupCode
left join mst_medical_specialists svrd on p.SVRDoctor=svrd.id_mst_medical_specialists
left join (SELECT * from mstlookup where Flag=45 AND LanguageID=1) svrrr on p.RejectionReason=svrrr.LookupCode
left join mstfacility labb on p.HAVElisaLabID=labb.id_mstfacility
left join mstfacility labc on p.HAVOtherLabID=labc.id_mstfacility
left join mstfacility labd on p.HBSRapidLabID=labd.id_mstfacility
left join mstfacility labe on p.HBSElisaLabID=labe.id_mstfacility
left join mstfacility labf on p.HBSOtherLabID=labf.id_mstfacility
left join mstfacility labg on p.HEVRapidLabID=labg.id_mstfacility
left join mstfacility labh on p.HEVElisaLabID=labh.id_mstfacility
left join mstfacility labi on p.HEVOtherLabID=labi.id_mstfacility
left join mstfacility labj on p.HCVRapidLabID=labj.id_mstfacility
left join mstfacility labk on p.HCVElisaLabID=labk.id_mstfacility
left join mstfacility labl on p.HCVOtherLabID=labl.id_mstfacility


left join mstfacility pastf on p.PastFacility=pastf.id_mstfacility
left join 
        (select PatientGUID as pguid from linelist ) b on p.PatientGUID=b.pguid where b.pguid is null and  p.id_mstfacility = ".$row->id_mstfacility."
group by p.id_tblpatient)";
$this->db->query($query);


		$update_array = array(
				"EndTime"         => date('Y-m-d H:i:s'),
				"Status"		=>'Success');
$array = array('StateID' => $row->id_mststate, 'id_mstfacility' => $row->id_mstfacility, 'STR_TO_DATE(`StartTime`,"%Y-%m-%d")' => date('Y-m-d'));
			$this->db->where($array);
			$this->db->update("line_list_tracker", $update_array);


	}

}

}


public function checkcount_linelist($Session_StateID = NULL){

	if($Session_StateID==3){
		$Session_StateIDv="p.Session_StateID=".$Session_StateID."";
	}elseif($Session_StateID==6){
		$Session_StateIDv="p.Session_StateID=".$Session_StateID."";
	}else{
		$Session_StateIDv="p.Session_StateID not in (3,6)";
	}

	 $sqlp="SELECT id_mstfacility,Session_StateID,COUNT(*) as countp FROM tblpatient p WHERE  ".$Session_StateIDv." GROUP BY p.id_mstfacility";
	$respatient = $this->Common_Model->query_data($sqlp);

	$sqll="SELECT id_mstfacility,Session_StateID,COUNT(*) as countl FROM linelist p WHERE ".$Session_StateIDv."   GROUP BY p.id_mstfacility";
	$reslinelist = $this->Common_Model->query_data($sqll);
//pr($respatient);
//pr($reslinelist);
$i=0; foreach ($respatient as $value) { 
	
//echo $value->id_mstfacility.'-'.$reslinelist[$i]->id_mstfacility.'<br>';
	$id_mstfacility_pat = $value->id_mstfacility;
	$Session_StateID_pat = $value->Session_StateID;
	$countp_pat = $value->countp;

if( ($id_mstfacility_pat==$reslinelist[$i]->id_mstfacility) && ($countp_pat!=$reslinelist[$i]->countl)){

	$this->update_linelistMulti($Session_StateID_pat,$id_mstfacility_pat);
	echo 'loop';
}

$i++;	
}

	/*$sqlp="SELECT id_mstfacility,COUNT(*) FROM tblpatient p WHERE ".$Session_StateIDv." GROUP BY p.id_mstfacility";
	$respatient = $this->Common_Model->query_data($sqlp);

	$sqll="SELECT id_mstfacility,COUNT(*) FROM linelist p WHERE ".$Session_StateIDv." GROUP BY p.id_mstfacility";
	$reslinelist = $this->Common_Model->query_data($sqll);*/


}



	/*End linelist update*/





}