<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public $colnames_follow_up = [
		'UID'              => 'UID',
		'FirstName'        => 'Patient Name',
		'hospital'         => 'Hospital',
		'address'          => 'Contact Address',
		'VillageTown'      => 'Village/Town',
		'BlockName'        => 'Block',
		'Mobile'           => 'Contact Number',
		'Next_Visitdt'     => 'Advised Visit Date',
		'NextVisitPurpose' => 'Purpose',
	];

	public function __construct()
	{	
		parent::__construct();
		$this->load->model('Common_Model');
		$this->load->model('Patient_Model');
		
		$loginData = $this->session->userdata('loginData');
		$loginData = $this->session->userdata('loginData');
		if($loginData == null)
		{
			redirect('login');
		}
	}

	public function index()
	{	
		$content['subview'] = 'reports';
		$this->load->view('admin/main_layout', $content);
	}

	public function monthly_report()
	{
error_reporting(0);
		$loginData = $this->session->userdata('loginData');

		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		

		if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_mstfacility'] = $this->input->post('facility');	
			$filters1['startdate']      = $this->input->post('startdate');	
			$filters1['enddate']        = $this->input->post('enddate');	
			$filters1['category']       = $this->input->post('category');
		}
		else
		{
			$filters1['id_mstfacility'] = 0;	
			$filters1['startdate']      = '2016-06-18';	
			$filters1['enddate']        = date('Y-m-d');
			$filters1['category']       = 0;
		}

			$filterdata = $this->session->set_userdata('filters1', $filters1);
	


	$this->load->model('Monthly_model');

	$content['filters1']   = $this->session->userdata('filters1');
	$content['count_1_1'] = $this->Monthly_model->get_data_1_1();

	$content['count_2_1'] = $this->Monthly_model->get_data_2_1();
	$content['count_2_2'] = $this->Monthly_model->get_data_2_2();
	$content['count_2_3'] = $this->Monthly_model->get_data_2_3();

	$content['count_3_1'] = $this->Monthly_model->get_data_3_1();
	$content['count_3_2'] = $this->Monthly_model->get_data_3_2();
	$content['count_3_3'] = $this->Monthly_model->get_data_3_3();
	$content['count_3_4'] = $this->Monthly_model->get_data_3_4();
	$content['count_3_5'] = $this->Monthly_model->get_data_3_5();
	$content['count_3_6'] = $this->Monthly_model->get_data_3_6();
	$content['count_3_7'] = $this->Monthly_model->get_data_3_7();

	$content['count_4_1'] = $this->Monthly_model->get_data_4_1();
	$content['count_4_2'] = $this->Monthly_model->get_data_4_2();
	$content['count_4_3'] = $this->Monthly_model->get_data_4_3();

	
	$content['start_date'] = '2019-01-01';
	$content['end_date']   = date('Y-m-d');

		$sql = "select * from mststate";
		$content['states'] = $this->db->query($sql)->result();

		$sql = "select * from mstdistrict";
		$content['districts'] = $this->db->query($sql)->result();

		$sql = "select * from mstfacility";
		$content['facilities'] = $this->db->query($sql)->result();	

		$content['subview'] = 'monthly_report';
		$this->load->view('pages/main_layout', $content);
	}	

}