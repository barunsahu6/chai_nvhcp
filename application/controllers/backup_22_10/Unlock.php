<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unlock extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Common_Model');
		$this->load->Model('Log4php_model');
		
		$loginData = $this->session->userdata('loginData');
		if($loginData == null)
		{
			redirect('login');
		}
		
	}
	private function getCurrentStatus($patientguid = null, $visit = null)
	{
		$sql = "select T_DurationValue,T_DurationOther from tblpatient where PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();
		
		switch($visit)
		{
			case 2 : switch($patient_data[0]->T_DurationValue)
			{
				case 8 : $status = 19;
				break;
				case 12 : $status = 5;
				break;
				case 16 : $status = 21;
				break;
				case 20 : $status = 25;
				break;
				case 24 : $status = 6;
				break;
			}
			break;
			case 3 : switch($patient_data[0]->T_DurationValue)
			{
				case 12 : $status = 7;
				break;
				case 16 : $status = 22;
				break;
				case 20 : $status = 26;
				break;
				case 24 : $status = 8;
				break;
			}
			break;
			case 4 : switch($patient_data[0]->T_DurationValue)
			{
				case 16 : $status = 23;
				break;
				case 20 : $status = 27;
				break;
				case 24 : $status = 9;
				break;
			}
			break;
			case 5 : switch($patient_data[0]->T_DurationValue)
			{
				case 20 : $status = 28;
				break;
				case 24 : $status = 10;
				break;
			}
			break;
			case 6 : switch($patient_data[0]->T_DurationValue)
			{
				case 24 : $status = 11;
				break;
			}
			break;
			

		}

		/*sss*/

		switch($visit)
		{
			case 2 : switch($patient_data[0]->T_DurationOther)
			{	
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			case 3 : switch($patient_data[0]->T_DurationOther)
			{
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			case 4 : switch($patient_data[0]->T_DurationOther)
			{
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			case 5 : switch($patient_data[0]->T_DurationOther)
			{
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			case 6 : switch($patient_data[0]->T_DurationOther)
			{
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			

		}
		/*end*/


		return $status;
	}

	public function unlock_process_registration($patientguid = null){

		$loginData = $this->session->userdata('loginData');
		$arr = array();

		/*registration*/
		/*$update_array = array(
			"id_mstfacility"    => $loginData->id_mstfacility,
			//"UID_Prefix"        => $this->security->xss_clean($this->input->post('hcv_uid_prefix')),
			'Session_StateID'   =>$loginData->State_ID,
			'Session_DistrictID'=>$loginData->DistrictID,
			"UID_Prefix"        => $this->security->xss_clean($this->getPrefix()),
			"UID_Num"           => $this->security->xss_clean($this->input->post('hcv_uid_num')),
			"OPD_Id"            => $this->security->xss_clean($this->input->post('opd_id')),
			"PTState"			=> $patient_type_state,
			"PTYear"			=> $this->security->xss_clean($this->input->post('patient_type_treatment_year')),
			"PastUID"			=> $this->security->xss_clean($this->input->post('patient_type_treatment_uid')),
			"PastFacility"		=> $patient_type_facility,
			"FirstName"         => $this->security->xss_clean($this->input->post('name')),
			"IsAgeMonths"       => $this->security->xss_clean($this->input->post('age_between')),
			"Age"               => $this->security->xss_clean($this->input->post('age')),
			"Gender"            => $this->security->xss_clean($this->input->post('gender')),
			"Pregnant"          => $this->security->xss_clean($this->input->post('pregnancy')),
			"Relation"          => $this->security->xss_clean($this->input->post('select_relative')),
			"FatherHusband"     => $this->security->xss_clean($this->input->post('relative_name')),
			"Add1"              => $this->security->xss_clean($this->input->post('address')),
			"State"             => $this->security->xss_clean($this->input->post('input_state')),
			"District"          => $this->security->xss_clean($this->input->post('input_district')),
			"DistrictOther"             => $this->security->xss_clean($this->input->post('input_district_other')),
			"BLOCK"             => $this->security->xss_clean($this->input->post('input_block')),
			"BlockOther"          => $this->security->xss_clean($this->input->post('input_block_other')),
			"VillageTown"       => $this->security->xss_clean($this->input->post('village')),
			"PIN"               => $this->security->xss_clean($this->input->post('pin')),
			"IsMobile_Landline" => $this->security->xss_clean($this->input->post('contact_type')),
			"Mobile"            => $this->security->xss_clean($this->input->post('contact_no')),
			"Aadhaar"           => $this->security->xss_clean($this->input->post('aadhar_health_id')),
			"IsSMSConsent"      => $this->security->xss_clean($this->input->post('sms_consent')),
			"Risk"              => $riskdata,
			"OtherRisk"         => $this->security->xss_clean($this->input->post('risk_factor_other')),
			"PatientType"       => $this->security->xss_clean($this->input->post('patient_type')),
			"ExperiencedCategory" => $ExperiencedCategory,
			"MF1"               => 1,
			"Status"            => 16,
			"SVR_TreatmentStatus" =>2,
			"CreatedBy"         => $loginData->id_tblusers,
			"UpdatedOn"			=> date('Y-m-d'),
			"UpdatedBy"			=> $loginData->id_tblusers,
			"UploadedBy"		=> $loginData->id_tblusers,
			"UploadedOn"		=> date('Y-m-d'),
			"basic_info_updated_on" => date('Y-m-d'),
			"basic_info_updated_by" => $loginData->id_tblusers,
			"IsEdited"			=> 1,
			"T_AntiHCV01_Result"=> 1,
			"NextVisitPurpose"  => 0
		);
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);*/
		/*patient_screening*/
		$update_array['fragment3editreason']         = $this->security->xss_clean($this->input->post('unlockresonval'));
		$update_array['LgmAntiHAV']              = null;
		$update_array['HAVRapid']                = null;
		$update_array['HAVElisa']                = null;
		$update_array['HAVOther']                = null;
		$update_array['HAVRapidDate']            = null;
		$update_array['HAVRapidResult']          = null;
		$update_array['HAVRapidPlace']           = null;
		$update_array['HAVRapidLabID']           = null;
		$update_array['HAVElisaDate']            = null;
		$update_array['HAVElisaResult']          = null;
		$update_array['HAVElisaPlace']           = null;
		$update_array['HAVElisaLabID']           = null;
		$update_array['HAVOtherName']            = null;
		$update_array['HAVOtherDate']            = null;
		$update_array['HAVOtherResult']          = null;
		$update_array['HAVOtherPlace']           = null;
		$update_array['HAVOtherLabID']           = null;
		$update_array['HAVRapidLabOther']        = null;
		$update_array['HAVLabOther']             = null;
		$update_array['HbsAg']                   = null;
		$update_array['HBSRapid']                = null;
		$update_array['HBSElisa']                = null;
		$update_array['HBSOther']                = null;
		$update_array['HBSRapidDate']            = null;
		$update_array['HBSRapidResult']          = null;
		$update_array['HBSRapidPlace']           = null;
		$update_array['HBSRapidLabID']           = null;
		$update_array['HBSElisaDate']            = null;
		$update_array['HBSElisaResult']          = null;
		$update_array['HBSElisaPlace']           = null;
		$update_array['HBSElisaLabID']           = null;
		$update_array['HBSOtherName']            = null;
		$update_array['HBSOtherDate']            = null;
		$update_array['HBSOtherResult']          = null;
		$update_array['HBSOtherPlace']           = null;
		$update_array['HBSOtherLabID']           = null;
		$update_array['HBSRapidLabOther']        = null;
		$update_array['HBSElisaLabOther']        = null;
		$update_array['HBSOther']                = 0;
		$update_array['HBSLabOther']             = null;
		$update_array['LgmAntiHBC']              = null;
		$update_array['HBCRapid']                = null;
		$update_array['HBCElisa']                = null;
		$update_array['HBCOther']                = null;
		$update_array['HBCRapidDate']            = null;
		$update_array['HBCRapidResult']          = null;
		$update_array['HBCRapidPlace']           = null;
		$update_array['HBCRapidLabID']           = null;
		$update_array['HBCElisaDate']            = null;
		$update_array['HBCElisaResult']          = null;
		$update_array['HBCElisaPlace']           = null;
		$update_array['HBCElisaLabID']           = null;
		$update_array['HBCOtherDate']            = null;
		$update_array['HBCOtherResult']          = null;
		$update_array['HBCOtherPlace']           = null;
		$update_array['HBCOtherLabID']           = null;
		$update_array['HBCLabOther']             = null;
		$update_array['HBCRapidLabOther']        = null;
		$update_array['HBCElisaLabOther']        = null;
		$update_array['AntiHCV']                 = null;
		$update_array['HCVRapid']                = null;
		$update_array['HCVElisa']                = null;
		$update_array['HCVOther']                = null;
		$update_array['HCVRapidDate']            = null;
		$update_array['HCVRapidResult']          = null;
		$update_array['HCVRapidPlace']           = null;
		$update_array['HCVRapidLabID']           = null;
		$update_array['HCVElisaDate']            = null;
		$update_array['HCVElisaResult']          = null;
		$update_array['HCVElisaPlace']           = null;
		$update_array['HCVElisaLabID']           = null;
		$update_array['HCVOtherName']            = null;
		$update_array['HCVOtherDate']            = null;
		$update_array['HCVOtherResult']          = null;
		$update_array['HCVOtherPlace']           = null;
		$update_array['HCVOtherLabID']           = null;
		$update_array['HCVRapidLabOther']        = null;
		$update_array['HCVElisaLabOther']        = null;
		$update_array['LgmAntiHEV']              = null;
		$update_array['HEVRapid']                = null;
		$update_array['HEVElisa']                = null;
		$update_array['HEVOther']                = null;
		$update_array['HEVRapidDate']            = null;
		$update_array['HEVRapidResult']          = null;
		$update_array['HEVRapidPlace']           = null;
		$update_array['HEVRapidLabID']           = null;
		$update_array['HEVElisaDate']            = null;
		$update_array['HEVElisaResult']          = null;
		$update_array['HEVElisaPlace']           = null;
		$update_array['HEVElisaLabID']           = null;
		$update_array['HEVOtherName']            = null;
		$update_array['HEVOtherDate']            = null;
		$update_array['HEVOtherResult']          = null;
		$update_array['HEVOtherPlace']           = null;
		$update_array['HEVOtherLabID']           = null;
		$update_array['HEVRapidLabOther']        = null;
		$update_array['Status']                  = 16;
		$update_array['MF2']                  = null; 
		$update_array['T_DLL_01_Date']                  = null; 

		$this->db->where('PatientGUID', $patientguid);
		$unitid                                  = $this->db->update('tblpatient', $update_array);
		/*patient_viral_load*/	
		$update_array                            = array(
			"VLHepC"                                 => null,
			"VLSampleCollectionDate"                 =>null,
			"IsVLSampleStored"                       => null,
			"VLStorageTemp"                          => null,
			"Storageduration"                        => null,
			"Storage_days_hrs"                       =>null,
			"IsVLSampleTransported"                  => null,
			"VLTransportTemp"                        => null,
			"VLTransporterName"                      => null,
			"VLTransporterDesignation"               => null,
			"VLTransportDate"                        => null,
			"VLLabID"                                => null,
			"VLLabID_Other"                          => null,
			"VLRecieptDate"                          => null,
			"VLReceiverName"                         => null,
			"VLReceiverDesignation"                  => null,
			"VLSCRemarks"                            => null,
			"IsSampleAccepted"                       => null,
			"T_DLL_01_VLC_Date"                      => null,
			"T_DLL_01_VLCount"                       => null,
			"T_DLL_01_VLC_Result"                    => null,
			"RejectionReason"                        => null,
			"VLResultRemarks"                        => null,
			"MF3"                                    => null,
			"BVLRecieptDate"                         => null,
			"IsBSampleAccepted"                      => null,
			"screening_updated_on"                   => null,
			"screening_updated_by"                   => null,
			"confirmation_updated_on"                => null,
			"confirmation_updated_by"                => null);

		$this->db->where('PatientGUID', $patientguid);
		$unitid                                  = $this->db->update('tblpatient', $update_array);

		$update_array['VLHepB']                  = null;
		$update_array['BVLSampleCollectionDate'] = null;
		$update_array['IsBVLSampleStored']       = null;
		$update_array['BVLStorageTemp']          = null;
		$update_array['IsBVLSampleTransported']  = null;
		$update_array['BVLTransportTemp']        = null;
		$update_array['BVLTransportDate']        = null;
		$update_array['BVLLabID']                = null;
		$update_array['BVLReceiverName']         = null;
		$update_array['BVLReceiverDesignation']  = null;
		$update_array['BVLSCRemarks']            = null;
		$update_array['T_DLL_01_BVLC_Date']      = null;
		$update_array['T_DLL_01_BVLCount']       = null;
		$update_array['T_DLL_01_BVLC_Result']    = null;
		$update_array['BVLResultRemarks']        = null;
		$update_array['BVLRecieptDate']          = null;
		$update_array['MF3']                     = null;



		$this->db->where('PatientGUID', $patientguid);
		$unitid                                  = $this->db->update('tblpatient', $update_array);
		/*testing*/
		$update_array                            = array(
			"PrescribingDate"                        =>  null,
			"PrescribingDate"                        =>  null,
			"V1_Haemoglobin"                         => null,
			"V1_Albumin"                             =>  null,
			"V1_Bilrubin"                            =>  null,
			"V1_INR"                                 =>  null,
			"ALT"                                    =>  null,
			"AST"                                    =>  null,
			"AST_ULN"                                =>  null,
			"V1_Platelets"                           =>  null,
			"Weight"                                 =>  null,
			"V1_Creatinine"                          =>  null,
			"V1_EGFR"                                =>  null,
			"V1_Cirrhosis"                           =>  null,
			"Cirr_TestDate"                          =>  null,
			"Cirr_Encephalopathy"                    =>  null,
			"Cirr_Ascites"                           =>  null,
			"Cirr_VaricealBleed"                     =>  null,
			"ChildScore"                             =>  null,
			"MF4"                                    =>  null,
			"baseline_updated_on"                    => date('Y-m-d'),
			"NextVisitPurpose"                       =>  null,
			"baseline_updated_by"                    => $loginData->id_tblusers,
			"special_updated_on"                     =>  date('Y-m-d'),
			"special_updated_by"                     =>  $loginData->id_tblusers,
			"treatment_updated_on"                   => date('Y-m-d'),
			"treatment_updated_by"                   => $loginData->id_tblusers,
			"CirrhosisStatus "                       =>  null,
			"Result"                                 => null);
		$this->db->where('PatientGUID', $patientguid);
		$unitid                                  = $this->db->update('tblpatient', $update_array);



		$insert_arraytest                        = array(

			"Visit_Date"                             => null,
			"Visit_No"                               =>null,
			"V1_Haemoglobin"                         =>null,
			"V1_Albumin"                             => null,
			"V1_Bilrubin"                            => null,
			"V1_INR"                                 => null,
			"ALT"                                    => null,
			"AST"                                    => null,
			"AST_ULN"                                => null,
			"V1_Platelets"                           => null,
			"V1_Creatinine"                          => null,
			"CreatedBy"                              => $loginData->id_tblusers,
		);
		$this->db->where('PatientGUID', $patientguid);
		$unitid   = $this->db->update('tblpatientaddltest', $insert_arraytest);

		$insert_array['FIB4']                    =null;
		$insert_array['FIB4_FIB4']               = null;
		$insert_array['Prescribing_Dt']          = null;
		$insert_array['LastTest_Dt']             = null;
		$insert_array['APRI_PC']                 = null;
		$insert_array['FIB4_PC']                 = null;
		$insert_array['CreatedBy']               = $loginData->id_tblusers;
		$insert_array['UpdatedBy']               = $loginData->id_tblusers;
		$insert_array['UpdatedOn']               =  date('Y-m-d');
		$insert_array['UploadedOn']              =  date('Y-m-d');
		$insert_array['id_mstfacility']          =  $loginData->id_mstfacility;

		$insert_array['FIB4']                    = null;
		$insert_array['fib4']                    = null;
		$insert_array['FIB4_FIB4']               = null;

		$insert_array['APRI']                    = 1;
		$insert_array['APRI_Dt']                 = null;
		$insert_array['APRI_Score']              = null;
		$insert_array['Fibroscan']               = null;
		$insert_array['Fibroscan_Dt']            = null;
		$insert_array['Fibroscan_LSM']           = null;
		$insert_array['Clinical_US']             = null;
		$insert_array['Clinical_US_Dt']          = null;
		$this->db->where('PatientGUID', $patientguid);
		$unitid   = $this->db->update('tblpatientcirrohosis', $insert_array);


		$update_array                            = array(
			"BreastFeeding"                          => null,
			"ART_Regimen"                            => null,
			"CKDStage"                               => null,
			"Ribavirin"                              => null,
			"PastTreatment"                          => null,
			"PreviousTreatingHospital"               => null,
			"PastRegimen"                            =>null,
			"PastRegimen_Other"                      => null,
			"PreviousDuration"                       => null,
			"Pregnant"                               =>null,
			"DeliveryDt"                             => null,
			"HCVPastTreatment"                       => null,
			"NWeeksCompleted"                        => null,
			"HCVPastOutcome"                         => null,
			"IsReferal"                              => null,
			"ReferingDoctor"                         => null,
			"ReferingDoctorOther"                    => null,
			"ReferTo"                                => null,
			"ReferalDate"                            =>null,
			"LastPillDate"                           => null,
			"LMP"                                    => null,
			"ChildScore"                             => null,
			"HCVHistory"                             =>null,
			"MF5"                                    => null,
			"SCRemarks"                              =>null
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblRiskProfile');

		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatient_regimen_drug_data');

		/*patient_prescription visit */

		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatientvisit');

		$update_array                            = array(
			"PrescribingFacility"                    =>null,
			"PrescribingDoctor"                      => null,
			"PrescribingDoctorOther"                 =>null,
			"T_Regimen"                              => null, 
			"HCVHistory"                             =>null,
			"PrescriptionPlace"                      =>null,
			"T_DurationValue"                        => null,
			"T_DurationValue"                        => null,
			"T_DurationOther"                        => null,
			"DurationReason"                         => null,
			"TreatmentUpdatedOn"                     => date('Y-m-d'),
			"TreatmentUpdatedBy"                     => $loginData->id_tblusers,
			"PrescribingDate"                        =>null,
			"DispensationPlace"                      => null,
			"MF6"                                    => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$update_array                            = array(
			"T_DurationOther"                        => null,
			"T_Initiation"                           => null,
			"T_VisitPlan"                            => null,
			"T_Regimen"                              =>null,
			"DispensationPlace"                      => null,
			"T_NoPillStart"                          => null,
			"AdvisedSVRDate"                         => null,
			"Current_Visitdt"                        =>null,
			"Next_Visitdt"                           => null,
			"MF7"                                    => null,
			"NextVisitPurpose"                       => null,
			"PDispensation"                          => null,
			"T_RmkDelay"                             => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$update_array                            = array(
			"Current_Visitdt"                        => null,
			"Next_Visitdt"                           => null,
			"AdvisedSVRDate"                         => null,
			"NextVisitPurpose"                       => null,
		);
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		/*eot*/		

		$update_array                            = array(
			"ETR_HCVViralLoad_Dt"                    => null,
			"ETR_PillsLeft"                          => null,
			"Adherence"                              => null,
			"AdvisedSVRDate"                         => null,
			"ETRComments"                            => null,
			"NextVisitPurpose"                       => null,
			"ETRDoctor"                              =>null,
			"ETRDoctorOther"                         => null,
			"SideEffectValue"                        => null,
			"Current_Visitdt"                        => null,
			"Next_Visitdt"                           => null,
			"DrugSideEffect"                         => null,
			"PDispensation"                          => null,
			"NAdherenceReason"                       =>null,
			"NAdherenceReasonOther"                  => null,
			"MF7"                                    =>null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		/*SVR*/			
		$update_array                            = array(

			'SVRDrawnDate'                           => null,
			'IsSVRSampleStored'                      => null,
			'SVRStorageTemp'                         => null,
			'IsSVRSampleTransported'                 => null,
			'SVRTransportTemp'                       => null,
			'SVRTransportDate'                       => null,
			'SVR12W_LabID'                           => null,
			'SVRTransporterName'                     => null,
			'SVRTransporterDesignation'              => null,
			'SVRTransportRemark'                     => null,
			'SVRReceiptDate'                         =>null,
			'SVRReceiverName'                        => null,
			'SVRReceiverDesignation'                 => null,
			'IsSVRSampleAccepted'                    => null,
			'SVR12W_HCVViralLoad_Dt'                 => null,
			'SVR12W_HCVViralLoadCount'               => null,
			'SVR12W_Result'                          => null,
			'SVRDoctor'                              => null,
			'SVRRejectionReason'                     => null,
			'SVRRejectionReasonOther'                => null,
			'SVRDoctorOther'                         => null,
			'SVRComments'                            => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$unitid  = $this->db->update('tblpatient', $update_array);
		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);
	}



	public function unlock_process_screening($patientguid = null){

		$loginData = $this->session->userdata('loginData');

		$arr = array();
		/*patient_screening*/
		$update_array['fragment3editreason']         = $this->security->xss_clean($this->input->post('unlockresonval'));
		/*$update_array['LgmAntiHAV']              = null;
		$update_array['HAVRapid']                = null;
		$update_array['HAVElisa']                = null;
		$update_array['HAVOther']                = null;
		$update_array['HAVRapidDate']            = null;
		$update_array['HAVRapidResult']          = null;
		$update_array['HAVRapidPlace']           = null;
		$update_array['HAVRapidLabID']           = null;
		$update_array['HAVElisaDate']            = null;
		$update_array['HAVElisaResult']          = null;
		$update_array['HAVElisaPlace']           = null;
		$update_array['HAVElisaLabID']           = null;
		$update_array['HAVOtherName']            = null;
		$update_array['HAVOtherDate']            = null;
		$update_array['HAVOtherResult']          = null;
		$update_array['HAVOtherPlace']           = null;
		$update_array['HAVOtherLabID']           = null;
		$update_array['HAVRapidLabOther']        = null;
		$update_array['HAVLabOther']             = null;
		$update_array['HbsAg']                   = null;
		$update_array['HBSRapid']                = null;
		$update_array['HBSElisa']                = null;
		$update_array['HBSOther']                = null;
		$update_array['HBSRapidDate']            = null;
		$update_array['HBSRapidResult']          = null;
		$update_array['HBSRapidPlace']           = null;
		$update_array['HBSRapidLabID']           = null;
		$update_array['HBSElisaDate']            = null;
		$update_array['HBSElisaResult']          = null;
		$update_array['HBSElisaPlace']           = null;
		$update_array['HBSElisaLabID']           = null;
		$update_array['HBSOtherName']            = null;
		$update_array['HBSOtherDate']            = null;
		$update_array['HBSOtherResult']          = null;
		$update_array['HBSOtherPlace']           = null;
		$update_array['HBSOtherLabID']           = null;
		$update_array['HBSRapidLabOther']        = null;
		$update_array['HBSElisaLabOther']        = null;
		$update_array['HBSOther']                = 0;
		$update_array['HBSLabOther']             = null;
		$update_array['LgmAntiHBC']              = null;
		$update_array['HBCRapid']                = null;
		$update_array['HBCElisa']                = null;
		$update_array['HBCOther']                = null;
		$update_array['HBCRapidDate']            = null;
		$update_array['HBCRapidResult']          = null;
		$update_array['HBCRapidPlace']           = null;
		$update_array['HBCRapidLabID']           = null;
		$update_array['HBCElisaDate']            = null;
		$update_array['HBCElisaResult']          = null;
		$update_array['HBCElisaPlace']           = null;
		$update_array['HBCElisaLabID']           = null;
		$update_array['HBCOtherDate']            = null;
		$update_array['HBCOtherResult']          = null;
		$update_array['HBCOtherPlace']           = null;
		$update_array['HBCOtherLabID']           = null;
		$update_array['HBCLabOther']             = null;
		$update_array['HBCRapidLabOther']        = null;
		$update_array['HBCElisaLabOther']        = null;
		$update_array['AntiHCV']                 = null;
		$update_array['HCVRapid']                = null;
		$update_array['HCVElisa']                = null;
		$update_array['HCVOther']                = null;
		$update_array['HCVRapidDate']            = null;
		$update_array['HCVRapidResult']          = null;
		$update_array['HCVRapidPlace']           = null;
		$update_array['HCVRapidLabID']           = null;
		$update_array['HCVElisaDate']            = null;
		$update_array['HCVElisaResult']          = null;
		$update_array['HCVElisaPlace']           = null;
		$update_array['HCVElisaLabID']           = null;
		$update_array['HCVOtherName']            = null;
		$update_array['HCVOtherDate']            = null;
		$update_array['HCVOtherResult']          = null;
		$update_array['HCVOtherPlace']           = null;
		$update_array['HCVOtherLabID']           = null;
		$update_array['HCVRapidLabOther']        = null;
		$update_array['HCVElisaLabOther']        = null;
		$update_array['LgmAntiHEV']              = null;
		$update_array['HEVRapid']                = null;
		$update_array['HEVElisa']                = null;
		$update_array['HEVOther']                = null;
		$update_array['HEVRapidDate']            = null;
		$update_array['HEVRapidResult']          = null;
		$update_array['HEVRapidPlace']           = null;
		$update_array['HEVRapidLabID']           = null;
		$update_array['HEVElisaDate']            = null;
		$update_array['HEVElisaResult']          = null;
		$update_array['HEVElisaPlace']           = null;
		$update_array['HEVElisaLabID']           = null;
		$update_array['HEVOtherName']            = null;
		$update_array['HEVOtherDate']            = null;
		$update_array['HEVOtherResult']          = null;
		$update_array['HEVOtherPlace']           = null;
		$update_array['HEVOtherLabID']           = null;
		$update_array['HEVRapidLabOther']        = null;
		$update_array['Status']                  = 16;
		$update_array['MF2']                  = null;*/
		$update_array['Status']                  = 1;

		$this->db->where('PatientGUID', $patientguid);
		$unitid                                  = $this->db->update('tblpatient', $update_array);
		/*patient_viral_load*/	
		$update_array                            = array(
			"VLHepC"                                 => null,
			"VLSampleCollectionDate"                 =>null,
			"IsVLSampleStored"                       => null,
			"VLStorageTemp"                          => null,
			"Storageduration"                        => null,
			"Storage_days_hrs"                       =>null,
			"IsVLSampleTransported"                  => null,
			"VLTransportTemp"                        => null,
			"VLTransporterName"                      => null,
			"VLTransporterDesignation"               => null,
			"VLTransportDate"                        => null,
			"VLLabID"                                => null,
			"VLLabID_Other"                          => null,
			"VLRecieptDate"                          => null,
			"VLReceiverName"                         => null,
			"VLReceiverDesignation"                  => null,
			"VLSCRemarks"                            => null,
			"IsSampleAccepted"                       => null,
			"T_DLL_01_VLC_Date"                      => null,
			"T_DLL_01_VLCount"                       => null,
			"T_DLL_01_VLC_Result"                    => null,
			"RejectionReason"                        => null,
			"VLResultRemarks"                        => null,
			"MF3"                                    => null,
			"BVLRecieptDate"                         => null,
			"IsBSampleAccepted"                      => null,
			"screening_updated_on"                   => null,
			"screening_updated_by"                   => null,
			"confirmation_updated_on"                => null,
			"confirmation_updated_by"                => null);

		$this->db->where('PatientGUID', $patientguid);
		$unitid                                  = $this->db->update('tblpatient', $update_array);

		$update_array['VLHepB']                  = null;
		$update_array['BVLSampleCollectionDate'] = null;
		$update_array['IsBVLSampleStored']       = null;
		$update_array['BVLStorageTemp']          = null;
		$update_array['IsBVLSampleTransported']  = null;
		$update_array['BVLTransportTemp']        = null;
		$update_array['BVLTransportDate']        = null;
		$update_array['BVLLabID']                = null;
		$update_array['BVLReceiverName']         = null;
		$update_array['BVLReceiverDesignation']  = null;
		$update_array['BVLSCRemarks']            = null;
		$update_array['T_DLL_01_BVLC_Date']      = null;
		$update_array['T_DLL_01_BVLCount']       = null;
		$update_array['T_DLL_01_BVLC_Result']    = null;
		$update_array['BVLResultRemarks']        = null;
		$update_array['BVLRecieptDate']          = null;
		$update_array['MF3']                     = null;



		$this->db->where('PatientGUID', $patientguid);
		$unitid                                  = $this->db->update('tblpatient', $update_array);
		/*testing*/
		$update_array                            = array(
			"PrescribingDate"                        =>  null,
			"PrescribingDate"                        =>  null,
			"V1_Haemoglobin"                         => null,
			"V1_Albumin"                             =>  null,
			"V1_Bilrubin"                            =>  null,
			"V1_INR"                                 =>  null,
			"ALT"                                    =>  null,
			"AST"                                    =>  null,
			"AST_ULN"                                =>  null,
			"V1_Platelets"                           =>  null,
			"Weight"                                 =>  null,
			"V1_Creatinine"                          =>  null,
			"V1_EGFR"                                =>  null,
			"V1_Cirrhosis"                           =>  null,
			"Cirr_TestDate"                          =>  null,
			"Cirr_Encephalopathy"                    =>  null,
			"Cirr_Ascites"                           =>  null,
			"Cirr_VaricealBleed"                     =>  null,
			"ChildScore"                             =>  null,
			"MF4"                                    =>  null,
			"baseline_updated_on"                    => date('Y-m-d'),
			"NextVisitPurpose"                       =>  null,
			"baseline_updated_by"                    => $loginData->id_tblusers,
			"special_updated_on"                     =>  date('Y-m-d'),
			"special_updated_by"                     =>  $loginData->id_tblusers,
			"treatment_updated_on"                   => date('Y-m-d'),
			"treatment_updated_by"                   => $loginData->id_tblusers,
			"CirrhosisStatus "                       =>  null,
			"Result"                                 => null);
		$this->db->where('PatientGUID', $patientguid);
		$unitid                                  = $this->db->update('tblpatient', $update_array);



		$insert_arraytest                        = array(

			"Visit_Date"                             => null,
			"Visit_No"                               =>null,
			"V1_Haemoglobin"                         =>null,
			"V1_Albumin"                             => null,
			"V1_Bilrubin"                            => null,
			"V1_INR"                                 => null,
			"ALT"                                    => null,
			"AST"                                    => null,
			"AST_ULN"                                => null,
			"V1_Platelets"                           => null,
			"V1_Creatinine"                          => null,
			"CreatedBy"                              => $loginData->id_tblusers,
		);
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatientaddltest', $insert_arraytest);

		$insert_array['FIB4']                    =null;
		$insert_array['FIB4_FIB4']               = null;
		$insert_array['Prescribing_Dt']          = null;
		$insert_array['LastTest_Dt']             = null;
		$insert_array['APRI_PC']                 = null;
		$insert_array['FIB4_PC']                 = null;
		$insert_array['CreatedBy']               = $loginData->id_tblusers;
		$insert_array['UpdatedBy']               = $loginData->id_tblusers;
		$insert_array['UpdatedOn']               =  date('Y-m-d');
		$insert_array['UploadedOn']              =  date('Y-m-d');
		$insert_array['id_mstfacility']          =  $loginData->id_mstfacility;

		$insert_array['FIB4']                    = null;
		$insert_array['fib4']                    = null;
		$insert_array['FIB4_FIB4']               = null;

		$insert_array['APRI']                    = 1;
		$insert_array['APRI_Dt']                 = null;
		$insert_array['APRI_Score']              = null;
		$insert_array['Fibroscan']               = null;
		$insert_array['Fibroscan_Dt']            = null;
		$insert_array['Fibroscan_LSM']           = null;
		$insert_array['Clinical_US']             = null;
		$insert_array['Clinical_US_Dt']          = null;
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatientcirrohosis', $insert_array);


		$update_array                            = array(
			"BreastFeeding"                          => null,
			"ART_Regimen"                            => null,
			"CKDStage"                               => null,
			"Ribavirin"                              => null,
			"PastTreatment"                          => null,
			"PreviousTreatingHospital"               => null,
			"PastRegimen"                            =>null,
			"PastRegimen_Other"                      => null,
			"PreviousDuration"                       => null,
			"Pregnant"                               =>null,
			"DeliveryDt"                             => null,
			"HCVPastTreatment"                       => null,
			"NWeeksCompleted"                        => null,
			"HCVPastOutcome"                         => null,
			"IsReferal"                              => null,
			"ReferingDoctor"                         => null,
			"ReferingDoctorOther"                    => null,
			"ReferTo"                                => null,
			"ReferalDate"                            =>null,
			"LastPillDate"                           => null,
			"LMP"                                    => null,
			"ChildScore"                             => null,
			"HCVHistory"                             =>null,
			"MF5"                                    => null,
			"SCRemarks"                              =>null
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblRiskProfile');

		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatient_regimen_drug_data');

		/*patient_prescription visit */

		/*insert visit guid start*/
		$sql = "SELECT PatientVisitGUID FROM `tblpatientvisit` WHERE PatientGUID = ?";
		$patient_visitdata = $this->db->query($sql,[$patientguid])->result();
		foreach ($patient_visitdata as $value) {

			$insert_array = array(
				"DeletedGUID"    => $value->PatientVisitGUID);
			$this->db->insert('tblDeleteVisits', $insert_array);
		}
		/*end visit insert*/

		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatientvisit');

		$update_array                            = array(
			"PrescribingFacility"                    =>null,
			"PrescribingDoctor"                      => null,
			"PrescribingDoctorOther"                 =>null,
			"T_Regimen"                              => null, 
			"HCVHistory"                             =>null,
			"PrescriptionPlace"                      =>null,
			"T_DurationValue"                        => null,
			"T_DurationValue"                        => null,
			"T_DurationOther"                        => null,
			"DurationReason"                         => null,
			"TreatmentUpdatedOn"                     => date('Y-m-d'),
			"TreatmentUpdatedBy"                     => $loginData->id_tblusers,
			"PrescribingDate"                        =>null,
			"DispensationPlace"                      => null,
			"MF6"                                    => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$update_array                            = array(
			"T_DurationOther"                        => null,
			"T_Initiation"                           => null,
			"T_VisitPlan"                            => null,
			"T_Regimen"                              =>null,
			"DispensationPlace"                      => null,
			"T_NoPillStart"                          => null,
			"AdvisedSVRDate"                         => null,
			"Current_Visitdt"                        =>null,
			"Next_Visitdt"                           => null,
			"MF7"                                    => null,
			"NextVisitPurpose"                       => null,
			"PDispensation"                          => null,
			"T_RmkDelay"                             => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$update_array                            = array(
			"Current_Visitdt"                        => null,
			"Next_Visitdt"                           => null,
			"AdvisedSVRDate"                         => null,
			"NextVisitPurpose"                       => null,
		);
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		/*eot*/		

		$update_array                            = array(
			"ETR_HCVViralLoad_Dt"                    => null,
			"ETR_PillsLeft"                          => null,
			"Adherence"                              => null,
			"AdvisedSVRDate"                         => null,
			"ETRComments"                            => null,
			"NextVisitPurpose"                       => null,
			"ETRDoctor"                              =>null,
			"ETRDoctorOther"                         => null,
			"SideEffectValue"                        => null,
			"Current_Visitdt"                        => null,
			"Next_Visitdt"                           => null,
			"DrugSideEffect"                         => null,
			"PDispensation"                          => null,
			"NAdherenceReason"                       =>null,
			"NAdherenceReasonOther"                  => null,
			"MF7"                                    =>null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		/*SVR*/			
		$update_array                            = array(

			'SVRDrawnDate'                           => null,
			'IsSVRSampleStored'                      => null,
			'SVRStorageTemp'                         => null,
			'IsSVRSampleTransported'                 => null,
			'SVRTransportTemp'                       => null,
			'SVRTransportDate'                       => null,
			'SVR12W_LabID'                           => null,
			'SVRTransporterName'                     => null,
			'SVRTransporterDesignation'              => null,
			'SVRTransportRemark'                     => null,
			'SVRReceiptDate'                         =>null,
			'SVRReceiverName'                        => null,
			'SVRReceiverDesignation'                 => null,
			'IsSVRSampleAccepted'                    => null,
			'SVR12W_HCVViralLoad_Dt'                 => null,
			'SVR12W_HCVViralLoadCount'               => null,
			'SVR12W_Result'                          => null,
			'SVRDoctor'                              => null,
			'SVRRejectionReason'                     => null,
			'SVRRejectionReasonOther'                => null,
			'SVRDoctorOther'                         => null,
			'SVRComments'                            => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);

	}



	public function unlock_process_viral_load($patientguid = null){
		//echo 'eeewqeqw';exit();
		$loginData = $this->session->userdata('loginData');
		$arr = array();

		/*patient_viral_load*/	
		$update_array                            = array(
		/*"VLHepC"                                 => null,
		"VLSampleCollectionDate"                 =>null,
		"IsVLSampleStored"                       => null,
		"VLStorageTemp"                          => null,
		"Storageduration"                        => null,
		"Storage_days_hrs"                       =>null,
		"IsVLSampleTransported"                  => null,
		"VLTransportTemp"                        => null,
		"VLTransporterName"                      => null,
		"VLTransporterDesignation"               => null,
		"VLTransportDate"                        => null,
		"VLLabID"                                => null,
		"VLLabID_Other"                          => null,
		"VLRecieptDate"                          => null,
		"VLReceiverName"                         => null,
		"VLReceiverDesignation"                  => null,
		"VLSCRemarks"                            => null,
		"IsSampleAccepted"                       => null,
		"T_DLL_01_VLC_Date"                      => null,
		"T_DLL_01_VLCount"                       => null,
		"T_DLL_01_VLC_Result"                    => null,
		"RejectionReason"                        => null,
		"VLResultRemarks"                        => null,
		"MF3"                                    => null,
		"BVLRecieptDate"                         => null,
		"IsBSampleAccepted"                      => null,
		"screening_updated_on"                   => null,
		"screening_updated_by"                   => null,
		"confirmation_updated_on"                => null,*/
		"Status"                                 => 32,
		"confirmation_updated_by"                => null);

		$this->db->where('PatientGUID', $patientguid);
		$unitid                                  = $this->db->update('tblpatient', $update_array);
		$update_array['fragment3editreason']         = $this->security->xss_clean($this->input->post('unlockresonval'));
		$update_array['VLHepB']                  = null;
		$update_array['BVLSampleCollectionDate'] = null;
		$update_array['IsBVLSampleStored']       = null;
		$update_array['BVLStorageTemp']          = null;
		$update_array['IsBVLSampleTransported']  = null;
		$update_array['BVLTransportTemp']        = null;
		$update_array['BVLTransportDate']        = null;
		$update_array['BVLLabID']                = null;
		$update_array['BVLReceiverName']         = null;
		$update_array['BVLReceiverDesignation']  = null;
		$update_array['BVLSCRemarks']            = null;
		$update_array['T_DLL_01_BVLC_Date']      = null;
		$update_array['T_DLL_01_BVLCount']       = null;
		$update_array['T_DLL_01_BVLC_Result']    = null;
		$update_array['BVLResultRemarks']        = null;
		$update_array['BVLRecieptDate']          = null;




		$this->db->where('PatientGUID', $patientguid);
		$unitid                                  = $this->db->update('tblpatient', $update_array);
		/*testing*/
		$update_array                            = array(
			"PrescribingDate"                        =>  null,
			"PrescribingDate"                        =>  null,
			"V1_Haemoglobin"                         => null,
			"V1_Albumin"                             =>  null,
			"V1_Bilrubin"                            =>  null,
			"V1_INR"                                 =>  null,
			"ALT"                                    =>  null,
			"AST"                                    =>  null,
			"AST_ULN"                                =>  null,
			"V1_Platelets"                           =>  null,
			"Weight"                                 =>  null,
			"V1_Creatinine"                          =>  null,
			"V1_EGFR"                                =>  null,
			"V1_Cirrhosis"                           =>  null,
			"Cirr_TestDate"                          =>  null,
			"Cirr_Encephalopathy"                    =>  null,
			"Cirr_Ascites"                           =>  null,
			"Cirr_VaricealBleed"                     =>  null,
			"ChildScore"                             =>  null,
			"MF4"                                    =>  null,
			"baseline_updated_on"                    => date('Y-m-d'),
			"NextVisitPurpose"                       =>  null,
			"baseline_updated_by"                    => $loginData->id_tblusers,
			"special_updated_on"                     =>  date('Y-m-d'),
			"special_updated_by"                     =>  $loginData->id_tblusers,
			"treatment_updated_on"                   => date('Y-m-d'),
			"treatment_updated_by"                   => $loginData->id_tblusers,
			"CirrhosisStatus "                       =>  null,
			"Result"                                 => null);
		$this->db->where('PatientGUID', $patientguid);
		$unitid                                  = $this->db->update('tblpatient', $update_array);



		$insert_arraytest                        = array(

			"Visit_Date"                             => null,
			"Visit_No"                               => null,
			"V1_Haemoglobin"                         => null,
			"V1_Albumin"                             => null,
			"V1_Bilrubin"                            => null,
			"V1_INR"                                 => null,
			"ALT"                                    => null,
			"AST"                                    => null,
			"AST_ULN"                                => null,
			"V1_Platelets"                           => null,
			"V1_Creatinine"                          => null,
			"CreatedBy"                              => $loginData->id_tblusers,
		);
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatientaddltest', $insert_arraytest);

		$insert_array['FIB4']                    = null;
		$insert_array['FIB4_FIB4']               = null;
		$insert_array['Prescribing_Dt']          = null;
		$insert_array['LastTest_Dt']             = null;
		$insert_array['APRI_PC']                 = null;
		$insert_array['FIB4_PC']                 = null;
		$insert_array['CreatedBy']               = $loginData->id_tblusers;
		$insert_array['UpdatedBy']               = $loginData->id_tblusers;
		$insert_array['UpdatedOn']               =  date('Y-m-d');
		$insert_array['UploadedOn']              =  date('Y-m-d');
		$insert_array['id_mstfacility']          =  $loginData->id_mstfacility;

		$insert_array['FIB4']                    = null;
		$insert_array['fib4']                    = null;
		$insert_array['FIB4_FIB4']               = null;

		$insert_array['APRI']                    = null;
		$insert_array['APRI_Dt']                 = null;
		$insert_array['APRI_Score']              = null;
		$insert_array['Fibroscan']               = null;
		$insert_array['Fibroscan_Dt']            = null;
		$insert_array['Fibroscan_LSM']           = null;
		$insert_array['Clinical_US']             = null;
		$insert_array['Clinical_US_Dt']          = null;
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatientcirrohosis', $insert_array);


		$update_array                            = array(
			"BreastFeeding"                          => null,
			"ART_Regimen"                            => null,
			"CKDStage"                               => null,
			"Ribavirin"                              => null,
			"PastTreatment"                          => null,
			"PreviousTreatingHospital"               => null,
			"PastRegimen"                            => null,
			"PastRegimen_Other"                      => null,
			"PreviousDuration"                       => null,
			"Pregnant"                               => null,
			"DeliveryDt"                             => null,
			"HCVPastTreatment"                       => null,
			"NWeeksCompleted"                        => null,
			"HCVPastOutcome"                         => null,
			"IsReferal"                              => null,
			"ReferingDoctor"                         => null,
			"ReferingDoctorOther"                    => null,
			"ReferTo"                                => null,
			"ReferalDate"                            => null,
			"LastPillDate"                           => null,
			"LMP"                                    => null,
			"ChildScore"                             => null,
			"HCVHistory"                             => null,
			"MF5"                                    => null,
			"SCRemarks"                              => null
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblRiskProfile');

		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatient_regimen_drug_data');
		/*patient_prescription visit */
		/*insert visit guid start*/
		$sql = "SELECT PatientVisitGUID FROM `tblpatientvisit` WHERE PatientGUID = ?";
		$patient_visitdata = $this->db->query($sql,[$patientguid])->result();
		foreach ($patient_visitdata as $value) {

			$insert_array = array(
				"DeletedGUID"    => $value->PatientVisitGUID);
			$this->db->insert('tblDeleteVisits', $insert_array);
		}
		/*end visit insert*/


		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatientvisit');

		$update_array                            = array(
			"PrescribingFacility"                    => null,
			"PrescribingDoctor"                      => null,
			"PrescribingDoctorOther"                 => null,
			"T_Regimen"                              => null, 
			"HCVHistory"                             => null,
			"PrescriptionPlace"                      => null,
			"T_DurationValue"                        => null,
			"T_DurationValue"                        => null,
			"T_DurationOther"                        => null,
			"DurationReason"                         => null,
			"TreatmentUpdatedOn"                     => date('Y-m-d'),
			"TreatmentUpdatedBy"                     => $loginData->id_tblusers,
			"PrescribingDate"                        => null,
			"DispensationPlace"                      => null,
			"MF6"                                    => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$update_array                            = array(
			"T_DurationOther"                        => null,
			"T_Initiation"                           => null,
			"T_VisitPlan"                            => null,
			"T_Regimen"                              =>null,
			"DispensationPlace"                      => null,
			"T_NoPillStart"                          => null,
			"AdvisedSVRDate"                         => null,
			"Current_Visitdt"                        =>null,
			"Next_Visitdt"                           => null,
			"MF7"                                    => null,
			"NextVisitPurpose"                       => null,
			"PDispensation"                          => null,
			"T_RmkDelay"                             => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$update_array                            = array(
			"Current_Visitdt"                        => null,
			"Next_Visitdt"                           => null,
			"AdvisedSVRDate"                         => null,
			"NextVisitPurpose"                       => null,
		);
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		/*eot*/		

		$update_array                            = array(
			"ETR_HCVViralLoad_Dt"                    => null,
			"ETR_PillsLeft"                          => null,
			"Adherence"                              => null,
			"AdvisedSVRDate"                         => null,
			"ETRComments"                            => null,
			"NextVisitPurpose"                       => null,
			"ETRDoctor"                              =>null,
			"ETRDoctorOther"                         => null,
			"SideEffectValue"                        => null,
			"Current_Visitdt"                        => null,
			"Next_Visitdt"                           => null,
			"DrugSideEffect"                         => null,
			"PDispensation"                          => null,
			"NAdherenceReason"                       =>null,
			"NAdherenceReasonOther"                  => null,
			"MF7"                                    =>null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		/*SVR*/			
		$update_array                            = array(

			'SVRDrawnDate'                           => null,
			'IsSVRSampleStored'                      => null,
			'SVRStorageTemp'                         => null,
			'IsSVRSampleTransported'                 => null,
			'SVRTransportTemp'                       => null,
			'SVRTransportDate'                       => null,
			'SVR12W_LabID'                           => null,
			'SVRTransporterName'                     => null,
			'SVRTransporterDesignation'              => null,
			'SVRTransportRemark'                     => null,
			'SVRReceiptDate'                         =>null,
			'SVRReceiverName'                        => null,
			'SVRReceiverDesignation'                 => null,
			'IsSVRSampleAccepted'                    => null,
			'SVR12W_HCVViralLoad_Dt'                 => null,
			'SVR12W_HCVViralLoadCount'               => null,
			'SVR12W_Result'                          => null,
			'SVRDoctor'                              => null,
			'SVRRejectionReason'                     => null,
			'SVRRejectionReasonOther'                => null,
			'SVRDoctorOther'                         => null,
			'SVRComments'                            => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$unitid = $this->db->update('tblpatient', $update_array);
		/*echo 'adasdasd'; exit();
		die($unitid);*/

		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);

	}

	public function unlock_process_testing($patientguid = null){

		$loginData = $this->session->userdata('loginData');
		$arr = array();
		/*testing*/
		$update_array                            = array(
			/*"PrescribingDate"                        =>  null,
			"PrescribingDate"                        =>  null,
			"V1_Haemoglobin"                         => null,
			"V1_Albumin"                             =>  null,
			"V1_Bilrubin"                            =>  null,
			"V1_INR"                                 =>  null,
			"ALT"                                    =>  null,
			"AST"                                    =>  null,
			"AST_ULN"                                =>  null,
			"V1_Platelets"                           =>  null,
			"Weight"                                 =>  null,
			"V1_Creatinine"                          =>  null,
			"V1_EGFR"                                =>  null,
			"V1_Cirrhosis"                           =>  null,
			"Cirr_TestDate"                          =>  null,
			"Cirr_Encephalopathy"                    =>  null,
			"Cirr_Ascites"                           =>  null,
			"Cirr_VaricealBleed"                     =>  null,
			"ChildScore"                             =>  null,
			"MF4"                                    =>  null,
			"baseline_updated_on"                    => date('Y-m-d'),
			"NextVisitPurpose"                       =>  null,
			"baseline_updated_by"                    => $loginData->id_tblusers,
			"special_updated_on"                     =>  date('Y-m-d'),
			"special_updated_by"                     =>  $loginData->id_tblusers,
			"treatment_updated_on"                   => date('Y-m-d'),
			"treatment_updated_by"                   => $loginData->id_tblusers,
			"CirrhosisStatus "                       =>  null,*/
			"Status"                                 => 32,
			"Result"                                 => null
		);
		$this->db->where('PatientGUID', $patientguid);
		$unitid                                  = $this->db->update('tblpatient', $update_array);

	/*	$insert_arraytest                        = array(

			"Visit_Date"                             => null,
			"Visit_No"                               => null,
			"V1_Haemoglobin"                         => null,
			"V1_Albumin"                             => null,
			"V1_Bilrubin"                            => null,
			"V1_INR"                                 => null,
			"ALT"                                    => null,
			"AST"                                    => null,
			"AST_ULN"                                => null,
			"V1_Platelets"                           => null,
			"V1_Creatinine"                          => null,
			"CreatedBy"                              => $loginData->id_tblusers,
		);
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatientaddltest', $insert_arraytest);*/

	/*	$insert_array['FIB4']                    = null;
		$insert_array['FIB4_FIB4']               = null;
		$insert_array['Prescribing_Dt']          = null;
		$insert_array['LastTest_Dt']             = null;
		$insert_array['APRI_PC']                 = null;
		$insert_array['FIB4_PC']                 = null;
		$insert_array['CreatedBy']               = $loginData->id_tblusers;
		$insert_array['UpdatedBy']               = $loginData->id_tblusers;
		$insert_array['UpdatedOn']               =  date('Y-m-d');
		$insert_array['UploadedOn']              =  date('Y-m-d');
		$insert_array['id_mstfacility']          =  $loginData->id_mstfacility;

		$insert_array['FIB4']                    = null;
		$insert_array['fib4']                    = null;
		$insert_array['FIB4_FIB4']               = null;

		$insert_array['APRI']                    = null;
		$insert_array['APRI_Dt']                 = null;
		$insert_array['APRI_Score']              = null;
		$insert_array['Fibroscan']               = null;
		$insert_array['Fibroscan_Dt']            = null;
		$insert_array['Fibroscan_LSM']           = null;
		$insert_array['Clinical_US']             = null;
		$insert_array['Clinical_US_Dt']          = null;
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatientcirrohosis', $insert_array);*/

		/*known histry*/
		$update_array                            = array(
			"BreastFeeding"                          => null,
			"ART_Regimen"                            => null,
			"CKDStage"                               => null,
			"Ribavirin"                              => null,
			"PastTreatment"                          => null,
			"PreviousTreatingHospital"               => null,
			"PastRegimen"                            => null,
			"PastRegimen_Other"                      => null,
			"PreviousDuration"                       => null,
			"Pregnant"                               => null,
			"DeliveryDt"                             => null,
			"HCVPastTreatment"                       => null,
			"NWeeksCompleted"                        => null,
			"HCVPastOutcome"                         => null,
			"IsReferal"                              => null,
			"ReferingDoctor"                         => null,
			"ReferingDoctorOther"                    => null,
			"ReferTo"                                => null,
			"ReferalDate"                            => null,
			"LastPillDate"                           => null,
			"LMP"                                    => null,
			"ChildScore"                             => null,
			"HCVHistory"                             => null,
			"MF5"                                    => null,
			"SCRemarks"                              => null
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblRiskProfile');

		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatient_regimen_drug_data');

		/*insert visit guid start*/
		$sql = "SELECT PatientVisitGUID FROM `tblpatientvisit` WHERE PatientGUID = ?";
		$patient_visitdata = $this->db->query($sql,[$patientguid])->result();
		foreach ($patient_visitdata as $value) {

			$insert_array = array(
				"DeletedGUID"    => $value->PatientVisitGUID);
			$this->db->insert('tblDeleteVisits', $insert_array);
		}
		/*end visit insert*/

		/*patient_prescription visit */

		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatientvisit');

		$update_array                            = array(
			"PrescribingFacility"                    => null,
			"PrescribingDoctor"                      => null,
			"PrescribingDoctorOther"                 => null,
			"T_Regimen"                              => null, 
			"HCVHistory"                             => null,
			"PrescriptionPlace"                      => null,
			"T_DurationValue"                        => null,
			"T_DurationValue"                        => null,
			"T_DurationOther"                        => null,
			"DurationReason"                         => null,
			"TreatmentUpdatedOn"                     => date('Y-m-d'),
			"TreatmentUpdatedBy"                     => $loginData->id_tblusers,
			"PrescribingDate"                        => null,
			"DispensationPlace"                      => null,
			"MF6"                                    => null,
		);
		$update_array['fragment3editreason']         = $this->security->xss_clean($this->input->post('unlockresonval'));
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$update_array                            = array(
			"T_DurationOther"                        => null,
			"T_Initiation"                           => null,
			"T_VisitPlan"                            => null,
			"T_Regimen"                              =>null,
			"DispensationPlace"                      => null,
			"T_NoPillStart"                          => null,
			"AdvisedSVRDate"                         => null,
			"Current_Visitdt"                        =>null,
			"Next_Visitdt"                           => null,
			"MF7"                                    => null,
			"NextVisitPurpose"                       => null,
			"PDispensation"                          => null,
			"T_RmkDelay"                             => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$update_array                            = array(
			"Current_Visitdt"                        => null,
			"Next_Visitdt"                           => null,
			"AdvisedSVRDate"                         => null,
			"NextVisitPurpose"                       => null,
		);
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		/*eot*/		

		$update_array                            = array(
			"ETR_HCVViralLoad_Dt"                    => null,
			"ETR_PillsLeft"                          => null,
			"Adherence"                              => null,
			"AdvisedSVRDate"                         => null,
			"ETRComments"                            => null,
			"NextVisitPurpose"                       => null,
			"ETRDoctor"                              =>null,
			"ETRDoctorOther"                         => null,
			"SideEffectValue"                        => null,
			"Current_Visitdt"                        => null,
			"Next_Visitdt"                           => null,
			"DrugSideEffect"                         => null,
			"PDispensation"                          => null,
			"NAdherenceReason"                       =>null,
			"NAdherenceReasonOther"                  => null,
			"MF7"                                    =>null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		/*SVR*/			
		$update_array                            = array(

			'SVRDrawnDate'                           => null,
			'IsSVRSampleStored'                      => null,
			'SVRStorageTemp'                         => null,
			'IsSVRSampleTransported'                 => null,
			'SVRTransportTemp'                       => null,
			'SVRTransportDate'                       => null,
			'SVR12W_LabID'                           => null,
			'SVRTransporterName'                     => null,
			'SVRTransporterDesignation'              => null,
			'SVRTransportRemark'                     => null,
			'SVRReceiptDate'                         =>null,
			'SVRReceiverName'                        => null,
			'SVRReceiverDesignation'                 => null,
			'IsSVRSampleAccepted'                    => null,
			'SVR12W_HCVViralLoad_Dt'                 => null,
			'SVR12W_HCVViralLoadCount'               => null,
			'SVR12W_Result'                          => null,
			'SVRDoctor'                              => null,
			'SVRRejectionReason'                     => null,
			'SVRRejectionReasonOther'                => null,
			'SVRDoctorOther'                         => null,
			'SVRComments'                            => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$unitid = $this->db->update('tblpatient', $update_array);

		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');

		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);

	}

	public function unlock_process_known_history($patientguid = null){

		$loginData = $this->session->userdata('loginData');
		$arr = array();
		$update_array['fragment3editreason']         = $this->security->xss_clean($this->input->post('unlockresonval'));
		

		$this->db->where('PatientGUID', $patientguid);
		$unitid =  $this->db->update('tblpatient', $update_array);

		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblRiskProfile');

		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatient_regimen_drug_data');

		/*insert visit guid start*/
		$sql = "SELECT PatientVisitGUID FROM `tblpatientvisit` WHERE PatientGUID = ?";
		$patient_visitdata = $this->db->query($sql,[$patientguid])->result();
		foreach ($patient_visitdata as $value) {

			$insert_array = array(
				"DeletedGUID"    => $value->PatientVisitGUID);
			$this->db->insert('tblDeleteVisits', $insert_array);
		}
		/*end visit insert*/

		/*patient_prescription visit */

		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatientvisit');

		$update_array                            = array(
			"PrescribingFacility"                    => null,
			"PrescribingDoctor"                      => null,
			"PrescribingDoctorOther"                 => null,
			"T_Regimen"                              => null, 
			"HCVHistory"                             => null,
			"PrescriptionPlace"                      => null,
			"T_DurationValue"                        => null,
			"T_DurationValue"                        => null,
			"T_DurationOther"                        => null,
			"DurationReason"                         => null,
			"TreatmentUpdatedOn"                     => date('Y-m-d'),
			"TreatmentUpdatedBy"                     => $loginData->id_tblusers,
			"PrescribingDate"                        => null,
			"DispensationPlace"                      => null,
			"MF5"                                    => null,
			"MF6"                                    => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$unitid = $this->db->update('tblpatient', $update_array);

		$update_array                            = array(
			"T_DurationOther"                        => null,
			"T_Initiation"                           => null,
			"T_VisitPlan"                            => null,
			"T_Regimen"                              =>null,
			"DispensationPlace"                      => null,
			"T_NoPillStart"                          => null,
			"AdvisedSVRDate"                         => null,
			"Current_Visitdt"                        =>null,
			"Next_Visitdt"                           => null,
			"MF7"                                    => null,
			"PDispensation"                          => null,
			"T_RmkDelay"                             => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$unitid = $this->db->update('tblpatient', $update_array);

		$update_array                            = array(
			"Current_Visitdt"                        => null,
			"Next_Visitdt"                           => null,
			"AdvisedSVRDate"                         => null,
			"NextVisitPurpose"                       => null,
		);
		$this->db->where('PatientGUID', $patientguid);
		$unitid = $this->db->update('tblpatient', $update_array);

		/*eot*/		

		$update_array                            = array(
			"ETR_HCVViralLoad_Dt"                    => null,
			"ETR_PillsLeft"                          => null,
			"Adherence"                              => null,
			"AdvisedSVRDate"                         => null,
			"ETRComments"                            => null,
			"ETRDoctor"                              =>null,
			"ETRDoctorOther"                         => null,
			"SideEffectValue"                        => null,
			"Current_Visitdt"                        => null,
			"Next_Visitdt"                           => null,
			"DrugSideEffect"                         => null,
			"PDispensation"                          => null,
			"NAdherenceReason"                       =>null,
			"NAdherenceReasonOther"                  => null,
			"MF7"                                    =>null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$unitid = $this->db->update('tblpatient', $update_array);
		/*SVR*/			
		$update_array                            = array(

			'SVRDrawnDate'                           => null,
			'IsSVRSampleStored'                      => null,
			'SVRStorageTemp'                         => null,
			'IsSVRSampleTransported'                 => null,
			'SVRTransportTemp'                       => null,
			'SVRTransportDate'                       => null,
			'SVR12W_LabID'                           => null,
			'SVRTransporterName'                     => null,
			'SVRTransporterDesignation'              => null,
			'SVRTransportRemark'                     => null,
			'SVRReceiptDate'                         =>null,
			'SVRReceiverName'                        => null,
			'SVRReceiverDesignation'                 => null,
			'IsSVRSampleAccepted'                    => null,
			'SVR12W_HCVViralLoad_Dt'                 => null,
			'SVR12W_HCVViralLoadCount'               => null,
			'SVR12W_Result'                          => null,
			'SVRDoctor'                              => null,
			'SVRRejectionReason'                     => null,
			'SVRRejectionReasonOther'                => null,
			'SVRDoctorOther'                         => null,
			'SVRComments'                            => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$unitid = $this->db->update('tblpatient', $update_array);

		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);
	}

	public function  unlock_process_prescription($patientguid = null){

		$loginData = $this->session->userdata('loginData');
		$arr = array();
		/*insert visit guid start*/
		$sql = "SELECT PatientVisitGUID FROM `tblpatientvisit` WHERE PatientGUID = ?";
		$patient_visitdata = $this->db->query($sql,[$patientguid])->result();
		foreach ($patient_visitdata as $value) {

			$insert_array = array(
				"DeletedGUID"    => $value->PatientVisitGUID);
			$this->db->insert('tblDeleteVisits', $insert_array);
		}
		/*end visit insert*/

		
		//$this->db->where('PatientGUID', $patientguid);
		//$this->db->delete('tblpatient_regimen_drug_data');

		/*patient_prescription visit */
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatientvisit');
		$update_array['fragment3editreason']         = $this->security->xss_clean($this->input->post('unlockresonval'));
		$update_array                            = array(
			/*"PrescribingFacility"                    => null,
			"PrescribingDoctor"                      => null,
			"PrescribingDoctorOther"                 => null,
			"T_Regimen"                              => null, 
			"HCVHistory"                             => null,
			"PrescriptionPlace"                      => null,
			"T_DurationValue"                        => null,
			"T_DurationValue"                        => null,
			"T_DurationOther"                        => null,
			"DurationReason"                         => null,
			"TreatmentUpdatedOn"                     => date('Y-m-d'),
			"TreatmentUpdatedBy"                     => $loginData->id_tblusers,
			"PrescribingDate"                        => null,
			"DispensationPlace"                      => null,
			"MF6"                                    => null,*/
			"Status"                                    => 12,
		);
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		
		$update_array                            = array(
			"T_DurationOther"                        => null,
			"T_Initiation"                           => null,
			"T_VisitPlan"                            => null,
			/*"T_Regimen"                              =>null,*/
			/*"DispensationPlace"                      => null,*/
			"T_NoPillStart"                          => null,
			"AdvisedSVRDate"                         => null,
			"Current_Visitdt"                        =>null,
			"Next_Visitdt"                           => null,
			"MF7"                                    => null,
			"PDispensation"                          => null,
			"T_RmkDelay"                             => null,
		);
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		
		$update_array                            = array(
			"Current_Visitdt"                        => null,
			"Next_Visitdt"                           => null,
			"AdvisedSVRDate"                         => null,
			"NextVisitPurpose"                       => null,
		);
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		
		/*eot*/		
		
		$update_array                            = array(
			"ETR_HCVViralLoad_Dt"                    => null,
			"ETR_PillsLeft"                          => null,
			"Adherence"                              => null,
			"AdvisedSVRDate"                         => null,
			"ETRComments"                            => null,
			"ETRDoctor"                              =>null,
			"ETRDoctorOther"                         => null,
			"SideEffectValue"                        => null,
			"Current_Visitdt"                        => null,
			"Next_Visitdt"                           => null,
			"DrugSideEffect"                         => null,
			"PDispensation"                          => null,
			"NAdherenceReason"                       =>null,
			"NAdherenceReasonOther"                  => null,
			"MF7"                                    =>null,
		);
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		/*SVR*/			
		$update_array                            = array(

			'SVRDrawnDate'                           => null,
			'IsSVRSampleStored'                      => null,
			'SVRStorageTemp'                         => null,
			'IsSVRSampleTransported'                 => null,
			'SVRTransportTemp'                       => null,
			'SVRTransportDate'                       => null,
			'SVR12W_LabID'                           => null,
			'SVRTransporterName'                     => null,
			'SVRTransporterDesignation'              => null,
			'SVRTransportRemark'                     => null,
			'SVRReceiptDate'                         =>null,
			'SVRReceiverName'                        => null,
			'SVRReceiverDesignation'                 => null,
			'IsSVRSampleAccepted'                    => null,
			'SVR12W_HCVViralLoad_Dt'                 => null,
			'SVR12W_HCVViralLoadCount'               => null,
			'SVR12W_Result'                          => null,
			'SVRDoctor'                              => null,
			'SVRRejectionReason'                     => null,
			'SVRRejectionReasonOther'                => null,
			'SVRDoctorOther'                         => null,
			'SVRComments'                            => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$unitid = $this->db->update('tblpatient', $update_array);

		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);
	}

	public function  unlock_process_dispensation_visit_1($patientguid = null){

		$loginData = $this->session->userdata('loginData');

		$arr = array();


		$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();
		$update_array['fragment3editreason']         = $this->security->xss_clean($this->input->post('unlockresonval'));
		$update_array                            = array(
			/*"T_Initiation"                           => null,
			"AdvisedSVRDate"                         => null,*/
			"Status"                         		=> 3,
		);
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);


		/*$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatient_regimen_drug_data');*/

		/*patient_prescription visit */
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatientvisit');
		
		
		$update_array                            = array(
			/*"Current_Visitdt"                        => null,
			"Next_Visitdt"                           => null,*/
			"NextVisitPurpose"                       => null,
			/*"AdvisedSVRDate"                         => null,*/
		);
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		
		$sql                          = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data                 = $this->db->query($sql,[$patientguid])->result();

		$Current_Visitdt              = $patient_data[0]->Current_Visitdt;
		$patient_data                 = $patient_data[0]->Next_Visitdt;

		$update_array['Next_Visitdt'] = $Current_Visitdt;

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		/*insert visit guid start*/
		$sql = "SELECT PatientVisitGUID FROM `tblpatientvisit` WHERE PatientGUID = ?";
		$patient_visitdata = $this->db->query($sql,[$patientguid])->result();
		foreach ($patient_visitdata as $value) {

			$insert_array = array(
				"DeletedGUID"    => $value->PatientVisitGUID);
			$this->db->insert('tblDeleteVisits', $insert_array);
		}
		/*end visit insert*/


		/*eot*/		
		
		$update_array                            = array(
			"ETR_HCVViralLoad_Dt"                    => null,
			"ETR_PillsLeft"                          => null,
			"Adherence"                              => null,
			/*"AdvisedSVRDate"                         => null,*/
			"ETRComments"                            => null,
			"ETRDoctor"                              =>null,
			"ETRDoctorOther"                         => null,
			"SideEffectValue"                        => null,
			/*"Current_Visitdt"                        => null,*/
			"DrugSideEffect"                         => null,
			"PDispensation"                          => null,
			"NAdherenceReason"                       =>null,
			"NAdherenceReasonOther"                  => null,
			"MF7"                                    =>null,
		);
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		/*SVR*/			
		$update_array                            = array(

			'SVRDrawnDate'                           => null,
			'IsSVRSampleStored'                      => null,
			'SVRStorageTemp'                         => null,
			'IsSVRSampleTransported'                 => null,
			'SVRTransportTemp'                       => null,
			'SVRTransportDate'                       => null,
			'SVR12W_LabID'                           => null,
			'SVRTransporterName'                     => null,
			'SVRTransporterDesignation'              => null,
			'SVRTransportRemark'                     => null,
			'SVRReceiptDate'                         =>null,
			'SVRReceiverName'                        => null,
			'SVRReceiverDesignation'                 => null,
			'IsSVRSampleAccepted'                    => null,
			'SVR12W_HCVViralLoad_Dt'                 => null,
			'SVR12W_HCVViralLoadCount'               => null,
			'SVR12W_Result'                          => null,
			'SVRDoctor'                              => null,
			'SVRRejectionReason'                     => null,
			'SVRRejectionReasonOther'                => null,
			'SVRDoctorOther'                         => null,
			'SVRComments'                            => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$unitid = $this->db->update('tblpatient', $update_array);

		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);

	}

	public function unlock_process_dispensation_visit_1_6($patientguid = null,$visit){

		$loginData = $this->session->userdata('loginData');
		$arr = array();
			//echo $visit;exit();
		$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();
		$update_array['fragment3editreason']         = $this->security->xss_clean($this->input->post('unlockresonval'));

		$update_array      = array(
			"Status"           => $this->getCurrentStatus($patientguid, $visit),
			"NextVisitPurpose" => 98,
		);
							//print_r($update_array);exit();
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		
		$sql = "SELECT PatientVisitGUID FROM `tblpatientvisit` WHERE PatientGUID = ? and VisitNo > ".$visit." ";
		$patient_visitdata = $this->db->query($sql,[$patientguid])->result();
		foreach ($patient_visitdata as $value) {

			$insert_array = array(
				"DeletedGUID"    => $value->PatientVisitGUID);
			$this->db->insert('tblDeleteVisits', $insert_array);
		}


		/*$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatient_regimen_drug_data');*/

		/*patient_prescription visit */
		
		/*$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatientvisit');*/
		$this->db->delete('tblpatientvisit',array('PatientGUID'=>$patientguid,'VisitNo >'=>$visit));
		
		$update_array      = array(
			/*"Current_Visitdt"  => null,
			"Next_Visitdt"     => null,
			"AdvisedSVRDate"   => null,*/
			"NextVisitPurpose" => 98,
			/*"Next_Visitdt"     => null,*/
		);
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$sql                          = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data                 = $this->db->query($sql,[$patientguid])->result();


		$Current_Visitdt              = $patient_data[0]->Current_Visitdt;
		$patient_data                 = $patient_data[0]->Next_Visitdt;

		$update_array['Next_Visitdt'] = $Current_Visitdt;

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		
		/*eot*/		
		
		$update_array                            = array(
			"ETR_HCVViralLoad_Dt"                    => null,
			"ETR_PillsLeft"                          => null,
			"Adherence"                              => null,
			"AdvisedSVRDate"                         => null,
			"ETRComments"                            => null,
			"ETRDoctor"                              =>null,
			"ETRDoctorOther"                         => null,
			"SideEffectValue"                        => null,
			/*"Current_Visitdt"                        => null,*/
			/*"Next_Visitdt"                           => null,*/
			"DrugSideEffect"                         => null,
			"PDispensation"                          => null,
			"NAdherenceReason"                       =>null,
			"NAdherenceReasonOther"                  => null,

		);
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		/*SVR*/			
		$update_array                            = array(

			'SVRDrawnDate'                           => null,
			'IsSVRSampleStored'                      => null,
			'SVRStorageTemp'                         => null,
			'IsSVRSampleTransported'                 => null,
			'SVRTransportTemp'                       => null,
			'SVRTransportDate'                       => null,
			'SVR12W_LabID'                           => null,
			'SVRTransporterName'                     => null,
			'SVRTransporterDesignation'              => null,
			'SVRTransportRemark'                     => null,
			'SVRReceiptDate'                         =>null,
			'SVRReceiverName'                        => null,
			'SVRReceiverDesignation'                 => null,
			'IsSVRSampleAccepted'                    => null,
			'SVR12W_HCVViralLoad_Dt'                 => null,
			'SVR12W_HCVViralLoadCount'               => null,
			'SVR12W_Result'                          => null,
			'SVRDoctor'                              => null,
			'SVRRejectionReason'                     => null,
			'SVRRejectionReasonOther'                => null,
			'SVRDoctorOther'                         => null,
			'SVRComments'                            => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$unitid = $this->db->update('tblpatient', $update_array);

		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);
	}

	public function unlock_process_dispensation_eot($patientguid = null){

		$loginData = $this->session->userdata('loginData');
		$arr = array();
			//echo $visit;exit();
		$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();


		$Current_Visitdt = $patient_data[0]->Current_Visitdt;
		$patient_data = $patient_data[0]->Next_Visitdt;
		$update_array['fragment3editreason']         = $this->security->xss_clean($this->input->post('unlockresonval'));
		
		/*eot*/		
		
		/*$update_array           = array(
		"ETR_HCVViralLoad_Dt"   => null,
		"ETR_PillsLeft"         => null,
		"Adherence"             => null,
		"AdvisedSVRDate"        => null,
		"ETRComments"           => null,
		"ETRDoctor"             =>null,
		"ETRDoctorOther"        => null,
		"SideEffectValue"       => null,
		//"Current_Visitdt"       => null,
		"Next_Visitdt"          => null,
		"DrugSideEffect"        => null,
		"PDispensation"         => null,
		"NAdherenceReason"      =>null,
		"NAdherenceReasonOther" => null,
		"MF7"                   =>null,
		"Status"                => 13,
		);
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);*/

		$update_array['Next_Visitdt']        = $Current_Visitdt;
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		/*SVR*/			
		$update_array                            = array(

			'SVRDrawnDate'                           => null,
			'IsSVRSampleStored'                      => null,
			'SVRStorageTemp'                         => null,
			'IsSVRSampleTransported'                 => null,
			'SVRTransportTemp'                       => null,
			'SVRTransportDate'                       => null,
			'SVR12W_LabID'                           => null,
			'SVRTransporterName'                     => null,
			'SVRTransporterDesignation'              => null,
			'SVRTransportRemark'                     => null,
			'SVRReceiptDate'                         =>null,
			'SVRReceiverName'                        => null,
			'SVRReceiverDesignation'                 => null,
			'IsSVRSampleAccepted'                    => null,
			'SVR12W_HCVViralLoad_Dt'                 => null,
			'SVR12W_HCVViralLoadCount'               => null,
			'SVR12W_Result'                          => null,
			'SVRDoctor'                              => null,
			'SVRRejectionReason'                     => null,
			'SVRRejectionReasonOther'                => null,
			'SVRDoctorOther'                         => null,
			'SVRComments'                            => null,
			"Status"                => 13,
		);

		$this->db->where('PatientGUID', $patientguid);
		$unitid = $this->db->update('tblpatient', $update_array);

		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);
	}
}	