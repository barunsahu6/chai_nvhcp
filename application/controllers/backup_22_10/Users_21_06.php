<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Common_Model');

		$loginData = $this->session->userdata('loginData');
		if($loginData == null)
		{
			redirect('login');
		}

		
	}


	public function index($id_user = null)
	{	
		$content['edit_flag'] = 0;

		$content['salt'] = md5(uniqid(rand(), TRUE));
		$this->session->set_userdata('salt', $content['salt']);
		$salt = $this->session->userdata('salt');
		$RequestMethod = $this->input->server('REQUEST_METHOD');


		

		if($RequestMethod == "POST")
		{

			 $hashed_password = hash('sha256', $this->input->post('password') );

			 $hashed_username = hash('sha256', $this->input->post('username') );

			if($id_user != null)
				{
					$updateArray = array(
						'username'       	        => $this->input->post('username'),
						'Operator_Name'       	    => $this->input->post('full_name'),
						'password'			 	    => $this->input->post('password'),
						'Mobile'				 	=> $this->input->post('mobile'),
						'Email'					 	=> $this->input->post('email'),
						'id_mstfacility'	        => $this->input->post('id_mstfacility'),
						'State_ID'	        		=> $this->input->post('input_state'),
						'DistrictID'	       		=> $this->input->post('input_district'),
						'user_type' 			    => $this->input->post('usertype'),
						);

					if(trim($this->input->post('password')) !== "")
					{
						$updateArray['password'] = $hashed_password;
					}
					else
					{
						unset($updateArray['password']);
					}

					if(trim($this->input->post('username')) !== "")
					{
						$updateArray['username'] = $hashed_username;
					}
					else
					{
						unset($updateArray['username']);
					}


					$this->Common_Model->update_data('tblusers', $updateArray, 'id_tblusers', $id_user);

					redirect("users");
				}
			else
			{
				$insertArray = array(
				'username'		 		     => hash('sha256',$this->input->post('username')),
				'Operator_Name'       	     => $this->input->post('full_name'),
				'password' 				     => hash('sha256', $this->input->post('password')),
				'Mobile'					 =>	$this->input->post('mobile'),
				'Email'						 => $this->input->post('email'),
				'State_ID'	        		=> $this->input->post('input_state'),
				'DistrictID'	       		=> $this->input->post('input_district'),
				'id_mstfacility'	         =>$this->input->post('id_mstfacility'),
				'user_type'				     => $this->input->post('usertype'),
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => $this->input->post('username'),
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);


			}
		}

		if($id_user != null)
		{
			$sql_details = "select * from tblusers where id_tblusers = ".$id_user;
			$res_details = $this->Common_Model->query_data($sql_details)[0];
			$content['user_details'] = $res_details;
			$content['edit_flag'] = 1;
		}

		$query = "select * from tblusers";
		$content['users_list'] = $this->Common_Model->query_data($query);
		$sql = "select * from mststate order by StateName";
		$content['states'] = $this->db->query($sql)->result();


		$sql_facility = "select * from mstfacility";
		$res = $this->Common_Model->query_data($sql_facility);

		$content['Facility'] = $res;

		$content['subview'] = "list_users";
		$this->load->view('admin/main_layout', $content);
	}

	public function delete($id_user = null)
	{
		$sql = "delete from tblusers where id_tblusers = $id_user";

		$this->db->query($sql);

		redirect("users");
	}

	public function getFacilities($id_mstdistrict = null)
	{
		$sql = "select * from mstfacility where id_mstdistrict = ? order by facility_short_name";
		$Facilities = $this->db->query($sql,[$id_mstdistrict])->result();

		$options = "<option value=''>Select Facility</option>";
		foreach ($Facilities as $getdistrict) {
			$options .= "<option value='".$getdistrict->id_mstfacility."'>".$getdistrict->facility_short_name."</option>";
		}

		echo $options;
	}

}