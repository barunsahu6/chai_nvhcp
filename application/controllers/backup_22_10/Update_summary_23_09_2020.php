<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Update_summary extends CI_Controller {

	public function __construct($category = 0)
	{
		parent::__construct();

		error_reporting(E_ALL);

		if ($this->uri->segment(4) === null)
		{
			$this->category = 0;
			$this->category_filter = 1;
		}
		else
		{
			$this->category = $this->uri->segment(4);

			$this->category_filter = " tblpatient.PatientCategory = ".$this->category;
		}

		$this->load->model('Summary_update_model');
		$this->load->model('Summary_genderwiseupdate_model');
	}

	public function index()
	{
		//$this->Summary_update_model->cascade_anti_hcv_screened();
		$this->Summary_update_model->update_summary_all($this->category_filter, $this->category);

		echo "<br>done updating summary";
	}
	public function bkstest(){
		
	}

	public function null_everything()
	{
		$this->Summary_update_model->null_everything();
		//$this->Summary_genderwiseupdate_model->hepatitis_b_screened_cohort_based();

		echo "<br>everything is null";
	}

	public function genderwise_null_everything()
	{
		$this->Summary_genderwiseupdate_model->everything_null();
		//$this->Summary_genderwiseupdate_model->hepatitis_b_screened_cohort_based();

		echo "<br>everything is null";
	}

	public function update_everything_hepb(){
		$this->Summary_update_model->update_everything_hepb();
		$this->Summary_update_model->update_age_fields_hepb();
	}

	public function update_everything()
	{
		$this->Summary_update_model->cascade_hav_patients();
		$this->Summary_update_model->cascade_hev_patients();
		$this->Summary_update_model->update_cirrhotic();
		$this->Summary_update_model->adherence_followup_analysis();
		$this->Summary_update_model->lfu_adherence_followup_analysis();
		$this->Summary_update_model->vl_samples_accepted_rejected();
		$this->Summary_update_model->update_lossup_spcase();
		$this->Summary_update_model->Adherencesummarytest();

		$this->Summary_update_model->update_cascade_fields();
		$this->Summary_update_model->timearounddata_summaryupdate();
		$this->Summary_update_model->regimenwisedistribution_regall();

		$this->Summary_update_model->update_everything_hepb();
		$this->Summary_update_model->update_age_fields_hepb();
	
		echo "<br>everything is update";
	}
	public function update_hepdetails(){

		$this->Summary_genderwiseupdate_model->cascade_anti_hepb_positive();
		$this->Summary_genderwiseupdate_model->cascade_anti_hepb_screen();

		echo "<br>Hep-B is update";
	}

public function nullregupdate(){
	$this->Summary_update_model->nullregupdate_regall();

}

public function regupdate(){
	$this->Summary_update_model->regimenwisedistribution_regall();
	//$this->Summary_update_model->cascade_initiatied_on_treatment();

}
	// the functions below are just for convenience in case you need to update summary table fields for individual graphs

/*Update agewise summary start*/
public function agewise_summaryupdated(){
$this->Summary_genderwiseupdate_model->update_cascade_fields_genderwise();
$this->Summary_genderwiseupdate_model->update_hepb_genderwise();


}

/*end agewise summary */


	public function cascade_hav_patients(){

	$this->Summary_update_model->cascade_hav_patients();


	}
	public function cascade_hev_patients(){
		$this->Summary_update_model->cascade_hev_patients();
	}
	public function Adherencesummarytest(){
	$this->Summary_update_model->Adherencesummarytest();
	}
	public function timearounddata_summaryupdate(){
		$this->Summary_update_model->timearounddata_summaryupdate();
	}

	public function update_cirrhotic(){
		$this->Summary_update_model->update_cirrhotic();
	}
	public function adherence_followup_analysis(){
	$this->Summary_update_model->adherence_followup_analysis();
	$this->Summary_update_model->lfu_adherence_followup_analysis();
}
	public function vl_samples_accepted_rejected(){
		$this->Summary_update_model->vl_samples_accepted_rejected();
	}
	public function update_age_fields()
	{
		$this->Summary_update_model->update_age_fields($this->category_filter, $this->category);
		echo __FUNCTION__." done";
	}
	public function update_lossup_spcase(){
		$this->Summary_update_model->update_lossup_spcase();
		echo __FUNCTION__." done";
	}

	public function update_cascade_fields()
	{
		$this->Summary_update_model->update_cascade_fields();
		echo __FUNCTION__." done";
	}





	public function update_patient_guid()
	{
		$this->Summary_update_model->update_patient_guid();
		echo __FUNCTION__." done";
	}

		public function update_srv_core_data()
		{
		$this->Summary_update_model->update_srv_core_data();
		echo __FUNCTION__." done";
		}
		
		public function confirmatory_svr_date(){
		
		$this->Summary_update_model->confirmatory_svr_date();
		echo __FUNCTION__." done";
		}
		
		public function genotype_svr_date(){
		
		$this->Summary_update_model->genotype_svr_date();
		echo __FUNCTION__." done";
		}

public function update_all_core_data(){
	ini_set('memory_limit', '-1');
	$this->Summary_update_model->update_all_core_data();
		echo __FUNCTION__." done";

}
public function update_all_core_dataor(){
	ini_set('memory_limit', '-1');
	$this->Summary_update_model->update_all_core_dataor();
		echo __FUNCTION__." done";

}

public function save_svrdata(){

	$this->Summary_update_model->save_svrdata();
		echo __FUNCTION__." done";
}
	public function import_core_diagnosis_data($page)
	{

		$sqls = "select max(record_updated_on) as record_updated_on from core_diagnostics_raw_import";
		$getmaxdate = $this->Common_Model->query_data($sqls);
		//print_r($getmaxdate);
		 $getmaxdateval = $getmaxdate[0]->record_updated_on;
		$maxdateee = explode(' ', $getmaxdateval);
		 $maxdateee[0];
		 $onedayadd = date('Y-m-d',strtotime($maxdateee[0] . "+1 days"));

		//exit();

		$ch = curl_init();
		$headers = array(
			'Accept: application/json',
			'Content-Type: application/json',
			'X-Authorization: uc2H4LCd5GTcBPmhxCSMyj5qS7bDjVNvTW7LW8iV4VexUTB0FxHowGWrG5NFFo8m'
		);

		curl_setopt($ch, CURLOPT_URL, 'http://punjabhepc.corediagnostics.in/api/GetCases/'.$onedayadd.'?page='.$page);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_HEADER, 0);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET"); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Timeout in seconds
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);

		$authToken = curl_exec($ch);

		return $authToken;
	}

	public function save_imported_core_data($page=NULL)
	{
				if($page                 !=NULL){
				$page                    =$page;
				}else{
				$page                    = 0;
				}
				$json_data               = $this->import_core_diagnosis_data($page);
				
				$data                    = json_decode($json_data);

		$sql_coredata = "SELECT max(id) as maxid FROM `core_diagnostics_raw_import`";
		$res_coredata = $this->Common_Model->query_data($sql_coredata);

				//echo $res_coredata[0]->maxid;
				foreach ($data->data as $row) {
					//echo $row->id;exit();
				//if($row->id > $res_coredata->maxid;)
				$insert_array            = array(
				'id'               			 => $row->id,
				'uid'                    => $row->uid,
				'case_no'                => $row->case_no,
				'test_code'              => $row->test_code,
				'test_name'              => $row->test_name,
				'specimen_name'          => $row->specimen_name,
				'patient_name'           => $row->patient_name,
				'sex'                    => $row->sex,
				'age'                    => $row->age,
				'mob_no'                 => $row->mob_no,
				'institution_code'       => $row->institution_code,
				'institution_name'       => $row->institution_name,
				'sample_collection_date' => $row->sample_collection_date,
				'sample_received_date'   => $row->sample_received_date,
				'report_date'            => $row->report_date,
				'report_document'        => $row->report_document,
				'result'                 => $row->result,
				'quant_result'           => $row->quant_result,
				'svr'                    => $row->svr,
				'record_updated_on'      => $row->record_updated_on
				);
				
				$this->db->insert('core_diagnostics_raw_import', $insert_array);
				}
				
				if($data->next_page_url  !=null){
				$page                    = $data->current_page+1;
				$this->save_imported_core_data($page);
				}else{
					echo 'Done';
				}

	}

}
