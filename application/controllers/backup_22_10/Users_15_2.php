<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Common_Model');
		$this->load->Model('Log4php_model');
		$this->load->library('form_validation');
		$this->load->helper('common');

		$loginData = $this->session->userdata('loginData');

		if($loginData == null)
		{
			redirect('login');
		}
	}



	public function index($id_user = null)
	{	
		error_reporting(0);
		$this->load->library('form_validation');
		$loginData = $this->session->userdata('loginData');
		$content['edit_flag'] = 0;

		$id_user = $this->security->xss_clean($id_user);

		$content['salt'] = md5(uniqid(rand(), TRUE));
		$this->session->set_userdata('salt', $content['salt']);
		$salt = $this->session->userdata('salt');
		$RequestMethod = $this->input->server('REQUEST_METHOD');		

		if($RequestMethod == "POST")
		{
			$hashed_password = hash('sha256',$this->security->xss_clean( $this->input->post('password')) );

			$hashed_username = hash('sha256', $this->security->xss_clean($this->input->post('username')) );

			if($id_user != null)
			{


					$this->form_validation->set_rules('full_name', 'Name', 'regex_match[/^([a-z ])+$/i]','trim|required|max_length[50]|xss_clean');
					
					$this->form_validation->set_rules('usertype', 'User type', 'trim|required|xss_clean');
					
					if ($this->form_validation->run() == FALSE) {

						$arr['status'] = 'false';
						$this->session->set_flashdata("tr_msg",validation_errors());
					}else{

				$updateArray = array(
					//'username'       	        => $this->input->post('username'),
					'Operator_Name'       	    => $this->security->xss_clean($this->input->post('full_name')),
					//'password'			 	    => hash('sha256',$this->security->xss_clean($this->input->post('password'))),
					'password'			 	    => $this->security->xss_clean($this->input->post('password')),
					'RoleId'					=> $this->security->xss_clean($this->input->post('role_authentication')),
					'Mobile'				 	=> $this->security->xss_clean($this->input->post('mobile')),
					'Email'					 	=> $this->security->xss_clean($this->input->post('email')),
					'id_mstfacility'	        => $this->security->xss_clean($this->input->post('id_mstfacility')),
					'State_ID'	        		=> $this->security->xss_clean($this->input->post('input_state')),
					'DistrictID'	       		=> $this->security->xss_clean($this->input->post('input_district')),
					'user_type' 			    => $this->security->xss_clean($this->input->post('usertype'))
					//'flag'						=> 2
				);

				if(trim($this->security->xss_clean($this->input->post('password'))) !== "")
				{
					$updateArray['password'] = $hashed_password;

					$updateArray['flag'] = 2;
				}
				else
				{
					unset($updateArray['password']);
				}

				/*	if(trim($this->input->post('username')) !== "")
					{
						$updateArray['username'] = $hashed_username;
					}
					else
					{
						unset($updateArray['username']);
					}*/


					$this->Common_Model->update_data('tblusers', $updateArray, 'id_tblusers', $id_user);

					$this->session->set_flashdata('tr_msg', 'User Successfully Updated.');

					//create log on update user data					
					$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$id_user,'Update','User Successfully Updated');

					if($loginData->user_type==2){
						redirect('patientinfo?p=1');
					}else{						
						redirect('users');
					}
				}
			}
				else
				{

					$this->form_validation->set_rules('full_name', 'Name','regex_match[/^([a-z ])+$/i]', 'trim|required|max_length[50]|xss_clean');
					$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[tbludata.tbluname]');
					$this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean');
					if($this->input->post('email')!=''){
						$this->form_validation->set_rules('email', 'Email Id', 'trim|required|valid_email|is_unique[tblusers.Email]|xss_clean');
					}
					$this->form_validation->set_rules('usertype', 'User type', 'trim|required|xss_clean');
					if($this->input->post('usertype')!=1){
						$this->form_validation->set_rules('input_state', 'State', 'trim|required|xss_clean');
					}
					if ($this->form_validation->run() == FALSE) {

						$arr['status'] = 'false';
						$this->session->set_flashdata("tr_msg",validation_errors());
					}else{


							$insertArray     = array(
							'username'       => hash('sha256',$this->security->xss_clean($this->input->post('username'))),
							'Operator_Name'  => $this->security->xss_clean($this->input->post('full_name')),
							//'password'     => hash('sha256', $this->input->post('password')),
							'password'       =>$this->security->xss_clean($this->input->post('password')),
							'RoleId'         => $this->security->xss_clean($this->input->post('role_authentication')),
							'Mobile'         =>	$this->security->xss_clean($this->input->post('mobile')),
							'Email'          => $this->security->xss_clean($this->input->post('email')),
							'State_ID'       => $this->security->xss_clean($this->input->post('input_state')),
							'DistrictID'     => $this->security->xss_clean($this->input->post('input_district')),
							'id_mstfacility' =>$this->security->xss_clean($this->input->post('id_mstfacility')),
							'user_type'      => $this->security->xss_clean($this->input->post('usertype')),
							);

						$this->Common_Model->insert_data('tblusers', $insertArray);
						$insert_id = $this->db->insert_id();
						$insertArray1 = array(
							'tbluid'		 		     => $insert_id,
							'tbluname'		 		     => $this->security->xss_clean($this->input->post('username')),
							'password' 				     => $this->security->xss_clean($this->input->post('password')),
							'date'				    	 => date('Y-m-d'),
						);

						$this->Common_Model->insert_data('tbludata', $insertArray1);
						
						$this->session->set_flashdata('tr_msg', 'User Successfully Created');
						//create log on create user data						
						$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$insert_id,'Create','User Successfully Created');
						redirect('users');
					}

				}
			}

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1 and (user_type=1 || user_type=3)";
				$stateid_where = "where 1";
				$district_where = "where 1";
				$mstdistrict_where= "AND 1";
				$mststate_where= " 1";
				$mstfacility_where = "1";
			}
			elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
				$mstdistrict_where= "AND id_mstdistrict = '".$loginData->DistrictID."'";
				$mststate_where= " id_mststate = '".$loginData->State_ID."'";
				$mstfacility_where = "id_mstfacility ='".$loginData->id_mstfacility."'";
			}
			elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND State_ID = '".$loginData->State_ID."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
				$mstdistrict_where= "AND 1";
				$mststate_where= " id_mststate = '".$loginData->State_ID."'";
				$mstfacility_where = "1";
			}
			elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
				$mstdistrict_where= "AND id_mstdistrict = '".$loginData->DistrictID."'";
				$mststate_where= " id_mststate = '".$loginData->State_ID."'";
				$mstfacility_where = "id_mstfacility ='".$loginData->id_mstfacility."'";
			}


			if($id_user != null)
			{
				$sql_details = "select * from tblusers where id_tblusers = ? ".$sess_where."";
				//$res_details = $this->Common_Model->query_data($sql_details)[0];

				$res_details = $this->db->query($sql_details,[$id_user])->result();
				$content['user_details'] = $res_details[0];
				$content['edit_flag'] = 1;
			}

			$query = "select * from tblusers where 1 and Status!=2 ".$sess_where."";
			$content['users_list'] = $this->Common_Model->query_data($query);

			$sql = "select mst.id_mststate,mst.StateName from mststate mst left join tblusers us on us.State_ID=mst.id_mststate ".$stateid_where." group by id_mststate  order by StateName ";
			$content['states'] = $this->db->query($sql)->result();

			$sql = "select dis.id_mstdistrict,dis.DistrictName from mstdistrict dis  left join tblusers us on us.DistrictID=dis.id_mstdistrict ".$stateid_where." order by DistrictName";
			$content['mstdistrict'] = $this->db->query($sql)->result();

			$sql = "select * from mstdistrict where ".$mststate_where." ".$mstdistrict_where."";
			$content['mstdistrict2'] = $this->db->query($sql)->result();


			//$sql_facility = "select * from mstfacility";
			$sql_facility = "select * from mstfacility where ".$mstfacility_where." and ".$mststate_where." order by facility_short_name";
			$res = $this->Common_Model->query_data($sql_facility);
			$content['Facility'] = $res;

			$content['id_user'] =$id_user;
			if($loginData->RoleId == '1' && $loginData->id_tblusers!=$id_user){
				$sql_MSTRole = "select * from MSTRole where RoleId!=1 order by Role";
				$content['mst_role'] = $this->Common_Model->query_data($sql_MSTRole);
			}elseif($loginData->RoleId == '1' && $loginData->id_tblusers==$id_user){
				$sql_MSTRole = "select * from MSTRole order by Role";
				$content['mst_role'] = $this->Common_Model->query_data($sql_MSTRole);
			}else{
				$sql_MSTRole = "select * from MSTRole order by Role";
				$content['mst_role'] = $this->Common_Model->query_data($sql_MSTRole);
			}
			$sql = "select flag from tblusers where id_tblusers = ".$loginData->id_tblusers."";
			$content['flag'] = $this->db->query($sql)->result();

			$content['subview'] = "list_users";
			$this->load->view('admin/main_layout', $content);
		}

		public function delete($id_user = null)
		{

			$loginData = $this->session->userdata('loginData');
			$Session_StateID = $loginData->State_ID;

			/*$sql = "delete from tblusers where id_tblusers = $id_user";
			$this->db->query($sql);*/

			$dataval = array('id_tblusers' => $id_user, 'State_ID' => $Session_StateID);
					$this->db->where($dataval);
			$data = array(
				'Status' => '2',
				'UpdatedOn' => date('Y-m-d'),
				'UpdatedBy' => $loginData->id_tblusers
			);

			$this->db->where('id_tblusers', $id_user);
			$this->db->update('tblusers', $data);


		/*$sql1 = "delete from tbludata where tbluid = $id_user";
		$this->db->query($sql1);*/
		$data1 = array(
			'tbluname' => null,
			'Operator_Name' =>'Deleted By-'.$loginData->id_tblusers,
			'date' => date('Y-m-d')
		);

		$this->db->where('tbluid', $id_user);
		$this->db->update('tbludata', $data1);

		//create log on create user data		
		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$id_user,'Delete','User Successfully Deleted');
		
		redirect("users");
	}

	public function getFacilities($id_mstdistrict = null)
	{	
		$loginData = $this->session->userdata('loginData'); 
		$filters1 = $this->session->userdata('filters1'); 
		$filters = $this->session->userdata('filters'); 
		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1 and (user_type=1 || user_type=3)";
			$stateid_where = "where 1";
			$district_where = "where 1";
			$mstdistrict_where= " 1";
			$mstfacility_where = "1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

			$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " id_mstdistrict = '".$loginData->DistrictID."'";
			$mstfacility_where = "id_mstfacility ='".$loginData->id_mstfacility."'";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND State_ID = '".$loginData->State_ID."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " 1";
			$mstfacility_where = "1";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " id_mstdistrict = '".$loginData->DistrictID."'";
			$mstfacility_where = "id_mstfacility ='".$loginData->id_mstfacility."'";
		}
		$sql = "select * from mstfacility where ".$mstfacility_where." and id_mstdistrict = ? order by facility_short_name";
		$Facilities = $this->db->query($sql,[$id_mstdistrict])->result();

		$options = "<option value=''>All Facilities</option>";
		foreach ($Facilities as $getdistrict) {
			if($loginData->id_mstfacility==$getdistrict->id_mstfacility){
				$select ='selected';
			}elseif($filters1['id_mstfacility']==$getdistrict->id_mstfacility){
				$select ='selected';
			}
			elseif($filters['id_mstfacility']==$getdistrict->id_mstfacility){
				$select ='selected';
			}
			else{
				$select='';
			}
			$options .= "<option value='".$getdistrict->id_mstfacility."' ".$select.">".$getdistrict->facility_short_name."</option>";
		}

		echo $options;
	}


	public function getFacilitiesname($id_mstdistrict = null)
	{	
		$loginData = $this->session->userdata('loginData'); 

		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1 and (user_type=1 || user_type=3)";
			$stateid_where = "where 1";
			$district_where = "where 1";
			$mstdistrict_where= " 1";
			$mstfacility_where = "1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

			$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " id_mstdistrict = '".$loginData->DistrictID."'";
			$mstfacility_where = "id_mstfacility ='".$loginData->id_mstfacility."'";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND State_ID = '".$loginData->State_ID."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " 1";
			$mstfacility_where = "1";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " id_mstdistrict = '".$loginData->DistrictID."'";
			$mstfacility_where = "id_mstfacility ='".$loginData->id_mstfacility."'";
		}
		$sql = "select * from mstfacility where ".$mstfacility_where." and id_mstdistrict = ? order by facility_short_name";
		$Facilities = $this->db->query($sql,[$id_mstdistrict])->result();

		$options = "<option value=''>All Facilities</option>";
		foreach ($Facilities as $getdistrict) {
			if($loginData->id_mstfacility==$getdistrict->id_mstfacility){
				$select ='';
			}else{
				$select='';
			}
			$options .= "<option value='".$getdistrict->id_mstfacility."' ".$select.">".$getdistrict->facility_short_name."</option>";
		}

		echo $options;
	}


	public function getDistricts($id_mststate = null)
	{	
		$loginData = $this->session->userdata('loginData'); 
		$filters1 = $this->session->userdata('filters1'); 
		$filters = $this->session->userdata('filters'); 
			//pr($filters1);


		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1 and (user_type=1 || user_type=3)";
			$stateid_where = "where 1";
			$district_where = "where 1";
			$mstdistrict_where= " 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

			$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " id_mstdistrict = '".$loginData->DistrictID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND State_ID = '".$loginData->State_ID."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " 1";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " id_mstdistrict = '".$loginData->DistrictID."'";
		}

		$sql = "select * from mstdistrict where ".$mstdistrict_where." and id_mststate = ? order by DistrictName";
		$districts = $this->db->query($sql,[$id_mststate])->result();
		$options = "<option value=''>All Districts</option>";
		foreach ($districts as $district) {
			if($loginData->DistrictID==$district->id_mstdistrict){
				$select ='selected';
			}elseif($filters1['id_input_district']==$district->id_mstdistrict){
				$select ='selected';
			}
			elseif($filters['id_input_district']==$district->id_mstdistrict){
				$select ='selected';
			}
			else{
				$select='';
			}


			$options .= "<option value='".$district->id_mstdistrict."' ".$select.">".$district->DistrictName."</option>";
		}

		echo $options;
	}

	public function getStateCode($id_mststate = null,$id_mstdistrict = null,$id_mstfacility = null)
	{	
		$loginData = $this->session->userdata('loginData'); 

		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1 and (user_type=1 || user_type=3)";
			$stateid_where = "where 1";
			$district_where = "where 1";
			$mstdistrict_where= " 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

			$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " id_mstdistrict = '".$loginData->DistrictID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND State_ID = '".$loginData->State_ID."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " 1";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " id_mstdistrict = '".$loginData->DistrictID."'";
		}
		$arr = array();
		  //$sql = "select s.id_mststate,s.StateCd,f.FacilityCode,f.is_Mtc from mststate s inner join mstfacility f on f.id_mststate=s.id_mststate where s.id_mststate = ? and id_mstfacility = ?";

		$sqlval= "select * from mstfacility where id_mstfacility = ? ";
		$mstfacmtctc = $this->db->query($sqlval,[$id_mstfacility])->result();
			//print_r($mstfacmtctc);
		if(empty($mstfacmtctc)){
			$ismtcv=0;	
		}else{
			$ismtcv= $mstfacmtctc[0]->is_Mtc;
		}

		$sql="select f.FacilityType,s.id_mststate,s.StateCd,f.FacilityCode,f.is_Mtc,d.DistrictCd,sum(case when f.is_Mtc=".$ismtcv." then 1 else 0 end)as is_Mtcval from mststate s inner join mstfacility f on f.id_mststate=s.id_mststate left join mstdistrict d on f.id_mstdistrict=d.id_mstdistrict left join tblusers u 
		on u.id_mstfacility=f.id_mstfacility where s.id_mststate = ".$id_mststate." and f.id_mstdistrict = ".$id_mstdistrict."  and u.id_mstfacility = '".$id_mstfacility."' and f.id_mstfacility = '".$id_mstfacility."' ";
		$districts = $this->db->query($sql)->result();

		if($districts){
			$arr['status'] = 'true';
			$arr['fields'] = json_encode($districts[0]);
		}

		echo json_encode($arr);
	}

	public function getStateCodeAdmin($id_mststate = null)
	{	
		$loginData = $this->session->userdata('loginData'); 
		
		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1 and (user_type=1 || user_type=3)";
			$stateid_where = "where 1";
			$district_where = "where 1";
			$mstdistrict_where= " 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

			$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " id_mstdistrict = '".$loginData->DistrictID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND State_ID = '".$loginData->State_ID."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " 1";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."'";
			$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
			$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			$mstdistrict_where= " id_mstdistrict = '".$loginData->DistrictID."'";
		}
		$arr = array();
		$sql = "select s.id_mststate,s.StateCd from mststate s  where s.id_mststate = ?";
		$districts = $this->db->query($sql,[$id_mststate])->result();

		if($districts){
			$arr['status'] = 'true';
			$arr['fields'] = json_encode($districts[0]);
		}

		echo json_encode($arr);
	}

	public function add_user(){
		/*$insertArray = array(
				'username'		 		     => hash('sha256','NVHCP_Admin_1'),
				'Operator_Name'       	     => 'NVHCP_Admin_1',
				'password' 				     => hash('sha256', 'NV@A#200'),
				'RoleId'					=> 1,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> '',
				'user_type'				     => 0,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => 'NVHCP_Admin_1',
				'password' 				     =>'NV@A#200',
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);

			$insertArray = array(
				'username'		 		     => hash('sha256','NVHCP_Admin_2'),
				'Operator_Name'       	     => 'NVHCP_Admin_2',
				'password' 				     => hash('sha256', 'NV@A#109'),
				'RoleId'					=> 1,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> '',
				'user_type'				     => 0,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => 'NVHCP_Admin_2',
				'password' 				     =>'NV@A#109',
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);

			$insertArray = array(
				'username'		 		     => hash('sha256','NVHCP_Admin_3'),
				'Operator_Name'       	     => 'NVHCP_Admin_3',
				'password' 				     => hash('sha256', 'NV@A#116'),
				'RoleId'					=> 1,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> '',
				'user_type'				     => 0,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => 'NVHCP_Admin_3',
				'password' 				     =>'NV@A#116',
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);

			$insertArray = array(
				'username'		 		     => hash('sha256','NVHCP_Admin_4'),
				'Operator_Name'       	     => 'NVHCP_Admin_4',
				'password' 				     => hash('sha256', 'NV@A#134'),
				'RoleId'					=> 1,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> '',
				'user_type'				     => 0,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => 'NVHCP_Admin_4',
				'password' 				     =>'NV@A#134',
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);

			$insertArray = array(
				'username'		 		     => hash('sha256','NVHCP_Admin_5'),
				'Operator_Name'       	     => 'NVHCP_Admin_5',
				'password' 				     => hash('sha256', 'NV@A#134'),
				'RoleId'					=> 1,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> '',
				'user_type'				     => 0,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => 'NVHCP_Admin_5',
				'password' 				     =>'NV@A#134',
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);

			$insertArray = array(
				'username'		 		     => hash('sha256','NVHCP_Admin_6'),
				'Operator_Name'       	     => 'NVHCP_Admin_6',
				'password' 				     => hash('sha256', 'NV@A#179'),
				'RoleId'					=> 1,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> '',
				'user_type'				     => 0,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => 'NVHCP_Admin_6',
				'password' 				     =>'NV@A#179',
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);

			$insertArray = array(
				'username'		 		     => hash('sha256','NVHCP_Admin_7'),
				'Operator_Name'       	     => 'NVHCP_Admin_7',
				'password' 				     => hash('sha256', 'NV@A#156'),
				'RoleId'					=> 1,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> '',
				'user_type'				     => 0,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => 'NVHCP_Admin_7',
				'password' 				     =>'NV@A#156',
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);

			
			$insertArray = array(
				'username'		 		     => hash('sha256','NVHCP_Admin_8'),
				'Operator_Name'       	     => 'NVHCP_Admin_8',
				'password' 				     => hash('sha256', 'NV@A#122'),
				'RoleId'					=> 1,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> '',
				'user_type'				     => 0,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => 'NVHCP_Admin_8',
				'password' 				     =>'NV@A#122',
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);

			$insertArray = array(
				'username'		 		     => hash('sha256','NVHCP_Admin_9'),
				'Operator_Name'       	     => 'NVHCP_Admin_9',
				'password' 				     => hash('sha256', 'NV@A#191'),
				'RoleId'					=> 1,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> '',
				'user_type'				     => 0,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => 'NVHCP_Admin_9',
				'password' 				     =>'NV@A#191',
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);

			$insertArray = array(
				'username'		 		     => hash('sha256','NVHCP_Admin_10'),
				'Operator_Name'       	     => 'NVHCP_Admin_10',
				'password' 				     => hash('sha256', 'NV@A#186'),
				'RoleId'					=> 1,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> '',
				'user_type'				     => 0,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => 'NVHCP_Admin_10',
				'password' 				     =>'NV@A#186',
				'date'				    	 => date('Y-m-d'),
				);

				$this->Common_Model->insert_data('tbludata', $insertArray1);*/


				
		// state user
 		//$sql_facility = "select * from mststate where LanguageID is null";
		//$ress = $this->Common_Model->query_data($sql_facility);
		/*
		foreach ($ress as $value) {
		$password= 'password';

		$insertArray = array(
				'username'		 		     => hash('sha256',$value->StateCd.'_'.'ADMIN'),
				'Operator_Name'       	     => $value->StateName,
				'password' 				     => hash('sha256', $password),
				'RoleId'					=> 1,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> $value->id_mststate,
				'user_type'				     => 3,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => $value->StateCd.'_'.'ADMIN',
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);

		}*/
	}


	public function add_facuser(){	
		// fac user
		$sql_facility = "select * from mstfacility where id_mststate !=3";
		$ress = $this->Common_Model->query_data($sql_facility);


		foreach ($ress as $value) {
			/*Data entry operator (DO)*/
			//$username = 'DO'.'@'.$value->FacilityCode;
			//$password= $value->FacilityCode.''.'130';

			/*Medical Officer (MO)*/
			//$username = 'MO'.'@'.$value->FacilityCode;
			//$password= $value->FacilityCode.''.'130';

			/*Laboratory Technician (LT)*/
			//$username = 'LT'.'@'.$value->FacilityCode;
			//$password= $value->FacilityCode.''.'130';

			/*Pharmacist (PH)*/
			$username = 'PH'.'@'.$value->FacilityCode;
			$password= $value->FacilityCode.''.'130';


			/*$insertArray = array(
				'username'		 		     => hash('sha256',$username),
				'Operator_Name'       	     => $value->facility_short_name,
				'password' 				     => hash('sha256', $password),
				'RoleId'					=> 7,
				'id_mstfacility'			=> $value->id_mstfacility,
				'DistrictID'				=> $value->id_mstdistrict,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> $value->id_mststate,
				'user_type'				     => 2,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => $username,
				'password' 				     => $password,
				'Operator_Name'       	     => $value->facility_short_name,
				'date'				    	 => date('Y-m-d'),
				);

				$this->Common_Model->insert_data('tbludata', $insertArray1);*/

			}
		}


		public function add_fuser(){
			$sql_facility = "select * from mstdistrict where id_mststate!=3";
			$ress = $this->Common_Model->query_data($sql_facility);		
			
		// state user
		/*foreach ($ress as $value) {
		$password= 'password';
		 $usernamev = substr($value->DistrictName,0,3);

		$insertArray = array(
				'FacilityCode'       	     => $usernamev.'-'.'DH',
				'FacilityType' 				=> 'DH',
				'id_mststate'				=> $value->id_mststate,
				'id_mstdistrict'			=> $value->id_mstdistrict,
				'Name'						=> 'District Hospital',
				'facility_name'				=> $value->DistrictName.'-'.'District Hospital',
				'facility_short_name'		=> $value->DistrictName.'-'.'DH'
				);

			$this->Common_Model->insert_data('mstfacility', $insertArray);

		}*/
	}


	public function add_doctor($id_user = null){
		$RequestMethod = $this->input->server('REQUEST_METHOD');
		$loginData = $this->session->userdata('loginData');
		
		$max_dr = $this->Common_Model->maxadvancepurchase_id();

		if($RequestMethod == "POST")
		{	
			if($id_user != null)
			{
				$this->form_validation->set_rules('full_name', 'full_name','regex_match[/^([a-z ])+$/i]', 'trim|required|xss_clean');


				if ($this->form_validation->run() == FALSE) {

				$arr['status'] = 'false';
				$this->session->set_flashdata("tr_msg",validation_errors());
				}else{

				$updateArray = array(
					'name'       	    		=> $this->security->xss_clean(htmlentities($this->input->post('full_name'))),
				);
				
				$this->Common_Model->update_data('mst_medical_specialists', $updateArray, 'ai_id_mst_medical_specialists', $id_user);

				$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$id_user,'Update','Doctor Successfully Updated');
				$this->session->set_flashdata('tr_msg', 'Doctor Successfully Updated');
				redirect("users/add_doctor");
			}

			}else{
				$this->form_validation->set_rules('full_name', 'full_name','regex_match[/^([a-z ])+$/i]', 'trim|required|xss_clean');


				if ($this->form_validation->run() == FALSE) {

				$arr['status'] = 'false';
				$this->session->set_flashdata("tr_msg",validation_errors());
				}else{
				$insertArray = array(
					'name'       	    		=> $this->security->xss_clean(htmlentities($this->input->post('full_name'))),
					'id_mst_medical_specialists'=>$max_dr,						
					'id_mstfacility'	        => $loginData->id_mstfacility,
					'Session_StateID'	        => $loginData->State_ID
				);
				
				$insert_id = $this->Common_Model->insert_data('mst_medical_specialists', $insertArray);
				$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$insert_id,'Create','Doctor Successfully Created');
				$this->session->set_flashdata('tr_msg', 'Doctor Successfully Created');
				redirect("users/add_doctor");
			}			
		}
		}
		$query = "select * from mst_medical_specialists where id_mstfacility=".$loginData->id_mstfacility." ";
		$content['users_list'] = $this->Common_Model->query_data($query);
		
		if($id_user != null)
		{
			$sql_details = "select * from mst_medical_specialists where ai_id_mst_medical_specialists = ? ";
			$res_details = $this->db->query($sql_details,[$id_user])->result();
			//$res_details = $this->Common_Model->query_data($sql_details)[0];
			$content['user_details'] = $res_details[0];
			$content['edit_flag'] = 1;
		}
		$sql_facility = "select * from mstfacility";
		$res = $this->Common_Model->query_data($sql_facility);
		$content['Facility'] = $res;

		$sql_MSTRole = "select * from MSTRole order by Role";
		$content['mst_role'] = $this->Common_Model->query_data($sql_MSTRole);

		$content['subview'] = "add_doctor";
		$this->load->view('admin/main_layout', $content);

	}

	public function dodelete($id_user = null)
	{	
		$loginData = $this->session->userdata('loginData');
		 $Session_StateID = $loginData->State_ID;
		 $id_user = $id_user;

		 $sql = "delete from mst_medical_specialists where ai_id_mst_medical_specialists = ? and Session_StateID= ?";

		//$this->db->query($sql);

		$this->db->query($sql,[$id_user,$Session_StateID]);
		$this->session->set_flashdata('tr_msg', 'Delete','Doctor Successfully Deleted.');
		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$id_user,'Delete','Doctor Successfully Deleted');

		redirect("users/add_doctor");
	}

	/* Add sample transporter*/
	public function add_designation($id_user = null){
		$RequestMethod = $this->input->server('REQUEST_METHOD');
		$loginData = $this->session->userdata('loginData');
		
		$max_dr = $this->Common_Model->maxdesc_id();

		if($RequestMethod == "POST")
		{	
			if($id_user != null)
			{
					$this->form_validation->set_rules('full_name', 'Name', 'regex_match[/^([a-z ])+$/i]','trim|required|xss_clean');
					
					
					if ($this->form_validation->run() == FALSE) {
					
					$arr['status'] = 'false';
					$this->session->set_flashdata("tr_msg",validation_errors());
					}else{

				$updateArray = array(
					'Designation'       	    		=> $this->security->xss_clean($this->input->post('full_name')),
				);
				
				$this->Common_Model->update_data('mst_designation_list', $updateArray, 'ai_designation_listId', $id_user);
				$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$id_user,'Update','Designation Successfully Updated');
				$this->session->set_flashdata('tr_msg', 'Designation Successfully Updated');
				redirect("users/add_designation");
			}
			}else{

				$this->form_validation->set_rules('full_name', 'Name','regex_match[/^([a-z ])+$/i]', 'trim|required|max_length[50]|xss_clean');
					
					
					if ($this->form_validation->run() == FALSE) {
					
					$arr['status'] = 'false';
					$this->session->set_flashdata("tr_msg",validation_errors());
					}else{
				$insertArray = array(
					'Designation'       	    => $this->security->xss_clean($this->input->post('full_name')),
					'designation_listId'		=>$max_dr,
					'id_mstfacility'	        => $loginData->id_mstfacility,						
					'Session_StateID'	        => $loginData->State_ID
				);

				
				$insert_id = $this->Common_Model->insert_data('mst_designation_list', $insertArray);
				$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$insert_id,'Create','Designation Successfully Created');
				$this->session->set_flashdata('tr_msg', 'Designation Successfully Created');
				redirect("users/add_designation");
			}
			}
			
		}

		
		$query = "select * from mst_designation_list where id_mstfacility=".$loginData->id_mstfacility."";
		$content['users_list'] = $this->Common_Model->query_data($query);
		
		if($id_user != null)
		{
			$sql_details = "select * from mst_designation_list where ai_designation_listId = ".$id_user."";
			$res_details = $this->Common_Model->query_data($sql_details)[0];
			$content['user_details'] = $res_details;
			$content['edit_flag'] = 1;
		}
		$sql_facility = "select * from mstfacility";
		$res = $this->Common_Model->query_data($sql_facility);
		$content['Facility'] = $res;

		$sql_MSTRole = "select * from MSTRole order by Role";
		$content['mst_role'] = $this->Common_Model->query_data($sql_MSTRole);

		$content['subview'] = "add_designation";
		$this->load->view('admin/main_layout', $content);

	}

	public function designationdelete($id_user = null)
	{	
		$loginData = $this->session->userdata('loginData');
			$Session_StateID = $loginData->State_ID;

		$sql = "DELETE from mst_designation_list where ai_designation_listId = ? AND Session_StateID = ?";

		$this->db->query($sql,[$id_user,$Session_StateID]);

		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$id_user,'Delete','Designation Successfully Deleted');

		redirect("users/add_designation");
	}


	public function add_facuserdemo1(){		
		
		// fac user
		$sql_facility = "select * from mstfacility where id_mststate =9";
		$ress = $this->Common_Model->query_data($sql_facility);

		//exit();
		$i=0;foreach ($ress as $value) { $i++;
		//echo $i;exit;
			/*Data entry operator (DO)*/
		//$username = 'DO'.'@'.$value->FacilityCode;
		//$password= $value->FacilityCode.''.'130';

			/*Medical Officer (MO)*/
		//$username = 'MO'.'@'.$value->FacilityCode;
		//$password= $value->FacilityCode.''.'130';

			/*Laboratory Technician (LT)*/
		//$username = 'LT'.'@'.$value->FacilityCode;
		//$password= $value->FacilityCode.''.'130';

			/*Pharmacist (PH)*/
		//$username = 'PH'.'@'.$value->FacilityCode;
		//$password= $value->FacilityCode.''.'130';


		/*$insertArray = array(
				'username'		 		     => hash('sha256',$username),
				'Operator_Name'       	     => $value->facility_short_name,
				'password' 				     => hash('sha256', $password),
				'RoleId'					=> 7,
				'id_mstfacility'			=> $value->id_mstfacility,
				'DistrictID'				=> $value->id_mstdistrict,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> $value->id_mststate,
				'user_type'				     => 2,
			);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => $username,
				'password' 				     => $password,
				'Operator_Name'       	     => $value->facility_short_name,
				'date'				    	 => date('Y-m-d'),
				);

				$this->Common_Model->insert_data('tbludata', $insertArray1);*/


			//for ($i=0; $i < 10; $i++) { 
				
				$username = 'demo_'.$i;
				$password= 'password';


				$insertArray = array(
					'username'		 		     => hash('sha256',$username),
					'Operator_Name'       	     => 'demo_'.$i,
					'password' 				     => hash('sha256', $password),
					'RoleId'					=> 1,
					'id_mstfacility'			=> $value->id_mstfacility,
					'DistrictID'				=> $value->id_mstdistrict,
					'UpdatedOn'					=> date('Y-m-d'),
					'State_ID'	        		=> '9',
					'user_type'				     => 2,
				);

				$this->Common_Model->insert_data('tblusers', $insertArray);

				$insert_id = $this->db->insert_id();

				$insertArray1 = array(
					'tbluid'		 		     => $insert_id,
					'tbluname'		 		     => $username,
					'password' 				     => $password,
					'Operator_Name'       	     => $value->facility_short_name,
					'date'				    	 => date('Y-m-d'),
				);

				$this->Common_Model->insert_data('tbludata', $insertArray1);

			//}
			//19
			}
		}
		public function add_doctorname(){
			// fac user
			$sql_facility = "select * from mstfacility where id_mststate =19";
			$ress = $this->Common_Model->query_data($sql_facility);

			$i=0;foreach ($ress as $value) { $i++;

				$nsertArray1 = array(
					'id_mst_medical_specialists'		 => 5+$i,
					'name'		 		    			 => 'Test Dr.',
					'id_mstfacility' 				     => $value->id_mstfacility,
					'Session_StateID'       	         => $value->id_mststate,
					'is_deleted'				    	 => 0,
				);
				$this->Common_Model->insert_data('mst_medical_specialists', $nsertArray1);
			}
		}


		public function getDistrictscode($id_mststate = null,$is_Mtc = null)
		{	
			$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1 and (user_type=1 || user_type=3)";
				$stateid_where = "where 1";
				$district_where = "where 1";
				$mstdistrict_where= " 1";
			}
			elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
				$mstdistrict_where= " id_mstdistrict = '".$loginData->DistrictID."'";
			}
			elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND State_ID = '".$loginData->State_ID."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
				$mstdistrict_where= " 1";
			}
			elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
				$mstdistrict_where= " id_mstdistrict = '".$loginData->DistrictID."'";
			}
			$arr = array();
		  //$sql = "select DistrictCd from mstdistrict   where id_mstdistrict = ?";
			//$idmststate = explode('/', $id_mststate);
			//print_r($idmststate);
			$sql = "select d.DistrictCd,sum(case when f.is_Mtc=".$is_Mtc." then 1 else 0 end) as is_Mtc from mstdistrict d left join mstfacility f on  d.id_mstdistrict=f.id_mstdistrict   where d.id_mstdistrict=".$id_mststate."  group by d.id_mstdistrict";
			$districts = $this->db->query($sql)->result();

			if($districts){
				$arr['status'] = 'true';
				$arr['fields'] = json_encode($districts[0]);
			}

			echo json_encode($arr);
		}

		public function eHRManagement($id = null){
			$content['users_list'] = array();
			$RequestMethod = $this->input->server('REQUEST_METHOD');
			$loginData = $this->session->userdata('loginData');

			if($RequestMethod == "POST")
			{			
				if($id != null)
				{
					$this->form_validation->set_rules('nvhcp', 'NVHCP Label','regex_match[/^([a-z ])+$/i]', 'trim|required|xss_clean');
					$this->form_validation->set_rules('ehr_code', 'EHR Standard Code','regex_match[/^([a-z ])+$/i]', 'trim|required|xss_clean');
					$this->form_validation->set_rules('stand_ref', 'EHR Standard Ref.','regex_match[/^([a-z ])+$/i]', 'trim|required|xss_clean');


				if ($this->form_validation->run() == FALSE) {

				$arr['status'] = 'false';
				$this->session->set_flashdata("tr_msg",validation_errors());
				}else{

					$updateArray = array(
						'nvhcp'     => $this->security->xss_clean($this->input->post('nvhcp')),
						'ehr_code'	=> $this->security->xss_clean($this->input->post('ehr_code')),	
						'nvhcp_stand_ref'	=> $this->security->xss_clean($this->input->post('stand_ref')),					
						'status'	=> 1
					);

					$this->db->where('id_ehr_management',$id);
					$this->db->update('ehr_management', $updateArray);
					$this->session->set_flashdata('tr_msg', 'EHR Data Successfully Updated');
					$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$id,'Update','EHR Data Successfully Updated');
					redirect("users/eHRManagement");
				}
				}else{

					
					$this->form_validation->set_rules('nvhcp', 'NVHCP Label','regex_match[/^([a-z ])+$/i]', 'trim|required|xss_clean');
					$this->form_validation->set_rules('ehr_code', 'EHR Standard Code','regex_match[/^([a-z ])+$/i]', 'trim|required|xss_clean');
					$this->form_validation->set_rules('stand_ref', 'EHR Standard Ref.','regex_match[/^([a-z ])+$/i]', 'trim|required|xss_clean');
					
					
					if ($this->form_validation->run() == FALSE) {
					
					$arr['status'] = 'false';
					$this->session->set_flashdata("tr_msg",validation_errors());
					}else{

					$insertArray = array(
						'nvhcp'     => $this->security->xss_clean($this->input->post('nvhcp')),
						'ehr_code'	=> $this->security->xss_clean($this->input->post('ehr_code')),		
						'nvhcp_stand_ref'	=> $this->security->xss_clean($this->input->post('stand_ref')),
						'status'	=> 1					
					);

					$insert_id = $this->db->insert('ehr_management', $insertArray);
					$this->session->set_flashdata('tr_msg', 'EHR Data Successfully Created');
					$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$insert_id,'Create','EHR Data Successfully Created');
					redirect("users/eHRManagement");

				}
				}			
			}

			$query = "select * from ehr_management ";
			$content['ehr_code_list'] = $this->db->query($query)->result();

			if($id != null)
			{
				$this->db->select('*');
				$this->db->where('id_ehr_management',$id);				
				$content['ehr_code'] = $this->db->get('ehr_management')->row();				
				$content['edit_flag'] = 1;
			}
			
			$content['subview'] = "add_ehrmanagement";
			$this->load->view('admin/main_layout', $content);
		}

		public function delete_eHRManagement($id = null){
			$loginData = $this->session->userdata('loginData');
			$Session_StateID = $loginData->State_ID;
			if($id != null){
				$this->db->where('id_ehr_management',$id);
				$this->db->delete('ehr_management');
				$this->session->set_flashdata('tr_msg', 'EHR Data Deleted Successfully');
				$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$id,'Delete','EHR Data Successfully Deleted');
				redirect("users/eHRManagement");
			}

		}

		public function changepassword($id_user = null){

			$this->load->library('form_validation');
			$loginData = $this->session->userdata('loginData'); 

			if($loginData->id_tblusers!=base64_decode($id_user)){
				redirect('login/logout');
			}

			$RequestMethod = $this->input->server('REQUEST_METHOD');	
			if($RequestMethod == "POST")
			{
				$hashed_password = $this->security->xss_clean($this->input->post('password') );
				if($id_user != null)
				{
					$this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean');
					if ($this->form_validation->run() == FALSE) {
						$arr['status'] = 'false';
						$this->session->set_flashdata("tr_msg",validation_errors());
					}else{
						if($hashed_password==$loginData->password){
							$this->session->set_flashdata("tr_msg",'Old password not accepted please choose new password');
							$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$id_user,'Oldpassword','Old password not accepted please choose new password');

							redirect('users/changepassword/'.base64_encode($loginData->id_tblusers));
						}

						if(trim($this->input->post('password')) !== "")
						{
							$updateArray['password'] = $hashed_password;
							$updateArray['flag'] = 2;
							$updateArray['UpdatedBy'] = $loginData->id_tblusers;
							$updateArray['UpdatedOn'] = date('Y-m-d');
						}
						else
						{
							unset($updateArray['password']);
						}
						$this->Common_Model->update_data('tblusers', $updateArray, 'id_tblusers', base64_decode($id_user));

						$this->session->set_flashdata('tr_msg', 'User Successfully Updated.');
						$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$id_user,'Updated','Password Change Successfully');
						if($loginData->user_type==2){
							redirect('patientinfo?p=1');
						}else{						
							redirect('dashboard');
						}
					}
				}
			}

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1 and (user_type=1 || user_type=3)";
				$stateid_where = "where 1";
				$district_where = "where 1";
				$mstdistrict_where= "AND 1";
				$mststate_where= " 1";
				$mstfacility_where = "1";
			}
			elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND State_ID = '".$loginData->State_ID."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
				$mstdistrict_where= "AND id_mstdistrict = '".$loginData->DistrictID."'";
				$mststate_where= " id_mststate = '".$loginData->State_ID."'";
				$mstfacility_where = "id_mstfacility ='".$loginData->id_mstfacility."'";
			}
			elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND State_ID = '".$loginData->State_ID."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
				$mstdistrict_where= "AND 1";
				$mststate_where= " id_mststate = '".$loginData->State_ID."'";
				$mstfacility_where = "1";
			}
			elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND State_ID = '".$loginData->State_ID."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
				$mstdistrict_where= "AND id_mstdistrict = '".$loginData->DistrictID."'";
				$mststate_where= " id_mststate = '".$loginData->State_ID."'";
				$mstfacility_where = "id_mstfacility ='".$loginData->id_mstfacility."'";
			}


			if($id_user != null)
			{
				$sql_details = "select * from tblusers where id_tblusers = ".base64_decode($id_user)." ".$sess_where."";
				$res_details = $this->Common_Model->query_data($sql_details);
				$content['user_details'] = $res_details;
				//$content['edit_flag'] = 1;
			}



			$content['id_user'] =base64_decode($id_user);
			if($loginData->RoleId == '1' && $loginData->id_tblusers!=base64_decode($id_user)){
				$sql_MSTRole = "select * from MSTRole where RoleId!=1 order by Role";
				$content['mst_role'] = $this->Common_Model->query_data($sql_MSTRole);
			}elseif($loginData->RoleId == '1' && $loginData->id_tblusers==base64_decode($id_user)){
				$sql_MSTRole = "select * from MSTRole order by Role";
				$content['mst_role'] = $this->Common_Model->query_data($sql_MSTRole);
			}else{
				$sql_MSTRole = "select * from MSTRole order by Role";
				$content['mst_role'] = $this->Common_Model->query_data($sql_MSTRole);
			}
			$sql = "select flag from tblusers where id_tblusers = ".$loginData->id_tblusers."";
			$content['flag'] = $this->db->query($sql)->result();

			if($content['flag'][0]->flag==2){

				redirect('login/logout');
			}

			$content['subview'] = "changepassword";
			$this->load->view('admin/main_layout', $content);


		}

		public function upload_excel(){

			$this->load->library('form_validation');
			$loginData = $this->session->userdata('loginData');
			//pr($loginData);
				//create directories
			$Dir = "Uploads";	
			$Dir_2 = $Dir.'/upload_excel/';
			if (!file_exists($Dir)) 
				mkdir($Dir, 0777, true);

			if (!file_exists($Dir_2)) 
				mkdir($Dir_2, 0777, true);
			
			$thumbnail_images = '';
			$related_images = '';

			$thumbnail_images = $Dir_2;	
			$related_images = $Dir_2;	

			if (!file_exists($thumbnail_images)) 
				mkdir($thumbnail_images, 0777, true);

			if (!file_exists($related_images)) 
				mkdir($related_images, 0777, true);

			$milliseconds = round(microtime(true) * 1000);

			if( isset($_FILES['thumbnail_image']) && !empty($_FILES['thumbnail_image']['name']) ){
				echo $ext = pathinfo($_FILES['thumbnail_image']['name'], PATHINFO_EXTENSION);
				if($ext != 'xls' || $ext !='csv') {
					$this->session->set_flashdata('tr_msg', 'invalid file format.');
						//redirect('users/upload_excel');
				}
		/*elseif($ext !='xls') {
			$this->session->set_flashdata('tr_msg', 'invalid file format.');
						//redirect('users/upload_excel');
		}
		elseif($ext !='ods'){
			$this->session->set_flashdata('tr_msg', 'invalid file format.');
						//redirect('users/upload_excel');
		}elseif($ext !='csv'){
			$this->session->set_flashdata('tr_msg', 'invalid file format.');
						//redirect('users/upload_excel');
		}*/else{


			//unset($thumbnail_images);
			
			$filepath=explode('.',$_FILES['thumbnail_image']['name']);
			$filename=$filepath[0];
			$ext=$filepath[1];	
			$imageFullPath= $thumbnail_images.$milliseconds.'_'.$loginData->id_tblusers.'.'.$ext;
			move_uploaded_file($_FILES["thumbnail_image"]["tmp_name"], $imageFullPath);

			$datath = array( 

				'excel_file' => $imageFullPath,
				'id_mststate'=> $loginData->State_ID,
				'Uploadby'=>$loginData->id_tblusers,
				'date'	=>date("Y-m-d H:i:s")
			);
               // $this->Supplier_Model->thumbnail_image($datath,$loginData->id_tblusers);

			$this->Common_Model->insert_data('upload_excel', $datath);

			$this->session->set_flashdata('tr_msg', 'File Successfully uploaded.');
			redirect('users/upload_excel');

		}
	}
	
	$sql = "select * from upload_excel where Uploadby = ".$loginData->id_tblusers." order by id desc";
	$content['filedata'] = $this->db->query($sql)->result();

	$content['subview'] = "upload_excel";
	$this->load->view('admin/main_layout', $content);
}

public function testcron(){

			$insertArray = array(
							'patientguid'		 		     =>'454353453443',
							'patient_uid'       	     => '45345435',
							'file_path' 				     => 'fdgdgdfgdf'
						);

						$this->Common_Model->insert_data('tbl_treatment_card_file_paths', $insertArray);


}

}