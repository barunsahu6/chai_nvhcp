<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public $colnames_follow_up = [
		'UID'              => 'UID',
		'FirstName'        => 'Patient Name',
		'hospital'         => 'Hospital',
		'address'          => 'Contact Address',
		'VillageTown'      => 'Village/Town',
		'BlockName'        => 'Block',
		'Mobile'           => 'Contact Number',
		'Next_Visitdt'     => 'Advised Visit Date',
		'NextVisitPurpose' => 'Purpose',
	];

	public function __construct()
	{	
		parent::__construct();
		$this->load->model('Common_Model');
		$this->load->model('Patient_Model');
		//$this->load->model('Dashboard_Model');
		//error_reporting(E_ALL);
		$this->load->helper('common');
		//error_reporting(E_ALL);
		$loginData = $this->session->userdata('loginData');
		if($loginData == null)
		{
			redirect('login');
		}
	}

	public function index()
	{	
		$content['subview'] = 'reports';
		$this->load->view('admin/main_layout', $content);
	}

	public function monthly_report()
	{
//error_reporting(E_ALL);
		$loginData = $this->session->userdata('loginData');
		//pr($loginData);

		 $REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		

		if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('facility');	
			$filters1['startdate']      = timeStamp($this->input->post('startdate'));	
			$filters1['enddate']        = date($this->input->post('enddate'));
			//$filters1['enddate1']        = timeStamp($this->input->post('enddate'));
			if ($filters1['enddate']==date('d-m-Y')) {
				 $filters1['enddate']        = timeStamp(timeStampShow('-1 day',$filters1['enddate']));
			}
			else{
				$filters1['enddate']        = timeStamp($this->input->post('enddate'));
			}
			
		}
		else
		{	
			 $filters1['id_search_state'] = 0;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;	
			$filters1['startdate']      = date('Y-m-01');	
			$filters1['enddate']        = timeStamp(timeStampShow('-1 day',date('Y-m-d')));
			//$filters1['enddate1']        = date('Y-m-d');
			//pr($content['filters1']);exit();
		}

		 $this->session->set_userdata('filters1', $filters1);
	
	$this->load->model('Monthly_model');

	$content['filters1']   = $this->session->userdata('filters1');
	
	//$content['count_1_1'] = $this->Monthly_model->get_data_1_1();

	$content['count_1_1'] = $this->Monthly_model->get_data_1_1_summ();

	$content['count_2_1'] = $this->Monthly_model->get_data_2_1();

	$content['Ini_male'] = $this->Monthly_model->IniTreatmentduring_male();
	$content['Ini_female'] = $this->Monthly_model->IniTreatmentduring_female();
	$content['Ini_children'] = $this->Monthly_model->IniTreatmentduring_children();
	$content['Ini_transgender'] = $this->Monthly_model->IniTreatmentduring_transgender();
	
	$content['count_2_2'] = $this->Monthly_model->get_data_2_2();
	$content['count_2_3'] = $this->Monthly_model->get_data_2_3();

	//$content['count_3_1'] = $this->Monthly_model->get_data_3_1();

	$content['count_3_1'] = $this->Monthly_model->get_completed_treatment();


	$content['count_3_2'] = $this->Monthly_model->get_data_3_2();
	$content['count_3_3'] = $this->Monthly_model->get_data_3_3();
	$content['count_3_4'] = $this->Monthly_model->get_data_3_4();
	$content['count_3_5'] = $this->Monthly_model->get_data_3_5();
	$content['count_3_6'] = $this->Monthly_model->get_data_3_6();
	$content['count_3_7'] = $this->Monthly_model->get_data_3_7();

	//$content['count_4_1'] = $this->Monthly_model->get_data_4_1();
	$content['count_4_1'] = $this->Monthly_model->get_eligible_forsvr();
	
	$content['count_4_2'] = $this->Monthly_model->get_data_4_2();
	$content['count_4_3'] = $this->Monthly_model->get_data_4_3();

	$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}

	$content['start_date'] = date('Y-m-d');
	$content['end_date']   = date('Y-m-d');

		 $sql = "select * from mststate where ".$sess_where." order by StateName ASC";
		$content['states'] = $this->db->query($sql)->result();

		 $sql = "select * from mstdistrict where ".$sess_mstdistrict."";
		$content['districts'] = $this->db->query($sql)->result();
		//pr($content['districts']);
		 $sql = "select * from mstfacility where  ".$sess_mstfacility."";
		$content['facilities'] = $this->db->query($sql)->result();	
		$sql = "select flag from tblusers where id_tblusers = ".$loginData->id_tblusers."";
		$content['flag'] = $this->db->query($sql)->result();

		$content['subview'] = 'monthly_report';
		$this->load->view('pages/main_layout', $content);
	}

public function niti_aayog(){
	
			$loginData = $this->session->userdata('loginData');

			if($loginData->user_type != 1){
			$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login');	
		}


		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		

		if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('facility');	
			$filters1['startdate']      = timeStamp($this->input->post('startdate'));	
			$filters1['enddate']        = date($this->input->post('enddate'));
			if ($filters1['enddate']==date('d-m-Y')) {
				 $filters1['enddate']        = timeStamp(timeStampShow('-1 day',$filters1['enddate']));
			}
			else{
				$filters1['enddate']        = timeStamp($this->input->post('enddate'));
			}
		}
		else
		{	
			$filters1['id_search_state'] = 0;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;	
			$filters1['startdate']      = date('Y-01-01');	
			$filters1['enddate']        = timeStamp(timeStampShow('-1 day',date('Y-m-d')));
		}

			$filterdata = $this->session->set_userdata('filters1', $filters1);
	


	$this->load->model('Monthly_model');

	$content['filters1']   = $this->session->userdata('filters1');

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}
			
			$content['start_date'] = date('Y-01-01');
	$content['end_date']   = date('Y-m-d');

			//$content['hepatitis_c_a'] = $this->Monthly_model->hepatitis_c_a();

			$content['hepatitis_c_a'] = $this->Monthly_model->get_serological_test();


			//$content['hepatitis_b_a'] = $this->Monthly_model->hepatitis_b_a();
			$content['hepatitis_b_a'] = $this->Monthly_model->get_hepb_positive();

			/*22-02-2020 start*/
			$content['get_hepb_initiated'] = $this->Monthly_model->get_hepb_initiated();

			/*22-02-2020 end*/
			//$content['hepatitis_c_b'] = $this->Monthly_model->hepatitis_c_b();

			$content['hepatitis_c_b'] = $this->Monthly_model->initiated_on_treatment_consolidated();

			$content['hep_b'] = $this->Monthly_model->hep_b();
			//$content['hep_c'] = $this->Monthly_model->hep_c();

			$content['hep_c'] = $this->Monthly_model->get_completed_treatment_niti();


			$sql = "select * from mststate where ".$sess_where." order by StateName ASC";
			$content['states'] = $this->db->query($sql)->result();

			$sql = "select * from mstdistrict where ".$sess_mstdistrict."";
			$content['districts'] = $this->db->query($sql)->result();

			$sql = "select * from mstfacility where  ".$sess_mstfacility."";
			$content['facilities'] = $this->db->query($sql)->result();	

	$content['subview'] = 'niti_aayog';
	$this->load->view('pages/main_layout', $content);
}
/*National*/

public function niti_aayog_national(){
	
			$loginData = $this->session->userdata('loginData');

			if($loginData->user_type != 1){
			$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login');	
		}

		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		

		if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('facility');	
			$filters1['startdate']      = timeStamp($this->input->post('startdate'));	
			$filters1['enddate']        = date($this->input->post('enddate'));
			if ($filters1['enddate']==date('d-m-Y')) {
				 $filters1['enddate']        = timeStamp(timeStampShow('-1 day',$filters1['enddate']));
			}
			else{
				$filters1['enddate']        = timeStamp($this->input->post('enddate'));
			}
		}
		else
		{	
			$filters1['id_search_state'] = 0;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;	
			$filters1['startdate']      = date('Y-01-01');	
			$filters1['enddate']        = timeStamp(timeStampShow('-1 day',date('Y-m-d')));
		}

			$filterdata = $this->session->set_userdata('filters1', $filters1);
	


	$this->load->model('Monthly_model');

	$content['filters1']   = $this->session->userdata('filters1');

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}
			
			$content['start_date'] = date('Y-01-01');
	$content['end_date']   = date('Y-m-d');

			$content['hepatitis_c_a'] = $this->Monthly_model->hepatitis_c_a();
			$content['hepatitis_c_b'] = $this->Monthly_model->hepatitis_c_b();
			$content['hep_b'] = $this->Monthly_model->hep_b();
			$content['hep_c'] = $this->Monthly_model->hep_c();

			//$content['nationaldata'] = $this->Monthly_model->nationaldata_statewise();
			$content['nationaldata'] = $this->Monthly_model->initiated_on_treatment_state();
			

			$sql = "select * from mststate where ".$sess_where." order by StateName ASC";
			$content['states'] = $this->db->query($sql)->result();

			$sql = "select * from mstdistrict where ".$sess_mstdistrict."";
			$content['districts'] = $this->db->query($sql)->result();

			$sql = "select * from mstfacility where  ".$sess_mstfacility."";
			$content['facilities'] = $this->db->query($sql)->result();	

	$content['subview'] = 'niti_aayog_national';
	$this->load->view('pages/main_layout', $content);
}


/*end*/
public function lfu_report(){

	$loginData = $this->session->userdata('loginData');

		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		

		if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('facility');	
			$filters1['startdate']      = timeStamp($this->input->post('startdate'));	
			$filters1['enddate']        = date($this->input->post('enddate'));
			if ($filters1['enddate']==date('d-m-Y')) {
				 $filters1['enddate']        = timeStamp(timeStampShow('-1 day',$filters1['enddate']));
			}
			else{
				$filters1['enddate']        = timeStamp($this->input->post('enddate'));
			}
		}
		else
		{	
			$filters1['id_search_state'] = 0;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;	
			$filters1['startdate']      = date('Y-01-01');	
			$filters1['enddate']        = timeStamp(timeStampShow('-1 day',date('Y-m-d')));
		}

			$filterdata = $this->session->set_userdata('filters1', $filters1);
	


	$this->load->model('Monthly_model');

	$content['filters1']   = $this->session->userdata('filters1');

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}
			
			$content['start_date'] = date('Y-01-01');
	$content['end_date']   = date('Y-m-d');

			


			$sql = "select * from mststate where ".$sess_where." order by StateName ASC";
			$content['states'] = $this->db->query($sql)->result();

			$sql = "select * from mstdistrict where ".$sess_mstdistrict."";
			$content['districts'] = $this->db->query($sql)->result();

			$sql = "select * from mstfacility where  ".$sess_mstfacility."";
			$content['facilities'] = $this->db->query($sql)->result();

			$sql = "SELECT * FROM `mstlookup` where flag = 49 and LanguageID = 1";
			$content['reason_flunodata'] = $this->db->query($sql)->result();	

			$content['reason_flu'] = $this->Monthly_model->reason_flu();

	$content['subview'] = 'lfu_report';
	$this->load->view('pages/main_layout', $content);
}
public function inventory_management(){
	$loginData = $this->session->userdata('loginData');

		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		

		if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('facility');	
			$filters1['startdate']      = timeStamp($this->input->post('startdate'));	
			$filters1['enddate']        = timeStamp($this->input->post('enddate'));	
		}
		else
		{	
			$filters1['id_search_state'] = 0;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;	
			$filters1['startdate']      = date('Y-01-01');	
			$filters1['enddate']        = date('Y-m-d');
		}

			$filterdata = $this->session->set_userdata('filters1', $filters1);
	


	$this->load->model('Monthly_model');

	$content['filters1']   = $this->session->userdata('filters1');

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}
			
			$content['start_date'] = date('Y-01-01');
	$content['end_date']   = date('Y-m-d');

			$content['hepatitis_c_a'] = $this->Monthly_model->hepatitis_c_a();
			$content['hepatitis_c_b'] = $this->Monthly_model->hepatitis_c_b();
			$content['hep_b'] = $this->Monthly_model->hep_b();
			$content['hep_c'] = $this->Monthly_model->hep_c();


			$sql = "select * from mststate where ".$sess_where." order by StateName ASC";
			$content['states'] = $this->db->query($sql)->result();

			$sql = "select * from mstdistrict where ".$sess_mstdistrict."";
			$content['districts'] = $this->db->query($sql)->result();

			$sql = "select * from mstfacility where  ".$sess_mstfacility."";
			$content['facilities'] = $this->db->query($sql)->result();

			 $sql = "SELECT * FROM `mstlookup` where flag = 49 and LanguageID = 1";
			$content['reason_flu'] = $this->db->query($sql)->result();	

	$content['subview'] = 'inventory_management';
	$this->load->view('pages/main_layout', $content);
}
public function reasons_for_death(){

$loginData = $this->session->userdata('loginData');

		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		

		if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('facility');	
			$filters1['startdate']      = timeStamp($this->input->post('startdate'));	
			$filters1['enddate']        = date($this->input->post('enddate'));
			if ($filters1['enddate']==date('d-m-Y')) {
				 $filters1['enddate']        = timeStamp(timeStampShow('-1 day',$filters1['enddate']));
			}
			else{
				$filters1['enddate']        = timeStamp($this->input->post('enddate'));
			}
		}
		else
		{	
			$filters1['id_search_state'] = 0;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;	
			$filters1['startdate']      = date('Y-01-01');	
			$filters1['enddate']        = timeStamp(timeStampShow('-1 day',date('Y-m-d')));
		}

			$filterdata = $this->session->set_userdata('filters1', $filters1);
	


	$this->load->model('Monthly_model');

	$content['filters1']   = $this->session->userdata('filters1');

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}
			
			$content['start_date'] = date('Y-01-01');
			$content['end_date']   = date('Y-m-d');

			$content['reasons_for_death'] = $this->Monthly_model->reasons_for_death();

			$sql = "select * from mststate where ".$sess_where." order by StateName ASC";
			$content['states'] = $this->db->query($sql)->result();

			$sql = "select * from mstdistrict where ".$sess_mstdistrict."";
			$content['districts'] = $this->db->query($sql)->result();

			$sql = "select * from mstfacility where  ".$sess_mstfacility."";
			$content['facilities'] = $this->db->query($sql)->result();	

	$content['subview'] = 'reasons_for_death';
	$this->load->view('pages/main_layout', $content);

}
public function hmis_report(){

	$loginData = $this->session->userdata('loginData');

		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		

		if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('facility');	
			$filters1['startdate']      = timeStamp($this->input->post('startdate'));	
			$filters1['enddate']        = date($this->input->post('enddate'));
			if ($filters1['enddate']==date('d-m-Y')) {
				 $filters1['enddate']        = timeStamp(timeStampShow('-1 day',$filters1['enddate']));
			}
			else{
				$filters1['enddate']        = timeStamp($this->input->post('enddate'));
			}
		}
		else
		{	
			$filters1['id_search_state'] = 0;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;	
			$filters1['startdate']      = date('Y-01-01');	
			$filters1['enddate']        = timeStamp(timeStampShow('-1 day',date('Y-m-d')));
		}

			$filterdata = $this->session->set_userdata('filters1', $filters1);
	


	$this->load->model('Monthly_model');

	$content['filters1']   = $this->session->userdata('filters1');

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}
			
			$content['start_date'] = date('Y-01-01');
	$content['end_date']   = date('Y-m-d');

			

			$content['hepatitis_c_a'] = $this->Monthly_model->hims_hepatitis_a();
			$content['hepatitis_c_b'] = $this->Monthly_model->hims_hepatitis_b();
			$content['hepatitis_c'] = $this->Monthly_model->hims_hepatitis_c();
			$content['hepatitis_d'] = $this->Monthly_model->hims_hepatitis_e();

			$content['hepatitisp_c_a'] = $this->Monthly_model->hims_hepatitis_a_positive();
			$content['hepatitisp_c_b'] = $this->Monthly_model->hims_hepatitis_b_positive();
			$content['hepatitisp_c'] = $this->Monthly_model->hims_hepatitis_c_positive();
			$content['hepatitisp_d'] = $this->Monthly_model->hims_hepatitis_e_positive();
			$content['hcv_vl_detected'] = $this->Monthly_model->hims_hepatitis_c_positive_elegible();
			$content['hep_b_confirmatory'] = $this->Monthly_model->hims_hepatitis_b_positive_HBV_DNA();
			$content['firstst_dispensation'] = $this->Monthly_model->hmis_hep_c_T_Initiation(); 
			$content['etr_during'] = $this->Monthly_model->hmis_hep_c_treatment_completed(); 
			$content['svr_not_detected'] = $this->Monthly_model->SVR12Wdetected();  
			$content['hbv_detected'] = $this->Monthly_model->hmis_hbv_elegible(); 
			$content['hbs_elegible'] = $this->Monthly_model->hmis_hepB_T_Initiation(); 


			$sql = "select * from mststate where ".$sess_where." order by StateName ASC";
			$content['states'] = $this->db->query($sql)->result();

			$sql = "select * from mstdistrict where ".$sess_mstdistrict."";
			$content['districts'] = $this->db->query($sql)->result();

			$sql = "select * from mstfacility where  ".$sess_mstfacility."";
			$content['facilities'] = $this->db->query($sql)->result();	

	$content['subview'] = 'hmis_report';
	$this->load->view('pages/main_layout', $content);
}
public function exceldata(){

	 
        $start = 1;
       
       
                $rs[] = '1 (a). Hepatitis C - Functional labs reporting under the program';
                 $rs[] = 'Opening Inventory';
                  $rs[] = 'Opening Inventory';
                   $rs[] = 'Opening Inventory';
                    $rs[] = 'Opening Inventory';

         
            
        
        
        $ex = get_excel_obj();
        $h1 = array('1. Number of Hepatitis C infected people seeking care at the treatment center (Registering in Care)', 'Adult Male', 'Adult Female', 'Children < 18 years', 'Total');//, 'Lab 7'
       
        $h = array('Period', 'Inventory Report', 'NICED', 'AIIMS', 'KASTURBA', 'NIMHANS', 'TRC', 'NARI');//, 'TNMGR'
        set_page_heading('Stock Status Report', $ex);
        set_table_heading($h1, '6', $ex);
        set_table_data($rs, '8', $ex);
       
        excel_download("Stock_Status_" . date('m-Y'), $ex);
    

}

public function exporttoexcel()  {

	if( count($this->session->userdata('reporttoexcel')['Data']) > 0 ) {
	
		//$subscribers = $this->phpexcel_model->get_users();

		//require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';
		require_once APPPATH . '/third_party/Phpexcel/Bootstrap.php';

		// Create new Spreadsheet object
		$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

// Set document properties
		$spreadsheet->getProperties()->setCreator('NVHCP')
				->setLastModifiedBy('NVHCP')
				->setTitle('NVHCP')
				->setSubject('NVHCP')
				->setDescription('NVHCP');

		// add style to the header
		$styleArray = array(
				'font' => array(
						'bold' => true,
				),
				'alignment' => array(
						'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
						'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
				),
				'borders' => array(
						'top' => array(
								'style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
						),
				),
				'fill' => array(
						'type' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
						'rotation' => 90,
						'startcolor' => array(
								'argb' => 'FFA0A0A0',
						),
						'endcolor' => array(
								'argb' => 'FFFFFFFF',
						),
				),
		);
//echo '<pre>'; print_r($this->session->userdata('reporttoexcel')); exit;
		if($this->session->userdata('reporttoexcel')){

			$alphaRange = range('A', 'Z');
			$totalHeaderData = $this->session->userdata('reporttoexcel')['Header'];
			$letterRange = ($alphaRange[count($totalHeaderData)-1]);
		}
		$spreadsheet->getActiveSheet()->getStyle('A1:'.$letterRange.'1')->applyFromArray($styleArray);


		// auto fit column to content

		foreach(range('A','F') as $columnID) {
			$spreadsheet->getActiveSheet()->getColumnDimension($columnID)
					->setAutoSize(true);
		}
		// set the names of header cells
		if($this->session->userdata('reporttoexcel')){
			$x= 2;
			$alphaRange = range('A', 'Z');
			$totalHeaderData = $this->session->userdata('reporttoexcel')['Header'];
			for($i=0; $i<count($totalHeaderData); $i++){

				$letterRange = ($alphaRange[count($totalHeaderData)]);
				$newAlphaRange = range('A', $letterRange);
				$alphaVal = $newAlphaRange[$i].'1';
				$spreadsheet->setActiveSheetIndex(0)->setCellValue($alphaVal,$totalHeaderData[$i]);
				$x++;
			}
			unset($i);unset($x);
		}

			


		// Add some data
		$alphaRange = range('A', 'Z');
		$x= 2;
		if($this->session->userdata('reporttoexcel')){
			
			$totalMainData = $this->session->userdata('reporttoexcel')['Data'];
			for($i=0; $i<count($totalMainData); $i++){

				$totalData = $this->session->userdata('reporttoexcel')['Data'][$i];
				$letterRange = ($alphaRange[count($totalData)]);
				$newAlphaRange = range('A', $letterRange);
				for($j=0; $j<count($totalData); $j++){
					
					$alphaVal = $newAlphaRange[$j].$x;
					$spreadsheet->setActiveSheetIndex(0)->setCellValue($alphaVal,$totalMainData[$i][$j]);
					
				}
				$x++;
			}
		}
		

	
		



// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Users Information');

// set right to left direction
//		$spreadsheet->getActiveSheet()->setRightToLeft(true);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel2007)
		$fileName = $this->uri->segment(2).'.xlsx';
		ob_end_clean();
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		//header('Content-Disposition: attachment;filename="subscribers_sheet.xlsx"');
		header("Content-Disposition: attachment;filename=$fileName");
		header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Excel2007');
		$writer->save('php://output');
		exit;

		//  create new file and remove Compatibility mode from word title


	}

	}

public function treatment_completed_list($id_mstfacility = null)
	{
		$loginData     = $this->session->userdata('loginData');
		$RequestMethod = $this->input->server('REQUEST_METHOD');
		$start_date    = date("Y-m-d");
		$end_date      = date("Y-m-d", strtotime("+7 days"));

		if($loginData->id_mstfacility == 0)
		{
			if($id_mstfacility == null && $RequestMethod != 'POST')
			{
				$facility_query_add = "";
				$no_filter_limit    = " limit 100";
				$facilityid         = 0;
			}
			if($id_mstfacility != null)
			{
				$facility_query_add = " and f.id_mstfacility =".$id_mstfacility;
				$no_filter_limit    = "";
				$facilityid         = $id_mstfacility;
			}
			if($RequestMethod == "POST")
			{	
				$facilityid = $this->input->post('facility');
				if($facilityid != 0)
				{
					$facility_query_add = " and f.id_mstfacility =".$facilityid;
				}
				else
				{
					$facility_query_add = "";
				}
				$start_date      = timeStamp($this->input->post('startdate'));
				$end_date        = timeStamp($this->input->post('enddate'));
				$no_filter_limit = "";
				$export          = $this->input->post('export_type');
			}

			$sql_get_facilities = "SELECT f.id_mstfacility, CONCAT(d.DistrictName,'-',f.`FacilityType`) as hospital FROM `mstfacility` f 
			inner join mstdistrict d 
			on d.id_mstdistrict = f.id_mstdistrict";

		}
		else if($loginData->id_mstfacility > 0)
		{
			$facility_query_add = " and f.id_mstfacility =".$loginData->id_mstfacility;
			$no_filter_limit    = "";
			$export             = $this->input->post('export_type');
			$facilityid         = $loginData->id_mstfacility;
			$sql_get_facilities = "SELECT f.id_mstfacility, CONCAT(d.DistrictName,'-',f.`FacilityType`) as hospital FROM `mstfacility` f 
			inner join mstdistrict d 
			on d.id_mstdistrict = f.id_mstdistrict and f.id_mstfacility = ".$loginData->id_mstfacility;
		}

		$sql_get_facilities = "SELECT f.id_mstfacility, CONCAT(d.DistrictName,'-',f.`FacilityType`) as hospital FROM `mstfacility` f 
		inner join mstdistrict d 
		on d.id_mstdistrict = f.id_mstdistrict";

		$facilities = $this->Common_Model->query_data($sql_get_facilities);

		$sql_patient_data = "select 
		CONCAT(p.UID_Prefix,'-',p.UID_Num) as UID, 
		p.FirstName, 
		CONCAT(d.DistrictName,'-',f.Name) as hospital, 
		CONCAT(p.Add1) as address,
		p.VillageTown, 
		b.BlockName, 
		p.Mobile, 
		p.Next_Visitdt, 
		p.AdvisedSVRDate, 
		p.EtrDt, 
		p.T_DurationValue, 
		p.SVR12W_Result,
		p.NextVisitPurpose from tblpatient p inner join mstfacility f on p.id_mstfacility = f.id_mstfacility inner join mstdistrict d on f.id_mstdistrict = d.id_mstdistrict left join mstblock b on p.Block = b.id_mstblock where p.NextVisitPurpose = 99".$facility_query_add.$no_filter_limit;

		$content['patient_data'] = $this->Common_Model->query_data($sql_patient_data);

		if(isset($export))
		{
			if($export == "excel")
			{
				if(count($content['patient_data']) > 0)
				{
					$filename = "chai_data_" . date('Ymd') . ".csv";

						  // print_r($content['patient_data']); die();

					header("Content-Disposition: attachment; filename=\"$filename\"");
					header("Content-Type: text/csv");

					$out  = fopen("php://output", 'w');
					$flag = false;
					foreach($content['patient_data'] as $row) {
						  	// print_r($row); die();
						if(!$flag) {
						      // display field/column names as first row
							$firstline = array_map(array($this,'map_colnames'), array_keys((array)$row));
							fputcsv($out, $firstline, ',', '"');
							$flag = true;
						}
						    // array_walk($row, $this->cleanData());
						fputcsv($out, array_values((array)$row), ',', '"');
					}

					fclose($out);
					exit();
				}
			}
		}

		$content['facilityid'] = $facilityid;
		$content['start_date'] = $start_date;
		$content['end_date']   = $end_date;
		$content['facilities'] = $facilities;

		$content['subview'] = 'report_treatment_completed';
		$this->load->view('admin/main_layout', $content);
	}

public function missed_appointment($id_mstfacility = null)
	{
		$loginData     = $this->session->userdata('loginData');
		$RequestMethod = $this->input->server('REQUEST_METHOD');		
		$start_date    = date('Y-m-d',strtotime('-1 week'));
		$end_date      = date("Y-m-d");

		if($id_mstfacility == null && $RequestMethod != 'POST')
		{
			$facility_query_add = "";
				// $no_filter_limit = " limit 100";
			$no_filter_limit    = "";
			$facilityid         = 0;
		}
		if($id_mstfacility != null)
		{
			$facility_query_add = " and f.id_mstfacility =".$id_mstfacility;
			$no_filter_limit    = "";
			$facilityid         = $id_mstfacility;
		}
		if($RequestMethod == "POST")
		{	
			$facilityid = $this->input->post('facility');
			if($facilityid != 0)
			{
				$facility_query_add = " and f.id_mstfacility =".$facilityid;
			}
			else
			{
				$facility_query_add = "";
			}
			$start_date      = timeStamp($this->input->post('startdate'));
			$end_date        = timeStamp($this->input->post('enddate'));
			$no_filter_limit = "";
			$export          = $this->input->post('export_type');
		}
		$sql_get_facilities = "SELECT f.id_mstfacility, CONCAT(d.DistrictName,'-',f.`FacilityType`) as hospital FROM `mstfacility` f 
		inner join mstdistrict d 
		on d.id_mstdistrict = f.id_mstdistrict";


		$facilities       = $this->Common_Model->query_data($sql_get_facilities);
		$sql_patient_data = "select CONCAT(p.UID_Prefix,'-',p.UID_Num) as UID, p.FirstName, CONCAT(d.DistrictName,'-',f.Name) as hospital, CONCAT(p.Add1) as address, p.VillageTown, b.BlockName, p.Mobile, p.Next_Visitdt, p.AdvisedSVRDate, p.EtrDt, p.T_DurationValue, p.NextVisitPurpose from tblpatient p inner join mstfacility f on p.id_mstfacility = f.id_mstfacility inner join mstdistrict d on f.id_mstdistrict = d.id_mstdistrict left join mstblock b on p.Block = b.id_mstblock where (Next_Visitdt > STR_TO_DATE('$start_date', '%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date', '%Y-%m-%d'))".$facility_query_add.$no_filter_limit;

		$content['patient_data'] = $this->Common_Model->query_data($sql_patient_data);

// echo $export; die();
		if(count($content['patient_data']) > 500)
		{
			$export = "excel";
		}
		if(isset($export))
		{
			if($export == "excel")
			{
				if(count($content['patient_data']) > 0)
				{
					$filename = "chai_data_missed_appointment_" . date('Ymd') . ".csv";

						  // print_r($content['patient_data']); die();

					header("Content-Disposition: attachment; filename=\"$filename\"");
					header("Content-Type: text/csv");

					$out  = fopen("php://output", 'w');
					$flag = false;
					foreach($content['patient_data'] as $row) {
						  	// print_r($row); die();
						if(!$flag) {
						      // display field/column names as first row
							$firstline = array_map(array($this,'map_colnames'), array_keys((array)$row));
							fputcsv($out, $firstline, ',', '"');
							$flag = true;
						}
						    // array_walk($row, $this->cleanData());
						fputcsv($out, array_values((array)$row), ',', '"');
					}

					fclose($out);
					exit();
				}
			}
		}

		$content['facilityid'] = $facilityid;
		$content['start_date'] = $start_date;
		$content['end_date']   = $end_date;
		$content['facilities'] = $facilities;


		$content['subview'] = 'report_missed_appointment';
		$this->load->view('admin/main_layout', $content);
	}

public function patients_to_be_followed_up($id_mstfacility = null)
	{	
		$loginData     = $this->session->userdata('loginData');
		$RequestMethod = $this->input->server('REQUEST_METHOD');		
		$start_date    = date("Y-m-d");
		$end_date      = date("Y-m-d", strtotime("+7 days"));

		if($loginData->id_mstfacility == 0)
		{
			if($id_mstfacility == null && $RequestMethod != 'POST')
			{
				$facility_query_add = "";
				$no_filter_limit    = "";
				$facilityid         = 0;
			}
			if($id_mstfacility != null)
			{
				$facility_query_add = " and f.id_mstfacility =".$id_mstfacility;
				$no_filter_limit    = "";
				$facilityid         = $id_mstfacility;
			}
			if($RequestMethod == "POST")
			{
				$facilityid = $this->input->post('facility');
				$start_date = timeStamp($this->input->post('startdate'));
				$end_date   = timeStamp($this->input->post('enddate'));
				if($facilityid != 0)
				{
					$facility_query_add = " and f.id_mstfacility =".$facilityid;
				}
				else
				{
					$facility_query_add = "";
				}
				$no_filter_limit = "";
				$export          = $this->input->post('export_type');

			}

			$sql_get_facilities = "SELECT f.id_mstfacility, CONCAT(d.DistrictName,'-',f.`FacilityType`) as hospital FROM `mstfacility` f 
			inner join mstdistrict d 
			on d.id_mstdistrict = f.id_mstdistrict";

		}
		else if($loginData->id_mstfacility > 0)
		{
			$facility_query_add = " and f.id_mstfacility =".$loginData->id_mstfacility;
			$no_filter_limit    = "";
			$facilityid         = $loginData->id_mstfacility;
			$export             = $this->input->post('export_type');
			$sql_get_facilities = "SELECT f.id_mstfacility, CONCAT(d.DistrictName,'-',f.`FacilityType`) as hospital FROM `mstfacility` f 
			inner join mstdistrict d 
			on d.id_mstdistrict = f.id_mstdistrict and f.id_mstfacility = ".$loginData->id_mstfacility;
		}

		$facilities       = $this->Common_Model->query_data($sql_get_facilities);
		$sql_patient_data = "select 
		CONCAT(p.UID_Prefix,'-',p.UID_Num) as UID, 
		p.FirstName, 
		CONCAT(d.DistrictName,'-',f.Name) as hospital, 
		CONCAT(p.Add1) as address, 
		p.VillageTown, 
		b.BlockName, p.Mobile, p.Next_Visitdt, p.AdvisedSVRDate, p.EtrDt, p.T_DurationValue, p.NextVisitPurpose 
		from tblpatient p 
		inner join mstfacility f 
		on p.id_mstfacility = f.id_mstfacility 
		inner join mstdistrict d 
		on f.id_mstdistrict = d.id_mstdistrict 
		left join mstblock b 
		on p.Block = b.id_mstblock 
		where (Next_Visitdt >= STR_TO_DATE('$start_date', '%Y-%m-%d') 
		and Next_Visitdt < STR_TO_DATE('$end_date', '%Y-%m-%d'))".$facility_query_add.$no_filter_limit;
		$content['patient_data'] = $this->Common_Model->query_data($sql_patient_data);
// echo $sql_patient_data; die();
		if(count($content['patient_data']) > 500)
		{
			$export = "excel";
		}
		if(isset($export))
		{
			if($export == "excel")
			{
				if(count($content['patient_data']) > 0)
				{
					$filename = "chai_data_to_follow_up_" . date('Ymd') . ".csv";

						  // print_r($content['patient_data']); die();

					header("Content-Disposition: attachment; filename=\"$filename\"");
					header("Content-Type: text/csv");

					$out  = fopen("php://output", 'w');
					$flag = false;
					foreach($content['patient_data'] as $row) {
						  	// print_r($row); die();
						if(!$flag) {
						      // display field/column names as first row
							$firstline = array_map(array($this,'map_colnames'), array_keys((array)$row));
							fputcsv($out, $firstline, ',', '"');
							$flag = true;
						}
						    // array_walk($row, $this->cleanData());
						fputcsv($out, array_values((array)$row), ',', '"');
					}

					fclose($out);
					exit();
				}
			}
		}


		$content['facilityid'] = $facilityid;
		$content['start_date'] = $start_date;
		$content['end_date']   = $end_date;
		$content['facilities'] = $facilities;

		$content['subview'] = 'report_follow_up';
		$this->load->view('admin/main_layout', $content);
	}


public function cascade_report()
	{
//error_reporting(E_ALL);
		$loginData = $this->session->userdata('loginData');
		//pr($loginData);

		if($loginData->user_type != 1){
			$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login');	
		}

		 $REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		

		if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('facility');	
			$filters1['startdate']      = timeStamp($this->input->post('startdate'));	
			$filters1['enddate']        = date($this->input->post('enddate'));
			//$filters1['enddate1']        = timeStamp($this->input->post('enddate'));
			if ($filters1['enddate']==date('d-m-Y')) {
				 $filters1['enddate']        = timeStamp(timeStampShow('-1 day',$filters1['enddate']));
			}
			else{
				$filters1['enddate']        = timeStamp($this->input->post('enddate'));
			}
			
		}
		else
		{	
			 $filters1['id_search_state'] = 0;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;	
			$filters1['startdate']      = date('Y-m-01');	
			$filters1['enddate']        = timeStamp(timeStampShow('-1 day',date('Y-m-d')));
			//$filters1['enddate1']        = date('Y-m-d');
			//pr($content['filters1']);exit();
		}

		 $this->session->set_userdata('filters1', $filters1);
	
	$this->load->model('Monthly_model');

	$content['filters1']   = $this->session->userdata('filters1');


	$content['nationaldata'] = $this->Monthly_model->monthly_cascadereport();
	


	$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}

		$content['start_date'] = date('Y-m-d');
		$content['end_date']   = date('Y-m-d');

		 $sql = "select * from mststate where ".$sess_where." order by StateName ASC";
		$content['states'] = $this->db->query($sql)->result();

		 $sql = "select * from mstdistrict where ".$sess_mstdistrict."";
		$content['districts'] = $this->db->query($sql)->result();
		//pr($content['districts']);
		 $sql = "select * from mstfacility where  ".$sess_mstfacility."";
		$content['facilities'] = $this->db->query($sql)->result();	
		$sql = "select flag from tblusers where id_tblusers = ".$loginData->id_tblusers."";
		$content['flag'] = $this->db->query($sql)->result();

		$content['subview'] = 'cascade_report';
		$this->load->view('admin/main_layout', $content);
	}


/*public function state_wise_monthly_indicators(){

		$loginData = $this->session->userdata('loginData');
		//pr($loginData);

		 $REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		

		if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('facility');	
			$filters1['startdate']      = timeStamp($this->input->post('startdate'));	
			$filters1['enddate']        = date($this->input->post('enddate'));
			//$filters1['enddate1']        = timeStamp($this->input->post('enddate'));
			if ($filters1['enddate']==date('d-m-Y')) {
				 $filters1['enddate']        = timeStamp(timeStampShow('-1 day',$filters1['enddate']));
			}
			else{
				$filters1['enddate']        = timeStamp($this->input->post('enddate'));
			}
			
		}
		else
		{	
			 $filters1['id_search_state'] = 0;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;	
			$filters1['startdate']      = date('Y-m-01');	
			$filters1['enddate']        = timeStamp(timeStampShow('-1 day',date('Y-m-d')));
			//$filters1['enddate1']        = date('Y-m-d');
			//pr($content['filters1']);exit();
		}

		 $this->session->set_userdata('filters1', $filters1);
	
	$this->load->model('Monthly_model');

	$content['filters1']   = $this->session->userdata('filters1');
	
	$content['nationaldata'] = $this->Monthly_model->monthly_cascadereport();


	$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}

		$content['start_date'] = date('Y-m-d');
		$content['end_date']   = date('Y-m-d');

		 $sql = "select * from mststate where ".$sess_where." order by StateName ASC";
		$content['states'] = $this->db->query($sql)->result();

		 $sql = "select * from mstdistrict where ".$sess_mstdistrict."";
		$content['districts'] = $this->db->query($sql)->result();
		//pr($content['districts']);
		 $sql = "select * from mstfacility where  ".$sess_mstfacility."";
		$content['facilities'] = $this->db->query($sql)->result();	
		$sql = "select flag from tblusers where id_tblusers = ".$loginData->id_tblusers."";
		$content['flag'] = $this->db->query($sql)->result();

		$content['subview'] = 'state_wise_monthly_indicators';
		$this->load->view('admin/main_layout', $content);	
}*/


public function indicators_cohort_based(){

		$loginData = $this->session->userdata('loginData');
		//pr($loginData);
		if($loginData->user_type != 1){
			$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login');	
		}

		 $REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		

		if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('facility');	
			$filters1['startdate']      = timeStamp($this->input->post('startdate'));	
			$filters1['enddate']        = date($this->input->post('enddate'));
			//$filters1['enddate1']        = timeStamp($this->input->post('enddate'));
			if ($filters1['enddate']==date('d-m-Y')) {
				 $filters1['enddate']        = timeStamp(timeStampShow('-1 day',$filters1['enddate']));
			}
			else{
				$filters1['enddate']        = timeStamp($this->input->post('enddate'));
			}
			
		}
		else
		{	
			 $filters1['id_search_state'] = 0;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;	
			$filters1['startdate']      = date('Y-m-01');	
			$filters1['enddate']        = timeStamp(timeStampShow('-1 day',date('Y-m-d')));
			//$filters1['enddate1']        = date('Y-m-d');
			//pr($content['filters1']);exit();
		}

		 $this->session->set_userdata('filters1', $filters1);
	
	$this->load->model('Monthly_model');

	$content['filters1']   = $this->session->userdata('filters1');
	
	$content['nationaldata_cohort_based'] = $this->Monthly_model->monthly_cascadereport_cohort_based();


	$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}

		$content['start_date'] = date('Y-m-d');
		$content['end_date']   = date('Y-m-d');

		 $sql = "select * from mststate where ".$sess_where." order by StateName ASC";
		$content['states'] = $this->db->query($sql)->result();

		 $sql = "select * from mstdistrict where ".$sess_mstdistrict."";
		$content['districts'] = $this->db->query($sql)->result();
		//pr($content['districts']);
		 $sql = "select * from mstfacility where  ".$sess_mstfacility."";
		$content['facilities'] = $this->db->query($sql)->result();	
		$sql = "select flag from tblusers where id_tblusers = ".$loginData->id_tblusers."";
		$content['flag'] = $this->db->query($sql)->result();

		$content['subview'] = 'state_wise_monthly_indicators';
		$this->load->view('admin/main_layout', $content);	
}




public function late_monthly_report()
{
//error_reporting(E_ALL);
$loginData = $this->session->userdata('loginData');
//pr($loginData);

$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');


if($REQUEST_METHOD == 'POST')
{

$filters1['id_search_state'] = $this->input->post('search_state');
$filters1['id_input_district'] = $this->input->post('input_district');
$filters1['id_mstfacility'] = $this->input->post('facility');
$month                      =$this->input->post('month');
$year = date("Y",strtotime('01-01-'.$this->input->post('year')));
$last_day = date('t', strtotime('01-'.$month.'-'.$year));
$filters1['enddate']        = timeStamp($last_day.'-'.$month.'-'.$year);
$filters1['startdate']   =timeStamp('01-'.$month.'-'.$year);
$filters1['enddate1']        = timeStamp($last_day.'-'.$month.'-'.$year);
}
else
{
$filters1['id_search_state'] = 0;
$filters1['id_input_district'] = 0;
$filters1['id_mstfacility'] = 0;
$filters1['startdate']      = date('Y-m-d',strtotime(date('Y-m-01'). '-1 MONTH'));
 $filters1['enddate']   =  date('Y-m-t',strtotime(date('Y-m-t'). '-1 MONTH'));
$filters1['enddate1']       =date('Y-m-t',strtotime(date('Y-m-t'). '-1 MONTH'));
//pr($content['filters1']);exit();
}

$this->session->set_userdata('filters1', $filters1);

$this->load->model('Monthly_model');

$content['count_1_1'] = $this->Monthly_model->late_report_get_data_1_1(1);

$content['count_2_1'] = $this->Monthly_model->late_report_get_data_2_1(1);
$content['count_2_2'] = $this->Monthly_model->late_report_get_data_2_2(1);
$content['count_2_3'] = $this->Monthly_model->late_report_get_data_2_3(1);

$content['count_3_1'] = $this->Monthly_model->late_report_get_data_3_1(1);
$content['count_3_2'] = $this->Monthly_model->late_report_get_data_3_2(1);
$content['count_3_3'] = $this->Monthly_model->late_report_get_data_3_3(1);
$content['count_3_4'] = $this->Monthly_model->late_report_get_data_3_4(1);
$content['count_3_5'] = $this->Monthly_model->late_report_get_data_3_5(1);
$content['count_3_6'] = $this->Monthly_model->late_report_get_data_3_6(1);
$content['count_3_7'] = $this->Monthly_model->late_report_get_data_3_7(1);

$content['count_4_1'] = $this->Monthly_model->late_report_get_data_4_1(1);

$content['count_4_2'] = $this->Monthly_model->get_data_4_2(1);
$content['count_4_3'] = $this->Monthly_model->get_data_4_3(1);


$content['late_count_1_1'] = $this->Monthly_model->get_data_1_1_summ();

$content['late_count_2_1'] = $this->Monthly_model->get_data_2_1();

$content['Ini_male'] = $this->Monthly_model->IniTreatmentduring_male();
$content['Ini_female'] = $this->Monthly_model->IniTreatmentduring_female();
$content['Ini_children'] = $this->Monthly_model->IniTreatmentduring_children();
$content['Ini_transgender'] = $this->Monthly_model->IniTreatmentduring_transgender();

$content['late_count_2_2'] = $this->Monthly_model->get_data_2_2();
$content['late_count_2_3'] = $this->Monthly_model->get_data_2_3();

//$content['count_3_1'] = $this->Monthly_model->get_data_3_1();

$content['late_count_3_1'] = $this->Monthly_model->get_completed_treatment();


$content['late_count_3_2'] = $this->Monthly_model->get_data_3_2();
$content['late_count_3_3'] = $this->Monthly_model->get_data_3_3();
$content['late_count_3_4'] = $this->Monthly_model->get_data_3_4();
$content['late_count_3_5'] = $this->Monthly_model->get_data_3_5();
$content['late_count_3_6'] = $this->Monthly_model->get_data_3_6();
$content['late_count_3_7'] = $this->Monthly_model->get_data_3_7();

//$content['count_4_1'] = $this->Monthly_model->get_data_4_1();
$content['late_count_4_1'] = $this->Monthly_model->get_eligible_forsvr();

$content['late_count_4_2'] = $this->Monthly_model->get_data_4_2();
$content['late_count_4_3'] = $this->Monthly_model->get_data_4_3();




$content['filters1']   = $this->session->userdata('filters1');


$loginData = $this->session->userdata('loginData');

if( ($loginData) && $loginData->user_type == '1' ){
$sess_where = " 1";
$sess_mstdistrict =" 1";
$sess_mstfacility =" 1";
}
elseif( ($loginData) && $loginData->user_type == '2' ){

$sess_where = " id_mststate = '".$loginData->State_ID."'";

$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

}
elseif( ($loginData) && $loginData->user_type == '3' ){
$sess_where = " id_mststate = '".$loginData->State_ID."'";
$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
}
elseif( ($loginData) && $loginData->user_type == '4' ){
$sess_where = " id_mststate = '".$loginData->State_ID."'";
$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
}

$content['start_date'] = date('Y-m-d');
$content['end_date']   = date('Y-m-d');

$sql = "select * from mststate where ".$sess_where." order by StateName ASC";
$content['states'] = $this->db->query($sql)->result();

$sql = "select * from mstdistrict where ".$sess_mstdistrict."";
$content['districts'] = $this->db->query($sql)->result();
//pr($content['districts']);
$sql = "select * from mstfacility where  ".$sess_mstfacility."";
$content['facilities'] = $this->db->query($sql)->result();
$sql = "select flag from tblusers where id_tblusers = ".$loginData->id_tblusers."";
$content['flag'] = $this->db->query($sql)->result();

$content['subview'] = 'late_entry_monthly_report';
$this->load->view('pages/main_layout', $content);
}






public function progress_hep_b()
{
//error_reporting(E_ALL);
$loginData = $this->session->userdata('loginData');
if($loginData->user_type != 1){
			$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login');	
		}

$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');


if($REQUEST_METHOD == 'POST')
{

$filters1['id_search_state'] = $this->input->post('search_state');
$filters1['id_input_district'] = $this->input->post('input_district');
$filters1['id_mstfacility'] = $this->input->post('facility');
$filters1['startdate']      = timeStamp($this->input->post('startdate'));
$filters1['enddate']        = date($this->input->post('enddate'));
if ($filters1['enddate']==date('d-m-Y')) {
$filters1['enddate']        = timeStamp(timeStampShow('-1 day',$filters1['enddate']));
}
else{
$filters1['enddate']        = timeStamp($this->input->post('enddate'));
}
}
else
{
$filters1['id_search_state'] = 0;
$filters1['id_input_district'] = 0;
$filters1['id_mstfacility'] = 0;
$filters1['startdate']      = date('Y-01-01');
$filters1['enddate']        = timeStamp(timeStampShow('-1 day',date('Y-m-d')));
}

$filterdata = $this->session->set_userdata('filters1', $filters1);



$this->load->model('Monthly_model');

$content['filters1']   = $this->session->userdata('filters1');

if( ($loginData) && $loginData->user_type == '1' ){
$sess_where = " 1";
$sess_mstdistrict =" 1";
$sess_mstfacility =" 1";
}
elseif( ($loginData) && $loginData->user_type == '2' ){

$sess_where = " id_mststate = '".$loginData->State_ID."'";

$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

}
elseif( ($loginData) && $loginData->user_type == '3' ){
$sess_where = " id_mststate = '".$loginData->State_ID."'";
$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
}
elseif( ($loginData) && $loginData->user_type == '4' ){
$sess_where = " id_mststate = '".$loginData->State_ID."'";
$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
}

$content['start_date'] = date('Y-01-01');
$content['end_date']   = date('Y-m-d');

$content['hepatitis_b_screened'] = $this->Monthly_model->hepatitis_b_screened();


$sql = "select * from mststate where ".$sess_where." order by StateName ASC";
$content['states'] = $this->db->query($sql)->result();

$sql = "select * from mstdistrict where ".$sess_mstdistrict."";
$content['districts'] = $this->db->query($sql)->result();

$sql = "select * from mstfacility where  ".$sess_mstfacility."";
$content['facilities'] = $this->db->query($sql)->result();

$content['subview'] = 'progress_report_hep_b';
$this->load->view('pages/main_layout', $content);
}

public function cohort_based_progress_hep_b()
{
//error_reporting(E_ALL);
$loginData = $this->session->userdata('loginData');
if($loginData->user_type != 1){
			$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login');	
		}

$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');


if($REQUEST_METHOD == 'POST')
{

$filters1['id_search_state'] = $this->input->post('search_state');
$filters1['id_input_district'] = $this->input->post('input_district');
$filters1['id_mstfacility'] = $this->input->post('facility');
$filters1['startdate']      = timeStamp($this->input->post('startdate'));
$filters1['enddate']        = date($this->input->post('enddate'));
if ($filters1['enddate']==date('d-m-Y')) {
$filters1['enddate']        = timeStamp(timeStampShow('-1 day',$filters1['enddate']));
}
else{
$filters1['enddate']        = timeStamp($this->input->post('enddate'));
}
}
else
{
$filters1['id_search_state'] = 0;
$filters1['id_input_district'] = 0;
$filters1['id_mstfacility'] = 0;
$filters1['startdate']      = date('Y-01-01');
$filters1['enddate']        = timeStamp(timeStampShow('-1 day',date('Y-m-d')));
}

$filterdata = $this->session->set_userdata('filters1', $filters1);



$this->load->model('Monthly_model');

$content['filters1']   = $this->session->userdata('filters1');

if( ($loginData) && $loginData->user_type == '1' ){
$sess_where = " 1";
$sess_mstdistrict =" 1";
$sess_mstfacility =" 1";
}
elseif( ($loginData) && $loginData->user_type == '2' ){

$sess_where = " id_mststate = '".$loginData->State_ID."'";

$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

}
elseif( ($loginData) && $loginData->user_type == '3' ){
$sess_where = " id_mststate = '".$loginData->State_ID."'";
$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
}
elseif( ($loginData) && $loginData->user_type == '4' ){
$sess_where = " id_mststate = '".$loginData->State_ID."'";
$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
}

$content['start_date'] = date('Y-01-01');
$content['end_date']   = date('Y-m-d');

//$content['hepatitis_b_screened'] = $this->Monthly_model->hepatitis_b_screened_cohort_based();
$content['hepatitis_b_screened'] = $this->Monthly_model->hepatitis_b_screened();


$sql = "select * from mststate where ".$sess_where." order by StateName ASC";
$content['states'] = $this->db->query($sql)->result();

$sql = "select * from mstdistrict where ".$sess_mstdistrict."";
$content['districts'] = $this->db->query($sql)->result();

$sql = "select * from mstfacility where  ".$sess_mstfacility."";
$content['facilities'] = $this->db->query($sql)->result();

$content['subview'] = 'cohort_based_progress_hep_b';
$this->load->view('pages/main_layout', $content);
}
public function regdistribution_report()
{

$loginData = $this->session->userdata('loginData');

$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');


if($REQUEST_METHOD == 'POST')
{

$filters1['id_search_state'] = $this->input->post('search_state');
$filters1['id_input_district'] = $this->input->post('input_district');
$filters1['id_mstfacility'] = $this->input->post('facility');
$filters1['startdate']      = timeStamp($this->input->post('startdate'));
$filters1['enddate']        = date($this->input->post('enddate'));
if ($filters1['enddate']==date('d-m-Y')) {
$filters1['enddate']        = timeStamp(timeStampShow('-1 day',$filters1['enddate']));
}
else{
$filters1['enddate']        = timeStamp($this->input->post('enddate'));
}
}
else
{
$filters1['id_search_state'] = 0;
$filters1['id_input_district'] = 0;
$filters1['id_mstfacility'] = 0;
$filters1['startdate']      = date('Y-01-01');
$filters1['enddate']        = timeStamp(timeStampShow('-1 day',date('Y-m-d')));
}

$filterdata = $this->session->set_userdata('filters1', $filters1);



$this->load->model('Monthly_model');

$content['filters1']   = $this->session->userdata('filters1');

if( ($loginData) && $loginData->user_type == '1' ){
$sess_where = " 1";
$sess_mstdistrict =" 1";
$sess_mstfacility =" 1";
}
elseif( ($loginData) && $loginData->user_type == '2' ){

$sess_where = " id_mststate = '".$loginData->State_ID."'";

$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

}
elseif( ($loginData) && $loginData->user_type == '3' ){
$sess_where = " id_mststate = '".$loginData->State_ID."'";
$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
}
elseif( ($loginData) && $loginData->user_type == '4' ){
$sess_where = " id_mststate = '".$loginData->State_ID."'";
$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
}

$content['start_date'] = date('Y-01-01');
$content['end_date']   = date('Y-m-d');

$content['regdistribution'] = $this->Monthly_model->regdistribution();



$sql = "select * from mststate where ".$sess_where." order by StateName ASC";
$content['states'] = $this->db->query($sql)->result();

$sql = "select * from mstdistrict where ".$sess_mstdistrict."";
$content['districts'] = $this->db->query($sql)->result();

$sql = "select * from mstfacility where  ".$sess_mstfacility."";
$content['facilities'] = $this->db->query($sql)->result();
//pr($content['facilities']);
$content['subview'] = 'regdistribution_report';
$this->load->view('pages/main_layout', $content);
}

/*20-2-2020*/
public function hbv_monthly_report()
	{
//error_reporting(E_ALL);
		$loginData = $this->session->userdata('loginData');
		//pr($loginData);

		 $REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		

		if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('facility');	
			$filters1['startdate']      = timeStamp($this->input->post('startdate'));	
			$filters1['enddate']        = date($this->input->post('enddate'));
			//$filters1['enddate1']        = timeStamp($this->input->post('enddate'));
			if ($filters1['enddate']==date('d-m-Y')) {
				 $filters1['enddate']        = timeStamp(timeStampShow('-1 day',$filters1['enddate']));
			}
			else{
				$filters1['enddate']        = timeStamp($this->input->post('enddate'));
			}
			
		}
		else
		{	
			 $filters1['id_search_state'] = 0;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;	
			$filters1['startdate']      = date('Y-m-01');	
			$filters1['enddate']        = timeStamp(timeStampShow('-1 day',date('Y-m-d')));
			//$filters1['enddate1']        = date('Y-m-d');
			//pr($content['filters1']);exit();
		}

		 $this->session->set_userdata('filters1', $filters1);
	
	$this->load->model('Monthly_model');

	$content['filters1']   = $this->session->userdata('filters1');
	
	//$content['count_1_1'] = $this->Monthly_model->get_data_1_1();

	$content['count_1_1'] = $this->Monthly_model->hbv_get_data_1_1();
	$content['count_1_2'] = $this->Monthly_model->hbv_get_data_1_2();

	$content['count_2_1'] = $this->Monthly_model->hbv_get_data_2_1();
	
	//$content['count_2_2'] = $this->Monthly_model->get_data_2_2();
	//$content['count_2_3'] = $this->Monthly_model->get_data_2_3();

	//$content['count_3_1'] = $this->Monthly_model->get_data_3_1();

	$content['count_3_1'] = $this->Monthly_model->hbv_get_data_3_1();
	$content['count_3_2'] = $this->Monthly_model->hbv_get_data_3_2();
	$content['count_3_3'] = $this->Monthly_model->hbv_get_data_3_3();
	$content['count_3_4'] = $this->Monthly_model->hbv_get_data_3_4();

	$content['count_4_1'] = $this->Monthly_model->hbv_get_data_4_1();
	
	$content['count_4_2'] = $this->Monthly_model->hbv_get_data_4_2();
	$content['count_4_3'] = $this->Monthly_model->hbv_treatment_4_3();
	$content['count_4_5'] = $this->Monthly_model->hbv_get_data_4_5();

	$content['count_5_1'] = $this->Monthly_model->hbv_get_data_5_1();
	$content['count_5_2'] = $this->Monthly_model->hbv_get_data_5_2();
	$content['count_5_3'] = $this->Monthly_model->hbv_get_data_5_3();

	$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}

	$content['start_date'] = date('Y-m-d');
	$content['end_date']   = date('Y-m-d');

		 $sql = "select * from mststate where ".$sess_where." order by StateName ASC";
		$content['states'] = $this->db->query($sql)->result();

		 $sql = "select * from mstdistrict where ".$sess_mstdistrict."";
		$content['districts'] = $this->db->query($sql)->result();
		//pr($content['districts']);
		 $sql = "select * from mstfacility where  ".$sess_mstfacility."";
		$content['facilities'] = $this->db->query($sql)->result();	
		$sql = "select flag from tblusers where id_tblusers = ".$loginData->id_tblusers."";
		$content['flag'] = $this->db->query($sql)->result();

		$content['subview'] = 'hbv_monthly_report';
		$this->load->view('pages/main_layout', $content);
	}


public function hbv_late_monthly_report()
{
//error_reporting(E_ALL);
$loginData = $this->session->userdata('loginData');
//pr($loginData);

$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');


if($REQUEST_METHOD == 'POST')
{

$filters1['id_search_state'] = $this->input->post('search_state');
$filters1['id_input_district'] = $this->input->post('input_district');
$filters1['id_mstfacility'] = $this->input->post('facility');
$month                      =$this->input->post('month');
$year = date("Y",strtotime('01-01-'.$this->input->post('year')));
$last_day = date('t', strtotime('01-'.$month.'-'.$year));
$filters1['enddate']        = timeStamp($last_day.'-'.$month.'-'.$year);
$filters1['startdate']   =timeStamp('01-'.$month.'-'.$year);
$filters1['enddate1']        = timeStamp($last_day.'-'.$month.'-'.$year);
}
else
{
$filters1['id_search_state'] = 0;
$filters1['id_input_district'] = 0;
$filters1['id_mstfacility'] = 0;
$filters1['startdate']      = date('Y-m-d',strtotime(date('Y-m-01'). '-1 MONTH'));
$filters1['enddate']   =  date('Y-m-t',strtotime(date('Y-m-t'). '-1 MONTH'));
$filters1['enddate1']       =date('Y-m-t',strtotime(date('Y-m-t'). '-1 MONTH'));
}

$this->session->set_userdata('filters1', $filters1);

$this->load->model('Monthly_model');

$content['filters1']   = $this->session->userdata('filters1');


$content['count_1_1'] = $this->Monthly_model->hbv_screened_1_1();
$content['count_1_2'] = $this->Monthly_model->hbv_positive_1_2();

$content['count_2_1'] = $this->Monthly_model->hbv_initiated_2_1();


$content['count_3_1'] = $this->Monthly_model->hbv_regimen_taf_3_1();
$content['count_3_2'] = $this->Monthly_model->hbv_regimen_ent_3_2();
$content['count_3_3'] = $this->Monthly_model->hbv_regimen_tdf_3_3();
$content['count_3_4'] = $this->Monthly_model->hbv_regimen_other_3_4();

$content['count_4_1'] = $this->Monthly_model->hbv_treatment_4_1();
$content['count_4_2'] = $this->Monthly_model->hbv_treatment_4_2();
$content['count_4_3'] = $this->Monthly_model->hbv_treatment_4_3(1);
$content['count_4_5'] = $this->Monthly_model->hbv_treatment_4_5();

$content['count_5_1'] = $this->Monthly_model->hbv_test_5_1();
$content['count_5_2'] = $this->Monthly_model->hbv_test_5_2();
$content['count_5_3'] = $this->Monthly_model->hbv_test_5_3();



$content['late_count_1_1'] = $this->Monthly_model->hbv_get_data_1_1();
$content['late_count_1_2'] = $this->Monthly_model->hbv_get_data_1_2();

$content['late_count_2_1'] = $this->Monthly_model->hbv_get_data_2_1();


$content['late_count_3_1'] = $this->Monthly_model->hbv_get_data_3_1();
$content['late_count_3_2'] = $this->Monthly_model->hbv_get_data_3_2();
$content['late_count_3_3'] = $this->Monthly_model->hbv_get_data_3_3();
$content['late_count_3_4'] = $this->Monthly_model->hbv_get_data_3_4();

$content['late_count_4_1'] = $this->Monthly_model->hbv_get_data_4_1();
$content['late_count_4_2'] = $this->Monthly_model->hbv_get_data_4_2();
$content['late_count_4_3'] = $this->Monthly_model->hbv_treatment_4_3();
$content['late_count_4_5'] = $this->Monthly_model->hbv_get_data_4_5();

$content['late_count_5_1'] = $this->Monthly_model->hbv_get_data_5_1();
$content['late_count_5_2'] = $this->Monthly_model->hbv_get_data_5_2();
$content['late_count_5_3'] = $this->Monthly_model->hbv_get_data_5_3();

$loginData = $this->session->userdata('loginData');

if( ($loginData) && $loginData->user_type == '1' ){
$sess_where = " 1";
$sess_mstdistrict =" 1";
$sess_mstfacility =" 1";
}
elseif( ($loginData) && $loginData->user_type == '2' ){

$sess_where = " id_mststate = '".$loginData->State_ID."'";

$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

}
elseif( ($loginData) && $loginData->user_type == '3' ){
$sess_where = " id_mststate = '".$loginData->State_ID."'";
$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
}
elseif( ($loginData) && $loginData->user_type == '4' ){
$sess_where = " id_mststate = '".$loginData->State_ID."'";
$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
}

$content['start_date'] = date('Y-m-d');
$content['end_date']   = date('Y-m-d');

$sql = "select * from mststate where ".$sess_where." order by StateName ASC";
$content['states'] = $this->db->query($sql)->result();

$sql = "select * from mstdistrict where ".$sess_mstdistrict."";
$content['districts'] = $this->db->query($sql)->result();
//pr($content['districts']);
$sql = "select * from mstfacility where  ".$sess_mstfacility."";
$content['facilities'] = $this->db->query($sql)->result();
$sql = "select flag from tblusers where id_tblusers = ".$loginData->id_tblusers."";
$content['flag'] = $this->db->query($sql)->result();

$content['subview'] = 'hbv_late_monthly_report';
$this->load->view('pages/main_layout', $content);
}


}