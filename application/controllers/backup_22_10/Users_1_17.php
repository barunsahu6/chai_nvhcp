<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Common_Model');
		$this->load->library('form_validation');
		$this->load->helper('common');

		$loginData = $this->session->userdata('loginData');
		if($loginData == null)
		{
			redirect('login');
		}

		
	}


	public function index($id_user = null)
	{	
		$this->load->library('form_validation');
		$loginData = $this->session->userdata('loginData'); 
		$content['edit_flag'] = 0;

		$content['salt'] = md5(uniqid(rand(), TRUE));
		$this->session->set_userdata('salt', $content['salt']);
		$salt = $this->session->userdata('salt');
		$RequestMethod = $this->input->server('REQUEST_METHOD');


		

		if($RequestMethod == "POST")
		{

			 $hashed_password = hash('sha256', $this->input->post('password') );

			 $hashed_username = hash('sha256', $this->input->post('username') );

			if($id_user != null)
				{
					$updateArray = array(
						//'username'       	        => $this->input->post('username'),
						'Operator_Name'       	    => $this->input->post('full_name'),
						'password'			 	    => $this->input->post('password'),
						'RoleId'					=> $this->input->post('role_authentication'),
						'Mobile'				 	=> $this->input->post('mobile'),
						'Email'					 	=> $this->input->post('email'),
						'id_mstfacility'	        => $this->input->post('id_mstfacility'),
						'State_ID'	        		=> $this->input->post('input_state'),
						'DistrictID'	       		=> $this->input->post('input_district'),
						'user_type' 			    => $this->input->post('usertype'),
						);

					if(trim($this->input->post('password')) !== "")
					{
						$updateArray['password'] = $hashed_password;
					}
					else
					{
						unset($updateArray['password']);
					}

				/*	if(trim($this->input->post('username')) !== "")
					{
						$updateArray['username'] = $hashed_username;
					}
					else
					{
						unset($updateArray['username']);
					}*/


					$this->Common_Model->update_data('tblusers', $updateArray, 'id_tblusers', $id_user);

					redirect("users");
				}
			else
			{

				$this->form_validation->set_rules('full_name', 'Name', 'trim|required|xss_clean');
				$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[tbludata.tbluname]');
				$this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean');
				$this->form_validation->set_rules('email', 'Email Id', 'trim|required|valid_email|is_unique[tblusers.Email]|xss_clean');
				$this->form_validation->set_rules('usertype', 'User type', 'trim|required|xss_clean');
				$this->form_validation->set_rules('input_state', 'State', 'trim|required|xss_clean');

				if ($this->form_validation->run() == FALSE) {

					$arr['status'] = 'false';
					 $this->session->set_flashdata("tr_msg",validation_errors());
				}else{


				$insertArray = array(
				'username'		 		     => hash('sha256',$this->input->post('username')),
				'Operator_Name'       	     => $this->input->post('full_name'),
				'password' 				     => hash('sha256', $this->input->post('password')),
				'RoleId'					=> $this->input->post('role_authentication'),
				'Mobile'					 =>	$this->input->post('mobile'),
				'Email'						 => $this->input->post('email'),
				'State_ID'	        		=> $this->input->post('input_state'),
				'DistrictID'	       		=> $this->input->post('input_district'),
				'id_mstfacility'	         =>$this->input->post('id_mstfacility'),
				'user_type'				     => $this->input->post('usertype'),
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => $this->input->post('username'),
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);
		}

			}
		}

		if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1 and (user_type=1 || user_type=3)";
				$stateid_where = "where 1";
				$district_where = "where 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND State_ID = '".$loginData->State_ID."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND State_ID = '".$loginData->State_ID."' AND DistrictID ='".$loginData->DistrictID."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				$district_where = "where us.DistrictID = '".$loginData->DistrictID."'";
			}


		if($id_user != null)
		{
			$sql_details = "select * from tblusers where id_tblusers = ".$id_user." ".$sess_where."";
			$res_details = $this->Common_Model->query_data($sql_details)[0];
			$content['user_details'] = $res_details;
			$content['edit_flag'] = 1;
		}

		 $query = "select * from tblusers where 1 ".$sess_where."";
		$content['users_list'] = $this->Common_Model->query_data($query);
		 $sql = "select mst.id_mststate,mst.StateName from mststate mst left join tblusers us on us.State_ID=mst.id_mststate ".$stateid_where." group by id_mststate  order by StateName ";
		$content['states'] = $this->db->query($sql)->result();

		 $sql = "select dis.id_mstdistrict,dis.DistrictName from mstdistrict dis  left join tblusers us on us.DistrictID=dis.id_mstdistrict ".$stateid_where." order by DistrictName";
		$content['mstdistrict'] = $this->db->query($sql)->result();

		$sql = "select * from mstdistrict where id_mststate=".$loginData->State_ID."";
		$content['mstdistrict2'] = $this->db->query($sql)->result();


		$sql_facility = "select * from mstfacility";
		$res = $this->Common_Model->query_data($sql_facility);
		$content['Facility'] = $res;

		$sql_MSTRole = "select * from MSTRole order by Role";
		$content['mst_role'] = $this->Common_Model->query_data($sql_MSTRole);

		

		$content['subview'] = "list_users";
		$this->load->view('admin/main_layout', $content);
	}

	public function delete($id_user = null)
	{
		$sql = "delete from tblusers where id_tblusers = $id_user";

		$this->db->query($sql);

		redirect("users");
	}

	public function getFacilities($id_mstdistrict = null)
	{	
		 $loginData = $this->session->userdata('loginData'); 
		$sql = "select * from mstfacility where id_mstdistrict = ? order by facility_short_name";
		$Facilities = $this->db->query($sql,[$id_mstdistrict])->result();

		$options = "<option value=''>Select Facility</option>";
		foreach ($Facilities as $getdistrict) {
			if($loginData->id_mstfacility==$getdistrict->id_mstfacility){
				$select ='selected';
			}else{
				$select='';
			}
			$options .= "<option value='".$getdistrict->id_mstfacility."' ".$select.">".$getdistrict->facility_short_name."</option>";
		}

		echo $options;
	}

	public function getDistricts($id_mststate = null)
	{	
		 $loginData = $this->session->userdata('loginData'); 
		 $sql = "select * from mstdistrict where id_mststate = ? order by DistrictName";
		$districts = $this->db->query($sql,[$id_mststate])->result();
		$options = "<option value=''>Select District</option>";
		foreach ($districts as $district) {
			if($loginData->DistrictID==$district->id_mstdistrict){
				$select ='selected';
		}else{
			$select='';
		}


			$options .= "<option value='".$district->id_mstdistrict."' ".$select.">".$district->DistrictName."</option>";
		}
	
		echo $options;
	}
public function add_user(){

	
// state user
 //$sql_facility = "select * from mststate where LanguageID is null";
	//$ress = $this->Common_Model->query_data($sql_facility);
/*
foreach ($ress as $value) {
	$password= 'password';

	$insertArray = array(
				'username'		 		     => hash('sha256',$value->StateCd.'_'.'ADMIN'),
				'Operator_Name'       	     => $value->StateName,
				'password' 				     => hash('sha256', $password),
				'RoleId'					=> 1,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> $value->id_mststate,
				'user_type'				     => 3,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => $value->StateCd.'_'.'ADMIN',
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);

}*/
}


public function add_facuser(){

	
	
// fac user
  $sql_facility = "select * from mstfacility where id_mststate !=3";
 $ress = $this->Common_Model->query_data($sql_facility);

//exit();
foreach ($ress as $value) {
	/*Data entry operator (DO)*/
	//$username = 'DO'.'@'.$value->FacilityCode;
	//$password= $value->FacilityCode.''.'130';

	/*Medical Officer (MO)*/
	//$username = 'MO'.'@'.$value->FacilityCode;
	//$password= $value->FacilityCode.''.'130';

	/*Laboratory Technician (LT)*/
	//$username = 'LT'.'@'.$value->FacilityCode;
	//$password= $value->FacilityCode.''.'130';

	/*Pharmacist (PH)*/
	$username = 'PH'.'@'.$value->FacilityCode;
	$password= $value->FacilityCode.''.'130';


	/*$insertArray = array(
				'username'		 		     => hash('sha256',$username),
				'Operator_Name'       	     => $value->facility_short_name,
				'password' 				     => hash('sha256', $password),
				'RoleId'					=> 7,
				'id_mstfacility'			=> $value->id_mstfacility,
				'DistrictID'				=> $value->id_mstdistrict,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> $value->id_mststate,
				'user_type'				     => 2,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => $username,
				'password' 				     => $password,
				'Operator_Name'       	     => $value->facility_short_name,
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);*/

}
}


public function add_fuser(){

	 $sql_facility = "select * from mstdistrict where id_mststate!=3";
	$ress = $this->Common_Model->query_data($sql_facility);
	
	
// state user


/*foreach ($ress as $value) {
	$password= 'password';
	 $usernamev = substr($value->DistrictName,0,3);

	$insertArray = array(
				'FacilityCode'       	     => $usernamev.'-'.'DH',
				'FacilityType' 				=> 'DH',
				'id_mststate'				=> $value->id_mststate,
				'id_mstdistrict'			=> $value->id_mstdistrict,
				'Name'						=> 'District Hospital',
				'facility_name'				=> $value->DistrictName.'-'.'District Hospital',
				'facility_short_name'		=> $value->DistrictName.'-'.'DH'
				);

			$this->Common_Model->insert_data('mstfacility', $insertArray);

			 

}*/
}


public function add_doctor($id_user = null){




		$RequestMethod = $this->input->server('REQUEST_METHOD');
		$loginData = $this->session->userdata('loginData');
		
		 $max_dr = $this->Common_Model->maxadvancepurchase_id();

		if($RequestMethod == "POST")
		{

			
			if($id_user != null)
				{
					$updateArray = array(
						'name'       	    		=> $this->input->post('full_name'),
						);

				
					$this->Common_Model->update_data('mst_medical_specialists', $updateArray, 'ai_id_mst_medical_specialists', $id_user);

					redirect("users/add_doctor");
				}else{

					$insertArray = array(
						'name'       	    		=> $this->input->post('full_name'),
						'id_mst_medical_specialists'=>$max_dr,						
						'id_mstfacility'	        => $loginData->id_mstfacility,
						'Session_StateID'	        => $loginData->State_ID
						);

				
					$this->Common_Model->insert_data('mst_medical_specialists', $insertArray);

					redirect("users/add_doctor");

				}
			
		}

		
		 $query = "select * from mst_medical_specialists where Session_StateID=".$loginData->State_ID."";
		$content['users_list'] = $this->Common_Model->query_data($query);
		
		if($id_user != null)
		{
			 $sql_details = "select * from mst_medical_specialists where ai_id_mst_medical_specialists = ".$id_user."";
			$res_details = $this->Common_Model->query_data($sql_details)[0];
			$content['user_details'] = $res_details;
			$content['edit_flag'] = 1;
		}
		$sql_facility = "select * from mstfacility";
		$res = $this->Common_Model->query_data($sql_facility);
		$content['Facility'] = $res;

		$sql_MSTRole = "select * from MSTRole order by Role";
		$content['mst_role'] = $this->Common_Model->query_data($sql_MSTRole);

		$content['subview'] = "add_doctor";
		$this->load->view('admin/main_layout', $content);

}

	public function dodelete($id_user = null)
	{
		$sql = "delete from mst_medical_specialists where ai_id_mst_medical_specialists = $id_user";

		$this->db->query($sql);

		redirect("users/add_doctor");
	}

/* Add sample transporter*/
	public function add_designation($id_user = null){




		$RequestMethod = $this->input->server('REQUEST_METHOD');
		$loginData = $this->session->userdata('loginData');
		
		 $max_dr = $this->Common_Model->maxdesc_id();

		if($RequestMethod == "POST")
		{

			
			if($id_user != null)
				{
					$updateArray = array(
						'Designation'       	    		=> $this->input->post('full_name'),
						);

				
					$this->Common_Model->update_data('mst_designation_list', $updateArray, 'ai_designation_listId', $id_user);

					redirect("users/add_designation");
				}else{

					$insertArray = array(
						'Designation'       	    => $this->input->post('full_name'),
						'designation_listId'		=>$max_dr,
						'id_mstfacility'	        => $loginData->id_mstfacility,						
						'Session_StateID'	        => $loginData->State_ID
						);

				
					$this->Common_Model->insert_data('mst_designation_list', $insertArray);

					redirect("users/add_designation");

				}
			
		}

		
		 $query = "select * from mst_designation_list where id_mstfacility=".$loginData->id_mstfacility."";
		$content['users_list'] = $this->Common_Model->query_data($query);
		
		if($id_user != null)
		{
			 $sql_details = "select * from mst_designation_list where ai_designation_listId = ".$id_user."";
			$res_details = $this->Common_Model->query_data($sql_details)[0];
			$content['user_details'] = $res_details;
			$content['edit_flag'] = 1;
		}
		$sql_facility = "select * from mstfacility";
		$res = $this->Common_Model->query_data($sql_facility);
		$content['Facility'] = $res;

		$sql_MSTRole = "select * from MSTRole order by Role";
		$content['mst_role'] = $this->Common_Model->query_data($sql_MSTRole);

		$content['subview'] = "add_designation";
		$this->load->view('admin/main_layout', $content);

}

	public function designationdelete($id_user = null)
	{
		$sql = "delete from mst_designation_list where ai_designation_listId = $id_user";

		$this->db->query($sql);

		redirect("users/add_designation");
	}


public function add_facuserdemo1(){

	
	
// fac user
  $sql_facility = "select * from mstfacility where id_mststate =27";
 $ress = $this->Common_Model->query_data($sql_facility);

//exit();
$i=0;foreach ($ress as $value) { $i++;
	//echo $i;exit;
	/*Data entry operator (DO)*/
	//$username = 'DO'.'@'.$value->FacilityCode;
	//$password= $value->FacilityCode.''.'130';

	/*Medical Officer (MO)*/
	//$username = 'MO'.'@'.$value->FacilityCode;
	//$password= $value->FacilityCode.''.'130';

	/*Laboratory Technician (LT)*/
	//$username = 'LT'.'@'.$value->FacilityCode;
	//$password= $value->FacilityCode.''.'130';

	/*Pharmacist (PH)*/
	//$username = 'PH'.'@'.$value->FacilityCode;
	//$password= $value->FacilityCode.''.'130';


	/*$insertArray = array(
				'username'		 		     => hash('sha256',$username),
				'Operator_Name'       	     => $value->facility_short_name,
				'password' 				     => hash('sha256', $password),
				'RoleId'					=> 7,
				'id_mstfacility'			=> $value->id_mstfacility,
				'DistrictID'				=> $value->id_mstdistrict,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> $value->id_mststate,
				'user_type'				     => 2,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => $username,
				'password' 				     => $password,
				'Operator_Name'       	     => $value->facility_short_name,
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);*/


			//for ($i=0; $i < 21; $i++) { 
			
	/*$username = 'demo_mtc'.$i;
	$password= 'password';


	$insertArray = array(
				'username'		 		     => hash('sha256',$username),
				'Operator_Name'       	     => 'demo'.$i,
				'password' 				     => hash('sha256', $password),
				'RoleId'					=> 1,
				'id_mstfacility'			=> $value->id_mstfacility,
				'DistrictID'				=> $value->id_mstdistrict,
				'UpdatedOn'					=> date('Y-m-d'),
				'State_ID'	        		=> '27',
				'user_type'				     => 2,
				);

			$this->Common_Model->insert_data('tblusers', $insertArray);

			 $insert_id = $this->db->insert_id();

			$insertArray1 = array(
				'tbluid'		 		     => $insert_id,
				'tbluname'		 		     => $username,
				'password' 				     => $password,
				'Operator_Name'       	     => $value->facility_short_name,
				'date'				    	 => date('Y-m-d'),
				);

			$this->Common_Model->insert_data('tbludata', $insertArray1);*/

//}
//19
}
}
public function add_doctorname(){
	// fac user
  $sql_facility = "select * from mstfacility where id_mststate =19";
 $ress = $this->Common_Model->query_data($sql_facility);

//exit();
$i=0;foreach ($ress as $value) { $i++;

$nsertArray1 = array(
				'id_mst_medical_specialists'		 		     => 5+$i,
				'name'		 		    			 => 'Test Dr.',
				'id_mstfacility' 				     => $value->id_mstfacility,
				'Session_StateID'       	     => $value->id_mststate,
				'is_deleted'				    	 => 0,
				);

			$this->Common_Model->insert_data('mst_medical_specialists', $nsertArray1);

}
}

}