<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unlock_hepb extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Common_Model');
		//$this->load->Model('Log4php_model');
		
		$loginData = $this->session->userdata('loginData');
		if($loginData == null)
		{
			redirect('login');
		}
		
	}
	private function getCurrentStatus($patientguid = null, $visit = null)
	{
		$sql = "select T_DurationValue,T_DurationOther from tblpatient where PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();
		
		switch($visit)
		{
			case 2 : switch($patient_data[0]->T_DurationValue)
			{
				case 8 : $status = 19;
				break;
				case 12 : $status = 5;
				break;
				case 16 : $status = 21;
				break;
				case 20 : $status = 25;
				break;
				case 24 : $status = 6;
				break;
			}
			break;
			case 3 : switch($patient_data[0]->T_DurationValue)
			{
				case 12 : $status = 7;
				break;
				case 16 : $status = 22;
				break;
				case 20 : $status = 26;
				break;
				case 24 : $status = 8;
				break;
			}
			break;
			case 4 : switch($patient_data[0]->T_DurationValue)
			{
				case 16 : $status = 23;
				break;
				case 20 : $status = 27;
				break;
				case 24 : $status = 9;
				break;
			}
			break;
			case 5 : switch($patient_data[0]->T_DurationValue)
			{
				case 20 : $status = 28;
				break;
				case 24 : $status = 10;
				break;
			}
			break;
			case 6 : switch($patient_data[0]->T_DurationValue)
			{
				case 24 : $status = 11;
				break;
			}
			break;
			

		}

		/*sss*/

		switch($visit)
		{
			case 2 : switch($patient_data[0]->T_DurationOther)
			{	
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			case 3 : switch($patient_data[0]->T_DurationOther)
			{
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			case 4 : switch($patient_data[0]->T_DurationOther)
			{
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			case 5 : switch($patient_data[0]->T_DurationOther)
			{
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			case 6 : switch($patient_data[0]->T_DurationOther)
			{
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			

		}
		/*end*/


		return $status;
	}

	public function unlock_process_registration($patientguid = null){

		$loginData = $this->session->userdata('loginData');
		$arr = array();
						
						$update_array['HbsAg']                    = NULL;
						$update_array['VLHepB']                   = NULL;
						$update_array['HBSElisa']                 = NULL;
						$update_array['HBSElisaDate']             = NULL;
						$update_array['HepB_ElisaCollectionDate'] = NULL;
						
						$update_array['HBSElisaResult']           = NULL;
						$update_array['HBSElisaPlace']            = NULL;
						$update_array['HBSElisaLabID']            = NULL;
						$update_array['HBSElisaLabOther']         = NULL;
						$update_array['HBSRapid']                 = NULL;
						$update_array['HbsAg']                    = null;
						$update_array['VLHepB']                   = null;
						$update_array['HBSRapid']                 = null;
						$update_array['HBSElisa']                 = null;
						$update_array['HBSOther']                 = null;
						$update_array['HBSRapidDate']             = null;
						$update_array['HBSRapidResult']           = null;
						$update_array['HBSRapidPlace']            = null;
						$update_array['HBSRapidLabID']            = null;
						$update_array['HBSElisaDate']             = null;
						$update_array['HBSElisaResult']           = null;
						$update_array['HBSElisaPlace']            = null;
						$update_array['HBSElisaLabID']            = null;
						$update_array['HBSOtherName']             = null;
						$update_array['HBSOtherDate']             = null;
						$update_array['HBSOtherResult']           = null;
						$update_array['HBSOtherPlace']            = null;
						$update_array['HBSOtherLabID']            = null;
						$update_array['HepB_ElisaCollectionDate'] = NULL;
						$update_array['HBSOther']                 = NULL;
						$update_array['HBSOtherName']             = null;
						$update_array['HBSOtherDate']             = null;
						$update_array['HBSOtherResult']           = null;
						$update_array['HBSOtherPlace']            = null;
						$update_array['HBSOtherLabID']            = null;
						$update_array['HBSLabOther']              = null;
						$update_array['HepB_OtherCollectionDate'] = null;
						
						$this->db->where('PatientGUID', $patientguid);
						$unitid = $this->db->update('tblpatient', $update_array);

						$update_array22         = array(
						"V1_Haemoglobin"      => NULL,
						"V1_Albumin"          => NULL,
						"V1_Bilrubin"         => NULL,
						"V1_INR"              => NULL,
						"ALT"                 => NULL,
						"AST"                 => NULL,
						"AST_ULN"             => NULL,
						"V1_Platelets"        => NULL,
						"Weight"              => NULL,
						"V1_Creatinine"       => NULL,
						"V1_EGFR"             => NULL,
						"V1_Cirrhosis"        => NULL,
						"Cirr_Encephalopathy" => NULL,
						"Cirr_Ascites"        => NULL,
						"Cirr_VaricealBleed"  => NULL,
						"ChildScore"          => NULL,
						"Prescribing_Dt"      => NULL,
						"LastTest_Dt"         =>NULL,
						"APRI_Score"          => NULL,
						"FIB4_FIB4"           => NULL,
						"MF3"                 => NULL,
						"baseline_updated_on" => date('Y-m-d'),
						"NextVisitPurpose"    => NULL,
						"baseline_updated_by" => NULL,
						"special_updated_on"  =>  date('Y-m-d'),
						"special_updated_by"  =>  $loginData->id_tblusers,
						"CirrhosisStatus "    => NULL,
						"Result"              => NULL
						);
					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('hepb_tblpatient', $update_array22);

						$update_array['VLHepB']                    =  NULL;
						$update_array['BVLSampleCollectionDate']   =  NULL;
						$update_array['TreatmentRecommended']      =  NULL;
						$update_array['IsBVLSampleStored']         =  NULL;
						$update_array['BVLStorageTemp']            =  NULL;
						$update_array['BStorage_days_hrs']         =  NULL;
						$update_array['BStorageduration']          =  NULL;
						$update_array['IsBVLSampleTransported']    =  NULL;
						$update_array['BVLTransportTemp']          =  NULL;
						$update_array['BVLTransportDate']          =  NULL;
						$update_array['BVLLabID']                  =  NULL;
						$update_array['BVLReceiverName']           =  NULL;
						$update_array['BVLTransporterName']        =  NULL;
						$update_array['BVLTransporterDesignation'] =  NULL;
						$update_array['BVLReceiverDesignation']    =  NULL;
						$update_array['BVLSCRemarks']              =  NULL;
						$update_array['IsBSampleAccepted']         =  NULL;
						$update_array['T_DLL_01_BVLC_Date']        =  NULL;
						$update_array['T_DLL_01_BVLCount']         =  NULL;
						$update_array['T_DLL_01_BVLC_Result']      =  NULL;
						$update_array['BRejectionReason']          =  NULL;
						$update_array['BVLResultRemarks']          =  NULL;
						$update_array['BVLRecieptDate']            =  NULL;
						$update_array['TreatmentRecommended']      =  NULL;

						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('tblpatient', $update_array);

						$this->db->where('PatientGUID', $patientguid);
						$this->db->delete('tblpatientvlsamplelist');

						$update_arrayhepb          = array(
						"BreastFeeding"            => NULL,
						"ART_Regimen"              => NULL,
						"CKDStage"                 => NULL,
						"Ribavirin"                => NULL,
						"PreviousTreatingHospital" => NULL,
						"PastRegimen"              => NULL,
						"PastRegimen_Other"        => NULL,
						"PreviousDuration"         => NULL,
						"Pregnant"                 => NULL,
						"DeliveryDt"               => NULL,
						"HCVPastTreatment"         => NULL,
						"NWeeksCompleted"          => NULL,
						"HCVPastOutcome"           => NULL,
						"IsReferal"                => NULL,
						"ReferingDoctor"           => NULL,
						"ReferingDoctorOther"      => NULL,
						"ReferTo"                  => NULL,
						"ReferalDate"              => NULL,
						"LastPillDate"             => NULL,
						"LMP"                      => NULL,
						"HCVHistory"               => NULL,
						'MF2'                      => NULL,
						"MF5"                      => NULL,
						"MF4"                      => NULL,
						"SCRemarks"                => NULL
						);
						
						$this->db->where('PatientGUID', $patientguid);
						$unitid = $this->db->update('hepb_tblpatient', $update_arrayhepb);

						$update_array            = array(
						"PrescribingFacility"    => NULL,
						"PrescribingDoctor"      => NULL,
						"Drugs_Added"            => NULL,
						'Drug_Dosage'            => NULL,
						"OtherRegimenStrength"   => NULL,
						"PillsToBeTaken"         => NULL,
						"PrescribingDoctorOther" => NULL,
						"RegimenChangeReason"    => NULL,
						"HCVHistory"             => NULL,
						"PrescriptionPlace"      => NULL,
						"TreatmentUpdatedOn"     => date('Y-m-d'),
						"TreatmentUpdatedBy"     => $loginData->id_tblusers,
						"PrescribingDate"        => NULL,
						"DispensationPlace"      => NULL,
						"MF6"                    => NULL,
						"Status"				 => 1
						);

				$this->db->where('PatientGUID', $patientguid);
				$this->db->update('hepb_tblpatient', $update_array);

				$this->db->where('PatientGUID', $patientguid);
				$this->db->delete('tblpatientdispensationb');
				




		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);
	}



	public function unlock_process_screening($patientguid = null){

		$loginData = $this->session->userdata('loginData');

		$arr = array();
		/*patient_screening*/
		//$update_array['fragment3editreason']         = $this->security->xss_clean($this->input->post('unlockresonval'));
		
						$update_array1        = array(
						"V1_Haemoglobin"      => NULL,
						"V1_Albumin"          => NULL,
						"V1_Bilrubin"         => NULL,
						"V1_INR"              => NULL,
						"ALT"                 => NULL,
						"AST"                 => NULL,
						"AST_ULN"             => NULL,
						"V1_Platelets"        => NULL,
						"Weight"              => NULL,
						"V1_Creatinine"       => NULL,
						"V1_EGFR"             => NULL,
						"V1_Cirrhosis"        => NULL,
						"Cirr_Encephalopathy" => NULL,
						"Cirr_Ascites"        => NULL,
						"Cirr_VaricealBleed"  => NULL,
						"ChildScore"          => NULL,
						"Prescribing_Dt"      => NULL,
						"LastTest_Dt"         =>NULL,
						"APRI_Score"          => NULL,
						"FIB4_FIB4"           => NULL,
						"MF3"                 => NULL,
						"baseline_updated_on" => date('Y-m-d'),
						"NextVisitPurpose"    => NULL,
						"baseline_updated_by" => NULL,
						"special_updated_on"  =>  date('Y-m-d'),
						"special_updated_by"  =>  $loginData->id_tblusers,
						"CirrhosisStatus "    => NULL,
						"Result"              => NULL
						);
					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('hepb_tblpatient', $update_array1);

						$update_array['VLHepB']                    =  NULL;
						$update_array['BVLSampleCollectionDate']   =  NULL;
						$update_array['TreatmentRecommended']      =  NULL;
						$update_array['IsBVLSampleStored']         =  NULL;
						$update_array['BVLStorageTemp']            =  NULL;
						$update_array['BStorage_days_hrs']         =  NULL;
						$update_array['BStorageduration']          =  NULL;
						$update_array['IsBVLSampleTransported']    =  NULL;
						$update_array['BVLTransportTemp']          =  NULL;
						$update_array['BVLTransportDate']          =  NULL;
						$update_array['BVLLabID']                  =  NULL;
						$update_array['BVLReceiverName']           =  NULL;
						$update_array['BVLTransporterName']        =  NULL;
						$update_array['BVLTransporterDesignation'] =  NULL;
						$update_array['BVLReceiverDesignation']    =  NULL;
						$update_array['BVLSCRemarks']              =  NULL;
						$update_array['IsBSampleAccepted']         =  NULL;
						$update_array['T_DLL_01_BVLC_Date']        =  NULL;
						$update_array['T_DLL_01_BVLCount']         =  NULL;
						$update_array['T_DLL_01_BVLC_Result']      =  NULL;
						$update_array['BRejectionReason']          =  NULL;
						$update_array['BVLResultRemarks']          =  NULL;
						$update_array['BVLRecieptDate']            =  NULL;
						$update_array['TreatmentRecommended']      =  NULL;

						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('tblpatient', $update_array);

						$this->db->where('PatientGUID', $patientguid);
						$this->db->delete('tblpatientvlsamplelist');

						$update_arrayhepb          = array(
						"BreastFeeding"            => NULL,
						"ART_Regimen"              => NULL,
						"CKDStage"                 => NULL,
						"Ribavirin"                => NULL,
						"PreviousTreatingHospital" => NULL,
						"PastRegimen"              => NULL,
						"PastRegimen_Other"        => NULL,
						"PreviousDuration"         => NULL,
						"Pregnant"                 => NULL,
						"DeliveryDt"               => NULL,
						"HCVPastTreatment"         => NULL,
						"NWeeksCompleted"          => NULL,
						"HCVPastOutcome"           => NULL,
						"IsReferal"                => NULL,
						"ReferingDoctor"           => NULL,
						"ReferingDoctorOther"      => NULL,
						"ReferTo"                  => NULL,
						"ReferalDate"              => NULL,
						"LastPillDate"             => NULL,
						"LMP"                      => NULL,
						"HCVHistory"               => NULL,
						"MF5"                      => NULL,
						"MF4"                      => NULL,

						"SCRemarks"                => NULL,
						);
						
						$this->db->where('PatientGUID', $patientguid);
						$unitid = $this->db->update('hepb_tblpatient', $update_arrayhepb);

						$update_array            = array(
						"PrescribingFacility"    => NULL,
						"PrescribingDoctor"      => NULL,
						"Drugs_Added"            => NULL,
						'Drug_Dosage'            => NULL,
						"OtherRegimenStrength"   => NULL,
						"PillsToBeTaken"         => NULL,
						"PrescribingDoctorOther" => NULL,
						"RegimenChangeReason"    => NULL,
						"HCVHistory"             => NULL,
						"PrescriptionPlace"      => NULL,
						"TreatmentUpdatedOn"     => date('Y-m-d'),
						"TreatmentUpdatedBy"     => $loginData->id_tblusers,
						"PrescribingDate"        => NULL,
						"DispensationPlace"      => NULL,
						"MF6"                    => NULL,
						"Status"				 => 3
						);

				$this->db->where('PatientGUID', $patientguid);
				$unitid = $this->db->update('hepb_tblpatient', $update_array);

				$this->db->where('PatientGUID', $patientguid);
				$this->db->delete('tblpatientdispensationb');

		//$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);

	}


public function unlock_process_testing($patientguid = null){

		$loginData = $this->session->userdata('loginData');
		$arr = array();
		/*testing*/
					$query = "SELECT Elevated_ALT_Level FROM `hepb_tblpatient` WHERE PatientGUID = ?";
					$patient_ALT_Level = $this->db->query($query,[$patientguid])->row();

					 $patient_ALT_Level->Elevated_ALT_Level;



						$update_array['VLHepB']                    =  NULL;
						$update_array['VLHepB']                    =  NULL;
						$update_array['BVLSampleCollectionDate']   =  NULL;
						$update_array['TreatmentRecommended']      =  NULL;
						$update_array['IsBVLSampleStored']         =  NULL;
						$update_array['BVLStorageTemp']            =  NULL;
						$update_array['BStorage_days_hrs']         =  NULL;
						$update_array['BStorageduration']          =  NULL;
						$update_array['IsBVLSampleTransported']    =  NULL;
						$update_array['BVLTransportTemp']          =  NULL;
						$update_array['BVLTransportDate']          =  NULL;
						$update_array['BVLLabID']                  =  NULL;
						$update_array['BVLReceiverName']           =  NULL;
						$update_array['BVLTransporterName']        =  NULL;
						$update_array['BVLTransporterDesignation'] =  NULL;
						$update_array['BVLReceiverDesignation']    =  NULL;
						$update_array['BVLSCRemarks']              =  NULL;
						$update_array['IsBSampleAccepted']         =  NULL;
						$update_array['T_DLL_01_BVLC_Date']        =  NULL;
						$update_array['T_DLL_01_BVLCount']         =  NULL;
						$update_array['T_DLL_01_BVLC_Result']      =  NULL;
						$update_array['BRejectionReason']          =  NULL;
						$update_array['BVLResultRemarks']          =  NULL;
						$update_array['BVLRecieptDate']            =  NULL;
						$update_array['TreatmentRecommended']      =  NULL;

						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('tblpatient', $update_array);

						$this->db->where('PatientGUID', $patientguid);
						$this->db->delete('tblpatientvlsamplelist');

						$update_arrayhepb          = array(
						"BreastFeeding"            => NULL,
						"ART_Regimen"              => NULL,
						"CKDStage"                 => NULL,
						"Ribavirin"                => NULL,
						"PreviousTreatingHospital" => NULL,
						"PastRegimen"              => NULL,
						"PastRegimen_Other"        => NULL,
						"PreviousDuration"         => NULL,
						"Pregnant"                 => NULL,
						"DeliveryDt"               => NULL,
						"HCVPastTreatment"         => NULL,
						"NWeeksCompleted"          => NULL,
						"HCVPastOutcome"           => NULL,
						"IsReferal"                => NULL,
						"ReferingDoctor"           => NULL,
						"ReferingDoctorOther"      => NULL,
						"ReferTo"                  => NULL,
						"ReferalDate"              => NULL,
						"LastPillDate"             => NULL,
						"LMP"                      => NULL,
						"HCVHistory"               => NULL,
						"MF5"                      => NULL,
						"MF4"                      => NULL,

						"SCRemarks"                => NULL,
						);
						
						$this->db->where('PatientGUID', $patientguid);
						$unitid = $this->db->update('hepb_tblpatient', $update_arrayhepb);

						$update_array            = array(
						"PrescribingFacility"    => NULL,
						"PrescribingDoctor"      => NULL,
						"Drugs_Added"            => NULL,
						'Drug_Dosage'            => NULL,
						"OtherRegimenStrength"   => NULL,
						"PillsToBeTaken"         => NULL,
						"PrescribingDoctorOther" => NULL,
						"RegimenChangeReason"    => NULL,
						"HCVHistory"             => NULL,
						"PrescriptionPlace"      => NULL,
						"TreatmentUpdatedOn"     => date('Y-m-d'),
						"TreatmentUpdatedBy"     => $loginData->id_tblusers,
						"PrescribingDate"        => NULL,
						"DispensationPlace"      => NULL,
						"MF6"                    => NULL,
						"Status"				 => 4
						);

				$this->db->where('PatientGUID', $patientguid);
				$unitid = $this->db->update('hepb_tblpatient', $update_array);

				$this->db->where('PatientGUID', $patientguid);
				$this->db->delete('tblpatientdispensationb');

		//$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);
	}


	public function unlock_process_viral_load($patientguid = null){
		//echo 'eeewqeqw';exit();
		$loginData = $this->session->userdata('loginData');

		$query = "SELECT Status FROM `hepb_tblpatient` WHERE PatientGUID = ?";
					$Status = $this->db->query($query,[$patientguid])->row();

					 $Status->Status;


		$arr = array();


						$update_arrayhepb          = array(
						"BreastFeeding"            => NULL,
						"ART_Regimen"              => NULL,
						"CKDStage"                 => NULL,
						"Ribavirin"                => NULL,
						"PreviousTreatingHospital" => NULL,
						"PastRegimen"              => NULL,
						"PastRegimen_Other"        => NULL,
						"PreviousDuration"         => NULL,
						"Pregnant"                 => NULL,
						"DeliveryDt"               => NULL,
						"HCVPastTreatment"         => NULL,
						"NWeeksCompleted"          => NULL,
						"HCVPastOutcome"           => NULL,
						"IsReferal"                => NULL,
						"ReferingDoctor"           => NULL,
						"ReferingDoctorOther"      => NULL,
						"ReferTo"                  => NULL,
						"ReferalDate"              => NULL,
						"LastPillDate"             => NULL,
						"LMP"                      => NULL,
						"HCVHistory"               => NULL,
						"MF5"                      => NULL,
						"SCRemarks"                => NULL,
						);
						
						$this->db->where('PatientGUID', $patientguid);
						$unitid = $this->db->update('hepb_tblpatient', $update_arrayhepb);

						$update_array            = array(
						"PrescribingFacility"    => NULL,
						"PrescribingDoctor"      => NULL,
						"Drugs_Added"            => NULL,
						'Drug_Dosage'            => NULL,
						"OtherRegimenStrength"   => NULL,
						"PillsToBeTaken"         => NULL,
						"PrescribingDoctorOther" => NULL,
						"RegimenChangeReason"    => NULL,
						"HCVHistory"             => NULL,
						"PrescriptionPlace"      => NULL,
						"TreatmentUpdatedOn"     => date('Y-m-d'),
						"TreatmentUpdatedBy"     => $loginData->id_tblusers,
						"PrescribingDate"        => NULL,
						"DispensationPlace"      => NULL,
						"MF6"                    => NULL,
						"Status"				 =>  4; //$Status->Status
						);

				$this->db->where('PatientGUID', $patientguid);
				$unitid = $this->db->update('hepb_tblpatient', $update_array);

				$this->db->where('PatientGUID', $patientguid);
				$this->db->delete('tblpatientdispensationb');
		

		//$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);

	}

	


public function unlock_process_known_history($patientguid = null){


			$loginData = $this->session->userdata('loginData');
		$arr = array();
	

						$update_arrayhis            = array(
						"PrescribingFacility"    => NULL,
						"PrescribingDoctor"      => NULL,
						"Drugs_Added"            => NULL,
						'Drug_Dosage'            => NULL,
						"OtherRegimenStrength"   => NULL,
						"PillsToBeTaken"         => NULL,
						"PrescribingDoctorOther" => NULL,
						"RegimenChangeReason"    => NULL,
						"HCVHistory"             => NULL,
						"PrescriptionPlace"      => NULL,
						"TreatmentUpdatedOn"     => date('Y-m-d'),
						"TreatmentUpdatedBy"     => NULL,
						"PrescribingDate"        => NULL,
						"DispensationPlace"      => NULL,
						"MF6"                    => NULL
						);

				$this->db->where('PatientGUID', $patientguid);
				$this->db->update('hepb_tblpatient', $update_arrayhis);

				$this->db->where('PatientGUID', $patientguid);
				$unitid = $this->db->delete('tblpatientdispensationb');

		//$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);

}

	public function  unlock_process_prescription($patientguid = null){

		$loginData = $this->session->userdata('loginData');
		$arr = array();
		

						$update_array = array(
						
						"Status"      => 5
						);

				$this->db->where('PatientGUID', $patientguid);
				$unitid = $this->db->update('hepb_tblpatient', $update_array);

				$this->db->where('PatientGUID', $patientguid);
				$unitid = $this->db->delete('tblpatientdispensationb');

		
		//$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);
	}

	public function  unlock_process_dispensation_visit_1($patientguid = null){

		$loginData = $this->session->userdata('loginData');

		$arr = array();


		$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();
		$update_array['fragment3editreason']         = $this->security->xss_clean($this->input->post('unlockresonval'));
		$update_array                            = array(
			/*"T_Initiation"                           => null,
			"AdvisedSVRDate"                         => null,*/
			"Status"                         		=> 3,
		);
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);


		/*$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatient_regimen_drug_data');*/

		/*patient_prescription visit */
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatientvisit');
		
		
		$update_array                            = array(
			/*"Current_Visitdt"                        => null,
			"Next_Visitdt"                           => null,*/
			"NextVisitPurpose"                       => null,
			/*"AdvisedSVRDate"                         => null,*/
		);
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		
		$sql                          = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data                 = $this->db->query($sql,[$patientguid])->result();

		$Current_Visitdt              = $patient_data[0]->Current_Visitdt;
		$patient_data                 = $patient_data[0]->Next_Visitdt;

		$update_array['Next_Visitdt'] = $Current_Visitdt;

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		/*insert visit guid start*/
		$sql = "SELECT PatientVisitGUID FROM `tblpatientvisit` WHERE PatientGUID = ?";
		$patient_visitdata = $this->db->query($sql,[$patientguid])->result();
		foreach ($patient_visitdata as $value) {

			$insert_array = array(
				"DeletedGUID"    => $value->PatientVisitGUID);
			$this->db->insert('tblDeleteVisits', $insert_array);
		}
		/*end visit insert*/


		/*eot*/		
		
		$update_array                            = array(
			"ETR_HCVViralLoad_Dt"                    => null,
			"ETR_PillsLeft"                          => null,
			"Adherence"                              => null,
			/*"AdvisedSVRDate"                         => null,*/
			"ETRComments"                            => null,
			"ETRDoctor"                              =>null,
			"ETRDoctorOther"                         => null,
			"SideEffectValue"                        => null,
			/*"Current_Visitdt"                        => null,*/
			"DrugSideEffect"                         => null,
			"PDispensation"                          => null,
			"NAdherenceReason"                       =>null,
			"NAdherenceReasonOther"                  => null,
			"MF7"                                    =>null,
		);
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		/*SVR*/			
		$update_array                            = array(

			'SVRDrawnDate'                           => null,
			'IsSVRSampleStored'                      => null,
			'SVRStorageTemp'                         => null,
			'IsSVRSampleTransported'                 => null,
			'SVRTransportTemp'                       => null,
			'SVRTransportDate'                       => null,
			'SVR12W_LabID'                           => null,
			'SVRTransporterName'                     => null,
			'SVRTransporterDesignation'              => null,
			'SVRTransportRemark'                     => null,
			'SVRReceiptDate'                         =>null,
			'SVRReceiverName'                        => null,
			'SVRReceiverDesignation'                 => null,
			'IsSVRSampleAccepted'                    => null,
			'SVR12W_HCVViralLoad_Dt'                 => null,
			'SVR12W_HCVViralLoadCount'               => null,
			'SVR12W_Result'                          => null,
			'SVRDoctor'                              => null,
			'SVRRejectionReason'                     => null,
			'SVRRejectionReasonOther'                => null,
			'SVRDoctorOther'                         => null,
			'SVRComments'                            => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$unitid = $this->db->update('tblpatient', $update_array);

		//$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);

	}

	public function unlock_process_dispensation_visit_1_6($patientguid = null,$visit){

		$loginData = $this->session->userdata('loginData');
		$arr = array();
			//echo $visit;exit();
		$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();
		$update_array['fragment3editreason']         = $this->security->xss_clean($this->input->post('unlockresonval'));

		$update_array      = array(
			"Status"           => $this->getCurrentStatus($patientguid, $visit),
			"NextVisitPurpose" => 98,
		);
							//print_r($update_array);exit();
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		
		$sql = "SELECT PatientVisitGUID FROM `tblpatientvisit` WHERE PatientGUID = ? and VisitNo > ".$visit." ";
		$patient_visitdata = $this->db->query($sql,[$patientguid])->result();
		foreach ($patient_visitdata as $value) {

			$insert_array = array(
				"DeletedGUID"    => $value->PatientVisitGUID);
			$this->db->insert('tblDeleteVisits', $insert_array);
		}


		/*$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatient_regimen_drug_data');*/

		/*patient_prescription visit */
		
		/*$this->db->where('PatientGUID', $patientguid);
		$this->db->delete('tblpatientvisit');*/
		$this->db->delete('tblpatientvisit',array('PatientGUID'=>$patientguid,'VisitNo >'=>$visit));
		
		$update_array      = array(
			/*"Current_Visitdt"  => null,
			"Next_Visitdt"     => null,
			"AdvisedSVRDate"   => null,*/
			"NextVisitPurpose" => 98,
			/*"Next_Visitdt"     => null,*/
		);
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		$sql                          = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data                 = $this->db->query($sql,[$patientguid])->result();


		$Current_Visitdt              = $patient_data[0]->Current_Visitdt;
		$patient_data                 = $patient_data[0]->Next_Visitdt;

		$update_array['Next_Visitdt'] = $Current_Visitdt;

		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		
		/*eot*/		
		
		$update_array                            = array(
			"ETR_HCVViralLoad_Dt"                    => null,
			"ETR_PillsLeft"                          => null,
			"Adherence"                              => null,
			"AdvisedSVRDate"                         => null,
			"ETRComments"                            => null,
			"ETRDoctor"                              =>null,
			"ETRDoctorOther"                         => null,
			"SideEffectValue"                        => null,
			/*"Current_Visitdt"                        => null,*/
			/*"Next_Visitdt"                           => null,*/
			"DrugSideEffect"                         => null,
			"PDispensation"                          => null,
			"NAdherenceReason"                       =>null,
			"NAdherenceReasonOther"                  => null,

		);
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);
		/*SVR*/			
		$update_array                            = array(

			'SVRDrawnDate'                           => null,
			'IsSVRSampleStored'                      => null,
			'SVRStorageTemp'                         => null,
			'IsSVRSampleTransported'                 => null,
			'SVRTransportTemp'                       => null,
			'SVRTransportDate'                       => null,
			'SVR12W_LabID'                           => null,
			'SVRTransporterName'                     => null,
			'SVRTransporterDesignation'              => null,
			'SVRTransportRemark'                     => null,
			'SVRReceiptDate'                         =>null,
			'SVRReceiverName'                        => null,
			'SVRReceiverDesignation'                 => null,
			'IsSVRSampleAccepted'                    => null,
			'SVR12W_HCVViralLoad_Dt'                 => null,
			'SVR12W_HCVViralLoadCount'               => null,
			'SVR12W_Result'                          => null,
			'SVRDoctor'                              => null,
			'SVRRejectionReason'                     => null,
			'SVRRejectionReasonOther'                => null,
			'SVRDoctorOther'                         => null,
			'SVRComments'                            => null,
		);

		$this->db->where('PatientGUID', $patientguid);
		$unitid = $this->db->update('tblpatient', $update_array);

		//$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);
	}

	public function unlock_process_dispensation_eot($patientguid = null){

		$loginData = $this->session->userdata('loginData');
		$arr = array();
			//echo $visit;exit();
		$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();


		$Current_Visitdt = $patient_data[0]->Current_Visitdt;
		$patient_data = $patient_data[0]->Next_Visitdt;
		$update_array['fragment3editreason']         = $this->security->xss_clean($this->input->post('unlockresonval'));
		
		/*eot*/		
		
		/*$update_array           = array(
		"ETR_HCVViralLoad_Dt"   => null,
		"ETR_PillsLeft"         => null,
		"Adherence"             => null,
		"AdvisedSVRDate"        => null,
		"ETRComments"           => null,
		"ETRDoctor"             =>null,
		"ETRDoctorOther"        => null,
		"SideEffectValue"       => null,
		//"Current_Visitdt"       => null,
		"Next_Visitdt"          => null,
		"DrugSideEffect"        => null,
		"PDispensation"         => null,
		"NAdherenceReason"      =>null,
		"NAdherenceReasonOther" => null,
		"MF7"                   =>null,
		"Status"                => 13,
		);
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);*/

		$update_array['Next_Visitdt']        = $Current_Visitdt;
		
		$this->db->where('PatientGUID', $patientguid);
		$this->db->update('tblpatient', $update_array);

		/*SVR*/			
		$update_array                            = array(

			'SVRDrawnDate'                           => null,
			'IsSVRSampleStored'                      => null,
			'SVRStorageTemp'                         => null,
			'IsSVRSampleTransported'                 => null,
			'SVRTransportTemp'                       => null,
			'SVRTransportDate'                       => null,
			'SVR12W_LabID'                           => null,
			'SVRTransporterName'                     => null,
			'SVRTransporterDesignation'              => null,
			'SVRTransportRemark'                     => null,
			'SVRReceiptDate'                         =>null,
			'SVRReceiverName'                        => null,
			'SVRReceiverDesignation'                 => null,
			'IsSVRSampleAccepted'                    => null,
			'SVR12W_HCVViralLoad_Dt'                 => null,
			'SVR12W_HCVViralLoadCount'               => null,
			'SVR12W_Result'                          => null,
			'SVRDoctor'                              => null,
			'SVRRejectionReason'                     => null,
			'SVRRejectionReasonOther'                => null,
			'SVRDoctorOther'                         => null,
			'SVRComments'                            => null,
			"Status"                => 13,
		);

		$this->db->where('PatientGUID', $patientguid);
		$unitid = $this->db->update('tblpatient', $update_array);

		//$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$patientguid,'Update','Patientinfo Successfully Unlocked');


		if ($unitid > 0) {
			$arr['status'] = 'true';
			$arr['message'] = '';
		}else{

			$arr['status'] = 'false';
			$arr['message'] = 'Please fill out all the required fields';
		}
		echo json_encode($arr);
	}
}	