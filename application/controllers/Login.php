<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->Model('Common_Model');
		$this->load->helper('captcha');
		$this->load->Model('Log4php_model');
		$this->load->library('form_validation');

		set_time_limit(100000);
		ini_set('memory_limit', '100000M');
		ini_set('upload_max_filesize', '300000M');
		ini_set('post_max_size', '200000M');
		ini_set('max_input_time', 100000);
		ini_set('max_execution_time', 0);
		ini_set('memory_limit','-1');
      //$this->rand = random_string('numeric', 4);


		$loginData = $this->session->userdata('loginData');
		/*if($loginData != null)
		{
			redirect('dashboard');
		}*/

	}

function convertString ($date) 
    { 
        // convert date and time to seconds 
        $sec = strtotime($date); 
  
        // convert seconds into a specific format 
        $date = date("Y-m-d H:i", $sec); 
  
        // append seconds to the date and time 
        $date = $date . ":00"; 
  
        // print final date and time 
        echo $date; 
    } 
      
    // Driver code 
   

	public function index_1()
	{
	
$oldDate = '1609372800';
$length = strrpos($oldDate," ");
$newDate = explode( "-" , substr($oldDate,$length));
$output = $newDate[2]."/".$newDate[1]."/".$newDate[0];



		$content['salt'] = md5(uniqid(rand(), TRUE));
		$this->session->set_userdata('salt', $content['salt']);
	 $this->load->view('under_construction', $content);	

	}
	public function index()
	{	



	//error_reporting(0);
		$content['salt'] = md5(uniqid(rand(), TRUE));
		$this->session->set_userdata('salt', $content['salt']);
		

		 /*captcha*/
         $config = array(
         	'word'   => rand(10000,99999),
            'img_url' => base_url() . 'image_for_captcha/',
            'img_path' => 'image_for_captcha/',
             'font_path' => base_url() . 'fount/Verdana.ttf',
			 'font_size'     => 16,
			'img_width' => 130,
			'img_height' => 40,
			'word_length'   => 5,
			'expiration' => 7200,
			 'img_id'        => 'Imageid',
        	'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
			 'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
        )
			
        );
        // pr($config);exit();
        $captcha = create_captcha($config);
        //pr($captcha);
        $this->session->unset_userdata('valuecaptchaCode');
        $this->session->set_userdata('valuecaptchaCode', $captcha['word']);
        $content['captchaImg'] = $captcha['image'];
        /*end captcha*/
        $this->load->view('login', $content);
	}


	/**
	 * 6Le6j5cUAAAAAEPUqM3QDl8JIFc2ie2k9pD09PeF
	 * 6Le6j5cUAAAAAPqnaGONpJPJwYDYcWd8CLhSFvKE
	 */
	public function verifylogin()
	{
//print_r($this->input->post());exit();
		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		if ($username == '' || $password == '') {
			$this->session->set_flashdata('er_msg', 'Please enter username and password.');
			$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,null,'Login','Please enter username and password.');
			redirect('login');
		}
		$termsandcondition = $this->security->xss_clean($this->input->post('termsandcondition'));
		if($termsandcondition ==''){
			$this->session->set_flashdata('er_msg', 'Please accept the terms and conditions.');
			$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,null,'Login','Please accept the terms and conditions.');
			redirect('login');
		}
		if($this->security->xss_clean($this->input->post('captcha'))==''){
			$this->session->set_flashdata('er_msg', 'Please enter captcha.');
			$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,null,'Login','Please enter captcha');
			redirect('login');
		}

		 
		$captcha_insert = $this->security->xss_clean($this->input->post('captcha'));
         $contain_sess_captcha = $this->session->userdata('valuecaptchaCode');
           //echo $captcha_insert.'/'. $contain_sess_captcha;exit();
            if ($captcha_insert === $contain_sess_captcha) {
            	
            }else{
            	$this->session->set_flashdata('er_msg', 'Please enter valid captcha');
            	$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,null,'Login','Not valid captcha enter');
			redirect('login');
            }
		// check recaptcha 

		/*$captcha=$this->input->post('g-recaptcha-response');
		
		$data = array(
			'secret' => "6Le6j5cUAAAAAPqnaGONpJPJwYDYcWd8CLhSFvKE",
			'response' => $captcha,
		);

		$verify = curl_init();
		curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
		curl_setopt($verify, CURLOPT_POST, true);
		curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($verify);

		$response = json_decode($response, true);

		if($response['success'] == false)
		{
			$this->session->set_flashdata('er_msg', 'Please check the checkbox');
			redirect('login');
		}*/

		$this->db->where('username',hash('sha256',$username),'Status!=',2 );
		$this->db->where('Status!=',2 );
		$result = $this->db->get('tblusers')->result();
		if (count($result) != 1) {
			$this->session->set_flashdata('er_msg', 'Your username or password was incorrect');
			redirect('login');
		}

		$user_record = $result[0];

		$salt = $this->session->userdata('salt');
		if ($salt == null || trim($salt) == '') {
			$this->log_login($user_record->id_tblusers, "salt not present");
			$this->session->set_flashdata('tr_msg', 'salt not present');
			$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,null,'Login','salt not present');
			redirect('login');
		}

		$hashed_password = hash('sha256', $user_record->password . $salt);

		if ($hashed_password != $password) {
			$this->log_login($user_record->id_tblusers, "incorrect password");
			$this->session->set_flashdata('er_msg', 'Your username or password was incorrect');
			$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,null,'Login','username or password was incorrect');
			redirect('login');
		}

		session_regenerate_id();

		if (!in_array($user_record->user_type, [1,2,3,4,5])) 
		{
			$this->log_login($user_record->id_tblusers, "Your role is not allowed to access date");
			$this->session->set_flashdata('er_msg', 'Your role is not allowed to access date');
			$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,null,'Login','Your role is not allowed to access date');
			redirect('login');
		}

		$this->log_login($user_record->id_tblusers, "login successful");

		$this->session->set_userdata('loginData',$user_record);
		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,null,'Login','login successful');
		if($user_record->flag == 1){
			redirect ('users/changepassword/'.base64_encode($user_record->id_tblusers));
		}	
		
		if($user_record->user_type==2){
			redirect('patientinfo?p=1');
		}
		elseif($user_record->user_type==5){ 
			redirect('linelist');
		}else{
		redirect('dashboard');
	}
	}

	public function logout()
	{
		$this->session->set_flashdata('er_msg', 'Please login again to continue...');
		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,null,'Logout','Logout Successfully');
		$this->session->sess_destroy();
		session_regenerate_id();
		redirect('login');
	}

	public function refresh()
    {
        $config = array(
        	 'word'   => rand(10000,99999),
            'img_url' => base_url() . 'image_for_captcha/',
            'img_path' => 'image_for_captcha/',
			'img_width' => 130,
			'img_height' => 40,
			'word_length'   => 5,
			 'font_path' => base_url() . 'image_for_captcha/fount/Verdana.ttf',
			 'font_size'     => 16,
			'expiration' => 7200,
			 'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
        )
        );
        $captcha = create_captcha($config);
        $this->session->unset_userdata('valuecaptchaCode');
        $this->session->set_userdata('valuecaptchaCode', $captcha['word']);
        echo $captcha['image'];
    }


	private function log_login($id_tblusers, $login_result='')
	{
		$insertArr = array(
			'id_tblusers'  => $id_tblusers,
			'login_at'     => date('Y-m-d H:i:s'),
			'ip'           => $this->input->ip_address(),
			'session'      => session_id(),
			'login_result' => $login_result,
		);
		$this->db->insert('tbl_login_log', $insertArr);
	}
	public function test(){
		
for ($i=56546; $i <=2847033 ; $i++) { 
 $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'; 
   $random_stringvaldata = substr(str_shuffle($str_result),0,40);

/*  $sql = 'SELECT SHG_Code FROM SHG_Profile limit 50000';
$patient_list = $this->db->query($sql)->result(); 
print_r($patient_list);*/
//echo $i;exit();

$sql = "UPDATE Member_profile SET member_guid = ? WHERE member_id = ?";
$this->db->query($sql, array($random_stringvaldata, $i));





}


}


}
