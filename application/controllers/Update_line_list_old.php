<?php 

class Update_line_list_old extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_Model');
		$this->load->model("Update_everything_old");
	}

	public function index()
	{
		die("nothing here, use update_line_list()");
	}

public function update_tat(){

	$this->Update_everything_old->update_tat();
	echo "<br><code>everything done</code>";
}
	public function update_line_list()
	{
		    // $this->db->trans_begin();
				@set_time_limit(300);
		 ini_set('memory_limit', '-1');
		 ini_set('max_execution_time', 0); 
ini_set('memory_limit','2048M');

				$insert_array = array(
					"marker"             => 1,
					"marker_description" => "started updating linelist",
					"start_time"         => date('Y-m-d H:i:s'),
				);
				$this->db->insert("tbl_service_tracker", $insert_array);

				$sql_del_visit_duplicates = "delete from tblpatientvisit where id_tblpatientvisit in (select min from ((SELECT min(tblpatientvisit.id_tblpatientvisit)min FROM tblpatientvisit group by patientguid,visitno having count(visitno)>1)a))";
				$res_del_visit_duplicates = $this->Common_Model->update_data_sql($sql_del_visit_duplicates);

				$sql_fix_facility_id = "update tblpatientvisit v inner join tblpatient p on p.PatientGUID = v.PatientGUID set v.id_mstfacility = p.id_mstfacility";
				$res_fix_facility_id = $this->Common_Model->update_data_sql($sql_fix_facility_id);

				$sql = "select id_mstfacility from mstfacility where id_mstfacility between 1 and 25";
				$res = $this->Common_Model->query_data($sql);
				foreach ($res as $row) {

					$sql_max_date = "SELECT max(gen_on) as max_date FROM `line_list_tracker` where id_mstfacility = ".$row->id_mstfacility;
					$res_max_date = $this->Common_Model->query_data($sql_max_date);
					if($res_max_date[0]->max_date == null)
					{
		        // echo "cannot start incremental linelist build, no previous data in line list and cannot build linelist fromthe beginning in this automated system"; die();     
						$start_date = '2016-06-18';   
					}
					else
					{
						$start_date = $res_max_date[0]->max_date;
					}

					$sql_del = "delete l.* FROM linelist l inner join `tblpatient` p on l.patientguid = p.patientguid where p.UploadedOn >= '".$start_date."' and  p.id_mstfacility = ".$row->id_mstfacility;
					$this->Common_Model->update_data_sql($sql_del);

					$populate_line_list = "INSERT INTO linelist
											SELECT NULL
											    ,
											    p.patientguid,
											    p.id_mstfacility,
											    CONCAT(
											        p.UID_Prefix,
											        '-',
											        LPAD(p.UID_Num, 6, '0')
											    ) AS uid,
											    p.Aadhaar,
											    f.facility_name,
											    p.FirstName,
											    p.age,
											    gender.LookupValue AS gender,
											    risk.LookupValue AS risk,
											    p.FatherHusband,
											    p.Add1,
											    p.VillageTown,
											    AREA.LookupValue AS AREA,
											    b.BlockName,
											    cd.BlockName,
											    d.DistrictName,
											    p.PIN,
											    p.Mobile,
											    CASE p.T_DLL_01_Date WHEN '' THEN NULL ELSE IFNULL(
											        DATE_FORMAT(p.T_DLL_01_Date, '%d/%m/%Y'),
											        NULL
											    )
											END AS sample_coll,
											CASE p.T_DLL_01_Date WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(p.T_DLL_01_Date, '%d/%m/%Y'),
											    NULL
											)
											END AS confirmatory,
											CASE p.T_DLL_01_Date WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(p.T_DLL_01_Date, '%d/%m/%Y'),
											    NULL
											)
											END AS anti_hcv_res_date,
											elisa_res.LookupValue AS elisa_lv,
											CASE p.T_DLL_01_VLC_Date WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(p.T_DLL_01_VLC_Date, '%d/%m/%Y'),
											    NULL
											)
											END AS sample_date,
											'' AS 'Sample Collection center name for VL',
											CASE p.T_DLL_01_VLC_Date WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(p.T_DLL_01_VLC_Date, '%d/%m/%Y'),
											    NULL
											)
											END AS vl,
											p.T_DLL_01_VLCount,
											p.T_DLL_01_VLC_Result,
											p.T_Initiation AS 'Date of Arrival to Hospital',
											medical_specialists.name,
											p.MF1,
											CASE WHEN p.V1_Cirrhosis = 1 THEN 'Cirrhosis' WHEN p.V1_Cirrhosis = 2 THEN 'No-Cirrhosis'
											END,
											CASE c.Clinical_US_Dt WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(c.Clinical_US_Dt, '%d/%m/%Y'),
											    NULL
											)
											END,
											CASE WHEN c.Clinical_US = 1 THEN 'Cirrhosis' WHEN c.Clinical_US = 2 THEN 'No-Cirrhosis'
											END AS clinical_usg_status,
											CASE c.Fibroscan_Dt WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(c.Fibroscan_Dt, '%d/%m/%Y'),
											    NULL
											)
											END,
											c.Fibroscan_LSM,
											CASE WHEN c.Fibroscan_LSM = 1 THEN 'Cirrhosis' WHEN c.Fibroscan_LSM = 2 THEN 'No-Cirrhosis'
											END AS fibroscan_status,
											CASE c.APRI_Dt WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(c.APRI_Dt, '%d/%m/%Y'),
											    NULL
											)
											END,
											c.APRI,
											c.FIB4,
											c.APRI_Score,
											CASE c.Fibroscan_Dt WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(c.Fibroscan_Dt, '%d/%m/%Y'),
											    NULL
											)
											END,
											c.APRI_PC,
											c.FIB4_FIB4,
											p.V1_Albumin,
											p.V1_Bilrubin,
											p.V1_INR,
											p.V1_Haemoglobin AS h1,
											v1.Haemoglobin AS h2,
											v2.Haemoglobin AS h3,
											v3.Haemoglobin AS h4,
											v4.Haemoglobin AS h5,
											v5.Haemoglobin AS h6,
											v6.Haemoglobin AS h7,
											p.V1_Platelets AS p1,
											v1.PlateletCount AS p2,
											v2.PlateletCount AS p3,
											v3.PlateletCount AS p4,
											v4.PlateletCount AS p5,
											v5.PlateletCount AS p6,
											v6.PlateletCount AS p7,
											decomp_cirr.LookupValue AS decomp_cirr_res,
											CASE p.Cirr_TestDate WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(p.Cirr_TestDate, '%d/%m/%Y'),
											    NULL
											)
											END,
											p.Cirr_Encephalopathy,
											p.Cirr_Ascites,
											p.Cirr_VaricealBleed,
											p.ChildScore,
											c.GenotypeTest_Result,
											CASE c.Clinical_US_Dt WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(c.Clinical_US_Dt, '%d/%m/%Y'),
											    NULL
											)
											END,
											CASE p.T_Initiation WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(p.T_Initiation, '%d/%m/%Y'),
											    NULL
											)
											END,
											reg.LookupValue AS regimen,
											CASE WHEN p.T_DurationValue = 99 THEN(
											    CASE p.T_DurationOther WHEN 1 THEN 4 WHEN 2 THEN 8 WHEN 3 THEN 16 WHEN 4 THEN 20
											END
											) ELSE p.T_DurationValue
											END AS duration,
											p.T_NoPillStart,
											CASE p.T_VisitPlan WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(p.T_VisitPlan, '%d/%m/%Y'),
											    NULL
											)
											END AS visit2only,
											CASE v1.Visit_Dt WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(v1.Visit_Dt, '%d/%m/%Y'),
											    NULL
											)
											END AS v1d,
											v1.PillsLeft AS v1p,
											CASE v1.NextVisit_Dt WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(v1.NextVisit_Dt, '%d/%m/%Y'),
											    NULL
											)
											END AS v1dn,
											v1.Adherence AS v1a,
											CASE v2.Visit_Dt WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(v2.Visit_Dt, '%d/%m/%Y'),
											    NULL
											)
											END AS v2d,
											v2.PillsLeft AS v2p,
											CASE v2.NextVisit_Dt WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(v2.NextVisit_Dt, '%d/%m/%Y'),
											    NULL
											)
											END AS v2dn,
											v2.Adherence AS v2a,
											CASE v3.Visit_Dt WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(v3.Visit_Dt, '%d/%m/%Y'),
											    NULL
											)
											END AS v3d,
											v3.PillsLeft AS v3p,
											CASE v3.NextVisit_Dt WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(v3.NextVisit_Dt, '%d/%m/%Y'),
											    NULL
											)
											END AS v3dn,
											v3.Adherence AS v3a,
											CASE v4.Visit_Dt WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(v4.Visit_Dt, '%d/%m/%Y'),
											    NULL
											)
											END AS v4d,
											v4.PillsLeft AS v4p,
											CASE v4.NextVisit_Dt WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(v4.NextVisit_Dt, '%d/%m/%Y'),
											    NULL
											)
											END AS v4dn,
											v4.Adherence AS v4a,
											CASE v5.Visit_Dt WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(v5.Visit_Dt, '%d/%m/%Y'),
											    NULL
											)
											END AS v5d,
											v5.PillsLeft AS v5p,
											CASE v5.NextVisit_Dt WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(v5.NextVisit_Dt, '%d/%m/%Y'),
											    NULL
											)
											END AS v5dn,
											v5.Adherence AS v5a,
											CASE v6.Visit_Dt WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(v6.Visit_Dt, '%d/%m/%Y'),
											    NULL
											)
											END AS v6d,
											v6.PillsLeft AS v6p,
											CASE v6.NextVisit_Dt WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(v6.NextVisit_Dt, '%d/%m/%Y'),
											    NULL
											)
											END AS v6dn,
											v6.Adherence AS v6a,
											CASE p.ETR_HCVViralLoad_Dt WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(
											        p.ETR_HCVViralLoad_Dt,
											        '%d/%m/%Y'
											    ),
											    NULL
											)
											END,
											p.ETR_HCVViralLoadCount,
											p.ETR_Result,
											CASE p.SVR12W_HCVViralLoad_Dt WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(
											        p.SVR12W_HCVViralLoad_Dt,
											        '%d/%m/%Y'
											    ),
											    NULL
											)
											END,
											p.SVR12W_HCVViralLoadCount,
											CASE p.SVR12W_Result WHEN '1' THEN 'Detected' WHEN '2' THEN 'Not Detected' ELSE ''
											END AS 'svr result',
											p.DrugSideEffect,
											'' AS 'drug compliance',
											patient_status.LookupValue AS pstatus,
											treatment_status.LookupValue,
											'' AS 'Reason for Treatment Failure',
											CASE p.T_Initiation WHEN '2' THEN 'New' WHEN '1' THEN 'Old' ELSE 'New'
											END AS card_type,
											p.SVRComments,
											p.PastDuration,
											CASE p.Session_StateID WHEN 1 THEN 'General' WHEN 2 THEN 'Jail' WHEN 3 THEN 'HRG' ELSE ''
											END AS Session_StateID,
											p.Session_DistrictID,
											IFNULL(
											    DATE_FORMAT(p.UploadedOn, '%d/%m/%Y'),
											    DATE_FORMAT(p.CreatedOn, '%d/%m/%Y')
											),
											IFNULL(
											    DATE_FORMAT(lal_svr.SVRDate, '%d/%m/%Y'),
											    NULL
											) AS lal_svr_date,
											CASE lal_svr.SVRResult WHEN 1 THEN 'Detected' WHEN 2 THEN 'Not Detected'
											END AS lal_svr_result,
											lal_svr.SVRVLC AS lal_svr_viral_load,
											p.Mobile,
											CASE p.HCVHistory WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' ELSE NULL
											END,
											CASE p.PreviousUIDNumber WHEN 0 THEN '' ELSE p.PreviousUIDNumber
											END,
											CASE p.HCVPastOutcome WHEN 1 THEN 'SVR Pending' WHEN 2 THEN 'Cured' WHEN 3 THEN 'Not cured' ELSE ''
											END,
											CASE p.OccupationID WHEN 0 THEN NULL ELSE occupation.LookupValue
											END,
											p.FatherHusband,
											CASE p.T_DLL_01_VLCount WHEN 1 THEN 'Yes' ELSE 'No'
											END,
											CASE p.T_DLL_01_VLCount WHEN 1 THEN 'Yes' ELSE 'No'
											END,
											CASE p.MF1 WHEN 1 THEN 'Yes' ELSE 'No'
											END,
											CASE p.MF1 WHEN 1 THEN 'Yes' ELSE 'No'
											END,
											CASE p.T_DLL_01_VLCount WHEN 1 THEN 'Yes' ELSE 'No'
											END,
											CASE p.IsETRDone WHEN 1 THEN 'Yes' ELSE 'No'
											END,
											CASE p.ETRTestDate WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(p.ETRTestDate, '%d/%m/%Y'),
											    NULL
											)
											END,
											'' as call_output,
											CASE cumulative.svr_date WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(cumulative.svr_date, '%d/%m/%Y'),
											    NULL
											)
											END,
											f1.facility_name,
											cumulative.svr_viral_load,
											CASE cumulative.svr_result WHEN 1 THEN 'Treatment Unsuccessful' WHEN 2 THEN 'Treatment Successful' ELSE ''
											END,
											case p.ETRAutoupdate when 1 then 'Updated Automatically' when 0 then 'Updated Manually' else '' end,
											case p.Pregnant when 1 then 'Yes' else 'No' end,
											CASE p.DeliveryDt WHEN '' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(p.DeliveryDt, '%d/%m/%Y'),
											    NULL
											) end,
											c.FIB4_PC,
											c.FIB4_PC,
											c.FIB4_PC,
										    case p.`PatientType` when 1 then 'New' when 2 then 'Experienced' when 3 then 'Transferred' else '' end as PatientType,
										    past_facility.facility_short_name,
										    p.`PastUID`,
										    p.`Weight`,
										    p.`CKDStage`,
										    p.`NWeeksCompleted`,
										    case p.`LastPillDate` when '' then null else date_format(p.`LastPillDate`, '%d/%m/%Y') end,
										    p.`PastRegimen`,
										    p.`PreviousDuration`,
										    case p.`MF1` when 1 then 'In' when 2 then 'Out' else '' end as MF1,
										    case p.`MF1` when 1 then 'In' when 2 then 'Out' else '' end as MF1,
										    p.`MF1`,
										    case p.`MF1` when 1 then 'Yes' when 2 then 'No' else '' end as MF1,
										    p.`MF1`,
										    p.`MF1`,
										    case p.`T_DLL_01_Date` when '' then null else date_format(p.`T_DLL_01_Date`, '%d/%m/%Y') end,
										    p.`MF1`,
										    case p.`T_DLL_01_Date` when '' then null else date_format(p.`T_DLL_01_Date`, '%d/%m/%Y') end,
										    initiatedoctor.name,
										    etrdoctor.name,
										    svrdoctor.name,
										    interrupt_reason.LookupValue,
										    p.`InterruptToStage`,
										    case p.`MF1` when '' then null else date_format(p.`T_DLL_01_Date`, '%d/%m/%Y') end,
										    svr_lfu.LookupValue,
										    p.`MF1`,
										    case p.IsSMSConsent when 1 then 'Yes' else 'No' end as IsSMSConsent,
										    CASE p.AdvisedSVRDate WHEN '' THEN NULL WHEN '1900-01-19' THEN NULL ELSE IFNULL(
											    DATE_FORMAT(p.AdvisedSVRDate, '%d/%m/%Y'),
											    NULL
											)
											END AS AdvisedSVRDate,
											case when (p.MF1 IS NOT NULL AND p.MF1 != 0) then CONCAT(
										        'PB-ART-',
										        diag.FacilityCode,
										        '-D-',
										        lpad(p.MF1, 6, '0')
										    ) else '' end AS diag_uid
											FROM
											    (
											    SELECT
											        *
											    FROM
											        tblpatient
											    WHERE
											       id_mstfacility = ".$row->id_mstfacility." AND uploadedon > '".$start_date."'
											) p
											LEFT JOIN mstfacility f1 ON
											    p.PreviousTreatingHospital = f1.id_mstfacility
											LEFT JOIN(
											    SELECT *
											    FROM
											        tblpatientvisit
											    WHERE
											        VisitNo = 2 AND id_mstfacility = ".$row->id_mstfacility."
											) v1
											ON
											    p.PatientGUID = v1.PatientGUID
											LEFT JOIN(
											    SELECT *
											    FROM
											        tblpatientvisit
											    WHERE
											        VisitNo = 3 AND id_mstfacility = ".$row->id_mstfacility."
											) v2
											ON
											    p.PatientGUID = v2.PatientGUID
											LEFT JOIN(
											    SELECT *
											    FROM
											        tblpatientvisit
											    WHERE
											        VisitNo = 4 AND id_mstfacility = ".$row->id_mstfacility."
											) v3
											ON
											    p.PatientGUID = v3.PatientGUID
											LEFT JOIN(
											    SELECT *
											    FROM
											        tblpatientvisit
											    WHERE
											        VisitNo = 5 AND id_mstfacility = ".$row->id_mstfacility."
											) v4
											ON
											    p.PatientGUID = v4.PatientGUID
											LEFT JOIN(
											    SELECT *
											    FROM
											        tblpatientvisit
											    WHERE
											        VisitNo = 6 AND id_mstfacility = ".$row->id_mstfacility."
											) v5
											ON
											    p.PatientGUID = v5.PatientGUID
											LEFT JOIN(
											    SELECT *
											    FROM
											        tblpatientvisit
											    WHERE
											        VisitNo = 7 AND id_mstfacility = ".$row->id_mstfacility."
											) v6
											ON
											    p.PatientGUID = v6.PatientGUID
											LEFT JOIN mstblock b ON
											    p.Block = b.id_mstblock
											    LEFT JOIN mstblock cd ON
											    p.Block = cd.id_mstblock
											LEFT JOIN mstdistrict d ON
											    p.District = d.id_mstdistrict
											INNER JOIN mstfacility f ON
											    f.id_mstfacility = p.id_mstfacility
											LEFT JOIN(
											    SELECT *
											    FROM
											        mstlookup
											    WHERE
											        flag = 1 AND LanguageID = 1
											) gender
											ON
											    p.gender = gender.LookupCode
											LEFT JOIN(
											    SELECT *
											    FROM
											        mstlookup
											    WHERE
											        flag = 3 AND LanguageID = 1
											) risk
											ON
											    p.riskFactor = risk.LookupCode
											LEFT JOIN(
											    SELECT *
											    FROM
											        mstlookup
											    WHERE
											        flag = 2 AND LanguageID = 1
											) AREA
											ON
											    p.T_Initiation = AREA.LookupCode
											LEFT JOIN(
											    SELECT *
											    FROM
											        mstlookup
											    WHERE
											        flag = 10 AND LanguageID = 1
											) reg
											ON
											    p.t_regimen = reg.LookupCode
											LEFT JOIN(
											    SELECT *
											    FROM
											        mstlookup
											    WHERE
											        flag = 13 AND LanguageID = 1
											) patient_status
											ON
											    p.status = patient_status.LookupCode
											LEFT JOIN(
											    SELECT *
											    FROM
											        mstlookup
											    WHERE
											        flag = 6 AND LanguageID = 1
											) decomp_cirr
											ON
											    p.Result = decomp_cirr.LookupCode
											LEFT JOIN(
											    SELECT *
											    FROM
											        mstlookup
											    WHERE
											        flag = 4 AND LanguageID = 1
											) elisa_res
											ON
											    p.T_AntiHCV01_Result = elisa_res.LookupCode
											LEFT JOIN(
											    SELECT *
											    FROM
											        mstlookup
											    WHERE
											        flag = 11 AND LanguageID = 1
											) treatment_status
											ON
											    p.SVR_TreatmentStatus = treatment_status.LookupCode
											LEFT JOIN tblpatientcirrohosis c ON
											    p.patientguid = c.patientguid
											LEFT JOIN tblpatientSVR lal_svr ON
											    lal_svr.patientguid = p.patientguid
											LEFT JOIN mstlookup occupation ON
											    occupation.LookupCode = p.OccupationID AND occupation.Flag = 22 AND occupation.LanguageID = 1
											LEFT JOIN mstlookup svr_lfu ON
											    svr_lfu.LookupCode = p.LFUreason AND svr_lfu.Flag = 34 AND svr_lfu.LanguageID = 1
											LEFT JOIN mstlookup interrupt_reason ON
											    interrupt_reason.LookupCode = p.InterruptReason AND interrupt_reason.Flag = 32 AND interrupt_reason.LanguageID = 1
											LEFT JOIN cumulative_svr cumulative ON
											    p.patientguid = cumulative.patientguid
											LEFT JOIN mst_medical_specialists medical_specialists ON
											    p.id_mst_medical_specialists = medical_specialists.id_mst_medical_specialists
											LEFT JOIN mst_medical_specialists initiatedoctor ON
											    p.InitiateDoctor = initiatedoctor.id_mst_medical_specialists
											LEFT JOIN mst_medical_specialists etrdoctor ON
											    p.ETRDoctor = etrdoctor.id_mst_medical_specialists
											LEFT JOIN mst_medical_specialists svrdoctor ON
											    p.SVRDoctor = svrdoctor.id_mst_medical_specialists
											  left join mstfacility past_facility on p.PastFacility = past_facility.id_mstfacility
											LEFT JOIN mst_diagnostics_labs diag ON
											    p.ChildScore = diag.id_mst_diagnostics_labs
											WHERE
											    f.id_mstfacility = ".$row->id_mstfacility;

		  		// echo $populate_line_list; die();

										$this->Common_Model->update_data_sql($populate_line_list);

										$insert_array = array(
											"gen"            => 1,
											"id_mstfacility" => $row->id_mstfacility,
											"gen_on"         => date('Y-m-d H:i:s')
										);

										$this->Common_Model->insert_data('line_list_tracker',$insert_array);
									}

									$sql_linelist_svr_lal = "update `linelist` l inner join tblpatientSVR s on l.patientguid = s.PatientGUID set l.lal_svr_date = DATE_FORMAT(s.SVRDate,'%d/%m/%Y'), l.lal_viral_load = s.SVRVLC, l.lal_svr_result = (case s.SVRResult when 1 then 'Detected' when 2 then 'Not Detected' end) where l.lal_svr_date is null";

									$this->Common_Model->update_data_sql($sql_linelist_svr_lal);

									$sql_linelist_cirrhosis_status = "update linelist l 
									inner join tblpatient p 
									on l.patientguid = p.PatientGUID
									set l.CirrhosisNoCirrhosisSTatus = (case p.V1_Cirrhosis when 1 then 'Cirrhosis' when 2 then 'No-Cirrhosis' else '' end)";

									$this->Common_Model->update_data_sql($sql_linelist_cirrhosis_status);

									/*echo "<br><code>line list updated</code>";
									$this->Update_everything_old->update_everything();
									echo "<br><code>everything done</code>";
*/
									$this->db->where_not_in('marker', '3');
									$this->db->order_by('id_tbl_service_tracker', 'DESC');
									$this->db->limit(1);
									$res_tracker = $this->db->get('tbl_service_tracker')->result();
									
									$update_array = array(
										"marker"             => 8,
										"marker_description" => "Sevice finished",
										"end_time"           => date('Y-m-d H:i:s'),
									);
									$this->db->where('id_tbl_service_tracker', $res_tracker[0]->id_tbl_service_tracker);
									$this->db->update("tbl_service_tracker", $update_array);
									
					  // if ($this->db->trans_status() === FALSE)
					  //   {
					  //     // print_r($this->db->error()); die();
					  //           $this->db->trans_rollback();
					  //           $res['error'] = 'Error : Service Failed';
					  //       log_message('error','transaction failed');
					  //   }
					  //   else
					  //   {   
					  //       log_message('info','Service successful');
					  //           $this->db->trans_commit();
					  //           $res = null;
					  //   }
	}	

	public function update_lal_data()
	{
		$this->Update_everything->update_lal_data();
	}

	public function import_eaushadhi()
	{
		$this->Update_everything->import_eaushadhi();
	}

/*  Not in use by bks  */
	public function update_art_line_list()
	{


$insert_art_line_list = "INSERT INTO art_linelist
											SELECT
    b.facility_name as testing_center_name,
    CONCAT(a.`hcv_uid_prefix` ,'-', lpad(a.`hcv_uid_num`, 6, 0)) AS hcv_uid_prefix,
    a.`name`,
    a.`father_husband_name`,
    CASE a.`gender` WHEN 1 THEN 'Male' WHEN 2 THEN 'Female' WHEN 3 THEN 'Transgender' ELSE ''
END AS gender,
a.`age`,
a.`Residential_address`,
d.BlockName,
a.`village_town`,
CASE a.`area` WHEN 1 THEN 'Urban' WHEN 2 THEN 'Rural' ELSE ''
END AS AREA,
a.`pin`,
a.`contact_no`,
CASE a.`art_treatment_line` WHEN 1 THEN '1st Line' WHEN 2 THEN '2nd Line' WHEN 3 THEN '3rd Line' ELSE ''
END AS art_line,
CASE a.`art_treatment_regimen` WHEN 1 THEN 'TLE' WHEN 2 THEN 'ZLN' WHEN 3 THEN 'TL + Atazanavir/Ritonavir' WHEN 4 THEN 'ZL + Lopinavir/Ritonavir' WHEN 5 THEN 'Raltegravir' WHEN 6 THEN 'Darunavir' WHEN 7 THEN 'Others' ELSE ''
END AS art_treatment_regimen,
risk.LookupValue AS risk,
occupation.LookupValue AS occupation,
DATE_FORMAT(
     case a.`screening_test_date` when '0000-00-00 00:00:00' then '' else a.screening_test_date end,
    '%d/%m/%Y'
) AS screening_test_date,
CASE a.`screening_test_result` WHEN 1 THEN 'Positive' WHEN 2 THEN 'Negative' ELSE ''
END AS screening_test_result,
DATE_FORMAT(
     case a.`screening_sample_coll_dt` when '0000-00-00 00:00:00' then '' else a.screening_sample_coll_dt end,
    '%d/%m/%Y'
) AS screening_sample_coll_dt,
CONCAT(
    a.`screening_sample_coll_dt_hh`,
    ' : ',
    a.`screening_sample_coll_dt_mm`,
    ' ',
    a.`screening_sample_coll_dt_am_pm`
) AS screening_sample_coll_time,
e.facility_name as facility_name1,
f.facility_short_name as facility_short_name,
a.`screening_storage_temp`,
DATE_FORMAT(
     case a.`vl_sample_receipt_dt` when '0000-00-00 00:00:00' then '' else a.vl_sample_receipt_dt end,
    '%d/%m/%Y'
) AS vl_sample_receipt_dt,
CONCAT(
    a.`vl_sample_receipt_dt_hh`,
    ' : ',
    a.`vl_sample_receipt_dt_mm`,
    ' ',
    a.`vl_sample_receipt_dt_am_pm`
) AS vl_sample_receipt_time,
a.`vl_sample_received_temp`,
DATE_FORMAT(
     case a.`vl_test_date` when '0000-00-00 00:00:00' then '' else a.vl_test_date end,
    '%d/%m/%Y'
) AS vl_test_date,
g.facility_name as vl_gx_lab_name,
a.`vl_viral_load`,
CASE a.`vl_test_result` WHEN 1 THEN 'Positive' WHEN 2 THEN 'Negative' ELSE ''
END AS vl_test_result,
k.`storage_temp1`,
 DATE_FORMAT(
    case a.`genotype_sample_date` when '0000-00-00 00:00:00' then '' else a.`genotype_sample_date`end,'%d/%m/%Y'
) AS genotype_sample_date,
 DATE_FORMAT(
    case a.`genotype_test_date` when '0000-00-00 00:00:00' then '' else a.`genotype_test_date` end,
    '%d/%m/%Y'
) AS genotype_test_date,
a.`genotype_lab_name`,
a.`genotype_test_result`,
f.facility_short_name as treating_hospital_name,
a.`hcv_uid_num`,
a.`genotype_test_result`,
 DATE_FORMAT(
    case k.`advised_svr_date` when '0000-00-00 00:00:00' then '' else k.`advised_svr_date` end,
    '%d/%m/%Y'
) AS advised_svr_date,
DATE_FORMAT(
    case k.`sample_collection_dt` when '0000-00-00 00:00:00' then '' else k.`sample_collection_dt` end,
    '%d/%m/%Y'
) AS sample_collection_dt,
DATE_FORMAT(
    case k.`sample_shipped_to` when '0000-00-00 00:00:00' then '' else k.`sample_shipped_to` end,
    '%d/%m/%Y'
) AS sample_shipped_to,
b.`Name`,
k.`storage_temp`,
DATE_FORMAT(
    case k.`hcv_test_date` when '0000-00-00 00:00:00' then '' else k.`hcv_test_date` end,
    '%d/%m/%Y'
) AS hcv_test_date,
m.`ShortName`,
k.`hcv_vl`,
 case k.`hcv_test_result` when '1' then 'Detected' WHEN 2 THEN 'Not Detected' else ''
end AS hcv_test_result


FROM
    `tbldiagnostic_labs_form2` a
LEFT JOIN(
    SELECT
        *
    FROM
        `mst_diagnostics_labs`
    WHERE
        FacilityType = 'ART'
) b
ON
    a.testing_center_name = b.id_mst_diagnostics_labs
LEFT JOIN mstdistrict c ON
    a.district = c.id_mstdistrict
LEFT JOIN(
    SELECT
        *
    FROM
        mstblock
    WHERE
        isactive = 1
) d
ON
    a.block = d.id_mstblock
LEFT JOIN(
    SELECT
        *
    FROM
        `mstlookup`
    WHERE
        Flag = 22 AND LanguageID = 1
) occupation
ON
    a.occupation = occupation.LookupCode
LEFT JOIN(
    SELECT
        *
    FROM
        `mstlookup`
    WHERE
        Flag = 3 AND LanguageID = 1
) risk
ON
    a.risk_profile = risk.LookupCode
    LEFT JOIN(
    SELECT
        *
    FROM
        `mst_diagnostics_labs`
    WHERE
        FacilityType = 'Gx'
) e
ON
    a.screening_sample_shipped_to = e.id_mst_diagnostics_labs
    LEFT JOIN(
    SELECT
        *
    FROM
        `mstfacility`
) f
ON
    a.preferred_treatment_hospital = f.id_mstfacility
    LEFT JOIN(
    SELECT
        *
    FROM
        `mst_diagnostics_labs`
    WHERE
        FacilityType = 'Gx'
) g
ON
    a.vl_gx_lab_name = g.id_mst_diagnostics_labs

    LEFT JOIN(
    SELECT
        *
    FROM
        `tblpatient`
    
) h
ON
    a.testing_center_name = h.DiagnosticCentreID and a.hcv_uid_num=h.DiagnosticUID

    LEFT JOIN(
    SELECT
        *
    FROM
        `tbldiagnostic_labs_form4`
    
) k
ON
    h.id_mstfacility = k.treating_hospital_name and h.UID_Num=k.hcv_uid_num
 LEFT JOIN(
    SELECT
        *
    FROM
        `mst_diagnostics_labs`
         WHERE
        FacilityType = 'Gx'
   
) m
ON
    k.gx_lab_name = m.id_mst_diagnostics_labs";


//$this->Common_Model->insert_data('art_linelist',$insert_art_line_list);

    	$this->Common_Model->update_data_sql($insert_art_line_list);

	}
}	