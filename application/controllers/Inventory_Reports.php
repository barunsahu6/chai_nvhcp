<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory_Reports extends CI_Controller {

	public function __construct()
	{	
		parent::__construct();
		$this->load->model('Common_Model');
		$this->load->model('Inventory_Model');
		$this->load->model('Inventory_Report_Model');
		$this->load->model('Inventory_Model');
		$this->load->helper('common');
		//error_reporting(0);
		$loginData = $this->session->userdata('loginData');
		if($loginData == null)
		{
			redirect('login');
		}
		elseif ($loginData->user_type==2) {
			$set_facility=$this->get_child_facilies($loginData->DistrictID,$loginData->id_mstfacility);
		$this->session->set_userdata('set_facility',$set_facility);
		}
	}

	public function index($flag=NULL,$child_report=NULL)
	{	


		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

		$query="SELECT * FROM `mstlookup` WHERE flag='58'";
		$filter['item_mst_look']=$this->db->query($query)->result();
		$query="SELECT * FROM `mstlookup` WHERE flag='94'";
		$content['report_type']=$this->db->query($query)->result();
		//pr($filter['report_type']);
		$loginData = $this->session->userdata('loginData');
		if ($loginData->user_type==2) {
			$set_facility=$this->get_child_facilies($loginData->DistrictID,$loginData->id_mstfacility);
		$this->session->set_userdata('set_facility',$set_facility);
		}
		
		//$content['items']=$this->Inventory_Model->get_items();
		if($REQUEST_METHOD == 'POST')
		{
			//pr($_POST);exit();
			$month                      =$this->input->post('month');
			$year = date("Y",strtotime('01-01-'.$this->input->post('year')));
			$last_day = date('t', strtotime('01-'.($month).'-'.$year));
			 $filters1['startdate']        = timeStamp('01-'.($month).'-'.$year);
			 $filters1['enddate']        = timeStamp($last_day.'-'.($month).'-'.$year);
			 $last_day1 = date('t', strtotime('01-'.($month-1).'-'.$year));
			 $filters1['startdate1']        = timeStamp('01-'.($month-1).'-'.$year);
			 $filters1['enddate1']        = timeStamp($last_day1.'-'.($month).'-'.$year);
			  $last_day3 = date('t', strtotime('01-'.($month).'-'.$year));
			  $filters1['treatment_start_date']        = timeStamp('01-'.($month-3).'-'.$year);
			  $filters1['treatment_end_date']        = timeStamp($last_day3.'-'.($month).'-'.$year);
			$content['Repo_flg']=$flag;
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('mstfacilitylogin');	
			$filters1['child_report'] = $this->input->post('child_report')==0 ? NULL : $this->input->post('child_report');
			$child_report=	$filters1['child_report'];
			$filter['item_type']=$this->input->post('item_type');
		}	
		else
		{

						//pr($loginData);
			$filters1['enddate']   =  date('Y-m-t',strtotime(date('Y-m-t')));
			$filters1['startdate']   =  date('Y-m-01');
			$month                      =date('m');
			$year = date("Y");
			$last_day = date('t', strtotime('01-'.($month-1).'-'.$year));
			 $filters1['startdate1']        = timeStamp('01-'.($month-1).'-'.$year);
			 $filters1['enddate1']        = timeStamp($last_day.'-'.($month-1).'-'.$year);
			$filter['item_type']='';
			$content['Repo_flg']=$flag;
			$filters1['id_search_state'] ='';
			$filters1['id_input_district'] ='';
			$filters1['id_mstfacility'] ='';
			$filters1['child_report'] = $child_report;
			 $last_day3 = date('t', strtotime('01-'.($month).'-'.$year));
			  $filters1['treatment_start_date']        = timeStamp('01-'.($month-3).'-'.$year);
			  $filters1['treatment_end_date']        = timeStamp($last_day3.'-'.($month).'-'.$year);
		}
		
		
		$this->session->set_userdata('inv_repo_filter',$filter);	
		$this->session->set_userdata('filters1',$filters1);
		if(($loginData->user_type==2 && $child_report==NULL) || $filters1['id_mstfacility']!=''){
			
			if($flag==1){
				$content['items']=$this->Inventory_Model->get_items(NULL,1);
				//echo "facility";exit();
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_lab_stock_report(1);
			$content['Inventory_Repo_sum']=$this->Inventory_Report_Model->get_facility_inventory_Report(1);
			
			//pr($content['Inventory_Repo']);
			 
			}
			elseif($flag==2){
				$content['items']=$this->Inventory_Model->get_items(NULL,2);
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_lab_stock_report(2);
				$content['Inventory_Repo_sum']=$this->Inventory_Report_Model->get_facility_inventory_Report(2);
			
			}

			elseif($flag==3){
				$content['items']=$this->Inventory_Model->get_items(NULL,3);
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_lab_stock_report(3);
				$content['Inventory_Repo_sum']=$this->Inventory_Report_Model->get_facility_inventory_Report(3);
			
			}
			elseif($flag==4){
				$content['items']=$this->Inventory_Model->get_items(NULL,4);
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_lab_stock_report(4);
				$content['Inventory_Repo_sum']=$this->Inventory_Report_Model->get_facility_inventory_Report(4);
			
			}
			
		}
		else if(($loginData->user_type==1)&&($filters1['id_mstfacility']=='')){
			if($flag==1){
				$content['items']=$this->Inventory_Model->get_items(NULL,1);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_state_inventory_Report(1);
				$content['Inventory_Repo_sum']=$this->Inventory_Report_Model->get_state_inventory_Report(1,1);
				
				//pr($content['Inventory_Repo_sum']);
			
			}
			elseif($flag==2){
				$content['items']=$this->Inventory_Model->get_items(NULL,2);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_state_inventory_Report(2);
				$content['Inventory_Repo_sum']=$this->Inventory_Report_Model->get_state_inventory_Report(2,1);
			
			}
			elseif($flag==3){
				$content['items']=$this->Inventory_Model->get_items(NULL,3);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_state_inventory_Report(3);
				$content['Inventory_Repo_sum']=$this->Inventory_Report_Model->get_state_inventory_Report(3,1);
			
			}
			elseif($flag==4){
				$content['items']=$this->Inventory_Model->get_items(NULL,4);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_state_inventory_Report(4);
				$content['Inventory_Repo_sum']=$this->Inventory_Report_Model->get_state_inventory_Report(4,1);
			
			}
		}
			else if((($loginData->user_type==3)&&($filters1['id_mstfacility']==''))|| (($loginData->user_type==2)&&($child_report==1)) ){
			if($flag==1){
				$content['items']=$this->Inventory_Model->get_items(NULL,1);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_facility_inventory_Report(1);
				$content['Inventory_Repo_sum']=$this->Inventory_Report_Model->get_state_inventory_Report(1);
			}
			elseif($flag==2){
				$content['items']=$this->Inventory_Model->get_items(NULL,2);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_facility_inventory_Report(2);
				$content['Inventory_Repo_sum']=$this->Inventory_Report_Model->get_state_inventory_Report(2);
			
			}
			elseif($flag==3){
				$content['items']=$this->Inventory_Model->get_items(NULL,3);
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_facility_inventory_Report(3);
				$content['Inventory_Repo_sum']=$this->Inventory_Report_Model->get_state_inventory_Report(3);
			
			}
			elseif($flag==4){
				$content['items']=$this->Inventory_Model->get_items(NULL,4);
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_facility_inventory_Report(4);
				$content['Inventory_Repo_sum']=$this->Inventory_Report_Model->get_state_inventory_Report(4);
			
			}
		}

		$sql = "select * from mststate ORDER BY StateName ASC";
		$content['states'] = $this->db->query($sql)->result();
		 if($flag==1 || $flag==2){ 
		$content['subview'] = 'inventory_lab_stock_report';
	 } elseif($flag==3){
	 	$content['subview'] = 'inventory_lab_stock_report_hbv';
	 } elseif($flag==4){
	 	$content['subview'] = 'inventory_lab_stock_report_hbv_vac';

	 }
		$this->load->view('inventory/main_layout', $content);
	}

public function get_facility_data(){


if($this->input->get()){

$arr = array();
$id_search_state=$this->input->get('id_mststate');
$id_mst_drugs=$this->input->get('id_mst_drugs');
$type=$this->input->get('type');

$data = $this->Inventory_Report_Model->get_facility_inventory_Report($type,$id_search_state,$id_mst_drugs);

if($data){
$arr['status'] = 'true';
$arr['fields'] = json_encode($data);
}else{
$arr['status'] = 'false';
$arr['fields'] = '';
}

echo json_encode($arr);
}

}	
public function get_item_data(){


if($this->input->get()){

$arr = array();
$id_mstfacility=$this->input->get('id_mstfacility');
$id_mst_drugs=$this->input->get('id_mst_drugs');
$type=$this->input->get('type');

$data = $this->Inventory_Report_Model->get_lab_stock_report($type,$id_mstfacility,$id_mst_drugs);

if($data){
$arr['status'] = 'true';
$arr['fields'] = json_encode($data);
}else{
$arr['status'] = 'false';
$arr['fields'] = '';
}

echo json_encode($arr);
}

}	

public function stock_indent_status(){
		$loginData = $this->session->userdata('loginData');
		if( ($loginData) && $loginData->user_type != '2' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
		$this->load->library('form_validation');
		$this->load->model('Inventory_Model');
		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
			$query="SELECT * FROM `mstlookup` WHERE flag='58'";
			$filter['item_mst_look']=$this->db->query($query)->result();
			//pr($filter['item_mst_look']);
		
						
						//echo "<pre>";print_r($loginData);
						
						if( ($loginData) && $loginData->user_type == '1' ){
						$sess_where = " 1";
						}
						elseif( ($loginData) && $loginData->user_type == '2' ){
						
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
						}
						elseif( ($loginData) && $loginData->user_type == '3' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
						}
						elseif( ($loginData) && $loginData->user_type == '4' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
						}
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						$State_ID=$loginData->State_ID;
						$DistrictID=$loginData->DistrictID;
						$id_mstfacility=$loginData->id_mstfacility;
						if($loginData->State_ID==''||$loginData->State_ID==NULL){
						 $State_ID=0;
						}
						if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
						$DistrictID=0;
						}
						if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
						$id_mstfacility=0;
						}
						if ($this->security->xss_clean($this->input->post('search'))=='search') {
						
			$filter['Start_Date'] = timeStamp($this->input->post('startdate'));
			$filter['End_Date'] = timeStamp($this->input->post('enddate'));
			$filter['year'] = $this->security->xss_clean($this->input->post('year'));
			 $filter['item_type']=$this->security->xss_clean($this->input->post('item_type'));
			 $filter['id_mstfacility']=$this->security->xss_clean($this->input->post('id_mstfacility'));
						}
						
					
				}
				
					else
		{
			$filter['Start_Date'] = timeStamp(timeStampShow(date('Y-01-01')));
			$filter['End_Date'] = timeStamp(timeStampShow(date('Y-m-d')));
			$filter['item_type']='';
			$filter['id_mstfacility']='';
			$year = date("Y",strtotime($filter['Start_Date']));
		 $year1 = date("Y",strtotime($filter['End_Date']));
			if (date('m', strtotime(($filter['Start_Date']))) < 4) {
			 $filter['year'] =($year-1)."-".$year1;
			  $content['start_date']=date("01-04-".($year-1));
		}
		else{
			 if ($year==$year1) {
		 	$year1+=1;
		 }
			 $filter['year'] =($year)."-".$year1;
			 $content['start_date']=date("01-04-".$year);
		}
		$filter['Start_Date']=$content['start_date'];
		
		}
		$content['items']=$this->Inventory_Model->get_items();
		
		$this->session->set_userdata('invfilter',$filter);	
	
		
		/*$query="SELECT FacilityCode FROM `mstfacility` WHERE id_mstfacility=?";	
		$content['FacilityCode']=$this->db->query($query,[$loginData->id_mstfacility])->result();*/
		/* $content['get_facilities'] = $this->Inventory_Model->get_all_facilities();*/
		$content['inventory_detail_all'] =$this->Inventory_Model->get_pending_indent();
		$content['indent_details'] =$this->Inventory_Model->get_indent_status();
		$content['indent_all_details'] =$this->Inventory_Model->get_all_indent_data();
		$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['relocation_status']=$this->db->query($query,['80'])->result();
						$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['reason_for_rejection']=$this->db->query($query,['78'])->result();

		$content['indent_items']=$this->Inventory_Model->get_items(NULL,NULL,'I');
		$content['subview']           = "stock_indent_status";
		$this->load->view('inventory/main_layout', $content);
			
	}
public function stock_rejection(){
		$loginData = $this->session->userdata('loginData');
		$this->load->library('form_validation');
		$this->load->model('Inventory_Model');
		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
			$query="SELECT * FROM `mstlookup` WHERE flag='58'";
			$filter['item_mst_look']=$this->db->query($query)->result();
			//pr($filter['item_mst_look']);
		
						
						//echo "<pre>";print_r($loginData);
						
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						
					
						
			$filter['Start_Date'] = timeStamp($this->input->post('startdate'));
			$filter['End_Date'] = timeStamp($this->input->post('enddate'));
			 $filter['item_type']=$this->security->xss_clean($this->input->post('item_type'));
					
						
					
				}
				
					else
		{
			$filter['Start_Date'] = timeStamp(timeStampShow(date('Y-m-01')));
			$filter['End_Date'] = timeStamp(timeStampShow(date('Y-m-d')));
			$filter['item_type']='';
		
		}
		$content['items']=$this->Inventory_Model->get_items();
		
		$this->session->set_userdata('invfilter',$filter);	
		if($loginData->user_type==2){
			
		
			
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_stock_rejected_data_facility();
			
			
		}
		else if(($loginData->user_type==1)){
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_stock_rejected_data_national();
		}
			else if(($loginData->user_type==3)){
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_stock_rejected_data_state();
		}
		$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
		$content['reason_for_rejection']=$this->db->query($query,['78'])->result();
		$content['subview']           = "stock_rejection";
		$this->load->view('inventory/main_layout', $content);
			
	}
	public function get_rejection_data(){


if($this->input->get()){

$arr = array();
$id_search_state=$this->input->get('id_mststate');
$id_mst_drugs=$this->input->get('id_mst_drugs');
$type=$this->input->get('type');

$data = $this->Inventory_Report_Model->get_stock_rejected_data_state($type,$id_search_state,$id_mst_drugs);

if($data){
$arr['status'] = 'true';
$arr['fields'] = json_encode($data);
}else{
$arr['status'] = 'false';
$arr['fields'] = '';
}

echo json_encode($arr);
}

}	
public function get_item_rejection_data(){


if($this->input->get()){

$arr = array();
$id_mstfacility=$this->input->get('id_mstfacility');
$id_mst_drugs=$this->input->get('id_mst_drugs');
$type=$this->input->get('type');

$data = $this->Inventory_Report_Model->get_stock_rejected_data_facility($type,$id_mstfacility,$id_mst_drugs);

if($data){
$arr['status'] = 'true';
$arr['fields'] = json_encode($data);
}else{
$arr['status'] = 'false';
$arr['fields'] = '';
}

echo json_encode($arr);
}

}	
public function stock_expiry_report(){
		$loginData = $this->session->userdata('loginData');
		$this->load->library('form_validation');
		$this->load->model('Inventory_Model');
		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
			$query="SELECT * FROM `mstlookup` WHERE flag='58'";
			$filter['item_mst_look']=$this->db->query($query)->result();
			$query="SELECT * FROM `mstlookup` WHERE flag='84'";
			$content['Expire_filter']=$this->db->query($query)->result();
			//pr($filter['item_mst_look']);
		
						
						//echo "<pre>";print_r($loginData);
						
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						
					
						
			$filter['expiry_filter'] = ($this->input->post('expiry_filter'));
			$filter['End_Date'] = timeStamp(timeStampShow(date('Y-m-d')));
			 $filter['item_type']=$this->security->xss_clean($this->input->post('item_type'));
			 $filter['rec_filter']=$this->security->xss_clean($this->input->post('rec_filter'));
					
						
					
				}
				
					else
		{
			$filter['expiry_filter'] = '';
			$filter['End_Date'] = timeStamp(timeStampShow(date('Y-m-d')));
			$filter['item_type']='';
			$filter['rec_filter']='';
		
		}
		$content['items']=$this->Inventory_Model->get_items();
		
		$this->session->set_userdata('inv_repo_filter',$filter);
		if($loginData->user_type==2){
			
		
			
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_stock_expiry_facility_wise();
			$content['filtered_arr']=$this->Inventory_Report_Model->get_stock_expiry_state();
			//pr($content['Inventory_Repo']);
			
		}
		else if(($loginData->user_type==1)){
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_stock_expiry_national();
			$content['filtered_arr']=$this->Inventory_Report_Model->get_stock_expiry_national_sum();
			//pr($content['Inventory_Repo']);
		}
			else if(($loginData->user_type==3)){
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_stock_expiry_state();

			$content['filtered_arr']=$this->Inventory_Report_Model->get_stock_expiry_national();
			//pr($content['Inventory_Repo']);
		}
		$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
		$content['reason_for_rejection']=$this->db->query($query,['78'])->result();
		$content['subview']           = "expiry_report";
		$this->load->view('inventory/main_layout', $content);
			
	}
	public function get_stock_expiry_data(){


if($this->input->get()){

$arr = array();
$id_search_state=$this->input->get('id_mststate');
$id_mst_drugs=$this->input->get('id_mst_drugs');
$type=$this->input->get('type');

$data = $this->Inventory_Report_Model->get_stock_expiry_state($type,$id_search_state,$id_mst_drugs);

if($data){
$arr['status'] = 'true';
$arr['fields'] = json_encode($data);
}else{
$arr['status'] = 'false';
$arr['fields'] = '';
}

echo json_encode($arr);
}

}	
public function get_item_expiry_report(){


if($this->input->get()){

$arr = array();
$id_mstfacility=$this->input->get('id_mstfacility');
$id_mst_drugs=$this->input->get('id_mst_drugs');
$type=$this->input->get('type');

$data = $this->Inventory_Report_Model->get_stock_expiry_facility_wise($type,$id_mstfacility,$id_mst_drugs);

if($data){
$arr['status'] = 'true';
$arr['fields'] = json_encode($data);
}else{
$arr['status'] = 'false';
$arr['fields'] = '';
}

echo json_encode($arr);
}

}	
public function lab_drug_forecast($flag=NULL,$child_report=NULL)
	{	


		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		$query="SELECT * FROM `mstlookup` WHERE flag='58'";
		$filter['item_mst_look']=$this->db->query($query)->result();

		$loginData = $this->session->userdata('loginData');
		$query="SELECT * FROM `mstlookup` WHERE flag='94'";
		$content['report_type']=$this->db->query($query)->result();
if ($loginData->user_type==2) {
			$set_facility=$this->get_child_facilies($loginData->DistrictID,$loginData->id_mstfacility);
		$this->session->set_userdata('set_facility',$set_facility);
		}
		//$content['items']=$this->Inventory_Model->get_items();
		$query="SELECT bare_minimum_stock as bare_minimum_stock FROM `mstappstateconfiguration` WHERE id_tblappstateconfiguration=?";
			$content['bare_minimum_stock']=$this->db->query($query,[1])->result();
			///.,+pr($content['bare_minimum_stock']);
			if($REQUEST_METHOD == 'POST')
		{
			$month                      =$this->input->post('month');
			$year = date("Y",strtotime('01-01-'.$this->input->post('year')));
			$last_day = date('t', strtotime('01-'.($month).'-'.$year));
			 $filters1['startdate']        = timeStamp('01-'.($month).'-'.$year);
			 $filters1['enddate']        = timeStamp($last_day.'-'.($month).'-'.$year);
			 $last_day1 = date('t', strtotime('01-'.($month-1).'-'.$year));
			 $filters1['startdate1']        = timeStamp('01-'.($month-1).'-'.$year);
			 $filters1['enddate1']        = timeStamp($last_day1.'-'.($month).'-'.$year);
			$content['Repo_flg']=$flag;
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['child_report'] = $this->input->post('child_report')==0 ? NULL : $this->input->post('child_report');
			$child_report=	$filters1['child_report'];
			$filters1['id_mstfacility'] = $this->input->post('mstfacilitylogin');	
			$filter['item_type']=$this->input->post('item_type');
		}	
		else
		{

						//pr($loginData);
			$filters1['enddate']   =  date('Y-m-t',strtotime(date('Y-m-t')));
			$filters1['startdate']   =  date('Y-m-01');
			$month                      =date('m');
			$year = date("Y");
			$last_day = date('t', strtotime('01-'.($month-1).'-'.$year));
			 $filters1['startdate1']        = timeStamp('01-'.($month-1).'-'.$year);
			 $filters1['enddate1']        = timeStamp($last_day.'-'.($month-1).'-'.$year);
			$filter['item_type']='';
			$content['Repo_flg']=$flag;
			$filters1['id_search_state'] ='';
			$filters1['id_input_district'] ='';
			$filters1['id_mstfacility'] ='';
			$filters1['child_report'] = $child_report;
		}
		
		$this->session->set_userdata('inv_repo_filter',$filter);	
		$this->session->set_userdata('filters1',$filters1);
		if(($loginData->user_type==2 && $child_report==NULL) || $filters1['id_mstfacility']!=''){
			
			if($flag==1){
				$content['items']=$this->Inventory_Model->get_items(NULL,1);
				//echo "facility";exit();
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_lab_indent_forcast(1);
			//pr($content['Inventory_Repo']);
			}
			elseif($flag==2){
				$content['items']=$this->Inventory_Model->get_items(NULL,2);
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_lab_indent_forcast(2);
			//pr($content['Inventory_Repo']);
			}
			elseif($flag==4){
				$content['items']=$this->Inventory_Model->get_items(NULL,4);
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_lab_indent_forcast(4);
			}
		}
		else if(($loginData->user_type==1)&&($filters1['id_mstfacility']=='')){
			if($flag==1){
				$content['items']=$this->Inventory_Model->get_items(NULL,1);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_state_forcast_Report(1);
				//$content['hcv_initiated_sum']=($this->Inventory_Report_Model->hcv_initiated_2_1());
				//pr($content['Inventory_Repo']);
			
			}
			elseif($flag==2){
				$content['items']=$this->Inventory_Model->get_items(NULL,2);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_state_forcast_Report(2);
				//pr($content['Inventory_Repo']);
			}
			elseif($flag==4){
				$content['items']=$this->Inventory_Model->get_items(NULL,4);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_state_forcast_Report(4);
				//pr($content['Inventory_Repo']);
			}
		}
		else if((($loginData->user_type==3)&&($filters1['id_mstfacility']==''))|| (($loginData->user_type==2)&&($child_report==1)) ){
			if($flag==1){
				$content['items']=$this->Inventory_Model->get_items(NULL,1);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_facility_lab_forcast_Report(1);
				//pr($content['Inventory_Repo']);
				$content['hcv_initiated_sum']=array($this->Inventory_Report_Model->hcv_initiated_2_1());
			}
			elseif($flag==2){
				$content['items']=$this->Inventory_Model->get_items(NULL,2);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_facility_lab_forcast_Report(2);
			
			}
			elseif($flag==4){
				$content['items']=$this->Inventory_Model->get_items(NULL,4);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_facility_lab_forcast_Report(4);
			
			}
		}
			
		//pr($content['Inventory_Repo']);
		$sql = "select * from mststate ORDER BY StateName ASC";
		$content['states'] = $this->db->query($sql)->result();
		if($flag==1){
		$content['subview'] = 'drug_stock_forecast_report';
	}
	elseif($flag==2 || $flag==4){
		$content['subview'] = 'laboratory_indent_rorecast';
	}
		
		$this->load->view('inventory/main_layout', $content);
	}
public function get_item_forcast_data(){


if($this->input->get()){

$arr = array();
$id_mstfacility=$this->input->get('id_mstfacility');
$id_mst_drugs=$this->input->get('id_mst_drugs');
$type=$this->input->get('type');

$data = $this->Inventory_Report_Model->get_lab_indent_forcast($type,$id_mstfacility,$id_mst_drugs);

if($data){
$arr['status'] = 'true';
$arr['fields'] = json_encode($data);
}else{
$arr['status'] = 'false';
$arr['fields'] = '';
}

echo json_encode($arr);
}

}	
public function get_facility_forcast_data(){


if($this->input->get()){

$arr = array();
$id_search_state=$this->input->get('id_mststate');
$id_mst_drugs=$this->input->get('id_mst_drugs');
$type=$this->input->get('type');

$data = $this->Inventory_Report_Model->get_facility_lab_forcast_Report($type,$id_search_state,$id_mst_drugs);
if($data){
$arr['status'] = 'true';
$arr['fields'] = json_encode($data);
}else{
$arr['status'] = 'false';
$arr['fields'] = '';
}

echo json_encode($arr);
}

}
public function stock_receipt_mismatch(){
		$loginData = $this->session->userdata('loginData');
		$this->load->library('form_validation');
		$this->load->model('Inventory_Model');
		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
			$query="SELECT * FROM `mstlookup` WHERE flag='58'";
			$filter['item_mst_look']=$this->db->query($query)->result();
			$query="SELECT * FROM `mstlookup` WHERE flag='87'";
			$content['mismatch_reason']=$this->db->query($query)->result();
			//pr($content['mismatch_resaon']);
		
						
						//echo "<pre>";print_r($loginData);
						
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						
					
						
			$filter['Start_Date'] = timeStamp($this->input->post('startdate'));
			$filter['End_Date'] = timeStamp($this->input->post('enddate'));
			 $filter['item_type']=$this->security->xss_clean($this->input->post('item_type'));
					
						
					
				}
				
					else
		{
			$filter['Start_Date'] = timeStamp(timeStampShow(date('Y-m-01')));
			$filter['End_Date'] = timeStamp(timeStampShow(date('Y-m-d')));
			$filter['item_type']='';
		
		}
		$content['items']=$this->Inventory_Model->get_items();

		//pr($content['items']);
		$this->session->set_userdata('invfilter',$filter);	
		if($loginData->user_type==2){
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_stock_mismatch_data_facility();
			$content['filtered_arr']=$this->Inventory_Report_Model->get_stock_mismatch_data_facility(NULL,NULL,NULL,'group');
			//pr($content['filtered_arr']);
			
		}
		else if(($loginData->user_type==1)){
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_stock_mismatch_data_national();
		}
			else if(($loginData->user_type==3)){
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_stock_mismatch_data_state();
		}
		$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
		$content['reason_for_rejection']=$this->db->query($query,['78'])->result();
		$content['subview']           = "stock_mismatch";
		$this->load->view('inventory/main_layout', $content);
			
	}
	public function get_mismatch_data(){


if($this->input->get()){

$arr = array();
$id_search_state=$this->input->get('id_mststate');
$id_mst_drugs=$this->input->get('id_mst_drugs');
$type=$this->input->get('type');

$data = $this->Inventory_Report_Model->get_stock_mismatch_data_state($type,$id_search_state,$id_mst_drugs);

if($data){
$arr['status'] = 'true';
$arr['fields'] = json_encode($data);
}else{
$arr['status'] = 'false';
$arr['fields'] = '';
}

echo json_encode($arr);
}

}	
public function get_item_mismatch_data(){


if($this->input->get()){

$arr = array();
$id_mstfacility=$this->input->get('id_mstfacility');
$id_mst_drugs=$this->input->get('id_mst_drugs');
$type=$this->input->get('type');

$data = $this->Inventory_Report_Model->get_stock_mismatch_data_facility($type,$id_mstfacility,$id_mst_drugs);

if($data){
$arr['status'] = 'true';
$arr['fields'] = json_encode($data);
}else{
$arr['status'] = 'false';
$arr['fields'] = '';
}

echo json_encode($arr);
}

}	

public function stock_reporting_mismatch($ids = null)
	{
		//error_reporting(0);
		$loginData = $this->session->userdata('loginData');
		$query="SELECT * FROM `mstlookup` WHERE flag='58'";
		$filter['item_mst_look']=$this->db->query($query)->result();
		$content['items']=$this->Inventory_Model->get_items();
	//echo "<pre>";print_r($loginData);exit();
if( ($loginData) && $loginData->user_type != '2' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}


		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

	if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->security->xss_clean($this->input->post('search_state'));
			$month                      =$this->security->xss_clean($this->input->post('month'));
			$year = date("Y",strtotime('01-01-'.$this->security->xss_clean($this->input->post('year'))));
			$last_day = date('t', strtotime('01-'.$month.'-'.$year));
			$filters1['End_Date']        = timeStamp($last_day.'-'.$month.'-'.$year);
			$filters1['Start_Date']   =timeStamp('01-'.$month.'-'.$year);
			$filters1['enddate1']        = timeStamp($last_day.'-'.$month.'-'.$year);
			$filter['item_type']=$this->input->post('item_type');
			if (($month)<4) {
				 $filters1['startdate1']   =date('Y-m-d',strtotime($year.'-04-01 -1 year'));
			}
			else{
				$filters1['startdate1']   =date($year.'-04-01');
			}
if ($this->input->post('save')=='save') { 
			$id_mst_drugs= $this->security->xss_clean($this->input->post('id_mst_drug_strength'));
			$remarks= $this->security->xss_clean($this->input->post('remark'));
			$gap= $this->security->xss_clean($this->input->post('gap'));
			$mis= $this->security->xss_clean($this->input->post('mis'));
			$utilize= $this->security->xss_clean($this->input->post('utilize'));
			$type= $this->security->xss_clean($this->input->post('type'));
			foreach ($id_mst_drugs as $key => $value) {
				$insert_array= array('id_mst_drugs' => $id_mst_drugs[$key],
									'gap_remark' => $remarks[$key],
									'utilize' => $utilize[$key],
									'mis' => $mis[$key],
									'type' => $type[$key],
									'year' => $year,
									'month' =>$month ,
									'id_mststate' =>$loginData->State_ID ,
									'id_mstfacility' =>$loginData->id_mstfacility,
									);
				$get_info_monthly_record=$this->Inventory_Report_Model->get_info_monthly_record($month,$year,$id_mst_drugs[$key]);
				if (empty($get_info_monthly_record)) {
				$array2 = array("id_tblusers" => $loginData->id_tblusers, "CreatedOn" => date('Y-m-d'));
				$data = array_merge($insert_array, $array2);
				$this->db->insert('tbl_inventory_mismatch',$data);
			}
			else{
				$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
				$data = array_merge($insert_array, $array2);
				$where=array('id_tbl_mismatch'=>$get_info_monthly_record->id_tbl_mismatch);
				$this->db->where($where);
				$this->db->update('tbl_inventory_mismatch',$data);
			}
			}
			$this->session->set_flashdata('tr_msg','IMS-MIS Mismatch saved Successfully');
			redirect('Inventory_Reports/stock_reporting_mismatch/'); 
			}
		}
		
					
		else
		{
			$filters1['id_search_state'] =$loginData->State_ID;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;
			$filters1['End_Date']   =  date('Y-m-t',strtotime(date('Y-m-t')));
			$filters1['Start_Date']   =  date('Y-m-01',strtotime(date('Y-m-01')));
			$filter['item_type']='';
			$month=date('m');
			$year=date('Y');
			if (($month)<4) {
				 $filters1['startdate1']   =date('Y-m-d',strtotime($year.'-04-01 -1 year'));
			}
			else{
				$filters1['startdate1']   =date($year.'-04-01');
			}
			
		}
//pr($content['children']);

		 $this->session->set_userdata('filters1', $filters1);
		 $this->session->set_userdata('invfilter',$filter);
		 	$content['Inventory_Repo']=$this->Inventory_Report_Model->get_hcv_elisa_count();
		 	$content['filtered_arr']=$this->Inventory_Report_Model->get_hcv_elisa_count('group');
		  $sql = "select * from mststate where ".$sess_where." order by StateName ASC";
		$content['states'] = $this->db->query($sql)->result();

		 $sql = "select * from mstdistrict where ".$sess_mstdistrict."";
		$content['districts'] = $this->db->query($sql)->result();
		//pr($content['Inventory_Repo']);
		 $sql = "select * from mstfacility where  ".$sess_mstfacility."";
		$content['facilities'] = $this->db->query($sql)->result();	
		$content['subview'] = 'stock_reporting_mismatch';
		$this->load->view('inventory/main_layout', $content);
	}

public function stock_reporting_mismatch_state_wise($ids = null)
	{
		//error_reporting(0);
		$loginData = $this->session->userdata('loginData');
		$query="SELECT * FROM `mstlookup` WHERE flag='58'";
		$filter['item_mst_look']=$this->db->query($query)->result();
		$content['items']=$this->Inventory_Model->get_items();
	//echo "<pre>";print_r($loginData);exit();
if( ($loginData) && $loginData->user_type != '3' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}


		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

	if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->security->xss_clean($this->input->post('search_state'));
			$month                      =$this->security->xss_clean($this->input->post('month'));
			$year = date("Y",strtotime('01-01-'.$this->security->xss_clean($this->input->post('year'))));
			$last_day = date('t', strtotime('01-'.$month.'-'.$year));
			$filters1['End_Date']        = timeStamp($last_day.'-'.$month.'-'.$year);
			$filters1['Start_Date']   =timeStamp('01-'.$month.'-'.$year);
			$filters1['enddate1']        = timeStamp($last_day.'-'.$month.'-'.$year);
			$filter['item_type']=$this->input->post('item_type');
			if (($month)<4) {
				 $filters1['startdate1']   =date('Y-m-d',strtotime($year.'-04-01 -1 year'));
			}
			else{
				$filters1['startdate1']   =date($year.'-04-01');
			}
		}
		
					
		else
		{
			$filters1['id_search_state'] =$loginData->State_ID;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;
			$filters1['End_Date']   =  date('Y-m-t',strtotime(date('Y-m-t')));
			$filters1['Start_Date']   =  date('Y-m-01',strtotime(date('Y-m-01')));
			$filter['item_type']='';
			$month=date('m');
			$year=date('Y');
			if (($month)<4) {
				 $filters1['startdate1']   =date('Y-m-d',strtotime($year.'-04-01 -1 year'));
			}
			else{
				$filters1['startdate1']   =date($year.'-04-01');
			}
			
		}
//pr($content['children']);

		 $this->session->set_userdata('filters1', $filters1);
		 $this->session->set_userdata('invfilter',$filter);
		 	$content['Inventory_Repo']=$this->Inventory_Report_Model->get_info_monthly_record_state_wise($month,$year);
		 	$content['Inventory_Repo_sum']=$this->Inventory_Report_Model->get_info_monthly_record_state_wise_sum($month,$year);
		  $sql = "select * from mststate where ".$sess_where." order by StateName ASC";
		$content['states'] = $this->db->query($sql)->result();

		 $sql = "select * from mstdistrict where ".$sess_mstdistrict."";
		$content['districts'] = $this->db->query($sql)->result();
		//pr($content['districts']);
		 $sql = "select * from mstfacility where  ".$sess_mstfacility."";
		$content['facilities'] = $this->db->query($sql)->result();	
		$content['subview'] = 'stock_reporting_mismatch_state_wise';
		$this->load->view('inventory/main_layout', $content);
	}
public function stock_out_instance_facility($ids = null)
	{
		//error_reporting(0);
		$loginData = $this->session->userdata('loginData');
		$query="SELECT * FROM `mstlookup` WHERE flag='58'";
		$filter['item_mst_look']=$this->db->query($query)->result();
		$content['items']=$this->Inventory_Model->get_items();
		$query="SELECT * FROM `mstlookup` WHERE flag='86'";
			$content['confirmed']=$this->db->query($query)->result();
	//echo "<pre>";print_r($loginData);exit();
if( ($loginData) && $loginData->user_type != '2' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}


		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

	if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->security->xss_clean($this->input->post('search_state'));
			$month                      =$this->security->xss_clean($this->input->post('month'));
			$year = date("Y",strtotime('01-01-'.$this->security->xss_clean($this->input->post('year'))));
			$last_day = date('t', strtotime('01-'.$month.'-'.$year));
			$filters1['End_Date']        = timeStamp($last_day.'-'.$month.'-'.$year);
			$filters1['Start_Date']   =timeStamp('01-'.$month.'-'.$year);
			$filters1['enddate1']        = timeStamp($last_day.'-'.$month.'-'.$year);
			$filter['item_type']=$this->input->post('item_type');
			if (($month)<4) {
				 $filters1['startdate1']   =date('Y-m-d',strtotime($year.'-04-01 -1 year'));
			}
			else{
				$filters1['startdate1']   =date($year.'-04-01');
			}

		}
		
					
		else
		{
			$filters1['id_search_state'] =$loginData->State_ID;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;
			$filters1['End_Date']   =  date('Y-m-t',strtotime(date('Y-m-t')));
			$filters1['Start_Date']   =  date('Y-m-01',strtotime(date('Y-m-01')));
			$filter['item_type']='';
			$month=date('m');
			$year=date('Y');
			if (($month)<4) {
				 $filters1['startdate1']   =date('Y-m-d',strtotime($year.'-04-01 -1 year'));
			}
			else{
				$filters1['startdate1']   =date($year.'-04-01');
			}
			
		}
//pr($content['children']);

		 $this->session->set_userdata('filters1', $filters1);
		 $this->session->set_userdata('invfilter',$filter);
		 	$content['Inventory_Repo']=$this->Inventory_Report_Model->get_stock_out_instance_facility();
		 	$content['filtered_arr']=$this->Inventory_Report_Model->get_stock_out_instance_facility('group');
		 	//pr($content['filtered_arr']);
		  $sql = "select * from mststate where ".$sess_where." order by StateName ASC";
		$content['states'] = $this->db->query($sql)->result();

		 $sql = "select * from mstdistrict where ".$sess_mstdistrict."";
		$content['districts'] = $this->db->query($sql)->result();
		//pr($content['districts']);
		 $sql = "select * from mstfacility where  ".$sess_mstfacility."";
		$content['facilities'] = $this->db->query($sql)->result();	
		$content['subview'] = 'stock_out_instances';
		$this->load->view('inventory/main_layout', $content);
	}
public function reports_section()
	{	
		$content['subview'] = 'reports_page';
		$this->load->view('inventory/main_layout', $content);
	}
public function add_stock_out()
{
		$loginData = $this->session->userdata('loginData');
	//echo "<pre>";print_r($loginData);exit();
if( ($loginData) && $loginData->user_type != '2' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}


		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

	if($REQUEST_METHOD == 'POST')
		{
			
	if ($this->input->post('save')=='save') { 
			$id_mst_drugs= $this->security->xss_clean($this->input->post('item_name'));
			$stockout_date= timeStamp($this->security->xss_clean($this->input->post('stockout_date')));
			$out_date= timeStamp($this->security->xss_clean($this->input->post('out_date')));
			$confirmed= $this->security->xss_clean($this->input->post('confirmed'));
			$stockout_remark= $this->security->xss_clean($this->input->post('stockout_remark'));
			$out_count= $this->security->xss_clean($this->input->post('out_count'));
			$type= $this->security->xss_clean($this->input->post('type'));
			$date_arr=explode("-",$out_date);
			$year=$date_arr[0];
			$month=$date_arr[1];
				$insert_array= array('stockout_date' => $stockout_date,
									'confirmed' => $confirmed,
									'stockout_remark' => $stockout_remark,
									'out_count' => $out_count,
									'type' => $type,
									'year' => $year,
									'month' =>$month ,
									'id_mststate' =>$loginData->State_ID ,
									'id_mstfacility' =>$loginData->id_mstfacility,
									);
				
				$get_info_monthly_record=$this->Inventory_Report_Model->get_info_monthly_record($month,$year,$id_mst_drugs);
				//pr($get_info_monthly_record);exit();
				if (empty($get_info_monthly_record)) {
				$array2 = array("id_tblusers" => $loginData->id_tblusers, "CreatedOn" => date('Y-m-d'));
				$data = array_merge($insert_array, $array2);
				$this->db->insert('tbl_inventory_mismatch',$data);
			}
			else{
				$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
				$data = array_merge($insert_array, $array2);
				$where=array('id_tbl_mismatch'=>$get_info_monthly_record->id_tbl_mismatch);
				$this->db->where($where);
				$this->db->update('tbl_inventory_mismatch',$data);
			}
			redirect('Inventory_Reports/stock_out_instance_facility/'); 
			}
		}
}
public function stock_out_instance_state_wise($ids = null)
	{
		//error_reporting(0);
		$loginData = $this->session->userdata('loginData');
		$query="SELECT * FROM `mstlookup` WHERE flag='58'";
		$filter['item_mst_look']=$this->db->query($query)->result();
		$content['items']=$this->Inventory_Model->get_items();
		$query="SELECT * FROM `mstlookup` WHERE flag='86'";
			$content['confirmed']=$this->db->query($query)->result();
	//echo "<pre>";print_r($loginData);exit();
if( ($loginData) && ($loginData->user_type != '3')){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
			/*elseif( ($loginData) && ($loginData->user_type != '1')){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}*/
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}


		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

	if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->security->xss_clean($this->input->post('search_state'));
			$month                      =$this->security->xss_clean($this->input->post('month'));
			$year = date("Y",strtotime('01-01-'.$this->security->xss_clean($this->input->post('year'))));
			$last_day = date('t', strtotime('01-'.$month.'-'.$year));
			$filters1['End_Date']        = timeStamp($last_day.'-'.$month.'-'.$year);
			$filters1['Start_Date']   =timeStamp('01-'.$month.'-'.$year);
			$filters1['enddate1']        = timeStamp($last_day.'-'.$month.'-'.$year);
			$filter['item_type']=$this->input->post('item_type');
			if (($month)<4) {
				 $filters1['startdate1']   =date('Y-m-d',strtotime($year.'-04-01 -1 year'));
			}
			else{
				$filters1['startdate1']   =date($year.'-04-01');
			}
		}
		
					
		else
		{
			$filters1['id_search_state'] =$loginData->State_ID;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;
			$filters1['End_Date']   =  date('Y-m-t',strtotime(date('Y-m-t')));
			$filters1['Start_Date']   =  date('Y-m-01',strtotime(date('Y-m-01')));
			$filter['item_type']='';
			$month=date('m');
			$year=date('Y');
			if (($month)<4) {
				 $filters1['startdate1']   =date('Y-m-d',strtotime($year.'-04-01 -1 year'));
			}
			else{
				$filters1['startdate1']   =date($year.'-04-01');
			}
			
		}
//pr($content['children']);

		 $this->session->set_userdata('filters1', $filters1);
		 $this->session->set_userdata('invfilter',$filter);
		 if(($loginData->user_type==1)){
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_stock_mismatch_data_national();
		}
			else if(($loginData->user_type==3)){
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_stock_out_state_wise($month,$year);
		 	$content['Inventory_Repo_sum']=$this->Inventory_Report_Model->get_stock_out_state_wise_sum($month,$year);
		}
		 	
		  $sql = "select * from mststate where ".$sess_where." order by StateName ASC";
		$content['states'] = $this->db->query($sql)->result();

		 $sql = "select * from mstdistrict where ".$sess_mstdistrict."";
		$content['districts'] = $this->db->query($sql)->result();
		//pr($content['districts']);
		 $sql = "select * from mstfacility where  ".$sess_mstfacility."";
		$content['facilities'] = $this->db->query($sql)->result();	
		$content['subview'] = 'stock_out_instance_state_wise';
		$this->load->view('inventory/main_layout', $content);
	}
	function get_indent_quantity(){
	$inventory_id = $this->input->get('inventory_id',TRUE);
	$loginData = $this->session->userdata('loginData');
	$query="SELECT i1.*,case when i1.quantity_dispatched IS NOT NULL AND i1.quantity_dispatched>0 AND i1.is_temp='1' then i2.quantity_dispatched when (i2.relocated is NULL || i2.relocated=0) then i1.requested_quantity when (i2.relocated<i1.requested_quantity and relocation_status is NULL or relocation_status=0) then i1.requested_quantity ELSE i2.relocated END AS relocated,case when (i1.approved_quantity is NULL || i1.approved_quantity=0) then (case when i2.quantity_dispatched IS NOT NULL AND i2.quantity_dispatched>0 AND i1.is_temp='1' then i2.quantity_dispatched ELSE relocated END) ELSE i1.approved_quantity END AS approved FROM (SELECT * FROM tbl_inventory where is_deleted=? AND inventory_id=? and id_mststate=?)AS i1
LEFT JOIN 
(SELECT SUM(relocated_quantity) AS relocated,SUM(quantity_dispatched) AS quantity_dispatched,indent_num,id_mstfacility FROM tbl_inventory where is_deleted=? AND  id_mststate=? and inventory_id=? GROUP BY indent_num) AS i2
ON i1.indent_num=i2.indent_num AND i1.id_mstfacility=i2.id_mstfacility";
							$data['inventory_details']=$this->db->query($query,['0',$inventory_id,$loginData->State_ID,'0',$loginData->State_ID,$inventory_id])->result();
	echo json_encode($data);						
}
public function fix_mismatch(){
		$loginData = $this->session->userdata('loginData');
		if( ($loginData) && $loginData->user_type != '2' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
		$this->load->library('form_validation');
		$this->load->model('Inventory_Model');
		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
			$query="SELECT * FROM `mstlookup` WHERE flag='58'";
			$filter['item_mst_look']=$this->db->query($query)->result();
			//pr($filter['item_mst_look']);
		
						
						//echo "<pre>";print_r($loginData);
						
						if( ($loginData) && $loginData->user_type == '1' ){
						$sess_where = " 1";
						}
						elseif( ($loginData) && $loginData->user_type == '2' ){
						
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
						}
						elseif( ($loginData) && $loginData->user_type == '3' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
						}
						elseif( ($loginData) && $loginData->user_type == '4' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
						}
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						$State_ID=$loginData->State_ID;
						$DistrictID=$loginData->DistrictID;
						$id_mstfacility=$loginData->id_mstfacility;
						if($loginData->State_ID==''||$loginData->State_ID==NULL){
						 $State_ID=0;
						}
						if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
						$DistrictID=0;
						}
						if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
						$id_mstfacility=0;
						}
						if ($this->security->xss_clean($this->input->post('search'))=='search') {
						
			 $filter['item_type']=$this->security->xss_clean($this->input->post('item_type'));
			 $filter['id_mstfacility']=$this->security->xss_clean($this->input->post('id_mstfacility'));
						}
						elseif ($this->security->xss_clean($this->input->post('save'))=='save') {

						$item_id=$this->security->xss_clean($this->input->post('item_name'));

						$inventory_id=$this->security->xss_clean(strip_tags($this->input->post('inventory_id')));
						 $this->form_validation->set_rules('approved_quantity', 'approved_quantity', 'trim|required|numeric|xss_clean');
						  $this->form_validation->set_rules('quantity_received', 'received quantity ', 'trim|required|numeric|xss_clean');
						   $this->form_validation->set_rules('already_received_quantity', 'received quantity', 'trim|numeric|xss_clean');
						    $this->form_validation->set_rules('quantity_dispatched', 'dispatch quantity', 'trim|numeric|xss_clean');
						    $this->form_validation->set_rules('quantity_dispatched', 'dispatch quantity', 'trim|numeric|xss_clean');
						     $this->form_validation->set_rules('already_dispatched_quantity', 'dispatch quantity', 'trim|xss_clean');
						     $this->form_validation->set_rules('mismatch_reason', 'mismatch_reason', 'trim|xss_clean');
						

					if ($this->form_validation->run() == FALSE) {

						$arr['status'] = 'false';
						$this->session->set_flashdata("tr_msg",validation_errors());
						//echo("arg1");
						redirect('Inventory/approved_indent/');
					}else{

							$id_mstfacility=$this->security->xss_clean(strip_tags($this->input->post('id_mstfacility')));
							$indent_num=$this->security->xss_clean(strip_tags($this->input->post('indent_num')));
							$quantity_received=$this->security->xss_clean(strip_tags($this->input->post('quantity_received')));
							$already_received_quantity=$this->security->xss_clean(strip_tags($this->input->post('already_received_quantity')));
							$quantity_dispatched=$this->security->xss_clean(strip_tags($this->input->post('quantity_dispatched')));
							$already_dispatched_quantity=$this->security->xss_clean(strip_tags($this->input->post('already_dispatched_quantity')));
							$mismatch_reason=$this->security->xss_clean(strip_tags($this->input->post('mismatch_reason')));
							$flag=$this->security->xss_clean(strip_tags($this->input->post('flag')));
							$not_quantity_received=$quantity_dispatched-$quantity_received;
							$rem=$already_dispatched_quantity-$quantity_dispatched;
						if ($quantity_received>=$quantity_dispatched) {
								$status=3;
							}
							else if ($quantity_received<$quantity_dispatched) {
								$status=5;
							}
						//echo $id_mstfacility;exit();
						if ($loginData->id_mstfacility==$id_mstfacility && ($flag='I' or $flag='R')) {
								$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
								$update_array = array(  
						'quantity_received'     => $quantity_received,
						'not_quantity_received'     => $not_quantity_received,
						'mismatch_reason'     => $mismatch_reason,
						'current_status'=>5,
						'indent_status'=>$status,
						'relocation_status'=>$status
						
						);
								//pr($update_array);exit();
							$data = array_merge($update_array, $array2);
							$this->db->where('inventory_id',$inventory_id);
							$this->db->update('tbl_inventory',$data);
							$this->session->set_flashdata('tr_msg','Stock Mismatch fixed Successfully');
						redirect('Inventory_Reports/stock_receipt_mismatch');
						}
						else {
							$update_array = array(  
						'quantity_received'     => $quantity_received,
						'quantity_dispatched'     => $quantity_dispatched,
						'relocated_quantity'     => $quantity_dispatched,
						'not_relocated_quantity'     => $rem,
						'not_quantity_received'     => $not_quantity_received,
						'mismatch_reason'     => $mismatch_reason,
						'current_status'=>5,
						'indent_status'=>$status,
						'relocation_status'=>$statuss
						
						);
							//pr($update_array);exit();
							$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
							$data = array_merge($update_array, $array2);
							$this->db->where('inventory_id',$inventory_id);
							$this->db->update('tbl_inventory',$data);
							if ($rem>0) 
							{
								$query="SELECT * FROM tbl_inventory i WHERE inventory_id=?";
							$get_insert_array=$this->db->query($query,[$inventory_id])->result();
							//pr($get_insert_array);exit();
							$get_insert_array[0]->inventory_id=NULL;
							$get_insert_array[0]->refrence_id=$inventory_id;
							$get_insert_array[0]->quantity=NULL;
							$get_insert_array[0]->requested_quantity=0;
							$get_insert_array[0]->pending_quantity=$rem;
							$get_insert_array[0]->id_tblusers=$loginData->id_tblusers;
							$get_insert_array[0]->CreatedOn=date('Y-m-d');
							$get_insert_array[0]->indent_status=NULL;
							$get_insert_array[0]->relocation_status= NULL;
							$get_insert_array[0]->Entry_Date= NULL;
							$get_insert_array[0]->Expiry_Date= NULL;
							$get_insert_array[0]->quantity_dispatched= NULL;
							$get_insert_array[0]->relocated_quantity= NULL;
							$get_insert_array[0]->not_relocated_quantity= NULL;
							$get_insert_array[0]->not_quantity_received= NULL;
							$get_insert_array[0]->quantity_rejected= NULL;
							$get_insert_array[0]->quantity_received= NULL;
							$get_insert_array[0]->warehouse_request_quantity= NULL;
							$get_insert_array[0]->updateBy= NULL;
							$get_insert_array[0]->updatedOn= NULL;
							$get_insert_array[0]->dispatch_date= NULL;
							$get_insert_array[0]->issue_num= NULL;
							$get_insert_array[0]->relocation_remark= NULL;
							$get_insert_array[0]->batch_num= NULL;
							$get_insert_array[0]->relocated_quantity= NULL;
							$get_insert_array[0]->mismatch_reason= NULL;
							$get_insert_array[0]->is_notification= '0';
							$get_insert_array[0]->is_temp= '0';
							$get_insert_array[0]->is_temp= '0';
							$get_insert_array[0]->current_status= 2;

							$set_insert_array_values=$get_insert_array[0];
							$this->db->insert('tbl_inventory',$set_insert_array_values);
						}
							$this->session->set_flashdata('tr_msg','Stock Mismatch Fixed Successfully');
						redirect('Inventory_Reports/stock_receipt_mismatch');
						}
						///pr($update_array);exit();
						//echo $inventory_id;exit;
						/*else if ($inventory_id!=NULL) {
						$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						$data = array_merge($update_array, $array2);
						$this->db->where('inventory_id',$inventory_id);
						$this->db->update('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Indent Approved Successfully');
						redirect('Inventory/approved_indent');
						}	*/
						}
					}
					
				}
							
	}


public function stock_indent_forecast($flag=NULL)
	{	


		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		$query="SELECT * FROM `mstlookup` WHERE flag='58'";
		$filter['item_mst_look']=$this->db->query($query)->result();
		$loginData = $this->session->userdata('loginData');
		//$content['items']=$this->Inventory_Model->get_items();
			if($REQUEST_METHOD == 'POST')
		{
			$month                      =$this->input->post('month');
			$year = date("Y",strtotime('01-01-'.$this->input->post('year')));
			$last_day = date('t', strtotime('01-'.($month).'-'.$year));
			 $filters1['startdate']        = timeStamp('01-'.($month).'-'.$year);
			 $filters1['enddate']        = timeStamp($last_day.'-'.($month).'-'.$year);
			 $last_day1 = date('t', strtotime('01-'.($month-1).'-'.$year));
			 $filters1['startdate1']        = timeStamp('01-'.($month-1).'-'.$year);
			 $filters1['enddate1']        = timeStamp($last_day1.'-'.($month).'-'.$year);
			$content['Repo_flg']=$flag;
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('mstfacilitylogin');	
			$filter['item_type']=$this->input->post('item_type');
		}	
		else
		{

						//pr($loginData);
			$filters1['enddate']   =  date('Y-m-t',strtotime(date('Y-m-t')));
			$filters1['startdate']   =  date('Y-m-01');
			$month                      =date('m');
			$year = date("Y");
			$last_day = date('t', strtotime('01-'.($month-1).'-'.$year));
			 $filters1['startdate1']        = timeStamp('01-'.($month-1).'-'.$year);
			 $filters1['enddate1']        = timeStamp($last_day.'-'.($month-1).'-'.$year);
			$filter['item_type']='';
			$content['Repo_flg']=$flag;
			$filters1['id_search_state'] ='';
			$filters1['id_input_district'] ='';
			$filters1['id_mstfacility'] ='';
		}
		
		$this->session->set_userdata('inv_repo_filter',$filter);	
		$this->session->set_userdata('filters1',$filters1);
		if($loginData->user_type==2){
			
			if($flag==1){
				$content['items']=$this->Inventory_Model->get_items(NULL,1);
				//echo "facility";exit();
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_lab_indent_forcast(1);
			
			 
			}
			elseif($flag==2){
				$content['items']=$this->Inventory_Model->get_items(NULL,2);
			$content['Inventory_Repo']=$this->Inventory_Report_Model->get_lab_indent_forcast(2);
			}
			
		}
		else if(($loginData->user_type==1)&&($filters1['id_mstfacility']=='')){
			if($flag==1){
				$content['items']=$this->Inventory_Model->get_items(NULL,1);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_state_forcast_Report(1);
			
			}
			elseif($flag==2){
				$content['items']=$this->Inventory_Model->get_items(NULL,2	);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_state_forcast_Report(2);
				//pr($content['Inventory_Repo']);
			}
		}
		else if(($loginData->user_type==3)&&($filters1['id_mstfacility']=='')){
			if($flag==1){
				$content['items']=$this->Inventory_Model->get_items(NULL,1);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_facility_lab_forcast_Report(1);
			}
			elseif($flag==2){
				$content['items']=$this->Inventory_Model->get_items(NULL,2);
				$content['Inventory_Repo']=$this->Inventory_Report_Model->get_facility_lab_forcast_Report(2);
			
			}
		}
			
		//pr($content['Inventory_Repo']);
		$sql = "select * from mststate ORDER BY StateName ASC";
		$content['states'] = $this->db->query($sql)->result();
		if($flag==1){
		$content['subview'] = 'stock_indent_forecast';
	}
	//$content['subview'] = 'stock_indent_forecast';
	/*elseif($flag==2){
		$content['subview'] = 'laboratory_indent_rorecast';
	}*/
		
		$this->load->view('inventory/main_layout', $content);
	}


public function get_child_facilies($district,$id_mstfacility)
{
	$sql = "select * from mstfacility where id_mstdistrict = ? and is_Warehouse!=1 and id_mstfacility=?";
	$res_facility=$this->db->query($sql,[$district,$id_mstfacility])->result();
	return ($res_facility);
}

}