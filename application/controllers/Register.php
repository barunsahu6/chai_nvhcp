<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	private $patientFilters;

	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('Common_Model');
		$this->load->model('Patient_Model');
		
		$loginData = $this->session->userdata('loginData');
		if($loginData->user_type != 1){
			$this->session->set_flashdata('er_msg','You are not logged-in, please login again to continue');
			redirect('login');	
		}
		
		$this->patientFilters = $this->session->userdata('patientFilters');
	}
	public function index(){
		// die('hjk');

		$content['subview'] = 'register_view';
		$this->load->view('pages/main_layout', $content);

	}	

}