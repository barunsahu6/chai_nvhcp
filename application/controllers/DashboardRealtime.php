<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardRealtime extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Common_Model');
		$this->load->helper('common');
		 set_time_limit(100000);
        ini_set('memory_limit', '100000M');
        ini_set('upload_max_filesize', '300000M');
        ini_set('post_max_size', '200000M');
        ini_set('max_input_time', 100000);
        ini_set('max_execution_time', 0);
        ini_set('memory_limit','-1');

error_reporting(0);
		$loginData = $this->session->userdata('loginData');

			if($this->session->userdata('filters') != null)
		{

			
			$filters = $this->session->userdata('filters');
			//pr($filters);
		if($filters['id_search_state']  == 0)
				{
				$loginData = $this->session->userdata('loginData'); 
				$this->facility_state              = " and 1";
				$this->facility_state1              = " and 1";
					if( ($loginData) && $loginData->user_type == '1' ){
					$this->facility_state  = "AND 1";
					$this->facility_state1  = "AND 1";
					$this->id_mstfacilityf = "AND 1";
					}
					
				}
				else
				{
				$this->facility_state              = " and Session_StateID = ".$filters['id_search_state'];
				$this->facility_state1              = " p.Session_StateID = ".$filters['id_search_state'];
				$this->id_mstfacilityf = "AND 1";
				}

				if($filters['id_input_district']      == 0)
				{
				$loginData = $this->session->userdata('loginData'); 

					if( ($loginData) && $loginData->user_type == '1' ){
					$this->facility_district = "AND 1";
					$this->facility_district1 = "AND 1";
					}
				$this->facility_district              = "AND 1";	
				}
				else
				{
				$this->facility_district              = "AND Session_DistrictID = ".$filters['id_input_district'];
				$this->facility_district1              = "AND p.Session_DistrictID = ".$filters['id_input_district'];
				$this->id_mstfacilityf = "AND 1";
				}

					if($filters['id_mstfacility']      == 0)
				{
				$loginData = $this->session->userdata('loginData'); 

					if( ($loginData) && $loginData->user_type == '1' ){
					$this->facility_district = "AND 1";
					$this->facility_district1 = "AND 1";
					}
				$this->id_mstfacility              = "AND 1";
				$this->id_mstfacilityp              = "AND 1";	
				}
				else
				{
				$this->id_mstfacility              = "AND id_mstfacility = ".$filters['id_mstfacility'];
				$this->id_mstfacilityp              = "AND p.id_mstfacility = ".$filters['id_mstfacility'];
				$this->id_mstfacilityf 				= "AND tblsummary_new.id_mstfacility = ".$filters['id_mstfacility'];
				}
}
		if($loginData == null)
		{
			redirect('login');
		}
	}



	public function index($ids = null)
	{
		$loginData = $this->session->userdata('loginData');
	//echo "<pre>";print_r($loginData);exit();

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_wherep = " 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
				$sess_wherep = " Session_StateID = '".$loginData->State_ID."' ";
			}


		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

		if($REQUEST_METHOD == 'POST')
		{
			
			$filters['id_search_state'] = $this->input->post('search_state');
			$filters['id_input_district'] = $this->input->post('input_district');
			$filters['id_mstfacility'] = $this->input->post('mstfacilitylogin');	
			$filters['startdate']      = timeStamp($this->input->post('startdate'));	
			$filters['enddate']        = timeStamp($this->input->post('enddate'));
			$filters['source_of_patient']=$this->input->post('source_of_patient');

			$headerdata['id_search_state']=$this->input->post('search_state');
			$headerdata['source_of_patient']=$this->input->post('source_of_patient');
			//$filtershav['getmonthanalys2'] = $this->input->post('getmonthanalys2');
			//pr($filters['getmonthanalys2']);exit();
			if(empty($headerdata['id_search_state'])){
				$id_search_stateval= ' 1';
			}else{
				$id_search_stateval = "id_mststate=".$headerdata['id_search_state']."";
			}
			$id_search_states = $id_search_stateval;
		}
		else
		{	
			 $filters['id_search_state'] = 0;
			$filters['id_input_district'] = 0;
			$filters['id_mstfacility'] = 0;	
			$filters['source_of_patient']=0;
			$filters['startdate']      = '2019-07-01';	
			$filters['enddate']        = date('Y-m-d');
			//$filtershav['getmonthanalys2'] = '';
			//pr($content['filters1']);exit();

			$headerdata['id_search_state'] = $loginData->State_ID;
			//pr($filters['getmonthanalys2']);exit();
			if(empty($headerdata['id_search_state'])){
				$id_search_stateval= '1';
			}else{
				$id_search_stateval = "id_mststate=".$headerdata['id_search_state']."";
			}
			$id_search_states = $id_search_stateval;


		}

		 //edit by vikram
		$this->session->set_userdata('filters', $filters);
		$this->session->set_userdata('headerdata', $filters);

		//$this->session->set_userdata('filtershav', $filtershav);
		$user_data = $this->session->userdata('filters');
		//pr($user_data);
		$headerdata = $this->session->userdata('headerdata');
        $statequery="select StateName,id_mststate from mststate WHERE ".$id_search_states."";

        	$statename=$this->db->query($statequery)->result();
        	//pr($statename);exit();
        	$d_id=0;
		if(count($statename)>1){
			$csvheader['sname']="ALL States";
		}
		else{
			$csvheader['sname']=$statename[0]->StateName;
			
		}

		if(($user_data['id_input_district']==NULL || empty($user_data['id_input_district'])) && $loginData->id_mstfacility==NULL)
		{
			$d_id="1";
			$idmststate = $id_search_states;
		}
		else if(($loginData->DistrictID!=NULL || $loginData->DistrictID!=0 || $loginData->DistrictID!='') && ($user_data['id_input_district']==NULL || empty($user_data['id_input_district'])) ){
			$idmststate = $id_search_states;
			$d_id="id_mstdistrict = ".$loginData->DistrictID."";
		}

		else if(($loginData->DistrictID!=NULL) && $loginData->id_mstfacility!=NULL){
			$idmststate = $id_search_states;
			$d_id="id_mstdistrict = ".$loginData->DistrictID."";
		}

		else{
			$d_id="id_mstdistrict = ".$user_data['id_input_district']."";
			$idmststate = $id_search_states;
		}
		$distquery="select DistrictName from mstdistrict WHERE ".$idmststate." AND ".$d_id."";
        	$distname=$this->db->query($distquery)->result();
        	
		//echo count($distname);exit();
		if(count($distname)>1){
			$csvheader['dname']=" ALL District";
		}
		else{
			$csvheader['dname']=$distname[0]->DistrictName;
		}
		if(($user_data['id_mstfacility']==NULL || $user_data['id_mstfacility']=='') && ($loginData->id_mstfacility==NULL || $loginData->id_mstfacility==0 || $loginData->id_mstfacility==''))
		{
			$f_id="1";
		}
		else if(($loginData->id_mstfacility!=NULL || $loginData->id_mstfacility!=0 || $loginData->id_mstfacility!='') && ($user_data['id_mstfacility']==NULL || $user_data['id_mstfacility']=='') ){
			$f_id="id_mstfacility = ".$loginData->id_mstfacility."";
		}
		else{
			$f_id="id_mstfacility = ".$user_data['id_mstfacility']."";
		}
		$facilityquery="select facility_short_name from mstfacility WHERE ".$id_search_states." AND ".$d_id." AND ".$f_id."";
        	$facilityname=$this->db->query($facilityquery)->result();
		//echo count($facilityname);exit();
		if(count($facilityname)>1){
			$csvheader['fname']=" All Facility";
		}
		else{
			$csvheader['fname']=$facilityname[0]->facility_short_name;
		}
			$csvheader['startdate']=timestampshow($user_data['startdate']);    
			$csvheader['enddate']=timestampshow($user_data['enddate']);
		$this->session->set_userdata('csvheader', $csvheader);
		//ends here edit by vikram
		$this->load->model('Dashboard_Model');

		$content['filters']             = $this->session->userdata('filters');

		$getmonthanalys2 = $this->input->post('getmonthanalys2');

		$content['real_anti_hcv_screened']  = $this->Dashboard_Model->real_anti_hcv_screened();
		$content['real_anti_hcv_positive']  = $this->Dashboard_Model->real_anti_hcv_positive();
		$content['real_viral_load_tested']  = $this->Dashboard_Model->real_viral_load_tested();
		$content['real_viral_load_detected']  = $this->Dashboard_Model->real_viral_load_detected();
		$content['real_initiatied_on_treatment']  = $this->Dashboard_Model->real_initiatied_on_treatment();
		$content['real_treatment_completed']  = $this->Dashboard_Model->real_treatment_completed();
		$content['real_svr_done']  = $this->Dashboard_Model->real_svr_done();
		$content['real_treatment_successful']  = $this->Dashboard_Model->real_treatment_successful();
		$content['real_treatment_failure']  = $this->Dashboard_Model->real_treatment_failure();
		if ($loginData->user_type==1 || $loginData->user_type==3) { 
		$content['treatment_Initiations_by_District']    = $this->Dashboard_Model->real_treatment_Initiations_by_District();
	}
		 if ($loginData->user_type==1) {
		$content['screened_antihcv_across_states']    = $this->Dashboard_Model->real_screened_antihcv_across_states();
	}
		

		

		/*check start mtc TC USER*/
			if(($loginData->id_mstfacility=='' || $loginData->id_mstfacility==0 ) && $user_data['id_mstfacility']!='') {
			$id_mstfacility = "f.id_mstfacility = ".$user_data['id_mstfacility'];
		}elseif($loginData->id_mstfacility=='' || $loginData->id_mstfacility==0){
			$id_mstfacility = 1;
		}
		else{
			$id_mstfacility = " f.id_mstfacility = ".$loginData->id_mstfacility;
		}

		 //echo 'gggggg'.$sql = "select f.is_Mtc from tblusers as u inner join mstfacility f on u.id_mstfacility=f.id_mstfacility where ".$id_mstfacility." and u.id_tblusers=".$loginData->id_tblusers." ";
		 $sql = "select f.is_Mtc from tblusers as u inner join mstfacility f on u.id_mstfacility=f.id_mstfacility where ".$id_mstfacility." limit 1 ";
		$content['mtc_user'] = $this->db->query($sql)->result();
		
		$sql ="SELECT * FROM mstlookup where  flag=91 and languageID=1 order by Sequence ASC";
		$content['source_of_patient'] = $this->db->query($sql)->result();
		$sql = "select * from mststate ORDER BY StateName ASC";
		$content['states'] = $this->db->query($sql)->result();

		$sql = "select * from mstdistrict";
		$content['districts'] = $this->db->query($sql)->result();

		$sql = "select * from mstfacility";
		$content['facilities'] = $this->db->query($sql)->result();	

		$content['start_date'] = '2016-08-16';
		$content['end_date']   = date('Y-m-d');
		$content['facilities'] = $this->Dashboard_Model->facilities();
		
		$content['subview'] = 'dashboard_realtime';
		$this->load->view('pages/main_layout', $content);
	}


public function casecase_vl_export(){

$this->load->model('Dashboard_Model');
$sel_id = $this->input->get('sel_id',true);

$data['screened_antihcv_across_state']= $this->Dashboard_Model->real_screened_antihcv_across_states($sel_id);
//$this->load->view('pages/components/dashboard',$data);
 echo json_encode($data);

}

public function hcv_screened_dis(){

$this->load->model('Dashboard_Model');
$sel_id = $this->input->get('sel_id',true);

$data['screened_hcv_across_dis']= $this->Dashboard_Model->real_treatment_Initiations_by_District(NULL,$sel_id);
//$this->load->view('pages/components/dashboard',$data);
echo json_encode($data);
//echo json_encode(array('data'=>$data));
}



}	