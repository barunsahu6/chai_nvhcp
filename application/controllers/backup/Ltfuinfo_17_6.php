<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ltfuinfo extends CI_Controller {

	private $patientFilters;

	public function __construct()
	{
		parent::__construct();

		 /*if(empty($this->session->userdata("loginData")))
         {
         redirect(site_url(),'refresh');
         }*/


		$this->load->library('form_validation');
		$this->load->model('Common_Model');
		$this->load->model('Patient_Model');
		 set_time_limit(100000);
        ini_set('memory_limit', '100000M');
        ini_set('upload_max_filesize', '300000M');
        ini_set('post_max_size', '200000M');
        ini_set('max_input_time', 100000);
        ini_set('max_execution_time', 0);
        ini_set('memory_limit','-1');
        $this->load->library('pagination');
        $this->load->helper('common');

		error_reporting(0);
		$loginData = $this->session->userdata('loginData');
		//pr($loginData);
		if($loginData == null)
		{
			redirect('login');
		}

		if($loginData->user_type != 2){
			$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login');	
		}


		$this->patientFilters = $this->session->userdata('patientFilters');
	}


public function index(){


		$loginData = $this->session->userdata('loginData');


		$gtid= $this->security->xss_clean($_GET['p']);
			if(!empty($gtid)){
		if($gtid==1){
			$LookupCode = "3,2,1,4,5,16";

		}elseif($gtid==2){
			$LookupCode = "3,2,1,4,5,12,30,14,15,13";
		}elseif($gtid==3){
			$LookupCode = "4,5,6,7,8,9,10,11,12,13,14,15";
		}else{
			$LookupCode = "0";
		}
	}else{
		$LookupCode = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16";
	}

	 if(!empty($gtid)){	
	 	if($gtid==2){

	 	$sql = "SELECT * FROM mstlookup where flag = 47  and LookupCode in($LookupCode) and LanguageID = 1 order by LookupCode ASC";
	 } elseif($gtid==1){
	 	$sql = "SELECT * FROM mstlookup where flag = 47  and LookupCode in($LookupCode) and LanguageID = 1 order by LookupCode ASC";
	 }
	 	else{

		$sql = "SELECT * FROM mstlookup where flag = 47  and LanguageID = 1 and LookupCode in($LookupCode) order by LookupCode ASC";
	 }	
}else{
	$sql = "SELECT * FROM mstlookup where flag = 47  and LanguageID = 1 and LookupCode in(7,8,9,10,11) order by LookupCode ASC";
}
		$content['status'] = $this->Common_Model->query_data($sql);


		$patient_list = array();
		$where = " where 1 ";

		$RequestMethod = $this->input->server('REQUEST_METHOD');
$query_params = [];
		if($RequestMethod == 'POST')
		{
			$search_by = $this->security->xss_clean($this->input->post('search_by'));

			

			if($search_by == 1)
			{
				
				$uid_contact = $this->security->xss_clean($this->input->post('uid_contact'));
				$query_params[]  = $uid_contact;									
				$uid_contact_len = strlen($uid_contact);
				if($uid_contact_len <= 6)
				{
					$where = " where p.UID_Num = ?";
				}
				else if($uid_contact_len > 6)
				{
					$where = " where p.Mobile = ?";
				}
			}
			elseif($search_by == 3)
			{
				
				$pat_name = $this->security->xss_clean($this->input->post('pat_name'));
				
				
				 $where = "where p.FirstName LIKE '%$pat_name%'";
				
			}
			else if($search_by == 2)
			{

				$search_status = $this->security->xss_clean($this->input->post('search_status'));

				if($search_status == 1  ){

					$search_statusvaldb =" where u.Contacted = 1 ";

				}
				elseif($search_status == 2){

					$search_statusvaldb =" where u.Contacted = 2";
				}

				elseif($search_status == 3){
					$search_statusvaldb =" where p.Status = 1";
				}

				elseif($search_status == 4){
					$search_statusvaldb =" where p.Status = 32";
				}

				elseif($search_status == 5){
					$search_statusvaldb =" where p.Status = 12";
				}

				elseif($search_status == 6){ 
					$search_statusvaldb =" where p.Status in(3,4,17,18,20)";
				}

				elseif($search_status == 7){
					$search_statusvaldb =" where p.Status in(5,6,19,21,25)";
				}

				elseif($search_status == 8){
					$search_statusvaldb =" where p.Status in (7,8,22,26)";
				}

				elseif($search_status == 9){
					$search_statusvaldb =" where p.Status in (9,23,27)";
				}

				elseif($search_status == 10){
					$search_statusvaldb =" where p.Status in (10,28)";
				}

				elseif($search_status == 11){
					$search_statusvaldb =" where p.Status = 11";
				}

				elseif($search_status == 12){
					$search_statusvaldb =" where p.Status = 13";
				}

				elseif($search_status == 13){
					$search_statusvaldb =" where p.Status = 30";
				}

				elseif($search_status == 14){
					$search_statusvaldb =" where p.Status = 14";
				}

				elseif($search_status == 15){
					$search_statusvaldb =" where p.Status = 15";
				}
				elseif($search_status == 16){
					$search_statusvaldb =" where p.Status = 16";
				}
				elseif($search_status == 32){
					$search_statusvaldb =" where p.Status = 32";
				}
				elseif($search_status == 30){
					$search_statusvaldb =" where p.Status = 30";
				}

				$where = $search_statusvaldb;
			}
			else
			{
				$where = " where 1 ";
			}
				

		}

$LookupCodeval = '3,4,5,6,7,8,9,10,11,17,18,19,20,21,22,23,24,25,26,27,28';

//echo "<pre>";print_r($loginData);
		if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

$offset=0;
		/*Pahnation start*/
   $sql11 = "SELECT count(*) as count,p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID,p.TransferFromFacility,LookupCode,Mobile,d.DistrictName,p.Next_Visitdt,p.T_Initiation,p.Current_Visitdt,u.Contacted,u.ContactedRemark,u.CreatedOn from tblpatient p left join mstdistrict d on d.id_mstdistrict=p.District left join ltfu_contact u on u.PatientGUID=p.PatientGUID left join (SELECT * FROM mstlookup where Flag =13 and LanguageID = 1) b on p.status = b.LookupCode  ".$where."  ".$sess_where." and HepC=1 and  T_Initiation IS not NULL and status in($LookupCodeval) AND DATEDIFF (NOW(),Next_Visitdt ) >=7";
		 $patient_listcount = $this->db->query($sql11, $query_params)->result();
//echo $patient_listcount[0]->count;
				$config['total_rows'] = $patient_listcount[0]->count;
				 $content['total_count'] = $config['total_rows'];
				$config['suffix'] = '';

				if ($config['total_rows'] > 0) {
				 $page_number = $this->uri->segment(3);
				$config['base_url'] = base_url() . 'Ltfuinfo/index/';
				if (empty($page_number))
				$page_number = 1;
				
				$offset = ($page_number - 1) * $this->pagination->per_page;
				
				$content['offsetdata'] = ($page_number - 1) * $this->pagination->per_page;
				$content['perpage'] = $this->pagination->per_page;
				$content['pagecount'] =  $page_number;

				$this->pagination->cur_page = $page_number;
				$this->pagination->initialize($config);
				$content['page_links'] = $this->pagination->create_links();
}



			  $sql = "SELECT p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID,p.TransferFromFacility,LookupCode,Mobile,d.DistrictName,p.Next_Visitdt,p.T_Initiation,p.Current_Visitdt,u.Contacted,u.ContactedRemark,u.CreatedOn from tblpatient p left join mstdistrict d on d.id_mstdistrict=p.District left join ltfu_contact u on u.PatientGUID=p.PatientGUID left join (SELECT * FROM mstlookup where Flag =13 and LanguageID = 1) b on p.status = b.LookupCode ".$where."  ".$sess_where." and HepC=1 and status in($LookupCodeval)  and  T_Initiation IS not NULL and DATEDIFF (NOW(),Next_Visitdt ) >=7 order by p.CreatedOn DESC limit ".$this->pagination->per_page." offset ".$offset."";

		$patient_list = $this->db->query($sql, $query_params)->result();
		
		$content['patient_list'] = $patient_list;

		$content['patient_listCount'] = $patient_listcount[0]->count;

		$sql = "select * from mststate";
		$content['states'] = $this->db->query($sql)->result();

		$sql = "select * from mstdistrict";
		$content['districts'] = $this->db->query($sql)->result();

		$sql = "select * from mstfacility";
		$content['facilities'] = $this->db->query($sql)->result();	
		

		 $sql = "select flag from tblusers where id_tblusers = ".$loginData->id_tblusers."";
		$content['flag'] = $this->db->query($sql)->result();	


		$content['subview'] = 'ltfu_patient';
		$this->load->view('pages/main_layout', $content);
}


public function ltfu_process(){


			$loginData = $this->session->userdata('loginData');
			$PatientGUID = $this->input->get('patid');
			$arr = array();

			
			$this->db->where('PatientGUID', $PatientGUID);
					$this->db->delete('ltfu_contact');

			$data = array(
						'PatientGUID' => $this->security->xss_clean($this->input->get('patid')),
						'Contacted' => $this->security->xss_clean($this->input->get('Contacted')),
						'ContactedRemark' => $this->security->xss_clean(strip_tags($this->input->get('contactremarks'))),
						'CreatedOn' =>date('Y-m-d'),
						'CreatedBy' => $loginData->id_tblusers
					);

					$result = $this->db->insert('ltfu_contact', $data);

					if (count($result) >0) {
						$arr['status'] = 'true';
						$arr['message'] = '';
						$arr['fields'] = json_encode($result[0]);
					}else{

						$arr['status'] = 'false';
						$arr['message'] = '';
						$arr['fields'] = '';
					}
echo json_encode($arr);


}


public function ltfuhbv(){


		$loginData = $this->session->userdata('loginData');


		$gtid= $this->security->xss_clean($_GET['p']);
			if(!empty($gtid)){
		if($gtid==1){
			$LookupCode = "3,2,1,4,5,16";

		}elseif($gtid==2){
			$LookupCode = "3,2,1,4,5,12,30,14,15,13";
		}elseif($gtid==3){
			$LookupCode = "4,5,6,7,8,9,10,11,12,13,14,15";
		}else{
			$LookupCode = "0";
		}
	}else{
		$LookupCode = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16";
	}

	 if(!empty($gtid)){	
	 	if($gtid==2){

	 	$sql = "SELECT * FROM mstlookup where flag = 47  and LookupCode in($LookupCode) and LanguageID = 1 order by LookupCode ASC";
	 } elseif($gtid==1){
	 	$sql = "SELECT * FROM mstlookup where flag = 47  and LookupCode in($LookupCode) and LanguageID = 1 order by LookupCode ASC";
	 }
	 	else{

		$sql = "SELECT * FROM mstlookup where flag = 47  and LanguageID = 1 and LookupCode in($LookupCode) order by LookupCode ASC";
	 }	
}else{
	$sql = "SELECT * FROM mstlookup where flag = 47  and LanguageID = 1 and LookupCode in(7,8,9,10,11) order by LookupCode ASC";
}
		$content['status'] = $this->Common_Model->query_data($sql);


		$patient_list = array();
		$where = " where 1 ";

		$RequestMethod = $this->input->server('REQUEST_METHOD');
$query_params = [];
		if($RequestMethod == 'POST')
		{
			$search_by = $this->security->xss_clean($this->input->post('search_by'));

			

			if($search_by == 1)
			{
				
				$uid_contact = $this->security->xss_clean($this->input->post('uid_contact'));
				$query_params[]  = $uid_contact;									
				$uid_contact_len = strlen($uid_contact);
				if($uid_contact_len <= 6)
				{
					$where = " where p.UID_Num = ?";
				}
				else if($uid_contact_len > 6)
				{
					$where = " where p.Mobile = ?";
				}
			}
			elseif($search_by == 3)
			{
				
				$pat_name = $this->security->xss_clean($this->input->post('pat_name'));
				
				
				 $where = "where p.FirstName LIKE '%$pat_name%'";
				
			}
			else if($search_by == 2)
			{

				$search_status = $this->security->xss_clean($this->input->post('search_status'));

				if($search_status == 1  ){

					$search_statusvaldb =" where u.Contacted = 1 ";

				}
				elseif($search_status == 2){

					$search_statusvaldb =" where u.Contacted = 2";
				}

				

				$where = $search_statusvaldb;
			}
			else
			{
				$where = " where 1 ";
			}
				

		}

$LookupCodeval = '3,4,5,6,7,8,9,10,11,17,18,19,20,21,22,23,24,25,26,27,28';

//echo "<pre>";print_r($loginData);
		if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = "AND 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND p.id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
			}

$offset=0;
		/*Pahnation start*/
    $sql11 = "SELECT count(*) as count,p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID,p.TransferFromFacility,LookupCode,Mobile,d.DistrictName,dis.NextVisit as Next_Visitdt,dis.Treatment_Dt as T_Initiation,dis.Treatment_Dt as Current_Visitdt,u.Contacted,u.ContactedRemark,u.CreatedOn from tblpatient p inner join hepb_tblpatient hp on hp.PatientGUID=p.PatientGUID left join mstdistrict d on d.id_mstdistrict=p.District left join ltfu_contact u on u.PatientGUID=p.PatientGUID LEFT JOIN (select * from tblpatientdispensationb  group by PatientGUID order by PatientID DESC) dis on hp.PatientGUID=dis.PatientGUID  left join (SELECT * FROM mstlookup where Flag =72 AND LanguageID = 1) b on hp.status = b.LookupCode ".$where."  ".$sess_where." AND HbsAg = 1 AND dis.Treatment_Dt IS not NULL and DATEDIFF (NOW(),NextVisit ) >=7  AND (AntiHCV is null  OR (AntiHCV=1) ) and hp.Status=6";
		 $patient_listcount = $this->db->query($sql11, $query_params)->result();
		 //pr($patient_listcount);
//echo $patient_listcount[0]->count;
				$config['total_rows'] = $patient_listcount[0]->count;
				 $content['total_count'] = $config['total_rows'];
				$config['suffix'] = '';

				if ($config['total_rows'] > 0) {
				 $page_number = $this->uri->segment(3);
				$config['base_url'] = base_url() . 'Ltfuinfo/ltfuhbv/';
				if (empty($page_number))
				$page_number = 1;
				
				$offset = ($page_number - 1) * $this->pagination->per_page;
				
				$content['offsetdata'] = ($page_number - 1) * $this->pagination->per_page;
				$content['perpage'] = $this->pagination->per_page;
				$content['pagecount'] =  $page_number;

				$this->pagination->cur_page = $page_number;
				$this->pagination->initialize($config);
				$content['page_links'] = $this->pagination->create_links();
}



			  $sql = "SELECT p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID,p.TransferFromFacility,LookupCode,Mobile,d.DistrictName,dis.NextVisit as Next_Visitdt,dis.Treatment_Dt as T_Initiation,dis.Treatment_Dt as Current_Visitdt,u.Contacted,u.ContactedRemark,u.CreatedOn from tblpatient p left join hepb_tblpatient hp on hp.PatientGUID=p.PatientGUID left join mstdistrict d on d.id_mstdistrict=p.District left join ltfu_contact u on u.PatientGUID=p.PatientGUID LEFT JOIN tblpatientdispensationb dis on hp.PatientGUID=dis.PatientGUID  left join (SELECT * FROM mstlookup where Flag =72 AND LanguageID = 1) b on hp.status = b.LookupCode ".$where."  ".$sess_where." AND HbsAg = 1 AND dis.Treatment_Dt IS not NULL and DATEDIFF (NOW(),NextVisit ) >=7 AND (AntiHCV is null  OR (AntiHCV=1) ) and hp.Status=6 group by hp.PatientGUID order by p.CreatedOn DESC limit ".$this->pagination->per_page." offset ".$offset."";

		$patient_list = $this->db->query($sql, $query_params)->result();
		
		$content['patient_list'] = $patient_list;

		 $content['patient_listCount'] = $patient_listcount[0]->count;

		$sql = "select * from mststate";
		$content['states'] = $this->db->query($sql)->result();

		$sql = "select * from mstdistrict";
		$content['districts'] = $this->db->query($sql)->result();

		$sql = "select * from mstfacility";
		$content['facilities'] = $this->db->query($sql)->result();	
		

		 $sql = "select flag from tblusers where id_tblusers = ".$loginData->id_tblusers."";
		$content['flag'] = $this->db->query($sql)->result();	


		$content['subview'] = 'ltfu_patient_hepb';
		$this->load->view('pages/main_layout', $content);
}




}
