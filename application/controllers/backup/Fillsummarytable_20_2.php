<?php 

class Fillsummarytable extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_Model');
		$this->load->model('Patient_Model');
		 set_time_limit(100000);
        ini_set('memory_limit', '100000M');
        ini_set('upload_max_filesize', '300000M');
        ini_set('post_max_size', '200000M');
        ini_set('max_input_time', 100000);
        ini_set('max_execution_time', 0);
        ini_set('memory_limit','-1');
		$loginData = $this->session->userdata('loginData');
		//print_r($loginData);exit();
	/*	if($loginData->user_type == 1 )
		{
			$this->session->set_flashdata('er_msg','You are not logged-in, please login again to continue');
			redirect('login');	
		}*/

	}

	public function index()
	{

		for ($i=1; $i <= 3; $i++) { 
					$m=0;
					for ($j=$i; $j <= $i+1 ; $j++) { 
						$m++;
						echo $i ." ".$m .'<br/>';
					}}
		echo "nothing here, user fill_dates() or update_with_data() to do an operation";
		die();
	}
	public function fill_dates()
	{
		echo "started";
		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);exit();
		 //echo $sql_mstfacility = "select id_mstfacility,StateName from mstfacility m left join mststate s on m.id_mststate=s.id_mststate where id_mstfacility = '".$loginData->State_ID."'";exit();
		$sql_mstfacility ="select s.id_mststate,d.id_mstdistrict,m.id_mstfacility,s.StateName,m.facility_short_name,(case when dt is null then '2019-01-01'  else dt end) as dt from mstfacility m left join mststate s on m.id_mststate=s.id_mststate left join mstdistrict d on m.id_mstdistrict=d.id_mstdistrict left join (select id_mstfacility,date_add(max(date),INTERVAL 1 day) as dt from tblsummary_new group by id_mstfacility ) sf on sf.id_mstfacility = m.id_mstfacility   group by m.id_mstfacility
";
		$res_mstfacility = $this->Common_Model->query_data($sql_mstfacility);
		//echo "<pre>";print_r($res_mstfacility);exit();
		foreach ($res_mstfacility as $facility) {
			 $start_date = date_create($facility->dt);
			//echo Date('Y-m-d');exit();
			while($start_date < date_create(Date('Y-m-d')))
			{
				
				 $date_to_insert =  $start_date->format('Y-m-d');

				//for ($i=0; $i < 4; $i++) { 
					$insert_array = array(
						"id_mstfacility" => $facility->id_mstfacility,
						"Session_StateID" => $facility->id_mststate,
						"Session_DistrictID" => $facility->id_mstdistrict,
						'StateName'			=> $facility->StateName,
						"date"           => $date_to_insert
						);

					//print_r($insert_array);exit();
					$this->Common_Model->insert_data('tblsummary_new', $insert_array);
				//}
				date_add($start_date, date_interval_create_from_date_string("1 days"));
			}
			//break;
		}
			echo "done";
	}

/*monthly report summary start*/
	
	public function fill_monthly_dates()
	{
		echo "started";
		$loginData = $this->session->userdata('loginData'); 
		//print_r($loginData);exit();
		 //echo $sql_mstfacility = "select id_mstfacility,StateName from mstfacility m left join mststate s on m.id_mststate=s.id_mststate where id_mstfacility = '".$loginData->State_ID."'";exit();
		$sql_mstfacility ="select s.id_mststate,d.id_mstdistrict,m.id_mstfacility,s.StateName,m.facility_short_name,(case when dt is null then '2018-09-01'  else dt end) as dt from mstfacility m left join mststate s on m.id_mststate=s.id_mststate left join mstdistrict d on m.id_mstdistrict=d.id_mstdistrict left join (select id_mstfacility,date_add(max(date),INTERVAL 1 day) as dt from tblsummary_genderwise group by id_mstfacility ) sf on sf.id_mstfacility = m.id_mstfacility   group by m.id_mstfacility
";
		$res_mstfacility = $this->Common_Model->query_data($sql_mstfacility);
		//echo "<pre>";print_r($res_mstfacility);exit();
		foreach ($res_mstfacility as $facility) {
			 $start_date = date_create($facility->dt);
			//echo Date('Y-m-d');exit();
			while($start_date < date_create(Date('Y-m-d')))
			{
				
				 $date_to_insert =  $start_date->format('Y-m-d');

				for ($i=1; $i <= 3; $i++) { 
					$m=0;
					for ($j=$i; $j <= $i+1 ; $j++) { 
						$m++;
					$insert_array = array(
						"id_mstfacility" => $facility->id_mstfacility,
						"Session_StateID" => $facility->id_mststate,
						"Session_DistrictID" => $facility->id_mstdistrict,
						'StateName'			=> $facility->StateName,
						"date"           => $date_to_insert,
						"Gender"		=>$i,
						"Agewise"		=>$m
						);

					//print_r($insert_array);exit();
					$this->Common_Model->insert_data('tblsummary_genderwise', $insert_array);
				}
			}
				date_add($start_date, date_interval_create_from_date_string("1 days"));
			}
			//break;
		}
			echo "done";
	}

/*end monthly treport*/
	public function update_with_data()
	{

		$sql_max_date = "select max(date) as date from tblsummary";
		$res_max_date = $this->Common_Model->query_data($sql_max_date);
		$max_date = strtotime($res_max_date[0]->date); 
		if(date('Y-m-d') > date('Y-m-d',$max_date))
		{
			$sql_mstfacility = "select id_mstfacility from mstfacility";
			$res_mstfacility = $this->Common_Model->query_data($sql_mstfacility);
			foreach ($res_mstfacility as $facility) {
				
				$insert_array = array(
					"id_mstfacility" => $facility->id_mstfacility,
					"date" => date('Y-m-d'),
					);
				$this->Common_Model->insert_data('tblsummary', $insert_array);
		}
		}
		
		$sql_everything_null = "update tblsummary set antibody=null,antibodypositive=null,rna=null,chronichiv=null,treatmentinitiated=null,treatmentmid=null,treatmentcompleted=null, TreatmentSuccessful = null, Gen1= null,Gen2= null,Gen3= null, Gen4= null, Gen5= null, Gen6= null, SVR = null";
		$this->Common_Model->update_data_sql($sql_everything_null);

		$sql_update_antibody_positive = "update tblsummary s inner join (select id_mstfacility,T_AntiHCV01_Date as dt,count(patientguid) as Anti_Positive from tblpatient where T_AntiHCV01_Result=1 group by T_AntiHCV01_Date,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.AntibodyPositive=a.Anti_Positive";
		$this->Common_Model->update_data_sql($sql_update_antibody_positive);

		$sql_update_confirmatory_done = "update tblsummary s inner join (select id_mstfacility,T_DLL_01_VLC_Date as dt ,count(patientguid) as Confirmatory from tblpatient where T_DLL_01_VLC_Date is not null group by T_DLL_01_VLC_Date,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.RNA=a.Confirmatory";
		$this->Common_Model->update_data_sql($sql_update_confirmatory_done);

		$sql_update_chronic_hcv = "update tblsummary s inner join (select id_mstfacility,T_DLL_01_VLC_Date as dt ,count(patientguid) as Confirmatory from tblpatient where T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Result=1 group by T_DLL_01_VLC_Date,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.ChronicHIV=a.Confirmatory";
		$this->Common_Model->update_data_sql($sql_update_chronic_hcv);

		$sql_update_chronic_hcv_not_infected = "update tblsummary s inner join (select id_mstfacility,T_DLL_01_VLC_Date as dt ,count(patientguid) as Confirmatory from tblpatient where T_DLL_01_VLC_Date is not null and T_DLL_01_VLC_Result=2 group by T_DLL_01_VLC_Date,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.ChronicHIV_not_infected=a.Confirmatory";
		$this->Common_Model->update_data_sql($sql_update_chronic_hcv_not_infected);

		$sql_update_treatment_initiated = "update tblsummary s inner join (select id_mstfacility,(case when T_Initiation  <'2016-06-18' then '2016-06-18' else T_Initiation end) as dt,count(patientguid) as initiated from tblpatient where T_Initiation is not null group by (case when T_Initiation  <'2016-06-18' then '2016-06-18' else T_Initiation end),id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.TreatmentInitiated=a.initiated";
		$this->Common_Model->update_data_sql($sql_update_treatment_initiated);

		$sql_update_treatment_initiated_lfu = "update tblsummary s inner join (select id_mstfacility,date_add(T_DLL_01_VLC_Date, INTERVAL 31 DAY) as dt,count(patientguid) as TreatmentInitiated_lfu from tblpatient where T_DLL_01_VLC_Date is not null and T_Initiation is null group by date_add(T_DLL_01_VLC_Date, INTERVAL 31 DAY),id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.TreatmentInitiated_lfu=a.TreatmentInitiated_lfu";
		$this->Common_Model->update_data_sql($sql_update_treatment_initiated_lfu);

		$sql_update_treatment_completed = "update tblsummary s inner join ( select id_mstfacility,ETR_HCVViralLoad_Dt as dt ,count(patientguid) ETR from tblpatient where ETR_HCVViralLoad_Dt is not null group by ETR_HCVViralLoad_Dt,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.TreatmentCompleted=a.etr";
		$this->Common_Model->update_data_sql($sql_update_treatment_completed);

		$sql_update_treatment_completed_lfu = "update tblsummary s inner join ( select id_mstfacility,date_add(Next_Visitdt, INTERVAL 31 day) as dt ,count(patientguid) Treatment_completed_lfu from tblpatient where ETR_HCVViralLoad_Dt is null and NextVisitPurpose > 1 and NextVisitPurpose < 99 and Next_Visitdt is not null group by date_add(Next_Visitdt, INTERVAL 31 day),id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.TreatmentCompleted_lfu=a.Treatment_completed_lfu";
		$this->Common_Model->update_data_sql($sql_update_treatment_completed_lfu);

		$sql_update_svr = "update tblsummary s inner join ( select id_mstfacility,SVR12W_HCVViralLoad_Dt as dt ,count(patientguid) svr from tblpatient where SVR12W_HCVViralLoad_Dt is not null and status in (14,15) group by SVR12W_HCVViralLoad_Dt ,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.SVR=a.svr";
		$this->Common_Model->update_data_sql($sql_update_svr);

		$sql_update_svr = "update tblsummary s inner join ( select id_mstfacility,date_add(ETR_HCVViralLoad_Dt, INTERVAL 31 day) as dt ,count(patientguid) svr_lfu from tblpatient where SVR12W_HCVViralLoad_Dt is null and ETR_HCVViralLoad_Dt is not null group by date_add(ETR_HCVViralLoad_Dt, INTERVAL 31 day) ,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.SVR_lfu=a.svr_lfu";
		$this->Common_Model->update_data_sql($sql_update_svr);

		$sql_update_svr_test_done = "update tblsummary s inner join (select id_mstfacility,SVR12W_HCVViralLoad_Dt as dt ,count(patientguid) TreatmentSuccessful from tblpatient where SVR12W_HCVViralLoad_Dt is not null and status = 14 group by SVR12W_HCVViralLoad_Dt,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.SVRAchieved=a.TreatmentSuccessful";
		$this->Common_Model->update_data_sql($sql_update_svr_test_done);

		$sql_update_treatment_successful = "update tblsummary s inner join (select id_mstfacility,SVR12W_HCVViralLoad_Dt as dt ,count(patientguid) TreatmentSuccessful from tblpatient where SVR12W_HCVViralLoad_Dt is not null and status = 14 group by SVR12W_HCVViralLoad_Dt,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.TreatmentSuccessful=a.TreatmentSuccessful";
		$this->Common_Model->update_data_sql($sql_update_treatment_successful);

		$sql_update_treatment_not_successful = "update tblsummary s inner join (select id_mstfacility,SVR12W_HCVViralLoad_Dt as dt ,count(patientguid) Treatment_not_successful from tblpatient where SVR12W_HCVViralLoad_Dt is not null and status = 15 group by SVR12W_HCVViralLoad_Dt,id_mstfacility ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.Treatment_not_successful=a.Treatment_not_successful";
		$this->Common_Model->update_data_sql($sql_update_treatment_not_successful);

		$sql_update_gen1 = "update tblsummary s inner join (select id_mstfacility, GenotypeTest_Dt  as dt ,count(patientguid) as g1 from tblpatientcirrohosis where GenotypeTest_Dt  is not null and GenotypeTest_Result=1 group by id_mstfacility, (case when GenotypeTest_Dt  <'2016-06-18' then '2016-06-18' else GenotypeTest_Dt end)) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.Gen1 =a.g1";
		$this->Common_Model->update_data_sql($sql_update_gen1);

		$sql_update_gen2 = "update tblsummary s inner join (select id_mstfacility, GenotypeTest_Dt as dt ,count(patientguid) as g2 from tblpatientcirrohosis where GenotypeTest_Dt is not null and GenotypeTest_Result=2 group by id_mstfacility,(case when GenotypeTest_Dt  <'2016-06-18' then '2016-06-18' else GenotypeTest_Dt end) ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.Gen2 =a.g2";
		$this->Common_Model->update_data_sql($sql_update_gen2);

		$sql_update_gen3 = "update tblsummary s inner join (select id_mstfacility, GenotypeTest_Dt as dt ,count(patientguid) as g3 from tblpatientcirrohosis where GenotypeTest_Dt is not null and GenotypeTest_Result=3 group by id_mstfacility,(case when GenotypeTest_Dt  <'2016-06-18' then '2016-06-18' else GenotypeTest_Dt end) ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.Gen3 =a.g3";
		$this->Common_Model->update_data_sql($sql_update_gen3);

		$sql_update_gen4 = "update tblsummary s inner join (select id_mstfacility, GenotypeTest_Dt as dt ,count(patientguid) as g4 from tblpatientcirrohosis where GenotypeTest_Dt is not null and GenotypeTest_Result=4 group by id_mstfacility,(case when GenotypeTest_Dt  <'2016-06-18' then '2016-06-18' else GenotypeTest_Dt end) ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.Gen4 =a.g4";
		$this->Common_Model->update_data_sql($sql_update_gen4);

		$sql_update_gen5 = "update tblsummary s inner join (select id_mstfacility, GenotypeTest_Dt as dt ,count(patientguid) as g5 from tblpatientcirrohosis where GenotypeTest_Dt is not null and GenotypeTest_Result=5 group by id_mstfacility,(case when GenotypeTest_Dt  <'2016-06-18' then '2016-06-18' else GenotypeTest_Dt end) ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.Gen5 =a.g5";
		$this->Common_Model->update_data_sql($sql_update_gen5);

		$sql_update_gen6 = "update tblsummary s inner join (select id_mstfacility, GenotypeTest_Dt as dt ,count(patientguid) as g6 from tblpatientcirrohosis where GenotypeTest_Dt is not null and GenotypeTest_Result=6 group by id_mstfacility,(case when GenotypeTest_Dt  <'2016-06-18' then '2016-06-18' else GenotypeTest_Dt end) ) a on s.id_mstfacility=a.id_mstfacility and s.date=a.dt set s.Gen6 =a.g6";
		$this->Common_Model->update_data_sql($sql_update_gen6);

		echo "done";
	}

}