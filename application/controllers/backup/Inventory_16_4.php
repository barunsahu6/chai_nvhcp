<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$loginData = $this->session->userdata('loginData');
		$this->load->helper('common');
		$this->load->model('Common_Model');
		
		$this->load->library('form_validation');
		$this->load->model('Inventory_Model');
		if($loginData == null)
		{
			redirect('login');
		}
	}

	public function index($flag=NULL)
	{
		// die('heh');
		$loginData = $this->session->userdata('loginData');
		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
			$query="SELECT * FROM `mstlookup` WHERE flag='58'";
			$filter['item_mst_look']=$this->db->query($query)->result();
			//pr($filter['item_mst_look']);
		if($REQUEST_METHOD == 'POST')
		{
			$filter['Start_Date'] = timeStamp($this->input->post('startdate'));
			$filter['End_Date'] = timeStamp($this->input->post('enddate'));
			$filter['year'] = $this->input->post('year');
			 $filter['item_type']=$this->input->post('item_type');
		}	
		else
		{
			$filter['Start_Date'] = timeStamp(timeStampShow(date('Y-m-01')));
			$filter['End_Date'] = timeStamp(timeStampShow(date('Y-m-d')));
			$filter['item_type']='';
			if (date('m', strtotime(date('Y-m-d'))) < 4) {
			$filter['year'] =(date('Y')-1)."-".date("Y");
		}
		else{
			$filter['year'] =(date('Y'))."-".(date("Y")+1);
		}
		}
		$content['items']=$this->Inventory_Model->get_items();
		
		$this->session->set_userdata('invfilter',$filter);	
		if($flag=='R'){	
		$query="SELECT FacilityCode FROM `mstfacility` WHERE id_mstfacility=?";	
		$content['FacilityCode']=$this->db->query($query,[$loginData->id_mstfacility])->result();	
			$content['inventory_detail_all'] =$this->Inventory_Model->get_inventory_details($flag);
			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['Acceptance_Status']=$this->db->query($query,['76'])->result();

			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['source_name']=$this->db->query($query,['77'])->result();

			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['reason_for_rejection']=$this->db->query($query,['78'])->result();

			$query="SELECT inventory_id,indent_num FROM `tbl_inventory` where flag=?";
						$content['indent_num']=$this->db->query($query,['I'])->result();
				$content['receipt_items']=$this->Inventory_Model->get_items(NULL,NULL,'R');

			$content['subview']           = "inventory";
			$this->load->view('inventory/main_layout', $content);
		}
		else if($flag=='L'){		
			$query="SELECT FacilityCode FROM `mstfacility` WHERE id_mstfacility=?";	
		$content['FacilityCode']=$this->db->query($query,[$loginData->id_mstfacility])->result();	
			$content['inventory_detail_all'] =$this->Inventory_Model->get_inventory_details($flag);
			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['Acceptance_Status']=$this->db->query($query,['76'])->result();

			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['source_name']=$this->db->query($query,['77'])->result();

			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['reason_for_rejection']=$this->db->query($query,['78'])->result();

			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['relocation_status']=$this->db->query($query,['80'])->result();			

			$query="SELECT inventory_id,indent_num FROM `tbl_inventory` where flag=?";
						$content['indent_num']=$this->db->query($query,['I'])->result();
				$content['relocation_items']=$this->Inventory_Model->get_items(NULL,NULL,'L');
				//pr($content['relocation_items']);
			$content['subview']           = "relocation";
			$this->load->view('inventory/main_layout', $content);
		}
		else if($flag=='U'){		
			$query="SELECT FacilityCode FROM `mstfacility` WHERE id_mstfacility=?";	
			$content['FacilityCode']=$this->db->query($query,[$loginData->id_mstfacility])->result();
			$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag=?";
						$content['utilization_purpose']=$this->db->query($query,['79'])->result();
		
		$content['inventory_detail_all'] =$this->Inventory_Model->get_inventory_details($flag);
		$content['utilize_items']=$this->Inventory_Model->get_items(NULL,NULL,'F');
		//pr($content['inventory_detail_all']);exit();

		$content['subview']           = "stock_utilization";
		$this->load->view('inventory/main_layout', $content);
		}
else if($flag=='W'){		
			$content['inventory_details'] =$this->Inventory_Model->get_inventory_details($flag);
			$content['subview']           = "Wastage_Missing_Report";
			$this->load->view('inventory/main_layout', $content);
		}
		else if($flag=='I'){	
		$query="SELECT FacilityCode FROM `mstfacility` WHERE id_mstfacility=?";	
		$content['FacilityCode']=$this->db->query($query,[$loginData->id_mstfacility])->result();
		$content['inventory_detail_all'] =$this->Inventory_Model->get_inventory_details($flag);
		//pr($content['inventory_details']);exit();
		$content['indent_items']=$this->Inventory_Model->get_items(NULL,NULL,'I');
		//pr($content['indent_items']);
		$content['subview']           = "stock_indent_report";
		$this->load->view('inventory/main_layout', $content);
		}
		else if($flag=='F'){	
		$query="SELECT FacilityCode FROM `mstfacility` WHERE id_mstfacility=?";	
		$content['FacilityCode']=$this->db->query($query,[$loginData->id_mstfacility])->result();
		$content['inventory_detail_all'] =$this->Inventory_Model->get_inventory_details($flag);
		//pr($content['inventory_detail_all']);exit();
		$content['failure_items']=$this->Inventory_Model->get_items(NULL,NULL,'F');

		$content['subview']           = "stock_failure";
		$this->load->view('inventory/main_layout', $content);
		}
		else{
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			
		}
	}
public function Stock_indent(){
		//echo $inventory_id;
						$loginData = $this->session->userdata('loginData');
						$this->load->library('form_validation');
						$this->load->model('Inventory_Model');
						//echo "<pre>";print_r($loginData);
						
						if( ($loginData) && $loginData->user_type == '1' ){
						$sess_where = " 1";
						}
						elseif( ($loginData) && $loginData->user_type == '2' ){
						
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
						}
						elseif( ($loginData) && $loginData->user_type == '3' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
						}
						elseif( ($loginData) && $loginData->user_type == '4' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
						}
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						$State_ID=$loginData->State_ID;
						$DistrictID=$loginData->DistrictID;
						$id_mstfacility=$loginData->id_mstfacility;
						if($loginData->State_ID==''||$loginData->State_ID==NULL){
						 $State_ID=0;
						}
						if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
						$DistrictID=0;
						}
						if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
						$id_mstfacility=0;
						}
						//print_r($loginData);
						if ($this->security->xss_clean($this->input->post('save'))=='save') {

						$item_id=$this->security->xss_clean($this->input->post('item_name'));

						$inventory_id=$this->security->xss_clean($this->input->post('inventory_id'));
						 $this->form_validation->set_rules('quantity', 'quantity', 'trim|required|numeric|xss_clean|greater_than[0]');
						  $this->form_validation->set_rules('item_name', 'Item Name', 'trim|required|numeric|xss_clean');
					 $this->form_validation->set_rules('indent_num', 'indent Number', 'trim|required|xss_clean'); 
					 $this->form_validation->set_rules('indent_remark', 'indent Remark', 'trim|max_length[100]|xss_clean');

					if ($this->form_validation->run() == FALSE) {

						$arr['status'] = 'false';
						$this->session->set_flashdata("tr_msg",validation_errors());
						//echo("arg1");
						redirect('Inventory/index/I');
					}else{
						$item_name_ar=$this->Inventory_Model->get_items($item_id);
						
						$item_name=$item_name_ar[0]->id_mst_drugs;
						$item_type=$item_name_ar[0]->type;

						if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$item_name_ar[0]->strength*$this->security->xss_clean($this->input->post('quantity'));
						}
						else{
						
						$screening_tests=0;
						}
						$insert_array = array(  
						'id_mst_drugs'  =>$item_id,
						'drug_name'  =>$item_name,
						'type'=>$item_type,
						'indent_date'=>$this->security->xss_clean(timeStamp($this->input->post('indent_date'))),
						'indent_num'=>$this->security->xss_clean($this->input->post('indent_num')),
						'indent_remark'=>$this->security->xss_clean($this->input->post('indent_remark')),
						'quantity'     => $this->security->xss_clean($this->input->post('quantity')),
						'quantity_screening_tests'     => $screening_tests, 
						'id_mststate'   =>  $State_ID,
						'id_mstdistrict' => $DistrictID,
						'id_mstfacility'     => $id_mstfacility,
						'indent_status'=>0,
						'flag'=>	'I',
						'is_deleted'=>'0'
						);
						
						//pr($data);
						//echo $inventory_id;exit;
						if ($inventory_id!=NULL) {
						$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->where('inventory_id',$inventory_id);
						$this->db->update('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Updated Successfully');
						redirect('Inventory/index/I');
						}
						else{
						$array2 = array("id_tblusers" => $loginData->id_tblusers, "CreatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->insert('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Added Successfully');
						redirect('Inventory/index/I');
						}	
						}
					}
					//redirect('Inventory/index/I');
				}
			
	}

function get_sequence_no($drug_name=NULL,$indent_date=NULL){
	$drug_name_val = $this->input->get('drug_name',TRUE);
	$indent_date1 =timeStamp($this->input->get('indent_date',TRUE));
	$loginData = $this->session->userdata('loginData');
	$query="SELECT IFNULL(count(drug_name),0) as count FROM tbl_inventory WHERE drug_name=? and indent_date=? and id_mstfacility=? and flag=?";
							$data['item_sequence']=$this->db->query($query,[$drug_name_val,$indent_date1,$loginData->id_mstfacility,'I'])->result();
	echo json_encode($data);						
}

function get_issue_batch_sequence(){
	$drug_name_val = $this->input->get('drug_name',TRUE);
	$Entry_Date =timeStamp($this->input->get('Entry_Date',TRUE));
	$loginData = $this->session->userdata('loginData');
	$query="SELECT IFNULL(count(issue_num),0) as issue_count,IFNULL(count(batch_num),0) as batch_count FROM tbl_inventory WHERE drug_name=? and Entry_Date=? and id_mstfacility=? and flag=?";
							$data['item_sequence']=$this->db->query($query,[$drug_name_val,$Entry_Date,$loginData->id_mstfacility,'R'])->result();
	echo json_encode($data);						
}

function get_indent_num(){
	$item_name = $this->input->get('item_name',TRUE);
	$loginData = $this->session->userdata('loginData');
	

	$query="SELECT rem,TYPE,indent_num FROM 
(SELECT Flag,inventory_id FROM tbl_inventory WHERE (Flag='I' OR Flag='R') AND is_deleted =? AND id_mst_drugs=? and id_mstfacility=? ) AS p
LEFT JOIN 
(SELECT SUM((quantity-quantity_received)+quantity_rejected)as rem,type,indent_num,inventory_id from tbl_inventory where is_deleted =? AND id_mst_drugs=? and id_mstfacility=? GROUP BY indent_num
) AS i1
ON p.inventory_id=i1.inventory_id
 WHERE i1.rem>0";

							$data['get_indent_num']=$this->db->query($query,['0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility])->result();
	echo json_encode($data);						
}
function get_batch_num(){
	$item_name = $this->input->get('item_name',TRUE);
	$loginData = $this->session->userdata('loginData');
	$query="SELECT DISTINCT type,batch_num,Entry_Date,quantity_received FROM tbl_inventory where is_deleted=? AND id_mst_drugs=? and id_mstfacility=? and flag=? and quantity_received is not null and quantity_received>0 and Entry_Date>DATE_ADD(Now(), INTERVAL-6 MONTH);";
							$data['get_batch_num']=$this->db->query($query,['0',$item_name,$loginData->id_mstfacility,'R'])->result();
	echo json_encode($data);						
}
function edit_inventory_data(){
	$inventory_id = $this->input->get('inventory_id',TRUE);
	$loginData = $this->session->userdata('loginData');
	$query="SELECT * FROM tbl_inventory where is_deleted=? AND inventory_id=? and id_mstfacility=?";
							$data['inventory_details']=$this->db->query($query,['0',$inventory_id,$loginData->id_mstfacility])->result();
	echo json_encode($data);						
}

function get_data_for_utilize(){
	$item_name = $this->input->get('item_name',TRUE);
	$indent_num = $this->input->get('indent_num',TRUE);
	$loginData = $this->session->userdata('loginData');
	if ($indent_num!='') {
		$arr=['0',$item_name,$indent_num,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility];
		$indent_qry=" AND indent_num=? ";
	}
	else{
		$arr=['0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility,'0',$item_name,$loginData->id_mstfacility];
		$indent_qry="";
	}
	$query="SELECT i1.rem,TYPE,i1.batch_num,i3.to_Date,case when i2.to_Date IS NULL then '0' ELSE '1' end 	AS to_Date_flag FROM 
				(SELECT Flag,inventory_id,batch_num FROM tbl_inventory WHERE (Flag='R' OR FLag='F' OR FLag='U') AND is_deleted =? AND id_mst_drugs=?".$indent_qry." and id_mstfacility=? ) AS p
			LEFT JOIN 
				(SELECT SUM(quantity_received-(quantity_rejected+dispensed_quantity+control_used+returned_quantity+relocated_quantity))as rem,type,batch_num,inventory_id from tbl_inventory where is_deleted =? AND id_mst_drugs=? and id_mstfacility=? GROUP BY batch_num ) AS i1
			ON p.inventory_id=i1.inventory_id
			LEFT JOIN 
				(SELECT to_Date,inventory_id,batch_num from tbl_inventory where is_deleted =? AND id_mst_drugs=? and id_mstfacility=? and Flag='U' GROUP BY batch_num ) AS i2
			ON p.batch_num=i2.batch_num
			LEFT JOIN 
				(SELECT inventory_id,batch_num,MAX(GREATEST(COALESCE((to_Date), 0),COALESCE((Entry_Date), 0))) AS to_Date from tbl_inventory where is_deleted =? AND id_mst_drugs=? and id_mstfacility=? and (Flag='U' or Flag='R') GROUP BY batch_num ) AS i3
			ON p.batch_num=i3.batch_num
 			WHERE i1.rem>0";
							$data['get_batch_num']=$this->db->query($query,$arr)->result();
	echo json_encode($data);						
}
function delete_receipt($inventory_id=NULL,$flag=NULL){

$this->Inventory_Model->delete_receipt($inventory_id);
if($flag=='R'){
	redirect('Inventory');
}
else if($flag=='L'){
	redirect('Inventory/index/L');
}
else if($flag=='U'){
	redirect('Inventory/index/U');
}

else if($flag=='F'){
	redirect('Inventory/index/F');
}
else if($flag=='I'){
	redirect('Inventory/index/I');
}
}

	public function AddReceipt($inventory_id=NULL){
		//echo $inventory_id;
						$loginData = $this->session->userdata('loginData');
						//echo "<pre>";print_r($loginData);
						
						if( ($loginData) && $loginData->user_type == '1' ){
						$sess_where = " 1";
						}
						elseif( ($loginData) && $loginData->user_type == '2' ){
						
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
						}
						elseif( ($loginData) && $loginData->user_type == '3' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
						}
						elseif( ($loginData) && $loginData->user_type == '4' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
						}
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						$State_ID=$loginData->State_ID;
						$DistrictID=$loginData->DistrictID;
						$id_mstfacility=$loginData->id_mstfacility;
						if($loginData->State_ID==''||$loginData->State_ID==NULL){
						 $State_ID=0;
						}
						if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
						$DistrictID=0;
						}
						if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
						$id_mstfacility=0;
						}
						if ($this->security->xss_clean($this->input->post('save'))=='save') {
						$item_id=$this->security->xss_clean($this->input->post('item_name'));
						$item_name_ar=$this->Inventory_Model->get_items($item_id);
						$from_to_type=$this->security->xss_clean($this->input->post('from_to_type'));
						
						$item_name=$item_name_ar[0]->id_mst_drugs;
						$item_type=$item_name_ar[0]->type;
						
						
						/*if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$item_name_ar[0]->strength*$this->input->post('quantity');
						}
						else{
						
						$screening_tests=0;
						}*/
						if($from_to_type==3 || $from_to_type==4){
							 $unrecognised=$this->security->xss_clean($this->input->post('unrecognised'));
							$source_name=NULL;
						}
						else{
							$source_name=$this->security->xss_clean($this->input->post('source_name'));
							$unrecognised=NULL;
						}
						$insert_array         = array(  
						'id_mst_drugs'        =>$item_id,
						'drug_name'           =>$item_name,
						'type'                =>$item_type,
						'indent_num'          => $this->security->xss_clean($this->input->post('indent_num')),  
						'batch_num'           => $this->security->xss_clean($this->input->post('batch_num')),  
						'issue_num'           => $this->security->xss_clean($this->input->post('issue_num')),  
						'Entry_Date'          => timeStamp($this->security->xss_clean($this->input->post('Entry_Date'))),
						'approved_quantity'   => $this->security->xss_clean($this->input->post('approved_quantity')),
						'quantity_dispatched' => $this->security->xss_clean($this->input->post('quantity_dispatched')),
						'quantity_received'   => $this->security->xss_clean($this->input->post('quantity_received')),
						'quantity_rejected'   => $this->security->xss_clean($this->input->post('quantity_rejected')),
						'Expiry_Date'         => timeStamp($this->security->xss_clean($this->input->post('Expiry_Date'))),  
						'from_to_type'        => $this->security->xss_clean($this->input->post('from_to_type')),  
						'source_name'         => $source_name,
						'unrecognised'        =>$unrecognised,
						'Acceptance_Status'   => $this->security->xss_clean($this->input->post('Acceptance_Status')),  
						'rejection_reason'    => $this->security->xss_clean($this->input->post('rejection_reason')),  
						'id_mststate'         =>  $State_ID,
						'id_mstdistrict'      => $DistrictID,
						'id_mstfacility'      => $id_mstfacility,
						'flag'                =>	'R',
						'is_deleted'          =>'0' 
						);
						
						//pr($data);exit();
						//echo $inventory_id;exit;
						if ($inventory_id!=NULL) {
						$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->where('inventory_id',$inventory_id);
						$this->db->update('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Receipt Updated Successfully');
						redirect('Inventory/index/R');
						}
						else{
						$array2 = array("id_tblusers" => $loginData->id_tblusers, "CreatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->insert('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Receipt Added Successfully');
						redirect('Inventory/index/R');
						}	
						
					}
				}
			
	}

public function stock_failure(){
		//echo $inventory_id;
						$loginData = $this->session->userdata('loginData');
						$this->load->library('form_validation');
						$this->load->model('Inventory_Model');
						//echo "<pre>";print_r($loginData);
						
						if( ($loginData) && $loginData->user_type == '1' ){
						$sess_where = " 1";
						}
						elseif( ($loginData) && $loginData->user_type == '2' ){
						
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
						}
						elseif( ($loginData) && $loginData->user_type == '3' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
						}
						elseif( ($loginData) && $loginData->user_type == '4' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
						}
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						$State_ID=$loginData->State_ID;
						$DistrictID=$loginData->DistrictID;
						$id_mstfacility=$loginData->id_mstfacility;
						if($loginData->State_ID==''||$loginData->State_ID==NULL){
						 $State_ID=0;
						}
						if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
						$DistrictID=0;
						}
						if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
						$id_mstfacility=0;
						}
						//print_r($loginData);
						if ($this->security->xss_clean($this->input->post('save'))=='save') {

						$item_id=$this->security->xss_clean($this->input->post('item_name'));

						$inventory_id=$this->security->xss_clean($this->input->post('inventory_id'));
						 $this->form_validation->set_rules('returned_quantity', 'returned quantity', 'trim|required|numeric|xss_clean|greater_than[0]');
						  $this->form_validation->set_rules('item_name', 'Item Name', 'trim|required|numeric|xss_clean');
					 $this->form_validation->set_rules('batch_num', 'Batch Number', 'trim|required|xss_clean');
					  $this->form_validation->set_rules('failure_issue', 'Failure Issue', 'trim|required|max_length[100]|xss_clean');
					  $this->form_validation->set_rules('failure_date', 'Failure Date', 'trim|required|xss_clean');

					if ($this->form_validation->run() == FALSE) {

						$arr['status'] = 'false';
						$this->session->set_flashdata("tr_msg",validation_errors());
						//echo("arg1");
						redirect('Inventory/index/F');
					}else{
						$item_name_ar=$this->Inventory_Model->get_items($item_id);
						
						$item_name=$item_name_ar[0]->id_mst_drugs;
						$item_type=$item_name_ar[0]->type;

						if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$item_name_ar[0]->strength*$this->security->xss_clean($this->input->post('quantity'));
						}
						else{
						
						$screening_tests=0;
						}
						$batch_num=$this->security->xss_clean($this->input->post('batch_num'));
						$query="SELECT DISTINCT indent_num FROM tbl_inventory WHERE batch_num=? AND indent_num IS NOT NULL";
						$get_indent_num=$this->db->query($query,[$batch_num])->result();
						$insert_array = array(  
						'id_mst_drugs'  =>$item_id,
						'drug_name'  =>$item_name,
						'indent_num' =>$get_indent_num[0]->indent_num,
						'type'=>$item_type,
						'failure_date'=>$this->security->xss_clean(timeStamp($this->input->post('failure_date'))),
						'batch_num'=>$this->security->xss_clean($this->input->post('batch_num')),
						'failure_issue'=>$this->security->xss_clean($this->input->post('failure_issue')),
						'returned_quantity'     => $this->security->xss_clean($this->input->post('returned_quantity')),
						'quantity_screening_tests'     => $screening_tests, 
						'id_mststate'   =>  $State_ID,
						'id_mstdistrict' => $DistrictID,
						'id_mstfacility'     => $id_mstfacility,
						'indent_status'=>0,
						'flag'=>	'F',
						'is_deleted'=>'0'
						);
						
						//pr($data);
						//echo $inventory_id;exit;
						if ($inventory_id!=NULL) {
						$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->where('inventory_id',$inventory_id);
						$this->db->update('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Failure Updated Successfully');
						redirect('Inventory/index/F');
						}
						else{
						$array2 = array("id_tblusers" => $loginData->id_tblusers, "CreatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->insert('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Failure Added Successfully');
						redirect('Inventory/index/F');
						}	
						}
					}
				}
			
	}

public function stock_utilization(){
		//echo $inventory_id;
						$loginData = $this->session->userdata('loginData');
						$this->load->library('form_validation');
						$this->load->model('Inventory_Model');
						//echo "<pre>";print_r($loginData);
						
						if( ($loginData) && $loginData->user_type == '1' ){
						$sess_where = " 1";
						}
						elseif( ($loginData) && $loginData->user_type == '2' ){
						
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
						}
						elseif( ($loginData) && $loginData->user_type == '3' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
						}
						elseif( ($loginData) && $loginData->user_type == '4' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
						}
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						$State_ID=$loginData->State_ID;
						$DistrictID=$loginData->DistrictID;
						$id_mstfacility=$loginData->id_mstfacility;
						if($loginData->State_ID==''||$loginData->State_ID==NULL){
						 $State_ID=0;
						}
						if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
						$DistrictID=0;
						}
						if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
						$id_mstfacility=0;
						}
						//print_r($loginData);
						if ($this->security->xss_clean($this->input->post('save'))=='save') {

						$item_id=$this->security->xss_clean($this->input->post('item_name'));

						$inventory_id=$this->security->xss_clean($this->input->post('inventory_id'));
						 $this->form_validation->set_rules('item_name', 'Item Name', 'trim|required|numeric|xss_clean');
					 $this->form_validation->set_rules('batch_num', 'Batch Number', 'trim|required|xss_clean');
					$this->form_validation->set_rules('from_Date', 'From Date', 'trim|required|xss_clean');
					  $this->form_validation->set_rules('to_Date', 'To Date', 'trim|required|xss_clean');
						 $this->form_validation->set_rules('dispensed_quantity', 'Dispensed quantity', 'trim|required|numeric|xss_clean|greater_than[0]');
						  $this->form_validation->set_rules('dispensed_quantity', 'Dispensed quantity', 'trim|required|numeric|xss_clean|greater_than[0]');
						  $this->form_validation->set_rules('repeat_quantity', 'Repeat quantity', 'trim|required|numeric|xss_clean|greater_than[0]');
						  $this->form_validation->set_rules('control_used', 'Number of Control Used', 'trim|required|numeric|xss_clean|greater_than[0]');
						 
					  $this->form_validation->set_rules('utilization_purpose', 'Purpose of Utilization', 'trim|required|xss_clean');
					

					if ($this->form_validation->run() == FALSE) {

						$arr['status'] = 'false';
						$this->session->set_flashdata("tr_msg",validation_errors());
						//echo("arg1");
						redirect('Inventory/index/U');
					}else{
						$item_name_ar=$this->Inventory_Model->get_items($item_id);
						
						$item_name=$item_name_ar[0]->id_mst_drugs;
						$item_type=$item_name_ar[0]->type;

						if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$item_name_ar[0]->strength*$this->security->xss_clean($this->input->post('quantity'));
						}
						else{
						
						$screening_tests=0;
						}
						$batch_num=$this->security->xss_clean($this->input->post('batch_num'));
						$query="SELECT DISTINCT indent_num FROM tbl_inventory WHERE batch_num=? AND indent_num IS NOT NULL";
						$get_indent_num=$this->db->query($query,[$batch_num])->result();
						// echo $get_indent_num[0]->indent_num;exit;
						$insert_array = array(  
						'id_mst_drugs'  =>$item_id,
						'drug_name'  =>$item_name,
						'type'=>$item_type,
						'indent_num'=>$get_indent_num[0]->indent_num,
						'batch_num'=>$this->security->xss_clean($this->input->post('batch_num')),
						'from_Date'=>$this->security->xss_clean(timeStamp($this->input->post('from_Date'))),
						'to_Date'=>$this->security->xss_clean(timeStamp($this->input->post('to_Date'))),
						'dispensed_quantity'     => $this->security->xss_clean($this->input->post('dispensed_quantity')),
						'repeat_quantity'     => $this->security->xss_clean($this->input->post('repeat_quantity')),
						'control_used'     => $this->security->xss_clean($this->input->post('control_used')),
						'available_quantity'   => $this->security->xss_clean($this->input->post('available_quantity')),
						'utilization_purpose'     => $this->security->xss_clean($this->input->post('utilization_purpose')),
						'quantity_screening_tests'     => $screening_tests, 
						'id_mststate'   =>  $State_ID,
						'id_mstdistrict' => $DistrictID,
						'id_mstfacility'     => $id_mstfacility,
						'indent_status'=>0,
						'flag'=>	'U',
						'is_deleted'=>'0'
						);
						
						//pr($data);
						
						if ($inventory_id!=NULL) {
						$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->where('inventory_id',$inventory_id);
						$this->db->update('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Failure Updated Successfully');
						redirect('Inventory/index/U');
						}
						else{
						$array2 = array("id_tblusers" => $loginData->id_tblusers, "CreatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->insert('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Stock Failure Added Successfully');
						redirect('Inventory/index/U');
						}	
						}
					}
				}
			
	}
public function relocation($inventory_id=NULL){
		//echo $inventory_id;
						$this->load->library('form_validation');
						$loginData = $this->session->userdata('loginData');
						//echo "<pre>";print_r($loginData);
						
						if( ($loginData) && $loginData->user_type == '1' ){
						$sess_where = " 1";
						}
						elseif( ($loginData) && $loginData->user_type == '2' ){
						
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
						}
						elseif( ($loginData) && $loginData->user_type == '3' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
						}
						elseif( ($loginData) && $loginData->user_type == '4' ){ 
						$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
						}
						
						$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
						
						if($REQUEST_METHOD == 'POST')
						{
						$loginData = $this->session->userdata('loginData');
						$State_ID=$loginData->State_ID;
						$DistrictID=$loginData->DistrictID;
						$id_mstfacility=$loginData->id_mstfacility;
						if($loginData->State_ID==''||$loginData->State_ID==NULL){
						 $State_ID=0;
						}
						if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
						$DistrictID=0;
						}
						if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
						$id_mstfacility=0;
						}
						if ($this->security->xss_clean($this->input->post('save'))=='save') {
							 
			$this->form_validation->set_rules('item_name', 'Item Name', 'trim|required|numeric|xss_clean');
			

			$this->form_validation->set_rules('indent_num', 'Indent Number', 'trim|required|xss_clean');
			
			$this->form_validation->set_rules('batch_num', 'Batch Number', 'trim|required|xss_clean');

			$this->form_validation->set_rules('issue_num', 'Issue Number', 'trim|required|xss_clean');
			
			$this->form_validation->set_rules('request_date', 'Request Date', 'trim|required|xss_clean');
			
			$this->form_validation->set_rules('dispatch_date', 'Dispatch Date', 'trim|required|xss_clean');
			
			$this->form_validation->set_rules('relocated_quantity', 'Relocated quantity', 'trim|required|numeric|xss_clean|greater_than[0]');
			 
			 $this->form_validation->set_rules('from_to_type', 'Facility/State Warehouse', 'trim|required|numeric|xss_clean');

			$this->form_validation->set_rules('requested_quantity', 'Request quantity', 'trim|required|numeric|xss_clean|greater_than[0]');  
					
			$this->form_validation->set_rules('relocation_remark', 'Remark', 'trim|xss_clean');  
					if ($this->form_validation->run() == FALSE) {

						$arr['status'] = 'false';
						$this->session->set_flashdata("tr_msg",validation_errors());
						//echo("arg1");
						redirect('Inventory/index/L');
					}else{
						$item_id=$this->security->xss_clean($this->input->post('item_name'));
						$item_name_ar=$this->Inventory_Model->get_items($item_id);
						$from_to_type=$this->security->xss_clean($this->input->post('from_to_type'));
						
						$item_name=$item_name_ar[0]->id_mst_drugs;
						$item_type=$item_name_ar[0]->type;
						
						
						/*if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$item_name_ar[0]->strength*$this->input->post('quantity');
						}
						else{
						
						$screening_tests=0;
						}*/
						/*if($this->input->post('from_to_type')==3){
						  $unrecognised=$this->input->post('unrecognised');
							$source_name=NULL;
							$transfer_to=NULL;
						}
						else{
							$source_name=$this->input->post('source_name');
							$unrecognised=NULL;
							$transfer_to=$this->input->post('destination_name');
						}*/
						if($from_to_type==3 || $from_to_type==4){
							 $unrecognised=$this->security->xss_clean($this->input->post('unrecognised'));
							$transfer_to=NULL;
						}
						else{
							$transfer_to=$this->security->xss_clean($this->input->post('source_name'));
							$unrecognised=NULL;
						}
						$insert_array         = array(  
						'id_mst_drugs'        =>$item_id,
						'drug_name'           =>$item_name,
						'type'                =>$item_type,
						'indent_num'          => $this->security->xss_clean($this->input->post('indent_num')),  
						'batch_num'           => $this->security->xss_clean($this->input->post('batch_num')),  
						'issue_num'           => $this->security->xss_clean($this->input->post('issue_num')),  
						'requested_quantity'          => $this->security->xss_clean($this->input->post('requested_quantity')),
						'from_to_type'        => $this->security->xss_clean($this->input->post('from_to_type')),  
						'transfer_to'         => $transfer_to,
						'unrecognised'        =>$unrecognised,
						'request_date'         => timeStamp($this->security->xss_clean($this->input->post('request_date'))),  
						'dispatch_date'         => timeStamp($this->security->xss_clean($this->input->post('dispatch_date'))),  
						'relocated_quantity' => $this->security->xss_clean($this->input->post('relocated_quantity')),
						'relocation_remark'    => $this->security->xss_clean($this->input->post('relocation_remark')),  
						'relocation_status'         => 1,
						'id_mststate'         =>  $State_ID,
						'id_mstdistrict'      => $DistrictID,
						'id_mstfacility'      => $id_mstfacility,
						'flag'                =>	'L',
						'is_deleted'          =>'0' 
						);
						/*echo "<pre>";
						print_r($insert_array);exit();*/
						//echo $inventory_id;exit;
						if ($inventory_id!=NULL) {
						$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->where('inventory_id',$inventory_id);
						$this->db->update('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Relocation Updated Successfully');
						redirect('Inventory/index/L');
						}
						else{
						$array2 = array("id_tblusers" => $loginData->id_tblusers, "CreatedOn" => date('Y-m-d'));
						$data = array_merge($insert_array, $array2);
						$this->db->insert('tbl_inventory',$data);
						$this->session->set_flashdata('tr_msg','Relocation Added Successfully');
						redirect('Inventory/index/L');
						}	
						}
					}
				}
			
	}

function AddTransferOut($inventory_id=NULL){
		//echo $inventory_id;
							$loginData = $this->session->userdata('loginData');
							//echo "<pre>";print_r($loginData);
							
							if( ($loginData) && $loginData->user_type == '1' ){
							$sess_where = " 1";
							}
							elseif( ($loginData) && $loginData->user_type == '2' ){
							
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
							}
							elseif( ($loginData) && $loginData->user_type == '3' ){ 
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
							}
							elseif( ($loginData) && $loginData->user_type == '4' ){ 
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
							}
							
							$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
							
							if($REQUEST_METHOD == 'POST')
							{
							$loginData = $this->session->userdata('loginData');
							$State_ID=$loginData->State_ID;
							$DistrictID=$loginData->DistrictID;
							$id_mstfacility=$loginData->id_mstfacility;
							if($loginData->State_ID==''||$loginData->State_ID==NULL){
							$State_ID=0;
							}
							if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
							$DistrictID=0;
							}
							if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
							$id_mstfacility=0;
							}
							//print_r($loginData);
							$item_id=$this->input->post('item_type');
							$item_name_ar=$this->Inventory_Model->get_items($item_id);
							
							if($item_id!=999)
							{
							$item_id=$this->input->post('item_type');
							$item_name=$item_name_ar[0]->id_mst_drugs;
							$item_type=$item_name_ar[0]->type;
							//echo $item_name[0]->drug_name;exit();
							}
							else{
							$item_id=$this->input->post('item_type');
							$batch_num   = $this->input->post('batch_num');
							$get_item_details=$this->Inventory_Model->get_receipt_in_transfer_out($item_id,$batch_num);
							//print_r($item_name_ar);
							$item_name=$get_item_details[0]->drug_name;
							$item_type=$get_item_details[0]->type;
							}
							if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$item_name_ar[0]->strength*$this->input->post('quantity');
						}
						else{
						
						$screening_tests=0;
						}
						if($this->input->post('from_to_type')==3){
						  $unrecognised=$this->input->post('unrecognised');
							$source_name=NULL;
							$transfer_to=NULL;
						}
						else{
							$source_name=$this->input->post('source_name');
							$unrecognised=NULL;
							$transfer_to=$this->input->post('destination_name');
						}
							$data = array(  
							'id_mst_drugs'  =>$item_id,
							'drug_name'  =>$item_name,
							'type'=>$item_type,
							'batch_num'   => $this->input->post('batch_num'),  
							'Entry_Date' => timeStamp($this->input->post('Entry_Date')),
							'quantity'     => $this->input->post('quantity'), 
							'quantity_screening_tests'     => $screening_tests,
							/*  'Expiry_Date'  => timeStamp($this->input->post('Expiry_Date')),*/  
							'from_to_type'   => $this->input->post('from_to_type'),  
							'source_name' => $source_name,
							'unrecognised'=>$unrecognised,
							/* 'Acceptance_Status'     => $this->input->post('Acceptance_Status'),*/  
							/* 'rejection'  => $this->input->post('rejection'), */  
							'id_mststate'   =>  $State_ID,
							'id_mstdistrict' => $DistrictID,
							'id_mstfacility'     => $id_mstfacility, 
							'transfer_to'=> $transfer_to, 
							'id_tblusers'  => $loginData->id_tblusers ,
							'flag'=>	'T',
							'is_deleted'=>'0'
							);
							
							//print_r($data);
							//echo $inventory_id;exit;
							if ($inventory_id!=NULL) {
							$this->db->where('inventory_id',$inventory_id);
							$this->db->update('tbl_inventory',$data);
							$this->session->set_flashdata('tr_msg','Receipt Updated Successfully');
							redirect('Inventory/AddTransferOut');
							}
							else{
							$this->db->insert('tbl_inventory',$data);
							$this->session->set_flashdata('tr_msg','Transfer Out Information Added Successfully');
							redirect('Inventory/AddTransferOut');
							}	
							}
							if($inventory_id!=NULL){
							//echo $inventory_id;
							$res_details = $this->Inventory_Model->edit_inventory_receipt($inventory_id);
							$content['receipt_details'] = $res_details;
							$content['edit_flag'] = 1;
							//print_r($res_details);
							}
							else{
							$content['edit_flag'] = 0;
							}
							$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag='58'";
							$content['item_type']=$this->db->query($query)->result();
							$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag='59'";
							$content['source_name']=$this->db->query($query)->result();
							$content['receiptitems']=$this->Inventory_Model->get_receipt_in_transfer_out();
							$content['mst_drug']=$this->Inventory_Model->get_items();
							$content['subview'] = "add_transfer_out";
							$this->load->view('inventory/main_layout', $content);
}
/*function get_batch_num(){
	$this->load->model('Inventory_Model');
        $id_mst_drugs = $this->input->post('id',TRUE);
        $data['batch_nums'] = $this->Inventory_Model->get_receipt_in_transfer_out($id_mst_drugs);
        echo json_encode($data);
    }*/

function get_facilities()
{
		//$id_mstfacility = $this->input->post('id',TRUE);
	 $this->load->model('Inventory_Model');
        $data['id_mstfacility'] = $this->Inventory_Model->get_all_facilities();
        echo json_encode($data);
}  
function AddUtilization($inventory_id=NULL){
	$loginData = $this->session->userdata('loginData');
							//echo "<pre>";print_r($loginData);
							
							if( ($loginData) && $loginData->user_type == '1' ){
							$sess_where = " 1";
							}
							elseif( ($loginData) && $loginData->user_type == '2' ){
							
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
							}
							elseif( ($loginData) && $loginData->user_type == '3' ){ 
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
							}
							elseif( ($loginData) && $loginData->user_type == '4' ){ 
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
							}
							
							$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
							
							if($REQUEST_METHOD == 'POST')
							{
							$loginData = $this->session->userdata('loginData');
							$State_ID=$loginData->State_ID;
							$DistrictID=$loginData->DistrictID;
							$id_mstfacility=$loginData->id_mstfacility;
							if($loginData->State_ID==''||$loginData->State_ID==NULL){
							$State_ID=0;
							}
							if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
							$DistrictID=0;
							}
							if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
							$id_mstfacility=0;
							}
							//print_r($loginData);
							$item_id=$this->input->post('item_type');
							$item_name_ar=$this->Inventory_Model->get_items($item_id);
							
							if($item_id!=999)
							{
							$item_id=$this->input->post('item_type');
							$item_name=$item_name_ar[0]->id_mst_drugs;
							$item_type=$item_name_ar[0]->type;
							//echo $item_name[0]->drug_name;exit();
							}
							else{
							$item_id=$this->input->post('item_type');
							$batch_num   = $this->input->post('batch_num');
							$get_item_details=$this->Inventory_Model->get_receipt_in_transfer_out($item_id,$batch_num);
							//print_r($get_item_details);
							$item_name=$get_item_details[0]->drug_name;
							$item_type=$get_item_details[0]->type;
							}
							if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$this->input->post('quantity');
						$quantity=0;
						}
						else{
						
						$screening_tests=0;
						$quantity=$this->input->post('quantity');
					}
					
							$data = array(  
							'id_mst_drugs'  =>$item_id,
							'drug_name'  =>$item_name,
							'type'=>$item_type,
							'batch_num'   => $this->input->post('batch_num'),  
							'Entry_Date' => timeStamp($this->input->post('Entry_Date')),
							'quantity'     => $quantity,
							
							'quantity_screening_tests'     => $screening_tests,
							'Expiry_Date'  => timeStamp($this->input->post('Expiry_Date')), 
			
							'id_mststate'   =>  $State_ID,
							'id_mstdistrict' => $DistrictID,
							'id_mstfacility'     => $id_mstfacility, 
							'transfer_to'=> $transfer_to, 
							'id_tblusers'  => $loginData->id_tblusers ,
							'flag'=>	'U',
							'is_deleted'=>'0'
							);
							
							//print_r($data);
							//echo $inventory_id;exit;
							if ($inventory_id!=NULL) {
							$this->db->where('inventory_id',$inventory_id);
							$this->db->update('tbl_inventory',$data);
							$this->session->set_flashdata('tr_msg','Utilization Information Updated Successfully');
							redirect('Inventory/index/U');
							}
							else{
							$this->db->insert('tbl_inventory',$data);
							$lastid=$this->db->insert_id();

							$avaiable=array('Avaiable_quantity'=>$this->input->post('quantityRem'));
							$this->db->where('inventory_id',$lastid);
							$this->db->update('tbl_inventory',$avaiable);


							$this->session->set_flashdata('tr_msg','Utilization Information Added Successfully');
							redirect('Inventory/AddUtilization');
							}	
							}
							if($inventory_id!=NULL){
							//echo $inventory_id;
							$res_details = $this->Inventory_Model->edit_inventory_receipt($inventory_id);
							$content['receipt_details'] = $res_details;
							$content['edit_flag'] = 1;
							//print_r($res_details);
							}
							else{
							$content['edit_flag'] = 0;
							}
							$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag='58'";
							$content['item_type']=$this->db->query($query)->result();
							$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag='59'";
							$content['source_name']=$this->db->query($query)->result();
							$content['receiptitems']=$this->Inventory_Model->get_receipt_in_transfer_out();
	$content['subview'] = "Add_Utilization";
	$this->load->view('inventory/main_layout', $content);
}
function AddWastageMissing($inventory_id=NULL){
	$loginData = $this->session->userdata('loginData');
							//echo "<pre>";print_r($loginData);
							
							if( ($loginData) && $loginData->user_type == '1' ){
							$sess_where = " 1";
							}
							elseif( ($loginData) && $loginData->user_type == '2' ){
							
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
							}
							elseif( ($loginData) && $loginData->user_type == '3' ){ 
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
							}
							elseif( ($loginData) && $loginData->user_type == '4' ){ 
							$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
							}
							
							$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
							
							if($REQUEST_METHOD == 'POST')
							{
							$loginData = $this->session->userdata('loginData');
							$State_ID=$loginData->State_ID;
							$DistrictID=$loginData->DistrictID;
							$id_mstfacility=$loginData->id_mstfacility;
							if($loginData->State_ID==''||$loginData->State_ID==NULL){
							$State_ID=0;
							}
							if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
							$DistrictID=0;
							}
							if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
							$id_mstfacility=0;
							}
							//print_r($loginData);
							$item_id=$this->input->post('item_type');
							$item_name_ar=$this->Inventory_Model->get_items($item_id);
							
							if($item_id!=999)
							{
							$item_id=$this->input->post('item_type');
							$item_name=$item_name_ar[0]->id_mst_drugs;
							$item_type=$item_name_ar[0]->type;
							//echo $item_name[0]->drug_name;exit();
							}
							else{
							$item_id=$this->input->post('item_type');
							$batch_num   = $this->input->post('batch_num');
							$get_item_details=$this->Inventory_Model->get_receipt_in_transfer_out($item_id,$batch_num);
							//print_r($item_name_ar);
							$item_name=$get_item_details[0]->drug_name;
							$item_type=$get_item_details[0]->type;
							}
							if($item_name_ar[0]->strength!=0 && $item_name_ar[0]->type==2){
						$screening_tests=$this->input->post('quantity');
						$quantity=0;
						}
						else{
						
						$screening_tests=0;
						$quantity=$this->input->post('quantity');
						}
					
							$data = array(  
							'id_mst_drugs'  =>$item_id,
							'drug_name'  =>$item_name,
							'type'=>$item_type,
							'batch_num'   => $this->input->post('batch_num'),  
							'Entry_Date' => timeStamp($this->input->post('Entry_Date')),
							'quantity'     => $quantity,
							
							'quantity_screening_tests'     => $screening_tests,
							'rejection'  => $this->input->post('rejection'), 
							'waste_type'=>$this->input->post('waste_type'),
							'id_mststate'   =>  $State_ID,
							'id_mstdistrict' => $DistrictID,
							'id_mstfacility'     => $id_mstfacility,  
							'id_tblusers'  => $loginData->id_tblusers ,
							'flag'=>	'W',
							'is_deleted'=>'0' 
							);
							
							//print_r($data);
							//echo $inventory_id;exit;
							if ($inventory_id!=NULL) {
							$this->db->where('inventory_id',$inventory_id);
							$this->db->update('tbl_inventory',$data);
							$this->session->set_flashdata('tr_msg','Wastage Missing Information Updated Successfully');
							redirect('Inventory/AddWastageMissing');
							}
							else{
							$this->db->insert('tbl_inventory',$data);
							$lastid=$this->db->insert_id();

							$avaiable=array('Avaiable_quantity'=>$this->input->post('quantityRem'));
							$this->db->where('inventory_id',$lastid);
							$this->db->update('tbl_inventory',$avaiable);


							$this->session->set_flashdata('tr_msg','Wastage Missing Information Added Successfully');
							redirect('Inventory/AddWastageMissing');
							}	
							}
							if($inventory_id!=NULL){
							//echo $inventory_id;
							$res_details = $this->Inventory_Model->edit_inventory_receipt($inventory_id);
							$content['receipt_details'] = $res_details;
							$content['edit_flag'] = 1;
							//print_r($res_details);
							}
							else{
							$content['edit_flag'] = 0;
							}
							$query="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag='58'";
							$content['item_type']=$this->db->query($query)->result();
						$query1="SELECT LookupCode,LookupValue FROM `mstlookup` WHERE flag='60'";
						$content['Waste_type']=$this->db->query($query1)->result();
						//print_r($content['Waste_type']);
						$content['receiptitems']=$this->Inventory_Model->get_receipt_in_transfer_out();
	$content['subview'] = "Add_Wastage_Missing";
	$this->load->view('inventory/main_layout', $content);
}
function get_rem_val(){
        $batch_num = $this->input->post('id',TRUE);
        $type = $this->input->post('type',TRUE);
        $data['Rem_value'] = $this->Inventory_Model->Remaining_stock($batch_num,$type);	
        
        //print_r($data);die();
        echo json_encode($data);
    }  
    function get_maxdate_val(){
        $batch_num = $this->input->post('id',TRUE);
        $data['Last_date'] = $this->Inventory_Model->getmaxdate($batch_num);
        //print_r($data);die();
        echo json_encode($data);
    }  
}
