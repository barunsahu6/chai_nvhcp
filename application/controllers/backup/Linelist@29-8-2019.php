<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Linelist extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Common_Model');
		$this->load->model('Patient_Model');
        $this->load->helper('common');
		ini_set('memory_limit', '1024M');
		ini_set('memory_limit', '-1');
		$loginData = $this->session->userdata('loginData');
    	/*	if($loginData->user_type != 1){
            $this->session->set_flashdata('er_msg','You are not logged-in, please login again to continue');
            redirect('login');
        }*/
        if ($this->session->userdata('loginData') == null) {
        	redirect('login');
        }

        $this->header = array(
        	'id_tblpatient',		    
        	'StateName',
        	'DistrictName',
        	'BlockName',
        	'VillageTown',
        	'FacilityCode',
        	'FacilityType',
        	'facilityName',
        	'PatientGUID',
        	'OPD_Id',
        	'UID_Prefix',		    	    
        	'gender',
        	'GenotypeTest_Result',
        	'FatherHusband',
        	'Add1',		    
        	'PIN',
        	'Mobile',
        	'T_DLL_01_Date',
        	'T_AntiHCV01_Result',
        	'T_DLL_01_VLC_Date',
        	'T_DLL_01_VLCount',
        	'T_DLL_01_VLC_Result',
        	'V1_Haemoglobin',
        	'V1_Albumin',
        	'V1_Bilrubin',
        	'V1_INR',
        	'V1_Cirrhosis',
        	'Cirr_TestDate',
        	'Cirr_Encephalopathy',
        	'Cirr_Ascites',
        	'Cirr_VaricealBleed',
        	'ChildScore',
        	'Result',
        	'T_Initiation',
        	'T_RmkDelay',		    
        	'T_NoPillStart',
        	'T_VisitPlan',
        	'DrugSideEffect',
        	'ETR_HCVViralLoad_Dt',
        	'ETR_HCVViralLoadCount',
        	'ETR_Result',
        	'SVR12W_HCVViralLoad_Dt',
        	'SVR12W_HCVViralLoadCount',
        	'SVR12W_Result',
        	'SVR12W_LabID',
        	'patientAdherence',
        	'SVR_TreatmentStatus',
        	'Age',
        	'FirstName',
        	'T_DurationValue',
        	'SVR_LabName',
        	'InitiationDt',
        	'ETRDt',
        	'Pregnant',
        	'DeliveryDt',
        	'HCVHistory',
        	'HCVPastTreatment',
        	'HCVPastOutcome',
        	'V1_Platelets',		    
        	'AdvisedSVRDate',
        	'ETR_PillsLeft',
        	'T_DurationOther',
        	'Current_Visitdt',
        	'Next_Visitdt',
        	'PastTreatmentDuration',
        	'PastDuration',
        	'IsMobile_Landline',
        	'fragment3editreason',
        	'NextVisitPurpose',
        	'ETRComments',
        	'SVRComments',
        	'IsETRDone',
        	'ETRTestDate',
        	'PreviousTreatingHospital',
        	'PreviousUIDNumber',
        	'ETRAutoupdate',
        	'NewPatient',
        	'PatientType',
        	'PastTreatment',
        	'PastFacility',
        	'PastUID',
        	'Weight',
        	'CKDStage',
        	'NWeeksCompleted',
        	'LastPillDate',
        	'PastRegimen',
        	'PreviousDuration',
        	'InitiateDoctor',
        	'ETRDoctor',
        	'SVRDoctor',
        	'InterruptReason',
        	'InterruptToStage',
        	'VisitInterrupted',
        	'TransferRequest',
        	'TransferRequestAccepted',
        	'TransferUID',
        	'TransferFromFacility',
        	'TransferToFacility',
        	'id_mst_medical_specialists',
        	'HospitalName',
        	'IsSMSConsent',
        	'OtherRisk',
        	'OccupationID',
        	'ETRLFUreason',
        	'InitiateDoctorOther',
        	'ETRDoctorOther',
        	'SVRDoctorOther',
        	'VLSampleCollectionDate',
        	'VLStorageTemp',
        	'VLTransportDate',
        	'VLLabID',
        	'VLRecieptDate',
        	'Relation',
        	'State',
        	'AntiHCV',
        	'HCVRapid',
        	'HCVRapidDate',
        	'HCVRapidResult',
        	'HCVRapidPlace',
        	'HCVRapidLabID',
        	'HCVRapidLabOther',
        	'HCVElisa',
        	'HCVElisaDate',
        	'HCVElisaResult',
        	'HCVElisaPlace',
        	'HCVElisaLabID',
        	'HCVElisaLabOther',
        	'HCVOther',
        	'HCVOtherName',
        	'HCVOtherDate',
        	'HCVOtherResult',
        	'HCVOtherPlace',
        	'HCVOtherLabID',
        	'HCVLabOther',
        	'LgmAntiHAV',
        	'HAVRapid',
        	'HAVRapidDate',
        	'HAVRapidResult',
        	'HAVRapidPlace',
        	'HAVRapidLabID',
        	'HAVRapidLabOther',
        	'HAVElisa',
        	'HAVElisaDate',
        	'HAVElisaResult',
        	'HAVElisaPlace',
        	'HAVElisaLabID',
        	'HAVElisaLabOther',
        	'HAVOther',
        	'HAVOtherName',
        	'HAVOtherDate',
        	'HAVOtherResult',
        	'HAVOtherPlace',
        	'HAVOtherLabID',
        	'HAVLabOther',
        	'HbsAg',
        	'HBSRapid',
        	'HBSRapidDate',
        	'HBSRapidResult',
        	'HBSRapidPlace',
        	'HBSRapidLabID',
        	'HBSRapidLabOther',
        	'HBSElisa',
        	'HBSElisaDate',
        	'HBSElisaResult',
        	'HBSElisaPlace',
        	'HBSElisaLabID',
        	'HBSElisaLabOther',
        	'HBSOther',
        	'HBSOtherName',
        	'HBSOtherDate',
        	'HBSOtherResult',
        	'HBSOtherPlace',
        	'HBSOtherLabID',
        	'HBSLabOther',
        	'LgmAntiHBC',
        	'HBCRapid',
        	'HBCRapidDate',
        	'HBCRapidResult',
        	'HBCRapidPlace',
        	'HBCRapidLabID',
        	'HBCRapidLabOther',
        	'HBCElisa',
        	'HBCElisaDate',
        	'HBCElisaResult',
        	'HBCElisaPlace',
        	'HBCElisaLabID',
        	'HBCElisaLabOther',
        	'HBCOther',
        	'HBCOtherName',
        	'HBCOtherDate',
        	'HBCOtherResult',
        	'HBCOtherPlace',
        	'HBCOtherLabID',
        	'HBCLabOther',
        	'LgmAntiHEV',
        	'HEVRapid',
        	'HEVRapidDate',
        	'HEVRapidResult',
        	'HEVRapidPlace',
        	'HEVRapidLabID',
        	'HEVRapidLabOther',
        	'HEVElisa',
        	'HEVElisaDate',
        	'HEVElisaResult',
        	'HEVElisaPlace',
        	'HEVElisaLabID',
        	'HEVElisaLabOther',
        	'HEVOther',
        	'HEVOtherName',
        	'HEVOtherDate',
        	'HEVOtherResult',
        	'HEVOtherPlace',
        	'HEVOtherLabID',
        	'HEVLabOther',
        	'VLTransporterName',
        	'VLTransporterDesignation',
        	'VLReceiverName',
        	'VLReceiverDesignation',
        	'SVRDrawnDate',
        	'SVRStorageTemp',
        	'SVRTransportDate',
        	'SVRLabID',
        	'SVRTransporterName',
        	'SVRTransporterDesignation',
        	'SVRReceiptDate',
        	'SVRReceiverName',
        	'SVRReceiverDesignation',
        	'V1_Creatinine',
        	'V1_EGFR',
        	'BreastFeeding',
        	'ART_Regimen',
        	'Ribavirin',
        	'IsReferal',
        	'ReferingDoctor',
        	'ReferTo',
        	'ReferalDate',
        	'PrescribingFacility',
        	'PrescribingDoctor',
        	'PrescribingDate',
        	'ReferingDoctorOther',
        	'PrescribingDoctorOther',
        	'MF5',
        	'MF6',
        	'fragment3_1editreason',
        	'fragment3_2editreason',
        	'IsAgeMonths',
        	'TreatmentUpdatedBy',
        	'TreatmentUpdatedOn',
        	'fragment3_3editreason',
        	'VLLabID_Other',
        	'PastRegimen_Other',
        	'SCRemarks',
        	'PrescriptionPlace',
        	'IsVLSampleStored',
        	'IsVLSampleTransported',
        	'VLTransportTemp',
        	'VLSCRemarks',
        	'VLResultRemarks',
        	'VLHepB',
        	'VLHepC',
        	'VLReferredLab',
        	'ALT',
        	'AST',
        	'AST_ULN',
        	'DispensationPlace',
        	'CirrhosisStatus',
        	'IsSVRSampleStored',
        	'IsSVRSampleTransported',
        	'SVRTransportTemp',
        	'SVRTransportRemark',
        	'BVLTransportTemp',
        	'IsBVLSampleStored',
        	'IsBVLSampleTransported',
        	'BVLSCRemarks',
        	'BVLResultRemarks',
        	'BVLSampleCollectionDate',
        	'BVLTransportDate',
        	'BVLStorageTemp',
        	'BVLTransporterName',
        	'BVLReceiverName',
        	'BVLTransporterDesignation',
        	'BVLReceiverDesignation',
        	'T_DLL_01_BVLC_Result',
        	'BVLLabID',
        	'BVLLabID_Other',
        	'T_DLL_01_BVLCount',
        	'T_DLL_01_BVLC_Date',
        	'BVLRecieptDate',
        	'patientPDispensation',
        	'PTState',
        	'PTYear',
        	'Storage_days_hrs',
        	'Storageduration',
        	'ReferredDispensationPlace',
        	'BStorage_days_hrs',
        	'BStorageduration',
        	'IsSampleAccepted',
        	'IsBSampleAccepted',
        	'RejectionReason',
        	'BRejectionReason',
        	'RejectionReasonOther',
        	'BRejectionReasonOther',
        	'patientNAdherenceReason',
        	'patientNAdherenceReasonOther',
        	'LMP',		    
        	'DeathReason',
        	'LFUReason',
        	'ExperiencedCategory',
        	'OutsideYear',
        	'RecommendedRegimen',
        	'RecommendedDuration',
        	'SideEffectValue',
        	'DurationReason',
        	'DistrictOther',
        	'BlockOther',
        	'Refer_FacilityHAV',
        	'Refer_HigherFacilityHAV',
        	'Refer_FacilityHEV',
        	'Refer_HigherFacilityHEV',
        	'CaseType',
        	'InterruptReasonOther',
        	'IsSVRSampleAccepted',
        	'SVRRejectionReason',
        	'SVRRejectionReasonOther',
        	'SVR12W_LabOther',
        	'ShortName',
        	'ActiveFrom',
        	'ActiveTo',
        	'PrefixMedCard',
        	'AddressLine1',
        	'AddressLine2',
        	'POBox',
        	'City',
        	'PinCode',
        	'STDCode',
        	'Phone1',
        	'Phone2',
        	'email',
        	'LocationDet',
        	'NameLocalLng',
        	'LanguageID',
        	'facility_name',
        	'facility_short_name',
        	'name',
        	'Visit_Dt',
        	'ExpectedVIsits',
        	'PillsLeft',
        	'PillsIssued',
        	'DaysSinceLastVisit',
        	'PillsConsumed',
        	'Adherence',
        	'NextVisit_Dt',
        	'Regimen',
        	'Haemoglobin',
        	'PlateletCount',
        	'SideEffect',
        	'Comments',
        	'VisitFlag',
        	'Doctor',
        	'DoctorOther',
        	'pvSideEffectValue',
        	'PDispensation',
        	'NAdherenceReason',
        	'NAdherenceReasonOther',
        	'regimen_name',
        	'strength',
        	'RiskFactor'
        );
$this->folder_path = FCPATH . "application" . DIRECTORY_SEPARATOR . "linelists" . DIRECTORY_SEPARATOR;

$this->load->helper('file');
delete_files($this->folder_path);
}

public function index()
{
	$loginData = $this->session->userdata('loginData');
	$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
	if($REQUEST_METHOD == 'POST')
	{
		$filters1['id_search_state'] = $this->input->post('search_state');
		$filters1['id_input_district'] = $this->input->post('input_district');
		$filters1['id_mstfacility'] = $this->input->post('facility');	
		$filters1['startdate']      = timeStamp($this->input->post('startdate'));	
		$filters1['enddate']        = timeStamp($this->input->post('enddate'));	
	}
	else
	{	
		$filters1['id_search_state'] = 0;
		$filters1['id_input_district'] = 0;
		$filters1['id_mstfacility'] = 0;	
		$filters1['startdate']      = date('Y-m-01');	
		$filters1['enddate']        = date('Y-m-d');			
	}

	$this->session->set_userdata('filters1', $filters1);

	if( ($loginData) && $loginData->user_type == '1' ){
		$sess_where = " 1";
		$sess_mstdistrict =" 1";
		$sess_mstfacility =" 1";
	}
	elseif( ($loginData) && $loginData->user_type == '2' ){

		$sess_where = $loginData->State_ID;

		$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

		$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";
	}
	elseif( ($loginData) && $loginData->user_type == '3' ){ 
		$sess_where = $loginData->State_ID;
		$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
		$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
	}
	elseif( ($loginData) && $loginData->user_type == '4' ){ 
		$sess_where =$loginData->State_ID;
		$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
		$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
	}

	$content['start_date'] = date('Y-m-d');
	$content['end_date']   = date('Y-m-d');



	$sql = "select 
		* 
		from 
		(	
			SELECT 
			f.id_mstfacility, 
			f.facility_short_name AS hospital, 
			COUNT(PatientGUID) AS 'patients' 
			FROM view_linelist p 
			INNER JOIN mstfacility f 
			ON p.id_mstfacility = f.id_mstfacility 
			WHERE p.Session_StateID = '".$sess_where."' 
			
			AND p.CreatedOn between '".($filters1['startdate'])."' AND '".($filters1['enddate'])."' 
			GROUP BY f.facility_short_name
			UNION 
			SELECT 
			0, 'All' AS hospital, 
			COUNT(PatientGUID) AS 'patients' 
			FROM view_linelist p1 
		) a ORDER BY a.id_mstfacility";

	
		// die($sql);

	$res = $this->Common_Model->query_data($sql);
	$sql = "select * from mststate where ".$sess_where."";
	$content['states'] = $this->db->query($sql)->result();

	$sql = "select * from mstdistrict where ".$sess_mstdistrict."";
	$content['districts'] = $this->db->query($sql)->result();

	$sql = "select * from mstfacility where  ".$sess_mstfacility."";
	$content['facilities'] = $this->db->query($sql)->result();	
	$content['facilities'] = $res;
	$content['subview'] = 'linelist_hospital';
	$this->load->view('admin/main_layout', $content);
}

public function getlist($id_mstfacility = null)
{


	ini_set('memory_limit', '-1');
	if ($id_mstfacility > 0) {

		$sql_facility_name = "select facilitycode from mstfacility where id_mstfacility = " . $id_mstfacility;
		$facility_name = $this->Common_Model->query_data($sql_facility_name);
		$facility = $facility_name[0]->facilitycode;
		$facility_where = "where id_mstfacility = " . $id_mstfacility;
	} else {
		$facility_where = "";
		$facility = "ALL";
	}

        // $sql_total_recs = "select count(*) as count from tblpatient where id_mstfacility = ".$id_mstfacility;
        // $res = $this->Common_Model->query_data($sql_total_recs);
        // $total_recs = $res[0]->count;

	$filename = $facility . '_' . date('Y-m-d') . ".csv";
	header("Content-Disposition: attachment; filename=\"$filename\"");
	header("Content-Type: text/csv");

	$out = fopen("php://output", 'w');
	$flag = false;


	$sql_line_list = "select 
	UID,TreatingHospital,Name,Age,Gender,RiskFactors,FathersHusbandsName,Address,Villagetown,Area,Block,District,Pincode,ContactNo,SampleCollectionDateforAntiHCVTest,AntiHCVTestDate,ResultReceivedDatebyPatient,RESULTOFANTIHCVTESTINGELISA,SampleCollectionDateforVL,SampleCollectioncenternameforVL,ViralLoadTestDate,BaselineViralLoad,ViralLoadStatus,DateofArrivaltoHospital,MedicalSpecialist,CirrhosisNoCirrhosisSTatus,ClinicalUSGtestdate,ClinicalUSGresult,Fibroscantestdate,LSMValueinkPa,Fibroscanresult,APRItestdate,AST,ASTNormal,APRIScore,FIB4testdate,ALT,FIB4Score,Albumin,Bilurubin,INR,Baseline1_val,Visit2_1_val,Visit3_1_val,Visit4_1_val,Visit5_1_val,Visit6_1_val,Visit7_1,Baseline2_val,Visit2_2_val,Visit3_2_val,Visit4_2_val,Visit5_2_val,Visit6_2_val,Visit7_2_val,DecompensatedCirrhosisResult,DecompensatedCirrhosisTestdate,Encephalopathy,Ascites,VaricealBleed,ChildScore,Genotype,GenotypeTestDate,Treatmentinitiationdate,Regimen,Duration,PillsDispensed,AdvisedNextVisitDate,Visit2_2,PillsLeft_2,AdvisedNextVisitDate_2,Adherence_2,Visit3_3,PillsLeft_3,AdvisedNextVisitDate_3,Adherence_3,Visit4_4,PillsLeft_4,AdvisedNextVisitDate_4,Adherence_4,Visit5_5,PillsLeft_5,AdvisedNextVisitDate_5,Adherence_5,Visit6_6,PillsLeft_6,AdvisedNextVisitDate_6,Adherence_6,Visit7_7,PillsLeft_7,AdvisedNextVisitDate_7,Adherence_7,ETRTestDate,ETRViralLoad,ETRTestResult,SVRTestDate,SVRViralLoad,SVRTestResult,DrugSideEffect,DrugCompliance,PatientStatus,TreatmentStatus,ReasonforTreatmentFailure,TreatmentCardType,Remarks,Durationoftrt_from_private,
	category,jailno,UploadedOn,lal_svr_date,lal_svr_result,lal_viral_load,
	AltContact,
	history_of_hcv,
	uid_serial_number,
	previous_treating_hospital,
	previous_treatment_outcome,
	occupation,
	occupation_other,
	hiv,
	ckd,
	diabetes,
	hypertension,
	hbv,
	call_output,
	cumulative_svr,
	cumulative_svr_viral_load,
	cumulative_svr_outcome
	from linelist " . $facility_where;

        // echo $sql_line_list; die();
        // ini_set('display_errors', 1);

	$content['line_list'] = $this->Common_Model->query_data($sql_line_list);

	if (count($content['line_list']) > 0) {

		foreach ($content['line_list'] as $row) {
                // print_r($row); die();
			if (!$flag) {
                    // display field/column names as first row
                    // $firstline = array_map(array($this,'map_colnames'), array_keys((array)$row));
				fputcsv($out, $this->header, ',', '"');
				$flag = true;
			}
			foreach ((array)$row as $value) {
				if ($value == null) {
					$value = '';
				}
			}
			fputcsv($out, array_values((array)$row), ',', '"');
		}

	}
        // }

	fclose($out);
	exit();
}

public function download_list_zipped($id_mstfacility = 0)
{

    $loginData = $this->session->userdata('loginData');
    $REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
    if($REQUEST_METHOD == 'POST')
    {
        $filters1['id_search_state'] = $this->input->post('search_state');
        $filters1['id_input_district'] = $this->input->post('input_district');
        $filters1['id_mstfacility'] = $this->input->post('facility');   
        $filters1['startdate']      = timeStamp($this->input->post('startdate'));   
        $filters1['enddate']        = timeStamp($this->input->post('enddate')); 
    }
    else
    {   
        $filters1['id_search_state'] = 0;
        $filters1['id_input_district'] = 0;
        $filters1['id_mstfacility'] = 0;    
        $filters1['startdate']      = date('Y-m-01');   
        $filters1['enddate']        = date('Y-m-d');            
    }

    $this->session->set_userdata('filters1', $filters1);

    if( ($loginData) && $loginData->user_type == '1' ){
        $sess_where = " 1";
        $sess_mstdistrict =" 1";
        $sess_mstfacility =" 1";
    }
    elseif( ($loginData) && $loginData->user_type == '2' ){

        $sess_where = $loginData->State_ID;

        $sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

        $sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";
    }
    elseif( ($loginData) && $loginData->user_type == '3' ){ 
        $sess_where = $loginData->State_ID;
        $sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
        $sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
    }
    elseif( ($loginData) && $loginData->user_type == '4' ){ 
        $sess_where =$loginData->State_ID;
        $sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
        $sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
    }

    $content['start_date'] = date('Y-m-d');
    $content['end_date']   = date('Y-m-d');

	ini_set('memory_limit', '-1');

	if ($id_mstfacility > 0) {

		$query = "select facilitycode from mstfacility where id_mststate= ".$sess_where." and  id_mstfacility = " . $id_mstfacility;
		$result = $this->db->query($query)->result();
		$filename = $result[0]->facilitycode . "-" . date('Y-m-d-His');
		$facility_where = " where id_mstfacility = " . $id_mstfacility;

	} else {
		$facility_where = " where id_mstfacility where id_mststate= ".$sess_where."";
		$filename = "ALL-" . date('Y-m-d-His');
	}

	$query = "select count(*) as count from view_linelist" . $facility_where;
	$count_res = $this->db->query($query)->result();
	$count = $count_res[0]->count;
	$offset = 0;
	$limit = 2000;
	$file_path = $this->folder_path . $filename;

	while ($offset < $count) {
		if ($offset == 0) {
			$file = fopen($file_path . ".csv", "w");
			if (!$file) {
				die('cant open file handler');
			}
			fputcsv($file, $this->header);
		} else {
			$file = fopen($file_path . ".csv", "a");
		}

		$result = $this->get_line_list_result($facility_where, $limit, $offset);             
		foreach ($result as $row) {
			fputcsv($file, (array)$row);
		}

		fclose($file);
		$offset += $limit;
	}

	$this->load->library('zip');

	if (!$this->zip->read_file($file_path . '.csv')) {
		echo "cant read file";
		die();
	} else {
		header('Content-Encoding: UTF-8');
		$this->zip->download($filename . '.zip');
	}
}

public function get_line_list_result($filter_facility = "", $limit = 5000, $offset = 0)
{
	ini_set('memory_limit', '-1');
	$query = "SELECT
	id_tblpatient,		    
	StateName,
	DistrictName,
	BlockName,
	VillageTown,
	FacilityCode,
	FacilityType,
	facilityName,
	PatientGUID,
	OPD_Id,
	UID_Prefix,		    	    
	gender,
	GenotypeTest_Result,
	FatherHusband,
	Add1,		    
	PIN,
	Mobile,
	T_DLL_01_Date,
	CASE T_AntiHCV01_Result WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS T_AntiHCV01_Result,
	T_DLL_01_VLC_Date,
	T_DLL_01_VLCount,
	CASE T_DLL_01_VLC_Result WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS T_DLL_01_VLC_Result,
	V1_Haemoglobin,
	V1_Albumin,
	V1_Bilrubin,
	V1_INR,
	V1_Cirrhosis,
	Cirr_TestDate,
	Cirr_Encephalopathy,
	Cirr_Ascites,
	Cirr_VaricealBleed,
	ChildScore,
	Result,
	T_Initiation,
	T_RmkDelay,		    
	T_NoPillStart,
	T_VisitPlan,
	DrugSideEffect,
	ETR_HCVViralLoad_Dt,
	ETR_HCVViralLoadCount,
	CASE ETR_Result WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS ETR_Result,	
	SVR12W_HCVViralLoad_Dt,
	SVR12W_HCVViralLoadCount,
	CASE SVR12W_Result WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS SVR12W_Result,	
	SVR12W_LabID,
	patientAdherence,
	SVR_TreatmentStatus,
	Age,
	FirstName,
	T_DurationValue,
	SVR_LabName,
	InitiationDt,
	ETRDt,
	Pregnant,
	DeliveryDt,
	HCVHistory,
	HCVPastTreatment,
	HCVPastOutcome,
	V1_Platelets,		    
	AdvisedSVRDate,
	ETR_PillsLeft,
	T_DurationOther,
	Current_Visitdt,
	Next_Visitdt,
	PastTreatmentDuration,
	PastDuration,
	IsMobile_Landline,
	fragment3editreason,
	NextVisitPurpose,
	ETRComments,
	SVRComments,
	IsETRDone,
	ETRTestDate,
	PreviousTreatingHospital,
	PreviousUIDNumber,
	ETRAutoupdate,
	NewPatient,
	PatientType,
	PastTreatment,
	PastFacility,
	PastUID,
	Weight,
	CKDStage,
	NWeeksCompleted,
	LastPillDate,
	PastRegimen,
	PreviousDuration,
	InitiateDoctor,
	ETRDoctor,
	SVRDoctor,
	InterruptReason,
	InterruptToStage,
	VisitInterrupted,
	TransferRequest,
	TransferRequestAccepted,
	TransferUID,
	TransferFromFacility,
	TransferToFacility,
	id_mst_medical_specialists,
	HospitalName,
	IsSMSConsent,
	OtherRisk,
	OccupationID,
	ETRLFUreason,
	InitiateDoctorOther,
	ETRDoctorOther,
	SVRDoctorOther,
	VLSampleCollectionDate,
	VLStorageTemp,
	VLTransportDate,
	VLLabID,
	VLRecieptDate,
	Relation,
	State,
	AntiHCV,
	HCVRapid,
	HCVRapidDate,
	CASE HCVRapidResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HCVRapidResult,
	HCVRapidPlace,
	HCVRapidLabID,
	HCVRapidLabOther,
	HCVElisa,
	HCVElisaDate,
	CASE HCVElisaResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HCVElisaResult,		    
	HCVElisaPlace,
	HCVElisaLabID,
	HCVElisaLabOther,
	HCVOther,
	HCVOtherName,
	HCVOtherDate,
	CASE HCVOtherResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HCVOtherResult,		    
	HCVOtherPlace,
	HCVOtherLabID,
	HCVLabOther,
	LgmAntiHAV,
	HAVRapid,
	HAVRapidDate,
	CASE HAVRapidResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HAVRapidResult,		    
	HAVRapidPlace,
	HAVRapidLabID,
	HAVRapidLabOther,
	HAVElisa,
	HAVElisaDate,
	CASE HAVElisaResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HAVElisaResult,		    
	HAVElisaPlace,
	HAVElisaLabID,
	HAVElisaLabOther,
	HAVOther,
	HAVOtherName,
	HAVOtherDate,
	CASE HAVOtherResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HAVOtherResult,	
	HAVOtherPlace,
	HAVOtherLabID,
	HAVLabOther,
	HbsAg,
	HBSRapid,
	HBSRapidDate,
	CASE HBSRapidResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HBSRapidResult,
	HBSRapidPlace,
	HBSRapidLabID,
	HBSRapidLabOther,
	HBSElisa,
	HBSElisaDate,
	CASE HBSElisaResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HBSElisaResult,		    
	HBSElisaPlace,
	HBSElisaLabID,
	HBSElisaLabOther,
	HBSOther,
	HBSOtherName,
	HBSOtherDate,
	CASE HBSOtherResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HBSOtherResult,			    
	HBSOtherPlace,
	HBSOtherLabID,
	HBSLabOther,
	LgmAntiHBC,
	HBCRapid,
	HBCRapidDate,
	CASE HBCRapidResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HBCRapidResult,			    
	HBCRapidPlace,
	HBCRapidLabID,
	HBCRapidLabOther,
	HBCElisa,
	HBCElisaDate,
	CASE HBCElisaResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HBCElisaResult,
	HBCElisaPlace,
	HBCElisaLabID,
	HBCElisaLabOther,
	HBCOther,
	HBCOtherName,
	HBCOtherDate,
	CASE HBCOtherResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HBCOtherResult,
	HBCOtherPlace,
	HBCOtherLabID,
	HBCLabOther,
	LgmAntiHEV,
	HEVRapid,
	HEVRapidDate,
	CASE HEVRapidResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HEVRapidResult,
	HEVRapidPlace,
	HEVRapidLabID,
	HEVRapidLabOther,
	HEVElisa,
	HEVElisaDate,
	CASE HEVElisaResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HEVElisaResult,		    
	HEVElisaPlace,
	HEVElisaLabID,
	HEVElisaLabOther,
	HEVOther,
	HEVOtherName,
	HEVOtherDate,
	CASE HEVOtherResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HEVOtherResult,	
	HEVOtherPlace,
	HEVOtherLabID,
	HEVLabOther,
	VLTransporterName,
	VLTransporterDesignation,
	VLReceiverName,
	VLReceiverDesignation,
	SVRDrawnDate,
	SVRStorageTemp,
	SVRTransportDate,
	SVRLabID,
	SVRTransporterName,
	SVRTransporterDesignation,
	SVRReceiptDate,
	SVRReceiverName,
	SVRReceiverDesignation,
	V1_Creatinine,
	V1_EGFR,
	BreastFeeding,
	ART_Regimen,
	Ribavirin,
	IsReferal,
	ReferingDoctor,
	ReferTo,
	ReferalDate,
	PrescribingFacility,
	PrescribingDoctor,
	PrescribingDate,
	ReferingDoctorOther,
	PrescribingDoctorOther,
	MF5,
	MF6,
	fragment3_1editreason,
	fragment3_2editreason,
	IsAgeMonths,
	TreatmentUpdatedBy,
	TreatmentUpdatedOn,
	fragment3_3editreason,
	VLLabID_Other,
	PastRegimen_Other,
	SCRemarks,
	PrescriptionPlace,
	IsVLSampleStored,
	IsVLSampleTransported,
	VLTransportTemp,
	VLSCRemarks,
	VLResultRemarks,
	VLHepB,
	VLHepC,
	VLReferredLab,
	ALT,
	AST,
	AST_ULN,
	DispensationPlace,
	CirrhosisStatus,
	IsSVRSampleStored,
	IsSVRSampleTransported,
	SVRTransportTemp,
	SVRTransportRemark,
	BVLTransportTemp,
	IsBVLSampleStored,
	IsBVLSampleTransported,
	BVLSCRemarks,
	BVLResultRemarks,
	BVLSampleCollectionDate,
	BVLTransportDate,
	BVLStorageTemp,
	BVLTransporterName,
	BVLReceiverName,
	BVLTransporterDesignation,
	BVLReceiverDesignation,
	CASE T_DLL_01_BVLC_Result WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS T_DLL_01_BVLC_Result,
	BVLLabID,
	BVLLabID_Other,
	T_DLL_01_BVLCount,
	T_DLL_01_BVLC_Date,
	BVLRecieptDate,
	patientPDispensation,
	PTState,
	PTYear,
	Storage_days_hrs,
	Storageduration,
	ReferredDispensationPlace,
	BStorage_days_hrs,
	BStorageduration,
	IsSampleAccepted,
	IsBSampleAccepted,
	RejectionReason,
	BRejectionReason,
	RejectionReasonOther,
	BRejectionReasonOther,
	patientNAdherenceReason,
	patientNAdherenceReasonOther,
	LMP,		    
	DeathReason,
	LFUReason,
	ExperiencedCategory,
	OutsideYear,
	RecommendedRegimen,
	RecommendedDuration,
	SideEffectValue,
	DurationReason,
	DistrictOther,
	BlockOther,
	Refer_FacilityHAV,
	Refer_HigherFacilityHAV,
	Refer_FacilityHEV,
	Refer_HigherFacilityHEV,
	CaseType,
	InterruptReasonOther,
	IsSVRSampleAccepted,
	SVRRejectionReason,
	SVRRejectionReasonOther,
	SVR12W_LabOther,
	ShortName,
	ActiveFrom,
	ActiveTo,
	PrefixMedCard,
	AddressLine1,
	AddressLine2,
	POBox,
	City,
	PinCode,
	STDCode,
	Phone1,
	Phone2,
	email,
	LocationDet,
	NameLocalLng,
	LanguageID,
	facility_name,
	facility_short_name,
	name,
	Visit_Dt,
	ExpectedVIsits,
	PillsLeft,
	PillsIssued,
	DaysSinceLastVisit,
	PillsConsumed,
	Adherence,
	NextVisit_Dt,
	Regimen,
	Haemoglobin,
	PlateletCount,
	SideEffect,
	Comments,
	VisitFlag,
	Doctor,
	DoctorOther,
	pvSideEffectValue,
	PDispensation,
	NAdherenceReason,
	NAdherenceReasonOther,
	regimen_name,
	strength,
	lookup_value
	FROM
	view_linelist " . $filter_facility . "  
	limit $limit 
	offset $offset";

        // echo $query; die();
	return $this->db->query($query)->result();
}
}
