<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Linelist_masters extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Common_Model');
		$this->load->Model('Log4php_model');
		
		$loginData = $this->session->userdata('loginData');
		if($loginData == null)
		{
			redirect('login');
		}
		
	}

	public function index($masterId = null)
	{
		$this->load->library('form_validation');
		ini_set('display_errors', 1);
		//echo 'asdasdasdas';exit();
		$masteID = $masterId;
		
		$loginData = $this->session->userdata('loginData');
		$tempidval = $this->security->xss_clean($this->input->post('tempidval'));

		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

		if($REQUEST_METHOD == 'POST'){

			$labelname = $this->security->xss_clean($this->input->post('label2')); 

			if ($this->input->post('tempidval')=='') {

				$this->form_validation->set_rules('Templatename', 'Name', 'trim|required|xss_clean');


			if ($this->form_validation->run() == FALSE) {

			$arr['status'] = 'false';
			$this->session->set_flashdata("tr_msg",validation_errors());
			}else{

				if (!empty($labelname)) {
					$arrayNameval = array(
						'template_name' => $this->security->xss_clean($this->input->post('Templatename')),
						'userId'        => $loginData->id_tblusers,
						'created_on'    => date('Y-m-d'),
					);
					$this->db->insert('linelist_master',$arrayNameval);
					$insertid = $this->db->insert_id();
					
					$i=0; foreach ($labelname as $row) { $i++;

						$array = array(
							'masterId'   => $insertid,
							'categoryId' => $row,
							'user_Id'    => $loginData->id_tblusers
						);
						$this->db->insert('linelist_template',$array);						
					}
					$this->session->set_flashdata('tr_msg', 'Template Successfully Created');
					$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$insertid,'Create','Template Successfully Created');
					redirect('Linelist_masters');
				}
				}
			}else{ 

				$this->db->where('masterId',$tempidval);
				$this->db->delete('linelist_template');

				$arrayNameval = array(
					'template_name' => $this->security->xss_clean($this->input->post('Templatename')),
					'userId'        => $loginData->id_tblusers,
					'created_on'    => date('Y-m-d'),
				);
				
				$this->db->where('masterId',$tempidval);
				$this->db->update('linelist_master',$arrayNameval);				
				
				$labelname = $this->security->xss_clean($this->input->post('label2'));

				$i=0; foreach ($labelname as $row) { 
					$i++;
					$array = array(

						'masterId'   => $tempidval,
						'categoryId' => $row,
						'user_Id'    => $loginData->id_tblusers
					);
					$this->db->insert('linelist_template',$array);
					$insert_id = $this->db->insert_id();
				}

				$this->session->set_flashdata('tr_msg', 'Template Successfully Updated');
				$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$tempidval,'Update','Template Successfully Updated');
				redirect('Linelist_masters');
			}
		}
		$query = "select Category_name,category_Id from linelist_category group by category_Id";
		$content['Category_list'] = $this->db->query($query)->result();

		$query = "select * from linelist_master";
		$content['tempname'] = $this->db->query($query)->result();

		$query = "select template_name from linelist_master where masterId = ? ";
		$content['temp_namelist'] = $this->db->query($query,[$masteID])->row();

		$query = "SELECT c.Category_name,c.category_Id FROM linelist_template t
		LEFT JOIN linelist_master m ON t.masterId=m.masterId
		LEFT JOIN linelist_category c ON m.masterId=c.category_Id
		WHERE t.user_Id = ? GROUP BY m.masterId";
		$content['category_namelist'] = $this->db->query($query,[$loginData->id_tblusers])->result();


		$content['subview'] = 'linelist_masters';
		$this->load->view('admin/main_layout', $content);
	}

	public function label_namelist()
	{
		$cat_id = $this->security->xss_clean($this->input->post('ids'));
		$query = "select label_name,category_Id,lcategoryId from linelist_category where category_Id in ?";
		$label_list = $this->db->query($query,[$cat_id])->result();

		$dom = '';

		foreach ($label_list as $value) { 
			$dom .= "<option value='".$value->lcategoryId."' selected='selected'>".$value->label_name."</option>";
		}
		echo $dom;

	}


	public function label_categorylist()
	{
		$loginData = $this->session->userdata('loginData');
		$id = $this->security->xss_clean($this->input->post('id'));
		$query = "SELECT c.label_name,m.masterId,m.template_name,t.categoryId FROM linelist_template t 
		LEFT JOIN linelist_master m ON t.masterId=m.masterId
		LEFT JOIN linelist_category c ON t.categoryId=c.lcategoryId WHERE t.user_Id = ".$loginData->id_tblusers." AND m.masterId = ".$id."";
		$label_list1 = $this->db->query($query)->result_array();
		
		echo json_encode($label_list1);

		// $dom = '';

		// foreach ($label_list1 as $value) { 
		// 	$dom .= "<option value='".$value->masterId."' selected='selected'>".$value->label_name."</option>";
		// }
		// echo $dom;

	}

	

	// public function get_line_list_result($filter_facility = "", $limit = 5000, $offset = 0,$mas_id= null)
	// {
	// 	$masterid= $mas_id;

	//     $query = "SELECT ct.label_name,ct.db_name FROM linelist_template tmp 
	// 	LEFT JOIN linelist_category ct ON tmp.categoryId=ct.lcategoryId 
	// 	WHERE tmp.masterId  = ".$masterid."";

	// 	$db_name= array();
	// 	$resdata = $this->db->query($query)->result();	

	// 	foreach($resdata as $row){
	// 		$db_nameval[] =  $row->db_name;
	// 		$db_namevalval = implode(" ", $db_nameval);

	// 	}
	//      $query = "SELECT
	// 	".rtrim($db_namevalval,',')." 
	// 	from linelist p";

	// 	return $this->db->query($query)->result();



	// }



}