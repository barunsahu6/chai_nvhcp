<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Linelist extends CI_Controller
{
    private $sess_where = '1';
    private $sess_mstdistrict ='1';
    private $sess_mstfacility ='1';
    private $filter = '1';
    private $filter1 = '1';
    public function __construct()
    {        
      parent::__construct(); 
      $this->load->model('Common_Model');
      $this->load->model('Patient_Model');
      $this->load->helper('common');
      ini_set('memory_limit', '1024M');
      ini_set('memory_limit', '-1');
      $loginData = $this->session->userdata('loginData');

      $startdate = date('Y-m-01');  
      $enddate = date('Y-m-d');
        //Login filter query
      if( ($loginData) && $loginData->user_type == '1' ){
          $sess_where = '1';
          $sess_mstdistrict ='1';
          $sess_mstfacility ='1';

          $this->filter = " p.CreatedOn between '".($startdate)."' and '".($enddate)."'";
          $this->filter1 = " p1.CreatedOn between '".($startdate)."' and '".($enddate)."'";
      }
      elseif( ($loginData) && $loginData->user_type == '2' ){

          $this->sess_where = " id_mststate = '".$loginData->State_ID."'";

          $this->sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

          $this->sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

          $this->filter = " p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."' AND p.id_mstfacility='".$loginData->id_mstfacility."' ";
          $this->filter1 = " p1.Session_StateID = '".$loginData->State_ID."' AND p1.Session_DistrictID ='".$loginData->DistrictID."' AND p1.id_mstfacility='".$loginData->id_mstfacility."' ";
      }
      elseif( ($loginData) && $loginData->user_type == '3' ){ 
          $this->sess_where = " id_mststate = '".$loginData->State_ID."'";
          $this->sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
          $this->sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";

          $this->filter = " p.Session_StateID = '".$loginData->State_ID."'";
          $this->filter1 = " p1.Session_StateID = '".$loginData->State_ID."'";
      }
      elseif( ($loginData) && $loginData->user_type == '4' ){ 
          $this->sess_where       = " id_mststate = '".$loginData->State_ID."'";
          $this->sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
          $this->sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";

          $this->filter = " AND p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."'";
          $this->filter1 = " AND p1.Session_StateID = '".$loginData->State_ID."' AND p1.Session_DistrictID ='".$loginData->DistrictID."'";
      }
    	/*	if($loginData->user_type != 1){
            $this->session->set_flashdata('er_msg','You are not logged-in, please login again to continue');
            redirect('login');
        }*/
        if ($this->session->userdata('loginData') == null) {
        	redirect('login');
        }

        $this->header = array( 
            'UID_NUM',
            'OPD_Id',     
            'FirstName',  			    
            'StateName',
            'DistrictName',
            'BlockName',
            'VillageTown',
            'FacilityCode',
            'FacilityType',
            'facilityName',  
            'UID_Prefix',		    	    
            'gender',
            'GenotypeTest_Result',
            'FatherHusband',
            'Add1',		    
            'PIN',
            'Mobile',
            'T_DLL_01_Date',
            'T_AntiHCV01_Result',
            'T_DLL_01_VLC_Date',
            'T_DLL_01_VLCount',
            'T_DLL_01_VLC_Result',
            'V1_Haemoglobin',
            'V1_Albumin',
            'V1_Bilrubin',
            'V1_INR',
            'V1_Cirrhosis',
            'Cirr_TestDate',
            'Cirr_Encephalopathy',
            'Cirr_Ascites',
            'Cirr_VaricealBleed',
            'ChildScore',
            'Result',
            'T_Initiation',
            'T_RmkDelay',		    
            'T_NoPillStart',
            'T_VisitPlan',
            'DrugSideEffect',
            'ETR_HCVViralLoad_Dt',
            'ETR_HCVViralLoadCount',
            'ETR_Result',
            'SVR12W_HCVViralLoad_Dt',
            'SVR12W_HCVViralLoadCount',
            'SVR12W_Result',
            'SVR12W_LabID',
            'patientAdherence',
            'SVR_TreatmentStatus',
            'Age',            
            'T_DurationValue',
            'SVR_LabName',
            'InitiationDt',
            'ETRDt',
            'Pregnant',
            'DeliveryDt',
            'HCVHistory',
            'HCVPastTreatment',
            'HCVPastOutcome',
            'V1_Platelets',		    
            'AdvisedSVRDate',
            'ETR_PillsLeft',
            'T_DurationOther',
            'Current_Visitdt',
            'Next_Visitdt',
            'PastTreatmentDuration',
            'PastDuration',
            'IsMobile_Landline',
            'fragment3editreason',
            'NextVisitPurpose',
            'ETRComments',
            'SVRComments',
            'IsETRDone',
            'ETRTestDate',
            'PreviousTreatingHospital',
            'PreviousUIDNumber',
            'ETRAutoupdate',
            'NewPatient',
            'PatientType',
            'PastTreatment',
            'PastFacility',
            'PastUID',
            'Weight',
            'CKDStage',
            'NWeeksCompleted',
            'LastPillDate',
            'PastRegimen',
            'PreviousDuration',
            'InitiateDoctor',
            'ETRDoctor',
            'SVRDoctor',
            'InterruptReason',
            'InterruptToStage',
            'VisitInterrupted',
            'TransferRequest',
            'TransferRequestAccepted',
            'TransferUID',
            'TransferFromFacility',
            'TransferToFacility',
            'id_mst_medical_specialists',
            'HospitalName',
            'IsSMSConsent',
            'OtherRisk',
            'OccupationID',
            'ETRLFUreason',
            'InitiateDoctorOther',
            'ETRDoctorOther',
            'SVRDoctorOther',
            'VLSampleCollectionDate',
            'VLStorageTemp',
            'VLTransportDate',
            'VLLabID',
            'VLRecieptDate',
            'Relation',
            'State',
            'AntiHCV',
            'HCVRapid',
            'HCVRapidDate',
            'HCVRapidResult',
            'HCVRapidPlace',
            'HCVRapidLabID',
            'HCVRapidLabOther',
            'HCVElisa',
            'HCVElisaDate',
            'HCVElisaResult',
            'HCVElisaPlace',
            'HCVElisaLabID',
            'HCVElisaLabOther',
            'HCVOther',
            'HCVOtherName',
            'HCVOtherDate',
            'HCVOtherResult',
            'HCVOtherPlace',
            'HCVOtherLabID',
            'HCVLabOther',
            'LgmAntiHAV',
            'HAVRapid',
            'HAVRapidDate',
            'HAVRapidResult',
            'HAVRapidPlace',
            'HAVRapidLabID',
            'HAVRapidLabOther',
            'HAVElisa',
            'HAVElisaDate',
            'HAVElisaResult',
            'HAVElisaPlace',
            'HAVElisaLabID',
            'HAVElisaLabOther',
            'HAVOther',
            'HAVOtherName',
            'HAVOtherDate',
            'HAVOtherResult',
            'HAVOtherPlace',
            'HAVOtherLabID',
            'HAVLabOther',
            'HbsAg',
            'HBSRapid',
            'HBSRapidDate',
            'HBSRapidResult',
            'HBSRapidPlace',
            'HBSRapidLabID',
            'HBSRapidLabOther',
            'HBSElisa',
            'HBSElisaDate',
            'HBSElisaResult',
            'HBSElisaPlace',
            'HBSElisaLabID',
            'HBSElisaLabOther',
            'HBSOther',
            'HBSOtherName',
            'HBSOtherDate',
            'HBSOtherResult',
            'HBSOtherPlace',
            'HBSOtherLabID',
            'HBSLabOther',
            'LgmAntiHBC',
            'HBCRapid',
            'HBCRapidDate',
            'HBCRapidResult',
            'HBCRapidPlace',
            'HBCRapidLabID',
            'HBCRapidLabOther',
            'HBCElisa',
            'HBCElisaDate',
            'HBCElisaResult',
            'HBCElisaPlace',
            'HBCElisaLabID',
            'HBCElisaLabOther',
            'HBCOther',
            'HBCOtherName',
            'HBCOtherDate',
            'HBCOtherResult',
            'HBCOtherPlace',
            'HBCOtherLabID',
            'HBCLabOther',
            'LgmAntiHEV',
            'HEVRapid',
            'HEVRapidDate',
            'HEVRapidResult',
            'HEVRapidPlace',
            'HEVRapidLabID',
            'HEVRapidLabOther',
            'HEVElisa',
            'HEVElisaDate',
            'HEVElisaResult',
            'HEVElisaPlace',
            'HEVElisaLabID',
            'HEVElisaLabOther',
            'HEVOther',
            'HEVOtherName',
            'HEVOtherDate',
            'HEVOtherResult',
            'HEVOtherPlace',
            'HEVOtherLabID',
            'HEVLabOther',
            'VLTransporterName',
            'VLTransporterDesignation',
            'VLReceiverName',
            'VLReceiverDesignation',
            'SVRDrawnDate',
            'SVRStorageTemp',
            'SVRTransportDate',
            'SVRLabID',
            'SVRTransporterName',
            'SVRTransporterDesignation',
            'SVRReceiptDate',
            'SVRReceiverName',
            'SVRReceiverDesignation',
            'V1_Creatinine',
            'V1_EGFR',
            'BreastFeeding',
            'ART_Regimen',
            'Ribavirin',
            'IsReferal',
            'ReferingDoctor',
            'ReferTo',
            'ReferalDate',
            'PrescribingFacility',
            'PrescribingDoctor',
            'PrescribingDate',
            'ReferingDoctorOther',
            'PrescribingDoctorOther',
            'MF5',
            'MF6',
            'fragment3_1editreason',
            'fragment3_2editreason',
            'IsAgeMonths',
            'TreatmentUpdatedBy',
            'TreatmentUpdatedOn',
            'fragment3_3editreason',
            'VLLabID_Other',
            'PastRegimen_Other',
            'SCRemarks',
            'PrescriptionPlace',
            'IsVLSampleStored',
            'IsVLSampleTransported',
            'VLTransportTemp',
            'VLSCRemarks',
            'VLResultRemarks',
            'VLHepB',
            'VLHepC',
            'VLReferredLab',
            'ALT',
            'AST',
            'AST_ULN',
            'DispensationPlace',
            'CirrhosisStatus',
            'IsSVRSampleStored',
            'IsSVRSampleTransported',
            'SVRTransportTemp',
            'SVRTransportRemark',
            'BVLTransportTemp',
            'IsBVLSampleStored',
            'IsBVLSampleTransported',
            'BVLSCRemarks',
            'BVLResultRemarks',
            'BVLSampleCollectionDate',
            'BVLTransportDate',
            'BVLStorageTemp',
            'BVLTransporterName',
            'BVLReceiverName',
            'BVLTransporterDesignation',
            'BVLReceiverDesignation',
            'T_DLL_01_BVLC_Result',
            'BVLLabID',
            'BVLLabID_Other',
            'T_DLL_01_BVLCount',
            'T_DLL_01_BVLC_Date',
            'BVLRecieptDate',
            'patientPDispensation',
            'PTState',
            'PTYear',
            'Storage_days_hrs',
            'Storageduration',
            'ReferredDispensationPlace',
            'BStorage_days_hrs',
            'BStorageduration',
            'IsSampleAccepted',
            'IsBSampleAccepted',
            'RejectionReason',
            'BRejectionReason',
            'RejectionReasonOther',
            'BRejectionReasonOther',
            'patientNAdherenceReason',
            'patientNAdherenceReasonOther',
            'LMP',		    
            'DeathReason',
            'LFUReason',
            'ExperiencedCategory',
            'OutsideYear',
            'RecommendedRegimen',
            'RecommendedDuration',
            'SideEffectValue',
            'DurationReason',
            'DistrictOther',
            'BlockOther',
            'Refer_FacilityHAV',
            'Refer_HigherFacilityHAV',
            'Refer_FacilityHEV',
            'Refer_HigherFacilityHEV',
            'CaseType',
            'InterruptReasonOther',
            'IsSVRSampleAccepted',
            'SVRRejectionReason',
            'SVRRejectionReasonOther',
            'SVR12W_LabOther',
            'ShortName',
            'ActiveFrom',
            'ActiveTo',
            'PrefixMedCard',
            'AddressLine1',
            'AddressLine2',
            'POBox',
            'City',
            'PinCode',
            'STDCode',
            'Phone1',
            'Phone2',
            'email',
            'LocationDet',
            'NameLocalLng',
            'LanguageID',
            'facility_name',
            'facility_short_name',
            'name',
            'Visit_Dt',
            'ExpectedVIsits',
            'PillsLeft',
            'PillsIssued',
            'DaysSinceLastVisit',
            'PillsConsumed',
            'Adherence',
            'NextVisit_Dt',
            'Regimen',
            'Haemoglobin',
            'PlateletCount',
            'SideEffect',
            'Comments',
            'VisitFlag',
            'Doctor',
            'DoctorOther',
            'pvSideEffectValue',
            'PDispensation',
            'NAdherenceReason',
            'NAdherenceReasonOther',
            'regimen_name',
            'strength',
            'RiskFactor'
        );
$this->folder_path = FCPATH . "application" . DIRECTORY_SEPARATOR . "linelists" . DIRECTORY_SEPARATOR;

$this->load->helper('file');
delete_files($this->folder_path);
}

public function index()
{
     // die($this->filter);
    $REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
    if($REQUEST_METHOD == 'POST')
    {
        $id_search_state = $this->input->post('search_state');
        $id_input_district = $this->input->post('input_district');
        $id_mstfacility = $this->input->post('facility');
        $startdate = $this->input->post('startdate');
        $enddate = $this->input->post('enddate');		

        $PostFilter = "p.Session_StateID = '".$id_search_state."' AND p.Session_DistrictID = '".$id_input_district."' AND p.id_mstfacility = '".$id_mstfacility."' AND p.CreatedOn between '".($startdate)."' AND '".($enddate)."'  ";
    }
    /*else
    {	
      $filters1['id_search_state'] = 0 ;
      $filters1['id_input_district'] = 0;
      $filters1['id_mstfacility'] = 0;	
      $startdate = date('Y-m-01');	
      $enddate = date('Y-m-d');

      $filter = " p.CreatedOn between '".($startdate)."' and '".($enddate)."'";			
      $filter1 = " p1.CreatedOn between '".($startdate)."' and '".($enddate)."'";         
  }*/

  // $this->session->set_userdata('filters1', $filters1);  

  $content['start_date'] = date('Y-m-d');
  $content['end_date']   = date('Y-m-d');

  $sql = "select 
          * 
  from 
  ( 
  SELECT 
  f.id_mstfacility, 
  f.facility_short_name AS hospital, 
  COUNT(PatientGUID) AS 'patients' 
  FROM view_linelist p 
  INNER JOIN mstfacility f 
  ON p.id_mstfacility = f.id_mstfacility 
  WHERE 1=1 AND ".(isset($PostFilter)?$PostFilter:$this->filter)." 
  GROUP BY f.facility_short_name 
  UNION 
  SELECT 
  0, 'All' AS hospital, 
  COUNT(PatientGUID) AS 'patients' 
  FROM view_linelist p1 where  ".$this->filter1." 
) a ORDER BY a.id_mstfacility";


$res = $this->Common_Model->query_data($sql);
$sql = "select * from mststate where ".$this->sess_where."";
$content['states'] = $this->db->query($sql)->result();

$sql = "select * from mstdistrict where ".$this->sess_mstdistrict."";
$content['districts'] = $this->db->query($sql)->result();

$sql = "select * from mstfacility where  ".$this->sess_mstfacility."";
$content['facilities'] = $this->db->query($sql)->result();	

$content['facilities'] = $res;
$content['subview'] = 'linelist_hospital';
$this->load->view('admin/main_layout', $content);
}

public function getlist($id_mstfacility = null)
{
	ini_set('memory_limit', '-1');
	if ($id_mstfacility > 0) {

		$sql_facility_name = "select facilitycode from mstfacility where id_mstfacility = " . $id_mstfacility;
		$facility_name = $this->Common_Model->query_data($sql_facility_name);
		$facility = $facility_name[0]->facilitycode;
		$facility_where = "where id_mstfacility = " . $id_mstfacility;
	} else {
		$facility_where = "";
		$facility = "ALL";
	}

        // $sql_total_recs = "select count(*) as count from tblpatient where id_mstfacility = ".$id_mstfacility;
        // $res = $this->Common_Model->query_data($sql_total_recs);
        // $total_recs = $res[0]->count;

	$filename = $facility . '_' . date('Y-m-d') . ".csv";
	header("Content-Disposition: attachment; filename=\"$filename\"");
	header("Content-Type: text/csv");

	$out = fopen("php://output", 'w');
	$flag = false;

	$sql_line_list = "select 
	UID,TreatingHospital,Name,Age,Gender,RiskFactors,FathersHusbandsName,Address,Villagetown,Area,Block,District,Pincode,ContactNo,SampleCollectionDateforAntiHCVTest,AntiHCVTestDate,ResultReceivedDatebyPatient,RESULTOFANTIHCVTESTINGELISA,SampleCollectionDateforVL,SampleCollectioncenternameforVL,ViralLoadTestDate,BaselineViralLoad,ViralLoadStatus,DateofArrivaltoHospital,MedicalSpecialist,CirrhosisNoCirrhosisSTatus,ClinicalUSGtestdate,ClinicalUSGresult,Fibroscantestdate,LSMValueinkPa,Fibroscanresult,APRItestdate,AST,ASTNormal,APRIScore,FIB4testdate,ALT,FIB4Score,Albumin,Bilurubin,INR,Baseline1_val,Visit2_1_val,Visit3_1_val,Visit4_1_val,Visit5_1_val,Visit6_1_val,Visit7_1,Baseline2_val,Visit2_2_val,Visit3_2_val,Visit4_2_val,Visit5_2_val,Visit6_2_val,Visit7_2_val,DecompensatedCirrhosisResult,DecompensatedCirrhosisTestdate,Encephalopathy,Ascites,VaricealBleed,ChildScore,Genotype,GenotypeTestDate,Treatmentinitiationdate,Regimen,Duration,PillsDispensed,AdvisedNextVisitDate,Visit2_2,PillsLeft_2,AdvisedNextVisitDate_2,Adherence_2,Visit3_3,PillsLeft_3,AdvisedNextVisitDate_3,Adherence_3,Visit4_4,PillsLeft_4,AdvisedNextVisitDate_4,Adherence_4,Visit5_5,PillsLeft_5,AdvisedNextVisitDate_5,Adherence_5,Visit6_6,PillsLeft_6,AdvisedNextVisitDate_6,Adherence_6,Visit7_7,PillsLeft_7,AdvisedNextVisitDate_7,Adherence_7,ETRTestDate,ETRViralLoad,ETRTestResult,SVRTestDate,SVRViralLoad,SVRTestResult,DrugSideEffect,DrugCompliance,PatientStatus,TreatmentStatus,ReasonforTreatmentFailure,TreatmentCardType,Remarks,Durationoftrt_from_private,
	category,jailno,UploadedOn,lal_svr_date,lal_svr_result,lal_viral_load,
	AltContact,history_of_hcv,uid_serial_number,previous_treating_hospital,previous_treatment_outcome,occupation,occupation_other,hiv,ckd,diabetes,hypertension,hbv,call_output,cumulative_svr,
	cumulative_svr_viral_load,cumulative_svr_outcome
	from linelist " . $facility_where;

        // echo $sql_line_list; die();
        // ini_set('display_errors', 1);

	$content['line_list'] = $this->Common_Model->query_data($sql_line_list);

	if (count($content['line_list']) > 0) {
		foreach ($content['line_list'] as $row) {
                // print_r($row); die();
			if (!$flag) {
                    // display field/column names as first row
                    // $firstline = array_map(array($this,'map_colnames'), array_keys((array)$row));
				fputcsv($out, $this->header, ',', '"');
				$flag = true;
			}
			foreach ((array)$row as $value) {
				if ($value == null) {
					$value = '';
				}
			}
			fputcsv($out, array_values((array)$row), ',', '"');
		}
	}
        // }
	fclose($out);
	exit();
}

public function download_list_zipped($id_mstfacility = 0)
{
	ini_set('memory_limit', '-1');

	if ($id_mstfacility > 0) {

		$query = "select facilitycode from mstfacility where id_mstfacility = " . $id_mstfacility;
		$result = $this->db->query($query)->result();
		$filename = $result[0]->facilitycode . "-" . date('Y-m-d-His');
		$facility_where = " ".$this->filter." and p.id_mstfacility = " . $id_mstfacility;

	} else {
		$facility_where = " ".$this->filter." ";
		$filename = "ALL-" . date('Y-m-d-His');
	}
    // die($facility_where);
	$query = "select count(*) as count from view_linelist p WHERE 1=1 AND" . $facility_where;
	$count_res = $this->db->query($query)->result();
	$count = $count_res[0]->count;
	$offset = 0;
	$limit = 2000;
	$file_path = $this->folder_path . $filename;

	while ($offset < $count) {
		if ($offset == 0) {

			$file = fopen($file_path . '.csv', "w");            
			if (!$file) {
				die('cant open file handler');
			}
			fputcsv($file, $this->header);
		} else {
			$file = fopen($file_path . ".csv", "a");
		}

		$result = $this->get_line_list_result($facility_where, $limit, $offset);  

        foreach ($result as $row) {
            fputcsv($file, (array)$row);
        }

        fclose($file);
        $offset += $limit;
    }

    $this->load->library('zip');

    if (!$this->zip->read_file($file_path . '.csv')) {
      echo "cant read file";
      die();
  } else {
      header('Content-Encoding: UTF-8');
      $this->zip->download($filename . '.zip');
  }
}

public function get_line_list_result($filter_facility = "", $limit = 5000, $offset = 0)
{
	ini_set('memory_limit', '-1');
	$query = "SELECT
    lpad(p.UID_Num,6,0) as uid_num,
    p.OPD_Id,
    p.FirstName,		    
    p.StateName,
    p.DistrictName,
    p.BlockName,
    p.VillageTown,
    p.FacilityCode,
    p.FacilityType,
    p.facilityName,
    p.UID_Prefix,		    	    
    p.gender,
    p.GenotypeTest_Result,
    p.FatherHusband,
    p.Add1,		    
    p.PIN,
    p.Mobile,
    p.T_DLL_01_Date,
    CASE p.T_AntiHCV01_Result WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS T_AntiHCV01_Result,
    p.T_DLL_01_VLC_Date,
    p.T_DLL_01_VLCount,
    CASE p.T_DLL_01_VLC_Result WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS T_DLL_01_VLC_Result,
    p.V1_Haemoglobin,
    p.V1_Albumin,
    p.V1_Bilrubin,
    p.V1_INR,
    p.V1_Cirrhosis,
    p.Cirr_TestDate,
    p.Cirr_Encephalopathy,
    p.Cirr_Ascites,
    p.Cirr_VaricealBleed,
    p.ChildScore,
    p.Result,
    p.T_Initiation,
    p.T_RmkDelay,		    
    p.T_NoPillStart,
    p.T_VisitPlan,
    p.DrugSideEffect,
    p.ETR_HCVViralLoad_Dt,
    p.ETR_HCVViralLoadCount,
    CASE p.ETR_Result WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS ETR_Result,	
    p.SVR12W_HCVViralLoad_Dt,
    p.SVR12W_HCVViralLoadCount,
    CASE p.SVR12W_Result WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS SVR12W_Result,	
    p.SVR12W_LabID,
    p.patientAdherence,
    p.SVR_TreatmentStatus,
    p.Age,	
    p.T_DurationValue,
    p.SVR_LabName,
    p.InitiationDt,
    p.ETRDt,
    p.Pregnant,
    p.DeliveryDt,
    p.HCVHistory,
    p.HCVPastTreatment,
    p.HCVPastOutcome,
    p.V1_Platelets,		    
    p.AdvisedSVRDate,
    p.ETR_PillsLeft,
    p.T_DurationOther,
    p.Current_Visitdt,
    p.Next_Visitdt,
    p.PastTreatmentDuration,
    p.PastDuration,
    p.IsMobile_Landline,
    p.fragment3editreason,
    p.NextVisitPurpose,
    p.ETRComments,
    p.SVRComments,
    p.IsETRDone,
    p.ETRTestDate,
    p.PreviousTreatingHospital,
    p.PreviousUIDNumber,
    p.ETRAutoupdate,
    p.NewPatient,
    p.PatientType,
    p.PastTreatment,
    p.PastFacility,
    p.PastUID,
    p.Weight,
    p.CKDStage,
    p.NWeeksCompleted,
    p.LastPillDate,
    p.PastRegimen,
    p.PreviousDuration,
    p.InitiateDoctor,
    p.ETRDoctor,
    p.SVRDoctor,
    p.InterruptReason,
    p.InterruptToStage,
    p.VisitInterrupted,
    p.TransferRequest,
    p.TransferRequestAccepted,
    p.TransferUID,
    p.TransferFromFacility,
    p.TransferToFacility,
    p.id_mst_medical_specialists,
    p.HospitalName,
    p.IsSMSConsent,
    p.OtherRisk,
    p.OccupationID,
    p.ETRLFUreason,
    p.InitiateDoctorOther,
    p.ETRDoctorOther,
    p.SVRDoctorOther,
    p.VLSampleCollectionDate,
    p.VLStorageTemp,
    p.VLTransportDate,
    p.VLLabID,
    p.VLRecieptDate,
    p.Relation,
    p.State,
    p.AntiHCV,
    p.HCVRapid,
    p.HCVRapidDate,
    CASE p.HCVRapidResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HCVRapidResult,
    p.HCVRapidPlace,
    p.HCVRapidLabID,
    p.HCVRapidLabOther,
    p.HCVElisa,
    p.HCVElisaDate,
    CASE p.HCVElisaResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HCVElisaResult,		    
    p.HCVElisaPlace,
    p.HCVElisaLabID,
    p.HCVElisaLabOther,
    p.HCVOther,
    p.HCVOtherName,
    p.HCVOtherDate,
    CASE p.HCVOtherResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HCVOtherResult,		    
    p.HCVOtherPlace,
    p.HCVOtherLabID,
    p.HCVLabOther,
    p.LgmAntiHAV,
    p.HAVRapid,
    p.HAVRapidDate,
    CASE p.HAVRapidResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HAVRapidResult,		    
    p.HAVRapidPlace,
    p.HAVRapidLabID,
    p.HAVRapidLabOther,
    p.HAVElisa,
    p.HAVElisaDate,
    CASE p.HAVElisaResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HAVElisaResult,		    
    p.HAVElisaPlace,
    p.HAVElisaLabID,
    p.HAVElisaLabOther,
    p.HAVOther,
    p.HAVOtherName,
    p.HAVOtherDate,
    CASE p.HAVOtherResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HAVOtherResult,	
    p.HAVOtherPlace,
    p.HAVOtherLabID,
    p.HAVLabOther,
    p.HbsAg,
    p.HBSRapid,
    p.HBSRapidDate,
    CASE p.HBSRapidResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HBSRapidResult,
    p.HBSRapidPlace,
    p.HBSRapidLabID,
    p.HBSRapidLabOther,
    p.HBSElisa,
    p.HBSElisaDate,
    CASE p.HBSElisaResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HBSElisaResult,		    
    p.HBSElisaPlace,
    p.HBSElisaLabID,
    p.HBSElisaLabOther,
    p.HBSOther,
    p.HBSOtherName,
    p.HBSOtherDate,
    CASE p.HBSOtherResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HBSOtherResult,			    
    p.HBSOtherPlace,
    p.HBSOtherLabID,
    p.HBSLabOther,
    p.LgmAntiHBC,
    p.HBCRapid,
    p.HBCRapidDate,
    CASE p.HBCRapidResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HBCRapidResult,			    
    p.HBCRapidPlace,
    p.HBCRapidLabID,
    p.HBCRapidLabOther,
    p.HBCElisa,
    p.HBCElisaDate,
    CASE p.HBCElisaResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HBCElisaResult,
    p.HBCElisaPlace,
    p.HBCElisaLabID,
    p.HBCElisaLabOther,
    p.HBCOther,
    p.HBCOtherName,
    p.HBCOtherDate,
    CASE p.HBCOtherResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HBCOtherResult,
    p.HBCOtherPlace,
    p.HBCOtherLabID,
    p.HBCLabOther,
    p.LgmAntiHEV,
    p.HEVRapid,
    p.HEVRapidDate,
    CASE p.HEVRapidResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HEVRapidResult,
    p.HEVRapidPlace,
    p.HEVRapidLabID,
    p.HEVRapidLabOther,
    p.HEVElisa,
    p.HEVElisaDate,
    CASE p.HEVElisaResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HEVElisaResult,		    
    p.HEVElisaPlace,
    p.HEVElisaLabID,
    p.HEVElisaLabOther,
    p.HEVOther,
    p.HEVOtherName,
    p.HEVOtherDate,
    CASE p.HEVOtherResult WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS HEVOtherResult,	
    p.HEVOtherPlace,
    p.HEVOtherLabID,
    p.HEVLabOther,
    p.VLTransporterName,
    p.VLTransporterDesignation,
    p.VLReceiverName,
    p.VLReceiverDesignation,
    p.SVRDrawnDate,
    p.SVRStorageTemp,
    p.SVRTransportDate,
    p.SVRLabID,
    p.SVRTransporterName,
    p.SVRTransporterDesignation,
    p.SVRReceiptDate,
    p.SVRReceiverName,
    p.SVRReceiverDesignation,
    p.V1_Creatinine,
    p.V1_EGFR,
    p.BreastFeeding,
    p.ART_Regimen,
    p.Ribavirin,
    p.IsReferal,
    p.ReferingDoctor,
    p.ReferTo,
    p.ReferalDate,
    p.PrescribingFacility,
    p.PrescribingDoctor,
    p.PrescribingDate,
    p.ReferingDoctorOther,
    p.PrescribingDoctorOther,
    p.MF5,
    p.MF6,
    p.fragment3_1editreason,
    p.fragment3_2editreason,
    p.IsAgeMonths,
    p.TreatmentUpdatedBy,
    p.TreatmentUpdatedOn,
    p.fragment3_3editreason,
    p.VLLabID_Other,
    p.PastRegimen_Other,
    p.SCRemarks,
    p.PrescriptionPlace,
    p.IsVLSampleStored,
    p.IsVLSampleTransported,
    p.VLTransportTemp,
    p.VLSCRemarks,
    p.VLResultRemarks,
    p.VLHepB,
    p.VLHepC,
    p.VLReferredLab,
    p.ALT,
    p.AST,
    p.AST_ULN,
    p.DispensationPlace,
    p.CirrhosisStatus,
    p.IsSVRSampleStored,
    p.IsSVRSampleTransported,
    p.SVRTransportTemp,
    p.SVRTransportRemark,
    p.BVLTransportTemp,
    p.IsBVLSampleStored,
    p.IsBVLSampleTransported,
    p.BVLSCRemarks,
    p.BVLResultRemarks,
    p.BVLSampleCollectionDate,
    p.BVLTransportDate,
    p.BVLStorageTemp,
    p.BVLTransporterName,
    p.BVLReceiverName,
    p.BVLTransporterDesignation,
    p.BVLReceiverDesignation,
    CASE p.T_DLL_01_BVLC_Result WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' END AS T_DLL_01_BVLC_Result,
    p.BVLLabID,
    p.BVLLabID_Other,
    p.T_DLL_01_BVLCount,
    p.T_DLL_01_BVLC_Date,
    p.BVLRecieptDate,
    p.patientPDispensation,
    p.PTState,
    p.PTYear,
    p.Storage_days_hrs,
    p.Storageduration,
    p.ReferredDispensationPlace,
    p.BStorage_days_hrs,
    p.BStorageduration,
    p.IsSampleAccepted,
    p.IsBSampleAccepted,
    p.RejectionReason,
    p.BRejectionReason,
    p.RejectionReasonOther,
    p.BRejectionReasonOther,
    p.patientNAdherenceReason,
    p.patientNAdherenceReasonOther,
    p.LMP,		    
    p.DeathReason,
    p.LFUReason,
    p.ExperiencedCategory,
    p.OutsideYear,
    p.RecommendedRegimen,
    p.RecommendedDuration,
    p.SideEffectValue,
    p.DurationReason,
    p.DistrictOther,
    p.BlockOther,
    p.Refer_FacilityHAV,
    p.Refer_HigherFacilityHAV,
    p.Refer_FacilityHEV,
    p.Refer_HigherFacilityHEV,
    p.CaseType,
    p.InterruptReasonOther,
    p.IsSVRSampleAccepted,
    p.SVRRejectionReason,
    p.SVRRejectionReasonOther,
    p.SVR12W_LabOther,
    p.ShortName,
    p.ActiveFrom,
    p.ActiveTo,
    p.PrefixMedCard,
    p.AddressLine1,
    p.AddressLine2,
    p.POBox,
    p.City,
    p.PinCode,
    p.STDCode,
    p.Phone1,
    p.Phone2,
    p.email,
    p.LocationDet,
    p.NameLocalLng,
    p.LanguageID,
    p.facility_name,
    p.facility_short_name,
    p.name,
    p.Visit_Dt,
    p.ExpectedVIsits,
    p.PillsLeft,
    p.PillsIssued,
    p.DaysSinceLastVisit,
    p.PillsConsumed,
    p.Adherence,
    p.NextVisit_Dt,
    p.Regimen,
    p.Haemoglobin,
    p.PlateletCount,
    p.SideEffect,
    p.Comments,
    p.VisitFlag,
    p.Doctor,
    p.DoctorOther,
    p.pvSideEffectValue,
    p.PDispensation,
    p.NAdherenceReason,
    p.NAdherenceReasonOther,
    p.regimen_name,
    p.strength,
    p.lookup_value
    FROM
    view_linelist p WHERE 1=1 AND " . $filter_facility . "  
    limit $limit 
    offset $offset";

    // die($query);

    return $this->db->query($query)->result();
}
}
