<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Common_Model');

		$loginData = $this->session->userdata('loginData');
		$loginData = $this->session->userdata('loginData');
		if($loginData == null)
		{
			redirect('login');
		}
	}

	public function index($ids = null)
	{
		//error_reporting(0);
		$loginData = $this->session->userdata('loginData');
		//echo "<pre>";print_r($loginData);
		$loginData = $this->session->userdata('loginData'); 

			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
			}


		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

		if($REQUEST_METHOD == 'POST')
		{
			
			$filters['id_search_state'] = $this->input->post('search_state');
			$filters['id_input_district'] = $this->input->post('input_district');
			$filters['id_mstfacility'] = $this->input->post('facility');	
			$filters['startdate']      = $this->input->post('startdate');	
			$filters['enddate']        = $this->input->post('enddate');	
		}
		else
		{	
			 $filters['id_search_state'] = 0;
			$filters['id_input_district'] = 0;
			$filters['id_mstfacility'] = 0;	
			$filters['startdate']      = '2018-01-01';	
			$filters['enddate']        = date('Y-m-d');
			//pr($content['filters1']);exit();
		}

		 
		$this->session->set_userdata('filters', $filters);
		$this->load->model('Dashboard_Model');

		$content['filters']             = $this->session->userdata('filters');

		$content['initiated_treatment']          = $this->Dashboard_Model->initiated_treatment();
		$content['treatment_completed']          = $this->Dashboard_Model->treatment_completed();
		$content['cascade']                      = $this->Dashboard_Model->cascade();
		$content['cascade_hav_patients']         = $this->Dashboard_Model->cascade_hav_patients();
		$content['cascade_hev_patients']         = $this->Dashboard_Model->cascade_hev_patients();
		$content['trend']                        = $this->Dashboard_Model->trend();
		$content['trendanti_hcv_positive']       = $this->Dashboard_Model->trendanti_hcv_positive();
		$content['trendviral_load_tested']       = $this->Dashboard_Model->trendviral_load_tested();
		$content['trendviral_load_detected']     = $this->Dashboard_Model->trendviral_load_detected();
		$content['trendinitiatied_on_treatment'] = $this->Dashboard_Model->trendinitiatied_on_treatment();
		$content['trendtreatment_completed']     = $this->Dashboard_Model->trendtreatment_completed(); 
		$content['trendsvr_done']                = $this->Dashboard_Model->trendsvr_done(); 
		$content['trendtreatment_successful']    = $this->Dashboard_Model->trendtreatment_successful(); 
		$content['trendtreatment_failure']       = $this->Dashboard_Model->trendtreatment_failure();
		$content['cirrhotic_details']            = $this->Dashboard_Model->cirrhotic_details();
		$content['regimen']                      = $this->Dashboard_Model->regimen();
		$content['regimen24']                    = $this->Dashboard_Model->regimen24();
		$content['regimensucess']                = $this->Dashboard_Model->regimensucess();
		$content['regimensucess24']              = $this->Dashboard_Model->regimensucess24();
		$content['regimenfailure']               = $this->Dashboard_Model->regimenfailure();
		$content['regimenfailure24']             = $this->Dashboard_Model->regimenfailure24();
		$content['anty_hcv_to_tesult']           = $this->Dashboard_Model->anty_hcv_to_tesult();
		$content['anty_hcv_to_tesult_median']    = $this->Dashboard_Model->anty_hcv_to_tesult_median();
		$content['vl_samples_accepted_rejected']         = $this->Dashboard_Model->vl_samples_accepted_rejected();
		$content['treatment_Initiations_by_District']    = $this->Dashboard_Model->treatment_Initiations_by_District();
		$content['treatment_Initiations_by_Districthav'] = $this->Dashboard_Model->treatment_Initiations_by_Districthav();
		$content['treatment_Initiations_by_Districthev'] = $this->Dashboard_Model->treatment_Initiations_by_Districthev();
		$content['treatment_by_Districtsvrsuc']          = $this->Dashboard_Model->treatment_by_Districtsvrsuc();
		$content['treatment_by_Districtsvrunsuc']        =$this->Dashboard_Model->treatment_by_Districtsvrunsuc();
		$content['lossfollowacrossdist']                 =$this->Dashboard_Model->lossfollowacrossdist();

		$content['eligible_stage']    =$this->Dashboard_Model->no_of_patients_eligible_stage();
		$content['ltfu_stage']        =$this->Dashboard_Model->no_of_patients_ltfu_stage();
		
		$content['actual_days_taken']    = $this->Dashboard_Model->actual_days_taken();
		$content['state_tat_median']     = $this->Dashboard_Model->state_tat_median();
		$content['special_case']         = $this->Dashboard_Model->lost_to_followup_special_case();
		$content['age']                  = $this->Dashboard_Model->age();
		$content['risk_factor_analysis'] = $this->Dashboard_Model->risk_factor_analysis();
		$content['age_vl_detected'] = $this->Dashboard_Model->age_wise_vl_detected();

		$content['scr_vs_tre_init'] = $this->Dashboard_Model->screening_vs_treatment_initiations();

	 if ($loginData->user_type==1) {
		$content['screened_antihcv_across_states']    = $this->Dashboard_Model->screened_antihcv_across_states();
		$content['treatment_Initiations_by_states']   = $this->Dashboard_Model->treatment_Initiations_by_Districthav_state();
		$content['treatment_Initiations_by_statehev'] = $this->Dashboard_Model->treatment_Initiations_by_statehev();
		$content['treatment_by_statesvrsucess']        =$this->Dashboard_Model->treatment_by_statesvrsucess();
		$content['treatment_by_statesvrunsuc']        =$this->Dashboard_Model->treatment_by_statesvrunsuc();
	}
		//print_r($content['regimen24']);
		/*$content['havhbc_status']    = $this->Dashboard_Model->havhbc_status();
		$content['genotype']            = $this->Dashboard_Model->genotype();
		$content['gender_area']         = $this->Dashboard_Model->gender_area();
		
		$content['risk_factor']         = $this->Dashboard_Model->risk_factor();
		$content['occupation']          = $this->Dashboard_Model->occupation();
		$content['tat']                 = $this->Dashboard_Model->tat();
		$content['adherence']           = $this->Dashboard_Model->adherence();
		
		$content['map_data']            = $this->Dashboard_Model->map_data();
		$content['regimen']             = $this->Dashboard_Model->regimen();
		
		$content['treatment_completed_facility_wise_data'] = $this->Dashboard_Model->treatment_completed_facility_wise_data();
		$content['missed_appointment']                     = $this->Dashboard_Model->missed_appointment();
		$content['missed_appointment_facility_wise_data']  = $this->Dashboard_Model->missed_appointment_facility_wise_data();
		$content['lfu']                                    = $this->Dashboard_Model->lfu();
		$content['lfu_facility_wise_data']                 = $this->Dashboard_Model->lfu_facility_wise_data();
		$content['follow_up']                              = $this->Dashboard_Model->follow_up();
		$content['follow_up_facility_wise_data']           = $this->Dashboard_Model->follow_up_facility_wise_data();*/
		
		if ($loginData->id_mstfacility=='' || $loginData->id_mstfacility==0) {
			$id_mstfacility = 1;
		}else{
			$id_mstfacility = "id_tblusers = ".$loginData->id_tblusers;
		}


		 $sql = "SELECT * FROM `tblusers` WHERE  ".$id_mstfacility;
		$loginuser_data = $this->db->query($sql)->result();
		
		if ($loginData->id_mstfacility=='' || $loginData->id_mstfacility==0) {
			$id_mstfacilityids = 1;
		}else{
			$id_mstfacilityids = "id_mstfacility = ".$loginuser_data[0]->id_mstfacility;
		}
		$sql = "select * from mstfacility where  ".$id_mstfacilityids;
		$content['default_facilities'] = $this->db->query($sql)->result();

		if ($loginData->id_mstfacility=='' || $loginData->id_mstfacility==0) {
			$id_mststate = 1;
		}else{
			$id_mststate = "id_mstdistrict = ".$content['default_facilities'][0]->id_mstdistrict;
		}
		 $sql = "select * from mstdistrict where ".$id_mststate;
		//die($sql);
		$content['default_districts'] = $this->db->query($sql)->result();

		//$sql = "SELECT * FROM `mstlookup` where flag = 44 and LanguageID = 1 order by Sequence ASC";
		 $sql = "SELECT SUM(LENGTH(p.SideEffectValue)) as count,m.LookupValue,p.SideEffectValue,m.LookupCode FROM mstlookup m left join tblpatient p  ON find_in_set(m.LookupCode,p.SideEffectValue) where m.flag = 3 and m.LanguageID = 1 and ".$sess_where." group by m.LookupCode order by m.Sequence ASC";
		$content['side_effects'] = $this->db->query($sql)->result();

		//$sql = "SELECT * FROM mstlookup m where m.flag = 3 and m.LanguageID = 1 order by m.Sequence ASC";
		//$content['side_effects'] = $this->db->query($sql)->result();
		
		/*  $sql = "select mst.id_mststate,mst.StateName from tblusers us inner join mststate mst on us.State_ID=mst.id_mststate  where  us.State_ID  = ".$loginData->State_ID;
		$content['default_state'] = $this->db->query($sql)->result();*/

		/*check start mtc TC USER*/
			if ($loginData->id_mstfacility=='' || $loginData->id_mstfacility==0) {
			$id_mstfacility = 1;
		}else{
			$id_mstfacility = " f.id_mstfacility = ".$loginData->id_mstfacility;
		}

		 $sql = "select f.is_Mtc from tblusers as u inner join mstfacility f on u.id_mstfacility=f.id_mstfacility where ".$id_mstfacility." and u.id_tblusers=".$loginData->id_tblusers." ";
		$content['mtc_user'] = $this->db->query($sql)->result();
		//pr($content['mtc_user']);

		$sql = "select * from mststate";
		$content['states'] = $this->db->query($sql)->result();

		$sql = "select * from mstdistrict";
		$content['districts'] = $this->db->query($sql)->result();

		$sql = "select * from mstfacility";
		$content['facilities'] = $this->db->query($sql)->result();	

		$content['start_date'] = '2016-08-16';
		$content['end_date']   = date('Y-m-d');
		$content['facilities'] = $this->Dashboard_Model->facilities();
		
		$content['subview'] = 'dashboard';
		$this->load->view('pages/main_layout', $content);
	}

	public function casecade_export_csv($excel = NULL){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->cascade($excel);
			//print_r($data);exit();
		$filename = 'users_'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
		$file = fopen('php://output','w');
		$header = array("anti_hcv_screened","anti_hcv_positive","viral_load_tested","viral_load_detected","initiatied_on_treatment","treatment_completed","svr_done","treatment_successful","treatment_failure"); 
		fputcsv($file, $header);
		//foreach ($data as $key=>$line){ 
			fputcsv($file,$data); 
		//}
		fclose($file); 
		exit;
	}
	public function casecade_hav_export($excel = NULL){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->cascade_hav_patients($excel);
			//print_r($data);exit();
		$filename = 'Cascade_of_Care_for_HAV_Patients'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
		$file = fopen('php://output','w');
		$header = array("screened_for_hav","hav_positive_patients","patients_managed_at_facility","patients_referred_for_management"); 
		fputcsv($file, $header);
		//foreach ($data as $key=>$line){ 
			fputcsv($file,$data); 
		//}
		fclose($file); 
		exit;
	}
	public function casecade_hev_export($excel = NULL){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->cascade_hev_patients($excel);
			//print_r($data);exit();
		$filename = 'Cascade_of_Care_for_HEV_Patients'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
		$file = fopen('php://output','w');
		$header = array("screened_for_hev","hev_positive_patients","patients_managed_at_facility_hev","patients_referred_for_management_hev"); 
		fputcsv($file, $header);
		//foreach ($data as $key=>$line){ 
			fputcsv($file,$data); 
		//}
		fclose($file); 
		exit;
	}
	public function treatment_Initiations_by_District_HCV_export($excel = NULL){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->treatment_Initiations_by_District($excel);
			//print_r($data);exit();

			foreach ($data AS $value) {
			$hos_name[]=$value['hospital'];
			$countIni[]=$value['countIni'];
		}

			
		$filename = 'treatment_Initiations_by_District_HCV'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
		$file = fopen('php://output','w');
		fputcsv($file, $hos_name);
		fputcsv($file,$countIni); 
		fclose($file); 
		exit;
	}
public function treatment_Initiations_by_District_HAV_export($excel = NULL){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->treatment_Initiations_by_Districthav($excel);
			//print_r($data);exit();

			foreach ($data AS $value) {
			$hos_name[]=$value['hospital'];
			$countIni[]=$value['countIni'];
		}

			
		$filename = 'treatment_Initiations_by_District_HAV'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
		$file = fopen('php://output','w');
		fputcsv($file, $hos_name);
		fputcsv($file,$countIni); 
		fclose($file); 
		exit;
	}
	public function treatment_Initiations_by_District_HEV_export($excel = NULL){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->treatment_Initiations_by_Districthev($excel);
			//print_r($data);exit();

			foreach ($data AS $value) {
			$hos_name[]=$value['hospital'];
			$countIni[]=$value['countIni'];
		}

			
		$filename = 'treatment_Initiations_by_District_HEV'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
		$file = fopen('php://output','w');
		fputcsv($file, $hos_name);
		fputcsv($file,$countIni); 
		fclose($file); 
		exit;
	}
	public function cirrhotic_details_export($excel = NULL){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->cirrhotic_details($excel);
			//print_r($data);exit();
		$filename = 'Stage_of_disease_for_VL_Positive_Patients'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
		$file = fopen('php://output','w');
		$header = array("Non Cirrhotic","Compensated Cirrhotic","Decompensated Cirrhotic"); 
		fputcsv($file, $header);
		//foreach ($data as $key=>$line){ 
			fputcsv($file,$data); 
		//}
		fclose($file); 
		exit;
	}
	public function regimen_export($excel = NULL){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->regimen($excel);
			$data1=$this->Dashboard_Model->regimen24($excel);
			//print_r($data);exit();
			foreach ($data AS $value) {
			$COUNT[]=$value['COUNT'];
			
		}
		if($data1[0]->COUNT=='') {
			$COUNT[]=0;
		}
			 else{
			 $COUNT[]= $data1[0]->COUNT;
			}
		$filename = 'Regimen_Wise_Distribution'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
		$file = fopen('php://output','w');
		$header = array("Initiated on Treatment", "Reg 1:(SOF + DCV)", "Reg 2:(SOF + VEL)", "Reg 3:(SOF +VEL+ Ribavirin)","Reg 3:(SOF+VEL(24 weeks))"); 
		fputcsv($file, $header);
			fputcsv($file,$COUNT); 
		fclose($file); 
		exit;
	}
	public function cascade_SVR_Tested_export(){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->cascade($excel=NULL);
			//echo $data->treatment_successful;exit();
				$data1[]=$data->treatment_successful;
				$data1[]=$data->treatment_failure;
		$filename = 'Success_Ratio_of_Treatment(SVR Tested)'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
		$file = fopen('php://output','w');
		$header = array("treatment_successful","treatment_failure"); 

		fputcsv($file, $header);
			fputcsv($file,$data1); 
		fclose($file); 
		exit;
	}
	public function Success_Ratio_of_Treatment_export(){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->regimensucess();
			$data24 = $this->Dashboard_Model->regimensucess24();
			$dataf = $this->Dashboard_Model->regimenfailure();
			$dataf24 = $this->Dashboard_Model->regimenfailure24();
			$header = array("Initiated on Treatment","Reg 1:(SOF + DCV)", "Reg 2:(SOF + VEL)","Reg 3:(SOF +VEL+ Ribavirin)","Reg 3:(SOF+VEL(24 weeks))"); 
			//echo $data->treatment_successful;exit();
			
			$filename = 'Regimen-Wise_Success_Ratio_of_Treatment'.date('Ymd').'.csv'; 
			header("Content-Description: File Transfer"); 
			header("Content-Disposition: attachment; filename=$filename"); 
			header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
			$file = fopen('php://output','w');
		
			$header1 = array("Name","Treatment Success","Treatment Fail");

			fputcsv($file, $header1);
			$csvdata='';
			for($i=0;$i<4;$i++)
			{
				$category=isset($header[$i])?($header[$i]):'';
				$success=isset($data[$i])?($data[$i]->COUNT):'';
				$fail=isset($dataf[$i])?($dataf[$i]->COUNT):'';
				$line = array($category,$success,$fail);
				fputcsv($file, $line);
				unset($line);
			} 
			$category=isset($header[4])?($header[4]):'';
			$success=isset($data24[0])?($data24[0]->COUNT):'';
			$fail=isset($dataf24[0])?($dataf24[0]->COUNT):'';
			$line = array($category,$success,$fail);
			fputcsv($file, $line);
			
			fclose($file); 
			exit;
	}
	public function treatment_by_Districtsvrsuc_export($excel = Null){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->treatment_by_Districtsvrsuc($excel);
			$data1 = $this->Dashboard_Model->treatment_by_Districtsvrunsuc();
			//print_r($data);exit();
			$filename = 'treatment_by_Districtsvrsuc'.date('Ymd').'.csv'; 
			header("Content-Description: File Transfer"); 
			header("Content-Disposition: attachment; filename=$filename"); 
			header("Content-Type: application/csv; ");
		
		// file creation 
			$file = fopen('php://output','w');
			foreach ($data AS $value => $code) {
			$hos_name=$code->hospital;
			$countInisuc=$code->countIni;
			$countIniunsuc=$data1[$value]->countIni;
			$line = array($hos_name,$countInisuc,$countIniunsuc);
			fputcsv($file, $line);
			unset($line);
		}
		fclose($file); 
		exit;
	}
	public function trendviral_load_detected_export(){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->trendviral_load_detected();
			//print_r($data);exit();

			foreach ($data AS $value) {
			$monthyear[]=$value->MONTHNAME."-".$value->YEAR;
			$countIni[]=$value->COUNT;
		}

			
		$filename = 'trendviral_load_detected_export'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
		$file = fopen('php://output','w');
		fputcsv($file, $monthyear);
		fputcsv($file,$countIni); 
		fclose($file); 
		exit;
	}
	public function lossofFollowup_HCV_export($excel=NULL){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->no_of_patients_eligible_stage($excel);
			$dataltfu = $this->Dashboard_Model->no_of_patients_ltfu_stage();
			
			$filename = 'Loss_to_Follow_Up_Analysis'.date('Ymd').'.csv'; 
			header("Content-Description: File Transfer"); 
			header("Content-Disposition: attachment; filename=$filename"); 
			header("Content-Type: application/csv; ");
		
		// file creation 
			$file = fopen('php://output','w');
		
			$header1 = array("Name","Number of patients eligible for each stage","Number of patients LTFU for each Stage");

			fputcsv($file, $header1);
			$Viral_Load=array("Viral Load",$data['follow_viral_load'],$dataltfu->loss_of_follow_viral_load);
			$St1_dispensation=array("1St Dispensation",$data['follow_1St_dispensation'],$dataltfu->loss_of_follow_1St_dispensation);
			$St2_dispensation=array("2St Dispensation",$data['follow_2St_dispensation'],$dataltfu->loss_of_follow_2St_dispensation);
			$St3_dispensation=array("3St Dispensation",$data['follow_3St_dispensation'],$dataltfu->loss_of_follow_3St_dispensation);
			$St4_dispensation=array("4St Dispensation",$data['follow_4St_dispensation'],$dataltfu->loss_of_follow_4St_dispensation);
			$St5_dispensation=array("5St Dispensation",$data['follow_5St_dispensation'],$dataltfu->loss_of_follow_5St_dispensation);
			$St6_dispensation=array("6St Dispensation",$data['follow_6St_dispensation'],$dataltfu->loss_of_follow_6St_dispensation);
			$SVR=array("SVR",$data['follow_SVR'],$dataltfu->loss_of_follow_SVR);
			
				fputcsv($file, $Viral_Load);
				fputcsv($file, $St1_dispensation);
				fputcsv($file, $St2_dispensation);
				fputcsv($file, $St3_dispensation);
				fputcsv($file, $St4_dispensation);
				fputcsv($file, $St5_dispensation);
				fputcsv($file, $St6_dispensation);
				fputcsv($file, $SVR);
			fclose($file); 
			exit;
	}

/* NEW*/
	public function lossfollowacrossdist_export(){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->lossfollowacrossdist();
			//print_r($data);exit();

			foreach ($data AS $value) {
			$hos_name[]=$value->hospital;
			$countIni[]=$value->countIni;
		}

			
		$filename = 'Loss_to_Follow_Up_Analysis'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
		$file = fopen('php://output','w');
		fputcsv($file, $hos_name);
		fputcsv($file,$countIni); 
		fclose($file); 
		exit;
	}
	public function adherencepercentage_export(){ 
		
			$this->load->model('Dashboard_Model');
			$dataf = $this->Dashboard_Model->regimenfailure();
			$dataf24 = $this->Dashboard_Model->regimenfailure24();
			//print_r($dataf24);
		$regimenfailure=$dataf[0]->COUNT+$dataf[1]->COUNT+$dataf[2]->COUNT+$dataf24[0]->COUNT;
		
		$regimenfailure_arr=array($dataf[0]->COUNT,$dataf[1]->COUNT,$dataf[2]->COUNT,$dataf24[0]->COUNT);
		//print_r($regimenfailure_arr);
		foreach ($regimenfailure_arr AS $value=>$key) {
			if($regimenfailure>0)
			{
			$regimenfailure_per[]=round(((($regimenfailure_arr[$value])*100)/$regimenfailure),2)."%";
			}
			else
			{
				$regimenfailure_per[]="0"."%";
			}
		}

			$filename = 'adherence_percentage_export'.date('Ymd').'.csv'; 
			header("Content-Description: File Transfer"); 
			header("Content-Disposition: attachment; filename=$filename"); 
			header("Content-Type: application/csv; ");
		// file creation 
			$file = fopen('php://output','w');
		
			$header1 = array("Regimen 1(in %)",  "Regimen 2(in %)", "Regimen 3(in %)", "Regimen 4(in %)");
			fputcsv($file, $header1);
		fputcsv($file,$regimenfailure_per); 
			fclose($file); 
			exit;
	}
	public function lost_to_followup_special_case_export(){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->lost_to_followup_special_case();
			//print_r($data);
		$special_case=$data->loss_to_follow_patient_referred_to_mtc+$data->loss_to_follow_patient_reporting_at_mtc+$data->loss_to_follow_up;
		$special_case_arr=array($data->loss_to_follow_patient_referred_to_mtc,$data->loss_to_follow_patient_reporting_at_mtc,$data->loss_to_follow_up);
		foreach ($special_case_arr AS $value=>$key) {
			if($special_case>0)
			{
				$special_case_per[]=round(((($special_case_arr[$value])*100)/$special_case),2)."%";
			}
			else
			{
				$special_case_per[]="0"."%";
			}
		}
			$filename = 'Loss_to_FollowUp_for_Special_Cases'.date('Ymd').'.csv'; 
			header("Content-Description: File Transfer"); 
			header("Content-Disposition: attachment; filename=$filename"); 
			header("Content-Type: application/csv; ");
		// file creation 
			$file = fopen('php://output','w');
		
			$header1 = array("Patient referred to MTC(in %)",  "Patient reporting at MTC(in %)", "Loss to Follow up(in %)");
			fputcsv($file, $header1);
		fputcsv($file,$special_case_per); 
			fclose($file); 
			exit;
	}
	public function AdherenceDistricts_export(){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->lossfollowacrossdist();
			//print_r($data);exit();

			foreach ($data AS $value) {
			$hos_name[]=$value->hospital;
			$countIni[]=$value->countIni;
		}

			
		$filename = 'AdherenceDistricts_for_regimen1'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
		$file = fopen('php://output','w');
		fputcsv($file, $hos_name);
		fputcsv($file,$countIni); 
		fclose($file); 
		exit;
	}
	public function Time_Analysis_across_districts_export(){ 
		
			$this->load->model('Dashboard_Model');
			$data = $this->Dashboard_Model->lossfollowacrossdist();
			//print_r($data);exit();

			foreach ($data AS $value) {
			$hos_name[]=$value->hospital;
			$countIni[]=$value->countIni;
		}

			
		$filename = 'Time_Analysis_across_districts_export'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
		$file = fopen('php://output','w');
		fputcsv($file, $hos_name);
		fputcsv($file,$countIni); 
		fclose($file); 
		exit;
	}
	//static code need State TAT Median model code
	public function Turn_Around_Time_Analysis_export($excel=NULL){ 
		
			$this->load->model('Dashboard_Model');
			$datad = (array)$this->Dashboard_Model->actual_days_taken($excel);
			$datat=array("10","1","2","5","8","2","5","2");
			//$data24 = $this->Dashboard_Model->regimensucess24();
			foreach ($datad as $value=>$key) 
    		$data[] =$datad[$value];

		$header = array("Anty HCV \nTest to \nAnti HCV Result",
		"Anti HCV \nResult to \nViral Load Test",
		"VL Test to \nDelivery of VL \nResult to Patient",
		"Delivery of \nResult to Patient\n to Prescription of\n Baseline test",
		"Prescription of \nBaseline testing to\n Date of Baseline Tests",
		"Date of Baseline \nTests to Result of\n Baseline Tests",
		"Result of Baseline\n Tests to Initiation\n of Treatment",
		"Treatment Completion \nto SVR"); 
			//echo $data->treatment_successful;exit();
			
			$filename = 'Turn_Around_Time_Analysis_export'.date('Ymd').'.csv'; 
			header("Content-Description: File Transfer"); 
			header("Content-Disposition: attachment; filename=$filename"); 
			header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
			$file = fopen('php://output','w');
		
			$header1 = array("Name","Actual Days Taken","State TAT Median");

			fputcsv($file, $header1);
			$csvdata='';
			foreach ($header as $i => $value) {
				$category=$header[$i];
				$Days=$data[$i];
				$TAT_Median=$datat[$i];
				$line = array($category,$Days,$TAT_Medianz);
				fputcsv($file, $line);
				unset($line);
			}
			
			fclose($file); 
			exit;
	}
	public function Age_wise_distribution_VL_Detected_Patients(){ 
		
			$this->load->model('Dashboard_Model');
			$data = (array)$this->Dashboard_Model->age_wise_vl_detected();
			//print_r($data);

			$filename = 'Age_wise_distribution_VL_Detected_Patients'.date('Ymd').'.csv'; 
			header("Content-Description: File Transfer"); 
			header("Content-Disposition: attachment; filename=$filename"); 
			header("Content-Type: application/csv; ");
		// file creation 
			$file = fopen('php://output','w');
		
			$header1 = array("<10 Years", "10-20 Years", "20-30 Years", "30-40 Years", "40-50 Years", "50-60 Years", ">=60 Years");
			fputcsv($file, $header1);
		fputcsv($file,$data); 
			fclose($file); 
			exit;
	}
	public function Gender_wise_Risk_Factor_Analysis(){ 
		
			$this->load->model('Dashboard_Model');
			$data = (array)$this->Dashboard_Model->risk_factor_analysis();
			//print_r($data);

			$filename = 'Gender_wise_Risk_Factor_Analysis'.date('Ymd').'.csv'; 
			header("Content-Description: File Transfer"); 
			header("Content-Disposition: attachment; filename=$filename"); 
			header("Content-Type: application/csv; ");
		// file creation 
			$file = fopen('php://output','w');
		
			$header1 = array("Male", "Femail", "Transgender");
			fputcsv($file, $header1);
		fputcsv($file,$data); 
			fclose($file); 
			exit;
	}
	public function Age_wise_Risk_Factor_Analysis(){ 
		
			$this->load->model('Dashboard_Model');
			$data =(array)$this->Dashboard_Model->age();
			print_r($data);

			$filename = 'Age_wise_Risk_Factor_Analysis'.date('Ymd').'.csv'; 
			header("Content-Description: File Transfer"); 
			header("Content-Disposition: attachment; filename=$filename"); 
			header("Content-Type: application/csv; ");
		// file creation 
			$file = fopen('php://output','w');
		
			$header1 = array("<10 Years", "10-20 Years", "20-30 Years", "30-40 Years", "40-50 Years", "50-60 Years", ">=60 Years");
			fputcsv($file, $header1);
			fputcsv($file,$data); 
			fclose($file); 
			exit;
	}
public function Risk_Factor_Analysis(){ 
		
		$sql = "SELECT SUM(LENGTH(p.SideEffectValue)) as count,m.LookupValue,p.SideEffectValue,m.LookupCode FROM mstlookup m left join tblpatient p  ON find_in_set(m.LookupCode,p.SideEffectValue) where m.flag = 3 and m.LanguageID = 1 group by m.LookupCode order by m.Sequence ASC";
		$side_effects = $this->db->query($sql)->result();
		foreach ($side_effects as  $value){ 
			$risk_factor[]=$value->LookupValue;
			if($value->count==''){
				$countIni[]=0;
			}
			else
			{
				$countIni[]=$value->count;
			}
 			
 		}
 		$filename = 'Risk_Factor_Analysis'.date('Ymd').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
		// file creation 
		$file = fopen('php://output','w');
		fputcsv($file, $risk_factor);
		fputcsv($file,$countIni); 
		fclose($file); 
		exit;	
	}
	public function TreatmentInitiations_export(){ 
		
			$this->load->model('Dashboard_Model');
			$datam=array("5","3","1");
			$dataf=array("4","2","0");
			$datat=array("3","1","0");
		$header = array("Anti HCV Screened",
		"Viral Load Test",
		"Treatment Initiations"); 
			//echo $data->treatment_successful;exit();
			
			$filename = 'TreatmentInitiations_export'.date('Ymd').'.csv'; 
			header("Content-Description: File Transfer"); 
			header("Content-Disposition: attachment; filename=$filename"); 
			header("Content-Type: application/csv; ");
	   // get data 
		
		// file creation 
			$file = fopen('php://output','w');
		
			$header1 = array("Name","Male", "Femail", "Transgender");

			fputcsv($file, $header1);
			$csvdata='';
			foreach ($header as $i => $value) {
				$category=$header[$i];
				$male=$datam[$i];
				$female=$dataf[$i];
				$Transgender=$datat[$i];
				$line = array($category,$male,$female,$Transgender);
				fputcsv($file, $line);
				unset($line);
			}
			
			fclose($file); 
			exit;
	}
	public function spCasesPrevalence_export(){ 
		
			$this->load->model('Dashboard_Model');
			$data = array("2",
			"6",
			"",
		"8",
		"12",
		"9",
		"6");
			//print_r($data);

			$filename = 'spCasesPrevalence_export'.date('Ymd').'.csv'; 
			header("Content-Description: File Transfer"); 
			header("Content-Disposition: attachment; filename=$filename"); 
			header("Content-Type: application/csv; ");
		// file creation 
			$file = fopen('php://output','w');
		
			$header1 = array("Treatment Experienced Patients",
		"People Who Inject Drugs",
		"Persons with Chronic Kidney Disease",
		"Persons with HIV/HCV Co-infection",
		"Persons with HBV/HCV Co-infection",
		"Persons with TB/HCV Co-infection",
		"Pregnant Women");
			fputcsv($file, $header1);
		fputcsv($file,$data); 
			fclose($file); 
			exit;
	}


}