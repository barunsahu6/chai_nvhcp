<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Linelist extends CI_Controller
{
    private $sess_where = '1';
    private $sess_mstdistrict ='1';
    private $sess_mstfacility ='1';
    private $filter = '1';
    private $filter1 = '1';
    public function __construct()
    {        
      parent::__construct(); 
      $this->load->model('Common_Model');
      $this->load->model('Patient_Model');
      $this->load->helper('common');
      ini_set('memory_limit', '1024M');
      ini_set('memory_limit', '-1');
      $loginData = $this->session->userdata('loginData');
      $sessiontemp =   $this->session->userdata('templatename'); 
    $tempval = $this->input->post('temp');

      $filters1data =   $this->session->userdata('filters1'); 
      //pr($filters1data);
      if($sessiontemp==''){
        $sessivaldata = 1;
      }else{
        $sessivaldata =   $sessiontemp;
      }

    if($filters1data['startdate']==''){
      $startdate = date('Y-07-01');  
  }else{
    $startdate =$filters1data['startdate'];
  }
  if($filters1data['enddate']==''){

      $enddate = date('Y-m-d');
  }else{
    $enddate = $filters1data['enddate'];
  }

        //Login filter query
      if( ($loginData) && $loginData->user_type == '1' ){
          $sess_where = '1';
          $sess_mstdistrict ='1';
          $sess_mstfacility ='1';

          $this->filter = " p.CreatedOn between '".($startdate)."' and '".($enddate)."'";
          $this->filter1 = " p1.CreatedOn between '".($startdate)."' and '".($enddate)."'";

          $this->datefilter = "AND p.CreatedOn between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."' ";
      }
      elseif( ($loginData) && $loginData->user_type == '2' ){

          $this->sess_where = " id_mststate = '".$loginData->State_ID."'";

          $this->sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

          $this->sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

          $this->filter = " p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."' AND p.id_mstfacility='".$loginData->id_mstfacility."' ";
          $this->filter1 = " p1.Session_StateID = '".$loginData->State_ID."' AND p1.Session_DistrictID ='".$loginData->DistrictID."' AND p1.id_mstfacility='".$loginData->id_mstfacility."' ";
           $this->datefilter = "AND p.CreatedOn between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."' ";
      }
      elseif( ($loginData) && $loginData->user_type == '3' ){ 
          $this->sess_where = " id_mststate = '".$loginData->State_ID."'";
          $this->sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
          $this->sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";

          $this->filter = " p.Session_StateID = '".$loginData->State_ID."'";
          $this->filter1 = " p1.Session_StateID = '".$loginData->State_ID."'";
           $this->datefilter = "AND p.CreatedOn between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."' ";
      }
      elseif( ($loginData) && $loginData->user_type == '4' ){ 
          $this->sess_where       = " id_mststate = '".$loginData->State_ID."'";
          $this->sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
          $this->sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";

          $this->filter = " AND p.Session_StateID = '".$loginData->State_ID."' AND p.Session_DistrictID ='".$loginData->DistrictID."'";
          $this->filter1 = " AND p1.Session_StateID = '".$loginData->State_ID."' AND p1.Session_DistrictID ='".$loginData->DistrictID."'";
           $this->datefilter = "AND p.CreatedOn between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."' ";
      }
      if($loginData->user_type == 1){
        $this->session->set_flashdata('er_msg','You are not logged-in, please login again to continue');
        redirect('login');
    }
    if ($this->session->userdata('loginData') == null) {
     redirect('login');
 }
//echo '//'.$sessiontemp.'||'. $tempval.' and'. $sessiontemp1;exit();
 if ($sessivaldata==1)
 {
  //echo 'ddddd';exit();
     $this->header = array( 
        'UID_NUM',
        'OPD/IPD ',     
        'NVHCP ID',                 
        'Patient Type',
        'Name',
        'Age between 0 and 1 Year',
        'Age (in years)',
        'Gender',
        'Relation',
        'Relation Name',  
        'Home & Street Address',                    
        'State',
        'District',
        'Block/Ward',
        'Other',
        'Village/Town/City',            
        'Pincode',
        'Mobile No.',
        'Consent for Receiving Communication',
        'Risk Factors',
        'Other',
        'lgM Anti HAV',
        'HAV Rapid Diagnostic Test',
        'HAV Date',
        'HAV Result',
        'HAV Place',
        'HAV Lab Name',
        'HAV Lab Name Other',
        'HAV ELISA Test',
        'HAV ELISA Date',
        'HAV ELISA Result',
        'HAV ELISA Place',
        'HAV ELISA Lab Name',
        'HAV ELISA Lab Name Other',
        'HAV Other Test',
        'HAV Test Name',
        'HAV Other Date',
        'HAV Other Result',
        'HAV Other Place',
        'HAV Other Lab Name',
        'HAV Others Lab Name ',
        'HAV Patient managed at the facility',
        'HAV Patient referred for management to higher facility',
        'HbsAg',
        'HBSRapid',
        'HBSRapidDate',
        'HBSRapidResult',
        'HBSRapidPlace',
        'HBSRapidLab Name',
        'HBSRapidOther Lab Name',
        'HBSElisa',
        'HBSElisaDate',
        'HBSElisaResult',
        'HBSElisaPlace',
        'HBSELISALab Name',
        'HBSELISAOther Lab Name',
        'HBSOther',
        'HBSOtherName',
        'HBSOtherDate',
        'HBSOtherResult',
        'HBSOtherPlace',
        'HBSLab Name',
        'HBSOtherLab Name',
        'AntiHCV',
        'HCVRapid',
        'HCVRapidDate',
        'HCVRapidResult',
        'HCVRapidPlace',
        'HCVRapidLab Name',
        'HCVRapidOther Lab Name',
        'HCVElisa',
        'HCVElisaDate',
        'HCVElisaResult',
        'HCVElisaPlace',
        'HCVElisaLab Name',
        'HCVElisaOther Lab Name',
        'HCVOther',
        'HCVOtherName',
        'HCVOtherDate',
        'HCVOtherResult',
        'HCVOtherPlace',
        'HCVLab Name',
        'HCVOther Lab Name',
        'LgmAntiHEV',
        'HEVRapid',
        'HEVRapidDate',
        'HEVRapidResult',
        'HEVRapidPlace',
        'HEVRapid Lab Name',
        'HEVRapid Othet Lab Name',
        'HEVElisa',
        'HEVElisaDate',
        'HEVElisaResult',
        'HEVElisaPlace',
        'HEVElisa Lab Name',
        'HEVElisa Othet Lab name',
        'HEVOther',
        'HEVOtherName',
        'HEVOtherDate',
        'HEVOtherResult',
        'HEVOtherPlace',
        'HEVLab Name',
        'HEV Other Lab Name',
        'HEV Patient managed at the facility',
        'HEV Patient referred for management to higher facility',
        'VL HepC',
        'VL SampleCollectionDate',
        'VL Is Sample Stored',
        'VL Sample Storage Temperature (°C)',
        'VL Sample Storage Duration',
        'VL Duration (in hours)',
        'VL IS Sample Transported',
        'VL Sample Transport Temperature (°C)',
        'VL Sample Transport Date',
        'VL Sample Transported To',
        'VL Other',
        'VL Transporter Name',
        'VL Transporter Designation',
        'VL Remarks',
        'VLD Sample Receipt Date',
        'VLD Sample Received by Name',
        'VLD Designation',
        'VLD Is Sample Accepted',
        'VLD Test Result Date',
        'VLD Result',
        'VLD Viral Load',
        'VLD Reason For Rejection',
        'Remarks',
        'VLB HepB',
        'VLB SampleCollectionDate',
        'VLB Is Sample Stored',
        'VLB Sample Storage Temperature (°C)',
        'VLB Sample Storage Duration',
        'VLB Duration (in hours)',
        'VLB IS Sample Transported',
        'VLB Sample Transport Temperature (°C)',
        'VLB Sample Transport Date',
        'VLB Sample Transported To',
        'VLB Other',
        'VLB Transporter Name',
        'VLB Transporter Designation',
        'VLB Remarks',
        'VLDB Sample Receipt Date',
        'VLDB Sample Received by Name',
        'VLDB Designation',
        'VLDB Is Sample Accepted',
        'VLDB Test Result Date',
        'VLDB Result',
        'VLDB Viral Load',
        'VLDB Reason For Rejection',
        'VLDB Remarks',
        'Date of Prescribing Tests',
        'Date of issue of last investigation report',
        'Haemoglobin',
        'S. Albumin',
        'Serum Bilirubin Total(mg/dL)',
        'PT INR',
        'ALT',
        'AST',
        'AST ULN (Upper Limit of Normal)',
        'Platelet Count',
        'Weight (in Kgs)',
        'S. Creatinine (in mg/dL)',
        'eGFR (estimated glomerular filtration rate)',
        'Ultrasound',
        'Ultrasound Date',
        'Fibroscan',
        'Fibroscan Date',
        'LSM value (in Kpa)',
        'APRI',
        'APRI Score',
        'FIB 4',
        'FIB 4 Score',
        'Complicated/Uncomplicated',
        'Variceal Bleed',
        'Ascites',
        'Encephalopathy',
        'Child Pugh Score',
        'Severity of HEP-C',
        'Risk Factor',
        'HIV/ART Regimen',
        'Renal/CKD Stage',
        'Reason For Prescribing Ribavarin',
        'Last Menstrual Period',
        'Pregnant',
        'Expected Date of Delivery',
        'Referred',
        'Referring Doctor',
        'Referring Doctor Other',
        'Referred To',
        'Date',
        'Observations',
        'Prescribing Doctor',
        'Prescribing Doctor Other',
        'Regimen Prescribed',
        'Sofosbuvir',
        'Daclatasvir',
        'Velpatasvir',
        'Ribavrin',
        'Total Regimen Duration(Weeks)',
        'Other Duration (Weeks)',
        'Reason',
        'Prescribing Date',
        'Place Of Dispensation',
        '1st Rx Date Of Treatment Initiation',
        '1st Rx Days of Pills Dispensed',
        '1st Rx Advised Next Visit Date',
        '1st Rx Comments',
        '2nd Rx Visit Date',
        '2nd Rx Haemoglobin',
        '2nd Rx Platelet Count',
        '2nd Rx Days of Pills Left in the bottle',
        '2nd Rx Adherence(%)',
        '2nd Rx Reason for Low Adherence',
        '2nd Rx Low Adherence Reason Other',
        '2nd Rx Advised Next Visit Date',
        '2nd Rx Doctor',
        '2nd Rx Doctor Other',
        '2nd Rx Comments',
        '3rd Rx Visit Date',
        '3rd Rx Haemoglobin',
        '3rd Rx Platelet Count',
        '3rd Rx Days of Pills Left in the bottle',
        '3rd Rx Adherence(%)',
        '3rd Rx Reason for Low Adherence',
        '3rd Rx Low Adherence Reason Other',
        '3rd Rx Advised Next Visit Date',
        '3rd Rx Doctor',
        '3rd Rx Doctor Other',
        '3rd Rx Other',
        '4th Rx Visit Date',
        '4th Rx Haemoglobin',
        '4th Rx Platelet Count',
        '4th Rx Days of Pills Left in the bottle',
        '4th Rx Adherence(%)',
        '4th Rx Reason for Low Adherence',
        '4th Rx Low Adherence Reason Other',
        '4th Rx Advised Next Visit Date',
        '4th Rx Doctor',
        '4th Rx Doctor Other',
        '4th Rx Other',
        '5th Rx Visit Date',
        '5th Rx Haemoglobin',
        '5th Rx Platelet Count',
        '5th Rx Days of Pills Left in the bottle',
        '5th Rx Adherence(%)',
        '5th Rx Reason for Low Adherence',
        '5th Rx Low Adherence Reason Other',
        '5th Rx Advised Next Visit Date',
        '5th Rx Doctor',
        '5th Rx Doctor Other',
        '5th Rx Other',
        '6th Rx Visit Date',
        '6th Rx Haemoglobin',
        '6th Rx Platelet Count',
        '6th Rx Days of Pills Left in the bottle',
        '6th Rx Adherence(%)',
        '6th Rx Reason for Low Adherence',
        '6th Rx Low Adherence Reason Other',
        '6th Rx Advised Next Visit Date',
        '6th Rx Doctor',
        '6th Rx Doctor Other',
        '6th Rx Other',
        'EoT Visit Date',
        'EoT Days of Pills Left',
        'EoT Adherence(%)',
        'EoT Reason for Low Adherence',
        'EoT Low Adherence Reason Other',
        'EoT Advised SVR Date',
        'EoT Doctor',
        'EoT Doctor Other',
        'EoT Comments',
        'HEP-C Sample Drawn On Date',
        'HEP-C Is Sample Stored',
        'HEP-C Sample Storage Temperature (°C)',
        'HEP-C Is Sample Transported',
        'HEP-C Sample Transport Temperature(°C)',
        'HEP-C Sample Transport Date',
        'HEP-C Sample Transported To',
        'HEP-C Sample Transported By : Name',
        'HEP-C Designation',
        'HEP-C Remarks',
        'SVR Sample Receipt Date',
        'SVR Sample Received By : Name',
        'SVR Designation',
        'SVR Is Sample Accepted',
        'SVR Test Result Date',
        'SVR Result',
        'SVR Viral Load Count',
        'SVR Reason for Rejection',
        'SVR Others specify',
        'SVR Doctor',
        'SVR Doctor Other',
        'SVR Comments',
        'Interruption Status'
    );
}else 
{

    $query = "SELECT ct.label_name FROM linelist_template tmp 
    LEFT JOIN linelist_category ct ON tmp.categoryId=ct.lcategoryId 
    WHERE tmp.masterId = ".$sessiontemp."";

    $db_nameval= '';
    $resdata = $this->db->query($query)->result();  
    
    foreach($resdata as $row){
      $db_nameval[] =  $row->label_name; 


  }
  $this->header = $db_nameval;
}

$this->folder_path = FCPATH . "application" . DIRECTORY_SEPARATOR . "linelists" . DIRECTORY_SEPARATOR;

$this->load->helper('file');
delete_files($this->folder_path);
}

public function index()
{
    //echo 'sdfdsfsd';exit();
     // die($this->filter);
   $loginData = $this->session->userdata('loginData');
        //echo "<pre>";print_r($loginData);

   //pr($_POST);

            if( ($loginData) && $loginData->user_type == '1' ){
                $sess_where = " 1";
                $sess_wherep = " 1";
            }
        elseif( ($loginData) && $loginData->user_type == '2' ){

                $sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND p.id_mstfacility='".$loginData->id_mstfacility."'";
                 $sess_wherep = " p1.Session_StateID = '".$loginData->State_ID."' AND p1.id_mstfacility='".$loginData->id_mstfacility."'";
            }
        elseif( ($loginData) && $loginData->user_type == '3' ){ 
                $sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
                $sess_wherep = " p1.Session_StateID = '".$loginData->State_ID."'";
            }
        elseif( ($loginData) && $loginData->user_type == '4' ){ 
                $sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
                $sess_wherep = "and  p1.Session_StateID = '".$loginData->State_ID."' ";
            }
      $sql= "select min(CreatedOn) as startdate from linelist p where ".$sess_where." and CreatedOn is not null and CreatedOn!='0000-00-00'";  
     $content['startdateval'] = $this->db->query($sql)->result();  
     $content['startdateval'][0]->startdate;  
    $REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
    if($REQUEST_METHOD == 'POST')
    {
        $id_search_state = $this->input->post('search_state');
        $id_input_district = $this->input->post('input_district');
        $id_mstfacility = $this->input->post('facility');
        $startdate = $this->input->post('startdate');
        $enddate = $this->input->post('enddate');
        $tempname = $this->input->post('temp');	

            $filters1['id_search_state'] = $this->input->post('search_state') ;
            $filters1['id_input_district'] = $this->input->post('input_district');
            $filters1['id_mstfacility'] = $this->input->post('facility');  
            $filters1['startdate']      = ($this->input->post('startdate'));   
            $filters1['enddate']        = ($this->input->post('enddate'));  



if($tempname!=''){
        $PostFilter = "p.Session_StateID = '".$id_search_state."'  AND p.id_mstfacility = '".$id_mstfacility."' AND p.CreatedOn between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."' AND '".($tempname)."'  ";
    }else{
        $PostFilter = "p.Session_StateID = '".$id_search_state."'  AND p.id_mstfacility = '".$id_mstfacility."' AND p.CreatedOn between '".timeStamp($startdate)."' AND '".timeStamp($enddate)."'   ";
    }
     $filter1 = " and p1.CreatedOn between '".timeStamp($startdate)."' and '".timeStamp($enddate)."'"; 

        $this->session->set_userdata('templatename',$tempname);
    }
    else
    {	
            $filters1['id_search_state'] = 0 ;
            $filters1['id_input_district'] = 0;
            $filters1['id_mstfacility'] = 0;	
            $filters1['startdate']      = $content['startdateval'][0]->startdate;   
            $filters1['enddate']        = date('d-m-Y');  
              $startdate = $content['startdateval'][0]->startdate;	
              $enddate = date('Y-m-d');
              $tempname = '';

      $filter = " p.CreatedOn between '".($startdate)."' and '".($enddate)."'";			
      $filter1 = " and p1.CreatedOn between '".($startdate)."' and '".($enddate)."'"; 

       $PostFilter = " p.CreatedOn between '".($startdate)."' AND '".($enddate)."'   ";

       $this->session->unset_userdata('templatename');
      // print_r($filters1); die;        
  }

   $this->session->set_userdata('filters1', $filters1);  

  /*$content['start_date'] = date('Y-m-d');
  $content['end_date']   = date('Y-m-d');*/

    $sql = "select 
          * 
  from 
  ( 
  SELECT 
  f.id_mstfacility, 
  f.facility_short_name AS hospital, 
  COUNT(PatientGUID) AS 'patients' 
  FROM linelist p 
  INNER JOIN mstfacility f 
  ON p.id_mstfacility = f.id_mstfacility 
  WHERE  ".$sess_where." and ".(isset($PostFilter)?$PostFilter:$this->filter)." 
  GROUP BY f.facility_short_name 
  UNION 
  SELECT 
  0, 'All' AS hospital, 
  COUNT(PatientGUID) AS 'patients' 
  FROM linelist p1 where ".$sess_wherep."   ".$filter1." 
) a ORDER BY a.id_mstfacility";


$res = $this->Common_Model->query_data($sql);
//pr($res);

$sql = "select * from mststate where ".$this->sess_where."";
$content['states'] = $this->db->query($sql)->result();

$sql = "select * from mstdistrict where ".$this->sess_mstdistrict."";
$content['districts'] = $this->db->query($sql)->result();

$sql = "select * from mstfacility where  ".$this->sess_mstfacility."";
$content['facilities'] = $this->db->query($sql)->result();	

$sql = "select * from linelist_master where userId = ".$loginData->id_tblusers."";
$content['templist'] = $this->db->query($sql)->result();


$content['facilities'] = $res;
$content['subview'] = 'linelist_hospital';
$this->load->view('admin/main_layout', $content);
}

public function getlist($id_mstfacility = null)
{
	ini_set('memory_limit', '-1');
	if ($id_mstfacility > 0) {

		$sql_facility_name = "select facilitycode from mstfacility where id_mstfacility = " . $id_mstfacility;
		$facility_name = $this->Common_Model->query_data($sql_facility_name);
		$facility = $facility_name[0]->facilitycode;
		$facility_where = "where id_mstfacility = " . $id_mstfacility;
	} else {
		$facility_where = "";
		$facility = "ALL";
	}

        // $sql_total_recs = "select count(*) as count from tblpatient where id_mstfacility = ".$id_mstfacility;
        // $res = $this->Common_Model->query_data($sql_total_recs);
        // $total_recs = $res[0]->count;

	$filename = $facility . '_' . date('Y-m-d') . ".csv";
	header("Content-Disposition: attachment; filename=\"$filename\"");
	header("Content-Type: text/csv");

	$out = fopen("php://output", 'w');
	$flag = false;

	$sql_line_list = "select 
	UID,TreatingHospital,Name,Age,Gender,RiskFactors,FathersHusbandsName,Address,Villagetown,Area,Block,District,Pincode,ContactNo,SampleCollectionDateforAntiHCVTest,AntiHCVTestDate,ResultReceivedDatebyPatient,RESULTOFANTIHCVTESTINGELISA,SampleCollectionDateforVL,SampleCollectioncenternameforVL,ViralLoadTestDate,BaselineViralLoad,ViralLoadStatus,DateofArrivaltoHospital,MedicalSpecialist,CirrhosisNoCirrhosisSTatus,ClinicalUSGtestdate,ClinicalUSGresult,Fibroscantestdate,LSMValueinkPa,Fibroscanresult,APRItestdate,AST,ASTNormal,APRIScore,FIB4testdate,ALT,FIB4Score,Albumin,Bilurubin,INR,Baseline1_val,Visit2_1_val,Visit3_1_val,Visit4_1_val,Visit5_1_val,Visit6_1_val,Visit7_1,Baseline2_val,Visit2_2_val,Visit3_2_val,Visit4_2_val,Visit5_2_val,Visit6_2_val,Visit7_2_val,DecompensatedCirrhosisResult,DecompensatedCirrhosisTestdate,Encephalopathy,Ascites,VaricealBleed,ChildScore,Genotype,GenotypeTestDate,Treatmentinitiationdate,Regimen,Duration,PillsDispensed,AdvisedNextVisitDate,Visit2_2,PillsLeft_2,AdvisedNextVisitDate_2,Adherence_2,Visit3_3,PillsLeft_3,AdvisedNextVisitDate_3,Adherence_3,Visit4_4,PillsLeft_4,AdvisedNextVisitDate_4,Adherence_4,Visit5_5,PillsLeft_5,AdvisedNextVisitDate_5,Adherence_5,Visit6_6,PillsLeft_6,AdvisedNextVisitDate_6,Adherence_6,Visit7_7,PillsLeft_7,AdvisedNextVisitDate_7,Adherence_7,ETRTestDate,ETRViralLoad,ETRTestResult,SVRTestDate,SVRViralLoad,SVRTestResult,DrugSideEffect,DrugCompliance,PatientStatus,TreatmentStatus,ReasonforTreatmentFailure,TreatmentCardType,Remarks,Durationoftrt_from_private,
	category,jailno,UploadedOn,lal_svr_date,lal_svr_result,lal_viral_load,
	AltContact,history_of_hcv,uid_serial_number,previous_treating_hospital,previous_treatment_outcome,occupation,occupation_other,hiv,ckd,diabetes,hypertension,hbv,call_output,cumulative_svr,
	cumulative_svr_viral_load,cumulative_svr_outcome
	from linelist " . $facility_where;

        // echo $sql_line_list; die();
        // ini_set('display_errors', 1);

	$content['line_list'] = $this->Common_Model->query_data($sql_line_list);

	if (count($content['line_list']) > 0) {
		foreach ($content['line_list'] as $row) {
                // print_r($row); die();
			if (!$flag) {
                    // display field/column names as first row
                    // $firstline = array_map(array($this,'map_colnames'), array_keys((array)$row));
				fputcsv($out, $this->header, ',', '"');
				$flag = true;
			}
			foreach ((array)$row as $value) {
				if ($value == null) {
					$value = '';
				}
			}
			fputcsv($out, array_values((array)$row), ',', '"');
		}
	}
        // }
	fclose($out);
	exit();
}

public function download_list_zipped($id_mstfacility = 0)
{
	ini_set('memory_limit', '-1');

	if ($id_mstfacility > 0) {

		$query = "select facilitycode from mstfacility where id_mstfacility = " . $id_mstfacility;
		$result = $this->db->query($query)->result();
		$filename = $result[0]->facilitycode . "-" . date('Y-m-d-His');
		$facility_where = " ".$this->filter." and p.id_mstfacility = " . $id_mstfacility;

	} else {
		$facility_where = " ".$this->filter." ";
		$filename = "ALL-" . date('Y-m-d-His');
	}
    //die($facility_where);
	   $query = "select count(*) as count from linelist p WHERE " . $facility_where .' ' .$this->datefilter;
	$count_res = $this->db->query($query)->result();
	$count = $count_res[0]->count;
	$offset = 0;
	$limit = 2000;
	$file_path = $this->folder_path . $filename;

	while ($offset < $count) {
		if ($offset == 0) {

			$file = fopen($file_path . '.csv', "w");            
			if (!$file) {
				die('cant open file handler');
			}
			fputcsv($file, $this->header);
		} else {
			$file = fopen($file_path . ".csv", "a");
		}

		$result = $this->get_line_list_result($facility_where, $limit, $offset);  

        foreach ($result as $row) {
            fputcsv($file, (array)$row);
        }

        fclose($file);
        $offset += $limit;
    }

    $this->load->library('zip');

    if (!$this->zip->read_file($file_path . '.csv')) {
      echo "cant read file";
      die();
  } else {
      header('Content-Encoding: UTF-8');
      $this->zip->download($filename . '.zip');
  }
}

public function get_line_list_result($filter_facility = "", $limit = 5000, $offset = 0)
{
  $sessiontemp =   $this->session->userdata('templatename'); 
  ini_set('memory_limit', '-1');

  if ($sessiontemp == '')
  {

     $query = "SELECT
    LPAD(p.UID_Num,6,'0') as uid_num,
    p.OPD_Id,
    concat(p.UID_Prefix, '-', lpad(p.UID_Num, 6, '0')),
    case p.PatientType when 1 then 'New' when 2 then 'Experienced' end as PatientType,  
    p.FirstName,
    case p.IsAgeMonths when 1 THEN 'Yes' WHEN 2 THEN 'No' end as  IsAgeMonths,
    p.age, 
    gender_look, 
    case p.Relation WHEN 1 THEN 'Father' WHEN 2 THEN 'Husband' WHEN 3 THEN 'Guardian' WHEN 4 THEN 'Mother' END As Relation,
    p.FatherHusband,
    p.Add1,
    p.StateName,
    p.DistrictName,
    p.BlockName,
    p.BlockOther,
    p.VillageTown,
    p.PIN,
    p.Mobile,
    case p.IsSMSConsent WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' end as IsSMSConsent,
    msrisk_lookup,
    p.OtherRisk,
    case p.LgmAntiHAV WHEN 1 THEN 'Anti Hav' end as LgmAntiHAV,
    case p.HAVRapid WHEN 1 THEN 'Hav Rapid' end as HAVRapid,
    case p.HAVRapidDate when '0000-00-00' then '' else p.HAVRapidDate end as HAVRapidDate,
    CASE p.HAVRapidResult WHEN 1 THEN 'Positive' WHEN 2 THEN 'Negative' END AS HAVRapidResult, 
    case p.HAVRapidPlace when 1 THEN 'Govt. Lab' WHEN 2 then 'Private Lab-PPP' end as HAVRapidPlace,
    hav_laba,
    p.HAVRapidLabOther,
    case p.HAVElisa WHEN 1 then 'Hav Elisa' end as HAVElisa,
    case p.HAVElisaDate  when '0000-00-00' then '' else p.HAVElisaDate end as HAVElisaDate,
    CASE p.HAVElisaResult WHEN 1 THEN 'Positive' WHEN 2 THEN 'Negative' END AS HAVElisaResult,          
    case p.HAVElisaPlace when 1 THEN 'Govt. Lab' WHEN 2 then 'Private Lab-PPP' end as HAVElisaPlace,
    hav_elisa_lab,
    p.HAVElisaLabOther,
    case p.HAVOther WHEN 1 then 'Hav Other' end as HAVOther,
    p.HAVOtherName,
    case p.HAVOtherDate when '0000-00-00' then '' else p.HAVOtherDate end as HAVOtherDate,
    CASE p.HAVOtherResult WHEN 1 THEN 'Positive' WHEN 2 THEN 'Negative' END AS HAVOtherResult,  
    case p.HAVOtherPlace when 1 THEN 'Govt. Lab' WHEN 2 then 'Private Lab-PPP' end as HAVOtherPlace,
    hav_other_lab,
    p.HAVLabOther,
    case p.Refer_FacilityHAV WHEN 1 THEN 'Hav Facility' end as Refer_FacilityHAV,
    case p.Refer_HigherFacilityHAV WHEN 1 THEN 'Higher Hav Facility' end as Refer_HigherFacilityHAV,
    case p.HbsAg WHEN 1 THEN 'HbsAg' end as HbsAg,
    case p.HBSRapid WHEN 1 then 'HBSRapid' end as HBSRapid,
    case p.HBSRapidDate when '0000-00-00' then '' else p.HBSRapidDate end as  HBSRapidDate,
    case p.HBSRapidResult WHEN 1 THEN 'Positive' WHEN 2 THEN 'Negative' end as HBSRapidResult,
    case p.HBSRapidPlace when 1 THEN 'Govt. Lab' WHEN 2 then 'Private Lab-PPP' end as HBSRapidPlace,
    hbs_labd,
    p.HBSRapidLabOther,
    case p.HBSElisa when 1 THEN 'HBSElisa' end as HBSElisa,
    case p.HBSElisaDate when '0000-00-00' then '' else p.HBSElisaDate end as HBSElisaDate,
    case p.HBSElisaResult WHEN 1 THEN 'Positive' WHEN 2 THEN 'Negative' end as HBSElisaResult,
    case p.HBSElisaPlace when 1 THEN 'Govt. Lab' WHEN 2 then 'Private Lab-PPP' end as HBSElisaPlace,
    hbs_labe,
    p.HBSElisaLabOther,
    case p.HBSOther when 1 then 'HBSOther' end as HBSOther,
    p.HBSOtherName,
    case p.HBSOtherDate when '0000-00-00' then '' else p.HBSOtherDate end as HBSOtherDate,
    case p.HBSOtherResult WHEN 1 THEN 'Positive' WHEN 2 THEN 'Negative' end as HBSOtherResult,
    case p.HBSOtherPlace when 1 THEN 'Govt. Lab' WHEN 2 then 'Private Lab-PPP' end as HBSOtherPlace,
    hbs_labf,
    p.HBSLabOther,
    case p.AntiHCV WHEN 1 then 'AntiHCV' end as AntiHCV,
    case p.HCVRapid when 1 then 'HCVRapid' end as HCVRapid,
    case p.HCVRapidDate when '0000-00-00' then '' else p.HCVRapidDate end as HCVRapidDate,
    case p.HCVRapidResult WHEN 1 then 'Positive' when 2 THEN 'Negative' end as HCVRapidResult,
    case p.HCVRapidPlace WHEN 1 then 'Govt. Lab' WHEN 2 then 'Private Lab-PPP' end as HCVRapidPlace,
    hev_labj,
    p.HCVRapidLabOther,
    case p.HCVElisa when 1 THEN 'HCVElisa' end as HCVElisa,
    case p.HCVElisaDate when '0000-00-00' then '' else p.HCVElisaDate end as HCVElisaDate,
    case p.HCVElisaResult WHEN 1 then 'Positive' when 2 THEN 'Negative' end as HCVElisaResult,
    case p.HCVElisaPlace WHEN 1 then 'Govt. Lab' WHEN 2 then 'Private Lab-PPP' end as HCVElisaPlace,
    hev_labk,
    p.HCVElisaLabOther,
    case p.HCVOther WHEN 1 THEN 'HCVOther' end as HCVOther,
    p.HCVOtherName,
    case p.HCVOtherDate when '0000-00-00' then '' else p.HCVOtherDate end as HCVOtherDate,
    case p.HCVOtherResult when 1 THEN 'Positive' when 2 THEN 'Negative' end as HCVOtherResult,
    case p.HCVOtherPlace WHEN 1 then 'Govt. Lab' WHEN 2 then 'Private Lab-PPP' end as HCVOtherPlace,
    hev_labl,
    p.HCVLabOther,
    case p.LgmAntiHEV WHEN 1 then 'LgmAntiHEV' end as LgmAntiHEV,
    case p.HEVRapid WHEN 1 then 'HEVRapid' end as HEVRapid,
    case p.HEVRapidDate when '0000-00-00' then '' else p.HEVRapidDate end as HEVRapidDate,
    case p.HEVRapidResult WHEN 1 THEN 'Positive' when 2 THEN 'Negative' end as HEVRapidResult,
    case p.HEVRapidPlace WHEN 1 then 'Govt. Lab' WHEN 2 then 'Private Lab-PPP' end as HEVRapidPlace,
    hcv_labg,
    p.HEVRapidLabOther,
    case p.HEVElisa WHEN 1 THEN 'HEVElisa' end as HEVElisa,
    case p.HEVElisaDate when '0000-00-00' then '' else p.HEVElisaDate end as HEVElisaDate,
    case p.HEVElisaResult when 1 THEN 'Positive' when 2 THEN 'Negative' end as HEVElisaResult,
    case p.HEVElisaPlace WHEN 1 then 'Govt. Lab' WHEN 2 then 'Private Lab-PPP' end as HEVElisaPlace,
    hcv_labh,
    p.HEVElisaLabOther,
    case p.HEVOther WHEN 1 then 'HEVOther' end as HEVOther,
    p.HEVOtherName,
    case p.HEVOtherDate when '0000-00-00' then '' else p.HEVOtherDate end as HEVOtherDate,
    case p.HEVOtherResult when 1 THEN 'Positive' when 2 THEN 'Negative' end as HEVOtherResult,
    case p.HEVOtherPlace WHEN 1 then 'Govt. Lab' WHEN 2 then 'Private Lab-PPP' end as HEVOtherPlace,
    hcv_labi,
    p.HEVLabOther,
    case p.Refer_FacilityHEV when 1 then 'Hev Facility' end as Refer_FacilityHEV,
    case p.Refer_HigherFacilityHEV WHEN 1 THEN 'Higher Hev Facility' end as Refer_HigherFacilityHEV,
    case p.VLHepC WHEN 1 THEN 'HEP-C' end as VLHepC, 
    case p.VLSampleCollectionDate when '0000-00-00' then '' else p.VLSampleCollectionDate end as VLSampleCollectionDate,
    case p.IsVLSampleStored WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' end as IsVLSampleStored,
    p.VLStorageTemp,
    case p.Storage_days_hrs when 1 then 'Less than 1 day' WHEN 2 THEN 'More than 1 day' end as Storage_days_hrs,
    p.Storageduration,
    case p.IsVLSampleTransported when 1 THEN 'Yes' WHEN 2 THEN 'No' end as IsVLSampleTransported,
    p.VLTransportTemp,
    case p.VLTransportDate when '0000-00-00' then '' else p.VLTransportDate end as VLTransportDate,
    case p.VLLabID WHEN 1 then 'Lab1' WHEN 2 THEN 'Lab2' WHEN 3 THEN 'Lab3' WHEN 99 THEN 'Others' end as VLLabID,
    p.VLLabID_Other,
    p.VLTransporterName,
    vl_designation,
    p.VLSCRemarks,
    case p.VLRecieptDate when '0000-00-00' then '' else p.VLRecieptDate end as VLRecieptDate,
    p.VLReceiverName,
    vld_designation,
    case p.IsSampleAccepted when 1 THEN 'Yes' when 2 THEN 'No' end as IsSampleAccepted,
    case p.T_DLL_01_VLC_Date when '0000-00-00' then '' else p.T_DLL_01_VLC_Date end as T_DLL_01_VLC_Date,
    case p.T_DLL_01_VLC_Result when 1 THEN 'Detected' WHEN 2 THEN 'Not Detected' end as T_DLL_01_VLC_Result, 
    p.T_DLL_01_VLCount,
    rejection_look,
    p.VLResultRemarks,
    case p.VLHepB when 1 THEN 'HepB' end as VLHepB,
    case p.BVLSampleCollectionDate when '0000-00-00' then '' else p.BVLSampleCollectionDate end as BVLSampleCollectionDate,
    case  p.IsBVLSampleStored when 1 THEN 'Yes' WHEN 2 THEN 'No' end as IsBVLSampleStored,
    p.BVLStorageTemp,
    case p.BStorage_days_hrs when 1 THEN 'Less than 1 day' WHEN 2 THEN 'More than 1 day' end as BStorage_days_hrs,
    p.BStorageduration,
    case p.IsBVLSampleTransported when 1 THEN 'Yes' when 2 THEN 'No' end as IsBVLSampleTransported,
    p.BVLTransportTemp,
    case p.BVLTransportDate when '0000-00-00' then '' else p.BVLTransportDate end as BVLTransportDate,
    case p.BVLLabID WHEN 1 then 'Lab1' WHEN 2 THEN 'Lab2' WHEN 3 THEN 'Lab3' WHEN 99 THEN 'Others' end as BVLLabID,
    p.BVLLabID_Other,
    p.BVLTransporterName,
    bvl_designation,
    p.BVLSCRemarks,
    case p.BVLRecieptDate when '0000-00-00' then '' else p.BVLRecieptDate end as BVLRecieptDate,
    p.BVLReceiverName,
    tbv_designation,
    case p.IsBSampleAccepted when 1 THEN 'Yes' WHEN 2 THEN 'No' end as IsBSampleAccepted, 
    case p.T_DLL_01_BVLC_Date when '0000-00-00' then '' else p.T_DLL_01_BVLC_Date end as T_DLL_01_BVLC_Date,
    case p.T_DLL_01_BVLC_Result when 1 THEN 'Detected' WHEN 2 THEN 'Not Detected' end as T_DLL_01_BVLC_Result,
    p.T_DLL_01_BVLCount,
    svrb_look,
    p.BVLResultRemarks,
    case ch_Prescribing_Dtt when '0000-00-00' then '' else ch_Prescribing_Dtt end as ch_Prescribing_Dtt,
    case ch_LastTest_Dt when '0000-00-00' then '' else ch_LastTest_Dt end as ch_LastTest_Dt,
    p.V1_Haemoglobin,
    p.V1_Albumin,
    p.V1_Bilrubin,
    p.V1_INR,
    p.ALT,
    p.AST,
    p.AST_ULN,
    p.V1_Platelets,
    p.Weight,
    p.V1_Creatinine,
    p.V1_EGFR,
    ch_Clinical_US,
    case ch_Clinical_US_Dt when '0000-00-00' then '' else ch_Clinical_US_Dt end as ch_Clinical_US_Dt,
    ch_Fibroscan,
    case ch_Fibroscan_Dt when '0000-00-00' then '' else ch_Fibroscan_Dt end as ch_Fibroscan_Dt,
    ch_Fibroscan_LSM,
    ch_APRI,
    ch_APRI_Score,
    ch_FIB4,
    ch_FIB4_FIB4,
    case p.V1_Cirrhosis when 1 then 'Complicated' when 2 then 'Uncomplicated' end as V1_Cirrhosis,
    p.Cirr_VaricealBleed,
    p.Cirr_Ascites,
    p.Cirr_Encephalopathy,
    p.ChildScore,
    case p.Result when 1 then 'Compensated Cirrohosis' when 2 then 'Decompensated Cirrohosis' end as Result,
    look_LookupValue,    
    hiv_look,
    p.CKDStage,
    ribavarin_lookupvalue,
    p.LMP,
    case p.Pregnant when 1 then 'Yes' when 2 then 'No' end as Pregnant,
    case p.DeliveryDt when '0000-00-00' then '' else p.DeliveryDt end as DeliveryDt,
    case p.IsReferal when 1 then 'Yes' when 2 then 'No' end as IsReferal,
    referd_doc,
    p.ReferingDoctorOther,
    faci_facility_short_name,
    case p.ReferalDate when '0000-00-00' then '' else p.ReferalDate end as ReferalDate,
    p.SCRemarks,
    prescribing_name,
    p.PrescribingDoctorOther,
    rg_name,
    drug_strength,
    Daclat_strength,
    Velpat_strength,
    ribpat_strength,
    case p.T_DurationValue when 99 then 'Other' when 12 then '12' when 24 then '24' end as T_DurationValueval,
    otherdw_look,
    p.DurationReason,
    case p.PrescribingDate when '0000-00-00' then '' else p.PrescribingDate end as PrescribingDate,
    dispensation_facicode,
    case p.T_Initiation when '0000-00-00' then '' else p.T_Initiation end as T_Initiation,
    p.T_NoPillStart,
    case p_Next_Visitdt when '0000-00-00' then '' else p_Next_Visitdt end as p_Next_Visitdt,
    p.T_RmkDelay,
    case v1_visit_Dt when '0000-00-00' then '' else v1_visit_Dt end as v1_visit_Dt,
    v12_Haemoglobin,
    v1_PlateletCount,
    v1_PillsLeft,
    v1_Adherence,
    case v1_NAdherenceReason when 99 then 'Other' end as v1_NAdherenceReasondl,
    v1_NAdherenceReasonOther,
    case v1_NextVisit_Dt when '0000-00-00' then '' else v1_NextVisit_Dt end as v1_NextVisit_Dt,
    v1_Doctor,
    v1_DoctorOther,
    v1_Comments,
    case v2_visit_Dt when '0000-00-00' then '' else v2_visit_Dt end as v2_visit_Dt,
    v2_Haemoglobin,
    v2_PlateletCount,
    v2_PillsLeft,
    v2_Adherence,
    v2_NAdherenceReason,
    v2_NAdherenceReasonOther,
    case v2_NextVisit_Dt when '0000-00-00' then '' else v2_NextVisit_Dt end as v2_NextVisit_Dt,
    v2_Doctor,
    v2_DoctorOther,
    v2_Comments,
    case v3_visit_Dt when '0000-00-00' then '' else v3_visit_Dt end as v3_visit_Dt,
    v3_Haemoglobin,
    v3_PlateletCount,
    v3_PillsLeft,
    v3_Adherence,
    v3_NAdherenceReason,
    v3_NAdherenceReasonOther,
    case v3_NextVisit_Dt when '0000-00-00' then '' else v3_NextVisit_Dt end as v3_NextVisit_Dt,
    v3_Doctor,
    v3_DoctorOther,
    v3_Comments,
    case v4_visit_Dt when '0000-00-00' then '' else v4_visit_Dt end as v4_visit_Dt,
    v4_Haemoglobin,
    v4_PlateletCount,
    v4_PillsLeft,
    v4_Adherence,
    v4_NAdherenceReason,
    v4_NAdherenceReasonOther,
    case v4_NextVisit_Dt when '0000-00-00' then '' else v4_NextVisit_Dt end as v4_NextVisit_Dt,
    v4_Doctor,
    v4_DoctorOther,
    v4_Comments,
    case v5_visit_Dt when '0000-00-00' then '' else v5_visit_Dt end as v5_visit_Dt,
    v5_Haemoglobin,
    v5_PlateletCount,
    v5_PillsLeft,
    v5_Adherence,
    v5_NAdherenceReason,
    v5_NAdherenceReasonOther,
    case v5_NextVisit_Dt when '0000-00-00' then '' else v5_NextVisit_Dt end as v5_NextVisit_Dt,
    v5_Doctor,
    v5_DoctorOther,
    v5_Comments,
    case p.ETR_HCVViralLoad_Dt when '0000-00-00' then '' else p.ETR_HCVViralLoad_Dt end as ETR_HCVViralLoad_Dt,
    p.ETR_PillsLeft,
    patientAdherence,
    patientNAdherenceReason,
    patientNAdherenceReasonOther,
    case p.AdvisedSVRDate when '0000-00-00' then '' else p.AdvisedSVRDate end as AdvisedSVRDate,
    p.ETRDoctor,
    p.ETRDoctorOther,
    p.ETRComments,
    case p.SVRDrawnDate when '0000-00-00' then '' else p.SVRDrawnDate end as SVRDrawnDate,
    case p.IsSVRSampleStored when 1 then 'Yes' when 2 then 'No' end as IsSVRSampleStored,
    p.SVRStorageTemp,
    case p.IsSVRSampleTransported when 1 then 'Yes' when 2 then 'No' end as IsSVRSampleTransported,
    p.SVRTransportTemp,
    case p.SVRTransportDate when '0000-00-00' then '' else p.SVRTransportDate end as SVRTransportDate,
    p.SVR12W_LabID,
    p.SVRTransporterName,
    svr_designation,
    p.SVRTransportRemark,
    case p.SVRReceiptDate when '0000-00-00' then '' else p.SVRReceiptDate end as SVRReceiptDate, 
    p.SVRReceiverName,
    svrr_desig,
    case p.IsSVRSampleAccepted when 1 then 'Yes' when 2 then 'No' end as IsSVRSampleAccepted,
    case p.SVR12W_HCVViralLoad_Dt when '0000-00-00' then '' else p.SVR12W_HCVViralLoad_Dt end as SVR12W_HCVViralLoad_Dt,
    result_lookup,
    p.SVR12W_HCVViralLoadCount,
    svrr_look,
    p.SVRRejectionReasonOther,
    svrd_name,
    p.SVRDoctorOther,
    p.SVRComments,
     p.interruption_status
    FROM
    linelist p WHERE  " . $filter_facility . "  ".$this->datefilter." 
    limit $limit 
    offset $offset";

    // die($query);
    return $this->db->query($query)->result();


}else 
{
   $query = "SELECT ct.label_name,ct.db_name FROM linelist_template tmp 
   LEFT JOIN linelist_category ct ON tmp.categoryId=ct.lcategoryId 
   WHERE tmp.masterId  = ".$sessiontemp."";

   $db_name= array();
   $resdata = $this->db->query($query)->result();  

   foreach($resdata as $row){
    $db_nameval[] =  $row->db_name;
    $db_namevalval = implode(" ", $db_nameval);

}
 $query = "SELECT
".rtrim($db_namevalval,',')." 
from linelist p WHERE  " . $filter_facility . "  ".$this->datefilter." 
    limit $limit 
    offset $offset";

return $this->db->query($query)->result();
}


}

}
