<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Hepb_vaccination extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Common_Model');
		$this->load->helper('common');
		$loginData = $this->session->userdata('loginData');
		$this->load->model('Hep_B_Vaccination_Model');


		if($loginData == null)
		{
			redirect('login');
		}
	}

	public function index($flag=NULL)
	{	
		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');
		$loginData = $this->session->userdata('loginData');
		if($REQUEST_METHOD == 'POST')
		{
			$filter['Start_Date'] = timeStamp($this->input->post('startdate'));
			$filter['End_Date'] = timeStamp($this->input->post('enddate'));
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('mstfacilitylogin');	
		}	
		else
		{
			 $filter['Start_Date'] = date('Y-01-01');
			 $filter['End_Date'] = date('Y-m-d');
			$filters1['id_search_state'] ='';
			$filters1['id_input_district'] ='';
			$filters1['id_mstfacility'] ='';
		}
		
		
		$hepb_repo_filter = $this->session->set_userdata('hepb_repo_filter1',$filter);	
		$this->session->set_userdata('filters1',$filters1);

		//Apply conditions to show data according to National/State/Facility wise
//Conditions Start from here
		
		if($loginData->user_type==2 || $filters1['id_mstfacility']!=''){
/* 1> pass flag Hep_form is open for insert of update 0 means for insert mode 1 means edit mode

2> if $Repo is true that means we will get summery for enterd data if false we will get detailed data
*/			$content['ediflag']=0;     
			if($loginData->user_type==2){
				if($flag!=NULL || $flag!=""){

					$content['Repo']="true";
					$content['get_hepb_vacc']=$this->Hep_B_Vaccination_Model->get_State_hepb_Report();
				}
				elseif ($flag==NULL || $flag=="") {
					$content['Repo']="false";
					$content['get_hepb_vacc']=$this->Hep_B_Vaccination_Model->get_hepb_vacc();
				}
			}
			elseif ($filters1['id_mstfacility']!='' || $loginData->user_type!=2) {

				$content['Repo']="false";

				$content['get_hepb_vacc']=$this->Hep_B_Vaccination_Model->get_hepb_vacc();
			}

		}
		else if(($loginData->user_type==1 || $loginData->user_type==3)&&($filters1['id_mstfacility']=='')){
			$content['ediflag']=0;
			$content['Repo']="true";
			$content['get_hepb_vacc']=$this->Hep_B_Vaccination_Model->get_State_hepb_Report();

		}
	// Conditions End here

		$sql = "select * from mststate ORDER BY StateName ASC";
		$content['states'] = $this->db->query($sql)->result();
		$content['subview'] = 'Hepb_Vaccination'; 

		//View Page Name view/admin/components/Hepb_Vaccination.php

		$this->load->view('admin/main_layout', $content);
	}

	public function Add_Hepb_Vacc($hepb_id=NULL,$key=NULL){
		$loginData = $this->session->userdata('loginData');
					//echo "<pre>";print_r($loginData);
			//echo "<pre>";print_r($loginData);
		if($loginData->user_type!='2'){
			$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 
		}
		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = " 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

			$sess_where = " p.Session_StateID = '".$loginData->State_ID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = " p.Session_StateID = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = " p.Session_StateID = '".$loginData->State_ID."' ";
		}

		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');


		if($REQUEST_METHOD == 'POST')
		{
			$loginData = $this->session->userdata('loginData');
			$State_ID=$loginData->State_ID;
			$DistrictID=$loginData->DistrictID;
			$id_mstfacility=$loginData->id_mstfacility;
			if($loginData->State_ID==''||$loginData->State_ID==NULL){
				$State_ID=0;
			}
			if($loginData->DistrictID==''||$loginData->DistrictID==NULL){
				$DistrictID=0;
			}
			if($loginData->id_mstfacility==''||$loginData->id_mstfacility==NULL){
				$id_mstfacility=0;
			}
			$data=array(
				'worker_name'=> $this->input->post('worker_name'),
				'father_or_husband'=> $this->input->post('father_or_husband'),
				'gender'=> $this->input->post('gender'),
				'age'=> $this->input->post('age'),
				'dos1'=> timeStamp($this->input->post('dos1')),
				'dos2'=> timeStamp($this->input->post('dos2')), 
				'dos3'=> timeStamp($this->input->post('dos3')),
				'id_mststate'   =>  $State_ID,
				'id_mstdistrict' => $DistrictID,
				'id_mstfacility'     => $id_mstfacility,  
				'id_tblusers'  => $loginData->id_tblusers,
				'is_deleted'=>'0',
				'is_locked'=>'0',

			);
			if($hepb_id==NULL){
				$this->db->insert('tbl_healtcare_worker_hepb',$data);
					//$this->session->set_flashdata('tr_msg','Information Added Successfully');
				redirect('Hepb_vaccination/Add_Hepb_Vacc');		
			}
			elseif ($hepb_id!=NULL) {
				$this->db->where('id_health_worker_hepb',$hepb_id);
				$this->db->update('tbl_healtcare_worker_hepb',$data);
					//$this->session->set_flashdata('tr_msg','Information Updated Successfully');
				redirect('Hepb_vaccination/Add_Hepb_Vacc');				
			}						

		}
		if($hepb_id!=NULL){
			$hepb_id=base64_decode($hepb_id);
			$content['get_hepb_deatails']=$this->Hep_B_Vaccination_Model->get_hepb_vacc($hepb_id);
			$content['ediflag']=1;
			$content['sn']=$key;
		}
		else{

			$content['ediflag']=0;
			$content['get_hepb_vacc']=$this->Hep_B_Vaccination_Model->get_hepb_vacc();
		}
		$sql = "select * from mststate ORDER BY StateName ASC";
		$content['Repo']="false";
		$content['states'] = $this->db->query($sql)->result();
		$content['subview'] = "Hepb_Vaccination";
		$this->load->view('admin/main_layout', $content);
}

function set_locked($hepb_id=NULL){
	$hepb_id=base64_decode($hepb_id);
	$this->db->set('is_locked', '1');
       $this->db->where('id_health_worker_hepb',$hepb_id);
	$this->db->update('tbl_healtcare_worker_hepb');
	//$this->session->set_flashdata('tr_msg','Information Updated Successfully');
	redirect('Hepb_vaccination/Add_Hepb_Vacc');								
    }  
 function get_facility_detailed_table($Facility_id=NULL)
		{

			$sql="Select id_mststate,id_mstdistrict from mstfacility where id_mstfacility=?";
			$get_state_id=$this->db->query($sql,[base64_decode($Facility_id)])->result();
			$filters1['id_search_state'] =$get_state_id[0]->id_mststate;
			$filters1['id_input_district'] =$get_state_id[0]->id_mstdistrict;
			$filters1['id_mstfacility'] =base64_decode($Facility_id);
			$this->session->set_userdata('filters1',$filters1);
			$sql = "select * from mststate ORDER BY StateName ASC";
			$content['Repo']="false";
			$content['ediflag']=0;
			$content['get_hepb_vacc']=$this->Hep_B_Vaccination_Model->get_hepb_vacc();
			$content['states'] = $this->db->query($sql)->result();
			$content['subview'] = "Hepb_Vaccination";
			$this->load->view('admin/main_layout', $content);
		}

}
