<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patientinfo_hep_b extends CI_Controller {

	private $patientFilters;

	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('Common_Model');
		
		$this->load->model('Patient_Model');
		set_time_limit(100000);
		ini_set('memory_limit', '100000M');
		ini_set('upload_max_filesize', '300000M');
		ini_set('post_max_size', '200000M');
		ini_set('max_input_time', 100000);
		ini_set('max_execution_time', 0);
		ini_set('memory_limit','-1');
		$this->load->library('pagination');
		 $this->load->helper('common');

		//error_reporting(0);
		$loginData = $this->session->userdata('loginData');
		//pr($loginData);
		if($loginData == null)
		{
			redirect('login');
		}

		$this->patientFilters = $this->session->userdata('patientFilters');
	}
	public function index()
	{

		$loginData = $this->session->userdata('loginData');		
		//$sql = "SELECT * FROM mstlookup where flag = 13 and LanguageID = 1";
		if(!empty($_GET['p'])){
			if($_GET['p']==1){
				$LookupCode = "3,2,1,4,5,16";

			}elseif($_GET['p']==2){
				$LookupCode = "3,2,1,4,5,12,30,14,15,13";
			}elseif($_GET['p']==3){
				$LookupCode = "4,5,6,7,8,9,10,11,12,13,14,15";
			}else{
				$LookupCode = "0";
			}
		}else{
			$LookupCode = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16";
		}


/*old*/
/*if(!empty($_GET['p'])){	
			if($_GET['p']==2){
				$sql = "SELECT * FROM mstlookup where flag = 72  and LookupCode in($LookupCode) and LanguageID = 1 order by LookupCode ASC";
			} elseif($_GET['p']==1){
				$sql = "SELECT * FROM mstlookup where flag = 72  and LookupCode in($LookupCode) and LanguageID = 1 order by LookupCode ASC";
			}
			else{

				$sql = "SELECT * FROM mstlookup where flag = 72  and LanguageID = 1 and LookupCode in($LookupCode) order by LookupCode ASC";
			}	
		}else{
			$sql = "SELECT * FROM mstlookup where flag = 72  and LanguageID = 1 order by LookupCode ASC";
		}
*/
		/*end old*/

		if(!empty($_GET['p'])){	
			if($_GET['p']==2){
				$sql = "SELECT * FROM mstlookup where flag = 72   and LanguageID = 1 order by LookupCode ASC";
			} elseif($_GET['p']==1){
				$sql = "SELECT * FROM mstlookup where flag = 72   and LanguageID = 1 order by LookupCode ASC";
			}
			else{

				$sql = "SELECT * FROM mstlookup where flag = 72  and LanguageID = 1  order by LookupCode ASC";
			}	
		}else{
			$sql = "SELECT * FROM mstlookup where flag = 72  and LanguageID = 1 order by LookupCode ASC";
		}
		$content['status'] = $this->Common_Model->query_data($sql);

		$patient_list = array();
		$where = " where 1 ";

		$RequestMethod = $this->input->server('REQUEST_METHOD');
		$query_params = [];
		if($RequestMethod == 'POST')
		{
			$search_by = $this->security->xss_clean($this->input->post('search_by'));
			if($search_by == 1)
			{				
				$uid_contact = $this->security->xss_clean($this->input->post('uid_contact'));
				$query_params[]  = $uid_contact;									
				$uid_contact_len = strlen($uid_contact);
				if($uid_contact_len <= 6)
				{
					$where = " where p.UID_Num = ?";
				}
				else if($uid_contact_len > 6)
				{
					$where = " where p.Mobile = ?";
				}
			}
			elseif($search_by == 3)
			{				
				$pat_name = $this->security->xss_clean($this->input->post('pat_name'));				
				$where = "where p.FirstName LIKE '%$pat_name%'";				
			}
			else if($search_by == 2)
			{
				$search_status = $this->security->xss_clean($this->input->post('search_status'));

				if($search_status == 1  ){

					$search_statusvaldb =" where hp.Status = 1 ";

				}
				elseif($search_status == 2){

					$search_statusvaldb =" where hp.Status = 2";
				}

				elseif($search_status == 3){
					$search_statusvaldb =" where hp.Status = 3";
				}

				elseif($search_status == 4){
					$search_statusvaldb =" where hp.Status = 4";
				}

				elseif($search_status == 5){
					$search_statusvaldb =" where hp.Status = 5";
				}

				elseif($search_status == 6){ 
					$search_statusvaldb =" where hp.Status =6";
				}

				elseif($search_status == 7){
					$search_statusvaldb =" where hp.Status =7";
				}

				elseif($search_status == 8){
					$search_statusvaldb =" where hp.Status =8";
				}

				elseif($search_status == 9){
					$search_statusvaldb =" where hp.Status =9";
				}

				elseif($search_status == 10){
					$search_statusvaldb =" where hp.Status =10";
				}

				elseif($search_status == 11){
					$search_statusvaldb =" where hp.Status = 11";
				}

				elseif($search_status == 12){
					$search_statusvaldb =" where hp.Status = 13";
				}

				elseif($search_status == 13){
					$search_statusvaldb =" where hp.Status = 30";
				}

				elseif($search_status == 14){
					$search_statusvaldb =" where hp.Status = 14";
				}

				elseif($search_status == 15){
					$search_statusvaldb =" where hp.Status = 15";
				}
				elseif($search_status == 16){
					$search_statusvaldb =" where hp.Status = 16";
				}
				elseif($search_status == 32){
					$search_statusvaldb =" where hp.Status = 32";
				}
				elseif($search_status == 30){
					$search_statusvaldb =" where hp.Status = 30";
				}

				$where = $search_statusvaldb;
			}
			else
			{
				$where = " where 1 ";
			}
				/*$uid_contact = $this->security->xss_clean($this->input->post('uid_contact'));
				$uid_contact_len = strlen($uid_contact);
				if($uid_contact_len <= 6)
				{
					$where = " where p.UID_Num = ?";
				}
				else if($uid_contact_len > 6)
				{
					$where = " where p.Mobile = ?";
				}
			}
			else if($search_by == 2)
			{

				$search_status = $this->security->xss_clean($this->input->post('search_status'));

				$where = " where p.Status = ?";
			}
			else
			{
				$where = " where 1 ";
			}*/

		}
		if( ($loginData) && $loginData->user_type == '1' ){
			$sess_where = "AND 1";
		}
		elseif( ($loginData) && $loginData->user_type == '2' ){

			$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
		}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."'";
		}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
			$sess_where = "AND Session_StateID = '".$loginData->State_ID."' AND Session_DistrictID ='".$loginData->DistrictID."'";
		}

		$offset=0;
		/*Pahnation start*/
		 $sql11 = "select count(*) as count,p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID from tblpatient p left join hepb_tblpatient hp on hp.PatientGUID=p.PatientGUID  left join (SELECT * FROM mstlookup where Flag =72 AND LanguageID = 1) b on hp.status = b.LookupCode ".$where."  ".$sess_where." AND HbsAg = 1 AND (AntiHCV is null  OR (AntiHCV=1 ) ) ";
		$patient_listcount = $this->db->query($sql11, $query_params)->result();		
//echo 'ggg'.count($patient_listcount);
		$config['total_rows'] = $patient_listcount[0]->count;
		$content['total_count'] = $config['total_rows'];
		$config['suffix'] = '';

		if ($config['total_rows'] > 0) {
			$page_number = $this->uri->segment(3);
			$config['base_url'] = base_url() . 'Patientinfo_hep_b/index/';
			if (empty($page_number))
				$page_number = 1;

			$offset = ($page_number - 1) * $this->pagination->per_page;

			$content['offsetdata'] = ($page_number - 1) * $this->pagination->per_page;
			$content['perpage'] = $this->pagination->per_page;
			$content['pagecount'] =  $page_number;

			$this->pagination->cur_page = $page_number;
			$this->pagination->initialize($config);
			$content['page_links'] = $this->pagination->create_links();
		}

		/*Pahnation end*/
		   //$sql = "select p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID from tblpatient p left join (SELECT * FROM mstlookup where Flag =13 AND LanguageID = 1) b on p.status = b.LookupCode ".$where."  ".$sess_where." AND (VLHepB = 1 OR HbsAg = 1) AND (VLHepC is null OR VLHepC = 0 OR (VLHepC=1 AND status in (15)) ) order by p.CreatedOn DESC limit ".$this->pagination->per_page." offset ".$offset."";

		 $sql = "select p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID from tblpatient p left join hepb_tblpatient hp on hp.PatientGUID=p.PatientGUID  left join (SELECT * FROM mstlookup where Flag =72 AND LanguageID = 1) b on hp.status = b.LookupCode ".$where."  ".$sess_where." AND HbsAg = 1 AND (AntiHCV is null  OR (AntiHCV=1) ) order by p.CreatedOn DESC limit ".$this->pagination->per_page." offset ".$offset."";

		$patient_list = $this->db->query($sql, $query_params)->result();
		
		$content['patient_list'] = $patient_list;

		$content['patient_listCount'] = $patient_listcount[0]->count;

		$sql = "select * from mststate";
		$content['states'] = $this->db->query($sql)->result();

		$sql = "select * from mstdistrict";
		$content['districts'] = $this->db->query($sql)->result();

		$sql = "select * from mstfacility";
		$content['facilities'] = $this->db->query($sql)->result();	

		$sql = "select flag from tblusers where id_tblusers = ".$loginData->id_tblusers."";
		$content['flag'] = $this->db->query($sql)->result();	

		$content['subview'] = 'hepb/patient_search_hep_b';
		$this->load->view('pages/main_layout', $content);
	}

	public function patient_redirect($patientguid = null, $visit = 1)
	{		
		 $sql = "SELECT PatientGUID, status, MF1, MF2, MF3, MF4, MF5, MF6 FROM hepb_tblpatient where PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();
		//pr($patient_data);exit();
		//|| $patient_data[0]->status == 12
		/*if($patient_data[0]->MF7 == 1)
		{ 
			if($patient_data[0]->status == 3 
				|| $patient_data[0]->status == 4
				|| $patient_data[0]->status == 18
				|| $patient_data[0]->status == 20
				|| $patient_data[0]->status == 24
				|| $patient_data[0]->status == 12)
			{
				$redirect_page = 'patient_dispensation/'.$patient_data[0]->PatientGUID."/2";
			}
			else if($patient_data[0]->status == 5 
				|| $patient_data[0]->status == 6
				|| $patient_data[0]->status == 21
				|| $patient_data[0]->status == 25)
			{
				$redirect_page = 'patient_dispensation/'.$patient_data[0]->PatientGUID."/3";
			}
			else if($patient_data[0]->status == 8 
				|| $patient_data[0]->status == 22
				|| $patient_data[0]->status == 26)
			{
				$redirect_page = 'patient_dispensation/'.$patient_data[0]->PatientGUID."/4";
			}
			else if($patient_data[0]->status == 9 
				|| $patient_data[0]->status == 27)
			{
				$redirect_page = 'patient_dispensation/'.$patient_data[0]->PatientGUID."/5";
			}
			else if($patient_data[0]->status == 10)
			{
				$redirect_page = 'patient_dispensation/'.$patient_data[0]->PatientGUID."/6";
			}
			else if($patient_data[0]->status == 17 
				|| $patient_data[0]->status == 19
				|| $patient_data[0]->status == 7
				|| $patient_data[0]->status == 23
				|| $patient_data[0]->status == 28
				|| $patient_data[0]->status == 11)
			{
				$redirect_page = 'patient_dispensation/'.$patient_data[0]->PatientGUID."/eot";
			}
			else
			{
				$redirect_page = 'patient_svr/'.$patient_data[0]->PatientGUID;
			}
		}*/

		 if($patient_data[0]->MF6 == 1)
		{
			$redirect_page = 'patient_dispensation/'.$patient_data[0]->PatientGUID;
		}

		else if($patient_data[0]->MF5 == 1)
		{
			$redirect_page = 'patient_prescription/'.$patient_data[0]->PatientGUID;
		}

		else if($patient_data[0]->MF4 == 1)
		{
			
				$redirect_page = 'known_history/'.$patient_data[0]->PatientGUID;
			
		}

		else if($patient_data[0]->MF3 == 1)
		{

			$query = "SELECT Elevated_ALT_Level FROM `hepb_tblpatient` WHERE PatientGUID = ?";
			$patient_ALT_Level = $this->db->query($query,[$patient_data[0]->PatientGUID])->row();

			if($patient_ALT_Level->Elevated_ALT_Level == 2 || $patient_ALT_Level->Elevated_ALT_Level == 3){

				$redirect_page = 'patient_viral_load/'.$patient_data[0]->PatientGUID;
			}elseif($patient_ALT_Level->Elevated_ALT_Level == 1){
				//gau chnaged known_history to viralload
				$redirect_page = 'patient_testing_followup_visit/'.$patient_data[0]->PatientGUID;
			}
			
		}

		else if($patient_data[0]->MF2 == 1)
		{	

			$redirect_page = 'patient_testing/'.$patient_data[0]->PatientGUID;
		}
		else if($patient_data[0]->MF1 == 1)
		{
			
			$redirect_page = 'patient_screening/'.$patient_data[0]->PatientGUID;
		}
		else
		{
			$redirect_page = '';
			$patientguid   = null;
		}

		redirect('patientinfo_hep_b/'.$redirect_page);
	}

	public function patient_register($patientguid = null)
	{
		$loginData = $this->session->userdata('loginData');		
		$RequestMethod = $this->input->server('REQUEST_METHOD');

		$patient_list      = array();
		$patient_data      = array();
		$search_state      = '';
		$search_hospital   = '';
		$search_uid_num    = '';
		$patient_districts = array();
		$patient_blocks    = array();

		$sql = "SELECT * FROM `tblpatient` where id_mstfacility = ".$loginData->id_mstfacility."";
		$patient_list = $this->db->query($sql)->result();


		//echo $sql1 = "SELECT max(id_tblpatient) as uidval FROM `tblpatient` where id_mstfacility = ".$loginData->id_mstfacility."";
		$sql1 = "SELECT COUNT(id_tblpatient) as uidval FROM `tblpatient` where id_mstfacility = ".$loginData->id_mstfacility."";
		$patient_uidval = $this->db->query($sql1)->result();
		$patient_uid = $patient_uidval[0]->uidval+1;


		if($RequestMethod == 'POST')
		{
			if(isset($_POST['patregsub']))
			{
				// form_validation start
				$this->form_validation->set_rules('opd_id', 'OPD ID', 'trim|required|xss_clean');
				$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[50]|min_length[1]|xss_clean');
				$this->form_validation->set_rules('hcv_uid_num', 'UID', 'trim|required|max_length[6]|min_length[6]|xss_clean');
				$this->form_validation->set_rules('pin','Pincode','required|max_length[6]|min_length[6]|xss_clean');
				// $this->form_validation->set_rules('age','Age','required|max_length[1]|min_length[150]|xss_clean');
				$this->form_validation->set_rules('gender', 'Gender', 'trim|required|xss_clean');
				$this->form_validation->set_rules('relative_name', 'Relative Name', 'trim|required|xss_clean');
				$this->form_validation->set_rules('address', 'Home & Street Address', 'trim|required|xss_clean');
				$this->form_validation->set_rules('sms_consent', 'Consent for Receiving Communication', 'trim|required|xss_clean');

				if ($this->form_validation->run() == FALSE) {

					$arr['status'] = 'false';
					$this->session->set_flashdata("tr_msg",validation_errors());
				}else{

					$uid = $this->security->xss_clean($this->input->post('hcv_uid'));
					if($this->security->xss_clean($this->input->post('risk'))!=""){
						$riskdata = implode(',', $this->security->xss_clean($this->input->post('risk')));
					}
					if($this->security->xss_clean($this->input->post('patient_type1')) == 2 && $this->security->xss_clean($this->input->post('patient_type1'))!=""){

						$ExperiencedCategory = 1;
					}

					else{

						$ExperiencedCategory = 2;
					}
					if($this->security->xss_clean($this->input->post('patient_type1'))==""){
						$ExperiencedCategory = 0;
					}

					if($this->security->xss_clean($this->input->post('patient_type'))==2){

						$patient_type_facility = $this->security->xss_clean($this->input->post('patient_type_facility'));
						$patient_type_state = $this->security->xss_clean($this->input->post('patient_type_state'));
					}else{

						$patient_type_facility=0;
						$patient_type_state =0; 
					}
					$insert_array = array(
						"id_mstfacility"    => $loginData->id_mstfacility,
						//"UID_Prefix"        => $this->security->xss_clean($this->input->post('hcv_uid_prefix')),
						'Session_StateID'   =>$loginData->State_ID,
						'Session_DistrictID'=>$loginData->DistrictID,
						'VLHepB' 			=>1,
						'HbsAg'				=>1,
						"UID_Prefix"        => $this->security->xss_clean($this->getPrefix()),
						"UID_Num"           => $this->security->xss_clean($this->input->post('hcv_uid_num')),
						"CreatedOn"         => $this->security->xss_clean($this->input->post('date_and_time')),
						"OPD_Id"            => $this->security->xss_clean($this->input->post('opd_id')),
						"PTState"			=> $patient_type_state,
						"PTYear"			=> $this->security->xss_clean($this->input->post('patient_type_treatment_year')),
						"PastUID"			=> $this->security->xss_clean($this->input->post('patient_type_treatment_uid')),
						"PastFacility"		=> $patient_type_facility,
						"FirstName"         => $this->security->xss_clean($this->input->post('name')),
						"IsAgeMonths"       => $this->security->xss_clean($this->input->post('age_between')),
						"Age"               => $this->security->xss_clean($this->input->post('age')),
						"Gender"            => $this->security->xss_clean($this->input->post('gender')),
						"Pregnant"          => $this->security->xss_clean($this->input->post('pregnancy')),
						"Relation"          => $this->security->xss_clean($this->input->post('select_relative')),
						"FatherHusband"     => $this->security->xss_clean($this->input->post('relative_name')),
						"Add1"              => $this->security->xss_clean($this->input->post('address')),
						"State"             => $this->security->xss_clean($this->input->post('input_state')),
						"District"          => $this->security->xss_clean($this->input->post('input_district')),
						"DistrictOther"             => $this->security->xss_clean($this->input->post('input_district_other')),
						"BLOCK"             => $this->security->xss_clean($this->input->post('input_block')),
						"BlockOther"          => $this->security->xss_clean($this->input->post('input_block_other')),
						"VillageTown"       => $this->security->xss_clean($this->input->post('village')),
						"PIN"               => $this->security->xss_clean($this->input->post('pin')),
						"IsMobile_Landline" => $this->security->xss_clean($this->input->post('contact_type')),
						"Mobile"            => $this->security->xss_clean($this->input->post('contact_no')),
						"Aadhaar"           => $this->security->xss_clean($this->input->post('aadhar_health_id')),
						"IsSMSConsent"      => $this->security->xss_clean($this->input->post('sms_consent')),
						"Risk"              => $riskdata,
						"OtherRisk"         => $this->security->xss_clean($this->input->post('risk_factor_other')),
						"PatientType"       => $this->security->xss_clean($this->input->post('patient_type')),
						"ExperiencedCategory" => $ExperiencedCategory,
						"date_of_patient_registration" => $this->security->xss_clean(timeStamp($this->input->post('date_of_patient_registration'))),
						
						"SVR_TreatmentStatus" =>2,
						"CreatedOn"         => date('Y-m-d'),
						"CreatedBy"         => $loginData->id_tblusers,
						"UpdatedOn"			=> date('Y-m-d'),
						"UpdatedBy"			=> $loginData->id_tblusers,
						"UploadedBy"		=> $loginData->id_tblusers,
						"UploadedOn"		=> date('Y-m-d'),
						"basic_info_updated_on" => date('Y-m-d'),
						"basic_info_updated_by" => $loginData->id_tblusers,
						"IsEdited"			=> 1,
						"T_AntiHCV01_Result"=> 1,
						"NextVisitPurpose"  => 0
					);

					$update_array = array(
						"id_mstfacility"    => $loginData->id_mstfacility,
						//"UID_Prefix"        => $this->security->xss_clean($this->input->post('hcv_uid_prefix')),
						'Session_StateID'   =>$loginData->State_ID,
						'Session_DistrictID'=>$loginData->DistrictID,
						"UID_Prefix"        => $this->security->xss_clean($this->getPrefix()),
						"UID_Num"           => $this->security->xss_clean($this->input->post('hcv_uid_num')),
						"CreatedOn"         => $this->security->xss_clean($this->input->post('date_and_time')),
						"OPD_Id"            => $this->security->xss_clean($this->input->post('opd_id')),
						"PTState"			=> $patient_type_state,
						"PTYear"			=> $this->security->xss_clean($this->input->post('patient_type_treatment_year')),
						"PastUID"			=> $this->security->xss_clean($this->input->post('patient_type_treatment_uid')),
						"PastFacility"		=> $patient_type_facility,
						"FirstName"         => $this->security->xss_clean($this->input->post('name')),
						"IsAgeMonths"       => $this->security->xss_clean($this->input->post('age_between')),
						"Age"               => $this->security->xss_clean($this->input->post('age')),
						"Gender"            => $this->security->xss_clean($this->input->post('gender')),
						"Pregnant"          => $this->security->xss_clean($this->input->post('pregnancy')),
						"Relation"          => $this->security->xss_clean($this->input->post('select_relative')),
						"FatherHusband"     => $this->security->xss_clean($this->input->post('relative_name')),
						"Add1"              => $this->security->xss_clean($this->input->post('address')),
						"State"             => $this->security->xss_clean($this->input->post('input_state')),
						"District"          => $this->security->xss_clean($this->input->post('input_district')),
						"DistrictOther"             => $this->security->xss_clean($this->input->post('input_district_other')),
						"BLOCK"             => $this->security->xss_clean($this->input->post('input_block')),
						"BlockOther"          => $this->security->xss_clean($this->input->post('input_block_other')),
						"VillageTown"       => $this->security->xss_clean($this->input->post('village')),
						"PIN"               => $this->security->xss_clean($this->input->post('pin')),
						"IsMobile_Landline" => $this->security->xss_clean($this->input->post('contact_type')),
						"Mobile"            => $this->security->xss_clean($this->input->post('contact_no')),
						"Aadhaar"           => $this->security->xss_clean($this->input->post('aadhar_health_id')),
						"IsSMSConsent"      => $this->security->xss_clean($this->input->post('sms_consent')),
					
						"OtherRisk"         => $this->security->xss_clean($this->input->post('risk_factor_other')),
						//"PatientType"       => $this->security->xss_clean($this->input->post('patient_type')),
						"date_of_patient_registration" => $this->security->xss_clean(timeStamp($this->input->post('date_of_patient_registration'))),
						//"ExperiencedCategory" => $ExperiencedCategory,
						"UpdatedOn"			=> date('Y-m-d'),
						"UpdatedBy"			=> $loginData->id_tblusers,
						"UploadedBy"		=> $loginData->id_tblusers,
						"UploadedOn"		=> date('Y-m-d'),
						"basic_info_updated_on" => date('Y-m-d'),
						"basic_info_updated_by" => $loginData->id_tblusers
					);

					
					if($patientguid == null)
					{
						$insert_array['PatientGUID'] = uniqid();
						$this->db->insert('tblpatient', $insert_array);
						$patientguid = $insert_array['PatientGUID'];
										
										$insert_arrayhepb = array(	'Risk'              => $riskdata,
																	'PatientGUID'       =>$patientguid,
																	"MF1"               => 1,
																	"Status"            => 1 );
						
						$this->db->insert('hepb_tblpatient', $insert_arrayhepb);
						

					}
					else
					{
						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('tblpatient', $update_array);

								
								$insert_arrayhepbg = array('Risk' => $riskdata);
						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('hepb_tblpatient', $insert_arrayhepbg);



					}

					if($this->input->post('interruption_status')==1){


						redirect('patientinfo_hep_b/patient_register/'.$patientguid);
					}elseif($this->input->post('interruption_status')==null || $this->input->post('interruption_status')==0){

					/*$data = array(
						'InterruptReason' => null,
						'DeathReason' => null,
						'LFUReason' => null
					);

					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('tblpatient', $data);*/
				}

				redirect('patientinfo_hep_b/patient_screening/'.$patientguid);
			}
	      } // from validation end
	  }
	  else if($patientguid != null)
	  {

	  	$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
	  	$patient_data = $this->db->query($sql,[$patientguid])->result();

	  	$sql = "SELECT * FROM `hepb_tblpatient` WHERE PatientGUID = ?";
	  	$patient_datahepb = $this->db->query($sql,[$patientguid])->result();


	  	$sql_patient_districts = "SELECT * FROM `mstdistrict` where id_mststate = ".$patient_data[0]->State;
	  	$patient_districts = $this->db->query($sql_patient_districts)->result();

	  	$sql_patient_blocks = "SELECT * FROM `mstblock` where id_mstdistrict = ".$patient_data[0]->District;
	  	$patient_blocks = $this->db->query($sql_patient_blocks)->result();

	  	$search_uid_num  = $patient_data[0]->UID_Num;

	  	$sql = "select p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID from tblpatient p left join (SELECT * FROM mstlookup where Flag =13 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
	  	$content['patient_status'] = $this->db->query($sql,[$patientguid])->result();

	  	$sql = "select  b.LookupValue as status, p.PatientGUID from hepb_tblpatient p left join (SELECT * FROM mstlookup where Flag =72 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
	  	$content['patient_statushepb'] = $this->db->query($sql,[$patientguid])->result();
	  	//print_r($content['patient_statushepb']);

	  }

	  $sql = "select * from mstfacility where id_mstfacility = ".$loginData->id_mstfacility;
	  $content['default_facilities'] = $this->db->query($sql)->result();

	  $sql = "select * from mstdistrict where id_mststate = ".$content['default_facilities'][0]->id_mststate;
	  $content['default_districts'] = $this->db->query($sql)->result();

	  $sql = "select * from mstblock where id_mstdistrict = ".$content['default_facilities'][0]->id_mstdistrict;
	  $content['default_block'] = $this->db->query($sql)->result();
		//print_r($content['default_districts']);

	  $sql = "select * from mstfacility";
	  $content['facilities'] = $this->db->query($sql)->result();

	  $sql = "select * from mststate order by StateName";
	  $content['states'] = $this->db->query($sql)->result();

	  $sql = "SELECT * FROM `mstlookup` where flag = 41 and LanguageID = 1";
	  $content['relatives_name'] = $this->db->query($sql)->result();

	  $sql = "SELECT * FROM `mstlookup` where flag = 70 and LanguageID = 1 order by Sequence";
	  $content['risk_factor'] = $this->db->query($sql)->result();

	  $sql = "SELECT * FROM `mstlookup` where flag = 48 and LanguageID = 1";
	  $content['reason_death'] = $this->db->query($sql)->result();

	  $sql = "SELECT * FROM `mstlookup` where flag = 49 and LanguageID = 1";
	  $content['reason_flu'] = $this->db->query($sql)->result();

	  $sql ="SELECT * FROM mstlookup where  flag=32 order by Sequence ASC";
	  $content['InterruptReason'] = $this->db->query($sql)->result();

	  $content['patient_list']      = $patient_list;
	  $content['patient_data']      = $patient_data;
	  $content['patient_datahepb']   = $patient_datahepb;
	  
	  $content['user_hospital']     = $loginData->id_mstfacility;
	  $content['patient_districts'] = $patient_districts;
	  $content['patient_blocks']    = $patient_blocks;
	  $content['uid_prefix']        = $this->getPrefix();
	  $content['patient_uid']        = $patient_uid;

	  $content['subview'] = 'hepb/patient_register_hep_b';
	  $this->load->view('pages/main_layout', $content);
	}

	public function patient_screening($patientguid = null)
	{
		$loginData = $this->session->userdata('loginData');

		$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();

		if(!$this->allow_url($patientguid, __FUNCTION__))
		{
			$this->patient_redirect($patientguid);
		}


		/*test update start*/
		$sql = "SELECT * FROM `hepb_tblpatient` WHERE PatientGUID = ?";
				$patient_datahepb = $this->db->query($sql,[$patientguid])->result();	
				
			if(empty($patient_datahepb[0]->MF2)){

				$update_array = array(
				
					"V1_Haemoglobin"      =>  $patient_data[0]->V1_Haemoglobin,
					"V1_Albumin"          =>  $patient_data[0]->V1_Albumin,
					"V1_Bilrubin"         =>  $patient_data[0]->V1_Bilrubin,
					"V1_INR"              =>  $patient_data[0]->V1_INR,
					"ALT"                 =>  $patient_data[0]->ALT,
					"AST"                 =>  $patient_data[0]->AST,
					"AST_ULN"             =>  $patient_data[0]->AST_ULN,
					"V1_Platelets"        =>  $patient_data[0]->V1_Platelets,
					"Weight"              =>  $patient_data[0]->Weight,
					"V1_Creatinine"       =>  $patient_data[0]->V1_Creatinine,
					"V1_EGFR"             =>  $patient_data[0]->V1_EGFR,
					"V1_Cirrhosis"        =>  $patient_data[0]->V1_Cirrhosis,
					"Cirr_Encephalopathy" =>  $patient_data[0]->Cirr_Encephalopathy,
					"Cirr_Ascites"        =>  $patient_data[0]->Cirr_Ascites,
					"Cirr_VaricealBleed"  =>  $patient_data[0]->Cirr_VaricealBleed,
					"ChildScore"          =>  $patient_data[0]->ChildScore,
					"CirrhosisStatus "	 =>  $patient_data[0]->CirrhosisStatus,
					"Result" 			=> $patient_data[0]->Result

					//"" =>$this->security->xss_clean($this->input->post('compensated_decompensated'))

				);


				$this->db->where('PatientGUID', $patientguid);
				$this->db->update('hepb_tblpatient', $update_array);

			}
		/*end test*/

		$facility_sql = "select id_mstfacility,FacilityCode from mstfacility where id_mststate=".$loginData->State_ID." union Select '999999' as id_mstfacility,'Others' as FacilityCode";
		$content['facility'] = $this->Common_Model->query_data($facility_sql);


		$sql = "select * from mstfacility where id_mstfacility = ".$loginData->id_mstfacility;
		$content['default_facilities'] = $this->db->query($sql)->result();
		

		$result_sql = "SELECT * FROM `mstlookup` where Flag = 4 and LanguageID = 1";
		$content['result_options'] = $this->Common_Model->query_data($result_sql);

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST')
		{
			if(isset($_POST['save']))
			{
				$viral_infection = false;
				$test_dates = array();

				//echo 'fffff'.timeStamp($this->input->post('hav_rapid_date'));exit();
				if($this->security->xss_clean($this->input->post('check_hav')) == null)
				{
					$update_array['LgmAntiHAV']     = null;
					$update_array['HAVRapid']       = null;
					$update_array['HAVElisa']       = null;
					$update_array['HAVOther']       = null;
					$update_array['HAVRapidDate']   = null;
					$update_array['HAVRapidResult'] = null;
					$update_array['HAVRapidPlace']  = null;
					$update_array['HAVRapidLabID']  = null;
					$update_array['HAVElisaDate']   = null;
					$update_array['HAVElisaResult'] = null;
					$update_array['HAVElisaPlace']  = null;
					$update_array['HAVElisaLabID']  = null;
					$update_array['HAVOtherName']   = null;
					$update_array['HAVOtherDate']   = null;
					$update_array['HAVOtherResult'] = null;
					$update_array['HAVOtherPlace']  = null;
					$update_array['HAVOtherLabID']  = null;
				}
				else
				{
					if($this->security->xss_clean($this->input->post('hav_rapid')) == null)
					{
						$update_array['HAVRapid']         = 0;
						$update_array['HAVRapidDate']     = null;
						$update_array['HAVRapidResult']   = null;
						$update_array['HAVRapidPlace']    = null;
						$update_array['HAVRapidLabID']    = null;
						$update_array['HAVRapidLabOther'] = null;
					}
					else
					{
						$update_array['LgmAntiHAV']       = 1;
						$update_array['HAVRapid']         = 1;
						$update_array['HAVRapidDate']     = $this->security->xss_clean(timeStamp($this->input->post('hav_rapid_date')));
						$update_array['HAVRapidResult']   = $this->security->xss_clean($this->input->post('hav_rapid_result'));
						$update_array['HAVRapidPlace']    = $this->security->xss_clean($this->input->post('hav_rapid_place_of_test'));
						$update_array['HAVRapidLabID']    = $this->security->xss_clean($this->input->post('hav_rapid_lab_name'));
						$update_array['HAVRapidLabOther'] = $this->security->xss_clean($this->input->post('hav_rapid_lab_name_other'));
						$update_array['MF2']              = 1;
						if($this->input->post('Refer_FacilityHAV')==1){
							$update_array['Refer_FacilityHAV']          = $this->security->xss_clean($this->input->post('Refer_FacilityHAV'));
							$update_array['Refer_HigherFacilityHAV'] =0;
						}else{
							$update_array['Refer_HigherFacilityHAV']    = $this->security->xss_clean($this->input->post('Refer_FacilityHAV'));
							$update_array['Refer_FacilityHAV'] =0;
						}
						$test_dates[] = $this->security->xss_clean(timeStamp($this->input->post('hav_rapid_date')));

						if($this->security->xss_clean($this->input->post('hav_rapid_result')) == 1)
						{
							$viral_infection = true;
						}
					}

					if($this->security->xss_clean($this->input->post('hav_elisa')) == null)
					{
						$update_array['HAVElisa']         = 0;
						$update_array['HAVElisaDate']     = null;
						$update_array['HAVElisaResult']   = null;
						$update_array['HAVElisaPlace']    = null;
						$update_array['HAVElisaLabID']    = null;
						$update_array['HAVElisaLabOther'] = null;
					}
					else
					{
						$update_array['LgmAntiHAV']       = 1;
						$update_array['HAVElisa']         = 1;
						$update_array['HAVElisaDate']     = $this->security->xss_clean(timeStamp($this->input->post('hav_elisa_date')));
						$update_array['HAVElisaResult']   = $this->security->xss_clean($this->input->post('hav_elisa_result'));
						$update_array['HAVElisaPlace']    = $this->security->xss_clean($this->input->post('hav_elisa_place_of_test'));
						$update_array['HAVElisaLabID']    = $this->security->xss_clean($this->input->post('hav_elisa_lab_name'));
						$update_array['HAVElisaLabOther'] = $this->security->xss_clean($this->input->post('hav_elisa_lab_name_other'));
						$update_array['MF2']              = 1;

						$test_dates[] = $this->security->xss_clean(timeStamp($this->input->post('hav_elisa_date')));

						if($this->security->xss_clean($this->input->post('hav_elisa_result')) == 1)
						{
							$viral_infection = true;
						}
					}

					if($this->security->xss_clean($this->input->post('hav_other')) == null)
					{
						$update_array['HAVOther']       = 0;
						$update_array['HAVOtherName']   = null;
						$update_array['HAVOtherDate']   = null;
						$update_array['HAVOtherResult'] = null;
						$update_array['HAVOtherPlace']  = null;
						$update_array['HAVOtherLabID']  = null;
						$update_array['HAVLabOther']    = null;
					}
					else
					{
						$update_array['LgmAntiHAV']     = 1;
						$update_array['HAVOther']       = 1;
						$update_array['HAVOtherName']   = $this->security->xss_clean($this->input->post('hav_other_test_name'));
						$update_array['HAVOtherDate']   = $this->security->xss_clean(timeStamp($this->input->post('hav_other_date')));
						$update_array['HAVOtherResult'] = $this->security->xss_clean($this->input->post('hav_other_result'));
						$update_array['HAVOtherPlace']  = $this->security->xss_clean($this->input->post('hav_other_place_of_test'));
						$update_array['HAVOtherLabID']  = $this->security->xss_clean($this->input->post('hav_other_lab_name'));
						$update_array['HAVLabOther']    = $this->security->xss_clean($this->input->post('hav_other_lab_name_other'));
						$update_array['MF2']            = 1;

						$test_dates[] = $this->security->xss_clean(timeStamp($this->input->post('hav_other_date')));

						if($this->security->xss_clean($this->input->post('hav_other_result')) == 1)
						{
							$viral_infection = true;
						}
					}
					
					if($this->security->xss_clean($this->input->post('hav_rapid')) == null && $this->security->xss_clean($this->input->post('hav_elisa')) == null && $this->security->xss_clean($this->input->post('hav_other')) == null)
					{
						$update_array['LgmAntiHAV']     = 0;
					}

				}

				if($this->security->xss_clean($this->input->post('check_hbs')) == null)
				{
					$update_array['HbsAg']       	= null;
					$update_array['VLHepB']       	= null;
					$update_array['HBSRapid']       = null;
					$update_array['HBSElisa']       = null;
					$update_array['HBSOther']       = null;
					$update_array['HBSRapidDate']   = null;
					$update_array['HBSRapidResult'] = null;
					$update_array['HBSRapidPlace']  = null;
					$update_array['HBSRapidLabID']  = null;
					$update_array['HBSElisaDate']   = null;
					$update_array['HBSElisaResult'] = null;
					$update_array['HBSElisaPlace']  = null;
					$update_array['HBSElisaLabID']  = null;
					$update_array['HBSOtherName']   = null;
					$update_array['HBSOtherDate']   = null;
					$update_array['HBSOtherResult'] = null;
					$update_array['HBSOtherPlace']  = null;
					$update_array['HBSOtherLabID']  = null;
				}
				else
				{




					if($this->security->xss_clean($this->input->post('hbs_rapid')) == null)
					{ 
						$update_array['HBSRapid']       = 0;
						$update_array['HBSRapidDate']   = null;
						$update_array['HBSRapidResult'] = null;
						$update_array['HBSRapidPlace']  = null;
						$update_array['HBSRapidLabID']  = null;
						$update_array['HBSRapidLabOther']  = null;
					}
					else
					{	
						$update_array['HbsAg']       	= 1;
						$update_array['VLHepB']       	= 1;
						$update_array['HBSRapid']       = 1;
						$update_array['HBSRapidDate']   = $this->security->xss_clean(timeStamp($this->input->post('hbs_rapid_date')));
						$update_array['HepB_RapidCollectionDate']   = $this->security->xss_clean(timeStamp($this->input->post('rap_Collection_date')));
						$update_array['HBSRapidResult'] = $this->security->xss_clean($this->input->post('hbs_rapid_result'));
						$update_array['HBSRapidPlace']  = $this->security->xss_clean($this->input->post('hbs_rapid_place_of_test'));
						$update_array['HBSRapidLabID']  = $this->security->xss_clean($this->input->post('hbs_rapid_lab_name'));
						$update_array['HBSRapidLabOther']  = $this->security->xss_clean($this->input->post('hbs_rapid_lab_name_other'));
						$update_array['HBSRapidLabOther']  = $this->security->xss_clean($this->input->post('hbs_rapid_lab_name_other'));
						$update_array['MF2']            = 1;
						$update_arrayhepb['MF2']            = 1;
						$update_arrayhepb['status']            = 3;

						$test_dates[] = $this->security->xss_clean(timeStamp($this->input->post('hbs_rapid_date')));

						if($this->security->xss_clean($this->input->post('hbs_rapid_result')) == 1)
						{
							$viral_infection = true;
						}
					}

					if($this->security->xss_clean($this->input->post('hbs_elisa')) == null)
					{
						$update_array['HBSElisa']       = 0;
						$update_array['HBSElisaDate']   = null;
						$update_array['HBSElisaResult'] = null;
						$update_array['HBSElisaPlace']  = null;
						$update_array['HBSElisaLabID']  = null;
						$update_array['HBSElisaLabOther']  = null;
					}
					else
					{
						$update_array['HbsAg']       	= 1;
						$update_array['VLHepB']       	= 1;
						$update_array['HBSElisa']       = 1;
						$update_array['HBSElisaDate']   = $this->security->xss_clean(timeStamp($this->input->post('hbs_elisa_date')));
						$update_array['HepB_ElisaCollectionDate']   = $this->security->xss_clean(timeStamp($this->input->post('elisa_Collection_date')));
						
						$update_array['HBSElisaResult'] = $this->security->xss_clean($this->input->post('hbs_elisa_result'));
						$update_array['HBSElisaPlace']  = $this->security->xss_clean($this->input->post('hbs_elisa_place_of_test'));
						$update_array['HBSElisaLabID']  = $this->security->xss_clean($this->input->post('hbs_elisa_lab_name'));
						$update_array['HBSElisaLabOther']  = $this->security->xss_clean($this->input->post('hbs_elisa_lab_name_other'));
						$update_array['MF2']            = 1;
						$update_arrayhepb['MF2']            = 1;
						$update_arrayhepb['status']            = 3;

						$test_dates[] = $this->security->xss_clean(timeStamp($this->input->post('hbs_elisa_date')));

						if($this->security->xss_clean($this->input->post('hbs_elisa_result')) == 1)
						{
							$viral_infection = true;
						}
					}

					if($this->security->xss_clean($this->input->post('hbs_other')) == null)
					{
						$update_array['HBSOther']       = 0;
						$update_array['HBSOtherName']   = null;
						$update_array['HBSOtherDate']   = null;
						$update_array['HBSOtherResult'] = null;
						$update_array['HBSOtherPlace']  = null;
						$update_array['HBSOtherLabID']  = null;
						$update_array['HBSLabOther']    = null;
					}
					else
					{
						$update_array['HbsAg']          = 1;
						$update_array['VLHepB']          = 1;
						$update_array['HBSOther']       = 1;
						$update_array['HBSOtherName']   = $this->security->xss_clean($this->input->post('hbs_other_test_name'));
						$update_array['HBSOtherDate']   = $this->security->xss_clean(timeStamp($this->input->post('hbs_other_date')));
						$update_array['HepB_OtherCollectionDate']   = $this->security->xss_clean(timeStamp($this->input->post('other_Collection_date')));
						$update_array['HBSOtherResult'] = $this->security->xss_clean($this->input->post('hbs_other_result'));
						$update_array['HBSOtherPlace']  = $this->security->xss_clean($this->input->post('hbs_other_place_of_test'));
						$update_array['HBSOtherLabID']  = $this->security->xss_clean($this->input->post('hbs_other_lab_name'));
						$update_array['HBSLabOther']    = $this->security->xss_clean($this->input->post('hbs_other_lab_name_other'));
						$update_array['MF2']            = 1;
						$update_arrayhepb['MF2']            = 1;
						$update_arrayhepb['status']            = 3;

						$test_dates[] = $this->security->xss_clean(timeStamp($this->input->post('hbs_other_date')));

						if($this->security->xss_clean($this->input->post('hbs_other_result')) == 1)
						{
							$viral_infection = true;
						}
					}
					
					if($this->security->xss_clean($this->input->post('hbs_rapid')) == null && $this->security->xss_clean($this->input->post('hbs_elisa')) == null && $this->security->xss_clean($this->input->post('hbs_other')) == null)
					{
						$update_array['HbsAg']     = 0;
						$update_array['VLHepB']     = 0;
					}

				}

				if($this->security->xss_clean($this->input->post('check_hbc')) == null)
				{
					$update_array['LgmAntiHBC']     = null;
					$update_array['HBCRapid']       = null;
					$update_array['HBCElisa']       = null;
					$update_array['HBCOther']       = null;
					$update_array['HBCRapidDate']   = null;
					$update_array['HBCRapidResult'] = null;
					$update_array['HBCRapidPlace']  = null;
					$update_array['HBCRapidLabID']  = null;
					$update_array['HBCElisaDate']   = null;
					$update_array['HBCElisaResult'] = null;
					$update_array['HBCElisaPlace']  = null;
					$update_array['HBCElisaLabID']  = null;
					$update_array['HBCOtherDate']   = null;
					$update_array['HBCOtherResult'] = null;
					$update_array['HBCOtherPlace']  = null;
					$update_array['HBCOtherLabID']  = null;
					$update_array['HBCLabOther']    = null;
				}
				else
				{
					if($this->security->xss_clean($this->input->post('hbc_rapid')) == null)
					{
						$update_array['HBCRapid']         = 0;
						$update_array['HBCRapidDate']   = null;
						$update_array['HBCRapidResult'] = null;
						$update_array['HBCRapidPlace']  = null;
						$update_array['HBCRapidLabID']  = null;
						$update_array['HBCRapidLabOther'] = null;
					}
					else
					{
						$update_array['LgmAntiHBC']       = 1;
						$update_array['HBCRapid']         = 1;
						$update_array['HBCRapidDate']     = $this->security->xss_clean($this->input->post('hbc_rapid_date'));
						$update_array['HBCRapidResult']   = $this->security->xss_clean($this->input->post('hbc_rapid_result'));
						$update_array['HBCRapidPlace']    = $this->security->xss_clean($this->input->post('hbc_rapid_place_of_test'));
						$update_array['HBCRapidLabID']    = $this->security->xss_clean($this->input->post('hbc_rapid_lab_name'));
						$update_array['HBCRapidLabOther'] = $this->security->xss_clean($this->input->post('hbc_rapid_lab_name_other'));
						$update_array['MF2']              = 1;

						$test_dates[] = $this->security->xss_clean($this->input->post('hbc_rapid_date'));

						if($this->security->xss_clean($this->input->post('hbc_rapid_result')) == 1)
						{
							$viral_infection = true;
						}
					}

					if($this->security->xss_clean($this->input->post('hbc_elisa')) == null)
					{
						$update_array['HBCElisa']         = 0;
						$update_array['HBCElisaDate']   = null;
						$update_array['HBCElisaResult'] = null;
						$update_array['HBCElisaPlace']  = null;
						$update_array['HBCElisaLabID']  = null;
						$update_array['HBCElisaLabOther'] = null;
					}
					else
					{
						$update_array['LgmAntiHBC']       = 1;
						$update_array['HBCElisa']         = 1;
						$update_array['HBCElisaDate']     = $this->security->xss_clean($this->input->post('hbc_elisa_date'));
						$update_array['HBCElisaResult']   = $this->security->xss_clean($this->input->post('hbc_elisa_result'));
						$update_array['HBCElisaPlace']    = $this->security->xss_clean($this->input->post('hbc_elisa_place_of_test'));
						$update_array['HBCElisaLabID']    = $this->security->xss_clean($this->input->post('hbc_elisa_lab_name'));
						$update_array['HBCElisaLabOther'] = $this->security->xss_clean($this->input->post('hbc_elisa_lab_name_other'));
						$update_array['MF2']              = 1;

						$test_dates[] = $this->security->xss_clean($this->input->post('hbc_elisa_date'));

						if($this->security->xss_clean($this->input->post('hbc_elisa_result')) == 1)
						{
							$viral_infection = true;
						}
					}

					if($this->security->xss_clean($this->input->post('hbc_other')) == null)
					{
						$update_array['HBCOther']       = 0;
						$update_array['HBCOtherDate']   = null;
						$update_array['HBCOtherResult'] = null;
						$update_array['HBCOtherPlace']  = null;
						$update_array['HBCOtherLabID']  = null;
						$update_array['HBCLabOther']    = null;
					}
					else
					{
						$update_array['LgmAntiHBC']     = 1;
						$update_array['HBCOther']       = 1;
						$update_array['HBCOtherName']   = $this->security->xss_clean($this->input->post('hbc_other_test_name'));
						$update_array['HBCOtherDate']   = $this->security->xss_clean($this->input->post('hbc_other_date'));
						$update_array['HBCOtherResult'] = $this->security->xss_clean($this->input->post('hbc_other_result'));
						$update_array['HBCOtherPlace']  = $this->security->xss_clean($this->input->post('hbc_other_place_of_test'));
						$update_array['HBCOtherLabID']  = $this->security->xss_clean($this->input->post('hbc_other_lab_name'));
						$update_array['HBCLabOther']    = $this->security->xss_clean($this->input->post('hbc_other_lab_name_other'));
						$update_array['MF2']            = 1;

						$test_dates[] = $this->security->xss_clean($this->input->post('hbc_other_date'));

						if($this->security->xss_clean($this->input->post('hbc_other_result')) == 1)
						{
							$viral_infection = true;
						}
					}
					
					if($this->security->xss_clean($this->input->post('hbc_rapid')) == null && $this->security->xss_clean($this->input->post('hbc_elisa')) == null && $this->security->xss_clean($this->input->post('hbc_other')) == null)
					{
						$update_array['LgmAntiHBC']     = 0;
					}

				}

				if($this->security->xss_clean($this->input->post('check_hcv')) == null)
				{
					$update_array['AntiHCV']     	= null;
					$update_array['HCVRapid']       = null;
					$update_array['HCVElisa']       = null;
					$update_array['HCVOther']       = null;
					$update_array['HCVRapidDate']   = null;
					$update_array['HCVRapidResult'] = null;
					$update_array['HCVRapidPlace']  = null;
					$update_array['HCVRapidLabID']  = null;
					$update_array['HCVElisaDate']   = null;
					$update_array['HCVElisaResult'] = null;
					$update_array['HCVElisaPlace']  = null;
					$update_array['HCVElisaLabID']  = null;
					$update_array['HCVOtherName']   = null;
					$update_array['HCVOtherDate']   = null;
					$update_array['HCVOtherResult'] = null;
					$update_array['HCVOtherPlace']  = null;
					$update_array['HCVOtherLabID']  = null;
				}
				else
				{
					if($this->security->xss_clean($this->input->post('hcv_rapid')) == null)
					{
						$update_array['HCVRapid']         = 0;
						$update_array['HCVRapidDate']   = null;
						$update_array['HCVRapidResult'] = null;
						$update_array['HCVRapidPlace']  = null;
						$update_array['HCVRapidLabID']  = null;
						$update_array['HCVRapidLabOther'] = null;
					}
					else
					{
						$update_array['AntiHCV']          = 1;
						$update_array['HCVRapid']         = 1;
						$update_array['HCVRapidDate']     = $this->security->xss_clean(timeStamp($this->input->post('hcv_rapid_date')));
						$update_array['HCVRapidResult']   = $this->security->xss_clean($this->input->post('hcv_rapid_result'));
						$update_array['HCVRapidPlace']    = $this->security->xss_clean($this->input->post('hcv_rapid_place_of_test'));
						$update_array['HCVRapidLabID']    = $this->security->xss_clean($this->input->post('hcv_rapid_lab_name'));
						$update_array['HCVRapidLabOther'] = $this->security->xss_clean($this->input->post('hcv_rapid_lab_name_other'));
						$update_array['MF2']              = 1;

						$test_dates[] = $this->security->xss_clean(timeStamp($this->input->post('hcv_rapid_date')));

						if($this->security->xss_clean($this->input->post('hcv_rapid_result')) == 1)
						{
							$viral_infection = true;
						}
					}

					if($this->security->xss_clean($this->input->post('hcv_elisa')) == null)
					{
						$update_array['HCVElisa']       = 0;
						$update_array['HCVElisaDate']   = null;
						$update_array['HCVElisaResult'] = null;
						$update_array['HCVElisaPlace']  = null;
						$update_array['HCVElisaLabID']  = null;
						$update_array['HCVElisaLabOther']  = null;
					}
					else
					{
						$update_array['AntiHCV']     	= 1;
						$update_array['HCVElisa']       = 1;
						$update_array['HCVElisaDate']   = $this->security->xss_clean(timeStamp($this->input->post('hcv_elisa_date')));
						$update_array['HCVElisaResult'] = $this->security->xss_clean($this->input->post('hcv_elisa_result'));
						$update_array['HCVElisaPlace']  = $this->security->xss_clean($this->input->post('hcv_elisa_place_of_test'));
						$update_array['HCVElisaLabID']  = $this->security->xss_clean($this->input->post('hcv_elisa_lab_name'));
						$update_array['HCVElisaLabOther']  = $this->security->xss_clean($this->input->post('hcv_elisa_lab_name_other'));
						$update_array['MF2']            = 1;

						$test_dates[] = $this->security->xss_clean(timeStamp($this->input->post('hcv_elisa_date')));

						if($this->security->xss_clean($this->input->post('hcv_elisa_result')) == 1)
						{
							$viral_infection = true;
						}
					}

					if($this->security->xss_clean($this->input->post('hcv_other')) == null)
					{
						$update_array['HCVOther']       = 0;
						$update_array['HCVOtherName']   = null;
						$update_array['HCVOtherDate']   = null;
						$update_array['HCVOtherResult'] = null;
						$update_array['HCVOtherPlace']  = null;
						$update_array['HCVOtherLabID']  = null;
					}
					else
					{
						$update_array['AntiHCV']        = 1;
						$update_array['HCVOther']       = 1;
						$update_array['HCVOtherName']   = $this->security->xss_clean($this->input->post('hcv_other_test_name'));
						$update_array['HCVOtherDate']   = $this->security->xss_clean(timeStamp($this->input->post('hcv_other_date')));
						$update_array['HCVOtherResult'] = $this->security->xss_clean($this->input->post('hcv_other_result'));
						$update_array['HCVOtherPlace']  = $this->security->xss_clean($this->input->post('hcv_other_place_of_test'));
						$update_array['HCVOtherLabID']  = $this->security->xss_clean($this->input->post('hcv_other_lab_name'));
						$update_array['HCVLabOther']    = $this->security->xss_clean($this->input->post('hcv_other_lab_name_other'));
						$update_array['MF2']            = 1;

						$test_dates[] = $this->security->xss_clean(timeStamp($this->input->post('hcv_other_date')));
						if($this->security->xss_clean($this->input->post('hcv_other_result')) == 1)
						{
							$viral_infection = true;
						}
					}
					
					if($this->security->xss_clean($this->input->post('hcv_rapid')) == null && $this->security->xss_clean($this->input->post('hcv_elisa')) == null && $this->security->xss_clean($this->input->post('hcv_other')) == null)
					{
						$update_array['AntiHCV']     = 0;
					}

				}

				if($this->security->xss_clean($this->input->post('check_hev')) == null)
				{
					$update_array['LgmAntiHEV']     = null;
					$update_array['HEVRapid']       = null;
					$update_array['HEVElisa']       = null;
					$update_array['HEVOther']       = null;
					$update_array['HEVRapidDate']   = null;
					$update_array['HEVRapidResult'] = null;
					$update_array['HEVRapidPlace']  = null;
					$update_array['HEVRapidLabID']  = null;
					$update_array['HEVElisaDate']   = null;
					$update_array['HEVElisaResult'] = null;
					$update_array['HEVElisaPlace']  = null;
					$update_array['HEVElisaLabID']  = null;
					$update_array['HEVOtherName']   = null;
					$update_array['HEVOtherDate']   = null;
					$update_array['HEVOtherResult'] = null;
					$update_array['HEVOtherPlace']  = null;
					$update_array['HEVOtherLabID']  = null;
				}
				else
				{
					if($this->security->xss_clean($this->input->post('hev_rapid')) == null)
					{
						$update_array['HEVRapid']         = 0;
						$update_array['HEVRapidDate']     = null;
						$update_array['HEVRapidResult']   = null;
						$update_array['HEVRapidPlace']    = null;
						$update_array['HEVRapidLabID']    = null;
						$update_array['HEVRapidLabOther'] = null;
					}
					else
					{
						$update_array['LgmAntiHEV']       = 1;
						$update_array['HEVRapid']         = 1;
						$update_array['HEVRapidDate']     = $this->security->xss_clean(timeStamp($this->input->post('hev_rapid_date')));
						$update_array['HEVRapidResult']   = $this->security->xss_clean($this->input->post('hev_rapid_result'));
						$update_array['HEVRapidPlace']    = $this->security->xss_clean($this->input->post('hev_rapid_place_of_test'));
						$update_array['HEVRapidLabID']    = $this->security->xss_clean($this->input->post('hev_rapid_lab_name'));
						$update_array['HEVRapidLabOther'] = $this->security->xss_clean($this->input->post('hev_rapid_lab_name_other'));
						$update_array['MF2']              = 1;
						if($this->input->post('Refer_FacilityHEV')==1){
							$update_array['Refer_FacilityHEV']   = $this->security->xss_clean($this->input->post('Refer_FacilityHEV'));
							$update_array['Refer_HigherFacilityHEV'] =0;
						} else{
							$update_array['Refer_HigherFacilityHEV']   = $this->security->xss_clean($this->input->post('Refer_FacilityHEV'));
							$update_array['Refer_FacilityHEV']=0;
						}

						$test_dates[] = $this->security->xss_clean(timeStamp($this->input->post('hev_rapid_date')));

						if($this->security->xss_clean($this->input->post('hev_rapid_result')) == 1)
						{
							$viral_infection = true;
						}
					}

					if($this->security->xss_clean($this->input->post('hev_elisa')) == null)
					{
						$update_array['HEVElisa']         = 0;
						$update_array['HEVElisaDate']     = null;
						$update_array['HEVElisaResult']   = null;
						$update_array['HEVElisaPlace']    = null;
						$update_array['HEVElisaLabID']    = null;
						$update_array['HEVElisaLabOther'] = null;
					}
					else
					{
						$update_array['LgmAntiHEV']       = 1;
						$update_array['HEVElisa']         = 1;
						$update_array['HEVElisaDate']     = $this->security->xss_clean(timeStamp($this->input->post('hev_elisa_date')));
						$update_array['HEVElisaResult']   = $this->security->xss_clean($this->input->post('hev_elisa_result'));
						$update_array['HEVElisaPlace']    = $this->security->xss_clean($this->input->post('hev_elisa_place_of_test'));
						$update_array['HEVElisaLabID']    = $this->security->xss_clean($this->input->post('hev_elisa_lab_name'));
						$update_array['HEVElisaLabOther'] = $this->security->xss_clean($this->input->post('hev_elisa_lab_name_other'));
						$update_array['MF2']              = 1;

						$test_dates[] = $this->security->xss_clean(timeStamp($this->input->post('hev_elisa_date')));

						if($this->security->xss_clean($this->input->post('hev_elisa_result')) == 1)
						{
							$viral_infection = true;
						}
					}

					if($this->security->xss_clean($this->input->post('hev_other')) == null)
					{
						$update_array['HEVOther']       = 0;
						$update_array['HEVOtherName']   = null;
						$update_array['HEVOtherDate']   = null;
						$update_array['HEVOtherResult'] = null;
						$update_array['HEVOtherPlace']  = null;
						$update_array['HEVOtherLabID']  = null;
						$update_array['HEVLabOther']    = null;
					}
					else
					{
						$update_array['LgmAntiHEV']     = 1;
						$update_array['HEVOther']       = 1;
						$update_array['HEVOtherName']   = $this->security->xss_clean($this->input->post('hev_other_test_name'));
						$update_array['HEVOtherDate']   = $this->security->xss_clean(timeStamp($this->input->post('hev_other_date')));
						$update_array['HEVOtherResult'] = $this->security->xss_clean($this->input->post('hev_other_result'));
						$update_array['HEVOtherPlace']  = $this->security->xss_clean($this->input->post('hev_other_place_of_test'));
						$update_array['HEVOtherLabID']  = $this->security->xss_clean($this->input->post('hev_other_lab_name'));
						$update_array['HEVLabOther']    = $this->security->xss_clean($this->input->post('hev_other_lab_name_other'));
						$update_array['MF2']            = 1;

						$test_dates[] = $this->security->xss_clean(timeStamp($this->input->post('hev_other_date')));

						if($this->security->xss_clean($this->input->post('hev_other_result')) == 1)
						{
							$viral_infection = true;
						}
					}

					if($this->security->xss_clean($this->input->post('hev_rapid')) == null && $this->security->xss_clean($this->input->post('hev_elisa')) == null && $this->security->xss_clean($this->input->post('hev_other')) == null)
					{
						$update_array['LgmAntiHEV']     = 0;
					}

				}

				if($this->security->xss_clean($this->input->post('check_hav')) == null && $this->security->xss_clean($this->input->post('check_hbs')) == null && $this->security->xss_clean($this->input->post('check_hbc')) == null && $this->security->xss_clean($this->input->post('check_hcv')) == null && $this->security->xss_clean($this->input->post('check_hev')) == null)
				{
					$update_array['MF2']        = 0;
					$update_array['LgmAntiHAV'] = 0;
					$update_array['HbsAg']      = 0;
					$update_array['VLHepB']      = 0;
					$update_array['LgmAntiHBC'] = 0;
					$update_array['AntiHCV']    = 0;
					$update_array['LgmAntiHEV'] = 0;
				}

				if($viral_infection)
				{
					$update_array['Status'] = 1;
				}
				else
				{
					$update_array['Status'] = 2;
				}

				if(count($test_dates) > 0)
				{
					$min_date = date('Y-m-d', strtotime($test_dates[0]));
					for($i = 1; $i < count($test_dates); $i++)
					{
						$next_date = date('Y-m-d', strtotime($test_dates[$i]));
						if($next_date < $min_date)
						{
							$min_date = $next_date;
						}
					}
				}

				$update_array['T_DLL_01_Date'] = $min_date;

				$this->db->where('PatientGUID', $patientguid);
				$this->db->update('tblpatient', $update_array);

				$this->db->where('PatientGUID', $patientguid);
				$this->db->update('hepb_tblpatient', $update_arrayhepb);


				$update_array['NextVisitPurpose'] = -1;
				$update_array['T_AntiHCV01_Result'] = 1;
				$update_array['screening_updated_on'] = date('Y-m-d');
				$update_array['screening_updated_by'] = $loginData->id_tblusers;
				

				$this->db->where('PatientGUID', $patientguid);
				$this->db->update('tblpatient', $update_array);

				$arrayresult = array(
					$update_array['HBSRapidResult'],
					$update_array['HBCRapidResult'],
					$update_array['HCVRapidResult'],
					//$update_array['HEVRapidResult'],
					//$update_array['HAVRapidResult'],
					$update_array['HBSElisaResult'],
					$update_array['HBCElisaResult'],
					$update_array['HCVElisaResult'],
					//$update_array['HEVElisaResult'],
					//$update_array['HAVElisaResult'],
					$update_array['HBSOtherResult'],
					$update_array['HBCOtherResult'],
					$update_array['HCVOtherResult'],
					//$update_array['HEVOtherResult'],
					//$update_array['HAVOtherResult']
				);
				
				$check_havval= $this->security->xss_clean($this->input->post('check_hav'));
				$check_hevval= $this->security->xss_clean($this->input->post('check_hev'));

				if(!in_array('1',$arrayresult))
				{
					$update_array['MF2']            = 0;
					$update_arrayhepb['MF2']            = 0;
					$update_arrayhepb['Status']            = 2;

					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('tblpatient', $update_array);

					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('hepb_tblpatient', $update_arrayhepb);

					redirect('patientinfo_hep_b?p=1');

				}
				if($this->input->post('interruption_status')==1){
					redirect('patientinfo_hep_b/patient_screening/'.$patientguid);

				}elseif($this->input->post('interruption_status')==null || $this->input->post('interruption_status')==0){

					$data = array(
						'InterruptReason' => null/*,
						'DeathReason' => null,
						'LFUReason' => null*/
					);

					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('hepb_tblpatient', $data); 
				}


				$hbv = array($this->security->xss_clean($this->input->post('check_hav')),
					$this->security->xss_clean($this->input->post('check_hbs')),
					$this->security->xss_clean($this->input->post('check_hbc')),
					$this->security->xss_clean($this->input->post('check_hcv')),
					$this->security->xss_clean($this->input->post('check_hev'))
				);
				// if(in_array('1',$hbv))
				// {
			 	 // echo count('1',$hbv);
				// print_r($update_array);
				// die();
				// }
				// else
				// 	{print_r($patientguid);}
				// 	die();

				//gau change viral load to testing
				redirect('patientinfo_hep_b/patient_testing/'.$patientguid);
			}

		}

		if($patientguid != null)
		{

			$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
			$patient_data = $this->db->query($sql,[$patientguid])->result();

			$sql = "SELECT * FROM `hepb_tblpatient` WHERE PatientGUID = ?";
			$patient_datahepb = $this->db->query($sql,[$patientguid])->result();

		}

		$sql = "select * from mstfacility where id_mstfacility = ".$loginData->id_mstfacility;
		$content['search_facilities'] = $this->db->query($sql)->result();

		$sql = "select * from mststate where id_mststate = ".$content['search_facilities'][0]->id_mststate;
		$content['search_states'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 48 and LanguageID = 1";
		$content['reason_death'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 49 and LanguageID = 1";
		$content['reason_flu'] = $this->db->query($sql)->result();

		$sql = "select p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID from tblpatient p left join (SELECT * FROM mstlookup where Flag =13 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
		$content['patient_status'] = $this->db->query($sql,[$patientguid])->result();

			$sql = "select  b.LookupValue as status, p.PatientGUID from hepb_tblpatient p left join (SELECT * FROM mstlookup where Flag =72 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
	  	$content['patient_statushepb'] = $this->db->query($sql,[$patientguid])->result();


		$sql ="SELECT * FROM mstlookup where  flag=32 order by Sequence ASC";
		$content['InterruptReason'] = $this->db->query($sql)->result();

		$sql ="SELECT * FROM mstlookup where  flag=19 and languageID=1 order by Sequence ASC";
		$content['unlockprocess'] = $this->db->query($sql)->result();


		$content['patient_data']   = $patient_data;
		$content['patient_datahepb']   = $patient_datahepb;
		$content['user_state']     = $content['search_facilities'][0]->id_mststate;
		$content['user_hospital']  = $loginData->id_mstfacility;

		$content['subview'] = 'hepb/patient_screening_hep_b';
		$this->load->view('pages/main_layout', $content);
	}

	public function patient_testing($patientguid = null)
	{
		$loginData = $this->session->userdata('loginData');

		$facility_sql = "select id_mstfacility, facility_short_name from mstfacility";
		$content['facility'] = $this->Common_Model->query_data($facility_sql);

		$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();
		
		if(!$this->allow_url($patientguid, __FUNCTION__))
		{
			$this->patient_redirect($patientguid);
		}
		

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST')
		{
			if(isset($_POST['save']))
			{
				// form_validation start
				$this->form_validation->set_rules('date_of_prescribing_tests', 'Date of Prescribing Tests', 'trim|required|xss_clean');
				$this->form_validation->set_rules('last_test_result_date', 'Date of Last Test Result ', 'trim|required|xss_clean');
				$this->form_validation->set_rules('haemoglobin', 'Haemoglobin', 'trim|required|xss_clean');
				$this->form_validation->set_rules('bilirubin', 'S.Bilirubin (Total)', 'trim|required|xss_clean');
				$this->form_validation->set_rules('albumin', 'S. Albumin', 'trim|required|xss_clean');
				$this->form_validation->set_rules('alt', 'ALT', 'trim|required|xss_clean');
				//$this->form_validation->set_rules('inr', 'PT INR', 'trim|required|xss_clean');
				$this->form_validation->set_rules('ast', 'AST', 'trim|required|xss_clean');
				$this->form_validation->set_rules('ast_uln', 'AST ULN (Upper Limit of Normal)', 'trim|required|xss_clean');
				$this->form_validation->set_rules('platelet_count', 'Platelet Count ', 'trim|required|xss_clean');
				//$this->form_validation->set_rules('weight', 'Weight (in Kgs)', 'trim|required|xss_clean');
				//$this->form_validation->set_rules('altlevels', 'ALT Levels', 'trim|required|xss_clean');
				$this->form_validation->set_rules('complicated_uncomplicated', 'Complicated/Uncomplicated', 'trim|required|xss_clean');
				if ($this->form_validation->run() == FALSE) {

					$arr['status'] = 'false';
					$this->session->set_flashdata("tr_msg",validation_errors());
				}
				else{
					if($this->security->xss_clean($this->input->post('complicated_uncomplicated')) == 1){
						$compensated_decompensated = 1;
						if($this->security->xss_clean($this->input->post('compensated_decompensated')) == 1){
							$Result =1;
						}
						elseif($this->security->xss_clean($this->input->post('compensated_decompensated')) == 2) {
							$Result =2;
						}
					}elseif($this->security->xss_clean($this->input->post('complicated_uncomplicated')) == 2){
						$compensated_decompensated = 2;
						$Result =3;
					}else{
						$Result =3;
					}
					/*	if($this->security->xss_clean($this->input->post('complicated_uncomplicated')) == 1){

					$compensated_decompensated = 1;
					$Result =1;

					}elseif($this->security->xss_clean($this->input->post('complicated_uncomplicated')) == 2){

						$compensated_decompensated = 2;
						$Result =2;
					}else{
						$compensated_decompensated = 3;
						$Result =3;
					}
					*/

					// child score

			        // if(patient!=null && patient.size()>0 && patient.get(0).getChildScore()>0){
			        //     Child_Score = patient.get(0).getChildScore();
			        // }else {
			        //     Child_Score = sCalculateChildScore();
			        // }

			        // if($this->security->xss_clean($this->input->post('child_score'))==5 || $this->security->xss_clean($this->input->post('child_score'))==6){
			        //     $childscore = "A";
			        // }else if($this->security->xss_clean($this->input->post('child_score'))>=7 && $this->security->xss_clean($this->input->post('child_score'))<=9){
			        //     $childscore = "B";
			        // }else if($this->security->xss_clean($this->input->post('child_score'))>=10 && $this->security->xss_clean($this->input->post('child_score'))<=15){
			        //     $childscore = "C";
			        // }
			    	// echo $this->security->xss_clean($this->input->post('child_score1'));
			    	// die;
		    		// child score
					if($this->security->xss_clean($this->input->post('weight'))==""){
						$weightval= NULL;
					}else{
						$weightval= $this->security->xss_clean($this->input->post('weight'));
					}

					if($this->security->xss_clean($this->input->post('egfr'))==""){
						$egfrval= NULL;
					}else{
						$egfrval= $this->security->xss_clean($this->input->post('egfr'));
					}

					if($this->security->xss_clean($this->input->post('altlevels')) == 1){
						$Statusdata = 7;
					}elseif($this->security->xss_clean($this->input->post('altlevels')) == 2){
						$Statusdata = 8;
					}elseif($this->security->xss_clean($this->input->post('altlevels')) == 3){
						$Statusdata = 9;
					}else{
						$Statusdata = 4;
					}
					if($this->security->xss_clean($this->input->post('altlevels'))==''){
						$altleveldata=null;
					}else{
						$altleveldata = $this->security->xss_clean($this->input->post('altlevels'));
					}

					$update_array = array(
						//"PrescribingDate"     => $this->security->xss_clean($this->input->post('date_of_prescribing_tests')),
						//"PrescribingDate"     => $this->security->xss_clean($this->input->post('last_test_result_date')),
						"V1_Haemoglobin"      => $this->security->xss_clean($this->input->post('haemoglobin')),
						"V1_Albumin"          => $this->security->xss_clean($this->input->post('albumin')),
						"V1_Bilrubin"         => $this->security->xss_clean($this->input->post('bilirubin')),
						"V1_INR"              => $this->security->xss_clean($this->input->post('inr')),
						"ALT"                 => $this->security->xss_clean($this->input->post('alt')),
						"AST"                 => $this->security->xss_clean($this->input->post('ast')),
						"AST_ULN"             => $this->security->xss_clean($this->input->post('ast_uln')),
						"V1_Platelets"        => $this->security->xss_clean($this->input->post('platelet_count')),
						"Weight"              => $weightval,
						"V1_Creatinine"       => $this->security->xss_clean($this->input->post('creatinine')),
						"V1_EGFR"             => $egfrval,
						"Elevated_ALT_Level"        => $altleveldata,
						"V1_Cirrhosis"        => $this->security->xss_clean($this->input->post('complicated_uncomplicated')),
						//"Cirr_TestDate"       => $this->security->xss_clean(timeStamp($this->input->post('cirrohsis_test_date'))),
						"Cirr_Encephalopathy" => $this->security->xss_clean($this->input->post('encephalopathy')),
						"Cirr_Ascites"        => $this->security->xss_clean($this->input->post('ascites')),
						"Cirr_VaricealBleed"  => $this->security->xss_clean($this->input->post('variceal_bleed')),
						"ChildScore"          => $this->security->xss_clean($this->input->post('child_score1')),
						"MF3"          		  => 1,
						"Status"          		  => $Statusdata,
						"baseline_updated_on" => date('Y-m-d'),
						"NextVisitPurpose"   => 0,
						"baseline_updated_by" => $loginData->id_tblusers,
						"special_updated_on" =>  date('Y-m-d'),
						"special_updated_by" =>  $loginData->id_tblusers,
						//"treatment_updated_on" => date('Y-m-d'),
						//"treatment_updated_by" => $loginData->id_tblusers,
						"CirrhosisStatus "	 => $compensated_decompensated,
						"Result" 			=>$Result,
						//"" =>$this->security->xss_clean($this->input->post('compensated_decompensated'))
					);
					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('hepb_tblpatient', $update_array);

					$insert_array['PatientGUID'] = $patientguid;

					/*Add Follow up visit*/
					$sql = "SELECT max(Visit_No) as visit_no FROM `hepb_followup` where PatientGUID = ?";
					$result = $this->db->query($sql,[$patientguid])->result();

					if(count($result) == 0)
					{
						$visit_no = 1;
					}
					else
					{
						$visit_no = $result[0]->visit_no + 1;
					}


					$sqlsic = "SELECT * FROM `hepb_followup` WHERE PatientGUID = ?";
					$patient_data1 = $this->db->query($sqlsic,[$patientguid])->result();

					


							$insert_arraytest     = array(
							"PatientGUID"		=> $patientguid,
							"V1_Haemoglobin"      => $this->security->xss_clean($this->input->post('haemoglobin')),
							"V1_Albumin"          => $this->security->xss_clean($this->input->post('albumin')),
							"V1_Bilrubin"         => $this->security->xss_clean($this->input->post('bilirubin')),
							"V1_INR"              => $this->security->xss_clean($this->input->post('inr')),
							"ALT"                 => $this->security->xss_clean($this->input->post('alt')),
							"AST"                 => $this->security->xss_clean($this->input->post('ast')),
							"AST_ULN"             => $this->security->xss_clean($this->input->post('ast_uln')),
							"V1_Platelets"        => $this->security->xss_clean($this->input->post('platelet_count')),
							"Weight"              => $weightval,
							"V1_Creatinine"       => $this->security->xss_clean($this->input->post('creatinine')),
							"V1_EGFR"             => $egfrval,
							"Elevated_ALT_Level"  => $altleveldata,
							"V1_Cirrhosis"        => $this->security->xss_clean($this->input->post('complicated_uncomplicated')),
							"Cirr_Encephalopathy" => $this->security->xss_clean($this->input->post('encephalopathy')),
							"Cirr_Ascites"        => $this->security->xss_clean($this->input->post('ascites')),
							"Cirr_VaricealBleed"  => $this->security->xss_clean($this->input->post('variceal_bleed')),
							"ChildScore"          => $this->security->xss_clean($this->input->post('child_score1')),
							"CreatedOn"           => date('Y-m-d'),
							"CreatedBy"           => $loginData->id_tblusers,
							"UpdatedOn"           =>  date('Y-m-d'),
							"UploadedBy"          =>  $loginData->id_tblusers,
							"CirrhosisStatus "    => $compensated_decompensated,
							"Result"              =>$Result,
							'Visit_No'			=> $visit_no
							);
					

					//$this->db->insert('tblpatientaddltest', $insert_arraytest);

							if(!empty($patient_data1) ){
						//$this->db->where('PatientGUID', $patientguid);
						//$this->db->update('hepb_followup', $insert_arraytest);
					}else{
						$this->db->insert('hepb_followup', $insert_arraytest);
					}

					

					/*End Visit*/

					if($this->security->xss_clean($this->input->post('ultrasound')) == null)
					{
						$insert_array['Clinical_US'] = null;
						$insert_array['Clinical_US_Dt'] = null;
					}
					else
					{
						$insert_array['Clinical_US']    = 1;
						$insert_array['Clinical_US_Dt'] = $this->security->xss_clean(timeStamp($this->input->post('ultrasound_date')));
					}

					if($this->security->xss_clean($this->input->post('fibroscan')) == null)
					{
						$insert_array['Fibroscan']     = null;
						$insert_array['Fibroscan_Dt']  = null;
						$insert_array['Fibroscan_LSM'] = null;
					}
					else
					{
						$insert_array['Fibroscan']     = 1;
						$insert_array['Fibroscan_Dt']  = $this->security->xss_clean(timeStamp($this->input->post('fibroscan_date')));
						$insert_array['Fibroscan_LSM'] = $this->security->xss_clean($this->input->post('fibroscan_lsm'));
					}

					if($this->security->xss_clean($this->input->post('apri')) == null)
					{
						$insert_array['APRI']       = null;
						$insert_array['APRI_Dt']    = null;
						$insert_array['APRI_Score'] = null;
					}
					else
					{
						$insert_array['APRI']       = 1;
						// $insert_array['APRI_Dt']    = $this->security->xss_clean($this->input->post('apri_date'));
						$insert_array['APRI_Score'] = $this->security->xss_clean($this->input->post('apri_score'));
					}

					if($this->security->xss_clean($this->input->post('fib4')) == null)
					{ 
						$insert_array['FIB4']       = null;
						$insert_array['fib4']   	= null;
						$insert_array['FIB4_FIB4']  = null;
					}
					else
					{ 
						$insert_array['FIB4'] = 1;
						// $insert_array['FIB4_Dt']     = $this->security->xss_clean($this->input->post('fib4_date'));
						$insert_array['FIB4_FIB4']   = $this->security->xss_clean($this->input->post('fib4_score'));
					}

					$insert_array['Prescribing_Dt']   = $this->security->xss_clean(timeStamp($this->input->post('date_of_prescribing_tests')));
					$insert_array['LastTest_Dt']   = $this->security->xss_clean(timeStamp($this->input->post('last_test_result_date')));

					$insert_array['APRI_PC']   = $this->security->xss_clean($this->input->post('platelet_count'));
					$insert_array['FIB4_PC']   = $this->security->xss_clean($this->input->post('platelet_count'));
					$insert_array['CreatedOn'] =  date('Y-m-d');
					$insert_array['CreatedBy'] = $loginData->id_tblusers;
					$insert_array['UpdatedBy'] = $loginData->id_tblusers;
					$insert_array['UpdatedOn'] =  date('Y-m-d');
					//$insert_array['UploadedOn'] =  date('Y-m-d');
					$insert_array['id_mstfacility'] =  $loginData->id_mstfacility;				

					$insert_array['PatientGUID'] = $patientguid;

					$sqlsic = "SELECT * FROM `tblpatientcirrohosis` WHERE PatientGUID = ?";
					$patient_data = $this->db->query($sqlsic,[$patientguid])->result();

					if(!empty($patient_data) ){
						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('tblpatientcirrohosis', $insert_array);
					}else{
						$this->db->insert('tblpatientcirrohosis', $insert_array);
					}

					/*add flooup*/

					if($this->security->xss_clean($this->input->post('ultrasound')) == null)
					{
						$insert_arrayf['Clinical_US'] = null;
						$insert_arrayf['Clinical_US_Dt'] = null;
					}
					else
					{
						$insert_arrayf['Clinical_US']    = 1;
						$insert_arrayf['Clinical_US_Dt'] = $this->security->xss_clean(timeStamp($this->input->post('ultrasound_date')));
					}

					if($this->security->xss_clean($this->input->post('fibroscan')) == null)
					{
						$insert_arrayf['Fibroscan']     = null;
						$insert_arrayf['Fibroscan_Dt']  = null;
						$insert_arrayf['Fibroscan_LSM'] = null;
					}
					else
					{
						$insert_arrayf['Fibroscan']     = 1;
						$insert_arrayf['Fibroscan_Dt']  = $this->security->xss_clean(timeStamp($this->input->post('fibroscan_date')));
						$insert_arrayf['Fibroscan_LSM'] = $this->security->xss_clean($this->input->post('fibroscan_lsm'));
					}

					if($this->security->xss_clean($this->input->post('apri')) == null)
					{
						$insert_arrayf['APRI']       = null;
						$insert_arrayf['APRI_Dt']    = null;
						$insert_arrayf['APRI_Score'] = null;
					}
					else
					{
						$insert_arrayf['APRI']       = 1;
						// $insert_array['APRI_Dt']    = $this->security->xss_clean($this->input->post('apri_date'));
						$insert_arrayf['APRI_Score'] = $this->security->xss_clean($this->input->post('apri_score'));
					}

					if($this->security->xss_clean($this->input->post('fib4')) == null)
					{ 
						$insert_arrayf['FIB4']       = null;
						$insert_arrayf['fib4']   	= null;
						$insert_arrayf['FIB4_FIB4']  = null;
					}
					else
					{ 
						$insert_arrayf['FIB4'] = 1;
						// $insert_array['FIB4_Dt']     = $this->security->xss_clean($this->input->post('fib4_date'));
						$insert_arrayf['FIB4_FIB4']   = $this->security->xss_clean($this->input->post('fib4_score'));
					}

					$insert_arrayf['Prescribing_Dt']   = $this->security->xss_clean(timeStamp($this->input->post('date_of_prescribing_tests')));
					$insert_arrayf['LastTest_Dt']   = $this->security->xss_clean(timeStamp($this->input->post('last_test_result_date')));

					$insert_arrayf['APRI_PC']   = $this->security->xss_clean($this->input->post('platelet_count'));
					$insert_arrayf['FIB4_PC']   = $this->security->xss_clean($this->input->post('platelet_count'));
					
					if(empty($patient_data1) ){
					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('hepb_followup', $insert_arrayf);
				}

					if($this->input->post('interruption_status')==1){

						redirect('patientinfo_hep_b/patient_testing/'.$patientguid);

					}elseif($this->input->post('interruption_status')==null || $this->input->post('interruption_status')==0){

						$data = array(
							'InterruptReason' => null/*,
							'DeathReason' => null,
							'LFUReason' => null*/
						);

						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('tblpatient', $data);
					}

					$query = "SELECT Elevated_ALT_Level FROM `hepb_tblpatient` WHERE PatientGUID = ?";
					$patient_ALT_Level = $this->db->query($query,[$patientguid])->row();

					if( ($patient_ALT_Level->Elevated_ALT_Level == 2 || $patient_ALT_Level->Elevated_ALT_Level == 3) && ($this->input->post('apri_score') <=3 || $this->input->post('fib4_score') <='3.25') ){
						
						redirect('patientinfo_hep_b/patient_viral_load/'.$patientguid);
					}elseif($patient_ALT_Level->Elevated_ALT_Level == 1){
						//gau chnaged known_history to viralload

						redirect('patientinfo_hep_b/patient_testing_followup_visit/'.$patientguid);
					}elseif(  ($this->input->post('apri_score') >=3 || $this->input->post('fib4_score') >='3.25' ) ){

						$insert_arraych['MF3'] = 1;
						$insert_arraych['MF4'] = 1;
						$insert_arraych['Status'] = 4;
						
						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('hepb_tblpatient', $insert_arraych);

						redirect('patientinfo_hep_b/known_history/'.$patientguid);
					}
				}
			} // from validation end
		}
		else if($patientguid != null)
		{
			$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
			$patient_data = $this->db->query($sql,[$patientguid])->result();

			$sql = "SELECT * FROM `hepb_tblpatient` WHERE PatientGUID = ?";
			$patient_datahepb = $this->db->query($sql,[$patientguid])->result();

			$sql = "SELECT * FROM `tblpatientcirrohosis` WHERE PatientGUID = ?";
			$patient_cirrohosis_data = $this->db->query($sql,[$patientguid])->result();

			$sql = "select p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID from tblpatient p left join (SELECT * FROM mstlookup where Flag =13 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
			$content['patient_status'] = $this->db->query($sql,[$patientguid])->result();
				$sql = "select  b.LookupValue as status, p.PatientGUID from hepb_tblpatient p left join (SELECT * FROM mstlookup where Flag =72 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
	  	$content['patient_statushepb'] = $this->db->query($sql,[$patientguid])->result();

		}
		$sql = "select * from mstfacility where id_mstfacility = ".$loginData->id_mstfacility;
		$content['search_facilities'] = $this->db->query($sql)->result();

		$sql = "select * from mststate where id_mststate = ".$content['search_facilities'][0]->id_mststate;
		$content['search_states'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 7 and LanguageID = 1";
		$content['options_ence_ascites'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 8 and LanguageID = 1";
		$content['options_variceal_bleed'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 48 and LanguageID = 1";
		$content['reason_death'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 49 and LanguageID = 1";
		$content['reason_flu'] = $this->db->query($sql)->result();

		$sql ="SELECT * FROM mstlookup where  flag=32 order by Sequence ASC";
		$content['InterruptReason'] = $this->db->query($sql)->result();

		$sql ="SELECT * FROM mstlookup where  flag=19 and languageID=1 order by Sequence ASC";
		$content['unlockprocess'] = $this->db->query($sql)->result();

		$content['patient_data']            = $patient_data;
		$content['patient_datahepb']        = $patient_datahepb;
		$content['patient_cirrohosis_data'] = $patient_cirrohosis_data;
		$content['user_state']              = $content['search_facilities'][0]->id_mststate;
		$content['user_hospital']           = $loginData->id_mstfacility;

		$content['subview'] = 'hepb/patient_testing_hep_b';
		$this->load->view('pages/main_layout', $content);
	}

	public function patient_viral_load($patientguid = null)
	{
		$loginData = $this->session->userdata('loginData');

		$facility_sql = "select id_mstfacility, facility_short_name from mstfacility";
		$content['facility'] = $this->Common_Model->query_data($facility_sql);

		$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();

		$sqld = "SELECT count(PatientGUID) as count FROM `tblpatientvlsamplelist` WHERE PatientGUID = ?";
		$tblpatientvlsamplelist = $this->db->query($sqld,[$patientguid])->result();	
 $visitno = $tblpatientvlsamplelist[0]->count+1;

		//pr($patient_data);
		$HbsAg = $patient_data[0]->HbsAg;		

		if(!$this->allow_url($patientguid, __FUNCTION__))
		{
			$this->patient_redirect($patientguid);
		}

		$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();
		
		$arrayresult = array(
			$patient_data[0]->HBSRapidResult,
			$patient_data[0]->HBCRapidResult,
			$patient_data[0]->HCVRapidResult,
			$patient_data[0]->HEVRapidResult,
			$patient_data[0]->HAVRapidResult,
			$patient_data[0]->HBSElisaResult,
			$patient_data[0]->HBCElisaResult,
			$patient_data[0]->HCVElisaResult,
			$patient_data[0]->HEVElisaResult,
			$patient_data[0]->HAVElisaResult,
			$patient_data[0]->HBSOtherResult,
			$patient_data[0]->HBCOtherResult,
			$patient_data[0]->HCVOtherResult,
			$patient_data[0]->HEVOtherResult,
			$patient_data[0]->HAVOtherResult
		);

		if(!in_array('1',$arrayresult))
		{
			redirect('patientinfo_hep_b/patient_register');
		}

		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if($RequestMethod == 'POST')
		{
			if(isset($_POST['save']))
			{
				// form_validation start
				
				$this->form_validation->set_rules('hepb_vl_sample_drawn_date', 'Sample Drawn On Date of HepB', 'trim|required|xss_clean');
				
				if ($this->form_validation->run() == FALSE) {					
					$arr['status'] = 'false';
					$this->session->set_flashdata("tr_msg",validation_errors());
				}else{
					$viral_infection = false;

					/*if($this->security->xss_clean($this->input->post('hepc_vl_result')) ==1){
						$hepc_vl_resultMF4 = 1;
					}else{
						$hepc_vl_resultMF4 =0;
					}*/
					if($this->security->xss_clean($this->input->post('hepc_vl'))==''){
						$hepc_vl = NULL;
					}else{
						$hepc_vl = $this->security->xss_clean($this->input->post('hepc_vl'));
					}
					
					
						$update_array['VLHepB'] = 1;
						$update_array['BVLSampleCollectionDate'] = $this->security->xss_clean(timeStamp($this->input->post('hepb_vl_sample_drawn_date')));
						$update_array['Treatment_Recommended']  = $this->security->xss_clean($this->input->post('Treatment_Recommended'));
						$update_array['IsBVLSampleStored']       = $this->security->xss_clean($this->input->post('hepb_is_sample_stored'));
						$update_array['BVLStorageTemp']          = $this->security->xss_clean($this->input->post('hepb_sample_storage_temp'));


						$update_array['BStorage_days_hrs']        = $this->security->xss_clean($this->input->post('hepb_sample_stored_duration_more_less_than_day'));
						$update_array['BStorageduration']        = $this->security->xss_clean($this->input->post('hepb_sample_stored_duration'));
						$update_array['IsBVLSampleTransported']  = $this->security->xss_clean($this->input->post('hepb_is_sample_transported'));
						$update_array['BVLTransportTemp']        = $this->security->xss_clean($this->input->post('hepb_sample_transport_temp'));
						$update_array['BVLTransportDate']        = $this->security->xss_clean(timeStamp($this->input->post('hepb_sample_transport_date')));
						$update_array['BVLLabID']                = $this->security->xss_clean($this->input->post('hepb_sample_transported_to'));
						$update_array['BVLReceiverName']         = $this->security->xss_clean($this->input->post('hepb_sample_received_by_name'));

						$update_array['BVLTransporterName'] 	= $this->security->xss_clean($this->input->post('hepb_sample_transported_to_name'));
						$update_array['BVLTransporterDesignation'] = $this->security->xss_clean($this->input->post('hepb_sample_transported_to_designation'));
						$update_array['BVLReceiverDesignation']  = $this->security->xss_clean($this->input->post('hepb_sample_received_by_designation'));
						$update_array['BVLSCRemarks']            = $this->security->xss_clean($this->input->post('hepb_sample_remarks'));
						$update_array['IsBSampleAccepted']       = $this->security->xss_clean($this->input->post('hepb_is_sample_accepted'));
						$update_array['T_DLL_01_BVLC_Date']      = $this->security->xss_clean(timeStamp($this->input->post('hepb_vl_result_date')));
						$update_array['T_DLL_01_BVLCount']       = $this->security->xss_clean($this->input->post('hepb_vl'));
						$update_array['T_DLL_01_BVLC_Result']    = $this->security->xss_clean($this->input->post('hepb_vl_result'));
						$update_array['BRejectionReason']        = $this->security->xss_clean($this->input->post('hepb_sample_reason_for_rejection'));
						$update_array['BVLResultRemarks']        = $this->security->xss_clean($this->input->post('hepb_vl_remarks'));
						$update_array['BVLRecieptDate']        = $this->security->xss_clean(timeStamp($this->input->post('hepb_sample_receipt_date')));
						$update_array['MF4'] 			         = 1;
						$update_array['Treatment_Recommended']   = $this->security->xss_clean(($this->input->post('Treatment_Recommended')));

							$update_arrayhepb['MF4'] 			         = 1;
							//$update_arrayhepb['Treatment_Recommended']   = $this->security->xss_clean(($this->input->post('Treatment_Recommended')));

					

					if($this->security->xss_clean($this->input->post('hepb_vl_result')) == 1)
					{
						$viral_infection = true;
					}

					if($viral_infection)
					{
						$update_arrayhepb['Status'] = 4;
					}
					else
					{
						$update_arrayhepb['Status'] = 4;
					}
					if($this->security->xss_clean($this->input->post('hepc_is_sample_accepted')) == 2){
						$update_arrayhepb['Status'] = 2;
					}

					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('tblpatient', $update_array);

					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('hepb_tblpatient', $update_arrayhepb);

					

					/*test*/

					
						$update_arraytest['BVLSampleCollectionDate'] = $this->security->xss_clean(timeStamp($this->input->post('hepb_vl_sample_drawn_date')));
						
						$update_arraytest['IsBVLSampleStored']       = $this->security->xss_clean($this->input->post('hepb_is_sample_stored'));
						$update_arraytest['BVLStorageTemp']          = $this->security->xss_clean($this->input->post('hepb_sample_storage_temp'));


						$update_arraytest['BStorage_days_hrs']        = $this->security->xss_clean($this->input->post('hepb_sample_stored_duration_more_less_than_day'));
						$update_arraytest['BStorageduration']        = $this->security->xss_clean($this->input->post('hepb_sample_stored_duration'));
						$update_arraytest['IsBVLSampleTransported']  = $this->security->xss_clean($this->input->post('hepb_is_sample_transported'));
						$update_arraytest['BVLTransportTemp']        = $this->security->xss_clean($this->input->post('hepb_sample_transport_temp'));
						$update_arraytest['BVLTransportDate']        = $this->security->xss_clean(timeStamp($this->input->post('hepb_sample_transport_date')));
						$update_arraytest['BVLLabID']                = $this->security->xss_clean($this->input->post('hepb_sample_transported_to'));
						$update_arraytest['BVLReceiverName']         = $this->security->xss_clean($this->input->post('hepb_sample_received_by_name'));

						$update_arraytest['BVLTransporterName'] 	= $this->security->xss_clean($this->input->post('hepb_sample_transported_to_name'));
						$update_arraytest['BVLTransporterDesignation'] = $this->security->xss_clean($this->input->post('hepb_sample_transported_to_designation'));
						$update_arraytest['BVLReceiverDesignation']  = $this->security->xss_clean($this->input->post('hepb_sample_received_by_designation'));
						$update_arraytest['BVLSCRemarks']            = $this->security->xss_clean($this->input->post('hepb_sample_remarks'));
						$update_arraytest['IsBSampleAccepted']       = $this->security->xss_clean($this->input->post('hepb_is_sample_accepted'));
						$update_arraytest['T_DLL_01_BVLC_Date']      = $this->security->xss_clean(timeStamp($this->input->post('hepb_vl_result_date')));
						$update_arraytest['T_DLL_01_BVLCount']       = $this->security->xss_clean($this->input->post('hepb_vl'));
						$update_arraytest['T_DLL_01_BVLC_Result']    = $this->security->xss_clean($this->input->post('hepb_vl_result'));
						$update_arraytest['BRejectionReason']        = $this->security->xss_clean($this->input->post('hepb_sample_reason_for_rejection'));
						$update_arraytest['BVLResultRemarks']        = $this->security->xss_clean($this->input->post('hepb_vl_remarks'));
						$update_arraytest['BVLRecieptDate']        = $this->security->xss_clean(timeStamp($this->input->post('hepb_sample_receipt_date')));

					$update_arraytest['Visit_No'] 			         = $visitno;
					$update_arraytest['PatientGUID'] = $patientguid;
					$this->db->insert('tblpatientvlsamplelist', $update_arraytest);


					if($this->security->xss_clean($this->input->post('hepb_vl_result')) == 2 && $this->security->xss_clean($this->input->post('Treatment_Recommended'))==2){

						$update_arrayhepb['MF4'] 			         = null;
						$update_arrayhepb['Status'] 			         = 10;
						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('hepb_tblpatient', $update_arrayhepb);
						redirect('patientinfo_hep_b?p=1');
					}elseif($this->security->xss_clean($this->input->post('hepb_vl_result')) == 2 && $this->security->xss_clean($this->input->post('Treatment_Recommended'))==1){
						redirect('patientinfo_hep_b/known_history/'.$patientguid);
					}

					/*if($this->security->xss_clean($this->input->post('hepc_vl_result'))== "" && $this->security->xss_clean($this->input->post('hepc_vl_result_date'))== ""){
						redirect('patientinfo_hep_b?p=1');
					}*/

					/*if($this->security->xss_clean($this->input->post('hepc_is_sample_accepted')) == 2){
						redirect('patientinfo_hep_b/patient_viral_load/'.$patientguid);
					}*/

					if($this->input->post('interruption_status')==1){

						redirect('patientinfo_hep_b/patient_viral_load/'.$patientguid);
					}elseif($this->input->post('interruption_status')==null || $this->input->post('interruption_status')==0){

						$data = array(
							'InterruptReason' => null/*,
							'DeathReason' => null,
							'LFUReason' => null*/
						);

						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('tblpatient', $data);
					}

					if($this->security->xss_clean($this->input->post('hepb_vl_result'))!="" && $this->security->xss_clean($this->input->post('Treatment_Recommended'))=='1'){
						//gau testing changed to known_history
						redirect('patientinfo_hep_b/known_history/'.$patientguid);
					}elseif($this->security->xss_clean($this->input->post('hepb_vl_result'))!="" && $this->security->xss_clean($this->input->post('Treatment_Recommended'))=='2'){

						$update_arrayhepb['MF4'] 			         = null;
						$update_arrayhepb['Status'] 			         = 10;
						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('hepb_tblpatient', $update_arrayhepb);

						redirect('patientinfo_hep_b/hbvdna_followUp_visit/'.$patientguid);
					}

					else{

						redirect('patientinfo_hep_b/patient_viral_load/'.$patientguid);
					}
				} // from validation end
			}

		}
		else if($patientguid != null)
		{
			$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
			$patient_data = $this->db->query($sql,[$patientguid])->result();

			$sql = "SELECT * FROM `hepb_tblpatient` WHERE PatientGUID = ?";
			$patient_datahepb = $this->db->query($sql,[$patientguid])->result();
			//pr($patient_datahepb);

		}
		$sql = "SELECT * FROM `tblpatientcirrohosis` WHERE PatientGUID = ?";
			$patient_cirrohosis_data = $this->db->query($sql,[$patientguid])->result();

		$sql = "select * from mstfacility where id_mstfacility = ".$loginData->id_mstfacility;
		$content['search_facilities'] = $this->db->query($sql)->result();

		$sql = "select * from mststate where id_mststate = ".$content['search_facilities'][0]->id_mststate;
		$content['search_states'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where Flag = 39 and LanguageID = 1";
		$content['sample_transported_to_options'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where Flag = 5 and LanguageID = 1";
		$content['results_options'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where Flag = 37 and LanguageID = 1";
		$content['designation_options'] = $this->db->query($sql)->result();

		$sql = "SELECT  designation_listId, Designation FROM `mst_designation_list` where id_mstfacility = ".$loginData->id_mstfacility." or id_mstfacility=0 union Select '99' as designation_listId,'Peer Support' as Designation order by designation_listId asc ";
		$content['designation_options_list'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where Flag = 45 and LanguageID = 1";
		$content['rejection_options'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 48 and LanguageID = 1";
		$content['reason_death'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 49 and LanguageID = 1";
		$content['reason_flu'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 57 and LanguageID = 1";
		$content['vl_temp'] = $this->db->query($sql)->result();

		$sql ="SELECT * FROM mstlookup where  flag=32 order by Sequence ASC";
		$content['InterruptReason'] = $this->db->query($sql)->result();

		$sql ="SELECT * FROM mstlookup where  flag=19 and languageID=1 order by Sequence ASC";
		$content['unlockprocess'] = $this->db->query($sql)->result();

		$sql = "select p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID from tblpatient p left join (SELECT * FROM mstlookup where Flag =13 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
		$content['patient_status'] = $this->db->query($sql,[$patientguid])->result();
			$sql = "select  b.LookupValue as status, p.PatientGUID from hepb_tblpatient p left join (SELECT * FROM mstlookup where Flag =72 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
	  	$content['patient_statushepb'] = $this->db->query($sql,[$patientguid])->result();


		$content['patient_data']   = $patient_data;
		$content['patient_datahepb']        = $patient_datahepb;
		$content['user_state']     = $content['search_facilities'][0]->id_mststate;
		$content['user_hospital']  = $loginData->id_mstfacility;
		$content['patient_cirrohosis_data']=$patient_cirrohosis_data;

		$content['subview'] = 'hepb/patient_viral_load_hep_b';
		$this->load->view('pages/main_layout', $content);
	}

	public function known_history($patientguid = null)
	{
		$loginData = $this->session->userdata('loginData');

		

		$facility_sql = "select id_mstfacility, facility_short_name from mstfacility";
		$content['facility'] = $this->Common_Model->query_data($facility_sql);

		$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();
		 $gender = $patient_data[0]->Gender;

		if(count($patient_data) > 0 && $patient_data[0]->Gender == 2 && $patient_data[0]->IsAgeMonths == 2 && ($patient_data[0]->Age > 10 && $patient_data[0]->Age <= 55)){ 

		$sql = "SELECT * FROM `mstlookup` where flag = 68 AND LanguageID = 1 ORDER BY Sequence";
		$content['ailments_list'] = $this->db->query($sql)->result();
	}else{
		$sql = "SELECT * FROM `mstlookup` where flag = 68 AND LanguageID = 1 and LookupCode!=11 ORDER BY Sequence";
		$content['ailments_list'] = $this->db->query($sql)->result();
	}

		/*check start mtc TC USER*/
		$sql = "select f.is_Mtc from tblusers as u inner join mstfacility f on u.id_mstfacility=f.id_mstfacility where f.id_mstfacility=".$loginData->id_mstfacility." and u.id_tblusers=".$loginData->id_tblusers." ";
		$content['fac_user'] = $this->db->query($sql)->result();


		if(!$this->allow_url($patientguid, __FUNCTION__))
		{
			$this->patient_redirect($patientguid);
		}

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST')
		{
			if(isset($_POST['save']))
			{
				$ailment_array = $this->security->xss_clean($this->input->post('ailment'));

				$this->db->where('PatientGUID', $patientguid);
				$this->db->delete('tblRiskProfile');

				foreach ($ailment_array as $key => $value) {

					$insert_array = array(
						"PatientGUID" => $patientguid,
						"RiskID"      => $value
					);

					$this->db->insert('tblRiskProfile', $insert_array);
				}

				if($this->security->xss_clean($this->input->post('compensated_decompensated'))!=""){
					$child_score =7;
				}elseif($this->security->xss_clean($this->input->post('ascites'))==2 && $this->security->xss_clean($this->input->post('compensated_decompensated'))!=""){
					$child_score =10;
				}else{
					$child_score =0;
				}
				
				$update_arrayhepb = array(
					"BreastFeeding"            => $this->security->xss_clean($this->input->post('breast_feeding_woman')),
					"ART_Regimen"              => $this->security->xss_clean($this->input->post('hiv_regimen')),
					"CKDStage"                 => $this->security->xss_clean($this->input->post('ckd_stage')),
					"Ribavirin"                => $this->security->xss_clean($this->input->post('reason_ribavarin')),
					//"PastTreatment"            => $this->security->xss_clean($this->input->post('treatment_experienced')),
					"PreviousTreatingHospital" => $this->security->xss_clean($this->input->post('treating_hospital')),
					"PastRegimen"              => $this->security->xss_clean($this->input->post('previous_regimen')),
					"PastRegimen_Other"        => $this->security->xss_clean($this->input->post('previous_regimen_other')),
					"PreviousDuration"         => $this->security->xss_clean($this->input->post('previous_duration')),
					"Pregnant"        			 => $this->security->xss_clean($this->input->post('is_pregnant')),
					"DeliveryDt"        		 => $this->security->xss_clean(timeStamp($this->input->post('edd'))),
					"HCVPastTreatment"          => $this->security->xss_clean($this->input->post('previous_status')),
					"NWeeksCompleted"          => $this->security->xss_clean($this->input->post('no_of_weeks')),
					"HCVPastOutcome"           => $this->security->xss_clean($this->input->post('past_treatment_outcome')),
					"IsReferal"                => $this->security->xss_clean($this->input->post('referred')),
					"ReferingDoctor"           => $this->security->xss_clean($this->input->post('referring_doctor')),
					"ReferingDoctorOther"           => $this->security->xss_clean($this->input->post('referring_doctor_other')),
					"ReferTo"                  => $this->security->xss_clean($this->input->post('referred_to')),
					"ReferalDate"              => $this->security->xss_clean(timeStamp($this->input->post('referred_date'))),
					"LastPillDate"             => $this->security->xss_clean(timeStamp($this->input->post('last_pill_taken_on'))),
					"LMP"             => $this->security->xss_clean(timeStamp($this->input->post('lmp'))),
					//"ChildScore"			=> $child_score,
					"HCVHistory"           =>2,
					"MF5"                      => 1,
					"SCRemarks"                      => $this->security->xss_clean($this->input->post('observations')),
				);

				$update_arrayhepb['MF5'] 			         = 1;

				//$this->db->where('PatientGUID', $patientguid);
				//$this->db->update('tblpatient', $update_array);

				$this->db->where('PatientGUID', $patientguid);
					$this->db->update('hepb_tblpatient', $update_arrayhepb);
				/*decompensated cirrhosis or HIV  STOP*/
				
				$Patientinfo= $patient_data[0]->Result;

				$ailment_array = $this->security->xss_clean($this->input->post('ailment'));
				$is_Mtc = $content['fac_user'][0]->is_Mtc;

				if((in_array('2',$ailment_array) || in_array('1',$ailment_array) || in_array('5',$ailment_array) || in_array('7',$ailment_array) || $this->input->post('patient_typecheck')==2) && $Patientinfo==2 && $is_Mtc!=1){	
					$update_ptype = array('CaseType' =>1);
					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('hepb_tblpatient', $update_ptype);
						//echo 'yyyyyyy';exit();
					//redirect('patientinfo/known_history/'.$patientguid);	
				}else{
					$update_ptype = array('CaseType' =>NULL);
					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('hepb_tblpatient', $update_ptype);
				}
				

				if($this->input->post('interruption_status')==1){


					redirect('patientinfo_hep_b/known_history/'.$patientguid);
				}elseif($this->input->post('interruption_status')==null || $this->input->post('interruption_status')==0){

					$data = array(
						'InterruptReason' => null/*,
						'DeathReason' => null,
						'LFUReason' => null*/
					);

					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('tblpatient', $data);
				}

				redirect('patientinfo_hep_b/patient_prescription/'.$patientguid);
			}

		}
		else if($patientguid != null)
		{

			$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
			$patient_data = $this->db->query($sql,[$patientguid])->result();

			$sql = "SELECT * FROM `hepb_tblpatient` WHERE PatientGUID = ?";
			$patient_datahepb = $this->db->query($sql,[$patientguid])->result();

			$sql = "SELECT * FROM `tblRiskProfile` WHERE PatientGUID = ?";
			$patient_tblRiskProfile_data = $this->db->query($sql,[$patientguid])->result();

			$sql = "SELECT * FROM `tblpatientaddltest` where PatientGUID = ?  limit 5";
			$patient_tblpatientaddltest_data = $this->db->query($sql,[$patientguid])->result();

			$sql = "select p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID from tblpatient p left join (SELECT * FROM mstlookup where Flag =13 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
			$content['patient_status'] = $this->db->query($sql,[$patientguid])->result();
				$sql = "select  b.LookupValue as status, p.PatientGUID from hepb_tblpatient p left join (SELECT * FROM mstlookup where Flag =72 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
	  	$content['patient_statushepb'] = $this->db->query($sql,[$patientguid])->result();


		}

		$sql = "select * from mstfacility where id_mstfacility = ".$loginData->id_mstfacility;
		$content['search_facilities'] = $this->db->query($sql)->result();

		$sql = "select * from mststate where id_mststate = ".$content['search_facilities'][0]->id_mststate;
		$content['search_states'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 42 and LanguageID = 1";
		$content['hiv_regimen'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 30 and LanguageID = 1";
		$content['renal_ckd'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 43 and LanguageID = 1";
		$content['ribavirin_consideration'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 28 and LanguageID = 1";
		$content['previous_duration'] = $this->db->query($sql)->result();

		$sql = "SELECT id_mstfacility,FacilityCode FROM mstfacility";
		$content['treating_hospital'] = $this->db->query($sql)->result();

		// live query
		// $facility_sql = "select id_mstfacility,FacilityCode from mstfacility union Select '999999' as id_mstfacility,'Others' as FacilityCode";
		// $content['treating_hospital'] = $this->Common_Model->query_data($facility_sql);
		// live query

		$sql = "SELECT * FROM `mstlookup` where flag = 30 and LanguageID = 1";
		$content['previous_status'] = $this->db->query($sql)->result();

		/*$sql = "SELECT id_mst_medical_specialists,name FROM mst_medical_specialists";
		$content['referring_doctor'] = $this->db->query($sql)->result();*/

		$sql = "SELECT * FROM `mst_medical_specialists` where id_mstfacility = ".$loginData->id_mstfacility;
		$content['referring_doctor'] = $this->db->query($sql)->result();


		$sql = "SELECT * FROM `mstlookup` where flag = 28 and LanguageID = 1";
		$content['weeks'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mst_past_regimen` where is_deleted = 0";
		$content['past_regimen'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM mstfacility where id_mststate = ".$content['search_facilities'][0]->id_mststate;
		$content['referred_to'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 48 and LanguageID = 1";
		$content['reason_death'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 49 and LanguageID = 1";
		$content['reason_flu'] = $this->db->query($sql)->result();

		$sql ="SELECT * FROM mstlookup where  flag=32 order by Sequence ASC";
		$content['InterruptReason'] = $this->db->query($sql)->result();

		$sql ="SELECT * FROM mstlookup where  flag=19 and languageID=1 order by Sequence ASC";
		$content['unlockprocess'] = $this->db->query($sql)->result();


		$content['patient_data']                = $patient_data;
		$content['patient_datahepb']                = $patient_datahepb;
		$content['patient_tblpatientaddltest_data'] = $patient_tblpatientaddltest_data;
		$content['patient_tblRiskProfile_data'] = $patient_tblRiskProfile_data;
		$content['user_state']                  = $content['search_facilities'][0]->id_mststate;
		$content['user_hospital']               = $loginData->id_mstfacility;

		$content['subview'] = 'hepb/patient_known_history_hep_b';
		$this->load->view('pages/main_layout', $content);
	}

	public function patient_prescription($patientguid = null)
	{
		$loginData = $this->session->userdata('loginData');

		$sql = "SELECT * FROM `mstlookup` where flag = 26";
		$content['ailments_list'] = $this->db->query($sql)->result();

		$this->db->where('flag',64);
		$content['pills_to_be_taken_list'] = $this->db->get('mstlookup')->result();

		/*$this->db->where('flag',65);
		$content['drug_list'] = $this->db->get('mstlookup')->result();*/

		

		$sql = "select * from mst_drug_strength_hepb where is_deleted = 0";
		$content['mst_drug_strength_hepb'] =$this->Common_Model->query_data($sql);

		$facility_sql = "select id_mstfacility, facility_short_name from mstfacility";
		$content['facility'] = $this->Common_Model->query_data($facility_sql);

		$sql = "SELECT * FROM `hepb_tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();

		if(!$this->allow_url($patientguid, __FUNCTION__))
		{
			$this->patient_redirect($patientguid);
		}

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST')
		{
			if(isset($_POST['save']))
			{
				/*if($this->security->xss_clean($this->input->post('regimen_prescribed'))!=""){					
					$this->db->where('patientguid', $patientguid);
					$this->db->delete('tblpatient_regimen_drug_data');
				}*/
				if($this->security->xss_clean($this->input->post('prescribing_doctor'))== 999){ 
					$otherdrname = $this->security->xss_clean($this->input->post('prescribing_doctor_other'));
				}else{
					$otherdrname = NULL;
				}				

				if($this->security->xss_clean($this->input->post('duration')) == 99){
					$duration = $this->security->xss_clean($this->input->post('durationother'));
				}else{
					$duration = $this->security->xss_clean($this->input->post('duration'));
				}
						$update_array            = array(
						"PrescribingFacility"    => $this->security->xss_clean($this->input->post('prescribing_facility')),
						"PrescribingDoctor"      => $this->security->xss_clean($this->input->post('prescribing_doctor')),
						"Drugs_Added"            => $this->security->xss_clean($this->input->post('Drug')),
						'Drug_Dosage'            => $this->security->xss_clean($this->input->post('Drugsdosages')),
						"OtherRegimenStrength"      => $this->security->xss_clean($this->input->post('OtherRegimenStrength')),
						"PillsToBeTaken"         => $this->security->xss_clean($this->input->post('PillsToBeTaken')),
						"PrescribingDoctorOther" => $otherdrname,
						"RegimenChangeReason"    => $this->security->xss_clean($this->input->post('reasonforchangereg')),
						"HCVHistory"             =>2,
						"PrescriptionPlace"      => 1,
						/*"T_DurationValue"      => $this->security->xss_clean($this->input->post('duration')),
						//"T_DurationValue"      => $duration,
						"T_DurationOther"        => $this->security->xss_clean($this->input->post('durationother')),
						"DurationReason"         => $this->security->xss_clean($this->input->post('reason')),*/
						"TreatmentUpdatedOn"     => date('Y-m-d'),
						"TreatmentUpdatedBy"     => $loginData->id_tblusers,
						"PrescribingDate"        => $this->security->xss_clean(timeStamp($this->input->post('prescribing_date'))),
						"DispensationPlace"      => $this->security->xss_clean($this->input->post('place_of_dispensation')),
						"MF6"                    => 1,
						"Status"                 => 5,
						);

				$this->db->where('PatientGUID', $patientguid);
				$this->db->update('hepb_tblpatient', $update_array);

				if($this->security->xss_clean($this->input->post('regimen_prescribed'))==1){
					$array=$this->input->post('sofosbuvir');
					foreach($array as $key => $value)  {  

						$sql1 = "SELECT * FROM `mst_drug_strength` where id_mst_drug_strength = '".$array[$key]."'";
						$content['mst_drug_Daclatasvir'] = $this->db->query($sql1)->result();

						$sql1 = "SELECT * FROM `mst_drug_strength` where id_mst_drug = 3";
						$content['mst_drug_Ribavrin'] = $this->db->query($sql1)->result(); 

						$sql1 = "SELECT * FROM `mst_drug_strength` where id_mst_drug = 5";
						$content['mst_drug_Velpatasvir'] = $this->db->query($sql1)->result(); 
						if(empty($value)) 
							unset($array[$key]); 
						if(!empty($value)) {

							$sql1 = "SELECT * FROM `mst_drug_strength` where id_mst_drug_strength = '".$array[$key]."'";

							$mst_drug_Daclatasvir = $this->db->query($sql1)->result();

							$update_arrayd12 = array(
								"patientguid"	 	 => $patientguid,
								"id_mstfacility"	  => $this->security->xss_clean($this->input->post('prescribing_facility')),
								"id_mst_drugs"        => $this->security->xss_clean($mst_drug_Daclatasvir[0]->id_mst_drug),
								"id_mst_drug_strength"=> $this->security->xss_clean($array[$key]),
								"visit_no"			=>0,
								"created_by"		  => $loginData->id_tblusers,
								"created_on"		  =>date('Y-m-d')
							);
							$sqlsic = "SELECT * FROM `tblpatient_regimen_drug_data` WHERE PatientGUID = ?";
							$patient_data = $this->db->query($sqlsic,[$patientguid])->result();

							$this->db->insert('tblpatient_regimen_drug_data', $update_arrayd12);
						}
					}
				}
				elseif($this->security->xss_clean($this->input->post('regimen_prescribed'))==2){					
					$array=$this->input->post('sofosbuvir');
					foreach($array as $key => $value)  {    

						if(empty($value)) 
							unset($array[$key]); 
						if(!empty($value)) {

							$sql1 = "SELECT * FROM `mst_drug_strength` where id_mst_drug_strength = '".$array[$key]."'";

							$mst_drug_Daclatasvir = $this->db->query($sql1)->result();


							$update_arrayd12 = array(
								"patientguid"	 	 => $patientguid,
								"id_mstfacility"	  => $this->security->xss_clean($this->input->post('prescribing_facility')),
								"id_mst_drugs"        => $this->security->xss_clean($mst_drug_Daclatasvir[0]->id_mst_drug),
								"id_mst_drug_strength"=> $this->security->xss_clean($array[$key]),
								"visit_no"			=>0,
								"created_by"		  => $loginData->id_tblusers,
								"created_on"		  =>date('Y-m-d')

							);
							$sqlsic = "SELECT * FROM `tblpatient_regimen_drug_data` WHERE PatientGUID = ?";
							$patient_data = $this->db->query($sqlsic,[$patientguid])->result();

							$this->db->insert('tblpatient_regimen_drug_data', $update_arrayd12);
						}
					}
				}elseif($this->security->xss_clean($this->input->post('regimen_prescribed'))==3){
					
					$array=$this->input->post('sofosbuvir');
					foreach($array as $key => $value)  {   
						if(empty($value)) 
							unset($array[$key]); 
						if(!empty($value)) {

							$sql1 = "SELECT * FROM `mst_drug_strength` where id_mst_drug_strength = '".$array[$key]."'";

							$mst_drug_Daclatasvir = $this->db->query($sql1)->result();

							$update_arrayd12 = array(
								"patientguid"	 	 => $patientguid,
								"id_mstfacility"	  => $this->security->xss_clean($this->input->post('prescribing_facility')),
								"id_mst_drugs"        => $this->security->xss_clean($mst_drug_Daclatasvir[0]->id_mst_drug),
								"id_mst_drug_strength"=> $this->security->xss_clean($array[$key]),
								"visit_no"			=>0,
								"created_by"		  => $loginData->id_tblusers,
								"created_on"		  =>date('Y-m-d')
							);
							$sqlsic = "SELECT * FROM `tblpatient_regimen_drug_data` WHERE PatientGUID = ?";
							$patient_data = $this->db->query($sqlsic,[$patientguid])->result();
							$this->db->insert('tblpatient_regimen_drug_data', $update_arrayd12);
						}
					} 	
				}

				if($this->input->post('interruption_status')==1){
					redirect('patientinfo_hep_b/patient_prescription/'.$patientguid);
				}elseif($this->input->post('interruption_status')==null || $this->input->post('interruption_status')==0){

					$data = array(
						'InterruptReason' => null/*,
						'DeathReason' => null,
						'LFUReason' => null*/
					);

					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('tblpatient', $data);
				}
				redirect('patientinfo_hep_b/patient_dispensation/'.$patientguid.'/1');
			}
		}
		else if($patientguid != null)
		{

			$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
			$patient_data = $this->db->query($sql,[$patientguid])->result();

			$sql = "SELECT p.*,d.NextVisit FROM hepb_tblpatient p  left join tblpatientdispensationb d on p.PatientGUID=d.PatientGUID WHERE p.PatientGUID = ?";
			$patient_datahepb = $this->db->query($sql,[$patientguid])->result();


			$sql = "select p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID from tblpatient p left join (SELECT * FROM mstlookup where Flag =13 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
			$content['patient_status'] = $this->db->query($sql,[$patientguid])->result();
				$sql = "select  b.LookupValue as status, p.PatientGUID from hepb_tblpatient p left join (SELECT * FROM mstlookup where Flag =72 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
	  	$content['patient_statushepb'] = $this->db->query($sql,[$patientguid])->result();


		}
		$sql = "select * from mstfacility where id_mstfacility = ".$loginData->id_mstfacility;
		$content['search_facilities'] = $this->db->query($sql)->result();

		$sql = "select * from mststate where id_mststate = ".$content['search_facilities'][0]->id_mststate;
		$content['search_states'] = $this->db->query($sql)->result();

		$sql = "select * from mstfacility where id_mststate = ".$content['search_facilities'][0]->id_mststate;
		$content['prescribing_facilities'] = $this->db->query($sql)->result();

		// $sql = "SELECT id_mst_medical_specialists,name FROM mst_medical_specialists";
		// live query
		$sql = "SELECT * FROM `mst_medical_specialists` where id_mstfacility = ".$loginData->id_mstfacility;
		// live query
		$content['prescribing_doctor'] = $this->db->query($sql)->result();

		//$sql = "SELECT * FROM `mstlookup` where flag = 10 and LanguageID = 1";
		//$content['regimen_prescribed'] = $this->db->query($sql)->result();

		/*check start mtc TC USER*/
		$sql = "select f.is_Mtc from tblusers as u inner join mstfacility f on u.id_mstfacility=f.id_mstfacility where f.id_mstfacility=".$loginData->id_mstfacility." and u.id_tblusers=".$loginData->id_tblusers." ";
		$content['fac_user'] = $this->db->query($sql)->result();
		
		
		$sql1 = "SELECT * FROM `tblRiskProfile` WHERE PatientGUID = ?";
		$content['patient_tblRiskProfile_data'] = $this->db->query($sql1,[$patientguid])->result();
		//pr($content['fac_user']);
		if($content['fac_user'][0]->is_Mtc==1){

			$sql = "select * from mst_regimen_hepb where is_deleted = 0 order by Sequence ASC";
			$content['mst_regimen_hepb'] =$this->Common_Model->query_data($sql);

			

		}else{

			$sql = "select * from mst_regimen_hepb where is_deleted = 0 AND  is_mtc=0 order by Sequence ASC";
			$content['mst_regimen_hepb'] =$this->Common_Model->query_data($sql);

			
		}		

		/*check end mtc TC USER*/

		$sql1 = "SELECT * FROM `mst_drug_strength` where id_mst_drug = 1";
		$content['mst_drug_strength'] = $this->db->query($sql1)->result();		

		$sql1 = "SELECT * FROM `mst_drug_strength` where id_mst_drug = 3";
		$content['mst_drug_Ribavrin'] = $this->db->query($sql1)->result(); 

		$sql1 = "SELECT * FROM `mst_drug_strength` where id_mst_drug = 5";
		$content['mst_drug_Velpatasvir'] = $this->db->query($sql1)->result(); 
		
		$sql = "SELECT * FROM `mstlookup` where flag = 14 and LanguageID = 1";
		$content['duration'] = $this->db->query($sql)->result();

		$sqlsic1 = "SELECT * FROM `tblpatient_regimen_drug_data` WHERE patientguid = ?";
		$content['regimen_drug_data'] = $this->db->query($sqlsic1,[$patientguid])->result();

		$sql = "SELECT * FROM `tblpatientcirrohosis` WHERE PatientGUID = ?";
		$patient_cirrohosis_data = $this->db->query($sql,[$patientguid])->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 48 and LanguageID = 1";
		$content['reason_death'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 49 and LanguageID = 1";
		$content['reason_flu'] = $this->db->query($sql)->result();

		$sql ="SELECT * FROM mstlookup where  flag=32 order by Sequence ASC";
		$content['InterruptReason'] = $this->db->query($sql)->result();
		//echo "<pre>";print_r($content['regimen_drug_data']);
		if($patient_datahepb[0]->ReferTo==''){
			$ReferToid = $loginData->id_mstfacility;
		}else{
			$ReferToid = $patient_datahepb[0]->ReferTo;
		}

		$sql = "select * from mstfacility where id_mstfacility = ".$ReferToid;
		$content['ReferToFaci'] = $this->db->query($sql)->result();

		$sql ="SELECT * FROM mstlookup where  flag=19 and languageID=1 order by Sequence ASC";
		$content['unlockprocess'] = $this->db->query($sql)->result();

		

		//$sql = "Select id_mstfacility,FacilityCode from mstfacility where id_mstfacility = ".$loginData->id_mstfacility." union Select '9999999' as id_mstfacility,'Both' as FacilityCode from mstfacility";
		$sql = "Select id_mstfacility,FacilityCode from mstfacility where id_mstfacility = ".$loginData->id_mstfacility."";
		$content['place_of_dispensation'] = $this->db->query($sql)->result();

		$content['patient_data']   = $patient_data;
		$content['patient_datahepb']   = $patient_datahepb;
		$content['user_state']     = $content['search_facilities'][0]->id_mststate;
		$content['user_hospital']  = $loginData->id_mstfacility;
		$content['patient_cirrohosis_data'] = $patient_cirrohosis_data;

		$content['subview'] = 'hepb/patient_prescription_hep_b';
		$this->load->view('pages/main_layout', $content);
	}

	public function patient_dispensation($patientguid = null, $visit = 1)
	{
		$loginData = $this->session->userdata('loginData');
		$sql = "SELECT * FROM `mstlookup` where flag = 26";
		$content['ailments_list'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 74";
		$content['pills_dispensed'] = $this->db->query($sql)->result();

		$this->db->where('flag',64);
		$content['pills_to_be_taken_list'] = $this->db->get('mstlookup')->result();

		$sql = "select * from mst_regimen_hepb where is_deleted = 0 order by id_mst_regimenhepb desc";
		$content['mst_regimen_hepb'] =$this->Common_Model->query_data($sql);

		$sql = "select * from mst_drug_strength_hepb where is_deleted = 0";
		$content['mst_drug_strength_hepb'] =$this->Common_Model->query_data($sql);

		$query = "SELECT a.*,b.LookupValue as PillsTakenValue,c.FirstName as patientName from tblpatientdispensationb a 
		left join mstlookup b ON a.PillsTaken = b.LookupCode AND b.Flag = 64 AND b.LanguageID = 1
		left join tblpatient c ON a.PatientGUID = c.PatientGUID
		where a.PatientGUID = ? ORDER BY a.VisitNo DESC";

		$content['tblpatientdispensationb_list'] = $this->db->query($query, [$patientguid])->result();

		$query = "SELECT Treatment_Dt from tblpatientdispensationb a where a.PatientGUID = ? ORDER BY PatientID ASC";
		$content['Treatment_Dt'] = $this->db->query($query, [$patientguid])->result();



		//pr($content['tblpatientdispensationb_list']);
		$query ="SELECT Max(VisitNo ) as maxvisit FROM `tblpatientdispensationb` WHERE `PatientGUID` = ?";
		$visit_no = $this->db->query($query, [$patientguid])->row();

		$visit_no = $visit_no->maxvisit;
		
		if($visit_no == null OR $visit_no == 0){			
			$visit_no = 1;
		}else{	
			$visit_no += 1;
		}

		$content['VisitNo'] = $visit_no;

		$query ="SELECT Max(VisitNo ) as maxvisit FROM `tblpatientdispensationb` WHERE `PatientGUID` = ?";
		$content['maxvisit'] = $this->db->query($query, [$patientguid])->row();

		/*$this->db->where('PatientGUID',$patientguid);
		$content['VisitNo'] = $this->db->get('tblpatientdispensationb')->result();*/

		$facility_sql = "select id_mstfacility, facility_short_name from mstfacility";
		$content['facility'] = $this->Common_Model->query_data($facility_sql);

		$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();

		$sql = "select T_DurationValue as duration,T_DurationOther from tblpatient where patientguid = ?";
		$result = $this->db->query($sql,[$patientguid])->result();

		if(!$this->allow_url($patientguid, __FUNCTION__, $visit))
		{
			$this->patient_redirect($patientguid);
		}

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST')
		{

			if(isset($_POST['save']))
			{
				//echo 'kkkmm'.$RequestMethod;exit();
				$this->form_validation->set_rules('VisitNo', 'Visit No', 'trim|required|xss_clean');				
				$this->form_validation->set_rules('Treatment_Dt', 'Treatment Date', 'trim|required|xss_clean');				
				$this->form_validation->set_rules('PillsTaken', 'Treatment Date', 'trim|required|xss_clean');	

				
				//$this->form_validation->set_rules('PillstoBeDispensed', 'Solution to be dispensed', 'trim|required|xss_clean');	
				$PillsTakenDaily = isset($_POST['PillsTakenDaily']) ? $_POST['PillsTakenDaily'] : NULL;
				if($PillsTakenDaily != NULL)
				{
					$this->form_validation->set_rules('PillsTakenDaily', 'Pills Taken Daily', 'trim|required|xss_clean');	
				}

				$PillsDispensed = isset($_POST['PillsDispensed']) ? $_POST['PillsDispensed'] : NULL;
				if($PillsDispensed != NULL)
				{
					$this->form_validation->set_rules('PillsDispensed', '', 'trim|required|xss_clean');	
				}

				$SolutionTakenDaily = isset($_POST['SolutionTakenDaily']) ? $_POST['SolutionTakenDaily'] : NULL;
				if($SolutionTakenDaily != NULL)
				{
					$this->form_validation->set_rules('SolutionTakenDaily', 'Solution Taken Daily', 'trim|required|xss_clean');	
				}

				$PillsNeededDaily = isset($_POST['PillsNeededDaily']) ? $_POST['PillsNeededDaily'] : NULL;
				if($PillsNeededDaily != NULL)
				{
					$this->form_validation->set_rules('PillsNeededDaily', 'Solution Needed Daily', 'trim|required|xss_clean');	
				}

				$PillstoBeDispensed = isset($_POST['PillstoBeDispensed']) ? $_POST['PillstoBeDispensed'] : NULL;
				if($PillstoBeDispensed != NULL)
				{
					$this->form_validation->set_rules('PillstoBeDispensed', 'Solution to be Dispensed', 'trim|required|xss_clean');	
				}				

				$this->form_validation->set_rules('NextVisit', 'Next Visit Date', 'trim|required|xss_clean');
				//echo 'kkkmm'.$RequestMethod;exit();
				if ($this->form_validation->run() == FALSE) {					
					$arr['status'] = 'false';
					$this->session->set_flashdata("tr_msg",validation_errors());
				}else{


					if($this->security->xss_clean($this->input->post('side_effects'))==''){
						$side_effects = null;
					}else{
						$side_effects = implode(',', $this->security->xss_clean($this->input->post('side_effects')));
						//echo $side_effects;
					}

					$insert_array = array(
						"PatientGUID"          => $patientguid,
						"PatientVisitGUID"     => NULL,
						"Treatment_Dt"         => $this->security->xss_clean(timeStamp($this->input->post('Treatment_Dt'))),
						"RegimenPrescribed"    => $this->security->xss_clean($this->input->post('RegimenPrescribed')),
						"DispensationPlace"    => $this->security->xss_clean($this->input->post('DispensationPlace')),
						"PillsDaysDispensed"   => $this->security->xss_clean($this->input->post('PillsDaysDispensed')),
						"PillsTaken"           => $this->security->xss_clean($this->input->post('PillsTaken')),
						"PillsTakenDaily"      => $this->security->xss_clean($this->input->post('PillsTakenDaily')),
						"PillsDispensed"       => $this->security->xss_clean($this->input->post('PillsDispensed')),
						"SolutionTakenDaily"   => $this->security->xss_clean($this->input->post('SolutionTakenDaily')),
						"PillsNeededDaily"     => $this->security->xss_clean($this->input->post('PillsNeededDaily')),
						"PillstoBeDispensed"   => $this->security->xss_clean($this->input->post('PillstoBeDispensed')),
						"SideEffectValue"      => $side_effects,
						"SideEffectValueOther" => $this->security->xss_clean($this->input->post('side_effect_other')),
						"NextVisit"            => $this->security->xss_clean(timeStamp($this->input->post('NextVisit'))),
						'Adherence'            => $this->security->xss_clean($this->input->post('adherence')),
						"PIllsLeft"            =>$this->security->xss_clean($this->input->post('pills_left')),
						"AdherenceReason"      =>$this->security->xss_clean($this->input->post('reason_low_adherence')),
						"VisitNo"              => $this->security->xss_clean($this->input->post('VisitNo'))
					);
					 //pr($insert_array); exit();
					$PatientID = $this->security->xss_clean($this->input->post('PatientID'));
					$VisitNo = $this->security->xss_clean($this->input->post('VisitNo'));

					$query = "Select * FROM tblpatientdispensationb where PatientGUID = ? AND VisitNo = ?";
					$data = $this->db->query($query, [$patientguid, $VisitNo])->result();
					
					if (count($data)==0) {
					$this->db->insert('tblpatientdispensationb', $insert_array);	
					$update_arrayhepb['Status']            = 6;
							$this->db->where('PatientGUID', $patientguid);
							$this->db->update('hepb_tblpatient', $update_arrayhepb);					
					}
					else{ 
							$this->db->where('PatientID', $PatientID);
							$this->db->update('tblpatientdispensationb', $insert_array);

							$update_arrayhepb['Status']            = 6;
							$this->db->where('PatientGUID', $patientguid);
							$this->db->update('hepb_tblpatient', $update_arrayhepb);
					}


					
				}
				if($this->input->post('interruption_status')==1){

					redirect('patientinfo_hep_b/patient_dispensation/'.$patientguid.'/'.($visit));
				}elseif($this->input->post('interruption_status')==null || $this->input->post('interruption_status')==0){

					$data = array(
						'InterruptReason' => null/*,
						'DeathReason' => null,
						'LFUReason' => null,
						'InterruptToStage' => null*/
					);

					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('tblpatient', $data);
				}

				redirect('patientinfo_hep_b/patient_dispensation/'.$patientguid.'/'.($visit+1));
				
			}

		}
		else if($patientguid != null)
		{
			$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
			$patient_data = $this->db->query($sql,[$patientguid])->result();

			$sql = "SELECT * FROM `hepb_tblpatient` WHERE PatientGUID = ?";
			$patient_datahepb = $this->db->query($sql,[$patientguid])->result();

			$sql = "select p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID from tblpatient p left join (SELECT * FROM mstlookup where Flag =13 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
			$content['patient_status'] = $this->db->query($sql,[$patientguid])->result();
				$sql = "select  b.LookupValue as status, p.PatientGUID from hepb_tblpatient p left join (SELECT * FROM mstlookup where Flag =72 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
	  	$content['patient_statushepb'] = $this->db->query($sql,[$patientguid])->result();


		}


		$sql = "SELECT * FROM `tblpatientvisit` WHERE PatientGUID = ? and VisitNo=? order by id_tblpatientvisit desc limit 1";
		$content['visit_details'] = $this->db->query($sql,[$patientguid,$visit])->result();

		$sql = "SELECT * FROM `tblpatientvisit` WHERE PatientGUID = ? ";
		$content['visit_count'] = $this->db->query($sql,[$patientguid])->result();

		$sql = "SELECT * FROM `tblpatientvisit` WHERE PatientGUID = ? and VisitNo=? order by id_tblpatientvisit desc limit 1";
		$content['visit_detailsdate'] = $this->db->query($sql,[$patientguid,($visit-1)])->result();

		$sql = "SELECT * FROM `tblpatientvisit` WHERE PatientGUID = ?  order by id_tblpatientvisit desc limit 1";
		$content['visit_detailseto'] = $this->db->query($sql,[$patientguid])->result();

		//echo "<pre>";print_r($content['visit_detailseto']);

		$sql = "select * from mstfacility where id_mstfacility = ".$loginData->id_mstfacility;
		$content['search_facilities'] = $this->db->query($sql)->result();

		$sql = "select * from mststate where id_mststate = ".$content['search_facilities'][0]->id_mststate;
		$content['search_states'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 10 and LanguageID = 1";
		$content['regimen_prescribed'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 14 and LanguageID = 1";
		$content['duration'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 44 and LanguageID = 1 order by Sequence ASC";
		$content['side_effects'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 46 and LanguageID = 1";
		$content['low_adherence'] = $this->db->query($sql)->result();

		$sql1 = "SELECT * FROM `mst_drug_strength` where id_mst_drug = 1";
		$content['mst_drug_strength'] = $this->db->query($sql1)->result();

		$sql1 = "SELECT * FROM `mst_drug_strength` where id_mst_drug = 4";
		$content['mst_drug_Daclatasvir'] = $this->db->query($sql1)->result();

		$sql1 = "SELECT * FROM `mst_drug_strength` where id_mst_drug = 3";
		$content['mst_drug_Ribavrin'] = $this->db->query($sql1)->result(); 

		$sql1 = "SELECT * FROM `mst_drug_strength` where id_mst_drug = 5";
		$content['mst_drug_Velpatasvir'] = $this->db->query($sql1)->result(); 

		$sqlsic1 = "SELECT * FROM `tblpatient_regimen_drug_data` WHERE patientguid = ?";
		$content['regimen_drug_data'] = $this->db->query($sqlsic1,[$patientguid])->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 48 and LanguageID = 1";
		$content['reason_death'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 49 and LanguageID = 1";
		$content['reason_flu'] = $this->db->query($sql)->result();

		$sql ="SELECT * FROM mstlookup where  flag=32 order by Sequence ASC";
		$content['InterruptReason'] = $this->db->query($sql)->result();

		$sql ="SELECT * FROM mstlookup where  flag=19 and languageID=1 order by Sequence ASC";
		$content['unlockprocess'] = $this->db->query($sql)->result();

		// live query
		$sql = "SELECT * FROM `mst_medical_specialists` where id_mstfacility = ".$loginData->id_mstfacility;
		// live query

		// $sql = "SELECT id_mst_medical_specialists,name FROM mst_medical_specialists";
		$content['doctors'] = $this->db->query($sql)->result();

		if($patient_datahepb[0]->ReferTo==''){
			$ReferToid = $loginData->id_mstfacility;
		}else{
			$ReferToid = $patient_datahepb[0]->ReferTo;
		}

		$sql = "select * from mstfacility where id_mstfacility = ".$ReferToid;
		$content['ReferToFaci'] = $this->db->query($sql)->result();

		$sql = "Select id_mstfacility,FacilityCode from mstfacility where id_mstfacility = ".$loginData->id_mstfacility;
		$content['place_of_dispensation'] = $this->db->query($sql)->result();

		$content['patient_data']   = $patient_data;
		$content['patient_datahepb']   = $patient_datahepb;
		$content['user_state']     = $content['search_facilities'][0]->id_mststate;
		$content['user_hospital']  = $loginData->id_mstfacility;

		$content['subview'] = 'hepb/patient_dispensation_visits_hep_b';
		
		$this->load->view('pages/main_layout', $content);
	}	

	public function patient_prescription_followup_visit($patientguid = null)
	{
		$loginData = $this->session->userdata('loginData');

		$facility_sql = "select id_mstfacility, facility_short_name from mstfacility";
		$content['facility'] = $this->Common_Model->query_data($facility_sql);

		$sql = "SELECT * FROM `tblpatient`";
		$patient_list = $this->db->query($sql)->result();

		$patient_data   = array();
		$search_uid_num = '';

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST')
		{
			if(isset($_POST['save']))
			{

				$sql = "SELECT max(VisitNo) as visit_no FROM `tblpatientfu` where PatientGUID = ?";
				$result = $this->db->query($sql,[$patientguid])->result();

				if(count($result) == 0)
				{
					$visit_no = 1;
				}
				else
				{
					$visit_no = $result[0]->visit_no + 1;
				}

				$insert_array = array(
					"PatientGUID"         => $patientguid,
					"PatientVisitFUGUID"  => uniqid(),
					"VisitNo"             => $visit_no,
					"VisitDate"           => $this->security->xss_clean(timeStamp($this->input->post('visit_date'))),
					"TreatDoctor"         => $this->security->xss_clean($this->input->post('treating_doctor')),
					"IsReferred"          => $this->security->xss_clean($this->input->post('referral')),
					"RefDoctor"           => $this->security->xss_clean($this->input->post('referring_doctor')),
					"RefDoctorOther"      => $this->security->xss_clean($this->input->post('referring_doctor_other')),
					"ReferredTo"          => $this->security->xss_clean($this->input->post('referring_to')),
					"SideEffect"          => $this->security->xss_clean($this->input->post('side_effect')),
					"SideEffectOther"     => $this->security->xss_clean($this->input->post('side_effect_other')),
					"Remarks"             => $this->security->xss_clean($this->input->post('remarks')),
					"CreatedBy"           => $loginData->id_tblusers,
					"CreatedOn"           => date('Y-m-d'),
					"TreatingDoctorOther" => $this->security->xss_clean($this->input->post('treating_doctor_other'))
				);

				$this->db->insert('tblpatientfu', $insert_array);

				redirect('patientinfo_hep_b/patient_prescription_followup_visit/'.$patientguid);
			}

		}
		else if($patientguid != null)
		{

			$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
			$patient_data = $this->db->query($sql,[$patientguid])->result();

			$sql = "SELECT * FROM `hepb_tblpatient` WHERE PatientGUID = ?";
			$patient_datahepb = $this->db->query($sql,[$patientguid])->result();

			$sql = "SELECT * FROM `tblpatientfu` WHERE PatientGUID = ?";
			$patient_tblpatientfu_data = $this->db->query($sql,[$patientguid])->result();

			$sql_patient_districts = "SELECT * FROM `mstdistrict` where id_mststate = ".$patient_data[0]->State;
			$patient_districts = $this->db->query($sql_patient_districts)->result();

			$sql_patient_blocks = "SELECT * FROM `mstblock` where id_mstdistrict = ".$patient_data[0]->District;
			$patient_blocks = $this->db->query($sql_patient_blocks)->result();

			$search_uid_num  = $patient_data[0]->UID_Num;

			$sql = "select p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID from tblpatient p left join (SELECT * FROM mstlookup where Flag =13 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
			$content['patient_status'] = $this->db->query($sql,[$patientguid])->result();
				$sql = "select  b.LookupValue as status, p.PatientGUID from hepb_tblpatient p left join (SELECT * FROM mstlookup where Flag =72 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
	  	$content['patient_statushepb'] = $this->db->query($sql,[$patientguid])->result();


		}
		$sql = "select * from mstfacility where id_mstfacility = ".$loginData->id_mstfacility;
		$content['search_facilities'] = $this->db->query($sql)->result();

		$sql = "select * from mststate where id_mststate = ".$content['search_facilities'][0]->id_mststate;
		$content['search_states'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mst_medical_specialists` where id_mstfacility = ".$loginData->id_mstfacility;
		$content['prescribing_doctor'] = $this->db->query($sql)->result();

		$sqlss = "SELECT * FROM `mstlookup` where Flag = '44'";
		$content['side_effects'] = $this->db->query($sqlss)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 48 and LanguageID = 1";
		$content['reason_death'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 49 and LanguageID = 1";
		$content['reason_flu'] = $this->db->query($sql)->result();
		$sql ="SELECT * FROM mstlookup where  flag=19 and languageID=1 order by Sequence ASC";
		$content['unlockprocess'] = $this->db->query($sql)->result();



		$content['patient_data']              = $patient_data;
		$content['patient_datahepb']                = $patient_datahepb;
		$content['patient_tblpatientfu_data'] = $patient_tblpatientfu_data;
		$content['patient_list']              = $patient_list;
		$content['user_state']                = $content['search_facilities'][0]->id_mststate;
		$content['user_hospital']             = $loginData->id_mstfacility;
		$content['search_uid_num']            = $search_uid_num;

		$content['subview'] = 'hepb/patient_prescription_followup_visit_hep_b';
		$this->load->view('pages/main_layout', $content);
	}

	public function getDistricts($id_mststate = null)
	{
		$sql = "select * from mstdistrict where id_mststate = ? order by DistrictName";
		$districts = $this->db->query($sql,[$id_mststate])->result();

		$options = "<option value=''>Select District</option>";
		foreach ($districts as $district) {
			$options .= "<option value='".$district->id_mstdistrict."'>".$district->DistrictName."</option>";
		}

		echo $options;
	}

	public function getBlocks($id_mstdistrict = null)
	{
		$sql = "select * from mstblock where id_mstdistrict = ? order by BlockName";
		$blocks = $this->db->query($sql,[$id_mstdistrict])->result();

		$options = "<option value=''>Select Block</option>";
		foreach ($blocks as $block) {
			$options .= "<option value='".$block->id_mstblock."'>".$block->BlockName."</option>";
		}

		echo $options;
	}

	private function getPrefix()
	{
		$loginData = $this->session->userdata('loginData');

		$sql_facility_data = "SELECT * FROM `mstfacility` where id_mstfacility = ".$loginData->id_mstfacility;
		$facility_data = $this->db->query($sql_facility_data)->result();

		$sql_state_data = "SELECT * FROM `mststate` where id_mststate = ".$facility_data[0]->id_mststate;
		$state_data = $this->db->query($sql_state_data)->result();

		$sql_uid_num_data = "SELECT max(UID_Num) as max_uid FROM tblpatient WHERE id_mstfacility = ".$loginData->id_mstfacility;
		$uid_num_data = $this->db->query($sql_uid_num_data)->result();
		$FacilityCodeval = $facility_data[0]->FacilityCode;
		$FacilityCodevalexp= explode('-', $FacilityCodeval);
		$faccodedata= $FacilityCodevalexp[0].'-'.$FacilityCodevalexp[1];

		 //$uid_prefix = $state_data[0]->StateCd.'-'.$facility_data[0]->FacilityCode.'-'.$facility_data[0]->FacilityNumber.'-'.date('y');

		 $FacilityNumber = str_pad($facility_data[0]->FacilityNumber, 2, '0', STR_PAD_LEFT);

		$uid_prefix = $state_data[0]->StateCd.'-'.$faccodedata.'-'.$FacilityNumber.'-'.date('y');

		return $uid_prefix;
	}

	private function allow_url($patient_guid = null, $page = null, $visit = null)
	{
		if($patient_guid == null)
		{
			redirect('patientinfo_hep_b?p=1');
		}
		else
		{
			$sql = "SELECT * FROM `hepb_tblpatient` WHERE PatientGUID = ?";
			$patient_data = $this->db->query($sql,[$patient_guid])->result();
			//pr($patient_data[0]);die();			

			$sql = "SELECT ifnull(max(VisitNo), 1) as max_visit_no FROM `tblpatientvisit` WHERE PatientGUID = ?";
			$patient_visit_data = $this->db->query($sql,[$patient_guid])->result();

			switch($page)
			{
				case 'patient_screening' : if($patient_data[0]->MF1 == 1) 
				{
					return true;
				}
				else
				{
					$this->session->set_flashdata('error', array('header' => 'Access Denied', 'message' => "Please fill in the previous pages first."));

					return false;
				}
				break;
				case 'patient_testing' : if($patient_data[0]->MF1 == 1 && $patient_data[0]->MF2 == 1  ) 
				return true;
				else
				{				
					$this->session->set_flashdata('error', array('header' => 'Access Denied', 'message' => "Please fill in the previous pages first."));

					return false;
				}
				case 'patient_viral_load' : if($patient_data[0]->MF1 == 1 && $patient_data[0]->MF2 == 1 && $patient_data[0]->MF3 == 1 ) 
				{
					return true;
				}
				else
				{
					$this->session->set_flashdata('error', array('header' => 'Access Denied', 'message' => "Please fill in the previous pages first."));

					return false;
				}
				case 'known_history' : if($patient_data[0]->MF1 == 1 && $patient_data[0]->MF2 == 1 && $patient_data[0]->MF3 == 1 && $patient_data[0]->MF4 == 1) 
				return true;
				else
				{
					$this->session->set_flashdata('error', array('header' => 'Access Denied', 'message' => "Please fill in the previous pages first."));

					return false;
				}
				case 'patient_prescription' : if($patient_data[0]->MF1 == 1 && $patient_data[0]->MF2 == 1 && $patient_data[0]->MF3 == 1 && $patient_data[0]->MF4 == 1 && $patient_data[0]->MF5 == 1) 
				return true;
				else
				{
					$this->session->set_flashdata('error', array('header' => 'Access Denied', 'message' => "Please fill in the previous pages first."));

					return false;
				}
				case 'patient_dispensation' : 
				// echo $visit; die();
				if($patient_data[0]->MF1 == 1 && $patient_data[0]->MF2 == 1 && $patient_data[0]->MF3 == 1 && $patient_data[0]->MF4 == 1 && $patient_data[0]->MF5 == 1 && $patient_data[0]->MF6 == 1 ) 
				{

					//$total_visits_possible = $patient_data[0]->T_DurationValue/4;

					if($patient_data[0]->T_DurationOther==0){
						$total_visits_possible = $patient_data[0]->T_DurationValue/4;
					}else{
						$total_visits_possible = $patient_data[0]->T_DurationOther;
					}

					$visits_done           = $patient_visit_data[0]->max_visit_no;
					 //echo ($visits_done +1);
					 //if($visit <= ($visits_done + 1) && $visit != 'eot') by bks 18_09-19
					if($visit <= ($visits_done +1) && $visit != 'eot')
					{
						return true;
					}
					else if ($total_visits_possible == $visits_done && $visit == 'eot')
					{
						return true;
					}
					else if ($visit == 'eot' && $patient_data[0]->InterruptToStage == 1)
					{
						return true;
					}

					else
					{
						$this->session->set_flashdata('error', array('header' => 'Access Denied', 'message' => "Please fill in the previous pages first."));

						return false;
					}

				}
				else
				{
					$this->session->set_flashdata('error', array('header' => 'Access Denied', 'message' => "Please fill in the previous pages first."));

					return false;
				}

				case 'patient_svr' : if($patient_data[0]->MF1 == 1 && $patient_data[0]->MF2 == 1 && $patient_data[0]->MF3 == 1 && $patient_data[0]->MF4 == 1 && $patient_data[0]->MF5 == 1 && $patient_data[0]->MF6 == 1  && ($patient_data[0]->Status == 13 || $patient_data[0]->Status == 30 || $patient_data[0]->Status == 14 || $patient_data[0]->Status == 15  || $patient_data[0]->InterruptToStage == 2)) 
				return true;
				else
				{
					$this->session->set_flashdata('error', array('header' => 'Access Denied', 'message' => "Please fill in the previous pages first."));

					return false;
				}
				break;
				default : return false;
			}
		}
	}

	public function check_uid($uid_prefix = null, $uid_num = null)
	{
			$loginData = $this->session->userdata('loginData');

		//$sql = "SELECT UID_Num,UID_Prefix FROM `tblpatient` where UID_Prefix = ? and UID_Num = ?";
			 $sql = "SELECT UID_Num,UID_Prefix FROM `tblpatient` where id_mstfacility = '".$loginData->id_mstfacility."' and UID_Num = ?";
		$result = $this->db->query($sql,[$uid_num])->result();

		if(count($result) > 0)
		{
			echo "0";
		}
		else
		{
			echo "1";
		}
	}

	private function getCurrentStatus($patientguid = null, $visit = null)
	{
		$sql = "select T_DurationValue,T_DurationOther from tblpatient where PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();
		//echo $patient_data[0]->T_DurationValue;exit();
		switch($visit)
		{
			case 2 : switch($patient_data[0]->T_DurationValue)
			{
				case 8 : $status = 19;
				break;
				case 12 : $status = 5;
				break;
				case 16 : $status = 21;
				break;
				case 20 : $status = 25;
				break;
				case 24 : $status = 6;
				break;
			}
			break;
			case 3 : switch($patient_data[0]->T_DurationValue)
			{
				case 12 : $status = 7;
				break;
				case 16 : $status = 22;
				break;
				case 20 : $status = 26;
				break;
				case 24 : $status = 8;
				break;
			}
			break;
			case 4 : switch($patient_data[0]->T_DurationValue)
			{
				case 16 : $status = 23;
				break;
				case 20 : $status = 27;
				break;
				case 24 : $status = 9;
				break;
			}
			break;
			case 5 : switch($patient_data[0]->T_DurationValue)
			{
				case 20 : $status = 28;
				break;
				case 24 : $status = 10;
				break;
			}
			break;
			case 6 : switch($patient_data[0]->T_DurationValue)
			{
				case 24 : $status = 11;
				break;
			}
			break;
			

		}

		/*sss*/

		switch($visit)
		{
			case 2 : switch($patient_data[0]->T_DurationOther)
			{	
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			case 3 : switch($patient_data[0]->T_DurationOther)
			{
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			case 4 : switch($patient_data[0]->T_DurationOther)
			{
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			case 5 : switch($patient_data[0]->T_DurationOther)
			{
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			case 6 : switch($patient_data[0]->T_DurationOther)
			{
				case 1 : $status = 13;
				break;
				case 2 : $status = 13;
				break;
				case 12 : $status = 13;
				break;
				case 3 : $status = 13;
				break;
				case 4 : $status = 13;
				break;
				case 24 : $status = 13;
				break;
			}
			break;
			

		}
		/*end*/


		return $status;
	}

	public function patient_testing_followup_visit($patientguid = null ,$idval= NULL)
	{
		$loginData = $this->session->userdata('loginData');

		$facility_sql = "select id_mstfacility, facility_short_name from mstfacility";
		$content['facility'] = $this->Common_Model->query_data($facility_sql);

		$sql = "SELECT * FROM `tblpatient`";
		$patient_list = $this->db->query($sql)->result();

		$patient_data   = array();
		$search_uid_num = '';

		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == 'POST')
		{
			if(isset($_POST['save']))
			{



				if($this->security->xss_clean($this->input->post('complicated_uncomplicated')) == 1){
						$compensated_decompensated = 1;
						if($this->security->xss_clean($this->input->post('compensated_decompensated')) == 1){
							$Result =1;
						}
						elseif($this->security->xss_clean($this->input->post('compensated_decompensated')) == 2) {
							$Result =2;
						}
					}elseif($this->security->xss_clean($this->input->post('complicated_uncomplicated')) == 2){
						$compensated_decompensated = 2;
						$Result =3;
					}else{
						$Result =3;
					}
					
					if($this->security->xss_clean($this->input->post('weight'))==""){
						$weightval= NULL;
					}else{
						$weightval= $this->security->xss_clean($this->input->post('weight'));
					}

					if($this->security->xss_clean($this->input->post('egfr'))==""){
						$egfrval= NULL;
					}else{
						$egfrval= $this->security->xss_clean($this->input->post('egfr'));
					}

					/*if($this->security->xss_clean($this->input->post('altlevels')) == 1){
						$Statusdata = 8;
					}elseif($this->security->xss_clean($this->input->post('altlevels')) == 2){
						$Statusdata = 9;
					}elseif($this->security->xss_clean($this->input->post('altlevels')) == 3){
						$Statusdata = 7;
					}else{
						$Statusdata = 4;
					}*/
					/*if($this->security->xss_clean($this->input->post('altlevels'))==''){
						$altleveldata= '4';
					}else{
						$altleveldata = $this->security->xss_clean($this->input->post('altlevels'));
					}*/


				$sql = "SELECT max(Visit_No) as visit_no FROM `hepb_followup` where PatientGUID = ?";
				$result = $this->db->query($sql,[$patientguid])->result();

				if(count($result) == 0)
				{
					$visit_no = 1;
				}
				else
				{
					$visit_no = $result[0]->visit_no + 1;
				}

							$insert_array         = array(
							
							"PatientGUID"         => $patientguid,
							"V1_Haemoglobin"      => $this->security->xss_clean($this->input->post('haemoglobin')),
							"V1_Albumin"          => $this->security->xss_clean($this->input->post('albumin')),
							"V1_Bilrubin"         => $this->security->xss_clean($this->input->post('bilirubin')),
							"V1_INR"              => $this->security->xss_clean($this->input->post('inr')),
							"ALT"                 => $this->security->xss_clean($this->input->post('alt')),
							"AST"                 => $this->security->xss_clean($this->input->post('ast')),
							"AST_ULN"             => $this->security->xss_clean($this->input->post('ast_uln')),
							"V1_Platelets"        => $this->security->xss_clean($this->input->post('platelet_count')),
							"Weight"              => $weightval,
							"V1_Creatinine"       => $this->security->xss_clean($this->input->post('creatinine')),
							"V1_EGFR"             => $egfrval,
							//"Elevated_ALT_Level"  => $altleveldata,
							"V1_Cirrhosis"        => $this->security->xss_clean($this->input->post('complicated_uncomplicated')),
							"Cirr_Encephalopathy" => $this->security->xss_clean($this->input->post('encephalopathy')),
							"Cirr_Ascites"        => $this->security->xss_clean($this->input->post('ascites')),
							"Cirr_VaricealBleed"  => $this->security->xss_clean($this->input->post('variceal_bleed')),
							"ChildScore"          => $this->security->xss_clean($this->input->post('child_score1')),
							"CreatedOn"           => date('Y-m-d'),
							"CreatedBy"           => $loginData->id_tblusers,
							"UpdatedOn"           =>  date('Y-m-d'),
							"UploadedBy"          =>  $loginData->id_tblusers,
							"CirrhosisStatus "    => $compensated_decompensated,
							"Result"              =>$Result,
							'Visit_No'            =>$visit_no
							);

				


				if ($idval==NULL || $idval=='') {
				$this->db->insert('hepb_followup', $insert_array);
				$insert_id = $this->db->insert_id();
				}
				else{
				$insert_id=$idval;
				}

				/*update main table*/

				$update_array = array(
						"V1_Haemoglobin"      => $this->security->xss_clean($this->input->post('haemoglobin')),
						"V1_Albumin"          => $this->security->xss_clean($this->input->post('albumin')),
						"V1_Bilrubin"         => $this->security->xss_clean($this->input->post('bilirubin')),
						"V1_INR"              => $this->security->xss_clean($this->input->post('inr')),
						"ALT"                 => $this->security->xss_clean($this->input->post('alt')),
						"AST"                 => $this->security->xss_clean($this->input->post('ast')),
						"AST_ULN"             => $this->security->xss_clean($this->input->post('ast_uln')),
						"V1_Platelets"        => $this->security->xss_clean($this->input->post('platelet_count')),
						"Weight"              => $weightval,
						"V1_Creatinine"       => $this->security->xss_clean($this->input->post('creatinine')),
						"V1_EGFR"             => $egfrval,
						//"Elevated_ALT_Level"        => $altleveldata,
						"V1_Cirrhosis"        => $this->security->xss_clean($this->input->post('complicated_uncomplicated')),
						"Cirr_Encephalopathy" => $this->security->xss_clean($this->input->post('encephalopathy')),
						"Cirr_Ascites"        => $this->security->xss_clean($this->input->post('ascites')),
						"Cirr_VaricealBleed"  => $this->security->xss_clean($this->input->post('variceal_bleed')),
						"ChildScore"          => $this->security->xss_clean($this->input->post('child_score1')),
						"CirrhosisStatus "	 => $compensated_decompensated,
						"Result" 			=>$Result,
					);
					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('hepb_tblpatient', $update_array);
					/*end main table*/


				/*add flooup*/

					if($this->security->xss_clean($this->input->post('ultrasound')) == null)
					{
						$insert_arrayf['Clinical_US'] = null;
						$insert_arrayf['Clinical_US_Dt'] = null;
					}
					else
					{
						$insert_arrayf['Clinical_US']    = 1;
						$insert_arrayf['Clinical_US_Dt'] = $this->security->xss_clean(timeStamp($this->input->post('ultrasound_date')));
					}

					if($this->security->xss_clean($this->input->post('fibroscan')) == null)
					{
						$insert_arrayf['Fibroscan']     = null;
						$insert_arrayf['Fibroscan_Dt']  = null;
						$insert_arrayf['Fibroscan_LSM'] = null;
					}
					else
					{
						$insert_arrayf['Fibroscan']     = 1;
						$insert_arrayf['Fibroscan_Dt']  = $this->security->xss_clean(timeStamp($this->input->post('fibroscan_date')));
						$insert_arrayf['Fibroscan_LSM'] = $this->security->xss_clean($this->input->post('fibroscan_lsm'));
					}

					if($this->security->xss_clean($this->input->post('apri')) == null)
					{
						$insert_arrayf['APRI']       = null;
						$insert_arrayf['APRI_Dt']    = null;
						$insert_arrayf['APRI_Score'] = null;
					}
					else
					{
						$insert_arrayf['APRI']       = 1;
						// $insert_array['APRI_Dt']    = $this->security->xss_clean($this->input->post('apri_date'));
						$insert_arrayf['APRI_Score'] = $this->security->xss_clean($this->input->post('apri_score'));
					}

					if($this->security->xss_clean($this->input->post('fib4')) == null)
					{ 
						$insert_arrayf['FIB4']       = null;
						$insert_arrayf['fib4']   	= null;
						$insert_arrayf['FIB4_FIB4']  = null;
					}
					else
					{ 
						$insert_arrayf['FIB4'] = 1;
						// $insert_array['FIB4_Dt']     = $this->security->xss_clean($this->input->post('fib4_date'));
						$insert_arrayf['FIB4_FIB4']   = $this->security->xss_clean($this->input->post('fib4_score'));
					}

					$insert_arrayf['Prescribing_Dt']   = $this->security->xss_clean(timeStamp($this->input->post('date_of_prescribing_tests')));
					$insert_arrayf['LastTest_Dt']   = $this->security->xss_clean(timeStamp($this->input->post('last_test_result_date')));

					$insert_arrayf['APRI_PC']   = $this->security->xss_clean($this->input->post('platelet_count'));
					$insert_arrayf['FIB4_PC']   = $this->security->xss_clean($this->input->post('platelet_count'));

					/*hepb update*/

						/*if($this->security->xss_clean($this->input->post('ultrasound')) == null)
					{
						$insert_arrayf1['Clinical_US'] = null;
						$insert_arrayf1['Clinical_US_Dt'] = null;
					}
					else
					{
						$insert_arrayf1['Clinical_US']    = 1;
						$insert_arrayf1['Clinical_US_Dt'] = $this->security->xss_clean(timeStamp($this->input->post('ultrasound_date')));
					}*/

					/*if($this->security->xss_clean($this->input->post('fibroscan')) == null)
					{
						$insert_arrayf1['Fibroscan']     = null;
						$insert_arrayf1['Fibroscan_Dt']  = null;
						$insert_arrayf1['Fibroscan_LSM'] = null;
					}
					else
					{
						$insert_arrayf1['Fibroscan']     = 1;
						$insert_arrayf1['Fibroscan_Dt']  = $this->security->xss_clean(timeStamp($this->input->post('fibroscan_date')));
						$insert_arrayf1['Fibroscan_LSM'] = $this->security->xss_clean($this->input->post('fibroscan_lsm'));
					}*/

					if($this->security->xss_clean($this->input->post('apri')) == null)
					{
						//$insert_arrayf1['APRI']       = null;
						$insert_arrayf1['APRI_Dt']    = null;
						$insert_arrayf1['APRI_Score'] = null;
					}
					else
					{
						//$insert_arrayf1['APRI']       = 1;
						// $insert_array['APRI_Dt']    = $this->security->xss_clean($this->input->post('apri_date'));
						$insert_arrayf1['APRI_Score'] = $this->security->xss_clean($this->input->post('apri_score'));
					}

					if($this->security->xss_clean($this->input->post('fib4')) == null)
					{ 
						//$insert_arrayf1['FIB4']       = null;
						$insert_arrayf1['fib4']   	= null;
						$insert_arrayf1['FIB4_FIB4']  = null;
					}
					else
					{ 
						//$insert_arrayf1['FIB4'] = 1;
						// $insert_array['FIB4_Dt']     = $this->security->xss_clean($this->input->post('fib4_date'));
						$insert_arrayf1['FIB4_FIB4']   = $this->security->xss_clean($this->input->post('fib4_score'));
					}

					$insert_arrayf1['Prescribing_Dt']   = $this->security->xss_clean(timeStamp($this->input->post('date_of_prescribing_tests')));
					$insert_arrayf1['LastTest_Dt']   = $this->security->xss_clean(timeStamp($this->input->post('last_test_result_date')));

					//$insert_arrayf1['APRI_PC']   = $this->security->xss_clean($this->input->post('platelet_count'));
				//$insert_arrayf1['FIB4_PC']   = $this->security->xss_clean($this->input->post('platelet_count'));
					/*end*/

					

					$dataval = array('PatientGUID' => $patientguid, 'ID' => $insert_id);
					$this->db->where($dataval);
					$this->db->update('hepb_followup', $insert_arrayf);


				$this->db->where('PatientGUID', $patientguid);
				$this->db->update('hepb_tblpatient', $insert_arrayf1);


				if($this->security->xss_clean($this->input->post('ultrasound')) == null)
				{
					$insert_arraycirrohosis['Clinical_US'] = null;
					$insert_arraycirrohosis['Clinical_US_Dt'] = null;
				}
				else
				{
					$insert_array['Clinical_US']    = 1;
					$insert_array['Clinical_US_Dt'] = $this->security->xss_clean(timeStamp($this->input->post('ultrasound_date')));
				}

				if($this->security->xss_clean($this->input->post('fibroscan')) == null)
				{
					$insert_arraycirrohosis['Fibroscan']     = null;
					$insert_arraycirrohosis['Fibroscan_Dt']  = null;
					$insert_arraycirrohosis['Fibroscan_LSM'] = null;
				}
				else
				{
					$insert_arraycirrohosis['Fibroscan']     = 1;
					$insert_arraycirrohosis['Fibroscan_Dt']  = $this->security->xss_clean(timeStamp($this->input->post('fibroscan_date')));
					$insert_arraycirrohosis['Fibroscan_LSM'] = $this->security->xss_clean($this->input->post('fibroscan_lsm'));
				}

				if($this->security->xss_clean($this->input->post('apri')) == null)
				{
					$insert_array['APRI']       = null;
					$insert_arraycirrohosis['APRI_Dt']    = null;
					$insert_arraycirrohosis['APRI_Score'] = null;
				}
				else
				{
					$insert_arraycirrohosis['APRI']       = 1;
					// $insert_array['APRI_Dt']    = $this->security->xss_clean($this->input->post('apri_date'));
					$insert_arraycirrohosis['APRI_Score'] = $this->security->xss_clean($this->input->post('apri_score'));

					
				}

				if($this->security->xss_clean($this->input->post('fib4')) == null)
				{ 
					$insert_arraycirrohosis['FIB4']      = null;
					$insert_arraycirrohosiscirrohosis['fib4']   = null;
					$insert_arraycirrohosis['FIB4_FIB4'] = null;
				}
				else
				{ 
					$insert_arraycirrohosis['FIB4'] = 1;
					// $insert_array['FIB4_Dt']     = $this->security->xss_clean($this->input->post('fib4_date'));
					$insert_arraycirrohosis['FIB4_FIB4']   = $this->security->xss_clean($this->input->post('fib4_score'));
				}

				$insert_arraycirrohosis['Prescribing_Dt']   = $this->security->xss_clean(timeStamp($this->input->post('date_of_prescribing_tests')));
				$insert_arraycirrohosis['LastTest_Dt']   = $this->security->xss_clean(timeStamp($this->input->post('last_test_result_date')));

				


				$this->db->where('PatientGUID', $patientguid);
				$this->db->update('tblpatientcirrohosis', $insert_arraycirrohosis);

				$apri_score =$this->security->xss_clean($this->input->post('apri_score'));
				$fib4_score =$this->security->xss_clean($this->input->post('fib4_score'));

				if($this->security->xss_clean($this->input->post('complicated_uncomplicated'))==1 || $apri_score >=2 || $fib4_score >='3.25'){
						$insert_arraych['MF3'] = 1;
						$insert_arraych['Elevated_ALT_Level'] = 2;
						$insert_arraych['Status'] = 8;
						
						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('hepb_tblpatient', $insert_arraych);
					redirect('patientinfo_hep_b/patient_viral_load/'.$patientguid);
				}else{

						/*$insert_arraych['Elevated_ALT_Level'] = NULL;
						$insert_arraych['Status'] = 4;
						
						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('hepb_tblpatient', $insert_arraych);*/

					redirect('patientinfo_hep_b/patient_testing_followup_visit/'.$patientguid);
				}

				
			}

		}
		else if($patientguid != null)
		{
			$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
			$patient_data = $this->db->query($sql,[$patientguid])->result();

			$sql = "SELECT * FROM `hepb_followup` WHERE PatientGUID = ? order by id desc";
			$patient_tblpatientaddltest_data = $this->db->query($sql,[$patientguid])->result();

			$sql = "SELECT * FROM `hepb_followup` WHERE PatientGUID = ? and  id = ?";
			$content['fv_details'] = $this->db->query($sql,[$patientguid,$idval])->result();


		}

		$sql = "SELECT max(Visit_No) as visit_no,Visit_No  FROM `hepb_followup` where PatientGUID = ?";
		$result = $this->db->query($sql,[$patientguid])->result();
		
		$sql = "SELECT Visit_No  FROM `hepb_followup` where PatientGUID = ? and ID = ?";
		$content['Visit_No'] = $this->db->query($sql,[$patientguid,$idval])->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 7 and LanguageID = 1";
		$content['options_ence_ascites'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 8 and LanguageID = 1";
		$content['options_variceal_bleed'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 48 and LanguageID = 1";
		$content['reason_death'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 49 and LanguageID = 1";
		$content['reason_flu'] = $this->db->query($sql)->result();

		$sql ="SELECT * FROM mstlookup where  flag=32 order by Sequence ASC";
		$content['InterruptReason'] = $this->db->query($sql)->result();

		$sql = "select * from mstfacility where id_mstfacility = ".$loginData->id_mstfacility;
		$content['search_facilities'] = $this->db->query($sql)->result();

		$sql = "select * from mststate where id_mststate = ".$content['search_facilities'][0]->id_mststate;
		$content['search_states'] = $this->db->query($sql)->result();

		$content['patient_data']                    = $patient_data;
		$content['patient_tblpatientaddltest_data'] = $patient_tblpatientaddltest_data;
		$content['patient_list']                    = $patient_list;
		$content['user_state']                      = $content['search_facilities'][0]->id_mststate;
		$content['user_hospital']                   = $loginData->id_mstfacility;
		$content['search_uid_num']                  = $search_uid_num;
		$content['idvaldata']                       = $idval;
		$content['result']                          = $result[0]->visit_no;


		$content['subview'] = 'hepb/patient_testing_followup_visit_hep_b';
		$this->load->view('pages/main_layout', $content);
	}

	public function getFacilities($id_mststate = null)
	{
		$sql = "select * from mstfacility where id_mststate = ? order by facility_short_name";
		$Facilities = $this->db->query($sql,[$id_mststate])->result();

		$options = "<option value=''>Select Facility</option>";
		foreach ($Facilities as $getdistrict) {
			$options .= "<option value='".$getdistrict->id_mstfacility."'>".$getdistrict->facility_short_name."</option>";
		}
		echo $options;
	}


	public function interruptionstatus_process($patientguid = null) {
		if($this->input->post())
		{
			$arr = array();
			$this->form_validation->set_rules('resonval', 'Reson', 'trim|required|xss_clean');
			/*if($this->input->post('resonval')==1){
				$this->form_validation->set_rules('resonvaldeath', 'Reason for death', 'trim|required|xss_clean');
			}elseif($this->input->post('resonval')==2){
				$this->form_validation->set_rules('resonfluid', 'Reason for LFU', 'trim|required|xss_clean');
			}*/
			if($this->input->post('resonval')==99){
				$this->form_validation->set_rules('otherInterrupt', 'Other', 'trim|required|xss_clean');
			}

			if ($this->form_validation->run() == FALSE) {
				$arr['status'] = 'false';
				$arr['message'] = validation_errors();
			}else{
				$data = array(
					'InterruptReason' => $this->security->xss_clean($this->input->post('resonval'))/*,
					'DeathReason' => $this->security->xss_clean($this->input->post('resonvaldeath')),
					'LFUReason' => $this->security->xss_clean($this->input->post('resonfluid')),
					'InterruptReasonOther' => $this->security->xss_clean($this->input->post('otherInterrupt')),
					'InterruptToStage' => $this->security->xss_clean($this->input->post('interruption_stage'))*/
				);

				$this->db->where('PatientGUID', $patientguid);
				$unitid = $this->db->update('hepb_tblpatient', $data);

				$arr['interruptstage'] = $this->security->xss_clean($this->input->post('interruption_stage'));

				if ($unitid > 0) {
					$arr['status'] = 'true';
					$arr['message'] = '';
				}else{

					$arr['status'] = 'false';
					$arr['message'] = 'Please fill out all the required fields.000000';
				}
			}
			echo json_encode($arr);
		}
	}	



public function get_last_visit_details(){
if($this->input->post()){

$arr = array();
$PatientID = $this->input->post('PatientID');
$data = array(
'PatientID' => $PatientID
);
$query = "Select VisitNo, DATE_FORMAT(Treatment_Dt, '%d-%m-%Y') as Treatment_Dt, DispensationPlace, PillsTaken, PillsTakenDaily, PillsDispensed, SolutionTakenDaily, PillsNeededDaily, PillstoBeDispensed, DATE_FORMAT(NextVisit, '%d-%m-%Y') as NextVisit,PIllsLeft,Adherence,AdherenceReason,SideEffectValue,SideEffectValueOther,PillsDaysDispensed  FROM tblpatientdispensationb where PatientID = ?";
$data = $this->db->query($query, [$PatientID])->result();
if($data){
$arr['status'] = 'true';
$arr['fields'] = json_encode($data[0]);
}else{
$arr['status'] = 'false';
$arr['fields'] = '';
}

echo json_encode($arr);
}

}


public function get_drugsdosagesvals($RegimenId = null){


 $query = "select * from mst_drug_strength_hepb where is_deleted =0 and  id_mst_drughepb = ?";
$data = $this->db->query($query, [$RegimenId])->result();
		//$options = "<option value=''>All</option>";
		foreach ($data as $strengthval) {
		


			$options .= "<option value='".$strengthval->id_mst_drughepb."' >".$strengthval->strength."</option>";
		}

		echo $options;
}


public function checkhepbdata($patientguid = null){

//if($this->input->post()){

				$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
				$riskdatatblpat = $this->db->query($sql,[$patientguid])->result();
				
				$sql = "SELECT * FROM `tblpatientaddltest` WHERE PatientGUID = ?";
				$tblpatientaddltest = $this->db->query($sql,[$patientguid])->result();

				$sql = "SELECT * FROM `tblpatientcirrohosis` WHERE PatientGUID = ?";
				$tblpatientcirrohosis = $this->db->query($sql,[$patientguid])->result();

				

				$sql = "SELECT * FROM `hepb_tblpatient` WHERE PatientGUID = ?";
				$patient_datahepb = $this->db->query($sql,[$patientguid])->result();	

				if($patient_datahepb[0]->Risk==''){

							$insert_arrayhepb = array(	
							'PatientGUID'       =>$patientguid,
							'Risk'				=> $riskdatatblpat[0]->Risk,
							"MF1"               => 1,
							"Status"            => 3 );
							
							

							$this->db->where('PatientGUID', $riskdatatblpat[0]->PatientGUID);
							$this->db->update('hepb_tblpatient', $insert_arrayhepb);


						

				$sql = "SELECT * FROM `tblriskprofile` WHERE PatientGUID = ?";
				$riskdata = $this->db->query($sql,[$patientguid])->result();
				foreach ($riskdata as  $value) {

					$insert_riskdata = array(	
							'PatientGUID'       =>$value->PatientGUID,
							"RiskID"               =>$value->RiskID );
						//pr($insert_riskdata);exit();
							$this->db->insert('tblriskprofile_hepb', $insert_riskdata);
					
				}

			}


			if(empty($patient_datahepb[0]->MF2)){

				$update_array = array(
				
					"V1_Haemoglobin"      =>  $riskdatatblpat[0]->V1_Haemoglobin,
					"V1_Albumin"          =>  $riskdatatblpat[0]->V1_Albumin,
					"V1_Bilrubin"         =>  $riskdatatblpat[0]->V1_Bilrubin,
					"V1_INR"              =>  $riskdatatblpat[0]->V1_INR,
					"ALT"                 =>  $riskdatatblpat[0]->ALT,
					"AST"                 =>  $riskdatatblpat[0]->AST,
					"AST_ULN"             =>  $riskdatatblpat[0]->AST_ULN,
					"V1_Platelets"        =>  $riskdatatblpat[0]->V1_Platelets,
					"Weight"              =>  $riskdatatblpat[0]->Weight,
					"V1_Creatinine"       =>  $riskdatatblpat[0]->V1_Creatinine,
					"V1_EGFR"             =>  $riskdatatblpat[0]->V1_EGFR,
					"V1_Cirrhosis"        =>  $riskdatatblpat[0]->V1_Cirrhosis,
					"Cirr_Encephalopathy" =>  $riskdatatblpat[0]->Cirr_Encephalopathy,
					"Cirr_Ascites"        =>  $riskdatatblpat[0]->Cirr_Ascites,
					"Cirr_VaricealBleed"  =>  $riskdatatblpat[0]->Cirr_VaricealBleed,
					"ChildScore"          =>  $riskdatatblpat[0]->ChildScore,
					"CirrhosisStatus "	 =>  $riskdatatblpat[0]->CirrhosisStatus,
					"Result" 			=> $riskdatatblpat[0]->Result,

					//"" =>$this->security->xss_clean($this->input->post('compensated_decompensated'))

				);


				$this->db->where('PatientGUID', $patientguid);
				$this->db->update('hepb_tblpatient', $update_array);


				/*$insert_arraytest = array(
					"PatientGUID"          => $riskdatatblpat[0]->PatientGUID,
					"PatientTestVisitGUID" => uniqid(),
					"Visit_No"             => 1,
					"V1_Haemoglobin"       => $riskdatatblpat[0]->V1_Haemoglobin,
					"V1_Albumin"           => $riskdatatblpat[0]->V1_Albumin,
					"V1_Bilrubin"          => $riskdatatblpat[0]->V1_Albumin,
					"V1_INR"               => $riskdatatblpat[0]->V1_INR,
					"ALT"                  => $riskdatatblpat[0]->ALT,
					"AST"                  => $riskdatatblpat[0]->AST,
					"AST_ULN"              => $riskdatatblpat[0]->AST_ULN,
					"V1_Platelets"         => $riskdatatblpat[0]->V1_Platelets,
					"V1_Creatinine"        => $riskdatatblpat[0]->V1_Creatinine,
					"CreatedOn"            => date('Y-m-d'),
					"APRI_Score"			=>$tblpatientcirrohosis[0]->V1_Creatinine, 
					"FIB4_FIB4"				=>$tblpatientcirrohosis[0]->V1_Creatinine,
					"CreatedBy"            => $loginData->id_tblusers,
				);

				$this->db->insert('hepb_followup', $insert_arraytest);*/


			}

			

//}

}
//}


public function hbvdna_followUp_visit($patientguid = null , $idval = NULL){


	$loginData = $this->session->userdata('loginData');

		$facility_sql = "select id_mstfacility, facility_short_name from mstfacility";
		$content['facility'] = $this->Common_Model->query_data($facility_sql);

		$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();
		//pr($patient_data);
		$HbsAg = $patient_data[0]->HbsAg;	

		$sqld = "SELECT count(PatientGUID) as count FROM `tblpatientvlsamplelist` WHERE PatientGUID = ?";
		$tblpatientvlsamplelist = $this->db->query($sqld,[$patientguid])->result();	
 $visitno = $tblpatientvlsamplelist[0]->count+1;
		//pr($tblpatientvlsamplelist);exit;

		/*if(!$this->allow_url($patientguid, __FUNCTION__))
		{
			$this->patient_redirect($patientguid);
		}*/

		$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$patient_data = $this->db->query($sql,[$patientguid])->result();
		
		$arrayresult = array(
			$patient_data[0]->HBSRapidResult,
			$patient_data[0]->HBCRapidResult,
			$patient_data[0]->HCVRapidResult,
			$patient_data[0]->HEVRapidResult,
			$patient_data[0]->HAVRapidResult,
			$patient_data[0]->HBSElisaResult,
			$patient_data[0]->HBCElisaResult,
			$patient_data[0]->HCVElisaResult,
			$patient_data[0]->HEVElisaResult,
			$patient_data[0]->HAVElisaResult,
			$patient_data[0]->HBSOtherResult,
			$patient_data[0]->HBCOtherResult,
			$patient_data[0]->HCVOtherResult,
			$patient_data[0]->HEVOtherResult,
			$patient_data[0]->HAVOtherResult
		);

		if(!in_array('1',$arrayresult))
		{
			redirect('patientinfo_hep_b/patient_register');
		}

		$RequestMethod = $this->input->server('REQUEST_METHOD');
		if($RequestMethod == 'POST')
		{
			if(isset($_POST['save']))
			{
				// form_validation start
				
				$this->form_validation->set_rules('hepb_vl_sample_drawn_date', 'Sample Drawn On Date of HepB', 'trim|required|xss_clean');
				
				if ($this->form_validation->run() == FALSE) {					
					$arr['status'] = 'false';
					$this->session->set_flashdata("tr_msg",validation_errors());
				}else{
					$viral_infection = false;

					/*if($this->security->xss_clean($this->input->post('hepc_vl_result')) ==1){
						$hepc_vl_resultMF4 = 1;
					}else{
						$hepc_vl_resultMF4 =0;
					}*/
					if($this->security->xss_clean($this->input->post('hepc_vl'))==''){
						$hepc_vl = NULL;
					}else{
						$hepc_vl = $this->security->xss_clean($this->input->post('hepc_vl'));
					}
					
					
						//$update_array['VLHepB'] = 1;
						$update_array['BVLSampleCollectionDate'] = $this->security->xss_clean(timeStamp($this->input->post('hepb_vl_sample_drawn_date')));
						//$update_array['Treatment_Recommended']  = $this->security->xss_clean($this->input->post('Treatment_Recommended'));
						$update_array['IsBVLSampleStored']       = $this->security->xss_clean($this->input->post('hepb_is_sample_stored'));
						$update_array['BVLStorageTemp']          = $this->security->xss_clean($this->input->post('hepb_sample_storage_temp'));


						$update_array['BStorage_days_hrs']        = $this->security->xss_clean($this->input->post('hepb_sample_stored_duration_more_less_than_day'));
						$update_array['BStorageduration']        = $this->security->xss_clean($this->input->post('hepb_sample_stored_duration'));
						$update_array['IsBVLSampleTransported']  = $this->security->xss_clean($this->input->post('hepb_is_sample_transported'));
						$update_array['BVLTransportTemp']        = $this->security->xss_clean($this->input->post('hepb_sample_transport_temp'));
						$update_array['BVLTransportDate']        = $this->security->xss_clean(timeStamp($this->input->post('hepb_sample_transport_date')));
						$update_array['BVLLabID']                = $this->security->xss_clean($this->input->post('hepb_sample_transported_to'));
						$update_array['BVLReceiverName']         = $this->security->xss_clean($this->input->post('hepb_sample_received_by_name'));

						$update_array['BVLTransporterName'] 	= $this->security->xss_clean($this->input->post('hepb_sample_transported_to_name'));
						$update_array['BVLTransporterDesignation'] = $this->security->xss_clean($this->input->post('hepb_sample_transported_to_designation'));
						$update_array['BVLReceiverDesignation']  = $this->security->xss_clean($this->input->post('hepb_sample_received_by_designation'));
						$update_array['BVLSCRemarks']            = $this->security->xss_clean($this->input->post('hepb_sample_remarks'));
						$update_array['IsBSampleAccepted']       = $this->security->xss_clean($this->input->post('hepb_is_sample_accepted'));
						$update_array['T_DLL_01_BVLC_Date']      = $this->security->xss_clean(timeStamp($this->input->post('hepb_vl_result_date')));
						$update_array['T_DLL_01_BVLCount']       = $this->security->xss_clean($this->input->post('hepb_vl'));
						$update_array['T_DLL_01_BVLC_Result']    = $this->security->xss_clean($this->input->post('hepb_vl_result'));
						$update_array['BRejectionReason']        = $this->security->xss_clean($this->input->post('hepb_sample_reason_for_rejection'));
						$update_array['BVLResultRemarks']        = $this->security->xss_clean($this->input->post('hepb_vl_remarks'));
						$update_array['BVLRecieptDate']        = $this->security->xss_clean(timeStamp($this->input->post('hepb_sample_receipt_date')));
						
						//$update_array['Treatment_Recommended']   = $this->security->xss_clean(($this->input->post('Treatment_Recommended')));

							//$update_arrayhepb['MF4'] 			         = 1;

					

					if($this->security->xss_clean($this->input->post('hepc_vl_result')) == 1 || $this->security->xss_clean($this->input->post('hepb_vl_result')) == 1)
					{
						$viral_infection = true;
					}

				

					$this->db->where('PatientGUID', $patientguid);
					$this->db->update('tblpatient', $update_array);


					$update_array['Visit_No'] 			         = $visitno;
					$update_array['PatientGUID'] = $patientguid;
						$this->db->insert('tblpatientvlsamplelist', $update_array);



					/*$this->db->where('PatientGUID', $patientguid);
					$this->db->update('hepb_tblpatient', $update_arrayhepb);*/

					if($this->security->xss_clean($this->input->post('hepb_vl_result')) == 2){
						redirect('patientinfo_hep_b?p=1');
					}

					/*if($this->security->xss_clean($this->input->post('hepc_vl_result'))== "" && $this->security->xss_clean($this->input->post('hepc_vl_result_date'))== ""){
						redirect('patientinfo_hep_b?p=1');
					}*/

					/*if($this->security->xss_clean($this->input->post('hepc_is_sample_accepted')) == 2){
						redirect('patientinfo_hep_b/patient_viral_load/'.$patientguid);
					}*/

					if($this->input->post('interruption_status')==1){

						redirect('patientinfo_hep_b/patient_viral_load/'.$patientguid);
					}elseif($this->input->post('interruption_status')==null || $this->input->post('interruption_status')==0){

						$data = array(
							'InterruptReason' => null/*,
							'DeathReason' => null,
							'LFUReason' => null*/
						);

						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('tblpatient', $data);
					}

						if($this->security->xss_clean($this->input->post('hepb_vl_result')) == 2 && $this->security->xss_clean($this->input->post('Treatment_Recommended'))==2){

						$update_arrayhepb['MF4'] 			         = null;
						//$update_arrayhepb['Status'] 			         = 10;
						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('hepb_tblpatient', $update_arrayhepb);
						redirect('patientinfo_hep_b?p=1');
					}


					if($this->security->xss_clean($this->input->post('hepb_vl_result'))!="" && $this->security->xss_clean($this->input->post('Treatment_Recommended'))=='2'){
						//gau testing changed to known_history
						redirect('patientinfo_hep_b/hbvdna_followUp_visit/'.$patientguid);
					}


					/*elseif($this->security->xss_clean($this->input->post('hepb_vl_result'))!="" &&  $this->security->xss_clean($this->input->post('Treatment_Recommended'))=='1'){

						$update_arrayhepb['MF4'] 			         = 1;
						$update_arrayhepb['Status'] 			         = 4;
						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('hepb_tblpatient', $update_arrayhepb);

						$update_arrayhepbrec['Treatment_Recommended'] 			         = 1;
						$this->db->where('PatientGUID', $patientguid);
						$this->db->update('tblpatient', $update_arrayhepbrec);

						redirect('patientinfo_hep_b/known_history/'.$patientguid);
					}*/

					else{

						redirect('patientinfo_hep_b/hbvdna_followUp_visit/'.$patientguid);
					}
				} // from validation end
			}

		}
		else if($patientguid != null)
		{
			$sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
			$patient_data = $this->db->query($sql,[$patientguid])->result();

			$sql = "SELECT * FROM `hepb_tblpatient` WHERE PatientGUID = ?";
			$patient_datahepb = $this->db->query($sql,[$patientguid])->result();

			/*$sql = "SELECT * FROM `tblpatientvlsamplelist` WHERE PatientGUID = ? and Visit_No = ?";
			$patient_datahepb = $this->db->query($sql,[$patientguid,$idval])->result();*/

			$sql = "SELECT * FROM `tblpatientvlsamplelist` WHERE PatientGUID = ?  and Visit_No = ? ";
			$tblpatientvlsamplelist = $this->db->query($sql,[$patientguid,$idval])->result();

			$sql = "SELECT (case when IsBSampleAccepted=1 then 'Yes' else 'NO' end ) as IsBSampleAccepted,(case  when T_DLL_01_BVLC_Result = 1 then 'Detected' else 'Not Detected' end) as T_DLL_01_BVLC_Result,Visit_No,T_DLL_01_BVLC_Date,PatientGUID FROM `tblpatientvlsamplelist` WHERE PatientGUID = ?  order by Visit_No desc";
			$tblpatientvlsamplelistlist = $this->db->query($sql,[$patientguid])->result();

		}

		$sql = "select * from mstfacility where id_mstfacility = ".$loginData->id_mstfacility;
		$content['search_facilities'] = $this->db->query($sql)->result();

		$sql = "select * from mststate where id_mststate = ".$content['search_facilities'][0]->id_mststate;
		$content['search_states'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where Flag = 39 and LanguageID = 1";
		$content['sample_transported_to_options'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where Flag = 5 and LanguageID = 1";
		$content['results_options'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where Flag = 37 and LanguageID = 1";
		$content['designation_options'] = $this->db->query($sql)->result();

		$sql = "SELECT  designation_listId, Designation FROM `mst_designation_list` where id_mstfacility = ".$loginData->id_mstfacility." or id_mstfacility=0 union Select '99' as designation_listId,'Peer Support' as Designation order by designation_listId asc ";
		$content['designation_options_list'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where Flag = 45 and LanguageID = 1";
		$content['rejection_options'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 48 and LanguageID = 1";
		$content['reason_death'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 49 and LanguageID = 1";
		$content['reason_flu'] = $this->db->query($sql)->result();

		$sql = "SELECT * FROM `mstlookup` where flag = 57 and LanguageID = 1";
		$content['vl_temp'] = $this->db->query($sql)->result();

		$sql ="SELECT * FROM mstlookup where  flag=32 order by Sequence ASC";
		$content['InterruptReason'] = $this->db->query($sql)->result();

		$sql ="SELECT * FROM mstlookup where  flag=19 and languageID=1 order by Sequence ASC";
		$content['unlockprocess'] = $this->db->query($sql)->result();

		$sql = "select p.UID_Prefix, p.UID_Num, p.FirstName, b.LookupValue as status, p.PatientGUID from tblpatient p left join (SELECT * FROM mstlookup where Flag =13 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
		$content['patient_status'] = $this->db->query($sql,[$patientguid])->result();
			$sql = "select  b.LookupValue as status, p.PatientGUID from hepb_tblpatient p left join (SELECT * FROM mstlookup where Flag =72 and LanguageID = 1) b on p.status = b.LookupCode where patientguid= ?";
	  	$content['patient_statushepb'] = $this->db->query($sql,[$patientguid])->result();
	  	$sql = "SELECT max(Visit_No) as visit_no FROM `tblpatientvlsamplelist` where PatientGUID = ?";
		$max_visit = $this->db->query($sql,[$patientguid])->result();

		$content['patient_data']   = $patient_data;
		$content['patient_datahepb']        = $patient_datahepb;
		$content['tblpatientvlsamplelist']        = $tblpatientvlsamplelist;
		$content['tblpatientvlsamplelistlist']        = $tblpatientvlsamplelistlist;
		
		$content['user_state']     = $content['search_facilities'][0]->id_mststate;
		$content['user_hospital']  = $loginData->id_mstfacility;

		$content['idvaldata']      = $idval;
		$content['max_visit'] = $max_visit[0]->visit_no;


	$content['subview'] = 'hepb/hbvdna_followUp_visit_hep_b';
	$this->load->view('pages/main_layout', $content);

}



}