<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offline_monthly_records extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->model('Common_Model');
		$this->load->helper('common');
		$this->load->model('Offline_monthly_entry_Model');
//error_reporting(0);
		$loginData = $this->session->userdata('loginData');

		if($loginData == null)
		{
			redirect('login');
		}
	}



	public function index($ids = null)
	{
		//error_reporting(0);
		$loginData = $this->session->userdata('loginData');
	//echo "<pre>";print_r($loginData);exit();
if( ($loginData) && $loginData->user_type != '3' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}


		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

	if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->input->post('search_state');
			$filters1['id_input_district'] = $this->input->post('input_district');
			$filters1['id_mstfacility'] = $this->input->post('facility');
			$month                      =$this->input->post('month');
			$year = date("Y",strtotime('01-01-'.$this->input->post('year')));
			$last_day = date('t', strtotime('01-'.$month.'-'.$year));
			$filters1['enddate']        = timeStamp($last_day.'-'.$month.'-'.$year);
			$filters1['startdate']   =timeStamp('01-'.$month.'-'.$year);
			$filters1['enddate1']        = timeStamp($last_day.'-'.$month.'-'.$year);
		}
		else
		{
			$filters1['id_search_state'] = 0;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;
			$filters1['startdate']      = date('Y-m-d',strtotime(date('Y-m-01'). '-1 MONTH'));
			 $filters1['enddate']   =  date('Y-m-t',strtotime(date('Y-m-t'). '-1 MONTH'));
			$filters1['enddate1']       =date('Y-m-t',strtotime(date('Y-m-t'). '-1 MONTH'));
		}

		 $this->session->set_userdata('filters1', $filters1);
		  $sql = "select * from mststate where ".$sess_where." order by StateName ASC";
		$content['states'] = $this->db->query($sql)->result();

		 $sql = "select * from mstdistrict where ".$sess_mstdistrict."";
		$content['districts'] = $this->db->query($sql)->result();
		//pr($content['districts']);
		 $sql = "select * from mstfacility where  ".$sess_mstfacility."";
		$content['facilities'] = $this->db->query($sql)->result();	
		$content['subview'] = 'hbv_offline_entry';
		$this->load->view('admin/main_layout', $content);
	}
public function hcv_offline_monthly_records($ids = null)
	{
		//error_reporting(0);
		$loginData = $this->session->userdata('loginData');
	//echo "<pre>";print_r($loginData);exit();
if( ($loginData) && $loginData->user_type != '3' ){
				$this->session->set_flashdata('er_msg','Your role is not allowed to access data');
			redirect('login'); 

			}
			if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1";
				$sess_mstdistrict =" 1";
				$sess_mstfacility =" 1";
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."'";

				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'  ";

				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' and id_mstfacility='".$loginData->id_mstfacility."'";

			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."'  ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."'";
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_mstdistrict = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
				$sess_mstfacility = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' ";
			}


		$REQUEST_METHOD = $this->input->server('REQUEST_METHOD');

	if($REQUEST_METHOD == 'POST')
		{
			
			$filters1['id_search_state'] = $this->security->xss_clean($this->input->post('search_state'));
			$month                      =$this->security->xss_clean($this->input->post('month'));
			$year = date("Y",strtotime('01-01-'.$this->security->xss_clean($this->input->post('year'))));
			$last_day = date('t', strtotime('01-'.$month.'-'.$year));
			$filters1['enddate']        = timeStamp($last_day.'-'.$month.'-'.$year);
			$filters1['startdate']   =timeStamp('01-'.$month.'-'.$year);
			$filters1['enddate1']        = timeStamp($last_day.'-'.$month.'-'.$year);

			$content['get_info_monthly_record']=$this->Offline_monthly_entry_Model->get_info_monthly_record($month,$year);
			if (empty($content['get_info_monthly_record'])) {
				$array2 = array("id_tblusers" => $loginData->id_tblusers, "CreatedOn" => date('Y-m-d'));
			}
			elseif (!empty($content['get_info_monthly_record'])) {
				$array2 = array("updateBy" => $loginData->id_tblusers, "updatedOn" => date('Y-m-d'));
				
				$where1=array('month'=>$month,'year'=>$year,'genderwise'=>4,'agewise'=>2);
			}
			if ($this->input->post('save')=='save') {

				/*male female and transgender data into array start (VSG)*/

				$hcv_registered_cummulative=$this->input->post('hcv_registered_cummulative');
				$hcv_registered=$this->security->xss_clean($this->input->post('hcv_registered'));


				$hcv_initiated_on_treatment_cumulative=$this->security->xss_clean($this->input->post('hcv_initiated_on_treatment_cumulative'));

				$hcv_initiated_on_treatment=$this->security->xss_clean($this->input->post('hcv_initiated_on_treatment'));

				$hcv_tranfered_in=$this->security->xss_clean($this->input->post('hcv_tranfered_in'));


				$hcv_treatment_completed_cummulative=$this->security->xss_clean($this->input->post('hcv_treatment_completed_cummulative'));

				$hcv_tranfered_out=$this->security->xss_clean($this->input->post('hcv_tranfered_out'));

				$hcv_treatment_stopped=$this->security->xss_clean($this->input->post('hcv_treatment_stopped'));

				$hcv_ltfu=$this->security->xss_clean($this->input->post('hcv_ltfu'));

				$hcv_missed_doses=$this->security->xss_clean($this->input->post('hcv_missed_doses'));

				$hcv_reffered=$this->security->xss_clean($this->input->post('hcv_reffered'));

				$hcv_death_reported=$this->security->xss_clean($this->input->post('hcv_death_reported'));

				$hcv_eligible_for_svr=$this->security->xss_clean($this->input->post('hcv_eligible_for_svr'));

				$hcv_svr_done=$this->security->xss_clean($this->input->post('hcv_svr_done'));

				$hcv_rna=$this->security->xss_clean($this->input->post('hcv_rna'));

				/* END */

				/*All chlildren data Start */

				$hcv_registered_cummulative_children=$this->security->xss_clean($this->input->post('hcv_registered_cummulative_children'));


				$hcv_registered_children=$this->security->xss_clean($this->input->post('hcv_registered_children'));


				$hcv_initiated_on_treatment_cumulative_children=$this->security->xss_clean($this->input->post('hcv_initiated_on_treatment_cumulative_children'));

				$hcv_initiated_on_treatment_children=$this->security->xss_clean($this->input->post('hcv_initiated_on_treatment_children'));

				$hcv_tranfered_in_children=$this->security->xss_clean($this->input->post('hcv_tranfered_in_children'));


				$hcv_treatment_completed_cummulative_children=$this->security->xss_clean($this->input->post('hcv_treatment_completed_cummulative_children'));

				$hcv_tranfered_out_children=$this->security->xss_clean($this->input->post('hcv_tranfered_out_children'));

				$hcv_treatment_stopped_children=$this->security->xss_clean($this->input->post('hcv_treatment_stopped_children'));

				$hcv_ltfu_children=$this->security->xss_clean($this->input->post('hcv_ltfu_children'));

				$hcv_missed_doses_children=$this->security->xss_clean($this->input->post('hcv_missed_doses_children'));

				$hcv_reffered_children=$this->security->xss_clean($this->input->post('hcv_reffered_children'));

				$hcv_death_reported_children=$this->security->xss_clean($this->input->post('hcv_death_reported_children'));

				$hcv_eligible_for_svr_children=$this->security->xss_clean($this->input->post('hcv_eligible_for_svr_children'));

				$hcv_svr_done_children=$this->security->xss_clean($this->input->post('hcv_svr_done_children'));

				$hcv_rna_children=$this->security->xss_clean($this->input->post('hcv_rna_children'));


				for ($i=0; $i <3 ; $i++) { 
				$insert_array = array('hcv_registered_cummulative' =>$hcv_registered_cummulative[$i],
								  'hcv_registered' =>$hcv_registered[$i] ,
								  'hcv_treatment_start_cummulative' =>$hcv_initiated_on_treatment_cumulative[$i] ,
								  'hcv_treatment_start' =>$hcv_initiated_on_treatment[$i] ,
								  'hcv_transferred_in' => $hcv_tranfered_in[$i],
								  'hcv_treatment_completed_cummulative' =>$hcv_treatment_completed_cummulative[$i] ,
								  'hcv_transferred_out_cummulative' =>$hcv_tranfered_out[$i] ,
								  'hcv_treatment_stopped' =>$hcv_treatment_stopped[$i] ,
								  'hcv_ltfu' =>$hcv_ltfu[$i],
								  'hcv_missed_doses' =>$hcv_missed_doses[$i] ,
								  'hcv_reffered' =>$hcv_reffered[$i] ,
								  'hcv_death_reported' =>$hcv_death_reported[$i] ,
								  'hcv_eligible_for_svr' =>$hcv_eligible_for_svr[$i],
								  'hcv_svr_undergone' =>$hcv_svr_done[$i] ,
								  'hcv_rna' =>$hcv_rna[$i],
								  'genderwise' =>$i+1,
								  'agewise' => 1,
								  'month' =>$month ,
								  'year' =>$year,
								  'Session_StateID' =>$loginData->State_ID,
								 
									);
				$data = array_merge($insert_array, $array2);
				//pr($data);
				if (empty($content['get_info_monthly_record'])) {
				$this->db->insert('tbl_offline_monthly_records',$data);
			}
			elseif (!empty($content['get_info_monthly_record'])) {
				$where=array('month'=>$month,'year'=>$year,'genderwise'=>$i+1,'agewise'=>1);
				$this->db->where($where);
				$this->db->update('tbl_offline_monthly_records',$data);
			}
				
				}

				
				$insert_children_array = array('hcv_registered_cummulative' =>$hcv_registered_cummulative_children,
										  'hcv_registered' =>$hcv_registered_children ,
										  'hcv_treatment_start_cummulative' =>$hcv_initiated_on_treatment_cumulative_children ,
										  'hcv_treatment_start' =>$hcv_initiated_on_treatment_children ,
										  'hcv_transferred_in' => $hcv_tranfered_in_children,
										  'hcv_treatment_completed_cummulative' =>$hcv_treatment_completed_cummulative_children ,
										  'hcv_transferred_out_cummulative' =>$hcv_tranfered_out_children ,
										  'hcv_treatment_stopped' =>$hcv_treatment_stopped_children ,
										  'hcv_ltfu' =>$hcv_ltfu_children,
										  'hcv_missed_doses' =>$hcv_missed_doses_children ,
										  'hcv_reffered' =>$hcv_reffered_children ,
										  'hcv_death_reported' =>$hcv_death_reported_children ,
										  'hcv_eligible_for_svr' =>$hcv_eligible_for_svr_children,
										  'hcv_svr_undergone' =>$hcv_svr_done_children ,
										  'hcv_rna' =>$hcv_rna_children,
										  'genderwise' =>4,
										  'agewise' => 2,
										  'month' =>$month ,
										  'year' =>$year,
										  'Session_StateID' =>$loginData->State_ID,
								 
									);
				$data_child = array_merge($insert_children_array, $array2);

				if (empty($content['get_info_monthly_record'])) {
				$this->db->insert('tbl_offline_monthly_records',$data_child);
			}
			elseif (!empty($content['get_info_monthly_record'])) {
				$this->db->where($where1);
				$this->db->update('tbl_offline_monthly_records',$data_child);
			}
			}



		}
		else
		{
			$filters1['id_search_state'] = 0;
			$filters1['id_input_district'] = 0;
			$filters1['id_mstfacility'] = 0;
			$month=date('m');
			$year=date('Y');
			$content['get_info_monthly_record']=$this->Offline_monthly_entry_Model->get_info_monthly_record($month,$year);
			
		}
		if (!empty($content['get_info_monthly_record'])) {
			//eecho $month;echo $year;
			$content['male']=$this->Offline_monthly_entry_Model->get_male_monthly_record($month,$year);
			$content['female']=$this->Offline_monthly_entry_Model->get_female_monthly_record($month,$year);
			$content['transgender']=$this->Offline_monthly_entry_Model->get_transgender_monthly_record($month,$year);
			$content['children']=$this->Offline_monthly_entry_Model->get_children_monthly_record($month,$year);
			}
//pr($content['children']);
		 $this->session->set_userdata('filters1', $filters1);
		  $sql = "select * from mststate where ".$sess_where." order by StateName ASC";
		$content['states'] = $this->db->query($sql)->result();

		 $sql = "select * from mstdistrict where ".$sess_mstdistrict."";
		$content['districts'] = $this->db->query($sql)->result();
		//pr($content['districts']);
		 $sql = "select * from mstfacility where  ".$sess_mstfacility."";
		$content['facilities'] = $this->db->query($sql)->result();	
		$content['subview'] = 'hcv_offline_entry';
		$this->load->view('admin/main_layout', $content);
	}

}