<?php

class Api extends CI_Controller {

	private $user;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_Model');
		$this->load->model('Chai_api_model');
		$this->load->model('Wkhtmltopdf_model');
		 $this->load->library('html2pdf');
		//error_reporting(1);
	}

	public function get_token()
	{
		$token = $this->Chai_api_model->get_token();
		echo $token;
	}


	private function check_auth_login($username, $password)
	{

		$this->db->where('lower(username)',$username);
		$result = $this->db->get('tblusers')->result();
		if (count($result) < 1) {
			return false;
		}

		$this->user = $result[0];

		if (hash('sha256',$this->user->password . $this->user->token) != $password) {
			return false;
		}

		if (strtotime(date("Y-m-d H:i:s")) > strtotime($this->user->token_valid_till)) {
			echo "ERROR: Token expired. Please reissue token";
			die();
		}

		return true;
	}

	private function getuserid($username, $password)
	{
		$sql = "select * from tblusers where username = '".$username."' and password = '".md5($password)."'";
		$res = $this->Common_Model->query_data($sql);

		return $res[0]->id_tblusers;
	}

	private function getfacilityid($username, $password)
	{
		$sql = "select * from tblusers where username = '".$username."' and password = '".md5($password)."'";
		$res = $this->Common_Model->query_data($sql);

		return $res[0]->id_mstfacility;
	}

	public function getMasters()
	{

		header("Content-type: application/json");

		if($this->input->post('username') === null || $this->input->post('password') === null)
		{
			echo json_encode("ERROR : You must send username and password with the request");
			die();
		}
		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$authlogin = $this->check_auth_login($username, $password);

		if(!$authlogin)
		{
			echo json_encode("Incorrect username and/or password");
		}
		else
		{
			$data = array();

			$sql = "select * from tblusers where username = '".$username."'";
			$tblusers = $this->Common_Model->query_data($sql);

			$query = "select * from tblusers where id_mstfacility = ?";
			$tblusers_all = $this->db->query($query,[$this->user->id_mstfacility])->result();

			//updating table with app version

			if($this->input->post('versioncode') == null)
			{
				$insert_array_versioncode = array(
					'latest_version_used' => 'version other than 5.0'
				);

				$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);
				echo json_encode("Please update your app");
				die();
			}
			else
			{
				$versioncode = $this->security->xss_clean($this->input->post('versioncode'));


				$insert_array_versioncode = array(
					'latest_version_used' => $versioncode
				);

				$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);


				if($versioncode != 'Version 1.0')
				{
					$insert_array_versioncode = array(
						'latest_version_used' => 'version other than 5.0'
					);

					$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);
					echo json_encode("Please update your app");
					die();
				}
			}


			$insert_array = array(
				"id_mstfacility"    => $tblusers[0]->id_mstfacility,
				"download_type"     => "master",
				"download_datetime" => date('Y-m-d H:i:s'),
			);
			$this->db->insert('master_download_log', $insert_array);

			$sql = "SELECT f.*, d.DistrictCd, s.StateCd FROM `mstfacility` f inner join mstdistrict d inner join mststate s on f.id_mststate = s.id_mststate and f.id_mstdistrict = d.id_mstdistrict where f.id_mststate=".$tblusers[0]->State_ID." ";
			$facility = $this->Common_Model->query_data($sql);

			$sql = "select * from mststate where id_mststate = ".$tblusers[0]->State_ID."";
			$states = $this->Common_Model->query_data($sql);

			$sql = "select * from mstdistrict where id_mststate = ".$tblusers[0]->State_ID."";
			$districts = $this->Common_Model->query_data($sql);

			$sql = "select * from mstblock where isactive = 1 and id_mststate= ".$tblusers[0]->State_ID."  order by BlockName";
			$blocks = $this->Common_Model->query_data($sql);

			$sql = "select * from mstfacilitycontacts";
			$mstfacilitycontacts = $this->Common_Model->query_data($sql);

			//$sql = "select * from mst_designation_list where id_mstfacility=".$tblusers[0]->id_mstfacility."";
			 $sql = "SELECT designation_listId,Designation FROM `mst_designation_list` where id_mstfacility=".$tblusers[0]->id_mstfacility."  union Select '999' as designation_listId,'Peer Support' as Designation order by designation_listId asc ";
			$mst_designation_list = $this->Common_Model->query_data($sql);

			$sql = "select * from mstlookup";
			$mstlookup = $this->Common_Model->query_data($sql);

			$sql = "select * from tblregimenrules";
			$tblregimenrules = $this->Common_Model->query_data($sql);

			
			$sql = "select * from mst_regimen_hepb where is_deleted = 0";
			$mst_regimen_hepb = $this->Common_Model->query_data($sql);

			$sql = "select * from mst_drug_strength_hepb where is_deleted = 0";
			$mst_drug_strength_hepb = $this->Common_Model->query_data($sql);


			$sql = "select * from mstappstateconfiguration";
			$mstappstateconfiguration = $this->Common_Model->query_data($sql);

			/*$sql = "SELECT * FROM `mst_diagnostics_labs` where trim(lcase(FacilityType)) = 'art'";
			$mst_diagnostics_labs = $this->Common_Model->query_data($sql);*/

			$sql = "select * from mst_drugs where is_deleted = 0";
			$mst_drugs = $this->Common_Model->query_data($sql);


			/*check start mtc TC USER*/
		$sql = "select f.is_Mtc,FacilityType from tblusers as u inner join mstfacility f on u.id_mstfacility=f.id_mstfacility where f.id_mstfacility=".$tblusers[0]->id_mstfacility." and u.id_tblusers=".$tblusers[0]->id_tblusers." ";
		$content['fac_user'] = $this->db->query($sql)->result();
		
		if($content['fac_user'][0]->is_Mtc==1){
		$sql = "SELECT * FROM `mst_regimen` where is_deleted = 0";
		$mst_regimen = $this->db->query($sql)->result();

		$sql = "SELECT * FROM mst_regimen_hepb where is_deleted = 0";
		$mst_regimen_hepb = $this->Common_Model->query_data($sql);

		
		$sql = "SELECT
			`id_mst_drug_strength`,
			`id_mst_drug`,
			strength AS strength_int,
			CONCAT(strength, ' mg') AS strength,
			`is_mtc`,
			`is_deleted`
			FROM
			`mst_drug_strength`
			WHERE
			is_deleted = 0
			ORDER BY
			id_mst_drug,
			strength_int";
			$mst_drug_strength = $this->Common_Model->query_data($sql);


	}

	elseif($content['fac_user'][0]->FacilityType=='ART'){
		$sql = "SELECT * FROM `mst_regimen` where is_deleted = 0";
		$mst_regimen = $this->db->query($sql)->result();
		
		$sql = "SELECT
			`id_mst_drug_strength`,
			`id_mst_drug`,
			strength AS strength_int,
			CONCAT(strength, ' mg') AS strength,
			`is_mtc`,
			`is_deleted`
			FROM
			`mst_drug_strength`
			WHERE
			is_deleted = 0
			ORDER BY
			id_mst_drug,
			strength_int";
			$mst_drug_strength = $this->Common_Model->query_data($sql);


	}
	else{

		$sql = "SELECT * FROM `mst_regimen_hepb` where is_mtc = 0 and is_deleted = 0";
			$mst_regimen_hepb = $this->Common_Model->query_data($sql);

		$sql = "SELECT * FROM `mst_regimen` where is_mtc = 0 and is_deleted = 0";
		$mst_regimen = $this->db->query($sql)->result();

		
		$sql = "SELECT
			`id_mst_drug_strength`,
			`id_mst_drug`,
			strength AS strength_int,
			CONCAT(strength, ' mg') AS strength,
			`is_mtc`,
			`is_deleted`
			FROM
			`mst_drug_strength`
			WHERE
			is_deleted = 0 and is_mtc=0
			ORDER BY
			id_mst_drug,
			strength_int";
			$mst_drug_strength = $this->Common_Model->query_data($sql);

	}
/*check end mtc TC USER*/


			/*$sql = "SELECT
			`id_mst_drug_strength`,
			`id_mst_drug`,
			strength AS strength_int,
			CONCAT(strength, ' mg') AS strength,
			`is_deleted`
			FROM
			`mst_drug_strength`
			WHERE
			is_deleted = 0
			ORDER BY
			id_mst_drug,
			strength_int";
			$mst_drug_strength = $this->Common_Model->query_data($sql);

			$sql = "select * from mst_regimen where is_deleted = 0";
			$mst_regimen = $this->Common_Model->query_data($sql);*/

			$sql = "SELECT * FROM `MSTRole`";
			$MSTRole = $this->Common_Model->query_data($sql);

			$sql = "select * from mst_regimen_drugs where is_deleted = 0";
			$mst_regimen_drugs = $this->Common_Model->query_data($sql);

			$sql = "select * from mst_regimen_rules where is_deleted = 0";
			$mst_regimen_rules = $this->Common_Model->query_data($sql);

			$sql = "SELECT * FROM `mst_past_regimen` where is_deleted = 0";
			$mst_past_regimen = $this->Common_Model->query_data($sql);

			$sql = "select * from mst_medical_specialists where is_deleted = 0 and (id_mstfacility = ".$tblusers[0]->id_mstfacility." or id_mstfacility = 0)";
			$mst_medical_specialists = $this->Common_Model->query_data($sql);

			$sql = "SELECT count(*) as count FROM `tblpatient` where IsTablet=1 and id_mstfacility = ".$tblusers[0]->id_mstfacility;
			$patient_count = $this->Common_Model->query_data($sql);

			$sql = "select * from mstblock where old_new = 1";
			$mst_health_blocks = $this->Common_Model->query_data($sql);

			$mstappstateconfiguration[0]->patient_count = $patient_count[0]->count;

			if($username == 'tab_admin')
			{
				$district_wise = $this->cascade_download();
				$data['district_wise']            = $district_wise;

				$all_reports = $this->get_all_reports();
				$data['all_reports']            = $all_reports;
			}

			$data['mststates']                = $states;
			$data['mstdistrict']              = $districts;
			$data['mstblock']                 = $blocks;
			$data['mstfacility']              = $facility;
			$data['mstfacilitycontacts']      = $mstfacilitycontacts;
			$data['mstlookup']                = $mstlookup;
			$data['mst_designation_list']     = $mst_designation_list;
			$data['tblusers']                 = $tblusers_all;
			$data['mstappstateconfiguration'] = $mstappstateconfiguration;
			$data['mst_diagnostics_labs']     = $mst_diagnostics_labs;
			$data['mst_drugs']                = $mst_drugs;
			$data['mst_drug_strength']        = $mst_drug_strength;
			$data['mst_regimen']              = $mst_regimen;
			$data['mst_regimen_drugs']        = $mst_regimen_drugs;
			$data['mst_regimen_rules']        = $mst_regimen_rules;
			$data['mst_medical_specialists']  = $mst_medical_specialists;
			$data['mst_past_regimen']         = $mst_past_regimen;
			$data['mst_health_blocks']        = $mst_health_blocks;
			$data['MSTRole']                  = $MSTRole;
			$data['tblregimenrules']          = $tblregimenrules;
			$data['mst_regimen_hepb']         = $mst_regimen_hepb;
			$data['mst_drug_strength_hepb']   = $mst_drug_strength_hepb;

			echo json_encode($data);
		}
	}

	public function downloadData()
	{
		header("Content-type: application/json");


		if($this->input->post('username') === null || $this->input->post('password') === null)
		{
			echo json_encode(array("ERROR : Please send username and password along with the request"));
			die();
		}

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$authlogin = $this->check_auth_login($username, $password);

		if(!$authlogin)
		{
			echo json_encode(array("Username/Password is incorrect"));
			die();
		}
		else
		{

			$sql = "select * from tblusers where username = '".$username."'";
			$tblusers = $this->Common_Model->query_data($sql);

			//updating table with app version

			if($this->input->post('versioncode') == null)
			{
				$insert_array_versioncode = array(
					'latest_version_used' => 'version other than 5.0'
				);

				$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);
				echo json_encode("Please update your app");
				die();
			}
			else
			{
				$versioncode = $this->security->xss_clean($this->input->post('versioncode'));

				$insert_array_versioncode = array(
					'latest_version_used' => $versioncode
				);

				$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);


				if($versioncode != 'Version 1.0')
				{
					$insert_array_versioncode = array(
						'latest_version_used' => 'version other than 5.0'
					);

					$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);
					echo json_encode("Please update your app");
					die();
				}
			}

			 $current_pass = $this->security->xss_clean($this->input->post('CurrentPass'));
			 if ($this->input->post('CurrentPass')==0){
			$offset = $current_pass * 500;
		}else{
			$offset = 0;
		}

			// $userid = $this->getuserid($username, $password);
			$userid = $this->user->id_tblusers;
			// $facilityid = $this->getfacilityid($username, $password);
			$facilityid = $this->user->id_mstfacility;

			// echo json_encode($facilityid); die();

			$data                 = array();
			$tblpatientaddltest   = array();
			$tblpatientcirrohosis = array();
			$tblpatientfu         = array();
			$tblpatientvisit      = array();
			$tblpatientSVR        = array();
			$tblstock             = array();

			 $sql_patientguids = "select patientguid from tblpatient where id_mstfacility = ".$facilityid." limit 500 offset ".$offset;
			$res_patientguids = $this->Common_Model->query_data($sql_patientguids);
//print_r($res_patientguids);exit;
			$res_patientguids_array = array();
//print_r($res_patientguids);exit();
			if (!empty($res_patientguids)) {
			foreach ($res_patientguids as $row) {
				
					
				$res_patientguids_array[] = "'".$row->patientguid."'";
			}
			}

			if ($this->input->post('CurrentPass')==0) {
				 $patientguids = implode($res_patientguids_array,',');
			}
			
			else{
				$patientguids = $this->input->post('CurrentPass');
			}

			$sql = "select
				    `id_tblpatient`,
				    `id_mstfacility`,
				    `linelist_synced`,
				    `Serial`,
				    `PatientGUID`,
				    `OPD_Id`,
				    `UID_Prefix`,
				    `UID_Num`,
				    `Aadhaar`,
				    `Status`,
				    `Gender`,
				    `FatherHusband`,
				    `Add1`,
				    `VillageTown`,
				    `District`,
				    `PIN`,
				    `Mobile`,
				      DATE_FORMAT(
     case `T_DLL_01_Date` when '0000-00-00' then '' else T_DLL_01_Date end,
    '%Y-%m-%d'
) AS T_DLL_01_Date,
				    `T_AntiHCV01_Result`,
				     DATE_FORMAT(
     case `T_DLL_01_VLC_Date` when '0000-00-00' then '' else T_DLL_01_VLC_Date end,
    '%Y-%m-%d'
) AS T_DLL_01_VLC_Date,
				    `T_DLL_01_VLCount`,
				    `T_DLL_01_VLC_Result`,
				    `V1_Haemoglobin`,
				    `V1_Albumin`,
				    `V1_Bilrubin`,
				    `V1_INR`,
				    `V1_Cirrhosis`,
				     DATE_FORMAT(
     case `Cirr_TestDate` when '0000-00-00' then '' else Cirr_TestDate end,
    '%Y-%m-%d'
) AS Cirr_TestDate,
				    `Cirr_Encephalopathy`,
				    `Cirr_Ascites`,
				    `Cirr_VaricealBleed`,
				    `ChildScore`,
				    `Result`,
				    `T_Initiation`,
				    `T_RmkDelay`,
				    `T_Regimen`,
				    `T_NoPillStart`,
				    `T_VisitPlan`,
				    `DrugSideEffect`,
				    `ETR_HCVViralLoad_Dt`,
				    `ETR_HCVViralLoadCount`,
				    `ETR_Result`,
				    `SVR12W_HCVViralLoad_Dt`,
				    `SVR12W_HCVViralLoadCount`,
				    `SVR12W_Result`,
				    `SVR12W_LabID`,
				    `Adherence`,
				    `SVR_TreatmentStatus`,
				    `Age`,
				    `FirstName`,
				    `T_DurationValue`,
				    `SVR_LabName`,
				    `InitiationDt`,
				    `ETRDt`,
				    `Pregnant`,
				    `DeliveryDt`,
				    `HCVHistory`,
				    `HCVPastTreatment`,
				    `HCVPastOutcome`,
				    `V1_Platelets`,
				      DATE_FORMAT(
     case `CreatedOn` when '0000-00-00' then '' else CreatedOn end,
    '%Y-%m-%d'
) AS CreatedOn,
				    `CreatedBy`,
				     DATE_FORMAT(
     case `UpdatedOn` when '0000-00-00' then '' else UpdatedOn end,
    '%Y-%m-%d'
) AS UpdatedOn,
				    `UpdatedBy`,
				    `UploadedOn`,
				    `UploadedBy`,
				    `IsTablet`,
				    `IsEdited`,
				    `IsUploaded`,
				    `IsDeleted`,
				     DATE_FORMAT(
     case `AdvisedSVRDate` when '0000-00-00' then '' else AdvisedSVRDate end,
    '%Y-%m-%d'
) AS AdvisedSVRDate,
				    `ETR_PillsLeft`,
				    `T_DurationOther`,
				    `Current_Visitdt`,
				    `Next_Visitdt`,
				    `MF1`,
				    `MF2`,
				    `MF3`,
				    `MF4`,
				    `Block`,
				    `PastTreatmentDuration`,
				    `PastDuration`,
				    `IsMobile_Landline`,
				    `fragment3editreason`,
				    `NextVisitPurpose`,
				    `ETRComments`,
				    `SVRComments`,
				    `IsETRDone`,
				     DATE_FORMAT(
     case `ETRTestDate` when '0000-00-00' then '' else ETRTestDate end,
    '%Y-%m-%d'
) AS ETRTestDate,

				    `PreviousTreatingHospital`,
				    `PreviousUIDNumber`,
				    DATE_FORMAT(
     case `ETRAutoupdate` when '0000-00-00' then '' else ETRAutoupdate end,
    '%Y-%m-%d'
) AS ETRAutoupdate,
				    `NewPatient`,
				    `PatientType`,
				    `PastTreatment`,
				    `PastFacility`,
				    `PastUID`,
				    `Weight`,
				    `CKDStage`,
				    `NWeeksCompleted`,
				     DATE_FORMAT(
     case `LastPillDate` when '0000-00-00' then '' else LastPillDate end,
    '%Y-%m-%d'
) AS LastPillDate,
				    `PastRegimen`,
				    `PreviousDuration`,
				    `InitiateDoctor`,
				    `ETRDoctor`,
				    `SVRDoctor`,
				    `InterruptReason`,
				    `InterruptToStage`,
				    `VisitInterrupted`,
				    `TransferRequest`,
				    `TransferRequestAccepted`,
				    `TransferUID`,
				    `TransferFromFacility`,
				    `TransferToFacility`,
				    `id_mst_medical_specialists`,
				    `HospitalName`,
				    `IsSMSConsent`,
				    `OtherRisk`,
				    `ETRLFUreason`,
				    `InitiateDoctorOther`,
				    `ETRDoctorOther`,
				    `SVRDoctorOther`,
				    DATE_FORMAT(
     case `VLSampleCollectionDate` when '0000-00-00' then '' else VLSampleCollectionDate end,
    '%Y-%m-%d'
) AS VLSampleCollectionDate,
				    `VLStorageTemp`,
				    DATE_FORMAT(
     case `VLTransportDate` when '0000-00-00' then '' else VLTransportDate end,
    '%Y-%m-%d'
) AS VLTransportDate,
				    `VLLabID`,
				     DATE_FORMAT(
     case `VLRecieptDate` when '0000-00-00' then '' else VLRecieptDate end,
    '%Y-%m-%d'
) AS VLRecieptDate,
				    `Relation`,
				    `State`,
				    `AntiHCV`,
				    `HCVRapid`,
				     DATE_FORMAT(
     case `HCVRapidDate` when '0000-00-00' then '' else HCVRapidDate end,
    '%Y-%m-%d'
) AS HCVRapidDate,
				    `HCVRapidResult`,
				    `HCVRapidPlace`,
				    `HCVRapidLabID`,
				    `HCVRapidLabOther`,
				    `HCVElisa`,
				    DATE_FORMAT(
     case `HCVElisaDate` when '0000-00-00' then '' else HCVElisaDate end,
    '%Y-%m-%d'
) AS HCVElisaDate,
				    `HCVElisaResult`,
				    `HCVElisaPlace`,
				    `HCVElisaLabID`,
				    `HCVElisaLabOther`,
				    `HCVOther`,
				    `HCVOtherName`,
				     DATE_FORMAT(
     case `HCVOtherDate` when '0000-00-00' then '' else HCVOtherDate end,
    '%Y-%m-%d'
) AS HCVOtherDate,
				    `HCVOtherResult`,
				    `HCVOtherPlace`,
				    `HCVOtherLabID`,
				    `HCVLabOther`,
				    `LgmAntiHAV`,
				    HAVRapid,
				     DATE_FORMAT(
     case `HAVRapidDate` when '0000-00-00' then '' else HAVRapidDate end,
    '%Y-%m-%d'
) AS HAVRapidDate,
				    `HAVRapidResult`,
				    `HAVRapidPlace`,
				    `HAVRapidLabID`,
				    `HAVRapidLabOther`,
				    `HAVElisa`,
				     DATE_FORMAT(
     case `HAVElisaDate` when '0000-00-00' then '' else HAVElisaDate end,
    '%Y-%m-%d'
) AS HAVElisaDate,

				    `HAVElisaResult`,
				    `HAVElisaPlace`,
				    `HAVElisaLabID`,
				    `HAVElisaLabOther`,
				    `HAVOther`,
				    `HAVOtherName`,
				    DATE_FORMAT(
     case `HAVOtherDate` when '0000-00-00' then '' else HAVOtherDate end,
    '%Y-%m-%d'
) AS HAVOtherDate,

				    `HAVOtherResult`,
				    `HAVOtherPlace`,
				    `HAVOtherLabID`,
				    `HAVLabOther`,
				    `HbsAg`,
				    `HBSRapid`,
				    DATE_FORMAT(
     case `HBSRapidDate` when '0000-00-00' then '' else HBSRapidDate end,
    '%Y-%m-%d'
) AS HBSRapidDate,
				    `HBSRapidResult`,
				    `HBSRapidPlace`,
				    `HBSRapidLabID`,
				    `HBSRapidLabOther`,
				    `HBSElisa`,
				    DATE_FORMAT(
     case `HBSElisaDate` when '0000-00-00' then '' else HBSElisaDate end,
    '%Y-%m-%d'
) AS HBSElisaDate,
				    `HBSElisaResult`,
				    `HBSElisaPlace`,
				    `HBSElisaLabID`,
				    `HBSElisaLabOther`,
				    `HBSOther`,
				    `HBSOtherName`,
				    DATE_FORMAT(
     case `HBSOtherDate` when '0000-00-00' then '' else HBSOtherDate end,
    '%Y-%m-%d'
) AS HBSOtherDate,
				    `HBSOtherResult`,
				    `HBSOtherPlace`,
				    `HBSOtherLabID`,
				    `HBSLabOther`,
				    `LgmAntiHBC`,
				    `HBCRapid`,
				     DATE_FORMAT(
     case `HBCRapidDate` when '0000-00-00' then '' else HBCRapidDate end,
    '%Y-%m-%d'
) AS HBCRapidDate,
				    `HBCRapidResult`,
				    `HBCRapidPlace`,
				    `HBCRapidLabID`,
				    `HBCRapidLabOther`,
				    `HBCElisa`,
				     DATE_FORMAT(
     case `HBCElisaDate` when '0000-00-00' then '' else HBCElisaDate end,
    '%Y-%m-%d'
) AS HBCElisaDate,
				    `HBCElisaResult`,
				    `HBCElisaPlace`,
				    `HBCElisaLabID`,
				    `HBCElisaLabOther`,
				    `HBCOther`,
				    `HBCOtherName`,
				     DATE_FORMAT(
     case `HBCOtherDate` when '0000-00-00' then '' else HBCOtherDate end,
    '%Y-%m-%d'
) AS HBCOtherDate,
				    `HBCOtherResult`,
				    `HBCOtherPlace`,
				    `HBCOtherLabID`,
				    `HBCLabOther`,
				    `LgmAntiHEV`,
				    `HEVRapid`,
				     DATE_FORMAT(
     case `HEVRapidDate` when '0000-00-00' then '' else HEVRapidDate end,
    '%Y-%m-%d'
) AS HEVRapidDate,
				    `HEVRapidResult`,
				    `HEVRapidPlace`,
				    `HEVRapidLabID`,
				    `HEVRapidLabOther`,
				    `HEVElisa`,
				     DATE_FORMAT(
     case `HEVElisaDate` when '0000-00-00' then '' else HEVElisaDate end,
    '%Y-%m-%d'
) AS HEVElisaDate,
				    `HEVElisaResult`,
				    `HEVElisaPlace`,
				    `HEVElisaLabID`,
				    `HEVElisaLabOther`,
				    `HEVOther`,
				    `HEVOtherName`,
				     DATE_FORMAT(
     case `HEVOtherDate` when '0000-00-00' then '' else HEVOtherDate end,
    '%Y-%m-%d'
) AS HEVOtherDate,
				    `HEVOtherResult`,
				    `HEVOtherPlace`,
				    `HEVOtherLabID`,
				    `HEVLabOther`,
				    `VLTransporterName`,
				    `VLTransporterDesignation`,
				    `VLReceiverName`,
				    `VLReceiverDesignation`,
				     DATE_FORMAT(
     case `SVRDrawnDate` when '0000-00-00' then '' else SVRDrawnDate end,
    '%Y-%m-%d'
) AS SVRDrawnDate,
				    `SVRStorageTemp`,
				    DATE_FORMAT(
     case `SVRTransportDate` when '0000-00-00' then '' else SVRTransportDate end,
    '%Y-%m-%d'
) AS SVRTransportDate,
				    `SVRLabID`,
				    `SVRTransporterName`,
				    `SVRTransporterDesignation`,
				     DATE_FORMAT(
     case `SVRReceiptDate` when '0000-00-00' then '' else SVRReceiptDate end,
    '%Y-%m-%d'
) AS SVRReceiptDate,
				    `SVRReceiverName`,
				    `SVRReceiverDesignation`,
				    `V1_Creatinine`,
				    `V1_EGFR`,
				    `BreastFeeding`,
				    `ART_Regimen`,
				    `Ribavirin`,
				    `IsReferal`,
				    `ReferingDoctor`,
				    `ReferTo`,
				     DATE_FORMAT(
     case `ReferalDate` when '0000-00-00' then '' else ReferalDate end,
    '%Y-%m-%d'
) AS ReferalDate,
				    `PrescribingFacility`,
				    `PrescribingDoctor`,
				    DATE_FORMAT(
     case `PrescribingDate` when '0000-00-00' then '' else PrescribingDate end,
    '%Y-%m-%d'
) AS PrescribingDate,
				    `ReferingDoctorOther`,
				    `PrescribingDoctorOther`,
				    `MF5`,
				    `MF6`,
				    `fragment3_1editreason`,
				    `fragment3_2editreason`,
				    `IsAgeMonths`,
				     DATE_FORMAT(
     case `basic_info_updated_on` when '0000-00-00' then '' else basic_info_updated_on end,
    '%Y-%m-%d'
) AS basic_info_updated_on,
				    `basic_info_updated_by`,
				     DATE_FORMAT(
     case `screening_updated_on` when '0000-00-00' then '' else screening_updated_on end,
    '%Y-%m-%d'
) AS screening_updated_on,
				    `screening_updated_by`,
				     DATE_FORMAT(
     case `confirmation_updated_on` when '0000-00-00' then '' else confirmation_updated_on end,
    '%Y-%m-%d'
) AS confirmation_updated_on,
				    `confirmation_updated_by`,
				     DATE_FORMAT(
     case `baseline_updated_on` when '0000-00-00' then '' else baseline_updated_on end,
    '%Y-%m-%d'
) AS baseline_updated_on,
				    `baseline_updated_by`,
				     DATE_FORMAT(
     case `special_updated_on` when '0000-00-00' then '' else special_updated_on end,
    '%Y-%m-%d'
) AS special_updated_on,
				    `special_updated_by`,
				     DATE_FORMAT(
     case `treatment_updated_on` when '0000-00-00' then '' else treatment_updated_on end,
    '%Y-%m-%d'
) AS treatment_updated_on,

				    `treatment_updated_by`,
				    `etr_updated_on`,
				    `etr_updated_by`,
				     DATE_FORMAT(
     case `svr_updated_on` when '0000-00-00' then '' else svr_updated_on end,
    '%Y-%m-%d'
) AS svr_updated_on,
				    `svr_updated_by`,
				    `MF7`,
				    `TreatmentUpdatedBy`,
				     DATE_FORMAT(
     case `TreatmentUpdatedOn` when '0000-00-00' then '' else TreatmentUpdatedOn end,
    '%Y-%m-%d'
) AS TreatmentUpdatedOn,
				    `fragment3_3editreason`,
				    `VLLabID_Other`,
				    `PastRegimen_Other`,
				    `SCRemarks`,
				    `PrescriptionPlace`,
				    `IsVLSampleStored`,
				    `IsVLSampleTransported`,
				    `VLTransportTemp`,
				    `VLSCRemarks`,
				    `VLResultRemarks`,
				    `VLHepB`,
				    `VLHepC`,
				    `VLReferredLab`,
				    `ALT`,
				    `AST`,
				    `AST_ULN`,
				    `DispensationPlace`,
				    `CirrhosisStatus`,
				    `IsSVRSampleStored`,
				    `IsSVRSampleTransported`,
				    `SVRTransportTemp`,
				    `SVRTransportRemark`,
				    `BVLTransportTemp`,
				    `IsBVLSampleStored`,
				    `IsBVLSampleTransported`,
				    `BVLSCRemarks`,
				    `BVLResultRemarks`,
				   
				    DATE_FORMAT(
     case `BVLSampleCollectionDate` when '0000-00-00' then '' else BVLSampleCollectionDate end,
    '%Y-%m-%d'
) AS BVLSampleCollectionDate,
				     DATE_FORMAT(
     case `BVLTransportDate` when '0000-00-00' then '' else BVLTransportDate end,
    '%Y-%m-%d'
) AS BVLTransportDate,

				    `BVLStorageTemp`,
				    `BVLTransporterName`,
				    `BVLReceiverName`,
				    `BVLTransporterDesignation`,
				    `BVLReceiverDesignation`,
				    `T_DLL_01_BVLC_Result`,
				    `BVLLabID`,
				    `BVLLabID_Other`,
				    `T_DLL_01_BVLCount`,
				         DATE_FORMAT(
     case `T_DLL_01_BVLC_Date` when '0000-00-00' then '' else T_DLL_01_BVLC_Date end,
    '%Y-%m-%d'
) AS T_DLL_01_BVLC_Date,
				   
				     DATE_FORMAT(
     case `BVLRecieptDate` when '0000-00-00' then '' else BVLRecieptDate end,
    '%Y-%m-%d'
) AS BVLRecieptDate,

				    `PDispensation`,
				    `PTState`,
				    `PTYear`,
				    `Storage_days_hrs`,
				    `Storageduration`,
				    `ReferredDispensationPlace`,
				    `BStorage_days_hrs`,
				    `BStorageduration`,
				    `IsSampleAccepted`,
				    `IsBSampleAccepted`,
				    `RejectionReason`,
				    `BRejectionReason`,
				    `RejectionReasonOther`,
				    `BRejectionReasonOther`,
				    `NAdherenceReason`,
				    `NAdherenceReasonOther`,
				    `LMP`,
				    `Risk`,
				    `DeathReason`,
					`LFUReason`,
					`ExperiencedCategory`,
					`OutsideYear`,
					`RecommendedRegimen`,
					`RecommendedDuration`,
					`SideEffectValue`,
					`DurationReason`,
					`DistrictOther`,
					`BlockOther`,
					`Refer_FacilityHAV`,
					`Refer_HigherFacilityHAV`,
					`Refer_FacilityHEV`,
					`Refer_HigherFacilityHEV`,
					`CaseType`,
					`InterruptReasonOther`,
					`IsSVRSampleAccepted`,
					`SVRRejectionReason`,
					`SVRRejectionReasonOther`,
					`SVR12W_LabOther`

			from tblpatient where IsTablet=1 and patientguid in (".$patientguids.")";

			$patients = $this->Common_Model->query_data($sql);


			$sql = "SELECT
    `id_tblpatientcirrohosis`,
    `PatientID`,
    `PatientGUID`,
     DATE_FORMAT(
     case `Clinical_US_Dt` when '0000-00-00' then '' else Clinical_US_Dt end,
    '%Y-%m-%d'
) AS Clinical_US_Dt,

      DATE_FORMAT(
     case `Fibroscan_Dt` when '0000-00-00' then '' else Fibroscan_Dt end,
    '%Y-%m-%d'
) AS Fibroscan_Dt,
    `Fibroscan_LSM`,
     DATE_FORMAT(
     case `Prescribing_Dt` when '0000-00-00' then '' else Prescribing_Dt end,
    '%Y-%m-%d'
) AS Prescribing_Dt,
    `APRI_PC`,
    `APRI_Score`,
     DATE_FORMAT(
     case `LastTest_Dt` when '0000-00-00' then '' else LastTest_Dt end,
    '%Y-%m-%d'
) AS LastTest_Dt,
    `FIB4_PC`,
    `FIB4_FIB4`,
    `Fibroscan`,
    `APRI`,
    `FIB4`,
    `Clinical_US`,
     DATE_FORMAT(
     case `CreatedOn` when '0000-00-00' then '' else CreatedOn end,
    '%Y-%m-%d'
) AS CreatedOn,
    `CreatedBy`,
    `UpdatedBy`,
     DATE_FORMAT(
     case `UpdatedOn` when '0000-00-00' then '' else UpdatedOn end,
    '%Y-%m-%d'
) AS UpdatedOn,
    `IsEdited`,
    `IsUploaded`,
    `IsTablet`,
    `IsDeleted`,
     DATE_FORMAT(
     case `UploadedOn` when '0000-00-00' then '' else UploadedOn end,
    '%Y-%m-%d'
) AS UploadedOn,
    `UploadedBy`,
    `id_mstfacility`,
    `GenotypeTest_Result`
FROM
    `tblpatientcirrohosis` where IsTablet=1 and patientguid in (".$patientguids.")";

			$res_patientcirrohosis = $this->Common_Model->query_data($sql);


			foreach ($res_patientcirrohosis as $row) {

				$tblpatientcirrohosis[] = $row;

			}

			$sql = "select
			`id_tblpatientvisit`,
			replace(`PatientID`,'\'',' ') as PatientID,
			replace(`PatientGUID`,'\'',' ') as PatientGUID,
			replace(`PatientVisitGUID`,'\'',' ') as PatientVisitGUID,
			`VisitNo`,
			 DATE_FORMAT(
     case `Visit_Dt` when '0000-00-00' then '' else Visit_Dt end,
    '%Y-%m-%d'
) AS Visit_Dt,
			`ExpectedVIsits`,
			`PillsLeft`,
			`PillsIssued`,
			`DaysSinceLastVisit`,
			`PillsConsumed`,
			`Adherence`,
			 DATE_FORMAT(
     case `NextVisit_Dt` when '0000-00-00' then '' else NextVisit_Dt end,
    '%Y-%m-%d'
) AS NextVisit_Dt,
			`Regimen`,
			DATE_FORMAT(
     case `CreatedOn` when '0000-00-00' then '' else CreatedOn end,
    '%Y-%m-%d'
) AS CreatedOn,
			`CreatedBy`,
			DATE_FORMAT(
     case `UpdatedOn` when '0000-00-00' then '' else UpdatedOn end,
    '%Y-%m-%d'
) AS UpdatedOn,
			`UpdatedBy`,
			`IsEdited`,
			`IsUploaded`,
			`IsTablet`,
			`IsDeleted`,
			DATE_FORMAT(
     case `UploadedOn` when '0000-00-00' then '' else UploadedOn end,
    '%Y-%m-%d'
) AS UploadedOn,
			`UploadedBy`,
			`Haemoglobin`,
			`PlateletCount`,
			replace(`SideEffect`,'\'',' ') as SideEffect,
			`DummyFlag`,
			replace(`Comments`,'\'',' ') as Comments,
			`VisitFlag`,
			`id_mstfacility`,
			`Doctor`,
   		    `DoctorOther`,
   		    `SideEffectValue`,
   		    `PDispensation`,
   		    `NAdherenceReason`,
   		    `NAdherenceReasonOther`
			from tblpatientvisit where IsTablet=1 and patientguid in (".$patientguids.")";

			$res_patientvisit = $this->Common_Model->query_data($sql);


			foreach ($res_patientvisit as $row) {

				$tblpatientvisit[] = $row;

			}

			$sql = "SELECT
    `id`,
    `PatientID`,
    `PatientGUID`,
    `PatientTestVisitGUID`,
    `Visit_Date`,
    `Visit_No`,
    `V1_Haemoglobin`,
    `V1_Albumin`,
    `V1_Bilrubin`,
    `V1_INR`,
    `ALT`,
    `AST`,
    `AST_ULN`,
    `V1_Platelets`,
    `V1_Creatinine`,
    `Remarks`,
    DATE_FORMAT(
     case `CreatedOn` when '0000-00-00' then '' else CreatedOn end,
    '%Y-%m-%d'
) AS CreatedOn,

    `CreatedBy`,
    DATE_FORMAT(
     case `UpdatedOn` when '0000-00-00' then '' else UpdatedOn end,
    '%Y-%m-%d'
) AS UpdatedOn,
    `UpdatedBy`,
    `IsUploaded`,
    `IsEdited`,
    `IsDeleted`,
    `IsTablet`,
    `UploadedOn`,
    `UploadedBy`
FROM
    `tblpatientaddltest` where IsTablet=1 and patientguid in (".$patientguids.")";

			$res_tblpatientaddltest = $this->Common_Model->query_data($sql);


			foreach ($res_tblpatientaddltest as $row) {

				$tblpatientaddltest[] = $row;

			}

			$sql = "SELECT
    `id`,
    `PatientID`,
    `PatientGUID`,
    `PatientVisitFUGUID`,
    `VisitNo`,
     DATE_FORMAT(
     case `VisitDate` when '0000-00-00' then '' else VisitDate end,
    '%Y-%m-%d'
) AS VisitDate,
    `TreatDoctor`,
    `IsReferred`,
    `RefDoctor`,
    `RefDoctorOther`,
    `ReferredTo`,
    `SideEffect`,
    `SideEffectOther`,
    `Remarks`,
    `CreatedBy`,
     DATE_FORMAT(
     case `CreatedOn` when '0000-00-00' then '' else CreatedOn end,
    '%Y-%m-%d'
) AS CreatedOn,
     DATE_FORMAT(
     case `UpdatedOn` when '0000-00-00' then '' else UpdatedOn end,
    '%Y-%m-%d'
) AS UpdatedOn,
    `UpdatedBy`,
    `IsEdited`,
    `IsUploaded`,
    `IsDeleted`,
    `UploadedOn`,
    `UploadedBy`,
    `IsTablet`,
    `TreatingDoctorOther`
FROM
    `tblpatientfu` where IsTablet=1 and patientguid in (".$patientguids.")";

			$res_tblpatientfu = $this->Common_Model->query_data($sql);


			foreach ($res_tblpatientfu as $row) {

				$tblpatientfu[] = $row;

			}

			$query = "SELECT * FROM `tblRiskProfile` where patientguid in (".$patientguids.")";
			$tblRiskProfile = $this->db->query($query)->result();

			$query = "SELECT * FROM `tblpatientsvrsample` where PatientGUID in (".$patientguids.")";
			$tblpatientsvrsample = $this->db->query($query)->result();


			$query = "SELECT 

 			id_tblpatient_regimen_drug_data,
            patientguid,
            visitguid,
            visit_no,
            id_mst_drugs,
            id_mst_drug_strength,
            id_mstfacility,
            created_by,
            DATE_FORMAT(
     case `created_on` when '0000-00-00' then '' else created_on end,
    '%Y-%m-%d'
) AS created_on,
             DATE_FORMAT(
     case `updated_on` when '0000-00-00' then '' else updated_on end,
    '%Y-%m-%d'
) AS updated_on,
            updated_by
			FROM `tblpatient_regimen_drug_data` where  patientguid in (".$patientguids.")";
			$tblpatient_regimen_drug_data = $this->db->query($query)->result();

			$sql_svr_table = "select 

			 `tblpatientSVR`, 
		      `PatientGUID`,
		        DATE_FORMAT(
     case `SVRDate` when '0000-00-00' then '' else SVRDate end,
    '%Y-%m-%d'
) AS SVRDate,

		      `SVRResult`, 
		      `SVRVLC` ,
		      `svr_from`, 
		       DATE_FORMAT(
     case `confirmatory_test_date` when '0000-00-00' then '' else confirmatory_test_date end,
    '%Y-%m-%d'
) AS confirmatory_test_date,

		      `confirmatory_test_result`, 
		      `confirmatory_test_viral_load`, 
		      `confirmatory_from`,
		       DATE_FORMAT(
     case `genotype_test_date` when '0000-00-00' then '' else genotype_test_date end,
    '%Y-%m-%d'
) AS genotype_test_date,

		      `genotype_test_result`, 
		      `genotype_from` 

			from tblpatientsvr where  patientguid in (".$patientguids.")";
			$tblpatientSVR = $this->Common_Model->query_data($sql_svr_table);

			$this->db->where('id_mstfacility', $facilityid);
			$tblSVRfollowup = $this->db->get('tblSVRfollowup')->result();


			$data['tblpatient']                   = $patients;
			$data['tblpatientaddltest']           = $tblpatientaddltest;
			$data['tblpatientcirrohosis']         = $tblpatientcirrohosis;
			$data['tblpatientfu']                 = $tblpatientfu;
			$data['tblpatientvisit']              = $tblpatientvisit;
			$data['tblstock']                     = $tblstock;
			$data['tblpatientSVR']                = $tblpatientSVR;
			$data['tblSVRfollowup']               = $tblSVRfollowup;
			$data['tblRiskProfile']               = $tblRiskProfile;
			$data['tblpatient_regimen_drug_data'] = $tblpatient_regimen_drug_data;
			$data['tblpatientsvrsample'] = $tblpatientsvrsample;

			echo json_encode($data);
		}
	}

	public function downloadsvrdata()
	{
		header("Content-type: application/json");


		if($this->input->post('username') === null || $this->input->post('password') === null)
		{
			echo json_encode(array("ERROR : Please send username and password along with the request"));
			die();
		}

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$authlogin = $this->check_auth_login($username, $password);

		if(!$authlogin)
		{
			echo json_encode(array("Username/Password is incorrect"));
			die();
		}
		else
		{
			$facilityid = $this->getfacilityid($username, $password);
			$sql_svr_table = "SELECT
			s.`tblpatientSVR`,
			s.`PatientGUID`,
			s.`SVRDate`,
			s.`SVRResult`,
			s.`SVRVLC`,
			s.`confirmatory_test_date`,
			s.`confirmatory_test_result`,
			s.`confirmatory_test_viral_load`,
			s.`genotype_test_date`,
			s.`genotype_test_result`
			FROM
			tblpatientsvr s
			INNER JOIN tblpatient p ON
			p.patientguid = s.patientguid
			WHERE
			p.id_mstfacility = ".$facilityid;
			$tblpatientSVR = $this->Common_Model->query_data($sql_svr_table);

			// $query_svr_from_art = "SELECT
			// dv.id_tbldiagnostic_labs_data_viral_load,
			// p.PatientGUID,
			// dv.viral_load_test_date,
			// dv.viral_load_count,
			// dv.viral_load_result
			// FROM
			// `tbldiagnostic_labs_data_viral_load` dv
			// INNER JOIN tblpatient p ON
			// dv.referred_facility = p.id_mstfacility AND dv.uid_num = p.UID_Num
			// WHERE
			// 1";
			// $res_svr_from_art = $this->Common_Model->query_data($query_svr_from_art);


			// $data['tblpatientSVR'] = $tblpatientSVR + $res_svr_from_art;
			$data['tblpatientSVR'] = $tblpatientSVR;
			echo json_encode($data);
		}
	}
	public function uploadData()
	{
		//print_r('bkkkkkkss');exit;
		header("Content-Type: application/json");

		if($this->input->post('username') === null || $this->input->post('password') === null)
		{
			echo json_encode(array("ERROR : Please send username and password along with the request"));
			die();
		}

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$authlogin = $this->check_auth_login($username, $password);

		if(!$authlogin)
		{
			echo json_encode(array("Username/Password is incorrect"));
			die();
		}
		else
		{

			$sql = "select * from tblusers where username = '".$username."'";
			$tblusers = $this->Common_Model->query_data($sql);

			//updating table with app version

			if($this->input->post('versioncode') == null)
			{
				$insert_array_versioncode = array(
					'latest_version_used' => 'version other than 5.0'
				);

				$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);
				echo json_encode("Please update your app");
				die();
			}
			else
			{
				$versioncode = $this->security->xss_clean($this->input->post('versioncode'));

				$insert_array_versioncode = array(
					'latest_version_used' => $versioncode,
				);

				$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);


				if($versioncode != 'Version 1.0')
				{
					$insert_array_versioncode = array(
						'latest_version_used' => 'version other than 5.0'
					);

					$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);
					echo json_encode("Please update your app");
					die();
				}
			}

			$data = $this->input->post('data');
			$data_array = json_decode($data);
//print_r($data_array);exit;
			if($data_array == null)
			{
				echo json_encode("ERROR: Please send properly formatted json");
			}
			else
			{
				$res = $this->Chai_api_model->uploaddata();
				if($res === null)
				{
					echo json_encode("success");
				}
				else
				{
					echo json_encode($res);
				}
			}
		}
	}

	public function imageUpload()
	{

		if($this->input->post('username') === null || $this->input->post('password') === null)
		{
			echo json_encode("Please send username/password along with the request");
			die();
		}

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$authlogin = $this->check_auth_login($username, $password);

		if(!$authlogin)
		{
			echo json_encode(array("Username/Password is incorrect"));
			die();
		}
		else
		{

			// $userid = $this->getuserid($username, $password);
			$userid = $this->user->id_tblusers;
			$data = $this->input->post('data');
			$filename = $this->input->post('filename');

			$file_name = $this->base64_to_jpeg($data, $filename);

			echo 'success';

		}
	}

	private function base64_to_jpeg($imagebase64, $filename)
	{
		$image = explode(',', $imagebase64);

		$file = fopen(FCPATH.'application/uploads/treatment_card_images/'.$filename, 'wb');
		if($file)
		{

			fwrite($file, base64_decode($image[1]));
		}
		else {
			echo "unable to get file handler";
		}
		fclose($file);

		return $filename;
	}

	public function pass_count()
	{
		if($this->input->post('username') === null || $this->input->post('password') === null)
		{
			echo json_encode("Please send username/password along with the request");
			die();
		}

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$authlogin = $this->check_auth_login($username, $password);

		if(!$authlogin)
		{
			echo json_encode(array("Username/Password is incorrect"));
			die();
		}
		else
		{
			$userid = $this->user->id_tblusers;
			$facilityid = $this->user->id_mstfacility;

			// making log entry

			$insert_array = array(
				"id_mstfacility"    => $facilityid,
				"download_type"     => "data",
				"download_datetime" => date('Y-m-d H:i:s'),
			);
			$this->db->insert('master_download_log', $insert_array);

			$sql = "select count(*) as count from tblpatient where id_mstfacility = ".$facilityid;
			$res = $this->Common_Model->query_data($sql);

			if(count($res) == 0)
			{
				$passes = 0;
			}
			else
			{
				$passes = floor($res[0]->count/500)+1;
			}

			echo json_encode($passes);
		}
	}


// to download data of all facilities

	public function downloadAllData()
	{
		header("Content-type: application/json");


		if($this->input->post('username') === null || $this->input->post('password') === null)
		{
			echo json_encode(array("ERROR : Please send username and password along with the request"));
			die();
		}

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$authlogin = $this->check_auth_login($username, $password);

		if(!$authlogin)
		{
			echo json_encode(array("Username/Password is incorrect"));
			die();
		}
		else
		{

			$sql = "select * from tblusers where username = '".$username."'";
			$tblusers = $this->Common_Model->query_data($sql);

			//updating table with app version

			if($this->input->post('versioncode') == null)
			{
				$insert_array_versioncode = array(
					'latest_version_used' => 'version other than 5.0'
				);

				$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);
				echo json_encode("Please update your app");
				die();
			}
			else
			{
				$versioncode = $this->security->xss_clean($this->input->post('versioncode'));

				$insert_array_versioncode = array(
					'latest_version_used' => $versioncode
				);

				$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);


				if($versioncode != 'Version 1.0')
				{
					$insert_array_versioncode = array(
						'latest_version_used' => 'version other than 5.0'
					);

					$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);
					echo json_encode("Please update your app");
					die();
				}
			}

			$current_pass = $this->security->xss_clean($this->input->post('CurrentPass'));

			$offset = $current_pass * 500;

			$userid = $this->user->id_tblusers;
			$facilityid = $this->user->id_mstfacility;

			// echo json_encode($facilityid); die();

			$data                 = array();
			$tblpatientaddltest   = array();
			$tblpatientcirrohosis = array();
			$tblpatientfu         = array();
			$tblpatientvisit      = array();
			$tblpatientSVR        = array();
			$tblstock             = array();
			$tblpatientSVRSample  = array();

			$sql_patientguids = "select patientguid from tblpatient order by id_mstfacility,id_tblpatient limit 500 offset ".$offset;
			$res_patientguids = $this->Common_Model->query_data($sql_patientguids);

			$res_patientguids_array = array();

			foreach ($res_patientguids as $row) {

				$res_patientguids_array[] = "'".$row->patientguid."'";

			}
			$patientguids = implode($res_patientguids_array,',');

			$sql = "select
			`id_tblpatient`,
			`id_mstfacility`,
			`Serial`,
			replace(`PatientGUID`,'\'',' ') as PatientGUID,
			replace(`UID_Prefix`,'\'',' ') as UID_Prefix,
			`UID_Num`,
			replace(`Aadhaar`,'\'',' ') as Aadhaar,
			`Status`,
			`Gender`,
			`DOBAv`,
			`DOB`,
			`AgeAsOn`,
			`RiskFactor`,
			replace(`FatherHusband`,'\'',' ') as FatherHusband,
			replace(`FH_Name`,'\'',' ') as FH_Name,
			`Urban_Rural`,
			replace(`Add1`,'\'',' ') as Add1,
			replace(`Add2`,'\'',' ') as Add2,
			replace(`POBox`,'\'',' ') as POBox,
			replace(`Street`,'\'',' ') as Street,
			replace(`Locality`,'\'',' ') as Locality,
			replace(`VillageTown`,'\'',' ') as VillageTown,
			`District`,
			`Block`,
			replace(`PIN`,'\'',' ') as PIN,
			replace(`Mobile`,'\'',' ') as Mobile,
			replace(`AltContact`,'\'',' ') as AltContact,
			replace(`AltContactWith`,'\'',' ') as AltContactWith,
			replace(`T_DLL_01_Date`,'\'',' ') as T_DLL_01_Date,
			`T_AntiHCV01_Date`,
			`T_AntiHCV01_Result`,
			replace(`LabName`, '\'',' ') as LabName,
			replace(`LabID_Wservice`, '\'',' ') as LabID_Wservice,
			`T_DLL_01_VLC_Date`,
			`T_DLL_01_VLCount`,
			replace(`T_DLL_01_VLC_Result`, '\'',' ') as T_DLL_01_VLC_Result,
			replace(`V1_ArrivalHospital`, '\'',' ') as V1_ArrivalHospital,
			replace(`MedicalSpecialist`, '\'',' ') as MedicalSpecialist,
			`V1_Haemoglobin`,
			`V1_Albumin`,
			`V1_Bilrubin`,
			`V1_INR`,
			`V1_Cirrhosis`,
			`Cirr_TestDate`,
			`Cirr_Encephalopathy`,
			`Cirr_Ascites`,
			`Cirr_VaricealBleed`,
			`ChildScore`,
			`Result`,
			`T_Initiation`,
			`T_RmkDelay`,
			replace(`T_Regimen`, '\'',' ') as T_Regimen,
			`T_Duration`,
			replace(`T_NoPillStart`, '\'',' ') as T_NoPillStart,
			replace(`T_VisitPlan`, '\'',' ') as T_VisitPlan,
			replace(`DrugSideEffect`, '\'',' ') as DrugSideEffect,
			`ETR_HCVViralLoad_Dt`,
			`ETR_HCVViralLoadCount`,
			`ETR_Result`,
			`SVR12W_HCVViralLoad_Dt`,
			`SVR12W_HCVViralLoadCount`,
			`SVR12W_Result`,
			`SVR12W_LabID`,
			replace(`Adherence`, '\'',' ') as Adherence,
			`SVR_TreatmentStatus`,
			replace(`User_entry`, '\'',' ') as User_entry,
			replace(`Entry_Point`, '\'',' ') as Entry_Point,
			replace(`Entry_IP`, '\'',' ') as Entry_IP,
			`EntryDate`,
			`Age`,
			replace(`Title`, '\'',' ') as Title,
			replace(`FirstName`, '\'',' ') as FirstName,
			replace(`LastName`, '\'',' ') as LastName,
			replace(`LabID_Manual`, '\'',' ') as LabID_Manual,
			`SampleCollectionDt`,
			`SampleReceiveDt`,
			`SampleResultDt`,
			`T_DurationValue`,
			replace(`SVR_LabName`, '\'',' ') as SVR_LabName,
			`InitiationDt`,
			`ETRDt`,
			`Pregnant`,
			`DeliveryDt`,
			`HCVHistory`,
			`HCVPastTreatment`,
			`HCVPastOutcome`,
			`HCVSVRDt`,
			`HIV`,
			`CKD`,
			`Diabetes`,
			`Hypertension`,
			`HBV`,
			`V1_Platelets`,
			`ETR_PillsLeft`,
			`AdvisedSVRDate`,
			`T_DurationOther`,
			`Current_Visitdt`,
			`Next_Visitdt`,
			`IsMobile_Landline`,
			`CreatedOn`,
			`CreatedBy`,
			`UpdatedOn`,
			`UpdatedBy`,
			`UploadedOn`,
			`UploadedBy`,
			`IsTablet`,
			`IsEdited`,
			`IsUploaded`,
			`IsDeleted`,
			`PastTreatmentDuration`,
			`PastTreatment`,
			`PastDuration`,
			`fragment3editreason`,
			`NextVisitPurpose`,
			replace(`ETRComments`, '\'',' ') as ETRComments,
			replace(`SVRComments`, '\'',' ') as SVRComments,
			`IsETRDone`,
			`ETRTestDate`,
			`PatientCategory`,
			replace(`JailNo`, '\'',' ') as JailNo,
			`CardType`,
			`MF1`,
			`MF2`,
			`MF3`,
			`MF4`,
			`PreviousTreatingHospital`,
			`PreviousUIDNumber`,
			`CaseType`,
			`InterruptReasonOther`,
			`IsSVRSampleAccepted`,
			`SVRRejectionReason`,
			`SVRRejectionReasonOther`,
			`SVR12W_LabOther`
			from tblpatient where patientguid in (".$patientguids.")";

			$patients = $this->Common_Model->query_data($sql);


			$sql = "select
			`id_tblpatientcirrohosis`,
			replace(`PatientID`, '\'',' ') as PatientID,
			replace(`PatientGUID`, '\'',' ') as PatientGUID,
			replace(`PatientCirrohosisVisitGUID`, '\'',' ') as PatientCirrohosisVisitGUID,
			`Clinical_US_Dt`,
			replace(`Clinical_US_USG`, '\'',' ') as Clinical_US_USG,
			`Fibroscan_Dt`,
			`Fibroscan_LSM`,
			`Fibroscan_Remarks`,
			`APRI_Dt`,
			`APRI_AST`,
			`APRI_ASTN`,
			`APRI_PC`,
			`APRI_Score`,
			`APRI_Remarks`,
			`FIB4_Dt`,
			`FIB4_AST`,
			`FIB4_ALT`,
			`FIB4_PC`,
			`FIB4_FIB4`,
			`FIB4_Remarks`,
			`GenotypeTest_Dt`,
			`GenotypeTest_Result`,
			`Fibroscan`,
			`APRI`,
			`FIB4`,
			`Clinical_US`,
			`CreatedOn`,
			`CreatedBy`,
			`UpdatedBy`,
			`UpdatedOn`,
			`IsEdited`,
			`IsUploaded`,
			`IsTablet`,
			`IsDeleted`,
			`UploadedOn`,
			`UploadedBy`,
			`id_mstfacility`
			from tblpatientcirrohosis where patientguid in (".$patientguids.")";

			$res_patientcirrohosis = $this->Common_Model->query_data($sql);


			foreach ($res_patientcirrohosis as $row) {

				$tblpatientcirrohosis[] = $row;

			}

			$sql = "select
			`id_tblpatientvisit`,
			replace(`PatientID`,'\'',' ') as PatientID,
			replace(`PatientGUID`,'\'',' ') as PatientGUID,
			replace(`PatientVisitGUID`,'\'',' ') as PatientVisitGUID,
			`VisitNo`,
			`Visit_Dt`,
			`ExpectedVIsits`,
			`PillsLeft`,
			`PillsIssued`,
			`DaysSinceLastVisit`,
			`PillsConsumed`,
			`Adherence`,
			`NextVisit_Dt`,
			`Regimen`,
			`CreatedOn`,
			`CreatedBy`,
			`UpdatedOn`,
			`UpdatedBy`,
			`IsEdited`,
			`IsUploaded`,
			`IsTablet`,
			`IsDeleted`,
			`UploadedOn`,
			`UploadedBy`,
			`Haemoglobin`,
			`PlateletCount`,
			replace(`SideEffect`,'\'',' ') as SideEffect,
			`DummyFlag`,
			replace(`Comments`,'\'',' ') as Comments,
			`VisitFlag`,
			`id_mstfacility`
			from tblpatientvisit where patientguid in (".$patientguids.")";

			$res_patientvisit = $this->Common_Model->query_data($sql);


			foreach ($res_patientvisit as $row) {

				$tblpatientvisit[] = $row;

			}



			$sql_svr_table = "select * from tblpatientsvr where patientguid in (".$patientguids.")";
			$tblpatientSVR = $this->Common_Model->query_data($sql_svr_table);

			$sql_svr_table1 = "select * from tblpatientSVRSample where PatientGUID in (".$patientguids.")";
			$tblpatientSVRSample = $this->Common_Model->query_data($sql_svr_table1);


			$data['tblpatient'] = $patients;
			$data['tblpatientaddltest'] = $tblpatientaddltest;
			$data['tblpatientcirrohosis'] = $tblpatientcirrohosis;
			$data['tblpatientfu'] = $tblpatientfu;
			$data['tblpatientvisit'] = $tblpatientvisit;
			$data['tblstock'] = $tblstock;
			$data['tblpatientSVR'] = $tblpatientSVR;
			$data['tblpatientSVRSample'] = $tblpatientSVRSample;

			echo json_encode($data);
		}
	}

	public function getAllMasters()
	{

		header("Content-type: application/json");

		if($this->input->post('username') === null || $this->input->post('password') === null)
		{
			echo json_encode("ERROR : You must send username and password with the request");
			die();
		}
		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$authlogin = $this->check_auth_login($username, $password);

		if(!$authlogin)
		{
			echo json_encode("Incorrect username and/or password");
		}
		else
		{
			$data = array();

			$sql = "select * from tblusers where username = '".$username."'";
			$tblusers = $this->Common_Model->query_data($sql);

			//updating table with app version

			if($this->input->post('versioncode') == null)
			{
				$insert_array_versioncode = array(
					'latest_version_used' => 'version other than 5.0'
				);

				$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);
				echo json_encode("Please update your app");
				die();
			}
			else
			{
				$versioncode = $this->security->xss_clean($this->input->post('versioncode'));


				$insert_array_versioncode = array(
					'latest_version_used' => $versioncode
				);

				$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);


				if($versioncode != 'Version 1.0')
				{
					$insert_array_versioncode = array(
						'latest_version_used' => 'version other than 5.0'
					);

					$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);
					echo json_encode("Please update your app");
					die();
				}
			}



			$sql = "SELECT f.*, d.DistrictCd, s.StateCd FROM `mstfacility` f inner join mstdistrict d inner join mststate s on f.id_mststate = s.id_mststate and f.id_mstdistrict = d.id_mstdistrict";
			$facility = $this->Common_Model->query_data($sql);

			$sql = "select * from mststate";
			$states = $this->Common_Model->query_data($sql);

			$sql = "select * from mstdistrict";
			$districts = $this->Common_Model->query_data($sql);

			$sql = "select * from mstblock";
			$blocks = $this->Common_Model->query_data($sql);

			$sql = "select * from mstfacilitycontacts";
			$mstfacilitycontacts = $this->Common_Model->query_data($sql);

			$sql = "select * from mstlookup";
			$mstlookup = $this->Common_Model->query_data($sql);

			$sql = "select * from mstvillage where id_mstdistrict";
			$mstvillage = $this->Common_Model->query_data($sql);

			$sql = "select * from mstappstateconfiguration";
			$mstappstateconfiguration = $this->Common_Model->query_data($sql);

			$sql = "SELECT count(*) as count FROM `tblpatient` ";
			$patient_count = $this->Common_Model->query_data($sql);

			$sql = "select * from mstblock where old_new = 1";
			$mst_health_blocks = $this->Common_Model->query_data($sql);

			$sql = "select * from tblregimenrules";
			$tblregimenrules = $this->Common_Model->query_data($sql);

			$mstappstateconfiguration[0]->patient_count = $patient_count[0]->count;

			$data['mststates'] = $states;
			$data['mstdistrict'] = $districts;
			$data['mstblock'] = $blocks;
			$data['mstvillage'] = $mstvillage;
			$data['mstfacility'] = $facility;
			$data['mstfacilitycontacts'] = $mstfacilitycontacts;
			$data['mstlookup'] = $mstlookup;
			$data['tblusers'] = $tblusers;
			$data['mstappstateconfiguration'] = $mstappstateconfiguration;			
			$data['mst_health_blocks'] = $mst_health_blocks;
			$data['tblregimenrules'] = $tblregimenrules;

			echo json_encode($data);
		}
	}

	public function pass_count_all_data()
	{
		if($this->input->post('username') === null || $this->input->post('password') === null)
		{
			echo json_encode("Please send username/password along with the request");
			die();
		}

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$authlogin = $this->check_auth_login($username, $password);

		if(!$authlogin)
		{
			echo json_encode(array("Username/Password is incorrect"));
			die();
		}
		else
		{
			$userid = $this->user->id_tblusers;
			$facilityid = $this->user->id_mstfacility;

			// making log entry

			$insert_array = array(
				"id_mstfacility"    => $facilityid,
				"download_type"     => "data",
				"download_datetime" => date('Y-m-d H:i:s'),
			);
			$this->db->insert('master_download_log', $insert_array);

			$sql = "select count(*) as count from tblpatient";
			$res = $this->Common_Model->query_data($sql);

			$passes = floor($res[0]->count/500)+1;
			echo json_encode($passes);
		}
	}

	// public function download_single_record_art()
	// {
	// 	header("Content-type: application/json");

	// 	if($this->input->post('username') === null || $this->input->post('password') === null)
	// 	{
	// 		echo json_encode("Please send username/password along with the request");
	// 		die();
	// 	}

	// 	$username                = $this->security->xss_clean($this->input->post('username'));
	// 	$password                = $this->security->xss_clean($this->input->post('password'));
	// 	$id_mst_diagnostics_labs = $this->security->xss_clean($this->input->post('id_mst_diagnostics_labs'));
	// 	$uid                     = $this->security->xss_clean($this->input->post('uid'));

	// 	$authlogin = $this->check_auth_login($username, $password);

	// 	if(!$authlogin)
	// 	{
	// 		echo json_encode(array("Username/Password is incorrect"));
	// 		die();
	// 	}
	// 	else
	// 	{
	// 		$flag = 0;

	// 		$userid     = $this->getuserid($username, $password);
	// 		$facilityid = $this->getfacilityid($username, $password);

	// 		$sql = "SELECT * FROM `tbldiagnostic_labs_data_screening` where id_mst_diagnostics_labs = ".$id_mst_diagnostics_labs." and uid_num = ".$uid." and is_deleted = 0 limit 1";

	// 		$res = $this->Common_Model->query_data($sql);
	// 		$data['tbldiagnostic_labs_data_screening'] = $res;

	// 		if($res[0]->anti_hcv_test_date == '0000-00-00')
	// 		{
	// 			$res[0]->anti_hcv_test_date = null;
	// 		}

	// 		if($res[0]->confirmatory_sample_collection_date == '0000-00-00')
	// 		{
	// 			$res[0]->confirmatory_sample_collection_date = null;
	// 		}

	// 		// $sql = "SELECT * FROM `tbldiagnostic_labs_data_viral_load` where id_mst_diagnostics_labs = ".$id_mst_diagnostics_labs." and uid_num = ".$uid." limit 1";

	// 		$sql = "SELECT
	// 		a.`id_tbldiagnostic_labs_data_viral_load`,
	// 		a.`id_mst_diagnostics_labs`,
	// 		a.`uid_prefix`,
	// 		a.`uid_num`,
	// 		a.`treatment_stage`,
	// 		a.`sample_receipt_date`,
	// 		d.`viral_load_test_date`,
	// 		d.`viral_load_count`,
	// 		d.`viral_load_result`,
	// 		c.SVRDate,
	// 		c.SVRViralLoad,
	// 		c.SVRResult,
	// 		b.`genotype_test_date`,
	// 		b.`genotype_test_result`,
	// 		a.`result_report_date`,
	// 		a.`test_name`,
	// 		a.`test_type`,
	// 		a.`testing_center_name`,
	// 		a.`referred_facility`,
	// 		a.`created_on`,
	// 		a.`created_by`,
	// 		a.`updated_on`,
	// 		a.`updated_by`,
	// 		a.`is_deleted`
	// 		FROM
	// 		(SELECT * FROM `tbldiagnostic_labs_data_viral_load` group by uid_prefix, uid_num) a
	// 		LEFT JOIN(
	// 		SELECT
	// 		uid_num,
	// 		uid_prefix,
	// 		genotype_test_date,
	// 		genotype_test_result
	// 		FROM
	// 		tbldiagnostic_labs_data_viral_load
	// 		WHERE
	// 		test_type = 2 and is_deleted = 0
	// 		) b
	// 		ON
	// 		b.uid_prefix = a.uid_prefix AND b.uid_num = a.uid_num
	// 		LEFT JOIN(
	// 		SELECT
	// 		uid_num,
	// 		uid_prefix,
	// 		viral_load_test_date AS SVRDate,
	// 		viral_load_count AS SVRViralLoad,
	// 		viral_load_result AS SVRResult,
	// 		treatment_stage
	// 		FROM
	// 		tbldiagnostic_labs_data_viral_load
	// 		WHERE
	// 		test_type = 1 AND is_deleted = 0 and treatment_stage = 'T'
	// 		) c
	// 		ON
	// 		c.uid_prefix = a.uid_prefix AND c.uid_num = a.uid_num
	// 		LEFT JOIN(
	// 		SELECT
	// 		uid_num,
	// 		uid_prefix,
	// 		viral_load_test_date,
	// 		viral_load_count,
	// 		viral_load_result,
	// 		treatment_stage
	// 		FROM
	// 		tbldiagnostic_labs_data_viral_load
	// 		WHERE
	// 		test_type = 1 and is_deleted = 0 and treatment_stage = 'D'
	// 		) d
	// 		ON
	// 		d.uid_prefix = a.uid_prefix AND d.uid_num = a.uid_num
	// 		where a.uid_num = ".$uid." and id_mst_diagnostics_labs = ".$id_mst_diagnostics_labs."  and a.is_deleted = 0";
	// 						// echo $sql; die();
	// 		$res_viral_and_geno = $this->Common_Model->query_data($sql);

	// 			// 			$sql = "SELECT
	// 			//     *
	// 			// FROM
	// 			//     tbldiagnostic_labs_data_viral_load
	// 			// WHERE
	// 			//     test_type = 2 AND genotype_test_date IS NOT NULL AND viral_load_test_date IS NULL  AND is_deleted = 0 AND id_mst_diagnostics_labs = ".$id_mst_diagnostics_labs." and uid_num = ".$uid." and id_tbldiagnostic_labs_data_viral_load not in (SELECT
	// 			//     b.`id_tbldiagnostic_labs_data_viral_load`
	// 			// FROM
	// 			//     `tbldiagnostic_labs_data_viral_load` a
	// 			// LEFT JOIN(
	// 			//     SELECT
	// 			//     id_tbldiagnostic_labs_data_viral_load,
	// 			//         uid_num,
	// 			//         uid_prefix,
	// 			//         genotype_test_date,
	// 			//         genotype_test_result
	// 			//     FROM
	// 			//         tbldiagnostic_labs_data_viral_load
	// 			//     WHERE
	// 			//         test_type = 2 and is_deleted = 0
	// 			// ) b
	// 			// ON
	// 			//     b.uid_prefix = a.uid_prefix AND b.uid_num = a.uid_num
	// 			// WHERE
	// 			//     a.test_type = 1 and a.is_deleted = 0 and b.id_tbldiagnostic_labs_data_viral_load is not null)";
	// 			// 			$res1_only_geno = $this->Common_Model->query_data($sql);

	// 			// 			$res1 = array_merge($res1_viral_and_geno, $res1_only_geno);


	// 		$data['tbldiagnostic_labs_data_viral_load'] = $res_viral_and_geno;

	// 		// if($res1[0]->sample_receipt_date == '0000-00-00')
	// 		// {
	// 		// 	$res1[0]->sample_receipt_date = null;
	// 		// }

	// 		// if($res1[0]->viral_load_test_date == '0000-00-00')
	// 		// {
	// 		// 	$res1[0]->viral_load_test_date = null;
	// 		// }

	// 		// if($res1[0]->genotype_test_date == '0000-00-00')
	// 		// {
	// 		// 	$res1[0]->genotype_test_date = null;
	// 		// }

	// 		// if($res1[0]->result_report_date == '0000-00-00')
	// 		// {
	// 		// 	$res1[0]->result_report_date = null;
	// 		// }

	// 		// if(count($res) == 0 && count($res1) == 0)
	// 		// {
	// 		// 	echo 'unsuccessful';
	// 		// }
	// 		// else
	// 		// {
	// 		// }
	// 		echo json_encode($data);
	// 	}
	// }

	// public function download_referred_to_data()
	// {
	// 	header("Content-type: application/json");

	// 	if($this->input->post('username') === null || $this->input->post('password') === null)
	// 	{
	// 		echo json_encode("Please send username/password along with the request");
	// 		die();
	// 	}

	// 	$username = $this->security->xss_clean($this->input->post('username'));
	// 	$password = $this->security->xss_clean($this->input->post('password'));

	// 	$authlogin = $this->check_auth_login($username, $password);

	// 	if(!$authlogin)
	// 	{
	// 		echo json_encode(array("Username/Password is incorrect"));
	// 		die();
	// 	}
	// 	else
	// 	{
	// 		$userid     = $this->getuserid($username, $password);
	// 		$facilityid = $this->getfacilityid($username, $password);

	// 		// screening data

	// 		$sql = "SELECT * FROM `tbldiagnostic_labs_data_screening` where facility_referred_to = ".$facilityid;
	// 		$res = $this->Common_Model->query_data($sql);

	// 		$data['tbldiagnostic_labs_data_screening'] = $res;

	// 		foreach ($res as $row) {
	// 			if($row->anti_hcv_test_date == '0000-00-00')
	// 			{
	// 				$row->anti_hcv_test_date = null;
	// 			}

	// 			if($row->confirmatory_sample_collection_date == '0000-00-00')
	// 			{
	// 				$row->confirmatory_sample_collection_date = null;
	// 			}
	// 		}


	// 		// viral load and genotype data

	// 		// fetching all the records with viral load along with genotype

	// 		$sql = "SELECT
	// 		a.`id_tbldiagnostic_labs_data_viral_load`,
	// 		a.`id_mst_diagnostics_labs`,
	// 		a.`uid_prefix`,
	// 		a.`uid_num`,
	// 		a.`treatment_stage`,
	// 		a.`sample_receipt_date`,
	// 		d.`viral_load_test_date`,
	// 		d.`viral_load_count`,
	// 		d.`viral_load_result`,
	// 		c.SVRDate,
	// 		c.SVRViralLoad,
	// 		c.SVRResult,
	// 		b.`genotype_test_date`,
	// 		b.`genotype_test_result`,
	// 		a.`result_report_date`,
	// 		a.`test_name`,
	// 		a.`test_type`,
	// 		a.`testing_center_name`,
	// 		a.`referred_facility`,
	// 		a.`created_on`,
	// 		a.`created_by`,
	// 		a.`updated_on`,
	// 		a.`updated_by`,
	// 		a.`is_deleted`
	// 		FROM
	// 		(SELECT * FROM `tbldiagnostic_labs_data_viral_load` group by uid_prefix, uid_num) a
	// 		LEFT JOIN(
	// 		SELECT
	// 		uid_num,
	// 		uid_prefix,
	// 		genotype_test_date,
	// 		genotype_test_result
	// 		FROM
	// 		tbldiagnostic_labs_data_viral_load
	// 		WHERE
	// 		test_type = 2 and is_deleted = 0
	// 		) b
	// 		ON
	// 		b.uid_prefix = a.uid_prefix AND b.uid_num = a.uid_num
	// 		LEFT JOIN(
	// 		SELECT
	// 		uid_num,
	// 		uid_prefix,
	// 		viral_load_test_date AS SVRDate,
	// 		viral_load_count AS SVRViralLoad,
	// 		viral_load_result AS SVRResult,
	// 		treatment_stage
	// 		FROM
	// 		tbldiagnostic_labs_data_viral_load
	// 		WHERE
	// 		test_type = 1 AND is_deleted = 0 and treatment_stage = 'T'
	// 		) c
	// 		ON
	// 		c.uid_prefix = a.uid_prefix AND c.uid_num = a.uid_num
	// 		LEFT JOIN(
	// 		SELECT
	// 		uid_num,
	// 		uid_prefix,
	// 		viral_load_test_date,
	// 		viral_load_count,
	// 		viral_load_result,
	// 		treatment_stage
	// 		FROM
	// 		tbldiagnostic_labs_data_viral_load
	// 		WHERE
	// 		test_type = 1 and is_deleted = 0 and treatment_stage = 'D'
	// 		) d
	// 		ON
	// 		d.uid_prefix = a.uid_prefix AND d.uid_num = a.uid_num
	// 		where referred_facility = ".$facilityid;
	// 		$res_must_have_viral_load_test = $this->Common_Model->query_data($sql);

	// 						// fetching the records with only genotype

	// 			// 			$sql = "SELECT
	// 			//     *
	// 			// FROM
	// 			//     tbldiagnostic_labs_data_viral_load
	// 			// WHERE
	// 			//     test_type = 2 AND genotype_test_date IS NOT NULL AND viral_load_test_date IS NULL  AND is_deleted = 0 AND referred_facility =".$facilityid." and id_tbldiagnostic_labs_data_viral_load not in (SELECT
	// 			//     b.`id_tbldiagnostic_labs_data_viral_load`
	// 			// FROM
	// 			//     `tbldiagnostic_labs_data_viral_load` a
	// 			// LEFT JOIN(
	// 			//     SELECT
	// 			//     id_tbldiagnostic_labs_data_viral_load,
	// 			//         uid_num,
	// 			//         uid_prefix,
	// 			//         genotype_test_date,
	// 			//         genotype_test_result
	// 			//     FROM
	// 			//         tbldiagnostic_labs_data_viral_load
	// 			//     WHERE
	// 			//         test_type = 2 and is_deleted = 0
	// 			// ) b
	// 			// ON
	// 			//     b.uid_prefix = a.uid_prefix AND b.uid_num = a.uid_num
	// 			// WHERE
	// 			//     a.test_type = 1 and a.is_deleted = 0 and referred_facility = 1 and b.id_tbldiagnostic_labs_data_viral_load is not null)";
	// 			// 			$res_only_genotype = $this->Common_Model->query_data($sql);

	// 						// $res = array_merge($res_must_have_viral_load_test, $res_only_genotype);

	// 		foreach ($res_must_have_viral_load_test as $row) {
	// 			if($row->sample_receipt_date == '0000-00-00')
	// 			{
	// 				$row->sample_receipt_date = null;
	// 			}

	// 			if($row->viral_load_test_date == '0000-00-00')
	// 			{
	// 				$row->viral_load_test_date = null;
	// 			}

	// 			if($row->genotype_test_date == '0000-00-00')
	// 			{
	// 				$row->genotype_test_date = null;
	// 			}

	// 			if($row->result_report_date == '0000-00-00')
	// 			{
	// 				$row->result_report_date = null;
	// 			}
	// 		}

	// 		$data['tbldiagnostic_labs_data_viral_load'] = $res_must_have_viral_load_test;

	// 		echo json_encode($data);
	// 	}
	// }

	public function cascade_download()
	{
		header("Content-type: application/json");
		if($this->input->post('username') === null || $this->input->post('password') === null)
		{
			echo json_encode("Please send username/password along with the request");
			die();
		}

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$authlogin = $this->check_auth_login($username, $password);

		if(!$authlogin)
		{
			echo json_encode(array("Username/Password is incorrect"));
			die();
		}
		else
		{
			$end_date = date('Y-m-d');

			$sql = "SELECT f.id_mstfacility, f.facility_name AS hospital, SUM(s.cascade_antibody_positive) AntibodyPositive, SUM(s.cascade_chronic_hcv) AS ChronicHIV, SUM(s.cascade_treatment_initiated) AS TreatmentInitiated, SUM(s.cascade_treatment_completed) AS TreatmentCompleted, SUM(s.cascade_svr_test_done) AS SVRTestDone, SUM(s.cascade_svr_achieved) AS TreatmentSuccessful FROM mstfacility f left JOIN `tblsummary_new` s ON s.id_mstfacility = f.id_mstfacility GROUP BY s.id_mstfacility";

			$res = $this->Common_Model->query_data($sql);

			$total_AntibodyPositive = 0;
			$total_Confirmatory = 0;
			$total_ChronicHIV = 0;
			$total_TreatmentInitiated = 0;
			$total_TreatmentCompleted = 0;
			$total_SVR = 0;
			$total_TreatmentSuccessful = 0;
			$counter = 0;

			foreach ($res as $row) {

				$total_AntibodyPositive    += $row->AntibodyPositive;
				$total_ChronicHIV          += $row->ChronicHIV;
				$total_TreatmentInitiated  += $row->TreatmentInitiated;
				$total_TreatmentCompleted  += $row->TreatmentCompleted;
				$total_SVR                 += $row->SVRTestDone;
				$total_TreatmentSuccessful += $row->TreatmentSuccessful;
				$counter++;
			}

			$res[] = array(
				"id_mstfacility"      => "9999",
				"hospital"            => "Total",
				"AntibodyPositive"    => "$total_AntibodyPositive",
				"ChronicHIV"          => "$total_ChronicHIV",
				"TreatmentInitiated"  => "$total_TreatmentInitiated",
				"TreatmentCompleted"  => "$total_TreatmentCompleted",
				"SVRTestDone"         => "$total_SVR",
				"TreatmentSuccessful" => "$total_TreatmentSuccessful",
			);


			return $res;

		}

	}

	public function tab_admin_login()
	{
		header("Content-type: application/json");
		if($this->input->post('username') === null || $this->input->post('password') === null)
		{
			echo json_encode("Please send username/password along with the request");
			die();
		}

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$authlogin = $this->check_auth_login($username, $password);

		if(!$authlogin)
		{
			echo json_encode(array("Username/Password is incorrect"));
			die();
		}
		else
		{
			$this->db->where('user_type', 5);
			$this->db->where('username', $username);
			$this->db->where('password', md5($password));
			$user_details = $this->db->get('tblusers')->result();

			$facilities = $this->db->get('mstfacility')->result();

			$res = $this->cascade_download();
			echo json_encode(array("user_details" => $user_details, "mstfacility" => $facilities, "district_wise" => $res));
		}
	}

	public function uploadStock()
	{
		header("Content-Type: application/json");

		if($this->input->post('username') === null || $this->input->post('password') === null)
		{
			echo json_encode(array("ERROR : Please send username and password along with the request"));
			die();
		}

		$username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));

		$authlogin = $this->check_auth_login($username, $password);

		if(!$authlogin)
		{
			echo json_encode(array("Username/Password is incorrect"));
			die();
		}
		else
		{

			$sql = "select * from tblusers where username = '".$username."'";
			$tblusers = $this->Common_Model->query_data($sql);

		//updating table with app version

			if($this->input->post('versioncode') == null)
			{
				$insert_array_versioncode = array(
					'latest_version_used' => 'version other than 5.0'
				);

				$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);
				echo json_encode("Please update your app");
				die();
			}
			else
			{
				$versioncode = $this->security->xss_clean($this->input->post('versioncode'));

				$insert_array_versioncode = array(
					'latest_version_used' => $versioncode,
				);

				$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);


				if($versioncode != 'Version 1.0')
				{
					$insert_array_versioncode = array(
						'latest_version_used' => 'version other than 5.0'
					);

					$this->Common_Model->update_data('tblusers', $insert_array_versioncode,'id_tblusers', $tblusers[0]->id_tblusers);
					echo json_encode("Please update your app");
					die();
				}
			}

			$data = $this->input->post('data');
			$data_array = json_decode($data);
			if($data_array == null)
			{
				echo json_encode("ERROR: Please send properly formatted json");
			}
			else
			{
				$res = $this->Chai_api_model->uploadstock();
				if($res === null)
				{
					echo json_encode("success");
				}
				else
				{
					echo json_encode($res);
				}
			}
		}
	}

	public function genotype_report()
	{

		header('Content-Type: text/json');

		$start_date = '2016-06-18';
		$end_date = date('Y-m-d');

		$sql = "SELECT
		f1.facility_name,
		f1.id_mstfacility,
		ifnull(a.gen1_success, 0) as gen1_success,
		ifnull(b.gen1_notsuccess, 0) as gen1_notsuccess,
		ifnull(c.gen2_success, 0) as gen2_success,
		ifnull(d.gen2_notsuccess, 0) as gen2_notsuccess,
		ifnull(e.gen3_success, 0) as gen3_success,
		ifnull(f.gen3_notsuccess, 0) as gen3_notsuccess,
		ifnull(g.gen4_success, 0) as gen4_success,
		ifnull(h.gen4_notsuccess, 0) as gen4_notsuccess,
		ifnull(i.gen5_success, 0) as gen5_success,
		ifnull(j.gen5_notsuccess, 0) as gen5_notsuccess,
		ifnull(k.gen6_success, 0) as gen6_success,
		ifnull(l.gen6_notsuccess, 0) as gen6_notsuccess,
		ifnull(m.gen_notavailable_success, 0) as gen_notavailable_success,
		ifnull(n.gen_notavailable_notsuccess, 0) as gen_notavailable_notsuccess
		FROM
		mstfacility f1
		LEFT JOIN
		(
		SELECT
		COUNT(*) AS gen1_success,
		p.id_mstfacility
		FROM
		`cumulative_svr` c
		INNER JOIN
		tblpatientcirrohosis p
		ON
		p.patientguid = c.patientguid
		WHERE
		c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 1 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
		GROUP BY
		p.id_mstfacility
		) a on a.id_mstfacility = f1.id_mstfacility
		LEFT JOIN
		(
		SELECT
		COUNT(*) AS gen1_notsuccess,
		p.id_mstfacility
		FROM
		`cumulative_svr` c
		INNER JOIN
		tblpatientcirrohosis p
		ON
		p.patientguid = c.patientguid
		WHERE
		c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 1 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
		GROUP BY
		p.id_mstfacility
		) b
		ON
		b.id_mstfacility = f1.id_mstfacility
		LEFT JOIN
		(
		SELECT
		COUNT(*) AS gen2_success,
		p.id_mstfacility
		FROM
		`cumulative_svr` c
		INNER JOIN
		tblpatientcirrohosis p
		ON
		p.patientguid = c.patientguid
		WHERE
		c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 2 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
		GROUP BY
		p.id_mstfacility
		) c on c.id_mstfacility = f1.id_mstfacility
		LEFT JOIN
		(
		SELECT
		COUNT(*) AS gen2_notsuccess,
		p.id_mstfacility
		FROM
		`cumulative_svr` c
		INNER JOIN
		tblpatientcirrohosis p
		ON
		p.patientguid = c.patientguid
		WHERE
		c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 2 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
		GROUP BY
		p.id_mstfacility
		) d
		ON
		d.id_mstfacility = f1.id_mstfacility
		LEFT JOIN
		(
		SELECT
		COUNT(*) AS gen3_success,
		p.id_mstfacility
		FROM
		`cumulative_svr` c
		INNER JOIN
		tblpatientcirrohosis p
		ON
		p.patientguid = c.patientguid
		WHERE
		c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 3 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
		GROUP BY
		p.id_mstfacility
		) e on e.id_mstfacility = f1.id_mstfacility
		LEFT JOIN
		(
		SELECT
		COUNT(*) AS gen3_notsuccess,
		p.id_mstfacility
		FROM
		`cumulative_svr` c
		INNER JOIN
		tblpatientcirrohosis p
		ON
		p.patientguid = c.patientguid
		WHERE
		c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 3 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
		GROUP BY
		p.id_mstfacility
	) f
	ON
	f.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen4_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 4 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) g on g.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen4_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 4 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) h
ON
h.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen5_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 5 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) i on i.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen5_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 5 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) j
ON
j.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen6_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 6 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) k on k.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen6_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 6 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) l
ON
l.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen_notavailable_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 0 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) m on m.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen_notavailable_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 0 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) n
ON
n.id_mstfacility = f1.id_mstfacility";
$res = $this->Common_Model->query_data($sql);

echo json_encode(array("genotype_report" => $res));
}

public function cirrhosis_report()
{

	header('Content-Type: text/json');

	$start_date = '2016-06-18';
	$end_date = date('Y-m-d');

	$sql = "SELECT f.facility_short_name,f.id_mstfacility,  ifnull(a.count, 0) as 'Cir', ifnull(b.count, 0) as 'Noncir' FROM mstfacility f left join (select count(cirrhosis_status) as count, t.id_mstfacility from `tblpatient_tat_summary` t  inner join tblpatient p on p.patientguid = t.patientguid where  ((case when p.T_Initiation < '2016-06-18' then '2016-06-18' else p.T_Initiation end) between STR_TO_DATE('$start_date','%Y-%m-%d') and STR_TO_DATE('$end_date','%Y-%m-%d')) and cirrhosis_status = 1 group by id_mstfacility) a on f.id_mstfacility = a.id_mstfacility left join (select count(cirrhosis_status) as count, t.id_mstfacility from `tblpatient_tat_summary` t inner join tblpatient p on p.patientguid = t.patientguid where  ((case when p.T_Initiation < '2016-06-18' then '2016-06-18' else p.T_Initiation end) between STR_TO_DATE('$start_date','%Y-%m-%d') and STR_TO_DATE('$end_date','%Y-%m-%d')) and cirrhosis_status = 2 group by id_mstfacility) b on f.id_mstfacility = b.id_mstfacility order by f.id_mstfacility";
	$res = $this->Common_Model->query_data($sql);

	echo json_encode(array("cirrhosis_report" => $res));
}

public function age_report()
{

	header('Content-Type: text/json');

	$start_date = '2016-06-18';
	$end_date = date('Y-m-d');

	$sql = "select f.facility_short_name,f.id_mstfacility, ifnull(a.Patients, 0) as aa,ifnull(b.Patients, 0) as bb,ifnull(c.Patients, 0) as cc,ifnull(d.Patients, 0) as dd,ifnull(e.Patients, 0) as ee,ifnull(g.Patients, 0) as ff,ifnull(h.Patients, 0) as gg
	from mstfacility f
	left join
	(SELECT
	'<10 Years' AS AgeDistribution,
	COUNT(PatientGUID) AS Patients, id_mstfacility
	FROM
	`tblpatient`
	WHERE
	age < 10 AND age > 0 AND T_Initiation IS NOT NULL AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	) group by id_mstfacility) a
	on f.id_mstfacility = a.id_mstfacility
	left join (
	SELECT
	'<10-20 years' AS AgeDistribution,
	COUNT(PatientGUID) AS Patients, id_mstfacility
	FROM
	`tblpatient`
	WHERE
	age >= 10 AND age < 20 AND T_Initiation IS NOT NULL AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)group by id_mstfacility) b
	on f.id_mstfacility = b.id_mstfacility
	left join (
	SELECT
	'<20-30 years' AS AgeDistribution,
	COUNT(PatientGUID) AS Patients, id_mstfacility
	FROM
	`tblpatient`
	WHERE
	age >= 20 AND age < 30 AND T_Initiation IS NOT NULL AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	) group by id_mstfacility) c
	on f.id_mstfacility = c.id_mstfacility
	left join (
	SELECT
	'<30-40 years' AS AgeDistribution,
	COUNT(PatientGUID) AS Patients, id_mstfacility
	FROM
	`tblpatient`
	WHERE
	age >= 30 AND age < 40 AND T_Initiation IS NOT NULL AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)group by id_mstfacility) d
	on f.id_mstfacility = d.id_mstfacility
	left join (
	SELECT
	'<40-50 years' AS AgeDistribution,
	COUNT(PatientGUID) AS Patients, id_mstfacility
	FROM
	`tblpatient`
	WHERE
	age >= 40 AND age < 50 AND T_Initiation IS NOT NULL AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)group by id_mstfacility) e
	on f.id_mstfacility = e.id_mstfacility
	left join (
	SELECT
	'<50-60 years' AS AgeDistribution,
	COUNT(PatientGUID) AS Patients, id_mstfacility
	FROM
	`tblpatient`
	WHERE
	age >= 50 AND age < 60 AND T_Initiation IS NOT NULL AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)group by id_mstfacility) g
	on f.id_mstfacility = g.id_mstfacility
	left join (
	SELECT
	'>60 years' AS AgeDistribution,
	COUNT(PatientGUID) AS Patients, id_mstfacility
	FROM
	`tblpatient`
	WHERE
	age >= 60 AND T_Initiation IS NOT NULL AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)group by id_mstfacility) h
	on f.id_mstfacility = h.id_mstfacility order by f.id_mstfacility";
	$res = $this->Common_Model->query_data($sql);

	echo json_encode(array("age_report" => $res));
}

public function risk_factor_report()
{

	header('Content-Type: text/json');

	$start_date = '2016-06-18';
	$end_date = date('Y-m-d');

	$sql = "select facility_short_name,f.id_mstfacility, ifnull(a.COUNT, 0) as Unsafe_Injection_Use,ifnull(b.COUNT, 0) as IVDU, ifnull(c.COUNT, 0) as Unprotected_Sexual_Practice, ifnull(d.COUNT, 0) as Surgery, ifnull(e.COUNT, 0) as Dental, ifnull(g.COUNT, 0) as Others, ifnull(h.COUNT, 0) as not_available
	from mstfacility f
	left join
	(

	SELECT
	COUNT(PatientGUID) AS COUNT, id_mstfacility
	FROM
	tblpatient
	WHERE
	RiskFactor = 1 AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	) group by id_mstfacility) a
	on f.id_mstfacility = a.id_mstfacility
	left join (
	SELECT
	'IVDU' AS rf,
	COUNT(PatientGUID) AS COUNT, id_mstfacility
	FROM
	tblpatient
	WHERE
	RiskFactor = 2 AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)
	group by id_mstfacility) b
	on f.id_mstfacility =b.id_mstfacility
	left join (
	SELECT
	'Unprotected Sexual Practice' AS rf,
	COUNT(PatientGUID) AS COUNT, id_mstfacility
	FROM
	tblpatient
	WHERE
	RiskFactor = 3 AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)
	group by id_mstfacility) c
	on f.id_mstfacility = c.id_mstfacility
	left join (
	SELECT
	'Surgery' AS rf,
	COUNT(PatientGUID) AS COUNT, id_mstfacility
	FROM
	tblpatient
	WHERE
	RiskFactor = 4 AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)
	group by id_mstfacility) d
	on f.id_mstfacility = d.id_mstfacility
	left join (
	SELECT
	'Dental' AS rf,
	COUNT(PatientGUID) AS COUNT, id_mstfacility
	FROM
	tblpatient
	WHERE
	RiskFactor = 5 AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)
	group by id_mstfacility) e
	on f.id_mstfacility = e.id_mstfacility
	left join (
	SELECT
	'Others' AS rf,
	COUNT(PatientGUID) AS COUNT, id_mstfacility
	FROM
	tblpatient
	WHERE
	RiskFactor = 6 AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)
	group by id_mstfacility) g
	on f.id_mstfacility = g.id_mstfacility
	left join (
	SELECT
	'Not Available' AS rf,
	COUNT(PatientGUID) AS COUNT, id_mstfacility
	FROM
	tblpatient
	WHERE
	(
	RiskFactor IS NULL || RiskFactor = 0
	) AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)group by id_mstfacility) h
	on f.id_mstfacility = h.id_mstfacility order by f.id_mstfacility";
	$res = $this->Common_Model->query_data($sql);

	echo json_encode(array("risk_factor_report" => $res));
}

public function monthly_report()
{

	header('Content-Type: text/json');

	$start_date = '2016-06-18';
	$end_date = date('Y-m-d');

	$sql = "SELECT YEAR(p.date) as year,month(p.date) as month,left(MONTHNAME(p.date),3) as monthname, sum(`TreatmentInitiated`) as count FROM `tblsummary` p group by YEAR(p.date),MONTH(p.date) order by YEAR(p.date) desc, month DESC limit 12";
	$res = $this->Common_Model->query_data($sql);

	echo json_encode(array("monthly_report" => $res));
}

public function treatment_success_cirrhosis_report()
{

	header('Content-Type: text/json');

	$start_date = '2016-06-18';
	$end_date = date('Y-m-d');

	$sql = "SELECT
	f.facility_name,
	f.id_mstfacility,
	ifnull(a.cirr_treatment_success, 0) as cirr_treatment_success,
	ifnull(b.cirr_treatment_not_success, 0) as cirr_treatment_not_success,
	ifnull(c.no_cirr_treatment_success, 0) as no_cirr_treatment_success,
	ifnull(d.no_cirr_treatment_not_success, 0) as no_cirr_treatment_not_success
	FROM
	mstfacility f
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS cirr_treatment_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c1
	INNER JOIN
	tblpatient p
	ON
	c1.patientguid = p.PatientGUID
	WHERE
	c1.svr_date IS NOT NULL and p.v1_cirrhosis = 1 and c1.svr_result = 2 and c1.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by p.id_mstfacility
) a
ON
f.id_mstfacility = a.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS cirr_treatment_not_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c2
	INNER JOIN
	tblpatient p
	ON
	c2.patientguid = p.PatientGUID
	WHERE
	c2.svr_date IS NOT NULL AND p.v1_cirrhosis = 1 and c2.svr_result = 1 and c2.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by p.id_mstfacility
) b
ON
f.id_mstfacility = b.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS no_cirr_treatment_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c3
	INNER JOIN
	tblpatient p
	ON
	c3.patientguid = p.PatientGUID
	WHERE
	c3.svr_date IS NOT NULL and p.v1_cirrhosis = 2 and c3.svr_result = 2 and c3.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) c
ON
f.id_mstfacility = c.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS no_cirr_treatment_not_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c4
	INNER JOIN
	tblpatient p
	ON
	c4.patientguid = p.PatientGUID
	WHERE
	c4.svr_date IS NOT NULL AND p.v1_cirrhosis = 2 and c4.svr_result = 1 and c4.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
) d
ON
f.id_mstfacility = d.id_mstfacility
ORDER BY
f.facility_short_name";
$res = $this->Common_Model->query_data($sql);

echo json_encode(array("treatment_success_cirrhosis_report" => $res));
}

public function treatment_success_regimen_report()
{

	header('Content-Type: text/json');

	$start_date = '2016-06-18';
	$end_date = date('Y-m-d');

	$sql = "SELECT
	f.facility_name,
	f.id_mstfacility,
	ifnull(a.reg1_success, 0) as reg1_success,
	ifnull(b.reg1_notsuccess, 0) as reg1_notsuccess,
	ifnull(c.reg2_success, 0) as reg2_success,
	ifnull(d.reg2_notsuccess, 0) as reg2_notsuccess,
	ifnull(e.reg3_success, 0) as reg3_success,
	ifnull(f1.reg3_notsuccess, 0) as reg3_notsuccess,
	ifnull(g.reg4_success, 0) as reg4_success,
	ifnull(h.reg4_notsuccess, 0) as reg4_notsuccess
	FROM
	mstfacility f
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS reg1_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL and p.T_Regimen = 1 and svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
) a
ON
f.id_mstfacility = a.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS reg1_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL AND p.T_Regimen = 1 and svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
) b
ON
f.id_mstfacility = b.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS reg2_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL and p.T_Regimen = 2 and svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
) c
ON
f.id_mstfacility = c.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS reg2_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL AND p.T_Regimen = 2 and svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
) d
ON
f.id_mstfacility = d.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS reg3_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL and p.T_Regimen = 3 and svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
) e
ON
f.id_mstfacility = e.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS reg3_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL AND p.T_Regimen = 3 and svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
) f1
ON
f.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS reg4_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL and p.T_Regimen = 4 and svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
) g
ON
f.id_mstfacility = g.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS reg4_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL AND p.T_Regimen = 4 and svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
) h
ON
f.id_mstfacility = h.id_mstfacility";
$res = $this->Common_Model->query_data($sql);

echo json_encode(array("treatment_success_regimen_report" => $res));
}

public function treatment_success_genotype_report()
{

	header('Content-Type: text/json');

	$start_date = '2016-06-18';
	$end_date = date('Y-m-d');

	$sql = "SELECT
	f1.facility_name,
	f1.id_mstfacility,
	ifnull(a.gen1_success, 0) as gen1_success,
	ifnull(b.gen1_notsuccess, 0) as gen1_notsuccess,
	ifnull(c.gen2_success, 0) as gen2_success,
	ifnull(d.gen2_notsuccess, 0) as gen2_notsuccess,
	ifnull(e.gen3_success, 0) as gen3_success,
	ifnull(f.gen3_notsuccess, 0) as gen3_notsuccess,
	ifnull(g.gen4_success, 0) as gen4_success,
	ifnull(h.gen4_notsuccess, 0) as gen4_notsuccess,
	ifnull(i.gen5_success, 0) as gen5_success,
	ifnull(j.gen5_notsuccess, 0) as gen5_notsuccess,
	ifnull(k.gen6_success, 0) as gen6_success,
	ifnull(l.gen6_notsuccess, 0) as gen6_notsuccess,
	ifnull(m.gen_notavailable_success, 0) as gen_notavailable_success,
	ifnull(n.gen_notavailable_notsuccess, 0) as gen_notavailable_notsuccess
	FROM
	mstfacility f1
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen1_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 1 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) a on a.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen1_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 1 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) b
ON
b.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen2_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 2 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) c on c.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen2_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 2 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) d
ON
d.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen3_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 3 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) e on e.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen3_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 3 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) f
ON
f.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen4_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 4 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) g on g.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen4_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 4 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) h
ON
h.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen5_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 5 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) i on i.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen5_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 5 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) j
ON
j.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen6_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 6 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) k on k.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen6_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 6 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) l
ON
l.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen_notavailable_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 0 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) m on m.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS gen_notavailable_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 0 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
) n
ON
n.id_mstfacility = f1.id_mstfacility";
$res = $this->Common_Model->query_data($sql);

echo json_encode(array("treatment_success_genotype_report" => $res));
}

public function treatment_success_age_report()
{

	header('Content-Type: text/json');

	$start_date = '2016-06-18';
	$end_date = date('Y-m-d');

	$sql = "SELECT
	f1.facility_name,
	f1.id_mstfacility,
	ifnull(a.a_success, 0) as a_success,
	ifnull(b.b_notsuccess, 0) as b_notsuccess,
	ifnull(c.c_success, 0) as c_success,
	ifnull(d.d_notsuccess, 0) as d_notsuccess,
	ifnull(e.e_success, 0) as e_success,
	ifnull(f.f_notsuccess, 0) as f_notsuccess,
	ifnull(g.g_success, 0) as g_success,
	ifnull(h.h_notsuccess, 0) as h_notsuccess,
	ifnull(i.i_success, 0) as i_success,
	ifnull(j.j_notsuccess, 0) as j_notsuccess,
	ifnull(k.k_success, 0) as k_success,
	ifnull(l.l_notsuccess, 0) as l_notsuccess,
	ifnull(m.m_success, 0) as m_success,
	ifnull(n.n_notsuccess, 0) as n_notsuccess
	FROM
	mstfacility f1
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS a_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.age < 10 and p.age > 0 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
) a
ON
a.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS b_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.age < 10 and p.age > 0 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
) b
ON
b.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS c_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (age BETWEEN 10 AND 19) AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
) c
ON
c.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS d_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (age BETWEEN 10 AND 19) AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
) d
ON
d.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS e_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 20 AND 29) AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
) e
ON
e.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS f_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 20 AND 29) AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
) f
ON
f.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS g_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 30 AND 39) AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
) g
ON
g.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS h_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 30 AND 39) AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
) h
ON
h.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS i_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 40 AND 49) AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
) i
ON
i.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS j_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 40 AND 49) AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
) j
ON
j.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS k_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 50 AND 59) AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
) k
ON
k.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS l_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 50 AND 59) AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
) l
ON
l.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS m_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age >= 60) AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
) m
ON
m.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS n_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age >= 60) AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
) n
ON
n.id_mstfacility = f1.id_mstfacility";
$res = $this->Common_Model->query_data($sql);

echo json_encode(array("treatment_success_age_report" => $res));
}

public function treatment_success_gender_report()
{

	header('Content-Type: text/json');

	$start_date = '2016-06-18';
	$end_date = date('Y-m-d');

	$sql = "SELECT
	f1.facility_name,
	f1.id_mstfacility,
	ifnull(a.male_success, 0) as male_success,
	ifnull(b.male_notsuccess, 0) as male_notsuccess,
	ifnull(c.female_success, 0) as female_success,
	ifnull(d.female_notsuccess, 0) as female_notsuccess,
	ifnull(e.transgender_success, 0) as transgender_success,
	ifnull(f.transgender_notsuccess, 0) as transgender_notsuccess
	FROM
	mstfacility f1
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS male_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.Gender = 1 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
) a
ON
a.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS male_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.Gender = 1 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
) b
ON
b.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS female_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.Gender = 2 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
) c
ON
c.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS female_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.Gender = 2 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
) d
ON
d.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS transgender_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.Gender = 3 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
) e
ON
e.id_mstfacility = f1.id_mstfacility
LEFT JOIN
(
	SELECT
	COUNT(*) AS transgender_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.Gender = 3 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
) f
ON
f.id_mstfacility = f1.id_mstfacility";
$res = $this->Common_Model->query_data($sql);

echo json_encode(array("treatment_success_gender_report" => $res));
}

public function missed_appointment_report()
{

	header('Content-Type: text/json');

	$start_date = '2016-06-18';
	$end_date = date('Y-m-d');

	$sql = "Select f.id_mstfacility, CONCAT(d.DistrictName,'-',f.Name) as hospital, ifnull(a.cnt, 0) as Followup,ifnull(b.cnt, 0) as SVR,ifnull(c.cnt, 0) as TreatmentInitiation,ifnull(d.cnt, 0) as ConfirmatoryPending from
	mstfacility  f
	inner join mstdistrict d on f.id_mstdistrict = d.id_mstdistrict

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose between 2 and 98 and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d') Group by  id_mstfacility)a on f.id_mstfacility = a.id_mstfacility

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose =99 and SVR12W_HCVViralLoad_Dt is null and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d')  Group by  id_mstfacility)b on f.id_mstfacility = b.id_mstfacility

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose =1 and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d')  Group by  id_mstfacility)c on f.id_mstfacility =c.id_mstfacility

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose =-1 and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d')  Group by  id_mstfacility)d on f.id_mstfacility = d.id_mstfacility";
	$res = $this->Common_Model->query_data($sql);

	echo json_encode(array("missed_appointment_report" => $res));
}

public function lfu_report()
{

	header('Content-Type: text/json');

	$start_date = '2016-06-18';
	$end_date = date('Y-m-d');

	$sql_aggregate_loss_to_followup_days = "SELECT loss_to_followup_days FROM `mstappstateconfiguration`";
	$res_aggregate_loss_to_followup_days = $this->Common_Model->query_data($sql_aggregate_loss_to_followup_days);
	$loss_followup_days = $res_aggregate_loss_to_followup_days[0]->loss_to_followup_days;

	$sql = "Select f.id_mstfacility, CONCAT(d.DistrictName,'-',f.Name) as hospital, ifnull(a.cnt, 0) as Followup,ifnull(b.cnt, 0) as SVR,ifnull(c.cnt, 0) as TreatmentInitiation,ifnull(d.cnt, 0) as ConfirmatoryPending from
	mstfacility  f
	inner join mstdistrict d on f.id_mstdistrict = d.id_mstdistrict

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose between 2 and 98 and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d') and (datediff(STR_TO_DATE('$end_date', '%Y-%m-%d'), Next_VisitDt) >= ".$loss_followup_days.") Group by  id_mstfacility)a on f.id_mstfacility = a.id_mstfacility

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose =99  and SVR12W_HCVViralLoad_Dt is null and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d') and (datediff(STR_TO_DATE('$end_date', '%Y-%m-%d'), Next_VisitDt) >= ".$loss_followup_days.")  Group by  id_mstfacility)b on f.id_mstfacility = b.id_mstfacility

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose =1 and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d') and (datediff(STR_TO_DATE('$end_date', '%Y-%m-%d'), Next_VisitDt) >= ".$loss_followup_days.")  Group by  id_mstfacility)c on f.id_mstfacility =c.id_mstfacility

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose =-1 and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d') and (datediff(STR_TO_DATE('$end_date', '%Y-%m-%d'), Next_VisitDt) >= ".$loss_followup_days.")  Group by  id_mstfacility)d on f.id_mstfacility = d.id_mstfacility";
	$res = $this->Common_Model->query_data($sql);

	echo json_encode(array("lfu_report" => $res));
}

public function treatment_success_ratio_report()
{

	header('Content-Type: text/json');

	$start_date = '2016-06-18';
	$end_date = date('Y-m-d');

	$sql = "select f.id_mstfacility, ifnull((a.COUNT - b.COUNT), 0) as 'SVR Not Achieved',ifnull(b.COUNT, 0) as 'SVR Achieved' from mstfacility f left join (SELECT
	'SVR Done' AS title,
	COUNT(*) AS COUNT,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN tblpatient p ON
	c.patientguid = p.patientguid
	WHERE
	c.svr_date IS NOT NULL AND c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	) a
	on a.id_mstfacility = f.id_mstfacility
	left JOIN(
	SELECT
	'SVR Achieved' AS title,
	COUNT(*) AS COUNT,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN tblpatient p ON
	c.patientguid = p.patientguid
	WHERE
	c.svr_date IS NOT NULL AND svr_result = 2 AND c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')) b

	on b.id_mstfacility = f.id_mstfacility";

	$res = $this->Common_Model->query_data($sql);

	echo json_encode(array("treatment_success_ratio_report" => $res));
}

public function get_all_reports()
{
	header('Content-Type: text/json');

	$start_date = '2016-06-18';
	$end_date = date('Y-m-d');

	$sql_aggregate_loss_to_followup_days = "SELECT loss_to_followup_days FROM `mstappstateconfiguration`";
	$res_aggregate_loss_to_followup_days = $this->Common_Model->query_data($sql_aggregate_loss_to_followup_days);
	$loss_followup_days = $res_aggregate_loss_to_followup_days[0]->loss_to_followup_days;

	$sql = "select a.*,b.*, c.*, d.*,f.*,g.*,h.*,i.*, j.*, k.*, l.*, m.*, n.*, o.*, p.*, q.* from mstfacility ";

        // adding genotype_report

	$sql .= " left join (SELECT
	f1.facility_name,
	f1.id_mstfacility,
	ifnull(a.gen1_success, 0) as gen1_success,
	ifnull(b.gen1_notsuccess, 0) as gen1_notsuccess,
	ifnull(c.gen2_success, 0) as gen2_success,
	ifnull(d.gen2_notsuccess, 0) as gen2_notsuccess,
	ifnull(e.gen3_success, 0) as gen3_success,
	ifnull(f.gen3_notsuccess, 0) as gen3_notsuccess,
	ifnull(g.gen4_success, 0) as gen4_success,
	ifnull(h.gen4_notsuccess, 0) as gen4_notsuccess,
	ifnull(i.gen5_success, 0) as gen5_success,
	ifnull(j.gen5_notsuccess, 0) as gen5_notsuccess,
	ifnull(k.gen6_success, 0) as gen6_success,
	ifnull(l.gen6_notsuccess, 0) as gen6_notsuccess,
	ifnull(m.gen_notavailable_success, 0) as gen_notavailable_success,
	ifnull(n.gen_notavailable_notsuccess, 0) as gen_notavailable_notsuccess
	FROM
	mstfacility f1
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen1_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 1 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) a on a.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen1_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 1 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) b
	ON
	b.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen2_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 2 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) c on c.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen2_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 2 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) d
	ON
	d.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen3_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 3 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) e on e.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen3_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 3 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) f
	ON
	f.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen4_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 4 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) g on g.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen4_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 4 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) h
	ON
	h.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen5_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 5 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) i on i.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen5_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 5 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) j
	ON
	j.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen6_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 6 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) k on k.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen6_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 6 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) l
	ON
	l.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen_notavailable_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 0 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) m on m.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS gen_notavailable_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatientcirrohosis p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.`GenotypeTest_Result` = 0 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) n
	ON
	n.id_mstfacility = f1.id_mstfacility) a on mstfacility.id_mstfacility = a.id_mstfacility";

  // adding cirrhosis_report

	$sql .= " left join (SELECT f.facility_short_name,f.id_mstfacility,  ifnull(a.count, 0) as 'Cir', ifnull(b.count, 0) as 'Noncir' FROM mstfacility f left join (select count(cirrhosis_status) as count, t.id_mstfacility from `tblpatient_tat_summary` t  inner join tblpatient p on p.patientguid = t.patientguid where  ((case when p.T_Initiation < '2016-06-18' then '2016-06-18' else p.T_Initiation end) between STR_TO_DATE('$start_date','%Y-%m-%d') and STR_TO_DATE('$end_date','%Y-%m-%d')) and cirrhosis_status = 1 group by id_mstfacility) a on f.id_mstfacility = a.id_mstfacility left join (select count(cirrhosis_status) as count, t.id_mstfacility from `tblpatient_tat_summary` t inner join tblpatient p on p.patientguid = t.patientguid where  ((case when p.T_Initiation < '2016-06-18' then '2016-06-18' else p.T_Initiation end) between STR_TO_DATE('$start_date','%Y-%m-%d') and STR_TO_DATE('$end_date','%Y-%m-%d')) and cirrhosis_status = 2 group by id_mstfacility) b on f.id_mstfacility = b.id_mstfacility order by f.id_mstfacility) b on mstfacility.id_mstfacility = b.id_mstfacility";

  // adding age_report

	$sql .= " left join (select f.facility_short_name,f.id_mstfacility, ifnull(a.Patients, 0) as aa,ifnull(b.Patients, 0) as bb,ifnull(c.Patients, 0) as cc,ifnull(d.Patients, 0) as dd,ifnull(e.Patients, 0) as ee,ifnull(g.Patients, 0) as ff,ifnull(h.Patients, 0) as gg
	from mstfacility f
	left join
	(SELECT
	'<10 Years' AS AgeDistribution,
	COUNT(PatientGUID) AS Patients, id_mstfacility
	FROM
	`tblpatient`
	WHERE
	age < 10 AND age > 0 AND T_Initiation IS NOT NULL AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	) group by id_mstfacility) a
	on f.id_mstfacility = a.id_mstfacility
	left join (
	SELECT
	'<10-20 years' AS AgeDistribution,
	COUNT(PatientGUID) AS Patients, id_mstfacility
	FROM
	`tblpatient`
	WHERE
	age >= 10 AND age < 20 AND T_Initiation IS NOT NULL AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)group by id_mstfacility) b
	on f.id_mstfacility = b.id_mstfacility
	left join (
	SELECT
	'<20-30 years' AS AgeDistribution,
	COUNT(PatientGUID) AS Patients, id_mstfacility
	FROM
	`tblpatient`
	WHERE
	age >= 20 AND age < 30 AND T_Initiation IS NOT NULL AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	) group by id_mstfacility) c
	on f.id_mstfacility = c.id_mstfacility
	left join (
	SELECT
	'<30-40 years' AS AgeDistribution,
	COUNT(PatientGUID) AS Patients, id_mstfacility
	FROM
	`tblpatient`
	WHERE
	age >= 30 AND age < 40 AND T_Initiation IS NOT NULL AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)group by id_mstfacility) d
	on f.id_mstfacility = d.id_mstfacility
	left join (
	SELECT
	'<40-50 years' AS AgeDistribution,
	COUNT(PatientGUID) AS Patients, id_mstfacility
	FROM
	`tblpatient`
	WHERE
	age >= 40 AND age < 50 AND T_Initiation IS NOT NULL AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)group by id_mstfacility) e
	on f.id_mstfacility = e.id_mstfacility
	left join (
	SELECT
	'<50-60 years' AS AgeDistribution,
	COUNT(PatientGUID) AS Patients, id_mstfacility
	FROM
	`tblpatient`
	WHERE
	age >= 50 AND age < 60 AND T_Initiation IS NOT NULL AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)group by id_mstfacility) g
	on f.id_mstfacility = g.id_mstfacility
	left join (
	SELECT
	'>60 years' AS AgeDistribution,
	COUNT(PatientGUID) AS Patients, id_mstfacility
	FROM
	`tblpatient`
	WHERE
	age >= 60 AND T_Initiation IS NOT NULL AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)group by id_mstfacility) h
	on f.id_mstfacility = h.id_mstfacility order by f.id_mstfacility) c on mstfacility.id_mstfacility = c.id_mstfacility";

// adding risk_factor_report

	$sql .= " left join (select facility_short_name,f.id_mstfacility, ifnull(a.COUNT, 0) as Unsafe_Injection_Use,ifnull(b.COUNT, 0) as IVDU, ifnull(c.COUNT, 0) as Unprotected_Sexual_Practice, ifnull(d.COUNT, 0) as Surgery, ifnull(e.COUNT, 0) as Dental, ifnull(g.COUNT, 0) as Others, ifnull(h.COUNT, 0) as not_available
	from mstfacility f
	left join
	(

	SELECT
	COUNT(PatientGUID) AS COUNT, id_mstfacility
	FROM
	tblpatient
	WHERE
	RiskFactor = 1 AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	) group by id_mstfacility) a
	on f.id_mstfacility = a.id_mstfacility
	left join (
	SELECT
	'IVDU' AS rf,
	COUNT(PatientGUID) AS COUNT, id_mstfacility
	FROM
	tblpatient
	WHERE
	RiskFactor = 2 AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)
	group by id_mstfacility) b
	on f.id_mstfacility =b.id_mstfacility
	left join (
	SELECT
	'Unprotected Sexual Practice' AS rf,
	COUNT(PatientGUID) AS COUNT, id_mstfacility
	FROM
	tblpatient
	WHERE
	RiskFactor = 3 AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)
	group by id_mstfacility) c
	on f.id_mstfacility = c.id_mstfacility
	left join (
	SELECT
	'Surgery' AS rf,
	COUNT(PatientGUID) AS COUNT, id_mstfacility
	FROM
	tblpatient
	WHERE
	RiskFactor = 4 AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)
	group by id_mstfacility) d
	on f.id_mstfacility = d.id_mstfacility
	left join (
	SELECT
	'Dental' AS rf,
	COUNT(PatientGUID) AS COUNT, id_mstfacility
	FROM
	tblpatient
	WHERE
	RiskFactor = 5 AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)
	group by id_mstfacility) e
	on f.id_mstfacility = e.id_mstfacility
	left join (
	SELECT
	'Others' AS rf,
	COUNT(PatientGUID) AS COUNT, id_mstfacility
	FROM
	tblpatient
	WHERE
	RiskFactor = 6 AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)
	group by id_mstfacility) g
	on f.id_mstfacility = g.id_mstfacility
	left join (
	SELECT
	'Not Available' AS rf,
	COUNT(PatientGUID) AS COUNT, id_mstfacility
	FROM
	tblpatient
	WHERE
	(
	RiskFactor IS NULL || RiskFactor = 0
	) AND(
	(
	CASE WHEN T_Initiation < '2016-06-18' THEN '2016-06-18' ELSE T_Initiation END
	) BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	)group by id_mstfacility) h
	on f.id_mstfacility = h.id_mstfacility order by f.id_mstfacility) d on mstfacility.id_mstfacility = d.id_mstfacility";

  // adding treatment_success_cirrhosis_report

	$sql .= " left join (SELECT
	f.facility_name,
	f.id_mstfacility,
	ifnull(a.cirr_treatment_success, 0) as cirr_treatment_success,
	ifnull(b.cirr_treatment_not_success, 0) as cirr_treatment_not_success,
	ifnull(c.no_cirr_treatment_success, 0) as no_cirr_treatment_success,
	ifnull(d.no_cirr_treatment_not_success, 0) as no_cirr_treatment_not_success
	FROM
	mstfacility f
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS cirr_treatment_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c1
	INNER JOIN
	tblpatient p
	ON
	c1.patientguid = p.PatientGUID
	WHERE
	c1.svr_date IS NOT NULL and p.v1_cirrhosis = 1 and c1.svr_result = 2 and c1.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by p.id_mstfacility
	) a
	ON
	f.id_mstfacility = a.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS cirr_treatment_not_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c2
	INNER JOIN
	tblpatient p
	ON
	c2.patientguid = p.PatientGUID
	WHERE
	c2.svr_date IS NOT NULL AND p.v1_cirrhosis = 1 and c2.svr_result = 1 and c2.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by p.id_mstfacility
	) b
	ON
	f.id_mstfacility = b.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS no_cirr_treatment_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c3
	INNER JOIN
	tblpatient p
	ON
	c3.patientguid = p.PatientGUID
	WHERE
	c3.svr_date IS NOT NULL and p.v1_cirrhosis = 2 and c3.svr_result = 2 and c3.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	p.id_mstfacility
	) c
	ON
	f.id_mstfacility = c.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS no_cirr_treatment_not_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c4
	INNER JOIN
	tblpatient p
	ON
	c4.patientguid = p.PatientGUID
	WHERE
	c4.svr_date IS NOT NULL AND p.v1_cirrhosis = 2 and c4.svr_result = 1 and c4.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
	) d
	ON
	f.id_mstfacility = d.id_mstfacility
	ORDER BY
	f.facility_short_name) f on mstfacility.id_mstfacility = f.id_mstfacility";

  // adding treatment_success_regimen_report

	$sql .= " left join (SELECT
	f.facility_name,
	f.id_mstfacility,
	ifnull(a.reg1_success, 0) as reg1_success,
	ifnull(b.reg1_notsuccess, 0) as reg1_notsuccess,
	ifnull(c.reg2_success, 0) as reg2_success,
	ifnull(d.reg2_notsuccess, 0) as reg2_notsuccess,
	ifnull(e.reg3_success, 0) as reg3_success,
	ifnull(f1.reg3_notsuccess, 0) as reg3_notsuccess,
	ifnull(g.reg4_success, 0) as reg4_success,
	ifnull(h.reg4_notsuccess, 0) as reg4_notsuccess
	FROM
	mstfacility f
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS reg1_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL and p.T_Regimen = 1 and svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
	) a
	ON
	f.id_mstfacility = a.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS reg1_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL AND p.T_Regimen = 1 and svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
	) b
	ON
	f.id_mstfacility = b.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS reg2_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL and p.T_Regimen = 2 and svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
	) c
	ON
	f.id_mstfacility = c.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS reg2_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL AND p.T_Regimen = 2 and svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
	) d
	ON
	f.id_mstfacility = d.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS reg3_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL and p.T_Regimen = 3 and svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
	) e
	ON
	f.id_mstfacility = e.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS reg3_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL AND p.T_Regimen = 3 and svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
	) f1
	ON
	f.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS reg4_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL and p.T_Regimen = 4 and svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
	) g
	ON
	f.id_mstfacility = g.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS reg4_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	c.patientguid = p.PatientGUID
	WHERE
	c.svr_date IS NOT NULL AND p.T_Regimen = 4 and svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
	) h
	ON
	f.id_mstfacility = h.id_mstfacility) g on mstfacility.id_mstfacility = g.id_mstfacility";

  // adding treatment_success_genotype_report

	$sql .= " left join (select f.facility_short_name,f.id_mstfacility, ifnull(a.gen1, 0) as Genotype1, ifnull(a.gen2, 0) as Genotype2, ifnull(a.gen3, 0) as Genotype3, ifnull(a.gen4, 0) as Genotype4, ifnull(a.gen5, 0) as Genotype5, ifnull(a.gen6, 0) as Genotype6 from mstfacility f left join (select t.id_mstfacility, sum(case when genotype = 1 then 1 else 0 end) as gen1, sum(case when genotype = 2 then 1 else 0 end) as gen2, sum(case when genotype = 3 then 1 else 0 end) as gen3, sum(case when genotype = 4 then 1 else 0 end) as gen4, sum(case when genotype = 5 then 1 else 0 end) as gen5, sum(case when genotype = 6 then 1 else 0 end) as gen6 from tblpatient_tat_summary t inner join tblpatient p on p.patientguid = t.patientguid where ((case when p.T_Initiation < '2016-06-18' then '2016-06-18' else p.T_Initiation end) between STR_TO_DATE('$start_date','%Y-%m-%d') and STR_TO_DATE('$end_date','%Y-%m-%d')) group by t.id_mstfacility) a on f.id_mstfacility = a.id_mstfacility) h on mstfacility.id_mstfacility = h.id_mstfacility";

  // adding treatment_success_age_report

	$sql .= " left join (SELECT
	f1.facility_name,
	f1.id_mstfacility,
	ifnull(a.a_success, 0) as a_success,
	ifnull(b.b_notsuccess, 0) as b_notsuccess,
	ifnull(c.c_success, 0) as c_success,
	ifnull(d.d_notsuccess, 0) as d_notsuccess,
	ifnull(e.e_success, 0) as e_success,
	ifnull(f.f_notsuccess, 0) as f_notsuccess,
	ifnull(g.g_success, 0) as g_success,
	ifnull(h.h_notsuccess, 0) as h_notsuccess,
	ifnull(i.i_success, 0) as i_success,
	ifnull(j.j_notsuccess, 0) as j_notsuccess,
	ifnull(k.k_success, 0) as k_success,
	ifnull(l.l_notsuccess, 0) as l_notsuccess,
	ifnull(m.m_success, 0) as m_success,
	ifnull(n.n_notsuccess, 0) as n_notsuccess
	FROM
	mstfacility f1
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS a_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.age < 10 and p.age > 0 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
	) a
	ON
	a.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS b_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.age < 10 and p.age > 0 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
	) b
	ON
	b.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS c_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (age BETWEEN 10 AND 19) AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
	) c
	ON
	c.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS d_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (age BETWEEN 10 AND 19) AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
	) d
	ON
	d.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS e_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 20 AND 29) AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
	) e
	ON
	e.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS f_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 20 AND 29) AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
	) f
	ON
	f.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS g_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 30 AND 39) AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
	) g
	ON
	g.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS h_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 30 AND 39) AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
	) h
	ON
	h.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS i_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 40 AND 49) AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
	) i
	ON
	i.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS j_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 40 AND 49) AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
	) j
	ON
	j.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS k_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 50 AND 59) AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
	) k
	ON
	k.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS l_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age BETWEEN 50 AND 59) AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
	) l
	ON
	l.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS m_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age >= 60) AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
	) m
	ON
	m.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS n_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND (p.age >= 60) AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d') group by id_mstfacility
	) n
	ON
	n.id_mstfacility = f1.id_mstfacility) i on mstfacility.id_mstfacility = i.id_mstfacility";

  // adding treatment_success_gender_report

	$sql .= " left join (SELECT
	f1.facility_name,
	f1.id_mstfacility,
	ifnull(a.male_success, 0) as male_success,
	ifnull(b.male_notsuccess, 0) as male_notsuccess,
	ifnull(c.female_success, 0) as female_success,
	ifnull(d.female_notsuccess, 0) as female_notsuccess,
	ifnull(e.transgender_success, 0) as transgender_success,
	ifnull(f.transgender_notsuccess, 0) as transgender_notsuccess
	FROM
	mstfacility f1
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS male_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.Gender = 1 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
	) a
	ON
	a.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS male_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.Gender = 1 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	) b
	ON
	b.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS female_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.Gender = 2 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
	) c
	ON
	c.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS female_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.Gender = 2 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
	) d
	ON
	d.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS transgender_success,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.Gender = 3 AND svr_result = 2 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
	) e
	ON
	e.id_mstfacility = f1.id_mstfacility
	LEFT JOIN
	(
	SELECT
	COUNT(*) AS transgender_notsuccess,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN
	tblpatient p
	ON
	p.patientguid = c.patientguid
	WHERE
	c.svr_date IS NOT NULL AND p.Gender = 3 AND svr_result = 1 and c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	GROUP BY
	id_mstfacility
	) f
	ON
	f.id_mstfacility = f1.id_mstfacility) j on mstfacility.id_mstfacility = j.id_mstfacility";

  // adding missed_appointment_report

	$sql .= " left join (Select f.id_mstfacility, CONCAT(d.DistrictName,'-',f.Name) as hospital, ifnull(a.cnt, 0) as Missed_Followup,ifnull(b.cnt, 0) as Missed_SVR,ifnull(c.cnt, 0) as Missed_TreatmentInitiation,ifnull(d.cnt, 0) as Missed_ConfirmatoryPending from
	mstfacility  f
	inner join mstdistrict d on f.id_mstdistrict = d.id_mstdistrict

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose between 2 and 98 and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d') Group by  id_mstfacility)a on f.id_mstfacility = a.id_mstfacility

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose =99 and SVR12W_HCVViralLoad_Dt is null and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d')  Group by  id_mstfacility)b on f.id_mstfacility = b.id_mstfacility

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose =1 and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d')  Group by  id_mstfacility)c on f.id_mstfacility =c.id_mstfacility

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose =-1 and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d')  Group by  id_mstfacility)d on f.id_mstfacility = d.id_mstfacility) k on mstfacility.id_mstfacility = k.id_mstfacility";


        // adding lfu
	$sql .= " left join (Select f.id_mstfacility, CONCAT(d.DistrictName,'-',f.Name) as hospital, ifnull(a.cnt, 0) as LFU_Followup,ifnull(b.cnt, 0) as LFU_SVR,ifnull(c.cnt, 0) as LFU_TreatmentInitiation,ifnull(d.cnt, 0) as LFU_ConfirmatoryPending from
	mstfacility  f
	inner join mstdistrict d on f.id_mstdistrict = d.id_mstdistrict

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose between 2 and 98 and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d') and (datediff(STR_TO_DATE('$end_date', '%Y-%m-%d'), Next_VisitDt) >= ".$loss_followup_days.") Group by  id_mstfacility)a on f.id_mstfacility = a.id_mstfacility

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose =99  and SVR12W_HCVViralLoad_Dt is null and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d') and (datediff(STR_TO_DATE('$end_date', '%Y-%m-%d'), Next_VisitDt) >= ".$loss_followup_days.")  Group by  id_mstfacility)b on f.id_mstfacility = b.id_mstfacility

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose =1 and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d') and (datediff(STR_TO_DATE('$end_date', '%Y-%m-%d'), Next_VisitDt) >= ".$loss_followup_days.")  Group by  id_mstfacility)c on f.id_mstfacility =c.id_mstfacility

	left join (select id_mstfacility, count(patientguid)cnt from tblpatient p  Where nextvisitpurpose =-1 and Next_Visitdt > STR_TO_DATE('$start_date','%Y-%m-%d') and Next_Visitdt < STR_TO_DATE('$end_date','%Y-%m-%d') and (datediff(STR_TO_DATE('$end_date', '%Y-%m-%d'), Next_VisitDt) >= ".$loss_followup_days.")  Group by  id_mstfacility)d on f.id_mstfacility = d.id_mstfacility) l on mstfacility.id_mstfacility = l.id_mstfacility";


// adding treatment_success_ratio_report

	$sql .= " left join (select f.id_mstfacility, ifnull((a.COUNT - b.COUNT), 0) as 'SVR_Not_Achieved',ifnull(b.COUNT, 0) as 'SVR_Achieved' from mstfacility f left join (SELECT
	'SVR_Done' AS title,
	COUNT(*) AS COUNT,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN tblpatient p ON
	c.patientguid = p.patientguid
	WHERE
	c.svr_date IS NOT NULL AND c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')
	group by p.id_mstfacility) a
	on a.id_mstfacility = f.id_mstfacility
	left JOIN(
	SELECT
	'SVR_Achieved' AS title,
	COUNT(*) AS COUNT,
	p.id_mstfacility
	FROM
	`cumulative_svr` c
	INNER JOIN tblpatient p ON
	c.patientguid = p.patientguid
	WHERE
	c.svr_date IS NOT NULL AND svr_result = 2 AND c.svr_date BETWEEN STR_TO_DATE('$start_date', '%Y-%m-%d') AND STR_TO_DATE('$end_date', '%Y-%m-%d')  group by p.id_mstfacility) b

	on b.id_mstfacility = f.id_mstfacility) m on mstfacility.id_mstfacility = m.id_mstfacility";

// adding occupation report

	$sql .= " left join(SELECT
	mstfacility.id_mstfacility,
	IFNULL(a.count, 0) AS 'health_care_worker',
	IFNULL(b.count, 0) AS 'dental_worker',
	IFNULL(c.count, 0) AS 'lab_technician',
	IFNULL(d.count, 0) AS 'barber_beautician',
	IFNULL(e.count, 0) AS 'cleaner',
	IFNULL(f.count, 0) AS 'truck_driver',
	IFNULL(g.count, 0) AS 'farmer',
	IFNULL(h.count, 0) AS 'labourer',
	IFNULL(i.count, 0) AS 'service',
	IFNULL(j.count, 0) AS 'others'
	FROM
	`mstfacility`
	LEFT JOIN(
	SELECT id_mstfacility,
	COUNT(*) AS COUNT
	FROM
	tblpatient
	WHERE
	OccupationID = 1
	GROUP BY
	id_mstfacility
	) a
	ON
	mstfacility.id_mstfacility = a.id_mstfacility
	LEFT JOIN(
	SELECT id_mstfacility,
	COUNT(*) AS COUNT
	FROM
	tblpatient
	WHERE
	OccupationID = 2
	GROUP BY
	id_mstfacility
	) b
	ON
	mstfacility.id_mstfacility = b.id_mstfacility
	LEFT JOIN(
	SELECT id_mstfacility,
	COUNT(*) AS COUNT
	FROM
	tblpatient
	WHERE
	OccupationID = 3
	GROUP BY
	id_mstfacility
	) c
	ON
	mstfacility.id_mstfacility = c.id_mstfacility
	LEFT JOIN(
	SELECT id_mstfacility,
	COUNT(*) AS COUNT
	FROM
	tblpatient
	WHERE
	OccupationID = 4
	GROUP BY
	id_mstfacility
	) d
	ON
	mstfacility.id_mstfacility = d.id_mstfacility
	LEFT JOIN(
	SELECT id_mstfacility,
	COUNT(*) AS COUNT
	FROM
	tblpatient
	WHERE
	OccupationID = 5
	GROUP BY
	id_mstfacility
	) e
	ON
	mstfacility.id_mstfacility = e.id_mstfacility
	LEFT JOIN(
	SELECT id_mstfacility,
	COUNT(*) AS COUNT
	FROM
	tblpatient
	WHERE
	OccupationID = 6
	GROUP BY
	id_mstfacility
	) f
	ON
	mstfacility.id_mstfacility = f.id_mstfacility
	LEFT JOIN(
	SELECT id_mstfacility,
	COUNT(*) AS COUNT
	FROM
	tblpatient
	WHERE
	OccupationID = 7
	GROUP BY
	id_mstfacility
	) g
	ON
	mstfacility.id_mstfacility = g.id_mstfacility
	LEFT JOIN(
	SELECT id_mstfacility,
	COUNT(*) AS COUNT
	FROM
	tblpatient
	WHERE
	OccupationID = 8
	GROUP BY
	id_mstfacility
	) h
	ON
	mstfacility.id_mstfacility = h.id_mstfacility
	LEFT JOIN(
	SELECT id_mstfacility,
	COUNT(*) AS COUNT
	FROM
	tblpatient
	WHERE
	OccupationID = 9
	GROUP BY
	id_mstfacility
	) i
	ON
	mstfacility.id_mstfacility = i.id_mstfacility
	LEFT JOIN(
	SELECT id_mstfacility,
	COUNT(*) AS COUNT
	FROM
	tblpatient
	WHERE
	OccupationID = 10
	GROUP BY
	id_mstfacility
	) j
	ON
	mstfacility.id_mstfacility = j.id_mstfacility) n on mstfacility.id_mstfacility = n.id_mstfacility";

    // adding monthly_report

	$sql .= " left join(SELECT
	mstfacility.id_mstfacility,
	a.count AS month_12,
	date_format(a.date, '%b') as month_12_name,
	year(a.date) as month_12_year,
	b.count AS month_11,
	date_format(b.date, '%b') as month_11_name,
	year(b.date) as month_11_year,
	c.count AS month_10,
	date_format(c.date, '%b') as month_10_name,
	year(c.date) as month_10_year,
	d.count AS month_9,
	date_format(d.date, '%b') as month_9_name,
	year(d.date) as month_9_year,
	e.count AS month_8,
	date_format(e.date, '%b') as month_8_name,
	year(e.date) as month_8_year,
	f.count AS month_7,
	date_format(f.date, '%b') as month_7_name,
	year(f.date) as month_7_year,
	g.count AS month_6,
	date_format(g.date, '%b') as month_6_name,
	year(g.date) as month_6_year,
	h.count AS month_5,
	date_format(h.date, '%b') as month_5_name,
	year(h.date) as month_5_year,
	i.count AS month_4,
	date_format(i.date, '%b') as month_4_name,
	year(i.date) as month_4_year,
	j.count AS month_3,
	date_format(j.date, '%b') as month_3_name,
	year(j.date) as month_3_year,
	k.count AS month_2,
	date_format(k.date, '%b') as month_2_name,
	year(k.date) as month_2_year,
	l.count AS month_1,
	date_format(l.date, '%b') as month_1_name,
	year(l.date) as month_1_year
	FROM
	mstfacility
	LEFT JOIN(
	SELECT
	DATE as date,
	id_mstfacility,
	SUM(cascade_treatment_initiated) AS COUNT
	FROM
	`tblsummary_new`
	WHERE
	MONTH(DATE) = MONTH(CURDATE()) AND YEAR(DATE) = YEAR(CURDATE()) AND id_mstfacility BETWEEN 1 AND 25
	GROUP BY
	id_mstfacility) a
	ON
	mstfacility.id_mstfacility = a.id_mstfacility
	LEFT JOIN(
	SELECT
	DATE as date,
	id_mstfacility,
	SUM(cascade_treatment_initiated) AS COUNT
	FROM
	`tblsummary_new`
	WHERE
	DATE BETWEEN DATE_FORMAT(
	DATE(CURDATE() - INTERVAL 1 MONTH),
	'%Y-%m-01') AND last_day(
	DATE(CURDATE() - INTERVAL 1 MONTH)) AND id_mstfacility BETWEEN 1 AND 25
	GROUP BY
	id_mstfacility
	) b
	ON
	mstfacility.id_mstfacility = b.id_mstfacility
	LEFT JOIN(
	SELECT
	DATE as date,
	id_mstfacility,
	SUM(cascade_treatment_initiated) AS COUNT
	FROM
	`tblsummary_new`
	WHERE
	DATE BETWEEN DATE_FORMAT(
	DATE(CURDATE() - INTERVAL 2 MONTH),
	'%Y-%m-01') AND last_day(
	DATE(CURDATE() - INTERVAL 2 MONTH)) AND id_mstfacility BETWEEN 1 AND 25
	GROUP BY
	id_mstfacility
	) c
	ON
	mstfacility.id_mstfacility = c.id_mstfacility
	LEFT JOIN(
	SELECT
	DATE as date,
	id_mstfacility,
	SUM(cascade_treatment_initiated) AS COUNT
	FROM
	`tblsummary_new`
	WHERE
	DATE BETWEEN DATE_FORMAT(
	DATE(CURDATE() - INTERVAL 3 MONTH),
	'%Y-%m-01') AND last_day(
	DATE(CURDATE() - INTERVAL 3 MONTH)) AND id_mstfacility BETWEEN 1 AND 25
	GROUP BY
	id_mstfacility
	) d
	ON
	mstfacility.id_mstfacility = d.id_mstfacility
	LEFT JOIN(
	SELECT
	DATE as date,
	id_mstfacility,
	SUM(cascade_treatment_initiated) AS COUNT
	FROM
	`tblsummary_new`
	WHERE
	DATE BETWEEN DATE_FORMAT(
	DATE(CURDATE() - INTERVAL 4 MONTH),
	'%Y-%m-01') AND last_day(
	DATE(CURDATE() - INTERVAL 4 MONTH)) AND id_mstfacility BETWEEN 1 AND 25
	GROUP BY
	id_mstfacility
	) e
	ON
	mstfacility.id_mstfacility = e.id_mstfacility
	LEFT JOIN(
	SELECT
	DATE as date,
	id_mstfacility,
	SUM(cascade_treatment_initiated) AS COUNT
	FROM
	`tblsummary_new`
	WHERE
	DATE BETWEEN DATE_FORMAT(
	DATE(CURDATE() - INTERVAL 5 MONTH),
	'%Y-%m-01') AND last_day(
	DATE(CURDATE() - INTERVAL 5 MONTH)) AND id_mstfacility BETWEEN 1 AND 25
	GROUP BY
	id_mstfacility
	) f
	ON
	mstfacility.id_mstfacility = f.id_mstfacility
	LEFT JOIN(
	SELECT
	DATE as date,
	id_mstfacility,
	SUM(cascade_treatment_initiated) AS COUNT
	FROM
	`tblsummary_new`
	WHERE
	DATE BETWEEN DATE_FORMAT(
	DATE(CURDATE() - INTERVAL 6 MONTH),
	'%Y-%m-01') AND last_day(
	DATE(CURDATE() - INTERVAL 6 MONTH)) AND id_mstfacility BETWEEN 1 AND 25
	GROUP BY
	id_mstfacility
	) g
	ON
	mstfacility.id_mstfacility = g.id_mstfacility
	LEFT JOIN(
	SELECT
	DATE as date,
	id_mstfacility,
	SUM(cascade_treatment_initiated) AS COUNT
	FROM
	`tblsummary_new`
	WHERE
	DATE BETWEEN DATE_FORMAT(
	DATE(CURDATE() - INTERVAL 7 MONTH),
	'%Y-%m-01') AND last_day(
	DATE(CURDATE() - INTERVAL 7 MONTH)) AND id_mstfacility BETWEEN 1 AND 25
	GROUP BY
	id_mstfacility
	) h
	ON
	mstfacility.id_mstfacility = h.id_mstfacility
	LEFT JOIN(
	SELECT
	DATE as date,
	id_mstfacility,
	SUM(cascade_treatment_initiated) AS COUNT
	FROM
	`tblsummary_new`
	WHERE
	DATE BETWEEN DATE_FORMAT(
	DATE(CURDATE() - INTERVAL 8 MONTH),
	'%Y-%m-01') AND last_day(
	DATE(CURDATE() - INTERVAL 8 MONTH)) AND id_mstfacility BETWEEN 1 AND 25
	GROUP BY
	id_mstfacility
	) i
	ON
	mstfacility.id_mstfacility = i.id_mstfacility
	LEFT JOIN(
	SELECT
	DATE as date,
	id_mstfacility,
	SUM(cascade_treatment_initiated) AS COUNT
	FROM
	`tblsummary_new`
	WHERE
	DATE BETWEEN DATE_FORMAT(
	DATE(CURDATE() - INTERVAL 9 MONTH),
	'%Y-%m-01') AND last_day(
	DATE(CURDATE() - INTERVAL 9 MONTH)) AND id_mstfacility BETWEEN 1 AND 25
	GROUP BY
	id_mstfacility
	) j
	ON
	mstfacility.id_mstfacility = j.id_mstfacility
	LEFT JOIN(
	SELECT
	DATE as date,
	id_mstfacility,
	SUM(cascade_treatment_initiated) AS COUNT
	FROM
	`tblsummary_new`
	WHERE
	DATE BETWEEN DATE_FORMAT(
	DATE(CURDATE() - INTERVAL 10 MONTH),
	'%Y-%m-01') AND last_day(
	DATE(CURDATE() - INTERVAL 10 MONTH)) AND id_mstfacility BETWEEN 1 AND 25
	GROUP BY
	id_mstfacility
	) k
	ON
	mstfacility.id_mstfacility = k.id_mstfacility
	LEFT JOIN(
	SELECT
	DATE as date,
	id_mstfacility,
	SUM(cascade_treatment_initiated) AS COUNT
	FROM
	`tblsummary_new`
	WHERE
	DATE BETWEEN DATE_FORMAT(
	DATE(CURDATE() - INTERVAL 11 MONTH),
	'%Y-%m-01') AND last_day(
	DATE(CURDATE() - INTERVAL 11 MONTH)) AND id_mstfacility BETWEEN 1 AND 25
	GROUP BY
	id_mstfacility
	) l
	ON
	mstfacility.id_mstfacility = l.id_mstfacility) o on mstfacility.id_mstfacility = o.id_mstfacility";

	$sql .=" left join (SELECT
	f.id_mstfacility,
	a.count as regimen1,
	b.count as regimen2,
	c.count as regimen3,
	d.count as regimen4
	FROM
	mstfacility f
	LEFT JOIN(
	SELECT
	COUNT(patientguid) AS COUNT,
	id_mstfacility
	FROM
	`tblpatient`
	WHERE
	T_Regimen = 1
	GROUP BY
	id_mstfacility
	) a
	ON
	f.id_mstfacility = a.id_mstfacility
	LEFT JOIN(
	SELECT
	COUNT(patientguid) AS COUNT,
	id_mstfacility
	FROM
	`tblpatient`
	WHERE
	T_Regimen = 2
	GROUP BY
	id_mstfacility
	) b
	ON
	f.id_mstfacility = b.id_mstfacility
	LEFT JOIN(
	SELECT
	COUNT(patientguid) AS COUNT,
	id_mstfacility
	FROM
	`tblpatient`
	WHERE
	T_Regimen = 3
	GROUP BY
	id_mstfacility
	) c
	ON
	f.id_mstfacility = c.id_mstfacility
	LEFT JOIN(
	SELECT
	COUNT(patientguid) AS COUNT,
	id_mstfacility
	FROM
	`tblpatient`
	WHERE
	T_Regimen = 4
	GROUP BY
	id_mstfacility
	) d
	ON
	f.id_mstfacility = d.id_mstfacility) p on mstfacility.id_mstfacility = p.id_mstfacility";

	$sql .= " left join(SELECT p.id_mstfacility, count(*) as SVR_due FROM `tblpatient` p inner join cumulative_svr c on p.patientguid = c.patientguid where p.ETR_HCVViralLoad_Dt is not null and c.svr_date is null group by id_mstfacility order by id_mstfacility) q on mstfacility.id_mstfacility = q.id_mstfacility";

// echo $sql; die();

	$sql .= " where mstfacility.id_mstfacility between 1 and 25";

	$res = $this->Common_Model->query_data($sql);


	$total = array();
	foreach ($res as $row) {
		$total['gen1_success']                  += $row->gen1_success;
		$total['gen1_notsuccess']               += $row->gen1_notsuccess;
		$total['gen2_success']                  += $row->gen2_success;
		$total['gen2_notsuccess']               += $row->gen2_notsuccess;
		$total['gen3_success']                  += $row->gen3_success;
		$total['gen3_notsuccess']               += $row->gen3_notsuccess;
		$total['gen4_success']                  += $row->gen4_success;
		$total['gen4_notsuccess']               += $row->gen4_notsuccess;
		$total['gen5_success']                  += $row->gen5_success;
		$total['gen5_notsuccess']               += $row->gen5_notsuccess;
		$total['gen6_success']                  += $row->gen6_success;
		$total['gen6_notsuccess']               += $row->gen6_notsuccess;
		$total['gen_notavailable_success']      += $row->gen6_success;
		$total['gen_notavailable_notsuccess']   += $row->gen6_notsuccess;
		$total['Cir']                           += $row->Cir;
		$total['Noncir']                        += $row->Noncir;
		$total['aa']                            += $row->aa;
		$total['bb']                            += $row->bb;
		$total['cc']                            += $row->cc;
		$total['dd']                            += $row->dd;
		$total['ee']                            += $row->ee;
		$total['ff']                            += $row->ff;
		$total['gg']                            += $row->gg;
		$total['Unsafe_Injection_Use']          += $row->Unsafe_Injection_Use;
		$total['IVDU']                          += $row->IVDU;
		$total['Unprotected_Sexual_Practice']   += $row->Unprotected_Sexual_Practice;
		$total['Surgery']                       += $row->Surgery;
		$total['Dental']                        += $row->Dental;
		$total['Others']                        += $row->Others;
		$total['not_available']                 += $row->not_available;
		$total['cirr_treatment_success']        += $row->cirr_treatment_success;
		$total['cirr_treatment_not_success']    += $row->cirr_treatment_not_success;
		$total['no_cirr_treatment_success']     += $row->no_cirr_treatment_success;
		$total['no_cirr_treatment_not_success'] += $row->no_cirr_treatment_not_success;
		$total['reg1_success']                  += $row->reg1_success;
		$total['reg1_notsuccess']               += $row->reg1_notsuccess;
		$total['reg2_success']                  += $row->reg2_success;
		$total['reg2_notsuccess']               += $row->reg2_notsuccess;
		$total['reg3_success']                  += $row->reg3_success;
		$total['reg3_notsuccess']               += $row->reg3_notsuccess;
		$total['reg4_success']                  += $row->reg4_success;
		$total['reg4_notsuccess']               += $row->reg4_notsuccess;
		$total['Genotype1']                     += $row->Genotype1;
		$total['Genotype2']                     += $row->Genotype2;
		$total['Genotype3']                     += $row->Genotype3;
		$total['Genotype4']                     += $row->Genotype4;
		$total['Genotype5']                     += $row->Genotype5;
		$total['Genotype6']                     += $row->Genotype6;
		$total['a_success']                     += $row->a_success;
		$total['b_notsuccess']                  += $row->b_notsuccess;
		$total['c_success']                     += $row->c_success;
		$total['d_notsuccess']                  += $row->d_notsuccess;
		$total['e_success']                     += $row->e_success;
		$total['f_notsuccess']                  += $row->f_notsuccess;
		$total['g_success']                     += $row->g_success;
		$total['h_notsuccess']                  += $row->h_notsuccess;
		$total['i_success']                     += $row->i_success;
		$total['j_notsuccess']                  += $row->j_notsuccess;
		$total['k_success']                     += $row->k_success;
		$total['l_notsuccess']                  += $row->l_notsuccess;
		$total['m_success']                     += $row->m_success;
		$total['n_notsuccess']                  += $row->n_notsuccess;
		$total['male_success']                  += $row->male_success;
		$total['male_notsuccess']               += $row->male_notsuccess;
		$total['female_success']                += $row->female_success;
		$total['female_notsuccess']             += $row->female_notsuccess;
		$total['transgender_success']           += $row->transgender_success;
		$total['transgender_notsuccess']        += $row->transgender_notsuccess;
		$total['Missed_Followup']               += $row->Missed_Followup;
		$total['Missed_SVR']                    += $row->Missed_SVR;
		$total['Missed_TreatmentInitiation']    += $row->Missed_TreatmentInitiation;
		$total['Missed_ConfirmatoryPending']    += $row->Missed_ConfirmatoryPending;
		$total['LFU_Followup']                  += $row->LFU_Followup;
		$total['LFU_SVR']                       += $row->LFU_SVR;
		$total['LFU_TreatmentInitiation']       += $row->LFU_TreatmentInitiation;
		$total['LFU_ConfirmatoryPending']       += $row->LFU_ConfirmatoryPending;
		$total['SVR_Not_Achieved']              += $row->SVR_Not_Achieved;
		$total['SVR_Achieved']                  += $row->SVR_Achieved;
		$total['health_care_worker']            += $row->health_care_worker;
		$total['dental_worker']                 += $row->dental_worker;
		$total['lab_technician']                += $row->lab_technician;
		$total['barber_beautician']             += $row->barber_beautician;
		$total['cleaner']                       += $row->Cleaner;
		$total['truck_driver']                  += $row->truck_driver;
		$total['farmer']                        += $row->farmer;
		$total['labourer']                      += $row->labourer;
		$total['service']                       += $row->service;
		$total['others']                        += $row->others;
		$total['month_12']                      += $row->month_12;
		$total['month_11']                      += $row->month_11;
		$total['month_10']                      += $row->month_10;
		$total['month_9']                       += $row->month_9;
		$total['month_8']                       += $row->month_8;
		$total['month_7']                       += $row->month_7;
		$total['month_6']                       += $row->month_6;
		$total['month_5']                       += $row->month_5;
		$total['month_4']                       += $row->month_4;
		$total['month_3']                       += $row->month_3;
		$total['month_2']                       += $row->month_2;
		$total['month_1']                       += $row->month_1;
		$total['regimen1']                       += $row->regimen1;
		$total['regimen2']                       += $row->regimen2;
		$total['regimen3']                       += $row->regimen3;
		$total['regimen4']                       += $row->regimen4;
		$total['SVR_due']                       += $row->SVR_due;
	}
	$total['month_12_name']       = $res[0]->month_12_name;
	$total['month_12_year']       = $res[0]->month_12_year;
	$total['month_11_name']       = $res[0]->month_11_name;
	$total['month_11_year']       = $res[0]->month_11_year;
	$total['month_10_name']       = $res[0]->month_10_name;
	$total['month_10_year']       = $res[0]->month_10_year;
	$total['month_9_name']        = $res[0]->month_9_name;
	$total['month_9_year']        = $res[0]->month_9_year;
	$total['month_8_name']        = $res[0]->month_8_name;
	$total['month_8_year']        = $res[0]->month_8_year;
	$total['month_7_name']        = $res[0]->month_7_name;
	$total['month_7_year']        = $res[0]->month_7_year;
	$total['month_6_name']        = $res[0]->month_6_name;
	$total['month_6_year']        = $res[0]->month_6_year;
	$total['month_5_name']        = $res[0]->month_5_name;
	$total['month_5_year']        = $res[0]->month_5_year;
	$total['month_4_name']        = $res[0]->month_4_name;
	$total['month_4_year']        = $res[0]->month_4_year;
	$total['month_3_name']        = $res[0]->month_3_name;
	$total['month_3_year']        = $res[0]->month_3_year;
	$total['month_2_name']        = $res[0]->month_2_name;
	$total['month_2_year']        = $res[0]->month_2_year;
	$total['month_1_name']        = $res[0]->month_1_name;
	$total['month_1_year']        = $res[0]->month_1_year;
	$total['facility_name']       = 'All Facility';
	$total['id_mstfacility']      = '9999';
	$total['facility_short_name'] = 'All Facility';
	$total['hospital']            = 'All';

	foreach ($total as $key => $value) {
		$total_string[$key] = "$value";
	}

	$res[] = $total_string;

	return $res;
}
public function download_all_reports()
{

	header('Content-Type: text/json');

	$district_wise = $this->cascade_download();

	$all_reports = $this->get_all_reports();

	echo json_encode(array("district_wise" => $district_wise, "all_reports" => $all_reports));
}

public function download_tblSVRFollowUp()
{
	header("Content-type: application/json");

	if($this->input->post('username') === null || $this->input->post('password') === null)
	{
		echo json_encode("ERROR : You must send username and password with the request");
		die();
	}
	$username = $this->security->xss_clean($this->input->post('username'));
	$password = $this->security->xss_clean($this->input->post('password'));

	$authlogin = $this->check_auth_login($username, $password);

	if(!$authlogin)
	{
		echo json_encode("Incorrect username and/or password");
	}
	else
	{
		$id_mstfacility = $this->user->id_mstfacility;
		$id_tblusers   = $this->user->id_tblusers;

		$this->db->where('id_mstfacility', $id_mstfacility);
		$res = $this->db->get('tblSVRfollowup')->result();

		echo json_encode(array('tblSVRfollowup' => $res));
	}
}

public function upload_tblSVRFollowUp()
{
	header("Content-type: application/json");

	if($this->input->post('username') === null || $this->input->post('password') === null)
	{
		echo json_encode("ERROR : You must send username and password with the request");
		die();
	}
	$username = $this->security->xss_clean($this->input->post('username'));
	$password = $this->security->xss_clean($this->input->post('password'));

	$authlogin = $this->check_auth_login($username, $password);

	if(!$authlogin)
	{
		echo json_encode("Incorrect username and/or password");
	}
	else
	{
		$res = $this->Chai_api_model->upload_tblSVRFollowUp();

		if($res == 1)
		{
			echo "success";
		}
		else
		{
			echo 'error in saving record';
		}
	}
}

public function save_data_json()
{
		// header("Content-type: application/json");

	if($this->input->post('username') === null || $this->input->post('password') === null)
	{
		echo json_encode("ERROR : You must send username and password with the request");
		die();
	}
	$username = $this->security->xss_clean($this->input->post('username'));
	$password = $this->security->xss_clean($this->input->post('password'));

	$authlogin = $this->check_auth_login($username, $password);

	if(!$authlogin)
	{
		echo json_encode("Incorrect username and/or password");
	}
	else
	{
		$id_mstfacility = $this->user->id_mstfacility;
		$id_tblusers   = $this->user->id_tblusers;

		$this->db->where('id_mstfacility', $id_mstfacility);
		$facility = $this->db->get('mstfacility')->result();


		$target_directory = "application/uploaded_json/";
		$target_file = $target_directory.$facility[0]->FacilityCode."_".date('Y_m_d')."_".basename($_FILES['json_file']['name']);

		if(move_uploaded_file($_FILES['json_file']['tmp_name'], $target_file))
		{

			$insert_array = array(
				"id_mstfacility" => $id_mstfacility,
				"file_name"      => $target_file,
				"date_uploaded"  => date('Y-m-d H:i:s'),
			);

			$this->db->insert('tbl_uploaded_json', $insert_array);
			echo "success";
		}
		else
		{
			echo "error in uploading file";
		}

	}
}

public function getExperiencedPatientData()
{
	header("Content-type: application/json");

	if($this->input->post('username') === null || $this->input->post('password') === null)
	{
		echo json_encode("ERROR : You must send username and password with the request");
		die();
	}

	$username     = $this->security->xss_clean($this->input->post('username'));
	$password     = $this->security->xss_clean($this->input->post('password'));
	$uid_num      = $this->security->xss_clean($this->input->post('uid_num'));
	$uidprefix   = $this->security->xss_clean($this->input->post('uidprefix'));
	$patient_type = $this->security->xss_clean($this->input->post('patient_type'));

	$authlogin = $this->check_auth_login($username, $password);

	if(!$authlogin)
	{
		echo json_encode("Incorrect username and/or password");
	}
	else
	{
		$query = "SELECT
		`PatientGUID`,
		`Aadhaar`,
		`Gender`,
		`DOBAv`,
		`DOB`,
		`AgeAsOn`,
		`RiskFactor`,
		`FatherHusband`,
		`Urban_Rural`,
		`Add1`,
		`Add2`,
		`POBox`,
		`Street`,
		`Locality`,
		`VillageTown`,
		`District`,
		`Block`,
		`PIN`,
		`Mobile`,
		`AltContact`,
		`T_AntiHCV01_Date`,
		`T_AntiHCV01_Result`,
		`T_DLL_01_VLC_Date`,
		`T_DLL_01_VLCount`,
		`T_DLL_01_VLC_Result`,
		`V1_ArrivalHospital`,
		`MedicalSpecialist`,
		`V1_Haemoglobin`,
		`V1_Albumin`,
		`V1_Bilrubin`,
		`V1_INR`,
		`V1_Cirrhosis`,
		`Cirr_TestDate`,
		`Cirr_Encephalopathy`,
		`Cirr_Ascites`,
		`Cirr_VaricealBleed`,
		`ChildScore`,
		`Result`,
		`Age`,
		`FirstName`,
		`Pregnant`,
		`DeliveryDt`,
		`HCVHistory`,
		`HCVPastTreatment`,
		`HCVPastOutcome`,
		`HCVSVRDt`,
		`V1_Platelets`,
		`IsMobile_Landline`,
		`PastTreatmentDuration`,
		`PastTreatment`,
		`PastDuration`,
		`PreviousTreatingHospital`,
		`PreviousUIDNumber`,
		`OccupationID`,
		`OccupationOther`,
		`TestType`,
		`Weight`,
		`CKDStage`,
		`NWeeksCompleted`,
		`LastPillDate`,
		`PastRegimen`,
		`PreviousDuration`,
		`ScreeningLabType`,
		`ConfirmatoryLabType`,
		`ConfirmatoryTestingLab`,
		`PGIReferred`,
		`Referredby`,
		`PGIArrival`,
		`PGIArrivaldate`,
		`PGITSpecialist`,
		`PGIPrescribingdate`,
		`ETR_HCVViralLoad_Dt`,
		`ETR_HCVViralLoadCount`,
		`ETR_Result`,
		`SVR12W_HCVViralLoad_Dt`,
		`SVR12W_HCVViralLoadCount`,
		`SVR12W_Result`,
		`JailNo`,
		`T_Initiation`,
		`Status`,
		`T_DurationValue`,
		`T_Regimen`,
		`HospitalName`,
		`JailName`,
		`PGITSpecialistName`
		FROM
		`tblpatient` where UID_Num = ".trim($uid_num)." and UID_Prefix = ".trim($uidprefix);

		$result_tblpatient = $this->db->query($query)->result();

		if($result_tblpatient[0]->T_AntiHCV01_Result == 1 && $result_tblpatient[0]->T_DLL_01_VLC_Result == 1)
		{
			$result_tblpatient[0]->HCVHistory = 1;
		}
		else
		{
			$result_tblpatient[0]->HCVHistory = 0;
		}

		if($result_tblpatient[0]->SVR12W_HCVViralLoad_Dt != null)
		{
			$result_tblpatient[0]->T_DLL_01_VLC_Date   = $result_tblpatient[0]->SVR12W_HCVViralLoad_Dt;
			$result_tblpatient[0]->T_DLL_01_VLCount    = $result_tblpatient[0]->SVR12W_HCVViralLoadCount;
			$result_tblpatient[0]->T_DLL_01_VLC_Result = $result_tblpatient[0]->SVR12W_Result;
		}
		else if($result_tblpatient[0]->ETR_HCVViralLoad_Dt != null)
		{
			$result_tblpatient[0]->T_DLL_01_VLC_Date   = $result_tblpatient[0]->ETR_HCVViralLoad_Dt;
			$result_tblpatient[0]->T_DLL_01_VLCount    = $result_tblpatient[0]->ETR_HCVViralLoadCount;
			$result_tblpatient[0]->T_DLL_01_VLC_Result = $result_tblpatient[0]->ETR_Result;
		}

				// additional data changes


		if($result_tblpatient[0]->T_DurationValue == 99)
		{
					// $result_tblpatient[0]->PreviousDuration = $result_tblpatient[0]->T_DurationOther;

			if($result_tblpatient[0]->T_DurationOther == 1)
			{
				$result_tblpatient[0]->PreviousDuration = 1;
			}
			else if($result_tblpatient[0]->T_DurationOther == 2)
			{
				$result_tblpatient[0]->PreviousDuration = 2;
			}
			else if($result_tblpatient[0]->T_DurationOther == 3)
			{
				$result_tblpatient[0]->PreviousDuration = 4;
			}
			else if($result_tblpatient[0]->T_DurationOther == 4)
			{
				$result_tblpatient[0]->PreviousDuration = 5;
			}

		}
		else
		{
			if($result_tblpatient[0]->T_DurationValue == 12)
			{
				$result_tblpatient[0]->PreviousDuration = 3;
			}
			else if($result_tblpatient[0]->T_DurationValue == 24)
			{
				$result_tblpatient[0]->PreviousDuration = 6;
			}
		}

		$sql = "SELECT * FROM `mst_past_regimen` where mapping_to_mst_regimen = ".$result_tblpatient[0]->T_Regimen;
		$result_regimen = $this->db->query($sql)->result();

		if(count($result_regimen) > 0)
		{

					$result_tblpatient[0]->PastRegimen = $result_regimen[0]->id_mst_past_regimen; // regimen;
				}
				else
				{
					$result_tblpatient[0]->PastRegimen = null;
				}

				$result_tblpatient[0]->NWeeksCompleted = null; // null by default

				if($result_tblpatient[0]->SVR_TreatmentStatus == 7)
				{

					$query = "select max(VisitNo) as visits from tblpatientvisit where patientguid = '".$result_tblpatient[0]->PatientGUID."'";
					$result_nweeks = $this->db->query($query)->result();

					if(count($result_nweeks) > 0)
					{
						$result_tblpatient[0]->NWeeksCompleted = $result_nweeks[0]->visits; // weeks in treatment;
					}
					else
					{
						if($result_tblpatient[0]->T_Initiation != null)
						{
							$result_tblpatient[0]->NWeeksCompleted = 1;
						}
						else
						{
							$result_tblpatient[0]->NWeeksCompleted = 0;
						}
					}

					if($result_tblpatient[0]->Status == 30)
					{
						$result_tblpatient[0]->HCVPastTreatment = 3;
						$result_tblpatient[0]->HCVPastOutcome = 1;
					}
					else if($result_tblpatient[0]->Status == 14)
					{
						$result_tblpatient[0]->HCVPastTreatment = 3;
						$result_tblpatient[0]->HCVPastOutcome = 2;
						$result_tblpatient[0]->HCVSVRDt = $result_tblpatient[0]->SVR12W_HCVViralLoad_Dt;
					}
					else if($result_tblpatient[0]->Status == 15)
					{
						$result_tblpatient[0]->HCVPastTreatment = 3;
						$result_tblpatient[0]->HCVPastOutcome = 3;
						$result_tblpatient[0]->HCVSVRDt = $result_tblpatient[0]->SVR12W_HCVViralLoad_Dt;
					}
					else
					{
						$result_tblpatient[0]->HCVPastTreatment = 2;
					}
				}else if($result_tblpatient[0]->SVR_TreatmentStatus != 7){

					if($result_tblpatient[0]->Status != 15 && $result_tblpatient[0]->Status != 14 && $result_tblpatient[0]->Status != 13)
					{
						$result_tblpatient[0]->HCVPastTreatment = 2;
					}else
					{
						if($result_tblpatient[0]->Status == 13)
						{
							$result_tblpatient[0]->HCVPastTreatment = 3;
							$result_tblpatient[0]->HCVPastOutcome = 1;
						}
						else if($result_tblpatient[0]->Status == 14)
						{
							$result_tblpatient[0]->HCVPastTreatment = 3;
							$result_tblpatient[0]->HCVPastOutcome = 2;
							$result_tblpatient[0]->HCVSVRDt = $result_tblpatient[0]->SVR12W_HCVViralLoad_Dt;
						}
						else if($result_tblpatient[0]->Status == 15)
						{
							$result_tblpatient[0]->HCVPastTreatment = 3;
							$result_tblpatient[0]->HCVPastOutcome = 3;
							$result_tblpatient[0]->HCVSVRDt = $result_tblpatient[0]->SVR12W_HCVViralLoad_Dt;
						}
					}
				}


				$data['tblpatient'] = $result_tblpatient;

				$query = "select * from tblpatientcirrohosis where patientguid = '".trim($result_tblpatient[0]->PatientGUID)."'";
				$result_tblpatientcirrohosis = $this->db->query($query)->result();

				if(count($result_tblpatientcirrohosis) == 0)
				{
					$result_tblpatientcirrohosis = array();
				}

				$data['tblpatientcirrohosis'] = $result_tblpatientcirrohosis;

				$query = "select * from tblRiskProfile where patientguid = '".trim($result_tblpatient[0]->PatientGUID)."'";
				$result_tblRiskProfile = $this->db->query($query)->result();

				if(count($result_tblRiskProfile) == 0)
				{
					$result_tblRiskProfile = array();
				}

				$data['tblRiskProfile'] = $result_tblRiskProfile;

				echo json_encode($data);
			}

		}

		public function initiate_transfer_request()
		{
			header("Content-type: application/json");

			if($this->input->post('username') === null || $this->input->post('password') === null)
			{
				echo json_encode("ERROR : You must send username and password with the request");
				die();
			}

			$username       = $this->security->xss_clean($this->input->post('username'));
			$password       = $this->security->xss_clean($this->input->post('password'));
			$uid_num        = $this->security->xss_clean($this->input->post('uid_num'));
			$id_mstfacility = $this->security->xss_clean($this->input->post('id_mstfacility'));

			$authlogin = $this->check_auth_login($username, $password);

			if(!$authlogin)
			{
				echo json_encode("Incorrect username and/or password");
			}
			else
			{
				$login_id_mstfacility = $this->getfacilityid($username, $password);

				$data = array();

				$this->db->where('UID_Num', $uid_num);
				$this->db->where('id_mstfacility', $id_mstfacility);
				$result = $this->db->get('tblpatient')->result();

				$data['tblpatient'] = $result;

				if($result[0]->Status == 14 || $result[0]->Status == 15 || $result[0]->Status == 31)
				{
					echo '0';
				}
				else
				{
				$patientguid = $result[0]->PatientGUID;
				if(count($result) > 0)
				{
					$update_array = array(
						"TransferRequest"      => 1,
						"TransferToFacility"   => $login_id_mstfacility,
						"TransferFromFacility" => $id_mstfacility,
					);
					$this->db->update('tblpatient',$update_array, array("PatientGUID" =>$patientguid));


					$this->db->where('patientguid', $patientguid);
					$result = $this->db->get('tblpatientvisit')->result();



					if(count($result) > 0)
					{
						foreach ($result as $visit) {
							$visit->PatientVisitGUID = uniqid();
						}
					}

					$data['tblpatientvisit'] = $result;
				// print_r($data['tblpatientvisit']);

				// echo $this->db->last_query(); die();
					$this->db->where('patientguid', $patientguid);
					$result = $this->db->get('tblpatientcirrohosis')->result();

					$data['tblpatientcirrohosis'] = $result;

					$this->db->where('patientguid', $patientguid);
					$result = $this->db->get('tblpatient_regimen_drug_data')->result();

					$data['tblpatient_regimen_drug_data'] = $result;

					$this->db->where('patientguid', $patientguid);
					$result = $this->db->get('tblRiskProfile')->result();

					$data['tblRiskProfile'] = $result;

				}

				echo json_encode($data);
			}

			}
		}

		public function get_transfer_requests()
		{
			header("Content-type: application/json");

			if($this->input->post('username') === null || $this->input->post('password') === null)
			{
				echo json_encode("ERROR : You must send username and password with the request");
				die();
			}
			$username = $this->security->xss_clean($this->input->post('username'));
			$password = $this->security->xss_clean($this->input->post('password'));

			$authlogin = $this->check_auth_login($username, $password);

			if(!$authlogin)
			{
				echo json_encode("Incorrect username and/or password");
			}
			else
			{
				$id_mstfacility = $this->getfacilityid($username, $password);

				$this->db->select('PatientGUID,TransferRequest,TransferRequestAccepted ,TransferFromFacility,TransferToFacility');
				$this->db->where('id_mstfacility', $id_mstfacility);
				$this->db->where('TransferRequest', 1);
				$this->db->where('(TransferRequestAccepted is null or TransferRequestAccepted = 0)');
				$result = $this->db->get('tblpatient')->result();

				echo json_encode(array("patients" => $result));
			}
		}



		public function save_treatment_card_image()
		{
		// header("Content-type: application/json");

			if($this->input->post('username') === null || $this->input->post('password') === null)
			{
				echo json_encode("ERROR : You must send username and password with the request");
				die();
			}
			$username = $this->security->xss_clean($this->input->post('username'));
			$password = $this->security->xss_clean($this->input->post('password'));

			$authlogin = $this->check_auth_login($username, $password);

			if(!$authlogin)
			{
				echo json_encode("Incorrect username and/or password");
			}
			else
			{
				$id_mstfacility = $this->user->id_mstfacility;
				$id_tblusers   = $this->user->id_tblusers;

				$this->db->where('id_mstfacility', $id_mstfacility);
				$facility = $this->db->get('mstfacility')->result();


				$target_directory = FCPATH."application/uploads/treatment_card_images/";
				$target_file = $target_directory.$facility[0]->FacilityCode."_".date('YmdHis')."_".basename($_FILES['treatment_card_image']['name']);

				$file_name = $facility[0]->FacilityCode."_".date('YmdHis')."_".basename($_FILES['treatment_card_image']['name']); 

				$names = explode('.', basename($_FILES['treatment_card_image']['name']) );
				$patient_uid = $names[0];
				
				$sql = "select patientguid as patientguid from tblpatient where concat(uid_prefix,'-', uid_num) = '".$patient_uid."'";
				$result = $this->db->query($sql)->result();

				if(count($result) > 0)
				{

				if(move_uploaded_file($_FILES['treatment_card_image']['tmp_name'], $target_file))
				{
					$insert_array = array(
						"patientguid" => $result[0]->patientguid,
						"patient_uid" => $patient_uid,
						"file_path"   => $file_name,
						);

					$this->db->insert('tbl_treatment_card_file_paths', $insert_array);

					echo "success";
				}
				else
				{
					echo "error in uploading file";
				}
				}
				else
				{
					echo "No patient with this uid found";
				}

			}
		}

		public function download_single_record_art()
		{

			header("Content-type: application/json");

			if($this->input->post('username') === null || $this->input->post('password') === null)
			{
				echo json_encode("Please send username/password along with the request");
				die();
			}

			$username                = $this->security->xss_clean($this->input->post('username'));
			$password                = $this->security->xss_clean($this->input->post('password'));
			$id_mst_diagnostics_labs = $this->security->xss_clean($this->input->post('id_mst_diagnostics_labs'));
			$uid                     = $this->security->xss_clean($this->input->post('uid'));

			$authlogin = $this->check_auth_login($username, $password);

			if(!$authlogin)
			{
				echo json_encode(array("Username/Password is incorrect"));
				die();
			}
			else
			{
				$flag = 0;

				$userid     = $this->user->id_tblusers;
				$facilityid = $this->user->id_mstfacility;

				$sql = "SELECT
    `id_tbldiagnostic_labs_form2`,
    `testing_center_name`,
    `hcv_uid_prefix`,
    `hcv_uid_num`,
    `art_no`,
    `name`,
    `father_husband_name`,
    `gender`,
    `age`,
    `district`,
    `block`,
    `village_town`,
    `area`,
    `pin`,
    `contact_no`,
    `art_treatment_line`,
    `art_treatment_regimen`,
    `art_treatment_regimen_other`,
    `risk_profile`,
    `risk_profile_other`,
    `occupation`,
    `occupation_other`,
    date_format(`screening_test_date`,'%Y-%m-%d') as screening_test_date,
    `screening_test_result`,
    date_format(`screening_sample_coll_dt`,'%Y-%m-%d') as screening_sample_coll_dt,
    `screening_sample_shipped_to`,
    date_format(`screening_sample_shipped_dt`,'%Y-%m-%d') as screening_sample_shipped_dt,
    `screening_storage_temp`,
    `preferred_treatment_hospital`,
    date_format(`vl_sample_receipt_dt`,'%Y-%m-%d') as vl_sample_receipt_dt,
    `vl_sample_received_temp`,
    date_format(`vl_test_date`,'%Y-%m-%d') as vl_test_date,
    `vl_gx_lab_name`,
    `vl_viral_load`,
    `vl_test_result`,
    date_format(`genotype_sample_date`,'%Y-%m-%d') as genotype_sample_date,
    `genotype_lab_name`,
    date_format(`genotype_test_date`,'%Y-%m-%d') as genotype_test_date,
    `genotype_test_result`,
    date_format(`created_on`,'%Y-%m-%d') as created_on,
    `created_by`,
    date_format(`updated_on`,'%Y-%m-%d') as updated_on,
    `updated_by`,
    `is_deleted`,
    `screening_sample_coll_dt_hh`,
    `screening_sample_coll_dt_mm`,
    `screening_sample_coll_dt_am_pm`,
    `screening_sample_shipped_dt_hh`,
    `screening_sample_shipped_dt_mm`,
    `screening_sample_shipped_dt_am_pm`,
    `vl_sample_receipt_dt_hh`,
    `vl_sample_receipt_dt_mm`,
    `vl_sample_receipt_dt_am_pm`,
    `Residential_address`
FROM
    `tbldiagnostic_labs_form2`
				WHERE
				testing_center_name = ".$id_mst_diagnostics_labs." AND hcv_uid_num = ".$uid." and is_deleted = 0 limit 1";

				$res = $this->Common_Model->query_data($sql);
				$data['tbldiagnostic_labs_form2'] = $res;

				echo json_encode($data);
			}
		}

		public function download_referred_to_data()
		{
			header("Content-type: application/json");

			if($this->input->post('username') === null || $this->input->post('password') === null)
			{
				echo json_encode("Please send username/password along with the request");
				die();
			}

			$username = $this->security->xss_clean($this->input->post('username'));
			$password = $this->security->xss_clean($this->input->post('password'));

			$authlogin = $this->check_auth_login($username, $password);

			if(!$authlogin)
			{
				echo json_encode(array("Username/Password is incorrect"));
				die();
			}
			else
			{
				$userid     = $this->user->id_tblusers;
				$facilityid = $this->user->id_mstfacility;

			// screening data

				$sql = "SELECT
    `id_tbldiagnostic_labs_form2`,
    `testing_center_name`,
    `hcv_uid_prefix`,
    `hcv_uid_num`,
    `art_no`,
    `name`,
    `father_husband_name`,
    `gender`,
    `age`,
    `district`,
    `block`,
    `village_town`,
    `area`,
    `pin`,
    `contact_no`,
    `art_treatment_line`,
    `art_treatment_regimen`,
    `art_treatment_regimen_other`,
    `risk_profile`,
    `risk_profile_other`,
    `occupation`,
    `occupation_other`,
    date_format(`screening_test_date`,'%Y-%m-%d') as screening_test_date,
    `screening_test_result`,
    date_format(`screening_sample_coll_dt`,'%Y-%m-%d') as screening_sample_coll_dt,
    `screening_sample_shipped_to`,
    date_format(`screening_sample_shipped_dt`,'%Y-%m-%d') as screening_sample_shipped_dt,
    `screening_storage_temp`,
    `preferred_treatment_hospital`,
    date_format(`vl_sample_receipt_dt`,'%Y-%m-%d') as vl_sample_receipt_dt,
    `vl_sample_received_temp`,
    date_format(`vl_test_date`,'%Y-%m-%d') as vl_test_date,
    `vl_gx_lab_name`,
    `vl_viral_load`,
    `vl_test_result`,
    date_format(`genotype_sample_date`,'%Y-%m-%d') as genotype_sample_date,
    `genotype_lab_name`,
    date_format(`genotype_test_date`,'%Y-%m-%d') as genotype_test_date,
    `genotype_test_result`,
    date_format(`created_on`,'%Y-%m-%d') as created_on,
    `created_by`,
    date_format(`updated_on`,'%Y-%m-%d') as updated_on,
    `updated_by`,
    `is_deleted`,
    `screening_sample_coll_dt_hh`,
    `screening_sample_coll_dt_mm`,
    `screening_sample_coll_dt_am_pm`,
    `screening_sample_shipped_dt_hh`,
    `screening_sample_shipped_dt_mm`,
    `screening_sample_shipped_dt_am_pm`,
    `vl_sample_receipt_dt_hh`,
    `vl_sample_receipt_dt_mm`,
    `vl_sample_receipt_dt_am_pm`,
    `Residential_address`
FROM
    `tbldiagnostic_labs_form2`
				WHERE
				preferred_treatment_hospital = ".$facilityid." AND is_deleted = 0";

				$res = $this->Common_Model->query_data($sql);

				$data['tbldiagnostic_labs_form2'] = $res;

				echo json_encode($data);
			}
		}

/*public function download_pdf()
{
//echo 'sdfsdfsd';exit();
$report_id = $this->input->post('patientguid');
$result = $this->Chai_api_model->download_pdf($report_id);
echo json_encode($result);

}*/


public function download_pdf()
    {
    	ini_set('display_errors',1);
    	
    	$patientguid = $this->security->xss_clean($this->input->post('patientguid'));

 $sql = "SELECT * FROM `tblpatient` WHERE PatientGUID = ?";
		$result = $this->db->query($sql,[$patientguid])->result();
		//pr($result);exit;

		$content['patient_data'] = $result;
		$stateID = $result[0]->State;
		$district_Code = $result[0]->District;
		$BlockCode = $result[0]->Block;

		$sql = "SELECT * FROM `mstlookup` where flag = 3 and LanguageID = 1";
		$content['risk_factor'] = $this->db->query($sql)->result();

		if($stateID != null){
			$sql = "select StateName from mststate WHERE id_mststate = ?";
			$content['states'] = $this->db->query($sql,[$stateID])->result();
		}

		if($district_Code != null && $stateID != null){
			$sql = "select DistrictName from mstdistrict WHERE id_mstdistrict = ? AND id_mststate = ?";
			$content['district'] = $this->db->query($sql,[$district_Code,$stateID])->result();
		}

		if($district_Code != null && $stateID != null && $BlockCode != null){
			$sql = "select BlockName from mstblock WHERE id_mststate = ? AND id_mstdistrict = ? AND id_mstblock = ?";
			$content['block'] = $this->db->query($sql,[$stateID,$district_Code,$BlockCode])->result();
		}

		$sql = "SELECT * FROM `tblpatientcirrohosis` WHERE PatientGUID = ?";
		$patient_cirrohosis_data = $this->db->query($sql,[$patientguid])->result();

		$content['patient_cirrohosis_data'] = $patient_cirrohosis_data;

		$id_mstfacilit = $result[0]->PreviousTreatingHospital;

		if($id_mstfacilit != null OR $id_mstfacilit != 0){
			$sql = "SELECT FacilityCode FROM mstfacility WHERE id_mstfacility = ? ";
			$content['FacilityCode'] = $this->db->query($sql,[$id_mstfacilit])->result();
		}

		$id_mst_past_regimen = $result[0]->PastRegimen;

		if($id_mst_past_regimen != null OR $id_mst_past_regimen != 0){
			$sql = "SELECT regimen_name FROM `mst_past_regimen` where id_mst_past_regimen= ? AND is_deleted = 0";
			$content['past_regimen'] = $this->db->query($sql,[$id_mst_past_regimen])->result();
		}

		$LookupCode = $result[0]->PreviousDuration;

		if($LookupCode != null OR $LookupCode != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 28 and LanguageID = 1 AND LookupCode = ? ";
			$content['previous_duration'] = $this->db->query($sql,[$LookupCode])->result();
		}

		$weeks_LookupCode = $result[0]->NWeeksCompleted;

		if($weeks_LookupCode != null OR $weeks_LookupCode != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 28 and LanguageID = 1 AND LookupCode = ? ";
			$content['weeks'] = $this->db->query($sql,[$weeks_LookupCode])->result();
		}

		$ART_Regimen_LookupCode = $result[0]->ART_Regimen;

		if($ART_Regimen_LookupCode != null OR $ART_Regimen_LookupCode != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 42 and LanguageID = 1 AND LookupCode = ? ";
			$content['hiv_regimen'] = $this->db->query($sql,[$ART_Regimen_LookupCode])->result();
		}

		//renal_ckd
		$CKDStage_LookupCode = $result[0]->CKDStage;

		if($CKDStage_LookupCode != null OR $CKDStage_LookupCode != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 30 and LanguageID = 1 AND LookupCode = ? ";
			$content['renal_ckd'] = $this->db->query($sql,[$CKDStage_LookupCode])->result();
		}

		//referring_doctor

		$id_mst_medical_specialists = $result[0]->ReferingDoctor;

		$sql = "SELECT name FROM mst_medical_specialists WHERE id_mst_medical_specialists = ?";
		$content['referring_doctor'] = $this->db->query($sql,[$id_mst_medical_specialists])->result();

		//referred to			

		$sql = "SELECT facility_short_name FROM mstfacility where id_mstfacility = ? ";
		$content['referred_to'] = $this->db->query($sql,[$result[0]->ReferTo])->result();

		//prescribing_doctor
		$sql = "SELECT name FROM mst_medical_specialists where id_mst_medical_specialists = ?";
		$content['prescribing_doctor'] = $this->db->query($sql,[$result[0]->PrescribingDoctor])->result();

		//regimen_prescribed
		$regimen_prescribed_LookupCode = $result[0]->T_Regimen;

		if($regimen_prescribed_LookupCode != null OR $regimen_prescribed_LookupCode != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 10 and LanguageID = 1 AND LookupCode = ? ";
			$content['regimen_prescribed'] = $this->db->query($sql,[$regimen_prescribed_LookupCode])->result();
		}

		
		$sqlsic1 = "SELECT * FROM tblpatient_regimen_drug_data WHERE patientguid = ?";
		$regimen_drug_data = $this->db->query($sqlsic1,[$patientguid])->result();
		if(count($regimen_drug_data) > 0){
			$content['regimen_drug_data'] = $regimen_drug_data;
			
			// echo '<pre>';
			// print_r($regimen_drug_data);die();

			//Sofosbuvir
			$id_mst_drug = $regimen_drug_data[0]->id_mst_drugs;
			$sql = "SELECT strength FROM `mst_drug_strength` where id_mst_drug_strength = ?";
			$content['Sofosbuvir_mst_drug_strength'] = $this->db->query($sql, [$id_mst_drug])->result();

			//Daclatasvir
			$id_mst_drug_strength = $regimen_drug_data[1]->id_mst_drug_strength;

			$sql = "SELECT strength FROM `mst_drug_strength` where id_mst_drug_strength = ?";
			$content['Daclatasvir_mst_drug_strength'] = $this->db->query($sql, [$id_mst_drug_strength])->result();

			//Velpatasvir

			$id_mst_drug_strength = $regimen_drug_data[1]->id_mst_drug_strength;

			$sql = "SELECT strength FROM `mst_drug_strength` where id_mst_drug_strength = ?";
			$content['Velpatasvir_mst_drug_strength'] = $this->db->query($sql, [$id_mst_drug_strength])->result();

			// print_r($content['Velpatasvir_mst_drug_strength']);die();

			//Ribavrin

			$id_mst_drug_strength = $regimen_drug_data[2]->id_mst_drug_strength;

			$sql = "SELECT strength FROM `mst_drug_strength` where id_mst_drug_strength = ?";
			$content['Ribavrin_mst_drug_strength'] = $this->db->query($sql, [$id_mst_drug_strength])->result();

		}

		//Dispensation information
		$sql = "SELECT * FROM tblpatientvisit WHERE PatientGUID = ?";
		$Dispensation_res = $this->db->query($sql,[$patientguid])->result();
		$content['dispensation_result'] = $Dispensation_res;


		//patient svr result
		$SVR12W_Result_LookupCode = $result[0]->SVR12W_Result;

		if($SVR12W_Result_LookupCode != null OR $SVR12W_Result_LookupCode != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 5 and LanguageID = 1 AND LookupCode = ? ";
			$content['svr_result'] = $this->db->query($sql,[$SVR12W_Result_LookupCode])->result();
		}
		
		$InterruptReason = $result[0]->InterruptReason;
		$DeathReason = $result[0]->DeathReason;
		$LFUReason = $result[0]->LFUReason;
		$InterruptToStage = $result[0]->InterruptToStage;

		if($InterruptReason != null OR $InterruptReason != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 48 and LanguageID = 1 AND LookupCode = ? ";
			$content['InterruptReason'] = $this->db->query($sql,[$InterruptReason])->result();
		}

		if($DeathReason != null OR $DeathReason != 0){
			$sql = "SELECT LookupValue FROM `mstlookup` where flag = 48 and LanguageID = 1 AND LookupCode = ? ";
			$content['DeathReason'] = $this->db->query($sql,[$DeathReason])->result();
		}

	    //Load the library
	    $this->load->library('html2pdf');
	    
	    //Set folder to save PDF to
	    $this->html2pdf->folder('./pdf_reports/');
	    
	    //Set the filename to save/download as
	    $this->html2pdf->filename(''.$patientguid.'.pdf');
	    
	    //Set the paper defaults
	    $this->html2pdf->paper('a4', 'portrait');
	    
	    $data = array(
	    	'title' => 'PDF Created',
	    	'message' => 'Hello World!'
	    );
	    
	    //Load html view
	   $this->html2pdf->html($this->load->view('pages/components/printapi.php', $content, true));
	    
	    if($this->html2pdf->create('save')) {
	    	//PDF was successfully saved or downloaded
	    	
	    	//return ["filename" => $pdf_url];
	    	echo json_encode(["filename" => $patientguid.'.pdf']);
	    	
	    }
	    
    } 





	}
