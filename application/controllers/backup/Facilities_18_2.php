<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facilities extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Common_Model');
		$this->load->Model('Log4php_model');
		
		$loginData = $this->session->userdata('loginData');

		if($loginData == null)
		{
			redirect('login');
		}
		
	}


	public function index($id_mstfacility = null)
	{	
		$this->load->library('form_validation');
		$content['edit_flag'] = 0;
		$loginData = $this->session->userdata('loginData'); 
		$RequestMethod = $this->input->server('REQUEST_METHOD');

		if($RequestMethod == "POST")
		{
				$this->form_validation->set_rules('input_state', 'State', 'trim|required|xss_clean');
				$this->form_validation->set_rules('district', 'District', 'trim|required|xss_clean');
				$this->form_validation->set_rules('facility_code', 'Facility Code', 'trim|required|xss_clean');
				$this->form_validation->set_rules('facility_type', 'Facility Type', 'trim|required|xss_clean');
				$this->form_validation->set_rules('facility_short_name', 'Facility Short Name','regex_match[/^([a-z ])+$/i]', 'trim|required|xss_clean');

				if ($this->form_validation->run() == FALSE) {

					$arr['status'] = 'false';
					 $this->session->set_flashdata("tr_msg",validation_errors());
				}else{

			if($id_mstfacility != null)
				{
					$updateArray = array(
						'id_mststate'							=> $this->security->xss_clean($this->input->post('input_state')),
						'id_mstdistrict'       	                => $this->security->xss_clean($this->input->post('district')),
						'is_Mtc'       	                		=> $this->security->xss_clean($this->input->post('delivery_level')),
						'FacilityCode'       	                => $this->security->xss_clean($this->input->post('facility_code')),
						'FacilityType'			 	            => $this->security->xss_clean($this->input->post('facility_type')),
						'facility_short_name'			 	    => $this->security->xss_clean($this->input->post('facility_short_name')),
						'AddressLine1'			 	            => $this->security->xss_clean($this->input->post('AddressLine1')),
						'City'			 	                    => $this->security->xss_clean($this->input->post('City')),
						'PinCode'			 	                => $this->security->xss_clean($this->input->post('PinCode')),
						'Phone1'			 	                => $this->security->xss_clean($this->input->post('Phone1')),
						'FacilityNumber'						=>$this->security->xss_clean($this->input->post('countoffacility')),
						'email'			 	                    => $this->security->xss_clean($this->input->post('email')),
						);
					$this->Common_Model->update_data('mstfacility', $updateArray, 'id_mstfacility', $id_mstfacility);
					$this->session->set_flashdata('tr_msg', 'Facility Successfully Updated.');

					//log for update facility					
					$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$id_mstfacility,'Update','Facility Successfully Updated');
					
					redirect("facilities");
				}
			else
			{
				$insertArray = array(
				'id_mststate'							 =>  $this->security->xss_clean($this->input->post('input_state')),
				'id_mstdistrict'       	                 => $this->security->xss_clean($this->input->post('district')),
				'is_Mtc'       	                		 => $this->security->xss_clean($this->input->post('delivery_level')),
				'FacilityCode'		 		             => $this->security->xss_clean($this->input->post('facility_code')),
				'FacilityType' 				             => $this->security->xss_clean($this->input->post('facility_type')),
				'facility_short_name' 				     => $this->security->xss_clean($this->input->post('facility_short_name')),
				'AddressLine1' 				             => $this->security->xss_clean($this->input->post('AddressLine1')),
				'City' 				                     => $this->security->xss_clean($this->input->post('City')),
				'PinCode' 				                 => $this->security->xss_clean($this->input->post('PinCode')),
				'Phone1' 				                 => $this->security->xss_clean($this->input->post('Phone1')),
				'ActiveFrom'							 => date('Y-m-d'),
				'FacilityNumber'						 => $this->security->xss_clean($this->input->post('countoffacility')),
				'email' 				                 => $this->security->xss_clean($this->input->post('email')),
				);
				
			$insert_id = $this->Common_Model->insert_data('mstfacility', $insertArray);
			 
			$this->session->set_flashdata('tr_msg', 'Facility Successfully Created');			
			$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$insert_id,'Create','Facility Successfully Created');
			redirect("facilities");

			}
		}
	}

		if($id_mstfacility != null)
		{
			$sql_details = "select * from mstfacility where id_mstfacility = ?";
			//$res_details = $this->Common_Model->query_data($sql_details)[0];
			$res_details = $this->db->query($sql_details,[$id_mstfacility])->result();
			$content['facility_details'] = $res_details[0];

			$sql_facility_contact = "select * from mstfacilitycontacts where id_mstfacility = ?";
			//$res_facility_contact = $this->Common_Model->query_data($sql_facility_contact);
			$res_facility_contact = $this->db->query($sql_facility_contact,[$id_mstfacility])->result();
			$content['facility_contact'] = $res_facility_contact;

			$content['edit_flag'] = 1;
		}

		if( ($loginData) && $loginData->user_type == '1' ){
				$sess_where = " 1 ";
				$sess_whered = " 1 ";
				$stateid_where = "where 1";
				
			}
		elseif( ($loginData) && $loginData->user_type == '2' ){

				$sess_where = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."' AND id_mstfacility='".$loginData->id_mstfacility."'";
				$sess_whered = " id_mststate = '".$loginData->State_ID."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				
			}
		elseif( ($loginData) && $loginData->user_type == '3' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."'";
				$sess_whered = " id_mststate = '".$loginData->State_ID."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				
			}
		elseif( ($loginData) && $loginData->user_type == '4' ){ 
				$sess_where = " id_mststate = '".$loginData->State_ID."' AND id_mstdistrict ='".$loginData->DistrictID."'";
				$sess_whered = " id_mststate = '".$loginData->State_ID."'";
				$stateid_where = "where us.State_ID = '".$loginData->State_ID."'";
				
			}

			$sql_facility = "select * from mstfacility where ".$sess_where."";
			$res = $this->Common_Model->query_data($sql_facility);
			$content['facility_list'] = $res;
			
			$sql = "select mst.id_mststate,mst.StateName from mststate mst left join tblusers us on us.State_ID=mst.id_mststate ".$stateid_where." group by id_mststate  order by StateName";
			$content['states'] = $this->db->query($sql)->result();
			
			$sql_district = "select * from mstdistrict where ".$sess_whered."";
			$res_district = $this->Common_Model->query_data($sql_district);
			$content['district_list'] = $res_district;

		$content['subview'] = "list_facility";
		$this->load->view('admin/main_layout', $content);
	}

	public function delete($id_mstfacility = null)
	{	

		$loginData = $this->session->userdata('loginData');
		$Session_StateID = $loginData->State_ID;

		$sqlval = "select * from tblusers where id_mstfacility = ?";
		$mstfacmtctc = $this->db->query($sqlval,[$id_mstfacility])->result();
		
		if(count($mstfacmtctc) >0){
			$this->session->set_flashdata('er_msg', 'You dont have permission to delete this facility');
			redirect("facilities");
		}else{

		$sql = "DELETE from mstfacility where id_mstfacility = ? AND id_mststate=?";
		$this->session->set_flashdata('er_msg', 'Facility Successfully Deleted.');
		$this->db->query($sql,[$id_mstfacility,$Session_StateID]);
		
		$this->Log4php_model->log(__CLASS__.'/'.__FUNCTION__,$id_mstfacility,'Delete','Facility Successfully Deleted');
		redirect("facilities");
	}
		
	}

	public function get_facility_contact_details($facility_id, $contact_id)
	{
		$sql = "select Name from mstfacilitycontacts where id_mstfacilitycontacts = ".$contact_id;
		$res = $this->Common_Model->query_data($sql)[0];

		echo json_encode($res);
	}

	public function delete_facility_contact_details($facility_id, $contact_id)
	{
		$sql_del = "delete from mstfacilitycontacts where id_mstfacilitycontacts = ".$contact_id;
		$this->Common_Model->update_data_sql($sql_del);

		$sql_facility_contact = "select * from mstfacilitycontacts where id_mstfacility = ".$facility_id;
			$res_facility_contact = $this->Common_Model->query_data($sql_facility_contact);
		echo json_encode($res_facility_contact);
	}

	public function add_facility_contact_details($facility_id)
	{
		$name = $this->security->xss_clean($this->input->post('name'));
		$contact_edit_flag = $this->security->xss_clean($this->input->post('contact_edit_flag'));

		if($contact_edit_flag == 0)
		{

		$insert_array = array(
			"id_mstfacility" => $facility_id,
			"Name" => $name,
			);
		
		$res = $this->Common_Model->insert_data('mstfacilitycontacts',$insert_array);
		}
		else if($contact_edit_flag == 1)
		{
			$contact_id = $this->security->xss_clean($this->input->post('contact_id'));

			$update_array = array(
				"id_mstfacility" => $facility_id,
				"Name" => $name,
				);
			
			$res = $this->Common_Model->update_data('mstfacilitycontacts',$update_array, 'id_mstfacilitycontacts', $contact_id);
		}

		$sql_facility_contact = "select * from mstfacilitycontacts where id_mstfacility = ".$facility_id;
			$res_facility_contact = $this->Common_Model->query_data($sql_facility_contact);
		echo json_encode($res_facility_contact);
	}

	
}