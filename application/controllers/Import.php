<?php 
 
defined('BASEPATH') OR exit('No direct script access allowed');  
  
class Import extends CI_Controller {  
 
	function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Import_modelexcel', 'import');
    }
      
function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
}
  public function uploadCsv(){
    
    $content['subview'] = "abc";
  $this->load->view('pages/main_layout', $content);
  }
  public function uploadData(){
 error_reporting(0);
  if ($this->input->post('submit')) {
            
            $path = 'uploads/';
            require_once APPPATH . "/third_party/PHPExcel.php";
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'xlsx|xls';
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);            
            if (!$this->upload->do_upload('uploadFile')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            if(empty($error)){
              if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
            } else {
                $import_xls_file = 0;
            }
            $inputFileName = $path . $import_xls_file;
            
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $elements = array_splice($allDataInSheet, 6, 200);

           

                

                foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    //echo 'WorkSheet' . $CurrentWorkSheetIndex++ . "\n";
    //echo 'Worksheet number - ', $objPHPExcel->getIndex($worksheet), PHP_EOL;
                 //echo 'Worksheet number - ', $objPHPExcel->getIndex($worksheet), PHP_EOL;
    $highestRow = $worksheet->getHighestDataRow();
    $highestColumn = $worksheet->getHighestDataColumn();
    $headings = $worksheet->rangeToArray('A1:' . $highestColumn . 1,
        NULL,
        TRUE,
        FALSE);

    for ($row = 2; $row <= $highestRow; $row++) {

        $rowData = $worksheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
        $rowData[0] = array_combine($headings[0], $rowData[0]);
       // echo "<pre>"; print_r($rowData);
        foreach ($rowData as $key => $value) {
        //echo "<pre>"; print_r($rowData);

        $value['PRADAN'];
         $CalenderYear = chop($value['PRADAN'],"HOLIDAYLISTIMPORTTEMPLATE CalenderYear HOLIDAYLISTIMPORT CalenderYear");
    $years= date("Y");
    $CalenderYear_array = (array_filter(explode(" ",$CalenderYear)));
        foreach ($CalenderYear_array as $element) {

                $v = trim($element);
                
                if(is_numeric($v) && strlen($v)==4 ){
                    
                }elseif(is_numeric($v)){
                   echo var_export($element, true) . ' Calendar year should be 4 digits<br>';
                    return false;
                }
              
                else{
                    echo var_export($element, true) . " Calender Year is NOT numeric , Please enter numeric value", PHP_EOL;
                     return false;
                }
                
                 if( ($v) <= ($years)){
               
                  echo var_export($element, true) . " Calendar year should be greater than current year", PHP_EOL;
                    return false;
                }
               
    }
         
      //$GUIDELINES = (array_filter(explode(" ",$value['GUIDELINES'])));
          
      $Office_ID = (array_filter(explode(" ",$value['Office ID'])));

       $Office_array[] = (array_filter(explode(" ",$value['Office ID'])));

      foreach ($Office_ID as $element) {
            if (is_numeric($element)) {

            } else {
              echo var_export($element, true) . "Office ID is NOT numeric", PHP_EOL;
              return false;
              }
      }

 $Holiday_ID = (array_filter(explode(" ",$value['Holiday ID'])));

 $Holiday_array[] = (array_filter(explode(" ",$value['Holiday ID'])));

        foreach ($Holiday_ID as $element) {
         if (is_numeric($element)) {
        
    } else {
        echo var_export($element, true) . "Holiday ID is NOT numeric", PHP_EOL;
        return false;
    }
}

 }


    }

}


                $flag = true;
                $i=0;
                foreach ($elements as $value) {


  $Office_check = chop($value['B'],"Office Id");
  $officeid_conv_a = array_filter(explode(" ",$Office_check));

             if (in_array($officeid_conv_a, $Office_array)) 
                { 
                //echo "found"; 
                } 
              else
                { 
                echo "Office Id not matched Office List."; 
                return false;
                }
   //Holiday             
  $Holiday_check = chop($value['C'],"Holiday ID");

  $Holiday_conv_a = array_filter(explode(" ",$Holiday_check));

             if (in_array($Holiday_conv_a, $Holiday_array)) 
                { 
                //echo "found"; 
                } 
              else
                { 
                echo "Holiday Id not matched Holiday Type."; 
                return false;
                }  

               
       $Holiday_date = ($value['D']);
      

    if($Holiday_date=='Holiday date (YYYY-MM-DD)'){
      $dataformat= '2020-01-01';
    }else{
      $dataformat = $Holiday_date;
    }
  

  $dateformatingval =   ($dataformat);


if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$dateformatingval)) {
    //return true;
} else {
    echo "Invalid date - please ensure you provide a date in YYYY-MM-DD format.\n";
          return false;
}



                  if($flag){
                    $flag =false;
                    continue;
                  }
                  $inserdata[$i]['year'] = $value['A'];
                  $inserdata[$i]['officeid'] = $value['B'];
                  $inserdata[$i]['holidayid'] = $value['C'];
                   $inserdata[$i]['date'] = date('Y-m-d', strtotime($value['D']));
                 
                  $i++;
                }               
                $result = $this->import->importdata($inserdata);   
                if($result){
                  echo "Imported successfully";
                }else{
                  echo "ERROR !";
                }             
 
          } catch (Exception $e) {
               die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' .$e->getMessage());
            }
          }else{
              echo $error['error'];
            }
            
            
    }
   $content['subview'] = "abc";
  $this->load->view('pages/main_layout', $content);
  }
}
?>